.class public Landroid/text/util/Linkify;
.super Ljava/lang/Object;
.source "Linkify.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/util/Linkify$TransformFilter;,
        Landroid/text/util/Linkify$MatchFilter;
    }
.end annotation


# static fields
.field public static final ALL:I = 0xf

.field public static final EMAIL_ADDRESSES:I = 0x2

.field public static final MAP_ADDRESSES:I = 0x8

.field public static final PHONE_NUMBERS:I = 0x4

.field private static final PHONE_NUMBER_MINIMUM_DIGITS:I = 0x5

.field public static final WEB_URLS:I = 0x1

.field public static final isILCountry:Z

.field public static final sPhoneNumberMatchFilter:Landroid/text/util/Linkify$MatchFilter;

.field public static final sPhoneNumberTransformFilter:Landroid/text/util/Linkify$TransformFilter;

.field public static final sUrlMatchFilter:Landroid/text/util/Linkify$MatchFilter;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 94
    const-string v0, "IL"

    #@2
    const-string/jumbo v1, "ro.build.target_country"

    #@5
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    sput-boolean v0, Landroid/text/util/Linkify;->isILCountry:Z

    #@f
    .line 101
    new-instance v0, Landroid/text/util/Linkify$1;

    #@11
    invoke-direct {v0}, Landroid/text/util/Linkify$1;-><init>()V

    #@14
    sput-object v0, Landroid/text/util/Linkify;->sUrlMatchFilter:Landroid/text/util/Linkify$MatchFilter;

    #@16
    .line 119
    new-instance v0, Landroid/text/util/Linkify$2;

    #@18
    invoke-direct {v0}, Landroid/text/util/Linkify$2;-><init>()V

    #@1b
    sput-object v0, Landroid/text/util/Linkify;->sPhoneNumberMatchFilter:Landroid/text/util/Linkify$MatchFilter;

    #@1d
    .line 142
    new-instance v0, Landroid/text/util/Linkify$3;

    #@1f
    invoke-direct {v0}, Landroid/text/util/Linkify$3;-><init>()V

    #@22
    sput-object v0, Landroid/text/util/Linkify;->sPhoneNumberTransformFilter:Landroid/text/util/Linkify$TransformFilter;

    #@24
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 184
    return-void
.end method

.method private static final addLinkMovementMethod(Landroid/widget/TextView;)V
    .registers 3
    .parameter "t"

    #@0
    .prologue
    .line 294
    invoke-virtual {p0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    #@3
    move-result-object v0

    #@4
    .line 296
    .local v0, m:Landroid/text/method/MovementMethod;
    if-eqz v0, :cond_a

    #@6
    instance-of v1, v0, Landroid/text/method/LinkMovementMethod;

    #@8
    if-nez v1, :cond_17

    #@a
    .line 297
    :cond_a
    invoke-virtual {p0}, Landroid/widget/TextView;->getLinksClickable()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_17

    #@10
    .line 298
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    #@17
    .line 301
    :cond_17
    return-void
.end method

.method public static final addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V
    .registers 4
    .parameter "text"
    .parameter "pattern"
    .parameter "scheme"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 316
    invoke-static {p0, p1, p2, v0, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    #@4
    .line 317
    return-void
.end method

.method public static final addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V
    .registers 7
    .parameter "text"
    .parameter "p"
    .parameter "scheme"
    .parameter "matchFilter"
    .parameter "transformFilter"

    #@0
    .prologue
    .line 336
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@3
    move-result-object v1

    #@4
    invoke-static {v1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    #@7
    move-result-object v0

    #@8
    .line 338
    .local v0, s:Landroid/text/SpannableString;
    invoke-static {v0, p1, p2, p3, p4}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_14

    #@e
    .line 339
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@11
    .line 340
    invoke-static {p0}, Landroid/text/util/Linkify;->addLinkMovementMethod(Landroid/widget/TextView;)V

    #@14
    .line 342
    :cond_14
    return-void
.end method

.method public static final addLinks(Landroid/text/Spannable;I)Z
    .registers 12
    .parameter "text"
    .parameter "mask"

    #@0
    .prologue
    .line 205
    if-nez p1, :cond_4

    #@2
    .line 206
    const/4 v1, 0x0

    #@3
    .line 256
    :goto_3
    return v1

    #@4
    .line 209
    :cond_4
    const/4 v1, 0x0

    #@5
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@8
    move-result v2

    #@9
    const-class v3, Landroid/text/style/URLSpan;

    #@b
    invoke-interface {p0, v1, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@e
    move-result-object v9

    #@f
    check-cast v9, [Landroid/text/style/URLSpan;

    #@11
    .line 211
    .local v9, old:[Landroid/text/style/URLSpan;
    array-length v1, v9

    #@12
    add-int/lit8 v6, v1, -0x1

    #@14
    .local v6, i:I
    :goto_14
    if-ltz v6, :cond_1e

    #@16
    .line 212
    aget-object v1, v9, v6

    #@18
    invoke-interface {p0, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@1b
    .line 211
    add-int/lit8 v6, v6, -0x1

    #@1d
    goto :goto_14

    #@1e
    .line 215
    :cond_1e
    new-instance v0, Ljava/util/ArrayList;

    #@20
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@23
    .line 217
    .local v0, links:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/text/util/LinkSpec;>;"
    and-int/lit8 v1, p1, 0x1

    #@25
    if-eqz v1, :cond_43

    #@27
    .line 218
    sget-object v2, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    #@29
    const/4 v1, 0x3

    #@2a
    new-array v3, v1, [Ljava/lang/String;

    #@2c
    const/4 v1, 0x0

    #@2d
    const-string v4, "http://"

    #@2f
    aput-object v4, v3, v1

    #@31
    const/4 v1, 0x1

    #@32
    const-string v4, "https://"

    #@34
    aput-object v4, v3, v1

    #@36
    const/4 v1, 0x2

    #@37
    const-string/jumbo v4, "rtsp://"

    #@3a
    aput-object v4, v3, v1

    #@3c
    sget-object v4, Landroid/text/util/Linkify;->sUrlMatchFilter:Landroid/text/util/Linkify$MatchFilter;

    #@3e
    const/4 v5, 0x0

    #@3f
    move-object v1, p0

    #@40
    invoke-static/range {v0 .. v5}, Landroid/text/util/Linkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    #@43
    .line 223
    :cond_43
    and-int/lit8 v1, p1, 0x2

    #@45
    if-eqz v1, :cond_58

    #@47
    .line 224
    sget-object v2, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    #@49
    const/4 v1, 0x1

    #@4a
    new-array v3, v1, [Ljava/lang/String;

    #@4c
    const/4 v1, 0x0

    #@4d
    const-string/jumbo v4, "mailto:"

    #@50
    aput-object v4, v3, v1

    #@52
    const/4 v4, 0x0

    #@53
    const/4 v5, 0x0

    #@54
    move-object v1, p0

    #@55
    invoke-static/range {v0 .. v5}, Landroid/text/util/Linkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    #@58
    .line 229
    :cond_58
    and-int/lit8 v1, p1, 0x4

    #@5a
    if-eqz v1, :cond_85

    #@5c
    .line 230
    sget-object v2, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    #@5e
    const/4 v1, 0x1

    #@5f
    new-array v3, v1, [Ljava/lang/String;

    #@61
    const/4 v1, 0x0

    #@62
    const-string/jumbo v4, "tel:"

    #@65
    aput-object v4, v3, v1

    #@67
    sget-object v4, Landroid/text/util/Linkify;->sPhoneNumberMatchFilter:Landroid/text/util/Linkify$MatchFilter;

    #@69
    sget-object v5, Landroid/text/util/Linkify;->sPhoneNumberTransformFilter:Landroid/text/util/Linkify$TransformFilter;

    #@6b
    move-object v1, p0

    #@6c
    invoke-static/range {v0 .. v5}, Landroid/text/util/Linkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    #@6f
    .line 234
    sget-boolean v1, Landroid/text/util/Linkify;->isILCountry:Z

    #@71
    if-eqz v1, :cond_85

    #@73
    .line 235
    sget-object v2, Landroid/util/Patterns;->PHONE_WITHSTAR:Ljava/util/regex/Pattern;

    #@75
    const/4 v1, 0x1

    #@76
    new-array v3, v1, [Ljava/lang/String;

    #@78
    const/4 v1, 0x0

    #@79
    const-string/jumbo v4, "tel:"

    #@7c
    aput-object v4, v3, v1

    #@7e
    const/4 v4, 0x0

    #@7f
    sget-object v5, Landroid/text/util/Linkify;->sPhoneNumberTransformFilter:Landroid/text/util/Linkify$TransformFilter;

    #@81
    move-object v1, p0

    #@82
    invoke-static/range {v0 .. v5}, Landroid/text/util/Linkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    #@85
    .line 242
    :cond_85
    and-int/lit8 v1, p1, 0x8

    #@87
    if-eqz v1, :cond_8c

    #@89
    .line 243
    invoke-static {v0, p0}, Landroid/text/util/Linkify;->gatherMapLinks(Ljava/util/ArrayList;Landroid/text/Spannable;)V

    #@8c
    .line 246
    :cond_8c
    invoke-static {v0}, Landroid/text/util/Linkify;->pruneOverlaps(Ljava/util/ArrayList;)V

    #@8f
    .line 248
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@92
    move-result v1

    #@93
    if-nez v1, :cond_98

    #@95
    .line 249
    const/4 v1, 0x0

    #@96
    goto/16 :goto_3

    #@98
    .line 252
    :cond_98
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9b
    move-result-object v7

    #@9c
    .local v7, i$:Ljava/util/Iterator;
    :goto_9c
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@9f
    move-result v1

    #@a0
    if-eqz v1, :cond_b2

    #@a2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a5
    move-result-object v8

    #@a6
    check-cast v8, Landroid/text/util/LinkSpec;

    #@a8
    .line 253
    .local v8, link:Landroid/text/util/LinkSpec;
    iget-object v1, v8, Landroid/text/util/LinkSpec;->url:Ljava/lang/String;

    #@aa
    iget v2, v8, Landroid/text/util/LinkSpec;->start:I

    #@ac
    iget v3, v8, Landroid/text/util/LinkSpec;->end:I

    #@ae
    invoke-static {v1, v2, v3, p0}, Landroid/text/util/Linkify;->applyLink(Ljava/lang/String;IILandroid/text/Spannable;)V

    #@b1
    goto :goto_9c

    #@b2
    .line 256
    .end local v8           #link:Landroid/text/util/LinkSpec;
    :cond_b2
    const/4 v1, 0x1

    #@b3
    goto/16 :goto_3
.end method

.method public static final addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;)Z
    .registers 4
    .parameter "text"
    .parameter "pattern"
    .parameter "scheme"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 356
    invoke-static {p0, p1, p2, v0, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static final addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)Z
    .registers 15
    .parameter "s"
    .parameter "p"
    .parameter "scheme"
    .parameter "matchFilter"
    .parameter "transformFilter"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 376
    const/4 v2, 0x0

    #@2
    .line 377
    .local v2, hasMatches:Z
    if-nez p2, :cond_33

    #@4
    const-string v4, ""

    #@6
    .line 378
    .local v4, prefix:Ljava/lang/String;
    :goto_6
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@9
    move-result-object v3

    #@a
    .line 380
    .local v3, m:Ljava/util/regex/Matcher;
    :cond_a
    :goto_a
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    #@d
    move-result v7

    #@e
    if-eqz v7, :cond_38

    #@10
    .line 381
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    #@13
    move-result v5

    #@14
    .line 382
    .local v5, start:I
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    #@17
    move-result v1

    #@18
    .line 383
    .local v1, end:I
    const/4 v0, 0x1

    #@19
    .line 385
    .local v0, allowed:Z
    if-eqz p3, :cond_1f

    #@1b
    .line 386
    invoke-interface {p3, p0, v5, v1}, Landroid/text/util/Linkify$MatchFilter;->acceptMatch(Ljava/lang/CharSequence;II)Z

    #@1e
    move-result v0

    #@1f
    .line 389
    :cond_1f
    if-eqz v0, :cond_a

    #@21
    .line 390
    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@24
    move-result-object v7

    #@25
    const/4 v8, 0x1

    #@26
    new-array v8, v8, [Ljava/lang/String;

    #@28
    aput-object v4, v8, v9

    #@2a
    invoke-static {v7, v8, v3, p4}, Landroid/text/util/Linkify;->makeUrl(Ljava/lang/String;[Ljava/lang/String;Ljava/util/regex/Matcher;Landroid/text/util/Linkify$TransformFilter;)Ljava/lang/String;

    #@2d
    move-result-object v6

    #@2e
    .line 393
    .local v6, url:Ljava/lang/String;
    invoke-static {v6, v5, v1, p0}, Landroid/text/util/Linkify;->applyLink(Ljava/lang/String;IILandroid/text/Spannable;)V

    #@31
    .line 394
    const/4 v2, 0x1

    #@32
    goto :goto_a

    #@33
    .line 377
    .end local v0           #allowed:Z
    .end local v1           #end:I
    .end local v3           #m:Ljava/util/regex/Matcher;
    .end local v4           #prefix:Ljava/lang/String;
    .end local v5           #start:I
    .end local v6           #url:Ljava/lang/String;
    :cond_33
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    goto :goto_6

    #@38
    .line 398
    .restart local v3       #m:Ljava/util/regex/Matcher;
    .restart local v4       #prefix:Ljava/lang/String;
    :cond_38
    return v2
.end method

.method public static final addLinks(Landroid/widget/TextView;I)Z
    .registers 7
    .parameter "text"
    .parameter "mask"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 266
    if-nez p1, :cond_5

    #@4
    .line 289
    :cond_4
    :goto_4
    return v2

    #@5
    .line 270
    :cond_5
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@8
    move-result-object v1

    #@9
    .line 272
    .local v1, t:Ljava/lang/CharSequence;
    instance-of v4, v1, Landroid/text/Spannable;

    #@b
    if-eqz v4, :cond_1a

    #@d
    .line 273
    check-cast v1, Landroid/text/Spannable;

    #@f
    .end local v1           #t:Ljava/lang/CharSequence;
    invoke-static {v1, p1}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_4

    #@15
    .line 274
    invoke-static {p0}, Landroid/text/util/Linkify;->addLinkMovementMethod(Landroid/widget/TextView;)V

    #@18
    move v2, v3

    #@19
    .line 275
    goto :goto_4

    #@1a
    .line 280
    .restart local v1       #t:Ljava/lang/CharSequence;
    :cond_1a
    invoke-static {v1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    #@1d
    move-result-object v0

    #@1e
    .line 282
    .local v0, s:Landroid/text/SpannableString;
    invoke-static {v0, p1}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    #@21
    move-result v4

    #@22
    if-eqz v4, :cond_4

    #@24
    .line 283
    invoke-static {p0}, Landroid/text/util/Linkify;->addLinkMovementMethod(Landroid/widget/TextView;)V

    #@27
    .line 284
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@2a
    move v2, v3

    #@2b
    .line 286
    goto :goto_4
.end method

.method private static final applyLink(Ljava/lang/String;IILandroid/text/Spannable;)V
    .registers 6
    .parameter "url"
    .parameter "start"
    .parameter "end"
    .parameter "text"

    #@0
    .prologue
    .line 402
    new-instance v0, Landroid/text/style/URLSpan;

    #@2
    invoke-direct {v0, p0}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    #@5
    .line 404
    .local v0, span:Landroid/text/style/URLSpan;
    const/16 v1, 0x21

    #@7
    invoke-interface {p3, v0, p1, p2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@a
    .line 405
    return-void
.end method

.method private static final gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V
    .registers 12
    .parameter
    .parameter "s"
    .parameter "pattern"
    .parameter "schemes"
    .parameter "matchFilter"
    .parameter "transformFilter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/text/util/LinkSpec;",
            ">;",
            "Landroid/text/Spannable;",
            "Ljava/util/regex/Pattern;",
            "[",
            "Ljava/lang/String;",
            "Landroid/text/util/Linkify$MatchFilter;",
            "Landroid/text/util/Linkify$TransformFilter;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 440
    .local p0, links:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/text/util/LinkSpec;>;"
    invoke-virtual {p2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@3
    move-result-object v1

    #@4
    .line 442
    .local v1, m:Ljava/util/regex/Matcher;
    :cond_4
    :goto_4
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_32

    #@a
    .line 443
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    #@d
    move-result v3

    #@e
    .line 444
    .local v3, start:I
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    #@11
    move-result v0

    #@12
    .line 446
    .local v0, end:I
    if-eqz p4, :cond_1a

    #@14
    invoke-interface {p4, p1, v3, v0}, Landroid/text/util/Linkify$MatchFilter;->acceptMatch(Ljava/lang/CharSequence;II)Z

    #@17
    move-result v5

    #@18
    if-eqz v5, :cond_4

    #@1a
    .line 447
    :cond_1a
    new-instance v2, Landroid/text/util/LinkSpec;

    #@1c
    invoke-direct {v2}, Landroid/text/util/LinkSpec;-><init>()V

    #@1f
    .line 448
    .local v2, spec:Landroid/text/util/LinkSpec;
    const/4 v5, 0x0

    #@20
    invoke-virtual {v1, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@23
    move-result-object v5

    #@24
    invoke-static {v5, p3, v1, p5}, Landroid/text/util/Linkify;->makeUrl(Ljava/lang/String;[Ljava/lang/String;Ljava/util/regex/Matcher;Landroid/text/util/Linkify$TransformFilter;)Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    .line 450
    .local v4, url:Ljava/lang/String;
    iput-object v4, v2, Landroid/text/util/LinkSpec;->url:Ljava/lang/String;

    #@2a
    .line 451
    iput v3, v2, Landroid/text/util/LinkSpec;->start:I

    #@2c
    .line 452
    iput v0, v2, Landroid/text/util/LinkSpec;->end:I

    #@2e
    .line 454
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@31
    goto :goto_4

    #@32
    .line 457
    .end local v0           #end:I
    .end local v2           #spec:Landroid/text/util/LinkSpec;
    .end local v3           #start:I
    .end local v4           #url:Ljava/lang/String;
    :cond_32
    return-void
.end method

.method private static final gatherMapLinks(Ljava/util/ArrayList;Landroid/text/Spannable;)V
    .registers 13
    .parameter
    .parameter "s"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/text/util/LinkSpec;",
            ">;",
            "Landroid/text/Spannable;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 460
    .local p0, links:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/text/util/LinkSpec;>;"
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v8

    #@4
    .line 462
    .local v8, string:Ljava/lang/String;
    const/4 v1, 0x0

    #@5
    .line 464
    .local v1, base:I
    :goto_5
    invoke-static {v8}, Landroid/webkit/WebView;->findAddress(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .local v0, address:Ljava/lang/String;
    if-eqz v0, :cond_11

    #@b
    .line 465
    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@e
    move-result v7

    #@f
    .line 467
    .local v7, start:I
    if-gez v7, :cond_12

    #@11
    .line 491
    .end local v7           #start:I
    :cond_11
    return-void

    #@12
    .line 471
    .restart local v7       #start:I
    :cond_12
    new-instance v6, Landroid/text/util/LinkSpec;

    #@14
    invoke-direct {v6}, Landroid/text/util/LinkSpec;-><init>()V

    #@17
    .line 472
    .local v6, spec:Landroid/text/util/LinkSpec;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@1a
    move-result v5

    #@1b
    .line 473
    .local v5, length:I
    add-int v4, v7, v5

    #@1d
    .line 475
    .local v4, end:I
    add-int v9, v1, v7

    #@1f
    iput v9, v6, Landroid/text/util/LinkSpec;->start:I

    #@21
    .line 476
    add-int v9, v1, v4

    #@23
    iput v9, v6, Landroid/text/util/LinkSpec;->end:I

    #@25
    .line 477
    invoke-virtual {v8, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@28
    move-result-object v8

    #@29
    .line 478
    add-int/2addr v1, v4

    #@2a
    .line 480
    const/4 v3, 0x0

    #@2b
    .line 483
    .local v3, encodedAddress:Ljava/lang/String;
    :try_start_2b
    const-string v9, "UTF-8"

    #@2d
    invoke-static {v0, v9}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_30
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2b .. :try_end_30} :catch_4a

    #@30
    move-result-object v3

    #@31
    .line 488
    new-instance v9, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v10, "geo:0,0?q="

    #@38
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v9

    #@3c
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v9

    #@40
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v9

    #@44
    iput-object v9, v6, Landroid/text/util/LinkSpec;->url:Ljava/lang/String;

    #@46
    .line 489
    invoke-virtual {p0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@49
    goto :goto_5

    #@4a
    .line 484
    :catch_4a
    move-exception v2

    #@4b
    .line 485
    .local v2, e:Ljava/io/UnsupportedEncodingException;
    goto :goto_5
.end method

.method private static final makeUrl(Ljava/lang/String;[Ljava/lang/String;Ljava/util/regex/Matcher;Landroid/text/util/Linkify$TransformFilter;)Ljava/lang/String;
    .registers 13
    .parameter "url"
    .parameter "prefixes"
    .parameter "m"
    .parameter "filter"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 409
    if-eqz p3, :cond_7

    #@3
    .line 410
    invoke-interface {p3, p2, p0}, Landroid/text/util/Linkify$TransformFilter;->transformUrl(Ljava/util/regex/Matcher;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object p0

    #@7
    .line 413
    :cond_7
    const/4 v7, 0x0

    #@8
    .line 415
    .local v7, hasPrefix:Z
    const/4 v8, 0x0

    #@9
    .local v8, i:I
    :goto_9
    array-length v0, p1

    #@a
    if-ge v8, v0, :cond_4c

    #@c
    .line 416
    const/4 v1, 0x1

    #@d
    aget-object v3, p1, v8

    #@f
    aget-object v0, p1, v8

    #@11
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@14
    move-result v5

    #@15
    move-object v0, p0

    #@16
    move v4, v2

    #@17
    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_62

    #@1d
    .line 418
    const/4 v7, 0x1

    #@1e
    .line 421
    aget-object v4, p1, v8

    #@20
    aget-object v0, p1, v8

    #@22
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@25
    move-result v6

    #@26
    move-object v1, p0

    #@27
    move v3, v2

    #@28
    move v5, v2

    #@29
    invoke-virtual/range {v1 .. v6}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    #@2c
    move-result v0

    #@2d
    if-nez v0, :cond_4c

    #@2f
    .line 423
    new-instance v0, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    aget-object v1, p1, v8

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v0

    #@3a
    aget-object v1, p1, v8

    #@3c
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@3f
    move-result v1

    #@40
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v0

    #@48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object p0

    #@4c
    .line 430
    :cond_4c
    if-nez v7, :cond_61

    #@4e
    .line 431
    new-instance v0, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    aget-object v1, p1, v2

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object p0

    #@61
    .line 434
    :cond_61
    return-object p0

    #@62
    .line 415
    :cond_62
    add-int/lit8 v8, v8, 0x1

    #@64
    goto :goto_9
.end method

.method private static final pruneOverlaps(Ljava/util/ArrayList;)V
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/text/util/LinkSpec;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 494
    .local p0, links:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/text/util/LinkSpec;>;"
    new-instance v2, Landroid/text/util/Linkify$4;

    #@2
    invoke-direct {v2}, Landroid/text/util/Linkify$4;-><init>()V

    #@5
    .line 520
    .local v2, c:Ljava/util/Comparator;,"Ljava/util/Comparator<Landroid/text/util/LinkSpec;>;"
    invoke-static {p0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@8
    .line 522
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v4

    #@c
    .line 523
    .local v4, len:I
    const/4 v3, 0x0

    #@d
    .line 525
    .local v3, i:I
    :goto_d
    add-int/lit8 v6, v4, -0x1

    #@f
    if-ge v3, v6, :cond_5d

    #@11
    .line 526
    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/text/util/LinkSpec;

    #@17
    .line 527
    .local v0, a:Landroid/text/util/LinkSpec;
    add-int/lit8 v6, v3, 0x1

    #@19
    invoke-virtual {p0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Landroid/text/util/LinkSpec;

    #@1f
    .line 528
    .local v1, b:Landroid/text/util/LinkSpec;
    const/4 v5, -0x1

    #@20
    .line 530
    .local v5, remove:I
    iget v6, v0, Landroid/text/util/LinkSpec;->start:I

    #@22
    iget v7, v1, Landroid/text/util/LinkSpec;->start:I

    #@24
    if-gt v6, v7, :cond_5a

    #@26
    iget v6, v0, Landroid/text/util/LinkSpec;->end:I

    #@28
    iget v7, v1, Landroid/text/util/LinkSpec;->start:I

    #@2a
    if-le v6, v7, :cond_5a

    #@2c
    .line 531
    iget v6, v1, Landroid/text/util/LinkSpec;->end:I

    #@2e
    iget v7, v0, Landroid/text/util/LinkSpec;->end:I

    #@30
    if-gt v6, v7, :cond_3d

    #@32
    .line 532
    add-int/lit8 v5, v3, 0x1

    #@34
    .line 539
    :cond_34
    :goto_34
    const/4 v6, -0x1

    #@35
    if-eq v5, v6, :cond_5a

    #@37
    .line 540
    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@3a
    .line 541
    add-int/lit8 v4, v4, -0x1

    #@3c
    .line 542
    goto :goto_d

    #@3d
    .line 533
    :cond_3d
    iget v6, v0, Landroid/text/util/LinkSpec;->end:I

    #@3f
    iget v7, v0, Landroid/text/util/LinkSpec;->start:I

    #@41
    sub-int/2addr v6, v7

    #@42
    iget v7, v1, Landroid/text/util/LinkSpec;->end:I

    #@44
    iget v8, v1, Landroid/text/util/LinkSpec;->start:I

    #@46
    sub-int/2addr v7, v8

    #@47
    if-le v6, v7, :cond_4c

    #@49
    .line 534
    add-int/lit8 v5, v3, 0x1

    #@4b
    goto :goto_34

    #@4c
    .line 535
    :cond_4c
    iget v6, v0, Landroid/text/util/LinkSpec;->end:I

    #@4e
    iget v7, v0, Landroid/text/util/LinkSpec;->start:I

    #@50
    sub-int/2addr v6, v7

    #@51
    iget v7, v1, Landroid/text/util/LinkSpec;->end:I

    #@53
    iget v8, v1, Landroid/text/util/LinkSpec;->start:I

    #@55
    sub-int/2addr v7, v8

    #@56
    if-ge v6, v7, :cond_34

    #@58
    .line 536
    move v5, v3

    #@59
    goto :goto_34

    #@5a
    .line 547
    :cond_5a
    add-int/lit8 v3, v3, 0x1

    #@5c
    .line 548
    goto :goto_d

    #@5d
    .line 549
    .end local v0           #a:Landroid/text/util/LinkSpec;
    .end local v1           #b:Landroid/text/util/LinkSpec;
    .end local v5           #remove:I
    :cond_5d
    return-void
.end method
