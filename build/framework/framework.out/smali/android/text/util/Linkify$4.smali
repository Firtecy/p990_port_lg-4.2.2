.class final Landroid/text/util/Linkify$4;
.super Ljava/lang/Object;
.source "Linkify.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/text/util/Linkify;->pruneOverlaps(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/text/util/LinkSpec;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 494
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public final compare(Landroid/text/util/LinkSpec;Landroid/text/util/LinkSpec;)I
    .registers 7
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, -0x1

    #@2
    .line 496
    iget v2, p1, Landroid/text/util/LinkSpec;->start:I

    #@4
    iget v3, p2, Landroid/text/util/LinkSpec;->start:I

    #@6
    if-ge v2, v3, :cond_9

    #@8
    .line 512
    :cond_8
    :goto_8
    return v0

    #@9
    .line 500
    :cond_9
    iget v2, p1, Landroid/text/util/LinkSpec;->start:I

    #@b
    iget v3, p2, Landroid/text/util/LinkSpec;->start:I

    #@d
    if-le v2, v3, :cond_11

    #@f
    move v0, v1

    #@10
    .line 501
    goto :goto_8

    #@11
    .line 504
    :cond_11
    iget v2, p1, Landroid/text/util/LinkSpec;->end:I

    #@13
    iget v3, p2, Landroid/text/util/LinkSpec;->end:I

    #@15
    if-ge v2, v3, :cond_19

    #@17
    move v0, v1

    #@18
    .line 505
    goto :goto_8

    #@19
    .line 508
    :cond_19
    iget v1, p1, Landroid/text/util/LinkSpec;->end:I

    #@1b
    iget v2, p2, Landroid/text/util/LinkSpec;->end:I

    #@1d
    if-gt v1, v2, :cond_8

    #@1f
    .line 512
    const/4 v0, 0x0

    #@20
    goto :goto_8
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 494
    check-cast p1, Landroid/text/util/LinkSpec;

    #@2
    .end local p1
    check-cast p2, Landroid/text/util/LinkSpec;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Landroid/text/util/Linkify$4;->compare(Landroid/text/util/LinkSpec;Landroid/text/util/LinkSpec;)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 516
    const/4 v0, 0x0

    #@1
    return v0
.end method
