.class public Landroid/text/util/Rfc822Token;
.super Ljava/lang/Object;
.source "Rfc822Token.java"


# instance fields
.field private mAddress:Ljava/lang/String;

.field private mComment:Ljava/lang/String;

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "name"
    .parameter "address"
    .parameter "comment"

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 31
    iput-object p1, p0, Landroid/text/util/Rfc822Token;->mName:Ljava/lang/String;

    #@5
    .line 32
    iput-object p2, p0, Landroid/text/util/Rfc822Token;->mAddress:Ljava/lang/String;

    #@7
    .line 33
    iput-object p3, p0, Landroid/text/util/Rfc822Token;->mComment:Ljava/lang/String;

    #@9
    .line 34
    return-void
.end method

.method public static quoteComment(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "comment"

    #@0
    .prologue
    const/16 v5, 0x5c

    #@2
    .line 156
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v2

    #@6
    .line 157
    .local v2, len:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    .line 159
    .local v3, sb:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v2, :cond_25

    #@e
    .line 160
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@11
    move-result v0

    #@12
    .line 162
    .local v0, c:C
    const/16 v4, 0x28

    #@14
    if-eq v0, v4, :cond_1c

    #@16
    const/16 v4, 0x29

    #@18
    if-eq v0, v4, :cond_1c

    #@1a
    if-ne v0, v5, :cond_1f

    #@1c
    .line 163
    :cond_1c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    .line 166
    :cond_1f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@22
    .line 159
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_c

    #@25
    .line 169
    .end local v0           #c:C
    :cond_25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    return-object v4
.end method

.method public static quoteName(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "name"

    #@0
    .prologue
    const/16 v5, 0x5c

    #@2
    .line 134
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 136
    .local v3, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@a
    move-result v2

    #@b
    .line 137
    .local v2, len:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v2, :cond_21

    #@e
    .line 138
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@11
    move-result v0

    #@12
    .line 140
    .local v0, c:C
    if-eq v0, v5, :cond_18

    #@14
    const/16 v4, 0x22

    #@16
    if-ne v0, v4, :cond_1b

    #@18
    .line 141
    :cond_18
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1b
    .line 144
    :cond_1b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1e
    .line 137
    add-int/lit8 v1, v1, 0x1

    #@20
    goto :goto_c

    #@21
    .line 147
    .end local v0           #c:C
    :cond_21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    return-object v4
.end method

.method public static quoteNameIfNecessary(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "name"

    #@0
    .prologue
    const/16 v5, 0x22

    #@2
    .line 112
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v2

    #@6
    .line 114
    .local v2, len:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v2, :cond_42

    #@9
    .line 115
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v0

    #@d
    .line 117
    .local v0, c:C
    const/16 v3, 0x41

    #@f
    if-lt v0, v3, :cond_15

    #@11
    const/16 v3, 0x5a

    #@13
    if-le v0, v3, :cond_43

    #@15
    :cond_15
    const/16 v3, 0x61

    #@17
    if-lt v0, v3, :cond_1d

    #@19
    const/16 v3, 0x7a

    #@1b
    if-le v0, v3, :cond_43

    #@1d
    :cond_1d
    const/16 v3, 0x20

    #@1f
    if-eq v0, v3, :cond_43

    #@21
    const/16 v3, 0x30

    #@23
    if-lt v0, v3, :cond_29

    #@25
    const/16 v3, 0x39

    #@27
    if-le v0, v3, :cond_43

    #@29
    .line 121
    :cond_29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-static {p0}, Landroid/text/util/Rfc822Token;->quoteName(Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object p0

    #@42
    .line 125
    .end local v0           #c:C
    .end local p0
    :cond_42
    return-object p0

    #@43
    .line 114
    .restart local v0       #c:C
    .restart local p0
    :cond_43
    add-int/lit8 v1, v1, 0x1

    #@45
    goto :goto_7
.end method

.method private static stringEquals(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 181
    if-nez p0, :cond_8

    #@2
    .line 182
    if-nez p1, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    .line 184
    :goto_5
    return v0

    #@6
    .line 182
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5

    #@8
    .line 184
    :cond_8
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 189
    instance-of v2, p1, Landroid/text/util/Rfc822Token;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 193
    :cond_5
    :goto_5
    return v1

    #@6
    :cond_6
    move-object v0, p1

    #@7
    .line 192
    check-cast v0, Landroid/text/util/Rfc822Token;

    #@9
    .line 193
    .local v0, other:Landroid/text/util/Rfc822Token;
    iget-object v2, p0, Landroid/text/util/Rfc822Token;->mName:Ljava/lang/String;

    #@b
    iget-object v3, v0, Landroid/text/util/Rfc822Token;->mName:Ljava/lang/String;

    #@d
    invoke-static {v2, v3}, Landroid/text/util/Rfc822Token;->stringEquals(Ljava/lang/String;Ljava/lang/String;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_5

    #@13
    iget-object v2, p0, Landroid/text/util/Rfc822Token;->mAddress:Ljava/lang/String;

    #@15
    iget-object v3, v0, Landroid/text/util/Rfc822Token;->mAddress:Ljava/lang/String;

    #@17
    invoke-static {v2, v3}, Landroid/text/util/Rfc822Token;->stringEquals(Ljava/lang/String;Ljava/lang/String;)Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_5

    #@1d
    iget-object v2, p0, Landroid/text/util/Rfc822Token;->mComment:Ljava/lang/String;

    #@1f
    iget-object v3, v0, Landroid/text/util/Rfc822Token;->mComment:Ljava/lang/String;

    #@21
    invoke-static {v2, v3}, Landroid/text/util/Rfc822Token;->stringEquals(Ljava/lang/String;Ljava/lang/String;)Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_5

    #@27
    const/4 v1, 0x1

    #@28
    goto :goto_5
.end method

.method public getAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Landroid/text/util/Rfc822Token;->mAddress:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getComment()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/text/util/Rfc822Token;->mComment:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 40
    iget-object v0, p0, Landroid/text/util/Rfc822Token;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 173
    const/16 v0, 0x11

    #@2
    .line 174
    .local v0, result:I
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mName:Ljava/lang/String;

    #@4
    if-eqz v1, :cond_e

    #@6
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mName:Ljava/lang/String;

    #@8
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@b
    move-result v1

    #@c
    add-int/lit16 v0, v1, 0x20f

    #@e
    .line 175
    :cond_e
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mAddress:Ljava/lang/String;

    #@10
    if-eqz v1, :cond_1c

    #@12
    mul-int/lit8 v1, v0, 0x1f

    #@14
    iget-object v2, p0, Landroid/text/util/Rfc822Token;->mAddress:Ljava/lang/String;

    #@16
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    #@19
    move-result v2

    #@1a
    add-int v0, v1, v2

    #@1c
    .line 176
    :cond_1c
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mComment:Ljava/lang/String;

    #@1e
    if-eqz v1, :cond_2a

    #@20
    mul-int/lit8 v1, v0, 0x1f

    #@22
    iget-object v2, p0, Landroid/text/util/Rfc822Token;->mComment:Ljava/lang/String;

    #@24
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    #@27
    move-result v2

    #@28
    add-int v0, v1, v2

    #@2a
    .line 177
    :cond_2a
    return v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .registers 2
    .parameter "address"

    #@0
    .prologue
    .line 68
    iput-object p1, p0, Landroid/text/util/Rfc822Token;->mAddress:Ljava/lang/String;

    #@2
    .line 69
    return-void
.end method

.method public setComment(Ljava/lang/String;)V
    .registers 2
    .parameter "comment"

    #@0
    .prologue
    .line 75
    iput-object p1, p0, Landroid/text/util/Rfc822Token;->mComment:Ljava/lang/String;

    #@2
    .line 76
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 61
    iput-object p1, p0, Landroid/text/util/Rfc822Token;->mName:Ljava/lang/String;

    #@2
    .line 62
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 86
    .local v0, sb:Ljava/lang/StringBuilder;
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mName:Ljava/lang/String;

    #@7
    if-eqz v1, :cond_1f

    #@9
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mName:Ljava/lang/String;

    #@b
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_1f

    #@11
    .line 87
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mName:Ljava/lang/String;

    #@13
    invoke-static {v1}, Landroid/text/util/Rfc822Token;->quoteNameIfNecessary(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 88
    const/16 v1, 0x20

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    .line 91
    :cond_1f
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mComment:Ljava/lang/String;

    #@21
    if-eqz v1, :cond_3e

    #@23
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mComment:Ljava/lang/String;

    #@25
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_3e

    #@2b
    .line 92
    const/16 v1, 0x28

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@30
    .line 93
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mComment:Ljava/lang/String;

    #@32
    invoke-static {v1}, Landroid/text/util/Rfc822Token;->quoteComment(Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    .line 94
    const-string v1, ") "

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    .line 97
    :cond_3e
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mAddress:Ljava/lang/String;

    #@40
    if-eqz v1, :cond_59

    #@42
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mAddress:Ljava/lang/String;

    #@44
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@47
    move-result v1

    #@48
    if-eqz v1, :cond_59

    #@4a
    .line 98
    const/16 v1, 0x3c

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4f
    .line 99
    iget-object v1, p0, Landroid/text/util/Rfc822Token;->mAddress:Ljava/lang/String;

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    .line 100
    const/16 v1, 0x3e

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@59
    .line 103
    :cond_59
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    return-object v1
.end method
