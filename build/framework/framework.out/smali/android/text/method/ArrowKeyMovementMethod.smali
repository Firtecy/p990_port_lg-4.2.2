.class public Landroid/text/method/ArrowKeyMovementMethod;
.super Landroid/text/method/BaseMovementMethod;
.source "ArrowKeyMovementMethod.java"

# interfaces
.implements Landroid/text/method/MovementMethod;


# static fields
.field private static final LAST_TAP_DOWN:Ljava/lang/Object;

.field private static sInstance:Landroid/text/method/ArrowKeyMovementMethod;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 332
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/text/method/ArrowKeyMovementMethod;->LAST_TAP_DOWN:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/text/method/BaseMovementMethod;-><init>()V

    #@3
    return-void
.end method

.method private static getCurrentLineTop(Landroid/text/Spannable;Landroid/text/Layout;)I
    .registers 3
    .parameter "buffer"
    .parameter "layout"

    #@0
    .prologue
    .line 39
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@3
    move-result v0

    #@4
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    #@7
    move-result v0

    #@8
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public static getInstance()Landroid/text/method/MovementMethod;
    .registers 1

    #@0
    .prologue
    .line 325
    sget-object v0, Landroid/text/method/ArrowKeyMovementMethod;->sInstance:Landroid/text/method/ArrowKeyMovementMethod;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 326
    new-instance v0, Landroid/text/method/ArrowKeyMovementMethod;

    #@6
    invoke-direct {v0}, Landroid/text/method/ArrowKeyMovementMethod;-><init>()V

    #@9
    sput-object v0, Landroid/text/method/ArrowKeyMovementMethod;->sInstance:Landroid/text/method/ArrowKeyMovementMethod;

    #@b
    .line 329
    :cond_b
    sget-object v0, Landroid/text/method/ArrowKeyMovementMethod;->sInstance:Landroid/text/method/ArrowKeyMovementMethod;

    #@d
    return-object v0
.end method

.method private static getPageHeight(Landroid/widget/TextView;)I
    .registers 3
    .parameter "widget"

    #@0
    .prologue
    .line 46
    new-instance v0, Landroid/graphics/Rect;

    #@2
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@5
    .line 47
    .local v0, rect:Landroid/graphics/Rect;
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_10

    #@b
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@e
    move-result v1

    #@f
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method private static isSelecting(Landroid/text/Spannable;)Z
    .registers 3
    .parameter "buffer"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 34
    invoke-static {p0, v0}, Landroid/text/method/MetaKeyKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@4
    move-result v1

    #@5
    if-eq v1, v0, :cond_f

    #@7
    const/16 v1, 0x800

    #@9
    invoke-static {p0, v1}, Landroid/text/method/MetaKeyKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_10

    #@f
    :cond_f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method


# virtual methods
.method protected bottom(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 168
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 169
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    #@9
    move-result v0

    #@a
    invoke-static {p2, v0}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@d
    .line 173
    :goto_d
    const/4 v0, 0x1

    #@e
    return v0

    #@f
    .line 171
    :cond_f
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    #@12
    move-result v0

    #@13
    invoke-static {p2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@16
    goto :goto_d
.end method

.method public canSelectArbitrarily()Z
    .registers 2

    #@0
    .prologue
    .line 297
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected down(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 100
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v0

    #@4
    .line 101
    .local v0, layout:Landroid/text/Layout;
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 102
    invoke-static {p2, v0}, Landroid/text/Selection;->extendDown(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@d
    move-result v1

    #@e
    .line 104
    :goto_e
    return v1

    #@f
    :cond_f
    invoke-static {p2, v0}, Landroid/text/Selection;->moveDown(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@12
    move-result v1

    #@13
    goto :goto_e
.end method

.method protected end(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 221
    invoke-virtual {p0, p1, p2}, Landroid/text/method/ArrowKeyMovementMethod;->lineEnd(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected handleMovementKey(Landroid/widget/TextView;Landroid/text/Spannable;IILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "widget"
    .parameter "buffer"
    .parameter "keyCode"
    .parameter "movementMetaState"
    .parameter "event"

    #@0
    .prologue
    .line 53
    packed-switch p3, :pswitch_data_28

    #@3
    .line 65
    :cond_3
    invoke-super/range {p0 .. p5}, Landroid/text/method/BaseMovementMethod;->handleMovementKey(Landroid/widget/TextView;Landroid/text/Spannable;IILandroid/view/KeyEvent;)Z

    #@6
    move-result v0

    #@7
    :goto_7
    return v0

    #@8
    .line 55
    :pswitch_8
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_3

    #@e
    .line 56
    invoke-virtual {p5}, Landroid/view/KeyEvent;->getAction()I

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_3

    #@14
    invoke-virtual {p5}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_3

    #@1a
    const/16 v0, 0x800

    #@1c
    invoke-static {p2, v0}, Landroid/text/method/MetaKeyKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_3

    #@22
    .line 60
    invoke-virtual {p1}, Landroid/widget/TextView;->showContextMenu()Z

    #@25
    move-result v0

    #@26
    goto :goto_7

    #@27
    .line 53
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x17
        :pswitch_8
    .end packed-switch
.end method

.method protected home(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 216
    invoke-virtual {p0, p1, p2}, Landroid/text/method/ArrowKeyMovementMethod;->lineStart(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public initialize(Landroid/widget/TextView;Landroid/text/Spannable;)V
    .registers 4
    .parameter "widget"
    .parameter "text"

    #@0
    .prologue
    .line 302
    const/4 v0, 0x0

    #@1
    invoke-static {p2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@4
    .line 303
    return-void
.end method

.method protected left(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v0

    #@4
    .line 71
    .local v0, layout:Landroid/text/Layout;
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 72
    invoke-static {p2, v0}, Landroid/text/Selection;->extendLeft(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@d
    move-result v1

    #@e
    .line 74
    :goto_e
    return v1

    #@f
    :cond_f
    invoke-static {p2, v0}, Landroid/text/Selection;->moveLeft(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@12
    move-result v1

    #@13
    goto :goto_e
.end method

.method protected leftWord(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 6
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 199
    invoke-virtual {p1}, Landroid/widget/TextView;->getSelectionEnd()I

    #@3
    move-result v0

    #@4
    .line 200
    .local v0, selectionEnd:I
    invoke-virtual {p1}, Landroid/widget/TextView;->getWordIterator()Landroid/text/method/WordIterator;

    #@7
    move-result-object v1

    #@8
    .line 201
    .local v1, wordIterator:Landroid/text/method/WordIterator;
    invoke-virtual {v1, p2, v0, v0}, Landroid/text/method/WordIterator;->setCharSequence(Ljava/lang/CharSequence;II)V

    #@b
    .line 202
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@e
    move-result v2

    #@f
    invoke-static {p2, v1, v2}, Landroid/text/Selection;->moveToPreceding(Landroid/text/Spannable;Landroid/text/Selection$PositionIterator;Z)Z

    #@12
    move-result v2

    #@13
    return v2
.end method

.method protected lineEnd(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 188
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v0

    #@4
    .line 189
    .local v0, layout:Landroid/text/Layout;
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 190
    invoke-static {p2, v0}, Landroid/text/Selection;->extendToRightEdge(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@d
    move-result v1

    #@e
    .line 192
    :goto_e
    return v1

    #@f
    :cond_f
    invoke-static {p2, v0}, Landroid/text/Selection;->moveToRightEdge(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@12
    move-result v1

    #@13
    goto :goto_e
.end method

.method protected lineStart(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 178
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v0

    #@4
    .line 179
    .local v0, layout:Landroid/text/Layout;
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 180
    invoke-static {p2, v0}, Landroid/text/Selection;->extendToLeftEdge(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@d
    move-result v1

    #@e
    .line 182
    :goto_e
    return v1

    #@f
    :cond_f
    invoke-static {p2, v0}, Landroid/text/Selection;->moveToLeftEdge(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@12
    move-result v1

    #@13
    goto :goto_e
.end method

.method public onTakeFocus(Landroid/widget/TextView;Landroid/text/Spannable;I)V
    .registers 7
    .parameter "view"
    .parameter "text"
    .parameter "dir"

    #@0
    .prologue
    .line 307
    and-int/lit16 v1, p3, 0x82

    #@2
    if-eqz v1, :cond_30

    #@4
    .line 308
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@7
    move-result-object v1

    #@8
    if-nez v1, :cond_12

    #@a
    .line 310
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    #@d
    move-result v1

    #@e
    invoke-static {p2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@11
    .line 322
    :cond_11
    :goto_11
    return-void

    #@12
    .line 312
    :cond_12
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    #@19
    move-result v1

    #@1a
    if-lez v1, :cond_11

    #@1c
    .line 313
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@1f
    move-result v1

    #@20
    invoke-static {p2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@23
    move-result v2

    #@24
    sub-int v0, v1, v2

    #@26
    .line 314
    .local v0, selectionLength:I
    if-nez v0, :cond_11

    #@28
    .line 315
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    #@2b
    move-result v1

    #@2c
    invoke-static {p2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@2f
    goto :goto_11

    #@30
    .line 320
    .end local v0           #selectionLength:I
    :cond_30
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    #@33
    move-result v1

    #@34
    invoke-static {p2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@37
    goto :goto_11
.end method

.method public onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .registers 12
    .parameter "widget"
    .parameter "buffer"
    .parameter "event"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 226
    const/4 v2, -0x1

    #@2
    .line 227
    .local v2, initialScrollX:I
    const/4 v3, -0x1

    #@3
    .line 228
    .local v3, initialScrollY:I
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    #@6
    move-result v0

    #@7
    .line 230
    .local v0, action:I
    if-ne v0, v5, :cond_11

    #@9
    .line 231
    invoke-static {p1, p2}, Landroid/text/method/Touch;->getInitialScrollX(Landroid/widget/TextView;Landroid/text/Spannable;)I

    #@c
    move-result v2

    #@d
    .line 232
    invoke-static {p1, p2}, Landroid/text/method/Touch;->getInitialScrollY(Landroid/widget/TextView;Landroid/text/Spannable;)I

    #@10
    move-result v3

    #@11
    .line 235
    :cond_11
    invoke-static {p1, p2, p3}, Landroid/text/method/Touch;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    #@14
    move-result v1

    #@15
    .line 237
    .local v1, handled:Z
    invoke-virtual {p1}, Landroid/widget/TextView;->isFocused()Z

    #@18
    move-result v6

    #@19
    if-eqz v6, :cond_43

    #@1b
    invoke-virtual {p1}, Landroid/widget/TextView;->didTouchFocusSelect()Z

    #@1e
    move-result v6

    #@1f
    if-nez v6, :cond_43

    #@21
    .line 238
    if-nez v0, :cond_44

    #@23
    .line 239
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@26
    move-result v6

    #@27
    if-eqz v6, :cond_43

    #@29
    .line 240
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    #@2c
    move-result v6

    #@2d
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    #@30
    move-result v7

    #@31
    invoke-virtual {p1, v6, v7}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@34
    move-result v4

    #@35
    .line 242
    .local v4, offset:I
    sget-object v6, Landroid/text/method/ArrowKeyMovementMethod;->LAST_TAP_DOWN:Ljava/lang/Object;

    #@37
    const/16 v7, 0x22

    #@39
    invoke-interface {p2, v6, v4, v4, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@3c
    .line 248
    invoke-virtual {p1}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    #@3f
    move-result-object v6

    #@40
    invoke-interface {v6, v5}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@43
    .line 292
    .end local v1           #handled:Z
    .end local v4           #offset:I
    :cond_43
    :goto_43
    return v1

    #@44
    .line 250
    .restart local v1       #handled:Z
    :cond_44
    const/4 v6, 0x2

    #@45
    if-ne v0, v6, :cond_63

    #@47
    .line 251
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@4a
    move-result v6

    #@4b
    if-eqz v6, :cond_43

    #@4d
    if-eqz v1, :cond_43

    #@4f
    .line 258
    invoke-virtual {p1}, Landroid/widget/TextView;->cancelLongPress()V

    #@52
    .line 263
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    #@55
    move-result v6

    #@56
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    #@59
    move-result v7

    #@5a
    invoke-virtual {p1, v6, v7}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@5d
    move-result v4

    #@5e
    .line 265
    .restart local v4       #offset:I
    invoke-static {p2, v4}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@61
    move v1, v5

    #@62
    .line 266
    goto :goto_43

    #@63
    .line 268
    .end local v4           #offset:I
    :cond_63
    if-ne v0, v5, :cond_43

    #@65
    .line 273
    if-ltz v3, :cond_6d

    #@67
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@6a
    move-result v6

    #@6b
    if-ne v3, v6, :cond_75

    #@6d
    :cond_6d
    if-ltz v2, :cond_7a

    #@6f
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@72
    move-result v6

    #@73
    if-eq v2, v6, :cond_7a

    #@75
    .line 275
    :cond_75
    invoke-virtual {p1}, Landroid/widget/TextView;->moveCursorToVisibleOffset()Z

    #@78
    move v1, v5

    #@79
    .line 276
    goto :goto_43

    #@7a
    .line 279
    :cond_7a
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    #@7d
    move-result v6

    #@7e
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    #@81
    move-result v7

    #@82
    invoke-virtual {p1, v6, v7}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@85
    move-result v4

    #@86
    .line 280
    .restart local v4       #offset:I
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@89
    move-result v6

    #@8a
    if-eqz v6, :cond_94

    #@8c
    .line 281
    sget-object v6, Landroid/text/method/ArrowKeyMovementMethod;->LAST_TAP_DOWN:Ljava/lang/Object;

    #@8e
    invoke-interface {p2, v6}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@91
    .line 282
    invoke-static {p2, v4}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@94
    .line 285
    :cond_94
    invoke-static {p2}, Landroid/text/method/MetaKeyKeyListener;->adjustMetaAfterKeypress(Landroid/text/Spannable;)V

    #@97
    .line 286
    invoke-static {p2}, Landroid/text/method/MetaKeyKeyListener;->resetLockedMeta(Landroid/text/Spannable;)V

    #@9a
    move v1, v5

    #@9b
    .line 288
    goto :goto_43
.end method

.method protected pageDown(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 10
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 134
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v1

    #@4
    .line 135
    .local v1, layout:Landroid/text/Layout;
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@7
    move-result v3

    #@8
    .line 136
    .local v3, selecting:Z
    invoke-static {p2, v1}, Landroid/text/method/ArrowKeyMovementMethod;->getCurrentLineTop(Landroid/text/Spannable;Landroid/text/Layout;)I

    #@b
    move-result v5

    #@c
    invoke-static {p1}, Landroid/text/method/ArrowKeyMovementMethod;->getPageHeight(Landroid/widget/TextView;)I

    #@f
    move-result v6

    #@10
    add-int v4, v5, v6

    #@12
    .line 137
    .local v4, targetY:I
    const/4 v0, 0x0

    #@13
    .line 139
    .local v0, handled:Z
    :cond_13
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@16
    move-result v2

    #@17
    .line 140
    .local v2, previousSelectionEnd:I
    if-eqz v3, :cond_23

    #@19
    .line 141
    invoke-static {p2, v1}, Landroid/text/Selection;->extendDown(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@1c
    .line 145
    :goto_1c
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@1f
    move-result v5

    #@20
    if-ne v5, v2, :cond_27

    #@22
    .line 153
    :goto_22
    return v0

    #@23
    .line 143
    :cond_23
    invoke-static {p2, v1}, Landroid/text/Selection;->moveDown(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@26
    goto :goto_1c

    #@27
    .line 148
    :cond_27
    const/4 v0, 0x1

    #@28
    .line 149
    invoke-static {p2, v1}, Landroid/text/method/ArrowKeyMovementMethod;->getCurrentLineTop(Landroid/text/Spannable;Landroid/text/Layout;)I

    #@2b
    move-result v5

    #@2c
    if-lt v5, v4, :cond_13

    #@2e
    goto :goto_22
.end method

.method protected pageUp(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 10
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v1

    #@4
    .line 111
    .local v1, layout:Landroid/text/Layout;
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@7
    move-result v3

    #@8
    .line 112
    .local v3, selecting:Z
    invoke-static {p2, v1}, Landroid/text/method/ArrowKeyMovementMethod;->getCurrentLineTop(Landroid/text/Spannable;Landroid/text/Layout;)I

    #@b
    move-result v5

    #@c
    invoke-static {p1}, Landroid/text/method/ArrowKeyMovementMethod;->getPageHeight(Landroid/widget/TextView;)I

    #@f
    move-result v6

    #@10
    sub-int v4, v5, v6

    #@12
    .line 113
    .local v4, targetY:I
    const/4 v0, 0x0

    #@13
    .line 115
    .local v0, handled:Z
    :cond_13
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@16
    move-result v2

    #@17
    .line 116
    .local v2, previousSelectionEnd:I
    if-eqz v3, :cond_23

    #@19
    .line 117
    invoke-static {p2, v1}, Landroid/text/Selection;->extendUp(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@1c
    .line 121
    :goto_1c
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@1f
    move-result v5

    #@20
    if-ne v5, v2, :cond_27

    #@22
    .line 129
    :goto_22
    return v0

    #@23
    .line 119
    :cond_23
    invoke-static {p2, v1}, Landroid/text/Selection;->moveUp(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@26
    goto :goto_1c

    #@27
    .line 124
    :cond_27
    const/4 v0, 0x1

    #@28
    .line 125
    invoke-static {p2, v1}, Landroid/text/method/ArrowKeyMovementMethod;->getCurrentLineTop(Landroid/text/Spannable;Landroid/text/Layout;)I

    #@2b
    move-result v5

    #@2c
    if-gt v5, v4, :cond_13

    #@2e
    goto :goto_22
.end method

.method protected right(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 80
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v0

    #@4
    .line 81
    .local v0, layout:Landroid/text/Layout;
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 82
    invoke-static {p2, v0}, Landroid/text/Selection;->extendRight(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@d
    move-result v1

    #@e
    .line 84
    :goto_e
    return v1

    #@f
    :cond_f
    invoke-static {p2, v0}, Landroid/text/Selection;->moveRight(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@12
    move-result v1

    #@13
    goto :goto_e
.end method

.method protected rightWord(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 6
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 208
    invoke-virtual {p1}, Landroid/widget/TextView;->getSelectionEnd()I

    #@3
    move-result v0

    #@4
    .line 209
    .local v0, selectionEnd:I
    invoke-virtual {p1}, Landroid/widget/TextView;->getWordIterator()Landroid/text/method/WordIterator;

    #@7
    move-result-object v1

    #@8
    .line 210
    .local v1, wordIterator:Landroid/text/method/WordIterator;
    invoke-virtual {v1, p2, v0, v0}, Landroid/text/method/WordIterator;->setCharSequence(Ljava/lang/CharSequence;II)V

    #@b
    .line 211
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@e
    move-result v2

    #@f
    invoke-static {p2, v1, v2}, Landroid/text/Selection;->moveToFollowing(Landroid/text/Spannable;Landroid/text/Selection$PositionIterator;Z)Z

    #@12
    move-result v2

    #@13
    return v2
.end method

.method protected top(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 158
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 159
    invoke-static {p2, v1}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@a
    .line 163
    :goto_a
    const/4 v0, 0x1

    #@b
    return v0

    #@c
    .line 161
    :cond_c
    invoke-static {p2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@f
    goto :goto_a
.end method

.method protected up(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 90
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v0

    #@4
    .line 91
    .local v0, layout:Landroid/text/Layout;
    invoke-static {p2}, Landroid/text/method/ArrowKeyMovementMethod;->isSelecting(Landroid/text/Spannable;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 92
    invoke-static {p2, v0}, Landroid/text/Selection;->extendUp(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@d
    move-result v1

    #@e
    .line 94
    :goto_e
    return v1

    #@f
    :cond_f
    invoke-static {p2, v0}, Landroid/text/Selection;->moveUp(Landroid/text/Spannable;Landroid/text/Layout;)Z

    #@12
    move-result v1

    #@13
    goto :goto_e
.end method
