.class public Landroid/text/method/SingleLineTransformationMethod;
.super Landroid/text/method/ReplacementTransformationMethod;
.source "SingleLineTransformationMethod.java"


# static fields
.field private static ORIGINAL:[C

.field private static REPLACEMENT:[C

.field private static sInstance:Landroid/text/method/SingleLineTransformationMethod;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 35
    new-array v0, v1, [C

    #@3
    fill-array-data v0, :array_10

    #@6
    sput-object v0, Landroid/text/method/SingleLineTransformationMethod;->ORIGINAL:[C

    #@8
    .line 36
    new-array v0, v1, [C

    #@a
    fill-array-data v0, :array_16

    #@d
    sput-object v0, Landroid/text/method/SingleLineTransformationMethod;->REPLACEMENT:[C

    #@f
    return-void

    #@10
    .line 35
    :array_10
    .array-data 0x2
        0xat 0x0t
        0xdt 0x0t
    .end array-data

    #@16
    .line 36
    :array_16
    .array-data 0x2
        0x20t 0x0t
        0xfft 0xfet
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Landroid/text/method/ReplacementTransformationMethod;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Landroid/text/method/SingleLineTransformationMethod;
    .registers 1

    #@0
    .prologue
    .line 54
    sget-object v0, Landroid/text/method/SingleLineTransformationMethod;->sInstance:Landroid/text/method/SingleLineTransformationMethod;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 55
    sget-object v0, Landroid/text/method/SingleLineTransformationMethod;->sInstance:Landroid/text/method/SingleLineTransformationMethod;

    #@6
    .line 58
    :goto_6
    return-object v0

    #@7
    .line 57
    :cond_7
    new-instance v0, Landroid/text/method/SingleLineTransformationMethod;

    #@9
    invoke-direct {v0}, Landroid/text/method/SingleLineTransformationMethod;-><init>()V

    #@c
    sput-object v0, Landroid/text/method/SingleLineTransformationMethod;->sInstance:Landroid/text/method/SingleLineTransformationMethod;

    #@e
    .line 58
    sget-object v0, Landroid/text/method/SingleLineTransformationMethod;->sInstance:Landroid/text/method/SingleLineTransformationMethod;

    #@10
    goto :goto_6
.end method


# virtual methods
.method protected getOriginal()[C
    .registers 2

    #@0
    .prologue
    .line 42
    sget-object v0, Landroid/text/method/SingleLineTransformationMethod;->ORIGINAL:[C

    #@2
    return-object v0
.end method

.method protected getReplacement()[C
    .registers 2

    #@0
    .prologue
    .line 50
    sget-object v0, Landroid/text/method/SingleLineTransformationMethod;->REPLACEMENT:[C

    #@2
    return-object v0
.end method
