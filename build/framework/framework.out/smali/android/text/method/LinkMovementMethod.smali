.class public Landroid/text/method/LinkMovementMethod;
.super Landroid/text/method/ScrollingMovementMethod;
.source "LinkMovementMethod.java"


# static fields
.field private static final CLICK:I = 0x1

.field private static final DOWN:I = 0x3

.field private static FROM_BELOW:Ljava/lang/Object; = null

.field private static final UP:I = 0x2

.field private static sInstance:Landroid/text/method/LinkMovementMethod;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 253
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    #@2
    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    #@5
    sput-object v0, Landroid/text/method/LinkMovementMethod;->FROM_BELOW:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    #@3
    return-void
.end method

.method private action(ILandroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 29
    .parameter "what"
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 92
    invoke-virtual/range {p2 .. p2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v15

    #@4
    .line 94
    .local v15, layout:Landroid/text/Layout;
    invoke-virtual/range {p2 .. p2}, Landroid/widget/TextView;->getTotalPaddingTop()I

    #@7
    move-result v23

    #@8
    invoke-virtual/range {p2 .. p2}, Landroid/widget/TextView;->getTotalPaddingBottom()I

    #@b
    move-result v24

    #@c
    add-int v19, v23, v24

    #@e
    .line 96
    .local v19, padding:I
    invoke-virtual/range {p2 .. p2}, Landroid/widget/TextView;->getScrollY()I

    #@11
    move-result v6

    #@12
    .line 97
    .local v6, areatop:I
    invoke-virtual/range {p2 .. p2}, Landroid/widget/TextView;->getHeight()I

    #@15
    move-result v23

    #@16
    add-int v23, v23, v6

    #@18
    sub-int v5, v23, v19

    #@1a
    .line 99
    .local v5, areabot:I
    invoke-virtual {v15, v6}, Landroid/text/Layout;->getLineForVertical(I)I

    #@1d
    move-result v17

    #@1e
    .line 100
    .local v17, linetop:I
    invoke-virtual {v15, v5}, Landroid/text/Layout;->getLineForVertical(I)I

    #@21
    move-result v16

    #@22
    .line 102
    .local v16, linebot:I
    move/from16 v0, v17

    #@24
    invoke-virtual {v15, v0}, Landroid/text/Layout;->getLineStart(I)I

    #@27
    move-result v12

    #@28
    .line 103
    .local v12, first:I
    invoke-virtual/range {v15 .. v16}, Landroid/text/Layout;->getLineEnd(I)I

    #@2b
    move-result v14

    #@2c
    .line 105
    .local v14, last:I
    const-class v23, Landroid/text/style/ClickableSpan;

    #@2e
    move-object/from16 v0, p3

    #@30
    move-object/from16 v1, v23

    #@32
    invoke-interface {v0, v12, v14, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@35
    move-result-object v10

    #@36
    check-cast v10, [Landroid/text/style/ClickableSpan;

    #@38
    .line 107
    .local v10, candidates:[Landroid/text/style/ClickableSpan;
    invoke-static/range {p3 .. p3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@3b
    move-result v4

    #@3c
    .line 108
    .local v4, a:I
    invoke-static/range {p3 .. p3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@3f
    move-result v7

    #@40
    .line 110
    .local v7, b:I
    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    #@43
    move-result v21

    #@44
    .line 111
    .local v21, selStart:I
    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    #@47
    move-result v20

    #@48
    .line 113
    .local v20, selEnd:I
    if-gez v21, :cond_5c

    #@4a
    .line 114
    sget-object v23, Landroid/text/method/LinkMovementMethod;->FROM_BELOW:Ljava/lang/Object;

    #@4c
    move-object/from16 v0, p3

    #@4e
    move-object/from16 v1, v23

    #@50
    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@53
    move-result v23

    #@54
    if-ltz v23, :cond_5c

    #@56
    .line 115
    invoke-interface/range {p3 .. p3}, Landroid/text/Spannable;->length()I

    #@59
    move-result v20

    #@5a
    move/from16 v21, v20

    #@5c
    .line 119
    :cond_5c
    move/from16 v0, v21

    #@5e
    if-le v0, v14, :cond_65

    #@60
    .line 120
    const v20, 0x7fffffff

    #@63
    move/from16 v21, v20

    #@65
    .line 121
    :cond_65
    move/from16 v0, v20

    #@67
    if-ge v0, v12, :cond_6d

    #@69
    .line 122
    const/16 v20, -0x1

    #@6b
    move/from16 v21, v20

    #@6d
    .line 124
    :cond_6d
    packed-switch p1, :pswitch_data_128

    #@70
    .line 185
    :cond_70
    :goto_70
    const/16 v23, 0x0

    #@72
    :goto_72
    return v23

    #@73
    .line 126
    :pswitch_73
    move/from16 v0, v21

    #@75
    move/from16 v1, v20

    #@77
    if-ne v0, v1, :cond_7c

    #@79
    .line 127
    const/16 v23, 0x0

    #@7b
    goto :goto_72

    #@7c
    .line 130
    :cond_7c
    const-class v23, Landroid/text/style/ClickableSpan;

    #@7e
    move-object/from16 v0, p3

    #@80
    move/from16 v1, v21

    #@82
    move/from16 v2, v20

    #@84
    move-object/from16 v3, v23

    #@86
    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@89
    move-result-object v18

    #@8a
    check-cast v18, [Landroid/text/style/ClickableSpan;

    #@8c
    .line 132
    .local v18, link:[Landroid/text/style/ClickableSpan;
    move-object/from16 v0, v18

    #@8e
    array-length v0, v0

    #@8f
    move/from16 v23, v0

    #@91
    const/16 v24, 0x1

    #@93
    move/from16 v0, v23

    #@95
    move/from16 v1, v24

    #@97
    if-eq v0, v1, :cond_9c

    #@99
    .line 133
    const/16 v23, 0x0

    #@9b
    goto :goto_72

    #@9c
    .line 135
    :cond_9c
    const/16 v23, 0x0

    #@9e
    aget-object v23, v18, v23

    #@a0
    move-object/from16 v0, v23

    #@a2
    move-object/from16 v1, p2

    #@a4
    invoke-virtual {v0, v1}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    #@a7
    goto :goto_70

    #@a8
    .line 141
    .end local v18           #link:[Landroid/text/style/ClickableSpan;
    :pswitch_a8
    const/4 v9, -0x1

    #@a9
    .line 142
    .local v9, beststart:I
    const/4 v8, -0x1

    #@aa
    .line 144
    .local v8, bestend:I
    const/4 v13, 0x0

    #@ab
    .local v13, i:I
    :goto_ab
    array-length v0, v10

    #@ac
    move/from16 v23, v0

    #@ae
    move/from16 v0, v23

    #@b0
    if-ge v13, v0, :cond_d6

    #@b2
    .line 145
    aget-object v23, v10, v13

    #@b4
    move-object/from16 v0, p3

    #@b6
    move-object/from16 v1, v23

    #@b8
    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@bb
    move-result v11

    #@bc
    .line 147
    .local v11, end:I
    move/from16 v0, v20

    #@be
    if-lt v11, v0, :cond_c6

    #@c0
    move/from16 v0, v21

    #@c2
    move/from16 v1, v20

    #@c4
    if-ne v0, v1, :cond_d3

    #@c6
    .line 148
    :cond_c6
    if-le v11, v8, :cond_d3

    #@c8
    .line 149
    aget-object v23, v10, v13

    #@ca
    move-object/from16 v0, p3

    #@cc
    move-object/from16 v1, v23

    #@ce
    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@d1
    move-result v9

    #@d2
    .line 150
    move v8, v11

    #@d3
    .line 144
    :cond_d3
    add-int/lit8 v13, v13, 0x1

    #@d5
    goto :goto_ab

    #@d6
    .line 155
    .end local v11           #end:I
    :cond_d6
    if-ltz v9, :cond_70

    #@d8
    .line 156
    move-object/from16 v0, p3

    #@da
    invoke-static {v0, v8, v9}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@dd
    .line 157
    const/16 v23, 0x1

    #@df
    goto :goto_72

    #@e0
    .line 163
    .end local v8           #bestend:I
    .end local v9           #beststart:I
    .end local v13           #i:I
    :pswitch_e0
    const v9, 0x7fffffff

    #@e3
    .line 164
    .restart local v9       #beststart:I
    const v8, 0x7fffffff

    #@e6
    .line 166
    .restart local v8       #bestend:I
    const/4 v13, 0x0

    #@e7
    .restart local v13       #i:I
    :goto_e7
    array-length v0, v10

    #@e8
    move/from16 v23, v0

    #@ea
    move/from16 v0, v23

    #@ec
    if-ge v13, v0, :cond_117

    #@ee
    .line 167
    aget-object v23, v10, v13

    #@f0
    move-object/from16 v0, p3

    #@f2
    move-object/from16 v1, v23

    #@f4
    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@f7
    move-result v22

    #@f8
    .line 169
    .local v22, start:I
    move/from16 v0, v22

    #@fa
    move/from16 v1, v21

    #@fc
    if-gt v0, v1, :cond_104

    #@fe
    move/from16 v0, v21

    #@100
    move/from16 v1, v20

    #@102
    if-ne v0, v1, :cond_114

    #@104
    .line 170
    :cond_104
    move/from16 v0, v22

    #@106
    if-ge v0, v9, :cond_114

    #@108
    .line 171
    move/from16 v9, v22

    #@10a
    .line 172
    aget-object v23, v10, v13

    #@10c
    move-object/from16 v0, p3

    #@10e
    move-object/from16 v1, v23

    #@110
    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@113
    move-result v8

    #@114
    .line 166
    :cond_114
    add-int/lit8 v13, v13, 0x1

    #@116
    goto :goto_e7

    #@117
    .line 177
    .end local v22           #start:I
    :cond_117
    const v23, 0x7fffffff

    #@11a
    move/from16 v0, v23

    #@11c
    if-ge v8, v0, :cond_70

    #@11e
    .line 178
    move-object/from16 v0, p3

    #@120
    invoke-static {v0, v9, v8}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@123
    .line 179
    const/16 v23, 0x1

    #@125
    goto/16 :goto_72

    #@127
    .line 124
    nop

    #@128
    :pswitch_data_128
    .packed-switch 0x1
        :pswitch_73
        :pswitch_a8
        :pswitch_e0
    .end packed-switch
.end method

.method public static getInstance()Landroid/text/method/MovementMethod;
    .registers 1

    #@0
    .prologue
    .line 246
    sget-object v0, Landroid/text/method/LinkMovementMethod;->sInstance:Landroid/text/method/LinkMovementMethod;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 247
    new-instance v0, Landroid/text/method/LinkMovementMethod;

    #@6
    invoke-direct {v0}, Landroid/text/method/LinkMovementMethod;-><init>()V

    #@9
    sput-object v0, Landroid/text/method/LinkMovementMethod;->sInstance:Landroid/text/method/LinkMovementMethod;

    #@b
    .line 249
    :cond_b
    sget-object v0, Landroid/text/method/LinkMovementMethod;->sInstance:Landroid/text/method/LinkMovementMethod;

    #@d
    return-object v0
.end method


# virtual methods
.method protected down(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 66
    const/4 v0, 0x3

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/text/method/LinkMovementMethod;->action(ILandroid/widget/TextView;Landroid/text/Spannable;)Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_9

    #@7
    .line 67
    const/4 v0, 0x1

    #@8
    .line 70
    :goto_8
    return v0

    #@9
    :cond_9
    invoke-super {p0, p1, p2}, Landroid/text/method/ScrollingMovementMethod;->down(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@c
    move-result v0

    #@d
    goto :goto_8
.end method

.method protected handleMovementKey(Landroid/widget/TextView;Landroid/text/Spannable;IILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter "widget"
    .parameter "buffer"
    .parameter "keyCode"
    .parameter "movementMetaState"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 41
    sparse-switch p3, :sswitch_data_22

    #@4
    .line 52
    :cond_4
    invoke-super/range {p0 .. p5}, Landroid/text/method/ScrollingMovementMethod;->handleMovementKey(Landroid/widget/TextView;Landroid/text/Spannable;IILandroid/view/KeyEvent;)Z

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    .line 44
    :sswitch_9
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_4

    #@f
    .line 45
    invoke-virtual {p5}, Landroid/view/KeyEvent;->getAction()I

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_4

    #@15
    invoke-virtual {p5}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@18
    move-result v1

    #@19
    if-nez v1, :cond_4

    #@1b
    invoke-direct {p0, v0, p1, p2}, Landroid/text/method/LinkMovementMethod;->action(ILandroid/widget/TextView;Landroid/text/Spannable;)Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_4

    #@21
    goto :goto_8

    #@22
    .line 41
    :sswitch_data_22
    .sparse-switch
        0x17 -> :sswitch_9
        0x42 -> :sswitch_9
    .end sparse-switch
.end method

.method public initialize(Landroid/widget/TextView;Landroid/text/Spannable;)V
    .registers 4
    .parameter "widget"
    .parameter "text"

    #@0
    .prologue
    .line 230
    invoke-static {p2}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    #@3
    .line 231
    sget-object v0, Landroid/text/method/LinkMovementMethod;->FROM_BELOW:Ljava/lang/Object;

    #@5
    invoke-interface {p2, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@8
    .line 232
    return-void
.end method

.method protected left(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 75
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/text/method/LinkMovementMethod;->action(ILandroid/widget/TextView;Landroid/text/Spannable;)Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_9

    #@7
    .line 76
    const/4 v0, 0x1

    #@8
    .line 79
    :goto_8
    return v0

    #@9
    :cond_9
    invoke-super {p0, p1, p2}, Landroid/text/method/ScrollingMovementMethod;->left(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@c
    move-result v0

    #@d
    goto :goto_8
.end method

.method public onTakeFocus(Landroid/widget/TextView;Landroid/text/Spannable;I)V
    .registers 7
    .parameter "view"
    .parameter "text"
    .parameter "dir"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 236
    invoke-static {p2}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    #@4
    .line 238
    and-int/lit8 v0, p3, 0x1

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 239
    sget-object v0, Landroid/text/method/LinkMovementMethod;->FROM_BELOW:Ljava/lang/Object;

    #@a
    const/16 v1, 0x22

    #@c
    invoke-interface {p2, v0, v2, v2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@f
    .line 243
    :goto_f
    return-void

    #@10
    .line 241
    :cond_10
    sget-object v0, Landroid/text/method/LinkMovementMethod;->FROM_BELOW:Ljava/lang/Object;

    #@12
    invoke-interface {p2, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@15
    goto :goto_f
.end method

.method public onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .registers 14
    .parameter "widget"
    .parameter "buffer"
    .parameter "event"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 191
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    #@5
    move-result v0

    #@6
    .line 193
    .local v0, action:I
    if-eq v0, v7, :cond_a

    #@8
    if-nez v0, :cond_5d

    #@a
    .line 195
    :cond_a
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    #@d
    move-result v8

    #@e
    float-to-int v5, v8

    #@f
    .line 196
    .local v5, x:I
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    #@12
    move-result v8

    #@13
    float-to-int v6, v8

    #@14
    .line 198
    .local v6, y:I
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    #@17
    move-result v8

    #@18
    sub-int/2addr v5, v8

    #@19
    .line 199
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    #@1c
    move-result v8

    #@1d
    sub-int/2addr v6, v8

    #@1e
    .line 201
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@21
    move-result v8

    #@22
    add-int/2addr v5, v8

    #@23
    .line 202
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@26
    move-result v8

    #@27
    add-int/2addr v6, v8

    #@28
    .line 204
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@2b
    move-result-object v1

    #@2c
    .line 205
    .local v1, layout:Landroid/text/Layout;
    invoke-virtual {v1, v6}, Landroid/text/Layout;->getLineForVertical(I)I

    #@2f
    move-result v2

    #@30
    .line 206
    .local v2, line:I
    int-to-float v8, v5

    #@31
    invoke-virtual {v1, v2, v8}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    #@34
    move-result v4

    #@35
    .line 208
    .local v4, off:I
    const-class v8, Landroid/text/style/ClickableSpan;

    #@37
    invoke-interface {p2, v4, v4, v8}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@3a
    move-result-object v3

    #@3b
    check-cast v3, [Landroid/text/style/ClickableSpan;

    #@3d
    .line 210
    .local v3, link:[Landroid/text/style/ClickableSpan;
    array-length v8, v3

    #@3e
    if-eqz v8, :cond_5a

    #@40
    .line 211
    if-ne v0, v7, :cond_48

    #@42
    .line 212
    aget-object v8, v3, v9

    #@44
    invoke-virtual {v8, p1}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    #@47
    .line 225
    .end local v1           #layout:Landroid/text/Layout;
    .end local v2           #line:I
    .end local v3           #link:[Landroid/text/style/ClickableSpan;
    .end local v4           #off:I
    .end local v5           #x:I
    .end local v6           #y:I
    :cond_47
    :goto_47
    return v7

    #@48
    .line 213
    .restart local v1       #layout:Landroid/text/Layout;
    .restart local v2       #line:I
    .restart local v3       #link:[Landroid/text/style/ClickableSpan;
    .restart local v4       #off:I
    .restart local v5       #x:I
    .restart local v6       #y:I
    :cond_48
    if-nez v0, :cond_47

    #@4a
    .line 214
    aget-object v8, v3, v9

    #@4c
    invoke-interface {p2, v8}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@4f
    move-result v8

    #@50
    aget-object v9, v3, v9

    #@52
    invoke-interface {p2, v9}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@55
    move-result v9

    #@56
    invoke-static {p2, v8, v9}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@59
    goto :goto_47

    #@5a
    .line 221
    :cond_5a
    invoke-static {p2}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    #@5d
    .line 225
    .end local v1           #layout:Landroid/text/Layout;
    .end local v2           #line:I
    .end local v3           #link:[Landroid/text/style/ClickableSpan;
    .end local v4           #off:I
    .end local v5           #x:I
    .end local v6           #y:I
    :cond_5d
    invoke-super {p0, p1, p2, p3}, Landroid/text/method/ScrollingMovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    #@60
    move-result v7

    #@61
    goto :goto_47
.end method

.method protected right(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 84
    const/4 v0, 0x3

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/text/method/LinkMovementMethod;->action(ILandroid/widget/TextView;Landroid/text/Spannable;)Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_9

    #@7
    .line 85
    const/4 v0, 0x1

    #@8
    .line 88
    :goto_8
    return v0

    #@9
    :cond_9
    invoke-super {p0, p1, p2}, Landroid/text/method/ScrollingMovementMethod;->right(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@c
    move-result v0

    #@d
    goto :goto_8
.end method

.method protected up(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 57
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/text/method/LinkMovementMethod;->action(ILandroid/widget/TextView;Landroid/text/Spannable;)Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_9

    #@7
    .line 58
    const/4 v0, 0x1

    #@8
    .line 61
    :goto_8
    return v0

    #@9
    :cond_9
    invoke-super {p0, p1, p2}, Landroid/text/method/ScrollingMovementMethod;->up(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@c
    move-result v0

    #@d
    goto :goto_8
.end method
