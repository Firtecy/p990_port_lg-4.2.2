.class public Landroid/text/method/DialerKeyListener;
.super Landroid/text/method/NumberKeyListener;
.source "DialerKeyListener.java"


# static fields
.field public static final CHARACTERS:[C

.field private static sInstance:Landroid/text/method/DialerKeyListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 111
    const/16 v0, 0x16

    #@2
    new-array v0, v0, [C

    #@4
    fill-array-data v0, :array_a

    #@7
    sput-object v0, Landroid/text/method/DialerKeyListener;->CHARACTERS:[C

    #@9
    return-void

    #@a
    :array_a
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
        0x23t 0x0t
        0x2at 0x0t
        0x2bt 0x0t
        0x2dt 0x0t
        0x28t 0x0t
        0x29t 0x0t
        0x2ct 0x0t
        0x2ft 0x0t
        0x4et 0x0t
        0x2et 0x0t
        0x20t 0x0t
        0x3bt 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct {p0}, Landroid/text/method/NumberKeyListener;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Landroid/text/method/DialerKeyListener;
    .registers 1

    #@0
    .prologue
    .line 40
    sget-object v0, Landroid/text/method/DialerKeyListener;->sInstance:Landroid/text/method/DialerKeyListener;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 41
    sget-object v0, Landroid/text/method/DialerKeyListener;->sInstance:Landroid/text/method/DialerKeyListener;

    #@6
    .line 44
    :goto_6
    return-object v0

    #@7
    .line 43
    :cond_7
    new-instance v0, Landroid/text/method/DialerKeyListener;

    #@9
    invoke-direct {v0}, Landroid/text/method/DialerKeyListener;-><init>()V

    #@c
    sput-object v0, Landroid/text/method/DialerKeyListener;->sInstance:Landroid/text/method/DialerKeyListener;

    #@e
    .line 44
    sget-object v0, Landroid/text/method/DialerKeyListener;->sInstance:Landroid/text/method/DialerKeyListener;

    #@10
    goto :goto_6
.end method


# virtual methods
.method protected getAcceptedChars()[C
    .registers 2

    #@0
    .prologue
    .line 36
    sget-object v0, Landroid/text/method/DialerKeyListener;->CHARACTERS:[C

    #@2
    return-object v0
.end method

.method public getInputType()I
    .registers 2

    #@0
    .prologue
    .line 48
    const/4 v0, 0x3

    #@1
    return v0
.end method

.method protected lookup(Landroid/view/KeyEvent;Landroid/text/Spannable;)I
    .registers 11
    .parameter "event"
    .parameter "content"

    #@0
    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    #@3
    move-result v6

    #@4
    invoke-static {p2}, Landroid/text/method/DialerKeyListener;->getMetaState(Ljava/lang/CharSequence;)I

    #@7
    move-result v7

    #@8
    or-int v4, v6, v7

    #@a
    .line 57
    .local v4, meta:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getNumber()C

    #@d
    move-result v5

    #@e
    .line 63
    .local v5, number:I
    and-int/lit8 v6, v4, 0x3

    #@10
    if-nez v6, :cond_15

    #@12
    .line 64
    if-eqz v5, :cond_15

    #@14
    .line 100
    .end local v5           #number:I
    :cond_14
    :goto_14
    return v5

    #@15
    .line 69
    .restart local v5       #number:I
    :cond_15
    invoke-super {p0, p1, p2}, Landroid/text/method/NumberKeyListener;->lookup(Landroid/view/KeyEvent;Landroid/text/Spannable;)I

    #@18
    move-result v3

    #@19
    .line 71
    .local v3, match:I
    if-eqz v3, :cond_1d

    #@1b
    move v5, v3

    #@1c
    .line 72
    goto :goto_14

    #@1d
    .line 81
    :cond_1d
    if-eqz v4, :cond_14

    #@1f
    .line 82
    new-instance v2, Landroid/view/KeyCharacterMap$KeyData;

    #@21
    invoke-direct {v2}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    #@24
    .line 83
    .local v2, kd:Landroid/view/KeyCharacterMap$KeyData;
    invoke-virtual {p0}, Landroid/text/method/DialerKeyListener;->getAcceptedChars()[C

    #@27
    move-result-object v0

    #@28
    .line 85
    .local v0, accepted:[C
    invoke-virtual {p1, v2}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    #@2b
    move-result v6

    #@2c
    if-eqz v6, :cond_14

    #@2e
    .line 86
    const/4 v1, 0x1

    #@2f
    .local v1, i:I
    :goto_2f
    iget-object v6, v2, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@31
    array-length v6, v6

    #@32
    if-ge v1, v6, :cond_14

    #@34
    .line 87
    iget-object v6, v2, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@36
    aget-char v6, v6, v1

    #@38
    invoke-static {v0, v6}, Landroid/text/method/DialerKeyListener;->ok([CC)Z

    #@3b
    move-result v6

    #@3c
    if-eqz v6, :cond_43

    #@3e
    .line 88
    iget-object v6, v2, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@40
    aget-char v5, v6, v1

    #@42
    goto :goto_14

    #@43
    .line 86
    :cond_43
    add-int/lit8 v1, v1, 0x1

    #@45
    goto :goto_2f
.end method
