.class public Landroid/text/method/WordIterator;
.super Ljava/lang/Object;
.source "WordIterator.java"

# interfaces
.implements Landroid/text/Selection$PositionIterator;


# static fields
.field private static final WINDOW_WIDTH:I = 0x32


# instance fields
.field private mIterator:Ljava/text/BreakIterator;

.field private mOffsetShift:I

.field private mString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 47
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/text/method/WordIterator;-><init>(Ljava/util/Locale;)V

    #@7
    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 55
    invoke-static {p1}, Ljava/text/BreakIterator;->getWordInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/text/method/WordIterator;->mIterator:Ljava/text/BreakIterator;

    #@9
    .line 56
    return-void
.end method

.method private checkOffsetIsValid(I)V
    .registers 6
    .parameter "shiftedOffset"

    #@0
    .prologue
    .line 175
    if-ltz p1, :cond_a

    #@2
    iget-object v0, p0, Landroid/text/method/WordIterator;->mString:Ljava/lang/String;

    #@4
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@7
    move-result v0

    #@8
    if-le p1, v0, :cond_4b

    #@a
    .line 176
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Invalid offset: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget v2, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@19
    add-int/2addr v2, p1

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v2, ". Valid range is ["

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    iget v2, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, ", "

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    iget-object v2, p0, Landroid/text/method/WordIterator;->mString:Ljava/lang/String;

    #@32
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@35
    move-result v2

    #@36
    iget v3, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@38
    add-int/2addr v2, v3

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, "]"

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v0

    #@4b
    .line 180
    :cond_4b
    return-void
.end method

.method private isAfterLetterOrDigit(I)Z
    .registers 5
    .parameter "shiftedOffset"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 159
    if-lt p1, v1, :cond_18

    #@3
    iget-object v2, p0, Landroid/text/method/WordIterator;->mString:Ljava/lang/String;

    #@5
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@8
    move-result v2

    #@9
    if-gt p1, v2, :cond_18

    #@b
    .line 160
    iget-object v2, p0, Landroid/text/method/WordIterator;->mString:Ljava/lang/String;

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/String;->codePointBefore(I)I

    #@10
    move-result v0

    #@11
    .line 161
    .local v0, codePoint:I
    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_18

    #@17
    .line 163
    .end local v0           #codePoint:I
    :goto_17
    return v1

    #@18
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_17
.end method

.method private isOnLetterOrDigit(I)Z
    .registers 4
    .parameter "shiftedOffset"

    #@0
    .prologue
    .line 167
    if-ltz p1, :cond_18

    #@2
    iget-object v1, p0, Landroid/text/method/WordIterator;->mString:Ljava/lang/String;

    #@4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@7
    move-result v1

    #@8
    if-ge p1, v1, :cond_18

    #@a
    .line 168
    iget-object v1, p0, Landroid/text/method/WordIterator;->mString:Ljava/lang/String;

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/String;->codePointAt(I)I

    #@f
    move-result v0

    #@10
    .line 169
    .local v0, codePoint:I
    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_18

    #@16
    const/4 v1, 0x1

    #@17
    .line 171
    .end local v0           #codePoint:I
    :goto_17
    return v1

    #@18
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_17
.end method


# virtual methods
.method public following(I)I
    .registers 5
    .parameter "offset"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 86
    iget v2, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@3
    sub-int v0, p1, v2

    #@5
    .line 88
    .local v0, shiftedOffset:I
    :cond_5
    iget-object v2, p0, Landroid/text/method/WordIterator;->mIterator:Ljava/text/BreakIterator;

    #@7
    invoke-virtual {v2, v0}, Ljava/text/BreakIterator;->following(I)I

    #@a
    move-result v0

    #@b
    .line 89
    if-ne v0, v1, :cond_e

    #@d
    .line 93
    :goto_d
    return v1

    #@e
    .line 92
    :cond_e
    invoke-direct {p0, v0}, Landroid/text/method/WordIterator;->isAfterLetterOrDigit(I)Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_5

    #@14
    .line 93
    iget v1, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@16
    add-int/2addr v1, v0

    #@17
    goto :goto_d
.end method

.method public getBeginning(I)I
    .registers 5
    .parameter "offset"

    #@0
    .prologue
    .line 111
    iget v1, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@2
    sub-int v0, p1, v1

    #@4
    .line 112
    .local v0, shiftedOffset:I
    invoke-direct {p0, v0}, Landroid/text/method/WordIterator;->checkOffsetIsValid(I)V

    #@7
    .line 114
    invoke-direct {p0, v0}, Landroid/text/method/WordIterator;->isOnLetterOrDigit(I)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_23

    #@d
    .line 115
    iget-object v1, p0, Landroid/text/method/WordIterator;->mIterator:Ljava/text/BreakIterator;

    #@f
    invoke-virtual {v1, v0}, Ljava/text/BreakIterator;->isBoundary(I)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_19

    #@15
    .line 116
    iget v1, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@17
    add-int/2addr v1, v0

    #@18
    .line 125
    :goto_18
    return v1

    #@19
    .line 118
    :cond_19
    iget-object v1, p0, Landroid/text/method/WordIterator;->mIterator:Ljava/text/BreakIterator;

    #@1b
    invoke-virtual {v1, v0}, Ljava/text/BreakIterator;->preceding(I)I

    #@1e
    move-result v1

    #@1f
    iget v2, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@21
    add-int/2addr v1, v2

    #@22
    goto :goto_18

    #@23
    .line 121
    :cond_23
    invoke-direct {p0, v0}, Landroid/text/method/WordIterator;->isAfterLetterOrDigit(I)Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_33

    #@29
    .line 122
    iget-object v1, p0, Landroid/text/method/WordIterator;->mIterator:Ljava/text/BreakIterator;

    #@2b
    invoke-virtual {v1, v0}, Ljava/text/BreakIterator;->preceding(I)I

    #@2e
    move-result v1

    #@2f
    iget v2, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@31
    add-int/2addr v1, v2

    #@32
    goto :goto_18

    #@33
    .line 125
    :cond_33
    const/4 v1, -0x1

    #@34
    goto :goto_18
.end method

.method public getEnd(I)I
    .registers 5
    .parameter "offset"

    #@0
    .prologue
    .line 141
    iget v1, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@2
    sub-int v0, p1, v1

    #@4
    .line 142
    .local v0, shiftedOffset:I
    invoke-direct {p0, v0}, Landroid/text/method/WordIterator;->checkOffsetIsValid(I)V

    #@7
    .line 144
    invoke-direct {p0, v0}, Landroid/text/method/WordIterator;->isAfterLetterOrDigit(I)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_23

    #@d
    .line 145
    iget-object v1, p0, Landroid/text/method/WordIterator;->mIterator:Ljava/text/BreakIterator;

    #@f
    invoke-virtual {v1, v0}, Ljava/text/BreakIterator;->isBoundary(I)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_19

    #@15
    .line 146
    iget v1, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@17
    add-int/2addr v1, v0

    #@18
    .line 155
    :goto_18
    return v1

    #@19
    .line 148
    :cond_19
    iget-object v1, p0, Landroid/text/method/WordIterator;->mIterator:Ljava/text/BreakIterator;

    #@1b
    invoke-virtual {v1, v0}, Ljava/text/BreakIterator;->following(I)I

    #@1e
    move-result v1

    #@1f
    iget v2, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@21
    add-int/2addr v1, v2

    #@22
    goto :goto_18

    #@23
    .line 151
    :cond_23
    invoke-direct {p0, v0}, Landroid/text/method/WordIterator;->isOnLetterOrDigit(I)Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_33

    #@29
    .line 152
    iget-object v1, p0, Landroid/text/method/WordIterator;->mIterator:Ljava/text/BreakIterator;

    #@2b
    invoke-virtual {v1, v0}, Ljava/text/BreakIterator;->following(I)I

    #@2e
    move-result v1

    #@2f
    iget v2, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@31
    add-int/2addr v1, v2

    #@32
    goto :goto_18

    #@33
    .line 155
    :cond_33
    const/4 v1, -0x1

    #@34
    goto :goto_18
.end method

.method public preceding(I)I
    .registers 5
    .parameter "offset"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 72
    iget v2, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@3
    sub-int v0, p1, v2

    #@5
    .line 74
    .local v0, shiftedOffset:I
    :cond_5
    iget-object v2, p0, Landroid/text/method/WordIterator;->mIterator:Ljava/text/BreakIterator;

    #@7
    invoke-virtual {v2, v0}, Ljava/text/BreakIterator;->preceding(I)I

    #@a
    move-result v0

    #@b
    .line 75
    if-ne v0, v1, :cond_e

    #@d
    .line 79
    :goto_d
    return v1

    #@e
    .line 78
    :cond_e
    invoke-direct {p0, v0}, Landroid/text/method/WordIterator;->isOnLetterOrDigit(I)Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_5

    #@14
    .line 79
    iget v1, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@16
    add-int/2addr v1, v0

    #@17
    goto :goto_d
.end method

.method public setCharSequence(Ljava/lang/CharSequence;II)V
    .registers 7
    .parameter "charSequence"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 59
    const/4 v1, 0x0

    #@1
    add-int/lit8 v2, p2, -0x32

    #@3
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@6
    move-result v1

    #@7
    iput v1, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@9
    .line 60
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@c
    move-result v1

    #@d
    add-int/lit8 v2, p3, 0x32

    #@f
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@12
    move-result v0

    #@13
    .line 62
    .local v0, windowEnd:I
    instance-of v1, p1, Landroid/text/SpannableStringBuilder;

    #@15
    if-eqz v1, :cond_29

    #@17
    .line 63
    check-cast p1, Landroid/text/SpannableStringBuilder;

    #@19
    .end local p1
    iget v1, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@1b
    invoke-virtual {p1, v1, v0}, Landroid/text/SpannableStringBuilder;->substring(II)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    iput-object v1, p0, Landroid/text/method/WordIterator;->mString:Ljava/lang/String;

    #@21
    .line 67
    :goto_21
    iget-object v1, p0, Landroid/text/method/WordIterator;->mIterator:Ljava/text/BreakIterator;

    #@23
    iget-object v2, p0, Landroid/text/method/WordIterator;->mString:Ljava/lang/String;

    #@25
    invoke-virtual {v1, v2}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    #@28
    .line 68
    return-void

    #@29
    .line 65
    .restart local p1
    :cond_29
    iget v1, p0, Landroid/text/method/WordIterator;->mOffsetShift:I

    #@2b
    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    iput-object v1, p0, Landroid/text/method/WordIterator;->mString:Ljava/lang/String;

    #@35
    goto :goto_21
.end method
