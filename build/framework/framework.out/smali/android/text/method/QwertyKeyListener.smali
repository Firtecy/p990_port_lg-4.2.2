.class public Landroid/text/method/QwertyKeyListener;
.super Landroid/text/method/BaseKeyListener;
.source "QwertyKeyListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/method/QwertyKeyListener$Replaced;
    }
.end annotation


# static fields
.field private static PICKER_SETS:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sFullKeyboardInstance:Landroid/text/method/QwertyKeyListener;

.field private static sInstance:[Landroid/text/method/QwertyKeyListener;


# instance fields
.field private mAutoCap:Landroid/text/method/TextKeyListener$Capitalize;

.field private mAutoText:Z

.field private mFullKeyboard:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 36
    invoke-static {}, Landroid/text/method/TextKeyListener$Capitalize;->values()[Landroid/text/method/TextKeyListener$Capitalize;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    mul-int/lit8 v0, v0, 0x2

    #@7
    new-array v0, v0, [Landroid/text/method/QwertyKeyListener;

    #@9
    sput-object v0, Landroid/text/method/QwertyKeyListener;->sInstance:[Landroid/text/method/QwertyKeyListener;

    #@b
    .line 430
    new-instance v0, Landroid/util/SparseArray;

    #@d
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@10
    sput-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@12
    .line 433
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@14
    const/16 v1, 0x41

    #@16
    const-string/jumbo v2, "\u00c0\u00c1\u00c2\u00c4\u00c6\u00c3\u00c5\u0104\u0100"

    #@19
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1c
    .line 434
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1e
    const/16 v1, 0x43

    #@20
    const-string/jumbo v2, "\u00c7\u0106\u010c"

    #@23
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@26
    .line 435
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@28
    const/16 v1, 0x44

    #@2a
    const-string/jumbo v2, "\u010e"

    #@2d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@30
    .line 436
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@32
    const/16 v1, 0x45

    #@34
    const-string/jumbo v2, "\u00c8\u00c9\u00ca\u00cb\u0118\u011a\u0112"

    #@37
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@3a
    .line 437
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@3c
    const/16 v1, 0x47

    #@3e
    const-string/jumbo v2, "\u011e"

    #@41
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@44
    .line 438
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@46
    const/16 v1, 0x4c

    #@48
    const-string/jumbo v2, "\u0141"

    #@4b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@4e
    .line 439
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@50
    const/16 v1, 0x49

    #@52
    const-string/jumbo v2, "\u00cc\u00cd\u00ce\u00cf\u012a\u0130"

    #@55
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@58
    .line 440
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@5a
    const/16 v1, 0x4e

    #@5c
    const-string/jumbo v2, "\u00d1\u0143\u0147"

    #@5f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@62
    .line 441
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@64
    const/16 v1, 0x4f

    #@66
    const-string/jumbo v2, "\u00d8\u0152\u00d5\u00d2\u00d3\u00d4\u00d6\u014c"

    #@69
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@6c
    .line 442
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@6e
    const/16 v1, 0x52

    #@70
    const-string/jumbo v2, "\u0158"

    #@73
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@76
    .line 443
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@78
    const/16 v1, 0x53

    #@7a
    const-string/jumbo v2, "\u015a\u0160\u015e"

    #@7d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@80
    .line 444
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@82
    const/16 v1, 0x54

    #@84
    const-string/jumbo v2, "\u0164"

    #@87
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@8a
    .line 445
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@8c
    const/16 v1, 0x55

    #@8e
    const-string/jumbo v2, "\u00d9\u00da\u00db\u00dc\u016e\u016a"

    #@91
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@94
    .line 446
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@96
    const/16 v1, 0x59

    #@98
    const-string/jumbo v2, "\u00dd\u0178"

    #@9b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@9e
    .line 447
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@a0
    const/16 v1, 0x5a

    #@a2
    const-string/jumbo v2, "\u0179\u017b\u017d"

    #@a5
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@a8
    .line 448
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@aa
    const/16 v1, 0x61

    #@ac
    const-string/jumbo v2, "\u00e0\u00e1\u00e2\u00e4\u00e6\u00e3\u00e5\u0105\u0101"

    #@af
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@b2
    .line 449
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@b4
    const/16 v1, 0x63

    #@b6
    const-string/jumbo v2, "\u00e7\u0107\u010d"

    #@b9
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@bc
    .line 450
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@be
    const/16 v1, 0x64

    #@c0
    const-string/jumbo v2, "\u010f"

    #@c3
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@c6
    .line 451
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@c8
    const/16 v1, 0x65

    #@ca
    const-string/jumbo v2, "\u00e8\u00e9\u00ea\u00eb\u0119\u011b\u0113"

    #@cd
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@d0
    .line 452
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@d2
    const/16 v1, 0x67

    #@d4
    const-string/jumbo v2, "\u011f"

    #@d7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@da
    .line 453
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@dc
    const/16 v1, 0x69

    #@de
    const-string/jumbo v2, "\u00ec\u00ed\u00ee\u00ef\u012b\u0131"

    #@e1
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@e4
    .line 454
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@e6
    const/16 v1, 0x6c

    #@e8
    const-string/jumbo v2, "\u0142"

    #@eb
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@ee
    .line 455
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@f0
    const/16 v1, 0x6e

    #@f2
    const-string/jumbo v2, "\u00f1\u0144\u0148"

    #@f5
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@f8
    .line 456
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@fa
    const/16 v1, 0x6f

    #@fc
    const-string/jumbo v2, "\u00f8\u0153\u00f5\u00f2\u00f3\u00f4\u00f6\u014d"

    #@ff
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@102
    .line 457
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@104
    const/16 v1, 0x72

    #@106
    const-string/jumbo v2, "\u0159"

    #@109
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@10c
    .line 458
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@10e
    const/16 v1, 0x73

    #@110
    const-string/jumbo v2, "\u00a7\u00df\u015b\u0161\u015f"

    #@113
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@116
    .line 459
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@118
    const/16 v1, 0x74

    #@11a
    const-string/jumbo v2, "\u0165"

    #@11d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@120
    .line 460
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@122
    const/16 v1, 0x75

    #@124
    const-string/jumbo v2, "\u00f9\u00fa\u00fb\u00fc\u016f\u016b"

    #@127
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@12a
    .line 461
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@12c
    const/16 v1, 0x79

    #@12e
    const-string/jumbo v2, "\u00fd\u00ff"

    #@131
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@134
    .line 462
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@136
    const/16 v1, 0x7a

    #@138
    const-string/jumbo v2, "\u017a\u017c\u017e"

    #@13b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@13e
    .line 463
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@140
    const v1, 0xef01

    #@143
    const-string/jumbo v2, "\u2026\u00a5\u2022\u00ae\u00a9\u00b1[]{}\\|"

    #@146
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@149
    .line 465
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@14b
    const/16 v1, 0x2f

    #@14d
    const-string v2, "\\"

    #@14f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@152
    .line 469
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@154
    const/16 v1, 0x31

    #@156
    const-string/jumbo v2, "\u00b9\u00bd\u2153\u00bc\u215b"

    #@159
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@15c
    .line 470
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@15e
    const/16 v1, 0x32

    #@160
    const-string/jumbo v2, "\u00b2\u2154"

    #@163
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@166
    .line 471
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@168
    const/16 v1, 0x33

    #@16a
    const-string/jumbo v2, "\u00b3\u00be\u215c"

    #@16d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@170
    .line 472
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@172
    const/16 v1, 0x34

    #@174
    const-string/jumbo v2, "\u2074"

    #@177
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@17a
    .line 473
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@17c
    const/16 v1, 0x35

    #@17e
    const-string/jumbo v2, "\u215d"

    #@181
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@184
    .line 474
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@186
    const/16 v1, 0x37

    #@188
    const-string/jumbo v2, "\u215e"

    #@18b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@18e
    .line 475
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@190
    const/16 v1, 0x30

    #@192
    const-string/jumbo v2, "\u207f\u2205"

    #@195
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@198
    .line 476
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@19a
    const/16 v1, 0x24

    #@19c
    const-string/jumbo v2, "\u00a2\u00a3\u20ac\u00a5\u20a3\u20a4\u20b1"

    #@19f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1a2
    .line 477
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1a4
    const/16 v1, 0x25

    #@1a6
    const-string/jumbo v2, "\u2030"

    #@1a9
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1ac
    .line 478
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1ae
    const/16 v1, 0x2a

    #@1b0
    const-string/jumbo v2, "\u2020\u2021"

    #@1b3
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1b6
    .line 479
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1b8
    const/16 v1, 0x2d

    #@1ba
    const-string/jumbo v2, "\u2013\u2014"

    #@1bd
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1c0
    .line 480
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1c2
    const/16 v1, 0x2b

    #@1c4
    const-string/jumbo v2, "\u00b1"

    #@1c7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1ca
    .line 481
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1cc
    const/16 v1, 0x28

    #@1ce
    const-string v2, "[{<"

    #@1d0
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1d3
    .line 482
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1d5
    const/16 v1, 0x29

    #@1d7
    const-string v2, "]}>"

    #@1d9
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1dc
    .line 483
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1de
    const/16 v1, 0x21

    #@1e0
    const-string/jumbo v2, "\u00a1"

    #@1e3
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1e6
    .line 484
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1e8
    const/16 v1, 0x22

    #@1ea
    const-string/jumbo v2, "\u201c\u201d\u00ab\u00bb\u02dd"

    #@1ed
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1f0
    .line 485
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1f2
    const/16 v1, 0x3f

    #@1f4
    const-string/jumbo v2, "\u00bf"

    #@1f7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1fa
    .line 486
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@1fc
    const/16 v1, 0x2c

    #@1fe
    const-string/jumbo v2, "\u201a\u201e"

    #@201
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@204
    .line 490
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@206
    const/16 v1, 0x3d

    #@208
    const-string/jumbo v2, "\u2260\u2248\u221e"

    #@20b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@20e
    .line 491
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@210
    const/16 v1, 0x3c

    #@212
    const-string/jumbo v2, "\u2264\u00ab\u2039"

    #@215
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@218
    .line 492
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@21a
    const/16 v1, 0x3e

    #@21c
    const-string/jumbo v2, "\u2265\u00bb\u203a"

    #@21f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@222
    .line 493
    return-void
.end method

.method public constructor <init>(Landroid/text/method/TextKeyListener$Capitalize;Z)V
    .registers 4
    .parameter "cap"
    .parameter "autoText"

    #@0
    .prologue
    .line 51
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/text/method/QwertyKeyListener;-><init>(Landroid/text/method/TextKeyListener$Capitalize;ZZ)V

    #@4
    .line 52
    return-void
.end method

.method private constructor <init>(Landroid/text/method/TextKeyListener$Capitalize;ZZ)V
    .registers 4
    .parameter "cap"
    .parameter "autoText"
    .parameter "fullKeyboard"

    #@0
    .prologue
    .line 44
    invoke-direct {p0}, Landroid/text/method/BaseKeyListener;-><init>()V

    #@3
    .line 45
    iput-object p1, p0, Landroid/text/method/QwertyKeyListener;->mAutoCap:Landroid/text/method/TextKeyListener$Capitalize;

    #@5
    .line 46
    iput-boolean p2, p0, Landroid/text/method/QwertyKeyListener;->mAutoText:Z

    #@7
    .line 47
    iput-boolean p3, p0, Landroid/text/method/QwertyKeyListener;->mFullKeyboard:Z

    #@9
    .line 48
    return-void
.end method

.method public static getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/QwertyKeyListener;
    .registers 5
    .parameter "autoText"
    .parameter "cap"

    #@0
    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/text/method/TextKeyListener$Capitalize;->ordinal()I

    #@3
    move-result v1

    #@4
    mul-int/lit8 v2, v1, 0x2

    #@6
    if-eqz p0, :cond_1f

    #@8
    const/4 v1, 0x1

    #@9
    :goto_9
    add-int v0, v2, v1

    #@b
    .line 61
    .local v0, off:I
    sget-object v1, Landroid/text/method/QwertyKeyListener;->sInstance:[Landroid/text/method/QwertyKeyListener;

    #@d
    aget-object v1, v1, v0

    #@f
    if-nez v1, :cond_1a

    #@11
    .line 62
    sget-object v1, Landroid/text/method/QwertyKeyListener;->sInstance:[Landroid/text/method/QwertyKeyListener;

    #@13
    new-instance v2, Landroid/text/method/QwertyKeyListener;

    #@15
    invoke-direct {v2, p1, p0}, Landroid/text/method/QwertyKeyListener;-><init>(Landroid/text/method/TextKeyListener$Capitalize;Z)V

    #@18
    aput-object v2, v1, v0

    #@1a
    .line 65
    :cond_1a
    sget-object v1, Landroid/text/method/QwertyKeyListener;->sInstance:[Landroid/text/method/QwertyKeyListener;

    #@1c
    aget-object v1, v1, v0

    #@1e
    return-object v1

    #@1f
    .line 59
    .end local v0           #off:I
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_9
.end method

.method public static getInstanceForFullKeyboard()Landroid/text/method/QwertyKeyListener;
    .registers 4

    #@0
    .prologue
    .line 74
    sget-object v0, Landroid/text/method/QwertyKeyListener;->sFullKeyboardInstance:Landroid/text/method/QwertyKeyListener;

    #@2
    if-nez v0, :cond_f

    #@4
    .line 75
    new-instance v0, Landroid/text/method/QwertyKeyListener;

    #@6
    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    #@8
    const/4 v2, 0x0

    #@9
    const/4 v3, 0x1

    #@a
    invoke-direct {v0, v1, v2, v3}, Landroid/text/method/QwertyKeyListener;-><init>(Landroid/text/method/TextKeyListener$Capitalize;ZZ)V

    #@d
    sput-object v0, Landroid/text/method/QwertyKeyListener;->sFullKeyboardInstance:Landroid/text/method/QwertyKeyListener;

    #@f
    .line 77
    :cond_f
    sget-object v0, Landroid/text/method/QwertyKeyListener;->sFullKeyboardInstance:Landroid/text/method/QwertyKeyListener;

    #@11
    return-object v0
.end method

.method private getReplacement(Ljava/lang/CharSequence;IILandroid/view/View;)Ljava/lang/String;
    .registers 15
    .parameter "src"
    .parameter "start"
    .parameter "end"
    .parameter "view"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v9, 0x0

    #@2
    .line 361
    sub-int v4, p3, p2

    #@4
    .line 362
    .local v4, len:I
    const/4 v1, 0x0

    #@5
    .line 364
    .local v1, changecase:Z
    invoke-static {p1, p2, p3, p4}, Landroid/text/AutoText;->get(Ljava/lang/CharSequence;IILandroid/view/View;)Ljava/lang/String;

    #@8
    move-result-object v6

    #@9
    .line 366
    .local v6, replacement:Ljava/lang/String;
    if-nez v6, :cond_1e

    #@b
    .line 367
    invoke-static {p1, p2, p3}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    #@e
    move-result-object v8

    #@f
    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    .line 368
    .local v3, key:Ljava/lang/String;
    sub-int v8, p3, p2

    #@15
    invoke-static {v3, v9, v8, p4}, Landroid/text/AutoText;->get(Ljava/lang/CharSequence;IILandroid/view/View;)Ljava/lang/String;

    #@18
    move-result-object v6

    #@19
    .line 369
    const/4 v1, 0x1

    #@1a
    .line 371
    if-nez v6, :cond_1e

    #@1c
    move-object v5, v7

    #@1d
    .line 399
    .end local v3           #key:Ljava/lang/String;
    :cond_1d
    :goto_1d
    return-object v5

    #@1e
    .line 375
    :cond_1e
    const/4 v0, 0x0

    #@1f
    .line 377
    .local v0, caps:I
    if-eqz v1, :cond_33

    #@21
    .line 378
    move v2, p2

    #@22
    .local v2, j:I
    :goto_22
    if-ge v2, p3, :cond_33

    #@24
    .line 379
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@27
    move-result v8

    #@28
    invoke-static {v8}, Ljava/lang/Character;->isUpperCase(C)Z

    #@2b
    move-result v8

    #@2c
    if-eqz v8, :cond_30

    #@2e
    .line 380
    add-int/lit8 v0, v0, 0x1

    #@30
    .line 378
    :cond_30
    add-int/lit8 v2, v2, 0x1

    #@32
    goto :goto_22

    #@33
    .line 386
    .end local v2           #j:I
    :cond_33
    if-nez v0, :cond_44

    #@35
    .line 387
    move-object v5, v6

    #@36
    .line 395
    .local v5, out:Ljava/lang/String;
    :goto_36
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@39
    move-result v8

    #@3a
    if-ne v8, v4, :cond_1d

    #@3c
    invoke-static {p1, p2, v5, v9, v4}, Landroid/text/TextUtils;->regionMatches(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    #@3f
    move-result v8

    #@40
    if-eqz v8, :cond_1d

    #@42
    move-object v5, v7

    #@43
    .line 397
    goto :goto_1d

    #@44
    .line 388
    .end local v5           #out:Ljava/lang/String;
    :cond_44
    const/4 v8, 0x1

    #@45
    if-ne v0, v8, :cond_4c

    #@47
    .line 389
    invoke-static {v6}, Landroid/text/method/QwertyKeyListener;->toTitleCase(Ljava/lang/String;)Ljava/lang/String;

    #@4a
    move-result-object v5

    #@4b
    .restart local v5       #out:Ljava/lang/String;
    goto :goto_36

    #@4c
    .line 390
    .end local v5           #out:Ljava/lang/String;
    :cond_4c
    if-ne v0, v4, :cond_53

    #@4e
    .line 391
    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    .restart local v5       #out:Ljava/lang/String;
    goto :goto_36

    #@53
    .line 393
    .end local v5           #out:Ljava/lang/String;
    :cond_53
    invoke-static {v6}, Landroid/text/method/QwertyKeyListener;->toTitleCase(Ljava/lang/String;)Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    .restart local v5       #out:Ljava/lang/String;
    goto :goto_36
.end method

.method public static markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V
    .registers 11
    .parameter "content"
    .parameter "start"
    .parameter "end"
    .parameter "original"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 417
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@4
    move-result v4

    #@5
    const-class v5, Landroid/text/method/QwertyKeyListener$Replaced;

    #@7
    invoke-interface {p0, v6, v4, v5}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@a
    move-result-object v3

    #@b
    check-cast v3, [Landroid/text/method/QwertyKeyListener$Replaced;

    #@d
    .line 418
    .local v3, repl:[Landroid/text/method/QwertyKeyListener$Replaced;
    const/4 v0, 0x0

    #@e
    .local v0, a:I
    :goto_e
    array-length v4, v3

    #@f
    if-ge v0, v4, :cond_19

    #@11
    .line 419
    aget-object v4, v3, v0

    #@13
    invoke-interface {p0, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@16
    .line 418
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_e

    #@19
    .line 422
    :cond_19
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    #@1c
    move-result v1

    #@1d
    .line 423
    .local v1, len:I
    new-array v2, v1, [C

    #@1f
    .line 424
    .local v2, orig:[C
    invoke-virtual {p3, v6, v1, v2, v6}, Ljava/lang/String;->getChars(II[CI)V

    #@22
    .line 426
    new-instance v4, Landroid/text/method/QwertyKeyListener$Replaced;

    #@24
    invoke-direct {v4, v2}, Landroid/text/method/QwertyKeyListener$Replaced;-><init>([C)V

    #@27
    const/16 v5, 0x21

    #@29
    invoke-interface {p0, v4, p1, p2, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@2c
    .line 428
    return-void
.end method

.method private showCharacterPicker(Landroid/view/View;Landroid/text/Editable;CZI)Z
    .registers 13
    .parameter "view"
    .parameter "content"
    .parameter "c"
    .parameter "insert"
    .parameter "count"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 497
    sget-object v0, Landroid/text/method/QwertyKeyListener;->PICKER_SETS:Landroid/util/SparseArray;

    #@3
    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v4

    #@7
    check-cast v4, Ljava/lang/String;

    #@9
    .line 498
    .local v4, set:Ljava/lang/String;
    if-nez v4, :cond_d

    #@b
    .line 499
    const/4 v0, 0x0

    #@c
    .line 507
    :goto_c
    return v0

    #@d
    .line 502
    :cond_d
    if-ne p5, v6, :cond_1e

    #@f
    .line 503
    new-instance v0, Landroid/text/method/CharacterPickerDialog;

    #@11
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@14
    move-result-object v1

    #@15
    move-object v2, p1

    #@16
    move-object v3, p2

    #@17
    move v5, p4

    #@18
    invoke-direct/range {v0 .. v5}, Landroid/text/method/CharacterPickerDialog;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/text/Editable;Ljava/lang/String;Z)V

    #@1b
    invoke-virtual {v0}, Landroid/text/method/CharacterPickerDialog;->show()V

    #@1e
    :cond_1e
    move v0, v6

    #@1f
    .line 507
    goto :goto_c
.end method

.method private static toTitleCase(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 511
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const/4 v1, 0x0

    #@6
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@9
    move-result v1

    #@a
    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    #@d
    move-result v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const/4 v1, 0x1

    #@13
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    return-object v0
.end method


# virtual methods
.method public getInputType()I
    .registers 3

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Landroid/text/method/QwertyKeyListener;->mAutoCap:Landroid/text/method/TextKeyListener$Capitalize;

    #@2
    iget-boolean v1, p0, Landroid/text/method/QwertyKeyListener;->mAutoText:Z

    #@4
    invoke-static {v0, v1}, Landroid/text/method/QwertyKeyListener;->makeTextContentType(Landroid/text/method/TextKeyListener$Capitalize;Z)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .registers 49
    .parameter "view"
    .parameter "content"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 87
    const/16 v34, 0x0

    #@2
    .line 89
    .local v34, pref:I
    if-eqz p1, :cond_10

    #@4
    .line 90
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    #@7
    move-result-object v5

    #@8
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@b
    move-result-object v6

    #@c
    invoke-virtual {v5, v6}, Landroid/text/method/TextKeyListener;->getPrefs(Landroid/content/Context;)I

    #@f
    move-result v34

    #@10
    .line 94
    :cond_10
    invoke-static/range {p2 .. p2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@13
    move-result v17

    #@14
    .line 95
    .local v17, a:I
    invoke-static/range {p2 .. p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@17
    move-result v21

    #@18
    .line 97
    .local v21, b:I
    move/from16 v0, v17

    #@1a
    move/from16 v1, v21

    #@1c
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@1f
    move-result v39

    #@20
    .line 98
    .local v39, selStart:I
    move/from16 v0, v17

    #@22
    move/from16 v1, v21

    #@24
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@27
    move-result v38

    #@28
    .line 100
    .local v38, selEnd:I
    if-ltz v39, :cond_2c

    #@2a
    if-gez v38, :cond_37

    #@2c
    .line 101
    :cond_2c
    const/16 v38, 0x0

    #@2e
    move/from16 v39, v38

    #@30
    .line 102
    const/4 v5, 0x0

    #@31
    const/4 v6, 0x0

    #@32
    move-object/from16 v0, p2

    #@34
    invoke-static {v0, v5, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@37
    .line 106
    :cond_37
    sget-object v5, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@39
    move-object/from16 v0, p2

    #@3b
    invoke-interface {v0, v5}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@3e
    move-result v20

    #@3f
    .line 107
    .local v20, activeStart:I
    sget-object v5, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@41
    move-object/from16 v0, p2

    #@43
    invoke-interface {v0, v5}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@46
    move-result v19

    #@47
    .line 111
    .local v19, activeEnd:I
    invoke-virtual/range {p4 .. p4}, Landroid/view/KeyEvent;->getMetaState()I

    #@4a
    move-result v5

    #@4b
    invoke-static/range {p2 .. p2}, Landroid/text/method/QwertyKeyListener;->getMetaState(Ljava/lang/CharSequence;)I

    #@4e
    move-result v6

    #@4f
    or-int/2addr v5, v6

    #@50
    move-object/from16 v0, p4

    #@52
    invoke-virtual {v0, v5}, Landroid/view/KeyEvent;->getUnicodeChar(I)I

    #@55
    move-result v29

    #@56
    .line 113
    .local v29, i:I
    move-object/from16 v0, p0

    #@58
    iget-boolean v5, v0, Landroid/text/method/QwertyKeyListener;->mFullKeyboard:Z

    #@5a
    if-nez v5, :cond_90

    #@5c
    .line 114
    invoke-virtual/range {p4 .. p4}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@5f
    move-result v10

    #@60
    .line 115
    .local v10, count:I
    if-lez v10, :cond_90

    #@62
    move/from16 v0, v39

    #@64
    move/from16 v1, v38

    #@66
    if-ne v0, v1, :cond_90

    #@68
    if-lez v39, :cond_90

    #@6a
    .line 116
    add-int/lit8 v5, v39, -0x1

    #@6c
    move-object/from16 v0, p2

    #@6e
    invoke-interface {v0, v5}, Landroid/text/Editable;->charAt(I)C

    #@71
    move-result v8

    #@72
    .line 118
    .local v8, c:C
    move/from16 v0, v29

    #@74
    if-eq v8, v0, :cond_7e

    #@76
    invoke-static/range {v29 .. v29}, Ljava/lang/Character;->toUpperCase(I)I

    #@79
    move-result v5

    #@7a
    if-ne v8, v5, :cond_90

    #@7c
    if-eqz p1, :cond_90

    #@7e
    .line 119
    :cond_7e
    const/4 v9, 0x0

    #@7f
    move-object/from16 v5, p0

    #@81
    move-object/from16 v6, p1

    #@83
    move-object/from16 v7, p2

    #@85
    invoke-direct/range {v5 .. v10}, Landroid/text/method/QwertyKeyListener;->showCharacterPicker(Landroid/view/View;Landroid/text/Editable;CZI)Z

    #@88
    move-result v5

    #@89
    if-eqz v5, :cond_90

    #@8b
    .line 120
    invoke-static/range {p2 .. p2}, Landroid/text/method/QwertyKeyListener;->resetMetaState(Landroid/text/Spannable;)V

    #@8e
    .line 121
    const/4 v5, 0x1

    #@8f
    .line 356
    .end local v8           #c:C
    .end local v10           #count:I
    :goto_8f
    return v5

    #@90
    .line 127
    :cond_90
    const v5, 0xef01

    #@93
    move/from16 v0, v29

    #@95
    if-ne v0, v5, :cond_ad

    #@97
    .line 128
    if-eqz p1, :cond_a8

    #@99
    .line 129
    const v14, 0xef01

    #@9c
    const/4 v15, 0x1

    #@9d
    const/16 v16, 0x1

    #@9f
    move-object/from16 v11, p0

    #@a1
    move-object/from16 v12, p1

    #@a3
    move-object/from16 v13, p2

    #@a5
    invoke-direct/range {v11 .. v16}, Landroid/text/method/QwertyKeyListener;->showCharacterPicker(Landroid/view/View;Landroid/text/Editable;CZI)Z

    #@a8
    .line 132
    :cond_a8
    invoke-static/range {p2 .. p2}, Landroid/text/method/QwertyKeyListener;->resetMetaState(Landroid/text/Spannable;)V

    #@ab
    .line 133
    const/4 v5, 0x1

    #@ac
    goto :goto_8f

    #@ad
    .line 136
    :cond_ad
    const v5, 0xef00

    #@b0
    move/from16 v0, v29

    #@b2
    if-ne v0, v5, :cond_fb

    #@b4
    .line 139
    move/from16 v0, v39

    #@b6
    move/from16 v1, v38

    #@b8
    if-ne v0, v1, :cond_d6

    #@ba
    .line 140
    move/from16 v41, v38

    #@bc
    .line 143
    .local v41, start:I
    :goto_bc
    if-lez v41, :cond_d8

    #@be
    sub-int v5, v38, v41

    #@c0
    const/4 v6, 0x4

    #@c1
    if-ge v5, v6, :cond_d8

    #@c3
    add-int/lit8 v5, v41, -0x1

    #@c5
    move-object/from16 v0, p2

    #@c7
    invoke-interface {v0, v5}, Landroid/text/Editable;->charAt(I)C

    #@ca
    move-result v5

    #@cb
    const/16 v6, 0x10

    #@cd
    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    #@d0
    move-result v5

    #@d1
    if-ltz v5, :cond_d8

    #@d3
    .line 144
    add-int/lit8 v41, v41, -0x1

    #@d5
    goto :goto_bc

    #@d6
    .line 147
    .end local v41           #start:I
    :cond_d6
    move/from16 v41, v39

    #@d8
    .line 150
    .restart local v41       #start:I
    :cond_d8
    const/16 v22, -0x1

    #@da
    .line 152
    .local v22, ch:I
    :try_start_da
    move-object/from16 v0, p2

    #@dc
    move/from16 v1, v41

    #@de
    move/from16 v2, v38

    #@e0
    invoke-static {v0, v1, v2}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    #@e3
    move-result-object v28

    #@e4
    .line 153
    .local v28, hex:Ljava/lang/String;
    const/16 v5, 0x10

    #@e6
    move-object/from16 v0, v28

    #@e8
    invoke-static {v0, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_eb
    .catch Ljava/lang/NumberFormatException; {:try_start_da .. :try_end_eb} :catch_408

    #@eb
    move-result v22

    #@ec
    .line 156
    .end local v28           #hex:Ljava/lang/String;
    :goto_ec
    if-ltz v22, :cond_281

    #@ee
    .line 157
    move/from16 v39, v41

    #@f0
    .line 158
    move-object/from16 v0, p2

    #@f2
    move/from16 v1, v39

    #@f4
    move/from16 v2, v38

    #@f6
    invoke-static {v0, v1, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@f9
    .line 159
    move/from16 v29, v22

    #@fb
    .line 165
    .end local v22           #ch:I
    .end local v41           #start:I
    :cond_fb
    :goto_fb
    if-eqz v29, :cond_342

    #@fd
    .line 166
    const/16 v25, 0x0

    #@ff
    .line 168
    .local v25, dead:Z
    const/high16 v5, -0x8000

    #@101
    and-int v5, v5, v29

    #@103
    if-eqz v5, :cond_10c

    #@105
    .line 169
    const/16 v25, 0x1

    #@107
    .line 170
    const v5, 0x7fffffff

    #@10a
    and-int v29, v29, v5

    #@10c
    .line 173
    :cond_10c
    move/from16 v0, v20

    #@10e
    move/from16 v1, v39

    #@110
    if-ne v0, v1, :cond_148

    #@112
    move/from16 v0, v19

    #@114
    move/from16 v1, v38

    #@116
    if-ne v0, v1, :cond_148

    #@118
    .line 174
    const/16 v37, 0x0

    #@11a
    .line 176
    .local v37, replace:Z
    sub-int v5, v38, v39

    #@11c
    add-int/lit8 v5, v5, -0x1

    #@11e
    if-nez v5, :cond_136

    #@120
    .line 177
    move-object/from16 v0, p2

    #@122
    move/from16 v1, v39

    #@124
    invoke-interface {v0, v1}, Landroid/text/Editable;->charAt(I)C

    #@127
    move-result v18

    #@128
    .line 178
    .local v18, accent:C
    move/from16 v0, v18

    #@12a
    move/from16 v1, v29

    #@12c
    invoke-static {v0, v1}, Landroid/view/KeyEvent;->getDeadChar(II)I

    #@12f
    move-result v23

    #@130
    .line 180
    .local v23, composed:I
    if-eqz v23, :cond_136

    #@132
    .line 181
    move/from16 v29, v23

    #@134
    .line 182
    const/16 v37, 0x1

    #@136
    .line 186
    .end local v18           #accent:C
    .end local v23           #composed:I
    :cond_136
    if-nez v37, :cond_148

    #@138
    .line 187
    move-object/from16 v0, p2

    #@13a
    move/from16 v1, v38

    #@13c
    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@13f
    .line 188
    sget-object v5, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@141
    move-object/from16 v0, p2

    #@143
    invoke-interface {v0, v5}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@146
    .line 189
    move/from16 v39, v38

    #@148
    .line 193
    .end local v37           #replace:Z
    :cond_148
    and-int/lit8 v5, v34, 0x1

    #@14a
    if-eqz v5, :cond_187

    #@14c
    invoke-static/range {v29 .. v29}, Ljava/lang/Character;->isLowerCase(I)Z

    #@14f
    move-result v5

    #@150
    if-eqz v5, :cond_187

    #@152
    move-object/from16 v0, p0

    #@154
    iget-object v5, v0, Landroid/text/method/QwertyKeyListener;->mAutoCap:Landroid/text/method/TextKeyListener$Capitalize;

    #@156
    move-object/from16 v0, p2

    #@158
    move/from16 v1, v39

    #@15a
    invoke-static {v5, v0, v1}, Landroid/text/method/TextKeyListener;->shouldCap(Landroid/text/method/TextKeyListener$Capitalize;Ljava/lang/CharSequence;I)Z

    #@15d
    move-result v5

    #@15e
    if-eqz v5, :cond_187

    #@160
    .line 196
    sget-object v5, Landroid/text/method/TextKeyListener;->CAPPED:Ljava/lang/Object;

    #@162
    move-object/from16 v0, p2

    #@164
    invoke-interface {v0, v5}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@167
    move-result v42

    #@168
    .line 197
    .local v42, where:I
    sget-object v5, Landroid/text/method/TextKeyListener;->CAPPED:Ljava/lang/Object;

    #@16a
    move-object/from16 v0, p2

    #@16c
    invoke-interface {v0, v5}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    #@16f
    move-result v27

    #@170
    .line 199
    .local v27, flags:I
    move/from16 v0, v42

    #@172
    move/from16 v1, v39

    #@174
    if-ne v0, v1, :cond_285

    #@176
    shr-int/lit8 v5, v27, 0x10

    #@178
    const v6, 0xffff

    #@17b
    and-int/2addr v5, v6

    #@17c
    move/from16 v0, v29

    #@17e
    if-ne v5, v0, :cond_285

    #@180
    .line 200
    sget-object v5, Landroid/text/method/TextKeyListener;->CAPPED:Ljava/lang/Object;

    #@182
    move-object/from16 v0, p2

    #@184
    invoke-interface {v0, v5}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@187
    .line 216
    .end local v27           #flags:I
    .end local v42           #where:I
    :cond_187
    :goto_187
    move/from16 v0, v39

    #@189
    move/from16 v1, v38

    #@18b
    if-eq v0, v1, :cond_194

    #@18d
    .line 217
    move-object/from16 v0, p2

    #@18f
    move/from16 v1, v38

    #@191
    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@194
    .line 219
    :cond_194
    sget-object v5, Landroid/text/method/QwertyKeyListener;->OLD_SEL_START:Ljava/lang/Object;

    #@196
    const/16 v6, 0x11

    #@198
    move-object/from16 v0, p2

    #@19a
    move/from16 v1, v39

    #@19c
    move/from16 v2, v39

    #@19e
    invoke-interface {v0, v5, v1, v2, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@1a1
    .line 222
    move/from16 v0, v29

    #@1a3
    int-to-char v5, v0

    #@1a4
    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@1a7
    move-result-object v5

    #@1a8
    move-object/from16 v0, p2

    #@1aa
    move/from16 v1, v39

    #@1ac
    move/from16 v2, v38

    #@1ae
    invoke-interface {v0, v1, v2, v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@1b1
    .line 224
    sget-object v5, Landroid/text/method/QwertyKeyListener;->OLD_SEL_START:Ljava/lang/Object;

    #@1b3
    move-object/from16 v0, p2

    #@1b5
    invoke-interface {v0, v5}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@1b8
    move-result v32

    #@1b9
    .line 225
    .local v32, oldStart:I
    invoke-static/range {p2 .. p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@1bc
    move-result v38

    #@1bd
    .line 227
    move/from16 v0, v32

    #@1bf
    move/from16 v1, v38

    #@1c1
    if-ge v0, v1, :cond_1e8

    #@1c3
    .line 228
    sget-object v5, Landroid/text/method/TextKeyListener;->LAST_TYPED:Ljava/lang/Object;

    #@1c5
    const/16 v6, 0x21

    #@1c7
    move-object/from16 v0, p2

    #@1c9
    move/from16 v1, v32

    #@1cb
    move/from16 v2, v38

    #@1cd
    invoke-interface {v0, v5, v1, v2, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@1d0
    .line 232
    if-eqz v25, :cond_1e8

    #@1d2
    .line 233
    move-object/from16 v0, p2

    #@1d4
    move/from16 v1, v32

    #@1d6
    move/from16 v2, v38

    #@1d8
    invoke-static {v0, v1, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@1db
    .line 234
    sget-object v5, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@1dd
    const/16 v6, 0x21

    #@1df
    move-object/from16 v0, p2

    #@1e1
    move/from16 v1, v32

    #@1e3
    move/from16 v2, v38

    #@1e5
    invoke-interface {v0, v5, v1, v2, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@1e8
    .line 239
    :cond_1e8
    invoke-static/range {p2 .. p2}, Landroid/text/method/QwertyKeyListener;->adjustMetaAfterKeypress(Landroid/text/Spannable;)V

    #@1eb
    .line 244
    and-int/lit8 v5, v34, 0x2

    #@1ed
    if-eqz v5, :cond_2db

    #@1ef
    move-object/from16 v0, p0

    #@1f1
    iget-boolean v5, v0, Landroid/text/method/QwertyKeyListener;->mAutoText:Z

    #@1f3
    if-eqz v5, :cond_2db

    #@1f5
    const/16 v5, 0x20

    #@1f7
    move/from16 v0, v29

    #@1f9
    if-eq v0, v5, :cond_22d

    #@1fb
    const/16 v5, 0x9

    #@1fd
    move/from16 v0, v29

    #@1ff
    if-eq v0, v5, :cond_22d

    #@201
    const/16 v5, 0xa

    #@203
    move/from16 v0, v29

    #@205
    if-eq v0, v5, :cond_22d

    #@207
    const/16 v5, 0x2c

    #@209
    move/from16 v0, v29

    #@20b
    if-eq v0, v5, :cond_22d

    #@20d
    const/16 v5, 0x2e

    #@20f
    move/from16 v0, v29

    #@211
    if-eq v0, v5, :cond_22d

    #@213
    const/16 v5, 0x21

    #@215
    move/from16 v0, v29

    #@217
    if-eq v0, v5, :cond_22d

    #@219
    const/16 v5, 0x3f

    #@21b
    move/from16 v0, v29

    #@21d
    if-eq v0, v5, :cond_22d

    #@21f
    const/16 v5, 0x22

    #@221
    move/from16 v0, v29

    #@223
    if-eq v0, v5, :cond_22d

    #@225
    invoke-static/range {v29 .. v29}, Ljava/lang/Character;->getType(I)I

    #@228
    move-result v5

    #@229
    const/16 v6, 0x16

    #@22b
    if-ne v5, v6, :cond_2db

    #@22d
    :cond_22d
    sget-object v5, Landroid/text/method/TextKeyListener;->INHIBIT_REPLACEMENT:Ljava/lang/Object;

    #@22f
    move-object/from16 v0, p2

    #@231
    invoke-interface {v0, v5}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@234
    move-result v5

    #@235
    move/from16 v0, v32

    #@237
    if-eq v5, v0, :cond_2db

    #@239
    .line 252
    move/from16 v43, v32

    #@23b
    .local v43, x:I
    :goto_23b
    if-lez v43, :cond_24f

    #@23d
    .line 253
    add-int/lit8 v5, v43, -0x1

    #@23f
    move-object/from16 v0, p2

    #@241
    invoke-interface {v0, v5}, Landroid/text/Editable;->charAt(I)C

    #@244
    move-result v8

    #@245
    .line 254
    .restart local v8       #c:C
    const/16 v5, 0x27

    #@247
    if-eq v8, v5, :cond_2a9

    #@249
    invoke-static {v8}, Ljava/lang/Character;->isLetter(C)Z

    #@24c
    move-result v5

    #@24d
    if-nez v5, :cond_2a9

    #@24f
    .line 259
    .end local v8           #c:C
    :cond_24f
    move-object/from16 v0, p0

    #@251
    move-object/from16 v1, p2

    #@253
    move/from16 v2, v43

    #@255
    move/from16 v3, v32

    #@257
    move-object/from16 v4, p1

    #@259
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/text/method/QwertyKeyListener;->getReplacement(Ljava/lang/CharSequence;IILandroid/view/View;)Ljava/lang/String;

    #@25c
    move-result-object v35

    #@25d
    .line 261
    .local v35, rep:Ljava/lang/String;
    if-eqz v35, :cond_2db

    #@25f
    .line 262
    const/4 v5, 0x0

    #@260
    invoke-interface/range {p2 .. p2}, Landroid/text/Editable;->length()I

    #@263
    move-result v6

    #@264
    const-class v7, Landroid/text/method/QwertyKeyListener$Replaced;

    #@266
    move-object/from16 v0, p2

    #@268
    invoke-interface {v0, v5, v6, v7}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@26b
    move-result-object v36

    #@26c
    check-cast v36, [Landroid/text/method/QwertyKeyListener$Replaced;

    #@26e
    .line 264
    .local v36, repl:[Landroid/text/method/QwertyKeyListener$Replaced;
    const/16 v17, 0x0

    #@270
    :goto_270
    move-object/from16 v0, v36

    #@272
    array-length v5, v0

    #@273
    move/from16 v0, v17

    #@275
    if-ge v0, v5, :cond_2ac

    #@277
    .line 265
    aget-object v5, v36, v17

    #@279
    move-object/from16 v0, p2

    #@27b
    invoke-interface {v0, v5}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@27e
    .line 264
    add-int/lit8 v17, v17, 0x1

    #@280
    goto :goto_270

    #@281
    .line 161
    .end local v25           #dead:Z
    .end local v32           #oldStart:I
    .end local v35           #rep:Ljava/lang/String;
    .end local v36           #repl:[Landroid/text/method/QwertyKeyListener$Replaced;
    .end local v43           #x:I
    .restart local v22       #ch:I
    .restart local v41       #start:I
    :cond_281
    const/16 v29, 0x0

    #@283
    goto/16 :goto_fb

    #@285
    .line 202
    .end local v22           #ch:I
    .end local v41           #start:I
    .restart local v25       #dead:Z
    .restart local v27       #flags:I
    .restart local v42       #where:I
    :cond_285
    shl-int/lit8 v27, v29, 0x10

    #@287
    .line 203
    invoke-static/range {v29 .. v29}, Ljava/lang/Character;->toUpperCase(I)I

    #@28a
    move-result v29

    #@28b
    .line 205
    if-nez v39, :cond_29a

    #@28d
    .line 206
    sget-object v5, Landroid/text/method/TextKeyListener;->CAPPED:Ljava/lang/Object;

    #@28f
    const/4 v6, 0x0

    #@290
    const/4 v7, 0x0

    #@291
    or-int/lit8 v9, v27, 0x11

    #@293
    move-object/from16 v0, p2

    #@295
    invoke-interface {v0, v5, v6, v7, v9}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@298
    goto/16 :goto_187

    #@29a
    .line 209
    :cond_29a
    sget-object v5, Landroid/text/method/TextKeyListener;->CAPPED:Ljava/lang/Object;

    #@29c
    add-int/lit8 v6, v39, -0x1

    #@29e
    or-int/lit8 v7, v27, 0x21

    #@2a0
    move-object/from16 v0, p2

    #@2a2
    move/from16 v1, v39

    #@2a4
    invoke-interface {v0, v5, v6, v1, v7}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@2a7
    goto/16 :goto_187

    #@2a9
    .line 252
    .end local v27           #flags:I
    .end local v42           #where:I
    .restart local v8       #c:C
    .restart local v32       #oldStart:I
    .restart local v43       #x:I
    :cond_2a9
    add-int/lit8 v43, v43, -0x1

    #@2ab
    goto :goto_23b

    #@2ac
    .line 267
    .end local v8           #c:C
    .restart local v35       #rep:Ljava/lang/String;
    .restart local v36       #repl:[Landroid/text/method/QwertyKeyListener$Replaced;
    :cond_2ac
    sub-int v5, v32, v43

    #@2ae
    new-array v0, v5, [C

    #@2b0
    move-object/from16 v33, v0

    #@2b2
    .line 268
    .local v33, orig:[C
    const/4 v5, 0x0

    #@2b3
    move-object/from16 v0, p2

    #@2b5
    move/from16 v1, v43

    #@2b7
    move/from16 v2, v32

    #@2b9
    move-object/from16 v3, v33

    #@2bb
    invoke-static {v0, v1, v2, v3, v5}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@2be
    .line 270
    new-instance v5, Landroid/text/method/QwertyKeyListener$Replaced;

    #@2c0
    move-object/from16 v0, v33

    #@2c2
    invoke-direct {v5, v0}, Landroid/text/method/QwertyKeyListener$Replaced;-><init>([C)V

    #@2c5
    const/16 v6, 0x21

    #@2c7
    move-object/from16 v0, p2

    #@2c9
    move/from16 v1, v43

    #@2cb
    move/from16 v2, v32

    #@2cd
    invoke-interface {v0, v5, v1, v2, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@2d0
    .line 272
    move-object/from16 v0, p2

    #@2d2
    move/from16 v1, v43

    #@2d4
    move/from16 v2, v32

    #@2d6
    move-object/from16 v3, v35

    #@2d8
    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@2db
    .line 278
    .end local v33           #orig:[C
    .end local v35           #rep:Ljava/lang/String;
    .end local v36           #repl:[Landroid/text/method/QwertyKeyListener$Replaced;
    .end local v43           #x:I
    :cond_2db
    and-int/lit8 v5, v34, 0x4

    #@2dd
    if-eqz v5, :cond_33f

    #@2df
    move-object/from16 v0, p0

    #@2e1
    iget-boolean v5, v0, Landroid/text/method/QwertyKeyListener;->mAutoText:Z

    #@2e3
    if-eqz v5, :cond_33f

    #@2e5
    .line 279
    invoke-static/range {p2 .. p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@2e8
    move-result v38

    #@2e9
    .line 280
    add-int/lit8 v5, v38, -0x3

    #@2eb
    if-ltz v5, :cond_33f

    #@2ed
    .line 281
    add-int/lit8 v5, v38, -0x1

    #@2ef
    move-object/from16 v0, p2

    #@2f1
    invoke-interface {v0, v5}, Landroid/text/Editable;->charAt(I)C

    #@2f4
    move-result v5

    #@2f5
    const/16 v6, 0x20

    #@2f7
    if-ne v5, v6, :cond_33f

    #@2f9
    add-int/lit8 v5, v38, -0x2

    #@2fb
    move-object/from16 v0, p2

    #@2fd
    invoke-interface {v0, v5}, Landroid/text/Editable;->charAt(I)C

    #@300
    move-result v5

    #@301
    const/16 v6, 0x20

    #@303
    if-ne v5, v6, :cond_33f

    #@305
    .line 283
    add-int/lit8 v5, v38, -0x3

    #@307
    move-object/from16 v0, p2

    #@309
    invoke-interface {v0, v5}, Landroid/text/Editable;->charAt(I)C

    #@30c
    move-result v8

    #@30d
    .line 285
    .restart local v8       #c:C
    add-int/lit8 v30, v38, -0x3

    #@30f
    .local v30, j:I
    :goto_30f
    if-lez v30, :cond_328

    #@311
    .line 286
    const/16 v5, 0x22

    #@313
    if-eq v8, v5, :cond_31d

    #@315
    invoke-static {v8}, Ljava/lang/Character;->getType(C)I

    #@318
    move-result v5

    #@319
    const/16 v6, 0x16

    #@31b
    if-ne v5, v6, :cond_328

    #@31d
    .line 288
    :cond_31d
    add-int/lit8 v5, v30, -0x1

    #@31f
    move-object/from16 v0, p2

    #@321
    invoke-interface {v0, v5}, Landroid/text/Editable;->charAt(I)C

    #@324
    move-result v8

    #@325
    .line 285
    add-int/lit8 v30, v30, -0x1

    #@327
    goto :goto_30f

    #@328
    .line 294
    :cond_328
    invoke-static {v8}, Ljava/lang/Character;->isLetter(C)Z

    #@32b
    move-result v5

    #@32c
    if-nez v5, :cond_334

    #@32e
    invoke-static {v8}, Ljava/lang/Character;->isDigit(C)Z

    #@331
    move-result v5

    #@332
    if-eqz v5, :cond_33f

    #@334
    .line 295
    :cond_334
    add-int/lit8 v5, v38, -0x2

    #@336
    add-int/lit8 v6, v38, -0x1

    #@338
    const-string v7, "."

    #@33a
    move-object/from16 v0, p2

    #@33c
    invoke-interface {v0, v5, v6, v7}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@33f
    .line 301
    .end local v8           #c:C
    .end local v30           #j:I
    :cond_33f
    const/4 v5, 0x1

    #@340
    goto/16 :goto_8f

    #@342
    .line 302
    .end local v25           #dead:Z
    .end local v32           #oldStart:I
    :cond_342
    const/16 v5, 0x43

    #@344
    move/from16 v0, p3

    #@346
    if-ne v0, v5, :cond_402

    #@348
    invoke-virtual/range {p4 .. p4}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@34b
    move-result v5

    #@34c
    if-nez v5, :cond_357

    #@34e
    const/4 v5, 0x2

    #@34f
    move-object/from16 v0, p4

    #@351
    invoke-virtual {v0, v5}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@354
    move-result v5

    #@355
    if-eqz v5, :cond_402

    #@357
    :cond_357
    move/from16 v0, v39

    #@359
    move/from16 v1, v38

    #@35b
    if-ne v0, v1, :cond_402

    #@35d
    .line 307
    const/16 v24, 0x1

    #@35f
    .line 314
    .local v24, consider:I
    sget-object v5, Landroid/text/method/TextKeyListener;->LAST_TYPED:Ljava/lang/Object;

    #@361
    move-object/from16 v0, p2

    #@363
    invoke-interface {v0, v5}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@366
    move-result v5

    #@367
    move/from16 v0, v39

    #@369
    if-ne v5, v0, :cond_379

    #@36b
    .line 315
    add-int/lit8 v5, v39, -0x1

    #@36d
    move-object/from16 v0, p2

    #@36f
    invoke-interface {v0, v5}, Landroid/text/Editable;->charAt(I)C

    #@372
    move-result v5

    #@373
    const/16 v6, 0xa

    #@375
    if-eq v5, v6, :cond_379

    #@377
    .line 316
    const/16 v24, 0x2

    #@379
    .line 319
    :cond_379
    sub-int v5, v39, v24

    #@37b
    const-class v6, Landroid/text/method/QwertyKeyListener$Replaced;

    #@37d
    move-object/from16 v0, p2

    #@37f
    move/from16 v1, v39

    #@381
    invoke-interface {v0, v5, v1, v6}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@384
    move-result-object v36

    #@385
    check-cast v36, [Landroid/text/method/QwertyKeyListener$Replaced;

    #@387
    .line 322
    .restart local v36       #repl:[Landroid/text/method/QwertyKeyListener$Replaced;
    move-object/from16 v0, v36

    #@389
    array-length v5, v0

    #@38a
    if-lez v5, :cond_402

    #@38c
    .line 323
    const/4 v5, 0x0

    #@38d
    aget-object v5, v36, v5

    #@38f
    move-object/from16 v0, p2

    #@391
    invoke-interface {v0, v5}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@394
    move-result v40

    #@395
    .line 324
    .local v40, st:I
    const/4 v5, 0x0

    #@396
    aget-object v5, v36, v5

    #@398
    move-object/from16 v0, p2

    #@39a
    invoke-interface {v0, v5}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@39d
    move-result v26

    #@39e
    .line 325
    .local v26, en:I
    new-instance v31, Ljava/lang/String;

    #@3a0
    const/4 v5, 0x0

    #@3a1
    aget-object v5, v36, v5

    #@3a3
    invoke-static {v5}, Landroid/text/method/QwertyKeyListener$Replaced;->access$000(Landroid/text/method/QwertyKeyListener$Replaced;)[C

    #@3a6
    move-result-object v5

    #@3a7
    move-object/from16 v0, v31

    #@3a9
    invoke-direct {v0, v5}, Ljava/lang/String;-><init>([C)V

    #@3ac
    .line 327
    .local v31, old:Ljava/lang/String;
    const/4 v5, 0x0

    #@3ad
    aget-object v5, v36, v5

    #@3af
    move-object/from16 v0, p2

    #@3b1
    invoke-interface {v0, v5}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@3b4
    .line 333
    move/from16 v0, v39

    #@3b6
    move/from16 v1, v26

    #@3b8
    if-lt v0, v1, :cond_3f9

    #@3ba
    .line 334
    sget-object v5, Landroid/text/method/TextKeyListener;->INHIBIT_REPLACEMENT:Ljava/lang/Object;

    #@3bc
    const/16 v6, 0x22

    #@3be
    move-object/from16 v0, p2

    #@3c0
    move/from16 v1, v26

    #@3c2
    move/from16 v2, v26

    #@3c4
    invoke-interface {v0, v5, v1, v2, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@3c7
    .line 336
    move-object/from16 v0, p2

    #@3c9
    move/from16 v1, v40

    #@3cb
    move/from16 v2, v26

    #@3cd
    move-object/from16 v3, v31

    #@3cf
    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@3d2
    .line 338
    sget-object v5, Landroid/text/method/TextKeyListener;->INHIBIT_REPLACEMENT:Ljava/lang/Object;

    #@3d4
    move-object/from16 v0, p2

    #@3d6
    invoke-interface {v0, v5}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@3d9
    move-result v26

    #@3da
    .line 339
    add-int/lit8 v5, v26, -0x1

    #@3dc
    if-ltz v5, :cond_3f1

    #@3de
    .line 340
    sget-object v5, Landroid/text/method/TextKeyListener;->INHIBIT_REPLACEMENT:Ljava/lang/Object;

    #@3e0
    add-int/lit8 v6, v26, -0x1

    #@3e2
    const/16 v7, 0x21

    #@3e4
    move-object/from16 v0, p2

    #@3e6
    move/from16 v1, v26

    #@3e8
    invoke-interface {v0, v5, v6, v1, v7}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@3eb
    .line 346
    :goto_3eb
    invoke-static/range {p2 .. p2}, Landroid/text/method/QwertyKeyListener;->adjustMetaAfterKeypress(Landroid/text/Spannable;)V

    #@3ee
    .line 352
    const/4 v5, 0x1

    #@3ef
    goto/16 :goto_8f

    #@3f1
    .line 344
    :cond_3f1
    sget-object v5, Landroid/text/method/TextKeyListener;->INHIBIT_REPLACEMENT:Ljava/lang/Object;

    #@3f3
    move-object/from16 v0, p2

    #@3f5
    invoke-interface {v0, v5}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@3f8
    goto :goto_3eb

    #@3f9
    .line 348
    :cond_3f9
    invoke-static/range {p2 .. p2}, Landroid/text/method/QwertyKeyListener;->adjustMetaAfterKeypress(Landroid/text/Spannable;)V

    #@3fc
    .line 349
    invoke-super/range {p0 .. p4}, Landroid/text/method/BaseKeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@3ff
    move-result v5

    #@400
    goto/16 :goto_8f

    #@402
    .line 356
    .end local v24           #consider:I
    .end local v26           #en:I
    .end local v31           #old:Ljava/lang/String;
    .end local v36           #repl:[Landroid/text/method/QwertyKeyListener$Replaced;
    .end local v40           #st:I
    :cond_402
    invoke-super/range {p0 .. p4}, Landroid/text/method/BaseKeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@405
    move-result v5

    #@406
    goto/16 :goto_8f

    #@408
    .line 154
    .restart local v22       #ch:I
    .restart local v41       #start:I
    :catch_408
    move-exception v5

    #@409
    goto/16 :goto_ec
.end method
