.class public Landroid/text/method/HideReturnsTransformationMethod;
.super Landroid/text/method/ReplacementTransformationMethod;
.source "HideReturnsTransformationMethod.java"


# static fields
.field private static ORIGINAL:[C

.field private static REPLACEMENT:[C

.field private static sInstance:Landroid/text/method/HideReturnsTransformationMethod;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 33
    new-array v0, v3, [C

    #@4
    const/16 v1, 0xd

    #@6
    aput-char v1, v0, v2

    #@8
    sput-object v0, Landroid/text/method/HideReturnsTransformationMethod;->ORIGINAL:[C

    #@a
    .line 34
    new-array v0, v3, [C

    #@c
    const v1, 0xfeff

    #@f
    aput-char v1, v0, v2

    #@11
    sput-object v0, Landroid/text/method/HideReturnsTransformationMethod;->REPLACEMENT:[C

    #@13
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct {p0}, Landroid/text/method/ReplacementTransformationMethod;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Landroid/text/method/HideReturnsTransformationMethod;
    .registers 1

    #@0
    .prologue
    .line 51
    sget-object v0, Landroid/text/method/HideReturnsTransformationMethod;->sInstance:Landroid/text/method/HideReturnsTransformationMethod;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 52
    sget-object v0, Landroid/text/method/HideReturnsTransformationMethod;->sInstance:Landroid/text/method/HideReturnsTransformationMethod;

    #@6
    .line 55
    :goto_6
    return-object v0

    #@7
    .line 54
    :cond_7
    new-instance v0, Landroid/text/method/HideReturnsTransformationMethod;

    #@9
    invoke-direct {v0}, Landroid/text/method/HideReturnsTransformationMethod;-><init>()V

    #@c
    sput-object v0, Landroid/text/method/HideReturnsTransformationMethod;->sInstance:Landroid/text/method/HideReturnsTransformationMethod;

    #@e
    .line 55
    sget-object v0, Landroid/text/method/HideReturnsTransformationMethod;->sInstance:Landroid/text/method/HideReturnsTransformationMethod;

    #@10
    goto :goto_6
.end method


# virtual methods
.method protected getOriginal()[C
    .registers 2

    #@0
    .prologue
    .line 40
    sget-object v0, Landroid/text/method/HideReturnsTransformationMethod;->ORIGINAL:[C

    #@2
    return-object v0
.end method

.method protected getReplacement()[C
    .registers 2

    #@0
    .prologue
    .line 47
    sget-object v0, Landroid/text/method/HideReturnsTransformationMethod;->REPLACEMENT:[C

    #@2
    return-object v0
.end method
