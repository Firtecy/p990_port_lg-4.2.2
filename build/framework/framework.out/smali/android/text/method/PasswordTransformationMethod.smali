.class public Landroid/text/method/PasswordTransformationMethod;
.super Ljava/lang/Object;
.source "PasswordTransformationMethod.java"

# interfaces
.implements Landroid/text/method/TransformationMethod;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/method/PasswordTransformationMethod$ViewReference;,
        Landroid/text/method/PasswordTransformationMethod$Visible;,
        Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;
    }
.end annotation


# static fields
.field private static DOT:C

.field private static sInstance:Landroid/text/method/PasswordTransformationMethod;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 266
    const/16 v0, 0x2022

    #@2
    sput-char v0, Landroid/text/method/PasswordTransformationMethod;->DOT:C

    #@4
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 258
    return-void
.end method

.method static synthetic access$100()C
    .registers 1

    #@0
    .prologue
    .line 35
    sget-char v0, Landroid/text/method/PasswordTransformationMethod;->DOT:C

    #@2
    return v0
.end method

.method public static getInstance()Landroid/text/method/PasswordTransformationMethod;
    .registers 1

    #@0
    .prologue
    .line 64
    sget-object v0, Landroid/text/method/PasswordTransformationMethod;->sInstance:Landroid/text/method/PasswordTransformationMethod;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 65
    sget-object v0, Landroid/text/method/PasswordTransformationMethod;->sInstance:Landroid/text/method/PasswordTransformationMethod;

    #@6
    .line 68
    :goto_6
    return-object v0

    #@7
    .line 67
    :cond_7
    new-instance v0, Landroid/text/method/PasswordTransformationMethod;

    #@9
    invoke-direct {v0}, Landroid/text/method/PasswordTransformationMethod;-><init>()V

    #@c
    sput-object v0, Landroid/text/method/PasswordTransformationMethod;->sInstance:Landroid/text/method/PasswordTransformationMethod;

    #@e
    .line 68
    sget-object v0, Landroid/text/method/PasswordTransformationMethod;->sInstance:Landroid/text/method/PasswordTransformationMethod;

    #@10
    goto :goto_6
.end method

.method private static removeVisibleSpans(Landroid/text/Spannable;)V
    .registers 6
    .parameter "sp"

    #@0
    .prologue
    .line 133
    const/4 v2, 0x0

    #@1
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@4
    move-result v3

    #@5
    const-class v4, Landroid/text/method/PasswordTransformationMethod$Visible;

    #@7
    invoke-interface {p0, v2, v3, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@a
    move-result-object v1

    #@b
    check-cast v1, [Landroid/text/method/PasswordTransformationMethod$Visible;

    #@d
    .line 134
    .local v1, old:[Landroid/text/method/PasswordTransformationMethod$Visible;
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    array-length v2, v1

    #@f
    if-ge v0, v2, :cond_19

    #@11
    .line 135
    aget-object v2, v1, v0

    #@13
    invoke-interface {p0, v2}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@16
    .line 134
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_e

    #@19
    .line 137
    :cond_19
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 118
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    #@0
    .prologue
    .line 74
    return-void
.end method

.method public getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;
    .registers 9
    .parameter "source"
    .parameter "view"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 39
    instance-of v3, p1, Landroid/text/Spannable;

    #@3
    if-eqz v3, :cond_2d

    #@5
    move-object v1, p1

    #@6
    .line 40
    check-cast v1, Landroid/text/Spannable;

    #@8
    .line 48
    .local v1, sp:Landroid/text/Spannable;
    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    #@b
    move-result v3

    #@c
    const-class v4, Landroid/text/method/PasswordTransformationMethod$ViewReference;

    #@e
    invoke-interface {v1, v5, v3, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, [Landroid/text/method/PasswordTransformationMethod$ViewReference;

    #@14
    .line 50
    .local v2, vr:[Landroid/text/method/PasswordTransformationMethod$ViewReference;
    const/4 v0, 0x0

    #@15
    .local v0, i:I
    :goto_15
    array-length v3, v2

    #@16
    if-ge v0, v3, :cond_20

    #@18
    .line 51
    aget-object v3, v2, v0

    #@1a
    invoke-interface {v1, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@1d
    .line 50
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_15

    #@20
    .line 54
    :cond_20
    invoke-static {v1}, Landroid/text/method/PasswordTransformationMethod;->removeVisibleSpans(Landroid/text/Spannable;)V

    #@23
    .line 56
    new-instance v3, Landroid/text/method/PasswordTransformationMethod$ViewReference;

    #@25
    invoke-direct {v3, p2}, Landroid/text/method/PasswordTransformationMethod$ViewReference;-><init>(Landroid/view/View;)V

    #@28
    const/16 v4, 0x22

    #@2a
    invoke-interface {v1, v3, v5, v5, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@2d
    .line 60
    .end local v0           #i:I
    .end local v1           #sp:Landroid/text/Spannable;
    .end local v2           #vr:[Landroid/text/method/PasswordTransformationMethod$ViewReference;
    :cond_2d
    new-instance v3, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;

    #@2f
    invoke-direct {v3, p1}, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;-><init>(Ljava/lang/CharSequence;)V

    #@32
    return-object v3
.end method

.method public onFocusChanged(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V
    .registers 8
    .parameter "view"
    .parameter "sourceText"
    .parameter "focused"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 123
    if-nez p3, :cond_c

    #@2
    .line 124
    instance-of v1, p2, Landroid/text/Spannable;

    #@4
    if-eqz v1, :cond_c

    #@6
    move-object v0, p2

    #@7
    .line 125
    check-cast v0, Landroid/text/Spannable;

    #@9
    .line 127
    .local v0, sp:Landroid/text/Spannable;
    invoke-static {v0}, Landroid/text/method/PasswordTransformationMethod;->removeVisibleSpans(Landroid/text/Spannable;)V

    #@c
    .line 130
    .end local v0           #sp:Landroid/text/Spannable;
    :cond_c
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 13
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    .line 78
    instance-of v5, p1, Landroid/text/Spannable;

    #@2
    if-eqz v5, :cond_17

    #@4
    move-object v2, p1

    #@5
    .line 79
    check-cast v2, Landroid/text/Spannable;

    #@7
    .line 80
    .local v2, sp:Landroid/text/Spannable;
    const/4 v5, 0x0

    #@8
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@b
    move-result v6

    #@c
    const-class v7, Landroid/text/method/PasswordTransformationMethod$ViewReference;

    #@e
    invoke-interface {v2, v5, v6, v7}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@11
    move-result-object v4

    #@12
    check-cast v4, [Landroid/text/method/PasswordTransformationMethod$ViewReference;

    #@14
    .line 82
    .local v4, vr:[Landroid/text/method/PasswordTransformationMethod$ViewReference;
    array-length v5, v4

    #@15
    if-nez v5, :cond_18

    #@17
    .line 114
    .end local v2           #sp:Landroid/text/Spannable;
    .end local v4           #vr:[Landroid/text/method/PasswordTransformationMethod$ViewReference;
    :cond_17
    :goto_17
    return-void

    #@18
    .line 93
    .restart local v2       #sp:Landroid/text/Spannable;
    .restart local v4       #vr:[Landroid/text/method/PasswordTransformationMethod$ViewReference;
    :cond_18
    const/4 v3, 0x0

    #@19
    .line 94
    .local v3, v:Landroid/view/View;
    const/4 v0, 0x0

    #@1a
    .local v0, i:I
    :goto_1a
    if-nez v3, :cond_2a

    #@1c
    array-length v5, v4

    #@1d
    if-ge v0, v5, :cond_2a

    #@1f
    .line 95
    aget-object v5, v4, v0

    #@21
    invoke-virtual {v5}, Landroid/text/method/PasswordTransformationMethod$ViewReference;->get()Ljava/lang/Object;

    #@24
    move-result-object v3

    #@25
    .end local v3           #v:Landroid/view/View;
    check-cast v3, Landroid/view/View;

    #@27
    .line 94
    .restart local v3       #v:Landroid/view/View;
    add-int/lit8 v0, v0, 0x1

    #@29
    goto :goto_1a

    #@2a
    .line 98
    :cond_2a
    if-eqz v3, :cond_17

    #@2c
    .line 102
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v5, v6}, Landroid/text/method/TextKeyListener;->getPrefs(Landroid/content/Context;)I

    #@37
    move-result v1

    #@38
    .line 103
    .local v1, pref:I
    and-int/lit8 v5, v1, 0x8

    #@3a
    if-eqz v5, :cond_17

    #@3c
    .line 104
    if-lez p4, :cond_17

    #@3e
    .line 105
    invoke-static {v2}, Landroid/text/method/PasswordTransformationMethod;->removeVisibleSpans(Landroid/text/Spannable;)V

    #@41
    .line 107
    const/4 v5, 0x1

    #@42
    if-ne p4, v5, :cond_17

    #@44
    .line 108
    new-instance v5, Landroid/text/method/PasswordTransformationMethod$Visible;

    #@46
    invoke-direct {v5, v2, p0}, Landroid/text/method/PasswordTransformationMethod$Visible;-><init>(Landroid/text/Spannable;Landroid/text/method/PasswordTransformationMethod;)V

    #@49
    add-int v6, p2, p4

    #@4b
    const/16 v7, 0x21

    #@4d
    invoke-interface {v2, v5, p2, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@50
    goto :goto_17
.end method
