.class public Landroid/text/method/DigitsKeyListener;
.super Landroid/text/method/NumberKeyListener;
.source "DigitsKeyListener.java"


# static fields
.field private static final CHARACTERS:[[C = null

.field private static final DECIMAL:I = 0x2

.field private static final SIGN:I = 0x1

.field private static sInstance:[Landroid/text/method/DigitsKeyListener;


# instance fields
.field private mAccepted:[C

.field private mDecimal:Z

.field private mSign:Z


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/16 v4, 0xb

    #@2
    const/4 v3, 0x4

    #@3
    .line 52
    new-array v0, v3, [[C

    #@5
    const/4 v1, 0x0

    #@6
    const/16 v2, 0xa

    #@8
    new-array v2, v2, [C

    #@a
    fill-array-data v2, :array_30

    #@d
    aput-object v2, v0, v1

    #@f
    const/4 v1, 0x1

    #@10
    new-array v2, v4, [C

    #@12
    fill-array-data v2, :array_3e

    #@15
    aput-object v2, v0, v1

    #@17
    const/4 v1, 0x2

    #@18
    new-array v2, v4, [C

    #@1a
    fill-array-data v2, :array_4e

    #@1d
    aput-object v2, v0, v1

    #@1f
    const/4 v1, 0x3

    #@20
    const/16 v2, 0xc

    #@22
    new-array v2, v2, [C

    #@24
    fill-array-data v2, :array_5e

    #@27
    aput-object v2, v0, v1

    #@29
    sput-object v0, Landroid/text/method/DigitsKeyListener;->CHARACTERS:[[C

    #@2b
    .line 221
    new-array v0, v3, [Landroid/text/method/DigitsKeyListener;

    #@2d
    sput-object v0, Landroid/text/method/DigitsKeyListener;->sInstance:[Landroid/text/method/DigitsKeyListener;

    #@2f
    return-void

    #@30
    .line 52
    :array_30
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
    .end array-data

    #@3e
    :array_3e
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
        0x2dt 0x0t
    .end array-data

    #@4d
    nop

    #@4e
    :array_4e
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
        0x2et 0x0t
    .end array-data

    #@5d
    nop

    #@5e
    :array_5e
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
        0x2dt 0x0t
        0x2et 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 63
    invoke-direct {p0, v0, v0}, Landroid/text/method/DigitsKeyListener;-><init>(ZZ)V

    #@4
    .line 64
    return-void
.end method

.method public constructor <init>(ZZ)V
    .registers 6
    .parameter "sign"
    .parameter "decimal"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 71
    invoke-direct {p0}, Landroid/text/method/NumberKeyListener;-><init>()V

    #@4
    .line 72
    iput-boolean p1, p0, Landroid/text/method/DigitsKeyListener;->mSign:Z

    #@6
    .line 73
    iput-boolean p2, p0, Landroid/text/method/DigitsKeyListener;->mDecimal:Z

    #@8
    .line 75
    if-eqz p1, :cond_17

    #@a
    const/4 v2, 0x1

    #@b
    :goto_b
    if-eqz p2, :cond_e

    #@d
    const/4 v1, 0x2

    #@e
    :cond_e
    or-int v0, v2, v1

    #@10
    .line 76
    .local v0, kind:I
    sget-object v1, Landroid/text/method/DigitsKeyListener;->CHARACTERS:[[C

    #@12
    aget-object v1, v1, v0

    #@14
    iput-object v1, p0, Landroid/text/method/DigitsKeyListener;->mAccepted:[C

    #@16
    .line 77
    return-void

    #@17
    .end local v0           #kind:I
    :cond_17
    move v2, v1

    #@18
    .line 75
    goto :goto_b
.end method

.method public static getInstance()Landroid/text/method/DigitsKeyListener;
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 83
    invoke-static {v0, v0}, Landroid/text/method/DigitsKeyListener;->getInstance(ZZ)Landroid/text/method/DigitsKeyListener;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;
    .registers 5
    .parameter "accepted"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 109
    new-instance v0, Landroid/text/method/DigitsKeyListener;

    #@3
    invoke-direct {v0}, Landroid/text/method/DigitsKeyListener;-><init>()V

    #@6
    .line 111
    .local v0, dim:Landroid/text/method/DigitsKeyListener;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@9
    move-result v1

    #@a
    new-array v1, v1, [C

    #@c
    iput-object v1, v0, Landroid/text/method/DigitsKeyListener;->mAccepted:[C

    #@e
    .line 112
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@11
    move-result v1

    #@12
    iget-object v2, v0, Landroid/text/method/DigitsKeyListener;->mAccepted:[C

    #@14
    invoke-virtual {p0, v3, v1, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    #@17
    .line 114
    return-object v0
.end method

.method public static getInstance(ZZ)Landroid/text/method/DigitsKeyListener;
    .registers 5
    .parameter "sign"
    .parameter "decimal"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 92
    if-eqz p0, :cond_14

    #@3
    const/4 v2, 0x1

    #@4
    :goto_4
    if-eqz p1, :cond_7

    #@6
    const/4 v1, 0x2

    #@7
    :cond_7
    or-int v0, v2, v1

    #@9
    .line 94
    .local v0, kind:I
    sget-object v1, Landroid/text/method/DigitsKeyListener;->sInstance:[Landroid/text/method/DigitsKeyListener;

    #@b
    aget-object v1, v1, v0

    #@d
    if-eqz v1, :cond_16

    #@f
    .line 95
    sget-object v1, Landroid/text/method/DigitsKeyListener;->sInstance:[Landroid/text/method/DigitsKeyListener;

    #@11
    aget-object v1, v1, v0

    #@13
    .line 98
    :goto_13
    return-object v1

    #@14
    .end local v0           #kind:I
    :cond_14
    move v2, v1

    #@15
    .line 92
    goto :goto_4

    #@16
    .line 97
    .restart local v0       #kind:I
    :cond_16
    sget-object v1, Landroid/text/method/DigitsKeyListener;->sInstance:[Landroid/text/method/DigitsKeyListener;

    #@18
    new-instance v2, Landroid/text/method/DigitsKeyListener;

    #@1a
    invoke-direct {v2, p0, p1}, Landroid/text/method/DigitsKeyListener;-><init>(ZZ)V

    #@1d
    aput-object v2, v1, v0

    #@1f
    .line 98
    sget-object v1, Landroid/text/method/DigitsKeyListener;->sInstance:[Landroid/text/method/DigitsKeyListener;

    #@21
    aget-object v1, v1, v0

    #@23
    goto :goto_13
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .registers 17
    .parameter "source"
    .parameter "start"
    .parameter "end"
    .parameter "dest"
    .parameter "dstart"
    .parameter "dend"

    #@0
    .prologue
    .line 131
    invoke-super/range {p0 .. p6}, Landroid/text/method/NumberKeyListener;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    #@3
    move-result-object v4

    #@4
    .line 133
    .local v4, out:Ljava/lang/CharSequence;
    iget-boolean v8, p0, Landroid/text/method/DigitsKeyListener;->mSign:Z

    #@6
    if-nez v8, :cond_d

    #@8
    iget-boolean v8, p0, Landroid/text/method/DigitsKeyListener;->mDecimal:Z

    #@a
    if-nez v8, :cond_d

    #@c
    .line 217
    .end local v4           #out:Ljava/lang/CharSequence;
    :cond_c
    :goto_c
    return-object v4

    #@d
    .line 137
    .restart local v4       #out:Ljava/lang/CharSequence;
    :cond_d
    if-eqz v4, :cond_15

    #@f
    .line 138
    move-object p1, v4

    #@10
    .line 139
    const/4 p2, 0x0

    #@11
    .line 140
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@14
    move-result p3

    #@15
    .line 143
    :cond_15
    const/4 v5, -0x1

    #@16
    .line 144
    .local v5, sign:I
    const/4 v1, -0x1

    #@17
    .line 145
    .local v1, decimal:I
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    #@1a
    move-result v2

    #@1b
    .line 151
    .local v2, dlen:I
    const/4 v3, 0x0

    #@1c
    .local v3, i:I
    :goto_1c
    if-ge v3, p5, :cond_30

    #@1e
    .line 152
    invoke-interface {p4, v3}, Landroid/text/Spanned;->charAt(I)C

    #@21
    move-result v0

    #@22
    .line 154
    .local v0, c:C
    const/16 v8, 0x2d

    #@24
    if-ne v0, v8, :cond_2a

    #@26
    .line 155
    move v5, v3

    #@27
    .line 151
    :cond_27
    :goto_27
    add-int/lit8 v3, v3, 0x1

    #@29
    goto :goto_1c

    #@2a
    .line 156
    :cond_2a
    const/16 v8, 0x2e

    #@2c
    if-ne v0, v8, :cond_27

    #@2e
    .line 157
    move v1, v3

    #@2f
    goto :goto_27

    #@30
    .line 160
    .end local v0           #c:C
    :cond_30
    move/from16 v3, p6

    #@32
    :goto_32
    if-ge v3, v2, :cond_47

    #@34
    .line 161
    invoke-interface {p4, v3}, Landroid/text/Spanned;->charAt(I)C

    #@37
    move-result v0

    #@38
    .line 163
    .restart local v0       #c:C
    const/16 v8, 0x2d

    #@3a
    if-ne v0, v8, :cond_3f

    #@3c
    .line 164
    const-string v4, ""

    #@3e
    goto :goto_c

    #@3f
    .line 165
    :cond_3f
    const/16 v8, 0x2e

    #@41
    if-ne v0, v8, :cond_44

    #@43
    .line 166
    move v1, v3

    #@44
    .line 160
    :cond_44
    add-int/lit8 v3, v3, 0x1

    #@46
    goto :goto_32

    #@47
    .line 177
    .end local v0           #c:C
    :cond_47
    const/4 v7, 0x0

    #@48
    .line 179
    .local v7, stripped:Landroid/text/SpannableStringBuilder;
    add-int/lit8 v3, p3, -0x1

    #@4a
    :goto_4a
    if-lt v3, p2, :cond_85

    #@4c
    .line 180
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    #@4f
    move-result v0

    #@50
    .line 181
    .restart local v0       #c:C
    const/4 v6, 0x0

    #@51
    .line 183
    .local v6, strip:Z
    const/16 v8, 0x2d

    #@53
    if-ne v0, v8, :cond_69

    #@55
    .line 184
    if-ne v3, p2, :cond_59

    #@57
    if-eqz p5, :cond_63

    #@59
    .line 185
    :cond_59
    const/4 v6, 0x1

    #@5a
    .line 199
    :cond_5a
    :goto_5a
    if-eqz v6, :cond_82

    #@5c
    .line 200
    add-int/lit8 v8, p2, 0x1

    #@5e
    if-ne p3, v8, :cond_73

    #@60
    .line 201
    const-string v4, ""

    #@62
    goto :goto_c

    #@63
    .line 186
    :cond_63
    if-ltz v5, :cond_67

    #@65
    .line 187
    const/4 v6, 0x1

    #@66
    goto :goto_5a

    #@67
    .line 189
    :cond_67
    move v5, v3

    #@68
    goto :goto_5a

    #@69
    .line 191
    :cond_69
    const/16 v8, 0x2e

    #@6b
    if-ne v0, v8, :cond_5a

    #@6d
    .line 192
    if-ltz v1, :cond_71

    #@6f
    .line 193
    const/4 v6, 0x1

    #@70
    goto :goto_5a

    #@71
    .line 195
    :cond_71
    move v1, v3

    #@72
    goto :goto_5a

    #@73
    .line 204
    :cond_73
    if-nez v7, :cond_7a

    #@75
    .line 205
    new-instance v7, Landroid/text/SpannableStringBuilder;

    #@77
    .end local v7           #stripped:Landroid/text/SpannableStringBuilder;
    invoke-direct {v7, p1, p2, p3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;II)V

    #@7a
    .line 208
    .restart local v7       #stripped:Landroid/text/SpannableStringBuilder;
    :cond_7a
    sub-int v8, v3, p2

    #@7c
    add-int/lit8 v9, v3, 0x1

    #@7e
    sub-int/2addr v9, p2

    #@7f
    invoke-virtual {v7, v8, v9}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    #@82
    .line 179
    :cond_82
    add-int/lit8 v3, v3, -0x1

    #@84
    goto :goto_4a

    #@85
    .line 212
    .end local v0           #c:C
    .end local v6           #strip:Z
    :cond_85
    if-eqz v7, :cond_89

    #@87
    move-object v4, v7

    #@88
    .line 213
    goto :goto_c

    #@89
    .line 214
    :cond_89
    if-nez v4, :cond_c

    #@8b
    .line 217
    const/4 v4, 0x0

    #@8c
    goto :goto_c
.end method

.method protected getAcceptedChars()[C
    .registers 2

    #@0
    .prologue
    .line 43
    iget-object v0, p0, Landroid/text/method/DigitsKeyListener;->mAccepted:[C

    #@2
    return-object v0
.end method

.method public getInputType()I
    .registers 3

    #@0
    .prologue
    .line 118
    const/4 v0, 0x2

    #@1
    .line 119
    .local v0, contentType:I
    iget-boolean v1, p0, Landroid/text/method/DigitsKeyListener;->mSign:Z

    #@3
    if-eqz v1, :cond_7

    #@5
    .line 120
    or-int/lit16 v0, v0, 0x1000

    #@7
    .line 122
    :cond_7
    iget-boolean v1, p0, Landroid/text/method/DigitsKeyListener;->mDecimal:Z

    #@9
    if-eqz v1, :cond_d

    #@b
    .line 123
    or-int/lit16 v0, v0, 0x2000

    #@d
    .line 125
    :cond_d
    return v0
.end method
