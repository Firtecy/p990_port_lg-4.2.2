.class Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;
.super Ljava/lang/Object;
.source "PasswordTransformationMethod.java"

# interfaces
.implements Ljava/lang/CharSequence;
.implements Landroid/text/GetChars;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/method/PasswordTransformationMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PasswordCharSequence"
.end annotation


# instance fields
.field private mSource:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 142
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 143
    iput-object p1, p0, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->mSource:Ljava/lang/CharSequence;

    #@5
    .line 144
    return-void
.end method


# virtual methods
.method public charAt(I)C
    .registers 10
    .parameter "i"

    #@0
    .prologue
    .line 151
    iget-object v5, p0, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->mSource:Ljava/lang/CharSequence;

    #@2
    instance-of v5, v5, Landroid/text/Spanned;

    #@4
    if-eqz v5, :cond_58

    #@6
    .line 152
    iget-object v2, p0, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->mSource:Ljava/lang/CharSequence;

    #@8
    check-cast v2, Landroid/text/Spanned;

    #@a
    .line 154
    .local v2, sp:Landroid/text/Spanned;
    sget-object v5, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@c
    invoke-interface {v2, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@f
    move-result v3

    #@10
    .line 155
    .local v3, st:I
    sget-object v5, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@12
    invoke-interface {v2, v5}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@15
    move-result v1

    #@16
    .line 157
    .local v1, en:I
    if-lt p1, v3, :cond_21

    #@18
    if-ge p1, v1, :cond_21

    #@1a
    .line 158
    iget-object v5, p0, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->mSource:Ljava/lang/CharSequence;

    #@1c
    invoke-interface {v5, p1}, Ljava/lang/CharSequence;->charAt(I)C

    #@1f
    move-result v5

    #@20
    .line 175
    .end local v1           #en:I
    .end local v2           #sp:Landroid/text/Spanned;
    .end local v3           #st:I
    :goto_20
    return v5

    #@21
    .line 161
    .restart local v1       #en:I
    .restart local v2       #sp:Landroid/text/Spanned;
    .restart local v3       #st:I
    :cond_21
    const/4 v5, 0x0

    #@22
    invoke-interface {v2}, Landroid/text/Spanned;->length()I

    #@25
    move-result v6

    #@26
    const-class v7, Landroid/text/method/PasswordTransformationMethod$Visible;

    #@28
    invoke-interface {v2, v5, v6, v7}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@2b
    move-result-object v4

    #@2c
    check-cast v4, [Landroid/text/method/PasswordTransformationMethod$Visible;

    #@2e
    .line 163
    .local v4, visible:[Landroid/text/method/PasswordTransformationMethod$Visible;
    const/4 v0, 0x0

    #@2f
    .local v0, a:I
    :goto_2f
    array-length v5, v4

    #@30
    if-ge v0, v5, :cond_58

    #@32
    .line 164
    aget-object v5, v4, v0

    #@34
    invoke-static {v5}, Landroid/text/method/PasswordTransformationMethod$Visible;->access$000(Landroid/text/method/PasswordTransformationMethod$Visible;)Landroid/text/method/PasswordTransformationMethod;

    #@37
    move-result-object v5

    #@38
    invoke-interface {v2, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@3b
    move-result v5

    #@3c
    if-ltz v5, :cond_55

    #@3e
    .line 165
    aget-object v5, v4, v0

    #@40
    invoke-interface {v2, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@43
    move-result v3

    #@44
    .line 166
    aget-object v5, v4, v0

    #@46
    invoke-interface {v2, v5}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@49
    move-result v1

    #@4a
    .line 168
    if-lt p1, v3, :cond_55

    #@4c
    if-ge p1, v1, :cond_55

    #@4e
    .line 169
    iget-object v5, p0, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->mSource:Ljava/lang/CharSequence;

    #@50
    invoke-interface {v5, p1}, Ljava/lang/CharSequence;->charAt(I)C

    #@53
    move-result v5

    #@54
    goto :goto_20

    #@55
    .line 163
    :cond_55
    add-int/lit8 v0, v0, 0x1

    #@57
    goto :goto_2f

    #@58
    .line 175
    .end local v0           #a:I
    .end local v1           #en:I
    .end local v2           #sp:Landroid/text/Spanned;
    .end local v3           #st:I
    .end local v4           #visible:[Landroid/text/method/PasswordTransformationMethod$Visible;
    :cond_58
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->access$100()C

    #@5b
    move-result v5

    #@5c
    goto :goto_20
.end method

.method public getChars(II[CI)V
    .registers 21
    .parameter "start"
    .parameter "end"
    .parameter "dest"
    .parameter "off"

    #@0
    .prologue
    .line 190
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->mSource:Ljava/lang/CharSequence;

    #@4
    move/from16 v0, p1

    #@6
    move/from16 v1, p2

    #@8
    move-object/from16 v2, p3

    #@a
    move/from16 v3, p4

    #@c
    invoke-static {v13, v0, v1, v2, v3}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@f
    .line 192
    const/4 v10, -0x1

    #@10
    .local v10, st:I
    const/4 v5, -0x1

    #@11
    .line 193
    .local v5, en:I
    const/4 v8, 0x0

    #@12
    .line 194
    .local v8, nvisible:I
    const/4 v11, 0x0

    #@13
    .local v11, starts:[I
    const/4 v6, 0x0

    #@14
    .line 196
    .local v6, ends:[I
    move-object/from16 v0, p0

    #@16
    iget-object v13, v0, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->mSource:Ljava/lang/CharSequence;

    #@18
    instance-of v13, v13, Landroid/text/Spanned;

    #@1a
    if-eqz v13, :cond_62

    #@1c
    .line 197
    move-object/from16 v0, p0

    #@1e
    iget-object v9, v0, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->mSource:Ljava/lang/CharSequence;

    #@20
    check-cast v9, Landroid/text/Spanned;

    #@22
    .line 199
    .local v9, sp:Landroid/text/Spanned;
    sget-object v13, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@24
    invoke-interface {v9, v13}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@27
    move-result v10

    #@28
    .line 200
    sget-object v13, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@2a
    invoke-interface {v9, v13}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@2d
    move-result v5

    #@2e
    .line 202
    const/4 v13, 0x0

    #@2f
    invoke-interface {v9}, Landroid/text/Spanned;->length()I

    #@32
    move-result v14

    #@33
    const-class v15, Landroid/text/method/PasswordTransformationMethod$Visible;

    #@35
    invoke-interface {v9, v13, v14, v15}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@38
    move-result-object v12

    #@39
    check-cast v12, [Landroid/text/method/PasswordTransformationMethod$Visible;

    #@3b
    .line 203
    .local v12, visible:[Landroid/text/method/PasswordTransformationMethod$Visible;
    array-length v8, v12

    #@3c
    .line 204
    new-array v11, v8, [I

    #@3e
    .line 205
    new-array v6, v8, [I

    #@40
    .line 207
    const/4 v7, 0x0

    #@41
    .local v7, i:I
    :goto_41
    if-ge v7, v8, :cond_62

    #@43
    .line 208
    aget-object v13, v12, v7

    #@45
    invoke-static {v13}, Landroid/text/method/PasswordTransformationMethod$Visible;->access$000(Landroid/text/method/PasswordTransformationMethod$Visible;)Landroid/text/method/PasswordTransformationMethod;

    #@48
    move-result-object v13

    #@49
    invoke-interface {v9, v13}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@4c
    move-result v13

    #@4d
    if-ltz v13, :cond_5f

    #@4f
    .line 209
    aget-object v13, v12, v7

    #@51
    invoke-interface {v9, v13}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@54
    move-result v13

    #@55
    aput v13, v11, v7

    #@57
    .line 210
    aget-object v13, v12, v7

    #@59
    invoke-interface {v9, v13}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@5c
    move-result v13

    #@5d
    aput v13, v6, v7

    #@5f
    .line 207
    :cond_5f
    add-int/lit8 v7, v7, 0x1

    #@61
    goto :goto_41

    #@62
    .line 215
    .end local v7           #i:I
    .end local v9           #sp:Landroid/text/Spanned;
    .end local v12           #visible:[Landroid/text/method/PasswordTransformationMethod$Visible;
    :cond_62
    move/from16 v7, p1

    #@64
    .restart local v7       #i:I
    :goto_64
    move/from16 v0, p2

    #@66
    if-ge v7, v0, :cond_8b

    #@68
    .line 216
    if-lt v7, v10, :cond_6c

    #@6a
    if-lt v7, v5, :cond_85

    #@6c
    .line 217
    :cond_6c
    const/4 v12, 0x0

    #@6d
    .line 219
    .local v12, visible:Z
    const/4 v4, 0x0

    #@6e
    .local v4, a:I
    :goto_6e
    if-ge v4, v8, :cond_79

    #@70
    .line 220
    aget v13, v11, v4

    #@72
    if-lt v7, v13, :cond_88

    #@74
    aget v13, v6, v4

    #@76
    if-ge v7, v13, :cond_88

    #@78
    .line 221
    const/4 v12, 0x1

    #@79
    .line 226
    :cond_79
    if-nez v12, :cond_85

    #@7b
    .line 227
    sub-int v13, v7, p1

    #@7d
    add-int v13, v13, p4

    #@7f
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->access$100()C

    #@82
    move-result v14

    #@83
    aput-char v14, p3, v13

    #@85
    .line 215
    .end local v4           #a:I
    .end local v12           #visible:Z
    :cond_85
    add-int/lit8 v7, v7, 0x1

    #@87
    goto :goto_64

    #@88
    .line 219
    .restart local v4       #a:I
    .restart local v12       #visible:Z
    :cond_88
    add-int/lit8 v4, v4, 0x1

    #@8a
    goto :goto_6e

    #@8b
    .line 231
    .end local v4           #a:I
    .end local v12           #visible:Z
    :cond_8b
    return-void
.end method

.method public length()I
    .registers 2

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->mSource:Ljava/lang/CharSequence;

    #@2
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .registers 5
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 179
    sub-int v1, p2, p1

    #@2
    new-array v0, v1, [C

    #@4
    .line 181
    .local v0, buf:[C
    const/4 v1, 0x0

    #@5
    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->getChars(II[CI)V

    #@8
    .line 182
    new-instance v1, Ljava/lang/String;

    #@a
    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    #@d
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 186
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0}, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->length()I

    #@4
    move-result v1

    #@5
    invoke-virtual {p0, v0, v1}, Landroid/text/method/PasswordTransformationMethod$PasswordCharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method
