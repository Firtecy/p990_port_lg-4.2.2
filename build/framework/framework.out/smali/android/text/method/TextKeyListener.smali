.class public Landroid/text/method/TextKeyListener;
.super Landroid/text/method/BaseKeyListener;
.source "TextKeyListener.java"

# interfaces
.implements Landroid/text/SpanWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/method/TextKeyListener$SettingsObserver;,
        Landroid/text/method/TextKeyListener$NullKeyListener;,
        Landroid/text/method/TextKeyListener$Capitalize;
    }
.end annotation


# static fields
.field static final ACTIVE:Ljava/lang/Object; = null

.field static final AUTO_CAP:I = 0x1

.field static final AUTO_PERIOD:I = 0x4

.field static final AUTO_TEXT:I = 0x2

.field static final CAPPED:Ljava/lang/Object; = null

.field static final INHIBIT_REPLACEMENT:Ljava/lang/Object; = null

.field static final LAST_TYPED:Ljava/lang/Object; = null

.field static final SHOW_PASSWORD:I = 0x8

.field private static sInstance:[Landroid/text/method/TextKeyListener;


# instance fields
.field private mAutoCap:Landroid/text/method/TextKeyListener$Capitalize;

.field private mAutoText:Z

.field private mObserver:Landroid/text/method/TextKeyListener$SettingsObserver;

.field private mPrefs:I

.field private mPrefsInited:Z

.field private mResolver:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/ContentResolver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-static {}, Landroid/text/method/TextKeyListener$Capitalize;->values()[Landroid/text/method/TextKeyListener$Capitalize;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    mul-int/lit8 v0, v0, 0x2

    #@7
    new-array v0, v0, [Landroid/text/method/TextKeyListener;

    #@9
    sput-object v0, Landroid/text/method/TextKeyListener;->sInstance:[Landroid/text/method/TextKeyListener;

    #@b
    .line 45
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    #@d
    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    #@10
    sput-object v0, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@12
    .line 46
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    #@14
    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    #@17
    sput-object v0, Landroid/text/method/TextKeyListener;->CAPPED:Ljava/lang/Object;

    #@19
    .line 47
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    #@1b
    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    #@1e
    sput-object v0, Landroid/text/method/TextKeyListener;->INHIBIT_REPLACEMENT:Ljava/lang/Object;

    #@20
    .line 48
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    #@22
    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    #@25
    sput-object v0, Landroid/text/method/TextKeyListener;->LAST_TYPED:Ljava/lang/Object;

    #@27
    return-void
.end method

.method public constructor <init>(Landroid/text/method/TextKeyListener$Capitalize;Z)V
    .registers 3
    .parameter "cap"
    .parameter "autotext"

    #@0
    .prologue
    .line 70
    invoke-direct {p0}, Landroid/text/method/BaseKeyListener;-><init>()V

    #@3
    .line 71
    iput-object p1, p0, Landroid/text/method/TextKeyListener;->mAutoCap:Landroid/text/method/TextKeyListener$Capitalize;

    #@5
    .line 72
    iput-boolean p2, p0, Landroid/text/method/TextKeyListener;->mAutoText:Z

    #@7
    .line 73
    return-void
.end method

.method static synthetic access$000(Landroid/text/method/TextKeyListener;)Ljava/lang/ref/WeakReference;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/text/method/TextKeyListener;->mResolver:Ljava/lang/ref/WeakReference;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/text/method/TextKeyListener;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    iput-boolean p1, p0, Landroid/text/method/TextKeyListener;->mPrefsInited:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Landroid/text/method/TextKeyListener;Landroid/content/ContentResolver;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/text/method/TextKeyListener;->updatePrefs(Landroid/content/ContentResolver;)V

    #@3
    return-void
.end method

.method public static clear(Landroid/text/Editable;)V
    .registers 7
    .parameter "e"

    #@0
    .prologue
    .line 162
    invoke-interface {p0}, Landroid/text/Editable;->clear()V

    #@3
    .line 163
    sget-object v3, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@5
    invoke-interface {p0, v3}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@8
    .line 164
    sget-object v3, Landroid/text/method/TextKeyListener;->CAPPED:Ljava/lang/Object;

    #@a
    invoke-interface {p0, v3}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@d
    .line 165
    sget-object v3, Landroid/text/method/TextKeyListener;->INHIBIT_REPLACEMENT:Ljava/lang/Object;

    #@f
    invoke-interface {p0, v3}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@12
    .line 166
    sget-object v3, Landroid/text/method/TextKeyListener;->LAST_TYPED:Ljava/lang/Object;

    #@14
    invoke-interface {p0, v3}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@17
    .line 168
    const/4 v3, 0x0

    #@18
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@1b
    move-result v4

    #@1c
    const-class v5, Landroid/text/method/QwertyKeyListener$Replaced;

    #@1e
    invoke-interface {p0, v3, v4, v5}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@21
    move-result-object v2

    #@22
    check-cast v2, [Landroid/text/method/QwertyKeyListener$Replaced;

    #@24
    .line 170
    .local v2, repl:[Landroid/text/method/QwertyKeyListener$Replaced;
    array-length v0, v2

    #@25
    .line 171
    .local v0, count:I
    const/4 v1, 0x0

    #@26
    .local v1, i:I
    :goto_26
    if-ge v1, v0, :cond_30

    #@28
    .line 172
    aget-object v3, v2, v1

    #@2a
    invoke-interface {p0, v3}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@2d
    .line 171
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_26

    #@30
    .line 174
    :cond_30
    return-void
.end method

.method public static getInstance()Landroid/text/method/TextKeyListener;
    .registers 2

    #@0
    .prologue
    .line 98
    const/4 v0, 0x0

    #@1
    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    #@3
    invoke-static {v0, v1}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public static getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;
    .registers 5
    .parameter "autotext"
    .parameter "cap"

    #@0
    .prologue
    .line 84
    invoke-virtual {p1}, Landroid/text/method/TextKeyListener$Capitalize;->ordinal()I

    #@3
    move-result v1

    #@4
    mul-int/lit8 v2, v1, 0x2

    #@6
    if-eqz p0, :cond_1f

    #@8
    const/4 v1, 0x1

    #@9
    :goto_9
    add-int v0, v2, v1

    #@b
    .line 86
    .local v0, off:I
    sget-object v1, Landroid/text/method/TextKeyListener;->sInstance:[Landroid/text/method/TextKeyListener;

    #@d
    aget-object v1, v1, v0

    #@f
    if-nez v1, :cond_1a

    #@11
    .line 87
    sget-object v1, Landroid/text/method/TextKeyListener;->sInstance:[Landroid/text/method/TextKeyListener;

    #@13
    new-instance v2, Landroid/text/method/TextKeyListener;

    #@15
    invoke-direct {v2, p1, p0}, Landroid/text/method/TextKeyListener;-><init>(Landroid/text/method/TextKeyListener$Capitalize;Z)V

    #@18
    aput-object v2, v1, v0

    #@1a
    .line 90
    :cond_1a
    sget-object v1, Landroid/text/method/TextKeyListener;->sInstance:[Landroid/text/method/TextKeyListener;

    #@1c
    aget-object v1, v1, v0

    #@1e
    return-object v1

    #@1f
    .line 84
    .end local v0           #off:I
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_9
.end method

.method private getKeyListener(Landroid/view/KeyEvent;)Landroid/text/method/KeyListener;
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 187
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@3
    move-result-object v1

    #@4
    .line 188
    .local v1, kmap:Landroid/view/KeyCharacterMap;
    invoke-virtual {v1}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    #@7
    move-result v0

    #@8
    .line 190
    .local v0, kind:I
    const/4 v2, 0x3

    #@9
    if-ne v0, v2, :cond_14

    #@b
    .line 191
    iget-boolean v2, p0, Landroid/text/method/TextKeyListener;->mAutoText:Z

    #@d
    iget-object v3, p0, Landroid/text/method/TextKeyListener;->mAutoCap:Landroid/text/method/TextKeyListener$Capitalize;

    #@f
    invoke-static {v2, v3}, Landroid/text/method/QwertyKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/QwertyKeyListener;

    #@12
    move-result-object v2

    #@13
    .line 204
    :goto_13
    return-object v2

    #@14
    .line 192
    :cond_14
    const/4 v2, 0x1

    #@15
    if-ne v0, v2, :cond_20

    #@17
    .line 193
    iget-boolean v2, p0, Landroid/text/method/TextKeyListener;->mAutoText:Z

    #@19
    iget-object v3, p0, Landroid/text/method/TextKeyListener;->mAutoCap:Landroid/text/method/TextKeyListener$Capitalize;

    #@1b
    invoke-static {v2, v3}, Landroid/text/method/MultiTapKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/MultiTapKeyListener;

    #@1e
    move-result-object v2

    #@1f
    goto :goto_13

    #@20
    .line 194
    :cond_20
    const/4 v2, 0x4

    #@21
    if-eq v0, v2, :cond_26

    #@23
    const/4 v2, 0x5

    #@24
    if-ne v0, v2, :cond_2b

    #@26
    .line 201
    :cond_26
    invoke-static {}, Landroid/text/method/QwertyKeyListener;->getInstanceForFullKeyboard()Landroid/text/method/QwertyKeyListener;

    #@29
    move-result-object v2

    #@2a
    goto :goto_13

    #@2b
    .line 204
    :cond_2b
    invoke-static {}, Landroid/text/method/TextKeyListener$NullKeyListener;->getInstance()Landroid/text/method/TextKeyListener$NullKeyListener;

    #@2e
    move-result-object v2

    #@2f
    goto :goto_13
.end method

.method private initPrefs(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 259
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    .line 260
    .local v0, contentResolver:Landroid/content/ContentResolver;
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@7
    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@a
    iput-object v1, p0, Landroid/text/method/TextKeyListener;->mResolver:Ljava/lang/ref/WeakReference;

    #@c
    .line 261
    iget-object v1, p0, Landroid/text/method/TextKeyListener;->mObserver:Landroid/text/method/TextKeyListener$SettingsObserver;

    #@e
    if-nez v1, :cond_1e

    #@10
    .line 262
    new-instance v1, Landroid/text/method/TextKeyListener$SettingsObserver;

    #@12
    invoke-direct {v1, p0}, Landroid/text/method/TextKeyListener$SettingsObserver;-><init>(Landroid/text/method/TextKeyListener;)V

    #@15
    iput-object v1, p0, Landroid/text/method/TextKeyListener;->mObserver:Landroid/text/method/TextKeyListener$SettingsObserver;

    #@17
    .line 263
    sget-object v1, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    #@19
    iget-object v2, p0, Landroid/text/method/TextKeyListener;->mObserver:Landroid/text/method/TextKeyListener$SettingsObserver;

    #@1b
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1e
    .line 266
    :cond_1e
    invoke-direct {p0, v0}, Landroid/text/method/TextKeyListener;->updatePrefs(Landroid/content/ContentResolver;)V

    #@21
    .line 267
    iput-boolean v3, p0, Landroid/text/method/TextKeyListener;->mPrefsInited:Z

    #@23
    .line 268
    return-void
.end method

.method public static shouldCap(Landroid/text/method/TextKeyListener$Capitalize;Ljava/lang/CharSequence;I)Z
    .registers 6
    .parameter "cap"
    .parameter "cs"
    .parameter "off"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 115
    sget-object v0, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    #@4
    if-ne p0, v0, :cond_7

    #@6
    .line 122
    :goto_6
    return v2

    #@7
    .line 118
    :cond_7
    sget-object v0, Landroid/text/method/TextKeyListener$Capitalize;->CHARACTERS:Landroid/text/method/TextKeyListener$Capitalize;

    #@9
    if-ne p0, v0, :cond_d

    #@b
    move v2, v1

    #@c
    .line 119
    goto :goto_6

    #@d
    .line 122
    :cond_d
    sget-object v0, Landroid/text/method/TextKeyListener$Capitalize;->WORDS:Landroid/text/method/TextKeyListener$Capitalize;

    #@f
    if-ne p0, v0, :cond_1c

    #@11
    const/16 v0, 0x2000

    #@13
    :goto_13
    invoke-static {p1, p2, v0}, Landroid/text/TextUtils;->getCapsMode(Ljava/lang/CharSequence;II)I

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_1f

    #@19
    move v0, v1

    #@1a
    :goto_1a
    move v2, v0

    #@1b
    goto :goto_6

    #@1c
    :cond_1c
    const/16 v0, 0x4000

    #@1e
    goto :goto_13

    #@1f
    :cond_1f
    move v0, v2

    #@20
    goto :goto_1a
.end method

.method private updatePrefs(Landroid/content/ContentResolver;)V
    .registers 9
    .parameter "resolver"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 291
    const-string v6, "auto_caps"

    #@4
    invoke-static {p1, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@7
    move-result v6

    #@8
    if-lez v6, :cond_3a

    #@a
    move v0, v4

    #@b
    .line 292
    .local v0, cap:Z
    :goto_b
    const-string v6, "auto_replace"

    #@d
    invoke-static {p1, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v6

    #@11
    if-lez v6, :cond_3c

    #@13
    move v3, v4

    #@14
    .line 293
    .local v3, text:Z
    :goto_14
    const-string v6, "auto_punctuate"

    #@16
    invoke-static {p1, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@19
    move-result v6

    #@1a
    if-lez v6, :cond_3e

    #@1c
    move v1, v4

    #@1d
    .line 294
    .local v1, period:Z
    :goto_1d
    const-string/jumbo v6, "show_password"

    #@20
    invoke-static {p1, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@23
    move-result v6

    #@24
    if-lez v6, :cond_40

    #@26
    move v2, v4

    #@27
    .line 296
    .local v2, pw:Z
    :goto_27
    if-eqz v0, :cond_42

    #@29
    move v6, v4

    #@2a
    :goto_2a
    if-eqz v3, :cond_44

    #@2c
    const/4 v4, 0x2

    #@2d
    :goto_2d
    or-int/2addr v6, v4

    #@2e
    if-eqz v1, :cond_46

    #@30
    const/4 v4, 0x4

    #@31
    :goto_31
    or-int/2addr v4, v6

    #@32
    if-eqz v2, :cond_36

    #@34
    const/16 v5, 0x8

    #@36
    :cond_36
    or-int/2addr v4, v5

    #@37
    iput v4, p0, Landroid/text/method/TextKeyListener;->mPrefs:I

    #@39
    .line 300
    return-void

    #@3a
    .end local v0           #cap:Z
    .end local v1           #period:Z
    .end local v2           #pw:Z
    .end local v3           #text:Z
    :cond_3a
    move v0, v5

    #@3b
    .line 291
    goto :goto_b

    #@3c
    .restart local v0       #cap:Z
    :cond_3c
    move v3, v5

    #@3d
    .line 292
    goto :goto_14

    #@3e
    .restart local v3       #text:Z
    :cond_3e
    move v1, v5

    #@3f
    .line 293
    goto :goto_1d

    #@40
    .restart local v1       #period:Z
    :cond_40
    move v2, v5

    #@41
    .line 294
    goto :goto_27

    #@42
    .restart local v2       #pw:Z
    :cond_42
    move v6, v5

    #@43
    .line 296
    goto :goto_2a

    #@44
    :cond_44
    move v4, v5

    #@45
    goto :goto_2d

    #@46
    :cond_46
    move v4, v5

    #@47
    goto :goto_31
.end method


# virtual methods
.method public getInputType()I
    .registers 3

    #@0
    .prologue
    .line 128
    iget-object v0, p0, Landroid/text/method/TextKeyListener;->mAutoCap:Landroid/text/method/TextKeyListener$Capitalize;

    #@2
    iget-boolean v1, p0, Landroid/text/method/TextKeyListener;->mAutoText:Z

    #@4
    invoke-static {v0, v1}, Landroid/text/method/TextKeyListener;->makeTextContentType(Landroid/text/method/TextKeyListener$Capitalize;Z)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method getPrefs(Landroid/content/Context;)I
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 303
    monitor-enter p0

    #@1
    .line 304
    :try_start_1
    iget-boolean v0, p0, Landroid/text/method/TextKeyListener;->mPrefsInited:Z

    #@3
    if-eqz v0, :cond_d

    #@5
    iget-object v0, p0, Landroid/text/method/TextKeyListener;->mResolver:Ljava/lang/ref/WeakReference;

    #@7
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    if-nez v0, :cond_10

    #@d
    .line 305
    :cond_d
    invoke-direct {p0, p1}, Landroid/text/method/TextKeyListener;->initPrefs(Landroid/content/Context;)V

    #@10
    .line 307
    :cond_10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_14

    #@11
    .line 309
    iget v0, p0, Landroid/text/method/TextKeyListener;->mPrefs:I

    #@13
    return v0

    #@14
    .line 307
    :catchall_14
    move-exception v0

    #@15
    :try_start_15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "view"
    .parameter "content"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 134
    invoke-direct {p0, p4}, Landroid/text/method/TextKeyListener;->getKeyListener(Landroid/view/KeyEvent;)Landroid/text/method/KeyListener;

    #@3
    move-result-object v0

    #@4
    .line 136
    .local v0, im:Landroid/text/method/KeyListener;
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/method/KeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@7
    move-result v1

    #@8
    return v1
.end method

.method public onKeyOther(Landroid/view/View;Landroid/text/Editable;Landroid/view/KeyEvent;)Z
    .registers 6
    .parameter "view"
    .parameter "content"
    .parameter "event"

    #@0
    .prologue
    .line 149
    invoke-direct {p0, p3}, Landroid/text/method/TextKeyListener;->getKeyListener(Landroid/view/KeyEvent;)Landroid/text/method/KeyListener;

    #@3
    move-result-object v0

    #@4
    .line 151
    .local v0, im:Landroid/text/method/KeyListener;
    invoke-interface {v0, p1, p2, p3}, Landroid/text/method/KeyListener;->onKeyOther(Landroid/view/View;Landroid/text/Editable;Landroid/view/KeyEvent;)Z

    #@7
    move-result v1

    #@8
    return v1
.end method

.method public onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "view"
    .parameter "content"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 142
    invoke-direct {p0, p4}, Landroid/text/method/TextKeyListener;->getKeyListener(Landroid/view/KeyEvent;)Landroid/text/method/KeyListener;

    #@3
    move-result-object v0

    #@4
    .line 144
    .local v0, im:Landroid/text/method/KeyListener;
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@7
    move-result v1

    #@8
    return v1
.end method

.method public onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .registers 5
    .parameter "s"
    .parameter "what"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 176
    return-void
.end method

.method public onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V
    .registers 8
    .parameter "s"
    .parameter "what"
    .parameter "start"
    .parameter "end"
    .parameter "st"
    .parameter "en"

    #@0
    .prologue
    .line 181
    sget-object v0, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    #@2
    if-ne p2, v0, :cond_9

    #@4
    .line 182
    sget-object v0, Landroid/text/method/TextKeyListener;->ACTIVE:Ljava/lang/Object;

    #@6
    invoke-interface {p1, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@9
    .line 184
    :cond_9
    return-void
.end method

.method public onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .registers 5
    .parameter "s"
    .parameter "what"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 177
    return-void
.end method

.method public release()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 246
    iget-object v1, p0, Landroid/text/method/TextKeyListener;->mResolver:Ljava/lang/ref/WeakReference;

    #@3
    if-eqz v1, :cond_20

    #@5
    .line 247
    iget-object v1, p0, Landroid/text/method/TextKeyListener;->mResolver:Ljava/lang/ref/WeakReference;

    #@7
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/content/ContentResolver;

    #@d
    .line 248
    .local v0, contentResolver:Landroid/content/ContentResolver;
    if-eqz v0, :cond_19

    #@f
    .line 249
    iget-object v1, p0, Landroid/text/method/TextKeyListener;->mObserver:Landroid/text/method/TextKeyListener$SettingsObserver;

    #@11
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@14
    .line 250
    iget-object v1, p0, Landroid/text/method/TextKeyListener;->mResolver:Ljava/lang/ref/WeakReference;

    #@16
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->clear()V

    #@19
    .line 252
    :cond_19
    iput-object v2, p0, Landroid/text/method/TextKeyListener;->mObserver:Landroid/text/method/TextKeyListener$SettingsObserver;

    #@1b
    .line 253
    iput-object v2, p0, Landroid/text/method/TextKeyListener;->mResolver:Ljava/lang/ref/WeakReference;

    #@1d
    .line 254
    const/4 v1, 0x0

    #@1e
    iput-boolean v1, p0, Landroid/text/method/TextKeyListener;->mPrefsInited:Z

    #@20
    .line 256
    .end local v0           #contentResolver:Landroid/content/ContentResolver;
    :cond_20
    return-void
.end method
