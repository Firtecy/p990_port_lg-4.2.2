.class public abstract Landroid/text/method/NumberKeyListener;
.super Landroid/text/method/BaseKeyListener;
.source "NumberKeyListener.java"

# interfaces
.implements Landroid/text/InputFilter;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 35
    invoke-direct {p0}, Landroid/text/method/BaseKeyListener;-><init>()V

    #@3
    return-void
.end method

.method protected static ok([CC)Z
    .registers 4
    .parameter "accept"
    .parameter "c"

    #@0
    .prologue
    .line 86
    array-length v1, p0

    #@1
    add-int/lit8 v0, v1, -0x1

    #@3
    .local v0, i:I
    :goto_3
    if-ltz v0, :cond_e

    #@5
    .line 87
    aget-char v1, p0, v0

    #@7
    if-ne v1, p1, :cond_b

    #@9
    .line 88
    const/4 v1, 0x1

    #@a
    .line 92
    :goto_a
    return v1

    #@b
    .line 86
    :cond_b
    add-int/lit8 v0, v0, -0x1

    #@d
    goto :goto_3

    #@e
    .line 92
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_a
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .registers 15
    .parameter "source"
    .parameter "start"
    .parameter "end"
    .parameter "dest"
    .parameter "dstart"
    .parameter "dend"

    #@0
    .prologue
    .line 49
    invoke-virtual {p0}, Landroid/text/method/NumberKeyListener;->getAcceptedChars()[C

    #@3
    move-result-object v0

    #@4
    .line 50
    .local v0, accept:[C
    const/4 v1, 0x0

    #@5
    .line 53
    .local v1, filter:Z
    move v3, p2

    #@6
    .local v3, i:I
    :goto_6
    if-ge v3, p3, :cond_12

    #@8
    .line 54
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    #@b
    move-result v6

    #@c
    invoke-static {v0, v6}, Landroid/text/method/NumberKeyListener;->ok([CC)Z

    #@f
    move-result v6

    #@10
    if-nez v6, :cond_16

    #@12
    .line 59
    :cond_12
    if-ne v3, p3, :cond_19

    #@14
    .line 61
    const/4 v2, 0x0

    #@15
    .line 82
    :cond_15
    :goto_15
    return-object v2

    #@16
    .line 53
    :cond_16
    add-int/lit8 v3, v3, 0x1

    #@18
    goto :goto_6

    #@19
    .line 64
    :cond_19
    sub-int v6, p3, p2

    #@1b
    const/4 v7, 0x1

    #@1c
    if-ne v6, v7, :cond_21

    #@1e
    .line 66
    const-string v2, ""

    #@20
    goto :goto_15

    #@21
    .line 69
    :cond_21
    new-instance v2, Landroid/text/SpannableStringBuilder;

    #@23
    invoke-direct {v2, p1, p2, p3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;II)V

    #@26
    .line 71
    .local v2, filtered:Landroid/text/SpannableStringBuilder;
    sub-int/2addr v3, p2

    #@27
    .line 72
    sub-int/2addr p3, p2

    #@28
    .line 74
    sub-int v5, p3, p2

    #@2a
    .line 76
    .local v5, len:I
    add-int/lit8 v4, p3, -0x1

    #@2c
    .local v4, j:I
    :goto_2c
    if-lt v4, v3, :cond_15

    #@2e
    .line 77
    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@31
    move-result v6

    #@32
    invoke-static {v0, v6}, Landroid/text/method/NumberKeyListener;->ok([CC)Z

    #@35
    move-result v6

    #@36
    if-nez v6, :cond_3d

    #@38
    .line 78
    add-int/lit8 v6, v4, 0x1

    #@3a
    invoke-virtual {v2, v4, v6}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    #@3d
    .line 76
    :cond_3d
    add-int/lit8 v4, v4, -0x1

    #@3f
    goto :goto_2c
.end method

.method protected abstract getAcceptedChars()[C
.end method

.method protected lookup(Landroid/view/KeyEvent;Landroid/text/Spannable;)I
    .registers 6
    .parameter "event"
    .parameter "content"

    #@0
    .prologue
    .line 44
    invoke-virtual {p0}, Landroid/text/method/NumberKeyListener;->getAcceptedChars()[C

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    #@7
    move-result v1

    #@8
    invoke-static {p2}, Landroid/text/method/NumberKeyListener;->getMetaState(Ljava/lang/CharSequence;)I

    #@b
    move-result v2

    #@c
    or-int/2addr v1, v2

    #@d
    invoke-virtual {p1, v0, v1}, Landroid/view/KeyEvent;->getMatch([CI)C

    #@10
    move-result v0

    #@11
    return v0
.end method

.method public onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .registers 14
    .parameter "view"
    .parameter "content"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/16 v8, 0x30

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 101
    invoke-static {p2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@7
    move-result v0

    #@8
    .line 102
    .local v0, a:I
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@b
    move-result v1

    #@c
    .line 104
    .local v1, b:I
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@f
    move-result v5

    #@10
    .line 105
    .local v5, selStart:I
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@13
    move-result v4

    #@14
    .line 108
    .local v4, selEnd:I
    if-ltz v5, :cond_18

    #@16
    if-gez v4, :cond_1d

    #@18
    .line 109
    :cond_18
    const/4 v4, 0x0

    #@19
    move v5, v4

    #@1a
    .line 110
    invoke-static {p2, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@1d
    .line 113
    :cond_1d
    if-eqz p4, :cond_3e

    #@1f
    invoke-virtual {p0, p4, p2}, Landroid/text/method/NumberKeyListener;->lookup(Landroid/view/KeyEvent;Landroid/text/Spannable;)I

    #@22
    move-result v2

    #@23
    .line 114
    .local v2, i:I
    :goto_23
    if-eqz p4, :cond_29

    #@25
    invoke-virtual {p4}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@28
    move-result v3

    #@29
    .line 115
    .local v3, repeatCount:I
    :cond_29
    if-nez v3, :cond_40

    #@2b
    .line 116
    if-eqz v2, :cond_5f

    #@2d
    .line 117
    if-eq v5, v4, :cond_32

    #@2f
    .line 118
    invoke-static {p2, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@32
    .line 121
    :cond_32
    int-to-char v7, v2

    #@33
    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@36
    move-result-object v7

    #@37
    invoke-interface {p2, v5, v4, v7}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@3a
    .line 123
    invoke-static {p2}, Landroid/text/method/NumberKeyListener;->adjustMetaAfterKeypress(Landroid/text/Spannable;)V

    #@3d
    .line 138
    :goto_3d
    return v6

    #@3e
    .end local v2           #i:I
    .end local v3           #repeatCount:I
    :cond_3e
    move v2, v3

    #@3f
    .line 113
    goto :goto_23

    #@40
    .line 126
    .restart local v2       #i:I
    .restart local v3       #repeatCount:I
    :cond_40
    if-ne v2, v8, :cond_5f

    #@42
    if-ne v3, v6, :cond_5f

    #@44
    .line 129
    if-ne v5, v4, :cond_5f

    #@46
    if-lez v4, :cond_5f

    #@48
    add-int/lit8 v7, v5, -0x1

    #@4a
    invoke-interface {p2, v7}, Landroid/text/Editable;->charAt(I)C

    #@4d
    move-result v7

    #@4e
    if-ne v7, v8, :cond_5f

    #@50
    .line 131
    add-int/lit8 v7, v5, -0x1

    #@52
    const/16 v8, 0x2b

    #@54
    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@57
    move-result-object v8

    #@58
    invoke-interface {p2, v7, v4, v8}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@5b
    .line 132
    invoke-static {p2}, Landroid/text/method/NumberKeyListener;->adjustMetaAfterKeypress(Landroid/text/Spannable;)V

    #@5e
    goto :goto_3d

    #@5f
    .line 137
    :cond_5f
    invoke-static {p2}, Landroid/text/method/NumberKeyListener;->adjustMetaAfterKeypress(Landroid/text/Spannable;)V

    #@62
    .line 138
    invoke-super {p0, p1, p2, p3, p4}, Landroid/text/method/BaseKeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@65
    move-result v6

    #@66
    goto :goto_3d
.end method
