.class public Landroid/text/method/DateKeyListener;
.super Landroid/text/method/NumberKeyListener;
.source "DateKeyListener.java"


# static fields
.field public static final CHARACTERS:[C

.field private static sInstance:Landroid/text/method/DateKeyListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 56
    const/16 v0, 0xd

    #@2
    new-array v0, v0, [C

    #@4
    fill-array-data v0, :array_a

    #@7
    sput-object v0, Landroid/text/method/DateKeyListener;->CHARACTERS:[C

    #@9
    return-void

    #@a
    :array_a
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
        0x2ft 0x0t
        0x2dt 0x0t
        0x2et 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 29
    invoke-direct {p0}, Landroid/text/method/NumberKeyListener;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Landroid/text/method/DateKeyListener;
    .registers 1

    #@0
    .prologue
    .line 43
    sget-object v0, Landroid/text/method/DateKeyListener;->sInstance:Landroid/text/method/DateKeyListener;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 44
    sget-object v0, Landroid/text/method/DateKeyListener;->sInstance:Landroid/text/method/DateKeyListener;

    #@6
    .line 47
    :goto_6
    return-object v0

    #@7
    .line 46
    :cond_7
    new-instance v0, Landroid/text/method/DateKeyListener;

    #@9
    invoke-direct {v0}, Landroid/text/method/DateKeyListener;-><init>()V

    #@c
    sput-object v0, Landroid/text/method/DateKeyListener;->sInstance:Landroid/text/method/DateKeyListener;

    #@e
    .line 47
    sget-object v0, Landroid/text/method/DateKeyListener;->sInstance:Landroid/text/method/DateKeyListener;

    #@10
    goto :goto_6
.end method


# virtual methods
.method protected getAcceptedChars()[C
    .registers 2

    #@0
    .prologue
    .line 39
    sget-object v0, Landroid/text/method/DateKeyListener;->CHARACTERS:[C

    #@2
    return-object v0
.end method

.method public getInputType()I
    .registers 2

    #@0
    .prologue
    .line 32
    const/16 v0, 0x14

    #@2
    return v0
.end method
