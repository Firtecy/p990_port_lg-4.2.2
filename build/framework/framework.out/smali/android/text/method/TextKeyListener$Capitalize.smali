.class public final enum Landroid/text/method/TextKeyListener$Capitalize;
.super Ljava/lang/Enum;
.source "TextKeyListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/method/TextKeyListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Capitalize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/text/method/TextKeyListener$Capitalize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/text/method/TextKeyListener$Capitalize;

.field public static final enum CHARACTERS:Landroid/text/method/TextKeyListener$Capitalize;

.field public static final enum NONE:Landroid/text/method/TextKeyListener$Capitalize;

.field public static final enum SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

.field public static final enum WORDS:Landroid/text/method/TextKeyListener$Capitalize;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 208
    new-instance v0, Landroid/text/method/TextKeyListener$Capitalize;

    #@6
    const-string v1, "NONE"

    #@8
    invoke-direct {v0, v1, v2}, Landroid/text/method/TextKeyListener$Capitalize;-><init>(Ljava/lang/String;I)V

    #@b
    sput-object v0, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    #@d
    new-instance v0, Landroid/text/method/TextKeyListener$Capitalize;

    #@f
    const-string v1, "SENTENCES"

    #@11
    invoke-direct {v0, v1, v3}, Landroid/text/method/TextKeyListener$Capitalize;-><init>(Ljava/lang/String;I)V

    #@14
    sput-object v0, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    #@16
    new-instance v0, Landroid/text/method/TextKeyListener$Capitalize;

    #@18
    const-string v1, "WORDS"

    #@1a
    invoke-direct {v0, v1, v4}, Landroid/text/method/TextKeyListener$Capitalize;-><init>(Ljava/lang/String;I)V

    #@1d
    sput-object v0, Landroid/text/method/TextKeyListener$Capitalize;->WORDS:Landroid/text/method/TextKeyListener$Capitalize;

    #@1f
    new-instance v0, Landroid/text/method/TextKeyListener$Capitalize;

    #@21
    const-string v1, "CHARACTERS"

    #@23
    invoke-direct {v0, v1, v5}, Landroid/text/method/TextKeyListener$Capitalize;-><init>(Ljava/lang/String;I)V

    #@26
    sput-object v0, Landroid/text/method/TextKeyListener$Capitalize;->CHARACTERS:Landroid/text/method/TextKeyListener$Capitalize;

    #@28
    .line 207
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Landroid/text/method/TextKeyListener$Capitalize;

    #@2b
    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->WORDS:Landroid/text/method/TextKeyListener$Capitalize;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->CHARACTERS:Landroid/text/method/TextKeyListener$Capitalize;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Landroid/text/method/TextKeyListener$Capitalize;->$VALUES:[Landroid/text/method/TextKeyListener$Capitalize;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 207
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/text/method/TextKeyListener$Capitalize;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 207
    const-class v0, Landroid/text/method/TextKeyListener$Capitalize;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/text/method/TextKeyListener$Capitalize;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/text/method/TextKeyListener$Capitalize;
    .registers 1

    #@0
    .prologue
    .line 207
    sget-object v0, Landroid/text/method/TextKeyListener$Capitalize;->$VALUES:[Landroid/text/method/TextKeyListener$Capitalize;

    #@2
    invoke-virtual {v0}, [Landroid/text/method/TextKeyListener$Capitalize;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/text/method/TextKeyListener$Capitalize;

    #@8
    return-object v0
.end method
