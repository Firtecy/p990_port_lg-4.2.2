.class public Landroid/text/method/BaseMovementMethod;
.super Ljava/lang/Object;
.source "BaseMovementMethod.java"

# interfaces
.implements Landroid/text/method/MovementMethod;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private getBottomLine(Landroid/widget/TextView;)I
    .registers 5
    .parameter "widget"

    #@0
    .prologue
    .line 419
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@7
    move-result v1

    #@8
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getInnerHeight(Landroid/widget/TextView;)I

    #@b
    move-result v2

    #@c
    add-int/2addr v1, v2

    #@d
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForVertical(I)I

    #@10
    move-result v0

    #@11
    return v0
.end method

.method private getCharacterWidth(Landroid/widget/TextView;)I
    .registers 4
    .parameter "widget"

    #@0
    .prologue
    .line 431
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontSpacing()F

    #@7
    move-result v0

    #@8
    float-to-double v0, v0

    #@9
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    #@c
    move-result-wide v0

    #@d
    double-to-int v0, v0

    #@e
    return v0
.end method

.method private getInnerHeight(Landroid/widget/TextView;)I
    .registers 4
    .parameter "widget"

    #@0
    .prologue
    .line 427
    invoke-virtual {p1}, Landroid/widget/TextView;->getHeight()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    #@7
    move-result v1

    #@8
    sub-int/2addr v0, v1

    #@9
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingBottom()I

    #@c
    move-result v1

    #@d
    sub-int/2addr v0, v1

    #@e
    return v0
.end method

.method private getInnerWidth(Landroid/widget/TextView;)I
    .registers 4
    .parameter "widget"

    #@0
    .prologue
    .line 423
    invoke-virtual {p1}, Landroid/widget/TextView;->getWidth()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    #@7
    move-result v1

    #@8
    sub-int/2addr v0, v1

    #@9
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingRight()I

    #@c
    move-result v1

    #@d
    sub-int/2addr v0, v1

    #@e
    return v0
.end method

.method private getScrollBoundsLeft(Landroid/widget/TextView;)I
    .registers 10
    .parameter "widget"

    #@0
    .prologue
    .line 435
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v1

    #@4
    .line 436
    .local v1, layout:Landroid/text/Layout;
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getTopLine(Landroid/widget/TextView;)I

    #@7
    move-result v5

    #@8
    .line 437
    .local v5, topLine:I
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getBottomLine(Landroid/widget/TextView;)I

    #@b
    move-result v0

    #@c
    .line 438
    .local v0, bottomLine:I
    if-le v5, v0, :cond_10

    #@e
    .line 439
    const/4 v2, 0x0

    #@f
    .line 448
    :cond_f
    return v2

    #@10
    .line 441
    :cond_10
    const v2, 0x7fffffff

    #@13
    .line 442
    .local v2, left:I
    move v3, v5

    #@14
    .local v3, line:I
    :goto_14
    if-gt v3, v0, :cond_f

    #@16
    .line 443
    invoke-virtual {v1, v3}, Landroid/text/Layout;->getLineLeft(I)F

    #@19
    move-result v6

    #@1a
    float-to-double v6, v6

    #@1b
    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    #@1e
    move-result-wide v6

    #@1f
    double-to-int v4, v6

    #@20
    .line 444
    .local v4, lineLeft:I
    if-ge v4, v2, :cond_23

    #@22
    .line 445
    move v2, v4

    #@23
    .line 442
    :cond_23
    add-int/lit8 v3, v3, 0x1

    #@25
    goto :goto_14
.end method

.method private getScrollBoundsRight(Landroid/widget/TextView;)I
    .registers 10
    .parameter "widget"

    #@0
    .prologue
    .line 452
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v1

    #@4
    .line 453
    .local v1, layout:Landroid/text/Layout;
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getTopLine(Landroid/widget/TextView;)I

    #@7
    move-result v5

    #@8
    .line 454
    .local v5, topLine:I
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getBottomLine(Landroid/widget/TextView;)I

    #@b
    move-result v0

    #@c
    .line 455
    .local v0, bottomLine:I
    if-le v5, v0, :cond_10

    #@e
    .line 456
    const/4 v4, 0x0

    #@f
    .line 465
    :cond_f
    return v4

    #@10
    .line 458
    :cond_10
    const/high16 v4, -0x8000

    #@12
    .line 459
    .local v4, right:I
    move v2, v5

    #@13
    .local v2, line:I
    :goto_13
    if-gt v2, v0, :cond_f

    #@15
    .line 460
    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineRight(I)F

    #@18
    move-result v6

    #@19
    float-to-double v6, v6

    #@1a
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    #@1d
    move-result-wide v6

    #@1e
    double-to-int v3, v6

    #@1f
    .line 461
    .local v3, lineRight:I
    if-le v3, v4, :cond_22

    #@21
    .line 462
    move v4, v3

    #@22
    .line 459
    :cond_22
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_13
.end method

.method private getTopLine(Landroid/widget/TextView;)I
    .registers 4
    .parameter "widget"

    #@0
    .prologue
    .line 415
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForVertical(I)I

    #@b
    move-result v0

    #@c
    return v0
.end method


# virtual methods
.method protected bottom(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 349
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public canSelectArbitrarily()Z
    .registers 2

    #@0
    .prologue
    .line 32
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected down(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 301
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected end(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 411
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected getMovementMetaState(Landroid/text/Spannable;Landroid/view/KeyEvent;)I
    .registers 6
    .parameter "buffer"
    .parameter "event"

    #@0
    .prologue
    .line 138
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    #@3
    move-result v1

    #@4
    invoke-static {p1}, Landroid/text/method/MetaKeyKeyListener;->getMetaState(Ljava/lang/CharSequence;)I

    #@7
    move-result v2

    #@8
    or-int/2addr v1, v2

    #@9
    and-int/lit16 v0, v1, -0x601

    #@b
    .line 140
    .local v0, metaState:I
    invoke-static {v0}, Landroid/view/KeyEvent;->normalizeMetaState(I)I

    #@e
    move-result v1

    #@f
    and-int/lit16 v1, v1, -0xc2

    #@11
    return v1
.end method

.method protected handleMovementKey(Landroid/widget/TextView;Landroid/text/Spannable;IILandroid/view/KeyEvent;)Z
    .registers 9
    .parameter "widget"
    .parameter "buffer"
    .parameter "keyCode"
    .parameter "movementMetaState"
    .parameter "event"

    #@0
    .prologue
    const/16 v2, 0x1000

    #@2
    const/4 v1, 0x2

    #@3
    .line 162
    sparse-switch p3, :sswitch_data_ee

    #@6
    .line 253
    :cond_6
    const/4 v0, 0x0

    #@7
    :goto_7
    return v0

    #@8
    .line 164
    :sswitch_8
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_6

    #@e
    .line 165
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->left(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@11
    move-result v0

    #@12
    goto :goto_7

    #@13
    .line 170
    :sswitch_13
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_6

    #@19
    .line 171
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->right(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@1c
    move-result v0

    #@1d
    goto :goto_7

    #@1e
    .line 176
    :sswitch_1e
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_29

    #@24
    .line 177
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->left(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@27
    move-result v0

    #@28
    goto :goto_7

    #@29
    .line 178
    :cond_29
    invoke-static {p4, v2}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_34

    #@2f
    .line 180
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->leftWord(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@32
    move-result v0

    #@33
    goto :goto_7

    #@34
    .line 181
    :cond_34
    invoke-static {p4, v1}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_6

    #@3a
    .line 183
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->lineStart(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@3d
    move-result v0

    #@3e
    goto :goto_7

    #@3f
    .line 188
    :sswitch_3f
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@42
    move-result v0

    #@43
    if-eqz v0, :cond_4a

    #@45
    .line 189
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->right(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@48
    move-result v0

    #@49
    goto :goto_7

    #@4a
    .line 190
    :cond_4a
    invoke-static {p4, v2}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@4d
    move-result v0

    #@4e
    if-eqz v0, :cond_55

    #@50
    .line 192
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->rightWord(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@53
    move-result v0

    #@54
    goto :goto_7

    #@55
    .line 193
    :cond_55
    invoke-static {p4, v1}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@58
    move-result v0

    #@59
    if-eqz v0, :cond_6

    #@5b
    .line 195
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->lineEnd(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@5e
    move-result v0

    #@5f
    goto :goto_7

    #@60
    .line 200
    :sswitch_60
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@63
    move-result v0

    #@64
    if-eqz v0, :cond_6b

    #@66
    .line 201
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->up(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@69
    move-result v0

    #@6a
    goto :goto_7

    #@6b
    .line 202
    :cond_6b
    invoke-static {p4, v1}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@6e
    move-result v0

    #@6f
    if-eqz v0, :cond_6

    #@71
    .line 204
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->top(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@74
    move-result v0

    #@75
    goto :goto_7

    #@76
    .line 209
    :sswitch_76
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@79
    move-result v0

    #@7a
    if-eqz v0, :cond_81

    #@7c
    .line 210
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->down(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@7f
    move-result v0

    #@80
    goto :goto_7

    #@81
    .line 211
    :cond_81
    invoke-static {p4, v1}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@84
    move-result v0

    #@85
    if-eqz v0, :cond_6

    #@87
    .line 213
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->bottom(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@8a
    move-result v0

    #@8b
    goto/16 :goto_7

    #@8d
    .line 218
    :sswitch_8d
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@90
    move-result v0

    #@91
    if-eqz v0, :cond_99

    #@93
    .line 219
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->pageUp(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@96
    move-result v0

    #@97
    goto/16 :goto_7

    #@99
    .line 220
    :cond_99
    invoke-static {p4, v1}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@9c
    move-result v0

    #@9d
    if-eqz v0, :cond_6

    #@9f
    .line 222
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->top(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@a2
    move-result v0

    #@a3
    goto/16 :goto_7

    #@a5
    .line 227
    :sswitch_a5
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@a8
    move-result v0

    #@a9
    if-eqz v0, :cond_b1

    #@ab
    .line 228
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->pageDown(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@ae
    move-result v0

    #@af
    goto/16 :goto_7

    #@b1
    .line 229
    :cond_b1
    invoke-static {p4, v1}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@b4
    move-result v0

    #@b5
    if-eqz v0, :cond_6

    #@b7
    .line 231
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->bottom(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@ba
    move-result v0

    #@bb
    goto/16 :goto_7

    #@bd
    .line 236
    :sswitch_bd
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@c0
    move-result v0

    #@c1
    if-eqz v0, :cond_c9

    #@c3
    .line 237
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->home(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@c6
    move-result v0

    #@c7
    goto/16 :goto_7

    #@c9
    .line 238
    :cond_c9
    invoke-static {p4, v2}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@cc
    move-result v0

    #@cd
    if-eqz v0, :cond_6

    #@cf
    .line 240
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->top(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@d2
    move-result v0

    #@d3
    goto/16 :goto_7

    #@d5
    .line 245
    :sswitch_d5
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@d8
    move-result v0

    #@d9
    if-eqz v0, :cond_e1

    #@db
    .line 246
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->end(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@de
    move-result v0

    #@df
    goto/16 :goto_7

    #@e1
    .line 247
    :cond_e1
    invoke-static {p4, v2}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@e4
    move-result v0

    #@e5
    if-eqz v0, :cond_6

    #@e7
    .line 249
    invoke-virtual {p0, p1, p2}, Landroid/text/method/BaseMovementMethod;->bottom(Landroid/widget/TextView;Landroid/text/Spannable;)Z

    #@ea
    move-result v0

    #@eb
    goto/16 :goto_7

    #@ed
    .line 162
    nop

    #@ee
    :sswitch_data_ee
    .sparse-switch
        0x13 -> :sswitch_60
        0x14 -> :sswitch_76
        0x15 -> :sswitch_1e
        0x16 -> :sswitch_3f
        0x5c -> :sswitch_8d
        0x5d -> :sswitch_a5
        0x7a -> :sswitch_bd
        0x7b -> :sswitch_d5
        0xe7 -> :sswitch_8
        0xe8 -> :sswitch_13
    .end sparse-switch
.end method

.method protected home(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 397
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public initialize(Landroid/widget/TextView;Landroid/text/Spannable;)V
    .registers 3
    .parameter "widget"
    .parameter "text"

    #@0
    .prologue
    .line 37
    return-void
.end method

.method protected left(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 265
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected leftWord(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 378
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected lineEnd(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 373
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected lineStart(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 361
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onGenericMotionEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "widget"
    .parameter "text"
    .parameter "event"

    #@0
    .prologue
    const/16 v4, 0x9

    #@2
    const/4 v5, 0x0

    #@3
    .line 94
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getSource()I

    #@6
    move-result v3

    #@7
    and-int/lit8 v3, v3, 0x2

    #@9
    if-eqz v3, :cond_12

    #@b
    .line 95
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    #@e
    move-result v3

    #@f
    packed-switch v3, :pswitch_data_70

    #@12
    .line 122
    :cond_12
    const/4 v0, 0x0

    #@13
    :cond_13
    :goto_13
    return v0

    #@14
    .line 99
    :pswitch_14
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getMetaState()I

    #@17
    move-result v3

    #@18
    and-int/lit8 v3, v3, 0x1

    #@1a
    if-eqz v3, :cond_43

    #@1c
    .line 100
    const/4 v2, 0x0

    #@1d
    .line 101
    .local v2, vscroll:F
    invoke-virtual {p3, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@20
    move-result v1

    #@21
    .line 107
    .local v1, hscroll:F
    :goto_21
    const/4 v0, 0x0

    #@22
    .line 108
    .local v0, handled:Z
    cmpg-float v3, v1, v5

    #@24
    if-gez v3, :cond_4f

    #@26
    .line 109
    neg-float v3, v1

    #@27
    float-to-double v3, v3

    #@28
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    #@2b
    move-result-wide v3

    #@2c
    double-to-int v3, v3

    #@2d
    invoke-virtual {p0, p1, p2, v3}, Landroid/text/method/BaseMovementMethod;->scrollLeft(Landroid/widget/TextView;Landroid/text/Spannable;I)Z

    #@30
    move-result v3

    #@31
    or-int/2addr v0, v3

    #@32
    .line 113
    :cond_32
    :goto_32
    cmpg-float v3, v2, v5

    #@34
    if-gez v3, :cond_5f

    #@36
    .line 114
    neg-float v3, v2

    #@37
    float-to-double v3, v3

    #@38
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    #@3b
    move-result-wide v3

    #@3c
    double-to-int v3, v3

    #@3d
    invoke-virtual {p0, p1, p2, v3}, Landroid/text/method/BaseMovementMethod;->scrollUp(Landroid/widget/TextView;Landroid/text/Spannable;I)Z

    #@40
    move-result v3

    #@41
    or-int/2addr v0, v3

    #@42
    goto :goto_13

    #@43
    .line 103
    .end local v0           #handled:Z
    .end local v1           #hscroll:F
    .end local v2           #vscroll:F
    :cond_43
    invoke-virtual {p3, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@46
    move-result v3

    #@47
    neg-float v2, v3

    #@48
    .line 104
    .restart local v2       #vscroll:F
    const/16 v3, 0xa

    #@4a
    invoke-virtual {p3, v3}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@4d
    move-result v1

    #@4e
    .restart local v1       #hscroll:F
    goto :goto_21

    #@4f
    .line 110
    .restart local v0       #handled:Z
    :cond_4f
    cmpl-float v3, v1, v5

    #@51
    if-lez v3, :cond_32

    #@53
    .line 111
    float-to-double v3, v1

    #@54
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    #@57
    move-result-wide v3

    #@58
    double-to-int v3, v3

    #@59
    invoke-virtual {p0, p1, p2, v3}, Landroid/text/method/BaseMovementMethod;->scrollRight(Landroid/widget/TextView;Landroid/text/Spannable;I)Z

    #@5c
    move-result v3

    #@5d
    or-int/2addr v0, v3

    #@5e
    goto :goto_32

    #@5f
    .line 115
    :cond_5f
    cmpl-float v3, v2, v5

    #@61
    if-lez v3, :cond_13

    #@63
    .line 116
    float-to-double v3, v2

    #@64
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    #@67
    move-result-wide v3

    #@68
    double-to-int v3, v3

    #@69
    invoke-virtual {p0, p1, p2, v3}, Landroid/text/method/BaseMovementMethod;->scrollDown(Landroid/widget/TextView;Landroid/text/Spannable;I)Z

    #@6c
    move-result v3

    #@6d
    or-int/2addr v0, v3

    #@6e
    goto :goto_13

    #@6f
    .line 95
    nop

    #@70
    :pswitch_data_70
    .packed-switch 0x8
        :pswitch_14
    .end packed-switch
.end method

.method public onKeyDown(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z
    .registers 12
    .parameter "widget"
    .parameter "text"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 41
    invoke-virtual {p0, p2, p4}, Landroid/text/method/BaseMovementMethod;->getMovementMetaState(Landroid/text/Spannable;Landroid/view/KeyEvent;)I

    #@3
    move-result v4

    #@4
    .local v4, movementMetaState:I
    move-object v0, p0

    #@5
    move-object v1, p1

    #@6
    move-object v2, p2

    #@7
    move v3, p3

    #@8
    move-object v5, p4

    #@9
    .line 42
    invoke-virtual/range {v0 .. v5}, Landroid/text/method/BaseMovementMethod;->handleMovementKey(Landroid/widget/TextView;Landroid/text/Spannable;IILandroid/view/KeyEvent;)Z

    #@c
    move-result v6

    #@d
    .line 43
    .local v6, handled:Z
    if-eqz v6, :cond_15

    #@f
    .line 44
    invoke-static {p2}, Landroid/text/method/MetaKeyKeyListener;->adjustMetaAfterKeypress(Landroid/text/Spannable;)V

    #@12
    .line 45
    invoke-static {p2}, Landroid/text/method/MetaKeyKeyListener;->resetLockedMeta(Landroid/text/Spannable;)V

    #@15
    .line 47
    :cond_15
    return v6
.end method

.method public onKeyOther(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/KeyEvent;)Z
    .registers 13
    .parameter "widget"
    .parameter "text"
    .parameter "event"

    #@0
    .prologue
    .line 52
    invoke-virtual {p0, p2, p3}, Landroid/text/method/BaseMovementMethod;->getMovementMetaState(Landroid/text/Spannable;Landroid/view/KeyEvent;)I

    #@3
    move-result v4

    #@4
    .line 53
    .local v4, movementMetaState:I
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    #@7
    move-result v3

    #@8
    .line 54
    .local v3, keyCode:I
    if-eqz v3, :cond_30

    #@a
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@d
    move-result v0

    #@e
    const/4 v1, 0x2

    #@f
    if-ne v0, v1, :cond_30

    #@11
    .line 56
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@14
    move-result v8

    #@15
    .line 57
    .local v8, repeat:I
    const/4 v6, 0x0

    #@16
    .line 58
    .local v6, handled:Z
    const/4 v7, 0x0

    #@17
    .local v7, i:I
    :goto_17
    if-ge v7, v8, :cond_23

    #@19
    move-object v0, p0

    #@1a
    move-object v1, p1

    #@1b
    move-object v2, p2

    #@1c
    move-object v5, p3

    #@1d
    .line 59
    invoke-virtual/range {v0 .. v5}, Landroid/text/method/BaseMovementMethod;->handleMovementKey(Landroid/widget/TextView;Landroid/text/Spannable;IILandroid/view/KeyEvent;)Z

    #@20
    move-result v0

    #@21
    if-nez v0, :cond_2c

    #@23
    .line 64
    :cond_23
    if-eqz v6, :cond_2b

    #@25
    .line 65
    invoke-static {p2}, Landroid/text/method/MetaKeyKeyListener;->adjustMetaAfterKeypress(Landroid/text/Spannable;)V

    #@28
    .line 66
    invoke-static {p2}, Landroid/text/method/MetaKeyKeyListener;->resetLockedMeta(Landroid/text/Spannable;)V

    #@2b
    .line 70
    .end local v6           #handled:Z
    .end local v7           #i:I
    .end local v8           #repeat:I
    :cond_2b
    :goto_2b
    return v6

    #@2c
    .line 62
    .restart local v6       #handled:Z
    .restart local v7       #i:I
    .restart local v8       #repeat:I
    :cond_2c
    const/4 v6, 0x1

    #@2d
    .line 58
    add-int/lit8 v7, v7, 0x1

    #@2f
    goto :goto_17

    #@30
    .line 70
    .end local v6           #handled:Z
    .end local v7           #i:I
    .end local v8           #repeat:I
    :cond_30
    const/4 v6, 0x0

    #@31
    goto :goto_2b
.end method

.method public onKeyUp(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "widget"
    .parameter "text"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 75
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onTakeFocus(Landroid/widget/TextView;Landroid/text/Spannable;I)V
    .registers 4
    .parameter "widget"
    .parameter "text"
    .parameter "direction"

    #@0
    .prologue
    .line 80
    return-void
.end method

.method public onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "widget"
    .parameter "text"
    .parameter "event"

    #@0
    .prologue
    .line 84
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onTrackballEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "widget"
    .parameter "text"
    .parameter "event"

    #@0
    .prologue
    .line 89
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected pageDown(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 325
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected pageUp(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 313
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected right(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 277
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected rightWord(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 383
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected scrollBottom(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 8
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 638
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v0

    #@4
    .line 639
    .local v0, layout:Landroid/text/Layout;
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    #@7
    move-result v1

    #@8
    .line 640
    .local v1, lineCount:I
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getBottomLine(Landroid/widget/TextView;)I

    #@b
    move-result v2

    #@c
    add-int/lit8 v3, v1, -0x1

    #@e
    if-gt v2, v3, :cond_22

    #@10
    .line 641
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@13
    move-result v2

    #@14
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    #@17
    move-result v3

    #@18
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getInnerHeight(Landroid/widget/TextView;)I

    #@1b
    move-result v4

    #@1c
    sub-int/2addr v3, v4

    #@1d
    invoke-static {p1, v0, v2, v3}, Landroid/text/method/Touch;->scrollTo(Landroid/widget/TextView;Landroid/text/Layout;II)V

    #@20
    .line 643
    const/4 v2, 0x1

    #@21
    .line 645
    :goto_21
    return v2

    #@22
    :cond_22
    const/4 v2, 0x0

    #@23
    goto :goto_21
.end method

.method protected scrollDown(Landroid/widget/TextView;Landroid/text/Spannable;I)Z
    .registers 11
    .parameter "widget"
    .parameter "buffer"
    .parameter "amount"

    #@0
    .prologue
    .line 548
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v3

    #@4
    .line 549
    .local v3, layout:Landroid/text/Layout;
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getInnerHeight(Landroid/widget/TextView;)I

    #@7
    move-result v2

    #@8
    .line 550
    .local v2, innerHeight:I
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@b
    move-result v5

    #@c
    add-int v0, v5, v2

    #@e
    .line 551
    .local v0, bottom:I
    invoke-virtual {v3, v0}, Landroid/text/Layout;->getLineForVertical(I)I

    #@11
    move-result v1

    #@12
    .line 552
    .local v1, bottomLine:I
    add-int/lit8 v5, v1, 0x1

    #@14
    invoke-virtual {v3, v5}, Landroid/text/Layout;->getLineTop(I)I

    #@17
    move-result v5

    #@18
    add-int/lit8 v6, v0, 0x1

    #@1a
    if-ge v5, v6, :cond_1e

    #@1c
    .line 556
    add-int/lit8 v1, v1, 0x1

    #@1e
    .line 558
    :cond_1e
    invoke-virtual {v3}, Landroid/text/Layout;->getLineCount()I

    #@21
    move-result v5

    #@22
    add-int/lit8 v4, v5, -0x1

    #@24
    .line 559
    .local v4, limit:I
    if-gt v1, v4, :cond_3e

    #@26
    .line 560
    add-int v5, v1, p3

    #@28
    add-int/lit8 v5, v5, -0x1

    #@2a
    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    #@2d
    move-result v1

    #@2e
    .line 561
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@31
    move-result v5

    #@32
    add-int/lit8 v6, v1, 0x1

    #@34
    invoke-virtual {v3, v6}, Landroid/text/Layout;->getLineTop(I)I

    #@37
    move-result v6

    #@38
    sub-int/2addr v6, v2

    #@39
    invoke-static {p1, v3, v5, v6}, Landroid/text/method/Touch;->scrollTo(Landroid/widget/TextView;Landroid/text/Layout;II)V

    #@3c
    .line 563
    const/4 v5, 0x1

    #@3d
    .line 565
    :goto_3d
    return v5

    #@3e
    :cond_3e
    const/4 v5, 0x0

    #@3f
    goto :goto_3d
.end method

.method protected scrollLeft(Landroid/widget/TextView;Landroid/text/Spannable;I)Z
    .registers 7
    .parameter "widget"
    .parameter "buffer"
    .parameter "amount"

    #@0
    .prologue
    .line 479
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getScrollBoundsLeft(Landroid/widget/TextView;)I

    #@3
    move-result v0

    #@4
    .line 480
    .local v0, minScrollX:I
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@7
    move-result v1

    #@8
    .line 481
    .local v1, scrollX:I
    if-le v1, v0, :cond_1e

    #@a
    .line 482
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getCharacterWidth(Landroid/widget/TextView;)I

    #@d
    move-result v2

    #@e
    mul-int/2addr v2, p3

    #@f
    sub-int v2, v1, v2

    #@11
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    #@14
    move-result v1

    #@15
    .line 483
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@18
    move-result v2

    #@19
    invoke-virtual {p1, v1, v2}, Landroid/widget/TextView;->scrollTo(II)V

    #@1c
    .line 484
    const/4 v2, 0x1

    #@1d
    .line 486
    :goto_1d
    return v2

    #@1e
    :cond_1e
    const/4 v2, 0x0

    #@1f
    goto :goto_1d
.end method

.method protected scrollLineEnd(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 7
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 677
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getScrollBoundsRight(Landroid/widget/TextView;)I

    #@3
    move-result v2

    #@4
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getInnerWidth(Landroid/widget/TextView;)I

    #@7
    move-result v3

    #@8
    sub-int v0, v2, v3

    #@a
    .line 678
    .local v0, maxScrollX:I
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@d
    move-result v1

    #@e
    .line 679
    .local v1, scrollX:I
    if-ge v1, v0, :cond_19

    #@10
    .line 680
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@13
    move-result v2

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/widget/TextView;->scrollTo(II)V

    #@17
    .line 681
    const/4 v2, 0x1

    #@18
    .line 683
    :goto_18
    return v2

    #@19
    :cond_19
    const/4 v2, 0x0

    #@1a
    goto :goto_18
.end method

.method protected scrollLineStart(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 6
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 658
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getScrollBoundsLeft(Landroid/widget/TextView;)I

    #@3
    move-result v0

    #@4
    .line 659
    .local v0, minScrollX:I
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@7
    move-result v1

    #@8
    .line 660
    .local v1, scrollX:I
    if-le v1, v0, :cond_13

    #@a
    .line 661
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@d
    move-result v2

    #@e
    invoke-virtual {p1, v0, v2}, Landroid/widget/TextView;->scrollTo(II)V

    #@11
    .line 662
    const/4 v2, 0x1

    #@12
    .line 664
    :goto_12
    return v2

    #@13
    :cond_13
    const/4 v2, 0x0

    #@14
    goto :goto_12
.end method

.method protected scrollPageDown(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 9
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 598
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v3

    #@4
    .line 599
    .local v3, layout:Landroid/text/Layout;
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getInnerHeight(Landroid/widget/TextView;)I

    #@7
    move-result v2

    #@8
    .line 600
    .local v2, innerHeight:I
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@b
    move-result v4

    #@c
    add-int/2addr v4, v2

    #@d
    add-int v0, v4, v2

    #@f
    .line 601
    .local v0, bottom:I
    invoke-virtual {v3, v0}, Landroid/text/Layout;->getLineForVertical(I)I

    #@12
    move-result v1

    #@13
    .line 602
    .local v1, bottomLine:I
    invoke-virtual {v3}, Landroid/text/Layout;->getLineCount()I

    #@16
    move-result v4

    #@17
    add-int/lit8 v4, v4, -0x1

    #@19
    if-gt v1, v4, :cond_2b

    #@1b
    .line 603
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@1e
    move-result v4

    #@1f
    add-int/lit8 v5, v1, 0x1

    #@21
    invoke-virtual {v3, v5}, Landroid/text/Layout;->getLineTop(I)I

    #@24
    move-result v5

    #@25
    sub-int/2addr v5, v2

    #@26
    invoke-static {p1, v3, v4, v5}, Landroid/text/method/Touch;->scrollTo(Landroid/widget/TextView;Landroid/text/Layout;II)V

    #@29
    .line 605
    const/4 v4, 0x1

    #@2a
    .line 607
    :goto_2a
    return v4

    #@2b
    :cond_2b
    const/4 v4, 0x0

    #@2c
    goto :goto_2a
.end method

.method protected scrollPageUp(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 8
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 578
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v0

    #@4
    .line 579
    .local v0, layout:Landroid/text/Layout;
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@7
    move-result v3

    #@8
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getInnerHeight(Landroid/widget/TextView;)I

    #@b
    move-result v4

    #@c
    sub-int v1, v3, v4

    #@e
    .line 580
    .local v1, top:I
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForVertical(I)I

    #@11
    move-result v2

    #@12
    .line 581
    .local v2, topLine:I
    if-ltz v2, :cond_21

    #@14
    .line 582
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@17
    move-result v3

    #@18
    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineTop(I)I

    #@1b
    move-result v4

    #@1c
    invoke-static {p1, v0, v3, v4}, Landroid/text/method/Touch;->scrollTo(Landroid/widget/TextView;Landroid/text/Layout;II)V

    #@1f
    .line 583
    const/4 v3, 0x1

    #@20
    .line 585
    :goto_20
    return v3

    #@21
    :cond_21
    const/4 v3, 0x0

    #@22
    goto :goto_20
.end method

.method protected scrollRight(Landroid/widget/TextView;Landroid/text/Spannable;I)Z
    .registers 8
    .parameter "widget"
    .parameter "buffer"
    .parameter "amount"

    #@0
    .prologue
    .line 500
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getScrollBoundsRight(Landroid/widget/TextView;)I

    #@3
    move-result v2

    #@4
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getInnerWidth(Landroid/widget/TextView;)I

    #@7
    move-result v3

    #@8
    sub-int v0, v2, v3

    #@a
    .line 501
    .local v0, maxScrollX:I
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@d
    move-result v1

    #@e
    .line 502
    .local v1, scrollX:I
    if-ge v1, v0, :cond_23

    #@10
    .line 503
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getCharacterWidth(Landroid/widget/TextView;)I

    #@13
    move-result v2

    #@14
    mul-int/2addr v2, p3

    #@15
    add-int/2addr v2, v1

    #@16
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    #@19
    move-result v1

    #@1a
    .line 504
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@1d
    move-result v2

    #@1e
    invoke-virtual {p1, v1, v2}, Landroid/widget/TextView;->scrollTo(II)V

    #@21
    .line 505
    const/4 v2, 0x1

    #@22
    .line 507
    :goto_22
    return v2

    #@23
    :cond_23
    const/4 v2, 0x0

    #@24
    goto :goto_22
.end method

.method protected scrollTop(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 6
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 620
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@4
    move-result-object v0

    #@5
    .line 621
    .local v0, layout:Landroid/text/Layout;
    invoke-direct {p0, p1}, Landroid/text/method/BaseMovementMethod;->getTopLine(Landroid/widget/TextView;)I

    #@8
    move-result v2

    #@9
    if-ltz v2, :cond_17

    #@b
    .line 622
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@e
    move-result v2

    #@f
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    #@12
    move-result v1

    #@13
    invoke-static {p1, v0, v2, v1}, Landroid/text/method/Touch;->scrollTo(Landroid/widget/TextView;Landroid/text/Layout;II)V

    #@16
    .line 623
    const/4 v1, 0x1

    #@17
    .line 625
    :cond_17
    return v1
.end method

.method protected scrollUp(Landroid/widget/TextView;Landroid/text/Spannable;I)Z
    .registers 9
    .parameter "widget"
    .parameter "buffer"
    .parameter "amount"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 521
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@4
    move-result-object v0

    #@5
    .line 522
    .local v0, layout:Landroid/text/Layout;
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    #@8
    move-result v1

    #@9
    .line 523
    .local v1, top:I
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForVertical(I)I

    #@c
    move-result v2

    #@d
    .line 524
    .local v2, topLine:I
    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineTop(I)I

    #@10
    move-result v4

    #@11
    if-ne v4, v1, :cond_15

    #@13
    .line 527
    add-int/lit8 v2, v2, -0x1

    #@15
    .line 529
    :cond_15
    if-ltz v2, :cond_2b

    #@17
    .line 530
    sub-int v4, v2, p3

    #@19
    add-int/lit8 v4, v4, 0x1

    #@1b
    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    #@1e
    move-result v2

    #@1f
    .line 531
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    #@22
    move-result v3

    #@23
    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineTop(I)I

    #@26
    move-result v4

    #@27
    invoke-static {p1, v0, v3, v4}, Landroid/text/method/Touch;->scrollTo(Landroid/widget/TextView;Landroid/text/Layout;II)V

    #@2a
    .line 532
    const/4 v3, 0x1

    #@2b
    .line 534
    :cond_2b
    return v3
.end method

.method protected top(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 337
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected up(Landroid/widget/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    .line 289
    const/4 v0, 0x0

    #@1
    return v0
.end method
