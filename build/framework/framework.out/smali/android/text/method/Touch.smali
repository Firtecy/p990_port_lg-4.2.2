.class public Landroid/text/method/Touch;
.super Ljava/lang/Object;
.source "Touch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/method/Touch$DragState;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getInitialScrollX(Landroid/widget/TextView;Landroid/text/Spannable;)I
    .registers 6
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 182
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    #@4
    move-result v1

    #@5
    const-class v2, Landroid/text/method/Touch$DragState;

    #@7
    invoke-interface {p1, v3, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, [Landroid/text/method/Touch$DragState;

    #@d
    .line 183
    .local v0, ds:[Landroid/text/method/Touch$DragState;
    array-length v1, v0

    #@e
    if-lez v1, :cond_15

    #@10
    aget-object v1, v0, v3

    #@12
    iget v1, v1, Landroid/text/method/Touch$DragState;->mScrollX:I

    #@14
    :goto_14
    return v1

    #@15
    :cond_15
    const/4 v1, -0x1

    #@16
    goto :goto_14
.end method

.method public static getInitialScrollY(Landroid/widget/TextView;Landroid/text/Spannable;)I
    .registers 6
    .parameter "widget"
    .parameter "buffer"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 191
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    #@4
    move-result v1

    #@5
    const-class v2, Landroid/text/method/Touch$DragState;

    #@7
    invoke-interface {p1, v3, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, [Landroid/text/method/Touch$DragState;

    #@d
    .line 192
    .local v0, ds:[Landroid/text/method/Touch$DragState;
    array-length v1, v0

    #@e
    if-lez v1, :cond_15

    #@10
    aget-object v1, v0, v3

    #@12
    iget v1, v1, Landroid/text/method/Touch$DragState;->mScrollY:I

    #@14
    :goto_14
    return v1

    #@15
    :cond_15
    const/4 v1, -0x1

    #@16
    goto :goto_14
.end method

.method public static onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .registers 25
    .parameter "widget"
    .parameter "buffer"
    .parameter "event"

    #@0
    .prologue
    .line 90
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionMasked()I

    #@3
    move-result v17

    #@4
    packed-switch v17, :pswitch_data_21c

    #@7
    .line 174
    :cond_7
    const/16 v17, 0x0

    #@9
    :goto_9
    return v17

    #@a
    .line 92
    :pswitch_a
    const/16 v17, 0x0

    #@c
    invoke-interface/range {p1 .. p1}, Landroid/text/Spannable;->length()I

    #@f
    move-result v18

    #@10
    const-class v19, Landroid/text/method/Touch$DragState;

    #@12
    move-object/from16 v0, p1

    #@14
    move/from16 v1, v17

    #@16
    move/from16 v2, v18

    #@18
    move-object/from16 v3, v19

    #@1a
    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@1d
    move-result-object v6

    #@1e
    check-cast v6, [Landroid/text/method/Touch$DragState;

    #@20
    .line 94
    .local v6, ds:[Landroid/text/method/Touch$DragState;
    const/4 v9, 0x0

    #@21
    .local v9, i:I
    :goto_21
    array-length v0, v6

    #@22
    move/from16 v17, v0

    #@24
    move/from16 v0, v17

    #@26
    if-ge v9, v0, :cond_34

    #@28
    .line 95
    aget-object v17, v6, v9

    #@2a
    move-object/from16 v0, p1

    #@2c
    move-object/from16 v1, v17

    #@2e
    invoke-interface {v0, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@31
    .line 94
    add-int/lit8 v9, v9, 0x1

    #@33
    goto :goto_21

    #@34
    .line 98
    :cond_34
    new-instance v17, Landroid/text/method/Touch$DragState;

    #@36
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    #@39
    move-result v18

    #@3a
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    #@3d
    move-result v19

    #@3e
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getScrollX()I

    #@41
    move-result v20

    #@42
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getScrollY()I

    #@45
    move-result v21

    #@46
    invoke-direct/range {v17 .. v21}, Landroid/text/method/Touch$DragState;-><init>(FFII)V

    #@49
    const/16 v18, 0x0

    #@4b
    const/16 v19, 0x0

    #@4d
    const/16 v20, 0x11

    #@4f
    move-object/from16 v0, p1

    #@51
    move-object/from16 v1, v17

    #@53
    move/from16 v2, v18

    #@55
    move/from16 v3, v19

    #@57
    move/from16 v4, v20

    #@59
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@5c
    .line 101
    const/16 v17, 0x1

    #@5e
    goto :goto_9

    #@5f
    .line 104
    .end local v6           #ds:[Landroid/text/method/Touch$DragState;
    .end local v9           #i:I
    :pswitch_5f
    const/16 v17, 0x0

    #@61
    invoke-interface/range {p1 .. p1}, Landroid/text/Spannable;->length()I

    #@64
    move-result v18

    #@65
    const-class v19, Landroid/text/method/Touch$DragState;

    #@67
    move-object/from16 v0, p1

    #@69
    move/from16 v1, v17

    #@6b
    move/from16 v2, v18

    #@6d
    move-object/from16 v3, v19

    #@6f
    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@72
    move-result-object v6

    #@73
    check-cast v6, [Landroid/text/method/Touch$DragState;

    #@75
    .line 106
    .restart local v6       #ds:[Landroid/text/method/Touch$DragState;
    const/4 v9, 0x0

    #@76
    .restart local v9       #i:I
    :goto_76
    array-length v0, v6

    #@77
    move/from16 v17, v0

    #@79
    move/from16 v0, v17

    #@7b
    if-ge v9, v0, :cond_89

    #@7d
    .line 107
    aget-object v17, v6, v9

    #@7f
    move-object/from16 v0, p1

    #@81
    move-object/from16 v1, v17

    #@83
    invoke-interface {v0, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@86
    .line 106
    add-int/lit8 v9, v9, 0x1

    #@88
    goto :goto_76

    #@89
    .line 110
    :cond_89
    array-length v0, v6

    #@8a
    move/from16 v17, v0

    #@8c
    if-lez v17, :cond_9e

    #@8e
    const/16 v17, 0x0

    #@90
    aget-object v17, v6, v17

    #@92
    move-object/from16 v0, v17

    #@94
    iget-boolean v0, v0, Landroid/text/method/Touch$DragState;->mUsed:Z

    #@96
    move/from16 v17, v0

    #@98
    if-eqz v17, :cond_9e

    #@9a
    .line 111
    const/16 v17, 0x1

    #@9c
    goto/16 :goto_9

    #@9e
    .line 113
    :cond_9e
    const/16 v17, 0x0

    #@a0
    goto/16 :goto_9

    #@a2
    .line 117
    .end local v6           #ds:[Landroid/text/method/Touch$DragState;
    .end local v9           #i:I
    :pswitch_a2
    const/16 v17, 0x0

    #@a4
    invoke-interface/range {p1 .. p1}, Landroid/text/Spannable;->length()I

    #@a7
    move-result v18

    #@a8
    const-class v19, Landroid/text/method/Touch$DragState;

    #@aa
    move-object/from16 v0, p1

    #@ac
    move/from16 v1, v17

    #@ae
    move/from16 v2, v18

    #@b0
    move-object/from16 v3, v19

    #@b2
    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@b5
    move-result-object v6

    #@b6
    check-cast v6, [Landroid/text/method/Touch$DragState;

    #@b8
    .line 119
    .restart local v6       #ds:[Landroid/text/method/Touch$DragState;
    array-length v0, v6

    #@b9
    move/from16 v17, v0

    #@bb
    if-lez v17, :cond_7

    #@bd
    .line 120
    const/16 v17, 0x0

    #@bf
    aget-object v17, v6, v17

    #@c1
    move-object/from16 v0, v17

    #@c3
    iget-boolean v0, v0, Landroid/text/method/Touch$DragState;->mFarEnough:Z

    #@c5
    move/from16 v17, v0

    #@c7
    if-nez v17, :cond_11b

    #@c9
    .line 121
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@cc
    move-result-object v17

    #@cd
    invoke-static/range {v17 .. v17}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@d0
    move-result-object v17

    #@d1
    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@d4
    move-result v16

    #@d5
    .line 123
    .local v16, slop:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    #@d8
    move-result v17

    #@d9
    const/16 v18, 0x0

    #@db
    aget-object v18, v6, v18

    #@dd
    move-object/from16 v0, v18

    #@df
    iget v0, v0, Landroid/text/method/Touch$DragState;->mX:F

    #@e1
    move/from16 v18, v0

    #@e3
    sub-float v17, v17, v18

    #@e5
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    #@e8
    move-result v17

    #@e9
    move/from16 v0, v16

    #@eb
    int-to-float v0, v0

    #@ec
    move/from16 v18, v0

    #@ee
    cmpl-float v17, v17, v18

    #@f0
    if-gez v17, :cond_10f

    #@f2
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    #@f5
    move-result v17

    #@f6
    const/16 v18, 0x0

    #@f8
    aget-object v18, v6, v18

    #@fa
    move-object/from16 v0, v18

    #@fc
    iget v0, v0, Landroid/text/method/Touch$DragState;->mY:F

    #@fe
    move/from16 v18, v0

    #@100
    sub-float v17, v17, v18

    #@102
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    #@105
    move-result v17

    #@106
    move/from16 v0, v16

    #@108
    int-to-float v0, v0

    #@109
    move/from16 v18, v0

    #@10b
    cmpl-float v17, v17, v18

    #@10d
    if-ltz v17, :cond_11b

    #@10f
    .line 125
    :cond_10f
    const/16 v17, 0x0

    #@111
    aget-object v17, v6, v17

    #@113
    const/16 v18, 0x1

    #@115
    move/from16 v0, v18

    #@117
    move-object/from16 v1, v17

    #@119
    iput-boolean v0, v1, Landroid/text/method/Touch$DragState;->mFarEnough:Z

    #@11b
    .line 129
    .end local v16           #slop:I
    :cond_11b
    const/16 v17, 0x0

    #@11d
    aget-object v17, v6, v17

    #@11f
    move-object/from16 v0, v17

    #@121
    iget-boolean v0, v0, Landroid/text/method/Touch$DragState;->mFarEnough:Z

    #@123
    move/from16 v17, v0

    #@125
    if-eqz v17, :cond_7

    #@127
    .line 130
    const/16 v17, 0x0

    #@129
    aget-object v17, v6, v17

    #@12b
    const/16 v18, 0x1

    #@12d
    move/from16 v0, v18

    #@12f
    move-object/from16 v1, v17

    #@131
    iput-boolean v0, v1, Landroid/text/method/Touch$DragState;->mUsed:Z

    #@133
    .line 131
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getMetaState()I

    #@136
    move-result v17

    #@137
    and-int/lit8 v17, v17, 0x1

    #@139
    if-nez v17, :cond_159

    #@13b
    const/16 v17, 0x1

    #@13d
    move-object/from16 v0, p1

    #@13f
    move/from16 v1, v17

    #@141
    invoke-static {v0, v1}, Landroid/text/method/MetaKeyKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@144
    move-result v17

    #@145
    const/16 v18, 0x1

    #@147
    move/from16 v0, v17

    #@149
    move/from16 v1, v18

    #@14b
    if-eq v0, v1, :cond_159

    #@14d
    const/16 v17, 0x800

    #@14f
    move-object/from16 v0, p1

    #@151
    move/from16 v1, v17

    #@153
    invoke-static {v0, v1}, Landroid/text/method/MetaKeyKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@156
    move-result v17

    #@157
    if-eqz v17, :cond_1f6

    #@159
    :cond_159
    const/4 v5, 0x1

    #@15a
    .line 138
    .local v5, cap:Z
    :goto_15a
    if-eqz v5, :cond_1f9

    #@15c
    .line 141
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    #@15f
    move-result v17

    #@160
    const/16 v18, 0x0

    #@162
    aget-object v18, v6, v18

    #@164
    move-object/from16 v0, v18

    #@166
    iget v0, v0, Landroid/text/method/Touch$DragState;->mX:F

    #@168
    move/from16 v18, v0

    #@16a
    sub-float v7, v17, v18

    #@16c
    .line 142
    .local v7, dx:F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    #@16f
    move-result v17

    #@170
    const/16 v18, 0x0

    #@172
    aget-object v18, v6, v18

    #@174
    move-object/from16 v0, v18

    #@176
    iget v0, v0, Landroid/text/method/Touch$DragState;->mY:F

    #@178
    move/from16 v18, v0

    #@17a
    sub-float v8, v17, v18

    #@17c
    .line 147
    .local v8, dy:F
    :goto_17c
    const/16 v17, 0x0

    #@17e
    aget-object v17, v6, v17

    #@180
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    #@183
    move-result v18

    #@184
    move/from16 v0, v18

    #@186
    move-object/from16 v1, v17

    #@188
    iput v0, v1, Landroid/text/method/Touch$DragState;->mX:F

    #@18a
    .line 148
    const/16 v17, 0x0

    #@18c
    aget-object v17, v6, v17

    #@18e
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    #@191
    move-result v18

    #@192
    move/from16 v0, v18

    #@194
    move-object/from16 v1, v17

    #@196
    iput v0, v1, Landroid/text/method/Touch$DragState;->mY:F

    #@198
    .line 150
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getScrollX()I

    #@19b
    move-result v17

    #@19c
    float-to-int v0, v7

    #@19d
    move/from16 v18, v0

    #@19f
    add-int v11, v17, v18

    #@1a1
    .line 151
    .local v11, nx:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getScrollY()I

    #@1a4
    move-result v17

    #@1a5
    float-to-int v0, v8

    #@1a6
    move/from16 v18, v0

    #@1a8
    add-int v12, v17, v18

    #@1aa
    .line 153
    .local v12, ny:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getTotalPaddingTop()I

    #@1ad
    move-result v17

    #@1ae
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getTotalPaddingBottom()I

    #@1b1
    move-result v18

    #@1b2
    add-int v15, v17, v18

    #@1b4
    .line 154
    .local v15, padding:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@1b7
    move-result-object v10

    #@1b8
    .line 156
    .local v10, layout:Landroid/text/Layout;
    invoke-virtual {v10}, Landroid/text/Layout;->getHeight()I

    #@1bb
    move-result v17

    #@1bc
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getHeight()I

    #@1bf
    move-result v18

    #@1c0
    sub-int v18, v18, v15

    #@1c2
    sub-int v17, v17, v18

    #@1c4
    move/from16 v0, v17

    #@1c6
    invoke-static {v12, v0}, Ljava/lang/Math;->min(II)I

    #@1c9
    move-result v12

    #@1ca
    .line 157
    const/16 v17, 0x0

    #@1cc
    move/from16 v0, v17

    #@1ce
    invoke-static {v12, v0}, Ljava/lang/Math;->max(II)I

    #@1d1
    move-result v12

    #@1d2
    .line 159
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getScrollX()I

    #@1d5
    move-result v13

    #@1d6
    .line 160
    .local v13, oldX:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getScrollY()I

    #@1d9
    move-result v14

    #@1da
    .line 162
    .local v14, oldY:I
    move-object/from16 v0, p0

    #@1dc
    invoke-static {v0, v10, v11, v12}, Landroid/text/method/Touch;->scrollTo(Landroid/widget/TextView;Landroid/text/Layout;II)V

    #@1df
    .line 165
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getScrollX()I

    #@1e2
    move-result v17

    #@1e3
    move/from16 v0, v17

    #@1e5
    if-ne v13, v0, :cond_1ef

    #@1e7
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getScrollY()I

    #@1ea
    move-result v17

    #@1eb
    move/from16 v0, v17

    #@1ed
    if-eq v14, v0, :cond_1f2

    #@1ef
    .line 166
    :cond_1ef
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->cancelLongPress()V

    #@1f2
    .line 169
    :cond_1f2
    const/16 v17, 0x1

    #@1f4
    goto/16 :goto_9

    #@1f6
    .line 131
    .end local v5           #cap:Z
    .end local v7           #dx:F
    .end local v8           #dy:F
    .end local v10           #layout:Landroid/text/Layout;
    .end local v11           #nx:I
    .end local v12           #ny:I
    .end local v13           #oldX:I
    .end local v14           #oldY:I
    .end local v15           #padding:I
    :cond_1f6
    const/4 v5, 0x0

    #@1f7
    goto/16 :goto_15a

    #@1f9
    .line 144
    .restart local v5       #cap:Z
    :cond_1f9
    const/16 v17, 0x0

    #@1fb
    aget-object v17, v6, v17

    #@1fd
    move-object/from16 v0, v17

    #@1ff
    iget v0, v0, Landroid/text/method/Touch$DragState;->mX:F

    #@201
    move/from16 v17, v0

    #@203
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    #@206
    move-result v18

    #@207
    sub-float v7, v17, v18

    #@209
    .line 145
    .restart local v7       #dx:F
    const/16 v17, 0x0

    #@20b
    aget-object v17, v6, v17

    #@20d
    move-object/from16 v0, v17

    #@20f
    iget v0, v0, Landroid/text/method/Touch$DragState;->mY:F

    #@211
    move/from16 v17, v0

    #@213
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    #@216
    move-result v18

    #@217
    sub-float v8, v17, v18

    #@219
    .restart local v8       #dy:F
    goto/16 :goto_17c

    #@21b
    .line 90
    nop

    #@21c
    :pswitch_data_21c
    .packed-switch 0x0
        :pswitch_a
        :pswitch_5f
        :pswitch_a2
    .end packed-switch
.end method

.method public static scrollTo(Landroid/widget/TextView;Landroid/text/Layout;II)V
    .registers 19
    .parameter "widget"
    .parameter "layout"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 38
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    #@3
    move-result v13

    #@4
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingRight()I

    #@7
    move-result v14

    #@8
    add-int v6, v13, v14

    #@a
    .line 39
    .local v6, horizontalPadding:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getWidth()I

    #@d
    move-result v13

    #@e
    sub-int v4, v13, v6

    #@10
    .line 41
    .local v4, availableWidth:I
    move-object/from16 v0, p1

    #@12
    move/from16 v1, p3

    #@14
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForVertical(I)I

    #@17
    move-result v11

    #@18
    .line 42
    .local v11, top:I
    move-object/from16 v0, p1

    #@1a
    invoke-virtual {v0, v11}, Landroid/text/Layout;->getParagraphAlignment(I)Landroid/text/Layout$Alignment;

    #@1d
    move-result-object v2

    #@1e
    .line 43
    .local v2, a:Landroid/text/Layout$Alignment;
    move-object/from16 v0, p1

    #@20
    invoke-virtual {v0, v11}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@23
    move-result v13

    #@24
    if-lez v13, :cond_66

    #@26
    const/4 v9, 0x1

    #@27
    .line 46
    .local v9, ltr:Z
    :goto_27
    invoke-virtual {p0}, Landroid/widget/TextView;->getHorizontallyScrolling()Z

    #@2a
    move-result v13

    #@2b
    if-eqz v13, :cond_68

    #@2d
    .line 47
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingTop()I

    #@30
    move-result v13

    #@31
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingBottom()I

    #@34
    move-result v14

    #@35
    add-int v12, v13, v14

    #@37
    .line 48
    .local v12, verticalPadding:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getHeight()I

    #@3a
    move-result v13

    #@3b
    add-int v13, v13, p3

    #@3d
    sub-int/2addr v13, v12

    #@3e
    move-object/from16 v0, p1

    #@40
    invoke-virtual {v0, v13}, Landroid/text/Layout;->getLineForVertical(I)I

    #@43
    move-result v5

    #@44
    .line 50
    .local v5, bottom:I
    const v8, 0x7fffffff

    #@47
    .line 51
    .local v8, left:I
    const/4 v10, 0x0

    #@48
    .line 53
    .local v10, right:I
    move v7, v11

    #@49
    .local v7, i:I
    :goto_49
    if-gt v7, v5, :cond_6a

    #@4b
    .line 54
    int-to-float v13, v8

    #@4c
    move-object/from16 v0, p1

    #@4e
    invoke-virtual {v0, v7}, Landroid/text/Layout;->getLineLeft(I)F

    #@51
    move-result v14

    #@52
    invoke-static {v13, v14}, Ljava/lang/Math;->min(FF)F

    #@55
    move-result v13

    #@56
    float-to-int v8, v13

    #@57
    .line 55
    int-to-float v13, v10

    #@58
    move-object/from16 v0, p1

    #@5a
    invoke-virtual {v0, v7}, Landroid/text/Layout;->getLineRight(I)F

    #@5d
    move-result v14

    #@5e
    invoke-static {v13, v14}, Ljava/lang/Math;->max(FF)F

    #@61
    move-result v13

    #@62
    float-to-int v10, v13

    #@63
    .line 53
    add-int/lit8 v7, v7, 0x1

    #@65
    goto :goto_49

    #@66
    .line 43
    .end local v5           #bottom:I
    .end local v7           #i:I
    .end local v8           #left:I
    .end local v9           #ltr:Z
    .end local v10           #right:I
    .end local v12           #verticalPadding:I
    :cond_66
    const/4 v9, 0x0

    #@67
    goto :goto_27

    #@68
    .line 58
    .restart local v9       #ltr:Z
    :cond_68
    const/4 v8, 0x0

    #@69
    .line 59
    .restart local v8       #left:I
    move v10, v4

    #@6a
    .line 62
    .restart local v10       #right:I
    :cond_6a
    sub-int v3, v10, v8

    #@6c
    .line 64
    .local v3, actualWidth:I
    if-ge v3, v4, :cond_92

    #@6e
    .line 65
    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    #@70
    if-ne v2, v13, :cond_80

    #@72
    .line 66
    sub-int v13, v4, v3

    #@74
    div-int/lit8 v13, v13, 0x2

    #@76
    sub-int p2, v8, v13

    #@78
    .line 79
    :goto_78
    move/from16 v0, p2

    #@7a
    move/from16 v1, p3

    #@7c
    invoke-virtual {p0, v0, v1}, Landroid/widget/TextView;->scrollTo(II)V

    #@7f
    .line 80
    return-void

    #@80
    .line 67
    :cond_80
    if-eqz v9, :cond_86

    #@82
    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@84
    if-eq v2, v13, :cond_8a

    #@86
    :cond_86
    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@88
    if-ne v2, v13, :cond_8f

    #@8a
    .line 70
    :cond_8a
    sub-int v13, v4, v3

    #@8c
    sub-int p2, v8, v13

    #@8e
    goto :goto_78

    #@8f
    .line 72
    :cond_8f
    move/from16 p2, v8

    #@91
    goto :goto_78

    #@92
    .line 75
    :cond_92
    sub-int v13, v10, v4

    #@94
    move/from16 v0, p2

    #@96
    invoke-static {v0, v13}, Ljava/lang/Math;->min(II)I

    #@99
    move-result p2

    #@9a
    .line 76
    move/from16 v0, p2

    #@9c
    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    #@9f
    move-result p2

    #@a0
    goto :goto_78
.end method
