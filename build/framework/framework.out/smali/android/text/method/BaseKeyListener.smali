.class public abstract Landroid/text/method/BaseKeyListener;
.super Landroid/text/method/MetaKeyKeyListener;
.source "BaseKeyListener.java"

# interfaces
.implements Landroid/text/method/KeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/method/BaseKeyListener$1;
    }
.end annotation


# static fields
.field static final OLD_SEL_START:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 38
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    #@2
    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    #@5
    sput-object v0, Landroid/text/method/BaseKeyListener;->OLD_SEL_START:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Landroid/text/method/MetaKeyKeyListener;-><init>()V

    #@3
    .line 142
    return-void
.end method

.method private backspaceOrForwardDelete(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;Z)Z
    .registers 11
    .parameter "view"
    .parameter "content"
    .parameter "keyCode"
    .parameter "event"
    .parameter "isForwardDelete"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 67
    invoke-virtual {p4}, Landroid/view/KeyEvent;->getMetaState()I

    #@5
    move-result v4

    #@6
    and-int/lit16 v4, v4, -0xf4

    #@8
    invoke-static {v4}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@b
    move-result v4

    #@c
    if-nez v4, :cond_f

    #@e
    .line 97
    :cond_e
    :goto_e
    return v2

    #@f
    .line 73
    :cond_f
    invoke-direct {p0, p1, p2}, Landroid/text/method/BaseKeyListener;->deleteSelection(Landroid/view/View;Landroid/text/Editable;)Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_17

    #@15
    move v2, v3

    #@16
    .line 74
    goto :goto_e

    #@17
    .line 78
    :cond_17
    invoke-virtual {p4}, Landroid/view/KeyEvent;->isAltPressed()Z

    #@1a
    move-result v4

    #@1b
    if-nez v4, :cond_24

    #@1d
    const/4 v4, 0x2

    #@1e
    invoke-static {p2, v4}, Landroid/text/method/BaseKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@21
    move-result v4

    #@22
    if-ne v4, v3, :cond_2c

    #@24
    .line 79
    :cond_24
    invoke-direct {p0, p1, p2}, Landroid/text/method/BaseKeyListener;->deleteLine(Landroid/view/View;Landroid/text/Editable;)Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_2c

    #@2a
    move v2, v3

    #@2b
    .line 80
    goto :goto_e

    #@2c
    .line 85
    :cond_2c
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@2f
    move-result v1

    #@30
    .line 87
    .local v1, start:I
    if-nez p5, :cond_3e

    #@32
    invoke-virtual {p4}, Landroid/view/KeyEvent;->isShiftPressed()Z

    #@35
    move-result v4

    #@36
    if-nez v4, :cond_3e

    #@38
    invoke-static {p2, v3}, Landroid/text/method/BaseKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@3b
    move-result v4

    #@3c
    if-ne v4, v3, :cond_51

    #@3e
    .line 89
    :cond_3e
    invoke-static {p2, v1}, Landroid/text/TextUtils;->getOffsetAfter(Ljava/lang/CharSequence;I)I

    #@41
    move-result v0

    #@42
    .line 93
    .local v0, end:I
    :goto_42
    if-eq v1, v0, :cond_e

    #@44
    .line 94
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    #@47
    move-result v2

    #@48
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    #@4b
    move-result v4

    #@4c
    invoke-interface {p2, v2, v4}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@4f
    move v2, v3

    #@50
    .line 95
    goto :goto_e

    #@51
    .line 91
    .end local v0           #end:I
    :cond_51
    invoke-static {p2, v1}, Landroid/text/TextUtils;->getOffsetBefore(Ljava/lang/CharSequence;I)I

    #@54
    move-result v0

    #@55
    .restart local v0       #end:I
    goto :goto_42
.end method

.method private deleteLine(Landroid/view/View;Landroid/text/Editable;)Z
    .registers 8
    .parameter "view"
    .parameter "content"

    #@0
    .prologue
    .line 116
    instance-of v4, p1, Landroid/widget/TextView;

    #@2
    if-eqz v4, :cond_23

    #@4
    .line 117
    check-cast p1, Landroid/widget/TextView;

    #@6
    .end local p1
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@9
    move-result-object v1

    #@a
    .line 118
    .local v1, layout:Landroid/text/Layout;
    if-eqz v1, :cond_23

    #@c
    .line 119
    invoke-static {p2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@f
    move-result v4

    #@10
    invoke-virtual {v1, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    #@13
    move-result v2

    #@14
    .line 120
    .local v2, line:I
    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineStart(I)I

    #@17
    move-result v3

    #@18
    .line 121
    .local v3, start:I
    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineEnd(I)I

    #@1b
    move-result v0

    #@1c
    .line 122
    .local v0, end:I
    if-eq v0, v3, :cond_23

    #@1e
    .line 123
    invoke-interface {p2, v3, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@21
    .line 124
    const/4 v4, 0x1

    #@22
    .line 128
    .end local v0           #end:I
    .end local v1           #layout:Landroid/text/Layout;
    .end local v2           #line:I
    .end local v3           #start:I
    :goto_22
    return v4

    #@23
    :cond_23
    const/4 v4, 0x0

    #@24
    goto :goto_22
.end method

.method private deleteSelection(Landroid/view/View;Landroid/text/Editable;)Z
    .registers 7
    .parameter "view"
    .parameter "content"

    #@0
    .prologue
    .line 101
    invoke-static {p2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@3
    move-result v1

    #@4
    .line 102
    .local v1, selectionStart:I
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@7
    move-result v0

    #@8
    .line 103
    .local v0, selectionEnd:I
    if-ge v0, v1, :cond_d

    #@a
    .line 104
    move v2, v0

    #@b
    .line 105
    .local v2, temp:I
    move v0, v1

    #@c
    .line 106
    move v1, v2

    #@d
    .line 108
    .end local v2           #temp:I
    :cond_d
    if-eq v1, v0, :cond_14

    #@f
    .line 109
    invoke-interface {p2, v1, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@12
    .line 110
    const/4 v3, 0x1

    #@13
    .line 112
    :goto_13
    return v3

    #@14
    :cond_14
    const/4 v3, 0x0

    #@15
    goto :goto_13
.end method

.method static makeTextContentType(Landroid/text/method/TextKeyListener$Capitalize;Z)I
    .registers 5
    .parameter "caps"
    .parameter "autoText"

    #@0
    .prologue
    .line 132
    const/4 v0, 0x1

    #@1
    .line 133
    .local v0, contentType:I
    sget-object v1, Landroid/text/method/BaseKeyListener$1;->$SwitchMap$android$text$method$TextKeyListener$Capitalize:[I

    #@3
    invoke-virtual {p0}, Landroid/text/method/TextKeyListener$Capitalize;->ordinal()I

    #@6
    move-result v2

    #@7
    aget v1, v1, v2

    #@9
    packed-switch v1, :pswitch_data_1c

    #@c
    .line 144
    :goto_c
    if-eqz p1, :cond_12

    #@e
    .line 145
    const v1, 0x8000

    #@11
    or-int/2addr v0, v1

    #@12
    .line 147
    :cond_12
    return v0

    #@13
    .line 135
    :pswitch_13
    or-int/lit16 v0, v0, 0x1000

    #@15
    .line 136
    goto :goto_c

    #@16
    .line 138
    :pswitch_16
    or-int/lit16 v0, v0, 0x2000

    #@18
    .line 139
    goto :goto_c

    #@19
    .line 141
    :pswitch_19
    or-int/lit16 v0, v0, 0x4000

    #@1b
    goto :goto_c

    #@1c
    .line 133
    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_13
        :pswitch_16
        :pswitch_19
    .end packed-switch
.end method


# virtual methods
.method public backspace(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .registers 11
    .parameter "view"
    .parameter "content"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 49
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move-object v4, p4

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/text/method/BaseKeyListener;->backspaceOrForwardDelete(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;Z)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public forwardDelete(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .registers 11
    .parameter "view"
    .parameter "content"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 61
    const/4 v5, 0x1

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move-object v4, p4

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/text/method/BaseKeyListener;->backspaceOrForwardDelete(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;Z)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "view"
    .parameter "content"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 153
    sparse-switch p3, :sswitch_data_18

    #@3
    .line 161
    const/4 v0, 0x0

    #@4
    .line 165
    .local v0, handled:Z
    :goto_4
    if-eqz v0, :cond_9

    #@6
    .line 166
    invoke-static {p2}, Landroid/text/method/BaseKeyListener;->adjustMetaAfterKeypress(Landroid/text/Spannable;)V

    #@9
    .line 169
    :cond_9
    invoke-super {p0, p1, p2, p3, p4}, Landroid/text/method/MetaKeyKeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@c
    move-result v1

    #@d
    return v1

    #@e
    .line 155
    .end local v0           #handled:Z
    :sswitch_e
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/text/method/BaseKeyListener;->backspace(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@11
    move-result v0

    #@12
    .line 156
    .restart local v0       #handled:Z
    goto :goto_4

    #@13
    .line 158
    .end local v0           #handled:Z
    :sswitch_13
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/text/method/BaseKeyListener;->forwardDelete(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@16
    move-result v0

    #@17
    .line 159
    .restart local v0       #handled:Z
    goto :goto_4

    #@18
    .line 153
    :sswitch_data_18
    .sparse-switch
        0x43 -> :sswitch_e
        0x70 -> :sswitch_13
    .end sparse-switch
.end method

.method public onKeyOther(Landroid/view/View;Landroid/text/Editable;Landroid/view/KeyEvent;)Z
    .registers 11
    .parameter "view"
    .parameter "content"
    .parameter "event"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 177
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@4
    move-result v5

    #@5
    const/4 v6, 0x2

    #@6
    if-ne v5, v6, :cond_e

    #@8
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    #@b
    move-result v5

    #@c
    if-eqz v5, :cond_f

    #@e
    .line 197
    :cond_e
    :goto_e
    return v4

    #@f
    .line 183
    :cond_f
    invoke-static {p2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@12
    move-result v1

    #@13
    .line 184
    .local v1, selectionStart:I
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@16
    move-result v0

    #@17
    .line 185
    .local v0, selectionEnd:I
    if-ge v0, v1, :cond_1c

    #@19
    .line 186
    move v2, v0

    #@1a
    .line 187
    .local v2, temp:I
    move v0, v1

    #@1b
    .line 188
    move v1, v2

    #@1c
    .line 191
    .end local v2           #temp:I
    :cond_1c
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    .line 192
    .local v3, text:Ljava/lang/CharSequence;
    if-eqz v3, :cond_e

    #@22
    .line 196
    invoke-interface {p2, v1, v0, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@25
    .line 197
    const/4 v4, 0x1

    #@26
    goto :goto_e
.end method
