.class public abstract Landroid/text/method/MetaKeyKeyListener;
.super Ljava/lang/Object;
.source "MetaKeyKeyListener.java"


# static fields
.field private static final ALT:Ljava/lang/Object; = null

.field private static final CAP:Ljava/lang/Object; = null

.field private static final LOCKED:I = 0x4000011

.field public static final META_ALT_LOCKED:I = 0x200

.field private static final META_ALT_MASK:J = 0x2020200000202L

.field public static final META_ALT_ON:I = 0x2

.field private static final META_ALT_PRESSED:J = 0x20000000000L

.field private static final META_ALT_RELEASED:J = 0x2000000000000L

.field private static final META_ALT_USED:J = 0x200000000L

.field public static final META_CAP_LOCKED:I = 0x100

.field private static final META_CAP_PRESSED:J = 0x10000000000L

.field private static final META_CAP_RELEASED:J = 0x1000000000000L

.field private static final META_CAP_USED:J = 0x100000000L

.field public static final META_SELECTING:I = 0x800

.field private static final META_SHIFT_MASK:J = 0x1010100000101L

.field public static final META_SHIFT_ON:I = 0x1

.field public static final META_SYM_LOCKED:I = 0x400

.field private static final META_SYM_MASK:J = 0x4040400000404L

.field public static final META_SYM_ON:I = 0x4

.field private static final META_SYM_PRESSED:J = 0x40000000000L

.field private static final META_SYM_RELEASED:J = 0x4000000000000L

.field private static final META_SYM_USED:J = 0x400000000L

.field private static final PRESSED:I = 0x1000011

.field private static final RELEASED:I = 0x2000011

.field private static final SELECTING:Ljava/lang/Object; = null

.field private static final SYM:Ljava/lang/Object; = null

.field private static final USED:I = 0x3000011


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 133
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    #@2
    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    #@5
    sput-object v0, Landroid/text/method/MetaKeyKeyListener;->CAP:Ljava/lang/Object;

    #@7
    .line 134
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    #@9
    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    #@c
    sput-object v0, Landroid/text/method/MetaKeyKeyListener;->ALT:Ljava/lang/Object;

    #@e
    .line 135
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    #@10
    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    #@13
    sput-object v0, Landroid/text/method/MetaKeyKeyListener;->SYM:Ljava/lang/Object;

    #@15
    .line 136
    new-instance v0, Landroid/text/NoCopySpan$Concrete;

    #@17
    invoke-direct {v0}, Landroid/text/NoCopySpan$Concrete;-><init>()V

    #@1a
    sput-object v0, Landroid/text/method/MetaKeyKeyListener;->SELECTING:Ljava/lang/Object;

    #@1c
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static adjust(Landroid/text/Spannable;Ljava/lang/Object;)V
    .registers 5
    .parameter "content"
    .parameter "what"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 237
    invoke-interface {p0, p1}, Landroid/text/Spannable;->getSpanFlags(Ljava/lang/Object;)I

    #@4
    move-result v0

    #@5
    .line 239
    .local v0, current:I
    const v1, 0x1000011

    #@8
    if-ne v0, v1, :cond_11

    #@a
    .line 240
    const v1, 0x3000011

    #@d
    invoke-interface {p0, p1, v2, v2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@10
    .line 243
    :cond_10
    :goto_10
    return-void

    #@11
    .line 241
    :cond_11
    const v1, 0x2000011

    #@14
    if-ne v0, v1, :cond_10

    #@16
    .line 242
    invoke-interface {p0, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@19
    goto :goto_10
.end method

.method public static adjustMetaAfterKeypress(J)J
    .registers 12
    .parameter "state"

    #@0
    .prologue
    const-wide v2, -0x1010100000102L

    #@5
    const-wide v8, -0x2020200000203L

    #@a
    const-wide v6, -0x4040400000405L

    #@f
    const-wide/16 v4, 0x0

    #@11
    .line 459
    const-wide v0, 0x10000000000L

    #@16
    and-long/2addr v0, p0

    #@17
    cmp-long v0, v0, v4

    #@19
    if-eqz v0, :cond_54

    #@1b
    .line 460
    and-long v0, p0, v2

    #@1d
    const-wide/16 v2, 0x1

    #@1f
    or-long/2addr v0, v2

    #@20
    const-wide v2, 0x100000000L

    #@25
    or-long p0, v0, v2

    #@27
    .line 465
    :cond_27
    :goto_27
    const-wide v0, 0x20000000000L

    #@2c
    and-long/2addr v0, p0

    #@2d
    cmp-long v0, v0, v4

    #@2f
    if-eqz v0, :cond_5d

    #@31
    .line 466
    and-long v0, p0, v8

    #@33
    const-wide/16 v2, 0x2

    #@35
    or-long/2addr v0, v2

    #@36
    const-wide v2, 0x200000000L

    #@3b
    or-long p0, v0, v2

    #@3d
    .line 471
    :cond_3d
    :goto_3d
    const-wide v0, 0x40000000000L

    #@42
    and-long/2addr v0, p0

    #@43
    cmp-long v0, v0, v4

    #@45
    if-eqz v0, :cond_66

    #@47
    .line 472
    and-long v0, p0, v6

    #@49
    const-wide/16 v2, 0x4

    #@4b
    or-long/2addr v0, v2

    #@4c
    const-wide v2, 0x400000000L

    #@51
    or-long p0, v0, v2

    #@53
    .line 476
    :cond_53
    :goto_53
    return-wide p0

    #@54
    .line 461
    :cond_54
    const-wide/high16 v0, 0x1

    #@56
    and-long/2addr v0, p0

    #@57
    cmp-long v0, v0, v4

    #@59
    if-eqz v0, :cond_27

    #@5b
    .line 462
    and-long/2addr p0, v2

    #@5c
    goto :goto_27

    #@5d
    .line 467
    :cond_5d
    const-wide/high16 v0, 0x2

    #@5f
    and-long/2addr v0, p0

    #@60
    cmp-long v0, v0, v4

    #@62
    if-eqz v0, :cond_3d

    #@64
    .line 468
    and-long/2addr p0, v8

    #@65
    goto :goto_3d

    #@66
    .line 473
    :cond_66
    const-wide/high16 v0, 0x4

    #@68
    and-long/2addr v0, p0

    #@69
    cmp-long v0, v0, v4

    #@6b
    if-eqz v0, :cond_53

    #@6d
    .line 474
    and-long/2addr p0, v6

    #@6e
    goto :goto_53
.end method

.method public static adjustMetaAfterKeypress(Landroid/text/Spannable;)V
    .registers 2
    .parameter "content"

    #@0
    .prologue
    .line 214
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->CAP:Ljava/lang/Object;

    #@2
    invoke-static {p0, v0}, Landroid/text/method/MetaKeyKeyListener;->adjust(Landroid/text/Spannable;Ljava/lang/Object;)V

    #@5
    .line 215
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->ALT:Ljava/lang/Object;

    #@7
    invoke-static {p0, v0}, Landroid/text/method/MetaKeyKeyListener;->adjust(Landroid/text/Spannable;Ljava/lang/Object;)V

    #@a
    .line 216
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SYM:Ljava/lang/Object;

    #@c
    invoke-static {p0, v0}, Landroid/text/method/MetaKeyKeyListener;->adjust(Landroid/text/Spannable;Ljava/lang/Object;)V

    #@f
    .line 217
    return-void
.end method

.method public static clearMetaKeyState(Landroid/text/Editable;I)V
    .registers 3
    .parameter "content"
    .parameter "states"

    #@0
    .prologue
    .line 363
    and-int/lit8 v0, p1, 0x1

    #@2
    if-eqz v0, :cond_9

    #@4
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->CAP:Ljava/lang/Object;

    #@6
    invoke-interface {p0, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@9
    .line 364
    :cond_9
    and-int/lit8 v0, p1, 0x2

    #@b
    if-eqz v0, :cond_12

    #@d
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->ALT:Ljava/lang/Object;

    #@f
    invoke-interface {p0, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@12
    .line 365
    :cond_12
    and-int/lit8 v0, p1, 0x4

    #@14
    if-eqz v0, :cond_1b

    #@16
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SYM:Ljava/lang/Object;

    #@18
    invoke-interface {p0, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@1b
    .line 366
    :cond_1b
    and-int/lit16 v0, p1, 0x800

    #@1d
    if-eqz v0, :cond_24

    #@1f
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SELECTING:Ljava/lang/Object;

    #@21
    invoke-interface {p0, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@24
    .line 367
    :cond_24
    return-void
.end method

.method private static getActive(Ljava/lang/CharSequence;Ljava/lang/Object;II)I
    .registers 8
    .parameter "text"
    .parameter "meta"
    .parameter "on"
    .parameter "lock"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 192
    instance-of v3, p0, Landroid/text/Spanned;

    #@3
    if-nez v3, :cond_7

    #@5
    move p3, v2

    #@6
    .line 204
    .end local p3
    :cond_6
    :goto_6
    return p3

    #@7
    .restart local p3
    :cond_7
    move-object v1, p0

    #@8
    .line 196
    check-cast v1, Landroid/text/Spanned;

    #@a
    .line 197
    .local v1, sp:Landroid/text/Spanned;
    invoke-interface {v1, p1}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    #@d
    move-result v0

    #@e
    .line 199
    .local v0, flag:I
    const v3, 0x4000011

    #@11
    if-eq v0, v3, :cond_6

    #@13
    .line 201
    if-eqz v0, :cond_17

    #@15
    move p3, p2

    #@16
    .line 202
    goto :goto_6

    #@17
    :cond_17
    move p3, v2

    #@18
    .line 204
    goto :goto_6
.end method

.method public static final getMetaState(J)I
    .registers 7
    .parameter "state"

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 399
    const/4 v0, 0x0

    #@3
    .line 401
    .local v0, result:I
    const-wide/16 v1, 0x100

    #@5
    and-long/2addr v1, p0

    #@6
    cmp-long v1, v1, v3

    #@8
    if-eqz v1, :cond_1f

    #@a
    .line 402
    or-int/lit16 v0, v0, 0x100

    #@c
    .line 407
    :cond_c
    :goto_c
    const-wide/16 v1, 0x200

    #@e
    and-long/2addr v1, p0

    #@f
    cmp-long v1, v1, v3

    #@11
    if-eqz v1, :cond_29

    #@13
    .line 408
    or-int/lit16 v0, v0, 0x200

    #@15
    .line 413
    :cond_15
    :goto_15
    const-wide/16 v1, 0x400

    #@17
    and-long/2addr v1, p0

    #@18
    cmp-long v1, v1, v3

    #@1a
    if-eqz v1, :cond_33

    #@1c
    .line 414
    or-int/lit16 v0, v0, 0x400

    #@1e
    .line 419
    :cond_1e
    :goto_1e
    return v0

    #@1f
    .line 403
    :cond_1f
    const-wide/16 v1, 0x1

    #@21
    and-long/2addr v1, p0

    #@22
    cmp-long v1, v1, v3

    #@24
    if-eqz v1, :cond_c

    #@26
    .line 404
    or-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_c

    #@29
    .line 409
    :cond_29
    const-wide/16 v1, 0x2

    #@2b
    and-long/2addr v1, p0

    #@2c
    cmp-long v1, v1, v3

    #@2e
    if-eqz v1, :cond_15

    #@30
    .line 410
    or-int/lit8 v0, v0, 0x2

    #@32
    goto :goto_15

    #@33
    .line 415
    :cond_33
    const-wide/16 v1, 0x4

    #@35
    and-long/2addr v1, p0

    #@36
    cmp-long v1, v1, v3

    #@38
    if-eqz v1, :cond_1e

    #@3a
    .line 416
    or-int/lit8 v0, v0, 0x4

    #@3c
    goto :goto_1e
.end method

.method public static final getMetaState(JI)I
    .registers 10
    .parameter "state"
    .parameter "meta"

    #@0
    .prologue
    const/4 v0, 0x2

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    const-wide/16 v5, 0x0

    #@5
    .line 431
    packed-switch p2, :pswitch_data_40

    #@8
    :pswitch_8
    move v0, v2

    #@9
    .line 448
    :cond_9
    :goto_9
    return v0

    #@a
    .line 433
    :pswitch_a
    const-wide/16 v3, 0x100

    #@c
    and-long/2addr v3, p0

    #@d
    cmp-long v3, v3, v5

    #@f
    if-nez v3, :cond_9

    #@11
    .line 434
    const-wide/16 v3, 0x1

    #@13
    and-long/2addr v3, p0

    #@14
    cmp-long v0, v3, v5

    #@16
    if-eqz v0, :cond_1a

    #@18
    move v0, v1

    #@19
    goto :goto_9

    #@1a
    :cond_1a
    move v0, v2

    #@1b
    .line 435
    goto :goto_9

    #@1c
    .line 438
    :pswitch_1c
    const-wide/16 v3, 0x200

    #@1e
    and-long/2addr v3, p0

    #@1f
    cmp-long v3, v3, v5

    #@21
    if-nez v3, :cond_9

    #@23
    .line 439
    const-wide/16 v3, 0x2

    #@25
    and-long/2addr v3, p0

    #@26
    cmp-long v0, v3, v5

    #@28
    if-eqz v0, :cond_2c

    #@2a
    move v0, v1

    #@2b
    goto :goto_9

    #@2c
    :cond_2c
    move v0, v2

    #@2d
    .line 440
    goto :goto_9

    #@2e
    .line 443
    :pswitch_2e
    const-wide/16 v3, 0x400

    #@30
    and-long/2addr v3, p0

    #@31
    cmp-long v3, v3, v5

    #@33
    if-nez v3, :cond_9

    #@35
    .line 444
    const-wide/16 v3, 0x4

    #@37
    and-long/2addr v3, p0

    #@38
    cmp-long v0, v3, v5

    #@3a
    if-eqz v0, :cond_3e

    #@3c
    move v0, v1

    #@3d
    goto :goto_9

    #@3e
    :cond_3e
    move v0, v2

    #@3f
    .line 445
    goto :goto_9

    #@40
    .line 431
    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_a
        :pswitch_1c
        :pswitch_8
        :pswitch_2e
    .end packed-switch
.end method

.method public static final getMetaState(Ljava/lang/CharSequence;)I
    .registers 6
    .parameter "text"

    #@0
    .prologue
    const/16 v4, 0x800

    #@2
    .line 157
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->CAP:Ljava/lang/Object;

    #@4
    const/4 v1, 0x1

    #@5
    const/16 v2, 0x100

    #@7
    invoke-static {p0, v0, v1, v2}, Landroid/text/method/MetaKeyKeyListener;->getActive(Ljava/lang/CharSequence;Ljava/lang/Object;II)I

    #@a
    move-result v0

    #@b
    sget-object v1, Landroid/text/method/MetaKeyKeyListener;->ALT:Ljava/lang/Object;

    #@d
    const/4 v2, 0x2

    #@e
    const/16 v3, 0x200

    #@10
    invoke-static {p0, v1, v2, v3}, Landroid/text/method/MetaKeyKeyListener;->getActive(Ljava/lang/CharSequence;Ljava/lang/Object;II)I

    #@13
    move-result v1

    #@14
    or-int/2addr v0, v1

    #@15
    sget-object v1, Landroid/text/method/MetaKeyKeyListener;->SYM:Ljava/lang/Object;

    #@17
    const/4 v2, 0x4

    #@18
    const/16 v3, 0x400

    #@1a
    invoke-static {p0, v1, v2, v3}, Landroid/text/method/MetaKeyKeyListener;->getActive(Ljava/lang/CharSequence;Ljava/lang/Object;II)I

    #@1d
    move-result v1

    #@1e
    or-int/2addr v0, v1

    #@1f
    sget-object v1, Landroid/text/method/MetaKeyKeyListener;->SELECTING:Ljava/lang/Object;

    #@21
    invoke-static {p0, v1, v4, v4}, Landroid/text/method/MetaKeyKeyListener;->getActive(Ljava/lang/CharSequence;Ljava/lang/Object;II)I

    #@24
    move-result v1

    #@25
    or-int/2addr v0, v1

    #@26
    return v0
.end method

.method public static final getMetaState(Ljava/lang/CharSequence;I)I
    .registers 5
    .parameter "text"
    .parameter "meta"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v1, 0x1

    #@2
    .line 172
    sparse-switch p1, :sswitch_data_24

    #@5
    .line 186
    const/4 v0, 0x0

    #@6
    :goto_6
    return v0

    #@7
    .line 174
    :sswitch_7
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->CAP:Ljava/lang/Object;

    #@9
    invoke-static {p0, v0, v1, v2}, Landroid/text/method/MetaKeyKeyListener;->getActive(Ljava/lang/CharSequence;Ljava/lang/Object;II)I

    #@c
    move-result v0

    #@d
    goto :goto_6

    #@e
    .line 177
    :sswitch_e
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->ALT:Ljava/lang/Object;

    #@10
    invoke-static {p0, v0, v1, v2}, Landroid/text/method/MetaKeyKeyListener;->getActive(Ljava/lang/CharSequence;Ljava/lang/Object;II)I

    #@13
    move-result v0

    #@14
    goto :goto_6

    #@15
    .line 180
    :sswitch_15
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SYM:Ljava/lang/Object;

    #@17
    invoke-static {p0, v0, v1, v2}, Landroid/text/method/MetaKeyKeyListener;->getActive(Ljava/lang/CharSequence;Ljava/lang/Object;II)I

    #@1a
    move-result v0

    #@1b
    goto :goto_6

    #@1c
    .line 183
    :sswitch_1c
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SELECTING:Ljava/lang/Object;

    #@1e
    invoke-static {p0, v0, v1, v2}, Landroid/text/method/MetaKeyKeyListener;->getActive(Ljava/lang/CharSequence;Ljava/lang/Object;II)I

    #@21
    move-result v0

    #@22
    goto :goto_6

    #@23
    .line 172
    nop

    #@24
    :sswitch_data_24
    .sparse-switch
        0x1 -> :sswitch_7
        0x2 -> :sswitch_e
        0x4 -> :sswitch_15
        0x800 -> :sswitch_1c
    .end sparse-switch
.end method

.method public static handleKeyDown(JILandroid/view/KeyEvent;)J
    .registers 17
    .parameter "state"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 483
    const/16 v0, 0x3b

    #@2
    if-eq p2, v0, :cond_8

    #@4
    const/16 v0, 0x3c

    #@6
    if-ne p2, v0, :cond_22

    #@8
    .line 484
    :cond_8
    const/4 v2, 0x1

    #@9
    const-wide v3, 0x1010100000101L

    #@e
    const-wide/16 v5, 0x100

    #@10
    const-wide v7, 0x10000000000L

    #@15
    const-wide/high16 v9, 0x1

    #@17
    const-wide v11, 0x100000000L

    #@1c
    move-wide v0, p0

    #@1d
    invoke-static/range {v0 .. v12}, Landroid/text/method/MetaKeyKeyListener;->press(JIJJJJJ)J

    #@20
    move-result-wide p0

    #@21
    .line 498
    .end local p0
    :cond_21
    :goto_21
    return-wide p0

    #@22
    .line 488
    .restart local p0
    :cond_22
    const/16 v0, 0x39

    #@24
    if-eq p2, v0, :cond_2e

    #@26
    const/16 v0, 0x3a

    #@28
    if-eq p2, v0, :cond_2e

    #@2a
    const/16 v0, 0x4e

    #@2c
    if-ne p2, v0, :cond_48

    #@2e
    .line 490
    :cond_2e
    const/4 v2, 0x2

    #@2f
    const-wide v3, 0x2020200000202L

    #@34
    const-wide/16 v5, 0x200

    #@36
    const-wide v7, 0x20000000000L

    #@3b
    const-wide/high16 v9, 0x2

    #@3d
    const-wide v11, 0x200000000L

    #@42
    move-wide v0, p0

    #@43
    invoke-static/range {v0 .. v12}, Landroid/text/method/MetaKeyKeyListener;->press(JIJJJJJ)J

    #@46
    move-result-wide p0

    #@47
    goto :goto_21

    #@48
    .line 494
    :cond_48
    const/16 v0, 0x3f

    #@4a
    if-ne p2, v0, :cond_21

    #@4c
    .line 495
    const/4 v2, 0x4

    #@4d
    const-wide v3, 0x4040400000404L

    #@52
    const-wide/16 v5, 0x400

    #@54
    const-wide v7, 0x40000000000L

    #@59
    const-wide/high16 v9, 0x4

    #@5b
    const-wide v11, 0x400000000L

    #@60
    move-wide v0, p0

    #@61
    invoke-static/range {v0 .. v12}, Landroid/text/method/MetaKeyKeyListener;->press(JIJJJJJ)J

    #@64
    move-result-wide p0

    #@65
    goto :goto_21
.end method

.method public static handleKeyUp(JILandroid/view/KeyEvent;)J
    .registers 16
    .parameter "state"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 521
    const/16 v0, 0x3b

    #@2
    if-eq p2, v0, :cond_8

    #@4
    const/16 v0, 0x3c

    #@6
    if-ne p2, v0, :cond_21

    #@8
    .line 522
    :cond_8
    const/4 v2, 0x1

    #@9
    const-wide v3, 0x1010100000101L

    #@e
    const-wide v5, 0x10000000000L

    #@13
    const-wide/high16 v7, 0x1

    #@15
    const-wide v9, 0x100000000L

    #@1a
    move-wide v0, p0

    #@1b
    move-object v11, p3

    #@1c
    invoke-static/range {v0 .. v11}, Landroid/text/method/MetaKeyKeyListener;->release(JIJJJJLandroid/view/KeyEvent;)J

    #@1f
    move-result-wide p0

    #@20
    .line 536
    .end local p0
    :cond_20
    :goto_20
    return-wide p0

    #@21
    .line 526
    .restart local p0
    :cond_21
    const/16 v0, 0x39

    #@23
    if-eq p2, v0, :cond_2d

    #@25
    const/16 v0, 0x3a

    #@27
    if-eq p2, v0, :cond_2d

    #@29
    const/16 v0, 0x4e

    #@2b
    if-ne p2, v0, :cond_46

    #@2d
    .line 528
    :cond_2d
    const/4 v2, 0x2

    #@2e
    const-wide v3, 0x2020200000202L

    #@33
    const-wide v5, 0x20000000000L

    #@38
    const-wide/high16 v7, 0x2

    #@3a
    const-wide v9, 0x200000000L

    #@3f
    move-wide v0, p0

    #@40
    move-object v11, p3

    #@41
    invoke-static/range {v0 .. v11}, Landroid/text/method/MetaKeyKeyListener;->release(JIJJJJLandroid/view/KeyEvent;)J

    #@44
    move-result-wide p0

    #@45
    goto :goto_20

    #@46
    .line 532
    :cond_46
    const/16 v0, 0x3f

    #@48
    if-ne p2, v0, :cond_20

    #@4a
    .line 533
    const/4 v2, 0x4

    #@4b
    const-wide v3, 0x4040400000404L

    #@50
    const-wide v5, 0x40000000000L

    #@55
    const-wide/high16 v7, 0x4

    #@57
    const-wide v9, 0x400000000L

    #@5c
    move-wide v0, p0

    #@5d
    move-object v11, p3

    #@5e
    invoke-static/range {v0 .. v11}, Landroid/text/method/MetaKeyKeyListener;->release(JIJJJJLandroid/view/KeyEvent;)J

    #@61
    move-result-wide p0

    #@62
    goto :goto_20
.end method

.method public static isMetaTracker(Ljava/lang/CharSequence;Ljava/lang/Object;)Z
    .registers 3
    .parameter "text"
    .parameter "what"

    #@0
    .prologue
    .line 224
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->CAP:Ljava/lang/Object;

    #@2
    if-eq p1, v0, :cond_10

    #@4
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->ALT:Ljava/lang/Object;

    #@6
    if-eq p1, v0, :cond_10

    #@8
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SYM:Ljava/lang/Object;

    #@a
    if-eq p1, v0, :cond_10

    #@c
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SELECTING:Ljava/lang/Object;

    #@e
    if-ne p1, v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public static isSelectingMetaTracker(Ljava/lang/CharSequence;Ljava/lang/Object;)Z
    .registers 3
    .parameter "text"
    .parameter "what"

    #@0
    .prologue
    .line 233
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SELECTING:Ljava/lang/Object;

    #@2
    if-ne p1, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private static press(JIJJJJJ)J
    .registers 17
    .parameter "state"
    .parameter "what"
    .parameter "mask"
    .parameter "locked"
    .parameter "pressed"
    .parameter "released"
    .parameter "used"

    #@0
    .prologue
    .line 503
    and-long v0, p0, p7

    #@2
    const-wide/16 v2, 0x0

    #@4
    cmp-long v0, v0, v2

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 514
    :cond_8
    :goto_8
    return-wide p0

    #@9
    .line 505
    :cond_9
    and-long v0, p0, p9

    #@b
    const-wide/16 v2, 0x0

    #@d
    cmp-long v0, v0, v2

    #@f
    if-eqz v0, :cond_1a

    #@11
    .line 506
    const-wide/16 v0, -0x1

    #@13
    xor-long/2addr v0, p3

    #@14
    and-long/2addr v0, p0

    #@15
    int-to-long v2, p2

    #@16
    or-long/2addr v0, v2

    #@17
    or-long p0, v0, p5

    #@19
    goto :goto_8

    #@1a
    .line 507
    :cond_1a
    and-long v0, p0, p11

    #@1c
    const-wide/16 v2, 0x0

    #@1e
    cmp-long v0, v0, v2

    #@20
    if-nez v0, :cond_8

    #@22
    .line 509
    and-long v0, p0, p5

    #@24
    const-wide/16 v2, 0x0

    #@26
    cmp-long v0, v0, v2

    #@28
    if-eqz v0, :cond_2f

    #@2a
    .line 510
    const-wide/16 v0, -0x1

    #@2c
    xor-long/2addr v0, p3

    #@2d
    and-long/2addr p0, v0

    #@2e
    goto :goto_8

    #@2f
    .line 512
    :cond_2f
    int-to-long v0, p2

    #@30
    or-long/2addr v0, p7

    #@31
    or-long/2addr p0, v0

    #@32
    goto :goto_8
.end method

.method private press(Landroid/text/Editable;Ljava/lang/Object;)V
    .registers 8
    .parameter "content"
    .parameter "what"

    #@0
    .prologue
    const v4, 0x4000011

    #@3
    const v3, 0x1000011

    #@6
    const/4 v2, 0x0

    #@7
    .line 287
    invoke-interface {p1, p2}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    #@a
    move-result v0

    #@b
    .line 289
    .local v0, state:I
    if-ne v0, v3, :cond_e

    #@d
    .line 299
    :cond_d
    :goto_d
    return-void

    #@e
    .line 291
    :cond_e
    const v1, 0x2000011

    #@11
    if-ne v0, v1, :cond_17

    #@13
    .line 292
    invoke-interface {p1, p2, v2, v2, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@16
    goto :goto_d

    #@17
    .line 293
    :cond_17
    const v1, 0x3000011

    #@1a
    if-eq v0, v1, :cond_d

    #@1c
    .line 295
    if-ne v0, v4, :cond_22

    #@1e
    .line 296
    invoke-interface {p1, p2}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@21
    goto :goto_d

    #@22
    .line 298
    :cond_22
    invoke-interface {p1, p2, v2, v2, v3}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@25
    goto :goto_d
.end method

.method private static release(JIJJJJLandroid/view/KeyEvent;)J
    .registers 16
    .parameter "state"
    .parameter "what"
    .parameter "mask"
    .parameter "pressed"
    .parameter "released"
    .parameter "used"
    .parameter "event"

    #@0
    .prologue
    .line 541
    invoke-virtual {p11}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getModifierBehavior()I

    #@7
    move-result v0

    #@8
    packed-switch v0, :pswitch_data_2a

    #@b
    .line 551
    const-wide/16 v0, -0x1

    #@d
    xor-long/2addr v0, p3

    #@e
    and-long/2addr p0, v0

    #@f
    .line 554
    :cond_f
    :goto_f
    return-wide p0

    #@10
    .line 543
    :pswitch_10
    and-long v0, p0, p9

    #@12
    const-wide/16 v2, 0x0

    #@14
    cmp-long v0, v0, v2

    #@16
    if-eqz v0, :cond_1d

    #@18
    .line 544
    const-wide/16 v0, -0x1

    #@1a
    xor-long/2addr v0, p3

    #@1b
    and-long/2addr p0, v0

    #@1c
    goto :goto_f

    #@1d
    .line 545
    :cond_1d
    and-long v0, p0, p5

    #@1f
    const-wide/16 v2, 0x0

    #@21
    cmp-long v0, v0, v2

    #@23
    if-eqz v0, :cond_f

    #@25
    .line 546
    int-to-long v0, p2

    #@26
    or-long/2addr v0, p7

    #@27
    or-long/2addr p0, v0

    #@28
    goto :goto_f

    #@29
    .line 541
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_10
    .end packed-switch
.end method

.method private release(Landroid/text/Editable;Ljava/lang/Object;Landroid/view/KeyEvent;)V
    .registers 7
    .parameter "content"
    .parameter "what"
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 342
    invoke-interface {p1, p2}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    #@4
    move-result v0

    #@5
    .line 344
    .local v0, current:I
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1}, Landroid/view/KeyCharacterMap;->getModifierBehavior()I

    #@c
    move-result v1

    #@d
    packed-switch v1, :pswitch_data_2a

    #@10
    .line 353
    invoke-interface {p1, p2}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@13
    .line 356
    :cond_13
    :goto_13
    return-void

    #@14
    .line 346
    :pswitch_14
    const v1, 0x3000011

    #@17
    if-ne v0, v1, :cond_1d

    #@19
    .line 347
    invoke-interface {p1, p2}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@1c
    goto :goto_13

    #@1d
    .line 348
    :cond_1d
    const v1, 0x1000011

    #@20
    if-ne v0, v1, :cond_13

    #@22
    .line 349
    const v1, 0x2000011

    #@25
    invoke-interface {p1, p2, v2, v2, v1}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@28
    goto :goto_13

    #@29
    .line 344
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_14
    .end packed-switch
.end method

.method private static resetLock(Landroid/text/Spannable;Ljava/lang/Object;)V
    .registers 4
    .parameter "content"
    .parameter "what"

    #@0
    .prologue
    .line 257
    invoke-interface {p0, p1}, Landroid/text/Spannable;->getSpanFlags(Ljava/lang/Object;)I

    #@3
    move-result v0

    #@4
    .line 259
    .local v0, current:I
    const v1, 0x4000011

    #@7
    if-ne v0, v1, :cond_c

    #@9
    .line 260
    invoke-interface {p0, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@c
    .line 261
    :cond_c
    return-void
.end method

.method public static resetLockedMeta(J)J
    .registers 6
    .parameter "state"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 374
    const-wide/16 v0, 0x100

    #@4
    and-long/2addr v0, p0

    #@5
    cmp-long v0, v0, v2

    #@7
    if-eqz v0, :cond_f

    #@9
    .line 375
    const-wide v0, -0x1010100000102L

    #@e
    and-long/2addr p0, v0

    #@f
    .line 377
    :cond_f
    const-wide/16 v0, 0x200

    #@11
    and-long/2addr v0, p0

    #@12
    cmp-long v0, v0, v2

    #@14
    if-eqz v0, :cond_1c

    #@16
    .line 378
    const-wide v0, -0x2020200000203L

    #@1b
    and-long/2addr p0, v0

    #@1c
    .line 380
    :cond_1c
    const-wide/16 v0, 0x400

    #@1e
    and-long/2addr v0, p0

    #@1f
    cmp-long v0, v0, v2

    #@21
    if-eqz v0, :cond_29

    #@23
    .line 381
    const-wide v0, -0x4040400000405L

    #@28
    and-long/2addr p0, v0

    #@29
    .line 383
    :cond_29
    return-wide p0
.end method

.method protected static resetLockedMeta(Landroid/text/Spannable;)V
    .registers 2
    .parameter "content"

    #@0
    .prologue
    .line 250
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->CAP:Ljava/lang/Object;

    #@2
    invoke-static {p0, v0}, Landroid/text/method/MetaKeyKeyListener;->resetLock(Landroid/text/Spannable;Ljava/lang/Object;)V

    #@5
    .line 251
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->ALT:Ljava/lang/Object;

    #@7
    invoke-static {p0, v0}, Landroid/text/method/MetaKeyKeyListener;->resetLock(Landroid/text/Spannable;Ljava/lang/Object;)V

    #@a
    .line 252
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SYM:Ljava/lang/Object;

    #@c
    invoke-static {p0, v0}, Landroid/text/method/MetaKeyKeyListener;->resetLock(Landroid/text/Spannable;Ljava/lang/Object;)V

    #@f
    .line 253
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SELECTING:Ljava/lang/Object;

    #@11
    invoke-static {p0, v0}, Landroid/text/method/MetaKeyKeyListener;->resetLock(Landroid/text/Spannable;Ljava/lang/Object;)V

    #@14
    .line 254
    return-void
.end method

.method public static resetMetaState(Landroid/text/Spannable;)V
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 142
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->CAP:Ljava/lang/Object;

    #@2
    invoke-interface {p0, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@5
    .line 143
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->ALT:Ljava/lang/Object;

    #@7
    invoke-interface {p0, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@a
    .line 144
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SYM:Ljava/lang/Object;

    #@c
    invoke-interface {p0, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@f
    .line 145
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SELECTING:Ljava/lang/Object;

    #@11
    invoke-interface {p0, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@14
    .line 146
    return-void
.end method

.method public static startSelecting(Landroid/view/View;Landroid/text/Spannable;)V
    .registers 5
    .parameter "view"
    .parameter "content"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 306
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SELECTING:Ljava/lang/Object;

    #@3
    const v1, 0x1000011

    #@6
    invoke-interface {p1, v0, v2, v2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@9
    .line 307
    return-void
.end method

.method public static stopSelecting(Landroid/view/View;Landroid/text/Spannable;)V
    .registers 3
    .parameter "view"
    .parameter "content"

    #@0
    .prologue
    .line 315
    sget-object v0, Landroid/text/method/MetaKeyKeyListener;->SELECTING:Ljava/lang/Object;

    #@2
    invoke-interface {p1, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@5
    .line 316
    return-void
.end method


# virtual methods
.method public clearMetaKeyState(JI)J
    .registers 8
    .parameter "state"
    .parameter "which"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 564
    and-int/lit8 v0, p3, 0x1

    #@4
    if-eqz v0, :cond_13

    #@6
    const-wide/16 v0, 0x100

    #@8
    and-long/2addr v0, p1

    #@9
    cmp-long v0, v0, v2

    #@b
    if-eqz v0, :cond_13

    #@d
    .line 565
    const-wide v0, -0x1010100000102L

    #@12
    and-long/2addr p1, v0

    #@13
    .line 567
    :cond_13
    and-int/lit8 v0, p3, 0x2

    #@15
    if-eqz v0, :cond_24

    #@17
    const-wide/16 v0, 0x200

    #@19
    and-long/2addr v0, p1

    #@1a
    cmp-long v0, v0, v2

    #@1c
    if-eqz v0, :cond_24

    #@1e
    .line 568
    const-wide v0, -0x2020200000203L

    #@23
    and-long/2addr p1, v0

    #@24
    .line 570
    :cond_24
    and-int/lit8 v0, p3, 0x4

    #@26
    if-eqz v0, :cond_35

    #@28
    const-wide/16 v0, 0x400

    #@2a
    and-long/2addr v0, p1

    #@2b
    cmp-long v0, v0, v2

    #@2d
    if-eqz v0, :cond_35

    #@2f
    .line 571
    const-wide v0, -0x4040400000405L

    #@34
    and-long/2addr p1, v0

    #@35
    .line 573
    :cond_35
    return-wide p1
.end method

.method public clearMetaKeyState(Landroid/view/View;Landroid/text/Editable;I)V
    .registers 4
    .parameter "view"
    .parameter "content"
    .parameter "states"

    #@0
    .prologue
    .line 359
    invoke-static {p2, p3}, Landroid/text/method/MetaKeyKeyListener;->clearMetaKeyState(Landroid/text/Editable;I)V

    #@3
    .line 360
    return-void
.end method

.method public onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "view"
    .parameter "content"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 267
    const/16 v1, 0x3b

    #@3
    if-eq p3, v1, :cond_9

    #@5
    const/16 v1, 0x3c

    #@7
    if-ne p3, v1, :cond_f

    #@9
    .line 268
    :cond_9
    sget-object v1, Landroid/text/method/MetaKeyKeyListener;->CAP:Ljava/lang/Object;

    #@b
    invoke-direct {p0, p2, v1}, Landroid/text/method/MetaKeyKeyListener;->press(Landroid/text/Editable;Ljava/lang/Object;)V

    #@e
    .line 283
    :goto_e
    return v0

    #@f
    .line 272
    :cond_f
    const/16 v1, 0x39

    #@11
    if-eq p3, v1, :cond_1b

    #@13
    const/16 v1, 0x3a

    #@15
    if-eq p3, v1, :cond_1b

    #@17
    const/16 v1, 0x4e

    #@19
    if-ne p3, v1, :cond_21

    #@1b
    .line 274
    :cond_1b
    sget-object v1, Landroid/text/method/MetaKeyKeyListener;->ALT:Ljava/lang/Object;

    #@1d
    invoke-direct {p0, p2, v1}, Landroid/text/method/MetaKeyKeyListener;->press(Landroid/text/Editable;Ljava/lang/Object;)V

    #@20
    goto :goto_e

    #@21
    .line 278
    :cond_21
    const/16 v1, 0x3f

    #@23
    if-ne p3, v1, :cond_2b

    #@25
    .line 279
    sget-object v1, Landroid/text/method/MetaKeyKeyListener;->SYM:Ljava/lang/Object;

    #@27
    invoke-direct {p0, p2, v1}, Landroid/text/method/MetaKeyKeyListener;->press(Landroid/text/Editable;Ljava/lang/Object;)V

    #@2a
    goto :goto_e

    #@2b
    .line 283
    :cond_2b
    const/4 v0, 0x0

    #@2c
    goto :goto_e
.end method

.method public onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "view"
    .parameter "content"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 322
    const/16 v1, 0x3b

    #@3
    if-eq p3, v1, :cond_9

    #@5
    const/16 v1, 0x3c

    #@7
    if-ne p3, v1, :cond_f

    #@9
    .line 323
    :cond_9
    sget-object v1, Landroid/text/method/MetaKeyKeyListener;->CAP:Ljava/lang/Object;

    #@b
    invoke-direct {p0, p2, v1, p4}, Landroid/text/method/MetaKeyKeyListener;->release(Landroid/text/Editable;Ljava/lang/Object;Landroid/view/KeyEvent;)V

    #@e
    .line 338
    :goto_e
    return v0

    #@f
    .line 327
    :cond_f
    const/16 v1, 0x39

    #@11
    if-eq p3, v1, :cond_1b

    #@13
    const/16 v1, 0x3a

    #@15
    if-eq p3, v1, :cond_1b

    #@17
    const/16 v1, 0x4e

    #@19
    if-ne p3, v1, :cond_21

    #@1b
    .line 329
    :cond_1b
    sget-object v1, Landroid/text/method/MetaKeyKeyListener;->ALT:Ljava/lang/Object;

    #@1d
    invoke-direct {p0, p2, v1, p4}, Landroid/text/method/MetaKeyKeyListener;->release(Landroid/text/Editable;Ljava/lang/Object;Landroid/view/KeyEvent;)V

    #@20
    goto :goto_e

    #@21
    .line 333
    :cond_21
    const/16 v1, 0x3f

    #@23
    if-ne p3, v1, :cond_2b

    #@25
    .line 334
    sget-object v1, Landroid/text/method/MetaKeyKeyListener;->SYM:Ljava/lang/Object;

    #@27
    invoke-direct {p0, p2, v1, p4}, Landroid/text/method/MetaKeyKeyListener;->release(Landroid/text/Editable;Ljava/lang/Object;Landroid/view/KeyEvent;)V

    #@2a
    goto :goto_e

    #@2b
    .line 338
    :cond_2b
    const/4 v0, 0x0

    #@2c
    goto :goto_e
.end method
