.class Landroid/text/HtmlToSpannedConverter;
.super Ljava/lang/Object;
.source "Html.java"

# interfaces
.implements Lorg/xml/sax/ContentHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/HtmlToSpannedConverter$1;,
        Landroid/text/HtmlToSpannedConverter$Header;,
        Landroid/text/HtmlToSpannedConverter$Href;,
        Landroid/text/HtmlToSpannedConverter$Font;,
        Landroid/text/HtmlToSpannedConverter$Sub;,
        Landroid/text/HtmlToSpannedConverter$Super;,
        Landroid/text/HtmlToSpannedConverter$Blockquote;,
        Landroid/text/HtmlToSpannedConverter$Monospace;,
        Landroid/text/HtmlToSpannedConverter$Small;,
        Landroid/text/HtmlToSpannedConverter$Big;,
        Landroid/text/HtmlToSpannedConverter$Underline;,
        Landroid/text/HtmlToSpannedConverter$Italic;,
        Landroid/text/HtmlToSpannedConverter$Bold;
    }
.end annotation


# static fields
.field private static COLORS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final HEADER_SIZES:[F


# instance fields
.field private mImageGetter:Landroid/text/Html$ImageGetter;

.field private mReader:Lorg/xml/sax/XMLReader;

.field private mSource:Ljava/lang/String;

.field private mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

.field private mTagHandler:Landroid/text/Html$TagHandler;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 419
    const/4 v0, 0x6

    #@1
    new-array v0, v0, [F

    #@3
    fill-array-data v0, :array_10

    #@6
    sput-object v0, Landroid/text/HtmlToSpannedConverter;->HEADER_SIZES:[F

    #@8
    .line 854
    invoke-static {}, Landroid/text/HtmlToSpannedConverter;->buildColorMap()Ljava/util/HashMap;

    #@b
    move-result-object v0

    #@c
    sput-object v0, Landroid/text/HtmlToSpannedConverter;->COLORS:Ljava/util/HashMap;

    #@e
    return-void

    #@f
    .line 419
    nop

    #@10
    :array_10
    .array-data 0x4
        0x0t 0x0t 0xc0t 0x3ft
        0x33t 0x33t 0xb3t 0x3ft
        0x66t 0x66t 0xa6t 0x3ft
        0x9at 0x99t 0x99t 0x3ft
        0xcdt 0xcct 0x8ct 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;Lorg/ccil/cowan/tagsoup/Parser;)V
    .registers 6
    .parameter "source"
    .parameter "imageGetter"
    .parameter "tagHandler"
    .parameter "parser"

    #@0
    .prologue
    .line 431
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 432
    iput-object p1, p0, Landroid/text/HtmlToSpannedConverter;->mSource:Ljava/lang/String;

    #@5
    .line 433
    new-instance v0, Landroid/text/SpannableStringBuilder;

    #@7
    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    #@a
    iput-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@c
    .line 434
    iput-object p2, p0, Landroid/text/HtmlToSpannedConverter;->mImageGetter:Landroid/text/Html$ImageGetter;

    #@e
    .line 435
    iput-object p3, p0, Landroid/text/HtmlToSpannedConverter;->mTagHandler:Landroid/text/Html$TagHandler;

    #@10
    .line 436
    iput-object p4, p0, Landroid/text/HtmlToSpannedConverter;->mReader:Lorg/xml/sax/XMLReader;

    #@12
    .line 437
    return-void
.end method

.method private static buildColorMap()Ljava/util/HashMap;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 857
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 858
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v1, "aqua"

    #@7
    const v2, 0xffff

    #@a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    .line 859
    const-string v1, "black"

    #@13
    const/4 v2, 0x0

    #@14
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 860
    const-string v1, "blue"

    #@1d
    const/16 v2, 0xff

    #@1f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@26
    .line 861
    const-string v1, "fuchsia"

    #@28
    const v2, 0xff00ff

    #@2b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    .line 862
    const-string v1, "green"

    #@34
    const v2, 0x8000

    #@37
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3e
    .line 863
    const-string v1, "grey"

    #@40
    const v2, 0x808080

    #@43
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4a
    .line 864
    const-string/jumbo v1, "lime"

    #@4d
    const v2, 0xff00

    #@50
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    .line 865
    const-string/jumbo v1, "maroon"

    #@5a
    const/high16 v2, 0x80

    #@5c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5f
    move-result-object v2

    #@60
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@63
    .line 866
    const-string/jumbo v1, "navy"

    #@66
    const/16 v2, 0x80

    #@68
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6f
    .line 867
    const-string/jumbo v1, "olive"

    #@72
    const v2, 0x808000

    #@75
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7c
    .line 868
    const-string/jumbo v1, "purple"

    #@7f
    const v2, 0x800080

    #@82
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@85
    move-result-object v2

    #@86
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@89
    .line 869
    const-string/jumbo v1, "red"

    #@8c
    const/high16 v2, 0xff

    #@8e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@91
    move-result-object v2

    #@92
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@95
    .line 870
    const-string/jumbo v1, "silver"

    #@98
    const v2, 0xc0c0c0

    #@9b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9e
    move-result-object v2

    #@9f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a2
    .line 871
    const-string/jumbo v1, "teal"

    #@a5
    const v2, 0x8080

    #@a8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ab
    move-result-object v2

    #@ac
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@af
    .line 872
    const-string/jumbo v1, "white"

    #@b2
    const v2, 0xffffff

    #@b5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b8
    move-result-object v2

    #@b9
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@bc
    .line 873
    const-string/jumbo v1, "yellow"

    #@bf
    const v2, 0xffff00

    #@c2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c5
    move-result-object v2

    #@c6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c9
    .line 874
    return-object v0
.end method

.method private static end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V
    .registers 7
    .parameter "text"
    .parameter "kind"
    .parameter "repl"

    #@0
    .prologue
    .line 618
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@3
    move-result v0

    #@4
    .line 619
    .local v0, len:I
    invoke-static {p0, p1}, Landroid/text/HtmlToSpannedConverter;->getLast(Landroid/text/Spanned;Ljava/lang/Class;)Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    .line 620
    .local v1, obj:Ljava/lang/Object;
    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    #@b
    move-result v2

    #@c
    .line 622
    .local v2, where:I
    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    #@f
    .line 624
    if-eq v2, v0, :cond_16

    #@11
    .line 625
    const/16 v3, 0x21

    #@13
    invoke-virtual {p0, p2, v2, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@16
    .line 628
    :cond_16
    return-void
.end method

.method private static endA(Landroid/text/SpannableStringBuilder;)V
    .registers 7
    .parameter "text"

    #@0
    .prologue
    .line 708
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@3
    move-result v1

    #@4
    .line 709
    .local v1, len:I
    const-class v4, Landroid/text/HtmlToSpannedConverter$Href;

    #@6
    invoke-static {p0, v4}, Landroid/text/HtmlToSpannedConverter;->getLast(Landroid/text/Spanned;Ljava/lang/Class;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 710
    .local v2, obj:Ljava/lang/Object;
    invoke-virtual {p0, v2}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    #@d
    move-result v3

    #@e
    .line 712
    .local v3, where:I
    invoke-virtual {p0, v2}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    #@11
    .line 714
    if-eq v3, v1, :cond_26

    #@13
    move-object v0, v2

    #@14
    .line 715
    check-cast v0, Landroid/text/HtmlToSpannedConverter$Href;

    #@16
    .line 717
    .local v0, h:Landroid/text/HtmlToSpannedConverter$Href;
    iget-object v4, v0, Landroid/text/HtmlToSpannedConverter$Href;->mHref:Ljava/lang/String;

    #@18
    if-eqz v4, :cond_26

    #@1a
    .line 718
    new-instance v4, Landroid/text/style/URLSpan;

    #@1c
    iget-object v5, v0, Landroid/text/HtmlToSpannedConverter$Href;->mHref:Ljava/lang/String;

    #@1e
    invoke-direct {v4, v5}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    #@21
    const/16 v5, 0x21

    #@23
    invoke-virtual {p0, v4, v3, v1, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@26
    .line 722
    .end local v0           #h:Landroid/text/HtmlToSpannedConverter$Href;
    :cond_26
    return-void
.end method

.method private static endFont(Landroid/text/SpannableStringBuilder;)V
    .registers 16
    .parameter "text"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    const/16 v14, 0x21

    #@4
    .line 663
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@7
    move-result v9

    #@8
    .line 664
    .local v9, len:I
    const-class v0, Landroid/text/HtmlToSpannedConverter$Font;

    #@a
    invoke-static {p0, v0}, Landroid/text/HtmlToSpannedConverter;->getLast(Landroid/text/Spanned;Ljava/lang/Class;)Ljava/lang/Object;

    #@d
    move-result-object v11

    #@e
    .line 665
    .local v11, obj:Ljava/lang/Object;
    invoke-virtual {p0, v11}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    #@11
    move-result v13

    #@12
    .line 667
    .local v13, where:I
    invoke-virtual {p0, v11}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    #@15
    .line 669
    if-eq v13, v9, :cond_5d

    #@17
    move-object v8, v11

    #@18
    .line 670
    check-cast v8, Landroid/text/HtmlToSpannedConverter$Font;

    #@1a
    .line 672
    .local v8, f:Landroid/text/HtmlToSpannedConverter$Font;
    iget-object v0, v8, Landroid/text/HtmlToSpannedConverter$Font;->mColor:Ljava/lang/String;

    #@1c
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_4f

    #@22
    .line 673
    iget-object v0, v8, Landroid/text/HtmlToSpannedConverter$Font;->mColor:Ljava/lang/String;

    #@24
    const-string v3, "@"

    #@26
    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_5e

    #@2c
    .line 674
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@2f
    move-result-object v12

    #@30
    .line 675
    .local v12, res:Landroid/content/res/Resources;
    iget-object v0, v8, Landroid/text/HtmlToSpannedConverter$Font;->mColor:Ljava/lang/String;

    #@32
    const/4 v3, 0x1

    #@33
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@36
    move-result-object v10

    #@37
    .line 676
    .local v10, name:Ljava/lang/String;
    const-string v0, "color"

    #@39
    const-string v3, "android"

    #@3b
    invoke-virtual {v12, v10, v0, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    move-result v7

    #@3f
    .line 677
    .local v7, colorRes:I
    if-eqz v7, :cond_4f

    #@41
    .line 678
    invoke-virtual {v12, v7}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@44
    move-result-object v4

    #@45
    .line 679
    .local v4, colors:Landroid/content/res/ColorStateList;
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    #@47
    move v3, v2

    #@48
    move-object v5, v1

    #@49
    invoke-direct/range {v0 .. v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    #@4c
    invoke-virtual {p0, v0, v13, v9, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@4f
    .line 693
    .end local v4           #colors:Landroid/content/res/ColorStateList;
    .end local v7           #colorRes:I
    .end local v10           #name:Ljava/lang/String;
    .end local v12           #res:Landroid/content/res/Resources;
    :cond_4f
    :goto_4f
    iget-object v0, v8, Landroid/text/HtmlToSpannedConverter$Font;->mFace:Ljava/lang/String;

    #@51
    if-eqz v0, :cond_5d

    #@53
    .line 694
    new-instance v0, Landroid/text/style/TypefaceSpan;

    #@55
    iget-object v1, v8, Landroid/text/HtmlToSpannedConverter$Font;->mFace:Ljava/lang/String;

    #@57
    invoke-direct {v0, v1}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    #@5a
    invoke-virtual {p0, v0, v13, v9, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@5d
    .line 698
    .end local v8           #f:Landroid/text/HtmlToSpannedConverter$Font;
    :cond_5d
    return-void

    #@5e
    .line 684
    .restart local v8       #f:Landroid/text/HtmlToSpannedConverter$Font;
    :cond_5e
    iget-object v0, v8, Landroid/text/HtmlToSpannedConverter$Font;->mColor:Ljava/lang/String;

    #@60
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->getHtmlColor(Ljava/lang/String;)I

    #@63
    move-result v6

    #@64
    .line 685
    .local v6, c:I
    const/4 v0, -0x1

    #@65
    if-eq v6, v0, :cond_4f

    #@67
    .line 686
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    #@69
    const/high16 v1, -0x100

    #@6b
    or-int/2addr v1, v6

    #@6c
    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    #@6f
    invoke-virtual {p0, v0, v13, v9, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@72
    goto :goto_4f
.end method

.method private static endHeader(Landroid/text/SpannableStringBuilder;)V
    .registers 9
    .parameter "text"

    #@0
    .prologue
    const/16 v7, 0x21

    #@2
    .line 725
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@5
    move-result v1

    #@6
    .line 726
    .local v1, len:I
    const-class v4, Landroid/text/HtmlToSpannedConverter$Header;

    #@8
    invoke-static {p0, v4}, Landroid/text/HtmlToSpannedConverter;->getLast(Landroid/text/Spanned;Ljava/lang/Class;)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    .line 728
    .local v2, obj:Ljava/lang/Object;
    invoke-virtual {p0, v2}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    #@f
    move-result v3

    #@10
    .line 730
    .local v3, where:I
    invoke-virtual {p0, v2}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    #@13
    .line 733
    :goto_13
    if-le v1, v3, :cond_22

    #@15
    add-int/lit8 v4, v1, -0x1

    #@17
    invoke-virtual {p0, v4}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@1a
    move-result v4

    #@1b
    const/16 v5, 0xa

    #@1d
    if-ne v4, v5, :cond_22

    #@1f
    .line 734
    add-int/lit8 v1, v1, -0x1

    #@21
    goto :goto_13

    #@22
    .line 737
    :cond_22
    if-eq v3, v1, :cond_40

    #@24
    move-object v0, v2

    #@25
    .line 738
    check-cast v0, Landroid/text/HtmlToSpannedConverter$Header;

    #@27
    .line 740
    .local v0, h:Landroid/text/HtmlToSpannedConverter$Header;
    new-instance v4, Landroid/text/style/RelativeSizeSpan;

    #@29
    sget-object v5, Landroid/text/HtmlToSpannedConverter;->HEADER_SIZES:[F

    #@2b
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter$Header;->access$900(Landroid/text/HtmlToSpannedConverter$Header;)I

    #@2e
    move-result v6

    #@2f
    aget v5, v5, v6

    #@31
    invoke-direct {v4, v5}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    #@34
    invoke-virtual {p0, v4, v3, v1, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@37
    .line 742
    new-instance v4, Landroid/text/style/StyleSpan;

    #@39
    const/4 v5, 0x1

    #@3a
    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    #@3d
    invoke-virtual {p0, v4, v3, v1, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@40
    .line 745
    .end local v0           #h:Landroid/text/HtmlToSpannedConverter$Header;
    :cond_40
    return-void
.end method

.method private static getHtmlColor(Ljava/lang/String;)I
    .registers 6
    .parameter "color"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 884
    sget-object v3, Landroid/text/HtmlToSpannedConverter;->COLORS:Ljava/util/HashMap;

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@6
    move-result-object v4

    #@7
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Ljava/lang/Integer;

    #@d
    .line 885
    .local v0, i:Ljava/lang/Integer;
    if-eqz v0, :cond_14

    #@f
    .line 886
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@12
    move-result v2

    #@13
    .line 891
    :goto_13
    return v2

    #@14
    .line 889
    :cond_14
    const/4 v3, -0x1

    #@15
    :try_start_15
    invoke-static {p0, v3}, Lcom/android/internal/util/XmlUtils;->convertValueToInt(Ljava/lang/CharSequence;I)I
    :try_end_18
    .catch Ljava/lang/NumberFormatException; {:try_start_15 .. :try_end_18} :catch_1a

    #@18
    move-result v2

    #@19
    goto :goto_13

    #@1a
    .line 890
    :catch_1a
    move-exception v1

    #@1b
    .line 891
    .local v1, nfe:Ljava/lang/NumberFormatException;
    goto :goto_13
.end method

.method private static getLast(Landroid/text/Spanned;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 5
    .parameter "text"
    .parameter "kind"

    #@0
    .prologue
    .line 602
    const/4 v1, 0x0

    #@1
    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    #@4
    move-result v2

    #@5
    invoke-interface {p0, v1, v2, p1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    .line 604
    .local v0, objs:[Ljava/lang/Object;
    array-length v1, v0

    #@a
    if-nez v1, :cond_e

    #@c
    .line 605
    const/4 v1, 0x0

    #@d
    .line 607
    :goto_d
    return-object v1

    #@e
    :cond_e
    array-length v1, v0

    #@f
    add-int/lit8 v1, v1, -0x1

    #@11
    aget-object v1, v0, v1

    #@13
    goto :goto_d
.end method

.method private static handleBr(Landroid/text/SpannableStringBuilder;)V
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 594
    const-string v0, "\n"

    #@2
    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@5
    .line 595
    return-void
.end method

.method private handleEndTag(Ljava/lang/String;)V
    .registers 8
    .parameter "tag"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x2

    #@3
    .line 528
    const-string v0, "br"

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_11

    #@b
    .line 529
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@d
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->handleBr(Landroid/text/SpannableStringBuilder;)V

    #@10
    .line 574
    :cond_10
    :goto_10
    return-void

    #@11
    .line 530
    :cond_11
    const-string/jumbo v0, "p"

    #@14
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_20

    #@1a
    .line 531
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@1c
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    #@1f
    goto :goto_10

    #@20
    .line 532
    :cond_20
    const-string v0, "div"

    #@22
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_2e

    #@28
    .line 533
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@2a
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    #@2d
    goto :goto_10

    #@2e
    .line 534
    :cond_2e
    const-string/jumbo v0, "strong"

    #@31
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_44

    #@37
    .line 535
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@39
    const-class v1, Landroid/text/HtmlToSpannedConverter$Bold;

    #@3b
    new-instance v2, Landroid/text/style/StyleSpan;

    #@3d
    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    #@40
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@43
    goto :goto_10

    #@44
    .line 536
    :cond_44
    const-string v0, "b"

    #@46
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@49
    move-result v0

    #@4a
    if-eqz v0, :cond_59

    #@4c
    .line 537
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@4e
    const-class v1, Landroid/text/HtmlToSpannedConverter$Bold;

    #@50
    new-instance v2, Landroid/text/style/StyleSpan;

    #@52
    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    #@55
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@58
    goto :goto_10

    #@59
    .line 538
    :cond_59
    const-string v0, "em"

    #@5b
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5e
    move-result v0

    #@5f
    if-eqz v0, :cond_6e

    #@61
    .line 539
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@63
    const-class v1, Landroid/text/HtmlToSpannedConverter$Italic;

    #@65
    new-instance v2, Landroid/text/style/StyleSpan;

    #@67
    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    #@6a
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@6d
    goto :goto_10

    #@6e
    .line 540
    :cond_6e
    const-string v0, "cite"

    #@70
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@73
    move-result v0

    #@74
    if-eqz v0, :cond_83

    #@76
    .line 541
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@78
    const-class v1, Landroid/text/HtmlToSpannedConverter$Italic;

    #@7a
    new-instance v2, Landroid/text/style/StyleSpan;

    #@7c
    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    #@7f
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@82
    goto :goto_10

    #@83
    .line 542
    :cond_83
    const-string v0, "dfn"

    #@85
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@88
    move-result v0

    #@89
    if-eqz v0, :cond_99

    #@8b
    .line 543
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@8d
    const-class v1, Landroid/text/HtmlToSpannedConverter$Italic;

    #@8f
    new-instance v2, Landroid/text/style/StyleSpan;

    #@91
    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    #@94
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@97
    goto/16 :goto_10

    #@99
    .line 544
    :cond_99
    const-string v0, "i"

    #@9b
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@9e
    move-result v0

    #@9f
    if-eqz v0, :cond_af

    #@a1
    .line 545
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@a3
    const-class v1, Landroid/text/HtmlToSpannedConverter$Italic;

    #@a5
    new-instance v2, Landroid/text/style/StyleSpan;

    #@a7
    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    #@aa
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@ad
    goto/16 :goto_10

    #@af
    .line 546
    :cond_af
    const-string v0, "big"

    #@b1
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@b4
    move-result v0

    #@b5
    if-eqz v0, :cond_c7

    #@b7
    .line 547
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@b9
    const-class v1, Landroid/text/HtmlToSpannedConverter$Big;

    #@bb
    new-instance v2, Landroid/text/style/RelativeSizeSpan;

    #@bd
    const/high16 v3, 0x3fa0

    #@bf
    invoke-direct {v2, v3}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    #@c2
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@c5
    goto/16 :goto_10

    #@c7
    .line 548
    :cond_c7
    const-string/jumbo v0, "small"

    #@ca
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@cd
    move-result v0

    #@ce
    if-eqz v0, :cond_e1

    #@d0
    .line 549
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@d2
    const-class v1, Landroid/text/HtmlToSpannedConverter$Small;

    #@d4
    new-instance v2, Landroid/text/style/RelativeSizeSpan;

    #@d6
    const v3, 0x3f4ccccd

    #@d9
    invoke-direct {v2, v3}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    #@dc
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@df
    goto/16 :goto_10

    #@e1
    .line 550
    :cond_e1
    const-string v0, "font"

    #@e3
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@e6
    move-result v0

    #@e7
    if-eqz v0, :cond_f0

    #@e9
    .line 551
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@eb
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->endFont(Landroid/text/SpannableStringBuilder;)V

    #@ee
    goto/16 :goto_10

    #@f0
    .line 552
    :cond_f0
    const-string v0, "blockquote"

    #@f2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@f5
    move-result v0

    #@f6
    if-eqz v0, :cond_10b

    #@f8
    .line 553
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@fa
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    #@fd
    .line 554
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@ff
    const-class v1, Landroid/text/HtmlToSpannedConverter$Blockquote;

    #@101
    new-instance v2, Landroid/text/style/QuoteSpan;

    #@103
    invoke-direct {v2}, Landroid/text/style/QuoteSpan;-><init>()V

    #@106
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@109
    goto/16 :goto_10

    #@10b
    .line 555
    :cond_10b
    const-string/jumbo v0, "tt"

    #@10e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@111
    move-result v0

    #@112
    if-eqz v0, :cond_125

    #@114
    .line 556
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@116
    const-class v1, Landroid/text/HtmlToSpannedConverter$Monospace;

    #@118
    new-instance v2, Landroid/text/style/TypefaceSpan;

    #@11a
    const-string/jumbo v3, "monospace"

    #@11d
    invoke-direct {v2, v3}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    #@120
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@123
    goto/16 :goto_10

    #@125
    .line 558
    :cond_125
    const-string v0, "a"

    #@127
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@12a
    move-result v0

    #@12b
    if-eqz v0, :cond_134

    #@12d
    .line 559
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@12f
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->endA(Landroid/text/SpannableStringBuilder;)V

    #@132
    goto/16 :goto_10

    #@134
    .line 560
    :cond_134
    const-string/jumbo v0, "u"

    #@137
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@13a
    move-result v0

    #@13b
    if-eqz v0, :cond_14b

    #@13d
    .line 561
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@13f
    const-class v1, Landroid/text/HtmlToSpannedConverter$Underline;

    #@141
    new-instance v2, Landroid/text/style/UnderlineSpan;

    #@143
    invoke-direct {v2}, Landroid/text/style/UnderlineSpan;-><init>()V

    #@146
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@149
    goto/16 :goto_10

    #@14b
    .line 562
    :cond_14b
    const-string/jumbo v0, "sup"

    #@14e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@151
    move-result v0

    #@152
    if-eqz v0, :cond_162

    #@154
    .line 563
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@156
    const-class v1, Landroid/text/HtmlToSpannedConverter$Super;

    #@158
    new-instance v2, Landroid/text/style/SuperscriptSpan;

    #@15a
    invoke-direct {v2}, Landroid/text/style/SuperscriptSpan;-><init>()V

    #@15d
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@160
    goto/16 :goto_10

    #@162
    .line 564
    :cond_162
    const-string/jumbo v0, "sub"

    #@165
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@168
    move-result v0

    #@169
    if-eqz v0, :cond_179

    #@16b
    .line 565
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@16d
    const-class v1, Landroid/text/HtmlToSpannedConverter$Sub;

    #@16f
    new-instance v2, Landroid/text/style/SubscriptSpan;

    #@171
    invoke-direct {v2}, Landroid/text/style/SubscriptSpan;-><init>()V

    #@174
    invoke-static {v0, v1, v2}, Landroid/text/HtmlToSpannedConverter;->end(Landroid/text/SpannableStringBuilder;Ljava/lang/Class;Ljava/lang/Object;)V

    #@177
    goto/16 :goto_10

    #@179
    .line 566
    :cond_179
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@17c
    move-result v0

    #@17d
    if-ne v0, v3, :cond_1a7

    #@17f
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    #@182
    move-result v0

    #@183
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    #@186
    move-result v0

    #@187
    const/16 v1, 0x68

    #@189
    if-ne v0, v1, :cond_1a7

    #@18b
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@18e
    move-result v0

    #@18f
    const/16 v1, 0x31

    #@191
    if-lt v0, v1, :cond_1a7

    #@193
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@196
    move-result v0

    #@197
    const/16 v1, 0x36

    #@199
    if-gt v0, v1, :cond_1a7

    #@19b
    .line 569
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@19d
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    #@1a0
    .line 570
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@1a2
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->endHeader(Landroid/text/SpannableStringBuilder;)V

    #@1a5
    goto/16 :goto_10

    #@1a7
    .line 571
    :cond_1a7
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mTagHandler:Landroid/text/Html$TagHandler;

    #@1a9
    if-eqz v0, :cond_10

    #@1ab
    .line 572
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mTagHandler:Landroid/text/Html$TagHandler;

    #@1ad
    iget-object v1, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@1af
    iget-object v2, p0, Landroid/text/HtmlToSpannedConverter;->mReader:Lorg/xml/sax/XMLReader;

    #@1b1
    invoke-interface {v0, v5, p1, v1, v2}, Landroid/text/Html$TagHandler;->handleTag(ZLjava/lang/String;Landroid/text/Editable;Lorg/xml/sax/XMLReader;)V

    #@1b4
    goto/16 :goto_10
.end method

.method private static handleP(Landroid/text/SpannableStringBuilder;)V
    .registers 4
    .parameter "text"

    #@0
    .prologue
    const/16 v2, 0xa

    #@2
    .line 577
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@5
    move-result v0

    #@6
    .line 579
    .local v0, len:I
    const/4 v1, 0x1

    #@7
    if-lt v0, v1, :cond_23

    #@9
    add-int/lit8 v1, v0, -0x1

    #@b
    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@e
    move-result v1

    #@f
    if-ne v1, v2, :cond_23

    #@11
    .line 580
    const/4 v1, 0x2

    #@12
    if-lt v0, v1, :cond_1d

    #@14
    add-int/lit8 v1, v0, -0x2

    #@16
    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@19
    move-result v1

    #@1a
    if-ne v1, v2, :cond_1d

    #@1c
    .line 591
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 584
    :cond_1d
    const-string v1, "\n"

    #@1f
    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@22
    goto :goto_1c

    #@23
    .line 588
    :cond_23
    if-eqz v0, :cond_1c

    #@25
    .line 589
    const-string v1, "\n\n"

    #@27
    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@2a
    goto :goto_1c
.end method

.method private handleStartTag(Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .registers 7
    .parameter "tag"
    .parameter "attributes"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 477
    const-string v0, "br"

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_b

    #@a
    .line 525
    :cond_a
    :goto_a
    return-void

    #@b
    .line 480
    :cond_b
    const-string/jumbo v0, "p"

    #@e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1a

    #@14
    .line 481
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@16
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    #@19
    goto :goto_a

    #@1a
    .line 482
    :cond_1a
    const-string v0, "div"

    #@1c
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_28

    #@22
    .line 483
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@24
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    #@27
    goto :goto_a

    #@28
    .line 484
    :cond_28
    const-string/jumbo v0, "strong"

    #@2b
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2e
    move-result v0

    #@2f
    if-eqz v0, :cond_3c

    #@31
    .line 485
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@33
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Bold;

    #@35
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Bold;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@38
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@3b
    goto :goto_a

    #@3c
    .line 486
    :cond_3c
    const-string v0, "b"

    #@3e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@41
    move-result v0

    #@42
    if-eqz v0, :cond_4f

    #@44
    .line 487
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@46
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Bold;

    #@48
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Bold;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@4b
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@4e
    goto :goto_a

    #@4f
    .line 488
    :cond_4f
    const-string v0, "em"

    #@51
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@54
    move-result v0

    #@55
    if-eqz v0, :cond_62

    #@57
    .line 489
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@59
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Italic;

    #@5b
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Italic;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@5e
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@61
    goto :goto_a

    #@62
    .line 490
    :cond_62
    const-string v0, "cite"

    #@64
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@67
    move-result v0

    #@68
    if-eqz v0, :cond_75

    #@6a
    .line 491
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@6c
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Italic;

    #@6e
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Italic;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@71
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@74
    goto :goto_a

    #@75
    .line 492
    :cond_75
    const-string v0, "dfn"

    #@77
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7a
    move-result v0

    #@7b
    if-eqz v0, :cond_88

    #@7d
    .line 493
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@7f
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Italic;

    #@81
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Italic;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@84
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@87
    goto :goto_a

    #@88
    .line 494
    :cond_88
    const-string v0, "i"

    #@8a
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@8d
    move-result v0

    #@8e
    if-eqz v0, :cond_9c

    #@90
    .line 495
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@92
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Italic;

    #@94
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Italic;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@97
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@9a
    goto/16 :goto_a

    #@9c
    .line 496
    :cond_9c
    const-string v0, "big"

    #@9e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a1
    move-result v0

    #@a2
    if-eqz v0, :cond_b0

    #@a4
    .line 497
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@a6
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Big;

    #@a8
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Big;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@ab
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@ae
    goto/16 :goto_a

    #@b0
    .line 498
    :cond_b0
    const-string/jumbo v0, "small"

    #@b3
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@b6
    move-result v0

    #@b7
    if-eqz v0, :cond_c5

    #@b9
    .line 499
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@bb
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Small;

    #@bd
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Small;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@c0
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@c3
    goto/16 :goto_a

    #@c5
    .line 500
    :cond_c5
    const-string v0, "font"

    #@c7
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@ca
    move-result v0

    #@cb
    if-eqz v0, :cond_d4

    #@cd
    .line 501
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@cf
    invoke-static {v0, p2}, Landroid/text/HtmlToSpannedConverter;->startFont(Landroid/text/SpannableStringBuilder;Lorg/xml/sax/Attributes;)V

    #@d2
    goto/16 :goto_a

    #@d4
    .line 502
    :cond_d4
    const-string v0, "blockquote"

    #@d6
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d9
    move-result v0

    #@da
    if-eqz v0, :cond_ed

    #@dc
    .line 503
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@de
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    #@e1
    .line 504
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@e3
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Blockquote;

    #@e5
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Blockquote;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@e8
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@eb
    goto/16 :goto_a

    #@ed
    .line 505
    :cond_ed
    const-string/jumbo v0, "tt"

    #@f0
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@f3
    move-result v0

    #@f4
    if-eqz v0, :cond_102

    #@f6
    .line 506
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@f8
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Monospace;

    #@fa
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Monospace;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@fd
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@100
    goto/16 :goto_a

    #@102
    .line 507
    :cond_102
    const-string v0, "a"

    #@104
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@107
    move-result v0

    #@108
    if-eqz v0, :cond_111

    #@10a
    .line 508
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@10c
    invoke-static {v0, p2}, Landroid/text/HtmlToSpannedConverter;->startA(Landroid/text/SpannableStringBuilder;Lorg/xml/sax/Attributes;)V

    #@10f
    goto/16 :goto_a

    #@111
    .line 509
    :cond_111
    const-string/jumbo v0, "u"

    #@114
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@117
    move-result v0

    #@118
    if-eqz v0, :cond_126

    #@11a
    .line 510
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@11c
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Underline;

    #@11e
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Underline;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@121
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@124
    goto/16 :goto_a

    #@126
    .line 511
    :cond_126
    const-string/jumbo v0, "sup"

    #@129
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@12c
    move-result v0

    #@12d
    if-eqz v0, :cond_13b

    #@12f
    .line 512
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@131
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Super;

    #@133
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Super;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@136
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@139
    goto/16 :goto_a

    #@13b
    .line 513
    :cond_13b
    const-string/jumbo v0, "sub"

    #@13e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@141
    move-result v0

    #@142
    if-eqz v0, :cond_150

    #@144
    .line 514
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@146
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Sub;

    #@148
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Sub;-><init>(Landroid/text/HtmlToSpannedConverter$1;)V

    #@14b
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@14e
    goto/16 :goto_a

    #@150
    .line 515
    :cond_150
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@153
    move-result v0

    #@154
    const/4 v1, 0x2

    #@155
    if-ne v0, v1, :cond_18b

    #@157
    const/4 v0, 0x0

    #@158
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@15b
    move-result v0

    #@15c
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    #@15f
    move-result v0

    #@160
    const/16 v1, 0x68

    #@162
    if-ne v0, v1, :cond_18b

    #@164
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@167
    move-result v0

    #@168
    const/16 v1, 0x31

    #@16a
    if-lt v0, v1, :cond_18b

    #@16c
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@16f
    move-result v0

    #@170
    const/16 v1, 0x36

    #@172
    if-gt v0, v1, :cond_18b

    #@174
    .line 518
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@176
    invoke-static {v0}, Landroid/text/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    #@179
    .line 519
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@17b
    new-instance v1, Landroid/text/HtmlToSpannedConverter$Header;

    #@17d
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@180
    move-result v2

    #@181
    add-int/lit8 v2, v2, -0x31

    #@183
    invoke-direct {v1, v2}, Landroid/text/HtmlToSpannedConverter$Header;-><init>(I)V

    #@186
    invoke-static {v0, v1}, Landroid/text/HtmlToSpannedConverter;->start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V

    #@189
    goto/16 :goto_a

    #@18b
    .line 520
    :cond_18b
    const-string v0, "img"

    #@18d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@190
    move-result v0

    #@191
    if-eqz v0, :cond_19c

    #@193
    .line 521
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@195
    iget-object v1, p0, Landroid/text/HtmlToSpannedConverter;->mImageGetter:Landroid/text/Html$ImageGetter;

    #@197
    invoke-static {v0, p2, v1}, Landroid/text/HtmlToSpannedConverter;->startImg(Landroid/text/SpannableStringBuilder;Lorg/xml/sax/Attributes;Landroid/text/Html$ImageGetter;)V

    #@19a
    goto/16 :goto_a

    #@19c
    .line 522
    :cond_19c
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mTagHandler:Landroid/text/Html$TagHandler;

    #@19e
    if-eqz v0, :cond_a

    #@1a0
    .line 523
    iget-object v0, p0, Landroid/text/HtmlToSpannedConverter;->mTagHandler:Landroid/text/Html$TagHandler;

    #@1a2
    iget-object v1, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@1a4
    iget-object v2, p0, Landroid/text/HtmlToSpannedConverter;->mReader:Lorg/xml/sax/XMLReader;

    #@1a6
    invoke-interface {v0, v3, p1, v1, v2}, Landroid/text/Html$TagHandler;->handleTag(ZLjava/lang/String;Landroid/text/Editable;Lorg/xml/sax/XMLReader;)V

    #@1a9
    goto/16 :goto_a
.end method

.method private static start(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V
    .registers 4
    .parameter "text"
    .parameter "mark"

    #@0
    .prologue
    .line 612
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@3
    move-result v0

    #@4
    .line 613
    .local v0, len:I
    const/16 v1, 0x11

    #@6
    invoke-virtual {p0, p1, v0, v0, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@9
    .line 614
    return-void
.end method

.method private static startA(Landroid/text/SpannableStringBuilder;Lorg/xml/sax/Attributes;)V
    .registers 6
    .parameter "text"
    .parameter "attributes"

    #@0
    .prologue
    .line 701
    const-string v2, ""

    #@2
    const-string v3, "href"

    #@4
    invoke-interface {p1, v2, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 703
    .local v0, href:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@b
    move-result v1

    #@c
    .line 704
    .local v1, len:I
    new-instance v2, Landroid/text/HtmlToSpannedConverter$Href;

    #@e
    invoke-direct {v2, v0}, Landroid/text/HtmlToSpannedConverter$Href;-><init>(Ljava/lang/String;)V

    #@11
    const/16 v3, 0x11

    #@13
    invoke-virtual {p0, v2, v1, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@16
    .line 705
    return-void
.end method

.method private static startFont(Landroid/text/SpannableStringBuilder;Lorg/xml/sax/Attributes;)V
    .registers 7
    .parameter "text"
    .parameter "attributes"

    #@0
    .prologue
    .line 655
    const-string v3, ""

    #@2
    const-string v4, "color"

    #@4
    invoke-interface {p1, v3, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 656
    .local v0, color:Ljava/lang/String;
    const-string v3, ""

    #@a
    const-string v4, "face"

    #@c
    invoke-interface {p1, v3, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    .line 658
    .local v1, face:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@13
    move-result v2

    #@14
    .line 659
    .local v2, len:I
    new-instance v3, Landroid/text/HtmlToSpannedConverter$Font;

    #@16
    invoke-direct {v3, v0, v1}, Landroid/text/HtmlToSpannedConverter$Font;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    const/16 v4, 0x11

    #@1b
    invoke-virtual {p0, v3, v2, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@1e
    .line 660
    return-void
.end method

.method private static startImg(Landroid/text/SpannableStringBuilder;Lorg/xml/sax/Attributes;Landroid/text/Html$ImageGetter;)V
    .registers 9
    .parameter "text"
    .parameter "attributes"
    .parameter "img"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 633
    const-string v3, ""

    #@3
    const-string/jumbo v4, "src"

    #@6
    invoke-interface {p1, v3, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    .line 634
    .local v2, src:Ljava/lang/String;
    const/4 v0, 0x0

    #@b
    .line 636
    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz p2, :cond_11

    #@d
    .line 637
    invoke-interface {p2, v2}, Landroid/text/Html$ImageGetter;->getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@10
    move-result-object v0

    #@11
    .line 640
    :cond_11
    if-nez v0, :cond_29

    #@13
    .line 641
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@16
    move-result-object v3

    #@17
    const v4, 0x108060e

    #@1a
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1d
    move-result-object v0

    #@1e
    .line 643
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@21
    move-result v3

    #@22
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@25
    move-result v4

    #@26
    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@29
    .line 646
    :cond_29
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@2c
    move-result v1

    #@2d
    .line 647
    .local v1, len:I
    const-string/jumbo v3, "\ufffc"

    #@30
    invoke-virtual {p0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@33
    .line 649
    new-instance v3, Landroid/text/style/ImageSpan;

    #@35
    invoke-direct {v3, v0, v2}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    #@38
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@3b
    move-result v4

    #@3c
    const/16 v5, 0x21

    #@3e
    invoke-virtual {p0, v3, v1, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@41
    .line 651
    return-void
.end method


# virtual methods
.method public characters([CII)V
    .registers 13
    .parameter "ch"
    .parameter "start"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v8, 0xa

    #@2
    const/16 v7, 0x20

    #@4
    .line 772
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 779
    .local v4, sb:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, p3, :cond_42

    #@c
    .line 780
    add-int v5, v1, p2

    #@e
    aget-char v0, p1, v5

    #@10
    .line 782
    .local v0, c:C
    if-eq v0, v7, :cond_14

    #@12
    if-ne v0, v8, :cond_3e

    #@14
    .line 784
    :cond_14
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    #@17
    move-result v2

    #@18
    .line 786
    .local v2, len:I
    if-nez v2, :cond_37

    #@1a
    .line 787
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@1c
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    #@1f
    move-result v2

    #@20
    .line 789
    if-nez v2, :cond_2e

    #@22
    .line 790
    const/16 v3, 0xa

    #@24
    .line 798
    .local v3, pred:C
    :goto_24
    if-eq v3, v7, :cond_2b

    #@26
    if-eq v3, v8, :cond_2b

    #@28
    .line 799
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2b
    .line 779
    .end local v2           #len:I
    .end local v3           #pred:C
    :cond_2b
    :goto_2b
    add-int/lit8 v1, v1, 0x1

    #@2d
    goto :goto_a

    #@2e
    .line 792
    .restart local v2       #len:I
    :cond_2e
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@30
    add-int/lit8 v6, v2, -0x1

    #@32
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@35
    move-result v3

    #@36
    .restart local v3       #pred:C
    goto :goto_24

    #@37
    .line 795
    .end local v3           #pred:C
    :cond_37
    add-int/lit8 v5, v2, -0x1

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    #@3c
    move-result v3

    #@3d
    .restart local v3       #pred:C
    goto :goto_24

    #@3e
    .line 802
    .end local v2           #len:I
    .end local v3           #pred:C
    :cond_3e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@41
    goto :goto_2b

    #@42
    .line 806
    .end local v0           #c:C
    :cond_42
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@44
    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@47
    .line 807
    return-void
.end method

.method public convert()Landroid/text/Spanned;
    .registers 11

    #@0
    .prologue
    const/16 v9, 0xa

    #@2
    .line 441
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mReader:Lorg/xml/sax/XMLReader;

    #@4
    invoke-interface {v5, p0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    #@7
    .line 443
    :try_start_7
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mReader:Lorg/xml/sax/XMLReader;

    #@9
    new-instance v6, Lorg/xml/sax/InputSource;

    #@b
    new-instance v7, Ljava/io/StringReader;

    #@d
    iget-object v8, p0, Landroid/text/HtmlToSpannedConverter;->mSource:Ljava/lang/String;

    #@f
    invoke-direct {v7, v8}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    #@12
    invoke-direct {v6, v7}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    #@15
    invoke-interface {v5, v6}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_18} :catch_61
    .catch Lorg/xml/sax/SAXException; {:try_start_7 .. :try_end_18} :catch_68

    #@18
    .line 453
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@1a
    const/4 v6, 0x0

    #@1b
    iget-object v7, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@1d
    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    #@20
    move-result v7

    #@21
    const-class v8, Landroid/text/style/ParagraphStyle;

    #@23
    invoke-virtual {v5, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@26
    move-result-object v3

    #@27
    .line 454
    .local v3, obj:[Ljava/lang/Object;
    const/4 v2, 0x0

    #@28
    .local v2, i:I
    :goto_28
    array-length v5, v3

    #@29
    if-ge v2, v5, :cond_79

    #@2b
    .line 455
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@2d
    aget-object v6, v3, v2

    #@2f
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    #@32
    move-result v4

    #@33
    .line 456
    .local v4, start:I
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@35
    aget-object v6, v3, v2

    #@37
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    #@3a
    move-result v1

    #@3b
    .line 459
    .local v1, end:I
    add-int/lit8 v5, v1, -0x2

    #@3d
    if-ltz v5, :cond_55

    #@3f
    .line 460
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@41
    add-int/lit8 v6, v1, -0x1

    #@43
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@46
    move-result v5

    #@47
    if-ne v5, v9, :cond_55

    #@49
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@4b
    add-int/lit8 v6, v1, -0x2

    #@4d
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@50
    move-result v5

    #@51
    if-ne v5, v9, :cond_55

    #@53
    .line 462
    add-int/lit8 v1, v1, -0x1

    #@55
    .line 466
    :cond_55
    if-ne v1, v4, :cond_6f

    #@57
    .line 467
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@59
    aget-object v6, v3, v2

    #@5b
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    #@5e
    .line 454
    :goto_5e
    add-int/lit8 v2, v2, 0x1

    #@60
    goto :goto_28

    #@61
    .line 444
    .end local v1           #end:I
    .end local v2           #i:I
    .end local v3           #obj:[Ljava/lang/Object;
    .end local v4           #start:I
    :catch_61
    move-exception v0

    #@62
    .line 446
    .local v0, e:Ljava/io/IOException;
    new-instance v5, Ljava/lang/RuntimeException;

    #@64
    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@67
    throw v5

    #@68
    .line 447
    .end local v0           #e:Ljava/io/IOException;
    :catch_68
    move-exception v0

    #@69
    .line 449
    .local v0, e:Lorg/xml/sax/SAXException;
    new-instance v5, Ljava/lang/RuntimeException;

    #@6b
    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@6e
    throw v5

    #@6f
    .line 469
    .end local v0           #e:Lorg/xml/sax/SAXException;
    .restart local v1       #end:I
    .restart local v2       #i:I
    .restart local v3       #obj:[Ljava/lang/Object;
    .restart local v4       #start:I
    :cond_6f
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@71
    aget-object v6, v3, v2

    #@73
    const/16 v7, 0x33

    #@75
    invoke-virtual {v5, v6, v4, v1, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@78
    goto :goto_5e

    #@79
    .line 473
    .end local v1           #end:I
    .end local v4           #start:I
    :cond_79
    iget-object v5, p0, Landroid/text/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    #@7b
    return-object v5
.end method

.method public endDocument()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 754
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "uri"
    .parameter "localName"
    .parameter "qName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 768
    invoke-direct {p0, p2}, Landroid/text/HtmlToSpannedConverter;->handleEndTag(Ljava/lang/String;)V

    #@3
    .line 769
    return-void
.end method

.method public endPrefixMapping(Ljava/lang/String;)V
    .registers 2
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 760
    return-void
.end method

.method public ignorableWhitespace([CII)V
    .registers 4
    .parameter "ch"
    .parameter "start"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 810
    return-void
.end method

.method public processingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "target"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 813
    return-void
.end method

.method public setDocumentLocator(Lorg/xml/sax/Locator;)V
    .registers 2
    .parameter "locator"

    #@0
    .prologue
    .line 748
    return-void
.end method

.method public skippedEntity(Ljava/lang/String;)V
    .registers 2
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 816
    return-void
.end method

.method public startDocument()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 751
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .registers 5
    .parameter "uri"
    .parameter "localName"
    .parameter "qName"
    .parameter "attributes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 764
    invoke-direct {p0, p2, p4}, Landroid/text/HtmlToSpannedConverter;->handleStartTag(Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    #@3
    .line 765
    return-void
.end method

.method public startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "prefix"
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 757
    return-void
.end method
