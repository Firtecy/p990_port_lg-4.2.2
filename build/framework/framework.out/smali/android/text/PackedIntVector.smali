.class Landroid/text/PackedIntVector;
.super Ljava/lang/Object;
.source "PackedIntVector.java"


# instance fields
.field private final mColumns:I

.field private mRowGapLength:I

.field private mRowGapStart:I

.field private mRows:I

.field private mValueGap:[I

.field private mValues:[I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "columns"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 44
    iput p1, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@6
    .line 45
    iput v0, p0, Landroid/text/PackedIntVector;->mRows:I

    #@8
    .line 47
    iput v0, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@a
    .line 48
    iget v0, p0, Landroid/text/PackedIntVector;->mRows:I

    #@c
    iput v0, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@e
    .line 50
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@11
    .line 51
    mul-int/lit8 v0, p1, 0x2

    #@13
    new-array v0, v0, [I

    #@15
    iput-object v0, p0, Landroid/text/PackedIntVector;->mValueGap:[I

    #@17
    .line 52
    return-void
.end method

.method private final growBuffer()V
    .registers 12

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 254
    iget v1, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@3
    .line 255
    .local v1, columns:I
    invoke-virtual {p0}, Landroid/text/PackedIntVector;->size()I

    #@6
    move-result v7

    #@7
    add-int/lit8 v3, v7, 0x1

    #@9
    .line 256
    .local v3, newsize:I
    mul-int v7, v3, v1

    #@b
    invoke-static {v7}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@e
    move-result v7

    #@f
    div-int v3, v7, v1

    #@11
    .line 257
    mul-int v7, v3, v1

    #@13
    new-array v4, v7, [I

    #@15
    .line 259
    .local v4, newvalues:[I
    iget-object v6, p0, Landroid/text/PackedIntVector;->mValueGap:[I

    #@17
    .line 260
    .local v6, valuegap:[I
    iget v5, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@19
    .line 262
    .local v5, rowgapstart:I
    iget v7, p0, Landroid/text/PackedIntVector;->mRows:I

    #@1b
    iget v8, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@1d
    add-int/2addr v8, v5

    #@1e
    sub-int v0, v7, v8

    #@20
    .line 264
    .local v0, after:I
    iget-object v7, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@22
    if-eqz v7, :cond_39

    #@24
    .line 265
    iget-object v7, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@26
    mul-int v8, v1, v5

    #@28
    invoke-static {v7, v9, v4, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2b
    .line 266
    iget-object v7, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@2d
    iget v8, p0, Landroid/text/PackedIntVector;->mRows:I

    #@2f
    sub-int/2addr v8, v0

    #@30
    mul-int/2addr v8, v1

    #@31
    sub-int v9, v3, v0

    #@33
    mul-int/2addr v9, v1

    #@34
    mul-int v10, v0, v1

    #@36
    invoke-static {v7, v8, v4, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@39
    .line 271
    :cond_39
    const/4 v2, 0x0

    #@3a
    .local v2, i:I
    :goto_3a
    if-ge v2, v1, :cond_52

    #@3c
    .line 272
    aget v7, v6, v2

    #@3e
    if-lt v7, v5, :cond_4f

    #@40
    .line 273
    aget v7, v6, v2

    #@42
    iget v8, p0, Landroid/text/PackedIntVector;->mRows:I

    #@44
    sub-int v8, v3, v8

    #@46
    add-int/2addr v7, v8

    #@47
    aput v7, v6, v2

    #@49
    .line 275
    aget v7, v6, v2

    #@4b
    if-ge v7, v5, :cond_4f

    #@4d
    .line 276
    aput v5, v6, v2

    #@4f
    .line 271
    :cond_4f
    add-int/lit8 v2, v2, 0x1

    #@51
    goto :goto_3a

    #@52
    .line 281
    :cond_52
    iget v7, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@54
    iget v8, p0, Landroid/text/PackedIntVector;->mRows:I

    #@56
    sub-int v8, v3, v8

    #@58
    add-int/2addr v7, v8

    #@59
    iput v7, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@5b
    .line 282
    iput v3, p0, Landroid/text/PackedIntVector;->mRows:I

    #@5d
    .line 283
    iput-object v4, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@5f
    .line 284
    return-void
.end method

.method private final moveRowGapTo(I)V
    .registers 14
    .parameter "where"

    #@0
    .prologue
    .line 314
    iget v9, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@2
    if-ne p1, v9, :cond_5

    #@4
    .line 367
    :goto_4
    return-void

    #@5
    .line 316
    :cond_5
    iget v9, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@7
    if-le p1, v9, :cond_4f

    #@9
    .line 317
    iget v9, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@b
    add-int/2addr v9, p1

    #@c
    iget v10, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@e
    iget v11, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@10
    add-int/2addr v10, v11

    #@11
    sub-int v5, v9, v10

    #@13
    .line 318
    .local v5, moving:I
    iget v0, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@15
    .line 319
    .local v0, columns:I
    iget-object v7, p0, Landroid/text/PackedIntVector;->mValueGap:[I

    #@17
    .line 320
    .local v7, valuegap:[I
    iget-object v8, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@19
    .line 321
    .local v8, values:[I
    iget v9, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@1b
    iget v10, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@1d
    add-int v2, v9, v10

    #@1f
    .line 323
    .local v2, gapend:I
    move v3, v2

    #@20
    .local v3, i:I
    :goto_20
    add-int v9, v2, v5

    #@22
    if-ge v3, v9, :cond_8f

    #@24
    .line 324
    sub-int v9, v3, v2

    #@26
    iget v10, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@28
    add-int v1, v9, v10

    #@2a
    .line 326
    .local v1, destrow:I
    const/4 v4, 0x0

    #@2b
    .local v4, j:I
    :goto_2b
    if-ge v4, v0, :cond_4c

    #@2d
    .line 327
    mul-int v9, v3, v0

    #@2f
    add-int/2addr v9, v4

    #@30
    aget v6, v8, v9

    #@32
    .line 329
    .local v6, val:I
    aget v9, v7, v4

    #@34
    if-lt v3, v9, :cond_3b

    #@36
    .line 330
    add-int v9, v4, v0

    #@38
    aget v9, v7, v9

    #@3a
    add-int/2addr v6, v9

    #@3b
    .line 333
    :cond_3b
    aget v9, v7, v4

    #@3d
    if-lt v1, v9, :cond_44

    #@3f
    .line 334
    add-int v9, v4, v0

    #@41
    aget v9, v7, v9

    #@43
    sub-int/2addr v6, v9

    #@44
    .line 337
    :cond_44
    mul-int v9, v1, v0

    #@46
    add-int/2addr v9, v4

    #@47
    aput v6, v8, v9

    #@49
    .line 326
    add-int/lit8 v4, v4, 0x1

    #@4b
    goto :goto_2b

    #@4c
    .line 323
    .end local v6           #val:I
    :cond_4c
    add-int/lit8 v3, v3, 0x1

    #@4e
    goto :goto_20

    #@4f
    .line 341
    .end local v0           #columns:I
    .end local v1           #destrow:I
    .end local v2           #gapend:I
    .end local v3           #i:I
    .end local v4           #j:I
    .end local v5           #moving:I
    .end local v7           #valuegap:[I
    .end local v8           #values:[I
    :cond_4f
    iget v9, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@51
    sub-int v5, v9, p1

    #@53
    .line 342
    .restart local v5       #moving:I
    iget v0, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@55
    .line 343
    .restart local v0       #columns:I
    iget-object v7, p0, Landroid/text/PackedIntVector;->mValueGap:[I

    #@57
    .line 344
    .restart local v7       #valuegap:[I
    iget-object v8, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@59
    .line 345
    .restart local v8       #values:[I
    iget v9, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@5b
    iget v10, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@5d
    add-int v2, v9, v10

    #@5f
    .line 347
    .restart local v2       #gapend:I
    add-int v9, p1, v5

    #@61
    add-int/lit8 v3, v9, -0x1

    #@63
    .restart local v3       #i:I
    :goto_63
    if-lt v3, p1, :cond_8f

    #@65
    .line 348
    sub-int v9, v3, p1

    #@67
    add-int/2addr v9, v2

    #@68
    sub-int v1, v9, v5

    #@6a
    .line 350
    .restart local v1       #destrow:I
    const/4 v4, 0x0

    #@6b
    .restart local v4       #j:I
    :goto_6b
    if-ge v4, v0, :cond_8c

    #@6d
    .line 351
    mul-int v9, v3, v0

    #@6f
    add-int/2addr v9, v4

    #@70
    aget v6, v8, v9

    #@72
    .line 353
    .restart local v6       #val:I
    aget v9, v7, v4

    #@74
    if-lt v3, v9, :cond_7b

    #@76
    .line 354
    add-int v9, v4, v0

    #@78
    aget v9, v7, v9

    #@7a
    add-int/2addr v6, v9

    #@7b
    .line 357
    :cond_7b
    aget v9, v7, v4

    #@7d
    if-lt v1, v9, :cond_84

    #@7f
    .line 358
    add-int v9, v4, v0

    #@81
    aget v9, v7, v9

    #@83
    sub-int/2addr v6, v9

    #@84
    .line 361
    :cond_84
    mul-int v9, v1, v0

    #@86
    add-int/2addr v9, v4

    #@87
    aput v6, v8, v9

    #@89
    .line 350
    add-int/lit8 v4, v4, 0x1

    #@8b
    goto :goto_6b

    #@8c
    .line 347
    .end local v6           #val:I
    :cond_8c
    add-int/lit8 v3, v3, -0x1

    #@8e
    goto :goto_63

    #@8f
    .line 366
    .end local v1           #destrow:I
    .end local v4           #j:I
    :cond_8f
    iput p1, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@91
    goto/16 :goto_4
.end method

.method private final moveValueGapTo(II)V
    .registers 10
    .parameter "column"
    .parameter "where"

    #@0
    .prologue
    .line 291
    iget-object v2, p0, Landroid/text/PackedIntVector;->mValueGap:[I

    #@2
    .line 292
    .local v2, valuegap:[I
    iget-object v3, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@4
    .line 293
    .local v3, values:[I
    iget v0, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@6
    .line 295
    .local v0, columns:I
    aget v4, v2, p1

    #@8
    if-ne p2, v4, :cond_b

    #@a
    .line 308
    :goto_a
    return-void

    #@b
    .line 297
    :cond_b
    aget v4, v2, p1

    #@d
    if-le p2, v4, :cond_22

    #@f
    .line 298
    aget v1, v2, p1

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, p2, :cond_36

    #@13
    .line 299
    mul-int v4, v1, v0

    #@15
    add-int/2addr v4, p1

    #@16
    aget v5, v3, v4

    #@18
    add-int v6, p1, v0

    #@1a
    aget v6, v2, v6

    #@1c
    add-int/2addr v5, v6

    #@1d
    aput v5, v3, v4

    #@1f
    .line 298
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_11

    #@22
    .line 302
    .end local v1           #i:I
    :cond_22
    move v1, p2

    #@23
    .restart local v1       #i:I
    :goto_23
    aget v4, v2, p1

    #@25
    if-ge v1, v4, :cond_36

    #@27
    .line 303
    mul-int v4, v1, v0

    #@29
    add-int/2addr v4, p1

    #@2a
    aget v5, v3, v4

    #@2c
    add-int v6, p1, v0

    #@2e
    aget v6, v2, v6

    #@30
    sub-int/2addr v5, v6

    #@31
    aput v5, v3, v4

    #@33
    .line 302
    add-int/lit8 v1, v1, 0x1

    #@35
    goto :goto_23

    #@36
    .line 307
    :cond_36
    aput p2, v2, p1

    #@38
    goto :goto_a
.end method

.method private setValueInternal(III)V
    .registers 7
    .parameter "row"
    .parameter "column"
    .parameter "value"

    #@0
    .prologue
    .line 123
    iget v1, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@2
    if-lt p1, v1, :cond_7

    #@4
    .line 124
    iget v1, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@6
    add-int/2addr p1, v1

    #@7
    .line 127
    :cond_7
    iget-object v0, p0, Landroid/text/PackedIntVector;->mValueGap:[I

    #@9
    .line 128
    .local v0, valuegap:[I
    aget v1, v0, p2

    #@b
    if-lt p1, v1, :cond_13

    #@d
    .line 129
    iget v1, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@f
    add-int/2addr v1, p2

    #@10
    aget v1, v0, v1

    #@12
    sub-int/2addr p3, v1

    #@13
    .line 132
    :cond_13
    iget-object v1, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@15
    iget v2, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@17
    mul-int/2addr v2, p1

    #@18
    add-int/2addr v2, p2

    #@19
    aput p3, v1, v2

    #@1b
    .line 133
    return-void
.end method


# virtual methods
.method public adjustValuesBelow(III)V
    .registers 7
    .parameter "startRow"
    .parameter "column"
    .parameter "delta"

    #@0
    .prologue
    .line 149
    or-int v0, p1, p2

    #@2
    if-ltz v0, :cond_10

    #@4
    invoke-virtual {p0}, Landroid/text/PackedIntVector;->size()I

    #@7
    move-result v0

    #@8
    if-gt p1, v0, :cond_10

    #@a
    invoke-virtual {p0}, Landroid/text/PackedIntVector;->width()I

    #@d
    move-result v0

    #@e
    if-lt p2, v0, :cond_2d

    #@10
    .line 151
    :cond_10
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 154
    :cond_2d
    iget v0, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@2f
    if-lt p1, v0, :cond_34

    #@31
    .line 155
    iget v0, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@33
    add-int/2addr p1, v0

    #@34
    .line 158
    :cond_34
    invoke-direct {p0, p2, p1}, Landroid/text/PackedIntVector;->moveValueGapTo(II)V

    #@37
    .line 159
    iget-object v0, p0, Landroid/text/PackedIntVector;->mValueGap:[I

    #@39
    iget v1, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@3b
    add-int/2addr v1, p2

    #@3c
    aget v2, v0, v1

    #@3e
    add-int/2addr v2, p3

    #@3f
    aput v2, v0, v1

    #@41
    .line 160
    return-void
.end method

.method public deleteAt(II)V
    .registers 6
    .parameter "row"
    .parameter "count"

    #@0
    .prologue
    .line 216
    or-int v0, p1, p2

    #@2
    if-ltz v0, :cond_c

    #@4
    add-int v0, p1, p2

    #@6
    invoke-virtual {p0}, Landroid/text/PackedIntVector;->size()I

    #@9
    move-result v1

    #@a
    if-le v0, v1, :cond_29

    #@c
    .line 217
    :cond_c
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ", "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 220
    :cond_29
    add-int v0, p1, p2

    #@2b
    invoke-direct {p0, v0}, Landroid/text/PackedIntVector;->moveRowGapTo(I)V

    #@2e
    .line 222
    iget v0, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@30
    sub-int/2addr v0, p2

    #@31
    iput v0, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@33
    .line 223
    iget v0, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@35
    add-int/2addr v0, p2

    #@36
    iput v0, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@38
    .line 227
    return-void
.end method

.method public getValue(II)I
    .registers 9
    .parameter "row"
    .parameter "column"

    #@0
    .prologue
    .line 67
    iget v0, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@2
    .line 69
    .local v0, columns:I
    or-int v3, p1, p2

    #@4
    if-ltz v3, :cond_e

    #@6
    invoke-virtual {p0}, Landroid/text/PackedIntVector;->size()I

    #@9
    move-result v3

    #@a
    if-ge p1, v3, :cond_e

    #@c
    if-lt p2, v0, :cond_2b

    #@e
    .line 70
    :cond_e
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v5, ", "

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-direct {v3, v4}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v3

    #@2b
    .line 73
    :cond_2b
    iget v3, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@2d
    if-lt p1, v3, :cond_32

    #@2f
    .line 74
    iget v3, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@31
    add-int/2addr p1, v3

    #@32
    .line 77
    :cond_32
    iget-object v3, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@34
    mul-int v4, p1, v0

    #@36
    add-int/2addr v4, p2

    #@37
    aget v1, v3, v4

    #@39
    .line 79
    .local v1, value:I
    iget-object v2, p0, Landroid/text/PackedIntVector;->mValueGap:[I

    #@3b
    .line 80
    .local v2, valuegap:[I
    aget v3, v2, p2

    #@3d
    if-lt p1, v3, :cond_44

    #@3f
    .line 81
    add-int v3, p2, v0

    #@41
    aget v3, v2, v3

    #@43
    add-int/2addr v1, v3

    #@44
    .line 84
    :cond_44
    return v1
.end method

.method public insertAt(I[I)V
    .registers 7
    .parameter "row"
    .parameter "values"

    #@0
    .prologue
    .line 176
    if-ltz p1, :cond_8

    #@2
    invoke-virtual {p0}, Landroid/text/PackedIntVector;->size()I

    #@5
    move-result v1

    #@6
    if-le p1, v1, :cond_22

    #@8
    .line 177
    :cond_8
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string/jumbo v3, "row "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 180
    :cond_22
    if-eqz p2, :cond_46

    #@24
    array-length v1, p2

    #@25
    invoke-virtual {p0}, Landroid/text/PackedIntVector;->width()I

    #@28
    move-result v2

    #@29
    if-ge v1, v2, :cond_46

    #@2b
    .line 181
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@2d
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string/jumbo v3, "value count "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    array-length v3, p2

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@45
    throw v1

    #@46
    .line 184
    :cond_46
    invoke-direct {p0, p1}, Landroid/text/PackedIntVector;->moveRowGapTo(I)V

    #@49
    .line 186
    iget v1, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@4b
    if-nez v1, :cond_50

    #@4d
    .line 187
    invoke-direct {p0}, Landroid/text/PackedIntVector;->growBuffer()V

    #@50
    .line 190
    :cond_50
    iget v1, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@52
    add-int/lit8 v1, v1, 0x1

    #@54
    iput v1, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@56
    .line 191
    iget v1, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@58
    add-int/lit8 v1, v1, -0x1

    #@5a
    iput v1, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@5c
    .line 193
    if-nez p2, :cond_6b

    #@5e
    .line 194
    iget v1, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@60
    add-int/lit8 v0, v1, -0x1

    #@62
    .local v0, i:I
    :goto_62
    if-ltz v0, :cond_79

    #@64
    .line 195
    const/4 v1, 0x0

    #@65
    invoke-direct {p0, p1, v0, v1}, Landroid/text/PackedIntVector;->setValueInternal(III)V

    #@68
    .line 194
    add-int/lit8 v0, v0, -0x1

    #@6a
    goto :goto_62

    #@6b
    .line 198
    .end local v0           #i:I
    :cond_6b
    iget v1, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@6d
    add-int/lit8 v0, v1, -0x1

    #@6f
    .restart local v0       #i:I
    :goto_6f
    if-ltz v0, :cond_79

    #@71
    .line 199
    aget v1, p2, v0

    #@73
    invoke-direct {p0, p1, v0, v1}, Landroid/text/PackedIntVector;->setValueInternal(III)V

    #@76
    .line 198
    add-int/lit8 v0, v0, -0x1

    #@78
    goto :goto_6f

    #@79
    .line 202
    :cond_79
    return-void
.end method

.method public setValue(III)V
    .registers 8
    .parameter "row"
    .parameter "column"
    .parameter "value"

    #@0
    .prologue
    .line 98
    or-int v1, p1, p2

    #@2
    if-ltz v1, :cond_e

    #@4
    invoke-virtual {p0}, Landroid/text/PackedIntVector;->size()I

    #@7
    move-result v1

    #@8
    if-ge p1, v1, :cond_e

    #@a
    iget v1, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@c
    if-lt p2, v1, :cond_2b

    #@e
    .line 99
    :cond_e
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    const-string v3, ", "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v1

    #@2b
    .line 102
    :cond_2b
    iget v1, p0, Landroid/text/PackedIntVector;->mRowGapStart:I

    #@2d
    if-lt p1, v1, :cond_32

    #@2f
    .line 103
    iget v1, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@31
    add-int/2addr p1, v1

    #@32
    .line 106
    :cond_32
    iget-object v0, p0, Landroid/text/PackedIntVector;->mValueGap:[I

    #@34
    .line 107
    .local v0, valuegap:[I
    aget v1, v0, p2

    #@36
    if-lt p1, v1, :cond_3e

    #@38
    .line 108
    iget v1, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@3a
    add-int/2addr v1, p2

    #@3b
    aget v1, v0, v1

    #@3d
    sub-int/2addr p3, v1

    #@3e
    .line 111
    :cond_3e
    iget-object v1, p0, Landroid/text/PackedIntVector;->mValues:[I

    #@40
    iget v2, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@42
    mul-int/2addr v2, p1

    #@43
    add-int/2addr v2, p2

    #@44
    aput p3, v1, v2

    #@46
    .line 112
    return-void
.end method

.method public size()I
    .registers 3

    #@0
    .prologue
    .line 236
    iget v0, p0, Landroid/text/PackedIntVector;->mRows:I

    #@2
    iget v1, p0, Landroid/text/PackedIntVector;->mRowGapLength:I

    #@4
    sub-int/2addr v0, v1

    #@5
    return v0
.end method

.method public width()I
    .registers 2

    #@0
    .prologue
    .line 246
    iget v0, p0, Landroid/text/PackedIntVector;->mColumns:I

    #@2
    return v0
.end method
