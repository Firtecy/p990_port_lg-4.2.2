.class public abstract Landroid/text/Layout;
.super Ljava/lang/Object;
.source "Layout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/Layout$Alignment;,
        Landroid/text/Layout$SpannedEllipsizer;,
        Landroid/text/Layout$Ellipsizer;,
        Landroid/text/Layout$Directions;,
        Landroid/text/Layout$TabStops;
    }
.end annotation


# static fields
.field static final DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions; = null

.field static final DIRS_ALL_RIGHT_TO_LEFT:Landroid/text/Layout$Directions; = null

.field public static final DIR_LEFT_TO_RIGHT:I = 0x1

.field static final DIR_REQUEST_DEFAULT_LTR:I = 0x2

.field static final DIR_REQUEST_DEFAULT_RTL:I = -0x2

.field static final DIR_REQUEST_LTR:I = 0x1

.field static final DIR_REQUEST_RTL:I = -0x1

.field public static final DIR_RIGHT_TO_LEFT:I = -0x1

.field static final ELLIPSIS_NORMAL:[C = null

.field static final ELLIPSIS_TWO_DOTS:[C = null

.field static final EMOJI_FACTORY:Landroid/emoji/EmojiFactory; = null

#the value of this static final field might be set in the static constructor
.field static final MAX_EMOJI:I = 0x0

#the value of this static final field might be set in the static constructor
.field static final MIN_EMOJI:I = 0x0

.field private static final NO_PARA_SPANS:[Landroid/text/style/ParagraphStyle; = null

.field static final RUN_LENGTH_MASK:I = 0x3ffffff

.field static final RUN_LEVEL_MASK:I = 0x3f

.field static final RUN_LEVEL_SHIFT:I = 0x1a

.field static final RUN_RTL_FLAG:I = 0x4000000

.field private static final TAB_INCREMENT:I = 0x14

.field private static final sTempRect:Landroid/graphics/Rect;


# instance fields
.field private mAlignment:Landroid/text/Layout$Alignment;

.field private mLineBackgroundSpans:Landroid/text/SpanSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/text/SpanSet",
            "<",
            "Landroid/text/style/LineBackgroundSpan;",
            ">;"
        }
    .end annotation
.end field

.field protected mLineContainsEmoji:[Z

.field private mPaint:Landroid/text/TextPaint;

.field private mSpacingAdd:F

.field private mSpacingMult:F

.field private mSpannedText:Z

.field private mText:Ljava/lang/CharSequence;

.field private mTextDir:Landroid/text/TextDirectionHeuristic;

.field private mWidth:I

.field mWorkPaint:Landroid/text/TextPaint;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v1, -0x1

    #@4
    .line 49
    const-class v0, Landroid/text/style/ParagraphStyle;

    #@6
    invoke-static {v0}, Lcom/android/internal/util/ArrayUtils;->emptyArray(Ljava/lang/Class;)[Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, [Landroid/text/style/ParagraphStyle;

    #@c
    sput-object v0, Landroid/text/Layout;->NO_PARA_SPANS:[Landroid/text/style/ParagraphStyle;

    #@e
    .line 52
    invoke-static {}, Landroid/emoji/EmojiFactory;->newAvailableInstance()Landroid/emoji/EmojiFactory;

    #@11
    move-result-object v0

    #@12
    sput-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@14
    .line 56
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@16
    if-eqz v0, :cond_58

    #@18
    .line 57
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@1a
    invoke-virtual {v0}, Landroid/emoji/EmojiFactory;->getMinimumAndroidPua()I

    #@1d
    move-result v0

    #@1e
    sput v0, Landroid/text/Layout;->MIN_EMOJI:I

    #@20
    .line 58
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@22
    invoke-virtual {v0}, Landroid/emoji/EmojiFactory;->getMaximumAndroidPua()I

    #@25
    move-result v0

    #@26
    sput v0, Landroid/text/Layout;->MAX_EMOJI:I

    #@28
    .line 2184
    :goto_28
    new-instance v0, Landroid/graphics/Rect;

    #@2a
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@2d
    sput-object v0, Landroid/text/Layout;->sTempRect:Landroid/graphics/Rect;

    #@2f
    .line 2214
    new-instance v0, Landroid/text/Layout$Directions;

    #@31
    new-array v1, v4, [I

    #@33
    fill-array-data v1, :array_5e

    #@36
    invoke-direct {v0, v1}, Landroid/text/Layout$Directions;-><init>([I)V

    #@39
    sput-object v0, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@3b
    .line 2216
    new-instance v0, Landroid/text/Layout$Directions;

    #@3d
    new-array v1, v4, [I

    #@3f
    fill-array-data v1, :array_66

    #@42
    invoke-direct {v0, v1}, Landroid/text/Layout$Directions;-><init>([I)V

    #@45
    sput-object v0, Landroid/text/Layout;->DIRS_ALL_RIGHT_TO_LEFT:Landroid/text/Layout$Directions;

    #@47
    .line 2219
    new-array v0, v3, [C

    #@49
    const/16 v1, 0x2026

    #@4b
    aput-char v1, v0, v2

    #@4d
    sput-object v0, Landroid/text/Layout;->ELLIPSIS_NORMAL:[C

    #@4f
    .line 2220
    new-array v0, v3, [C

    #@51
    const/16 v1, 0x2025

    #@53
    aput-char v1, v0, v2

    #@55
    sput-object v0, Landroid/text/Layout;->ELLIPSIS_TWO_DOTS:[C

    #@57
    return-void

    #@58
    .line 60
    :cond_58
    sput v1, Landroid/text/Layout;->MIN_EMOJI:I

    #@5a
    .line 61
    sput v1, Landroid/text/Layout;->MAX_EMOJI:I

    #@5c
    goto :goto_28

    #@5d
    .line 2214
    nop

    #@5e
    :array_5e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0x3t
    .end array-data

    #@66
    .line 2216
    :array_66
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0x7t
    .end array-data
.end method

.method protected constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V
    .registers 15
    .parameter "text"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "spacingMult"
    .parameter "spacingAdd"

    #@0
    .prologue
    .line 339
    sget-object v5, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move v3, p3

    #@6
    move-object v4, p4

    #@7
    move v6, p5

    #@8
    move v7, p6

    #@9
    invoke-direct/range {v0 .. v7}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FF)V

    #@c
    .line 341
    return-void
.end method

.method protected constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FF)V
    .registers 11
    .parameter "text"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "textDir"
    .parameter "spacingMult"
    .parameter "spacingAdd"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 360
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 2181
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@6
    iput-object v0, p0, Landroid/text/Layout;->mAlignment:Landroid/text/Layout$Alignment;

    #@8
    .line 2224
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@b
    .line 362
    if-gez p3, :cond_2c

    #@d
    .line 363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "Layout: "

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v2, " < 0"

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v0

    #@2c
    .line 369
    :cond_2c
    if-eqz p2, :cond_32

    #@2e
    .line 370
    iput v1, p2, Landroid/text/TextPaint;->bgColor:I

    #@30
    .line 371
    iput v1, p2, Landroid/text/TextPaint;->baselineShift:I

    #@32
    .line 374
    :cond_32
    iput-object p1, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@34
    .line 375
    iput-object p2, p0, Landroid/text/Layout;->mPaint:Landroid/text/TextPaint;

    #@36
    .line 376
    new-instance v0, Landroid/text/TextPaint;

    #@38
    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    #@3b
    iput-object v0, p0, Landroid/text/Layout;->mWorkPaint:Landroid/text/TextPaint;

    #@3d
    .line 377
    iput p3, p0, Landroid/text/Layout;->mWidth:I

    #@3f
    .line 378
    iput-object p4, p0, Landroid/text/Layout;->mAlignment:Landroid/text/Layout$Alignment;

    #@41
    .line 379
    iput p6, p0, Landroid/text/Layout;->mSpacingMult:F

    #@43
    .line 380
    iput p7, p0, Landroid/text/Layout;->mSpacingAdd:F

    #@45
    .line 381
    instance-of v0, p1, Landroid/text/Spanned;

    #@47
    iput-boolean v0, p0, Landroid/text/Layout;->mSpannedText:Z

    #@49
    .line 382
    iput-object p5, p0, Landroid/text/Layout;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@4b
    .line 383
    return-void
.end method

.method static synthetic access$000(Landroid/text/Layout;III[CILandroid/text/TextUtils$TruncateAt;)V
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p6}, Landroid/text/Layout;->ellipsize(III[CILandroid/text/TextUtils$TruncateAt;)V

    #@3
    return-void
.end method

.method private addSelection(IIIIILandroid/graphics/Path;)V
    .registers 26
    .parameter "line"
    .parameter "start"
    .parameter "end"
    .parameter "top"
    .parameter "bottom"
    .parameter "dest"

    #@0
    .prologue
    .line 1614
    invoke-virtual/range {p0 .. p1}, Landroid/text/Layout;->getLineStart(I)I

    #@3
    move-result v16

    #@4
    .line 1615
    .local v16, linestart:I
    invoke-virtual/range {p0 .. p1}, Landroid/text/Layout;->getLineEnd(I)I

    #@7
    move-result v15

    #@8
    .line 1616
    .local v15, lineend:I
    invoke-virtual/range {p0 .. p1}, Landroid/text/Layout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@b
    move-result-object v9

    #@c
    .line 1618
    .local v9, dirs:Landroid/text/Layout$Directions;
    move/from16 v0, v16

    #@e
    if-le v15, v0, :cond_20

    #@10
    move-object/from16 v0, p0

    #@12
    iget-object v3, v0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@14
    add-int/lit8 v5, v15, -0x1

    #@16
    invoke-interface {v3, v5}, Ljava/lang/CharSequence;->charAt(I)C

    #@19
    move-result v3

    #@1a
    const/16 v5, 0xa

    #@1c
    if-ne v3, v5, :cond_20

    #@1e
    .line 1619
    add-int/lit8 v15, v15, -0x1

    #@20
    .line 1621
    :cond_20
    const/4 v14, 0x0

    #@21
    .local v14, i:I
    :goto_21
    iget-object v3, v9, Landroid/text/Layout$Directions;->mDirections:[I

    #@23
    array-length v3, v3

    #@24
    if-ge v14, v3, :cond_86

    #@26
    .line 1622
    iget-object v3, v9, Landroid/text/Layout$Directions;->mDirections:[I

    #@28
    aget v3, v3, v14

    #@2a
    add-int v13, v16, v3

    #@2c
    .line 1623
    .local v13, here:I
    iget-object v3, v9, Landroid/text/Layout$Directions;->mDirections:[I

    #@2e
    add-int/lit8 v5, v14, 0x1

    #@30
    aget v3, v3, v5

    #@32
    const v5, 0x3ffffff

    #@35
    and-int/2addr v3, v5

    #@36
    add-int v18, v13, v3

    #@38
    .line 1625
    .local v18, there:I
    move/from16 v0, v18

    #@3a
    if-le v0, v15, :cond_3e

    #@3c
    .line 1626
    move/from16 v18, v15

    #@3e
    .line 1628
    :cond_3e
    move/from16 v0, p2

    #@40
    move/from16 v1, v18

    #@42
    if-gt v0, v1, :cond_83

    #@44
    move/from16 v0, p3

    #@46
    if-lt v0, v13, :cond_83

    #@48
    .line 1629
    move/from16 v0, p2

    #@4a
    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    #@4d
    move-result v17

    #@4e
    .line 1630
    .local v17, st:I
    move/from16 v0, p3

    #@50
    move/from16 v1, v18

    #@52
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@55
    move-result v10

    #@56
    .line 1632
    .local v10, en:I
    move/from16 v0, v17

    #@58
    if-eq v0, v10, :cond_83

    #@5a
    .line 1633
    const/4 v3, 0x0

    #@5b
    move-object/from16 v0, p0

    #@5d
    move/from16 v1, v17

    #@5f
    move/from16 v2, p1

    #@61
    invoke-direct {v0, v1, v3, v2}, Landroid/text/Layout;->getHorizontal(IZI)F

    #@64
    move-result v11

    #@65
    .line 1634
    .local v11, h1:F
    const/4 v3, 0x1

    #@66
    move-object/from16 v0, p0

    #@68
    move/from16 v1, p1

    #@6a
    invoke-direct {v0, v10, v3, v1}, Landroid/text/Layout;->getHorizontal(IZI)F

    #@6d
    move-result v12

    #@6e
    .line 1636
    .local v12, h2:F
    invoke-static {v11, v12}, Ljava/lang/Math;->min(FF)F

    #@71
    move-result v4

    #@72
    .line 1637
    .local v4, left:F
    invoke-static {v11, v12}, Ljava/lang/Math;->max(FF)F

    #@75
    move-result v6

    #@76
    .line 1639
    .local v6, right:F
    move/from16 v0, p4

    #@78
    int-to-float v5, v0

    #@79
    move/from16 v0, p5

    #@7b
    int-to-float v7, v0

    #@7c
    sget-object v8, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@7e
    move-object/from16 v3, p6

    #@80
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    #@83
    .line 1621
    .end local v4           #left:F
    .end local v6           #right:F
    .end local v10           #en:I
    .end local v11           #h1:F
    .end local v12           #h2:F
    .end local v17           #st:I
    :cond_83
    add-int/lit8 v14, v14, 0x2

    #@85
    goto :goto_21

    #@86
    .line 1643
    .end local v13           #here:I
    .end local v18           #there:I
    :cond_86
    return-void
.end method

.method private ellipsize(III[CILandroid/text/TextUtils$TruncateAt;)V
    .registers 20
    .parameter "start"
    .parameter "end"
    .parameter "line"
    .parameter "dest"
    .parameter "destoff"
    .parameter "method"

    #@0
    .prologue
    .line 2001
    move/from16 v0, p3

    #@2
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getEllipsisCount(I)I

    #@5
    move-result v3

    #@6
    .line 2003
    .local v3, ellipsisCount:I
    if-nez v3, :cond_9

    #@8
    .line 2051
    :cond_8
    return-void

    #@9
    .line 2007
    :cond_9
    move/from16 v0, p3

    #@b
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getEllipsisStart(I)I

    #@e
    move-result v4

    #@f
    .line 2008
    .local v4, ellipsisStart:I
    move/from16 v0, p3

    #@11
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineStart(I)I

    #@14
    move-result v9

    #@15
    .line 2010
    .local v9, linestart:I
    move v5, v4

    #@16
    .local v5, i:I
    :goto_16
    add-int v11, v4, v3

    #@18
    if-ge v5, v11, :cond_8

    #@1a
    .line 2013
    if-ne v5, v4, :cond_53

    #@1c
    .line 2014
    move-object/from16 v0, p6

    #@1e
    invoke-direct {p0, v0}, Landroid/text/Layout;->getEllipsisChar(Landroid/text/TextUtils$TruncateAt;)C

    #@21
    move-result v2

    #@22
    .line 2019
    .local v2, c:C
    :goto_22
    add-int v1, v5, v9

    #@24
    .line 2021
    .local v1, a:I
    if-lt v1, p1, :cond_7d

    #@26
    if-ge v1, p2, :cond_7d

    #@28
    .line 2023
    sget-boolean v11, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2a
    if-eqz v11, :cond_86

    #@2c
    .line 2024
    add-int v11, p5, v1

    #@2e
    sub-int v10, v11, p1

    #@30
    .line 2025
    .local v10, offset:I
    const/4 v6, 0x0

    #@31
    .line 2026
    .local v6, isCountryCodeEmoji:Z
    const/4 v7, 0x1

    #@32
    .local v7, j:I
    :goto_32
    const/4 v11, 0x3

    #@33
    if-gt v7, v11, :cond_5a

    #@35
    .line 2027
    if-gt v7, v10, :cond_57

    #@37
    sub-int v11, v10, v7

    #@39
    move-object/from16 v0, p4

    #@3b
    invoke-static {v0, v11}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z

    #@3e
    move-result v11

    #@3f
    if-eqz v11, :cond_57

    #@41
    .line 2028
    const/4 v6, 0x1

    #@42
    .line 2029
    sub-int v11, v10, v7

    #@44
    aput-char v2, p4, v11

    #@46
    .line 2030
    const/4 v8, 0x0

    #@47
    .local v8, k:I
    :goto_47
    if-ge v8, v7, :cond_5a

    #@49
    .line 2031
    sub-int v11, v10, v8

    #@4b
    const v12, 0xfeff

    #@4e
    aput-char v12, p4, v11

    #@50
    .line 2030
    add-int/lit8 v8, v8, 0x1

    #@52
    goto :goto_47

    #@53
    .line 2016
    .end local v1           #a:I
    .end local v2           #c:C
    .end local v6           #isCountryCodeEmoji:Z
    .end local v7           #j:I
    .end local v8           #k:I
    .end local v10           #offset:I
    :cond_53
    const v2, 0xfeff

    #@56
    .restart local v2       #c:C
    goto :goto_22

    #@57
    .line 2026
    .restart local v1       #a:I
    .restart local v6       #isCountryCodeEmoji:Z
    .restart local v7       #j:I
    .restart local v10       #offset:I
    :cond_57
    add-int/lit8 v7, v7, 0x1

    #@59
    goto :goto_32

    #@5a
    .line 2036
    :cond_5a
    if-nez v6, :cond_7d

    #@5c
    .line 2037
    if-lez v10, :cond_80

    #@5e
    add-int/lit8 v11, v10, -0x1

    #@60
    move-object/from16 v0, p4

    #@62
    invoke-static {v0, v11}, Landroid/text/Layout;->isDiacriticalMark([CI)Z

    #@65
    move-result v11

    #@66
    if-nez v11, :cond_74

    #@68
    add-int/lit8 v11, v10, -0x1

    #@6a
    aget-char v11, p4, v11

    #@6c
    aget-char v12, p4, v10

    #@6e
    invoke-static {v11, v12}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    #@71
    move-result v11

    #@72
    if-eqz v11, :cond_80

    #@74
    .line 2039
    :cond_74
    add-int/lit8 v11, v10, -0x1

    #@76
    aput-char v2, p4, v11

    #@78
    .line 2040
    const v11, 0xfeff

    #@7b
    aput-char v11, p4, v10

    #@7d
    .line 2010
    .end local v6           #isCountryCodeEmoji:Z
    .end local v7           #j:I
    .end local v10           #offset:I
    :cond_7d
    :goto_7d
    add-int/lit8 v5, v5, 0x1

    #@7f
    goto :goto_16

    #@80
    .line 2042
    .restart local v6       #isCountryCodeEmoji:Z
    .restart local v7       #j:I
    .restart local v10       #offset:I
    :cond_80
    add-int v11, p5, v1

    #@82
    sub-int/2addr v11, p1

    #@83
    aput-char v2, p4, v11

    #@85
    goto :goto_7d

    #@86
    .line 2047
    .end local v6           #isCountryCodeEmoji:Z
    .end local v7           #j:I
    .end local v10           #offset:I
    :cond_86
    add-int v11, p5, v1

    #@88
    sub-int/2addr v11, p1

    #@89
    aput-char v2, p4, v11

    #@8b
    goto :goto_7d
.end method

.method public static getDesiredWidth(Ljava/lang/CharSequence;IILandroid/text/TextPaint;)F
    .registers 9
    .parameter "source"
    .parameter "start"
    .parameter "end"
    .parameter "paint"

    #@0
    .prologue
    .line 302
    const/4 v1, 0x0

    #@1
    .line 305
    .local v1, need:F
    move v0, p1

    #@2
    .local v0, i:I
    :goto_2
    if-gt v0, p2, :cond_1a

    #@4
    .line 306
    const/16 v4, 0xa

    #@6
    invoke-static {p0, v4, v0, p2}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CII)I

    #@9
    move-result v2

    #@a
    .line 308
    .local v2, next:I
    if-gez v2, :cond_d

    #@c
    .line 309
    move v2, p2

    #@d
    .line 312
    :cond_d
    invoke-static {p3, p0, v0, v2}, Landroid/text/Layout;->measurePara(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)F

    #@10
    move-result v3

    #@11
    .line 314
    .local v3, w:F
    cmpl-float v4, v3, v1

    #@13
    if-lez v4, :cond_16

    #@15
    .line 315
    move v1, v3

    #@16
    .line 317
    :cond_16
    add-int/lit8 v2, v2, 0x1

    #@18
    .line 305
    move v0, v2

    #@19
    goto :goto_2

    #@1a
    .line 320
    .end local v2           #next:I
    .end local v3           #w:F
    :cond_1a
    return v1
.end method

.method public static getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F
    .registers 4
    .parameter "source"
    .parameter "paint"

    #@0
    .prologue
    .line 292
    const/4 v0, 0x0

    #@1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v1

    #@5
    invoke-static {p0, v0, v1, p1}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;IILandroid/text/TextPaint;)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method private getEllipsisChar(Landroid/text/TextUtils$TruncateAt;)C
    .registers 4
    .parameter "method"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1994
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END_SMALL:Landroid/text/TextUtils$TruncateAt;

    #@3
    if-ne p1, v0, :cond_a

    #@5
    sget-object v0, Landroid/text/Layout;->ELLIPSIS_TWO_DOTS:[C

    #@7
    aget-char v0, v0, v1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    sget-object v0, Landroid/text/Layout;->ELLIPSIS_NORMAL:[C

    #@c
    aget-char v0, v0, v1

    #@e
    goto :goto_9
.end method

.method public static getEmojiFactory()Landroid/emoji/EmojiFactory;
    .registers 1

    #@0
    .prologue
    .line 70
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 71
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@6
    .line 73
    :goto_6
    return-object v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method private getHorizontal(IZ)F
    .registers 5
    .parameter "offset"
    .parameter "trailing"

    #@0
    .prologue
    .line 1041
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@3
    move-result v0

    #@4
    .line 1043
    .local v0, line:I
    invoke-direct {p0, p1, p2, v0}, Landroid/text/Layout;->getHorizontal(IZI)F

    #@7
    move-result v1

    #@8
    return v1
.end method

.method private getHorizontal(IZI)F
    .registers 18
    .parameter "offset"
    .parameter "trailing"
    .parameter "line"

    #@0
    .prologue
    .line 1047
    move/from16 v0, p3

    #@2
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineStart(I)I

    #@5
    move-result v4

    #@6
    .line 1048
    .local v4, start:I
    move/from16 v0, p3

    #@8
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineEnd(I)I

    #@b
    move-result v5

    #@c
    .line 1049
    .local v5, end:I
    move/from16 v0, p3

    #@e
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@11
    move-result v6

    #@12
    .line 1051
    .local v6, dir:I
    move/from16 v0, p3

    #@14
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineContainsTab(I)Z

    #@17
    move-result v2

    #@18
    if-nez v2, :cond_22

    #@1a
    move/from16 v0, p3

    #@1c
    invoke-direct {p0, v0}, Landroid/text/Layout;->getLineContainsEmoji(I)Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_74

    #@22
    :cond_22
    const/4 v8, 0x1

    #@23
    .line 1052
    .local v8, hasTabOrEmoji:Z
    :goto_23
    move/from16 v0, p3

    #@25
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@28
    move-result-object v7

    #@29
    .line 1054
    .local v7, directions:Landroid/text/Layout$Directions;
    const/4 v9, 0x0

    #@2a
    .line 1055
    .local v9, tabStops:Landroid/text/Layout$TabStops;
    if-eqz v8, :cond_48

    #@2c
    iget-object v2, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@2e
    instance-of v2, v2, Landroid/text/Spanned;

    #@30
    if-eqz v2, :cond_48

    #@32
    .line 1058
    iget-object v2, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@34
    check-cast v2, Landroid/text/Spanned;

    #@36
    const-class v3, Landroid/text/style/TabStopSpan;

    #@38
    invoke-static {v2, v4, v5, v3}, Landroid/text/Layout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@3b
    move-result-object v12

    #@3c
    check-cast v12, [Landroid/text/style/TabStopSpan;

    #@3e
    .line 1059
    .local v12, tabs:[Landroid/text/style/TabStopSpan;
    array-length v2, v12

    #@3f
    if-lez v2, :cond_48

    #@41
    .line 1060
    new-instance v9, Landroid/text/Layout$TabStops;

    #@43
    .end local v9           #tabStops:Landroid/text/Layout$TabStops;
    const/16 v2, 0x14

    #@45
    invoke-direct {v9, v2, v12}, Landroid/text/Layout$TabStops;-><init>(I[Ljava/lang/Object;)V

    #@48
    .line 1064
    .end local v12           #tabs:[Landroid/text/style/TabStopSpan;
    .restart local v9       #tabStops:Landroid/text/Layout$TabStops;
    :cond_48
    invoke-static {}, Landroid/text/TextLine;->obtain()Landroid/text/TextLine;

    #@4b
    move-result-object v1

    #@4c
    .line 1065
    .local v1, tl:Landroid/text/TextLine;
    iget-object v2, p0, Landroid/text/Layout;->mPaint:Landroid/text/TextPaint;

    #@4e
    iget-object v3, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@50
    invoke-virtual/range {v1 .. v9}, Landroid/text/TextLine;->set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;)V

    #@53
    .line 1066
    sub-int v2, p1, v4

    #@55
    const/4 v3, 0x0

    #@56
    move/from16 v0, p2

    #@58
    invoke-virtual {v1, v2, v0, v3}, Landroid/text/TextLine;->measure(IZLandroid/graphics/Paint$FontMetricsInt;)F

    #@5b
    move-result v13

    #@5c
    .line 1067
    .local v13, wid:F
    invoke-static {v1}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    #@5f
    .line 1069
    move/from16 v0, p3

    #@61
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getParagraphLeft(I)I

    #@64
    move-result v10

    #@65
    .line 1070
    .local v10, left:I
    move/from16 v0, p3

    #@67
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getParagraphRight(I)I

    #@6a
    move-result v11

    #@6b
    .line 1072
    .local v11, right:I
    move/from16 v0, p3

    #@6d
    invoke-direct {p0, v0, v10, v11}, Landroid/text/Layout;->getLineStartPos(III)I

    #@70
    move-result v2

    #@71
    int-to-float v2, v2

    #@72
    add-float/2addr v2, v13

    #@73
    return v2

    #@74
    .line 1051
    .end local v1           #tl:Landroid/text/TextLine;
    .end local v7           #directions:Landroid/text/Layout$Directions;
    .end local v8           #hasTabOrEmoji:Z
    .end local v9           #tabStops:Landroid/text/Layout$TabStops;
    .end local v10           #left:I
    .end local v11           #right:I
    .end local v13           #wid:F
    :cond_74
    const/4 v8, 0x0

    #@75
    goto :goto_23
.end method

.method private getLineContainsEmoji(I)Z
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 2228
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    iget-object v0, p0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@6
    if-eqz v0, :cond_d

    #@8
    iget-object v0, p0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@a
    array-length v0, v0

    #@b
    if-lt p1, v0, :cond_f

    #@d
    .line 2229
    :cond_d
    const/4 v0, 0x0

    #@e
    .line 2231
    :goto_e
    return v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@11
    aget-boolean v0, v0, p1

    #@13
    goto :goto_e
.end method

.method private getLineExtent(ILandroid/text/Layout$TabStops;Z)F
    .registers 14
    .parameter "line"
    .parameter "tabStops"
    .parameter "full"

    #@0
    .prologue
    .line 1202
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineStart(I)I

    #@3
    move-result v3

    #@4
    .line 1203
    .local v3, start:I
    if-eqz p3, :cond_34

    #@6
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineEnd(I)I

    #@9
    move-result v4

    #@a
    .line 1205
    .local v4, end:I
    :goto_a
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineContainsTab(I)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_16

    #@10
    invoke-direct {p0, p1}, Landroid/text/Layout;->getLineContainsEmoji(I)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_39

    #@16
    :cond_16
    const/4 v7, 0x1

    #@17
    .line 1206
    .local v7, hasTabsOrEmoji:Z
    :goto_17
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@1a
    move-result-object v6

    #@1b
    .line 1207
    .local v6, directions:Landroid/text/Layout$Directions;
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@1e
    move-result v5

    #@1f
    .line 1209
    .local v5, dir:I
    invoke-static {}, Landroid/text/TextLine;->obtain()Landroid/text/TextLine;

    #@22
    move-result-object v0

    #@23
    .line 1210
    .local v0, tl:Landroid/text/TextLine;
    iget-object v1, p0, Landroid/text/Layout;->mPaint:Landroid/text/TextPaint;

    #@25
    iget-object v2, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@27
    move-object v8, p2

    #@28
    invoke-virtual/range {v0 .. v8}, Landroid/text/TextLine;->set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;)V

    #@2b
    .line 1211
    const/4 v1, 0x0

    #@2c
    invoke-virtual {v0, v1}, Landroid/text/TextLine;->metrics(Landroid/graphics/Paint$FontMetricsInt;)F

    #@2f
    move-result v9

    #@30
    .line 1212
    .local v9, width:F
    invoke-static {v0}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    #@33
    .line 1213
    return v9

    #@34
    .line 1203
    .end local v0           #tl:Landroid/text/TextLine;
    .end local v4           #end:I
    .end local v5           #dir:I
    .end local v6           #directions:Landroid/text/Layout$Directions;
    .end local v7           #hasTabsOrEmoji:Z
    .end local v9           #width:F
    :cond_34
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineVisibleEnd(I)I

    #@37
    move-result v4

    #@38
    goto :goto_a

    #@39
    .line 1205
    .restart local v4       #end:I
    :cond_39
    const/4 v7, 0x0

    #@3a
    goto :goto_17
.end method

.method private getLineExtent(IZ)F
    .registers 14
    .parameter "line"
    .parameter "full"

    #@0
    .prologue
    .line 1165
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineStart(I)I

    #@3
    move-result v3

    #@4
    .line 1166
    .local v3, start:I
    if-eqz p2, :cond_3e

    #@6
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineEnd(I)I

    #@9
    move-result v4

    #@a
    .line 1169
    .local v4, end:I
    :goto_a
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineContainsTab(I)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_16

    #@10
    invoke-direct {p0, p1}, Landroid/text/Layout;->getLineContainsEmoji(I)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_43

    #@16
    :cond_16
    const/4 v7, 0x1

    #@17
    .line 1170
    .local v7, hasTabsOrEmoji:Z
    :goto_17
    const/4 v8, 0x0

    #@18
    .line 1171
    .local v8, tabStops:Landroid/text/Layout$TabStops;
    if-eqz v7, :cond_36

    #@1a
    iget-object v1, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@1c
    instance-of v1, v1, Landroid/text/Spanned;

    #@1e
    if-eqz v1, :cond_36

    #@20
    .line 1174
    iget-object v1, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@22
    check-cast v1, Landroid/text/Spanned;

    #@24
    const-class v2, Landroid/text/style/TabStopSpan;

    #@26
    invoke-static {v1, v3, v4, v2}, Landroid/text/Layout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@29
    move-result-object v9

    #@2a
    check-cast v9, [Landroid/text/style/TabStopSpan;

    #@2c
    .line 1175
    .local v9, tabs:[Landroid/text/style/TabStopSpan;
    array-length v1, v9

    #@2d
    if-lez v1, :cond_36

    #@2f
    .line 1176
    new-instance v8, Landroid/text/Layout$TabStops;

    #@31
    .end local v8           #tabStops:Landroid/text/Layout$TabStops;
    const/16 v1, 0x14

    #@33
    invoke-direct {v8, v1, v9}, Landroid/text/Layout$TabStops;-><init>(I[Ljava/lang/Object;)V

    #@36
    .line 1179
    .end local v9           #tabs:[Landroid/text/style/TabStopSpan;
    .restart local v8       #tabStops:Landroid/text/Layout$TabStops;
    :cond_36
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@39
    move-result-object v6

    #@3a
    .line 1181
    .local v6, directions:Landroid/text/Layout$Directions;
    if-nez v6, :cond_45

    #@3c
    .line 1182
    const/4 v10, 0x0

    #@3d
    .line 1190
    :goto_3d
    return v10

    #@3e
    .line 1166
    .end local v4           #end:I
    .end local v6           #directions:Landroid/text/Layout$Directions;
    .end local v7           #hasTabsOrEmoji:Z
    .end local v8           #tabStops:Landroid/text/Layout$TabStops;
    :cond_3e
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineVisibleEnd(I)I

    #@41
    move-result v4

    #@42
    goto :goto_a

    #@43
    .line 1169
    .restart local v4       #end:I
    :cond_43
    const/4 v7, 0x0

    #@44
    goto :goto_17

    #@45
    .line 1184
    .restart local v6       #directions:Landroid/text/Layout$Directions;
    .restart local v7       #hasTabsOrEmoji:Z
    .restart local v8       #tabStops:Landroid/text/Layout$TabStops;
    :cond_45
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@48
    move-result v5

    #@49
    .line 1186
    .local v5, dir:I
    invoke-static {}, Landroid/text/TextLine;->obtain()Landroid/text/TextLine;

    #@4c
    move-result-object v0

    #@4d
    .line 1187
    .local v0, tl:Landroid/text/TextLine;
    iget-object v1, p0, Landroid/text/Layout;->mPaint:Landroid/text/TextPaint;

    #@4f
    iget-object v2, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@51
    invoke-virtual/range {v0 .. v8}, Landroid/text/TextLine;->set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;)V

    #@54
    .line 1188
    const/4 v1, 0x0

    #@55
    invoke-virtual {v0, v1}, Landroid/text/TextLine;->metrics(Landroid/graphics/Paint$FontMetricsInt;)F

    #@58
    move-result v10

    #@59
    .line 1189
    .local v10, width:F
    invoke-static {v0}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    #@5c
    goto :goto_3d
.end method

.method private getLineStartPos(III)I
    .registers 16
    .parameter "line"
    .parameter "left"
    .parameter "right"

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    .line 706
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphAlignment(I)Landroid/text/Layout$Alignment;

    #@4
    move-result-object v0

    #@5
    .line 707
    .local v0, align:Landroid/text/Layout$Alignment;
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@8
    move-result v1

    #@9
    .line 709
    .local v1, dir:I
    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    #@b
    if-ne v0, v9, :cond_1c

    #@d
    .line 710
    if-ne v1, v11, :cond_19

    #@f
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@11
    .line 716
    :cond_11
    :goto_11
    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@13
    if-ne v0, v9, :cond_2a

    #@15
    .line 717
    if-ne v1, v11, :cond_28

    #@17
    .line 718
    move v8, p2

    #@18
    .line 748
    .local v8, x:I
    :goto_18
    return v8

    #@19
    .line 710
    .end local v8           #x:I
    :cond_19
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@1b
    goto :goto_11

    #@1c
    .line 711
    :cond_1c
    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@1e
    if-ne v0, v9, :cond_11

    #@20
    .line 712
    if-ne v1, v11, :cond_25

    #@22
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@24
    :goto_24
    goto :goto_11

    #@25
    :cond_25
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@27
    goto :goto_24

    #@28
    .line 720
    :cond_28
    move v8, p3

    #@29
    .restart local v8       #x:I
    goto :goto_18

    #@2a
    .line 723
    .end local v8           #x:I
    :cond_2a
    const/4 v7, 0x0

    #@2b
    .line 724
    .local v7, tabStops:Landroid/text/Layout$TabStops;
    iget-boolean v9, p0, Landroid/text/Layout;->mSpannedText:Z

    #@2d
    if-eqz v9, :cond_59

    #@2f
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineContainsTab(I)Z

    #@32
    move-result v9

    #@33
    if-eqz v9, :cond_59

    #@35
    .line 725
    iget-object v4, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@37
    check-cast v4, Landroid/text/Spanned;

    #@39
    .line 726
    .local v4, spanned:Landroid/text/Spanned;
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineStart(I)I

    #@3c
    move-result v5

    #@3d
    .line 727
    .local v5, start:I
    invoke-interface {v4}, Landroid/text/Spanned;->length()I

    #@40
    move-result v9

    #@41
    const-class v10, Landroid/text/style/TabStopSpan;

    #@43
    invoke-interface {v4, v5, v9, v10}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    #@46
    move-result v3

    #@47
    .line 729
    .local v3, spanEnd:I
    const-class v9, Landroid/text/style/TabStopSpan;

    #@49
    invoke-static {v4, v5, v3, v9}, Landroid/text/Layout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@4c
    move-result-object v6

    #@4d
    check-cast v6, [Landroid/text/style/TabStopSpan;

    #@4f
    .line 731
    .local v6, tabSpans:[Landroid/text/style/TabStopSpan;
    array-length v9, v6

    #@50
    if-lez v9, :cond_59

    #@52
    .line 732
    new-instance v7, Landroid/text/Layout$TabStops;

    #@54
    .end local v7           #tabStops:Landroid/text/Layout$TabStops;
    const/16 v9, 0x14

    #@56
    invoke-direct {v7, v9, v6}, Landroid/text/Layout$TabStops;-><init>(I[Ljava/lang/Object;)V

    #@59
    .line 735
    .end local v3           #spanEnd:I
    .end local v4           #spanned:Landroid/text/Spanned;
    .end local v5           #start:I
    .end local v6           #tabSpans:[Landroid/text/style/TabStopSpan;
    .restart local v7       #tabStops:Landroid/text/Layout$TabStops;
    :cond_59
    const/4 v9, 0x0

    #@5a
    invoke-direct {p0, p1, v7, v9}, Landroid/text/Layout;->getLineExtent(ILandroid/text/Layout$TabStops;Z)F

    #@5d
    move-result v9

    #@5e
    float-to-int v2, v9

    #@5f
    .line 736
    .local v2, max:I
    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@61
    if-ne v0, v9, :cond_6b

    #@63
    .line 737
    if-ne v1, v11, :cond_68

    #@65
    .line 738
    sub-int v8, p3, v2

    #@67
    .restart local v8       #x:I
    goto :goto_18

    #@68
    .line 741
    .end local v8           #x:I
    :cond_68
    sub-int v8, p2, v2

    #@6a
    .restart local v8       #x:I
    goto :goto_18

    #@6b
    .line 744
    .end local v8           #x:I
    :cond_6b
    and-int/lit8 v2, v2, -0x2

    #@6d
    .line 745
    add-int v9, p2, p3

    #@6f
    sub-int/2addr v9, v2

    #@70
    shr-int/lit8 v8, v9, 0x1

    #@72
    .restart local v8       #x:I
    goto :goto_18
.end method

.method private getLineVisibleEnd(III)I
    .registers 7
    .parameter "line"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1368
    iget-object v1, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@2
    .line 1370
    .local v1, text:Ljava/lang/CharSequence;
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v2, v2, -0x1

    #@8
    if-ne p1, v2, :cond_e

    #@a
    move v2, p3

    #@b
    .line 1387
    :goto_b
    return v2

    #@c
    .line 1374
    .local v0, ch:C
    :cond_c
    add-int/lit8 p3, p3, -0x1

    #@e
    .end local v0           #ch:C
    :cond_e
    if-le p3, p2, :cond_25

    #@10
    .line 1375
    add-int/lit8 v2, p3, -0x1

    #@12
    invoke-interface {v1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@15
    move-result v0

    #@16
    .line 1377
    .restart local v0       #ch:C
    const/16 v2, 0xa

    #@18
    if-ne v0, v2, :cond_1d

    #@1a
    .line 1378
    add-int/lit8 v2, p3, -0x1

    #@1c
    goto :goto_b

    #@1d
    .line 1381
    :cond_1d
    const/16 v2, 0x20

    #@1f
    if-eq v0, v2, :cond_c

    #@21
    const/16 v2, 0x9

    #@23
    if-eq v0, v2, :cond_c

    #@25
    .end local v0           #ch:C
    :cond_25
    move v2, p3

    #@26
    .line 1387
    goto :goto_b
.end method

.method private getOffsetAtStartOf(I)I
    .registers 15
    .parameter "offset"

    #@0
    .prologue
    const v12, 0xdfff

    #@3
    const v11, 0xdc00

    #@6
    const v10, 0xdbff

    #@9
    const v9, 0xd800

    #@c
    .line 1477
    if-nez p1, :cond_10

    #@e
    .line 1478
    const/4 v8, 0x0

    #@f
    .line 1529
    :goto_f
    return v8

    #@10
    .line 1480
    :cond_10
    iget-object v7, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@12
    .line 1481
    .local v7, text:Ljava/lang/CharSequence;
    invoke-interface {v7, p1}, Ljava/lang/CharSequence;->charAt(I)C

    #@15
    move-result v0

    #@16
    .line 1483
    .local v0, c:C
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@18
    if-eqz v8, :cond_a7

    #@1a
    .line 1484
    add-int/lit8 v8, p1, -0x1

    #@1c
    invoke-static {v7, v8}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@1f
    move-result v8

    #@20
    if-eqz v8, :cond_51

    #@22
    .line 1485
    add-int/lit8 p1, p1, -0x1

    #@24
    .line 1516
    :cond_24
    :goto_24
    iget-boolean v8, p0, Landroid/text/Layout;->mSpannedText:Z

    #@26
    if-eqz v8, :cond_b9

    #@28
    move-object v8, v7

    #@29
    .line 1517
    check-cast v8, Landroid/text/Spanned;

    #@2b
    const-class v9, Landroid/text/style/ReplacementSpan;

    #@2d
    invoke-interface {v8, p1, p1, v9}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@30
    move-result-object v5

    #@31
    check-cast v5, [Landroid/text/style/ReplacementSpan;

    #@33
    .line 1520
    .local v5, spans:[Landroid/text/style/ReplacementSpan;
    const/4 v4, 0x0

    #@34
    .local v4, i:I
    :goto_34
    array-length v8, v5

    #@35
    if-ge v4, v8, :cond_b9

    #@37
    move-object v8, v7

    #@38
    .line 1521
    check-cast v8, Landroid/text/Spanned;

    #@3a
    aget-object v9, v5, v4

    #@3c
    invoke-interface {v8, v9}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@3f
    move-result v6

    #@40
    .local v6, start:I
    move-object v8, v7

    #@41
    .line 1522
    check-cast v8, Landroid/text/Spanned;

    #@43
    aget-object v9, v5, v4

    #@45
    invoke-interface {v8, v9}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@48
    move-result v3

    #@49
    .line 1524
    .local v3, end:I
    if-ge v6, p1, :cond_4e

    #@4b
    if-le v3, p1, :cond_4e

    #@4d
    .line 1525
    move p1, v6

    #@4e
    .line 1520
    :cond_4e
    add-int/lit8 v4, v4, 0x1

    #@50
    goto :goto_34

    #@51
    .line 1486
    .end local v3           #end:I
    .end local v4           #i:I
    .end local v5           #spans:[Landroid/text/style/ReplacementSpan;
    .end local v6           #start:I
    :cond_51
    add-int/lit8 v8, p1, -0x2

    #@53
    invoke-static {v7, v8}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@56
    move-result v8

    #@57
    if-eqz v8, :cond_5c

    #@59
    .line 1487
    add-int/lit8 p1, p1, -0x2

    #@5b
    goto :goto_24

    #@5c
    .line 1488
    :cond_5c
    add-int/lit8 v8, p1, -0x3

    #@5e
    invoke-static {v7, v8}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@61
    move-result v8

    #@62
    if-eqz v8, :cond_67

    #@64
    .line 1489
    add-int/lit8 p1, p1, -0x3

    #@66
    goto :goto_24

    #@67
    .line 1490
    :cond_67
    add-int/lit8 v8, p1, -0x4

    #@69
    invoke-static {v7, v8}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@6c
    move-result v8

    #@6d
    if-eqz v8, :cond_72

    #@6f
    .line 1491
    add-int/lit8 p1, p1, -0x4

    #@71
    goto :goto_24

    #@72
    .line 1493
    :cond_72
    if-lt v0, v11, :cond_93

    #@74
    if-gt v0, v12, :cond_93

    #@76
    .line 1494
    add-int/lit8 v8, p1, -0x1

    #@78
    invoke-static {v7, v8}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    #@7b
    move-result v2

    #@7c
    .line 1496
    .local v2, codept:I
    sget-object v8, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@7e
    sget-object v8, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@80
    invoke-virtual {v8, v2}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInCountryCodeRange(I)Z

    #@83
    move-result v8

    #@84
    if-nez v8, :cond_24

    #@86
    .line 1497
    add-int/lit8 v8, p1, -0x1

    #@88
    invoke-interface {v7, v8}, Ljava/lang/CharSequence;->charAt(I)C

    #@8b
    move-result v1

    #@8c
    .line 1499
    .local v1, c1:C
    if-lt v1, v9, :cond_24

    #@8e
    if-gt v1, v10, :cond_24

    #@90
    .line 1500
    add-int/lit8 p1, p1, -0x1

    #@92
    goto :goto_24

    #@93
    .line 1503
    .end local v1           #c1:C
    .end local v2           #codept:I
    :cond_93
    const/16 v8, 0x20e3

    #@95
    if-ne v0, v8, :cond_24

    #@97
    add-int/lit8 v8, p1, -0x1

    #@99
    if-ltz v8, :cond_24

    #@9b
    add-int/lit8 v8, p1, -0x1

    #@9d
    invoke-static {v7, v8}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@a0
    move-result v8

    #@a1
    if-eqz v8, :cond_24

    #@a3
    .line 1504
    add-int/lit8 p1, p1, -0x1

    #@a5
    goto/16 :goto_24

    #@a7
    .line 1508
    :cond_a7
    if-lt v0, v11, :cond_24

    #@a9
    if-gt v0, v12, :cond_24

    #@ab
    .line 1509
    add-int/lit8 v8, p1, -0x1

    #@ad
    invoke-interface {v7, v8}, Ljava/lang/CharSequence;->charAt(I)C

    #@b0
    move-result v1

    #@b1
    .line 1511
    .restart local v1       #c1:C
    if-lt v1, v9, :cond_24

    #@b3
    if-gt v1, v10, :cond_24

    #@b5
    .line 1512
    add-int/lit8 p1, p1, -0x1

    #@b7
    goto/16 :goto_24

    #@b9
    .end local v1           #c1:C
    :cond_b9
    move v8, p1

    #@ba
    .line 1529
    goto/16 :goto_f
.end method

.method private getOffsetToLeftRightOf(IZ)I
    .registers 18
    .parameter "caret"
    .parameter "toLeft"

    #@0
    .prologue
    .line 1423
    invoke-virtual/range {p0 .. p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@3
    move-result v12

    #@4
    .line 1424
    .local v12, line:I
    invoke-virtual {p0, v12}, Landroid/text/Layout;->getLineStart(I)I

    #@7
    move-result v4

    #@8
    .line 1425
    .local v4, lineStart:I
    invoke-virtual {p0, v12}, Landroid/text/Layout;->getLineEnd(I)I

    #@b
    move-result v5

    #@c
    .line 1426
    .local v5, lineEnd:I
    invoke-virtual {p0, v12}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@f
    move-result v6

    #@10
    .line 1428
    .local v6, lineDir:I
    const/4 v13, 0x0

    #@11
    .line 1429
    .local v13, lineChanged:Z
    const/4 v2, -0x1

    #@12
    if-ne v6, v2, :cond_62

    #@14
    const/4 v2, 0x1

    #@15
    :goto_15
    move/from16 v0, p2

    #@17
    if-ne v0, v2, :cond_64

    #@19
    const/4 v10, 0x1

    #@1a
    .line 1431
    .local v10, advance:Z
    :goto_1a
    if-eqz v10, :cond_69

    #@1c
    .line 1432
    move/from16 v0, p1

    #@1e
    if-ne v0, v5, :cond_2b

    #@20
    .line 1433
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    #@23
    move-result v2

    #@24
    add-int/lit8 v2, v2, -0x1

    #@26
    if-ge v12, v2, :cond_66

    #@28
    .line 1434
    const/4 v13, 0x1

    #@29
    .line 1435
    add-int/lit8 v12, v12, 0x1

    #@2b
    .line 1451
    :cond_2b
    :goto_2b
    if-eqz v13, :cond_40

    #@2d
    .line 1452
    invoke-virtual {p0, v12}, Landroid/text/Layout;->getLineStart(I)I

    #@30
    move-result v4

    #@31
    .line 1453
    invoke-virtual {p0, v12}, Landroid/text/Layout;->getLineEnd(I)I

    #@34
    move-result v5

    #@35
    .line 1454
    invoke-virtual {p0, v12}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@38
    move-result v14

    #@39
    .line 1455
    .local v14, newDir:I
    if-eq v14, v6, :cond_40

    #@3b
    .line 1459
    if-nez p2, :cond_76

    #@3d
    const/16 p2, 0x1

    #@3f
    .line 1460
    :goto_3f
    move v6, v14

    #@40
    .line 1464
    .end local v14           #newDir:I
    :cond_40
    invoke-virtual {p0, v12}, Landroid/text/Layout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@43
    move-result-object v7

    #@44
    .line 1466
    .local v7, directions:Landroid/text/Layout$Directions;
    invoke-static {}, Landroid/text/TextLine;->obtain()Landroid/text/TextLine;

    #@47
    move-result-object v1

    #@48
    .line 1468
    .local v1, tl:Landroid/text/TextLine;
    iget-object v2, p0, Landroid/text/Layout;->mPaint:Landroid/text/TextPaint;

    #@4a
    iget-object v3, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@4c
    const/4 v8, 0x0

    #@4d
    const/4 v9, 0x0

    #@4e
    invoke-virtual/range {v1 .. v9}, Landroid/text/TextLine;->set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;)V

    #@51
    .line 1469
    sub-int v2, p1, v4

    #@53
    move/from16 v0, p2

    #@55
    invoke-virtual {v1, v2, v0}, Landroid/text/TextLine;->getOffsetToLeftRightOf(IZ)I

    #@58
    move-result v2

    #@59
    add-int p1, v4, v2

    #@5b
    .line 1470
    invoke-static {v1}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    #@5e
    move-result-object v1

    #@5f
    move/from16 v11, p1

    #@61
    .line 1471
    .end local v1           #tl:Landroid/text/TextLine;
    .end local v7           #directions:Landroid/text/Layout$Directions;
    .end local p1
    .local v11, caret:I
    :goto_61
    return v11

    #@62
    .line 1429
    .end local v10           #advance:Z
    .end local v11           #caret:I
    .restart local p1
    :cond_62
    const/4 v2, 0x0

    #@63
    goto :goto_15

    #@64
    :cond_64
    const/4 v10, 0x0

    #@65
    goto :goto_1a

    #@66
    .restart local v10       #advance:Z
    :cond_66
    move/from16 v11, p1

    #@68
    .line 1437
    .end local p1
    .restart local v11       #caret:I
    goto :goto_61

    #@69
    .line 1441
    .end local v11           #caret:I
    .restart local p1
    :cond_69
    move/from16 v0, p1

    #@6b
    if-ne v0, v4, :cond_2b

    #@6d
    .line 1442
    if-lez v12, :cond_73

    #@6f
    .line 1443
    const/4 v13, 0x1

    #@70
    .line 1444
    add-int/lit8 v12, v12, -0x1

    #@72
    goto :goto_2b

    #@73
    :cond_73
    move/from16 v11, p1

    #@75
    .line 1446
    .end local p1
    .restart local v11       #caret:I
    goto :goto_61

    #@76
    .line 1459
    .end local v11           #caret:I
    .restart local v14       #newDir:I
    .restart local p1
    :cond_76
    const/16 p2, 0x0

    #@78
    goto :goto_3f
.end method

.method private getParagraphLeadingMargin(I)I
    .registers 18
    .parameter "line"

    #@0
    .prologue
    .line 1756
    move-object/from16 v0, p0

    #@2
    iget-boolean v14, v0, Landroid/text/Layout;->mSpannedText:Z

    #@4
    if-nez v14, :cond_8

    #@6
    .line 1757
    const/4 v6, 0x0

    #@7
    .line 1788
    :cond_7
    :goto_7
    return v6

    #@8
    .line 1759
    :cond_8
    move-object/from16 v0, p0

    #@a
    iget-object v11, v0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@c
    check-cast v11, Landroid/text/Spanned;

    #@e
    .line 1761
    .local v11, spanned:Landroid/text/Spanned;
    invoke-virtual/range {p0 .. p1}, Landroid/text/Layout;->getLineStart(I)I

    #@11
    move-result v5

    #@12
    .line 1762
    .local v5, lineStart:I
    invoke-virtual/range {p0 .. p1}, Landroid/text/Layout;->getLineEnd(I)I

    #@15
    move-result v4

    #@16
    .line 1763
    .local v4, lineEnd:I
    const-class v14, Landroid/text/style/LeadingMarginSpan;

    #@18
    invoke-interface {v11, v5, v4, v14}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    #@1b
    move-result v9

    #@1c
    .line 1765
    .local v9, spanEnd:I
    const-class v14, Landroid/text/style/LeadingMarginSpan;

    #@1e
    invoke-static {v11, v5, v9, v14}, Landroid/text/Layout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@21
    move-result-object v12

    #@22
    check-cast v12, [Landroid/text/style/LeadingMarginSpan;

    #@24
    .line 1767
    .local v12, spans:[Landroid/text/style/LeadingMarginSpan;
    array-length v14, v12

    #@25
    if-nez v14, :cond_29

    #@27
    .line 1768
    const/4 v6, 0x0

    #@28
    goto :goto_7

    #@29
    .line 1771
    :cond_29
    const/4 v6, 0x0

    #@2a
    .line 1773
    .local v6, margin:I
    if-eqz v5, :cond_36

    #@2c
    add-int/lit8 v14, v5, -0x1

    #@2e
    invoke-interface {v11, v14}, Landroid/text/Spanned;->charAt(I)C

    #@31
    move-result v14

    #@32
    const/16 v15, 0xa

    #@34
    if-ne v14, v15, :cond_62

    #@36
    :cond_36
    const/4 v3, 0x1

    #@37
    .line 1776
    .local v3, isFirstParaLine:Z
    :goto_37
    const/4 v2, 0x0

    #@38
    .local v2, i:I
    :goto_38
    array-length v14, v12

    #@39
    if-ge v2, v14, :cond_7

    #@3b
    .line 1777
    aget-object v8, v12, v2

    #@3d
    .line 1778
    .local v8, span:Landroid/text/style/LeadingMarginSpan;
    move v13, v3

    #@3e
    .line 1779
    .local v13, useFirstLineMargin:Z
    instance-of v14, v8, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;

    #@40
    if-eqz v14, :cond_5a

    #@42
    .line 1780
    invoke-interface {v11, v8}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@45
    move-result v7

    #@46
    .line 1781
    .local v7, spStart:I
    move-object/from16 v0, p0

    #@48
    invoke-virtual {v0, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    #@4b
    move-result v10

    #@4c
    .local v10, spanLine:I
    move-object v14, v8

    #@4d
    .line 1782
    check-cast v14, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;

    #@4f
    invoke-interface {v14}, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;->getLeadingMarginLineCount()I

    #@52
    move-result v1

    #@53
    .line 1783
    .local v1, count:I
    add-int v14, v10, v1

    #@55
    move/from16 v0, p1

    #@57
    if-ge v0, v14, :cond_64

    #@59
    const/4 v13, 0x1

    #@5a
    .line 1785
    .end local v1           #count:I
    .end local v7           #spStart:I
    .end local v10           #spanLine:I
    :cond_5a
    :goto_5a
    invoke-interface {v8, v13}, Landroid/text/style/LeadingMarginSpan;->getLeadingMargin(Z)I

    #@5d
    move-result v14

    #@5e
    add-int/2addr v6, v14

    #@5f
    .line 1776
    add-int/lit8 v2, v2, 0x1

    #@61
    goto :goto_38

    #@62
    .line 1773
    .end local v2           #i:I
    .end local v3           #isFirstParaLine:Z
    .end local v8           #span:Landroid/text/style/LeadingMarginSpan;
    .end local v13           #useFirstLineMargin:Z
    :cond_62
    const/4 v3, 0x0

    #@63
    goto :goto_37

    #@64
    .line 1783
    .restart local v1       #count:I
    .restart local v2       #i:I
    .restart local v3       #isFirstParaLine:Z
    .restart local v7       #spStart:I
    .restart local v8       #span:Landroid/text/style/LeadingMarginSpan;
    .restart local v10       #spanLine:I
    .restart local v13       #useFirstLineMargin:Z
    :cond_64
    const/4 v13, 0x0

    #@65
    goto :goto_5a
.end method

.method static getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;
    .registers 5
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/text/Spanned;",
            "II",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 1986
    .local p3, type:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    if-ne p1, p2, :cond_9

    #@2
    if-lez p1, :cond_9

    #@4
    .line 1987
    invoke-static {p3}, Lcom/android/internal/util/ArrayUtils;->emptyArray(Ljava/lang/Class;)[Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    .line 1990
    :goto_8
    return-object v0

    #@9
    :cond_9
    invoke-interface {p0, p1, p2, p3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    goto :goto_8
.end method

.method public static hasEmoji(Ljava/lang/CharSequence;)Z
    .registers 6
    .parameter "text"

    #@0
    .prologue
    .line 216
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2
    if-eqz v4, :cond_44

    #@4
    .line 217
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@7
    move-result v3

    #@8
    .line 219
    .local v3, len:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v3, :cond_44

    #@b
    .line 220
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@e
    move-result v0

    #@f
    .line 221
    .local v0, chr:C
    invoke-static {p0, v2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    #@12
    move-result v1

    #@13
    .line 223
    .local v1, code:I
    sget-object v4, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@15
    invoke-virtual {v4, v0}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_41

    #@1b
    .line 224
    invoke-static {v0}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z

    #@1e
    move-result v4

    #@1f
    if-nez v4, :cond_3f

    #@21
    add-int/lit8 v4, v2, 0x1

    #@23
    if-ge v4, v3, :cond_2b

    #@25
    invoke-static {v1}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@28
    move-result v4

    #@29
    if-nez v4, :cond_3f

    #@2b
    :cond_2b
    add-int/lit8 v4, v2, 0x1

    #@2d
    if-ge v4, v3, :cond_35

    #@2f
    invoke-static {p0, v2}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@32
    move-result v4

    #@33
    if-nez v4, :cond_3f

    #@35
    :cond_35
    add-int/lit8 v4, v2, 0x3

    #@37
    if-ge v4, v3, :cond_41

    #@39
    invoke-static {p0, v2}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@3c
    move-result v4

    #@3d
    if-eqz v4, :cond_41

    #@3f
    .line 228
    :cond_3f
    const/4 v4, 0x1

    #@40
    .line 233
    .end local v0           #chr:C
    .end local v1           #code:I
    .end local v2           #i:I
    .end local v3           #len:I
    :goto_40
    return v4

    #@41
    .line 219
    .restart local v0       #chr:C
    .restart local v1       #code:I
    .restart local v2       #i:I
    .restart local v3       #len:I
    :cond_41
    add-int/lit8 v2, v2, 0x1

    #@43
    goto :goto_9

    #@44
    .line 233
    .end local v0           #chr:C
    .end local v1           #code:I
    .end local v2           #i:I
    .end local v3           #len:I
    :cond_44
    const/4 v4, 0x0

    #@45
    goto :goto_40
.end method

.method public static hasSurrogatePairEmoji(Ljava/lang/CharSequence;)Z
    .registers 6
    .parameter "text"

    #@0
    .prologue
    .line 240
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2
    if-eqz v4, :cond_3e

    #@4
    .line 241
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@7
    move-result v3

    #@8
    .line 243
    .local v3, len:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v3, :cond_3e

    #@b
    .line 244
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@e
    move-result v0

    #@f
    .line 245
    .local v0, chr:C
    invoke-static {p0, v2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    #@12
    move-result v1

    #@13
    .line 247
    .local v1, code:I
    sget-object v4, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@15
    invoke-virtual {v4, v0}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_3b

    #@1b
    .line 248
    add-int/lit8 v4, v2, 0x1

    #@1d
    if-ge v4, v3, :cond_25

    #@1f
    invoke-static {v1}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@22
    move-result v4

    #@23
    if-nez v4, :cond_39

    #@25
    :cond_25
    add-int/lit8 v4, v2, 0x1

    #@27
    if-ge v4, v3, :cond_2f

    #@29
    invoke-static {p0, v2}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@2c
    move-result v4

    #@2d
    if-nez v4, :cond_39

    #@2f
    :cond_2f
    add-int/lit8 v4, v2, 0x3

    #@31
    if-ge v4, v3, :cond_3b

    #@33
    invoke-static {p0, v2}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_3b

    #@39
    .line 251
    :cond_39
    const/4 v4, 0x1

    #@3a
    .line 256
    .end local v0           #chr:C
    .end local v1           #code:I
    .end local v2           #i:I
    .end local v3           #len:I
    :goto_3a
    return v4

    #@3b
    .line 243
    .restart local v0       #chr:C
    .restart local v1       #code:I
    .restart local v2       #i:I
    .restart local v3       #len:I
    :cond_3b
    add-int/lit8 v2, v2, 0x1

    #@3d
    goto :goto_9

    #@3e
    .line 256
    .end local v0           #chr:C
    .end local v1           #code:I
    .end local v2           #i:I
    .end local v3           #len:I
    :cond_3e
    const/4 v4, 0x0

    #@3f
    goto :goto_3a
.end method

.method public static isDiacriticalMark(Ljava/lang/CharSequence;I)Z
    .registers 4
    .parameter "text"
    .parameter "index"

    #@0
    .prologue
    .line 125
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2
    if-eqz v0, :cond_2e

    #@4
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@6
    if-nez v0, :cond_2e

    #@8
    .line 126
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    #@b
    move-result v0

    #@c
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_1a

    #@12
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    #@15
    move-result v0

    #@16
    const/16 v1, 0x23

    #@18
    if-ne v0, v1, :cond_2e

    #@1a
    :cond_1a
    add-int/lit8 v0, p1, 0x1

    #@1c
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@1f
    move-result v1

    #@20
    if-ge v0, v1, :cond_2e

    #@22
    add-int/lit8 v0, p1, 0x1

    #@24
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@27
    move-result v0

    #@28
    const/16 v1, 0x20e3

    #@2a
    if-ne v0, v1, :cond_2e

    #@2c
    .line 128
    const/4 v0, 0x1

    #@2d
    .line 131
    :goto_2d
    return v0

    #@2e
    :cond_2e
    const/4 v0, 0x0

    #@2f
    goto :goto_2d
.end method

.method public static isDiacriticalMark([CI)Z
    .registers 4
    .parameter "text"
    .parameter "index"

    #@0
    .prologue
    .line 112
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2
    if-eqz v0, :cond_25

    #@4
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@6
    if-nez v0, :cond_25

    #@8
    .line 113
    aget-char v0, p0, p1

    #@a
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_16

    #@10
    aget-char v0, p0, p1

    #@12
    const/16 v1, 0x23

    #@14
    if-ne v0, v1, :cond_25

    #@16
    :cond_16
    add-int/lit8 v0, p1, 0x1

    #@18
    array-length v1, p0

    #@19
    if-ge v0, v1, :cond_25

    #@1b
    add-int/lit8 v0, p1, 0x1

    #@1d
    aget-char v0, p0, v0

    #@1f
    const/16 v1, 0x20e3

    #@21
    if-ne v0, v1, :cond_25

    #@23
    .line 115
    const/4 v0, 0x1

    #@24
    .line 118
    :goto_24
    return v0

    #@25
    :cond_25
    const/4 v0, 0x0

    #@26
    goto :goto_24
.end method

.method public static isInCountryCodeTable(Ljava/lang/CharSequence;I)Z
    .registers 9
    .parameter "text"
    .parameter "index"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 177
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@4
    if-eqz v5, :cond_1b

    #@6
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@8
    if-nez v5, :cond_1b

    #@a
    .line 178
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@c
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@e
    if-nez v5, :cond_1c

    #@10
    .line 179
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@12
    invoke-virtual {v5}, Landroid/emoji/EmojiFactory;->createEmojiSupport()V

    #@15
    .line 181
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@17
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@19
    if-nez v5, :cond_1c

    #@1b
    .line 209
    :cond_1b
    :goto_1b
    return v3

    #@1c
    .line 186
    :cond_1c
    if-ltz p1, :cond_1b

    #@1e
    add-int/lit8 v5, p1, 0x3

    #@20
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@23
    move-result v6

    #@24
    if-ge v5, v6, :cond_1b

    #@26
    .line 187
    const/4 v5, 0x2

    #@27
    new-array v1, v5, [I

    #@29
    .line 188
    .local v1, puaArray:[I
    invoke-static {p0, p1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    #@2c
    move-result v5

    #@2d
    aput v5, v1, v3

    #@2f
    .line 189
    add-int/lit8 v5, p1, 0x2

    #@31
    invoke-static {p0, v5}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    #@34
    move-result v5

    #@35
    aput v5, v1, v4

    #@37
    .line 191
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@39
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@3b
    aget v6, v1, v3

    #@3d
    invoke-virtual {v5, v6}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInCountryCodeRange(I)Z

    #@40
    move-result v5

    #@41
    if-eqz v5, :cond_1b

    #@43
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@45
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@47
    aget v6, v1, v4

    #@49
    invoke-virtual {v5, v6}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInCountryCodeRange(I)Z

    #@4c
    move-result v5

    #@4d
    if-eqz v5, :cond_1b

    #@4f
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@51
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@53
    invoke-virtual {v5, v1}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInCountryCodeTable([I)Z

    #@56
    move-result v5

    #@57
    if-eqz v5, :cond_1b

    #@59
    .line 194
    const/4 v2, 0x0

    #@5a
    .line 195
    .local v2, startPos:I
    move v2, p1

    #@5b
    :goto_5b
    if-ltz v2, :cond_6b

    #@5d
    .line 196
    invoke-static {p0, v2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    #@60
    move-result v0

    #@61
    .line 197
    .local v0, codept:I
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@63
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@65
    invoke-virtual {v5, v0}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInCountryCodeRange(I)Z

    #@68
    move-result v5

    #@69
    if-nez v5, :cond_77

    #@6b
    .line 201
    .end local v0           #codept:I
    :cond_6b
    add-int/lit8 v2, v2, 0x2

    #@6d
    .line 203
    sub-int v5, p1, v2

    #@6f
    div-int/lit8 v5, v5, 0x2

    #@71
    rem-int/lit8 v5, v5, 0x2

    #@73
    if-nez v5, :cond_1b

    #@75
    move v3, v4

    #@76
    .line 204
    goto :goto_1b

    #@77
    .line 195
    .restart local v0       #codept:I
    :cond_77
    add-int/lit8 v2, v2, -0x2

    #@79
    goto :goto_5b
.end method

.method public static isInCountryCodeTable([CI)Z
    .registers 9
    .parameter "text"
    .parameter "index"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 138
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@4
    if-eqz v5, :cond_1b

    #@6
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@8
    if-nez v5, :cond_1b

    #@a
    .line 139
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@c
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@e
    if-nez v5, :cond_1c

    #@10
    .line 140
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@12
    invoke-virtual {v5}, Landroid/emoji/EmojiFactory;->createEmojiSupport()V

    #@15
    .line 142
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@17
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@19
    if-nez v5, :cond_1c

    #@1b
    .line 170
    :cond_1b
    :goto_1b
    return v3

    #@1c
    .line 147
    :cond_1c
    if-ltz p1, :cond_1b

    #@1e
    add-int/lit8 v5, p1, 0x3

    #@20
    array-length v6, p0

    #@21
    if-ge v5, v6, :cond_1b

    #@23
    .line 148
    const/4 v5, 0x2

    #@24
    new-array v1, v5, [I

    #@26
    .line 149
    .local v1, puaArray:[I
    invoke-static {p0, p1}, Ljava/lang/Character;->codePointAt([CI)I

    #@29
    move-result v5

    #@2a
    aput v5, v1, v3

    #@2c
    .line 150
    add-int/lit8 v5, p1, 0x2

    #@2e
    invoke-static {p0, v5}, Ljava/lang/Character;->codePointAt([CI)I

    #@31
    move-result v5

    #@32
    aput v5, v1, v4

    #@34
    .line 152
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@36
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@38
    aget v6, v1, v3

    #@3a
    invoke-virtual {v5, v6}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInCountryCodeRange(I)Z

    #@3d
    move-result v5

    #@3e
    if-eqz v5, :cond_1b

    #@40
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@42
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@44
    aget v6, v1, v4

    #@46
    invoke-virtual {v5, v6}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInCountryCodeRange(I)Z

    #@49
    move-result v5

    #@4a
    if-eqz v5, :cond_1b

    #@4c
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@4e
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@50
    invoke-virtual {v5, v1}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInCountryCodeTable([I)Z

    #@53
    move-result v5

    #@54
    if-eqz v5, :cond_1b

    #@56
    .line 155
    const/4 v2, 0x0

    #@57
    .line 156
    .local v2, startPos:I
    move v2, p1

    #@58
    :goto_58
    if-ltz v2, :cond_68

    #@5a
    .line 157
    invoke-static {p0, v2}, Ljava/lang/Character;->codePointAt([CI)I

    #@5d
    move-result v0

    #@5e
    .line 158
    .local v0, codept:I
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@60
    sget-object v5, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@62
    invoke-virtual {v5, v0}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInCountryCodeRange(I)Z

    #@65
    move-result v5

    #@66
    if-nez v5, :cond_74

    #@68
    .line 162
    .end local v0           #codept:I
    :cond_68
    add-int/lit8 v2, v2, 0x2

    #@6a
    .line 164
    sub-int v5, p1, v2

    #@6c
    div-int/lit8 v5, v5, 0x2

    #@6e
    rem-int/lit8 v5, v5, 0x2

    #@70
    if-nez v5, :cond_1b

    #@72
    move v3, v4

    #@73
    .line 165
    goto :goto_1b

    #@74
    .line 156
    .restart local v0       #codept:I
    :cond_74
    add-int/lit8 v2, v2, -0x2

    #@76
    goto :goto_58
.end method

.method public static isInEmojiUnicodeTable(C)Z
    .registers 2
    .parameter "pua"

    #@0
    .prologue
    .line 96
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2
    if-eqz v0, :cond_1e

    #@4
    .line 97
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@6
    sget-object v0, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@8
    if-nez v0, :cond_f

    #@a
    .line 98
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@c
    invoke-virtual {v0}, Landroid/emoji/EmojiFactory;->createEmojiSupport()V

    #@f
    .line 101
    :cond_f
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@11
    sget-object v0, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@13
    if-eqz v0, :cond_1e

    #@15
    .line 102
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@17
    sget-object v0, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@19
    invoke-virtual {v0, p0}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInEmojiUnicodeTable(C)Z

    #@1c
    move-result v0

    #@1d
    .line 105
    :goto_1d
    return v0

    #@1e
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_1d
.end method

.method public static isInEmojiUnicodeTable(I)Z
    .registers 2
    .parameter "pua"

    #@0
    .prologue
    .line 80
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2
    if-eqz v0, :cond_1e

    #@4
    .line 81
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@6
    sget-object v0, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@8
    if-nez v0, :cond_f

    #@a
    .line 82
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@c
    invoke-virtual {v0}, Landroid/emoji/EmojiFactory;->createEmojiSupport()V

    #@f
    .line 85
    :cond_f
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@11
    sget-object v0, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@13
    if-eqz v0, :cond_1e

    #@15
    .line 86
    sget-object v0, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@17
    sget-object v0, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@19
    invoke-virtual {v0, p0}, Landroid/emoji/EmojiFactory$EmojiSupport;->isInEmojiUnicodeTable(I)Z

    #@1c
    move-result v0

    #@1d
    .line 89
    :goto_1d
    return v0

    #@1e
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_1d
.end method

.method public static lengthEmoji(Ljava/lang/CharSequence;)I
    .registers 7
    .parameter "text"

    #@0
    .prologue
    .line 263
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@3
    move-result v4

    #@4
    .line 264
    .local v4, textLen:I
    move v2, v4

    #@5
    .line 266
    .local v2, emojiLen:I
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@7
    if-eqz v5, :cond_42

    #@9
    .line 267
    const/4 v3, 0x0

    #@a
    .local v3, i:I
    :goto_a
    if-ge v3, v4, :cond_42

    #@c
    .line 268
    invoke-interface {p0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    #@f
    move-result v0

    #@10
    .line 269
    .local v0, chr:C
    invoke-static {p0, v3}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    #@13
    move-result v1

    #@14
    .line 271
    .local v1, code:I
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@16
    invoke-virtual {v5, v0}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@19
    move-result v5

    #@1a
    if-nez v5, :cond_30

    #@1c
    .line 272
    add-int/lit8 v5, v3, 0x1

    #@1e
    if-ge v5, v4, :cond_33

    #@20
    invoke-static {v1}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@23
    move-result v5

    #@24
    if-nez v5, :cond_2c

    #@26
    invoke-static {p0, v3}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@29
    move-result v5

    #@2a
    if-eqz v5, :cond_33

    #@2c
    .line 273
    :cond_2c
    add-int/lit8 v2, v2, -0x1

    #@2e
    .line 274
    add-int/lit8 v3, v3, 0x1

    #@30
    .line 267
    :cond_30
    :goto_30
    add-int/lit8 v3, v3, 0x1

    #@32
    goto :goto_a

    #@33
    .line 275
    :cond_33
    add-int/lit8 v5, v3, 0x3

    #@35
    if-ge v5, v4, :cond_30

    #@37
    invoke-static {p0, v3}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@3a
    move-result v5

    #@3b
    if-eqz v5, :cond_30

    #@3d
    .line 276
    add-int/lit8 v2, v2, -0x3

    #@3f
    .line 277
    add-int/lit8 v3, v3, 0x3

    #@41
    goto :goto_30

    #@42
    .line 282
    .end local v0           #chr:C
    .end local v1           #code:I
    .end local v3           #i:I
    :cond_42
    return v2
.end method

.method static measurePara(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)F
    .registers 26
    .parameter "paint"
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1794
    invoke-static {}, Landroid/text/MeasuredText;->obtain()Landroid/text/MeasuredText;

    #@3
    move-result-object v17

    #@4
    .line 1795
    .local v17, mt:Landroid/text/MeasuredText;
    invoke-static {}, Landroid/text/TextLine;->obtain()Landroid/text/TextLine;

    #@7
    move-result-object v21

    #@8
    .line 1797
    .local v21, tl:Landroid/text/TextLine;
    :try_start_8
    sget-object v4, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    #@a
    move-object/from16 v0, v17

    #@c
    move-object/from16 v1, p1

    #@e
    move/from16 v2, p2

    #@10
    move/from16 v3, p3

    #@12
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/MeasuredText;->setPara(Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;)V

    #@15
    .line 1800
    move-object/from16 v0, v17

    #@17
    iget-boolean v4, v0, Landroid/text/MeasuredText;->mEasy:Z

    #@19
    if-eqz v4, :cond_85

    #@1b
    .line 1801
    sget-object v10, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@1d
    .line 1802
    .local v10, directions:Landroid/text/Layout$Directions;
    const/4 v9, 0x1

    #@1e
    .line 1808
    .local v9, dir:I
    :goto_1e
    move-object/from16 v0, v17

    #@20
    iget-object v13, v0, Landroid/text/MeasuredText;->mChars:[C

    #@22
    .line 1809
    .local v13, chars:[C
    move-object/from16 v0, v17

    #@24
    iget v0, v0, Landroid/text/MeasuredText;->mLen:I

    #@26
    move/from16 v16, v0

    #@28
    .line 1810
    .local v16, len:I
    const/4 v11, 0x0

    #@29
    .line 1811
    .local v11, hasTabs:Z
    const/4 v12, 0x0

    #@2a
    .line 1812
    .local v12, tabStops:Landroid/text/Layout$TabStops;
    const/4 v15, 0x0

    #@2b
    .local v15, i:I
    :goto_2b
    move/from16 v0, v16

    #@2d
    if-ge v15, v0, :cond_6a

    #@2f
    .line 1813
    aget-char v4, v13, v15

    #@31
    const/16 v5, 0x9

    #@33
    if-ne v4, v5, :cond_a1

    #@35
    .line 1814
    const/4 v11, 0x1

    #@36
    .line 1815
    move-object/from16 v0, p1

    #@38
    instance-of v4, v0, Landroid/text/Spanned;

    #@3a
    if-eqz v4, :cond_6a

    #@3c
    .line 1816
    move-object/from16 v0, p1

    #@3e
    check-cast v0, Landroid/text/Spanned;

    #@40
    move-object/from16 v19, v0

    #@42
    .line 1817
    .local v19, spanned:Landroid/text/Spanned;
    const-class v4, Landroid/text/style/TabStopSpan;

    #@44
    move-object/from16 v0, v19

    #@46
    move/from16 v1, p2

    #@48
    move/from16 v2, p3

    #@4a
    invoke-interface {v0, v1, v2, v4}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    #@4d
    move-result v18

    #@4e
    .line 1819
    .local v18, spanEnd:I
    const-class v4, Landroid/text/style/TabStopSpan;

    #@50
    move-object/from16 v0, v19

    #@52
    move/from16 v1, p2

    #@54
    move/from16 v2, v18

    #@56
    invoke-static {v0, v1, v2, v4}, Landroid/text/Layout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@59
    move-result-object v20

    #@5a
    check-cast v20, [Landroid/text/style/TabStopSpan;

    #@5c
    .line 1821
    .local v20, spans:[Landroid/text/style/TabStopSpan;
    move-object/from16 v0, v20

    #@5e
    array-length v4, v0

    #@5f
    if-lez v4, :cond_6a

    #@61
    .line 1822
    new-instance v12, Landroid/text/Layout$TabStops;

    #@63
    .end local v12           #tabStops:Landroid/text/Layout$TabStops;
    const/16 v4, 0x14

    #@65
    move-object/from16 v0, v20

    #@67
    invoke-direct {v12, v4, v0}, Landroid/text/Layout$TabStops;-><init>(I[Ljava/lang/Object;)V

    #@6a
    .end local v18           #spanEnd:I
    .end local v19           #spanned:Landroid/text/Spanned;
    .end local v20           #spans:[Landroid/text/style/TabStopSpan;
    .restart local v12       #tabStops:Landroid/text/Layout$TabStops;
    :cond_6a
    :goto_6a
    move-object/from16 v4, v21

    #@6c
    move-object/from16 v5, p0

    #@6e
    move-object/from16 v6, p1

    #@70
    move/from16 v7, p2

    #@72
    move/from16 v8, p3

    #@74
    .line 1846
    invoke-virtual/range {v4 .. v12}, Landroid/text/TextLine;->set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;)V

    #@77
    .line 1847
    const/4 v4, 0x0

    #@78
    move-object/from16 v0, v21

    #@7a
    invoke-virtual {v0, v4}, Landroid/text/TextLine;->metrics(Landroid/graphics/Paint$FontMetricsInt;)F
    :try_end_7d
    .catchall {:try_start_8 .. :try_end_7d} :catchall_db

    #@7d
    move-result v4

    #@7e
    .line 1849
    invoke-static/range {v21 .. v21}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    #@81
    .line 1850
    invoke-static/range {v17 .. v17}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@84
    return v4

    #@85
    .line 1804
    .end local v9           #dir:I
    .end local v10           #directions:Landroid/text/Layout$Directions;
    .end local v11           #hasTabs:Z
    .end local v12           #tabStops:Landroid/text/Layout$TabStops;
    .end local v13           #chars:[C
    .end local v15           #i:I
    .end local v16           #len:I
    :cond_85
    :try_start_85
    move-object/from16 v0, v17

    #@87
    iget v4, v0, Landroid/text/MeasuredText;->mDir:I

    #@89
    move-object/from16 v0, v17

    #@8b
    iget-object v5, v0, Landroid/text/MeasuredText;->mLevels:[B

    #@8d
    const/4 v6, 0x0

    #@8e
    move-object/from16 v0, v17

    #@90
    iget-object v7, v0, Landroid/text/MeasuredText;->mChars:[C

    #@92
    const/4 v8, 0x0

    #@93
    move-object/from16 v0, v17

    #@95
    iget v9, v0, Landroid/text/MeasuredText;->mLen:I

    #@97
    invoke-static/range {v4 .. v9}, Landroid/text/AndroidBidi;->directions(I[BI[CII)Landroid/text/Layout$Directions;

    #@9a
    move-result-object v10

    #@9b
    .line 1806
    .restart local v10       #directions:Landroid/text/Layout$Directions;
    move-object/from16 v0, v17

    #@9d
    iget v9, v0, Landroid/text/MeasuredText;->mDir:I

    #@9f
    .restart local v9       #dir:I
    goto/16 :goto_1e

    #@a1
    .line 1828
    .restart local v11       #hasTabs:Z
    .restart local v12       #tabStops:Landroid/text/Layout$TabStops;
    .restart local v13       #chars:[C
    .restart local v15       #i:I
    .restart local v16       #len:I
    :cond_a1
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@a3
    if-eqz v4, :cond_d7

    #@a5
    sget-object v4, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@a7
    aget-char v5, v13, v15

    #@a9
    invoke-virtual {v4, v5}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@ac
    move-result v4

    #@ad
    if-nez v4, :cond_d7

    #@af
    .line 1829
    invoke-static {v13, v15}, Ljava/lang/Character;->codePointAt([CI)I

    #@b2
    move-result v14

    #@b3
    .line 1830
    .local v14, emoji:I
    invoke-static {v13, v15}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z

    #@b6
    move-result v4

    #@b7
    if-eqz v4, :cond_bd

    #@b9
    .line 1831
    add-int/lit8 v15, v15, 0x3

    #@bb
    .line 1832
    const/4 v11, 0x1

    #@bc
    .line 1833
    goto :goto_6a

    #@bd
    .line 1834
    :cond_bd
    invoke-static {v14}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@c0
    move-result v4

    #@c1
    if-nez v4, :cond_c9

    #@c3
    invoke-static {v13, v15}, Landroid/text/Layout;->isDiacriticalMark([CI)Z

    #@c6
    move-result v4

    #@c7
    if-eqz v4, :cond_cd

    #@c9
    .line 1835
    :cond_c9
    add-int/lit8 v15, v15, 0x1

    #@cb
    .line 1836
    const/4 v11, 0x1

    #@cc
    .line 1837
    goto :goto_6a

    #@cd
    .line 1838
    :cond_cd
    aget-char v4, v13, v15

    #@cf
    invoke-static {v4}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z
    :try_end_d2
    .catchall {:try_start_85 .. :try_end_d2} :catchall_db

    #@d2
    move-result v4

    #@d3
    if-eqz v4, :cond_d7

    #@d5
    .line 1839
    const/4 v11, 0x1

    #@d6
    .line 1840
    goto :goto_6a

    #@d7
    .line 1812
    .end local v14           #emoji:I
    :cond_d7
    add-int/lit8 v15, v15, 0x1

    #@d9
    goto/16 :goto_2b

    #@db
    .line 1849
    .end local v9           #dir:I
    .end local v10           #directions:Landroid/text/Layout$Directions;
    .end local v11           #hasTabs:Z
    .end local v12           #tabStops:Landroid/text/Layout$TabStops;
    .end local v13           #chars:[C
    .end local v15           #i:I
    .end local v16           #len:I
    :catchall_db
    move-exception v4

    #@dc
    invoke-static/range {v21 .. v21}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    #@df
    .line 1850
    invoke-static/range {v17 .. v17}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@e2
    throw v4
.end method

.method static nextTab(Ljava/lang/CharSequence;IIF[Ljava/lang/Object;)F
    .registers 11
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "h"
    .parameter "tabs"

    #@0
    .prologue
    const/high16 v5, 0x41a0

    #@2
    .line 1929
    const v2, 0x7f7fffff

    #@5
    .line 1930
    .local v2, nh:F
    const/4 v0, 0x0

    #@6
    .line 1932
    .local v0, alltabs:Z
    instance-of v4, p0, Landroid/text/Spanned;

    #@8
    if-eqz v4, :cond_41

    #@a
    .line 1933
    if-nez p4, :cond_15

    #@c
    .line 1934
    check-cast p0, Landroid/text/Spanned;

    #@e
    .end local p0
    const-class v4, Landroid/text/style/TabStopSpan;

    #@10
    invoke-static {p0, p1, p2, v4}, Landroid/text/Layout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@13
    move-result-object p4

    #@14
    .line 1935
    const/4 v0, 0x1

    #@15
    .line 1938
    :cond_15
    const/4 v1, 0x0

    #@16
    .local v1, i:I
    :goto_16
    array-length v4, p4

    #@17
    if-ge v1, v4, :cond_38

    #@19
    .line 1939
    if-nez v0, :cond_24

    #@1b
    .line 1940
    aget-object v4, p4, v1

    #@1d
    instance-of v4, v4, Landroid/text/style/TabStopSpan;

    #@1f
    if-nez v4, :cond_24

    #@21
    .line 1938
    :cond_21
    :goto_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_16

    #@24
    .line 1944
    :cond_24
    aget-object v4, p4, v1

    #@26
    check-cast v4, Landroid/text/style/TabStopSpan;

    #@28
    invoke-interface {v4}, Landroid/text/style/TabStopSpan;->getTabStop()I

    #@2b
    move-result v3

    #@2c
    .line 1946
    .local v3, where:I
    int-to-float v4, v3

    #@2d
    cmpg-float v4, v4, v2

    #@2f
    if-gez v4, :cond_21

    #@31
    int-to-float v4, v3

    #@32
    cmpl-float v4, v4, p3

    #@34
    if-lez v4, :cond_21

    #@36
    .line 1947
    int-to-float v2, v3

    #@37
    goto :goto_21

    #@38
    .line 1950
    .end local v3           #where:I
    :cond_38
    const v4, 0x7f7fffff

    #@3b
    cmpl-float v4, v2, v4

    #@3d
    if-eqz v4, :cond_41

    #@3f
    move v4, v2

    #@40
    .line 1954
    .end local v1           #i:I
    :goto_40
    return v4

    #@41
    :cond_41
    add-float v4, p3, v5

    #@43
    div-float/2addr v4, v5

    #@44
    float-to-int v4, v4

    #@45
    mul-int/lit8 v4, v4, 0x14

    #@47
    int-to-float v4, v4

    #@48
    goto :goto_40
.end method

.method private primaryIsTrailingPrevious(I)Z
    .registers 15
    .parameter "offset"

    #@0
    .prologue
    const v12, 0x3ffffff

    #@3
    const/4 v9, 0x0

    #@4
    const/4 v10, 0x1

    #@5
    .line 972
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@8
    move-result v4

    #@9
    .line 973
    .local v4, line:I
    invoke-virtual {p0, v4}, Landroid/text/Layout;->getLineStart(I)I

    #@c
    move-result v6

    #@d
    .line 974
    .local v6, lineStart:I
    invoke-virtual {p0, v4}, Landroid/text/Layout;->getLineEnd(I)I

    #@10
    move-result v5

    #@11
    .line 975
    .local v5, lineEnd:I
    invoke-virtual {p0, v4}, Landroid/text/Layout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@14
    move-result-object v11

    #@15
    iget-object v7, v11, Landroid/text/Layout$Directions;->mDirections:[I

    #@17
    .line 977
    .local v7, runs:[I
    const/4 v1, -0x1

    #@18
    .line 978
    .local v1, levelAt:I
    const/4 v0, 0x0

    #@19
    .local v0, i:I
    :goto_19
    array-length v11, v7

    #@1a
    if-ge v0, v11, :cond_39

    #@1c
    .line 979
    aget v11, v7, v0

    #@1e
    add-int v8, v6, v11

    #@20
    .line 980
    .local v8, start:I
    add-int/lit8 v11, v0, 0x1

    #@22
    aget v11, v7, v11

    #@24
    and-int/2addr v11, v12

    #@25
    add-int v3, v8, v11

    #@27
    .line 981
    .local v3, limit:I
    if-le v3, v5, :cond_2a

    #@29
    .line 982
    move v3, v5

    #@2a
    .line 984
    :cond_2a
    if-lt p1, v8, :cond_51

    #@2c
    if-ge p1, v3, :cond_51

    #@2e
    .line 985
    if-le p1, v8, :cond_31

    #@30
    .line 1017
    .end local v3           #limit:I
    .end local v8           #start:I
    :goto_30
    return v9

    #@31
    .line 989
    .restart local v3       #limit:I
    .restart local v8       #start:I
    :cond_31
    add-int/lit8 v11, v0, 0x1

    #@33
    aget v11, v7, v11

    #@35
    ushr-int/lit8 v11, v11, 0x1a

    #@37
    and-int/lit8 v1, v11, 0x3f

    #@39
    .line 993
    .end local v3           #limit:I
    .end local v8           #start:I
    :cond_39
    const/4 v11, -0x1

    #@3a
    if-ne v1, v11, :cond_43

    #@3c
    .line 995
    invoke-virtual {p0, v4}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@3f
    move-result v11

    #@40
    if-ne v11, v10, :cond_54

    #@42
    move v1, v9

    #@43
    .line 999
    :cond_43
    :goto_43
    const/4 v2, -0x1

    #@44
    .line 1000
    .local v2, levelBefore:I
    if-ne p1, v6, :cond_58

    #@46
    .line 1001
    invoke-virtual {p0, v4}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@49
    move-result v11

    #@4a
    if-ne v11, v10, :cond_56

    #@4c
    move v2, v9

    #@4d
    .line 1017
    :cond_4d
    :goto_4d
    if-ge v2, v1, :cond_7c

    #@4f
    :goto_4f
    move v9, v10

    #@50
    goto :goto_30

    #@51
    .line 978
    .end local v2           #levelBefore:I
    .restart local v3       #limit:I
    .restart local v8       #start:I
    :cond_51
    add-int/lit8 v0, v0, 0x2

    #@53
    goto :goto_19

    #@54
    .end local v3           #limit:I
    .end local v8           #start:I
    :cond_54
    move v1, v10

    #@55
    .line 995
    goto :goto_43

    #@56
    .restart local v2       #levelBefore:I
    :cond_56
    move v2, v10

    #@57
    .line 1001
    goto :goto_4d

    #@58
    .line 1003
    :cond_58
    add-int/lit8 p1, p1, -0x1

    #@5a
    .line 1004
    const/4 v0, 0x0

    #@5b
    :goto_5b
    array-length v11, v7

    #@5c
    if-ge v0, v11, :cond_4d

    #@5e
    .line 1005
    aget v11, v7, v0

    #@60
    add-int v8, v6, v11

    #@62
    .line 1006
    .restart local v8       #start:I
    add-int/lit8 v11, v0, 0x1

    #@64
    aget v11, v7, v11

    #@66
    and-int/2addr v11, v12

    #@67
    add-int v3, v8, v11

    #@69
    .line 1007
    .restart local v3       #limit:I
    if-le v3, v5, :cond_6c

    #@6b
    .line 1008
    move v3, v5

    #@6c
    .line 1010
    :cond_6c
    if-lt p1, v8, :cond_79

    #@6e
    if-ge p1, v3, :cond_79

    #@70
    .line 1011
    add-int/lit8 v11, v0, 0x1

    #@72
    aget v11, v7, v11

    #@74
    ushr-int/lit8 v11, v11, 0x1a

    #@76
    and-int/lit8 v2, v11, 0x3f

    #@78
    .line 1012
    goto :goto_4d

    #@79
    .line 1004
    :cond_79
    add-int/lit8 v0, v0, 0x2

    #@7b
    goto :goto_5b

    #@7c
    .end local v3           #limit:I
    .end local v8           #start:I
    :cond_7c
    move v10, v9

    #@7d
    .line 1017
    goto :goto_4f
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "c"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 408
    const/4 v0, 0x0

    #@2
    invoke-virtual {p0, p1, v1, v1, v0}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    #@5
    .line 409
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V
    .registers 14
    .parameter "canvas"
    .parameter "highlight"
    .parameter "highlightPaint"
    .parameter "cursorOffsetVertical"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineRangeForDraw(Landroid/graphics/Canvas;)J

    #@3
    move-result-wide v7

    #@4
    .line 424
    .local v7, lineRange:J
    invoke-static {v7, v8}, Landroid/text/TextUtils;->unpackRangeStartFromLong(J)I

    #@7
    move-result v5

    #@8
    .line 425
    .local v5, firstLine:I
    invoke-static {v7, v8}, Landroid/text/TextUtils;->unpackRangeEndFromLong(J)I

    #@b
    move-result v6

    #@c
    .line 426
    .local v6, lastLine:I
    if-gez v6, :cond_f

    #@e
    .line 431
    :goto_e
    return-void

    #@f
    :cond_f
    move-object v0, p0

    #@10
    move-object v1, p1

    #@11
    move-object v2, p2

    #@12
    move-object v3, p3

    #@13
    move v4, p4

    #@14
    .line 428
    invoke-virtual/range {v0 .. v6}, Landroid/text/Layout;->drawBackground(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;III)V

    #@17
    .line 430
    invoke-virtual {p0, p1, v5, v6}, Landroid/text/Layout;->drawText(Landroid/graphics/Canvas;II)V

    #@1a
    goto :goto_e
.end method

.method public drawBackground(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;III)V
    .registers 33
    .parameter "canvas"
    .parameter "highlight"
    .parameter "highlightPaint"
    .parameter "cursorOffsetVertical"
    .parameter "firstLine"
    .parameter "lastLine"

    #@0
    .prologue
    .line 595
    move-object/from16 v0, p0

    #@2
    iget-boolean v4, v0, Landroid/text/Layout;->mSpannedText:Z

    #@4
    if-eqz v4, :cond_fc

    #@6
    .line 596
    move-object/from16 v0, p0

    #@8
    iget-object v4, v0, Landroid/text/Layout;->mLineBackgroundSpans:Landroid/text/SpanSet;

    #@a
    if-nez v4, :cond_17

    #@c
    .line 597
    new-instance v4, Landroid/text/SpanSet;

    #@e
    const-class v6, Landroid/text/style/LineBackgroundSpan;

    #@10
    invoke-direct {v4, v6}, Landroid/text/SpanSet;-><init>(Ljava/lang/Class;)V

    #@13
    move-object/from16 v0, p0

    #@15
    iput-object v4, v0, Landroid/text/Layout;->mLineBackgroundSpans:Landroid/text/SpanSet;

    #@17
    .line 600
    :cond_17
    move-object/from16 v0, p0

    #@19
    iget-object v11, v0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@1b
    check-cast v11, Landroid/text/Spanned;

    #@1d
    .line 601
    .local v11, buffer:Landroid/text/Spanned;
    invoke-interface {v11}, Landroid/text/Spanned;->length()I

    #@20
    move-result v25

    #@21
    .line 602
    .local v25, textLength:I
    move-object/from16 v0, p0

    #@23
    iget-object v4, v0, Landroid/text/Layout;->mLineBackgroundSpans:Landroid/text/SpanSet;

    #@25
    const/4 v6, 0x0

    #@26
    move/from16 v0, v25

    #@28
    invoke-virtual {v4, v11, v6, v0}, Landroid/text/SpanSet;->init(Landroid/text/Spanned;II)V

    #@2b
    .line 604
    move-object/from16 v0, p0

    #@2d
    iget-object v4, v0, Landroid/text/Layout;->mLineBackgroundSpans:Landroid/text/SpanSet;

    #@2f
    iget v4, v4, Landroid/text/SpanSet;->numberOfSpans:I

    #@31
    if-lez v4, :cond_f5

    #@33
    .line 605
    move-object/from16 v0, p0

    #@35
    move/from16 v1, p5

    #@37
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    #@3a
    move-result v19

    #@3b
    .line 606
    .local v19, previousLineBottom:I
    move-object/from16 v0, p0

    #@3d
    move/from16 v1, p5

    #@3f
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineStart(I)I

    #@42
    move-result v20

    #@43
    .line 607
    .local v20, previousLineEnd:I
    sget-object v22, Landroid/text/Layout;->NO_PARA_SPANS:[Landroid/text/style/ParagraphStyle;

    #@45
    .line 608
    .local v22, spans:[Landroid/text/style/ParagraphStyle;
    const/16 v23, 0x0

    #@47
    .line 609
    .local v23, spansLength:I
    move-object/from16 v0, p0

    #@49
    iget-object v5, v0, Landroid/text/Layout;->mPaint:Landroid/text/TextPaint;

    #@4b
    .line 610
    .local v5, paint:Landroid/text/TextPaint;
    const/16 v21, 0x0

    #@4d
    .line 611
    .local v21, spanEnd:I
    move-object/from16 v0, p0

    #@4f
    iget v7, v0, Landroid/text/Layout;->mWidth:I

    #@51
    .line 612
    .local v7, width:I
    move/from16 v14, p5

    #@53
    .local v14, i:I
    :goto_53
    move/from16 v0, p6

    #@55
    if-gt v14, v0, :cond_f5

    #@57
    .line 613
    move/from16 v12, v20

    #@59
    .line 614
    .local v12, start:I
    add-int/lit8 v4, v14, 0x1

    #@5b
    move-object/from16 v0, p0

    #@5d
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getLineStart(I)I

    #@60
    move-result v13

    #@61
    .line 615
    .local v13, end:I
    move/from16 v20, v13

    #@63
    .line 617
    move/from16 v8, v19

    #@65
    .line 618
    .local v8, ltop:I
    add-int/lit8 v4, v14, 0x1

    #@67
    move-object/from16 v0, p0

    #@69
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getLineTop(I)I

    #@6c
    move-result v10

    #@6d
    .line 619
    .local v10, lbottom:I
    move/from16 v19, v10

    #@6f
    .line 620
    move-object/from16 v0, p0

    #@71
    invoke-virtual {v0, v14}, Landroid/text/Layout;->getLineDescent(I)I

    #@74
    move-result v4

    #@75
    sub-int v9, v10, v4

    #@77
    .line 622
    .local v9, lbaseline:I
    move/from16 v0, v21

    #@79
    if-lt v12, v0, :cond_dc

    #@7b
    .line 625
    move-object/from16 v0, p0

    #@7d
    iget-object v4, v0, Landroid/text/Layout;->mLineBackgroundSpans:Landroid/text/SpanSet;

    #@7f
    move/from16 v0, v25

    #@81
    invoke-virtual {v4, v12, v0}, Landroid/text/SpanSet;->getNextTransition(II)I

    #@84
    move-result v21

    #@85
    .line 627
    const/16 v23, 0x0

    #@87
    .line 629
    if-ne v12, v13, :cond_8b

    #@89
    if-nez v12, :cond_dc

    #@8b
    .line 632
    :cond_8b
    const/4 v15, 0x0

    #@8c
    .local v15, j:I
    :goto_8c
    move-object/from16 v0, p0

    #@8e
    iget-object v4, v0, Landroid/text/Layout;->mLineBackgroundSpans:Landroid/text/SpanSet;

    #@90
    iget v4, v4, Landroid/text/SpanSet;->numberOfSpans:I

    #@92
    if-ge v15, v4, :cond_dc

    #@94
    .line 635
    move-object/from16 v0, p0

    #@96
    iget-object v4, v0, Landroid/text/Layout;->mLineBackgroundSpans:Landroid/text/SpanSet;

    #@98
    iget-object v4, v4, Landroid/text/SpanSet;->spanStarts:[I

    #@9a
    aget v4, v4, v15

    #@9c
    if-ge v4, v13, :cond_a8

    #@9e
    move-object/from16 v0, p0

    #@a0
    iget-object v4, v0, Landroid/text/Layout;->mLineBackgroundSpans:Landroid/text/SpanSet;

    #@a2
    iget-object v4, v4, Landroid/text/SpanSet;->spanEnds:[I

    #@a4
    aget v4, v4, v15

    #@a6
    if-gt v4, v12, :cond_ab

    #@a8
    .line 632
    :cond_a8
    :goto_a8
    add-int/lit8 v15, v15, 0x1

    #@aa
    goto :goto_8c

    #@ab
    .line 637
    :cond_ab
    move-object/from16 v0, v22

    #@ad
    array-length v4, v0

    #@ae
    move/from16 v0, v23

    #@b0
    if-ne v0, v4, :cond_cb

    #@b2
    .line 639
    mul-int/lit8 v4, v23, 0x2

    #@b4
    invoke-static {v4}, Lcom/android/internal/util/ArrayUtils;->idealObjectArraySize(I)I

    #@b7
    move-result v17

    #@b8
    .line 640
    .local v17, newSize:I
    move/from16 v0, v17

    #@ba
    new-array v0, v0, [Landroid/text/style/ParagraphStyle;

    #@bc
    move-object/from16 v18, v0

    #@be
    .line 641
    .local v18, newSpans:[Landroid/text/style/ParagraphStyle;
    const/4 v4, 0x0

    #@bf
    const/4 v6, 0x0

    #@c0
    move-object/from16 v0, v22

    #@c2
    move-object/from16 v1, v18

    #@c4
    move/from16 v2, v23

    #@c6
    invoke-static {v0, v4, v1, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@c9
    .line 642
    move-object/from16 v22, v18

    #@cb
    .line 644
    .end local v17           #newSize:I
    .end local v18           #newSpans:[Landroid/text/style/ParagraphStyle;
    :cond_cb
    add-int/lit8 v24, v23, 0x1

    #@cd
    .end local v23           #spansLength:I
    .local v24, spansLength:I
    move-object/from16 v0, p0

    #@cf
    iget-object v4, v0, Landroid/text/Layout;->mLineBackgroundSpans:Landroid/text/SpanSet;

    #@d1
    iget-object v4, v4, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    #@d3
    check-cast v4, [Landroid/text/style/LineBackgroundSpan;

    #@d5
    aget-object v4, v4, v15

    #@d7
    aput-object v4, v22, v23

    #@d9
    move/from16 v23, v24

    #@db
    .end local v24           #spansLength:I
    .restart local v23       #spansLength:I
    goto :goto_a8

    #@dc
    .line 649
    .end local v15           #j:I
    :cond_dc
    const/16 v16, 0x0

    #@de
    .local v16, n:I
    :goto_de
    move/from16 v0, v16

    #@e0
    move/from16 v1, v23

    #@e2
    if-ge v0, v1, :cond_f1

    #@e4
    .line 650
    aget-object v3, v22, v16

    #@e6
    check-cast v3, Landroid/text/style/LineBackgroundSpan;

    #@e8
    .line 651
    .local v3, lineBackgroundSpan:Landroid/text/style/LineBackgroundSpan;
    const/4 v6, 0x0

    #@e9
    move-object/from16 v4, p1

    #@eb
    invoke-interface/range {v3 .. v14}, Landroid/text/style/LineBackgroundSpan;->drawBackground(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;III)V

    #@ee
    .line 649
    add-int/lit8 v16, v16, 0x1

    #@f0
    goto :goto_de

    #@f1
    .line 612
    .end local v3           #lineBackgroundSpan:Landroid/text/style/LineBackgroundSpan;
    :cond_f1
    add-int/lit8 v14, v14, 0x1

    #@f3
    goto/16 :goto_53

    #@f5
    .line 657
    .end local v5           #paint:Landroid/text/TextPaint;
    .end local v7           #width:I
    .end local v8           #ltop:I
    .end local v9           #lbaseline:I
    .end local v10           #lbottom:I
    .end local v12           #start:I
    .end local v13           #end:I
    .end local v14           #i:I
    .end local v16           #n:I
    .end local v19           #previousLineBottom:I
    .end local v20           #previousLineEnd:I
    .end local v21           #spanEnd:I
    .end local v22           #spans:[Landroid/text/style/ParagraphStyle;
    .end local v23           #spansLength:I
    :cond_f5
    move-object/from16 v0, p0

    #@f7
    iget-object v4, v0, Landroid/text/Layout;->mLineBackgroundSpans:Landroid/text/SpanSet;

    #@f9
    invoke-virtual {v4}, Landroid/text/SpanSet;->recycle()V

    #@fc
    .line 662
    .end local v11           #buffer:Landroid/text/Spanned;
    .end local v25           #textLength:I
    :cond_fc
    if-eqz p2, :cond_118

    #@fe
    .line 663
    if-eqz p4, :cond_109

    #@100
    const/4 v4, 0x0

    #@101
    move/from16 v0, p4

    #@103
    int-to-float v6, v0

    #@104
    move-object/from16 v0, p1

    #@106
    invoke-virtual {v0, v4, v6}, Landroid/graphics/Canvas;->translate(FF)V

    #@109
    .line 664
    :cond_109
    invoke-virtual/range {p1 .. p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@10c
    .line 665
    if-eqz p4, :cond_118

    #@10e
    const/4 v4, 0x0

    #@10f
    move/from16 v0, p4

    #@111
    neg-int v6, v0

    #@112
    int-to-float v6, v6

    #@113
    move-object/from16 v0, p1

    #@115
    invoke-virtual {v0, v4, v6}, Landroid/graphics/Canvas;->translate(FF)V

    #@118
    .line 667
    :cond_118
    return-void
.end method

.method public drawText(Landroid/graphics/Canvas;II)V
    .registers 51
    .parameter "canvas"
    .parameter "firstLine"
    .parameter "lastLine"

    #@0
    .prologue
    .line 437
    move-object/from16 v0, p0

    #@2
    move/from16 v1, p2

    #@4
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    #@7
    move-result v35

    #@8
    .line 438
    .local v35, previousLineBottom:I
    move-object/from16 v0, p0

    #@a
    move/from16 v1, p2

    #@c
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineStart(I)I

    #@f
    move-result v36

    #@10
    .line 439
    .local v36, previousLineEnd:I
    sget-object v39, Landroid/text/Layout;->NO_PARA_SPANS:[Landroid/text/style/ParagraphStyle;

    #@12
    .line 440
    .local v39, spans:[Landroid/text/style/ParagraphStyle;
    const/16 v38, 0x0

    #@14
    .line 441
    .local v38, spanEnd:I
    move-object/from16 v0, p0

    #@16
    iget-object v5, v0, Landroid/text/Layout;->mPaint:Landroid/text/TextPaint;

    #@18
    .line 442
    .local v5, paint:Landroid/text/TextPaint;
    move-object/from16 v0, p0

    #@1a
    iget-object v11, v0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@1c
    .line 444
    .local v11, buf:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    #@1e
    iget-object v0, v0, Landroid/text/Layout;->mAlignment:Landroid/text/Layout$Alignment;

    #@20
    move-object/from16 v34, v0

    #@22
    .line 445
    .local v34, paraAlign:Landroid/text/Layout$Alignment;
    const/16 v27, 0x0

    #@24
    .line 446
    .local v27, tabStops:Landroid/text/Layout$TabStops;
    const/16 v42, 0x0

    #@26
    .line 448
    .local v42, tabStopsIsInitialized:Z
    invoke-static {}, Landroid/text/TextLine;->obtain()Landroid/text/TextLine;

    #@29
    move-result-object v44

    #@2a
    .line 452
    .local v44, tl:Landroid/text/TextLine;
    move/from16 v30, p2

    #@2c
    .local v30, i:I
    move-object/from16 v41, v27

    #@2e
    .end local v27           #tabStops:Landroid/text/Layout$TabStops;
    .local v41, tabStops:Landroid/text/Layout$TabStops;
    :goto_2e
    move/from16 v0, v30

    #@30
    move/from16 v1, p3

    #@32
    if-gt v0, v1, :cond_22c

    #@34
    .line 453
    move/from16 v12, v36

    #@36
    .line 454
    .local v12, start:I
    add-int/lit8 v4, v30, 0x1

    #@38
    move-object/from16 v0, p0

    #@3a
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getLineStart(I)I

    #@3d
    move-result v36

    #@3e
    .line 455
    move-object/from16 v0, p0

    #@40
    move/from16 v1, v30

    #@42
    move/from16 v2, v36

    #@44
    invoke-direct {v0, v1, v12, v2}, Landroid/text/Layout;->getLineVisibleEnd(III)I

    #@47
    move-result v13

    #@48
    .line 457
    .local v13, end:I
    move/from16 v8, v35

    #@4a
    .line 458
    .local v8, ltop:I
    add-int/lit8 v4, v30, 0x1

    #@4c
    move-object/from16 v0, p0

    #@4e
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getLineTop(I)I

    #@51
    move-result v10

    #@52
    .line 459
    .local v10, lbottom:I
    move/from16 v35, v10

    #@54
    .line 460
    move-object/from16 v0, p0

    #@56
    move/from16 v1, v30

    #@58
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineDescent(I)I

    #@5b
    move-result v4

    #@5c
    sub-int v9, v10, v4

    #@5e
    .line 462
    .local v9, lbaseline:I
    move-object/from16 v0, p0

    #@60
    move/from16 v1, v30

    #@62
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@65
    move-result v7

    #@66
    .line 463
    .local v7, dir:I
    const/16 v18, 0x0

    #@68
    .line 464
    .local v18, left:I
    move-object/from16 v0, p0

    #@6a
    iget v6, v0, Landroid/text/Layout;->mWidth:I

    #@6c
    .line 466
    .local v6, right:I
    move-object/from16 v0, p0

    #@6e
    iget-boolean v4, v0, Landroid/text/Layout;->mSpannedText:Z

    #@70
    if-eqz v4, :cond_13e

    #@72
    move-object/from16 v37, v11

    #@74
    .line 467
    check-cast v37, Landroid/text/Spanned;

    #@76
    .line 468
    .local v37, sp:Landroid/text/Spanned;
    invoke-interface {v11}, Ljava/lang/CharSequence;->length()I

    #@79
    move-result v43

    #@7a
    .line 469
    .local v43, textLength:I
    if-eqz v12, :cond_86

    #@7c
    add-int/lit8 v4, v12, -0x1

    #@7e
    invoke-interface {v11, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@81
    move-result v4

    #@82
    const/16 v15, 0xa

    #@84
    if-ne v4, v15, :cond_112

    #@86
    :cond_86
    const/4 v14, 0x1

    #@87
    .line 481
    .local v14, isFirstParaLine:Z
    :goto_87
    move/from16 v0, v38

    #@89
    if-lt v12, v0, :cond_c6

    #@8b
    move/from16 v0, v30

    #@8d
    move/from16 v1, p2

    #@8f
    if-eq v0, v1, :cond_93

    #@91
    if-eqz v14, :cond_c6

    #@93
    .line 482
    :cond_93
    const-class v4, Landroid/text/style/ParagraphStyle;

    #@95
    move-object/from16 v0, v37

    #@97
    move/from16 v1, v43

    #@99
    invoke-interface {v0, v12, v1, v4}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    #@9c
    move-result v38

    #@9d
    .line 484
    const-class v4, Landroid/text/style/ParagraphStyle;

    #@9f
    move-object/from16 v0, v37

    #@a1
    move/from16 v1, v38

    #@a3
    invoke-static {v0, v12, v1, v4}, Landroid/text/Layout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@a6
    move-result-object v39

    #@a7
    .end local v39           #spans:[Landroid/text/style/ParagraphStyle;
    check-cast v39, [Landroid/text/style/ParagraphStyle;

    #@a9
    .line 486
    .restart local v39       #spans:[Landroid/text/style/ParagraphStyle;
    move-object/from16 v0, p0

    #@ab
    iget-object v0, v0, Landroid/text/Layout;->mAlignment:Landroid/text/Layout$Alignment;

    #@ad
    move-object/from16 v34, v0

    #@af
    .line 487
    move-object/from16 v0, v39

    #@b1
    array-length v4, v0

    #@b2
    add-int/lit8 v33, v4, -0x1

    #@b4
    .local v33, n:I
    :goto_b4
    if-ltz v33, :cond_c4

    #@b6
    .line 488
    aget-object v4, v39, v33

    #@b8
    instance-of v4, v4, Landroid/text/style/AlignmentSpan;

    #@ba
    if-eqz v4, :cond_115

    #@bc
    .line 489
    aget-object v4, v39, v33

    #@be
    check-cast v4, Landroid/text/style/AlignmentSpan;

    #@c0
    invoke-interface {v4}, Landroid/text/style/AlignmentSpan;->getAlignment()Landroid/text/Layout$Alignment;

    #@c3
    move-result-object v34

    #@c4
    .line 494
    :cond_c4
    const/16 v42, 0x0

    #@c6
    .line 499
    .end local v33           #n:I
    :cond_c6
    move-object/from16 v0, v39

    #@c8
    array-length v0, v0

    #@c9
    move/from16 v31, v0

    #@cb
    .line 500
    .local v31, length:I
    const/16 v33, 0x0

    #@cd
    .restart local v33       #n:I
    :goto_cd
    move/from16 v0, v33

    #@cf
    move/from16 v1, v31

    #@d1
    if-ge v0, v1, :cond_13e

    #@d3
    .line 501
    aget-object v4, v39, v33

    #@d5
    instance-of v4, v4, Landroid/text/style/LeadingMarginSpan;

    #@d7
    if-eqz v4, :cond_10f

    #@d9
    .line 502
    aget-object v3, v39, v33

    #@db
    check-cast v3, Landroid/text/style/LeadingMarginSpan;

    #@dd
    .line 503
    .local v3, margin:Landroid/text/style/LeadingMarginSpan;
    move/from16 v45, v14

    #@df
    .line 504
    .local v45, useFirstLineMargin:Z
    instance-of v4, v3, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;

    #@e1
    if-eqz v4, :cond_fe

    #@e3
    move-object v4, v3

    #@e4
    .line 505
    check-cast v4, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;

    #@e6
    invoke-interface {v4}, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;->getLeadingMarginLineCount()I

    #@e9
    move-result v29

    #@ea
    .line 506
    .local v29, count:I
    move-object/from16 v0, v37

    #@ec
    invoke-interface {v0, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@ef
    move-result v4

    #@f0
    move-object/from16 v0, p0

    #@f2
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    #@f5
    move-result v40

    #@f6
    .line 507
    .local v40, startLine:I
    add-int v4, v40, v29

    #@f8
    move/from16 v0, v30

    #@fa
    if-ge v0, v4, :cond_118

    #@fc
    const/16 v45, 0x1

    #@fe
    .line 510
    .end local v29           #count:I
    .end local v40           #startLine:I
    :cond_fe
    :goto_fe
    const/4 v4, -0x1

    #@ff
    if-ne v7, v4, :cond_11b

    #@101
    move-object/from16 v4, p1

    #@103
    move-object/from16 v15, p0

    #@105
    .line 511
    invoke-interface/range {v3 .. v15}, Landroid/text/style/LeadingMarginSpan;->drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V

    #@108
    .line 514
    move/from16 v0, v45

    #@10a
    invoke-interface {v3, v0}, Landroid/text/style/LeadingMarginSpan;->getLeadingMargin(Z)I

    #@10d
    move-result v4

    #@10e
    sub-int/2addr v6, v4

    #@10f
    .line 500
    .end local v3           #margin:Landroid/text/style/LeadingMarginSpan;
    .end local v45           #useFirstLineMargin:Z
    :cond_10f
    :goto_10f
    add-int/lit8 v33, v33, 0x1

    #@111
    goto :goto_cd

    #@112
    .line 469
    .end local v14           #isFirstParaLine:Z
    .end local v31           #length:I
    .end local v33           #n:I
    :cond_112
    const/4 v14, 0x0

    #@113
    goto/16 :goto_87

    #@115
    .line 487
    .restart local v14       #isFirstParaLine:Z
    .restart local v33       #n:I
    :cond_115
    add-int/lit8 v33, v33, -0x1

    #@117
    goto :goto_b4

    #@118
    .line 507
    .restart local v3       #margin:Landroid/text/style/LeadingMarginSpan;
    .restart local v29       #count:I
    .restart local v31       #length:I
    .restart local v40       #startLine:I
    .restart local v45       #useFirstLineMargin:Z
    :cond_118
    const/16 v45, 0x0

    #@11a
    goto :goto_fe

    #@11b
    .end local v29           #count:I
    .end local v40           #startLine:I
    :cond_11b
    move-object v15, v3

    #@11c
    move-object/from16 v16, p1

    #@11e
    move-object/from16 v17, v5

    #@120
    move/from16 v19, v7

    #@122
    move/from16 v20, v8

    #@124
    move/from16 v21, v9

    #@126
    move/from16 v22, v10

    #@128
    move-object/from16 v23, v11

    #@12a
    move/from16 v24, v12

    #@12c
    move/from16 v25, v13

    #@12e
    move/from16 v26, v14

    #@130
    move-object/from16 v27, p0

    #@132
    .line 516
    invoke-interface/range {v15 .. v27}, Landroid/text/style/LeadingMarginSpan;->drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V

    #@135
    .line 519
    move/from16 v0, v45

    #@137
    invoke-interface {v3, v0}, Landroid/text/style/LeadingMarginSpan;->getLeadingMargin(Z)I

    #@13a
    move-result v4

    #@13b
    add-int v18, v18, v4

    #@13d
    goto :goto_10f

    #@13e
    .line 525
    .end local v3           #margin:Landroid/text/style/LeadingMarginSpan;
    .end local v14           #isFirstParaLine:Z
    .end local v31           #length:I
    .end local v33           #n:I
    .end local v37           #sp:Landroid/text/Spanned;
    .end local v43           #textLength:I
    .end local v45           #useFirstLineMargin:Z
    :cond_13e
    const/16 v26, 0x0

    #@140
    .line 526
    .local v26, hasTabOrEmoji:Z
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@142
    if-eqz v4, :cond_1b9

    #@144
    .line 527
    move-object/from16 v0, p0

    #@146
    move/from16 v1, v30

    #@148
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineContainsTab(I)Z

    #@14b
    move-result v4

    #@14c
    if-nez v4, :cond_158

    #@14e
    move-object/from16 v0, p0

    #@150
    move/from16 v1, v30

    #@152
    invoke-direct {v0, v1}, Landroid/text/Layout;->getLineContainsEmoji(I)Z

    #@155
    move-result v4

    #@156
    if-eqz v4, :cond_1b6

    #@158
    :cond_158
    const/16 v26, 0x1

    #@15a
    .line 533
    :goto_15a
    if-eqz v26, :cond_230

    #@15c
    if-nez v42, :cond_230

    #@15e
    .line 534
    if-nez v41, :cond_1c2

    #@160
    .line 535
    new-instance v27, Landroid/text/Layout$TabStops;

    #@162
    const/16 v4, 0x14

    #@164
    move-object/from16 v0, v27

    #@166
    move-object/from16 v1, v39

    #@168
    invoke-direct {v0, v4, v1}, Landroid/text/Layout$TabStops;-><init>(I[Ljava/lang/Object;)V

    #@16b
    .line 539
    .end local v41           #tabStops:Landroid/text/Layout$TabStops;
    .restart local v27       #tabStops:Landroid/text/Layout$TabStops;
    :goto_16b
    const/16 v42, 0x1

    #@16d
    .line 543
    :goto_16d
    move-object/from16 v28, v34

    #@16f
    .line 544
    .local v28, align:Landroid/text/Layout$Alignment;
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    #@171
    move-object/from16 v0, v28

    #@173
    if-ne v0, v4, :cond_1d1

    #@175
    .line 545
    const/4 v4, 0x1

    #@176
    if-ne v7, v4, :cond_1ce

    #@178
    sget-object v28, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@17a
    .line 553
    :cond_17a
    :goto_17a
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@17c
    move-object/from16 v0, v28

    #@17e
    if-ne v0, v4, :cond_1e3

    #@180
    .line 554
    const/4 v4, 0x1

    #@181
    if-ne v7, v4, :cond_1e0

    #@183
    .line 555
    move/from16 v46, v18

    #@185
    .line 573
    .local v46, x:I
    :goto_185
    move-object/from16 v0, p0

    #@187
    move/from16 v1, v30

    #@189
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@18c
    move-result-object v25

    #@18d
    .line 574
    .local v25, directions:Landroid/text/Layout$Directions;
    sget-object v4, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@18f
    move-object/from16 v0, v25

    #@191
    if-ne v0, v4, :cond_20a

    #@193
    move-object/from16 v0, p0

    #@195
    iget-boolean v4, v0, Landroid/text/Layout;->mSpannedText:Z

    #@197
    if-nez v4, :cond_20a

    #@199
    if-nez v26, :cond_20a

    #@19b
    .line 576
    move/from16 v0, v46

    #@19d
    int-to-float v0, v0

    #@19e
    move/from16 v23, v0

    #@1a0
    int-to-float v0, v9

    #@1a1
    move/from16 v24, v0

    #@1a3
    move-object/from16 v19, p1

    #@1a5
    move-object/from16 v20, v11

    #@1a7
    move/from16 v21, v12

    #@1a9
    move/from16 v22, v13

    #@1ab
    move-object/from16 v25, v5

    #@1ad
    invoke-virtual/range {v19 .. v25}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    #@1b0
    .line 452
    .end local v25           #directions:Landroid/text/Layout$Directions;
    :goto_1b0
    add-int/lit8 v30, v30, 0x1

    #@1b2
    move-object/from16 v41, v27

    #@1b4
    .end local v27           #tabStops:Landroid/text/Layout$TabStops;
    .restart local v41       #tabStops:Landroid/text/Layout$TabStops;
    goto/16 :goto_2e

    #@1b6
    .line 527
    .end local v28           #align:Landroid/text/Layout$Alignment;
    .end local v46           #x:I
    :cond_1b6
    const/16 v26, 0x0

    #@1b8
    goto :goto_15a

    #@1b9
    .line 529
    :cond_1b9
    move-object/from16 v0, p0

    #@1bb
    move/from16 v1, v30

    #@1bd
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineContainsTab(I)Z

    #@1c0
    move-result v26

    #@1c1
    goto :goto_15a

    #@1c2
    .line 537
    :cond_1c2
    const/16 v4, 0x14

    #@1c4
    move-object/from16 v0, v41

    #@1c6
    move-object/from16 v1, v39

    #@1c8
    invoke-virtual {v0, v4, v1}, Landroid/text/Layout$TabStops;->reset(I[Ljava/lang/Object;)V

    #@1cb
    move-object/from16 v27, v41

    #@1cd
    .end local v41           #tabStops:Landroid/text/Layout$TabStops;
    .restart local v27       #tabStops:Landroid/text/Layout$TabStops;
    goto :goto_16b

    #@1ce
    .line 545
    .restart local v28       #align:Landroid/text/Layout$Alignment;
    :cond_1ce
    sget-object v28, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@1d0
    goto :goto_17a

    #@1d1
    .line 547
    :cond_1d1
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@1d3
    move-object/from16 v0, v28

    #@1d5
    if-ne v0, v4, :cond_17a

    #@1d7
    .line 548
    const/4 v4, 0x1

    #@1d8
    if-ne v7, v4, :cond_1dd

    #@1da
    sget-object v28, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@1dc
    :goto_1dc
    goto :goto_17a

    #@1dd
    :cond_1dd
    sget-object v28, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@1df
    goto :goto_1dc

    #@1e0
    .line 557
    :cond_1e0
    move/from16 v46, v6

    #@1e2
    .restart local v46       #x:I
    goto :goto_185

    #@1e3
    .line 560
    .end local v46           #x:I
    :cond_1e3
    const/4 v4, 0x0

    #@1e4
    move-object/from16 v0, p0

    #@1e6
    move/from16 v1, v30

    #@1e8
    move-object/from16 v2, v27

    #@1ea
    invoke-direct {v0, v1, v2, v4}, Landroid/text/Layout;->getLineExtent(ILandroid/text/Layout$TabStops;Z)F

    #@1ed
    move-result v4

    #@1ee
    float-to-int v0, v4

    #@1ef
    move/from16 v32, v0

    #@1f1
    .line 561
    .local v32, max:I
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@1f3
    move-object/from16 v0, v28

    #@1f5
    if-ne v0, v4, :cond_200

    #@1f7
    .line 562
    const/4 v4, 0x1

    #@1f8
    if-ne v7, v4, :cond_1fd

    #@1fa
    .line 563
    sub-int v46, v6, v32

    #@1fc
    .restart local v46       #x:I
    goto :goto_185

    #@1fd
    .line 565
    .end local v46           #x:I
    :cond_1fd
    sub-int v46, v18, v32

    #@1ff
    .restart local v46       #x:I
    goto :goto_185

    #@200
    .line 568
    .end local v46           #x:I
    :cond_200
    and-int/lit8 v32, v32, -0x2

    #@202
    .line 569
    add-int v4, v6, v18

    #@204
    sub-int v4, v4, v32

    #@206
    shr-int/lit8 v46, v4, 0x1

    #@208
    .restart local v46       #x:I
    goto/16 :goto_185

    #@20a
    .end local v32           #max:I
    .restart local v25       #directions:Landroid/text/Layout$Directions;
    :cond_20a
    move-object/from16 v19, v44

    #@20c
    move-object/from16 v20, v5

    #@20e
    move-object/from16 v21, v11

    #@210
    move/from16 v22, v12

    #@212
    move/from16 v23, v13

    #@214
    move/from16 v24, v7

    #@216
    .line 578
    invoke-virtual/range {v19 .. v27}, Landroid/text/TextLine;->set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;)V

    #@219
    .line 579
    move/from16 v0, v46

    #@21b
    int-to-float v0, v0

    #@21c
    move/from16 v21, v0

    #@21e
    move-object/from16 v19, v44

    #@220
    move-object/from16 v20, p1

    #@222
    move/from16 v22, v8

    #@224
    move/from16 v23, v9

    #@226
    move/from16 v24, v10

    #@228
    invoke-virtual/range {v19 .. v24}, Landroid/text/TextLine;->draw(Landroid/graphics/Canvas;FIII)V

    #@22b
    goto :goto_1b0

    #@22c
    .line 583
    .end local v6           #right:I
    .end local v7           #dir:I
    .end local v8           #ltop:I
    .end local v9           #lbaseline:I
    .end local v10           #lbottom:I
    .end local v12           #start:I
    .end local v13           #end:I
    .end local v18           #left:I
    .end local v25           #directions:Landroid/text/Layout$Directions;
    .end local v26           #hasTabOrEmoji:Z
    .end local v27           #tabStops:Landroid/text/Layout$TabStops;
    .end local v28           #align:Landroid/text/Layout$Alignment;
    .end local v46           #x:I
    .restart local v41       #tabStops:Landroid/text/Layout$TabStops;
    :cond_22c
    invoke-static/range {v44 .. v44}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    #@22f
    .line 584
    return-void

    #@230
    .restart local v6       #right:I
    .restart local v7       #dir:I
    .restart local v8       #ltop:I
    .restart local v9       #lbaseline:I
    .restart local v10       #lbottom:I
    .restart local v12       #start:I
    .restart local v13       #end:I
    .restart local v18       #left:I
    .restart local v26       #hasTabOrEmoji:Z
    :cond_230
    move-object/from16 v27, v41

    #@232
    .end local v41           #tabStops:Landroid/text/Layout$TabStops;
    .restart local v27       #tabStops:Landroid/text/Layout$TabStops;
    goto/16 :goto_16d
.end method

.method public final getAlignment()Landroid/text/Layout$Alignment;
    .registers 2

    #@0
    .prologue
    .line 806
    iget-object v0, p0, Landroid/text/Layout;->mAlignment:Landroid/text/Layout$Alignment;

    #@2
    return-object v0
.end method

.method public abstract getBottomPadding()I
.end method

.method public getCursorPath(ILandroid/graphics/Path;Ljava/lang/CharSequence;)V
    .registers 15
    .parameter "point"
    .parameter "dest"
    .parameter "editingBuffer"

    #@0
    .prologue
    .line 1540
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    #@3
    .line 1542
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@6
    move-result v6

    #@7
    .line 1543
    .local v6, line:I
    invoke-virtual {p0, v6}, Landroid/text/Layout;->getLineTop(I)I

    #@a
    move-result v7

    #@b
    .line 1544
    .local v7, top:I
    add-int/lit8 v8, v6, 0x1

    #@d
    invoke-virtual {p0, v8}, Landroid/text/Layout;->getLineTop(I)I

    #@10
    move-result v0

    #@11
    .line 1546
    .local v0, bottom:I
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@14
    move-result v8

    #@15
    const/high16 v9, 0x3f00

    #@17
    sub-float v4, v8, v9

    #@19
    .line 1547
    .local v4, h1:F
    invoke-virtual {p0, p1}, Landroid/text/Layout;->isLevelBoundary(I)Z

    #@1c
    move-result v8

    #@1d
    if-eqz v8, :cond_9f

    #@1f
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getSecondaryHorizontal(I)F

    #@22
    move-result v8

    #@23
    const/high16 v9, 0x3f00

    #@25
    sub-float v5, v8, v9

    #@27
    .line 1549
    .local v5, h2:F
    :goto_27
    const/4 v8, 0x1

    #@28
    invoke-static {p3, v8}, Landroid/text/method/TextKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@2b
    move-result v8

    #@2c
    const/16 v9, 0x800

    #@2e
    invoke-static {p3, v9}, Landroid/text/method/TextKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@31
    move-result v9

    #@32
    or-int v1, v8, v9

    #@34
    .line 1551
    .local v1, caps:I
    const/4 v8, 0x2

    #@35
    invoke-static {p3, v8}, Landroid/text/method/TextKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@38
    move-result v3

    #@39
    .line 1552
    .local v3, fn:I
    const/4 v2, 0x0

    #@3a
    .line 1554
    .local v2, dist:I
    if-nez v1, :cond_3e

    #@3c
    if-eqz v3, :cond_48

    #@3e
    .line 1555
    :cond_3e
    sub-int v8, v0, v7

    #@40
    shr-int/lit8 v2, v8, 0x2

    #@42
    .line 1557
    if-eqz v3, :cond_45

    #@44
    .line 1558
    add-int/2addr v7, v2

    #@45
    .line 1559
    :cond_45
    if-eqz v1, :cond_48

    #@47
    .line 1560
    sub-int/2addr v0, v2

    #@48
    .line 1563
    :cond_48
    const/high16 v8, 0x3f00

    #@4a
    cmpg-float v8, v4, v8

    #@4c
    if-gez v8, :cond_50

    #@4e
    .line 1564
    const/high16 v4, 0x3f00

    #@50
    .line 1565
    :cond_50
    const/high16 v8, 0x3f00

    #@52
    cmpg-float v8, v5, v8

    #@54
    if-gez v8, :cond_58

    #@56
    .line 1566
    const/high16 v5, 0x3f00

    #@58
    .line 1568
    :cond_58
    invoke-static {v4, v5}, Ljava/lang/Float;->compare(FF)I

    #@5b
    move-result v8

    #@5c
    if-nez v8, :cond_a1

    #@5e
    .line 1569
    int-to-float v8, v7

    #@5f
    invoke-virtual {p2, v4, v8}, Landroid/graphics/Path;->moveTo(FF)V

    #@62
    .line 1570
    int-to-float v8, v0

    #@63
    invoke-virtual {p2, v4, v8}, Landroid/graphics/Path;->lineTo(FF)V

    #@66
    .line 1579
    :goto_66
    const/4 v8, 0x2

    #@67
    if-ne v1, v8, :cond_ba

    #@69
    .line 1580
    int-to-float v8, v0

    #@6a
    invoke-virtual {p2, v5, v8}, Landroid/graphics/Path;->moveTo(FF)V

    #@6d
    .line 1581
    int-to-float v8, v2

    #@6e
    sub-float v8, v5, v8

    #@70
    add-int v9, v0, v2

    #@72
    int-to-float v9, v9

    #@73
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    #@76
    .line 1582
    int-to-float v8, v0

    #@77
    invoke-virtual {p2, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    #@7a
    .line 1583
    int-to-float v8, v2

    #@7b
    add-float/2addr v8, v5

    #@7c
    add-int v9, v0, v2

    #@7e
    int-to-float v9, v9

    #@7f
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    #@82
    .line 1595
    :cond_82
    :goto_82
    const/4 v8, 0x2

    #@83
    if-ne v3, v8, :cond_ee

    #@85
    .line 1596
    int-to-float v8, v7

    #@86
    invoke-virtual {p2, v4, v8}, Landroid/graphics/Path;->moveTo(FF)V

    #@89
    .line 1597
    int-to-float v8, v2

    #@8a
    sub-float v8, v4, v8

    #@8c
    sub-int v9, v7, v2

    #@8e
    int-to-float v9, v9

    #@8f
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    #@92
    .line 1598
    int-to-float v8, v7

    #@93
    invoke-virtual {p2, v4, v8}, Landroid/graphics/Path;->lineTo(FF)V

    #@96
    .line 1599
    int-to-float v8, v2

    #@97
    add-float/2addr v8, v4

    #@98
    sub-int v9, v7, v2

    #@9a
    int-to-float v9, v9

    #@9b
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    #@9e
    .line 1610
    :cond_9e
    :goto_9e
    return-void

    #@9f
    .end local v1           #caps:I
    .end local v2           #dist:I
    .end local v3           #fn:I
    .end local v5           #h2:F
    :cond_9f
    move v5, v4

    #@a0
    .line 1547
    goto :goto_27

    #@a1
    .line 1572
    .restart local v1       #caps:I
    .restart local v2       #dist:I
    .restart local v3       #fn:I
    .restart local v5       #h2:F
    :cond_a1
    int-to-float v8, v7

    #@a2
    invoke-virtual {p2, v4, v8}, Landroid/graphics/Path;->moveTo(FF)V

    #@a5
    .line 1573
    add-int v8, v7, v0

    #@a7
    shr-int/lit8 v8, v8, 0x1

    #@a9
    int-to-float v8, v8

    #@aa
    invoke-virtual {p2, v4, v8}, Landroid/graphics/Path;->lineTo(FF)V

    #@ad
    .line 1575
    add-int v8, v7, v0

    #@af
    shr-int/lit8 v8, v8, 0x1

    #@b1
    int-to-float v8, v8

    #@b2
    invoke-virtual {p2, v5, v8}, Landroid/graphics/Path;->moveTo(FF)V

    #@b5
    .line 1576
    int-to-float v8, v0

    #@b6
    invoke-virtual {p2, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    #@b9
    goto :goto_66

    #@ba
    .line 1584
    :cond_ba
    const/4 v8, 0x1

    #@bb
    if-ne v1, v8, :cond_82

    #@bd
    .line 1585
    int-to-float v8, v0

    #@be
    invoke-virtual {p2, v5, v8}, Landroid/graphics/Path;->moveTo(FF)V

    #@c1
    .line 1586
    int-to-float v8, v2

    #@c2
    sub-float v8, v5, v8

    #@c4
    add-int v9, v0, v2

    #@c6
    int-to-float v9, v9

    #@c7
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    #@ca
    .line 1588
    int-to-float v8, v2

    #@cb
    sub-float v8, v5, v8

    #@cd
    add-int v9, v0, v2

    #@cf
    int-to-float v9, v9

    #@d0
    const/high16 v10, 0x3f00

    #@d2
    sub-float/2addr v9, v10

    #@d3
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    #@d6
    .line 1589
    int-to-float v8, v2

    #@d7
    add-float/2addr v8, v5

    #@d8
    add-int v9, v0, v2

    #@da
    int-to-float v9, v9

    #@db
    const/high16 v10, 0x3f00

    #@dd
    sub-float/2addr v9, v10

    #@de
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    #@e1
    .line 1591
    int-to-float v8, v2

    #@e2
    add-float/2addr v8, v5

    #@e3
    add-int v9, v0, v2

    #@e5
    int-to-float v9, v9

    #@e6
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    #@e9
    .line 1592
    int-to-float v8, v0

    #@ea
    invoke-virtual {p2, v5, v8}, Landroid/graphics/Path;->lineTo(FF)V

    #@ed
    goto :goto_82

    #@ee
    .line 1600
    :cond_ee
    const/4 v8, 0x1

    #@ef
    if-ne v3, v8, :cond_9e

    #@f1
    .line 1601
    int-to-float v8, v7

    #@f2
    invoke-virtual {p2, v4, v8}, Landroid/graphics/Path;->moveTo(FF)V

    #@f5
    .line 1602
    int-to-float v8, v2

    #@f6
    sub-float v8, v4, v8

    #@f8
    sub-int v9, v7, v2

    #@fa
    int-to-float v9, v9

    #@fb
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    #@fe
    .line 1604
    int-to-float v8, v2

    #@ff
    sub-float v8, v4, v8

    #@101
    sub-int v9, v7, v2

    #@103
    int-to-float v9, v9

    #@104
    const/high16 v10, 0x3f00

    #@106
    add-float/2addr v9, v10

    #@107
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    #@10a
    .line 1605
    int-to-float v8, v2

    #@10b
    add-float/2addr v8, v4

    #@10c
    sub-int v9, v7, v2

    #@10e
    int-to-float v9, v9

    #@10f
    const/high16 v10, 0x3f00

    #@111
    add-float/2addr v9, v10

    #@112
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    #@115
    .line 1607
    int-to-float v8, v2

    #@116
    add-float/2addr v8, v4

    #@117
    sub-int v9, v7, v2

    #@119
    int-to-float v9, v9

    #@11a
    invoke-virtual {p2, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    #@11d
    .line 1608
    int-to-float v8, v7

    #@11e
    invoke-virtual {p2, v4, v8}, Landroid/graphics/Path;->lineTo(FF)V

    #@121
    goto/16 :goto_9e
.end method

.method public abstract getEllipsisCount(I)I
.end method

.method public abstract getEllipsisStart(I)I
.end method

.method public getEllipsizedWidth()I
    .registers 2

    #@0
    .prologue
    .line 779
    iget v0, p0, Landroid/text/Layout;->mWidth:I

    #@2
    return v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 799
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final getLineAscent(I)I
    .registers 5
    .parameter "line"

    #@0
    .prologue
    .line 1411
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineTop(I)I

    #@3
    move-result v0

    #@4
    add-int/lit8 v1, p1, 0x1

    #@6
    invoke-virtual {p0, v1}, Landroid/text/Layout;->getLineTop(I)I

    #@9
    move-result v1

    #@a
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineDescent(I)I

    #@d
    move-result v2

    #@e
    sub-int/2addr v1, v2

    #@f
    sub-int/2addr v0, v1

    #@10
    return v0
.end method

.method public final getLineBaseline(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1402
    add-int/lit8 v0, p1, 0x1

    #@2
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@5
    move-result v0

    #@6
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineDescent(I)I

    #@9
    move-result v1

    #@a
    sub-int/2addr v0, v1

    #@b
    return v0
.end method

.method public final getLineBottom(I)I
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 1394
    add-int/lit8 v0, p1, 0x1

    #@2
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getLineBounds(ILandroid/graphics/Rect;)I
    .registers 4
    .parameter "line"
    .parameter "bounds"

    #@0
    .prologue
    .line 845
    if-eqz p2, :cond_17

    #@2
    .line 846
    const/4 v0, 0x0

    #@3
    iput v0, p2, Landroid/graphics/Rect;->left:I

    #@5
    .line 847
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineTop(I)I

    #@8
    move-result v0

    #@9
    iput v0, p2, Landroid/graphics/Rect;->top:I

    #@b
    .line 848
    iget v0, p0, Landroid/text/Layout;->mWidth:I

    #@d
    iput v0, p2, Landroid/graphics/Rect;->right:I

    #@f
    .line 849
    add-int/lit8 v0, p1, 0x1

    #@11
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@14
    move-result v0

    #@15
    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    #@17
    .line 851
    :cond_17
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineBaseline(I)I

    #@1a
    move-result v0

    #@1b
    return v0
.end method

.method public abstract getLineContainsTab(I)Z
.end method

.method public abstract getLineCount()I
.end method

.method public abstract getLineDescent(I)I
.end method

.method public abstract getLineDirections(I)Landroid/text/Layout$Directions;
.end method

.method public final getLineEnd(I)I
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 1356
    add-int/lit8 v0, p1, 0x1

    #@2
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineStart(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getLineForOffset(I)I
    .registers 7
    .parameter "offset"

    #@0
    .prologue
    .line 1246
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    #@3
    move-result v1

    #@4
    .local v1, high:I
    const/4 v2, -0x1

    #@5
    .line 1248
    .local v2, low:I
    :goto_5
    sub-int v3, v1, v2

    #@7
    const/4 v4, 0x1

    #@8
    if-le v3, v4, :cond_18

    #@a
    .line 1249
    add-int v3, v1, v2

    #@c
    div-int/lit8 v0, v3, 0x2

    #@e
    .line 1251
    .local v0, guess:I
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineStart(I)I

    #@11
    move-result v3

    #@12
    if-le v3, p1, :cond_16

    #@14
    .line 1252
    move v1, v0

    #@15
    goto :goto_5

    #@16
    .line 1254
    :cond_16
    move v2, v0

    #@17
    goto :goto_5

    #@18
    .line 1257
    .end local v0           #guess:I
    :cond_18
    if-gez v2, :cond_1b

    #@1a
    .line 1258
    const/4 v2, 0x0

    #@1b
    .line 1260
    .end local v2           #low:I
    :cond_1b
    return v2
.end method

.method public getLineForVertical(I)I
    .registers 7
    .parameter "vertical"

    #@0
    .prologue
    .line 1223
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    #@3
    move-result v1

    #@4
    .local v1, high:I
    const/4 v2, -0x1

    #@5
    .line 1225
    .local v2, low:I
    :goto_5
    sub-int v3, v1, v2

    #@7
    const/4 v4, 0x1

    #@8
    if-le v3, v4, :cond_18

    #@a
    .line 1226
    add-int v3, v1, v2

    #@c
    div-int/lit8 v0, v3, 0x2

    #@e
    .line 1228
    .local v0, guess:I
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@11
    move-result v3

    #@12
    if-le v3, p1, :cond_16

    #@14
    .line 1229
    move v1, v0

    #@15
    goto :goto_5

    #@16
    .line 1231
    :cond_16
    move v2, v0

    #@17
    goto :goto_5

    #@18
    .line 1234
    .end local v0           #guess:I
    :cond_18
    if-gez v2, :cond_1b

    #@1a
    .line 1235
    const/4 v2, 0x0

    #@1b
    .line 1237
    .end local v2           #low:I
    :cond_1b
    return v2
.end method

.method public getLineLeft(I)F
    .registers 10
    .parameter "line"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1080
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@5
    move-result v1

    #@6
    .line 1081
    .local v1, dir:I
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphAlignment(I)Landroid/text/Layout$Alignment;

    #@9
    move-result-object v0

    #@a
    .line 1083
    .local v0, align:Landroid/text/Layout$Alignment;
    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    #@c
    if-ne v0, v6, :cond_f

    #@e
    .line 1102
    :cond_e
    :goto_e
    return v5

    #@f
    .line 1085
    :cond_f
    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@11
    if-ne v0, v6, :cond_20

    #@13
    .line 1086
    if-ne v1, v7, :cond_e

    #@15
    .line 1087
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphRight(I)I

    #@18
    move-result v5

    #@19
    int-to-float v5, v5

    #@1a
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineMax(I)F

    #@1d
    move-result v6

    #@1e
    sub-float/2addr v5, v6

    #@1f
    goto :goto_e

    #@20
    .line 1090
    :cond_20
    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@22
    if-ne v0, v6, :cond_2d

    #@24
    .line 1091
    iget v5, p0, Landroid/text/Layout;->mWidth:I

    #@26
    int-to-float v5, v5

    #@27
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineMax(I)F

    #@2a
    move-result v6

    #@2b
    sub-float/2addr v5, v6

    #@2c
    goto :goto_e

    #@2d
    .line 1092
    :cond_2d
    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@2f
    if-ne v0, v6, :cond_3c

    #@31
    .line 1093
    if-eq v1, v7, :cond_e

    #@33
    .line 1096
    iget v5, p0, Landroid/text/Layout;->mWidth:I

    #@35
    int-to-float v5, v5

    #@36
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineMax(I)F

    #@39
    move-result v6

    #@3a
    sub-float/2addr v5, v6

    #@3b
    goto :goto_e

    #@3c
    .line 1098
    :cond_3c
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphLeft(I)I

    #@3f
    move-result v2

    #@40
    .line 1099
    .local v2, left:I
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphRight(I)I

    #@43
    move-result v4

    #@44
    .line 1100
    .local v4, right:I
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineMax(I)F

    #@47
    move-result v5

    #@48
    float-to-int v5, v5

    #@49
    and-int/lit8 v3, v5, -0x2

    #@4b
    .line 1102
    .local v3, max:I
    sub-int v5, v4, v2

    #@4d
    sub-int/2addr v5, v3

    #@4e
    div-int/lit8 v5, v5, 0x2

    #@50
    add-int/2addr v5, v2

    #@51
    int-to-float v5, v5

    #@52
    goto :goto_e
.end method

.method public getLineMax(I)F
    .registers 6
    .parameter "line"

    #@0
    .prologue
    .line 1142
    invoke-direct {p0, p1}, Landroid/text/Layout;->getParagraphLeadingMargin(I)I

    #@3
    move-result v2

    #@4
    int-to-float v0, v2

    #@5
    .line 1143
    .local v0, margin:F
    const/4 v2, 0x0

    #@6
    invoke-direct {p0, p1, v2}, Landroid/text/Layout;->getLineExtent(IZ)F

    #@9
    move-result v1

    #@a
    .line 1144
    .local v1, signedExtent:F
    add-float v2, v0, v1

    #@c
    const/4 v3, 0x0

    #@d
    cmpl-float v2, v2, v3

    #@f
    if-ltz v2, :cond_12

    #@11
    .end local v1           #signedExtent:F
    :goto_11
    return v1

    #@12
    .restart local v1       #signedExtent:F
    :cond_12
    neg-float v1, v1

    #@13
    goto :goto_11
.end method

.method public getLineRangeForDraw(Landroid/graphics/Canvas;)J
    .registers 10
    .parameter "canvas"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 677
    sget-object v6, Landroid/text/Layout;->sTempRect:Landroid/graphics/Rect;

    #@4
    monitor-enter v6

    #@5
    .line 678
    :try_start_5
    sget-object v4, Landroid/text/Layout;->sTempRect:Landroid/graphics/Rect;

    #@7
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    #@a
    move-result v4

    #@b
    if-nez v4, :cond_15

    #@d
    .line 680
    const/4 v4, 0x0

    #@e
    const/4 v5, -0x1

    #@f
    invoke-static {v4, v5}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@12
    move-result-wide v4

    #@13
    monitor-exit v6

    #@14
    .line 691
    :goto_14
    return-wide v4

    #@15
    .line 683
    :cond_15
    sget-object v4, Landroid/text/Layout;->sTempRect:Landroid/graphics/Rect;

    #@17
    iget v2, v4, Landroid/graphics/Rect;->top:I

    #@19
    .line 684
    .local v2, dtop:I
    sget-object v4, Landroid/text/Layout;->sTempRect:Landroid/graphics/Rect;

    #@1b
    iget v1, v4, Landroid/graphics/Rect;->bottom:I

    #@1d
    .line 685
    .local v1, dbottom:I
    monitor-exit v6
    :try_end_1e
    .catchall {:try_start_5 .. :try_end_1e} :catchall_35

    #@1e
    .line 687
    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    #@21
    move-result v3

    #@22
    .line 688
    .local v3, top:I
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    #@25
    move-result v4

    #@26
    invoke-virtual {p0, v4}, Landroid/text/Layout;->getLineTop(I)I

    #@29
    move-result v4

    #@2a
    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    #@2d
    move-result v0

    #@2e
    .line 690
    .local v0, bottom:I
    if-lt v3, v0, :cond_38

    #@30
    invoke-static {v5, v7}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@33
    move-result-wide v4

    #@34
    goto :goto_14

    #@35
    .line 685
    .end local v0           #bottom:I
    .end local v1           #dbottom:I
    .end local v2           #dtop:I
    .end local v3           #top:I
    :catchall_35
    move-exception v4

    #@36
    :try_start_36
    monitor-exit v6
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    #@37
    throw v4

    #@38
    .line 691
    .restart local v0       #bottom:I
    .restart local v1       #dbottom:I
    .restart local v2       #dtop:I
    .restart local v3       #top:I
    :cond_38
    invoke-virtual {p0, v3}, Landroid/text/Layout;->getLineForVertical(I)I

    #@3b
    move-result v4

    #@3c
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineForVertical(I)I

    #@3f
    move-result v5

    #@40
    invoke-static {v4, v5}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@43
    move-result-wide v4

    #@44
    goto :goto_14
.end method

.method public getLineRight(I)F
    .registers 9
    .parameter "line"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 1111
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@4
    move-result v1

    #@5
    .line 1112
    .local v1, dir:I
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphAlignment(I)Landroid/text/Layout$Alignment;

    #@8
    move-result-object v0

    #@9
    .line 1114
    .local v0, align:Landroid/text/Layout$Alignment;
    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    #@b
    if-ne v0, v5, :cond_18

    #@d
    .line 1115
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphLeft(I)I

    #@10
    move-result v5

    #@11
    int-to-float v5, v5

    #@12
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineMax(I)F

    #@15
    move-result v6

    #@16
    add-float/2addr v5, v6

    #@17
    .line 1133
    :goto_17
    return v5

    #@18
    .line 1116
    :cond_18
    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@1a
    if-ne v0, v5, :cond_2d

    #@1c
    .line 1117
    if-ne v1, v6, :cond_22

    #@1e
    .line 1118
    iget v5, p0, Landroid/text/Layout;->mWidth:I

    #@20
    int-to-float v5, v5

    #@21
    goto :goto_17

    #@22
    .line 1120
    :cond_22
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphLeft(I)I

    #@25
    move-result v5

    #@26
    int-to-float v5, v5

    #@27
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineMax(I)F

    #@2a
    move-result v6

    #@2b
    add-float/2addr v5, v6

    #@2c
    goto :goto_17

    #@2d
    .line 1121
    :cond_2d
    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@2f
    if-ne v0, v5, :cond_35

    #@31
    .line 1122
    iget v5, p0, Landroid/text/Layout;->mWidth:I

    #@33
    int-to-float v5, v5

    #@34
    goto :goto_17

    #@35
    .line 1123
    :cond_35
    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@37
    if-ne v0, v5, :cond_44

    #@39
    .line 1124
    if-ne v1, v6, :cond_40

    #@3b
    .line 1125
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineMax(I)F

    #@3e
    move-result v5

    #@3f
    goto :goto_17

    #@40
    .line 1127
    :cond_40
    iget v5, p0, Landroid/text/Layout;->mWidth:I

    #@42
    int-to-float v5, v5

    #@43
    goto :goto_17

    #@44
    .line 1129
    :cond_44
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphLeft(I)I

    #@47
    move-result v2

    #@48
    .line 1130
    .local v2, left:I
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphRight(I)I

    #@4b
    move-result v4

    #@4c
    .line 1131
    .local v4, right:I
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineMax(I)F

    #@4f
    move-result v5

    #@50
    float-to-int v5, v5

    #@51
    and-int/lit8 v3, v5, -0x2

    #@53
    .line 1133
    .local v3, max:I
    sub-int v5, v4, v2

    #@55
    sub-int/2addr v5, v3

    #@56
    div-int/lit8 v5, v5, 0x2

    #@58
    sub-int v5, v4, v5

    #@5a
    int-to-float v5, v5

    #@5b
    goto :goto_17
.end method

.method public abstract getLineStart(I)I
.end method

.method public abstract getLineTop(I)I
.end method

.method public getLineVisibleEnd(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1364
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineStart(I)I

    #@3
    move-result v0

    #@4
    add-int/lit8 v1, p1, 0x1

    #@6
    invoke-virtual {p0, v1}, Landroid/text/Layout;->getLineStart(I)I

    #@9
    move-result v1

    #@a
    invoke-direct {p0, p1, v0, v1}, Landroid/text/Layout;->getLineVisibleEnd(III)I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public getLineWidth(I)F
    .registers 6
    .parameter "line"

    #@0
    .prologue
    .line 1152
    invoke-direct {p0, p1}, Landroid/text/Layout;->getParagraphLeadingMargin(I)I

    #@3
    move-result v2

    #@4
    int-to-float v0, v2

    #@5
    .line 1153
    .local v0, margin:F
    const/4 v2, 0x1

    #@6
    invoke-direct {p0, p1, v2}, Landroid/text/Layout;->getLineExtent(IZ)F

    #@9
    move-result v1

    #@a
    .line 1154
    .local v1, signedExtent:F
    add-float v2, v0, v1

    #@c
    const/4 v3, 0x0

    #@d
    cmpl-float v2, v2, v3

    #@f
    if-ltz v2, :cond_12

    #@11
    .end local v1           #signedExtent:F
    :goto_11
    return v1

    #@12
    .restart local v1       #signedExtent:F
    :cond_12
    neg-float v1, v1

    #@13
    goto :goto_11
.end method

.method public getOffsetForHorizontal(IF)I
    .registers 24
    .parameter "line"
    .parameter "horiz"

    #@0
    .prologue
    .line 1268
    invoke-virtual/range {p0 .. p1}, Landroid/text/Layout;->getLineEnd(I)I

    #@3
    move-result v19

    #@4
    add-int/lit8 v14, v19, -0x1

    #@6
    .line 1269
    .local v14, max:I
    invoke-virtual/range {p0 .. p1}, Landroid/text/Layout;->getLineStart(I)I

    #@9
    move-result v15

    #@a
    .line 1270
    .local v15, min:I
    invoke-virtual/range {p0 .. p1}, Landroid/text/Layout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@d
    move-result-object v7

    #@e
    .line 1272
    .local v7, dirs:Landroid/text/Layout$Directions;
    invoke-virtual/range {p0 .. p0}, Landroid/text/Layout;->getLineCount()I

    #@11
    move-result v19

    #@12
    add-int/lit8 v19, v19, -0x1

    #@14
    move/from16 v0, p1

    #@16
    move/from16 v1, v19

    #@18
    if-ne v0, v1, :cond_9a

    #@1a
    .line 1273
    add-int/lit8 v14, v14, 0x1

    #@1c
    .line 1283
    :cond_1c
    :goto_1c
    move v4, v15

    #@1d
    .line 1284
    .local v4, best:I
    move-object/from16 v0, p0

    #@1f
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@22
    move-result v19

    #@23
    sub-float v19, v19, p2

    #@25
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    #@28
    move-result v6

    #@29
    .line 1286
    .local v6, bestdist:F
    const/4 v12, 0x0

    #@2a
    .local v12, i:I
    :goto_2a
    iget-object v0, v7, Landroid/text/Layout$Directions;->mDirections:[I

    #@2c
    move-object/from16 v19, v0

    #@2e
    move-object/from16 v0, v19

    #@30
    array-length v0, v0

    #@31
    move/from16 v19, v0

    #@33
    move/from16 v0, v19

    #@35
    if-ge v12, v0, :cond_159

    #@37
    .line 1287
    iget-object v0, v7, Landroid/text/Layout$Directions;->mDirections:[I

    #@39
    move-object/from16 v19, v0

    #@3b
    aget v19, v19, v12

    #@3d
    add-int v10, v15, v19

    #@3f
    .line 1288
    .local v10, here:I
    iget-object v0, v7, Landroid/text/Layout$Directions;->mDirections:[I

    #@41
    move-object/from16 v19, v0

    #@43
    add-int/lit8 v20, v12, 0x1

    #@45
    aget v19, v19, v20

    #@47
    const v20, 0x3ffffff

    #@4a
    and-int v19, v19, v20

    #@4c
    add-int v18, v10, v19

    #@4e
    .line 1289
    .local v18, there:I
    iget-object v0, v7, Landroid/text/Layout$Directions;->mDirections:[I

    #@50
    move-object/from16 v19, v0

    #@52
    add-int/lit8 v20, v12, 0x1

    #@54
    aget v19, v19, v20

    #@56
    const/high16 v20, 0x400

    #@58
    and-int v19, v19, v20

    #@5a
    if-eqz v19, :cond_e8

    #@5c
    const/16 v17, -0x1

    #@5e
    .line 1291
    .local v17, swap:I
    :goto_5e
    move/from16 v0, v18

    #@60
    if-le v0, v14, :cond_64

    #@62
    .line 1292
    move/from16 v18, v14

    #@64
    .line 1293
    :cond_64
    add-int/lit8 v19, v18, -0x1

    #@66
    add-int/lit8 v11, v19, 0x1

    #@68
    .local v11, high:I
    add-int/lit8 v19, v10, 0x1

    #@6a
    add-int/lit8 v13, v19, -0x1

    #@6c
    .line 1295
    .local v13, low:I
    :goto_6c
    sub-int v19, v11, v13

    #@6e
    const/16 v20, 0x1

    #@70
    move/from16 v0, v19

    #@72
    move/from16 v1, v20

    #@74
    if-le v0, v1, :cond_ef

    #@76
    .line 1296
    add-int v19, v11, v13

    #@78
    div-int/lit8 v9, v19, 0x2

    #@7a
    .line 1297
    .local v9, guess:I
    move-object/from16 v0, p0

    #@7c
    invoke-direct {v0, v9}, Landroid/text/Layout;->getOffsetAtStartOf(I)I

    #@7f
    move-result v2

    #@80
    .line 1299
    .local v2, adguess:I
    move-object/from16 v0, p0

    #@82
    invoke-virtual {v0, v2}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@85
    move-result v19

    #@86
    move/from16 v0, v17

    #@88
    int-to-float v0, v0

    #@89
    move/from16 v20, v0

    #@8b
    mul-float v19, v19, v20

    #@8d
    move/from16 v0, v17

    #@8f
    int-to-float v0, v0

    #@90
    move/from16 v20, v0

    #@92
    mul-float v20, v20, p2

    #@94
    cmpl-float v19, v19, v20

    #@96
    if-ltz v19, :cond_ec

    #@98
    .line 1300
    move v11, v9

    #@99
    goto :goto_6c

    #@9a
    .line 1274
    .end local v2           #adguess:I
    .end local v4           #best:I
    .end local v6           #bestdist:F
    .end local v9           #guess:I
    .end local v10           #here:I
    .end local v11           #high:I
    .end local v12           #i:I
    .end local v13           #low:I
    .end local v17           #swap:I
    .end local v18           #there:I
    :cond_9a
    sget-boolean v19, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@9c
    if-eqz v19, :cond_1c

    #@9e
    .line 1275
    const/16 v19, 0x2

    #@a0
    move/from16 v0, v19

    #@a2
    if-le v14, v0, :cond_b6

    #@a4
    move-object/from16 v0, p0

    #@a6
    iget-object v0, v0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@a8
    move-object/from16 v19, v0

    #@aa
    add-int/lit8 v20, v14, -0x3

    #@ac
    invoke-static/range {v19 .. v20}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@af
    move-result v19

    #@b0
    if-eqz v19, :cond_b6

    #@b2
    .line 1276
    add-int/lit8 v14, v14, -0x3

    #@b4
    goto/16 :goto_1c

    #@b6
    .line 1277
    :cond_b6
    if-lez v14, :cond_1c

    #@b8
    move-object/from16 v0, p0

    #@ba
    iget-object v0, v0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@bc
    move-object/from16 v19, v0

    #@be
    add-int/lit8 v20, v14, -0x1

    #@c0
    invoke-interface/range {v19 .. v20}, Ljava/lang/CharSequence;->charAt(I)C

    #@c3
    move-result v19

    #@c4
    move-object/from16 v0, p0

    #@c6
    iget-object v0, v0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@c8
    move-object/from16 v20, v0

    #@ca
    move-object/from16 v0, v20

    #@cc
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    #@cf
    move-result v20

    #@d0
    invoke-static/range {v19 .. v20}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    #@d3
    move-result v19

    #@d4
    if-nez v19, :cond_e4

    #@d6
    move-object/from16 v0, p0

    #@d8
    iget-object v0, v0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@da
    move-object/from16 v19, v0

    #@dc
    add-int/lit8 v20, v14, -0x1

    #@de
    invoke-static/range {v19 .. v20}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@e1
    move-result v19

    #@e2
    if-eqz v19, :cond_1c

    #@e4
    .line 1279
    :cond_e4
    add-int/lit8 v14, v14, -0x1

    #@e6
    goto/16 :goto_1c

    #@e8
    .line 1289
    .restart local v4       #best:I
    .restart local v6       #bestdist:F
    .restart local v10       #here:I
    .restart local v12       #i:I
    .restart local v18       #there:I
    :cond_e8
    const/16 v17, 0x1

    #@ea
    goto/16 :goto_5e

    #@ec
    .line 1302
    .restart local v2       #adguess:I
    .restart local v9       #guess:I
    .restart local v11       #high:I
    .restart local v13       #low:I
    .restart local v17       #swap:I
    :cond_ec
    move v13, v9

    #@ed
    goto/16 :goto_6c

    #@ef
    .line 1305
    .end local v2           #adguess:I
    .end local v9           #guess:I
    :cond_ef
    add-int/lit8 v19, v10, 0x1

    #@f1
    move/from16 v0, v19

    #@f3
    if-ge v13, v0, :cond_f7

    #@f5
    .line 1306
    add-int/lit8 v13, v10, 0x1

    #@f7
    .line 1308
    :cond_f7
    move/from16 v0, v18

    #@f9
    if-ge v13, v0, :cond_136

    #@fb
    .line 1309
    move-object/from16 v0, p0

    #@fd
    invoke-direct {v0, v13}, Landroid/text/Layout;->getOffsetAtStartOf(I)I

    #@100
    move-result v13

    #@101
    .line 1311
    move-object/from16 v0, p0

    #@103
    invoke-virtual {v0, v13}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@106
    move-result v19

    #@107
    sub-float v19, v19, p2

    #@109
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    #@10c
    move-result v8

    #@10d
    .line 1313
    .local v8, dist:F
    move-object/from16 v0, p0

    #@10f
    iget-object v0, v0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@111
    move-object/from16 v19, v0

    #@113
    move-object/from16 v0, v19

    #@115
    invoke-static {v0, v13}, Landroid/text/TextUtils;->getOffsetAfter(Ljava/lang/CharSequence;I)I

    #@118
    move-result v3

    #@119
    .line 1314
    .local v3, aft:I
    move/from16 v0, v18

    #@11b
    if-ge v3, v0, :cond_14c

    #@11d
    .line 1315
    move-object/from16 v0, p0

    #@11f
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@122
    move-result v19

    #@123
    sub-float v19, v19, p2

    #@125
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    #@128
    move-result v16

    #@129
    .line 1317
    .local v16, other:F
    cmpg-float v19, v16, v8

    #@12b
    if-gez v19, :cond_130

    #@12d
    .line 1318
    move/from16 v8, v16

    #@12f
    .line 1319
    move v13, v3

    #@130
    .line 1326
    .end local v16           #other:F
    :cond_130
    cmpg-float v19, v8, v6

    #@132
    if-gez v19, :cond_136

    #@134
    .line 1327
    move v6, v8

    #@135
    .line 1328
    move v4, v13

    #@136
    .line 1332
    .end local v3           #aft:I
    .end local v8           #dist:F
    :cond_136
    move-object/from16 v0, p0

    #@138
    invoke-virtual {v0, v10}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@13b
    move-result v19

    #@13c
    sub-float v19, v19, p2

    #@13e
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    #@141
    move-result v8

    #@142
    .line 1334
    .restart local v8       #dist:F
    cmpg-float v19, v8, v6

    #@144
    if-gez v19, :cond_148

    #@146
    .line 1335
    move v6, v8

    #@147
    .line 1336
    move v4, v10

    #@148
    .line 1286
    :cond_148
    add-int/lit8 v12, v12, 0x2

    #@14a
    goto/16 :goto_2a

    #@14c
    .line 1321
    .restart local v3       #aft:I
    :cond_14c
    sget-boolean v19, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@14e
    if-eqz v19, :cond_130

    #@150
    move/from16 v0, v18

    #@152
    if-le v3, v0, :cond_130

    #@154
    if-le v3, v14, :cond_130

    #@156
    .line 1322
    move v4, v13

    #@157
    move v5, v4

    #@158
    .line 1349
    .end local v3           #aft:I
    .end local v4           #best:I
    .end local v10           #here:I
    .end local v11           #high:I
    .end local v13           #low:I
    .end local v17           #swap:I
    .end local v18           #there:I
    .local v5, best:I
    :goto_158
    return v5

    #@159
    .line 1340
    .end local v5           #best:I
    .end local v8           #dist:F
    .restart local v4       #best:I
    :cond_159
    move-object/from16 v0, p0

    #@15b
    invoke-virtual {v0, v14}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@15e
    move-result v19

    #@15f
    sub-float v19, v19, p2

    #@161
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    #@164
    move-result v8

    #@165
    .line 1344
    .restart local v8       #dist:F
    cmpg-float v19, v8, v6

    #@167
    if-gtz v19, :cond_16b

    #@169
    .line 1345
    move v6, v8

    #@16a
    .line 1346
    move v4, v14

    #@16b
    :cond_16b
    move v5, v4

    #@16c
    .line 1349
    .end local v4           #best:I
    .restart local v5       #best:I
    goto :goto_158
.end method

.method public getOffsetToLeftOf(I)I
    .registers 3
    .parameter "offset"

    #@0
    .prologue
    .line 1415
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Landroid/text/Layout;->getOffsetToLeftRightOf(IZ)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getOffsetToRightOf(I)I
    .registers 3
    .parameter "offset"

    #@0
    .prologue
    .line 1419
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/text/Layout;->getOffsetToLeftRightOf(IZ)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public final getPaint()Landroid/text/TextPaint;
    .registers 2

    #@0
    .prologue
    .line 764
    iget-object v0, p0, Landroid/text/Layout;->mPaint:Landroid/text/TextPaint;

    #@2
    return-object v0
.end method

.method public final getParagraphAlignment(I)Landroid/text/Layout$Alignment;
    .registers 9
    .parameter "line"

    #@0
    .prologue
    .line 1708
    iget-object v0, p0, Landroid/text/Layout;->mAlignment:Landroid/text/Layout$Alignment;

    #@2
    .line 1710
    .local v0, align:Landroid/text/Layout$Alignment;
    iget-boolean v4, p0, Landroid/text/Layout;->mSpannedText:Z

    #@4
    if-eqz v4, :cond_25

    #@6
    .line 1711
    iget-object v1, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@8
    check-cast v1, Landroid/text/Spanned;

    #@a
    .line 1712
    .local v1, sp:Landroid/text/Spanned;
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineStart(I)I

    #@d
    move-result v4

    #@e
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineEnd(I)I

    #@11
    move-result v5

    #@12
    const-class v6, Landroid/text/style/AlignmentSpan;

    #@14
    invoke-static {v1, v4, v5, v6}, Landroid/text/Layout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    check-cast v3, [Landroid/text/style/AlignmentSpan;

    #@1a
    .line 1716
    .local v3, spans:[Landroid/text/style/AlignmentSpan;
    array-length v2, v3

    #@1b
    .line 1717
    .local v2, spanLength:I
    if-lez v2, :cond_25

    #@1d
    .line 1718
    add-int/lit8 v4, v2, -0x1

    #@1f
    aget-object v4, v3, v4

    #@21
    invoke-interface {v4}, Landroid/text/style/AlignmentSpan;->getAlignment()Landroid/text/Layout$Alignment;

    #@24
    move-result-object v0

    #@25
    .line 1722
    .end local v1           #sp:Landroid/text/Spanned;
    .end local v2           #spanLength:I
    .end local v3           #spans:[Landroid/text/style/AlignmentSpan;
    :cond_25
    return-object v0
.end method

.method public abstract getParagraphDirection(I)I
.end method

.method public final getParagraphLeft(I)I
    .registers 5
    .parameter "line"

    #@0
    .prologue
    .line 1729
    const/4 v1, 0x0

    #@1
    .line 1730
    .local v1, left:I
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@4
    move-result v0

    #@5
    .line 1731
    .local v0, dir:I
    const/4 v2, -0x1

    #@6
    if-eq v0, v2, :cond_c

    #@8
    iget-boolean v2, p0, Landroid/text/Layout;->mSpannedText:Z

    #@a
    if-nez v2, :cond_d

    #@c
    .line 1734
    .end local v1           #left:I
    :cond_c
    :goto_c
    return v1

    #@d
    .restart local v1       #left:I
    :cond_d
    invoke-direct {p0, p1}, Landroid/text/Layout;->getParagraphLeadingMargin(I)I

    #@10
    move-result v1

    #@11
    goto :goto_c
.end method

.method public final getParagraphRight(I)I
    .registers 5
    .parameter "line"

    #@0
    .prologue
    .line 1741
    iget v1, p0, Landroid/text/Layout;->mWidth:I

    #@2
    .line 1742
    .local v1, right:I
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@5
    move-result v0

    #@6
    .line 1743
    .local v0, dir:I
    const/4 v2, 0x1

    #@7
    if-eq v0, v2, :cond_d

    #@9
    iget-boolean v2, p0, Landroid/text/Layout;->mSpannedText:Z

    #@b
    if-nez v2, :cond_e

    #@d
    .line 1746
    .end local v1           #right:I
    :cond_d
    :goto_d
    return v1

    #@e
    .restart local v1       #right:I
    :cond_e
    invoke-direct {p0, p1}, Landroid/text/Layout;->getParagraphLeadingMargin(I)I

    #@11
    move-result v2

    #@12
    sub-int/2addr v1, v2

    #@13
    goto :goto_d
.end method

.method public getPrimaryHorizontal(I)F
    .registers 4
    .parameter "offset"

    #@0
    .prologue
    .line 1026
    invoke-direct {p0, p1}, Landroid/text/Layout;->primaryIsTrailingPrevious(I)Z

    #@3
    move-result v0

    #@4
    .line 1027
    .local v0, trailing:Z
    invoke-direct {p0, p1, v0}, Landroid/text/Layout;->getHorizontal(IZ)F

    #@7
    move-result v1

    #@8
    return v1
.end method

.method public getSecondaryHorizontal(I)F
    .registers 4
    .parameter "offset"

    #@0
    .prologue
    .line 1036
    invoke-direct {p0, p1}, Landroid/text/Layout;->primaryIsTrailingPrevious(I)Z

    #@3
    move-result v0

    #@4
    .line 1037
    .local v0, trailing:Z
    if-nez v0, :cond_c

    #@6
    const/4 v1, 0x1

    #@7
    :goto_7
    invoke-direct {p0, p1, v1}, Landroid/text/Layout;->getHorizontal(IZ)F

    #@a
    move-result v1

    #@b
    return v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_7
.end method

.method public getSelectionPath(IILandroid/graphics/Path;)V
    .registers 23
    .parameter "start"
    .parameter "end"
    .parameter "dest"

    #@0
    .prologue
    .line 1652
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Path;->reset()V

    #@3
    .line 1654
    move/from16 v0, p1

    #@5
    move/from16 v1, p2

    #@7
    if-ne v0, v1, :cond_a

    #@9
    .line 1701
    :goto_9
    return-void

    #@a
    .line 1657
    :cond_a
    move/from16 v0, p2

    #@c
    move/from16 v1, p1

    #@e
    if-ge v0, v1, :cond_16

    #@10
    .line 1658
    move/from16 v17, p2

    #@12
    .line 1659
    .local v17, temp:I
    move/from16 p2, p1

    #@14
    .line 1660
    move/from16 p1, v17

    #@16
    .line 1663
    .end local v17           #temp:I
    :cond_16
    invoke-virtual/range {p0 .. p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@19
    move-result v3

    #@1a
    .line 1664
    .local v3, startline:I
    move-object/from16 v0, p0

    #@1c
    move/from16 v1, p2

    #@1e
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@21
    move-result v15

    #@22
    .line 1666
    .local v15, endline:I
    move-object/from16 v0, p0

    #@24
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineTop(I)I

    #@27
    move-result v6

    #@28
    .line 1667
    .local v6, top:I
    move-object/from16 v0, p0

    #@2a
    invoke-virtual {v0, v15}, Landroid/text/Layout;->getLineBottom(I)I

    #@2d
    move-result v7

    #@2e
    .line 1669
    .local v7, bottom:I
    if-ne v3, v15, :cond_3c

    #@30
    move-object/from16 v2, p0

    #@32
    move/from16 v4, p1

    #@34
    move/from16 v5, p2

    #@36
    move-object/from16 v8, p3

    #@38
    .line 1670
    invoke-direct/range {v2 .. v8}, Landroid/text/Layout;->addSelection(IIIIILandroid/graphics/Path;)V

    #@3b
    goto :goto_9

    #@3c
    .line 1672
    :cond_3c
    move-object/from16 v0, p0

    #@3e
    iget v2, v0, Landroid/text/Layout;->mWidth:I

    #@40
    int-to-float v0, v2

    #@41
    move/from16 v18, v0

    #@43
    .line 1674
    .local v18, width:F
    move-object/from16 v0, p0

    #@45
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineEnd(I)I

    #@48
    move-result v11

    #@49
    move-object/from16 v0, p0

    #@4b
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineBottom(I)I

    #@4e
    move-result v13

    #@4f
    move-object/from16 v8, p0

    #@51
    move v9, v3

    #@52
    move/from16 v10, p1

    #@54
    move v12, v6

    #@55
    move-object/from16 v14, p3

    #@57
    invoke-direct/range {v8 .. v14}, Landroid/text/Layout;->addSelection(IIIIILandroid/graphics/Path;)V

    #@5a
    .line 1677
    move-object/from16 v0, p0

    #@5c
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@5f
    move-result v2

    #@60
    const/4 v4, -0x1

    #@61
    if-ne v2, v4, :cond_9e

    #@63
    .line 1678
    move-object/from16 v0, p0

    #@65
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineLeft(I)F

    #@68
    move-result v9

    #@69
    int-to-float v10, v6

    #@6a
    const/4 v11, 0x0

    #@6b
    move-object/from16 v0, p0

    #@6d
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineBottom(I)I

    #@70
    move-result v2

    #@71
    int-to-float v12, v2

    #@72
    sget-object v13, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@74
    move-object/from16 v8, p3

    #@76
    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    #@79
    .line 1684
    :goto_79
    add-int/lit8 v16, v3, 0x1

    #@7b
    .local v16, i:I
    :goto_7b
    move/from16 v0, v16

    #@7d
    if-ge v0, v15, :cond_b6

    #@7f
    .line 1685
    move-object/from16 v0, p0

    #@81
    move/from16 v1, v16

    #@83
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    #@86
    move-result v6

    #@87
    .line 1686
    move-object/from16 v0, p0

    #@89
    move/from16 v1, v16

    #@8b
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineBottom(I)I

    #@8e
    move-result v7

    #@8f
    .line 1687
    const/4 v9, 0x0

    #@90
    int-to-float v10, v6

    #@91
    int-to-float v12, v7

    #@92
    sget-object v13, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@94
    move-object/from16 v8, p3

    #@96
    move/from16 v11, v18

    #@98
    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    #@9b
    .line 1684
    add-int/lit8 v16, v16, 0x1

    #@9d
    goto :goto_7b

    #@9e
    .line 1681
    .end local v16           #i:I
    :cond_9e
    move-object/from16 v0, p0

    #@a0
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineRight(I)F

    #@a3
    move-result v9

    #@a4
    int-to-float v10, v6

    #@a5
    move-object/from16 v0, p0

    #@a7
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineBottom(I)I

    #@aa
    move-result v2

    #@ab
    int-to-float v12, v2

    #@ac
    sget-object v13, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@ae
    move-object/from16 v8, p3

    #@b0
    move/from16 v11, v18

    #@b2
    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    #@b5
    goto :goto_79

    #@b6
    .line 1690
    .restart local v16       #i:I
    :cond_b6
    move-object/from16 v0, p0

    #@b8
    invoke-virtual {v0, v15}, Landroid/text/Layout;->getLineTop(I)I

    #@bb
    move-result v6

    #@bc
    .line 1691
    move-object/from16 v0, p0

    #@be
    invoke-virtual {v0, v15}, Landroid/text/Layout;->getLineBottom(I)I

    #@c1
    move-result v7

    #@c2
    .line 1693
    move-object/from16 v0, p0

    #@c4
    invoke-virtual {v0, v15}, Landroid/text/Layout;->getLineStart(I)I

    #@c7
    move-result v10

    #@c8
    move-object/from16 v8, p0

    #@ca
    move v9, v15

    #@cb
    move/from16 v11, p2

    #@cd
    move v12, v6

    #@ce
    move v13, v7

    #@cf
    move-object/from16 v14, p3

    #@d1
    invoke-direct/range {v8 .. v14}, Landroid/text/Layout;->addSelection(IIIIILandroid/graphics/Path;)V

    #@d4
    .line 1696
    move-object/from16 v0, p0

    #@d6
    invoke-virtual {v0, v15}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@d9
    move-result v2

    #@da
    const/4 v4, -0x1

    #@db
    if-ne v2, v4, :cond_f0

    #@dd
    .line 1697
    int-to-float v10, v6

    #@de
    move-object/from16 v0, p0

    #@e0
    invoke-virtual {v0, v15}, Landroid/text/Layout;->getLineRight(I)F

    #@e3
    move-result v11

    #@e4
    int-to-float v12, v7

    #@e5
    sget-object v13, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@e7
    move-object/from16 v8, p3

    #@e9
    move/from16 v9, v18

    #@eb
    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    #@ee
    goto/16 :goto_9

    #@f0
    .line 1699
    :cond_f0
    const/4 v9, 0x0

    #@f1
    int-to-float v10, v6

    #@f2
    move-object/from16 v0, p0

    #@f4
    invoke-virtual {v0, v15}, Landroid/text/Layout;->getLineLeft(I)F

    #@f7
    move-result v11

    #@f8
    int-to-float v12, v7

    #@f9
    sget-object v13, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@fb
    move-object/from16 v8, p3

    #@fd
    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    #@100
    goto/16 :goto_9
.end method

.method public final getSpacingAdd()F
    .registers 2

    #@0
    .prologue
    .line 820
    iget v0, p0, Landroid/text/Layout;->mSpacingAdd:F

    #@2
    return v0
.end method

.method public final getSpacingMultiplier()F
    .registers 2

    #@0
    .prologue
    .line 813
    iget v0, p0, Landroid/text/Layout;->mSpacingMult:F

    #@2
    return v0
.end method

.method public final getText()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 755
    iget-object v0, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public final getTextDirectionHeuristic()Landroid/text/TextDirectionHeuristic;
    .registers 2

    #@0
    .prologue
    .line 828
    iget-object v0, p0, Landroid/text/Layout;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@2
    return-object v0
.end method

.method public abstract getTopPadding()I
.end method

.method public final getWidth()I
    .registers 2

    #@0
    .prologue
    .line 771
    iget v0, p0, Landroid/text/Layout;->mWidth:I

    #@2
    return v0
.end method

.method public final increaseWidthTo(I)V
    .registers 4
    .parameter "wid"

    #@0
    .prologue
    .line 788
    iget v0, p0, Landroid/text/Layout;->mWidth:I

    #@2
    if-ge p1, v0, :cond_c

    #@4
    .line 789
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "attempted to reduce Layout width"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 792
    :cond_c
    iput p1, p0, Landroid/text/Layout;->mWidth:I

    #@e
    .line 793
    return-void
.end method

.method public isLevelBoundary(I)Z
    .registers 13
    .parameter "offset"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 918
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@5
    move-result v2

    #@6
    .line 919
    .local v2, line:I
    invoke-virtual {p0, v2}, Landroid/text/Layout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@9
    move-result-object v0

    #@a
    .line 920
    .local v0, dirs:Landroid/text/Layout$Directions;
    sget-object v10, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@c
    if-eq v0, v10, :cond_12

    #@e
    sget-object v10, Landroid/text/Layout;->DIRS_ALL_RIGHT_TO_LEFT:Landroid/text/Layout$Directions;

    #@10
    if-ne v0, v10, :cond_13

    #@12
    .line 939
    :cond_12
    :goto_12
    return v8

    #@13
    .line 924
    :cond_13
    iget-object v7, v0, Landroid/text/Layout$Directions;->mDirections:[I

    #@15
    .line 925
    .local v7, runs:[I
    invoke-virtual {p0, v2}, Landroid/text/Layout;->getLineStart(I)I

    #@18
    move-result v4

    #@19
    .line 926
    .local v4, lineStart:I
    invoke-virtual {p0, v2}, Landroid/text/Layout;->getLineEnd(I)I

    #@1c
    move-result v3

    #@1d
    .line 927
    .local v3, lineEnd:I
    if-eq p1, v4, :cond_21

    #@1f
    if-ne p1, v3, :cond_3d

    #@21
    .line 928
    :cond_21
    invoke-virtual {p0, v2}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@24
    move-result v10

    #@25
    if-ne v10, v9, :cond_37

    #@27
    move v5, v8

    #@28
    .line 929
    .local v5, paraLevel:I
    :goto_28
    if-ne p1, v4, :cond_39

    #@2a
    move v6, v8

    #@2b
    .line 930
    .local v6, runIndex:I
    :goto_2b
    add-int/lit8 v10, v6, 0x1

    #@2d
    aget v10, v7, v10

    #@2f
    ushr-int/lit8 v10, v10, 0x1a

    #@31
    and-int/lit8 v10, v10, 0x3f

    #@33
    if-eq v10, v5, :cond_12

    #@35
    move v8, v9

    #@36
    goto :goto_12

    #@37
    .end local v5           #paraLevel:I
    .end local v6           #runIndex:I
    :cond_37
    move v5, v9

    #@38
    .line 928
    goto :goto_28

    #@39
    .line 929
    .restart local v5       #paraLevel:I
    :cond_39
    array-length v10, v7

    #@3a
    add-int/lit8 v6, v10, -0x2

    #@3c
    goto :goto_2b

    #@3d
    .line 933
    .end local v5           #paraLevel:I
    :cond_3d
    sub-int/2addr p1, v4

    #@3e
    .line 934
    const/4 v1, 0x0

    #@3f
    .local v1, i:I
    :goto_3f
    array-length v10, v7

    #@40
    if-ge v1, v10, :cond_12

    #@42
    .line 935
    aget v10, v7, v1

    #@44
    if-ne p1, v10, :cond_48

    #@46
    move v8, v9

    #@47
    .line 936
    goto :goto_12

    #@48
    .line 934
    :cond_48
    add-int/lit8 v1, v1, 0x2

    #@4a
    goto :goto_3f
.end method

.method public isRtlCharAt(I)Z
    .registers 13
    .parameter "offset"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 948
    invoke-virtual {p0, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@5
    move-result v3

    #@6
    .line 949
    .local v3, line:I
    invoke-virtual {p0, v3}, Landroid/text/Layout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@9
    move-result-object v0

    #@a
    .line 950
    .local v0, dirs:Landroid/text/Layout$Directions;
    sget-object v9, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@c
    if-ne v0, v9, :cond_f

    #@e
    .line 968
    :cond_e
    :goto_e
    return v8

    #@f
    .line 953
    :cond_f
    sget-object v9, Landroid/text/Layout;->DIRS_ALL_RIGHT_TO_LEFT:Landroid/text/Layout$Directions;

    #@11
    if-ne v0, v9, :cond_15

    #@13
    move v8, v7

    #@14
    .line 954
    goto :goto_e

    #@15
    .line 956
    :cond_15
    iget-object v5, v0, Landroid/text/Layout$Directions;->mDirections:[I

    #@17
    .line 957
    .local v5, runs:[I
    invoke-virtual {p0, v3}, Landroid/text/Layout;->getLineStart(I)I

    #@1a
    move-result v4

    #@1b
    .line 958
    .local v4, lineStart:I
    const/4 v1, 0x0

    #@1c
    .local v1, i:I
    :goto_1c
    array-length v9, v5

    #@1d
    if-ge v1, v9, :cond_e

    #@1f
    .line 959
    aget v9, v5, v1

    #@21
    const v10, 0x3ffffff

    #@24
    and-int/2addr v9, v10

    #@25
    add-int v6, v4, v9

    #@27
    .line 962
    .local v6, start:I
    if-lt p1, v6, :cond_39

    #@29
    .line 963
    add-int/lit8 v9, v1, 0x1

    #@2b
    aget v9, v5, v9

    #@2d
    ushr-int/lit8 v9, v9, 0x1a

    #@2f
    and-int/lit8 v2, v9, 0x3f

    #@31
    .line 964
    .local v2, level:I
    and-int/lit8 v9, v2, 0x1

    #@33
    if-eqz v9, :cond_37

    #@35
    :goto_35
    move v8, v7

    #@36
    goto :goto_e

    #@37
    :cond_37
    move v7, v8

    #@38
    goto :goto_35

    #@39
    .line 958
    .end local v2           #level:I
    :cond_39
    add-int/lit8 v1, v1, 0x2

    #@3b
    goto :goto_1c
.end method

.method protected final isSpanned()Z
    .registers 2

    #@0
    .prologue
    .line 1958
    iget-boolean v0, p0, Landroid/text/Layout;->mSpannedText:Z

    #@2
    return v0
.end method

.method replaceWith(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V
    .registers 10
    .parameter "text"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"

    #@0
    .prologue
    .line 391
    if-gez p3, :cond_21

    #@2
    .line 392
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "Layout: "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " < 0"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 395
    :cond_21
    iput-object p1, p0, Landroid/text/Layout;->mText:Ljava/lang/CharSequence;

    #@23
    .line 396
    iput-object p2, p0, Landroid/text/Layout;->mPaint:Landroid/text/TextPaint;

    #@25
    .line 397
    iput p3, p0, Landroid/text/Layout;->mWidth:I

    #@27
    .line 398
    iput-object p4, p0, Landroid/text/Layout;->mAlignment:Landroid/text/Layout$Alignment;

    #@29
    .line 399
    iput p5, p0, Landroid/text/Layout;->mSpacingMult:F

    #@2b
    .line 400
    iput p6, p0, Landroid/text/Layout;->mSpacingAdd:F

    #@2d
    .line 401
    instance-of v0, p1, Landroid/text/Spanned;

    #@2f
    iput-boolean v0, p0, Landroid/text/Layout;->mSpannedText:Z

    #@31
    .line 402
    return-void
.end method
