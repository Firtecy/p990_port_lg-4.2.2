.class public final Landroid/text/SpannedString;
.super Landroid/text/SpannableStringInternal;
.source "SpannedString.java"

# interfaces
.implements Ljava/lang/CharSequence;
.implements Landroid/text/GetChars;
.implements Landroid/text/Spanned;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 30
    const/4 v0, 0x0

    #@1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v1

    #@5
    invoke-direct {p0, p1, v0, v1}, Landroid/text/SpannableStringInternal;-><init>(Ljava/lang/CharSequence;II)V

    #@8
    .line 31
    return-void
.end method

.method private constructor <init>(Ljava/lang/CharSequence;II)V
    .registers 4
    .parameter "source"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/text/SpannableStringInternal;-><init>(Ljava/lang/CharSequence;II)V

    #@3
    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannedString;
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 42
    instance-of v0, p0, Landroid/text/SpannedString;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 43
    check-cast p0, Landroid/text/SpannedString;

    #@6
    .line 45
    .end local p0
    :goto_6
    return-object p0

    #@7
    .restart local p0
    :cond_7
    new-instance v0, Landroid/text/SpannedString;

    #@9
    invoke-direct {v0, p0}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    #@c
    move-object p0, v0

    #@d
    goto :goto_6
.end method


# virtual methods
.method public bridge synthetic getSpanEnd(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/text/SpannableStringInternal;->getSpanEnd(Ljava/lang/Object;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public bridge synthetic getSpanFlags(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/text/SpannableStringInternal;->getSpanFlags(Ljava/lang/Object;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public bridge synthetic getSpanStart(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/text/SpannableStringInternal;->getSpanStart(Ljava/lang/Object;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public bridge synthetic getSpans(IILjava/lang/Class;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3}, Landroid/text/SpannableStringInternal;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic nextSpanTransition(IILjava/lang/Class;)I
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3}, Landroid/text/SpannableStringInternal;->nextSpanTransition(IILjava/lang/Class;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 38
    new-instance v0, Landroid/text/SpannedString;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;II)V

    #@5
    return-object v0
.end method
