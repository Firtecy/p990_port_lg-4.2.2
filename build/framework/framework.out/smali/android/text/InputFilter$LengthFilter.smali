.class public Landroid/text/InputFilter$LengthFilter;
.super Ljava/lang/Object;
.source "InputFilter.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LengthFilter"
.end annotation


# instance fields
.field private mMax:I


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .parameter "max"

    #@0
    .prologue
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 79
    iput p1, p0, Landroid/text/InputFilter$LengthFilter;->mMax:I

    #@5
    .line 80
    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .registers 12
    .parameter "source"
    .parameter "start"
    .parameter "end"
    .parameter "dest"
    .parameter "dstart"
    .parameter "dend"

    #@0
    .prologue
    .line 84
    iget v2, p0, Landroid/text/InputFilter$LengthFilter;->mMax:I

    #@2
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    #@5
    move-result v3

    #@6
    sub-int v4, p6, p5

    #@8
    sub-int/2addr v3, v4

    #@9
    sub-int v1, v2, v3

    #@b
    .line 86
    .local v1, keep:I
    if-gtz v1, :cond_10

    #@d
    .line 87
    const-string v2, ""

    #@f
    .line 110
    :goto_f
    return-object v2

    #@10
    .line 88
    :cond_10
    sub-int v2, p3, p2

    #@12
    if-lt v1, v2, :cond_16

    #@14
    .line 89
    const/4 v2, 0x0

    #@15
    goto :goto_f

    #@16
    .line 91
    :cond_16
    add-int/2addr v1, p2

    #@17
    .line 92
    add-int/lit8 v2, v1, -0x1

    #@19
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@1c
    move-result v2

    #@1d
    invoke-static {v2}, Ljava/lang/Character;->isHighSurrogate(C)Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_42

    #@23
    .line 93
    add-int/lit8 v1, v1, -0x1

    #@25
    .line 95
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@27
    if-eqz v2, :cond_3d

    #@29
    add-int/lit8 v2, v1, -0x2

    #@2b
    if-lt v2, p2, :cond_3d

    #@2d
    .line 96
    add-int/lit8 v2, v1, -0x2

    #@2f
    invoke-static {p1, v2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    #@32
    move-result v0

    #@33
    .line 97
    .local v0, codePoint:I
    add-int/lit8 v2, v1, -0x2

    #@35
    invoke-static {p1, v2}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_3d

    #@3b
    .line 98
    add-int/lit8 v1, v1, -0x2

    #@3d
    .line 101
    .end local v0           #codePoint:I
    :cond_3d
    if-ne v1, p2, :cond_59

    #@3f
    .line 102
    const-string v2, ""

    #@41
    goto :goto_f

    #@42
    .line 104
    :cond_42
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@44
    if-eqz v2, :cond_59

    #@46
    add-int/lit8 v2, v1, -0x1

    #@48
    if-lt v2, p2, :cond_59

    #@4a
    add-int/lit8 v2, v1, -0x1

    #@4c
    invoke-static {p1, v2}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@4f
    move-result v2

    #@50
    if-eqz v2, :cond_59

    #@52
    .line 105
    add-int/lit8 v1, v1, -0x1

    #@54
    .line 106
    if-ne v1, p2, :cond_59

    #@56
    .line 107
    const-string v2, ""

    #@58
    goto :goto_f

    #@59
    .line 110
    :cond_59
    invoke-interface {p1, p2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@5c
    move-result-object v2

    #@5d
    goto :goto_f
.end method
