.class public Landroid/text/Html;
.super Ljava/lang/Object;
.source "Html.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/Html$HtmlParser;,
        Landroid/text/Html$TagHandler;,
        Landroid/text/Html$ImageGetter;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 90
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 4
    .parameter "text"

    #@0
    .prologue
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 155
    .local v0, out:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@6
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@9
    move-result v2

    #@a
    invoke-static {v0, p0, v1, v2}, Landroid/text/Html;->withinStyle(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;II)V

    #@d
    .line 156
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    return-object v1
.end method

.method public static fromHtml(Ljava/lang/String;)Landroid/text/Spanned;
    .registers 2
    .parameter "source"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 101
    invoke-static {p0, v0, v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;
    .registers 8
    .parameter "source"
    .parameter "imageGetter"
    .parameter "tagHandler"

    #@0
    .prologue
    .line 124
    new-instance v2, Lorg/ccil/cowan/tagsoup/Parser;

    #@2
    invoke-direct {v2}, Lorg/ccil/cowan/tagsoup/Parser;-><init>()V

    #@5
    .line 126
    .local v2, parser:Lorg/ccil/cowan/tagsoup/Parser;
    :try_start_5
    const-string v3, "http://www.ccil.org/~cowan/tagsoup/properties/schema"

    #@7
    invoke-static {}, Landroid/text/Html$HtmlParser;->access$000()Lorg/ccil/cowan/tagsoup/HTMLSchema;

    #@a
    move-result-object v4

    #@b
    invoke-virtual {v2, v3, v4}, Lorg/ccil/cowan/tagsoup/Parser;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_e
    .catch Lorg/xml/sax/SAXNotRecognizedException; {:try_start_5 .. :try_end_e} :catch_18
    .catch Lorg/xml/sax/SAXNotSupportedException; {:try_start_5 .. :try_end_e} :catch_1f

    #@e
    .line 135
    new-instance v0, Landroid/text/HtmlToSpannedConverter;

    #@10
    invoke-direct {v0, p0, p1, p2, v2}, Landroid/text/HtmlToSpannedConverter;-><init>(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;Lorg/ccil/cowan/tagsoup/Parser;)V

    #@13
    .line 138
    .local v0, converter:Landroid/text/HtmlToSpannedConverter;
    invoke-virtual {v0}, Landroid/text/HtmlToSpannedConverter;->convert()Landroid/text/Spanned;

    #@16
    move-result-object v3

    #@17
    return-object v3

    #@18
    .line 127
    .end local v0           #converter:Landroid/text/HtmlToSpannedConverter;
    :catch_18
    move-exception v1

    #@19
    .line 129
    .local v1, e:Lorg/xml/sax/SAXNotRecognizedException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@1b
    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@1e
    throw v3

    #@1f
    .line 130
    .end local v1           #e:Lorg/xml/sax/SAXNotRecognizedException;
    :catch_1f
    move-exception v1

    #@20
    .line 132
    .local v1, e:Lorg/xml/sax/SAXNotSupportedException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@22
    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@25
    throw v3
.end method

.method private static getOpenParaTagWithDirection(Landroid/text/Spanned;II)Ljava/lang/String;
    .registers 9
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 215
    sub-int v1, p2, p1

    #@3
    .line 216
    .local v1, len:I
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->idealByteArraySize(I)I

    #@6
    move-result v4

    #@7
    new-array v2, v4, [B

    #@9
    .line 217
    .local v2, levels:[B
    invoke-static {v1}, Landroid/text/TextUtils;->obtain(I)[C

    #@c
    move-result-object v0

    #@d
    .line 218
    .local v0, buffer:[C
    invoke-static {p0, p1, p2, v0, v5}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@10
    .line 220
    const/4 v4, 0x2

    #@11
    invoke-static {v4, v0, v2, v1, v5}, Landroid/text/AndroidBidi;->bidi(I[C[BIZ)I

    #@14
    move-result v3

    #@15
    .line 222
    .local v3, paraDir:I
    packed-switch v3, :pswitch_data_1e

    #@18
    .line 227
    const-string v4, "<p dir=\"ltr\">"

    #@1a
    :goto_1a
    return-object v4

    #@1b
    .line 224
    :pswitch_1b
    const-string v4, "<p dir=\"rtl\">"

    #@1d
    goto :goto_1a

    #@1e
    .line 222
    :pswitch_data_1e
    .packed-switch -0x1
        :pswitch_1b
    .end packed-switch
.end method

.method public static toHtml(Landroid/text/Spanned;)Ljava/lang/String;
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 146
    .local v0, out:Ljava/lang/StringBuilder;
    invoke-static {v0, p0}, Landroid/text/Html;->withinHtml(Ljava/lang/StringBuilder;Landroid/text/Spanned;)V

    #@8
    .line 147
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    return-object v1
.end method

.method private static withinBlockquote(Ljava/lang/StringBuilder;Landroid/text/Spanned;II)V
    .registers 12
    .parameter "out"
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/16 v7, 0xa

    #@2
    .line 233
    invoke-static {p1, p2, p3}, Landroid/text/Html;->getOpenParaTagWithDirection(Landroid/text/Spanned;II)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9
    .line 236
    move v2, p2

    #@a
    .local v2, i:I
    :goto_a
    if-ge v2, p3, :cond_2f

    #@c
    .line 237
    invoke-static {p1, v7, v2, p3}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CII)I

    #@f
    move-result v6

    #@10
    .line 238
    .local v6, next:I
    if-gez v6, :cond_13

    #@12
    .line 239
    move v6, p3

    #@13
    .line 242
    :cond_13
    const/4 v4, 0x0

    #@14
    .line 244
    .local v4, nl:I
    :goto_14
    if-ge v6, p3, :cond_21

    #@16
    invoke-interface {p1, v6}, Landroid/text/Spanned;->charAt(I)C

    #@19
    move-result v0

    #@1a
    if-ne v0, v7, :cond_21

    #@1c
    .line 245
    add-int/lit8 v4, v4, 0x1

    #@1e
    .line 246
    add-int/lit8 v6, v6, 0x1

    #@20
    goto :goto_14

    #@21
    .line 249
    :cond_21
    sub-int v3, v6, v4

    #@23
    if-ne v6, p3, :cond_2d

    #@25
    const/4 v5, 0x1

    #@26
    :goto_26
    move-object v0, p0

    #@27
    move-object v1, p1

    #@28
    invoke-static/range {v0 .. v5}, Landroid/text/Html;->withinParagraph(Ljava/lang/StringBuilder;Landroid/text/Spanned;IIIZ)V

    #@2b
    .line 236
    move v2, v6

    #@2c
    goto :goto_a

    #@2d
    .line 249
    :cond_2d
    const/4 v5, 0x0

    #@2e
    goto :goto_26

    #@2f
    .line 252
    .end local v4           #nl:I
    .end local v6           #next:I
    :cond_2f
    const-string v0, "</p>\n"

    #@31
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 253
    return-void
.end method

.method private static withinDiv(Ljava/lang/StringBuilder;Landroid/text/Spanned;II)V
    .registers 12
    .parameter "out"
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 198
    move v1, p2

    #@1
    .local v1, i:I
    :goto_1
    if-ge v1, p3, :cond_34

    #@3
    .line 199
    const-class v7, Landroid/text/style/QuoteSpan;

    #@5
    invoke-interface {p1, v1, p3, v7}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    #@8
    move-result v4

    #@9
    .line 200
    .local v4, next:I
    const-class v7, Landroid/text/style/QuoteSpan;

    #@b
    invoke-interface {p1, v1, v4, v7}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@e
    move-result-object v6

    #@f
    check-cast v6, [Landroid/text/style/QuoteSpan;

    #@11
    .line 202
    .local v6, quotes:[Landroid/text/style/QuoteSpan;
    move-object v0, v6

    #@12
    .local v0, arr$:[Landroid/text/style/QuoteSpan;
    array-length v3, v0

    #@13
    .local v3, len$:I
    const/4 v2, 0x0

    #@14
    .local v2, i$:I
    :goto_14
    if-ge v2, v3, :cond_20

    #@16
    aget-object v5, v0, v2

    #@18
    .line 203
    .local v5, quote:Landroid/text/style/QuoteSpan;
    const-string v7, "<blockquote>"

    #@1a
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 202
    add-int/lit8 v2, v2, 0x1

    #@1f
    goto :goto_14

    #@20
    .line 206
    .end local v5           #quote:Landroid/text/style/QuoteSpan;
    :cond_20
    invoke-static {p0, p1, v1, v4}, Landroid/text/Html;->withinBlockquote(Ljava/lang/StringBuilder;Landroid/text/Spanned;II)V

    #@23
    .line 208
    move-object v0, v6

    #@24
    array-length v3, v0

    #@25
    const/4 v2, 0x0

    #@26
    :goto_26
    if-ge v2, v3, :cond_32

    #@28
    aget-object v5, v0, v2

    #@2a
    .line 209
    .restart local v5       #quote:Landroid/text/style/QuoteSpan;
    const-string v7, "</blockquote>\n"

    #@2c
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    .line 208
    add-int/lit8 v2, v2, 0x1

    #@31
    goto :goto_26

    #@32
    .line 198
    .end local v5           #quote:Landroid/text/style/QuoteSpan;
    :cond_32
    move v1, v4

    #@33
    goto :goto_1

    #@34
    .line 212
    .end local v0           #arr$:[Landroid/text/style/QuoteSpan;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v4           #next:I
    .end local v6           #quotes:[Landroid/text/style/QuoteSpan;
    :cond_34
    return-void
.end method

.method private static withinHtml(Ljava/lang/StringBuilder;Landroid/text/Spanned;)V
    .registers 12
    .parameter "out"
    .parameter "text"

    #@0
    .prologue
    .line 160
    invoke-interface {p1}, Landroid/text/Spanned;->length()I

    #@3
    move-result v4

    #@4
    .line 163
    .local v4, len:I
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    invoke-interface {p1}, Landroid/text/Spanned;->length()I

    #@8
    move-result v8

    #@9
    if-ge v2, v8, :cond_a0

    #@b
    .line 164
    const-class v8, Landroid/text/style/ParagraphStyle;

    #@d
    invoke-interface {p1, v2, v4, v8}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    #@10
    move-result v6

    #@11
    .line 165
    .local v6, next:I
    const-class v8, Landroid/text/style/ParagraphStyle;

    #@13
    invoke-interface {p1, v2, v6, v8}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@16
    move-result-object v7

    #@17
    check-cast v7, [Landroid/text/style/ParagraphStyle;

    #@19
    .line 166
    .local v7, style:[Landroid/text/style/ParagraphStyle;
    const-string v1, " "

    #@1b
    .line 167
    .local v1, elements:Ljava/lang/String;
    const/4 v5, 0x0

    #@1c
    .line 169
    .local v5, needDiv:Z
    const/4 v3, 0x0

    #@1d
    .local v3, j:I
    :goto_1d
    array-length v8, v7

    #@1e
    if-ge v3, v8, :cond_75

    #@20
    .line 170
    aget-object v8, v7, v3

    #@22
    instance-of v8, v8, Landroid/text/style/AlignmentSpan;

    #@24
    if-eqz v8, :cond_46

    #@26
    .line 171
    aget-object v8, v7, v3

    #@28
    check-cast v8, Landroid/text/style/AlignmentSpan;

    #@2a
    invoke-interface {v8}, Landroid/text/style/AlignmentSpan;->getAlignment()Landroid/text/Layout$Alignment;

    #@2d
    move-result-object v0

    #@2e
    .line 173
    .local v0, align:Landroid/text/Layout$Alignment;
    const/4 v5, 0x1

    #@2f
    .line 174
    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    #@31
    if-ne v0, v8, :cond_49

    #@33
    .line 175
    new-instance v8, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v9, "align=\"center\" "

    #@3a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v8

    #@3e
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v8

    #@42
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    .line 169
    .end local v0           #align:Landroid/text/Layout$Alignment;
    :cond_46
    :goto_46
    add-int/lit8 v3, v3, 0x1

    #@48
    goto :goto_1d

    #@49
    .line 176
    .restart local v0       #align:Landroid/text/Layout$Alignment;
    :cond_49
    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@4b
    if-ne v0, v8, :cond_61

    #@4d
    .line 177
    new-instance v8, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v9, "align=\"right\" "

    #@54
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v8

    #@58
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v8

    #@5c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v1

    #@60
    goto :goto_46

    #@61
    .line 179
    :cond_61
    new-instance v8, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v9, "align=\"left\" "

    #@68
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v8

    #@6c
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v8

    #@70
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    goto :goto_46

    #@75
    .line 183
    .end local v0           #align:Landroid/text/Layout$Alignment;
    :cond_75
    if-eqz v5, :cond_93

    #@77
    .line 184
    new-instance v8, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v9, "<div "

    #@7e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v8

    #@82
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v8

    #@86
    const-string v9, ">"

    #@88
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v8

    #@8c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v8

    #@90
    invoke-virtual {p0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    .line 187
    :cond_93
    invoke-static {p0, p1, v2, v6}, Landroid/text/Html;->withinDiv(Ljava/lang/StringBuilder;Landroid/text/Spanned;II)V

    #@96
    .line 189
    if-eqz v5, :cond_9d

    #@98
    .line 190
    const-string v8, "</div>"

    #@9a
    invoke-virtual {p0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    .line 163
    :cond_9d
    move v2, v6

    #@9e
    goto/16 :goto_5

    #@a0
    .line 193
    .end local v1           #elements:Ljava/lang/String;
    .end local v3           #j:I
    .end local v5           #needDiv:Z
    .end local v6           #next:I
    .end local v7           #style:[Landroid/text/style/ParagraphStyle;
    :cond_a0
    return-void
.end method

.method private static withinParagraph(Ljava/lang/StringBuilder;Landroid/text/Spanned;IIIZ)V
    .registers 15
    .parameter "out"
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "nl"
    .parameter "last"

    #@0
    .prologue
    .line 259
    move v1, p2

    #@1
    .local v1, i:I
    :goto_1
    if-ge v1, p3, :cond_1aa

    #@3
    .line 260
    const-class v7, Landroid/text/style/CharacterStyle;

    #@5
    invoke-interface {p1, v1, p3, v7}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    #@8
    move-result v3

    #@9
    .line 261
    .local v3, next:I
    const-class v7, Landroid/text/style/CharacterStyle;

    #@b
    invoke-interface {p1, v1, v3, v7}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@e
    move-result-object v6

    #@f
    check-cast v6, [Landroid/text/style/CharacterStyle;

    #@11
    .line 264
    .local v6, style:[Landroid/text/style/CharacterStyle;
    const/4 v2, 0x0

    #@12
    .local v2, j:I
    :goto_12
    array-length v7, v6

    #@13
    if-ge v2, v7, :cond_112

    #@15
    .line 265
    aget-object v7, v6, v2

    #@17
    instance-of v7, v7, Landroid/text/style/StyleSpan;

    #@19
    if-eqz v7, :cond_35

    #@1b
    .line 266
    aget-object v7, v6, v2

    #@1d
    check-cast v7, Landroid/text/style/StyleSpan;

    #@1f
    invoke-virtual {v7}, Landroid/text/style/StyleSpan;->getStyle()I

    #@22
    move-result v5

    #@23
    .line 268
    .local v5, s:I
    and-int/lit8 v7, v5, 0x1

    #@25
    if-eqz v7, :cond_2c

    #@27
    .line 269
    const-string v7, "<b>"

    #@29
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    .line 271
    :cond_2c
    and-int/lit8 v7, v5, 0x2

    #@2e
    if-eqz v7, :cond_35

    #@30
    .line 272
    const-string v7, "<i>"

    #@32
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    .line 275
    .end local v5           #s:I
    :cond_35
    aget-object v7, v6, v2

    #@37
    instance-of v7, v7, Landroid/text/style/TypefaceSpan;

    #@39
    if-eqz v7, :cond_51

    #@3b
    .line 276
    aget-object v7, v6, v2

    #@3d
    check-cast v7, Landroid/text/style/TypefaceSpan;

    #@3f
    invoke-virtual {v7}, Landroid/text/style/TypefaceSpan;->getFamily()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    .line 278
    .local v5, s:Ljava/lang/String;
    const-string/jumbo v7, "monospace"

    #@46
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v7

    #@4a
    if-eqz v7, :cond_51

    #@4c
    .line 279
    const-string v7, "<tt>"

    #@4e
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    .line 282
    .end local v5           #s:Ljava/lang/String;
    :cond_51
    aget-object v7, v6, v2

    #@53
    instance-of v7, v7, Landroid/text/style/SuperscriptSpan;

    #@55
    if-eqz v7, :cond_5c

    #@57
    .line 283
    const-string v7, "<sup>"

    #@59
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    .line 285
    :cond_5c
    aget-object v7, v6, v2

    #@5e
    instance-of v7, v7, Landroid/text/style/SubscriptSpan;

    #@60
    if-eqz v7, :cond_67

    #@62
    .line 286
    const-string v7, "<sub>"

    #@64
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    .line 288
    :cond_67
    aget-object v7, v6, v2

    #@69
    instance-of v7, v7, Landroid/text/style/UnderlineSpan;

    #@6b
    if-eqz v7, :cond_72

    #@6d
    .line 289
    const-string v7, "<u>"

    #@6f
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    .line 291
    :cond_72
    aget-object v7, v6, v2

    #@74
    instance-of v7, v7, Landroid/text/style/StrikethroughSpan;

    #@76
    if-eqz v7, :cond_7d

    #@78
    .line 292
    const-string v7, "<strike>"

    #@7a
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    .line 294
    :cond_7d
    aget-object v7, v6, v2

    #@7f
    instance-of v7, v7, Landroid/text/style/URLSpan;

    #@81
    if-eqz v7, :cond_98

    #@83
    .line 295
    const-string v7, "<a href=\""

    #@85
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    .line 296
    aget-object v7, v6, v2

    #@8a
    check-cast v7, Landroid/text/style/URLSpan;

    #@8c
    invoke-virtual {v7}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    #@8f
    move-result-object v7

    #@90
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    .line 297
    const-string v7, "\">"

    #@95
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    .line 299
    :cond_98
    aget-object v7, v6, v2

    #@9a
    instance-of v7, v7, Landroid/text/style/ImageSpan;

    #@9c
    if-eqz v7, :cond_b4

    #@9e
    .line 300
    const-string v7, "<img src=\""

    #@a0
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    .line 301
    aget-object v7, v6, v2

    #@a5
    check-cast v7, Landroid/text/style/ImageSpan;

    #@a7
    invoke-virtual {v7}, Landroid/text/style/ImageSpan;->getSource()Ljava/lang/String;

    #@aa
    move-result-object v7

    #@ab
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    .line 302
    const-string v7, "\">"

    #@b0
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    .line 305
    move v1, v3

    #@b4
    .line 307
    :cond_b4
    aget-object v7, v6, v2

    #@b6
    instance-of v7, v7, Landroid/text/style/AbsoluteSizeSpan;

    #@b8
    if-eqz v7, :cond_d1

    #@ba
    .line 308
    const-string v7, "<font size =\""

    #@bc
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    .line 309
    aget-object v7, v6, v2

    #@c1
    check-cast v7, Landroid/text/style/AbsoluteSizeSpan;

    #@c3
    invoke-virtual {v7}, Landroid/text/style/AbsoluteSizeSpan;->getSize()I

    #@c6
    move-result v7

    #@c7
    div-int/lit8 v7, v7, 0x6

    #@c9
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cc
    .line 310
    const-string v7, "\">"

    #@ce
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    .line 312
    :cond_d1
    aget-object v7, v6, v2

    #@d3
    instance-of v7, v7, Landroid/text/style/ForegroundColorSpan;

    #@d5
    if-eqz v7, :cond_10e

    #@d7
    .line 313
    const-string v7, "<font color =\"#"

    #@d9
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    .line 314
    aget-object v7, v6, v2

    #@de
    check-cast v7, Landroid/text/style/ForegroundColorSpan;

    #@e0
    invoke-virtual {v7}, Landroid/text/style/ForegroundColorSpan;->getForegroundColor()I

    #@e3
    move-result v7

    #@e4
    const/high16 v8, 0x100

    #@e6
    add-int/2addr v7, v8

    #@e7
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@ea
    move-result-object v0

    #@eb
    .line 316
    .local v0, color:Ljava/lang/String;
    :goto_eb
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@ee
    move-result v7

    #@ef
    const/4 v8, 0x6

    #@f0
    if-ge v7, v8, :cond_106

    #@f2
    .line 317
    new-instance v7, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v8, "0"

    #@f9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v7

    #@fd
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v7

    #@101
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v0

    #@105
    goto :goto_eb

    #@106
    .line 319
    :cond_106
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    .line 320
    const-string v7, "\">"

    #@10b
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    .line 264
    .end local v0           #color:Ljava/lang/String;
    :cond_10e
    add-int/lit8 v2, v2, 0x1

    #@110
    goto/16 :goto_12

    #@112
    .line 324
    :cond_112
    invoke-static {p0, p1, v1, v3}, Landroid/text/Html;->withinStyle(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;II)V

    #@115
    .line 326
    array-length v7, v6

    #@116
    add-int/lit8 v2, v7, -0x1

    #@118
    :goto_118
    if-ltz v2, :cond_1a7

    #@11a
    .line 327
    aget-object v7, v6, v2

    #@11c
    instance-of v7, v7, Landroid/text/style/ForegroundColorSpan;

    #@11e
    if-eqz v7, :cond_125

    #@120
    .line 328
    const-string v7, "</font>"

    #@122
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    .line 330
    :cond_125
    aget-object v7, v6, v2

    #@127
    instance-of v7, v7, Landroid/text/style/AbsoluteSizeSpan;

    #@129
    if-eqz v7, :cond_130

    #@12b
    .line 331
    const-string v7, "</font>"

    #@12d
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    .line 333
    :cond_130
    aget-object v7, v6, v2

    #@132
    instance-of v7, v7, Landroid/text/style/URLSpan;

    #@134
    if-eqz v7, :cond_13b

    #@136
    .line 334
    const-string v7, "</a>"

    #@138
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    .line 336
    :cond_13b
    aget-object v7, v6, v2

    #@13d
    instance-of v7, v7, Landroid/text/style/StrikethroughSpan;

    #@13f
    if-eqz v7, :cond_146

    #@141
    .line 337
    const-string v7, "</strike>"

    #@143
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    .line 339
    :cond_146
    aget-object v7, v6, v2

    #@148
    instance-of v7, v7, Landroid/text/style/UnderlineSpan;

    #@14a
    if-eqz v7, :cond_151

    #@14c
    .line 340
    const-string v7, "</u>"

    #@14e
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    .line 342
    :cond_151
    aget-object v7, v6, v2

    #@153
    instance-of v7, v7, Landroid/text/style/SubscriptSpan;

    #@155
    if-eqz v7, :cond_15c

    #@157
    .line 343
    const-string v7, "</sub>"

    #@159
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    .line 345
    :cond_15c
    aget-object v7, v6, v2

    #@15e
    instance-of v7, v7, Landroid/text/style/SuperscriptSpan;

    #@160
    if-eqz v7, :cond_167

    #@162
    .line 346
    const-string v7, "</sup>"

    #@164
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    .line 348
    :cond_167
    aget-object v7, v6, v2

    #@169
    instance-of v7, v7, Landroid/text/style/TypefaceSpan;

    #@16b
    if-eqz v7, :cond_183

    #@16d
    .line 349
    aget-object v7, v6, v2

    #@16f
    check-cast v7, Landroid/text/style/TypefaceSpan;

    #@171
    invoke-virtual {v7}, Landroid/text/style/TypefaceSpan;->getFamily()Ljava/lang/String;

    #@174
    move-result-object v5

    #@175
    .line 351
    .restart local v5       #s:Ljava/lang/String;
    const-string/jumbo v7, "monospace"

    #@178
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17b
    move-result v7

    #@17c
    if-eqz v7, :cond_183

    #@17e
    .line 352
    const-string v7, "</tt>"

    #@180
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@183
    .line 355
    .end local v5           #s:Ljava/lang/String;
    :cond_183
    aget-object v7, v6, v2

    #@185
    instance-of v7, v7, Landroid/text/style/StyleSpan;

    #@187
    if-eqz v7, :cond_1a3

    #@189
    .line 356
    aget-object v7, v6, v2

    #@18b
    check-cast v7, Landroid/text/style/StyleSpan;

    #@18d
    invoke-virtual {v7}, Landroid/text/style/StyleSpan;->getStyle()I

    #@190
    move-result v5

    #@191
    .line 358
    .local v5, s:I
    and-int/lit8 v7, v5, 0x1

    #@193
    if-eqz v7, :cond_19a

    #@195
    .line 359
    const-string v7, "</b>"

    #@197
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    .line 361
    :cond_19a
    and-int/lit8 v7, v5, 0x2

    #@19c
    if-eqz v7, :cond_1a3

    #@19e
    .line 362
    const-string v7, "</i>"

    #@1a0
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    .line 326
    .end local v5           #s:I
    :cond_1a3
    add-int/lit8 v2, v2, -0x1

    #@1a5
    goto/16 :goto_118

    #@1a7
    .line 259
    :cond_1a7
    move v1, v3

    #@1a8
    goto/16 :goto_1

    #@1aa
    .line 368
    .end local v2           #j:I
    .end local v3           #next:I
    .end local v6           #style:[Landroid/text/style/CharacterStyle;
    :cond_1aa
    if-eqz p5, :cond_1b7

    #@1ac
    const-string v4, ""

    #@1ae
    .line 370
    .local v4, p:Ljava/lang/String;
    :goto_1ae
    const/4 v7, 0x1

    #@1af
    if-ne p4, v7, :cond_1cf

    #@1b1
    .line 371
    const-string v7, "<br>\n"

    #@1b3
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b6
    .line 380
    :goto_1b6
    return-void

    #@1b7
    .line 368
    .end local v4           #p:Ljava/lang/String;
    :cond_1b7
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1bc
    const-string v8, "</p>\n"

    #@1be
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v7

    #@1c2
    invoke-static {p1, p2, p3}, Landroid/text/Html;->getOpenParaTagWithDirection(Landroid/text/Spanned;II)Ljava/lang/String;

    #@1c5
    move-result-object v8

    #@1c6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v7

    #@1ca
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cd
    move-result-object v4

    #@1ce
    goto :goto_1ae

    #@1cf
    .line 372
    .restart local v4       #p:Ljava/lang/String;
    :cond_1cf
    const/4 v7, 0x2

    #@1d0
    if-ne p4, v7, :cond_1d6

    #@1d2
    .line 373
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    goto :goto_1b6

    #@1d6
    .line 375
    :cond_1d6
    const/4 v1, 0x2

    #@1d7
    :goto_1d7
    if-ge v1, p4, :cond_1e1

    #@1d9
    .line 376
    const-string v7, "<br>"

    #@1db
    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1de
    .line 375
    add-int/lit8 v1, v1, 0x1

    #@1e0
    goto :goto_1d7

    #@1e1
    .line 378
    :cond_1e1
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e4
    goto :goto_1b6
.end method

.method private static withinStyle(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;II)V
    .registers 10
    .parameter "out"
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/16 v5, 0x20

    #@2
    .line 384
    move v2, p2

    #@3
    .local v2, i:I
    :goto_3
    if-ge v2, p3, :cond_b3

    #@5
    .line 385
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@8
    move-result v0

    #@9
    .line 387
    .local v0, c:C
    const/16 v3, 0x3c

    #@b
    if-ne v0, v3, :cond_15

    #@d
    .line 388
    const-string v3, "&lt;"

    #@f
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    .line 384
    :cond_12
    :goto_12
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_3

    #@15
    .line 389
    :cond_15
    const/16 v3, 0x3e

    #@17
    if-ne v0, v3, :cond_1f

    #@19
    .line 390
    const-string v3, "&gt;"

    #@1b
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    goto :goto_12

    #@1f
    .line 391
    :cond_1f
    const/16 v3, 0x26

    #@21
    if-ne v0, v3, :cond_29

    #@23
    .line 392
    const-string v3, "&amp;"

    #@25
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    goto :goto_12

    #@29
    .line 394
    :cond_29
    const v3, 0xd800

    #@2c
    if-lt v0, v3, :cond_70

    #@2e
    const v3, 0xdbff

    #@31
    if-gt v0, v3, :cond_70

    #@33
    .line 395
    invoke-static {p1, v2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    #@36
    move-result v1

    #@37
    .line 396
    .local v1, codept:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v4, "&#"

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    const-string v4, ";"

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    .line 397
    add-int/lit8 v3, v2, 0x1

    #@55
    if-ge v3, p3, :cond_12

    #@57
    add-int/lit8 v3, v2, 0x1

    #@59
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    #@5c
    move-result v3

    #@5d
    const v4, 0xdc00

    #@60
    if-lt v3, v4, :cond_12

    #@62
    add-int/lit8 v3, v2, 0x1

    #@64
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    #@67
    move-result v3

    #@68
    const v4, 0xdfff

    #@6b
    if-gt v3, v4, :cond_12

    #@6d
    .line 398
    add-int/lit8 v2, v2, 0x1

    #@6f
    goto :goto_12

    #@70
    .line 401
    .end local v1           #codept:I
    :cond_70
    const/16 v3, 0x7e

    #@72
    if-gt v0, v3, :cond_76

    #@74
    if-ge v0, v5, :cond_93

    #@76
    .line 402
    :cond_76
    new-instance v3, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v4, "&#"

    #@7d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@84
    move-result-object v3

    #@85
    const-string v4, ";"

    #@87
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v3

    #@8b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v3

    #@8f
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    goto :goto_12

    #@93
    .line 403
    :cond_93
    if-ne v0, v5, :cond_ae

    #@95
    .line 404
    :goto_95
    add-int/lit8 v3, v2, 0x1

    #@97
    if-ge v3, p3, :cond_a9

    #@99
    add-int/lit8 v3, v2, 0x1

    #@9b
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    #@9e
    move-result v3

    #@9f
    if-ne v3, v5, :cond_a9

    #@a1
    .line 405
    const-string v3, "&nbsp;"

    #@a3
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    .line 406
    add-int/lit8 v2, v2, 0x1

    #@a8
    goto :goto_95

    #@a9
    .line 409
    :cond_a9
    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@ac
    goto/16 :goto_12

    #@ae
    .line 411
    :cond_ae
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b1
    goto/16 :goto_12

    #@b3
    .line 414
    .end local v0           #c:C
    :cond_b3
    return-void
.end method
