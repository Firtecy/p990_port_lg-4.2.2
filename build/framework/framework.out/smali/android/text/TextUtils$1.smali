.class final Landroid/text/TextUtils$1;
.super Ljava/lang/Object;
.source "TextUtils.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/TextUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 653
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Ljava/lang/CharSequence;
    .registers 8
    .parameter "p"

    #@0
    .prologue
    .line 659
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 661
    .local v0, kind:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    .line 662
    .local v2, string:Ljava/lang/String;
    if-nez v2, :cond_c

    #@a
    .line 663
    const/4 v2, 0x0

    #@b
    .line 776
    .end local v2           #string:Ljava/lang/String;
    :cond_b
    :goto_b
    return-object v2

    #@c
    .line 666
    .restart local v2       #string:Ljava/lang/String;
    :cond_c
    const/4 v3, 0x1

    #@d
    if-eq v0, v3, :cond_b

    #@f
    .line 670
    new-instance v1, Landroid/text/SpannableString;

    #@11
    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    #@14
    .line 673
    .local v1, sp:Landroid/text/SpannableString;
    :goto_14
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 675
    if-nez v0, :cond_1c

    #@1a
    move-object v2, v1

    #@1b
    .line 776
    goto :goto_b

    #@1c
    .line 678
    :cond_1c
    packed-switch v0, :pswitch_data_114

    #@1f
    .line 772
    new-instance v3, Ljava/lang/RuntimeException;

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "bogus span encoding "

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@37
    throw v3

    #@38
    .line 680
    :pswitch_38
    new-instance v3, Landroid/text/style/AlignmentSpan$Standard;

    #@3a
    invoke-direct {v3, p1}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/os/Parcel;)V

    #@3d
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@40
    goto :goto_14

    #@41
    .line 684
    :pswitch_41
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    #@43
    invoke-direct {v3, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(Landroid/os/Parcel;)V

    #@46
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@49
    goto :goto_14

    #@4a
    .line 688
    :pswitch_4a
    new-instance v3, Landroid/text/style/RelativeSizeSpan;

    #@4c
    invoke-direct {v3, p1}, Landroid/text/style/RelativeSizeSpan;-><init>(Landroid/os/Parcel;)V

    #@4f
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@52
    goto :goto_14

    #@53
    .line 692
    :pswitch_53
    new-instance v3, Landroid/text/style/ScaleXSpan;

    #@55
    invoke-direct {v3, p1}, Landroid/text/style/ScaleXSpan;-><init>(Landroid/os/Parcel;)V

    #@58
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@5b
    goto :goto_14

    #@5c
    .line 696
    :pswitch_5c
    new-instance v3, Landroid/text/style/StrikethroughSpan;

    #@5e
    invoke-direct {v3, p1}, Landroid/text/style/StrikethroughSpan;-><init>(Landroid/os/Parcel;)V

    #@61
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@64
    goto :goto_14

    #@65
    .line 700
    :pswitch_65
    new-instance v3, Landroid/text/style/UnderlineSpan;

    #@67
    invoke-direct {v3, p1}, Landroid/text/style/UnderlineSpan;-><init>(Landroid/os/Parcel;)V

    #@6a
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@6d
    goto :goto_14

    #@6e
    .line 704
    :pswitch_6e
    new-instance v3, Landroid/text/style/StyleSpan;

    #@70
    invoke-direct {v3, p1}, Landroid/text/style/StyleSpan;-><init>(Landroid/os/Parcel;)V

    #@73
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@76
    goto :goto_14

    #@77
    .line 708
    :pswitch_77
    new-instance v3, Landroid/text/style/BulletSpan;

    #@79
    invoke-direct {v3, p1}, Landroid/text/style/BulletSpan;-><init>(Landroid/os/Parcel;)V

    #@7c
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@7f
    goto :goto_14

    #@80
    .line 712
    :pswitch_80
    new-instance v3, Landroid/text/style/QuoteSpan;

    #@82
    invoke-direct {v3, p1}, Landroid/text/style/QuoteSpan;-><init>(Landroid/os/Parcel;)V

    #@85
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@88
    goto :goto_14

    #@89
    .line 716
    :pswitch_89
    new-instance v3, Landroid/text/style/LeadingMarginSpan$Standard;

    #@8b
    invoke-direct {v3, p1}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(Landroid/os/Parcel;)V

    #@8e
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@91
    goto :goto_14

    #@92
    .line 720
    :pswitch_92
    new-instance v3, Landroid/text/style/URLSpan;

    #@94
    invoke-direct {v3, p1}, Landroid/text/style/URLSpan;-><init>(Landroid/os/Parcel;)V

    #@97
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@9a
    goto/16 :goto_14

    #@9c
    .line 724
    :pswitch_9c
    new-instance v3, Landroid/text/style/BackgroundColorSpan;

    #@9e
    invoke-direct {v3, p1}, Landroid/text/style/BackgroundColorSpan;-><init>(Landroid/os/Parcel;)V

    #@a1
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@a4
    goto/16 :goto_14

    #@a6
    .line 728
    :pswitch_a6
    new-instance v3, Landroid/text/style/TypefaceSpan;

    #@a8
    invoke-direct {v3, p1}, Landroid/text/style/TypefaceSpan;-><init>(Landroid/os/Parcel;)V

    #@ab
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@ae
    goto/16 :goto_14

    #@b0
    .line 732
    :pswitch_b0
    new-instance v3, Landroid/text/style/SuperscriptSpan;

    #@b2
    invoke-direct {v3, p1}, Landroid/text/style/SuperscriptSpan;-><init>(Landroid/os/Parcel;)V

    #@b5
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@b8
    goto/16 :goto_14

    #@ba
    .line 736
    :pswitch_ba
    new-instance v3, Landroid/text/style/SubscriptSpan;

    #@bc
    invoke-direct {v3, p1}, Landroid/text/style/SubscriptSpan;-><init>(Landroid/os/Parcel;)V

    #@bf
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@c2
    goto/16 :goto_14

    #@c4
    .line 740
    :pswitch_c4
    new-instance v3, Landroid/text/style/AbsoluteSizeSpan;

    #@c6
    invoke-direct {v3, p1}, Landroid/text/style/AbsoluteSizeSpan;-><init>(Landroid/os/Parcel;)V

    #@c9
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@cc
    goto/16 :goto_14

    #@ce
    .line 744
    :pswitch_ce
    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    #@d0
    invoke-direct {v3, p1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/os/Parcel;)V

    #@d3
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@d6
    goto/16 :goto_14

    #@d8
    .line 748
    :pswitch_d8
    new-instance v3, Landroid/text/Annotation;

    #@da
    invoke-direct {v3, p1}, Landroid/text/Annotation;-><init>(Landroid/os/Parcel;)V

    #@dd
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@e0
    goto/16 :goto_14

    #@e2
    .line 752
    :pswitch_e2
    new-instance v3, Landroid/text/style/SuggestionSpan;

    #@e4
    invoke-direct {v3, p1}, Landroid/text/style/SuggestionSpan;-><init>(Landroid/os/Parcel;)V

    #@e7
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@ea
    goto/16 :goto_14

    #@ec
    .line 756
    :pswitch_ec
    new-instance v3, Landroid/text/style/SpellCheckSpan;

    #@ee
    invoke-direct {v3, p1}, Landroid/text/style/SpellCheckSpan;-><init>(Landroid/os/Parcel;)V

    #@f1
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@f4
    goto/16 :goto_14

    #@f6
    .line 760
    :pswitch_f6
    new-instance v3, Landroid/text/style/SuggestionRangeSpan;

    #@f8
    invoke-direct {v3, p1}, Landroid/text/style/SuggestionRangeSpan;-><init>(Landroid/os/Parcel;)V

    #@fb
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@fe
    goto/16 :goto_14

    #@100
    .line 764
    :pswitch_100
    new-instance v3, Landroid/text/style/EasyEditSpan;

    #@102
    invoke-direct {v3}, Landroid/text/style/EasyEditSpan;-><init>()V

    #@105
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@108
    goto/16 :goto_14

    #@10a
    .line 768
    :pswitch_10a
    new-instance v3, Landroid/text/style/LocaleSpan;

    #@10c
    invoke-direct {v3, p1}, Landroid/text/style/LocaleSpan;-><init>(Landroid/os/Parcel;)V

    #@10f
    invoke-static {p1, v1, v3}, Landroid/text/TextUtils;->access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@112
    goto/16 :goto_14

    #@114
    .line 678
    :pswitch_data_114
    .packed-switch 0x1
        :pswitch_38
        :pswitch_41
        :pswitch_4a
        :pswitch_53
        :pswitch_5c
        :pswitch_65
        :pswitch_6e
        :pswitch_77
        :pswitch_80
        :pswitch_89
        :pswitch_92
        :pswitch_9c
        :pswitch_a6
        :pswitch_b0
        :pswitch_ba
        :pswitch_c4
        :pswitch_ce
        :pswitch_d8
        :pswitch_e2
        :pswitch_ec
        :pswitch_f6
        :pswitch_100
        :pswitch_10a
    .end packed-switch
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 653
    invoke-virtual {p0, p1}, Landroid/text/TextUtils$1;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Ljava/lang/CharSequence;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 781
    new-array v0, p1, [Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 653
    invoke-virtual {p0, p1}, Landroid/text/TextUtils$1;->newArray(I)[Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
