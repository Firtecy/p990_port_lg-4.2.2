.class Landroid/text/TextDirectionHeuristics$FirstStrong;
.super Ljava/lang/Object;
.source "TextDirectionHeuristics.java"

# interfaces
.implements Landroid/text/TextDirectionHeuristics$TextDirectionAlgorithm;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/TextDirectionHeuristics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FirstStrong"
.end annotation


# static fields
.field public static final INSTANCE:Landroid/text/TextDirectionHeuristics$FirstStrong;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 186
    new-instance v0, Landroid/text/TextDirectionHeuristics$FirstStrong;

    #@2
    invoke-direct {v0}, Landroid/text/TextDirectionHeuristics$FirstStrong;-><init>()V

    #@5
    sput-object v0, Landroid/text/TextDirectionHeuristics$FirstStrong;->INSTANCE:Landroid/text/TextDirectionHeuristics$FirstStrong;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 183
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 184
    return-void
.end method


# virtual methods
.method public checkRtl([CII)Landroid/text/TextDirectionHeuristics$TriState;
    .registers 8
    .parameter "text"
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    .line 176
    sget-object v2, Landroid/text/TextDirectionHeuristics$TriState;->UNKNOWN:Landroid/text/TextDirectionHeuristics$TriState;

    #@2
    .line 177
    .local v2, result:Landroid/text/TextDirectionHeuristics$TriState;
    move v1, p2

    #@3
    .local v1, i:I
    add-int v0, p2, p3

    #@5
    .local v0, e:I
    :goto_5
    if-ge v1, v0, :cond_18

    #@7
    sget-object v3, Landroid/text/TextDirectionHeuristics$TriState;->UNKNOWN:Landroid/text/TextDirectionHeuristics$TriState;

    #@9
    if-ne v2, v3, :cond_18

    #@b
    .line 178
    aget-char v3, p1, v1

    #@d
    invoke-static {v3}, Ljava/lang/Character;->getDirectionality(C)B

    #@10
    move-result v3

    #@11
    #calls: Landroid/text/TextDirectionHeuristics;->isRtlTextOrFormat(I)Landroid/text/TextDirectionHeuristics$TriState;
    invoke-static {v3}, Landroid/text/TextDirectionHeuristics;->access$100(I)Landroid/text/TextDirectionHeuristics$TriState;

    #@14
    move-result-object v2

    #@15
    .line 177
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_5

    #@18
    .line 180
    :cond_18
    return-object v2
.end method
