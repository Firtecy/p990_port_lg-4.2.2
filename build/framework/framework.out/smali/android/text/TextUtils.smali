.class public Landroid/text/TextUtils;
.super Ljava/lang/Object;
.source "TextUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/TextUtils$EllipsizeCallback;,
        Landroid/text/TextUtils$TruncateAt;,
        Landroid/text/TextUtils$Reverser;,
        Landroid/text/TextUtils$SimpleStringSplitter;,
        Landroid/text/TextUtils$StringSplitter;
    }
.end annotation


# static fields
.field public static final ABSOLUTE_SIZE_SPAN:I = 0x10

.field public static final ALIGNMENT_SPAN:I = 0x1

.field public static final ANNOTATION:I = 0x12

.field private static ARAB_SCRIPT_SUBTAG:Ljava/lang/String; = null

.field public static final BACKGROUND_COLOR_SPAN:I = 0xc

.field public static final BULLET_SPAN:I = 0x8

.field public static final CAP_MODE_CHARACTERS:I = 0x1000

.field public static final CAP_MODE_SENTENCES:I = 0x4000

.field public static final CAP_MODE_WORDS:I = 0x2000

.field public static final CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field public static final EASY_EDIT_SPAN:I = 0x16

.field private static EMPTY_STRING_ARRAY:[Ljava/lang/String; = null

.field private static final FIRST_RIGHT_TO_LEFT:C = '\u0590'

.field public static final FOREGROUND_COLOR_SPAN:I = 0x2

.field private static HEBR_SCRIPT_SUBTAG:Ljava/lang/String; = null

.field public static final LEADING_MARGIN_SPAN:I = 0xa

.field public static final LOCALE_SPAN:I = 0x17

.field public static final QUOTE_SPAN:I = 0x9

.field public static final RELATIVE_SIZE_SPAN:I = 0x3

.field public static final SCALE_X_SPAN:I = 0x4

.field public static final SPELL_CHECK_SPAN:I = 0x14

.field public static final STRIKETHROUGH_SPAN:I = 0x5

.field public static final STYLE_SPAN:I = 0x7

.field public static final SUBSCRIPT_SPAN:I = 0xf

.field public static final SUGGESTION_RANGE_SPAN:I = 0x15

.field public static final SUGGESTION_SPAN:I = 0x13

.field public static final SUPERSCRIPT_SPAN:I = 0xe

.field public static final TEXT_APPEARANCE_SPAN:I = 0x11

.field public static final TYPEFACE_SPAN:I = 0xd

.field public static final UNDERLINE_SPAN:I = 0x6

.field public static final URL_SPAN:I = 0xb

.field private static final ZWNBS_CHAR:C = '\ufeff'

.field private static sLock:Ljava/lang/Object;

.field private static sTemp:[C


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 652
    new-instance v0, Landroid/text/TextUtils$1;

    #@2
    invoke-direct {v0}, Landroid/text/TextUtils$1;-><init>()V

    #@5
    sput-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    .line 1809
    new-instance v0, Ljava/lang/Object;

    #@9
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@c
    sput-object v0, Landroid/text/TextUtils;->sLock:Ljava/lang/Object;

    #@e
    .line 1811
    const/4 v0, 0x0

    #@f
    sput-object v0, Landroid/text/TextUtils;->sTemp:[C

    #@11
    .line 1813
    const/4 v0, 0x0

    #@12
    new-array v0, v0, [Ljava/lang/String;

    #@14
    sput-object v0, Landroid/text/TextUtils;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    #@16
    .line 1817
    const-string v0, "Arab"

    #@18
    sput-object v0, Landroid/text/TextUtils;->ARAB_SCRIPT_SUBTAG:Ljava/lang/String;

    #@1a
    .line 1818
    const-string v0, "Hebr"

    #@1c
    sput-object v0, Landroid/text/TextUtils;->HEBR_SCRIPT_SUBTAG:Ljava/lang/String;

    #@1e
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$000(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-static {p0, p1, p2}, Landroid/text/TextUtils;->readSpan(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V

    #@3
    return-void
.end method

.method public static commaEllipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLjava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 11
    .parameter "text"
    .parameter "p"
    .parameter "avail"
    .parameter "oneMore"
    .parameter "more"

    #@0
    .prologue
    .line 1221
    sget-object v5, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v2, p2

    #@5
    move-object v3, p3

    #@6
    move-object v4, p4

    #@7
    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->commaEllipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLjava/lang/String;Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/CharSequence;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static commaEllipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLjava/lang/String;Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/CharSequence;
    .registers 28
    .parameter "text"
    .parameter "p"
    .parameter "avail"
    .parameter "oneMore"
    .parameter "more"
    .parameter "textDir"

    #@0
    .prologue
    .line 1231
    invoke-static {}, Landroid/text/MeasuredText;->obtain()Landroid/text/MeasuredText;

    #@3
    move-result-object v2

    #@4
    .line 1233
    .local v2, mt:Landroid/text/MeasuredText;
    :try_start_4
    invoke-interface/range {p0 .. p0}, Ljava/lang/CharSequence;->length()I

    #@7
    move-result v6

    #@8
    .line 1234
    .local v6, len:I
    const/4 v5, 0x0

    #@9
    move-object/from16 v3, p1

    #@b
    move-object/from16 v4, p0

    #@d
    move-object/from16 v7, p5

    #@f
    invoke-static/range {v2 .. v7}, Landroid/text/TextUtils;->setPara(Landroid/text/MeasuredText;Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;)F
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_d1

    #@12
    move-result v20

    #@13
    .line 1235
    .local v20, width:F
    cmpg-float v3, v20, p2

    #@15
    if-gtz v3, :cond_1b

    #@17
    .line 1289
    invoke-static {v2}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@1a
    .end local p0
    :goto_1a
    return-object p0

    #@1b
    .line 1239
    .restart local p0
    :cond_1b
    :try_start_1b
    iget-object v8, v2, Landroid/text/MeasuredText;->mChars:[C

    #@1d
    .line 1241
    .local v8, buf:[C
    const/4 v9, 0x0

    #@1e
    .line 1242
    .local v9, commaCount:I
    const/4 v12, 0x0

    #@1f
    .local v12, i:I
    :goto_1f
    if-ge v12, v6, :cond_2c

    #@21
    .line 1243
    aget-char v3, v8, v12

    #@23
    const/16 v4, 0x2c

    #@25
    if-ne v3, v4, :cond_29

    #@27
    .line 1244
    add-int/lit8 v9, v9, 0x1

    #@29
    .line 1242
    :cond_29
    add-int/lit8 v12, v12, 0x1

    #@2b
    goto :goto_1f

    #@2c
    .line 1248
    :cond_2c
    add-int/lit8 v17, v9, 0x1

    #@2e
    .line 1250
    .local v17, remaining:I
    const/4 v14, 0x0

    #@2f
    .line 1251
    .local v14, ok:I
    const-string v15, ""

    #@31
    .line 1253
    .local v15, okFormat:Ljava/lang/String;
    const/16 v19, 0x0

    #@33
    .line 1254
    .local v19, w:I
    const/4 v10, 0x0

    #@34
    .line 1255
    .local v10, count:I
    iget-object v0, v2, Landroid/text/MeasuredText;->mWidths:[F

    #@36
    move-object/from16 v21, v0

    #@38
    .line 1257
    .local v21, widths:[F
    invoke-static {}, Landroid/text/MeasuredText;->obtain()Landroid/text/MeasuredText;

    #@3b
    move-result-object v18

    #@3c
    .line 1258
    .local v18, tempMt:Landroid/text/MeasuredText;
    const/4 v12, 0x0

    #@3d
    :goto_3d
    if-ge v12, v6, :cond_b7

    #@3f
    .line 1259
    move/from16 v0, v19

    #@41
    int-to-float v3, v0

    #@42
    aget v4, v21, v12

    #@44
    add-float/2addr v3, v4

    #@45
    float-to-int v0, v3

    #@46
    move/from16 v19, v0

    #@48
    .line 1261
    aget-char v3, v8, v12

    #@4a
    const/16 v4, 0x2c

    #@4c
    if-ne v3, v4, :cond_90

    #@4e
    .line 1262
    add-int/lit8 v10, v10, 0x1

    #@50
    .line 1267
    add-int/lit8 v17, v17, -0x1

    #@52
    const/4 v3, 0x1

    #@53
    move/from16 v0, v17

    #@55
    if-ne v0, v3, :cond_93

    #@57
    .line 1268
    new-instance v3, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v4, " "

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    move-object/from16 v0, p3

    #@64
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v11

    #@6c
    .line 1274
    .local v11, format:Ljava/lang/String;
    :goto_6c
    const/4 v3, 0x0

    #@6d
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@70
    move-result v4

    #@71
    move-object/from16 v0, v18

    #@73
    move-object/from16 v1, p5

    #@75
    invoke-virtual {v0, v11, v3, v4, v1}, Landroid/text/MeasuredText;->setPara(Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;)V

    #@78
    .line 1275
    move-object/from16 v0, v18

    #@7a
    iget v3, v0, Landroid/text/MeasuredText;->mLen:I

    #@7c
    const/4 v4, 0x0

    #@7d
    move-object/from16 v0, v18

    #@7f
    move-object/from16 v1, p1

    #@81
    invoke-virtual {v0, v1, v3, v4}, Landroid/text/MeasuredText;->addStyleRun(Landroid/text/TextPaint;ILandroid/graphics/Paint$FontMetricsInt;)F

    #@84
    move-result v13

    #@85
    .line 1277
    .local v13, moreWid:F
    move/from16 v0, v19

    #@87
    int-to-float v3, v0

    #@88
    add-float/2addr v3, v13

    #@89
    cmpg-float v3, v3, p2

    #@8b
    if-gtz v3, :cond_90

    #@8d
    .line 1278
    add-int/lit8 v14, v12, 0x1

    #@8f
    .line 1279
    move-object v15, v11

    #@90
    .line 1258
    .end local v11           #format:Ljava/lang/String;
    .end local v13           #moreWid:F
    :cond_90
    add-int/lit8 v12, v12, 0x1

    #@92
    goto :goto_3d

    #@93
    .line 1270
    :cond_93
    new-instance v3, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v4, " "

    #@9a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v3

    #@9e
    const/4 v4, 0x1

    #@9f
    new-array v4, v4, [Ljava/lang/Object;

    #@a1
    const/4 v5, 0x0

    #@a2
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a5
    move-result-object v7

    #@a6
    aput-object v7, v4, v5

    #@a8
    move-object/from16 v0, p4

    #@aa
    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@ad
    move-result-object v4

    #@ae
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v3

    #@b2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v11

    #@b6
    .restart local v11       #format:Ljava/lang/String;
    goto :goto_6c

    #@b7
    .line 1283
    .end local v11           #format:Ljava/lang/String;
    :cond_b7
    invoke-static/range {v18 .. v18}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@ba
    .line 1285
    new-instance v16, Landroid/text/SpannableStringBuilder;

    #@bc
    move-object/from16 v0, v16

    #@be
    invoke-direct {v0, v15}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@c1
    .line 1286
    .local v16, out:Landroid/text/SpannableStringBuilder;
    const/4 v3, 0x0

    #@c2
    const/4 v4, 0x0

    #@c3
    move-object/from16 v0, v16

    #@c5
    move-object/from16 v1, p0

    #@c7
    invoke-virtual {v0, v3, v1, v4, v14}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    :try_end_ca
    .catchall {:try_start_1b .. :try_end_ca} :catchall_d1

    #@ca
    .line 1289
    invoke-static {v2}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@cd
    move-object/from16 p0, v16

    #@cf
    goto/16 :goto_1a

    #@d1
    .end local v6           #len:I
    .end local v8           #buf:[C
    .end local v9           #commaCount:I
    .end local v10           #count:I
    .end local v12           #i:I
    .end local v14           #ok:I
    .end local v15           #okFormat:Ljava/lang/String;
    .end local v16           #out:Landroid/text/SpannableStringBuilder;
    .end local v17           #remaining:I
    .end local v18           #tempMt:Landroid/text/MeasuredText;
    .end local v19           #w:I
    .end local v20           #width:F
    .end local v21           #widths:[F
    :catchall_d1
    move-exception v3

    #@d2
    invoke-static {v2}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@d5
    throw v3
.end method

.method public static varargs concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 10
    .parameter "text"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1406
    array-length v0, p0

    #@2
    if-nez v0, :cond_7

    #@4
    .line 1407
    const-string v0, ""

    #@6
    .line 1443
    :goto_6
    return-object v0

    #@7
    .line 1410
    :cond_7
    array-length v0, p0

    #@8
    const/4 v3, 0x1

    #@9
    if-ne v0, v3, :cond_e

    #@b
    .line 1411
    aget-object v0, p0, v1

    #@d
    goto :goto_6

    #@e
    .line 1414
    :cond_e
    const/4 v8, 0x0

    #@f
    .line 1415
    .local v8, spanned:Z
    const/4 v6, 0x0

    #@10
    .local v6, i:I
    :goto_10
    array-length v0, p0

    #@11
    if-ge v6, v0, :cond_1a

    #@13
    .line 1416
    aget-object v0, p0, v6

    #@15
    instance-of v0, v0, Landroid/text/Spanned;

    #@17
    if-eqz v0, :cond_2b

    #@19
    .line 1417
    const/4 v8, 0x1

    #@1a
    .line 1422
    :cond_1a
    new-instance v7, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    .line 1423
    .local v7, sb:Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    #@20
    :goto_20
    array-length v0, p0

    #@21
    if-ge v6, v0, :cond_2e

    #@23
    .line 1424
    aget-object v0, p0, v6

    #@25
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@28
    .line 1423
    add-int/lit8 v6, v6, 0x1

    #@2a
    goto :goto_20

    #@2b
    .line 1415
    .end local v7           #sb:Ljava/lang/StringBuilder;
    :cond_2b
    add-int/lit8 v6, v6, 0x1

    #@2d
    goto :goto_10

    #@2e
    .line 1427
    .restart local v7       #sb:Ljava/lang/StringBuilder;
    :cond_2e
    if-nez v8, :cond_35

    #@30
    .line 1428
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    goto :goto_6

    #@35
    .line 1431
    :cond_35
    new-instance v4, Landroid/text/SpannableString;

    #@37
    invoke-direct {v4, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    #@3a
    .line 1432
    .local v4, ss:Landroid/text/SpannableString;
    const/4 v5, 0x0

    #@3b
    .line 1433
    .local v5, off:I
    const/4 v6, 0x0

    #@3c
    :goto_3c
    array-length v0, p0

    #@3d
    if-ge v6, v0, :cond_58

    #@3f
    .line 1434
    aget-object v0, p0, v6

    #@41
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@44
    move-result v2

    #@45
    .line 1436
    .local v2, len:I
    aget-object v0, p0, v6

    #@47
    instance-of v0, v0, Landroid/text/Spanned;

    #@49
    if-eqz v0, :cond_54

    #@4b
    .line 1437
    aget-object v0, p0, v6

    #@4d
    check-cast v0, Landroid/text/Spanned;

    #@4f
    const-class v3, Ljava/lang/Object;

    #@51
    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    #@54
    .line 1440
    :cond_54
    add-int/2addr v5, v2

    #@55
    .line 1433
    add-int/lit8 v6, v6, 0x1

    #@57
    goto :goto_3c

    #@58
    .line 1443
    .end local v2           #len:I
    :cond_58
    new-instance v0, Landroid/text/SpannedString;

    #@5a
    invoke-direct {v0, v4}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    #@5d
    goto :goto_6
.end method

.method public static copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V
    .registers 14
    .parameter "source"
    .parameter "start"
    .parameter "end"
    .parameter "kind"
    .parameter "dest"
    .parameter "destoff"

    #@0
    .prologue
    .line 1028
    if-nez p3, :cond_4

    #@2
    .line 1029
    const-class p3, Ljava/lang/Object;

    #@4
    .line 1032
    :cond_4
    invoke-interface {p0, p1, p2, p3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@7
    move-result-object v3

    #@8
    .line 1034
    .local v3, spans:[Ljava/lang/Object;
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    array-length v5, v3

    #@a
    if-ge v2, v5, :cond_32

    #@c
    .line 1035
    aget-object v5, v3, v2

    #@e
    invoke-interface {p0, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@11
    move-result v4

    #@12
    .line 1036
    .local v4, st:I
    aget-object v5, v3, v2

    #@14
    invoke-interface {p0, v5}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@17
    move-result v0

    #@18
    .line 1037
    .local v0, en:I
    aget-object v5, v3, v2

    #@1a
    invoke-interface {p0, v5}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    #@1d
    move-result v1

    #@1e
    .line 1039
    .local v1, fl:I
    if-ge v4, p1, :cond_21

    #@20
    .line 1040
    move v4, p1

    #@21
    .line 1041
    :cond_21
    if-le v0, p2, :cond_24

    #@23
    .line 1042
    move v0, p2

    #@24
    .line 1044
    :cond_24
    aget-object v5, v3, v2

    #@26
    sub-int v6, v4, p1

    #@28
    add-int/2addr v6, p5

    #@29
    sub-int v7, v0, p1

    #@2b
    add-int/2addr v7, p5

    #@2c
    invoke-interface {p4, v5, v6, v7, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@2f
    .line 1034
    add-int/lit8 v2, v2, 0x1

    #@31
    goto :goto_9

    #@32
    .line 1047
    .end local v0           #en:I
    .end local v1           #fl:I
    .end local v4           #st:I
    :cond_32
    return-void
.end method

.method public static delimitedStringContains(Ljava/lang/String;CLjava/lang/String;)Z
    .registers 9
    .parameter "delimitedString"
    .parameter "delimiter"
    .parameter "item"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1658
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v5

    #@6
    if-nez v5, :cond_e

    #@8
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b
    move-result v5

    #@c
    if-eqz v5, :cond_10

    #@e
    :cond_e
    move v3, v4

    #@f
    .line 1676
    :cond_f
    :goto_f
    return v3

    #@10
    .line 1661
    :cond_10
    const/4 v2, -0x1

    #@11
    .line 1662
    .local v2, pos:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@14
    move-result v1

    #@15
    .line 1663
    .local v1, length:I
    :cond_15
    add-int/lit8 v5, v2, 0x1

    #@17
    invoke-virtual {p0, p2, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    #@1a
    move-result v2

    #@1b
    const/4 v5, -0x1

    #@1c
    if-eq v2, v5, :cond_37

    #@1e
    .line 1664
    if-lez v2, :cond_28

    #@20
    add-int/lit8 v5, v2, -0x1

    #@22
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@25
    move-result v5

    #@26
    if-ne v5, p1, :cond_15

    #@28
    .line 1667
    :cond_28
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@2b
    move-result v5

    #@2c
    add-int v0, v2, v5

    #@2e
    .line 1668
    .local v0, expectedDelimiterPos:I
    if-eq v0, v1, :cond_f

    #@30
    .line 1672
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@33
    move-result v5

    #@34
    if-ne v5, p1, :cond_15

    #@36
    goto :goto_f

    #@37
    .end local v0           #expectedDelimiterPos:I
    :cond_37
    move v3, v4

    #@38
    .line 1676
    goto :goto_f
.end method

.method static doesNotNeedBidi(Ljava/lang/CharSequence;II)Z
    .registers 6
    .parameter "s"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1323
    move v0, p1

    #@1
    .local v0, i:I
    :goto_1
    if-ge v0, p2, :cond_10

    #@3
    .line 1324
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@6
    move-result v1

    #@7
    const/16 v2, 0x590

    #@9
    if-lt v1, v2, :cond_d

    #@b
    .line 1325
    const/4 v1, 0x0

    #@c
    .line 1328
    :goto_c
    return v1

    #@d
    .line 1323
    :cond_d
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_1

    #@10
    .line 1328
    :cond_10
    const/4 v1, 0x1

    #@11
    goto :goto_c
.end method

.method static doesNotNeedBidi([CII)Z
    .registers 7
    .parameter "text"
    .parameter "start"
    .parameter "len"

    #@0
    .prologue
    .line 1333
    move v1, p1

    #@1
    .local v1, i:I
    add-int v0, v1, p2

    #@3
    .local v0, e:I
    :goto_3
    if-ge v1, v0, :cond_10

    #@5
    .line 1334
    aget-char v2, p0, v1

    #@7
    const/16 v3, 0x590

    #@9
    if-lt v2, v3, :cond_d

    #@b
    .line 1335
    const/4 v2, 0x0

    #@c
    .line 1338
    :goto_c
    return v2

    #@d
    .line 1333
    :cond_d
    add-int/lit8 v1, v1, 0x1

    #@f
    goto :goto_3

    #@10
    .line 1338
    :cond_10
    const/4 v2, 0x1

    #@11
    goto :goto_c
.end method

.method public static dumpSpans(Ljava/lang/CharSequence;Landroid/util/Printer;Ljava/lang/String;)V
    .registers 10
    .parameter "cs"
    .parameter "printer"
    .parameter "prefix"

    #@0
    .prologue
    .line 791
    instance-of v4, p0, Landroid/text/Spanned;

    #@2
    if-eqz v4, :cond_89

    #@4
    move-object v3, p0

    #@5
    .line 792
    check-cast v3, Landroid/text/Spanned;

    #@7
    .line 793
    .local v3, sp:Landroid/text/Spanned;
    const/4 v4, 0x0

    #@8
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@b
    move-result v5

    #@c
    const-class v6, Ljava/lang/Object;

    #@e
    invoke-interface {v3, v4, v5, v6}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    .line 795
    .local v2, os:[Ljava/lang/Object;
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    array-length v4, v2

    #@14
    if-ge v0, v4, :cond_a3

    #@16
    .line 796
    aget-object v1, v2, v0

    #@18
    .line 797
    .local v1, o:Ljava/lang/Object;
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-interface {v3, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@24
    move-result v5

    #@25
    invoke-interface {v3, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@28
    move-result v6

    #@29
    invoke-interface {p0, v5, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    const-string v5, ": "

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@3a
    move-result v5

    #@3b
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    const-string v5, " "

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    const-string v5, " ("

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    invoke-interface {v3, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@5e
    move-result v5

    #@5f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    const-string v5, "-"

    #@65
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    invoke-interface {v3, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@6c
    move-result v5

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    const-string v5, ") fl=#"

    #@73
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v4

    #@77
    invoke-interface {v3, v1}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    #@7a
    move-result v5

    #@7b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v4

    #@83
    invoke-interface {p1, v4}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@86
    .line 795
    add-int/lit8 v0, v0, 0x1

    #@88
    goto :goto_13

    #@89
    .line 805
    .end local v0           #i:I
    .end local v1           #o:Ljava/lang/Object;
    .end local v2           #os:[Ljava/lang/Object;
    .end local v3           #sp:Landroid/text/Spanned;
    :cond_89
    new-instance v4, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v4

    #@92
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v4

    #@96
    const-string v5, ": (no spans)"

    #@98
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v4

    #@9c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v4

    #@a0
    invoke-interface {p1, v4}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@a3
    .line 807
    :cond_a3
    return-void
.end method

.method public static ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;
    .registers 10
    .parameter "text"
    .parameter "p"
    .parameter "avail"
    .parameter "where"

    #@0
    .prologue
    .line 1077
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v2, p2

    #@5
    move-object v3, p3

    #@6
    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;ZLandroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public static ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;ZLandroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;
    .registers 14
    .parameter "text"
    .parameter "paint"
    .parameter "avail"
    .parameter "where"
    .parameter "preserveLength"
    .parameter "callback"

    #@0
    .prologue
    .line 1098
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END_SMALL:Landroid/text/TextUtils$TruncateAt;

    #@2
    if-ne p3, v0, :cond_1c

    #@4
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    const v1, 0x1040090

    #@b
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@e
    move-result-object v7

    #@f
    .line 1102
    .local v7, ellipsis:Ljava/lang/String;
    :goto_f
    sget-object v6, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    #@11
    move-object v0, p0

    #@12
    move-object v1, p1

    #@13
    move v2, p2

    #@14
    move-object v3, p3

    #@15
    move v4, p4

    #@16
    move-object v5, p5

    #@17
    invoke-static/range {v0 .. v7}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;ZLandroid/text/TextUtils$EllipsizeCallback;Landroid/text/TextDirectionHeuristic;Ljava/lang/String;)Ljava/lang/CharSequence;

    #@1a
    move-result-object v0

    #@1b
    return-object v0

    #@1c
    .line 1098
    .end local v7           #ellipsis:Ljava/lang/String;
    :cond_1c
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@1f
    move-result-object v0

    #@20
    const v1, 0x104008f

    #@23
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@26
    move-result-object v7

    #@27
    goto :goto_f
.end method

.method public static ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;ZLandroid/text/TextUtils$EllipsizeCallback;Landroid/text/TextDirectionHeuristic;Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 30
    .parameter "text"
    .parameter "paint"
    .parameter "avail"
    .parameter "where"
    .parameter "preserveLength"
    .parameter "callback"
    .parameter "textDir"
    .parameter "ellipsis"

    #@0
    .prologue
    .line 1127
    invoke-interface/range {p0 .. p0}, Ljava/lang/CharSequence;->length()I

    #@3
    move-result v15

    #@4
    .line 1129
    .local v15, len:I
    invoke-static {}, Landroid/text/MeasuredText;->obtain()Landroid/text/MeasuredText;

    #@7
    move-result-object v3

    #@8
    .line 1131
    .local v3, mt:Landroid/text/MeasuredText;
    const/4 v6, 0x0

    #@9
    :try_start_9
    invoke-interface/range {p0 .. p0}, Ljava/lang/CharSequence;->length()I

    #@c
    move-result v7

    #@d
    move-object/from16 v4, p1

    #@f
    move-object/from16 v5, p0

    #@11
    move-object/from16 v8, p6

    #@13
    invoke-static/range {v3 .. v8}, Landroid/text/TextUtils;->setPara(Landroid/text/MeasuredText;Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;)F

    #@16
    move-result v21

    #@17
    .line 1133
    .local v21, width:F
    cmpg-float v5, v21, p2

    #@19
    if-gtz v5, :cond_28

    #@1b
    .line 1134
    if-eqz p5, :cond_24

    #@1d
    .line 1135
    const/4 v5, 0x0

    #@1e
    const/4 v6, 0x0

    #@1f
    move-object/from16 v0, p5

    #@21
    invoke-interface {v0, v5, v6}, Landroid/text/TextUtils$EllipsizeCallback;->ellipsized(II)V
    :try_end_24
    .catchall {:try_start_9 .. :try_end_24} :catchall_13a

    #@24
    .line 1202
    :cond_24
    invoke-static {v3}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@27
    .end local p0
    :goto_27
    return-object p0

    #@28
    .line 1143
    .restart local p0
    :cond_28
    :try_start_28
    move-object/from16 v0, p1

    #@2a
    move-object/from16 v1, p7

    #@2c
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    #@2f
    move-result v11

    #@30
    .line 1144
    .local v11, ellipsiswid:F
    sub-float p2, p2, v11

    #@32
    .line 1146
    const/4 v13, 0x0

    #@33
    .line 1147
    .local v13, left:I
    move/from16 v17, v15

    #@35
    .line 1148
    .local v17, right:I
    const/4 v5, 0x0

    #@36
    cmpg-float v5, p2, v5

    #@38
    if-gez v5, :cond_72

    #@3a
    move v14, v13

    #@3b
    .line 1160
    .end local v13           #left:I
    .local v14, left:I
    :goto_3b
    if-eqz p5, :cond_44

    #@3d
    .line 1161
    move-object/from16 v0, p5

    #@3f
    move/from16 v1, v17

    #@41
    invoke-interface {v0, v14, v1}, Landroid/text/TextUtils$EllipsizeCallback;->ellipsized(II)V

    #@44
    .line 1164
    :cond_44
    iget-object v10, v3, Landroid/text/MeasuredText;->mChars:[C

    #@46
    .line 1165
    .local v10, buf:[C
    move-object/from16 v0, p0

    #@48
    instance-of v5, v0, Landroid/text/Spanned;

    #@4a
    if-eqz v5, :cond_b6

    #@4c
    move-object/from16 v0, p0

    #@4e
    check-cast v0, Landroid/text/Spanned;

    #@50
    move-object v5, v0

    #@51
    move-object v4, v5

    #@52
    .line 1167
    .local v4, sp:Landroid/text/Spanned;
    :goto_52
    sub-int v5, v17, v14

    #@54
    sub-int v16, v15, v5

    #@56
    .line 1168
    .local v16, remaining:I
    if-eqz p4, :cond_df

    #@58
    .line 1169
    if-lez v16, :cond_13f

    #@5a
    .line 1170
    add-int/lit8 v13, v14, 0x1

    #@5c
    .end local v14           #left:I
    .restart local v13       #left:I
    const/4 v5, 0x0

    #@5d
    move-object/from16 v0, p7

    #@5f
    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    #@62
    move-result v5

    #@63
    aput-char v5, v10, v14

    #@65
    .line 1172
    :goto_65
    move v12, v13

    #@66
    .local v12, i:I
    :goto_66
    move/from16 v0, v17

    #@68
    if-ge v12, v0, :cond_b8

    #@6a
    .line 1173
    const v5, 0xfeff

    #@6d
    aput-char v5, v10, v12

    #@6f
    .line 1172
    add-int/lit8 v12, v12, 0x1

    #@71
    goto :goto_66

    #@72
    .line 1150
    .end local v4           #sp:Landroid/text/Spanned;
    .end local v10           #buf:[C
    .end local v12           #i:I
    .end local v16           #remaining:I
    :cond_72
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    #@74
    move-object/from16 v0, p3

    #@76
    if-ne v0, v5, :cond_83

    #@78
    .line 1151
    const/4 v5, 0x0

    #@79
    move/from16 v0, p2

    #@7b
    invoke-virtual {v3, v15, v5, v0}, Landroid/text/MeasuredText;->breakText(IZF)I

    #@7e
    move-result v5

    #@7f
    sub-int v17, v15, v5

    #@81
    move v14, v13

    #@82
    .end local v13           #left:I
    .restart local v14       #left:I
    goto :goto_3b

    #@83
    .line 1152
    .end local v14           #left:I
    .restart local v13       #left:I
    :cond_83
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    #@85
    move-object/from16 v0, p3

    #@87
    if-eq v0, v5, :cond_8f

    #@89
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END_SMALL:Landroid/text/TextUtils$TruncateAt;

    #@8b
    move-object/from16 v0, p3

    #@8d
    if-ne v0, v5, :cond_98

    #@8f
    .line 1153
    :cond_8f
    const/4 v5, 0x1

    #@90
    move/from16 v0, p2

    #@92
    invoke-virtual {v3, v15, v5, v0}, Landroid/text/MeasuredText;->breakText(IZF)I

    #@95
    move-result v13

    #@96
    move v14, v13

    #@97
    .end local v13           #left:I
    .restart local v14       #left:I
    goto :goto_3b

    #@98
    .line 1155
    .end local v14           #left:I
    .restart local v13       #left:I
    :cond_98
    const/4 v5, 0x0

    #@99
    const/high16 v6, 0x4000

    #@9b
    div-float v6, p2, v6

    #@9d
    invoke-virtual {v3, v15, v5, v6}, Landroid/text/MeasuredText;->breakText(IZF)I

    #@a0
    move-result v5

    #@a1
    sub-int v17, v15, v5

    #@a3
    .line 1156
    move/from16 v0, v17

    #@a5
    invoke-virtual {v3, v0, v15}, Landroid/text/MeasuredText;->measure(II)F

    #@a8
    move-result v5

    #@a9
    sub-float p2, p2, v5

    #@ab
    .line 1157
    const/4 v5, 0x1

    #@ac
    move/from16 v0, v17

    #@ae
    move/from16 v1, p2

    #@b0
    invoke-virtual {v3, v0, v5, v1}, Landroid/text/MeasuredText;->breakText(IZF)I

    #@b3
    move-result v13

    #@b4
    move v14, v13

    #@b5
    .end local v13           #left:I
    .restart local v14       #left:I
    goto :goto_3b

    #@b6
    .line 1165
    .restart local v10       #buf:[C
    :cond_b6
    const/4 v4, 0x0

    #@b7
    goto :goto_52

    #@b8
    .line 1175
    .end local v14           #left:I
    .restart local v4       #sp:Landroid/text/Spanned;
    .restart local v12       #i:I
    .restart local v13       #left:I
    .restart local v16       #remaining:I
    :cond_b8
    new-instance v18, Ljava/lang/String;

    #@ba
    const/4 v5, 0x0

    #@bb
    move-object/from16 v0, v18

    #@bd
    invoke-direct {v0, v10, v5, v15}, Ljava/lang/String;-><init>([CII)V
    :try_end_c0
    .catchall {:try_start_28 .. :try_end_c0} :catchall_13a

    #@c0
    .line 1176
    .local v18, s:Ljava/lang/String;
    if-nez v4, :cond_c9

    #@c2
    .line 1202
    invoke-static {v3}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@c5
    move-object/from16 p0, v18

    #@c7
    goto/16 :goto_27

    #@c9
    .line 1179
    :cond_c9
    :try_start_c9
    new-instance v8, Landroid/text/SpannableString;

    #@cb
    move-object/from16 v0, v18

    #@cd
    invoke-direct {v8, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    #@d0
    .line 1180
    .local v8, ss:Landroid/text/SpannableString;
    const/4 v5, 0x0

    #@d1
    const-class v7, Ljava/lang/Object;

    #@d3
    const/4 v9, 0x0

    #@d4
    move v6, v15

    #@d5
    invoke-static/range {v4 .. v9}, Landroid/text/TextUtils;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V
    :try_end_d8
    .catchall {:try_start_c9 .. :try_end_d8} :catchall_13a

    #@d8
    .line 1202
    invoke-static {v3}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@db
    move-object/from16 p0, v8

    #@dd
    goto/16 :goto_27

    #@df
    .line 1184
    .end local v8           #ss:Landroid/text/SpannableString;
    .end local v12           #i:I
    .end local v13           #left:I
    .end local v18           #s:Ljava/lang/String;
    .restart local v14       #left:I
    :cond_df
    if-nez v16, :cond_e8

    #@e1
    .line 1185
    :try_start_e1
    const-string p0, ""
    :try_end_e3
    .catchall {:try_start_e1 .. :try_end_e3} :catchall_13a

    #@e3
    .line 1202
    .end local p0
    invoke-static {v3}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@e6
    goto/16 :goto_27

    #@e8
    .line 1188
    .restart local p0
    :cond_e8
    if-nez v4, :cond_116

    #@ea
    .line 1189
    :try_start_ea
    new-instance v19, Ljava/lang/StringBuilder;

    #@ec
    invoke-virtual/range {p7 .. p7}, Ljava/lang/String;->length()I

    #@ef
    move-result v5

    #@f0
    add-int v5, v5, v16

    #@f2
    move-object/from16 v0, v19

    #@f4
    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    #@f7
    .line 1190
    .local v19, sb:Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    #@f8
    move-object/from16 v0, v19

    #@fa
    invoke-virtual {v0, v10, v5, v14}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@fd
    .line 1191
    move-object/from16 v0, v19

    #@ff
    move-object/from16 v1, p7

    #@101
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    .line 1192
    sub-int v5, v15, v17

    #@106
    move-object/from16 v0, v19

    #@108
    move/from16 v1, v17

    #@10a
    invoke-virtual {v0, v10, v1, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@10d
    .line 1193
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_110
    .catchall {:try_start_ea .. :try_end_110} :catchall_13a

    #@110
    move-result-object p0

    #@111
    .line 1202
    .end local p0
    invoke-static {v3}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@114
    goto/16 :goto_27

    #@116
    .line 1196
    .end local v19           #sb:Ljava/lang/StringBuilder;
    .restart local p0
    :cond_116
    :try_start_116
    new-instance v20, Landroid/text/SpannableStringBuilder;

    #@118
    invoke-direct/range {v20 .. v20}, Landroid/text/SpannableStringBuilder;-><init>()V

    #@11b
    .line 1197
    .local v20, ssb:Landroid/text/SpannableStringBuilder;
    const/4 v5, 0x0

    #@11c
    move-object/from16 v0, v20

    #@11e
    move-object/from16 v1, p0

    #@120
    invoke-virtual {v0, v1, v5, v14}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@123
    .line 1198
    move-object/from16 v0, v20

    #@125
    move-object/from16 v1, p7

    #@127
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@12a
    .line 1199
    move-object/from16 v0, v20

    #@12c
    move-object/from16 v1, p0

    #@12e
    move/from16 v2, v17

    #@130
    invoke-virtual {v0, v1, v2, v15}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    :try_end_133
    .catchall {:try_start_116 .. :try_end_133} :catchall_13a

    #@133
    .line 1202
    invoke-static {v3}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@136
    move-object/from16 p0, v20

    #@138
    goto/16 :goto_27

    #@13a
    .end local v4           #sp:Landroid/text/Spanned;
    .end local v10           #buf:[C
    .end local v11           #ellipsiswid:F
    .end local v14           #left:I
    .end local v16           #remaining:I
    .end local v17           #right:I
    .end local v20           #ssb:Landroid/text/SpannableStringBuilder;
    .end local v21           #width:F
    :catchall_13a
    move-exception v5

    #@13b
    invoke-static {v3}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@13e
    throw v5

    #@13f
    .restart local v4       #sp:Landroid/text/Spanned;
    .restart local v10       #buf:[C
    .restart local v11       #ellipsiswid:F
    .restart local v14       #left:I
    .restart local v16       #remaining:I
    .restart local v17       #right:I
    .restart local v21       #width:F
    :cond_13f
    move v13, v14

    #@140
    .end local v14           #left:I
    .restart local v13       #left:I
    goto/16 :goto_65
.end method

.method public static equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .registers 8
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 484
    if-ne p0, p1, :cond_5

    #@4
    .line 496
    :cond_4
    :goto_4
    return v2

    #@5
    .line 486
    :cond_5
    if-eqz p0, :cond_32

    #@7
    if-eqz p1, :cond_32

    #@9
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@c
    move-result v1

    #@d
    .local v1, length:I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@10
    move-result v4

    #@11
    if-ne v1, v4, :cond_32

    #@13
    .line 487
    instance-of v4, p0, Ljava/lang/String;

    #@15
    if-eqz v4, :cond_20

    #@17
    instance-of v4, p1, Ljava/lang/String;

    #@19
    if-eqz v4, :cond_20

    #@1b
    .line 488
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v2

    #@1f
    goto :goto_4

    #@20
    .line 490
    :cond_20
    const/4 v0, 0x0

    #@21
    .local v0, i:I
    :goto_21
    if-ge v0, v1, :cond_4

    #@23
    .line 491
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@26
    move-result v4

    #@27
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@2a
    move-result v5

    #@2b
    if-eq v4, v5, :cond_2f

    #@2d
    move v2, v3

    #@2e
    goto :goto_4

    #@2f
    .line 490
    :cond_2f
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_21

    #@32
    .end local v0           #i:I
    .end local v1           #length:I
    :cond_32
    move v2, v3

    #@33
    .line 496
    goto :goto_4
.end method

.method public static varargs expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 9
    .parameter "template"
    .parameter "values"

    #@0
    .prologue
    const/16 v6, 0x5e

    #@2
    .line 861
    array-length v4, p1

    #@3
    const/16 v5, 0x9

    #@5
    if-le v4, v5, :cond_10

    #@7
    .line 862
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@9
    const-string/jumbo v5, "max of 9 values are supported"

    #@c
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v4

    #@10
    .line 865
    :cond_10
    new-instance v2, Landroid/text/SpannableStringBuilder;

    #@12
    invoke-direct {v2, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@15
    .line 868
    .local v2, ssb:Landroid/text/SpannableStringBuilder;
    const/4 v0, 0x0

    #@16
    .line 869
    .local v0, i:I
    :goto_16
    :try_start_16
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    #@19
    move-result v4

    #@1a
    if-ge v0, v4, :cond_5f

    #@1c
    .line 870
    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@1f
    move-result v4

    #@20
    if-ne v4, v6, :cond_a0

    #@22
    .line 871
    add-int/lit8 v4, v0, 0x1

    #@24
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@27
    move-result v1

    #@28
    .line 872
    .local v1, next:C
    if-ne v1, v6, :cond_34

    #@2a
    .line 873
    add-int/lit8 v4, v0, 0x1

    #@2c
    add-int/lit8 v5, v0, 0x2

    #@2e
    invoke-virtual {v2, v4, v5}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    #@31
    .line 874
    add-int/lit8 v0, v0, 0x1

    #@33
    .line 875
    goto :goto_16

    #@34
    .line 876
    :cond_34
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    #@37
    move-result v4

    #@38
    if-eqz v4, :cond_a0

    #@3a
    .line 877
    invoke-static {v1}, Ljava/lang/Character;->getNumericValue(C)I

    #@3d
    move-result v4

    #@3e
    add-int/lit8 v3, v4, -0x1

    #@40
    .line 878
    .local v3, which:I
    if-gez v3, :cond_60

    #@42
    .line 879
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@44
    new-instance v5, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string/jumbo v6, "template requests value ^"

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    add-int/lit8 v6, v3, 0x1

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5d
    throw v4

    #@5e
    .line 894
    .end local v1           #next:C
    .end local v3           #which:I
    :catch_5e
    move-exception v4

    #@5f
    .line 897
    :cond_5f
    return-object v2

    #@60
    .line 882
    .restart local v1       #next:C
    .restart local v3       #which:I
    :cond_60
    array-length v4, p1

    #@61
    if-lt v3, v4, :cond_90

    #@63
    .line 883
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@65
    new-instance v5, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string/jumbo v6, "template requests value ^"

    #@6d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    add-int/lit8 v6, v3, 0x1

    #@73
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    const-string v6, "; only "

    #@79
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    array-length v6, p1

    #@7e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v5

    #@82
    const-string v6, " provided"

    #@84
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v5

    #@88
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@8f
    throw v4

    #@90
    .line 887
    :cond_90
    add-int/lit8 v4, v0, 0x2

    #@92
    aget-object v5, p1, v3

    #@94
    invoke-virtual {v2, v0, v4, v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@97
    .line 888
    aget-object v4, p1, v3

    #@99
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I
    :try_end_9c
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_16 .. :try_end_9c} :catch_5e

    #@9c
    move-result v4

    #@9d
    add-int/2addr v0, v4

    #@9e
    .line 889
    goto/16 :goto_16

    #@a0
    .line 892
    .end local v1           #next:C
    .end local v3           #which:I
    :cond_a0
    add-int/lit8 v0, v0, 0x1

    #@a2
    goto/16 :goto_16
.end method

.method public static getCapsMode(Ljava/lang/CharSequence;II)I
    .registers 13
    .parameter "cs"
    .parameter "off"
    .parameter "reqModes"

    #@0
    .prologue
    const/16 v9, 0x27

    #@2
    const/16 v8, 0x22

    #@4
    const/16 v7, 0x2e

    #@6
    .line 1562
    if-gez p1, :cond_a

    #@8
    .line 1563
    const/4 v4, 0x0

    #@9
    .line 1647
    :cond_9
    :goto_9
    return v4

    #@a
    .line 1568
    :cond_a
    const/4 v4, 0x0

    #@b
    .line 1570
    .local v4, mode:I
    and-int/lit16 v5, p2, 0x1000

    #@d
    if-eqz v5, :cond_11

    #@f
    .line 1571
    or-int/lit16 v4, v4, 0x1000

    #@11
    .line 1573
    :cond_11
    and-int/lit16 v5, p2, 0x6000

    #@13
    if-eqz v5, :cond_9

    #@15
    .line 1579
    move v1, p1

    #@16
    .local v1, i:I
    :goto_16
    if-lez v1, :cond_2a

    #@18
    .line 1580
    add-int/lit8 v5, v1, -0x1

    #@1a
    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    #@1d
    move-result v0

    #@1e
    .line 1582
    .local v0, c:C
    if-eq v0, v8, :cond_3e

    #@20
    if-eq v0, v9, :cond_3e

    #@22
    invoke-static {v0}, Ljava/lang/Character;->getType(C)I

    #@25
    move-result v5

    #@26
    const/16 v6, 0x15

    #@28
    if-eq v5, v6, :cond_3e

    #@2a
    .line 1590
    .end local v0           #c:C
    :cond_2a
    move v2, v1

    #@2b
    .line 1591
    .local v2, j:I
    :goto_2b
    if-lez v2, :cond_41

    #@2d
    add-int/lit8 v5, v2, -0x1

    #@2f
    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    #@32
    move-result v0

    #@33
    .restart local v0       #c:C
    const/16 v5, 0x20

    #@35
    if-eq v0, v5, :cond_3b

    #@37
    const/16 v5, 0x9

    #@39
    if-ne v0, v5, :cond_41

    #@3b
    .line 1592
    :cond_3b
    add-int/lit8 v2, v2, -0x1

    #@3d
    goto :goto_2b

    #@3e
    .line 1579
    .end local v2           #j:I
    :cond_3e
    add-int/lit8 v1, v1, -0x1

    #@40
    goto :goto_16

    #@41
    .line 1594
    .end local v0           #c:C
    .restart local v2       #j:I
    :cond_41
    if-eqz v2, :cond_4d

    #@43
    add-int/lit8 v5, v2, -0x1

    #@45
    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    #@48
    move-result v5

    #@49
    const/16 v6, 0xa

    #@4b
    if-ne v5, v6, :cond_50

    #@4d
    .line 1595
    :cond_4d
    or-int/lit16 v4, v4, 0x2000

    #@4f
    goto :goto_9

    #@50
    .line 1600
    :cond_50
    and-int/lit16 v5, p2, 0x4000

    #@52
    if-nez v5, :cond_59

    #@54
    .line 1601
    if-eq v1, v2, :cond_9

    #@56
    or-int/lit16 v4, v4, 0x2000

    #@58
    goto :goto_9

    #@59
    .line 1607
    :cond_59
    if-eq v1, v2, :cond_9

    #@5b
    .line 1613
    :goto_5b
    if-lez v2, :cond_6f

    #@5d
    .line 1614
    add-int/lit8 v5, v2, -0x1

    #@5f
    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    #@62
    move-result v0

    #@63
    .line 1616
    .restart local v0       #c:C
    if-eq v0, v8, :cond_97

    #@65
    if-eq v0, v9, :cond_97

    #@67
    invoke-static {v0}, Ljava/lang/Character;->getType(C)I

    #@6a
    move-result v5

    #@6b
    const/16 v6, 0x16

    #@6d
    if-eq v5, v6, :cond_97

    #@6f
    .line 1622
    .end local v0           #c:C
    :cond_6f
    if-lez v2, :cond_9

    #@71
    .line 1623
    add-int/lit8 v5, v2, -0x1

    #@73
    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    #@76
    move-result v0

    #@77
    .line 1625
    .restart local v0       #c:C
    if-eq v0, v7, :cond_81

    #@79
    const/16 v5, 0x3f

    #@7b
    if-eq v0, v5, :cond_81

    #@7d
    const/16 v5, 0x21

    #@7f
    if-ne v0, v5, :cond_9

    #@81
    .line 1629
    :cond_81
    if-ne v0, v7, :cond_93

    #@83
    .line 1630
    add-int/lit8 v3, v2, -0x2

    #@85
    .local v3, k:I
    :goto_85
    if-ltz v3, :cond_93

    #@87
    .line 1631
    invoke-interface {p0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    #@8a
    move-result v0

    #@8b
    .line 1633
    if-eq v0, v7, :cond_9

    #@8d
    .line 1637
    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    #@90
    move-result v5

    #@91
    if-nez v5, :cond_9a

    #@93
    .line 1643
    .end local v3           #k:I
    :cond_93
    or-int/lit16 v4, v4, 0x4000

    #@95
    goto/16 :goto_9

    #@97
    .line 1613
    :cond_97
    add-int/lit8 v2, v2, -0x1

    #@99
    goto :goto_5b

    #@9a
    .line 1630
    .restart local v3       #k:I
    :cond_9a
    add-int/lit8 v3, v3, -0x1

    #@9c
    goto :goto_85
.end method

.method public static getChars(Ljava/lang/CharSequence;II[CI)V
    .registers 9
    .parameter "s"
    .parameter "start"
    .parameter "end"
    .parameter "dest"
    .parameter "destoff"

    #@0
    .prologue
    .line 69
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    .line 71
    .local v0, c:Ljava/lang/Class;,"Ljava/lang/Class<+Ljava/lang/CharSequence;>;"
    const-class v3, Ljava/lang/String;

    #@6
    if-ne v0, v3, :cond_e

    #@8
    .line 72
    check-cast p0, Ljava/lang/String;

    #@a
    .end local p0
    invoke-virtual {p0, p1, p2, p3, p4}, Ljava/lang/String;->getChars(II[CI)V

    #@d
    .line 83
    :goto_d
    return-void

    #@e
    .line 73
    .restart local p0
    :cond_e
    const-class v3, Ljava/lang/StringBuffer;

    #@10
    if-ne v0, v3, :cond_18

    #@12
    .line 74
    check-cast p0, Ljava/lang/StringBuffer;

    #@14
    .end local p0
    invoke-virtual {p0, p1, p2, p3, p4}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    #@17
    goto :goto_d

    #@18
    .line 75
    .restart local p0
    :cond_18
    const-class v3, Ljava/lang/StringBuilder;

    #@1a
    if-ne v0, v3, :cond_22

    #@1c
    .line 76
    check-cast p0, Ljava/lang/StringBuilder;

    #@1e
    .end local p0
    invoke-virtual {p0, p1, p2, p3, p4}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    #@21
    goto :goto_d

    #@22
    .line 77
    .restart local p0
    :cond_22
    instance-of v3, p0, Landroid/text/GetChars;

    #@24
    if-eqz v3, :cond_2c

    #@26
    .line 78
    check-cast p0, Landroid/text/GetChars;

    #@28
    .end local p0
    invoke-interface {p0, p1, p2, p3, p4}, Landroid/text/GetChars;->getChars(II[CI)V

    #@2b
    goto :goto_d

    #@2c
    .line 80
    .restart local p0
    :cond_2c
    move v2, p1

    #@2d
    .local v2, i:I
    move v1, p4

    #@2e
    .end local p4
    .local v1, destoff:I
    :goto_2e
    if-ge v2, p2, :cond_3c

    #@30
    .line 81
    add-int/lit8 p4, v1, 0x1

    #@32
    .end local v1           #destoff:I
    .restart local p4
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@35
    move-result v3

    #@36
    aput-char v3, p3, v1

    #@38
    .line 80
    add-int/lit8 v2, v2, 0x1

    #@3a
    move v1, p4

    #@3b
    .end local p4
    .restart local v1       #destoff:I
    goto :goto_2e

    #@3c
    :cond_3c
    move p4, v1

    #@3d
    .end local v1           #destoff:I
    .restart local p4
    goto :goto_d
.end method

.method private static getLayoutDirectionFromFirstChar(Ljava/util/Locale;)I
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1798
    invoke-virtual {p0, p0}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    #@8
    move-result v1

    #@9
    invoke-static {v1}, Ljava/lang/Character;->getDirectionality(C)B

    #@c
    move-result v1

    #@d
    packed-switch v1, :pswitch_data_14

    #@10
    .line 1805
    :goto_10
    return v0

    #@11
    .line 1801
    :pswitch_11
    const/4 v0, 0x1

    #@12
    goto :goto_10

    #@13
    .line 1798
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_11
        :pswitch_11
    .end packed-switch
.end method

.method public static getLayoutDirectionFromLocale(Ljava/util/Locale;)I
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 1770
    if-eqz p0, :cond_2f

    #@2
    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    #@4
    invoke-virtual {p0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_2f

    #@a
    .line 1771
    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-static {v1}, Llibcore/icu/ICU;->addLikelySubtags(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    invoke-static {v1}, Llibcore/icu/ICU;->getScript(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 1772
    .local v0, scriptSubtag:Ljava/lang/String;
    if-nez v0, :cond_1d

    #@18
    invoke-static {p0}, Landroid/text/TextUtils;->getLayoutDirectionFromFirstChar(Ljava/util/Locale;)I

    #@1b
    move-result v1

    #@1c
    .line 1780
    .end local v0           #scriptSubtag:Ljava/lang/String;
    :goto_1c
    return v1

    #@1d
    .line 1774
    .restart local v0       #scriptSubtag:Ljava/lang/String;
    :cond_1d
    sget-object v1, Landroid/text/TextUtils;->ARAB_SCRIPT_SUBTAG:Ljava/lang/String;

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@22
    move-result v1

    #@23
    if-nez v1, :cond_2d

    #@25
    sget-object v1, Landroid/text/TextUtils;->HEBR_SCRIPT_SUBTAG:Ljava/lang/String;

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_2f

    #@2d
    .line 1776
    :cond_2d
    const/4 v1, 0x1

    #@2e
    goto :goto_1c

    #@2f
    .line 1780
    .end local v0           #scriptSubtag:Ljava/lang/String;
    :cond_2f
    const/4 v1, 0x0

    #@30
    goto :goto_1c
.end method

.method public static getOffsetAfter(Ljava/lang/CharSequence;I)I
    .registers 11
    .parameter "text"
    .parameter "offset"

    #@0
    .prologue
    .line 962
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@3
    move-result v4

    #@4
    .line 964
    .local v4, len:I
    if-ne p1, v4, :cond_7

    #@6
    .line 1007
    .end local v4           #len:I
    :cond_6
    :goto_6
    return v4

    #@7
    .line 966
    .restart local v4       #len:I
    :cond_7
    add-int/lit8 v7, v4, -0x1

    #@9
    if-eq p1, v7, :cond_6

    #@b
    .line 969
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    #@e
    move-result v0

    #@f
    .line 971
    .local v0, c:C
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@11
    if-eqz v7, :cond_48

    #@13
    invoke-static {p0, p1}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@16
    move-result v7

    #@17
    if-eqz v7, :cond_48

    #@19
    .line 972
    add-int/lit8 p1, p1, 0x4

    #@1b
    .line 994
    :goto_1b
    instance-of v7, p0, Landroid/text/Spanned;

    #@1d
    if-eqz v7, :cond_a7

    #@1f
    move-object v7, p0

    #@20
    .line 995
    check-cast v7, Landroid/text/Spanned;

    #@22
    const-class v8, Landroid/text/style/ReplacementSpan;

    #@24
    invoke-interface {v7, p1, p1, v8}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@27
    move-result-object v5

    #@28
    check-cast v5, [Landroid/text/style/ReplacementSpan;

    #@2a
    .line 998
    .local v5, spans:[Landroid/text/style/ReplacementSpan;
    const/4 v3, 0x0

    #@2b
    .local v3, i:I
    :goto_2b
    array-length v7, v5

    #@2c
    if-ge v3, v7, :cond_a7

    #@2e
    move-object v7, p0

    #@2f
    .line 999
    check-cast v7, Landroid/text/Spanned;

    #@31
    aget-object v8, v5, v3

    #@33
    invoke-interface {v7, v8}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@36
    move-result v6

    #@37
    .local v6, start:I
    move-object v7, p0

    #@38
    .line 1000
    check-cast v7, Landroid/text/Spanned;

    #@3a
    aget-object v8, v5, v3

    #@3c
    invoke-interface {v7, v8}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@3f
    move-result v2

    #@40
    .line 1002
    .local v2, end:I
    if-ge v6, p1, :cond_45

    #@42
    if-le v2, p1, :cond_45

    #@44
    .line 1003
    move p1, v2

    #@45
    .line 998
    :cond_45
    add-int/lit8 v3, v3, 0x1

    #@47
    goto :goto_2b

    #@48
    .line 973
    .end local v2           #end:I
    .end local v3           #i:I
    .end local v5           #spans:[Landroid/text/style/ReplacementSpan;
    .end local v6           #start:I
    :cond_48
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@4a
    if-eqz v7, :cond_57

    #@4c
    add-int/lit8 v7, p1, -0x1

    #@4e
    invoke-static {p0, v7}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@51
    move-result v7

    #@52
    if-eqz v7, :cond_57

    #@54
    .line 974
    add-int/lit8 p1, p1, 0x3

    #@56
    goto :goto_1b

    #@57
    .line 975
    :cond_57
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@59
    if-eqz v7, :cond_66

    #@5b
    add-int/lit8 v7, p1, -0x2

    #@5d
    invoke-static {p0, v7}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@60
    move-result v7

    #@61
    if-eqz v7, :cond_66

    #@63
    .line 976
    add-int/lit8 p1, p1, 0x2

    #@65
    goto :goto_1b

    #@66
    .line 977
    :cond_66
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@68
    if-eqz v7, :cond_75

    #@6a
    add-int/lit8 v7, p1, -0x3

    #@6c
    invoke-static {p0, v7}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@6f
    move-result v7

    #@70
    if-eqz v7, :cond_75

    #@72
    .line 978
    add-int/lit8 p1, p1, 0x1

    #@74
    goto :goto_1b

    #@75
    .line 980
    :cond_75
    const v7, 0xd800

    #@78
    if-lt v0, v7, :cond_95

    #@7a
    const v7, 0xdbff

    #@7d
    if-gt v0, v7, :cond_95

    #@7f
    .line 981
    add-int/lit8 v7, p1, 0x1

    #@81
    invoke-interface {p0, v7}, Ljava/lang/CharSequence;->charAt(I)C

    #@84
    move-result v1

    #@85
    .line 983
    .local v1, c1:C
    const v7, 0xdc00

    #@88
    if-lt v1, v7, :cond_92

    #@8a
    const v7, 0xdfff

    #@8d
    if-gt v1, v7, :cond_92

    #@8f
    .line 984
    add-int/lit8 p1, p1, 0x2

    #@91
    goto :goto_1b

    #@92
    .line 986
    :cond_92
    add-int/lit8 p1, p1, 0x1

    #@94
    goto :goto_1b

    #@95
    .line 988
    .end local v1           #c1:C
    :cond_95
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@97
    if-eqz v7, :cond_a3

    #@99
    invoke-static {p0, p1}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@9c
    move-result v7

    #@9d
    if-eqz v7, :cond_a3

    #@9f
    .line 989
    add-int/lit8 p1, p1, 0x2

    #@a1
    goto/16 :goto_1b

    #@a3
    .line 991
    :cond_a3
    add-int/lit8 p1, p1, 0x1

    #@a5
    goto/16 :goto_1b

    #@a7
    :cond_a7
    move v4, p1

    #@a8
    .line 1007
    goto/16 :goto_6
.end method

.method public static getOffsetBefore(Ljava/lang/CharSequence;I)I
    .registers 14
    .parameter "text"
    .parameter "offset"

    #@0
    .prologue
    const v11, 0xdfff

    #@3
    const v10, 0xdc00

    #@6
    const v9, 0xdbff

    #@9
    const v8, 0xd800

    #@c
    const/4 v6, 0x0

    #@d
    .line 901
    if-nez p1, :cond_10

    #@f
    .line 958
    :cond_f
    :goto_f
    return v6

    #@10
    .line 903
    :cond_10
    const/4 v7, 0x1

    #@11
    if-eq p1, v7, :cond_f

    #@13
    .line 906
    add-int/lit8 v6, p1, -0x1

    #@15
    invoke-interface {p0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    #@18
    move-result v0

    #@19
    .line 908
    .local v0, c:C
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@1b
    if-eqz v6, :cond_97

    #@1d
    .line 909
    add-int/lit8 v6, p1, -0x1

    #@1f
    invoke-static {p0, v6}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@22
    move-result v6

    #@23
    if-eqz v6, :cond_54

    #@25
    .line 910
    add-int/lit8 p1, p1, -0x1

    #@27
    .line 945
    :goto_27
    instance-of v6, p0, Landroid/text/Spanned;

    #@29
    if-eqz v6, :cond_b0

    #@2b
    move-object v6, p0

    #@2c
    .line 946
    check-cast v6, Landroid/text/Spanned;

    #@2e
    const-class v7, Landroid/text/style/ReplacementSpan;

    #@30
    invoke-interface {v6, p1, p1, v7}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@33
    move-result-object v4

    #@34
    check-cast v4, [Landroid/text/style/ReplacementSpan;

    #@36
    .line 949
    .local v4, spans:[Landroid/text/style/ReplacementSpan;
    const/4 v3, 0x0

    #@37
    .local v3, i:I
    :goto_37
    array-length v6, v4

    #@38
    if-ge v3, v6, :cond_b0

    #@3a
    move-object v6, p0

    #@3b
    .line 950
    check-cast v6, Landroid/text/Spanned;

    #@3d
    aget-object v7, v4, v3

    #@3f
    invoke-interface {v6, v7}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@42
    move-result v5

    #@43
    .local v5, start:I
    move-object v6, p0

    #@44
    .line 951
    check-cast v6, Landroid/text/Spanned;

    #@46
    aget-object v7, v4, v3

    #@48
    invoke-interface {v6, v7}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@4b
    move-result v2

    #@4c
    .line 953
    .local v2, end:I
    if-ge v5, p1, :cond_51

    #@4e
    if-le v2, p1, :cond_51

    #@50
    .line 954
    move p1, v5

    #@51
    .line 949
    :cond_51
    add-int/lit8 v3, v3, 0x1

    #@53
    goto :goto_37

    #@54
    .line 911
    .end local v2           #end:I
    .end local v3           #i:I
    .end local v4           #spans:[Landroid/text/style/ReplacementSpan;
    .end local v5           #start:I
    :cond_54
    add-int/lit8 v6, p1, -0x2

    #@56
    invoke-static {p0, v6}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@59
    move-result v6

    #@5a
    if-eqz v6, :cond_5f

    #@5c
    .line 912
    add-int/lit8 p1, p1, -0x2

    #@5e
    goto :goto_27

    #@5f
    .line 913
    :cond_5f
    add-int/lit8 v6, p1, -0x3

    #@61
    invoke-static {p0, v6}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@64
    move-result v6

    #@65
    if-eqz v6, :cond_6a

    #@67
    .line 914
    add-int/lit8 p1, p1, -0x3

    #@69
    goto :goto_27

    #@6a
    .line 915
    :cond_6a
    add-int/lit8 v6, p1, -0x4

    #@6c
    invoke-static {p0, v6}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@6f
    move-result v6

    #@70
    if-eqz v6, :cond_75

    #@72
    .line 916
    add-int/lit8 p1, p1, -0x4

    #@74
    goto :goto_27

    #@75
    .line 918
    :cond_75
    if-lt v0, v10, :cond_89

    #@77
    if-gt v0, v11, :cond_89

    #@79
    .line 919
    add-int/lit8 v6, p1, -0x2

    #@7b
    invoke-interface {p0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    #@7e
    move-result v1

    #@7f
    .line 921
    .local v1, c1:C
    if-lt v1, v8, :cond_86

    #@81
    if-gt v1, v9, :cond_86

    #@83
    .line 922
    add-int/lit8 p1, p1, -0x2

    #@85
    goto :goto_27

    #@86
    .line 924
    :cond_86
    add-int/lit8 p1, p1, -0x1

    #@88
    goto :goto_27

    #@89
    .line 926
    .end local v1           #c1:C
    :cond_89
    add-int/lit8 v6, p1, -0x2

    #@8b
    invoke-static {p0, v6}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@8e
    move-result v6

    #@8f
    if-eqz v6, :cond_94

    #@91
    .line 927
    add-int/lit8 p1, p1, -0x2

    #@93
    goto :goto_27

    #@94
    .line 929
    :cond_94
    add-int/lit8 p1, p1, -0x1

    #@96
    goto :goto_27

    #@97
    .line 933
    :cond_97
    if-lt v0, v10, :cond_ac

    #@99
    if-gt v0, v11, :cond_ac

    #@9b
    .line 934
    add-int/lit8 v6, p1, -0x2

    #@9d
    invoke-interface {p0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    #@a0
    move-result v1

    #@a1
    .line 936
    .restart local v1       #c1:C
    if-lt v1, v8, :cond_a8

    #@a3
    if-gt v1, v9, :cond_a8

    #@a5
    .line 937
    add-int/lit8 p1, p1, -0x2

    #@a7
    goto :goto_27

    #@a8
    .line 939
    :cond_a8
    add-int/lit8 p1, p1, -0x1

    #@aa
    goto/16 :goto_27

    #@ac
    .line 941
    .end local v1           #c1:C
    :cond_ac
    add-int/lit8 p1, p1, -0x1

    #@ae
    goto/16 :goto_27

    #@b0
    :cond_b0
    move v6, p1

    #@b1
    .line 958
    goto/16 :goto_f
.end method

.method public static getReverse(Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;
    .registers 4
    .parameter "source"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 502
    new-instance v0, Landroid/text/TextUtils$Reverser;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/text/TextUtils$Reverser;-><init>(Ljava/lang/CharSequence;II)V

    #@5
    return-object v0
.end method

.method public static getTrimmedLength(Ljava/lang/CharSequence;)I
    .registers 6
    .parameter "s"

    #@0
    .prologue
    const/16 v4, 0x20

    #@2
    .line 460
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@5
    move-result v1

    #@6
    .line 462
    .local v1, len:I
    const/4 v2, 0x0

    #@7
    .line 463
    .local v2, start:I
    :goto_7
    if-ge v2, v1, :cond_12

    #@9
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@c
    move-result v3

    #@d
    if-gt v3, v4, :cond_12

    #@f
    .line 464
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_7

    #@12
    .line 467
    :cond_12
    move v0, v1

    #@13
    .line 468
    .local v0, end:I
    :goto_13
    if-le v0, v2, :cond_20

    #@15
    add-int/lit8 v3, v0, -0x1

    #@17
    invoke-interface {p0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    #@1a
    move-result v3

    #@1b
    if-gt v3, v4, :cond_20

    #@1d
    .line 469
    add-int/lit8 v0, v0, -0x1

    #@1f
    goto :goto_13

    #@20
    .line 472
    :cond_20
    sub-int v3, v0, v2

    #@22
    return v3
.end method

.method public static htmlEncode(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 1370
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1372
    .local v2, sb:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@9
    move-result v3

    #@a
    if-ge v1, v3, :cond_37

    #@c
    .line 1373
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v0

    #@10
    .line 1374
    .local v0, c:C
    sparse-switch v0, :sswitch_data_3c

    #@13
    .line 1395
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    .line 1372
    :goto_16
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_6

    #@19
    .line 1376
    :sswitch_19
    const-string v3, "&lt;"

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    goto :goto_16

    #@1f
    .line 1379
    :sswitch_1f
    const-string v3, "&gt;"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    goto :goto_16

    #@25
    .line 1382
    :sswitch_25
    const-string v3, "&amp;"

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    goto :goto_16

    #@2b
    .line 1389
    :sswitch_2b
    const-string v3, "&#39;"

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    goto :goto_16

    #@31
    .line 1392
    :sswitch_31
    const-string v3, "&quot;"

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    goto :goto_16

    #@37
    .line 1398
    .end local v0           #c:C
    :cond_37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    return-object v3

    #@3c
    .line 1374
    :sswitch_data_3c
    .sparse-switch
        0x22 -> :sswitch_31
        0x26 -> :sswitch_25
        0x27 -> :sswitch_2b
        0x3c -> :sswitch_19
        0x3e -> :sswitch_1f
    .end sparse-switch
.end method

.method public static indexOf(Ljava/lang/CharSequence;C)I
    .registers 3
    .parameter "s"
    .parameter "ch"

    #@0
    .prologue
    .line 86
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CI)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static indexOf(Ljava/lang/CharSequence;CI)I
    .registers 5
    .parameter "s"
    .parameter "ch"
    .parameter "start"

    #@0
    .prologue
    .line 90
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    .line 92
    .local v0, c:Ljava/lang/Class;,"Ljava/lang/Class<+Ljava/lang/CharSequence;>;"
    const-class v1, Ljava/lang/String;

    #@6
    if-ne v0, v1, :cond_f

    #@8
    .line 93
    check-cast p0, Ljava/lang/String;

    #@a
    .end local p0
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->indexOf(II)I

    #@d
    move-result v1

    #@e
    .line 95
    :goto_e
    return v1

    #@f
    .restart local p0
    :cond_f
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@12
    move-result v1

    #@13
    invoke-static {p0, p1, p2, v1}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CII)I

    #@16
    move-result v1

    #@17
    goto :goto_e
.end method

.method public static indexOf(Ljava/lang/CharSequence;CII)I
    .registers 12
    .parameter "s"
    .parameter "ch"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 99
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@4
    move-result-object v1

    #@5
    .line 101
    .local v1, c:Ljava/lang/Class;,"Ljava/lang/Class<+Ljava/lang/CharSequence;>;"
    instance-of v7, p0, Landroid/text/GetChars;

    #@7
    if-nez v7, :cond_15

    #@9
    const-class v7, Ljava/lang/StringBuffer;

    #@b
    if-eq v1, v7, :cond_15

    #@d
    const-class v7, Ljava/lang/StringBuilder;

    #@f
    if-eq v1, v7, :cond_15

    #@11
    const-class v7, Ljava/lang/String;

    #@13
    if-ne v1, v7, :cond_40

    #@15
    .line 103
    :cond_15
    const/16 v0, 0x1f4

    #@17
    .line 104
    .local v0, INDEX_INCREMENT:I
    const/16 v7, 0x1f4

    #@19
    invoke-static {v7}, Landroid/text/TextUtils;->obtain(I)[C

    #@1c
    move-result-object v5

    #@1d
    .line 106
    .local v5, temp:[C
    :goto_1d
    if-ge p2, p3, :cond_3b

    #@1f
    .line 107
    add-int/lit16 v4, p2, 0x1f4

    #@21
    .line 108
    .local v4, segend:I
    if-le v4, p3, :cond_24

    #@23
    .line 109
    move v4, p3

    #@24
    .line 111
    :cond_24
    const/4 v7, 0x0

    #@25
    invoke-static {p0, p2, v4, v5, v7}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@28
    .line 113
    sub-int v2, v4, p2

    #@2a
    .line 114
    .local v2, count:I
    const/4 v3, 0x0

    #@2b
    .local v3, i:I
    :goto_2b
    if-ge v3, v2, :cond_39

    #@2d
    .line 115
    aget-char v7, v5, v3

    #@2f
    if-ne v7, p1, :cond_36

    #@31
    .line 116
    invoke-static {v5}, Landroid/text/TextUtils;->recycle([C)V

    #@34
    .line 117
    add-int/2addr v3, p2

    #@35
    .line 132
    .end local v0           #INDEX_INCREMENT:I
    .end local v2           #count:I
    .end local v3           #i:I
    .end local v4           #segend:I
    .end local v5           #temp:[C
    :cond_35
    :goto_35
    return v3

    #@36
    .line 114
    .restart local v0       #INDEX_INCREMENT:I
    .restart local v2       #count:I
    .restart local v3       #i:I
    .restart local v4       #segend:I
    .restart local v5       #temp:[C
    :cond_36
    add-int/lit8 v3, v3, 0x1

    #@38
    goto :goto_2b

    #@39
    .line 121
    :cond_39
    move p2, v4

    #@3a
    .line 122
    goto :goto_1d

    #@3b
    .line 124
    .end local v2           #count:I
    .end local v3           #i:I
    .end local v4           #segend:I
    :cond_3b
    invoke-static {v5}, Landroid/text/TextUtils;->recycle([C)V

    #@3e
    move v3, v6

    #@3f
    .line 125
    goto :goto_35

    #@40
    .line 128
    .end local v0           #INDEX_INCREMENT:I
    .end local v5           #temp:[C
    :cond_40
    move v3, p2

    #@41
    .restart local v3       #i:I
    :goto_41
    if-ge v3, p3, :cond_4c

    #@43
    .line 129
    invoke-interface {p0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    #@46
    move-result v7

    #@47
    if-eq v7, p1, :cond_35

    #@49
    .line 128
    add-int/lit8 v3, v3, 0x1

    #@4b
    goto :goto_41

    #@4c
    :cond_4c
    move v3, v6

    #@4d
    .line 132
    goto :goto_35
.end method

.method public static indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I
    .registers 4
    .parameter "s"
    .parameter "needle"

    #@0
    .prologue
    .line 194
    const/4 v0, 0x0

    #@1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v1

    #@5
    invoke-static {p0, p1, v0, v1}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public static indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)I
    .registers 4
    .parameter "s"
    .parameter "needle"
    .parameter "start"

    #@0
    .prologue
    .line 198
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)I
    .registers 9
    .parameter "s"
    .parameter "needle"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 203
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@5
    move-result v1

    #@6
    .line 204
    .local v1, nlen:I
    if-nez v1, :cond_a

    #@8
    move v2, p2

    #@9
    .line 225
    :cond_9
    :goto_9
    return v2

    #@a
    .line 207
    :cond_a
    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@d
    move-result v0

    #@e
    .line 210
    .local v0, c:C
    :goto_e
    invoke-static {p0, v0, p2}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CI)I

    #@11
    move-result p2

    #@12
    .line 211
    sub-int v3, p3, v1

    #@14
    if-gt p2, v3, :cond_9

    #@16
    .line 215
    if-ltz p2, :cond_9

    #@18
    .line 219
    invoke-static {p0, p2, p1, v4, v1}, Landroid/text/TextUtils;->regionMatches(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_20

    #@1e
    move v2, p2

    #@1f
    .line 220
    goto :goto_9

    #@20
    .line 223
    :cond_20
    add-int/lit8 p2, p2, 0x1

    #@22
    goto :goto_e
.end method

.method public static isDigitsOnly(Ljava/lang/CharSequence;)Z
    .registers 4
    .parameter "str"

    #@0
    .prologue
    .line 1489
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@3
    move-result v1

    #@4
    .line 1490
    .local v1, len:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v1, :cond_16

    #@7
    .line 1491
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@a
    move-result v2

    #@b
    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    #@e
    move-result v2

    #@f
    if-nez v2, :cond_13

    #@11
    .line 1492
    const/4 v2, 0x0

    #@12
    .line 1495
    :goto_12
    return v2

    #@13
    .line 1490
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_5

    #@16
    .line 1495
    :cond_16
    const/4 v2, 0x1

    #@17
    goto :goto_12
.end method

.method public static isEmpty(Ljava/lang/CharSequence;)Z
    .registers 2
    .parameter "str"

    #@0
    .prologue
    .line 448
    if-eqz p0, :cond_8

    #@2
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    .line 449
    :cond_8
    const/4 v0, 0x1

    #@9
    .line 451
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static isGraphic(C)Z
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 1475
    invoke-static {p0}, Ljava/lang/Character;->getType(C)I

    #@3
    move-result v0

    #@4
    .line 1476
    .local v0, gc:I
    const/16 v1, 0xf

    #@6
    if-eq v0, v1, :cond_20

    #@8
    const/16 v1, 0x10

    #@a
    if-eq v0, v1, :cond_20

    #@c
    const/16 v1, 0x13

    #@e
    if-eq v0, v1, :cond_20

    #@10
    if-eqz v0, :cond_20

    #@12
    const/16 v1, 0xd

    #@14
    if-eq v0, v1, :cond_20

    #@16
    const/16 v1, 0xe

    #@18
    if-eq v0, v1, :cond_20

    #@1a
    const/16 v1, 0xc

    #@1c
    if-eq v0, v1, :cond_20

    #@1e
    const/4 v1, 0x1

    #@1f
    :goto_1f
    return v1

    #@20
    :cond_20
    const/4 v1, 0x0

    #@21
    goto :goto_1f
.end method

.method public static isGraphic(Ljava/lang/CharSequence;)Z
    .registers 6
    .parameter "str"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1450
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v2

    #@5
    .line 1452
    .local v2, len:I
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@7
    if-eqz v4, :cond_10

    #@9
    invoke-static {p0}, Landroid/text/Layout;->hasEmoji(Ljava/lang/CharSequence;)Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_10

    #@f
    .line 1468
    :cond_f
    :goto_f
    return v3

    #@10
    .line 1456
    :cond_10
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v2, :cond_38

    #@13
    .line 1457
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    #@16
    move-result v4

    #@17
    invoke-static {v4}, Ljava/lang/Character;->getType(C)I

    #@1a
    move-result v0

    #@1b
    .line 1458
    .local v0, gc:I
    const/16 v4, 0xf

    #@1d
    if-eq v0, v4, :cond_35

    #@1f
    const/16 v4, 0x10

    #@21
    if-eq v0, v4, :cond_35

    #@23
    const/16 v4, 0x13

    #@25
    if-eq v0, v4, :cond_35

    #@27
    if-eqz v0, :cond_35

    #@29
    const/16 v4, 0xd

    #@2b
    if-eq v0, v4, :cond_35

    #@2d
    const/16 v4, 0xe

    #@2f
    if-eq v0, v4, :cond_35

    #@31
    const/16 v4, 0xc

    #@33
    if-ne v0, v4, :cond_f

    #@35
    .line 1456
    :cond_35
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_11

    #@38
    .line 1468
    .end local v0           #gc:I
    :cond_38
    const/4 v3, 0x0

    #@39
    goto :goto_f
.end method

.method public static isPrintableAscii(C)Z
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 1502
    const/16 v0, 0x20

    #@2
    .line 1503
    .local v0, asciiFirst:I
    const/16 v1, 0x7e

    #@4
    .line 1504
    .local v1, asciiLast:I
    const/16 v2, 0x20

    #@6
    if-gt v2, p0, :cond_c

    #@8
    const/16 v2, 0x7e

    #@a
    if-le p0, v2, :cond_14

    #@c
    :cond_c
    const/16 v2, 0xd

    #@e
    if-eq p0, v2, :cond_14

    #@10
    const/16 v2, 0xa

    #@12
    if-ne p0, v2, :cond_16

    #@14
    :cond_14
    const/4 v2, 0x1

    #@15
    :goto_15
    return v2

    #@16
    :cond_16
    const/4 v2, 0x0

    #@17
    goto :goto_15
.end method

.method public static isPrintableAsciiOnly(Ljava/lang/CharSequence;)Z
    .registers 4
    .parameter "str"

    #@0
    .prologue
    .line 1511
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@3
    move-result v1

    #@4
    .line 1512
    .local v1, len:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v1, :cond_16

    #@7
    .line 1513
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@a
    move-result v2

    #@b
    invoke-static {v2}, Landroid/text/TextUtils;->isPrintableAscii(C)Z

    #@e
    move-result v2

    #@f
    if-nez v2, :cond_13

    #@11
    .line 1514
    const/4 v2, 0x0

    #@12
    .line 1517
    :goto_12
    return v2

    #@13
    .line 1512
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_5

    #@16
    .line 1517
    :cond_16
    const/4 v2, 0x1

    #@17
    goto :goto_12
.end method

.method public static join(Ljava/lang/Iterable;)Ljava/lang/CharSequence;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/CharSequence;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    #@0
    .prologue
    .line 278
    .local p0, list:Ljava/lang/Iterable;,"Ljava/lang/Iterable<Ljava/lang/CharSequence;>;"
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3
    move-result-object v1

    #@4
    const v2, 0x1040531

    #@7
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@a
    move-result-object v0

    #@b
    .line 279
    .local v0, delimiter:Ljava/lang/CharSequence;
    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    return-object v1
.end method

.method public static join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    .registers 7
    .parameter "delimiter"
    .parameter "tokens"

    #@0
    .prologue
    .line 307
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 308
    .local v2, sb:Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    #@6
    .line 309
    .local v0, firstTime:Z
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .local v1, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_1f

    #@10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    .line 310
    .local v3, token:Ljava/lang/Object;
    if-eqz v0, :cond_1b

    #@16
    .line 311
    const/4 v0, 0x0

    #@17
    .line 315
    :goto_17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    goto :goto_a

    #@1b
    .line 313
    :cond_1b
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@1e
    goto :goto_17

    #@1f
    .line 317
    .end local v3           #token:Ljava/lang/Object;
    :cond_1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    return-object v4
.end method

.method public static join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;
    .registers 9
    .parameter "delimiter"
    .parameter "tokens"

    #@0
    .prologue
    .line 288
    new-instance v4, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 289
    .local v4, sb:Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    #@6
    .line 290
    .local v1, firstTime:Z
    move-object v0, p1

    #@7
    .local v0, arr$:[Ljava/lang/Object;
    array-length v3, v0

    #@8
    .local v3, len$:I
    const/4 v2, 0x0

    #@9
    .local v2, i$:I
    :goto_9
    if-ge v2, v3, :cond_1a

    #@b
    aget-object v5, v0, v2

    #@d
    .line 291
    .local v5, token:Ljava/lang/Object;
    if-eqz v1, :cond_16

    #@f
    .line 292
    const/4 v1, 0x0

    #@10
    .line 296
    :goto_10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    .line 290
    add-int/lit8 v2, v2, 0x1

    #@15
    goto :goto_9

    #@16
    .line 294
    :cond_16
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@19
    goto :goto_10

    #@1a
    .line 298
    .end local v5           #token:Ljava/lang/Object;
    :cond_1a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v6

    #@1e
    return-object v6
.end method

.method public static lastIndexOf(Ljava/lang/CharSequence;C)I
    .registers 3
    .parameter "s"
    .parameter "ch"

    #@0
    .prologue
    .line 136
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@3
    move-result v0

    #@4
    add-int/lit8 v0, v0, -0x1

    #@6
    invoke-static {p0, p1, v0}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;CI)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public static lastIndexOf(Ljava/lang/CharSequence;CI)I
    .registers 5
    .parameter "s"
    .parameter "ch"
    .parameter "last"

    #@0
    .prologue
    .line 140
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    .line 142
    .local v0, c:Ljava/lang/Class;,"Ljava/lang/Class<+Ljava/lang/CharSequence;>;"
    const-class v1, Ljava/lang/String;

    #@6
    if-ne v0, v1, :cond_f

    #@8
    .line 143
    check-cast p0, Ljava/lang/String;

    #@a
    .end local p0
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->lastIndexOf(II)I

    #@d
    move-result v1

    #@e
    .line 145
    :goto_e
    return v1

    #@f
    .restart local p0
    :cond_f
    const/4 v1, 0x0

    #@10
    invoke-static {p0, p1, v1, p2}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;CII)I

    #@13
    move-result v1

    #@14
    goto :goto_e
.end method

.method public static lastIndexOf(Ljava/lang/CharSequence;CII)I
    .registers 13
    .parameter "s"
    .parameter "ch"
    .parameter "start"
    .parameter "last"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 150
    if-gez p3, :cond_5

    #@3
    move v4, v7

    #@4
    .line 190
    :cond_4
    :goto_4
    return v4

    #@5
    .line 152
    :cond_5
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@8
    move-result v8

    #@9
    if-lt p3, v8, :cond_11

    #@b
    .line 153
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@e
    move-result v8

    #@f
    add-int/lit8 p3, v8, -0x1

    #@11
    .line 155
    :cond_11
    add-int/lit8 v3, p3, 0x1

    #@13
    .line 157
    .local v3, end:I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@16
    move-result-object v1

    #@17
    .line 159
    .local v1, c:Ljava/lang/Class;,"Ljava/lang/Class<+Ljava/lang/CharSequence;>;"
    instance-of v8, p0, Landroid/text/GetChars;

    #@19
    if-nez v8, :cond_27

    #@1b
    const-class v8, Ljava/lang/StringBuffer;

    #@1d
    if-eq v1, v8, :cond_27

    #@1f
    const-class v8, Ljava/lang/StringBuilder;

    #@21
    if-eq v1, v8, :cond_27

    #@23
    const-class v8, Ljava/lang/String;

    #@25
    if-ne v1, v8, :cond_53

    #@27
    .line 161
    :cond_27
    const/16 v0, 0x1f4

    #@29
    .line 162
    .local v0, INDEX_INCREMENT:I
    const/16 v8, 0x1f4

    #@2b
    invoke-static {v8}, Landroid/text/TextUtils;->obtain(I)[C

    #@2e
    move-result-object v6

    #@2f
    .line 164
    .local v6, temp:[C
    :goto_2f
    if-ge p2, v3, :cond_4e

    #@31
    .line 165
    add-int/lit16 v5, v3, -0x1f4

    #@33
    .line 166
    .local v5, segstart:I
    if-ge v5, p2, :cond_36

    #@35
    .line 167
    move v5, p2

    #@36
    .line 169
    :cond_36
    const/4 v8, 0x0

    #@37
    invoke-static {p0, v5, v3, v6, v8}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@3a
    .line 171
    sub-int v2, v3, v5

    #@3c
    .line 172
    .local v2, count:I
    add-int/lit8 v4, v2, -0x1

    #@3e
    .local v4, i:I
    :goto_3e
    if-ltz v4, :cond_4c

    #@40
    .line 173
    aget-char v8, v6, v4

    #@42
    if-ne v8, p1, :cond_49

    #@44
    .line 174
    invoke-static {v6}, Landroid/text/TextUtils;->recycle([C)V

    #@47
    .line 175
    add-int/2addr v4, v5

    #@48
    goto :goto_4

    #@49
    .line 172
    :cond_49
    add-int/lit8 v4, v4, -0x1

    #@4b
    goto :goto_3e

    #@4c
    .line 179
    :cond_4c
    move v3, v5

    #@4d
    .line 180
    goto :goto_2f

    #@4e
    .line 182
    .end local v2           #count:I
    .end local v4           #i:I
    .end local v5           #segstart:I
    :cond_4e
    invoke-static {v6}, Landroid/text/TextUtils;->recycle([C)V

    #@51
    move v4, v7

    #@52
    .line 183
    goto :goto_4

    #@53
    .line 186
    .end local v0           #INDEX_INCREMENT:I
    .end local v6           #temp:[C
    :cond_53
    add-int/lit8 v4, v3, -0x1

    #@55
    .restart local v4       #i:I
    :goto_55
    if-lt v4, p2, :cond_60

    #@57
    .line 187
    invoke-interface {p0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@5a
    move-result v8

    #@5b
    if-eq v8, p1, :cond_4

    #@5d
    .line 186
    add-int/lit8 v4, v4, -0x1

    #@5f
    goto :goto_55

    #@60
    :cond_60
    move v4, v7

    #@61
    .line 190
    goto :goto_4
.end method

.method static obtain(I)[C
    .registers 4
    .parameter "len"

    #@0
    .prologue
    .line 1344
    sget-object v2, Landroid/text/TextUtils;->sLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1345
    :try_start_3
    sget-object v0, Landroid/text/TextUtils;->sTemp:[C

    #@5
    .line 1346
    .local v0, buf:[C
    const/4 v1, 0x0

    #@6
    sput-object v1, Landroid/text/TextUtils;->sTemp:[C

    #@8
    .line 1347
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_15

    #@9
    .line 1349
    if-eqz v0, :cond_e

    #@b
    array-length v1, v0

    #@c
    if-ge v1, p0, :cond_14

    #@e
    .line 1350
    :cond_e
    invoke-static {p0}, Lcom/android/internal/util/ArrayUtils;->idealCharArraySize(I)I

    #@11
    move-result v1

    #@12
    new-array v0, v1, [C

    #@14
    .line 1352
    :cond_14
    return-object v0

    #@15
    .line 1347
    .end local v0           #buf:[C
    :catchall_15
    move-exception v1

    #@16
    :try_start_16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    #@17
    throw v1
.end method

.method public static packRangeInLong(II)J
    .registers 6
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1736
    int-to-long v0, p0

    #@1
    const/16 v2, 0x20

    #@3
    shl-long/2addr v0, v2

    #@4
    int-to-long v2, p1

    #@5
    or-long/2addr v0, v2

    #@6
    return-wide v0
.end method

.method private static readSpan(Landroid/os/Parcel;Landroid/text/Spannable;Ljava/lang/Object;)V
    .registers 6
    .parameter "p"
    .parameter "sp"
    .parameter "o"

    #@0
    .prologue
    .line 1011
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@b
    move-result v2

    #@c
    invoke-interface {p1, p2, v0, v1, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@f
    .line 1012
    return-void
.end method

.method static recycle([C)V
    .registers 3
    .parameter "temp"

    #@0
    .prologue
    .line 1356
    array-length v0, p0

    #@1
    const/16 v1, 0x3e8

    #@3
    if-le v0, v1, :cond_6

    #@5
    .line 1362
    :goto_5
    return-void

    #@6
    .line 1359
    :cond_6
    sget-object v1, Landroid/text/TextUtils;->sLock:Ljava/lang/Object;

    #@8
    monitor-enter v1

    #@9
    .line 1360
    :try_start_9
    sput-object p0, Landroid/text/TextUtils;->sTemp:[C

    #@b
    .line 1361
    monitor-exit v1

    #@c
    goto :goto_5

    #@d
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_9 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public static regionMatches(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z
    .registers 10
    .parameter "one"
    .parameter "toffset"
    .parameter "two"
    .parameter "ooffset"
    .parameter "len"

    #@0
    .prologue
    .line 231
    mul-int/lit8 v3, p4, 0x2

    #@2
    invoke-static {v3}, Landroid/text/TextUtils;->obtain(I)[C

    #@5
    move-result-object v2

    #@6
    .line 233
    .local v2, temp:[C
    add-int v3, p1, p4

    #@8
    const/4 v4, 0x0

    #@9
    invoke-static {p0, p1, v3, v2, v4}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@c
    .line 234
    add-int v3, p3, p4

    #@e
    invoke-static {p2, p3, v3, v2, p4}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@11
    .line 236
    const/4 v1, 0x1

    #@12
    .line 237
    .local v1, match:Z
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    if-ge v0, p4, :cond_1e

    #@15
    .line 238
    aget-char v3, v2, v0

    #@17
    add-int v4, v0, p4

    #@19
    aget-char v4, v2, v4

    #@1b
    if-eq v3, v4, :cond_22

    #@1d
    .line 239
    const/4 v1, 0x0

    #@1e
    .line 244
    :cond_1e
    invoke-static {v2}, Landroid/text/TextUtils;->recycle([C)V

    #@21
    .line 245
    return v1

    #@22
    .line 237
    :cond_22
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_13
.end method

.method public static removeEmptySpans([Ljava/lang/Object;Landroid/text/Spanned;Ljava/lang/Class;)[Ljava/lang/Object;
    .registers 12
    .parameter
    .parameter "spanned"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Landroid/text/Spanned;",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    #@0
    .prologue
    .local p0, spans:[Ljava/lang/Object;,"[TT;"
    .local p2, klass:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v8, 0x0

    #@1
    .line 1698
    const/4 v0, 0x0

    #@2
    .line 1699
    .local v0, copy:[Ljava/lang/Object;,"[TT;"
    const/4 v1, 0x0

    #@3
    .line 1701
    .local v1, count:I
    const/4 v3, 0x0

    #@4
    .local v3, i:I
    :goto_4
    array-length v7, p0

    #@5
    if-ge v3, v7, :cond_2f

    #@7
    .line 1702
    aget-object v5, p0, v3

    #@9
    .line 1703
    .local v5, span:Ljava/lang/Object;,"TT;"
    invoke-interface {p1, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@c
    move-result v6

    #@d
    .line 1704
    .local v6, start:I
    invoke-interface {p1, v5}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@10
    move-result v2

    #@11
    .line 1706
    .local v2, end:I
    if-ne v6, v2, :cond_28

    #@13
    .line 1707
    if-nez v0, :cond_25

    #@15
    .line 1708
    array-length v7, p0

    #@16
    add-int/lit8 v7, v7, -0x1

    #@18
    invoke-static {p2, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    #@1b
    move-result-object v7

    #@1c
    check-cast v7, [Ljava/lang/Object;

    #@1e
    move-object v0, v7

    #@1f
    check-cast v0, [Ljava/lang/Object;

    #@21
    .line 1709
    invoke-static {p0, v8, v0, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@24
    .line 1710
    move v1, v3

    #@25
    .line 1701
    :cond_25
    :goto_25
    add-int/lit8 v3, v3, 0x1

    #@27
    goto :goto_4

    #@28
    .line 1713
    :cond_28
    if-eqz v0, :cond_25

    #@2a
    .line 1714
    aput-object v5, v0, v1

    #@2c
    .line 1715
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_25

    #@2f
    .line 1720
    .end local v2           #end:I
    .end local v5           #span:Ljava/lang/Object;,"TT;"
    .end local v6           #start:I
    :cond_2f
    if-eqz v0, :cond_3e

    #@31
    .line 1721
    invoke-static {p2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    #@34
    move-result-object v7

    #@35
    check-cast v7, [Ljava/lang/Object;

    #@37
    move-object v4, v7

    #@38
    check-cast v4, [Ljava/lang/Object;

    #@3a
    .line 1722
    .local v4, result:[Ljava/lang/Object;,"[TT;"
    invoke-static {v0, v8, v4, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3d
    .line 1725
    .end local v4           #result:[Ljava/lang/Object;,"[TT;"
    :goto_3d
    return-object v4

    #@3e
    :cond_3e
    move-object v4, p0

    #@3f
    goto :goto_3d
.end method

.method public static replace(Ljava/lang/CharSequence;[Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 11
    .parameter "template"
    .parameter "sources"
    .parameter "destinations"

    #@0
    .prologue
    .line 816
    new-instance v3, Landroid/text/SpannableStringBuilder;

    #@2
    invoke-direct {v3, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@5
    .line 818
    .local v3, tb:Landroid/text/SpannableStringBuilder;
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    array-length v5, p1

    #@7
    if-ge v1, v5, :cond_22

    #@9
    .line 819
    aget-object v5, p1, v1

    #@b
    invoke-static {v3, v5}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    #@e
    move-result v4

    #@f
    .line 821
    .local v4, where:I
    if-ltz v4, :cond_1f

    #@11
    .line 822
    aget-object v5, p1, v1

    #@13
    aget-object v6, p1, v1

    #@15
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@18
    move-result v6

    #@19
    add-int/2addr v6, v4

    #@1a
    const/16 v7, 0x21

    #@1c
    invoke-virtual {v3, v5, v4, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@1f
    .line 818
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_6

    #@22
    .line 826
    .end local v4           #where:I
    :cond_22
    const/4 v1, 0x0

    #@23
    :goto_23
    array-length v5, p1

    #@24
    if-ge v1, v5, :cond_3c

    #@26
    .line 827
    aget-object v5, p1, v1

    #@28
    invoke-virtual {v3, v5}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    #@2b
    move-result v2

    #@2c
    .line 828
    .local v2, start:I
    aget-object v5, p1, v1

    #@2e
    invoke-virtual {v3, v5}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    #@31
    move-result v0

    #@32
    .line 830
    .local v0, end:I
    if-ltz v2, :cond_39

    #@34
    .line 831
    aget-object v5, p2, v1

    #@36
    invoke-virtual {v3, v2, v0, v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@39
    .line 826
    :cond_39
    add-int/lit8 v1, v1, 0x1

    #@3b
    goto :goto_23

    #@3c
    .line 835
    .end local v0           #end:I
    .end local v2           #start:I
    :cond_3c
    return-object v3
.end method

.method private static setPara(Landroid/text/MeasuredText;Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;)F
    .registers 14
    .parameter "mt"
    .parameter "paint"
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "textDir"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1296
    invoke-virtual {p0, p2, p3, p4, p5}, Landroid/text/MeasuredText;->setPara(Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;)V

    #@4
    .line 1299
    instance-of v7, p2, Landroid/text/Spanned;

    #@6
    if-eqz v7, :cond_14

    #@8
    check-cast p2, Landroid/text/Spanned;

    #@a
    .end local p2
    move-object v1, p2

    #@b
    .line 1300
    .local v1, sp:Landroid/text/Spanned;
    :goto_b
    sub-int v0, p4, p3

    #@d
    .line 1301
    .local v0, len:I
    if-nez v1, :cond_16

    #@f
    .line 1302
    invoke-virtual {p0, p1, v0, v6}, Landroid/text/MeasuredText;->addStyleRun(Landroid/text/TextPaint;ILandroid/graphics/Paint$FontMetricsInt;)F

    #@12
    move-result v5

    #@13
    .line 1316
    .local v5, width:F
    :cond_13
    return v5

    #@14
    .end local v0           #len:I
    .end local v1           #sp:Landroid/text/Spanned;
    .end local v5           #width:F
    .restart local p2
    :cond_14
    move-object v1, v6

    #@15
    .line 1299
    goto :goto_b

    #@16
    .line 1304
    .end local p2
    .restart local v0       #len:I
    .restart local v1       #sp:Landroid/text/Spanned;
    :cond_16
    const/4 v5, 0x0

    #@17
    .line 1306
    .restart local v5       #width:F
    const/4 v3, 0x0

    #@18
    .local v3, spanStart:I
    :goto_18
    if-ge v3, v0, :cond_13

    #@1a
    .line 1307
    const-class v7, Landroid/text/style/MetricAffectingSpan;

    #@1c
    invoke-interface {v1, v3, v0, v7}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    #@1f
    move-result v2

    #@20
    .line 1309
    .local v2, spanEnd:I
    const-class v7, Landroid/text/style/MetricAffectingSpan;

    #@22
    invoke-interface {v1, v3, v2, v7}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@25
    move-result-object v4

    #@26
    check-cast v4, [Landroid/text/style/MetricAffectingSpan;

    #@28
    .line 1311
    .local v4, spans:[Landroid/text/style/MetricAffectingSpan;
    const-class v7, Landroid/text/style/MetricAffectingSpan;

    #@2a
    invoke-static {v4, v1, v7}, Landroid/text/TextUtils;->removeEmptySpans([Ljava/lang/Object;Landroid/text/Spanned;Ljava/lang/Class;)[Ljava/lang/Object;

    #@2d
    move-result-object v4

    #@2e
    .end local v4           #spans:[Landroid/text/style/MetricAffectingSpan;
    check-cast v4, [Landroid/text/style/MetricAffectingSpan;

    #@30
    .line 1312
    .restart local v4       #spans:[Landroid/text/style/MetricAffectingSpan;
    sub-int v7, v2, v3

    #@32
    invoke-virtual {p0, p1, v4, v7, v6}, Landroid/text/MeasuredText;->addStyleRun(Landroid/text/TextPaint;[Landroid/text/style/MetricAffectingSpan;ILandroid/graphics/Paint$FontMetricsInt;)F

    #@35
    move-result v7

    #@36
    add-float/2addr v5, v7

    #@37
    .line 1306
    move v3, v2

    #@38
    goto :goto_18
.end method

.method public static split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 3
    .parameter "text"
    .parameter "expression"

    #@0
    .prologue
    .line 331
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 332
    sget-object v0, Landroid/text/TextUtils;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    #@8
    .line 334
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, -0x1

    #@a
    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    goto :goto_8
.end method

.method public static split(Ljava/lang/String;Ljava/util/regex/Pattern;)[Ljava/lang/String;
    .registers 3
    .parameter "text"
    .parameter "pattern"

    #@0
    .prologue
    .line 348
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 349
    sget-object v0, Landroid/text/TextUtils;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    #@8
    .line 351
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, -0x1

    #@a
    invoke-virtual {p1, p0, v0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    goto :goto_8
.end method

.method public static stringOrSpannedString(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 432
    if-nez p0, :cond_4

    #@2
    .line 433
    const/4 p0, 0x0

    #@3
    .line 439
    .end local p0
    :cond_3
    :goto_3
    return-object p0

    #@4
    .line 434
    .restart local p0
    :cond_4
    instance-of v0, p0, Landroid/text/SpannedString;

    #@6
    if-nez v0, :cond_3

    #@8
    .line 436
    instance-of v0, p0, Landroid/text/Spanned;

    #@a
    if-eqz v0, :cond_13

    #@c
    .line 437
    new-instance v0, Landroid/text/SpannedString;

    #@e
    invoke-direct {v0, p0}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    #@11
    move-object p0, v0

    #@12
    goto :goto_3

    #@13
    .line 439
    :cond_13
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@16
    move-result-object p0

    #@17
    goto :goto_3
.end method

.method public static substring(Ljava/lang/CharSequence;II)Ljava/lang/String;
    .registers 7
    .parameter "source"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 256
    instance-of v2, p0, Ljava/lang/String;

    #@3
    if-eqz v2, :cond_c

    #@5
    .line 257
    check-cast p0, Ljava/lang/String;

    #@7
    .end local p0
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 268
    :goto_b
    return-object v0

    #@c
    .line 258
    .restart local p0
    :cond_c
    instance-of v2, p0, Ljava/lang/StringBuilder;

    #@e
    if-eqz v2, :cond_17

    #@10
    .line 259
    check-cast p0, Ljava/lang/StringBuilder;

    #@12
    .end local p0
    invoke-virtual {p0, p1, p2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    goto :goto_b

    #@17
    .line 260
    .restart local p0
    :cond_17
    instance-of v2, p0, Ljava/lang/StringBuffer;

    #@19
    if-eqz v2, :cond_22

    #@1b
    .line 261
    check-cast p0, Ljava/lang/StringBuffer;

    #@1d
    .end local p0
    invoke-virtual {p0, p1, p2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    goto :goto_b

    #@22
    .line 263
    .restart local p0
    :cond_22
    sub-int v2, p2, p1

    #@24
    invoke-static {v2}, Landroid/text/TextUtils;->obtain(I)[C

    #@27
    move-result-object v1

    #@28
    .line 264
    .local v1, temp:[C
    invoke-static {p0, p1, p2, v1, v3}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@2b
    .line 265
    new-instance v0, Ljava/lang/String;

    #@2d
    sub-int v2, p2, p1

    #@2f
    invoke-direct {v0, v1, v3, v2}, Ljava/lang/String;-><init>([CII)V

    #@32
    .line 266
    .local v0, ret:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->recycle([C)V

    #@35
    goto :goto_b
.end method

.method public static unpackRangeEndFromLong(J)I
    .registers 4
    .parameter "range"

    #@0
    .prologue
    .line 1756
    const-wide v0, 0xffffffffL

    #@5
    and-long/2addr v0, p0

    #@6
    long-to-int v0, v0

    #@7
    return v0
.end method

.method public static unpackRangeStartFromLong(J)I
    .registers 4
    .parameter "range"

    #@0
    .prologue
    .line 1746
    const/16 v0, 0x20

    #@2
    ushr-long v0, p0, v0

    #@4
    long-to-int v0, v0

    #@5
    return v0
.end method

.method public static writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
    .registers 12
    .parameter "cs"
    .parameter "p"
    .parameter "parcelableFlags"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 607
    instance-of v6, p0, Landroid/text/Spanned;

    #@3
    if-eqz v6, :cond_49

    #@5
    .line 608
    invoke-virtual {p1, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 609
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@b
    move-result-object v6

    #@c
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    move-object v5, p0

    #@10
    .line 611
    check-cast v5, Landroid/text/Spanned;

    #@12
    .line 612
    .local v5, sp:Landroid/text/Spanned;
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@15
    move-result v6

    #@16
    const-class v7, Ljava/lang/Object;

    #@18
    invoke-interface {v5, v8, v6, v7}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    .line 619
    .local v2, os:[Ljava/lang/Object;
    const/4 v0, 0x0

    #@1d
    .local v0, i:I
    :goto_1d
    array-length v6, v2

    #@1e
    if-ge v0, v6, :cond_45

    #@20
    .line 620
    aget-object v1, v2, v0

    #@22
    .line 621
    .local v1, o:Ljava/lang/Object;
    aget-object v3, v2, v0

    #@24
    .line 623
    .local v3, prop:Ljava/lang/Object;
    instance-of v6, v3, Landroid/text/style/CharacterStyle;

    #@26
    if-eqz v6, :cond_2e

    #@28
    .line 624
    check-cast v3, Landroid/text/style/CharacterStyle;

    #@2a
    .end local v3           #prop:Ljava/lang/Object;
    invoke-virtual {v3}, Landroid/text/style/CharacterStyle;->getUnderlying()Landroid/text/style/CharacterStyle;

    #@2d
    move-result-object v3

    #@2e
    .line 627
    :cond_2e
    instance-of v6, v3, Landroid/text/ParcelableSpan;

    #@30
    if-eqz v6, :cond_42

    #@32
    move-object v4, v3

    #@33
    .line 628
    check-cast v4, Landroid/text/ParcelableSpan;

    #@35
    .line 629
    .local v4, ps:Landroid/text/ParcelableSpan;
    invoke-interface {v4}, Landroid/text/ParcelableSpan;->getSpanTypeId()I

    #@38
    move-result v6

    #@39
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@3c
    .line 630
    invoke-interface {v4, p1, p2}, Landroid/text/ParcelableSpan;->writeToParcel(Landroid/os/Parcel;I)V

    #@3f
    .line 631
    invoke-static {p1, v5, v1}, Landroid/text/TextUtils;->writeWhere(Landroid/os/Parcel;Landroid/text/Spanned;Ljava/lang/Object;)V

    #@42
    .line 619
    .end local v4           #ps:Landroid/text/ParcelableSpan;
    :cond_42
    add-int/lit8 v0, v0, 0x1

    #@44
    goto :goto_1d

    #@45
    .line 635
    .end local v1           #o:Ljava/lang/Object;
    :cond_45
    invoke-virtual {p1, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@48
    .line 644
    .end local v0           #i:I
    .end local v2           #os:[Ljava/lang/Object;
    .end local v5           #sp:Landroid/text/Spanned;
    :goto_48
    return-void

    #@49
    .line 637
    :cond_49
    const/4 v6, 0x1

    #@4a
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@4d
    .line 638
    if-eqz p0, :cond_57

    #@4f
    .line 639
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@56
    goto :goto_48

    #@57
    .line 641
    :cond_57
    const/4 v6, 0x0

    #@58
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5b
    goto :goto_48
.end method

.method private static writeWhere(Landroid/os/Parcel;Landroid/text/Spanned;Ljava/lang/Object;)V
    .registers 4
    .parameter "p"
    .parameter "sp"
    .parameter "o"

    #@0
    .prologue
    .line 647
    invoke-interface {p1, p2}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 648
    invoke-interface {p1, p2}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@a
    move-result v0

    #@b
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 649
    invoke-interface {p1, p2}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    #@11
    move-result v0

    #@12
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 650
    return-void
.end method
