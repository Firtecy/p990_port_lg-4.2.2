.class Landroid/text/AndroidBidi;
.super Ljava/lang/Object;
.source "AndroidBidi.java"


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static bidi(I[C[BIZ)I
    .registers 7
    .parameter "dir"
    .parameter "chs"
    .parameter "chInfo"
    .parameter "n"
    .parameter "haveInfo"

    #@0
    .prologue
    .line 30
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_a

    #@4
    .line 31
    :cond_4
    new-instance v1, Ljava/lang/NullPointerException;

    #@6
    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    #@9
    throw v1

    #@a
    .line 34
    :cond_a
    if-ltz p3, :cond_12

    #@c
    array-length v1, p1

    #@d
    if-lt v1, p3, :cond_12

    #@f
    array-length v1, p2

    #@10
    if-ge v1, p3, :cond_18

    #@12
    .line 35
    :cond_12
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@14
    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@17
    throw v1

    #@18
    .line 38
    :cond_18
    packed-switch p0, :pswitch_data_30

    #@1b
    .line 43
    :pswitch_1b
    const/4 p0, 0x0

    #@1c
    .line 46
    :goto_1c
    invoke-static {p0, p1, p2, p3, p4}, Landroid/text/AndroidBidi;->runBidi(I[C[BIZ)I

    #@1f
    move-result v0

    #@20
    .line 47
    .local v0, result:I
    and-int/lit8 v1, v0, 0x1

    #@22
    if-nez v1, :cond_2e

    #@24
    const/4 v0, 0x1

    #@25
    .line 48
    :goto_25
    return v0

    #@26
    .line 39
    .end local v0           #result:I
    :pswitch_26
    const/4 p0, 0x0

    #@27
    goto :goto_1c

    #@28
    .line 40
    :pswitch_28
    const/4 p0, 0x1

    #@29
    goto :goto_1c

    #@2a
    .line 41
    :pswitch_2a
    const/4 p0, -0x2

    #@2b
    goto :goto_1c

    #@2c
    .line 42
    :pswitch_2c
    const/4 p0, -0x1

    #@2d
    goto :goto_1c

    #@2e
    .line 47
    .restart local v0       #result:I
    :cond_2e
    const/4 v0, -0x1

    #@2f
    goto :goto_25

    #@30
    .line 38
    :pswitch_data_30
    .packed-switch -0x2
        :pswitch_2c
        :pswitch_28
        :pswitch_1b
        :pswitch_26
        :pswitch_2a
    .end packed-switch
.end method

.method public static directions(I[BI[CII)Landroid/text/Layout$Directions;
    .registers 35
    .parameter "dir"
    .parameter "levels"
    .parameter "lstart"
    .parameter "chars"
    .parameter "cstart"
    .parameter "len"

    #@0
    .prologue
    .line 66
    const/16 v25, 0x1

    #@2
    move/from16 v0, p0

    #@4
    move/from16 v1, v25

    #@6
    if-ne v0, v1, :cond_9d

    #@8
    const/4 v2, 0x0

    #@9
    .line 67
    .local v2, baseLevel:I
    :goto_9
    aget-byte v5, p1, p2

    #@b
    .line 68
    .local v5, curLevel:I
    move/from16 v16, v5

    #@d
    .line 69
    .local v16, minLevel:I
    const/16 v20, 0x1

    #@f
    .line 73
    .local v20, runCount:I
    const/16 v21, 0x0

    #@11
    .line 75
    .local v21, startIsEmoji:Z
    :try_start_11
    sget-boolean v25, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@13
    if-eqz v25, :cond_49

    #@15
    sget-object v25, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@17
    aget-char v26, p3, p2

    #@19
    invoke-virtual/range {v25 .. v26}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@1c
    move-result v25

    #@1d
    if-nez v25, :cond_49

    #@1f
    .line 76
    const/16 v25, 0x1

    #@21
    move/from16 v0, v25

    #@23
    if-ne v2, v0, :cond_49

    #@25
    .line 77
    move-object/from16 v0, p3

    #@27
    move/from16 v1, p2

    #@29
    invoke-static {v0, v1}, Ljava/lang/Character;->codePointAt([CI)I

    #@2c
    move-result v4

    #@2d
    .line 78
    .local v4, codept:I
    invoke-static {v4}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@30
    move-result v25

    #@31
    if-nez v25, :cond_47

    #@33
    add-int/lit8 v25, p2, 0x3

    #@35
    add-int v26, p2, p5

    #@37
    move/from16 v0, v25

    #@39
    move/from16 v1, v26

    #@3b
    if-ge v0, v1, :cond_a0

    #@3d
    move-object/from16 v0, p3

    #@3f
    move/from16 v1, p2

    #@41
    invoke-static {v0, v1}, Landroid/text/Layout;->isDiacriticalMark([CI)Z
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_44} :catch_dc

    #@44
    move-result v25

    #@45
    if-eqz v25, :cond_a0

    #@47
    .line 79
    :cond_47
    const/16 v21, 0x1

    #@49
    .line 88
    .end local v4           #codept:I
    :cond_49
    :goto_49
    if-eqz v21, :cond_c2

    #@4b
    move/from16 v10, p2

    #@4d
    .local v10, i:I
    :goto_4d
    add-int v7, p2, p5

    #@4f
    .local v7, e:I
    move v6, v5

    #@50
    .end local v5           #curLevel:I
    .local v6, curLevel:I
    :goto_50
    if-ge v10, v7, :cond_143

    #@52
    .line 91
    :try_start_52
    sget-boolean v25, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@54
    if-eqz v25, :cond_23e

    #@56
    sget-object v25, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@58
    aget-char v26, p3, v10

    #@5a
    invoke-virtual/range {v25 .. v26}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@5d
    move-result v25

    #@5e
    if-nez v25, :cond_23e

    #@60
    .line 92
    const/16 v25, 0x1

    #@62
    move/from16 v0, v25

    #@64
    if-ne v2, v0, :cond_23e

    #@66
    .line 93
    move-object/from16 v0, p3

    #@68
    invoke-static {v0, v10}, Ljava/lang/Character;->codePointAt([CI)I

    #@6b
    move-result v4

    #@6c
    .line 94
    .restart local v4       #codept:I
    invoke-static {v4}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@6f
    move-result v25

    #@70
    if-nez v25, :cond_80

    #@72
    add-int/lit8 v25, v10, 0x3

    #@74
    move/from16 v0, v25

    #@76
    if-ge v0, v7, :cond_c5

    #@78
    move-object/from16 v0, p3

    #@7a
    invoke-static {v0, v10}, Landroid/text/Layout;->isDiacriticalMark([CI)Z

    #@7d
    move-result v25

    #@7e
    if-eqz v25, :cond_c5

    #@80
    .line 95
    :cond_80
    move/from16 v0, p2

    #@82
    if-ne v10, v0, :cond_244

    #@84
    .line 96
    int-to-byte v5, v2

    #@85
    aput-byte v5, p1, p2
    :try_end_87
    .catch Ljava/lang/Exception; {:try_start_52 .. :try_end_87} :catch_22f

    #@87
    .end local v6           #curLevel:I
    .restart local v5       #curLevel:I
    move/from16 v16, v5

    #@89
    .line 97
    :goto_89
    add-int/lit8 v25, v10, 0x1

    #@8b
    int-to-byte v0, v2

    #@8c
    move/from16 v26, v0

    #@8e
    :try_start_8e
    aput-byte v26, p1, v25

    #@90
    aput-byte v26, p1, v10

    #@92
    .line 112
    .end local v4           #codept:I
    :goto_92
    aget-byte v12, p1, v10

    #@94
    .line 113
    .local v12, level:I
    if-eq v12, v5, :cond_99

    #@96
    .line 114
    move v5, v12

    #@97
    .line 115
    add-int/lit8 v20, v20, 0x1

    #@99
    .line 88
    :cond_99
    add-int/lit8 v10, v10, 0x1

    #@9b
    move v6, v5

    #@9c
    .end local v5           #curLevel:I
    .restart local v6       #curLevel:I
    goto :goto_50

    #@9d
    .line 66
    .end local v2           #baseLevel:I
    .end local v6           #curLevel:I
    .end local v7           #e:I
    .end local v10           #i:I
    .end local v12           #level:I
    .end local v16           #minLevel:I
    .end local v20           #runCount:I
    .end local v21           #startIsEmoji:Z
    :cond_9d
    const/4 v2, 0x1

    #@9e
    goto/16 :goto_9

    #@a0
    .line 80
    .restart local v2       #baseLevel:I
    .restart local v4       #codept:I
    .restart local v5       #curLevel:I
    .restart local v16       #minLevel:I
    .restart local v20       #runCount:I
    .restart local v21       #startIsEmoji:Z
    :cond_a0
    aget-char v25, p3, p2

    #@a2
    invoke-static/range {v25 .. v25}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z

    #@a5
    move-result v25

    #@a6
    if-eqz v25, :cond_ab

    #@a8
    .line 81
    const/16 v21, 0x1

    #@aa
    goto :goto_49

    #@ab
    .line 82
    :cond_ab
    add-int/lit8 v25, p2, 0x3

    #@ad
    add-int v26, p2, p5

    #@af
    move/from16 v0, v25

    #@b1
    move/from16 v1, v26

    #@b3
    if-ge v0, v1, :cond_49

    #@b5
    move-object/from16 v0, p3

    #@b7
    move/from16 v1, p2

    #@b9
    invoke-static {v0, v1}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z
    :try_end_bc
    .catch Ljava/lang/Exception; {:try_start_8e .. :try_end_bc} :catch_dc

    #@bc
    move-result v25

    #@bd
    if-eqz v25, :cond_49

    #@bf
    .line 83
    const/16 v21, 0x1

    #@c1
    goto :goto_49

    #@c2
    .line 88
    .end local v4           #codept:I
    :cond_c2
    add-int/lit8 v10, p2, 0x1

    #@c4
    goto :goto_4d

    #@c5
    .line 99
    .end local v5           #curLevel:I
    .restart local v4       #codept:I
    .restart local v6       #curLevel:I
    .restart local v7       #e:I
    .restart local v10       #i:I
    :cond_c5
    :try_start_c5
    aget-char v25, p3, v10

    #@c7
    invoke-static/range {v25 .. v25}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z

    #@ca
    move-result v25

    #@cb
    if-eqz v25, :cond_119

    #@cd
    .line 100
    move/from16 v0, p2

    #@cf
    if-ne v10, v0, :cond_241

    #@d1
    .line 101
    int-to-byte v5, v2

    #@d2
    aput-byte v5, p1, p2
    :try_end_d4
    .catch Ljava/lang/Exception; {:try_start_c5 .. :try_end_d4} :catch_22f

    #@d4
    .end local v6           #curLevel:I
    .restart local v5       #curLevel:I
    move/from16 v16, v5

    #@d6
    .line 102
    :goto_d6
    int-to-byte v0, v2

    #@d7
    move/from16 v25, v0

    #@d9
    :try_start_d9
    aput-byte v25, p1, v10
    :try_end_db
    .catch Ljava/lang/Exception; {:try_start_d9 .. :try_end_db} :catch_dc

    #@db
    goto :goto_92

    #@dc
    .line 118
    .end local v4           #codept:I
    .end local v7           #e:I
    .end local v10           #i:I
    :catch_dc
    move-exception v8

    #@dd
    .line 119
    .local v8, ex:Ljava/lang/Exception;
    :goto_dd
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    #@e0
    .line 124
    .end local v8           #ex:Ljava/lang/Exception;
    :goto_e0
    move/from16 v23, p5

    #@e2
    .line 125
    .local v23, visLen:I
    and-int/lit8 v25, v5, 0x1

    #@e4
    and-int/lit8 v26, v2, 0x1

    #@e6
    move/from16 v0, v25

    #@e8
    move/from16 v1, v26

    #@ea
    if-eq v0, v1, :cond_106

    #@ec
    .line 127
    :cond_ec
    add-int/lit8 v23, v23, -0x1

    #@ee
    if-ltz v23, :cond_fc

    #@f0
    .line 128
    add-int v25, p4, v23

    #@f2
    aget-char v3, p3, v25

    #@f4
    .line 130
    .local v3, ch:C
    const/16 v25, 0xa

    #@f6
    move/from16 v0, v25

    #@f8
    if-ne v3, v0, :cond_145

    #@fa
    .line 131
    add-int/lit8 v23, v23, -0x1

    #@fc
    .line 139
    .end local v3           #ch:C
    :cond_fc
    :goto_fc
    add-int/lit8 v23, v23, 0x1

    #@fe
    .line 140
    move/from16 v0, v23

    #@100
    move/from16 v1, p5

    #@102
    if-eq v0, v1, :cond_106

    #@104
    .line 141
    add-int/lit8 v20, v20, 0x1

    #@106
    .line 145
    :cond_106
    const/16 v25, 0x1

    #@108
    move/from16 v0, v20

    #@10a
    move/from16 v1, v25

    #@10c
    if-ne v0, v1, :cond_155

    #@10e
    move/from16 v0, v16

    #@110
    if-ne v0, v2, :cond_155

    #@112
    .line 147
    and-int/lit8 v25, v16, 0x1

    #@114
    if-eqz v25, :cond_152

    #@116
    .line 148
    sget-object v25, Landroid/text/Layout;->DIRS_ALL_RIGHT_TO_LEFT:Landroid/text/Layout$Directions;

    #@118
    .line 220
    :goto_118
    return-object v25

    #@119
    .line 104
    .end local v5           #curLevel:I
    .end local v23           #visLen:I
    .restart local v4       #codept:I
    .restart local v6       #curLevel:I
    .restart local v7       #e:I
    .restart local v10       #i:I
    :cond_119
    add-int/lit8 v25, v10, 0x3

    #@11b
    move/from16 v0, v25

    #@11d
    if-ge v0, v7, :cond_23e

    #@11f
    :try_start_11f
    move-object/from16 v0, p3

    #@121
    invoke-static {v0, v10}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z

    #@124
    move-result v25

    #@125
    if-eqz v25, :cond_23e

    #@127
    .line 105
    move/from16 v0, p2

    #@129
    if-ne v10, v0, :cond_23b

    #@12b
    .line 106
    int-to-byte v5, v2

    #@12c
    aput-byte v5, p1, p2
    :try_end_12e
    .catch Ljava/lang/Exception; {:try_start_11f .. :try_end_12e} :catch_22f

    #@12e
    .end local v6           #curLevel:I
    .restart local v5       #curLevel:I
    move/from16 v16, v5

    #@130
    .line 107
    :goto_130
    add-int/lit8 v25, v10, 0x1

    #@132
    add-int/lit8 v26, v10, 0x2

    #@134
    add-int/lit8 v27, v10, 0x3

    #@136
    int-to-byte v0, v2

    #@137
    move/from16 v28, v0

    #@139
    :try_start_139
    aput-byte v28, p1, v27

    #@13b
    aput-byte v28, p1, v26

    #@13d
    aput-byte v28, p1, v25

    #@13f
    aput-byte v28, p1, v10
    :try_end_141
    .catch Ljava/lang/Exception; {:try_start_139 .. :try_end_141} :catch_dc

    #@141
    goto/16 :goto_92

    #@143
    .end local v4           #codept:I
    .end local v5           #curLevel:I
    .restart local v6       #curLevel:I
    :cond_143
    move v5, v6

    #@144
    .line 120
    .end local v6           #curLevel:I
    .restart local v5       #curLevel:I
    goto :goto_e0

    #@145
    .line 135
    .end local v7           #e:I
    .end local v10           #i:I
    .restart local v3       #ch:C
    .restart local v23       #visLen:I
    :cond_145
    const/16 v25, 0x20

    #@147
    move/from16 v0, v25

    #@149
    if-eq v3, v0, :cond_ec

    #@14b
    const/16 v25, 0x9

    #@14d
    move/from16 v0, v25

    #@14f
    if-eq v3, v0, :cond_ec

    #@151
    goto :goto_fc

    #@152
    .line 150
    .end local v3           #ch:C
    :cond_152
    sget-object v25, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@154
    goto :goto_118

    #@155
    .line 153
    :cond_155
    mul-int/lit8 v25, v20, 0x2

    #@157
    move/from16 v0, v25

    #@159
    new-array v11, v0, [I

    #@15b
    .line 154
    .local v11, ld:[I
    move/from16 v15, v16

    #@15d
    .line 155
    .local v15, maxLevel:I
    shl-int/lit8 v13, v16, 0x1a

    #@15f
    .line 160
    .local v13, levelBits:I
    const/16 v17, 0x1

    #@161
    .line 161
    .local v17, n:I
    move/from16 v19, p2

    #@163
    .line 162
    .local v19, prev:I
    move/from16 v5, v16

    #@165
    .line 163
    move/from16 v10, p2

    #@167
    .restart local v10       #i:I
    add-int v7, p2, v23

    #@169
    .restart local v7       #e:I
    move/from16 v18, v17

    #@16b
    .end local v17           #n:I
    .local v18, n:I
    :goto_16b
    if-ge v10, v7, :cond_195

    #@16d
    .line 164
    aget-byte v12, p1, v10

    #@16f
    .line 165
    .restart local v12       #level:I
    if-eq v12, v5, :cond_237

    #@171
    .line 166
    move v5, v12

    #@172
    .line 167
    if-le v12, v15, :cond_18e

    #@174
    .line 168
    move v15, v12

    #@175
    .line 173
    :cond_175
    :goto_175
    add-int/lit8 v17, v18, 0x1

    #@177
    .end local v18           #n:I
    .restart local v17       #n:I
    sub-int v25, v10, v19

    #@179
    or-int v25, v25, v13

    #@17b
    aput v25, v11, v18

    #@17d
    .line 174
    add-int/lit8 v18, v17, 0x1

    #@17f
    .end local v17           #n:I
    .restart local v18       #n:I
    sub-int v25, v10, p2

    #@181
    aput v25, v11, v17

    #@183
    .line 175
    shl-int/lit8 v13, v5, 0x1a

    #@185
    .line 176
    move/from16 v19, v10

    #@187
    move/from16 v17, v18

    #@189
    .line 163
    .end local v18           #n:I
    .restart local v17       #n:I
    :goto_189
    add-int/lit8 v10, v10, 0x1

    #@18b
    move/from16 v18, v17

    #@18d
    .end local v17           #n:I
    .restart local v18       #n:I
    goto :goto_16b

    #@18e
    .line 169
    :cond_18e
    move/from16 v0, v16

    #@190
    if-ge v12, v0, :cond_175

    #@192
    .line 170
    move/from16 v16, v12

    #@194
    goto :goto_175

    #@195
    .line 179
    .end local v12           #level:I
    :cond_195
    add-int v25, p2, v23

    #@197
    sub-int v25, v25, v19

    #@199
    or-int v25, v25, v13

    #@19b
    aput v25, v11, v18

    #@19d
    .line 180
    move/from16 v0, v23

    #@19f
    move/from16 v1, p5

    #@1a1
    if-ge v0, v1, :cond_233

    #@1a3
    .line 181
    add-int/lit8 v17, v18, 0x1

    #@1a5
    .end local v18           #n:I
    .restart local v17       #n:I
    aput v23, v11, v17

    #@1a7
    .line 182
    add-int/lit8 v17, v17, 0x1

    #@1a9
    sub-int v25, p5, v23

    #@1ab
    shl-int/lit8 v26, v2, 0x1a

    #@1ad
    or-int v25, v25, v26

    #@1af
    aput v25, v11, v17

    #@1b1
    .line 197
    :goto_1b1
    and-int/lit8 v25, v16, 0x1

    #@1b3
    move/from16 v0, v25

    #@1b5
    if-ne v0, v2, :cond_1ee

    #@1b7
    .line 198
    add-int/lit8 v16, v16, 0x1

    #@1b9
    .line 199
    move/from16 v0, v16

    #@1bb
    if-le v15, v0, :cond_1eb

    #@1bd
    const/16 v22, 0x1

    #@1bf
    .line 203
    .local v22, swap:Z
    :goto_1bf
    if-eqz v22, :cond_226

    #@1c1
    .line 204
    add-int/lit8 v12, v15, -0x1

    #@1c3
    .restart local v12       #level:I
    :goto_1c3
    move/from16 v0, v16

    #@1c5
    if-lt v12, v0, :cond_226

    #@1c7
    .line 205
    const/4 v10, 0x0

    #@1c8
    :goto_1c8
    array-length v0, v11

    #@1c9
    move/from16 v25, v0

    #@1cb
    move/from16 v0, v25

    #@1cd
    if-ge v10, v0, :cond_223

    #@1cf
    .line 206
    aget v25, v11, v10

    #@1d1
    aget-byte v25, p1, v25

    #@1d3
    move/from16 v0, v25

    #@1d5
    if-lt v0, v12, :cond_220

    #@1d7
    .line 207
    add-int/lit8 v7, v10, 0x2

    #@1d9
    .line 208
    :goto_1d9
    array-length v0, v11

    #@1da
    move/from16 v25, v0

    #@1dc
    move/from16 v0, v25

    #@1de
    if-ge v7, v0, :cond_1fc

    #@1e0
    aget v25, v11, v7

    #@1e2
    aget-byte v25, p1, v25

    #@1e4
    move/from16 v0, v25

    #@1e6
    if-lt v0, v12, :cond_1fc

    #@1e8
    .line 209
    add-int/lit8 v7, v7, 0x2

    #@1ea
    goto :goto_1d9

    #@1eb
    .line 199
    .end local v12           #level:I
    .end local v22           #swap:Z
    :cond_1eb
    const/16 v22, 0x0

    #@1ed
    goto :goto_1bf

    #@1ee
    .line 201
    :cond_1ee
    const/16 v25, 0x1

    #@1f0
    move/from16 v0, v20

    #@1f2
    move/from16 v1, v25

    #@1f4
    if-le v0, v1, :cond_1f9

    #@1f6
    const/16 v22, 0x1

    #@1f8
    .restart local v22       #swap:Z
    :goto_1f8
    goto :goto_1bf

    #@1f9
    .end local v22           #swap:Z
    :cond_1f9
    const/16 v22, 0x0

    #@1fb
    goto :goto_1f8

    #@1fc
    .line 211
    .restart local v12       #level:I
    .restart local v22       #swap:Z
    :cond_1fc
    move v14, v10

    #@1fd
    .local v14, low:I
    add-int/lit8 v9, v7, -0x2

    #@1ff
    .local v9, hi:I
    :goto_1ff
    if-ge v14, v9, :cond_21e

    #@201
    .line 212
    aget v24, v11, v14

    #@203
    .local v24, x:I
    aget v25, v11, v9

    #@205
    aput v25, v11, v14

    #@207
    aput v24, v11, v9

    #@209
    .line 213
    add-int/lit8 v25, v14, 0x1

    #@20b
    aget v24, v11, v25

    #@20d
    add-int/lit8 v25, v14, 0x1

    #@20f
    add-int/lit8 v26, v9, 0x1

    #@211
    aget v26, v11, v26

    #@213
    aput v26, v11, v25

    #@215
    add-int/lit8 v25, v9, 0x1

    #@217
    aput v24, v11, v25

    #@219
    .line 211
    add-int/lit8 v14, v14, 0x2

    #@21b
    add-int/lit8 v9, v9, -0x2

    #@21d
    goto :goto_1ff

    #@21e
    .line 215
    .end local v24           #x:I
    :cond_21e
    add-int/lit8 v10, v7, 0x2

    #@220
    .line 205
    .end local v9           #hi:I
    .end local v14           #low:I
    :cond_220
    add-int/lit8 v10, v10, 0x2

    #@222
    goto :goto_1c8

    #@223
    .line 204
    :cond_223
    add-int/lit8 v12, v12, -0x1

    #@225
    goto :goto_1c3

    #@226
    .line 220
    .end local v12           #level:I
    :cond_226
    new-instance v25, Landroid/text/Layout$Directions;

    #@228
    move-object/from16 v0, v25

    #@22a
    invoke-direct {v0, v11}, Landroid/text/Layout$Directions;-><init>([I)V

    #@22d
    goto/16 :goto_118

    #@22f
    .line 118
    .end local v5           #curLevel:I
    .end local v11           #ld:[I
    .end local v13           #levelBits:I
    .end local v15           #maxLevel:I
    .end local v17           #n:I
    .end local v19           #prev:I
    .end local v22           #swap:Z
    .end local v23           #visLen:I
    .restart local v6       #curLevel:I
    :catch_22f
    move-exception v8

    #@230
    move v5, v6

    #@231
    .end local v6           #curLevel:I
    .restart local v5       #curLevel:I
    goto/16 :goto_dd

    #@233
    .restart local v11       #ld:[I
    .restart local v13       #levelBits:I
    .restart local v15       #maxLevel:I
    .restart local v18       #n:I
    .restart local v19       #prev:I
    .restart local v23       #visLen:I
    :cond_233
    move/from16 v17, v18

    #@235
    .end local v18           #n:I
    .restart local v17       #n:I
    goto/16 :goto_1b1

    #@237
    .end local v17           #n:I
    .restart local v12       #level:I
    .restart local v18       #n:I
    :cond_237
    move/from16 v17, v18

    #@239
    .end local v18           #n:I
    .restart local v17       #n:I
    goto/16 :goto_189

    #@23b
    .end local v5           #curLevel:I
    .end local v11           #ld:[I
    .end local v12           #level:I
    .end local v13           #levelBits:I
    .end local v15           #maxLevel:I
    .end local v17           #n:I
    .end local v19           #prev:I
    .end local v23           #visLen:I
    .restart local v4       #codept:I
    .restart local v6       #curLevel:I
    :cond_23b
    move v5, v6

    #@23c
    .end local v6           #curLevel:I
    .restart local v5       #curLevel:I
    goto/16 :goto_130

    #@23e
    .end local v4           #codept:I
    .end local v5           #curLevel:I
    .restart local v6       #curLevel:I
    :cond_23e
    move v5, v6

    #@23f
    .end local v6           #curLevel:I
    .restart local v5       #curLevel:I
    goto/16 :goto_92

    #@241
    .end local v5           #curLevel:I
    .restart local v4       #codept:I
    .restart local v6       #curLevel:I
    :cond_241
    move v5, v6

    #@242
    .end local v6           #curLevel:I
    .restart local v5       #curLevel:I
    goto/16 :goto_d6

    #@244
    .end local v5           #curLevel:I
    .restart local v6       #curLevel:I
    :cond_244
    move v5, v6

    #@245
    .end local v6           #curLevel:I
    .restart local v5       #curLevel:I
    goto/16 :goto_89
.end method

.method private static native runBidi(I[C[BIZ)I
.end method
