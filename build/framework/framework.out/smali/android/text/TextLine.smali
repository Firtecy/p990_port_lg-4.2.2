.class Landroid/text/TextLine;
.super Ljava/lang/Object;
.source "TextLine.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final TAB_INCREMENT:I = 0x14

.field private static final sCached:[Landroid/text/TextLine;


# instance fields
.field private final mCharacterStyleSpanSet:Landroid/text/SpanSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/text/SpanSet",
            "<",
            "Landroid/text/style/CharacterStyle;",
            ">;"
        }
    .end annotation
.end field

.field private mChars:[C

.field private mCharsValid:Z

.field private mDir:I

.field private mDirections:Landroid/text/Layout$Directions;

.field private mEmojiPaint:Landroid/graphics/Paint;

.field private mHasTabs:Z

.field private mLen:I

.field private final mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/text/SpanSet",
            "<",
            "Landroid/text/style/MetricAffectingSpan;",
            ">;"
        }
    .end annotation
.end field

.field private mPaint:Landroid/text/TextPaint;

.field private final mReplacementSpanSpanSet:Landroid/text/SpanSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/text/SpanSet",
            "<",
            "Landroid/text/style/ReplacementSpan;",
            ">;"
        }
    .end annotation
.end field

.field private mSpanned:Landroid/text/Spanned;

.field private mStart:I

.field private mTabs:Landroid/text/Layout$TabStops;

.field private mText:Ljava/lang/CharSequence;

.field private final mWorkPaint:Landroid/text/TextPaint;

.field private mZWLeftOffset:I

.field private mZWRightOffset:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 73
    const/4 v0, 0x3

    #@1
    new-array v0, v0, [Landroid/text/TextLine;

    #@3
    sput-object v0, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    #@5
    return-void
.end method

.method constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 65
    new-instance v0, Landroid/text/TextPaint;

    #@6
    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    #@9
    iput-object v0, p0, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    #@b
    .line 66
    new-instance v0, Landroid/text/SpanSet;

    #@d
    const-class v1, Landroid/text/style/MetricAffectingSpan;

    #@f
    invoke-direct {v0, v1}, Landroid/text/SpanSet;-><init>(Ljava/lang/Class;)V

    #@12
    iput-object v0, p0, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    #@14
    .line 68
    new-instance v0, Landroid/text/SpanSet;

    #@16
    const-class v1, Landroid/text/style/CharacterStyle;

    #@18
    invoke-direct {v0, v1}, Landroid/text/SpanSet;-><init>(Ljava/lang/Class;)V

    #@1b
    iput-object v0, p0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    #@1d
    .line 70
    new-instance v0, Landroid/text/SpanSet;

    #@1f
    const-class v1, Landroid/text/style/ReplacementSpan;

    #@21
    invoke-direct {v0, v1}, Landroid/text/SpanSet;-><init>(Ljava/lang/Class;)V

    #@24
    iput-object v0, p0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    #@26
    .line 79
    iput v2, p0, Landroid/text/TextLine;->mZWLeftOffset:I

    #@28
    .line 80
    iput v2, p0, Landroid/text/TextLine;->mZWRightOffset:I

    #@2a
    .line 1330
    const/4 v0, 0x0

    #@2b
    iput-object v0, p0, Landroid/text/TextLine;->mEmojiPaint:Landroid/graphics/Paint;

    #@2d
    return-void
.end method

.method private adjustSpanRangeForLiguatures(Landroid/text/TextPaint;IIIIZ)Z
    .registers 29
    .parameter "wp"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "runIsRtl"

    #@0
    .prologue
    .line 1197
    sub-int v21, p3, p2

    #@2
    .line 1198
    .local v21, runLen:I
    const/16 v19, 0x0

    #@4
    .line 1201
    .local v19, pos:I
    if-nez v21, :cond_8

    #@6
    .line 1202
    const/4 v1, 0x0

    #@7
    .line 1234
    :goto_7
    return v1

    #@8
    .line 1205
    :cond_8
    sub-int v4, p5, p4

    #@a
    .line 1206
    .local v4, contextLen:I
    if-eqz p6, :cond_42

    #@c
    const/4 v7, 0x1

    #@d
    .line 1207
    .local v7, flags:I
    :goto_d
    new-array v8, v4, [F

    #@f
    .line 1209
    .local v8, advances:[F
    move-object/from16 v0, p0

    #@11
    iget-boolean v1, v0, Landroid/text/TextLine;->mCharsValid:Z

    #@13
    if-eqz v1, :cond_44

    #@15
    .line 1210
    move-object/from16 v0, p0

    #@17
    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    #@19
    const/4 v9, 0x0

    #@1a
    move-object/from16 v1, p1

    #@1c
    move/from16 v3, p4

    #@1e
    move/from16 v5, p4

    #@20
    move v6, v4

    #@21
    invoke-virtual/range {v1 .. v9}, Landroid/text/TextPaint;->getTextRunAdvances([CIIIII[FI)F

    #@24
    .line 1219
    :goto_24
    sub-int v19, p2, p4

    #@26
    move/from16 v20, v19

    #@28
    .line 1222
    .end local v19           #pos:I
    .local v20, pos:I
    :goto_28
    if-ltz v20, :cond_61

    #@2a
    add-int/lit8 v19, v20, -0x1

    #@2c
    .end local v20           #pos:I
    .restart local v19       #pos:I
    aget v1, v8, v20

    #@2e
    const/4 v2, 0x0

    #@2f
    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    #@32
    move-result v1

    #@33
    if-nez v1, :cond_63

    #@35
    .line 1223
    move-object/from16 v0, p0

    #@37
    iget v1, v0, Landroid/text/TextLine;->mZWLeftOffset:I

    #@39
    add-int/lit8 v1, v1, 0x1

    #@3b
    move-object/from16 v0, p0

    #@3d
    iput v1, v0, Landroid/text/TextLine;->mZWLeftOffset:I

    #@3f
    move/from16 v20, v19

    #@41
    .end local v19           #pos:I
    .restart local v20       #pos:I
    goto :goto_28

    #@42
    .line 1206
    .end local v7           #flags:I
    .end local v8           #advances:[F
    .end local v20           #pos:I
    .restart local v19       #pos:I
    :cond_42
    const/4 v7, 0x0

    #@43
    goto :goto_d

    #@44
    .line 1213
    .restart local v7       #flags:I
    .restart local v8       #advances:[F
    :cond_44
    move-object/from16 v0, p0

    #@46
    iget v0, v0, Landroid/text/TextLine;->mStart:I

    #@48
    move/from16 v18, v0

    #@4a
    .line 1214
    .local v18, delta:I
    move-object/from16 v0, p0

    #@4c
    iget-object v10, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    #@4e
    add-int v11, v18, p4

    #@50
    add-int v12, v18, p5

    #@52
    add-int v13, v18, p4

    #@54
    add-int v14, v18, p5

    #@56
    const/16 v17, 0x0

    #@58
    move-object/from16 v9, p1

    #@5a
    move v15, v7

    #@5b
    move-object/from16 v16, v8

    #@5d
    invoke-virtual/range {v9 .. v17}, Landroid/text/TextPaint;->getTextRunAdvances(Ljava/lang/CharSequence;IIIII[FI)F

    #@60
    goto :goto_24

    #@61
    .end local v18           #delta:I
    .end local v19           #pos:I
    .restart local v20       #pos:I
    :cond_61
    move/from16 v19, v20

    #@63
    .line 1226
    .end local v20           #pos:I
    .restart local v19       #pos:I
    :cond_63
    sub-int v19, p3, p4

    #@65
    move/from16 v20, v19

    #@67
    .line 1227
    .end local v19           #pos:I
    .restart local v20       #pos:I
    :goto_67
    sub-int v1, p5, p4

    #@69
    move/from16 v0, v20

    #@6b
    if-ge v0, v1, :cond_85

    #@6d
    add-int/lit8 v19, v20, 0x1

    #@6f
    .end local v20           #pos:I
    .restart local v19       #pos:I
    aget v1, v8, v20

    #@71
    const/4 v2, 0x0

    #@72
    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    #@75
    move-result v1

    #@76
    if-nez v1, :cond_87

    #@78
    .line 1228
    move-object/from16 v0, p0

    #@7a
    iget v1, v0, Landroid/text/TextLine;->mZWRightOffset:I

    #@7c
    add-int/lit8 v1, v1, 0x1

    #@7e
    move-object/from16 v0, p0

    #@80
    iput v1, v0, Landroid/text/TextLine;->mZWRightOffset:I

    #@82
    move/from16 v20, v19

    #@84
    .end local v19           #pos:I
    .restart local v20       #pos:I
    goto :goto_67

    #@85
    :cond_85
    move/from16 v19, v20

    #@87
    .line 1232
    .end local v20           #pos:I
    .restart local v19       #pos:I
    :cond_87
    move-object/from16 v0, p0

    #@89
    iget v1, v0, Landroid/text/TextLine;->mZWLeftOffset:I

    #@8b
    if-gtz v1, :cond_93

    #@8d
    move-object/from16 v0, p0

    #@8f
    iget v1, v0, Landroid/text/TextLine;->mZWRightOffset:I

    #@91
    if-lez v1, :cond_96

    #@93
    .line 1233
    :cond_93
    const/4 v1, 0x1

    #@94
    goto/16 :goto_7

    #@96
    .line 1234
    :cond_96
    const/4 v1, 0x0

    #@97
    goto/16 :goto_7
.end method

.method private drawRun(Landroid/graphics/Canvas;IIZFIIIZ)F
    .registers 24
    .parameter "c"
    .parameter "start"
    .parameter "limit"
    .parameter "runIsRtl"
    .parameter "x"
    .parameter "top"
    .parameter "y"
    .parameter "bottom"
    .parameter "needWidth"

    #@0
    .prologue
    .line 583
    iget v1, p0, Landroid/text/TextLine;->mDir:I

    #@2
    const/4 v2, 0x1

    #@3
    if-ne v1, v2, :cond_31

    #@5
    const/4 v1, 0x1

    #@6
    :goto_6
    move/from16 v0, p4

    #@8
    if-ne v1, v0, :cond_33

    #@a
    .line 584
    const/4 v6, 0x0

    #@b
    move-object v1, p0

    #@c
    move/from16 v2, p2

    #@e
    move/from16 v3, p3

    #@10
    move/from16 v4, p3

    #@12
    move/from16 v5, p4

    #@14
    invoke-direct/range {v1 .. v6}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    #@17
    move-result v1

    #@18
    neg-float v13, v1

    #@19
    .line 585
    .local v13, w:F
    add-float v7, p5, v13

    #@1b
    const/4 v11, 0x0

    #@1c
    const/4 v12, 0x0

    #@1d
    move-object v1, p0

    #@1e
    move/from16 v2, p2

    #@20
    move/from16 v3, p3

    #@22
    move/from16 v4, p3

    #@24
    move/from16 v5, p4

    #@26
    move-object v6, p1

    #@27
    move/from16 v8, p6

    #@29
    move/from16 v9, p7

    #@2b
    move/from16 v10, p8

    #@2d
    invoke-direct/range {v1 .. v12}, Landroid/text/TextLine;->handleRun(IIIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    #@30
    .line 590
    .end local v13           #w:F
    :goto_30
    return v13

    #@31
    .line 583
    :cond_31
    const/4 v1, 0x0

    #@32
    goto :goto_6

    #@33
    .line 590
    :cond_33
    const/4 v11, 0x0

    #@34
    move-object v1, p0

    #@35
    move/from16 v2, p2

    #@37
    move/from16 v3, p3

    #@39
    move/from16 v4, p3

    #@3b
    move/from16 v5, p4

    #@3d
    move-object v6, p1

    #@3e
    move/from16 v7, p5

    #@40
    move/from16 v8, p6

    #@42
    move/from16 v9, p7

    #@44
    move/from16 v10, p8

    #@46
    move/from16 v12, p9

    #@48
    invoke-direct/range {v1 .. v12}, Landroid/text/TextLine;->handleRun(IIIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    #@4b
    move-result v13

    #@4c
    goto :goto_30
.end method

.method private drawTextRun(Landroid/graphics/Canvas;Landroid/text/TextPaint;IIIIZFI)V
    .registers 31
    .parameter "c"
    .parameter "wp"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "runIsRtl"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1254
    if-eqz p7, :cond_22

    #@2
    const/4 v9, 0x1

    #@3
    .line 1255
    .local v9, flags:I
    :goto_3
    move-object/from16 v0, p0

    #@5
    iget-boolean v1, v0, Landroid/text/TextLine;->mCharsValid:Z

    #@7
    if-eqz v1, :cond_24

    #@9
    .line 1256
    sub-int v4, p4, p3

    #@b
    .line 1257
    .local v4, count:I
    sub-int v6, p6, p5

    #@d
    .line 1258
    .local v6, contextCount:I
    move-object/from16 v0, p0

    #@f
    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    #@11
    move/from16 v0, p9

    #@13
    int-to-float v8, v0

    #@14
    move-object/from16 v1, p1

    #@16
    move/from16 v3, p3

    #@18
    move/from16 v5, p5

    #@1a
    move/from16 v7, p8

    #@1c
    move-object/from16 v10, p2

    #@1e
    invoke-virtual/range {v1 .. v10}, Landroid/graphics/Canvas;->drawTextRun([CIIIIFFILandroid/graphics/Paint;)V

    #@21
    .line 1265
    .end local v4           #count:I
    .end local v6           #contextCount:I
    :goto_21
    return-void

    #@22
    .line 1254
    .end local v9           #flags:I
    :cond_22
    const/4 v9, 0x0

    #@23
    goto :goto_3

    #@24
    .line 1261
    .restart local v9       #flags:I
    :cond_24
    move-object/from16 v0, p0

    #@26
    iget v0, v0, Landroid/text/TextLine;->mStart:I

    #@28
    move/from16 v20, v0

    #@2a
    .line 1262
    .local v20, delta:I
    move-object/from16 v0, p0

    #@2c
    iget-object v11, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    #@2e
    add-int v12, v20, p3

    #@30
    add-int v13, v20, p4

    #@32
    add-int v14, v20, p5

    #@34
    add-int v15, v20, p6

    #@36
    move/from16 v0, p9

    #@38
    int-to-float v0, v0

    #@39
    move/from16 v17, v0

    #@3b
    move-object/from16 v10, p1

    #@3d
    move/from16 v16, p8

    #@3f
    move/from16 v18, v9

    #@41
    move-object/from16 v19, p2

    #@43
    invoke-virtual/range {v10 .. v19}, Landroid/graphics/Canvas;->drawTextRun(Ljava/lang/CharSequence;IIIIFFILandroid/graphics/Paint;)V

    #@46
    goto :goto_21
.end method

.method private static expandMetricsFromPaint(Landroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V
    .registers 8
    .parameter "fmi"
    .parameter "wp"

    #@0
    .prologue
    .line 877
    iget v1, p0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@2
    .line 878
    .local v1, previousTop:I
    iget v2, p0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@4
    .line 879
    .local v2, previousAscent:I
    iget v3, p0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@6
    .line 880
    .local v3, previousDescent:I
    iget v4, p0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@8
    .line 881
    .local v4, previousBottom:I
    iget v5, p0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    #@a
    .line 883
    .local v5, previousLeading:I
    invoke-virtual {p1, p0}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    #@d
    move-object v0, p0

    #@e
    .line 885
    invoke-static/range {v0 .. v5}, Landroid/text/TextLine;->updateMetrics(Landroid/graphics/Paint$FontMetricsInt;IIIII)V

    #@11
    .line 887
    return-void
.end method

.method private getOffsetBeforeAfter(IIIZIZ)I
    .registers 30
    .parameter "runIndex"
    .parameter "runStart"
    .parameter "runLimit"
    .parameter "runIsRtl"
    .parameter "offset"
    .parameter "after"

    #@0
    .prologue
    .line 810
    if-ltz p1, :cond_c

    #@2
    if-eqz p6, :cond_23

    #@4
    move-object/from16 v0, p0

    #@6
    iget v3, v0, Landroid/text/TextLine;->mLen:I

    #@8
    :goto_8
    move/from16 v0, p5

    #@a
    if-ne v0, v3, :cond_3a

    #@c
    .line 814
    :cond_c
    if-eqz p6, :cond_25

    #@e
    .line 815
    move-object/from16 v0, p0

    #@10
    iget-object v3, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    #@12
    move-object/from16 v0, p0

    #@14
    iget v5, v0, Landroid/text/TextLine;->mStart:I

    #@16
    add-int v5, v5, p5

    #@18
    invoke-static {v3, v5}, Landroid/text/TextUtils;->getOffsetAfter(Ljava/lang/CharSequence;I)I

    #@1b
    move-result v3

    #@1c
    move-object/from16 v0, p0

    #@1e
    iget v5, v0, Landroid/text/TextLine;->mStart:I

    #@20
    sub-int v20, v3, v5

    #@22
    .line 868
    :cond_22
    :goto_22
    return v20

    #@23
    .line 810
    :cond_23
    const/4 v3, 0x0

    #@24
    goto :goto_8

    #@25
    .line 817
    :cond_25
    move-object/from16 v0, p0

    #@27
    iget-object v3, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    #@29
    move-object/from16 v0, p0

    #@2b
    iget v5, v0, Landroid/text/TextLine;->mStart:I

    #@2d
    add-int v5, v5, p5

    #@2f
    invoke-static {v3, v5}, Landroid/text/TextUtils;->getOffsetBefore(Ljava/lang/CharSequence;I)I

    #@32
    move-result v3

    #@33
    move-object/from16 v0, p0

    #@35
    iget v5, v0, Landroid/text/TextLine;->mStart:I

    #@37
    sub-int v20, v3, v5

    #@39
    goto :goto_22

    #@3a
    .line 820
    :cond_3a
    move-object/from16 v0, p0

    #@3c
    iget-object v2, v0, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    #@3e
    .line 821
    .local v2, wp:Landroid/text/TextPaint;
    move-object/from16 v0, p0

    #@40
    iget-object v3, v0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@42
    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    #@45
    .line 823
    move/from16 v4, p2

    #@47
    .line 825
    .local v4, spanStart:I
    move-object/from16 v0, p0

    #@49
    iget-object v3, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@4b
    if-nez v3, :cond_68

    #@4d
    .line 826
    move/from16 v20, p3

    #@4f
    .line 862
    .local v20, spanLimit:I
    :cond_4f
    if-eqz p4, :cond_e7

    #@51
    const/4 v6, 0x1

    #@52
    .line 863
    .local v6, flags:I
    :goto_52
    if-eqz p6, :cond_ea

    #@54
    const/4 v8, 0x0

    #@55
    .line 864
    .local v8, cursorOpt:I
    :goto_55
    move-object/from16 v0, p0

    #@57
    iget-boolean v3, v0, Landroid/text/TextLine;->mCharsValid:Z

    #@59
    if-eqz v3, :cond_ed

    #@5b
    .line 865
    move-object/from16 v0, p0

    #@5d
    iget-object v3, v0, Landroid/text/TextLine;->mChars:[C

    #@5f
    sub-int v5, v20, v4

    #@61
    move/from16 v7, p5

    #@63
    invoke-virtual/range {v2 .. v8}, Landroid/text/TextPaint;->getTextRunCursor([CIIIII)I

    #@66
    move-result v20

    #@67
    goto :goto_22

    #@68
    .line 828
    .end local v6           #flags:I
    .end local v8           #cursorOpt:I
    .end local v20           #spanLimit:I
    :cond_68
    if-eqz p6, :cond_d3

    #@6a
    add-int/lit8 v22, p5, 0x1

    #@6c
    .line 829
    .local v22, target:I
    :goto_6c
    move-object/from16 v0, p0

    #@6e
    iget v3, v0, Landroid/text/TextLine;->mStart:I

    #@70
    add-int v17, v3, p3

    #@72
    .line 831
    .local v17, limit:I
    :goto_72
    move-object/from16 v0, p0

    #@74
    iget-object v3, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@76
    move-object/from16 v0, p0

    #@78
    iget v5, v0, Landroid/text/TextLine;->mStart:I

    #@7a
    add-int/2addr v5, v4

    #@7b
    const-class v7, Landroid/text/style/MetricAffectingSpan;

    #@7d
    move/from16 v0, v17

    #@7f
    invoke-interface {v3, v5, v0, v7}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    #@82
    move-result v3

    #@83
    move-object/from16 v0, p0

    #@85
    iget v5, v0, Landroid/text/TextLine;->mStart:I

    #@87
    sub-int v20, v3, v5

    #@89
    .line 833
    .restart local v20       #spanLimit:I
    move/from16 v0, v20

    #@8b
    move/from16 v1, v22

    #@8d
    if-lt v0, v1, :cond_d6

    #@8f
    .line 839
    move-object/from16 v0, p0

    #@91
    iget-object v3, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@93
    move-object/from16 v0, p0

    #@95
    iget v5, v0, Landroid/text/TextLine;->mStart:I

    #@97
    add-int/2addr v5, v4

    #@98
    move-object/from16 v0, p0

    #@9a
    iget v7, v0, Landroid/text/TextLine;->mStart:I

    #@9c
    add-int v7, v7, v20

    #@9e
    const-class v9, Landroid/text/style/MetricAffectingSpan;

    #@a0
    invoke-interface {v3, v5, v7, v9}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@a3
    move-result-object v21

    #@a4
    check-cast v21, [Landroid/text/style/MetricAffectingSpan;

    #@a6
    .line 841
    .local v21, spans:[Landroid/text/style/MetricAffectingSpan;
    move-object/from16 v0, p0

    #@a8
    iget-object v3, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@aa
    const-class v5, Landroid/text/style/MetricAffectingSpan;

    #@ac
    move-object/from16 v0, v21

    #@ae
    invoke-static {v0, v3, v5}, Landroid/text/TextUtils;->removeEmptySpans([Ljava/lang/Object;Landroid/text/Spanned;Ljava/lang/Class;)[Ljava/lang/Object;

    #@b1
    move-result-object v21

    #@b2
    .end local v21           #spans:[Landroid/text/style/MetricAffectingSpan;
    check-cast v21, [Landroid/text/style/MetricAffectingSpan;

    #@b4
    .line 843
    .restart local v21       #spans:[Landroid/text/style/MetricAffectingSpan;
    move-object/from16 v0, v21

    #@b6
    array-length v3, v0

    #@b7
    if-lez v3, :cond_4f

    #@b9
    .line 844
    const/16 v18, 0x0

    #@bb
    .line 845
    .local v18, replacement:Landroid/text/style/ReplacementSpan;
    const/16 v16, 0x0

    #@bd
    .local v16, j:I
    :goto_bd
    move-object/from16 v0, v21

    #@bf
    array-length v3, v0

    #@c0
    move/from16 v0, v16

    #@c2
    if-ge v0, v3, :cond_df

    #@c4
    .line 846
    aget-object v19, v21, v16

    #@c6
    .line 847
    .local v19, span:Landroid/text/style/MetricAffectingSpan;
    move-object/from16 v0, v19

    #@c8
    instance-of v3, v0, Landroid/text/style/ReplacementSpan;

    #@ca
    if-eqz v3, :cond_d9

    #@cc
    move-object/from16 v18, v19

    #@ce
    .line 848
    check-cast v18, Landroid/text/style/ReplacementSpan;

    #@d0
    .line 845
    :goto_d0
    add-int/lit8 v16, v16, 0x1

    #@d2
    goto :goto_bd

    #@d3
    .end local v16           #j:I
    .end local v17           #limit:I
    .end local v18           #replacement:Landroid/text/style/ReplacementSpan;
    .end local v19           #span:Landroid/text/style/MetricAffectingSpan;
    .end local v20           #spanLimit:I
    .end local v21           #spans:[Landroid/text/style/MetricAffectingSpan;
    .end local v22           #target:I
    :cond_d3
    move/from16 v22, p5

    #@d5
    .line 828
    goto :goto_6c

    #@d6
    .line 836
    .restart local v17       #limit:I
    .restart local v20       #spanLimit:I
    .restart local v22       #target:I
    :cond_d6
    move/from16 v4, v20

    #@d8
    goto :goto_72

    #@d9
    .line 850
    .restart local v16       #j:I
    .restart local v18       #replacement:Landroid/text/style/ReplacementSpan;
    .restart local v19       #span:Landroid/text/style/MetricAffectingSpan;
    .restart local v21       #spans:[Landroid/text/style/MetricAffectingSpan;
    :cond_d9
    move-object/from16 v0, v19

    #@db
    invoke-virtual {v0, v2}, Landroid/text/style/MetricAffectingSpan;->updateMeasureState(Landroid/text/TextPaint;)V

    #@de
    goto :goto_d0

    #@df
    .line 854
    .end local v19           #span:Landroid/text/style/MetricAffectingSpan;
    :cond_df
    if-eqz v18, :cond_4f

    #@e1
    .line 857
    if-nez p6, :cond_22

    #@e3
    move/from16 v20, v4

    #@e5
    goto/16 :goto_22

    #@e7
    .line 862
    .end local v16           #j:I
    .end local v17           #limit:I
    .end local v18           #replacement:Landroid/text/style/ReplacementSpan;
    .end local v21           #spans:[Landroid/text/style/MetricAffectingSpan;
    .end local v22           #target:I
    :cond_e7
    const/4 v6, 0x0

    #@e8
    goto/16 :goto_52

    #@ea
    .line 863
    .restart local v6       #flags:I
    :cond_ea
    const/4 v8, 0x2

    #@eb
    goto/16 :goto_55

    #@ed
    .line 868
    .restart local v8       #cursorOpt:I
    :cond_ed
    move-object/from16 v0, p0

    #@ef
    iget-object v10, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    #@f1
    move-object/from16 v0, p0

    #@f3
    iget v3, v0, Landroid/text/TextLine;->mStart:I

    #@f5
    add-int v11, v3, v4

    #@f7
    move-object/from16 v0, p0

    #@f9
    iget v3, v0, Landroid/text/TextLine;->mStart:I

    #@fb
    add-int v12, v3, v20

    #@fd
    move-object/from16 v0, p0

    #@ff
    iget v3, v0, Landroid/text/TextLine;->mStart:I

    #@101
    add-int v14, v3, p5

    #@103
    move-object v9, v2

    #@104
    move v13, v6

    #@105
    move v15, v8

    #@106
    invoke-virtual/range {v9 .. v15}, Landroid/text/TextPaint;->getTextRunCursor(Ljava/lang/CharSequence;IIIII)I

    #@109
    move-result v3

    #@10a
    move-object/from16 v0, p0

    #@10c
    iget v5, v0, Landroid/text/TextLine;->mStart:I

    #@10e
    sub-int v20, v3, v5

    #@110
    goto/16 :goto_22
.end method

.method private handleReplacement(Landroid/text/style/ReplacementSpan;Landroid/text/TextPaint;IIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F
    .registers 37
    .parameter "replacement"
    .parameter "wp"
    .parameter "start"
    .parameter "limit"
    .parameter "runIsRtl"
    .parameter "c"
    .parameter "x"
    .parameter "top"
    .parameter "y"
    .parameter "bottom"
    .parameter "fmi"
    .parameter "needWidth"

    #@0
    .prologue
    .line 1015
    const/16 v23, 0x0

    #@2
    .line 1017
    .local v23, ret:F
    move-object/from16 v0, p0

    #@4
    iget v1, v0, Landroid/text/TextLine;->mStart:I

    #@6
    add-int v4, v1, p3

    #@8
    .line 1018
    .local v4, textStart:I
    move-object/from16 v0, p0

    #@a
    iget v1, v0, Landroid/text/TextLine;->mStart:I

    #@c
    add-int v5, v1, p4

    #@e
    .line 1020
    .local v5, textLimit:I
    if-nez p12, :cond_14

    #@10
    if-eqz p6, :cond_4b

    #@12
    if-eqz p5, :cond_4b

    #@14
    .line 1021
    :cond_14
    const/4 v7, 0x0

    #@15
    .line 1022
    .local v7, previousTop:I
    const/4 v8, 0x0

    #@16
    .line 1023
    .local v8, previousAscent:I
    const/4 v9, 0x0

    #@17
    .line 1024
    .local v9, previousDescent:I
    const/4 v10, 0x0

    #@18
    .line 1025
    .local v10, previousBottom:I
    const/4 v11, 0x0

    #@19
    .line 1027
    .local v11, previousLeading:I
    if-eqz p11, :cond_71

    #@1b
    const/16 v22, 0x1

    #@1d
    .line 1029
    .local v22, needUpdateMetrics:Z
    :goto_1d
    if-eqz v22, :cond_33

    #@1f
    .line 1030
    move-object/from16 v0, p11

    #@21
    iget v7, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@23
    .line 1031
    move-object/from16 v0, p11

    #@25
    iget v8, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@27
    .line 1032
    move-object/from16 v0, p11

    #@29
    iget v9, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@2b
    .line 1033
    move-object/from16 v0, p11

    #@2d
    iget v10, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@2f
    .line 1034
    move-object/from16 v0, p11

    #@31
    iget v11, v0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    #@33
    .line 1037
    :cond_33
    move-object/from16 v0, p0

    #@35
    iget-object v3, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    #@37
    move-object/from16 v1, p1

    #@39
    move-object/from16 v2, p2

    #@3b
    move-object/from16 v6, p11

    #@3d
    invoke-virtual/range {v1 .. v6}, Landroid/text/style/ReplacementSpan;->getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I

    #@40
    move-result v1

    #@41
    int-to-float v0, v1

    #@42
    move/from16 v23, v0

    #@44
    .line 1039
    if-eqz v22, :cond_4b

    #@46
    move-object/from16 v6, p11

    #@48
    .line 1040
    invoke-static/range {v6 .. v11}, Landroid/text/TextLine;->updateMetrics(Landroid/graphics/Paint$FontMetricsInt;IIIII)V

    #@4b
    .line 1045
    .end local v7           #previousTop:I
    .end local v8           #previousAscent:I
    .end local v9           #previousDescent:I
    .end local v10           #previousBottom:I
    .end local v11           #previousLeading:I
    .end local v22           #needUpdateMetrics:Z
    :cond_4b
    if-eqz p6, :cond_69

    #@4d
    .line 1046
    if-eqz p5, :cond_51

    #@4f
    .line 1047
    sub-float p7, p7, v23

    #@51
    .line 1049
    :cond_51
    move-object/from16 v0, p0

    #@53
    iget-object v14, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    #@55
    move-object/from16 v12, p1

    #@57
    move-object/from16 v13, p6

    #@59
    move v15, v4

    #@5a
    move/from16 v16, v5

    #@5c
    move/from16 v17, p7

    #@5e
    move/from16 v18, p8

    #@60
    move/from16 v19, p9

    #@62
    move/from16 v20, p10

    #@64
    move-object/from16 v21, p2

    #@66
    invoke-virtual/range {v12 .. v21}, Landroid/text/style/ReplacementSpan;->draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V

    #@69
    .line 1053
    :cond_69
    if-eqz p5, :cond_70

    #@6b
    move/from16 v0, v23

    #@6d
    neg-float v0, v0

    #@6e
    move/from16 v23, v0

    #@70
    .end local v23           #ret:F
    :cond_70
    return v23

    #@71
    .line 1027
    .restart local v7       #previousTop:I
    .restart local v8       #previousAscent:I
    .restart local v9       #previousDescent:I
    .restart local v10       #previousBottom:I
    .restart local v11       #previousLeading:I
    .restart local v23       #ret:F
    :cond_71
    const/16 v22, 0x0

    #@73
    goto :goto_1d
.end method

.method private handleRun(IIIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F
    .registers 44
    .parameter "start"
    .parameter "measureLimit"
    .parameter "limit"
    .parameter "runIsRtl"
    .parameter "c"
    .parameter "x"
    .parameter "top"
    .parameter "y"
    .parameter "bottom"
    .parameter "fmi"
    .parameter "needWidth"

    #@0
    .prologue
    .line 1080
    move/from16 v0, p1

    #@2
    move/from16 v1, p2

    #@4
    if-ne v0, v1, :cond_1a

    #@6
    .line 1081
    move-object/from16 v0, p0

    #@8
    iget-object v3, v0, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    #@a
    .line 1082
    .local v3, wp:Landroid/text/TextPaint;
    move-object/from16 v0, p0

    #@c
    iget-object v2, v0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@e
    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    #@11
    .line 1083
    if-eqz p10, :cond_18

    #@13
    .line 1084
    move-object/from16 v0, p10

    #@15
    invoke-static {v0, v3}, Landroid/text/TextLine;->expandMetricsFromPaint(Landroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V

    #@18
    .line 1086
    :cond_18
    const/4 v2, 0x0

    #@19
    .line 1176
    .end local v3           #wp:Landroid/text/TextPaint;
    :goto_19
    return v2

    #@1a
    .line 1089
    :cond_1a
    move-object/from16 v0, p0

    #@1c
    iget-object v2, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@1e
    if-nez v2, :cond_51

    #@20
    .line 1090
    move-object/from16 v0, p0

    #@22
    iget-object v3, v0, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    #@24
    .line 1091
    .restart local v3       #wp:Landroid/text/TextPaint;
    move-object/from16 v0, p0

    #@26
    iget-object v2, v0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@28
    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    #@2b
    .line 1092
    move/from16 v5, p2

    #@2d
    .line 1093
    .local v5, mlimit:I
    if-nez p11, :cond_33

    #@2f
    move/from16 v0, p2

    #@31
    if-ge v5, v0, :cond_4f

    #@33
    :cond_33
    const/4 v15, 0x1

    #@34
    :goto_34
    move-object/from16 v2, p0

    #@36
    move/from16 v4, p1

    #@38
    move/from16 v6, p1

    #@3a
    move/from16 v7, p3

    #@3c
    move/from16 v8, p4

    #@3e
    move-object/from16 v9, p5

    #@40
    move/from16 v10, p6

    #@42
    move/from16 v11, p7

    #@44
    move/from16 v12, p8

    #@46
    move/from16 v13, p9

    #@48
    move-object/from16 v14, p10

    #@4a
    invoke-direct/range {v2 .. v15}, Landroid/text/TextLine;->handleText(Landroid/text/TextPaint;IIIIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    #@4d
    move-result v2

    #@4e
    goto :goto_19

    #@4f
    :cond_4f
    const/4 v15, 0x0

    #@50
    goto :goto_34

    #@51
    .line 1097
    .end local v3           #wp:Landroid/text/TextPaint;
    .end local v5           #mlimit:I
    :cond_51
    move-object/from16 v0, p0

    #@53
    iget-object v2, v0, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    #@55
    move-object/from16 v0, p0

    #@57
    iget-object v4, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@59
    move-object/from16 v0, p0

    #@5b
    iget v6, v0, Landroid/text/TextLine;->mStart:I

    #@5d
    add-int v6, v6, p1

    #@5f
    move-object/from16 v0, p0

    #@61
    iget v8, v0, Landroid/text/TextLine;->mStart:I

    #@63
    add-int v8, v8, p3

    #@65
    invoke-virtual {v2, v4, v6, v8}, Landroid/text/SpanSet;->init(Landroid/text/Spanned;II)V

    #@68
    .line 1098
    move-object/from16 v0, p0

    #@6a
    iget-object v2, v0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    #@6c
    move-object/from16 v0, p0

    #@6e
    iget-object v4, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@70
    move-object/from16 v0, p0

    #@72
    iget v6, v0, Landroid/text/TextLine;->mStart:I

    #@74
    add-int v6, v6, p1

    #@76
    move-object/from16 v0, p0

    #@78
    iget v8, v0, Landroid/text/TextLine;->mStart:I

    #@7a
    add-int v8, v8, p3

    #@7c
    invoke-virtual {v2, v4, v6, v8}, Landroid/text/SpanSet;->init(Landroid/text/Spanned;II)V

    #@7f
    .line 1105
    move/from16 v30, p6

    #@81
    .line 1106
    .local v30, originalX:F
    move/from16 v9, p1

    #@83
    .local v9, i:I
    :goto_83
    move/from16 v0, p2

    #@85
    if-ge v9, v0, :cond_20b

    #@87
    .line 1107
    move-object/from16 v0, p0

    #@89
    iget-object v3, v0, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    #@8b
    .line 1108
    .restart local v3       #wp:Landroid/text/TextPaint;
    move-object/from16 v0, p0

    #@8d
    iget-object v2, v0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@8f
    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    #@92
    .line 1110
    move-object/from16 v0, p0

    #@94
    iget-object v2, v0, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    #@96
    move-object/from16 v0, p0

    #@98
    iget v4, v0, Landroid/text/TextLine;->mStart:I

    #@9a
    add-int/2addr v4, v9

    #@9b
    move-object/from16 v0, p0

    #@9d
    iget v6, v0, Landroid/text/TextLine;->mStart:I

    #@9f
    add-int v6, v6, p3

    #@a1
    invoke-virtual {v2, v4, v6}, Landroid/text/SpanSet;->getNextTransition(II)I

    #@a4
    move-result v2

    #@a5
    move-object/from16 v0, p0

    #@a7
    iget v4, v0, Landroid/text/TextLine;->mStart:I

    #@a9
    sub-int v19, v2, v4

    #@ab
    .line 1112
    .local v19, inext:I
    move/from16 v0, v19

    #@ad
    move/from16 v1, p2

    #@af
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@b2
    move-result v5

    #@b3
    .line 1114
    .restart local v5       #mlimit:I
    const/4 v7, 0x0

    #@b4
    .line 1116
    .local v7, replacement:Landroid/text/style/ReplacementSpan;
    const/16 v16, 0x0

    #@b6
    .local v16, j:I
    move/from16 v28, v16

    #@b8
    .end local v16           #j:I
    .local v28, j:I
    :goto_b8
    move-object/from16 v0, p0

    #@ba
    iget-object v2, v0, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    #@bc
    iget v2, v2, Landroid/text/SpanSet;->numberOfSpans:I

    #@be
    move/from16 v0, v28

    #@c0
    if-ge v0, v2, :cond_100

    #@c2
    .line 1119
    move-object/from16 v0, p0

    #@c4
    iget-object v2, v0, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    #@c6
    iget-object v2, v2, Landroid/text/SpanSet;->spanStarts:[I

    #@c8
    aget v2, v2, v28

    #@ca
    move-object/from16 v0, p0

    #@cc
    iget v4, v0, Landroid/text/TextLine;->mStart:I

    #@ce
    add-int/2addr v4, v5

    #@cf
    if-ge v2, v4, :cond_e0

    #@d1
    move-object/from16 v0, p0

    #@d3
    iget-object v2, v0, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    #@d5
    iget-object v2, v2, Landroid/text/SpanSet;->spanEnds:[I

    #@d7
    aget v2, v2, v28

    #@d9
    move-object/from16 v0, p0

    #@db
    iget v4, v0, Landroid/text/TextLine;->mStart:I

    #@dd
    add-int/2addr v4, v9

    #@de
    if-gt v2, v4, :cond_e5

    #@e0
    .line 1116
    :cond_e0
    :goto_e0
    add-int/lit8 v16, v28, 0x1

    #@e2
    .end local v28           #j:I
    .restart local v16       #j:I
    move/from16 v28, v16

    #@e4
    .end local v16           #j:I
    .restart local v28       #j:I
    goto :goto_b8

    #@e5
    .line 1121
    :cond_e5
    move-object/from16 v0, p0

    #@e7
    iget-object v2, v0, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    #@e9
    iget-object v2, v2, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    #@eb
    check-cast v2, [Landroid/text/style/MetricAffectingSpan;

    #@ed
    aget-object v31, v2, v28

    #@ef
    .line 1122
    .local v31, span:Landroid/text/style/MetricAffectingSpan;
    move-object/from16 v0, v31

    #@f1
    instance-of v2, v0, Landroid/text/style/ReplacementSpan;

    #@f3
    if-eqz v2, :cond_fa

    #@f5
    move-object/from16 v7, v31

    #@f7
    .line 1123
    check-cast v7, Landroid/text/style/ReplacementSpan;

    #@f9
    goto :goto_e0

    #@fa
    .line 1127
    :cond_fa
    move-object/from16 v0, v31

    #@fc
    invoke-virtual {v0, v3}, Landroid/text/style/MetricAffectingSpan;->updateDrawState(Landroid/text/TextPaint;)V

    #@ff
    goto :goto_e0

    #@100
    .line 1131
    .end local v31           #span:Landroid/text/style/MetricAffectingSpan;
    :cond_100
    if-eqz v7, :cond_12b

    #@102
    .line 1132
    if-nez p11, :cond_108

    #@104
    move/from16 v0, p2

    #@106
    if-ge v5, v0, :cond_128

    #@108
    :cond_108
    const/16 v18, 0x1

    #@10a
    :goto_10a
    move-object/from16 v6, p0

    #@10c
    move-object v8, v3

    #@10d
    move v10, v5

    #@10e
    move/from16 v11, p4

    #@110
    move-object/from16 v12, p5

    #@112
    move/from16 v13, p6

    #@114
    move/from16 v14, p7

    #@116
    move/from16 v15, p8

    #@118
    move/from16 v16, p9

    #@11a
    move-object/from16 v17, p10

    #@11c
    invoke-direct/range {v6 .. v18}, Landroid/text/TextLine;->handleReplacement(Landroid/text/style/ReplacementSpan;Landroid/text/TextPaint;IIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    #@11f
    move-result v2

    #@120
    add-float p6, p6, v2

    #@122
    move/from16 v16, v28

    #@124
    .line 1106
    .end local v28           #j:I
    .restart local v16       #j:I
    :cond_124
    move/from16 v9, v19

    #@126
    goto/16 :goto_83

    #@128
    .line 1132
    .end local v16           #j:I
    .restart local v28       #j:I
    :cond_128
    const/16 v18, 0x0

    #@12a
    goto :goto_10a

    #@12b
    .line 1142
    :cond_12b
    move v12, v9

    #@12c
    .local v12, n:I
    :goto_12c
    if-ge v12, v5, :cond_17c

    #@12e
    .line 1143
    move-object/from16 v0, p0

    #@130
    iget-object v2, v0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    #@132
    move-object/from16 v0, p0

    #@134
    iget v4, v0, Landroid/text/TextLine;->mStart:I

    #@136
    add-int/2addr v4, v12

    #@137
    move-object/from16 v0, p0

    #@139
    iget v6, v0, Landroid/text/TextLine;->mStart:I

    #@13b
    add-int/2addr v6, v5

    #@13c
    invoke-virtual {v2, v4, v6}, Landroid/text/SpanSet;->getNextTransition(II)I

    #@13f
    move-result v2

    #@140
    move-object/from16 v0, p0

    #@142
    iget v4, v0, Landroid/text/TextLine;->mStart:I

    #@144
    sub-int v13, v2, v4

    #@146
    .line 1146
    .local v13, nnext:I
    const/4 v2, 0x0

    #@147
    move-object/from16 v0, p0

    #@149
    iput v2, v0, Landroid/text/TextLine;->mZWLeftOffset:I

    #@14b
    .line 1147
    const/4 v2, 0x0

    #@14c
    move-object/from16 v0, p0

    #@14e
    iput v2, v0, Landroid/text/TextLine;->mZWRightOffset:I

    #@150
    move-object/from16 v10, p0

    #@152
    move-object v11, v3

    #@153
    move v14, v9

    #@154
    move/from16 v15, v19

    #@156
    move/from16 v16, p4

    #@158
    .line 1148
    invoke-direct/range {v10 .. v16}, Landroid/text/TextLine;->adjustSpanRangeForLiguatures(Landroid/text/TextPaint;IIIIZ)Z

    #@15b
    move-result v2

    #@15c
    if-eqz v2, :cond_17a

    #@15e
    .line 1149
    move-object/from16 v0, p0

    #@160
    iget-object v2, v0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    #@162
    move-object/from16 v0, p0

    #@164
    iget v4, v0, Landroid/text/TextLine;->mZWLeftOffset:I

    #@166
    move-object/from16 v0, p0

    #@168
    iget v6, v0, Landroid/text/TextLine;->mZWRightOffset:I

    #@16a
    invoke-virtual {v2, v12, v13, v4, v6}, Landroid/text/SpanSet;->updateTransition(IIII)Z

    #@16d
    move-result v2

    #@16e
    if-eqz v2, :cond_17a

    #@170
    .line 1151
    move-object/from16 v0, p0

    #@172
    iget v2, v0, Landroid/text/TextLine;->mZWLeftOffset:I

    #@174
    sub-int/2addr v12, v2

    #@175
    .line 1152
    move-object/from16 v0, p0

    #@177
    iget v2, v0, Landroid/text/TextLine;->mZWRightOffset:I

    #@179
    add-int/2addr v13, v2

    #@17a
    .line 1142
    :cond_17a
    move v12, v13

    #@17b
    goto :goto_12c

    #@17c
    .line 1157
    .end local v13           #nnext:I
    :cond_17c
    move/from16 v16, v9

    #@17e
    .end local v28           #j:I
    .restart local v16       #j:I
    :goto_17e
    move/from16 v0, v16

    #@180
    if-ge v0, v5, :cond_124

    #@182
    .line 1158
    move-object/from16 v0, p0

    #@184
    iget-object v2, v0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    #@186
    move-object/from16 v0, p0

    #@188
    iget v4, v0, Landroid/text/TextLine;->mStart:I

    #@18a
    add-int v4, v4, v16

    #@18c
    move-object/from16 v0, p0

    #@18e
    iget v6, v0, Landroid/text/TextLine;->mStart:I

    #@190
    add-int/2addr v6, v5

    #@191
    invoke-virtual {v2, v4, v6}, Landroid/text/SpanSet;->getNextTransition(II)I

    #@194
    move-result v2

    #@195
    move-object/from16 v0, p0

    #@197
    iget v4, v0, Landroid/text/TextLine;->mStart:I

    #@199
    sub-int v17, v2, v4

    #@19b
    .line 1161
    .local v17, jnext:I
    move-object/from16 v0, p0

    #@19d
    iget-object v2, v0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@19f
    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    #@1a2
    .line 1162
    const/16 v29, 0x0

    #@1a4
    .local v29, k:I
    :goto_1a4
    move-object/from16 v0, p0

    #@1a6
    iget-object v2, v0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    #@1a8
    iget v2, v2, Landroid/text/SpanSet;->numberOfSpans:I

    #@1aa
    move/from16 v0, v29

    #@1ac
    if-ge v0, v2, :cond_1e1

    #@1ae
    .line 1164
    move-object/from16 v0, p0

    #@1b0
    iget-object v2, v0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    #@1b2
    iget-object v2, v2, Landroid/text/SpanSet;->spanStarts:[I

    #@1b4
    aget v2, v2, v29

    #@1b6
    move-object/from16 v0, p0

    #@1b8
    iget v4, v0, Landroid/text/TextLine;->mStart:I

    #@1ba
    add-int v4, v4, v17

    #@1bc
    if-ge v2, v4, :cond_1ce

    #@1be
    move-object/from16 v0, p0

    #@1c0
    iget-object v2, v0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    #@1c2
    iget-object v2, v2, Landroid/text/SpanSet;->spanEnds:[I

    #@1c4
    aget v2, v2, v29

    #@1c6
    move-object/from16 v0, p0

    #@1c8
    iget v4, v0, Landroid/text/TextLine;->mStart:I

    #@1ca
    add-int v4, v4, v16

    #@1cc
    if-gt v2, v4, :cond_1d1

    #@1ce
    .line 1162
    :cond_1ce
    :goto_1ce
    add-int/lit8 v29, v29, 0x1

    #@1d0
    goto :goto_1a4

    #@1d1
    .line 1167
    :cond_1d1
    move-object/from16 v0, p0

    #@1d3
    iget-object v2, v0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    #@1d5
    iget-object v2, v2, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    #@1d7
    check-cast v2, [Landroid/text/style/CharacterStyle;

    #@1d9
    aget-object v31, v2, v29

    #@1db
    .line 1168
    .local v31, span:Landroid/text/style/CharacterStyle;
    move-object/from16 v0, v31

    #@1dd
    invoke-virtual {v0, v3}, Landroid/text/style/CharacterStyle;->updateDrawState(Landroid/text/TextPaint;)V

    #@1e0
    goto :goto_1ce

    #@1e1
    .line 1171
    .end local v31           #span:Landroid/text/style/CharacterStyle;
    :cond_1e1
    if-nez p11, :cond_1e9

    #@1e3
    move/from16 v0, v17

    #@1e5
    move/from16 v1, p2

    #@1e7
    if-ge v0, v1, :cond_208

    #@1e9
    :cond_1e9
    const/16 v27, 0x1

    #@1eb
    :goto_1eb
    move-object/from16 v14, p0

    #@1ed
    move-object v15, v3

    #@1ee
    move/from16 v18, v9

    #@1f0
    move/from16 v20, p4

    #@1f2
    move-object/from16 v21, p5

    #@1f4
    move/from16 v22, p6

    #@1f6
    move/from16 v23, p7

    #@1f8
    move/from16 v24, p8

    #@1fa
    move/from16 v25, p9

    #@1fc
    move-object/from16 v26, p10

    #@1fe
    invoke-direct/range {v14 .. v27}, Landroid/text/TextLine;->handleText(Landroid/text/TextPaint;IIIIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    #@201
    move-result v2

    #@202
    add-float p6, p6, v2

    #@204
    .line 1157
    move/from16 v16, v17

    #@206
    goto/16 :goto_17e

    #@208
    .line 1171
    :cond_208
    const/16 v27, 0x0

    #@20a
    goto :goto_1eb

    #@20b
    .line 1176
    .end local v3           #wp:Landroid/text/TextPaint;
    .end local v5           #mlimit:I
    .end local v7           #replacement:Landroid/text/style/ReplacementSpan;
    .end local v12           #n:I
    .end local v16           #j:I
    .end local v17           #jnext:I
    .end local v19           #inext:I
    .end local v29           #k:I
    :cond_20b
    sub-float v2, p6, v30

    #@20d
    goto/16 :goto_19
.end method

.method private handleText(Landroid/text/TextPaint;IIIIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F
    .registers 41
    .parameter "wp"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "runIsRtl"
    .parameter "c"
    .parameter "x"
    .parameter "top"
    .parameter "y"
    .parameter "bottom"
    .parameter "fmi"
    .parameter "needWidth"

    #@0
    .prologue
    .line 922
    if-eqz p12, :cond_9

    #@2
    .line 923
    move-object/from16 v0, p12

    #@4
    move-object/from16 v1, p1

    #@6
    invoke-static {v0, v1}, Landroid/text/TextLine;->expandMetricsFromPaint(Landroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V

    #@9
    .line 926
    :cond_9
    sub-int v5, p3, p2

    #@b
    .line 928
    .local v5, runLen:I
    if-nez v5, :cond_10

    #@d
    .line 929
    const/16 v26, 0x0

    #@f
    .line 988
    :cond_f
    :goto_f
    return v26

    #@10
    .line 932
    :cond_10
    const/16 v26, 0x0

    #@12
    .line 934
    .local v26, ret:F
    sub-int v7, p5, p4

    #@14
    .line 935
    .local v7, contextLen:I
    if-nez p13, :cond_26

    #@16
    if-eqz p7, :cond_3f

    #@18
    move-object/from16 v0, p1

    #@1a
    iget v2, v0, Landroid/text/TextPaint;->bgColor:I

    #@1c
    if-nez v2, :cond_26

    #@1e
    move-object/from16 v0, p1

    #@20
    iget v2, v0, Landroid/text/TextPaint;->underlineColor:I

    #@22
    if-nez v2, :cond_26

    #@24
    if-eqz p6, :cond_3f

    #@26
    .line 936
    :cond_26
    if-eqz p6, :cond_105

    #@28
    const/4 v8, 0x1

    #@29
    .line 937
    .local v8, flags:I
    :goto_29
    move-object/from16 v0, p0

    #@2b
    iget-boolean v2, v0, Landroid/text/TextLine;->mCharsValid:Z

    #@2d
    if-eqz v2, :cond_108

    #@2f
    .line 938
    move-object/from16 v0, p0

    #@31
    iget-object v3, v0, Landroid/text/TextLine;->mChars:[C

    #@33
    const/4 v9, 0x0

    #@34
    const/4 v10, 0x0

    #@35
    move-object/from16 v2, p1

    #@37
    move/from16 v4, p2

    #@39
    move/from16 v6, p4

    #@3b
    invoke-virtual/range {v2 .. v10}, Landroid/text/TextPaint;->getTextRunAdvances([CIIIII[FI)F

    #@3e
    move-result v26

    #@3f
    .line 948
    .end local v8           #flags:I
    :cond_3f
    :goto_3f
    if-eqz p7, :cond_fc

    #@41
    .line 949
    if-eqz p6, :cond_45

    #@43
    .line 950
    sub-float p8, p8, v26

    #@45
    .line 953
    :cond_45
    move-object/from16 v0, p1

    #@47
    iget v2, v0, Landroid/text/TextPaint;->bgColor:I

    #@49
    if-eqz v2, :cond_82

    #@4b
    .line 954
    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getColor()I

    #@4e
    move-result v24

    #@4f
    .line 955
    .local v24, previousColor:I
    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getStyle()Landroid/graphics/Paint$Style;

    #@52
    move-result-object v25

    #@53
    .line 957
    .local v25, previousStyle:Landroid/graphics/Paint$Style;
    move-object/from16 v0, p1

    #@55
    iget v2, v0, Landroid/text/TextPaint;->bgColor:I

    #@57
    move-object/from16 v0, p1

    #@59
    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setColor(I)V

    #@5c
    .line 958
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@5e
    move-object/from16 v0, p1

    #@60
    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@63
    .line 959
    move/from16 v0, p9

    #@65
    int-to-float v11, v0

    #@66
    add-float v12, p8, v26

    #@68
    move/from16 v0, p11

    #@6a
    int-to-float v13, v0

    #@6b
    move-object/from16 v9, p7

    #@6d
    move/from16 v10, p8

    #@6f
    move-object/from16 v14, p1

    #@71
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@74
    .line 961
    move-object/from16 v0, p1

    #@76
    move-object/from16 v1, v25

    #@78
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@7b
    .line 962
    move-object/from16 v0, p1

    #@7d
    move/from16 v1, v24

    #@7f
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    #@82
    .line 965
    .end local v24           #previousColor:I
    .end local v25           #previousStyle:Landroid/graphics/Paint$Style;
    :cond_82
    move-object/from16 v0, p1

    #@84
    iget v2, v0, Landroid/text/TextPaint;->underlineColor:I

    #@86
    if-eqz v2, :cond_e1

    #@88
    .line 967
    move-object/from16 v0, p1

    #@8a
    iget v2, v0, Landroid/text/TextPaint;->baselineShift:I

    #@8c
    add-int v2, v2, p10

    #@8e
    int-to-float v2, v2

    #@8f
    const v3, 0x3de38e39

    #@92
    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getTextSize()F

    #@95
    move-result v4

    #@96
    mul-float/2addr v3, v4

    #@97
    add-float v11, v2, v3

    #@99
    .line 969
    .local v11, underlineTop:F
    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getColor()I

    #@9c
    move-result v24

    #@9d
    .line 970
    .restart local v24       #previousColor:I
    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getStyle()Landroid/graphics/Paint$Style;

    #@a0
    move-result-object v25

    #@a1
    .line 971
    .restart local v25       #previousStyle:Landroid/graphics/Paint$Style;
    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->isAntiAlias()Z

    #@a4
    move-result v23

    #@a5
    .line 973
    .local v23, previousAntiAlias:Z
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@a7
    move-object/from16 v0, p1

    #@a9
    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@ac
    .line 974
    const/4 v2, 0x1

    #@ad
    move-object/from16 v0, p1

    #@af
    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    #@b2
    .line 976
    move-object/from16 v0, p1

    #@b4
    iget v2, v0, Landroid/text/TextPaint;->underlineColor:I

    #@b6
    move-object/from16 v0, p1

    #@b8
    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setColor(I)V

    #@bb
    .line 977
    add-float v12, p8, v26

    #@bd
    move-object/from16 v0, p1

    #@bf
    iget v2, v0, Landroid/text/TextPaint;->underlineThickness:F

    #@c1
    add-float v13, v11, v2

    #@c3
    move-object/from16 v9, p7

    #@c5
    move/from16 v10, p8

    #@c7
    move-object/from16 v14, p1

    #@c9
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@cc
    .line 979
    move-object/from16 v0, p1

    #@ce
    move-object/from16 v1, v25

    #@d0
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@d3
    .line 980
    move-object/from16 v0, p1

    #@d5
    move/from16 v1, v24

    #@d7
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    #@da
    .line 981
    move-object/from16 v0, p1

    #@dc
    move/from16 v1, v23

    #@de
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    #@e1
    .line 984
    .end local v11           #underlineTop:F
    .end local v23           #previousAntiAlias:Z
    .end local v24           #previousColor:I
    .end local v25           #previousStyle:Landroid/graphics/Paint$Style;
    :cond_e1
    move-object/from16 v0, p1

    #@e3
    iget v2, v0, Landroid/text/TextPaint;->baselineShift:I

    #@e5
    add-int v21, p10, v2

    #@e7
    move-object/from16 v12, p0

    #@e9
    move-object/from16 v13, p7

    #@eb
    move-object/from16 v14, p1

    #@ed
    move/from16 v15, p2

    #@ef
    move/from16 v16, p3

    #@f1
    move/from16 v17, p4

    #@f3
    move/from16 v18, p5

    #@f5
    move/from16 v19, p6

    #@f7
    move/from16 v20, p8

    #@f9
    invoke-direct/range {v12 .. v21}, Landroid/text/TextLine;->drawTextRun(Landroid/graphics/Canvas;Landroid/text/TextPaint;IIIIZFI)V

    #@fc
    .line 988
    :cond_fc
    if-eqz p6, :cond_f

    #@fe
    move/from16 v0, v26

    #@100
    neg-float v0, v0

    #@101
    move/from16 v26, v0

    #@103
    goto/16 :goto_f

    #@105
    .line 936
    :cond_105
    const/4 v8, 0x0

    #@106
    goto/16 :goto_29

    #@108
    .line 941
    .restart local v8       #flags:I
    :cond_108
    move-object/from16 v0, p0

    #@10a
    iget v0, v0, Landroid/text/TextLine;->mStart:I

    #@10c
    move/from16 v22, v0

    #@10e
    .line 942
    .local v22, delta:I
    move-object/from16 v0, p0

    #@110
    iget-object v10, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    #@112
    add-int v11, v22, p2

    #@114
    add-int v12, v22, p3

    #@116
    add-int v13, v22, p4

    #@118
    add-int v14, v22, p5

    #@11a
    const/16 v16, 0x0

    #@11c
    const/16 v17, 0x0

    #@11e
    move-object/from16 v9, p1

    #@120
    move v15, v8

    #@121
    invoke-virtual/range {v9 .. v17}, Landroid/text/TextPaint;->getTextRunAdvances(Ljava/lang/CharSequence;IIIII[FI)F

    #@124
    move-result v26

    #@125
    goto/16 :goto_3f
.end method

.method private measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F
    .registers 18
    .parameter "start"
    .parameter "offset"
    .parameter "limit"
    .parameter "runIsRtl"
    .parameter "fmi"

    #@0
    .prologue
    .line 609
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v8, 0x0

    #@4
    const/4 v9, 0x0

    #@5
    const/4 v11, 0x1

    #@6
    move-object v0, p0

    #@7
    move v1, p1

    #@8
    move v2, p2

    #@9
    move v3, p3

    #@a
    move/from16 v4, p4

    #@c
    move-object/from16 v10, p5

    #@e
    invoke-direct/range {v0 .. v11}, Landroid/text/TextLine;->handleRun(IIIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    #@11
    move-result v0

    #@12
    return v0
.end method

.method static obtain()Landroid/text/TextLine;
    .registers 6

    #@0
    .prologue
    .line 90
    sget-object v4, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    #@2
    monitor-enter v4

    #@3
    .line 91
    :try_start_3
    sget-object v3, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    #@5
    array-length v0, v3

    #@6
    .local v0, i:I
    :cond_6
    add-int/lit8 v0, v0, -0x1

    #@8
    if-ltz v0, :cond_1c

    #@a
    .line 92
    sget-object v3, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    #@c
    aget-object v3, v3, v0

    #@e
    if-eqz v3, :cond_6

    #@10
    .line 93
    sget-object v3, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    #@12
    aget-object v1, v3, v0

    #@14
    .line 94
    .local v1, tl:Landroid/text/TextLine;
    sget-object v3, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    #@16
    const/4 v5, 0x0

    #@17
    aput-object v5, v3, v0

    #@19
    .line 95
    monitor-exit v4

    #@1a
    move-object v2, v1

    #@1b
    .line 103
    .end local v1           #tl:Landroid/text/TextLine;
    .local v2, tl:Ljava/lang/Object;
    :goto_1b
    return-object v2

    #@1c
    .line 98
    .end local v2           #tl:Ljava/lang/Object;
    :cond_1c
    monitor-exit v4
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_24

    #@1d
    .line 99
    new-instance v1, Landroid/text/TextLine;

    #@1f
    invoke-direct {v1}, Landroid/text/TextLine;-><init>()V

    #@22
    .restart local v1       #tl:Landroid/text/TextLine;
    move-object v2, v1

    #@23
    .line 103
    .restart local v2       #tl:Ljava/lang/Object;
    goto :goto_1b

    #@24
    .line 98
    .end local v1           #tl:Landroid/text/TextLine;
    .end local v2           #tl:Ljava/lang/Object;
    :catchall_24
    move-exception v3

    #@25
    :try_start_25
    monitor-exit v4
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    #@26
    throw v3
.end method

.method static recycle(Landroid/text/TextLine;)Landroid/text/TextLine;
    .registers 5
    .parameter "tl"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 114
    iput-object v3, p0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    #@3
    .line 115
    iput-object v3, p0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@5
    .line 116
    iput-object v3, p0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    #@7
    .line 118
    iget-object v1, p0, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    #@9
    invoke-virtual {v1}, Landroid/text/SpanSet;->recycle()V

    #@c
    .line 119
    iget-object v1, p0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    #@e
    invoke-virtual {v1}, Landroid/text/SpanSet;->recycle()V

    #@11
    .line 120
    iget-object v1, p0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    #@13
    invoke-virtual {v1}, Landroid/text/SpanSet;->recycle()V

    #@16
    .line 122
    sget-object v2, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    #@18
    monitor-enter v2

    #@19
    .line 123
    const/4 v0, 0x0

    #@1a
    .local v0, i:I
    :goto_1a
    :try_start_1a
    sget-object v1, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    #@1c
    array-length v1, v1

    #@1d
    if-ge v0, v1, :cond_29

    #@1f
    .line 124
    sget-object v1, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    #@21
    aget-object v1, v1, v0

    #@23
    if-nez v1, :cond_2b

    #@25
    .line 125
    sget-object v1, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    #@27
    aput-object p0, v1, v0

    #@29
    .line 129
    :cond_29
    monitor-exit v2

    #@2a
    .line 130
    return-object v3

    #@2b
    .line 123
    :cond_2b
    add-int/lit8 v0, v0, 0x1

    #@2d
    goto :goto_1a

    #@2e
    .line 129
    :catchall_2e
    move-exception v1

    #@2f
    monitor-exit v2
    :try_end_30
    .catchall {:try_start_1a .. :try_end_30} :catchall_2e

    #@30
    throw v1
.end method

.method static updateMetrics(Landroid/graphics/Paint$FontMetricsInt;IIIII)V
    .registers 7
    .parameter "fmi"
    .parameter "previousTop"
    .parameter "previousAscent"
    .parameter "previousDescent"
    .parameter "previousBottom"
    .parameter "previousLeading"

    #@0
    .prologue
    .line 891
    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@2
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    #@5
    move-result v0

    #@6
    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@8
    .line 892
    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@a
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    #@d
    move-result v0

    #@e
    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@10
    .line 893
    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@12
    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@18
    .line 894
    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@1a
    invoke-static {v0, p4}, Ljava/lang/Math;->max(II)I

    #@1d
    move-result v0

    #@1e
    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@20
    .line 895
    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    #@22
    invoke-static {v0, p5}, Ljava/lang/Math;->max(II)I

    #@25
    move-result v0

    #@26
    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    #@28
    .line 896
    return-void
.end method


# virtual methods
.method ascent(I)F
    .registers 11
    .parameter "pos"

    #@0
    .prologue
    .line 1275
    iget-object v6, p0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@2
    if-nez v6, :cond_b

    #@4
    .line 1276
    iget-object v6, p0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@6
    invoke-virtual {v6}, Landroid/text/TextPaint;->ascent()F

    #@9
    move-result v6

    #@a
    .line 1290
    :goto_a
    return v6

    #@b
    .line 1279
    :cond_b
    iget v6, p0, Landroid/text/TextLine;->mStart:I

    #@d
    add-int/2addr p1, v6

    #@e
    .line 1280
    iget-object v6, p0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@10
    add-int/lit8 v7, p1, 0x1

    #@12
    const-class v8, Landroid/text/style/MetricAffectingSpan;

    #@14
    invoke-interface {v6, p1, v7, v8}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@17
    move-result-object v4

    #@18
    check-cast v4, [Landroid/text/style/MetricAffectingSpan;

    #@1a
    .line 1281
    .local v4, spans:[Landroid/text/style/MetricAffectingSpan;
    array-length v6, v4

    #@1b
    if-nez v6, :cond_24

    #@1d
    .line 1282
    iget-object v6, p0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@1f
    invoke-virtual {v6}, Landroid/text/TextPaint;->ascent()F

    #@22
    move-result v6

    #@23
    goto :goto_a

    #@24
    .line 1285
    :cond_24
    iget-object v5, p0, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    #@26
    .line 1286
    .local v5, wp:Landroid/text/TextPaint;
    iget-object v6, p0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@28
    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    #@2b
    .line 1287
    move-object v0, v4

    #@2c
    .local v0, arr$:[Landroid/text/style/MetricAffectingSpan;
    array-length v2, v0

    #@2d
    .local v2, len$:I
    const/4 v1, 0x0

    #@2e
    .local v1, i$:I
    :goto_2e
    if-ge v1, v2, :cond_38

    #@30
    aget-object v3, v0, v1

    #@32
    .line 1288
    .local v3, span:Landroid/text/style/MetricAffectingSpan;
    invoke-virtual {v3, v5}, Landroid/text/style/MetricAffectingSpan;->updateMeasureState(Landroid/text/TextPaint;)V

    #@35
    .line 1287
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_2e

    #@38
    .line 1290
    .end local v3           #span:Landroid/text/style/MetricAffectingSpan;
    :cond_38
    invoke-virtual {v5}, Landroid/text/TextPaint;->ascent()F

    #@3b
    move-result v6

    #@3c
    goto :goto_a
.end method

.method descent(I)F
    .registers 11
    .parameter "pos"

    #@0
    .prologue
    .line 1295
    iget-object v6, p0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@2
    if-nez v6, :cond_b

    #@4
    .line 1296
    iget-object v6, p0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@6
    invoke-virtual {v6}, Landroid/text/TextPaint;->descent()F

    #@9
    move-result v6

    #@a
    .line 1310
    :goto_a
    return v6

    #@b
    .line 1299
    :cond_b
    iget v6, p0, Landroid/text/TextLine;->mStart:I

    #@d
    add-int/2addr p1, v6

    #@e
    .line 1300
    iget-object v6, p0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@10
    add-int/lit8 v7, p1, 0x1

    #@12
    const-class v8, Landroid/text/style/MetricAffectingSpan;

    #@14
    invoke-interface {v6, p1, v7, v8}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@17
    move-result-object v4

    #@18
    check-cast v4, [Landroid/text/style/MetricAffectingSpan;

    #@1a
    .line 1301
    .local v4, spans:[Landroid/text/style/MetricAffectingSpan;
    array-length v6, v4

    #@1b
    if-nez v6, :cond_24

    #@1d
    .line 1302
    iget-object v6, p0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@1f
    invoke-virtual {v6}, Landroid/text/TextPaint;->descent()F

    #@22
    move-result v6

    #@23
    goto :goto_a

    #@24
    .line 1305
    :cond_24
    iget-object v5, p0, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    #@26
    .line 1306
    .local v5, wp:Landroid/text/TextPaint;
    iget-object v6, p0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@28
    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    #@2b
    .line 1307
    move-object v0, v4

    #@2c
    .local v0, arr$:[Landroid/text/style/MetricAffectingSpan;
    array-length v2, v0

    #@2d
    .local v2, len$:I
    const/4 v1, 0x0

    #@2e
    .local v1, i$:I
    :goto_2e
    if-ge v1, v2, :cond_38

    #@30
    aget-object v3, v0, v1

    #@32
    .line 1308
    .local v3, span:Landroid/text/style/MetricAffectingSpan;
    invoke-virtual {v3, v5}, Landroid/text/style/MetricAffectingSpan;->updateMeasureState(Landroid/text/TextPaint;)V

    #@35
    .line 1307
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_2e

    #@38
    .line 1310
    .end local v3           #span:Landroid/text/style/MetricAffectingSpan;
    :cond_38
    invoke-virtual {v5}, Landroid/text/TextPaint;->descent()F

    #@3b
    move-result v6

    #@3c
    goto :goto_a
.end method

.method draw(Landroid/graphics/Canvas;FIII)V
    .registers 34
    .parameter "c"
    .parameter "x"
    .parameter "top"
    .parameter "y"
    .parameter "bottom"

    #@0
    .prologue
    .line 211
    move-object/from16 v0, p0

    #@2
    iget-boolean v2, v0, Landroid/text/TextLine;->mHasTabs:Z

    #@4
    if-nez v2, :cond_44

    #@6
    .line 212
    move-object/from16 v0, p0

    #@8
    iget-object v2, v0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    #@a
    sget-object v3, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@c
    if-ne v2, v3, :cond_25

    #@e
    .line 213
    const/4 v4, 0x0

    #@f
    move-object/from16 v0, p0

    #@11
    iget v5, v0, Landroid/text/TextLine;->mLen:I

    #@13
    const/4 v6, 0x0

    #@14
    const/4 v11, 0x0

    #@15
    move-object/from16 v2, p0

    #@17
    move-object/from16 v3, p1

    #@19
    move/from16 v7, p2

    #@1b
    move/from16 v8, p3

    #@1d
    move/from16 v9, p4

    #@1f
    move/from16 v10, p5

    #@21
    invoke-direct/range {v2 .. v11}, Landroid/text/TextLine;->drawRun(Landroid/graphics/Canvas;IIZFIIIZ)F

    #@24
    .line 345
    :cond_24
    :goto_24
    return-void

    #@25
    .line 216
    :cond_25
    move-object/from16 v0, p0

    #@27
    iget-object v2, v0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    #@29
    sget-object v3, Landroid/text/Layout;->DIRS_ALL_RIGHT_TO_LEFT:Landroid/text/Layout$Directions;

    #@2b
    if-ne v2, v3, :cond_44

    #@2d
    .line 217
    const/4 v4, 0x0

    #@2e
    move-object/from16 v0, p0

    #@30
    iget v5, v0, Landroid/text/TextLine;->mLen:I

    #@32
    const/4 v6, 0x1

    #@33
    const/4 v11, 0x0

    #@34
    move-object/from16 v2, p0

    #@36
    move-object/from16 v3, p1

    #@38
    move/from16 v7, p2

    #@3a
    move/from16 v8, p3

    #@3c
    move/from16 v9, p4

    #@3e
    move/from16 v10, p5

    #@40
    invoke-direct/range {v2 .. v11}, Landroid/text/TextLine;->drawRun(Landroid/graphics/Canvas;IIZFIIIZ)F

    #@43
    goto :goto_24

    #@44
    .line 222
    :cond_44
    const/16 v19, 0x0

    #@46
    .line 223
    .local v19, h:F
    move-object/from16 v0, p0

    #@48
    iget-object v2, v0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    #@4a
    iget-object v0, v2, Landroid/text/Layout$Directions;->mDirections:[I

    #@4c
    move-object/from16 v25, v0

    #@4e
    .line 224
    .local v25, runs:[I
    const/16 v18, 0x0

    #@50
    .line 226
    .local v18, emojiRect:Landroid/graphics/RectF;
    move-object/from16 v0, v25

    #@52
    array-length v2, v0

    #@53
    add-int/lit8 v21, v2, -0x2

    #@55
    .line 227
    .local v21, lastRunIndex:I
    const/16 v20, 0x0

    #@57
    .local v20, i:I
    :goto_57
    move-object/from16 v0, v25

    #@59
    array-length v2, v0

    #@5a
    move/from16 v0, v20

    #@5c
    if-ge v0, v2, :cond_24

    #@5e
    .line 228
    aget v24, v25, v20

    #@60
    .line 229
    .local v24, runStart:I
    add-int/lit8 v2, v20, 0x1

    #@62
    aget v2, v25, v2

    #@64
    const v3, 0x3ffffff

    #@67
    and-int/2addr v2, v3

    #@68
    add-int v23, v24, v2

    #@6a
    .line 230
    .local v23, runLimit:I
    move-object/from16 v0, p0

    #@6c
    iget v2, v0, Landroid/text/TextLine;->mLen:I

    #@6e
    move/from16 v0, v23

    #@70
    if-le v0, v2, :cond_78

    #@72
    .line 231
    move-object/from16 v0, p0

    #@74
    iget v0, v0, Landroid/text/TextLine;->mLen:I

    #@76
    move/from16 v23, v0

    #@78
    .line 233
    :cond_78
    add-int/lit8 v2, v20, 0x1

    #@7a
    aget v2, v25, v2

    #@7c
    const/high16 v3, 0x400

    #@7e
    and-int/2addr v2, v3

    #@7f
    if-eqz v2, :cond_141

    #@81
    const/4 v6, 0x1

    #@82
    .line 235
    .local v6, runIsRtl:Z
    :goto_82
    move/from16 v4, v24

    #@84
    .line 236
    .local v4, segstart:I
    move-object/from16 v0, p0

    #@86
    iget-boolean v2, v0, Landroid/text/TextLine;->mHasTabs:Z

    #@88
    if-eqz v2, :cond_144

    #@8a
    move/from16 v5, v24

    #@8c
    .local v5, j:I
    :goto_8c
    move/from16 v0, v23

    #@8e
    if-gt v5, v0, :cond_2ca

    #@90
    .line 237
    const/16 v16, 0x0

    #@92
    .line 238
    .local v16, codept:I
    const/4 v13, 0x0

    #@93
    .line 240
    .local v13, bm:Landroid/graphics/Bitmap;
    const/16 v17, 0x0

    #@95
    .line 241
    .local v17, emojiBytes:I
    const/16 v22, 0x0

    #@97
    .line 243
    .local v22, puaBuf:[I
    move-object/from16 v0, p0

    #@99
    iget-boolean v2, v0, Landroid/text/TextLine;->mHasTabs:Z

    #@9b
    if-eqz v2, :cond_f6

    #@9d
    move/from16 v0, v23

    #@9f
    if-ge v5, v0, :cond_f6

    #@a1
    .line 245
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@a3
    if-eqz v2, :cond_1b5

    #@a5
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@a7
    if-nez v2, :cond_1b5

    #@a9
    .line 246
    sget-object v2, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@ab
    move-object/from16 v0, p0

    #@ad
    iget-object v3, v0, Landroid/text/TextLine;->mChars:[C

    #@af
    aget-char v3, v3, v5

    #@b1
    invoke-virtual {v2, v3}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@b4
    move-result v2

    #@b5
    if-nez v2, :cond_f6

    #@b7
    .line 247
    move-object/from16 v0, p0

    #@b9
    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    #@bb
    invoke-static {v2, v5}, Ljava/lang/Character;->codePointAt([CI)I

    #@be
    move-result v16

    #@bf
    .line 248
    add-int/lit8 v2, v5, 0x3

    #@c1
    move/from16 v0, v23

    #@c3
    if-ge v2, v0, :cond_148

    #@c5
    move-object/from16 v0, p0

    #@c7
    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    #@c9
    invoke-static {v2, v5}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z

    #@cc
    move-result v2

    #@cd
    if-eqz v2, :cond_148

    #@cf
    .line 249
    const/4 v2, 0x2

    #@d0
    new-array v0, v2, [I

    #@d2
    move-object/from16 v22, v0

    #@d4
    .line 250
    const/4 v2, 0x0

    #@d5
    move-object/from16 v0, p0

    #@d7
    iget-object v3, v0, Landroid/text/TextLine;->mChars:[C

    #@d9
    invoke-static {v3, v5}, Ljava/lang/Character;->codePointAt([CI)I

    #@dc
    move-result v3

    #@dd
    aput v3, v22, v2

    #@df
    .line 251
    const/4 v2, 0x1

    #@e0
    move-object/from16 v0, p0

    #@e2
    iget-object v3, v0, Landroid/text/TextLine;->mChars:[C

    #@e4
    add-int/lit8 v7, v5, 0x2

    #@e6
    invoke-static {v3, v7}, Ljava/lang/Character;->codePointAt([CI)I

    #@e9
    move-result v3

    #@ea
    aput v3, v22, v2

    #@ec
    .line 252
    sget-object v2, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@ee
    move-object/from16 v0, v22

    #@f0
    invoke-virtual {v2, v0}, Landroid/emoji/EmojiFactory;->getBitmapFromAndroidPua([I)Landroid/graphics/Bitmap;

    #@f3
    move-result-object v13

    #@f4
    .line 253
    const/16 v17, 0x8

    #@f6
    .line 285
    :cond_f6
    :goto_f6
    move/from16 v0, v23

    #@f8
    if-eq v5, v0, :cond_102

    #@fa
    const/16 v2, 0x9

    #@fc
    move/from16 v0, v16

    #@fe
    if-eq v0, v2, :cond_102

    #@100
    if-eqz v13, :cond_13d

    #@102
    .line 286
    :cond_102
    add-float v7, p2, v19

    #@104
    move/from16 v0, v20

    #@106
    move/from16 v1, v21

    #@108
    if-ne v0, v1, :cond_110

    #@10a
    move-object/from16 v0, p0

    #@10c
    iget v2, v0, Landroid/text/TextLine;->mLen:I

    #@10e
    if-eq v5, v2, :cond_1f8

    #@110
    :cond_110
    const/4 v11, 0x1

    #@111
    :goto_111
    move-object/from16 v2, p0

    #@113
    move-object/from16 v3, p1

    #@115
    move/from16 v8, p3

    #@117
    move/from16 v9, p4

    #@119
    move/from16 v10, p5

    #@11b
    invoke-direct/range {v2 .. v11}, Landroid/text/TextLine;->drawRun(Landroid/graphics/Canvas;IIZFIIIZ)F

    #@11e
    move-result v2

    #@11f
    add-float v19, v19, v2

    #@121
    .line 289
    const/16 v2, 0x9

    #@123
    move/from16 v0, v16

    #@125
    if-ne v0, v2, :cond_1fb

    #@127
    .line 290
    move-object/from16 v0, p0

    #@129
    iget v2, v0, Landroid/text/TextLine;->mDir:I

    #@12b
    int-to-float v2, v2

    #@12c
    move-object/from16 v0, p0

    #@12e
    iget v3, v0, Landroid/text/TextLine;->mDir:I

    #@130
    int-to-float v3, v3

    #@131
    mul-float v3, v3, v19

    #@133
    move-object/from16 v0, p0

    #@135
    invoke-virtual {v0, v3}, Landroid/text/TextLine;->nextTab(F)F

    #@138
    move-result v3

    #@139
    mul-float v19, v2, v3

    #@13b
    .line 341
    :cond_13b
    :goto_13b
    add-int/lit8 v4, v5, 0x1

    #@13d
    .line 236
    :cond_13d
    :goto_13d
    add-int/lit8 v5, v5, 0x1

    #@13f
    goto/16 :goto_8c

    #@141
    .line 233
    .end local v4           #segstart:I
    .end local v5           #j:I
    .end local v6           #runIsRtl:Z
    .end local v13           #bm:Landroid/graphics/Bitmap;
    .end local v16           #codept:I
    .end local v17           #emojiBytes:I
    .end local v22           #puaBuf:[I
    :cond_141
    const/4 v6, 0x0

    #@142
    goto/16 :goto_82

    #@144
    .restart local v4       #segstart:I
    .restart local v6       #runIsRtl:Z
    :cond_144
    move/from16 v5, v23

    #@146
    .line 236
    goto/16 :goto_8c

    #@148
    .line 254
    .restart local v5       #j:I
    .restart local v13       #bm:Landroid/graphics/Bitmap;
    .restart local v16       #codept:I
    .restart local v17       #emojiBytes:I
    .restart local v22       #puaBuf:[I
    :cond_148
    add-int/lit8 v2, v5, 0x1

    #@14a
    move/from16 v0, v23

    #@14c
    if-ge v2, v0, :cond_15f

    #@14e
    invoke-static/range {v16 .. v16}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@151
    move-result v2

    #@152
    if-eqz v2, :cond_15f

    #@154
    .line 255
    sget-object v2, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@156
    move/from16 v0, v16

    #@158
    invoke-virtual {v2, v0}, Landroid/emoji/EmojiFactory;->getBitmapFromAndroidPua(I)Landroid/graphics/Bitmap;

    #@15b
    move-result-object v13

    #@15c
    .line 256
    const/16 v17, 0x4

    #@15e
    goto :goto_f6

    #@15f
    .line 257
    :cond_15f
    move-object/from16 v0, p0

    #@161
    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    #@163
    aget-char v2, v2, v5

    #@165
    invoke-static {v2}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z

    #@168
    move-result v2

    #@169
    if-eqz v2, :cond_176

    #@16b
    .line 258
    sget-object v2, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@16d
    move/from16 v0, v16

    #@16f
    invoke-virtual {v2, v0}, Landroid/emoji/EmojiFactory;->getBitmapFromAndroidPua(I)Landroid/graphics/Bitmap;

    #@172
    move-result-object v13

    #@173
    .line 259
    const/16 v17, 0x2

    #@175
    goto :goto_f6

    #@176
    .line 260
    :cond_176
    add-int/lit8 v2, v5, 0x1

    #@178
    move/from16 v0, v23

    #@17a
    if-ge v2, v0, :cond_1ab

    #@17c
    move-object/from16 v0, p0

    #@17e
    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    #@180
    invoke-static {v2, v5}, Landroid/text/Layout;->isDiacriticalMark([CI)Z

    #@183
    move-result v2

    #@184
    if-eqz v2, :cond_1ab

    #@186
    .line 261
    const/4 v2, 0x2

    #@187
    new-array v0, v2, [I

    #@189
    move-object/from16 v22, v0

    #@18b
    .line 262
    const/4 v2, 0x0

    #@18c
    move-object/from16 v0, p0

    #@18e
    iget-object v3, v0, Landroid/text/TextLine;->mChars:[C

    #@190
    aget-char v3, v3, v5

    #@192
    aput v3, v22, v2

    #@194
    .line 263
    const/4 v2, 0x1

    #@195
    move-object/from16 v0, p0

    #@197
    iget-object v3, v0, Landroid/text/TextLine;->mChars:[C

    #@199
    add-int/lit8 v7, v5, 0x1

    #@19b
    aget-char v3, v3, v7

    #@19d
    aput v3, v22, v2

    #@19f
    .line 264
    sget-object v2, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@1a1
    move-object/from16 v0, v22

    #@1a3
    invoke-virtual {v2, v0}, Landroid/emoji/EmojiFactory;->getBitmapFromAndroidPua([I)Landroid/graphics/Bitmap;

    #@1a6
    move-result-object v13

    #@1a7
    .line 265
    const/16 v17, 0x4

    #@1a9
    goto/16 :goto_f6

    #@1ab
    .line 266
    :cond_1ab
    const v2, 0xffff

    #@1ae
    move/from16 v0, v16

    #@1b0
    if-le v0, v2, :cond_f6

    #@1b2
    .line 267
    add-int/lit8 v5, v5, 0x1

    #@1b4
    .line 268
    goto :goto_13d

    #@1b5
    .line 272
    :cond_1b5
    move-object/from16 v0, p0

    #@1b7
    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    #@1b9
    aget-char v16, v2, v5

    #@1bb
    .line 273
    const v2, 0xd800

    #@1be
    move/from16 v0, v16

    #@1c0
    if-lt v0, v2, :cond_f6

    #@1c2
    const v2, 0xdc00

    #@1c5
    move/from16 v0, v16

    #@1c7
    if-ge v0, v2, :cond_f6

    #@1c9
    add-int/lit8 v2, v5, 0x1

    #@1cb
    move/from16 v0, v23

    #@1cd
    if-ge v2, v0, :cond_f6

    #@1cf
    .line 274
    move-object/from16 v0, p0

    #@1d1
    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    #@1d3
    invoke-static {v2, v5}, Ljava/lang/Character;->codePointAt([CI)I

    #@1d6
    move-result v16

    #@1d7
    .line 275
    sget v2, Landroid/text/Layout;->MIN_EMOJI:I

    #@1d9
    move/from16 v0, v16

    #@1db
    if-lt v0, v2, :cond_1ed

    #@1dd
    sget v2, Landroid/text/Layout;->MAX_EMOJI:I

    #@1df
    move/from16 v0, v16

    #@1e1
    if-gt v0, v2, :cond_1ed

    #@1e3
    .line 276
    sget-object v2, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@1e5
    move/from16 v0, v16

    #@1e7
    invoke-virtual {v2, v0}, Landroid/emoji/EmojiFactory;->getBitmapFromAndroidPua(I)Landroid/graphics/Bitmap;

    #@1ea
    move-result-object v13

    #@1eb
    goto/16 :goto_f6

    #@1ed
    .line 277
    :cond_1ed
    const v2, 0xffff

    #@1f0
    move/from16 v0, v16

    #@1f2
    if-le v0, v2, :cond_f6

    #@1f4
    .line 278
    add-int/lit8 v5, v5, 0x1

    #@1f6
    .line 279
    goto/16 :goto_13d

    #@1f8
    .line 286
    :cond_1f8
    const/4 v11, 0x0

    #@1f9
    goto/16 :goto_111

    #@1fb
    .line 291
    :cond_1fb
    if-eqz v13, :cond_13b

    #@1fd
    .line 292
    move-object/from16 v0, p0

    #@1ff
    invoke-virtual {v0, v5}, Landroid/text/TextLine;->ascent(I)F

    #@202
    move-result v14

    #@203
    .line 293
    .local v14, bmAscent:F
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    #@206
    move-result v2

    #@207
    int-to-float v12, v2

    #@208
    .line 295
    .local v12, bitmapHeight:F
    move-object/from16 v0, p0

    #@20a
    invoke-virtual {v0, v5}, Landroid/text/TextLine;->descent(I)F

    #@20d
    move-result v15

    #@20e
    .line 299
    .local v15, bmDescent:F
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@210
    if-eqz v2, :cond_26e

    #@212
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@214
    if-nez v2, :cond_26e

    #@216
    .line 300
    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    #@219
    move-result v2

    #@21a
    int-to-float v14, v2

    #@21b
    .line 301
    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    #@21e
    move-result v2

    #@21f
    int-to-float v15, v2

    #@220
    .line 302
    neg-float v2, v14

    #@221
    add-float/2addr v2, v15

    #@222
    div-float v26, v2, v12

    #@224
    .line 303
    .local v26, scale:F
    move-object/from16 v0, p0

    #@226
    iget v2, v0, Landroid/text/TextLine;->mDir:I

    #@228
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    #@22b
    move-result v3

    #@22c
    mul-int/2addr v2, v3

    #@22d
    int-to-float v2, v2

    #@22e
    mul-float v27, v2, v26

    #@230
    .line 309
    .local v27, width:F
    :goto_230
    if-nez v18, :cond_237

    #@232
    .line 310
    new-instance v18, Landroid/graphics/RectF;

    #@234
    .end local v18           #emojiRect:Landroid/graphics/RectF;
    invoke-direct/range {v18 .. v18}, Landroid/graphics/RectF;-><init>()V

    #@237
    .line 313
    .restart local v18       #emojiRect:Landroid/graphics/RectF;
    :cond_237
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@239
    if-eqz v2, :cond_283

    #@23b
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@23d
    if-nez v2, :cond_283

    #@23f
    .line 314
    add-float v2, p2, v19

    #@241
    move/from16 v0, p4

    #@243
    int-to-float v3, v0

    #@244
    add-float/2addr v3, v14

    #@245
    add-float v7, p2, v19

    #@247
    add-float v7, v7, v27

    #@249
    move/from16 v0, p4

    #@24b
    int-to-float v8, v0

    #@24c
    add-float/2addr v8, v15

    #@24d
    move-object/from16 v0, v18

    #@24f
    invoke-virtual {v0, v2, v3, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    #@252
    .line 317
    if-eqz v6, :cond_257

    #@254
    .line 318
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/RectF;->sort()V

    #@257
    .line 320
    :cond_257
    const/4 v2, 0x0

    #@258
    move-object/from16 v0, p0

    #@25a
    iget-object v3, v0, Landroid/text/TextLine;->mEmojiPaint:Landroid/graphics/Paint;

    #@25c
    move-object/from16 v0, p1

    #@25e
    move-object/from16 v1, v18

    #@260
    invoke-virtual {v0, v13, v2, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@263
    .line 321
    add-float v19, v19, v27

    #@265
    .line 322
    const/4 v2, 0x4

    #@266
    move/from16 v0, v17

    #@268
    if-ne v0, v2, :cond_279

    #@26a
    .line 323
    add-int/lit8 v5, v5, 0x1

    #@26c
    goto/16 :goto_13b

    #@26e
    .line 305
    .end local v26           #scale:F
    .end local v27           #width:F
    :cond_26e
    neg-float v2, v14

    #@26f
    div-float v26, v2, v12

    #@271
    .line 306
    .restart local v26       #scale:F
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    #@274
    move-result v2

    #@275
    int-to-float v2, v2

    #@276
    mul-float v27, v2, v26

    #@278
    .restart local v27       #width:F
    goto :goto_230

    #@279
    .line 324
    :cond_279
    const/16 v2, 0x8

    #@27b
    move/from16 v0, v17

    #@27d
    if-ne v0, v2, :cond_13b

    #@27f
    .line 325
    add-int/lit8 v5, v5, 0x3

    #@281
    goto/16 :goto_13b

    #@283
    .line 329
    :cond_283
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@285
    if-eqz v2, :cond_2b7

    #@287
    move-object/from16 v0, p0

    #@289
    iget v2, v0, Landroid/text/TextLine;->mDir:I

    #@28b
    const/4 v3, -0x1

    #@28c
    if-ne v2, v3, :cond_2b7

    #@28e
    .line 330
    move/from16 v0, v27

    #@290
    neg-float v0, v0

    #@291
    move/from16 v27, v0

    #@293
    .line 331
    add-float v2, p2, v19

    #@295
    add-float v2, v2, v27

    #@297
    move/from16 v0, p4

    #@299
    int-to-float v3, v0

    #@29a
    add-float/2addr v3, v14

    #@29b
    add-float v7, p2, v19

    #@29d
    move/from16 v0, p4

    #@29f
    int-to-float v8, v0

    #@2a0
    move-object/from16 v0, v18

    #@2a2
    invoke-virtual {v0, v2, v3, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    #@2a5
    .line 336
    :goto_2a5
    const/4 v2, 0x0

    #@2a6
    move-object/from16 v0, p0

    #@2a8
    iget-object v3, v0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@2aa
    move-object/from16 v0, p1

    #@2ac
    move-object/from16 v1, v18

    #@2ae
    invoke-virtual {v0, v13, v2, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@2b1
    .line 337
    add-float v19, v19, v27

    #@2b3
    .line 338
    add-int/lit8 v5, v5, 0x1

    #@2b5
    goto/16 :goto_13b

    #@2b7
    .line 333
    :cond_2b7
    add-float v2, p2, v19

    #@2b9
    move/from16 v0, p4

    #@2bb
    int-to-float v3, v0

    #@2bc
    add-float/2addr v3, v14

    #@2bd
    add-float v7, p2, v19

    #@2bf
    add-float v7, v7, v27

    #@2c1
    move/from16 v0, p4

    #@2c3
    int-to-float v8, v0

    #@2c4
    move-object/from16 v0, v18

    #@2c6
    invoke-virtual {v0, v2, v3, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    #@2c9
    goto :goto_2a5

    #@2ca
    .line 227
    .end local v12           #bitmapHeight:F
    .end local v13           #bm:Landroid/graphics/Bitmap;
    .end local v14           #bmAscent:F
    .end local v15           #bmDescent:F
    .end local v16           #codept:I
    .end local v17           #emojiBytes:I
    .end local v22           #puaBuf:[I
    .end local v26           #scale:F
    .end local v27           #width:F
    :cond_2ca
    add-int/lit8 v20, v20, 0x2

    #@2cc
    goto/16 :goto_57
.end method

.method getOffsetToLeftRightOf(IZ)I
    .registers 33
    .parameter "cursor"
    .parameter "toLeft"

    #@0
    .prologue
    .line 642
    const/16 v17, 0x0

    #@2
    .line 643
    .local v17, lineStart:I
    move-object/from16 v0, p0

    #@4
    iget v0, v0, Landroid/text/TextLine;->mLen:I

    #@6
    move/from16 v16, v0

    #@8
    .line 644
    .local v16, lineEnd:I
    move-object/from16 v0, p0

    #@a
    iget v2, v0, Landroid/text/TextLine;->mDir:I

    #@c
    const/4 v7, -0x1

    #@d
    if-ne v2, v7, :cond_7c

    #@f
    const/16 v21, 0x1

    #@11
    .line 645
    .local v21, paraIsRtl:Z
    :goto_11
    move-object/from16 v0, p0

    #@13
    iget-object v2, v0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    #@15
    iget-object v0, v2, Landroid/text/Layout$Directions;->mDirections:[I

    #@17
    move-object/from16 v28, v0

    #@19
    .line 647
    .local v28, runs:[I
    const/16 v27, 0x0

    #@1b
    .local v27, runLevel:I
    move/from16 v4, v17

    #@1d
    .local v4, runStart:I
    move/from16 v5, v16

    #@1f
    .local v5, runLimit:I
    const/16 v18, -0x1

    #@21
    .line 648
    .local v18, newCaret:I
    const/16 v29, 0x0

    #@23
    .line 650
    .local v29, trailing:Z
    move/from16 v0, p1

    #@25
    move/from16 v1, v17

    #@27
    if-ne v0, v1, :cond_7f

    #@29
    .line 651
    const/4 v3, -0x2

    #@2a
    .line 729
    .local v3, runIndex:I
    :cond_2a
    :goto_2a
    move/from16 v0, p2

    #@2c
    move/from16 v1, v21

    #@2e
    if-ne v0, v1, :cond_137

    #@30
    const/4 v8, 0x1

    #@31
    .line 730
    .local v8, advance:Z
    :goto_31
    if-eqz v8, :cond_13a

    #@33
    const/4 v2, 0x2

    #@34
    :goto_34
    add-int v10, v3, v2

    #@36
    .line 731
    .local v10, otherRunIndex:I
    if-ltz v10, :cond_159

    #@38
    move-object/from16 v0, v28

    #@3a
    array-length v2, v0

    #@3b
    if-ge v10, v2, :cond_159

    #@3d
    .line 732
    aget v2, v28, v10

    #@3f
    add-int v11, v17, v2

    #@41
    .line 733
    .local v11, otherRunStart:I
    add-int/lit8 v2, v10, 0x1

    #@43
    aget v2, v28, v2

    #@45
    const v7, 0x3ffffff

    #@48
    and-int/2addr v2, v7

    #@49
    add-int v12, v11, v2

    #@4b
    .line 735
    .local v12, otherRunLimit:I
    move/from16 v0, v16

    #@4d
    if-le v12, v0, :cond_51

    #@4f
    .line 736
    move/from16 v12, v16

    #@51
    .line 738
    :cond_51
    add-int/lit8 v2, v10, 0x1

    #@53
    aget v2, v28, v2

    #@55
    ushr-int/lit8 v2, v2, 0x1a

    #@57
    and-int/lit8 v20, v2, 0x3f

    #@59
    .line 740
    .local v20, otherRunLevel:I
    and-int/lit8 v2, v20, 0x1

    #@5b
    if-eqz v2, :cond_13d

    #@5d
    const/4 v13, 0x1

    #@5e
    .line 742
    .local v13, otherRunIsRtl:Z
    :goto_5e
    move/from16 v0, p2

    #@60
    if-ne v0, v13, :cond_140

    #@62
    const/4 v8, 0x1

    #@63
    .line 743
    :goto_63
    const/4 v2, -0x1

    #@64
    move/from16 v0, v18

    #@66
    if-ne v0, v2, :cond_149

    #@68
    .line 744
    if-eqz v8, :cond_143

    #@6a
    move v14, v11

    #@6b
    :goto_6b
    move-object/from16 v9, p0

    #@6d
    move v15, v8

    #@6e
    invoke-direct/range {v9 .. v15}, Landroid/text/TextLine;->getOffsetBeforeAfter(IIIZIZ)I

    #@71
    move-result v18

    #@72
    .line 747
    if-eqz v8, :cond_146

    #@74
    .end local v12           #otherRunLimit:I
    :goto_74
    move/from16 v0, v18

    #@76
    if-ne v0, v12, :cond_153

    #@78
    .line 750
    move v3, v10

    #@79
    .line 751
    move/from16 v27, v20

    #@7b
    .line 752
    goto :goto_2a

    #@7c
    .line 644
    .end local v3           #runIndex:I
    .end local v4           #runStart:I
    .end local v5           #runLimit:I
    .end local v8           #advance:Z
    .end local v10           #otherRunIndex:I
    .end local v11           #otherRunStart:I
    .end local v13           #otherRunIsRtl:Z
    .end local v18           #newCaret:I
    .end local v20           #otherRunLevel:I
    .end local v21           #paraIsRtl:Z
    .end local v27           #runLevel:I
    .end local v28           #runs:[I
    .end local v29           #trailing:Z
    :cond_7c
    const/16 v21, 0x0

    #@7e
    goto :goto_11

    #@7f
    .line 652
    .restart local v4       #runStart:I
    .restart local v5       #runLimit:I
    .restart local v18       #newCaret:I
    .restart local v21       #paraIsRtl:Z
    .restart local v27       #runLevel:I
    .restart local v28       #runs:[I
    .restart local v29       #trailing:Z
    :cond_7f
    move/from16 v0, p1

    #@81
    move/from16 v1, v16

    #@83
    if-ne v0, v1, :cond_89

    #@85
    .line 653
    move-object/from16 v0, v28

    #@87
    array-length v3, v0

    #@88
    .restart local v3       #runIndex:I
    goto :goto_2a

    #@89
    .line 657
    .end local v3           #runIndex:I
    :cond_89
    const/4 v3, 0x0

    #@8a
    .restart local v3       #runIndex:I
    :goto_8a
    move-object/from16 v0, v28

    #@8c
    array-length v2, v0

    #@8d
    if-ge v3, v2, :cond_fc

    #@8f
    .line 658
    aget v2, v28, v3

    #@91
    add-int v4, v17, v2

    #@93
    .line 659
    move/from16 v0, p1

    #@95
    if-lt v0, v4, :cond_12b

    #@97
    .line 660
    add-int/lit8 v2, v3, 0x1

    #@99
    aget v2, v28, v2

    #@9b
    const v7, 0x3ffffff

    #@9e
    and-int/2addr v2, v7

    #@9f
    add-int v5, v4, v2

    #@a1
    .line 661
    move/from16 v0, v16

    #@a3
    if-le v5, v0, :cond_a7

    #@a5
    .line 662
    move/from16 v5, v16

    #@a7
    .line 664
    :cond_a7
    move/from16 v0, p1

    #@a9
    if-ge v0, v5, :cond_12b

    #@ab
    .line 665
    add-int/lit8 v2, v3, 0x1

    #@ad
    aget v2, v28, v2

    #@af
    ushr-int/lit8 v2, v2, 0x1a

    #@b1
    and-int/lit8 v27, v2, 0x3f

    #@b3
    .line 667
    move/from16 v0, p1

    #@b5
    if-ne v0, v4, :cond_fc

    #@b7
    .line 672
    add-int/lit8 v22, p1, -0x1

    #@b9
    .line 673
    .local v22, pos:I
    const/16 v23, 0x0

    #@bb
    .local v23, prevRunIndex:I
    :goto_bb
    move-object/from16 v0, v28

    #@bd
    array-length v2, v0

    #@be
    move/from16 v0, v23

    #@c0
    if-ge v0, v2, :cond_fc

    #@c2
    .line 674
    aget v2, v28, v23

    #@c4
    add-int v26, v17, v2

    #@c6
    .line 675
    .local v26, prevRunStart:I
    move/from16 v0, v22

    #@c8
    move/from16 v1, v26

    #@ca
    if-lt v0, v1, :cond_128

    #@cc
    .line 676
    add-int/lit8 v2, v23, 0x1

    #@ce
    aget v2, v28, v2

    #@d0
    const v7, 0x3ffffff

    #@d3
    and-int/2addr v2, v7

    #@d4
    add-int v25, v26, v2

    #@d6
    .line 678
    .local v25, prevRunLimit:I
    move/from16 v0, v25

    #@d8
    move/from16 v1, v16

    #@da
    if-le v0, v1, :cond_de

    #@dc
    .line 679
    move/from16 v25, v16

    #@de
    .line 681
    :cond_de
    move/from16 v0, v22

    #@e0
    move/from16 v1, v25

    #@e2
    if-ge v0, v1, :cond_128

    #@e4
    .line 682
    add-int/lit8 v2, v23, 0x1

    #@e6
    aget v2, v28, v2

    #@e8
    ushr-int/lit8 v2, v2, 0x1a

    #@ea
    and-int/lit8 v24, v2, 0x3f

    #@ec
    .line 684
    .local v24, prevRunLevel:I
    move/from16 v0, v24

    #@ee
    move/from16 v1, v27

    #@f0
    if-ge v0, v1, :cond_128

    #@f2
    .line 686
    move/from16 v3, v23

    #@f4
    .line 687
    move/from16 v27, v24

    #@f6
    .line 688
    move/from16 v4, v26

    #@f8
    .line 689
    move/from16 v5, v25

    #@fa
    .line 690
    const/16 v29, 0x1

    #@fc
    .line 707
    .end local v22           #pos:I
    .end local v23           #prevRunIndex:I
    .end local v24           #prevRunLevel:I
    .end local v25           #prevRunLimit:I
    .end local v26           #prevRunStart:I
    :cond_fc
    move-object/from16 v0, v28

    #@fe
    array-length v2, v0

    #@ff
    if-eq v3, v2, :cond_2a

    #@101
    .line 708
    and-int/lit8 v2, v27, 0x1

    #@103
    if-eqz v2, :cond_12f

    #@105
    const/4 v6, 0x1

    #@106
    .line 709
    .local v6, runIsRtl:Z
    :goto_106
    move/from16 v0, p2

    #@108
    if-ne v0, v6, :cond_131

    #@10a
    const/4 v8, 0x1

    #@10b
    .line 710
    .restart local v8       #advance:Z
    :goto_10b
    if-eqz v8, :cond_133

    #@10d
    move v2, v5

    #@10e
    :goto_10e
    move/from16 v0, p1

    #@110
    if-ne v0, v2, :cond_116

    #@112
    move/from16 v0, v29

    #@114
    if-eq v8, v0, :cond_2a

    #@116
    :cond_116
    move-object/from16 v2, p0

    #@118
    move/from16 v7, p1

    #@11a
    .line 712
    invoke-direct/range {v2 .. v8}, Landroid/text/TextLine;->getOffsetBeforeAfter(IIIZIZ)I

    #@11d
    move-result v18

    #@11e
    .line 716
    if-eqz v8, :cond_135

    #@120
    move v2, v5

    #@121
    :goto_121
    move/from16 v0, v18

    #@123
    if-eq v0, v2, :cond_2a

    #@125
    move/from16 v19, v18

    #@127
    .line 787
    .end local v6           #runIsRtl:Z
    .end local v18           #newCaret:I
    .local v19, newCaret:I
    :goto_127
    return v19

    #@128
    .line 673
    .end local v8           #advance:Z
    .end local v19           #newCaret:I
    .restart local v18       #newCaret:I
    .restart local v22       #pos:I
    .restart local v23       #prevRunIndex:I
    .restart local v26       #prevRunStart:I
    :cond_128
    add-int/lit8 v23, v23, 0x2

    #@12a
    goto :goto_bb

    #@12b
    .line 657
    .end local v22           #pos:I
    .end local v23           #prevRunIndex:I
    .end local v26           #prevRunStart:I
    :cond_12b
    add-int/lit8 v3, v3, 0x2

    #@12d
    goto/16 :goto_8a

    #@12f
    .line 708
    :cond_12f
    const/4 v6, 0x0

    #@130
    goto :goto_106

    #@131
    .line 709
    .restart local v6       #runIsRtl:Z
    :cond_131
    const/4 v8, 0x0

    #@132
    goto :goto_10b

    #@133
    .restart local v8       #advance:Z
    :cond_133
    move v2, v4

    #@134
    .line 710
    goto :goto_10e

    #@135
    :cond_135
    move v2, v4

    #@136
    .line 716
    goto :goto_121

    #@137
    .line 729
    .end local v6           #runIsRtl:Z
    .end local v8           #advance:Z
    :cond_137
    const/4 v8, 0x0

    #@138
    goto/16 :goto_31

    #@13a
    .line 730
    .restart local v8       #advance:Z
    :cond_13a
    const/4 v2, -0x2

    #@13b
    goto/16 :goto_34

    #@13d
    .line 740
    .restart local v10       #otherRunIndex:I
    .restart local v11       #otherRunStart:I
    .restart local v12       #otherRunLimit:I
    .restart local v20       #otherRunLevel:I
    :cond_13d
    const/4 v13, 0x0

    #@13e
    goto/16 :goto_5e

    #@140
    .line 742
    .restart local v13       #otherRunIsRtl:Z
    :cond_140
    const/4 v8, 0x0

    #@141
    goto/16 :goto_63

    #@143
    :cond_143
    move v14, v12

    #@144
    .line 744
    goto/16 :goto_6b

    #@146
    :cond_146
    move v12, v11

    #@147
    .line 747
    goto/16 :goto_74

    #@149
    .line 758
    :cond_149
    move/from16 v0, v20

    #@14b
    move/from16 v1, v27

    #@14d
    if-ge v0, v1, :cond_153

    #@14f
    .line 760
    if-eqz v8, :cond_156

    #@151
    move/from16 v18, v11

    #@153
    .end local v11           #otherRunStart:I
    .end local v12           #otherRunLimit:I
    .end local v13           #otherRunIsRtl:Z
    .end local v20           #otherRunLevel:I
    :cond_153
    :goto_153
    move/from16 v19, v18

    #@155
    .line 787
    .end local v18           #newCaret:I
    .restart local v19       #newCaret:I
    goto :goto_127

    #@156
    .end local v19           #newCaret:I
    .restart local v11       #otherRunStart:I
    .restart local v12       #otherRunLimit:I
    .restart local v13       #otherRunIsRtl:Z
    .restart local v18       #newCaret:I
    .restart local v20       #otherRunLevel:I
    :cond_156
    move/from16 v18, v12

    #@158
    .line 760
    goto :goto_153

    #@159
    .line 765
    .end local v11           #otherRunStart:I
    .end local v12           #otherRunLimit:I
    .end local v13           #otherRunIsRtl:Z
    .end local v20           #otherRunLevel:I
    :cond_159
    const/4 v2, -0x1

    #@15a
    move/from16 v0, v18

    #@15c
    if-ne v0, v2, :cond_16a

    #@15e
    .line 769
    if-eqz v8, :cond_167

    #@160
    move-object/from16 v0, p0

    #@162
    iget v2, v0, Landroid/text/TextLine;->mLen:I

    #@164
    add-int/lit8 v18, v2, 0x1

    #@166
    .line 770
    :goto_166
    goto :goto_153

    #@167
    .line 769
    :cond_167
    const/16 v18, -0x1

    #@169
    goto :goto_166

    #@16a
    .line 781
    :cond_16a
    move/from16 v0, v18

    #@16c
    move/from16 v1, v16

    #@16e
    if-gt v0, v1, :cond_153

    #@170
    .line 782
    if-eqz v8, :cond_175

    #@172
    move/from16 v18, v16

    #@174
    :goto_174
    goto :goto_153

    #@175
    :cond_175
    move/from16 v18, v17

    #@177
    goto :goto_174
.end method

.method measure(IZLandroid/graphics/Paint$FontMetricsInt;)F
    .registers 29
    .parameter "offset"
    .parameter "trailing"
    .parameter "fmi"

    #@0
    .prologue
    .line 371
    if-eqz p2, :cond_8

    #@2
    add-int/lit8 v22, p1, -0x1

    #@4
    .line 372
    .local v22, target:I
    :goto_4
    if-gez v22, :cond_b

    #@6
    .line 373
    const/4 v14, 0x0

    #@7
    .line 560
    :cond_7
    :goto_7
    return v14

    #@8
    .end local v22           #target:I
    :cond_8
    move/from16 v22, p1

    #@a
    .line 371
    goto :goto_4

    #@b
    .line 376
    .restart local v22       #target:I
    :cond_b
    const/4 v14, 0x0

    #@c
    .line 378
    .local v14, h:F
    move-object/from16 v0, p0

    #@e
    iget-boolean v1, v0, Landroid/text/TextLine;->mHasTabs:Z

    #@10
    if-nez v1, :cond_44

    #@12
    .line 379
    move-object/from16 v0, p0

    #@14
    iget-object v1, v0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    #@16
    sget-object v3, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@18
    if-ne v1, v3, :cond_2b

    #@1a
    .line 380
    const/4 v2, 0x0

    #@1b
    move-object/from16 v0, p0

    #@1d
    iget v4, v0, Landroid/text/TextLine;->mLen:I

    #@1f
    const/4 v5, 0x0

    #@20
    move-object/from16 v1, p0

    #@22
    move/from16 v3, p1

    #@24
    move-object/from16 v6, p3

    #@26
    invoke-direct/range {v1 .. v6}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    #@29
    move-result v14

    #@2a
    goto :goto_7

    #@2b
    .line 382
    :cond_2b
    move-object/from16 v0, p0

    #@2d
    iget-object v1, v0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    #@2f
    sget-object v3, Landroid/text/Layout;->DIRS_ALL_RIGHT_TO_LEFT:Landroid/text/Layout$Directions;

    #@31
    if-ne v1, v3, :cond_44

    #@33
    .line 383
    const/4 v2, 0x0

    #@34
    move-object/from16 v0, p0

    #@36
    iget v4, v0, Landroid/text/TextLine;->mLen:I

    #@38
    const/4 v5, 0x1

    #@39
    move-object/from16 v1, p0

    #@3b
    move/from16 v3, p1

    #@3d
    move-object/from16 v6, p3

    #@3f
    invoke-direct/range {v1 .. v6}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    #@42
    move-result v14

    #@43
    goto :goto_7

    #@44
    .line 387
    :cond_44
    move-object/from16 v0, p0

    #@46
    iget-object v11, v0, Landroid/text/TextLine;->mChars:[C

    #@48
    .line 388
    .local v11, chars:[C
    move-object/from16 v0, p0

    #@4a
    iget-object v1, v0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    #@4c
    iget-object v0, v1, Landroid/text/Layout$Directions;->mDirections:[I

    #@4e
    move-object/from16 v21, v0

    #@50
    .line 391
    .local v21, runs:[I
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@52
    if-eqz v1, :cond_6e

    #@54
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@56
    if-nez v1, :cond_6e

    #@58
    .line 392
    if-eqz v11, :cond_6e

    #@5a
    if-ltz v22, :cond_6e

    #@5c
    move-object/from16 v0, p0

    #@5e
    iget v1, v0, Landroid/text/TextLine;->mLen:I

    #@60
    move/from16 v0, v22

    #@62
    if-ge v0, v1, :cond_6e

    #@64
    .line 393
    add-int/lit8 v1, v22, -0x1

    #@66
    invoke-static {v11, v1}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z

    #@69
    move-result v1

    #@6a
    if-eqz v1, :cond_111

    #@6c
    .line 394
    add-int/lit8 v22, v22, 0x3

    #@6e
    .line 412
    :cond_6e
    :goto_6e
    const/4 v15, 0x0

    #@6f
    .local v15, i:I
    :goto_6f
    move-object/from16 v0, v21

    #@71
    array-length v1, v0

    #@72
    if-ge v15, v1, :cond_7

    #@74
    .line 413
    aget v20, v21, v15

    #@76
    .line 414
    .local v20, runStart:I
    add-int/lit8 v1, v15, 0x1

    #@78
    aget v1, v21, v1

    #@7a
    const v3, 0x3ffffff

    #@7d
    and-int/2addr v1, v3

    #@7e
    add-int v19, v20, v1

    #@80
    .line 415
    .local v19, runLimit:I
    move-object/from16 v0, p0

    #@82
    iget v1, v0, Landroid/text/TextLine;->mLen:I

    #@84
    move/from16 v0, v19

    #@86
    if-le v0, v1, :cond_8e

    #@88
    .line 416
    move-object/from16 v0, p0

    #@8a
    iget v0, v0, Landroid/text/TextLine;->mLen:I

    #@8c
    move/from16 v19, v0

    #@8e
    .line 418
    :cond_8e
    add-int/lit8 v1, v15, 0x1

    #@90
    aget v1, v21, v1

    #@92
    const/high16 v3, 0x400

    #@94
    and-int/2addr v1, v3

    #@95
    if-eqz v1, :cond_16f

    #@97
    const/4 v5, 0x1

    #@98
    .line 420
    .local v5, runIsRtl:Z
    :goto_98
    move/from16 v2, v20

    #@9a
    .line 421
    .local v2, segstart:I
    move-object/from16 v0, p0

    #@9c
    iget-boolean v1, v0, Landroid/text/TextLine;->mHasTabs:Z

    #@9e
    if-eqz v1, :cond_172

    #@a0
    move/from16 v4, v20

    #@a2
    .local v4, j:I
    :goto_a2
    move/from16 v0, v19

    #@a4
    if-gt v4, v0, :cond_345

    #@a6
    .line 422
    const/4 v12, 0x0

    #@a7
    .line 423
    .local v12, codept:I
    const/4 v8, 0x0

    #@a8
    .line 425
    .local v8, bm:Landroid/graphics/Bitmap;
    const/16 v17, 0x0

    #@aa
    .line 426
    .local v17, isEmoji:Z
    const/4 v13, 0x0

    #@ab
    .line 428
    .local v13, emojiBytes:I
    move-object/from16 v0, p0

    #@ad
    iget-boolean v1, v0, Landroid/text/TextLine;->mHasTabs:Z

    #@af
    if-eqz v1, :cond_d7

    #@b1
    move/from16 v0, v19

    #@b3
    if-ge v4, v0, :cond_d7

    #@b5
    .line 430
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@b7
    if-eqz v1, :cond_1a5

    #@b9
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@bb
    if-nez v1, :cond_1a5

    #@bd
    if-eqz v11, :cond_1a5

    #@bf
    .line 431
    sget-object v1, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@c1
    aget-char v3, v11, v4

    #@c3
    invoke-virtual {v1, v3}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@c6
    move-result v1

    #@c7
    if-nez v1, :cond_d7

    #@c9
    .line 432
    invoke-static {v11, v4}, Ljava/lang/Character;->codePointAt([CI)I

    #@cc
    move-result v12

    #@cd
    .line 433
    invoke-static {v11, v4}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z

    #@d0
    move-result v1

    #@d1
    if-eqz v1, :cond_176

    #@d3
    .line 434
    const/16 v17, 0x1

    #@d5
    .line 435
    const/16 v13, 0x8

    #@d7
    .line 461
    :cond_d7
    :goto_d7
    move/from16 v0, v19

    #@d9
    if-eq v4, v0, :cond_eb

    #@db
    const/16 v1, 0x9

    #@dd
    if-eq v12, v1, :cond_eb

    #@df
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@e1
    if-eqz v1, :cond_e9

    #@e3
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@e5
    if-nez v1, :cond_e9

    #@e7
    if-nez v17, :cond_eb

    #@e9
    :cond_e9
    if-eqz v8, :cond_1a1

    #@eb
    .line 463
    :cond_eb
    move/from16 v0, v22

    #@ed
    if-lt v0, v2, :cond_1d3

    #@ef
    move/from16 v0, v22

    #@f1
    if-ge v0, v4, :cond_1d3

    #@f3
    const/16 v16, 0x1

    #@f5
    .line 465
    .local v16, inSegment:Z
    :goto_f5
    move-object/from16 v0, p0

    #@f7
    iget v1, v0, Landroid/text/TextLine;->mDir:I

    #@f9
    const/4 v3, -0x1

    #@fa
    if-ne v1, v3, :cond_1d7

    #@fc
    const/4 v1, 0x1

    #@fd
    :goto_fd
    if-ne v1, v5, :cond_1da

    #@ff
    const/4 v7, 0x1

    #@100
    .line 466
    .local v7, advance:Z
    :goto_100
    if-eqz v16, :cond_1dd

    #@102
    if-eqz v7, :cond_1dd

    #@104
    move-object/from16 v1, p0

    #@106
    move/from16 v3, p1

    #@108
    move-object/from16 v6, p3

    #@10a
    .line 467
    invoke-direct/range {v1 .. v6}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    #@10d
    move-result v1

    #@10e
    add-float/2addr v14, v1

    #@10f
    goto/16 :goto_7

    #@111
    .line 395
    .end local v2           #segstart:I
    .end local v4           #j:I
    .end local v5           #runIsRtl:Z
    .end local v7           #advance:Z
    .end local v8           #bm:Landroid/graphics/Bitmap;
    .end local v12           #codept:I
    .end local v13           #emojiBytes:I
    .end local v15           #i:I
    .end local v16           #inSegment:Z
    .end local v17           #isEmoji:Z
    .end local v19           #runLimit:I
    .end local v20           #runStart:I
    :cond_111
    add-int/lit8 v1, v22, -0x2

    #@113
    invoke-static {v11, v1}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z

    #@116
    move-result v1

    #@117
    if-eqz v1, :cond_11d

    #@119
    .line 396
    add-int/lit8 v22, v22, 0x2

    #@11b
    goto/16 :goto_6e

    #@11d
    .line 397
    :cond_11d
    add-int/lit8 v1, v22, -0x3

    #@11f
    invoke-static {v11, v1}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z

    #@122
    move-result v1

    #@123
    if-eqz v1, :cond_129

    #@125
    .line 398
    add-int/lit8 v22, v22, 0x1

    #@127
    goto/16 :goto_6e

    #@129
    .line 399
    :cond_129
    add-int/lit8 v1, v22, -0x1

    #@12b
    if-ltz v1, :cond_141

    #@12d
    move-object/from16 v0, p0

    #@12f
    iget v1, v0, Landroid/text/TextLine;->mLen:I

    #@131
    move/from16 v0, v22

    #@133
    if-ge v0, v1, :cond_141

    #@135
    add-int/lit8 v1, v22, -0x1

    #@137
    invoke-static {v11, v1}, Landroid/text/Layout;->isDiacriticalMark([CI)Z

    #@13a
    move-result v1

    #@13b
    if-eqz v1, :cond_141

    #@13d
    .line 400
    add-int/lit8 v22, v22, 0x1

    #@13f
    goto/16 :goto_6e

    #@141
    .line 401
    :cond_141
    add-int/lit8 v1, v22, -0x1

    #@143
    if-ltz v1, :cond_163

    #@145
    aget-char v1, v11, v22

    #@147
    const v3, 0xdc00

    #@14a
    if-lt v1, v3, :cond_163

    #@14c
    aget-char v1, v11, v22

    #@14e
    const v3, 0xdfff

    #@151
    if-gt v1, v3, :cond_163

    #@153
    .line 402
    add-int/lit8 v1, v22, -0x1

    #@155
    invoke-static {v11, v1}, Ljava/lang/Character;->codePointAt([CI)I

    #@158
    move-result v12

    #@159
    .line 403
    .restart local v12       #codept:I
    invoke-static {v12}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@15c
    move-result v1

    #@15d
    if-eqz v1, :cond_6e

    #@15f
    .line 404
    add-int/lit8 v22, v22, 0x1

    #@161
    goto/16 :goto_6e

    #@163
    .line 406
    .end local v12           #codept:I
    :cond_163
    aget-char v1, v11, v22

    #@165
    invoke-static {v1}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z

    #@168
    move-result v1

    #@169
    if-eqz v1, :cond_6e

    #@16b
    .line 407
    add-int/lit8 v22, v22, 0x1

    #@16d
    goto/16 :goto_6e

    #@16f
    .line 418
    .restart local v15       #i:I
    .restart local v19       #runLimit:I
    .restart local v20       #runStart:I
    :cond_16f
    const/4 v5, 0x0

    #@170
    goto/16 :goto_98

    #@172
    .restart local v2       #segstart:I
    .restart local v5       #runIsRtl:Z
    :cond_172
    move/from16 v4, v19

    #@174
    .line 421
    goto/16 :goto_a2

    #@176
    .line 436
    .restart local v4       #j:I
    .restart local v8       #bm:Landroid/graphics/Bitmap;
    .restart local v12       #codept:I
    .restart local v13       #emojiBytes:I
    .restart local v17       #isEmoji:Z
    :cond_176
    invoke-static {v12}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@179
    move-result v1

    #@17a
    if-nez v1, :cond_182

    #@17c
    invoke-static {v11, v4}, Landroid/text/Layout;->isDiacriticalMark([CI)Z

    #@17f
    move-result v1

    #@180
    if-eqz v1, :cond_18d

    #@182
    :cond_182
    add-int/lit8 v1, v4, 0x1

    #@184
    move/from16 v0, v19

    #@186
    if-ge v1, v0, :cond_18d

    #@188
    .line 437
    const/16 v17, 0x1

    #@18a
    .line 438
    const/4 v13, 0x4

    #@18b
    goto/16 :goto_d7

    #@18d
    .line 439
    :cond_18d
    aget-char v1, v11, v4

    #@18f
    invoke-static {v1}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z

    #@192
    move-result v1

    #@193
    if-eqz v1, :cond_19a

    #@195
    .line 440
    const/16 v17, 0x1

    #@197
    .line 441
    const/4 v13, 0x2

    #@198
    goto/16 :goto_d7

    #@19a
    .line 442
    :cond_19a
    const v1, 0xffff

    #@19d
    if-le v12, v1, :cond_d7

    #@19f
    .line 443
    add-int/lit8 v4, v4, 0x1

    #@1a1
    .line 421
    :cond_1a1
    :goto_1a1
    add-int/lit8 v4, v4, 0x1

    #@1a3
    goto/16 :goto_a2

    #@1a5
    .line 448
    :cond_1a5
    aget-char v12, v11, v4

    #@1a7
    .line 449
    const v1, 0xd800

    #@1aa
    if-lt v12, v1, :cond_d7

    #@1ac
    const v1, 0xdc00

    #@1af
    if-ge v12, v1, :cond_d7

    #@1b1
    add-int/lit8 v1, v4, 0x1

    #@1b3
    move/from16 v0, v19

    #@1b5
    if-ge v1, v0, :cond_d7

    #@1b7
    .line 450
    invoke-static {v11, v4}, Ljava/lang/Character;->codePointAt([CI)I

    #@1ba
    move-result v12

    #@1bb
    .line 451
    sget v1, Landroid/text/Layout;->MIN_EMOJI:I

    #@1bd
    if-lt v12, v1, :cond_1cb

    #@1bf
    sget v1, Landroid/text/Layout;->MAX_EMOJI:I

    #@1c1
    if-gt v12, v1, :cond_1cb

    #@1c3
    .line 452
    sget-object v1, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@1c5
    invoke-virtual {v1, v12}, Landroid/emoji/EmojiFactory;->getBitmapFromAndroidPua(I)Landroid/graphics/Bitmap;

    #@1c8
    move-result-object v8

    #@1c9
    goto/16 :goto_d7

    #@1cb
    .line 453
    :cond_1cb
    const v1, 0xffff

    #@1ce
    if-le v12, v1, :cond_d7

    #@1d0
    .line 454
    add-int/lit8 v4, v4, 0x1

    #@1d2
    .line 455
    goto :goto_1a1

    #@1d3
    .line 463
    :cond_1d3
    const/16 v16, 0x0

    #@1d5
    goto/16 :goto_f5

    #@1d7
    .line 465
    .restart local v16       #inSegment:Z
    :cond_1d7
    const/4 v1, 0x0

    #@1d8
    goto/16 :goto_fd

    #@1da
    :cond_1da
    const/4 v7, 0x0

    #@1db
    goto/16 :goto_100

    #@1dd
    .line 471
    .restart local v7       #advance:Z
    :cond_1dd
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@1df
    if-eqz v1, :cond_2a1

    #@1e1
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@1e3
    if-nez v1, :cond_2a1

    #@1e5
    if-eqz v11, :cond_2a1

    #@1e7
    .line 472
    move-object/from16 v0, p0

    #@1e9
    iget v1, v0, Landroid/text/TextLine;->mLen:I

    #@1eb
    move/from16 v0, v22

    #@1ed
    if-ge v0, v1, :cond_237

    #@1ef
    sget-object v1, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@1f1
    aget-char v3, v11, v22

    #@1f3
    invoke-virtual {v1, v3}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@1f6
    move-result v1

    #@1f7
    if-nez v1, :cond_237

    #@1f9
    .line 473
    move/from16 v0, v22

    #@1fb
    invoke-static {v11, v0}, Ljava/lang/Character;->codePointAt([CI)I

    #@1fe
    move-result v12

    #@1ff
    .line 474
    invoke-static {v12}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@202
    move-result v1

    #@203
    if-nez v1, :cond_223

    #@205
    aget-char v1, v11, v22

    #@207
    invoke-static {v1}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z

    #@20a
    move-result v1

    #@20b
    if-nez v1, :cond_223

    #@20d
    add-int/lit8 v1, v22, 0x1

    #@20f
    move/from16 v0, v19

    #@211
    if-ge v1, v0, :cond_21b

    #@213
    move/from16 v0, v22

    #@215
    invoke-static {v11, v0}, Landroid/text/Layout;->isDiacriticalMark([CI)Z

    #@218
    move-result v1

    #@219
    if-nez v1, :cond_223

    #@21b
    :cond_21b
    move/from16 v0, v22

    #@21d
    invoke-static {v11, v0}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z

    #@220
    move-result v1

    #@221
    if-eqz v1, :cond_237

    #@223
    .line 478
    :cond_223
    move/from16 v0, v22

    #@225
    if-ne v4, v0, :cond_237

    #@227
    move/from16 v0, v19

    #@229
    if-eq v4, v0, :cond_237

    #@22b
    move-object/from16 v1, p0

    #@22d
    move v3, v4

    #@22e
    move-object/from16 v6, p3

    #@230
    .line 479
    invoke-direct/range {v1 .. v6}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    #@233
    move-result v1

    #@234
    add-float/2addr v14, v1

    #@235
    goto/16 :goto_7

    #@237
    .line 484
    :cond_237
    move/from16 v0, v20

    #@239
    if-le v4, v0, :cond_25e

    #@23b
    .line 485
    const/16 v18, 0x0

    #@23d
    .line 487
    .local v18, prevIsEmoji:Z
    add-int/lit8 v1, v4, -0x3

    #@23f
    move-object/from16 v0, p0

    #@241
    iget v3, v0, Landroid/text/TextLine;->mLen:I

    #@243
    if-ge v1, v3, :cond_26c

    #@245
    add-int/lit8 v1, v4, -0x3

    #@247
    invoke-static {v11, v1}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z

    #@24a
    move-result v1

    #@24b
    if-eqz v1, :cond_26c

    #@24d
    .line 488
    const/16 v18, 0x1

    #@24f
    .line 499
    :cond_24f
    :goto_24f
    if-nez v18, :cond_25e

    #@251
    move-object/from16 v1, p0

    #@253
    move v3, v4

    #@254
    move-object/from16 v6, p3

    #@256
    .line 500
    invoke-direct/range {v1 .. v6}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    #@259
    move-result v23

    #@25a
    .line 501
    .local v23, w:F
    if-eqz v7, :cond_29b

    #@25c
    .end local v23           #w:F
    :goto_25c
    add-float v14, v14, v23

    #@25e
    .line 510
    .end local v18           #prevIsEmoji:Z
    :cond_25e
    :goto_25e
    if-eqz v16, :cond_2b5

    #@260
    .line 511
    const/4 v6, 0x0

    #@261
    move-object/from16 v1, p0

    #@263
    move/from16 v3, p1

    #@265
    invoke-direct/range {v1 .. v6}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    #@268
    move-result v1

    #@269
    add-float/2addr v14, v1

    #@26a
    goto/16 :goto_7

    #@26c
    .line 490
    .restart local v18       #prevIsEmoji:Z
    :cond_26c
    add-int/lit8 v1, v4, -0x1

    #@26e
    move-object/from16 v0, p0

    #@270
    iget v3, v0, Landroid/text/TextLine;->mLen:I

    #@272
    if-ge v1, v3, :cond_24f

    #@274
    .line 491
    add-int/lit8 v1, v4, -0x1

    #@276
    invoke-static {v11, v1}, Ljava/lang/Character;->codePointAt([CI)I

    #@279
    move-result v12

    #@27a
    .line 492
    invoke-static {v12}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@27d
    move-result v1

    #@27e
    if-nez v1, :cond_298

    #@280
    add-int/lit8 v1, v4, -0x1

    #@282
    aget-char v1, v11, v1

    #@284
    invoke-static {v1}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z

    #@287
    move-result v1

    #@288
    if-nez v1, :cond_298

    #@28a
    move-object/from16 v0, p0

    #@28c
    iget v1, v0, Landroid/text/TextLine;->mLen:I

    #@28e
    if-ge v4, v1, :cond_24f

    #@290
    add-int/lit8 v1, v4, -0x1

    #@292
    invoke-static {v11, v1}, Landroid/text/Layout;->isDiacriticalMark([CI)Z

    #@295
    move-result v1

    #@296
    if-eqz v1, :cond_24f

    #@298
    .line 495
    :cond_298
    const/16 v18, 0x1

    #@29a
    goto :goto_24f

    #@29b
    .line 501
    .restart local v23       #w:F
    :cond_29b
    move/from16 v0, v23

    #@29d
    neg-float v0, v0

    #@29e
    move/from16 v23, v0

    #@2a0
    goto :goto_25c

    #@2a1
    .end local v18           #prevIsEmoji:Z
    .end local v23           #w:F
    :cond_2a1
    move-object/from16 v1, p0

    #@2a3
    move v3, v4

    #@2a4
    move-object/from16 v6, p3

    #@2a6
    .line 506
    invoke-direct/range {v1 .. v6}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    #@2a9
    move-result v23

    #@2aa
    .line 507
    .restart local v23       #w:F
    if-eqz v7, :cond_2af

    #@2ac
    .end local v23           #w:F
    :goto_2ac
    add-float v14, v14, v23

    #@2ae
    goto :goto_25e

    #@2af
    .restart local v23       #w:F
    :cond_2af
    move/from16 v0, v23

    #@2b1
    neg-float v0, v0

    #@2b2
    move/from16 v23, v0

    #@2b4
    goto :goto_2ac

    #@2b5
    .line 514
    .end local v23           #w:F
    :cond_2b5
    const/16 v1, 0x9

    #@2b7
    if-ne v12, v1, :cond_2d4

    #@2b9
    .line 515
    move/from16 v0, p1

    #@2bb
    if-eq v0, v4, :cond_7

    #@2bd
    .line 518
    move-object/from16 v0, p0

    #@2bf
    iget v1, v0, Landroid/text/TextLine;->mDir:I

    #@2c1
    int-to-float v1, v1

    #@2c2
    move-object/from16 v0, p0

    #@2c4
    iget v3, v0, Landroid/text/TextLine;->mDir:I

    #@2c6
    int-to-float v3, v3

    #@2c7
    mul-float/2addr v3, v14

    #@2c8
    move-object/from16 v0, p0

    #@2ca
    invoke-virtual {v0, v3}, Landroid/text/TextLine;->nextTab(F)F

    #@2cd
    move-result v3

    #@2ce
    mul-float v14, v1, v3

    #@2d0
    .line 519
    move/from16 v0, v22

    #@2d2
    if-eq v0, v4, :cond_7

    #@2d4
    .line 524
    :cond_2d4
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2d6
    if-eqz v1, :cond_313

    #@2d8
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@2da
    if-nez v1, :cond_313

    #@2dc
    .line 525
    if-eqz v17, :cond_303

    #@2de
    move/from16 v0, p1

    #@2e0
    if-ge v4, v0, :cond_303

    #@2e2
    .line 526
    move-object/from16 v0, p0

    #@2e4
    invoke-virtual {v0, v4}, Landroid/text/TextLine;->ascent(I)F

    #@2e7
    move-result v1

    #@2e8
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@2eb
    move-result v9

    #@2ec
    .line 527
    .local v9, bmAscent:I
    move-object/from16 v0, p0

    #@2ee
    invoke-virtual {v0, v4}, Landroid/text/TextLine;->descent(I)F

    #@2f1
    move-result v1

    #@2f2
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@2f5
    move-result v10

    #@2f6
    .line 528
    .local v10, bmDescent:I
    neg-int v1, v9

    #@2f7
    add-int/2addr v1, v10

    #@2f8
    int-to-float v0, v1

    #@2f9
    move/from16 v24, v0

    #@2fb
    .line 530
    .local v24, wid:F
    move-object/from16 v0, p0

    #@2fd
    iget v1, v0, Landroid/text/TextLine;->mDir:I

    #@2ff
    int-to-float v1, v1

    #@300
    mul-float v1, v1, v24

    #@302
    add-float/2addr v14, v1

    #@303
    .line 532
    .end local v9           #bmAscent:I
    .end local v10           #bmDescent:I
    .end local v24           #wid:F
    :cond_303
    const/4 v1, 0x4

    #@304
    if-ne v13, v1, :cond_30c

    #@306
    .line 533
    add-int/lit8 v4, v4, 0x1

    #@308
    .line 555
    :cond_308
    :goto_308
    add-int/lit8 v2, v4, 0x1

    #@30a
    goto/16 :goto_1a1

    #@30c
    .line 534
    :cond_30c
    const/16 v1, 0x8

    #@30e
    if-ne v13, v1, :cond_308

    #@310
    .line 535
    add-int/lit8 v4, v4, 0x3

    #@312
    goto :goto_308

    #@313
    .line 538
    :cond_313
    if-eqz v8, :cond_308

    #@315
    .line 540
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@317
    if-eqz v1, :cond_31d

    #@319
    move/from16 v0, p1

    #@31b
    if-eq v0, v4, :cond_7

    #@31d
    .line 544
    :cond_31d
    move-object/from16 v0, p0

    #@31f
    invoke-virtual {v0, v4}, Landroid/text/TextLine;->ascent(I)F

    #@322
    move-result v9

    #@323
    .line 545
    .local v9, bmAscent:F
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    #@326
    move-result v1

    #@327
    int-to-float v1, v1

    #@328
    neg-float v3, v9

    #@329
    mul-float/2addr v1, v3

    #@32a
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    #@32d
    move-result v3

    #@32e
    int-to-float v3, v3

    #@32f
    div-float v24, v1, v3

    #@331
    .line 546
    .restart local v24       #wid:F
    move-object/from16 v0, p0

    #@333
    iget v1, v0, Landroid/text/TextLine;->mDir:I

    #@335
    int-to-float v1, v1

    #@336
    mul-float v1, v1, v24

    #@338
    add-float/2addr v14, v1

    #@339
    .line 547
    add-int/lit8 v4, v4, 0x1

    #@33b
    .line 549
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@33d
    if-eqz v1, :cond_308

    #@33f
    move/from16 v0, v22

    #@341
    if-ne v0, v4, :cond_308

    #@343
    goto/16 :goto_7

    #@345
    .line 412
    .end local v7           #advance:Z
    .end local v8           #bm:Landroid/graphics/Bitmap;
    .end local v9           #bmAscent:F
    .end local v12           #codept:I
    .end local v13           #emojiBytes:I
    .end local v16           #inSegment:Z
    .end local v17           #isEmoji:Z
    .end local v24           #wid:F
    :cond_345
    add-int/lit8 v15, v15, 0x2

    #@347
    goto/16 :goto_6f
.end method

.method metrics(Landroid/graphics/Paint$FontMetricsInt;)F
    .registers 4
    .parameter "fmi"

    #@0
    .prologue
    .line 354
    iget v0, p0, Landroid/text/TextLine;->mLen:I

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v0, v1, p1}, Landroid/text/TextLine;->measure(IZLandroid/graphics/Paint$FontMetricsInt;)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method nextTab(F)F
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1321
    iget-object v0, p0, Landroid/text/TextLine;->mTabs:Landroid/text/Layout$TabStops;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1322
    iget-object v0, p0, Landroid/text/TextLine;->mTabs:Landroid/text/Layout$TabStops;

    #@6
    invoke-virtual {v0, p1}, Landroid/text/Layout$TabStops;->nextTab(F)F

    #@9
    move-result v0

    #@a
    .line 1324
    :goto_a
    return v0

    #@b
    :cond_b
    const/16 v0, 0x14

    #@d
    invoke-static {p1, v0}, Landroid/text/Layout$TabStops;->nextDefaultStop(FI)F

    #@10
    move-result v0

    #@11
    goto :goto_a
.end method

.method set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;)V
    .registers 18
    .parameter "paint"
    .parameter "text"
    .parameter "start"
    .parameter "limit"
    .parameter "dir"
    .parameter "directions"
    .parameter "hasTabs"
    .parameter "tabStops"

    #@0
    .prologue
    .line 147
    iput-object p1, p0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@2
    .line 148
    iput-object p2, p0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    #@4
    .line 149
    iput p3, p0, Landroid/text/TextLine;->mStart:I

    #@6
    .line 150
    sub-int v7, p4, p3

    #@8
    iput v7, p0, Landroid/text/TextLine;->mLen:I

    #@a
    .line 151
    iput p5, p0, Landroid/text/TextLine;->mDir:I

    #@c
    .line 152
    iput-object p6, p0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    #@e
    .line 153
    iget-object v7, p0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    #@10
    if-nez v7, :cond_1a

    #@12
    .line 154
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@14
    const-string v8, "Directions cannot be null"

    #@16
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v7

    #@1a
    .line 156
    :cond_1a
    move/from16 v0, p7

    #@1c
    iput-boolean v0, p0, Landroid/text/TextLine;->mHasTabs:Z

    #@1e
    .line 157
    const/4 v7, 0x0

    #@1f
    iput-object v7, p0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@21
    .line 159
    const/4 v3, 0x0

    #@22
    .line 160
    .local v3, hasReplacement:Z
    instance-of v7, p2, Landroid/text/Spanned;

    #@24
    if-eqz v7, :cond_39

    #@26
    move-object v7, p2

    #@27
    .line 161
    check-cast v7, Landroid/text/Spanned;

    #@29
    iput-object v7, p0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@2b
    .line 162
    iget-object v7, p0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    #@2d
    iget-object v8, p0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    #@2f
    invoke-virtual {v7, v8, p3, p4}, Landroid/text/SpanSet;->init(Landroid/text/Spanned;II)V

    #@32
    .line 163
    iget-object v7, p0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    #@34
    iget v7, v7, Landroid/text/SpanSet;->numberOfSpans:I

    #@36
    if-lez v7, :cond_8f

    #@38
    const/4 v3, 0x1

    #@39
    .line 166
    :cond_39
    :goto_39
    if-nez v3, :cond_41

    #@3b
    if-nez p7, :cond_41

    #@3d
    sget-object v7, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@3f
    if-eq p6, v7, :cond_91

    #@41
    :cond_41
    const/4 v7, 0x1

    #@42
    :goto_42
    iput-boolean v7, p0, Landroid/text/TextLine;->mCharsValid:Z

    #@44
    .line 168
    iget-boolean v7, p0, Landroid/text/TextLine;->mCharsValid:Z

    #@46
    if-eqz v7, :cond_95

    #@48
    .line 169
    iget-object v7, p0, Landroid/text/TextLine;->mChars:[C

    #@4a
    if-eqz v7, :cond_53

    #@4c
    iget-object v7, p0, Landroid/text/TextLine;->mChars:[C

    #@4e
    array-length v7, v7

    #@4f
    iget v8, p0, Landroid/text/TextLine;->mLen:I

    #@51
    if-ge v7, v8, :cond_5d

    #@53
    .line 170
    :cond_53
    iget v7, p0, Landroid/text/TextLine;->mLen:I

    #@55
    invoke-static {v7}, Lcom/android/internal/util/ArrayUtils;->idealCharArraySize(I)I

    #@58
    move-result v7

    #@59
    new-array v7, v7, [C

    #@5b
    iput-object v7, p0, Landroid/text/TextLine;->mChars:[C

    #@5d
    .line 172
    :cond_5d
    iget-object v7, p0, Landroid/text/TextLine;->mChars:[C

    #@5f
    const/4 v8, 0x0

    #@60
    invoke-static {p2, p3, p4, v7, v8}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@63
    .line 173
    if-eqz v3, :cond_95

    #@65
    .line 179
    iget-object v1, p0, Landroid/text/TextLine;->mChars:[C

    #@67
    .line 180
    .local v1, chars:[C
    move v4, p3

    #@68
    .local v4, i:I
    :goto_68
    if-ge v4, p4, :cond_95

    #@6a
    .line 181
    iget-object v7, p0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    #@6c
    invoke-virtual {v7, v4, p4}, Landroid/text/SpanSet;->getNextTransition(II)I

    #@6f
    move-result v5

    #@70
    .line 182
    .local v5, inext:I
    iget-object v7, p0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    #@72
    invoke-virtual {v7, v4, v5}, Landroid/text/SpanSet;->hasSpansIntersecting(II)Z

    #@75
    move-result v7

    #@76
    if-eqz v7, :cond_93

    #@78
    .line 184
    sub-int v7, v4, p3

    #@7a
    const v8, 0xfffc

    #@7d
    aput-char v8, v1, v7

    #@7f
    .line 185
    sub-int v7, v4, p3

    #@81
    add-int/lit8 v6, v7, 0x1

    #@83
    .local v6, j:I
    sub-int v2, v5, p3

    #@85
    .local v2, e:I
    :goto_85
    if-ge v6, v2, :cond_93

    #@87
    .line 186
    const v7, 0xfeff

    #@8a
    aput-char v7, v1, v6

    #@8c
    .line 185
    add-int/lit8 v6, v6, 0x1

    #@8e
    goto :goto_85

    #@8f
    .line 163
    .end local v1           #chars:[C
    .end local v2           #e:I
    .end local v4           #i:I
    .end local v5           #inext:I
    .end local v6           #j:I
    :cond_8f
    const/4 v3, 0x0

    #@90
    goto :goto_39

    #@91
    .line 166
    :cond_91
    const/4 v7, 0x0

    #@92
    goto :goto_42

    #@93
    .line 180
    .restart local v1       #chars:[C
    .restart local v4       #i:I
    .restart local v5       #inext:I
    :cond_93
    move v4, v5

    #@94
    goto :goto_68

    #@95
    .line 192
    .end local v1           #chars:[C
    .end local v4           #i:I
    .end local v5           #inext:I
    :cond_95
    move-object/from16 v0, p8

    #@97
    iput-object v0, p0, Landroid/text/TextLine;->mTabs:Landroid/text/Layout$TabStops;

    #@99
    .line 194
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@9b
    if-eqz v7, :cond_b2

    #@9d
    .line 195
    new-instance v7, Landroid/graphics/Paint;

    #@9f
    iget-object v8, p0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    #@a1
    invoke-direct {v7, v8}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    #@a4
    iput-object v7, p0, Landroid/text/TextLine;->mEmojiPaint:Landroid/graphics/Paint;

    #@a6
    .line 196
    iget-object v7, p0, Landroid/text/TextLine;->mEmojiPaint:Landroid/graphics/Paint;

    #@a8
    const/4 v8, 0x1

    #@a9
    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    #@ac
    .line 197
    iget-object v7, p0, Landroid/text/TextLine;->mEmojiPaint:Landroid/graphics/Paint;

    #@ae
    const/4 v8, 0x1

    #@af
    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setDither(Z)V

    #@b2
    .line 199
    :cond_b2
    return-void
.end method
