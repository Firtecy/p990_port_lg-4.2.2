.class Landroid/text/TextUtils$Reverser;
.super Ljava/lang/Object;
.source "TextUtils.java"

# interfaces
.implements Ljava/lang/CharSequence;
.implements Landroid/text/GetChars;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/TextUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Reverser"
.end annotation


# instance fields
.field private mEnd:I

.field private mSource:Ljava/lang/CharSequence;

.field private mStart:I


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;II)V
    .registers 4
    .parameter "source"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 508
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 509
    iput-object p1, p0, Landroid/text/TextUtils$Reverser;->mSource:Ljava/lang/CharSequence;

    #@5
    .line 510
    iput p2, p0, Landroid/text/TextUtils$Reverser;->mStart:I

    #@7
    .line 511
    iput p3, p0, Landroid/text/TextUtils$Reverser;->mEnd:I

    #@9
    .line 512
    return-void
.end method


# virtual methods
.method public charAt(I)C
    .registers 4
    .parameter "off"

    #@0
    .prologue
    .line 531
    iget-object v0, p0, Landroid/text/TextUtils$Reverser;->mSource:Ljava/lang/CharSequence;

    #@2
    iget v1, p0, Landroid/text/TextUtils$Reverser;->mEnd:I

    #@4
    add-int/lit8 v1, v1, -0x1

    #@6
    sub-int/2addr v1, p1

    #@7
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    #@a
    move-result v0

    #@b
    invoke-static {v0}, Landroid/text/AndroidCharacter;->getMirror(C)C

    #@e
    move-result v0

    #@f
    return v0
.end method

.method public getChars(II[CI)V
    .registers 12
    .parameter "start"
    .parameter "end"
    .parameter "dest"
    .parameter "destoff"

    #@0
    .prologue
    .line 535
    iget-object v4, p0, Landroid/text/TextUtils$Reverser;->mSource:Ljava/lang/CharSequence;

    #@2
    iget v5, p0, Landroid/text/TextUtils$Reverser;->mStart:I

    #@4
    add-int/2addr v5, p1

    #@5
    iget v6, p0, Landroid/text/TextUtils$Reverser;->mStart:I

    #@7
    add-int/2addr v6, p2

    #@8
    invoke-static {v4, v5, v6, p3, p4}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@b
    .line 537
    const/4 v4, 0x0

    #@c
    sub-int v5, p2, p1

    #@e
    invoke-static {p3, v4, v5}, Landroid/text/AndroidCharacter;->mirror([CII)Z

    #@11
    .line 539
    sub-int v1, p2, p1

    #@13
    .line 540
    .local v1, len:I
    sub-int v4, p2, p1

    #@15
    div-int/lit8 v2, v4, 0x2

    #@17
    .line 541
    .local v2, n:I
    const/4 v0, 0x0

    #@18
    .local v0, i:I
    :goto_18
    if-ge v0, v2, :cond_33

    #@1a
    .line 542
    add-int v4, p4, v0

    #@1c
    aget-char v3, p3, v4

    #@1e
    .line 544
    .local v3, tmp:C
    add-int v4, p4, v0

    #@20
    add-int v5, p4, v1

    #@22
    sub-int/2addr v5, v0

    #@23
    add-int/lit8 v5, v5, -0x1

    #@25
    aget-char v5, p3, v5

    #@27
    aput-char v5, p3, v4

    #@29
    .line 545
    add-int v4, p4, v1

    #@2b
    sub-int/2addr v4, v0

    #@2c
    add-int/lit8 v4, v4, -0x1

    #@2e
    aput-char v3, p3, v4

    #@30
    .line 541
    add-int/lit8 v0, v0, 0x1

    #@32
    goto :goto_18

    #@33
    .line 547
    .end local v3           #tmp:C
    :cond_33
    return-void
.end method

.method public length()I
    .registers 3

    #@0
    .prologue
    .line 515
    iget v0, p0, Landroid/text/TextUtils$Reverser;->mEnd:I

    #@2
    iget v1, p0, Landroid/text/TextUtils$Reverser;->mStart:I

    #@4
    sub-int/2addr v0, v1

    #@5
    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .registers 5
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 519
    sub-int v1, p2, p1

    #@2
    new-array v0, v1, [C

    #@4
    .line 521
    .local v0, buf:[C
    const/4 v1, 0x0

    #@5
    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/text/TextUtils$Reverser;->getChars(II[CI)V

    #@8
    .line 522
    new-instance v1, Ljava/lang/String;

    #@a
    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    #@d
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 527
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0}, Landroid/text/TextUtils$Reverser;->length()I

    #@4
    move-result v1

    #@5
    invoke-virtual {p0, v0, v1}, Landroid/text/TextUtils$Reverser;->subSequence(II)Ljava/lang/CharSequence;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method
