.class Landroid/text/PackedObjectVector;
.super Ljava/lang/Object;
.source "PackedObjectVector.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mColumns:I

.field private mRowGapLength:I

.field private mRowGapStart:I

.field private mRows:I

.field private mValues:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(I)V
    .registers 5
    .parameter "columns"

    #@0
    .prologue
    .local p0, this:Landroid/text/PackedObjectVector;,"Landroid/text/PackedObjectVector<TE;>;"
    const/4 v2, 0x0

    #@1
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 34
    iput p1, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@6
    .line 35
    invoke-static {v2}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@9
    move-result v0

    #@a
    iget v1, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@c
    div-int/2addr v0, v1

    #@d
    iput v0, p0, Landroid/text/PackedObjectVector;->mRows:I

    #@f
    .line 37
    iput v2, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@11
    .line 38
    iget v0, p0, Landroid/text/PackedObjectVector;->mRows:I

    #@13
    iput v0, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@15
    .line 40
    iget v0, p0, Landroid/text/PackedObjectVector;->mRows:I

    #@17
    iget v1, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@19
    mul-int/2addr v0, v1

    #@1a
    new-array v0, v0, [Ljava/lang/Object;

    #@1c
    iput-object v0, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@1e
    .line 41
    return-void
.end method

.method private growBuffer()V
    .registers 8

    #@0
    .prologue
    .local p0, this:Landroid/text/PackedObjectVector;,"Landroid/text/PackedObjectVector<TE;>;"
    const/4 v6, 0x0

    #@1
    .line 112
    invoke-virtual {p0}, Landroid/text/PackedObjectVector;->size()I

    #@4
    move-result v3

    #@5
    add-int/lit8 v1, v3, 0x1

    #@7
    .line 113
    .local v1, newsize:I
    iget v3, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@9
    mul-int/2addr v3, v1

    #@a
    invoke-static {v3}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@d
    move-result v3

    #@e
    iget v4, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@10
    div-int v1, v3, v4

    #@12
    .line 114
    iget v3, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@14
    mul-int/2addr v3, v1

    #@15
    new-array v2, v3, [Ljava/lang/Object;

    #@17
    .line 116
    .local v2, newvalues:[Ljava/lang/Object;
    iget v3, p0, Landroid/text/PackedObjectVector;->mRows:I

    #@19
    iget v4, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@1b
    iget v5, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@1d
    add-int/2addr v4, v5

    #@1e
    sub-int v0, v3, v4

    #@20
    .line 118
    .local v0, after:I
    iget-object v3, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@22
    iget v4, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@24
    iget v5, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@26
    mul-int/2addr v4, v5

    #@27
    invoke-static {v3, v6, v2, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2a
    .line 119
    iget-object v3, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@2c
    iget v4, p0, Landroid/text/PackedObjectVector;->mRows:I

    #@2e
    sub-int/2addr v4, v0

    #@2f
    iget v5, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@31
    mul-int/2addr v4, v5

    #@32
    sub-int v5, v1, v0

    #@34
    iget v6, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@36
    mul-int/2addr v5, v6

    #@37
    iget v6, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@39
    mul-int/2addr v6, v0

    #@3a
    invoke-static {v3, v4, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3d
    .line 121
    iget v3, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@3f
    iget v4, p0, Landroid/text/PackedObjectVector;->mRows:I

    #@41
    sub-int v4, v1, v4

    #@43
    add-int/2addr v3, v4

    #@44
    iput v3, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@46
    .line 122
    iput v1, p0, Landroid/text/PackedObjectVector;->mRows:I

    #@48
    .line 123
    iput-object v2, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@4a
    .line 124
    return-void
.end method

.method private moveRowGapTo(I)V
    .registers 10
    .parameter "where"

    #@0
    .prologue
    .line 129
    .local p0, this:Landroid/text/PackedObjectVector;,"Landroid/text/PackedObjectVector<TE;>;"
    iget v5, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@2
    if-ne p1, v5, :cond_5

    #@4
    .line 166
    :goto_4
    return-void

    #@5
    .line 132
    :cond_5
    iget v5, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@7
    if-le p1, v5, :cond_47

    #@9
    .line 134
    iget v5, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@b
    add-int/2addr v5, p1

    #@c
    iget v6, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@e
    iget v7, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@10
    add-int/2addr v6, v7

    #@11
    sub-int v3, v5, v6

    #@13
    .line 136
    .local v3, moving:I
    iget v5, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@15
    iget v6, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@17
    add-int v1, v5, v6

    #@19
    .local v1, i:I
    :goto_19
    iget v5, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@1b
    iget v6, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@1d
    add-int/2addr v5, v6

    #@1e
    add-int/2addr v5, v3

    #@1f
    if-ge v1, v5, :cond_76

    #@21
    .line 138
    iget v5, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@23
    iget v6, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@25
    add-int/2addr v5, v6

    #@26
    sub-int v5, v1, v5

    #@28
    iget v6, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@2a
    add-int v0, v5, v6

    #@2c
    .line 140
    .local v0, destrow:I
    const/4 v2, 0x0

    #@2d
    .local v2, j:I
    :goto_2d
    iget v5, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@2f
    if-ge v2, v5, :cond_44

    #@31
    .line 142
    iget-object v5, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@33
    iget v6, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@35
    mul-int/2addr v6, v1

    #@36
    add-int/2addr v6, v2

    #@37
    aget-object v4, v5, v6

    #@39
    .line 144
    .local v4, val:Ljava/lang/Object;
    iget-object v5, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@3b
    iget v6, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@3d
    mul-int/2addr v6, v0

    #@3e
    add-int/2addr v6, v2

    #@3f
    aput-object v4, v5, v6

    #@41
    .line 140
    add-int/lit8 v2, v2, 0x1

    #@43
    goto :goto_2d

    #@44
    .line 136
    .end local v4           #val:Ljava/lang/Object;
    :cond_44
    add-int/lit8 v1, v1, 0x1

    #@46
    goto :goto_19

    #@47
    .line 150
    .end local v0           #destrow:I
    .end local v1           #i:I
    .end local v2           #j:I
    .end local v3           #moving:I
    :cond_47
    iget v5, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@49
    sub-int v3, v5, p1

    #@4b
    .line 152
    .restart local v3       #moving:I
    add-int v5, p1, v3

    #@4d
    add-int/lit8 v1, v5, -0x1

    #@4f
    .restart local v1       #i:I
    :goto_4f
    if-lt v1, p1, :cond_76

    #@51
    .line 154
    sub-int v5, v1, p1

    #@53
    iget v6, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@55
    add-int/2addr v5, v6

    #@56
    iget v6, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@58
    add-int/2addr v5, v6

    #@59
    sub-int v0, v5, v3

    #@5b
    .line 156
    .restart local v0       #destrow:I
    const/4 v2, 0x0

    #@5c
    .restart local v2       #j:I
    :goto_5c
    iget v5, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@5e
    if-ge v2, v5, :cond_73

    #@60
    .line 158
    iget-object v5, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@62
    iget v6, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@64
    mul-int/2addr v6, v1

    #@65
    add-int/2addr v6, v2

    #@66
    aget-object v4, v5, v6

    #@68
    .line 160
    .restart local v4       #val:Ljava/lang/Object;
    iget-object v5, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@6a
    iget v6, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@6c
    mul-int/2addr v6, v0

    #@6d
    add-int/2addr v6, v2

    #@6e
    aput-object v4, v5, v6

    #@70
    .line 156
    add-int/lit8 v2, v2, 0x1

    #@72
    goto :goto_5c

    #@73
    .line 152
    .end local v4           #val:Ljava/lang/Object;
    :cond_73
    add-int/lit8 v1, v1, -0x1

    #@75
    goto :goto_4f

    #@76
    .line 165
    .end local v0           #destrow:I
    .end local v2           #j:I
    :cond_76
    iput p1, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@78
    goto :goto_4
.end method


# virtual methods
.method public deleteAt(II)V
    .registers 5
    .parameter "row"
    .parameter "count"

    #@0
    .prologue
    .line 85
    .local p0, this:Landroid/text/PackedObjectVector;,"Landroid/text/PackedObjectVector<TE;>;"
    add-int v0, p1, p2

    #@2
    invoke-direct {p0, v0}, Landroid/text/PackedObjectVector;->moveRowGapTo(I)V

    #@5
    .line 87
    iget v0, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@7
    sub-int/2addr v0, p2

    #@8
    iput v0, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@a
    .line 88
    iget v0, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@c
    add-int/2addr v0, p2

    #@d
    iput v0, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@f
    .line 90
    iget v0, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@11
    invoke-virtual {p0}, Landroid/text/PackedObjectVector;->size()I

    #@14
    move-result v1

    #@15
    mul-int/lit8 v1, v1, 0x2

    #@17
    if-le v0, v1, :cond_19

    #@19
    .line 95
    :cond_19
    return-void
.end method

.method public dump()V
    .registers 7

    #@0
    .prologue
    .line 171
    .local p0, this:Landroid/text/PackedObjectVector;,"Landroid/text/PackedObjectVector<TE;>;"
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v3, p0, Landroid/text/PackedObjectVector;->mRows:I

    #@3
    if-ge v0, v3, :cond_61

    #@5
    .line 173
    const/4 v1, 0x0

    #@6
    .local v1, j:I
    :goto_6
    iget v3, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@8
    if-ge v1, v3, :cond_57

    #@a
    .line 175
    iget-object v3, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@c
    iget v4, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@e
    mul-int/2addr v4, v0

    #@f
    add-int/2addr v4, v1

    #@10
    aget-object v2, v3, v4

    #@12
    .line 177
    .local v2, val:Ljava/lang/Object;
    iget v3, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@14
    if-lt v0, v3, :cond_1d

    #@16
    iget v3, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@18
    iget v4, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@1a
    add-int/2addr v3, v4

    #@1b
    if-lt v0, v3, :cond_38

    #@1d
    .line 178
    :cond_1d
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@1f
    new-instance v4, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    const-string v5, " "

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    #@35
    .line 173
    :goto_35
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_6

    #@38
    .line 180
    :cond_38
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@3a
    new-instance v4, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v5, "("

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    const-string v5, ") "

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    #@56
    goto :goto_35

    #@57
    .line 183
    .end local v2           #val:Ljava/lang/Object;
    :cond_57
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@59
    const-string v4, " << \n"

    #@5b
    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    #@5e
    .line 171
    add-int/lit8 v0, v0, 0x1

    #@60
    goto :goto_1

    #@61
    .line 186
    .end local v1           #j:I
    :cond_61
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@63
    const-string v4, "-----\n\n"

    #@65
    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    #@68
    .line 187
    return-void
.end method

.method public getValue(II)Ljava/lang/Object;
    .registers 6
    .parameter "row"
    .parameter "column"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)TE;"
        }
    .end annotation

    #@0
    .prologue
    .line 46
    .local p0, this:Landroid/text/PackedObjectVector;,"Landroid/text/PackedObjectVector<TE;>;"
    iget v1, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@2
    if-lt p1, v1, :cond_7

    #@4
    .line 47
    iget v1, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@6
    add-int/2addr p1, v1

    #@7
    .line 49
    :cond_7
    iget-object v1, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@9
    iget v2, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@b
    mul-int/2addr v2, p1

    #@c
    add-int/2addr v2, p2

    #@d
    aget-object v0, v1, v2

    #@f
    .line 51
    .local v0, value:Ljava/lang/Object;
    return-object v0
.end method

.method public insertAt(I[Ljava/lang/Object;)V
    .registers 5
    .parameter "row"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[TE;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 66
    .local p0, this:Landroid/text/PackedObjectVector;,"Landroid/text/PackedObjectVector<TE;>;"
    .local p2, values:[Ljava/lang/Object;,"[TE;"
    invoke-direct {p0, p1}, Landroid/text/PackedObjectVector;->moveRowGapTo(I)V

    #@3
    .line 68
    iget v1, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@5
    if-nez v1, :cond_a

    #@7
    .line 69
    invoke-direct {p0}, Landroid/text/PackedObjectVector;->growBuffer()V

    #@a
    .line 71
    :cond_a
    iget v1, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@c
    add-int/lit8 v1, v1, 0x1

    #@e
    iput v1, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@10
    .line 72
    iget v1, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@12
    add-int/lit8 v1, v1, -0x1

    #@14
    iput v1, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@16
    .line 74
    if-nez p2, :cond_24

    #@18
    .line 75
    const/4 v0, 0x0

    #@19
    .local v0, i:I
    :goto_19
    iget v1, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@1b
    if-ge v0, v1, :cond_31

    #@1d
    .line 76
    const/4 v1, 0x0

    #@1e
    invoke-virtual {p0, p1, v0, v1}, Landroid/text/PackedObjectVector;->setValue(IILjava/lang/Object;)V

    #@21
    .line 75
    add-int/lit8 v0, v0, 0x1

    #@23
    goto :goto_19

    #@24
    .line 78
    .end local v0           #i:I
    :cond_24
    const/4 v0, 0x0

    #@25
    .restart local v0       #i:I
    :goto_25
    iget v1, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@27
    if-ge v0, v1, :cond_31

    #@29
    .line 79
    aget-object v1, p2, v0

    #@2b
    invoke-virtual {p0, p1, v0, v1}, Landroid/text/PackedObjectVector;->setValue(IILjava/lang/Object;)V

    #@2e
    .line 78
    add-int/lit8 v0, v0, 0x1

    #@30
    goto :goto_25

    #@31
    .line 80
    :cond_31
    return-void
.end method

.method public setValue(IILjava/lang/Object;)V
    .registers 6
    .parameter "row"
    .parameter "column"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IITE;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 57
    .local p0, this:Landroid/text/PackedObjectVector;,"Landroid/text/PackedObjectVector<TE;>;"
    .local p3, value:Ljava/lang/Object;,"TE;"
    iget v0, p0, Landroid/text/PackedObjectVector;->mRowGapStart:I

    #@2
    if-lt p1, v0, :cond_7

    #@4
    .line 58
    iget v0, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@6
    add-int/2addr p1, v0

    #@7
    .line 60
    :cond_7
    iget-object v0, p0, Landroid/text/PackedObjectVector;->mValues:[Ljava/lang/Object;

    #@9
    iget v1, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@b
    mul-int/2addr v1, p1

    #@c
    add-int/2addr v1, p2

    #@d
    aput-object p3, v0, v1

    #@f
    .line 61
    return-void
.end method

.method public size()I
    .registers 3

    #@0
    .prologue
    .line 100
    .local p0, this:Landroid/text/PackedObjectVector;,"Landroid/text/PackedObjectVector<TE;>;"
    iget v0, p0, Landroid/text/PackedObjectVector;->mRows:I

    #@2
    iget v1, p0, Landroid/text/PackedObjectVector;->mRowGapLength:I

    #@4
    sub-int/2addr v0, v1

    #@5
    return v0
.end method

.method public width()I
    .registers 2

    #@0
    .prologue
    .line 106
    .local p0, this:Landroid/text/PackedObjectVector;,"Landroid/text/PackedObjectVector<TE;>;"
    iget v0, p0, Landroid/text/PackedObjectVector;->mColumns:I

    #@2
    return v0
.end method
