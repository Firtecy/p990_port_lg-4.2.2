.class public Landroid/debug/JNITest;
.super Ljava/lang/Object;
.source "JNITest.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 24
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 25
    return-void
.end method

.method private native part1(IDLjava/lang/String;[I)I
.end method

.method private part2(DILjava/lang/String;)I
    .registers 9
    .parameter "doubleArg"
    .parameter "fromArray"
    .parameter "stringArg"

    #@0
    .prologue
    .line 39
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    const-string v3, " : "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    double-to-float v3, p1

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, " : "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@27
    .line 41
    invoke-static {p4}, Landroid/debug/JNITest;->part3(Ljava/lang/String;)I

    #@2a
    move-result v0

    #@2b
    .line 43
    .local v0, result:I
    add-int/lit8 v1, v0, 0x6

    #@2d
    return v1
.end method

.method private static native part3(Ljava/lang/String;)I
.end method


# virtual methods
.method public test(IDLjava/lang/String;)I
    .registers 11
    .parameter "intArg"
    .parameter "doubleArg"
    .parameter "stringArg"

    #@0
    .prologue
    .line 28
    const/4 v0, 0x4

    #@1
    new-array v5, v0, [I

    #@3
    fill-array-data v5, :array_10

    #@6
    .local v5, intArray:[I
    move-object v0, p0

    #@7
    move v1, p1

    #@8
    move-wide v2, p2

    #@9
    move-object v4, p4

    #@a
    .line 30
    invoke-direct/range {v0 .. v5}, Landroid/debug/JNITest;->part1(IDLjava/lang/String;[I)I

    #@d
    move-result v0

    #@e
    return v0

    #@f
    .line 28
    nop

    #@10
    :array_10
    .array-data 0x4
        0x2at 0x0t 0x0t 0x0t
        0x35t 0x0t 0x0t 0x0t
        0x41t 0x0t 0x0t 0x0t
        0x7ft 0x0t 0x0t 0x0t
    .end array-data
.end method
