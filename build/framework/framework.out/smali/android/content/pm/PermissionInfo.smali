.class public Landroid/content/pm/PermissionInfo;
.super Landroid/content/pm/PackageItemInfo;
.source "PermissionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/PermissionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final FLAG_COSTS_MONEY:I = 0x1

.field public static final PROTECTION_DANGEROUS:I = 0x1

.field public static final PROTECTION_FLAG_DEVELOPMENT:I = 0x20

.field public static final PROTECTION_FLAG_SYSTEM:I = 0x10

.field public static final PROTECTION_MASK_BASE:I = 0xf

.field public static final PROTECTION_MASK_FLAGS:I = 0xf0

.field public static final PROTECTION_NORMAL:I = 0x0

.field public static final PROTECTION_SIGNATURE:I = 0x2

.field public static final PROTECTION_SIGNATURE_OR_SYSTEM:I = 0x3


# instance fields
.field public descriptionRes:I

.field public flags:I

.field public group:Ljava/lang/String;

.field public nonLocalizedDescription:Ljava/lang/CharSequence;

.field public protectionLevel:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 214
    new-instance v0, Landroid/content/pm/PermissionInfo$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/PermissionInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/PermissionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 159
    invoke-direct {p0}, Landroid/content/pm/PackageItemInfo;-><init>()V

    #@3
    .line 160
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/PermissionInfo;)V
    .registers 3
    .parameter "orig"

    #@0
    .prologue
    .line 163
    invoke-direct {p0, p1}, Landroid/content/pm/PackageItemInfo;-><init>(Landroid/content/pm/PackageItemInfo;)V

    #@3
    .line 164
    iget v0, p1, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@5
    iput v0, p0, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@7
    .line 165
    iget v0, p1, Landroid/content/pm/PermissionInfo;->flags:I

    #@9
    iput v0, p0, Landroid/content/pm/PermissionInfo;->flags:I

    #@b
    .line 166
    iget-object v0, p1, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@d
    iput-object v0, p0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@f
    .line 167
    iget v0, p1, Landroid/content/pm/PermissionInfo;->descriptionRes:I

    #@11
    iput v0, p0, Landroid/content/pm/PermissionInfo;->descriptionRes:I

    #@13
    .line 168
    iget-object v0, p1, Landroid/content/pm/PermissionInfo;->nonLocalizedDescription:Ljava/lang/CharSequence;

    #@15
    iput-object v0, p0, Landroid/content/pm/PermissionInfo;->nonLocalizedDescription:Ljava/lang/CharSequence;

    #@17
    .line 169
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 225
    invoke-direct {p0, p1}, Landroid/content/pm/PackageItemInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@9
    .line 227
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/content/pm/PermissionInfo;->flags:I

    #@f
    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@15
    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/content/pm/PermissionInfo;->descriptionRes:I

    #@1b
    .line 230
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Ljava/lang/CharSequence;

    #@23
    iput-object v0, p0, Landroid/content/pm/PermissionInfo;->nonLocalizedDescription:Ljava/lang/CharSequence;

    #@25
    .line 231
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/PermissionInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/content/pm/PermissionInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public static fixProtectionLevel(I)I
    .registers 2
    .parameter "level"

    #@0
    .prologue
    .line 127
    const/4 v0, 0x3

    #@1
    if-ne p0, v0, :cond_5

    #@3
    .line 128
    const/16 p0, 0x12

    #@5
    .line 130
    :cond_5
    return p0
.end method

.method public static protectionToString(I)Ljava/lang/String;
    .registers 4
    .parameter "level"

    #@0
    .prologue
    .line 135
    const-string v0, "????"

    #@2
    .line 136
    .local v0, protLevel:Ljava/lang/String;
    and-int/lit8 v1, p0, 0xf

    #@4
    packed-switch v1, :pswitch_data_48

    #@7
    .line 150
    :goto_7
    and-int/lit8 v1, p0, 0x10

    #@9
    if-eqz v1, :cond_1f

    #@b
    .line 151
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string/jumbo v2, "|system"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 153
    :cond_1f
    and-int/lit8 v1, p0, 0x20

    #@21
    if-eqz v1, :cond_37

    #@23
    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    const-string/jumbo v2, "|development"

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    .line 156
    :cond_37
    return-object v0

    #@38
    .line 138
    :pswitch_38
    const-string v0, "dangerous"

    #@3a
    .line 139
    goto :goto_7

    #@3b
    .line 141
    :pswitch_3b
    const-string/jumbo v0, "normal"

    #@3e
    .line 142
    goto :goto_7

    #@3f
    .line 144
    :pswitch_3f
    const-string/jumbo v0, "signature"

    #@42
    .line 145
    goto :goto_7

    #@43
    .line 147
    :pswitch_43
    const-string/jumbo v0, "signatureOrSystem"

    #@46
    goto :goto_7

    #@47
    .line 136
    nop

    #@48
    :pswitch_data_48
    .packed-switch 0x0
        :pswitch_3b
        :pswitch_38
        :pswitch_3f
        :pswitch_43
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 202
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 6
    .parameter "pm"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 183
    iget-object v2, p0, Landroid/content/pm/PermissionInfo;->nonLocalizedDescription:Ljava/lang/CharSequence;

    #@3
    if-eqz v2, :cond_8

    #@5
    .line 184
    iget-object v0, p0, Landroid/content/pm/PermissionInfo;->nonLocalizedDescription:Ljava/lang/CharSequence;

    #@7
    .line 192
    :cond_7
    :goto_7
    return-object v0

    #@8
    .line 186
    :cond_8
    iget v2, p0, Landroid/content/pm/PermissionInfo;->descriptionRes:I

    #@a
    if-eqz v2, :cond_16

    #@c
    .line 187
    iget-object v2, p0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@e
    iget v3, p0, Landroid/content/pm/PermissionInfo;->descriptionRes:I

    #@10
    invoke-virtual {p1, v2, v3, v1}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@13
    move-result-object v0

    #@14
    .line 188
    .local v0, label:Ljava/lang/CharSequence;
    if-nez v0, :cond_7

    #@16
    .end local v0           #label:Ljava/lang/CharSequence;
    :cond_16
    move-object v0, v1

    #@17
    .line 192
    goto :goto_7
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "PermissionInfo{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    iget-object v1, p0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string/jumbo v1, "}"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 206
    invoke-super {p0, p1, p2}, Landroid/content/pm/PackageItemInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@3
    .line 207
    iget v0, p0, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 208
    iget v0, p0, Landroid/content/pm/PermissionInfo;->flags:I

    #@a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 209
    iget-object v0, p0, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 210
    iget v0, p0, Landroid/content/pm/PermissionInfo;->descriptionRes:I

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 211
    iget-object v0, p0, Landroid/content/pm/PermissionInfo;->nonLocalizedDescription:Ljava/lang/CharSequence;

    #@19
    invoke-static {v0, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@1c
    .line 212
    return-void
.end method
