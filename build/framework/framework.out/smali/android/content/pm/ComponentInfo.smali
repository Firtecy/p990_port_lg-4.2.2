.class public Landroid/content/pm/ComponentInfo;
.super Landroid/content/pm/PackageItemInfo;
.source "ComponentInfo.java"


# instance fields
.field public applicationInfo:Landroid/content/pm/ApplicationInfo;

.field public descriptionRes:I

.field public enabled:Z

.field public exported:Z

.field public processName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 66
    invoke-direct {p0}, Landroid/content/pm/PackageItemInfo;-><init>()V

    #@3
    .line 56
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@6
    .line 64
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/content/pm/ComponentInfo;->exported:Z

    #@9
    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/ComponentInfo;)V
    .registers 3
    .parameter "orig"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1}, Landroid/content/pm/PackageItemInfo;-><init>(Landroid/content/pm/PackageItemInfo;)V

    #@3
    .line 56
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@6
    .line 64
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/content/pm/ComponentInfo;->exported:Z

    #@9
    .line 71
    iget-object v0, p1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@b
    iput-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@d
    .line 72
    iget-object v0, p1, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@f
    iput-object v0, p0, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@11
    .line 73
    iget v0, p1, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@13
    iput v0, p0, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@15
    .line 74
    iget-boolean v0, p1, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@17
    iput-boolean v0, p0, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@19
    .line 75
    iget-boolean v0, p1, Landroid/content/pm/ComponentInfo;->exported:Z

    #@1b
    iput-boolean v0, p0, Landroid/content/pm/ComponentInfo;->exported:Z

    #@1d
    .line 76
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 149
    invoke-direct {p0, p1}, Landroid/content/pm/PackageItemInfo;-><init>(Landroid/os/Parcel;)V

    #@5
    .line 56
    iput-boolean v1, p0, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@7
    .line 64
    iput-boolean v2, p0, Landroid/content/pm/ComponentInfo;->exported:Z

    #@9
    .line 150
    sget-object v0, Landroid/content/pm/ApplicationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/content/pm/ApplicationInfo;

    #@11
    iput-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@13
    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@19
    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v0

    #@1d
    iput v0, p0, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@1f
    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_31

    #@25
    move v0, v1

    #@26
    :goto_26
    iput-boolean v0, p0, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@28
    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_33

    #@2e
    :goto_2e
    iput-boolean v1, p0, Landroid/content/pm/ComponentInfo;->exported:Z

    #@30
    .line 155
    return-void

    #@31
    :cond_31
    move v0, v2

    #@32
    .line 153
    goto :goto_26

    #@33
    :cond_33
    move v1, v2

    #@34
    .line 154
    goto :goto_2e
.end method


# virtual methods
.method protected dumpBack(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 130
    iget-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2
    if-eqz v0, :cond_36

    #@4
    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, "ApplicationInfo:"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1a
    .line 132
    iget-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1c
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, "  "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/ApplicationInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@32
    .line 136
    :goto_32
    invoke-super {p0, p1, p2}, Landroid/content/pm/PackageItemInfo;->dumpBack(Landroid/util/Printer;Ljava/lang/String;)V

    #@35
    .line 137
    return-void

    #@36
    .line 134
    :cond_36
    new-instance v0, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, "ApplicationInfo: null"

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@4c
    goto :goto_32
.end method

.method protected dumpFront(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 5
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 121
    invoke-super {p0, p1, p2}, Landroid/content/pm/PackageItemInfo;->dumpFront(Landroid/util/Printer;Ljava/lang/String;)V

    #@3
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    const-string v1, "enabled="

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    iget-boolean v1, p0, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    const-string v1, " exported="

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    iget-boolean v1, p0, Landroid/content/pm/ComponentInfo;->exported:Z

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    const-string v1, " processName="

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    iget-object v1, p0, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@37
    .line 124
    iget v0, p0, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@39
    if-eqz v0, :cond_57

    #@3b
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    const-string v1, "description="

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v0

    #@4a
    iget v1, p0, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v0

    #@54
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@57
    .line 127
    :cond_57
    return-void
.end method

.method protected getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .registers 2

    #@0
    .prologue
    .line 176
    iget-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2
    return-object v0
.end method

.method public final getIconResource()I
    .registers 2

    #@0
    .prologue
    .line 117
    iget v0, p0, Landroid/content/pm/PackageItemInfo;->icon:I

    #@2
    if-eqz v0, :cond_7

    #@4
    iget v0, p0, Landroid/content/pm/PackageItemInfo;->icon:I

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    iget-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@9
    iget v0, v0, Landroid/content/pm/PackageItemInfo;->icon:I

    #@b
    goto :goto_6
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 106
    iget-boolean v0, p0, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@6
    iget-boolean v0, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method protected loadDefaultIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "pm"

    #@0
    .prologue
    .line 161
    iget-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected loadDefaultLogo(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "pm"

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/pm/ApplicationInfo;->loadLogo(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 6
    .parameter "pm"

    #@0
    .prologue
    .line 79
    iget-object v2, p0, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@2
    if-eqz v2, :cond_7

    #@4
    .line 80
    iget-object v1, p0, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@6
    .line 99
    :cond_6
    :goto_6
    return-object v1

    #@7
    .line 82
    :cond_7
    iget-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@9
    .line 84
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    iget v2, p0, Landroid/content/pm/PackageItemInfo;->labelRes:I

    #@b
    if-eqz v2, :cond_17

    #@d
    .line 85
    iget-object v2, p0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@f
    iget v3, p0, Landroid/content/pm/PackageItemInfo;->labelRes:I

    #@11
    invoke-virtual {p1, v2, v3, v0}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@14
    move-result-object v1

    #@15
    .line 86
    .local v1, label:Ljava/lang/CharSequence;
    if-nez v1, :cond_6

    #@17
    .line 90
    .end local v1           #label:Ljava/lang/CharSequence;
    :cond_17
    iget-object v2, v0, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@19
    if-eqz v2, :cond_1e

    #@1b
    .line 91
    iget-object v1, v0, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@1d
    goto :goto_6

    #@1e
    .line 93
    :cond_1e
    iget v2, v0, Landroid/content/pm/PackageItemInfo;->labelRes:I

    #@20
    if-eqz v2, :cond_2c

    #@22
    .line 94
    iget-object v2, p0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@24
    iget v3, v0, Landroid/content/pm/PackageItemInfo;->labelRes:I

    #@26
    invoke-virtual {p1, v2, v3, v0}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@29
    move-result-object v1

    #@2a
    .line 95
    .restart local v1       #label:Ljava/lang/CharSequence;
    if-nez v1, :cond_6

    #@2c
    .line 99
    .end local v1           #label:Ljava/lang/CharSequence;
    :cond_2c
    iget-object v1, p0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@2e
    goto :goto_6
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 140
    invoke-super {p0, p1, p2}, Landroid/content/pm/PackageItemInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@5
    .line 141
    iget-object v0, p0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/content/pm/ApplicationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@a
    .line 142
    iget-object v0, p0, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 143
    iget v0, p0, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 144
    iget-boolean v0, p0, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@16
    if-eqz v0, :cond_24

    #@18
    move v0, v1

    #@19
    :goto_19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 145
    iget-boolean v0, p0, Landroid/content/pm/ComponentInfo;->exported:Z

    #@1e
    if-eqz v0, :cond_26

    #@20
    :goto_20
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 146
    return-void

    #@24
    :cond_24
    move v0, v2

    #@25
    .line 144
    goto :goto_19

    #@26
    :cond_26
    move v1, v2

    #@27
    .line 145
    goto :goto_20
.end method
