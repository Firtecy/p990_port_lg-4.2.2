.class public abstract Landroid/content/pm/IPackageManager$Stub;
.super Landroid/os/Binder;
.source "IPackageManager.java"

# interfaces
.implements Landroid/content/pm/IPackageManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/pm/IPackageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/pm/IPackageManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.content.pm.IPackageManager"

.field static final TRANSACTION_addPackageToPreferred:I = 0x2e

.field static final TRANSACTION_addPermission:I = 0x11

.field static final TRANSACTION_addPermissionAsync:I = 0x4b

.field static final TRANSACTION_addPreferredActivity:I = 0x31

.field static final TRANSACTION_canonicalToCurrentPackageNames:I = 0x5

.field static final TRANSACTION_checkPermission:I = 0xf

.field static final TRANSACTION_checkSignatures:I = 0x16

.field static final TRANSACTION_checkUidPermission:I = 0x10

.field static final TRANSACTION_checkUidSignatures:I = 0x17

.field static final TRANSACTION_clearApplicationUserData:I = 0x3d

.field static final TRANSACTION_clearPackagePreferredActivities:I = 0x33

.field static final TRANSACTION_currentToCanonicalPackageNames:I = 0x4

.field static final TRANSACTION_deleteApplicationCacheFiles:I = 0x3c

.field static final TRANSACTION_deletePackage:I = 0x2c

.field static final TRANSACTION_enterSafeMode:I = 0x42

.field static final TRANSACTION_extendVerificationTimeout:I = 0x52

.field static final TRANSACTION_finishPackageInstall:I = 0x2a

.field static final TRANSACTION_freeStorage:I = 0x3b

.field static final TRANSACTION_freeStorageAndNotify:I = 0x3a

.field static final TRANSACTION_getActivityInfo:I = 0xb

.field static final TRANSACTION_getAllPermissionGroups:I = 0x9

.field static final TRANSACTION_getApplicationEnabledSetting:I = 0x38

.field static final TRANSACTION_getApplicationInfo:I = 0xa

.field static final TRANSACTION_getComponentEnabledSetting:I = 0x36

.field static final TRANSACTION_getInstallLocation:I = 0x4d

.field static final TRANSACTION_getInstalledApplications:I = 0x22

.field static final TRANSACTION_getInstalledPackages:I = 0x21

.field static final TRANSACTION_getInstallerPackageName:I = 0x2d

.field static final TRANSACTION_getInstrumentationInfo:I = 0x27

.field static final TRANSACTION_getNameForUid:I = 0x19

.field static final TRANSACTION_getOverlayPath:I = 0x58

.field static final TRANSACTION_getPackageGids:I = 0x3

.field static final TRANSACTION_getPackageInfo:I = 0x1

.field static final TRANSACTION_getPackageSizeInfo:I = 0x3e

.field static final TRANSACTION_getPackageUid:I = 0x2

.field static final TRANSACTION_getPackagesForUid:I = 0x18

.field static final TRANSACTION_getPermissionGroupInfo:I = 0x8

.field static final TRANSACTION_getPermissionInfo:I = 0x6

.field static final TRANSACTION_getPersistentApplications:I = 0x23

.field static final TRANSACTION_getPreferredActivities:I = 0x34

.field static final TRANSACTION_getPreferredPackages:I = 0x30

.field static final TRANSACTION_getProviderInfo:I = 0xe

.field static final TRANSACTION_getReceiverInfo:I = 0xc

.field static final TRANSACTION_getServiceInfo:I = 0xd

.field static final TRANSACTION_getSystemAvailableFeatures:I = 0x40

.field static final TRANSACTION_getSystemSharedLibraryNames:I = 0x3f

.field static final TRANSACTION_getUidForSharedUser:I = 0x1a

.field static final TRANSACTION_getVerifierDeviceIdentity:I = 0x53

.field static final TRANSACTION_grantPermission:I = 0x13

.field static final TRANSACTION_hasSystemFeature:I = 0x41

.field static final TRANSACTION_hasSystemUidErrors:I = 0x45

.field static final TRANSACTION_installExistingPackage:I = 0x50

.field static final TRANSACTION_installPackage:I = 0x29

.field static final TRANSACTION_installPackageWithVerification:I = 0x4e

.field static final TRANSACTION_installPackageWithVerificationAndEncryption:I = 0x4f

.field static final TRANSACTION_isFirstBoot:I = 0x54

.field static final TRANSACTION_isOnlyCoreApps:I = 0x55

.field static final TRANSACTION_isPermissionEnforced:I = 0x57

.field static final TRANSACTION_isProtectedBroadcast:I = 0x15

.field static final TRANSACTION_isSafeMode:I = 0x43

.field static final TRANSACTION_isStorageLow:I = 0x59

.field static final TRANSACTION_movePackage:I = 0x4a

.field static final TRANSACTION_nextPackageToClean:I = 0x49

.field static final TRANSACTION_performBootDexOpt:I = 0x46

.field static final TRANSACTION_performDexOpt:I = 0x47

.field static final TRANSACTION_queryContentProviders:I = 0x26

.field static final TRANSACTION_queryInstrumentation:I = 0x28

.field static final TRANSACTION_queryIntentActivities:I = 0x1c

.field static final TRANSACTION_queryIntentActivityOptions:I = 0x1d

.field static final TRANSACTION_queryIntentReceivers:I = 0x1e

.field static final TRANSACTION_queryIntentServices:I = 0x20

.field static final TRANSACTION_queryPermissionsByGroup:I = 0x7

.field static final TRANSACTION_querySyncProviders:I = 0x25

.field static final TRANSACTION_removePackageFromPreferred:I = 0x2f

.field static final TRANSACTION_removePermission:I = 0x12

.field static final TRANSACTION_replacePreferredActivity:I = 0x32

.field static final TRANSACTION_resolveContentProvider:I = 0x24

.field static final TRANSACTION_resolveIntent:I = 0x1b

.field static final TRANSACTION_resolveService:I = 0x1f

.field static final TRANSACTION_revokePermission:I = 0x14

.field static final TRANSACTION_setApplicationEnabledSetting:I = 0x37

.field static final TRANSACTION_setComponentEnabledSetting:I = 0x35

.field static final TRANSACTION_setInstallLocation:I = 0x4c

.field static final TRANSACTION_setInstallerPackageName:I = 0x2b

.field static final TRANSACTION_setPackageStoppedState:I = 0x39

.field static final TRANSACTION_setPermissionEnforced:I = 0x56

.field static final TRANSACTION_systemReady:I = 0x44

.field static final TRANSACTION_updateExternalMediaStatus:I = 0x48

.field static final TRANSACTION_verifyPendingInstall:I = 0x51


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 21
    const-string v0, "android.content.pm.IPackageManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/content/pm/IPackageManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 22
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 29
    if-nez p0, :cond_4

    #@2
    .line 30
    const/4 v0, 0x0

    #@3
    .line 36
    :goto_3
    return-object v0

    #@4
    .line 32
    :cond_4
    const-string v1, "android.content.pm.IPackageManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 33
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/content/pm/IPackageManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 34
    check-cast v0, Landroid/content/pm/IPackageManager;

    #@12
    goto :goto_3

    #@13
    .line 36
    :cond_13
    new-instance v0, Landroid/content/pm/IPackageManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/content/pm/IPackageManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 32
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 44
    sparse-switch p1, :sswitch_data_e10

    #@3
    .line 1345
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v3

    #@7
    :goto_7
    return v3

    #@8
    .line 48
    :sswitch_8
    const-string v3, "android.content.pm.IPackageManager"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 49
    const/4 v3, 0x1

    #@10
    goto :goto_7

    #@11
    .line 53
    :sswitch_11
    const-string v3, "android.content.pm.IPackageManager"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 55
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    .line 57
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v5

    #@20
    .line 59
    .local v5, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v6

    #@24
    .line 60
    .local v6, _arg2:I
    move-object/from16 v0, p0

    #@26
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->getPackageInfo(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    #@29
    move-result-object v19

    #@2a
    .line 61
    .local v19, _result:Landroid/content/pm/PackageInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d
    .line 62
    if-eqz v19, :cond_3f

    #@2f
    .line 63
    const/4 v3, 0x1

    #@30
    move-object/from16 v0, p3

    #@32
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@35
    .line 64
    const/4 v3, 0x1

    #@36
    move-object/from16 v0, v19

    #@38
    move-object/from16 v1, p3

    #@3a
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@3d
    .line 69
    :goto_3d
    const/4 v3, 0x1

    #@3e
    goto :goto_7

    #@3f
    .line 67
    :cond_3f
    const/4 v3, 0x0

    #@40
    move-object/from16 v0, p3

    #@42
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@45
    goto :goto_3d

    #@46
    .line 73
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/PackageInfo;
    :sswitch_46
    const-string v3, "android.content.pm.IPackageManager"

    #@48
    move-object/from16 v0, p2

    #@4a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4d
    .line 75
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    .line 77
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@54
    move-result v5

    #@55
    .line 78
    .restart local v5       #_arg1:I
    move-object/from16 v0, p0

    #@57
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->getPackageUid(Ljava/lang/String;I)I

    #@5a
    move-result v19

    #@5b
    .line 79
    .local v19, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    .line 80
    move-object/from16 v0, p3

    #@60
    move/from16 v1, v19

    #@62
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@65
    .line 81
    const/4 v3, 0x1

    #@66
    goto :goto_7

    #@67
    .line 85
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v19           #_result:I
    :sswitch_67
    const-string v3, "android.content.pm.IPackageManager"

    #@69
    move-object/from16 v0, p2

    #@6b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6e
    .line 87
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@71
    move-result-object v4

    #@72
    .line 88
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@74
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->getPackageGids(Ljava/lang/String;)[I

    #@77
    move-result-object v19

    #@78
    .line 89
    .local v19, _result:[I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7b
    .line 90
    move-object/from16 v0, p3

    #@7d
    move-object/from16 v1, v19

    #@7f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeIntArray([I)V

    #@82
    .line 91
    const/4 v3, 0x1

    #@83
    goto :goto_7

    #@84
    .line 95
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v19           #_result:[I
    :sswitch_84
    const-string v3, "android.content.pm.IPackageManager"

    #@86
    move-object/from16 v0, p2

    #@88
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8b
    .line 97
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@8e
    move-result-object v4

    #@8f
    .line 98
    .local v4, _arg0:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@91
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->currentToCanonicalPackageNames([Ljava/lang/String;)[Ljava/lang/String;

    #@94
    move-result-object v19

    #@95
    .line 99
    .local v19, _result:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@98
    .line 100
    move-object/from16 v0, p3

    #@9a
    move-object/from16 v1, v19

    #@9c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@9f
    .line 101
    const/4 v3, 0x1

    #@a0
    goto/16 :goto_7

    #@a2
    .line 105
    .end local v4           #_arg0:[Ljava/lang/String;
    .end local v19           #_result:[Ljava/lang/String;
    :sswitch_a2
    const-string v3, "android.content.pm.IPackageManager"

    #@a4
    move-object/from16 v0, p2

    #@a6
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a9
    .line 107
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@ac
    move-result-object v4

    #@ad
    .line 108
    .restart local v4       #_arg0:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@af
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->canonicalToCurrentPackageNames([Ljava/lang/String;)[Ljava/lang/String;

    #@b2
    move-result-object v19

    #@b3
    .line 109
    .restart local v19       #_result:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b6
    .line 110
    move-object/from16 v0, p3

    #@b8
    move-object/from16 v1, v19

    #@ba
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@bd
    .line 111
    const/4 v3, 0x1

    #@be
    goto/16 :goto_7

    #@c0
    .line 115
    .end local v4           #_arg0:[Ljava/lang/String;
    .end local v19           #_result:[Ljava/lang/String;
    :sswitch_c0
    const-string v3, "android.content.pm.IPackageManager"

    #@c2
    move-object/from16 v0, p2

    #@c4
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c7
    .line 117
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ca
    move-result-object v4

    #@cb
    .line 119
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ce
    move-result v5

    #@cf
    .line 120
    .restart local v5       #_arg1:I
    move-object/from16 v0, p0

    #@d1
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    #@d4
    move-result-object v19

    #@d5
    .line 121
    .local v19, _result:Landroid/content/pm/PermissionInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d8
    .line 122
    if-eqz v19, :cond_eb

    #@da
    .line 123
    const/4 v3, 0x1

    #@db
    move-object/from16 v0, p3

    #@dd
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@e0
    .line 124
    const/4 v3, 0x1

    #@e1
    move-object/from16 v0, v19

    #@e3
    move-object/from16 v1, p3

    #@e5
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PermissionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@e8
    .line 129
    :goto_e8
    const/4 v3, 0x1

    #@e9
    goto/16 :goto_7

    #@eb
    .line 127
    :cond_eb
    const/4 v3, 0x0

    #@ec
    move-object/from16 v0, p3

    #@ee
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@f1
    goto :goto_e8

    #@f2
    .line 133
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v19           #_result:Landroid/content/pm/PermissionInfo;
    :sswitch_f2
    const-string v3, "android.content.pm.IPackageManager"

    #@f4
    move-object/from16 v0, p2

    #@f6
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f9
    .line 135
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@fc
    move-result-object v4

    #@fd
    .line 137
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@100
    move-result v5

    #@101
    .line 138
    .restart local v5       #_arg1:I
    move-object/from16 v0, p0

    #@103
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->queryPermissionsByGroup(Ljava/lang/String;I)Ljava/util/List;

    #@106
    move-result-object v24

    #@107
    .line 139
    .local v24, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PermissionInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@10a
    .line 140
    move-object/from16 v0, p3

    #@10c
    move-object/from16 v1, v24

    #@10e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@111
    .line 141
    const/4 v3, 0x1

    #@112
    goto/16 :goto_7

    #@114
    .line 145
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v24           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PermissionInfo;>;"
    :sswitch_114
    const-string v3, "android.content.pm.IPackageManager"

    #@116
    move-object/from16 v0, p2

    #@118
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11b
    .line 147
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@11e
    move-result-object v4

    #@11f
    .line 149
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@122
    move-result v5

    #@123
    .line 150
    .restart local v5       #_arg1:I
    move-object/from16 v0, p0

    #@125
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->getPermissionGroupInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionGroupInfo;

    #@128
    move-result-object v19

    #@129
    .line 151
    .local v19, _result:Landroid/content/pm/PermissionGroupInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@12c
    .line 152
    if-eqz v19, :cond_13f

    #@12e
    .line 153
    const/4 v3, 0x1

    #@12f
    move-object/from16 v0, p3

    #@131
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@134
    .line 154
    const/4 v3, 0x1

    #@135
    move-object/from16 v0, v19

    #@137
    move-object/from16 v1, p3

    #@139
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PermissionGroupInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@13c
    .line 159
    :goto_13c
    const/4 v3, 0x1

    #@13d
    goto/16 :goto_7

    #@13f
    .line 157
    :cond_13f
    const/4 v3, 0x0

    #@140
    move-object/from16 v0, p3

    #@142
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@145
    goto :goto_13c

    #@146
    .line 163
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v19           #_result:Landroid/content/pm/PermissionGroupInfo;
    :sswitch_146
    const-string v3, "android.content.pm.IPackageManager"

    #@148
    move-object/from16 v0, p2

    #@14a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14d
    .line 165
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@150
    move-result v4

    #@151
    .line 166
    .local v4, _arg0:I
    move-object/from16 v0, p0

    #@153
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->getAllPermissionGroups(I)Ljava/util/List;

    #@156
    move-result-object v23

    #@157
    .line 167
    .local v23, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PermissionGroupInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@15a
    .line 168
    move-object/from16 v0, p3

    #@15c
    move-object/from16 v1, v23

    #@15e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@161
    .line 169
    const/4 v3, 0x1

    #@162
    goto/16 :goto_7

    #@164
    .line 173
    .end local v4           #_arg0:I
    .end local v23           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PermissionGroupInfo;>;"
    :sswitch_164
    const-string v3, "android.content.pm.IPackageManager"

    #@166
    move-object/from16 v0, p2

    #@168
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16b
    .line 175
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@16e
    move-result-object v4

    #@16f
    .line 177
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@172
    move-result v5

    #@173
    .line 179
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@176
    move-result v6

    #@177
    .line 180
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@179
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    #@17c
    move-result-object v19

    #@17d
    .line 181
    .local v19, _result:Landroid/content/pm/ApplicationInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@180
    .line 182
    if-eqz v19, :cond_193

    #@182
    .line 183
    const/4 v3, 0x1

    #@183
    move-object/from16 v0, p3

    #@185
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@188
    .line 184
    const/4 v3, 0x1

    #@189
    move-object/from16 v0, v19

    #@18b
    move-object/from16 v1, p3

    #@18d
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ApplicationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@190
    .line 189
    :goto_190
    const/4 v3, 0x1

    #@191
    goto/16 :goto_7

    #@193
    .line 187
    :cond_193
    const/4 v3, 0x0

    #@194
    move-object/from16 v0, p3

    #@196
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@199
    goto :goto_190

    #@19a
    .line 193
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ApplicationInfo;
    :sswitch_19a
    const-string v3, "android.content.pm.IPackageManager"

    #@19c
    move-object/from16 v0, p2

    #@19e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a1
    .line 195
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1a4
    move-result v3

    #@1a5
    if-eqz v3, :cond_1d5

    #@1a7
    .line 196
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a9
    move-object/from16 v0, p2

    #@1ab
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1ae
    move-result-object v4

    #@1af
    check-cast v4, Landroid/content/ComponentName;

    #@1b1
    .line 202
    .local v4, _arg0:Landroid/content/ComponentName;
    :goto_1b1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b4
    move-result v5

    #@1b5
    .line 204
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b8
    move-result v6

    #@1b9
    .line 205
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@1bb
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->getActivityInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;

    #@1be
    move-result-object v19

    #@1bf
    .line 206
    .local v19, _result:Landroid/content/pm/ActivityInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c2
    .line 207
    if-eqz v19, :cond_1d7

    #@1c4
    .line 208
    const/4 v3, 0x1

    #@1c5
    move-object/from16 v0, p3

    #@1c7
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1ca
    .line 209
    const/4 v3, 0x1

    #@1cb
    move-object/from16 v0, v19

    #@1cd
    move-object/from16 v1, p3

    #@1cf
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ActivityInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d2
    .line 214
    :goto_1d2
    const/4 v3, 0x1

    #@1d3
    goto/16 :goto_7

    #@1d5
    .line 199
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ActivityInfo;
    :cond_1d5
    const/4 v4, 0x0

    #@1d6
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    goto :goto_1b1

    #@1d7
    .line 212
    .restart local v5       #_arg1:I
    .restart local v6       #_arg2:I
    .restart local v19       #_result:Landroid/content/pm/ActivityInfo;
    :cond_1d7
    const/4 v3, 0x0

    #@1d8
    move-object/from16 v0, p3

    #@1da
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1dd
    goto :goto_1d2

    #@1de
    .line 218
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ActivityInfo;
    :sswitch_1de
    const-string v3, "android.content.pm.IPackageManager"

    #@1e0
    move-object/from16 v0, p2

    #@1e2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e5
    .line 220
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1e8
    move-result v3

    #@1e9
    if-eqz v3, :cond_219

    #@1eb
    .line 221
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1ed
    move-object/from16 v0, p2

    #@1ef
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f2
    move-result-object v4

    #@1f3
    check-cast v4, Landroid/content/ComponentName;

    #@1f5
    .line 227
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    :goto_1f5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1f8
    move-result v5

    #@1f9
    .line 229
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1fc
    move-result v6

    #@1fd
    .line 230
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@1ff
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->getReceiverInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;

    #@202
    move-result-object v19

    #@203
    .line 231
    .restart local v19       #_result:Landroid/content/pm/ActivityInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@206
    .line 232
    if-eqz v19, :cond_21b

    #@208
    .line 233
    const/4 v3, 0x1

    #@209
    move-object/from16 v0, p3

    #@20b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@20e
    .line 234
    const/4 v3, 0x1

    #@20f
    move-object/from16 v0, v19

    #@211
    move-object/from16 v1, p3

    #@213
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ActivityInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@216
    .line 239
    :goto_216
    const/4 v3, 0x1

    #@217
    goto/16 :goto_7

    #@219
    .line 224
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ActivityInfo;
    :cond_219
    const/4 v4, 0x0

    #@21a
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    goto :goto_1f5

    #@21b
    .line 237
    .restart local v5       #_arg1:I
    .restart local v6       #_arg2:I
    .restart local v19       #_result:Landroid/content/pm/ActivityInfo;
    :cond_21b
    const/4 v3, 0x0

    #@21c
    move-object/from16 v0, p3

    #@21e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@221
    goto :goto_216

    #@222
    .line 243
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ActivityInfo;
    :sswitch_222
    const-string v3, "android.content.pm.IPackageManager"

    #@224
    move-object/from16 v0, p2

    #@226
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@229
    .line 245
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@22c
    move-result v3

    #@22d
    if-eqz v3, :cond_25d

    #@22f
    .line 246
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@231
    move-object/from16 v0, p2

    #@233
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@236
    move-result-object v4

    #@237
    check-cast v4, Landroid/content/ComponentName;

    #@239
    .line 252
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    :goto_239
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@23c
    move-result v5

    #@23d
    .line 254
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@240
    move-result v6

    #@241
    .line 255
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@243
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->getServiceInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ServiceInfo;

    #@246
    move-result-object v19

    #@247
    .line 256
    .local v19, _result:Landroid/content/pm/ServiceInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@24a
    .line 257
    if-eqz v19, :cond_25f

    #@24c
    .line 258
    const/4 v3, 0x1

    #@24d
    move-object/from16 v0, p3

    #@24f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@252
    .line 259
    const/4 v3, 0x1

    #@253
    move-object/from16 v0, v19

    #@255
    move-object/from16 v1, p3

    #@257
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ServiceInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@25a
    .line 264
    :goto_25a
    const/4 v3, 0x1

    #@25b
    goto/16 :goto_7

    #@25d
    .line 249
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ServiceInfo;
    :cond_25d
    const/4 v4, 0x0

    #@25e
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    goto :goto_239

    #@25f
    .line 262
    .restart local v5       #_arg1:I
    .restart local v6       #_arg2:I
    .restart local v19       #_result:Landroid/content/pm/ServiceInfo;
    :cond_25f
    const/4 v3, 0x0

    #@260
    move-object/from16 v0, p3

    #@262
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@265
    goto :goto_25a

    #@266
    .line 268
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ServiceInfo;
    :sswitch_266
    const-string v3, "android.content.pm.IPackageManager"

    #@268
    move-object/from16 v0, p2

    #@26a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@26d
    .line 270
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@270
    move-result v3

    #@271
    if-eqz v3, :cond_2a1

    #@273
    .line 271
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@275
    move-object/from16 v0, p2

    #@277
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@27a
    move-result-object v4

    #@27b
    check-cast v4, Landroid/content/ComponentName;

    #@27d
    .line 277
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    :goto_27d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@280
    move-result v5

    #@281
    .line 279
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@284
    move-result v6

    #@285
    .line 280
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@287
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->getProviderInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ProviderInfo;

    #@28a
    move-result-object v19

    #@28b
    .line 281
    .local v19, _result:Landroid/content/pm/ProviderInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@28e
    .line 282
    if-eqz v19, :cond_2a3

    #@290
    .line 283
    const/4 v3, 0x1

    #@291
    move-object/from16 v0, p3

    #@293
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@296
    .line 284
    const/4 v3, 0x1

    #@297
    move-object/from16 v0, v19

    #@299
    move-object/from16 v1, p3

    #@29b
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ProviderInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@29e
    .line 289
    :goto_29e
    const/4 v3, 0x1

    #@29f
    goto/16 :goto_7

    #@2a1
    .line 274
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ProviderInfo;
    :cond_2a1
    const/4 v4, 0x0

    #@2a2
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    goto :goto_27d

    #@2a3
    .line 287
    .restart local v5       #_arg1:I
    .restart local v6       #_arg2:I
    .restart local v19       #_result:Landroid/content/pm/ProviderInfo;
    :cond_2a3
    const/4 v3, 0x0

    #@2a4
    move-object/from16 v0, p3

    #@2a6
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2a9
    goto :goto_29e

    #@2aa
    .line 293
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ProviderInfo;
    :sswitch_2aa
    const-string v3, "android.content.pm.IPackageManager"

    #@2ac
    move-object/from16 v0, p2

    #@2ae
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2b1
    .line 295
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2b4
    move-result-object v4

    #@2b5
    .line 297
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2b8
    move-result-object v5

    #@2b9
    .line 298
    .local v5, _arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2bb
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    #@2be
    move-result v19

    #@2bf
    .line 299
    .local v19, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c2
    .line 300
    move-object/from16 v0, p3

    #@2c4
    move/from16 v1, v19

    #@2c6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2c9
    .line 301
    const/4 v3, 0x1

    #@2ca
    goto/16 :goto_7

    #@2cc
    .line 305
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v19           #_result:I
    :sswitch_2cc
    const-string v3, "android.content.pm.IPackageManager"

    #@2ce
    move-object/from16 v0, p2

    #@2d0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d3
    .line 307
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2d6
    move-result-object v4

    #@2d7
    .line 309
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2da
    move-result v5

    #@2db
    .line 310
    .local v5, _arg1:I
    move-object/from16 v0, p0

    #@2dd
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->checkUidPermission(Ljava/lang/String;I)I

    #@2e0
    move-result v19

    #@2e1
    .line 311
    .restart local v19       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e4
    .line 312
    move-object/from16 v0, p3

    #@2e6
    move/from16 v1, v19

    #@2e8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2eb
    .line 313
    const/4 v3, 0x1

    #@2ec
    goto/16 :goto_7

    #@2ee
    .line 317
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v19           #_result:I
    :sswitch_2ee
    const-string v3, "android.content.pm.IPackageManager"

    #@2f0
    move-object/from16 v0, p2

    #@2f2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f5
    .line 319
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2f8
    move-result v3

    #@2f9
    if-eqz v3, :cond_319

    #@2fb
    .line 320
    sget-object v3, Landroid/content/pm/PermissionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2fd
    move-object/from16 v0, p2

    #@2ff
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@302
    move-result-object v4

    #@303
    check-cast v4, Landroid/content/pm/PermissionInfo;

    #@305
    .line 325
    .local v4, _arg0:Landroid/content/pm/PermissionInfo;
    :goto_305
    move-object/from16 v0, p0

    #@307
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->addPermission(Landroid/content/pm/PermissionInfo;)Z

    #@30a
    move-result v19

    #@30b
    .line 326
    .local v19, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@30e
    .line 327
    if-eqz v19, :cond_31b

    #@310
    const/4 v3, 0x1

    #@311
    :goto_311
    move-object/from16 v0, p3

    #@313
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@316
    .line 328
    const/4 v3, 0x1

    #@317
    goto/16 :goto_7

    #@319
    .line 323
    .end local v4           #_arg0:Landroid/content/pm/PermissionInfo;
    .end local v19           #_result:Z
    :cond_319
    const/4 v4, 0x0

    #@31a
    .restart local v4       #_arg0:Landroid/content/pm/PermissionInfo;
    goto :goto_305

    #@31b
    .line 327
    .restart local v19       #_result:Z
    :cond_31b
    const/4 v3, 0x0

    #@31c
    goto :goto_311

    #@31d
    .line 332
    .end local v4           #_arg0:Landroid/content/pm/PermissionInfo;
    .end local v19           #_result:Z
    :sswitch_31d
    const-string v3, "android.content.pm.IPackageManager"

    #@31f
    move-object/from16 v0, p2

    #@321
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@324
    .line 334
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@327
    move-result-object v4

    #@328
    .line 335
    .local v4, _arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@32a
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->removePermission(Ljava/lang/String;)V

    #@32d
    .line 336
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@330
    .line 337
    const/4 v3, 0x1

    #@331
    goto/16 :goto_7

    #@333
    .line 341
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_333
    const-string v3, "android.content.pm.IPackageManager"

    #@335
    move-object/from16 v0, p2

    #@337
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@33a
    .line 343
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@33d
    move-result-object v4

    #@33e
    .line 345
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@341
    move-result-object v5

    #@342
    .line 346
    .local v5, _arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@344
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->grantPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@347
    .line 347
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@34a
    .line 348
    const/4 v3, 0x1

    #@34b
    goto/16 :goto_7

    #@34d
    .line 352
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    :sswitch_34d
    const-string v3, "android.content.pm.IPackageManager"

    #@34f
    move-object/from16 v0, p2

    #@351
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@354
    .line 354
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@357
    move-result-object v4

    #@358
    .line 356
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35b
    move-result-object v5

    #@35c
    .line 357
    .restart local v5       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@35e
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->revokePermission(Ljava/lang/String;Ljava/lang/String;)V

    #@361
    .line 358
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@364
    .line 359
    const/4 v3, 0x1

    #@365
    goto/16 :goto_7

    #@367
    .line 363
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    :sswitch_367
    const-string v3, "android.content.pm.IPackageManager"

    #@369
    move-object/from16 v0, p2

    #@36b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36e
    .line 365
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@371
    move-result-object v4

    #@372
    .line 366
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@374
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->isProtectedBroadcast(Ljava/lang/String;)Z

    #@377
    move-result v19

    #@378
    .line 367
    .restart local v19       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@37b
    .line 368
    if-eqz v19, :cond_386

    #@37d
    const/4 v3, 0x1

    #@37e
    :goto_37e
    move-object/from16 v0, p3

    #@380
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@383
    .line 369
    const/4 v3, 0x1

    #@384
    goto/16 :goto_7

    #@386
    .line 368
    :cond_386
    const/4 v3, 0x0

    #@387
    goto :goto_37e

    #@388
    .line 373
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v19           #_result:Z
    :sswitch_388
    const-string v3, "android.content.pm.IPackageManager"

    #@38a
    move-object/from16 v0, p2

    #@38c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@38f
    .line 375
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@392
    move-result-object v4

    #@393
    .line 377
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@396
    move-result-object v5

    #@397
    .line 378
    .restart local v5       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@399
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    #@39c
    move-result v19

    #@39d
    .line 379
    .local v19, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a0
    .line 380
    move-object/from16 v0, p3

    #@3a2
    move/from16 v1, v19

    #@3a4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3a7
    .line 381
    const/4 v3, 0x1

    #@3a8
    goto/16 :goto_7

    #@3aa
    .line 385
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v19           #_result:I
    :sswitch_3aa
    const-string v3, "android.content.pm.IPackageManager"

    #@3ac
    move-object/from16 v0, p2

    #@3ae
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b1
    .line 387
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3b4
    move-result v4

    #@3b5
    .line 389
    .local v4, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3b8
    move-result v5

    #@3b9
    .line 390
    .local v5, _arg1:I
    move-object/from16 v0, p0

    #@3bb
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->checkUidSignatures(II)I

    #@3be
    move-result v19

    #@3bf
    .line 391
    .restart local v19       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c2
    .line 392
    move-object/from16 v0, p3

    #@3c4
    move/from16 v1, v19

    #@3c6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3c9
    .line 393
    const/4 v3, 0x1

    #@3ca
    goto/16 :goto_7

    #@3cc
    .line 397
    .end local v4           #_arg0:I
    .end local v5           #_arg1:I
    .end local v19           #_result:I
    :sswitch_3cc
    const-string v3, "android.content.pm.IPackageManager"

    #@3ce
    move-object/from16 v0, p2

    #@3d0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d3
    .line 399
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3d6
    move-result v4

    #@3d7
    .line 400
    .restart local v4       #_arg0:I
    move-object/from16 v0, p0

    #@3d9
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->getPackagesForUid(I)[Ljava/lang/String;

    #@3dc
    move-result-object v19

    #@3dd
    .line 401
    .local v19, _result:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e0
    .line 402
    move-object/from16 v0, p3

    #@3e2
    move-object/from16 v1, v19

    #@3e4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@3e7
    .line 403
    const/4 v3, 0x1

    #@3e8
    goto/16 :goto_7

    #@3ea
    .line 407
    .end local v4           #_arg0:I
    .end local v19           #_result:[Ljava/lang/String;
    :sswitch_3ea
    const-string v3, "android.content.pm.IPackageManager"

    #@3ec
    move-object/from16 v0, p2

    #@3ee
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3f1
    .line 409
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3f4
    move-result v4

    #@3f5
    .line 410
    .restart local v4       #_arg0:I
    move-object/from16 v0, p0

    #@3f7
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->getNameForUid(I)Ljava/lang/String;

    #@3fa
    move-result-object v19

    #@3fb
    .line 411
    .local v19, _result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3fe
    .line 412
    move-object/from16 v0, p3

    #@400
    move-object/from16 v1, v19

    #@402
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@405
    .line 413
    const/4 v3, 0x1

    #@406
    goto/16 :goto_7

    #@408
    .line 417
    .end local v4           #_arg0:I
    .end local v19           #_result:Ljava/lang/String;
    :sswitch_408
    const-string v3, "android.content.pm.IPackageManager"

    #@40a
    move-object/from16 v0, p2

    #@40c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40f
    .line 419
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@412
    move-result-object v4

    #@413
    .line 420
    .local v4, _arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@415
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->getUidForSharedUser(Ljava/lang/String;)I

    #@418
    move-result v19

    #@419
    .line 421
    .local v19, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@41c
    .line 422
    move-object/from16 v0, p3

    #@41e
    move/from16 v1, v19

    #@420
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@423
    .line 423
    const/4 v3, 0x1

    #@424
    goto/16 :goto_7

    #@426
    .line 427
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v19           #_result:I
    :sswitch_426
    const-string v3, "android.content.pm.IPackageManager"

    #@428
    move-object/from16 v0, p2

    #@42a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@42d
    .line 429
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@430
    move-result v3

    #@431
    if-eqz v3, :cond_465

    #@433
    .line 430
    sget-object v3, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@435
    move-object/from16 v0, p2

    #@437
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@43a
    move-result-object v4

    #@43b
    check-cast v4, Landroid/content/Intent;

    #@43d
    .line 436
    .local v4, _arg0:Landroid/content/Intent;
    :goto_43d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@440
    move-result-object v5

    #@441
    .line 438
    .local v5, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@444
    move-result v6

    #@445
    .line 440
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@448
    move-result v7

    #@449
    .line 441
    .local v7, _arg3:I
    move-object/from16 v0, p0

    #@44b
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/content/pm/IPackageManager$Stub;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;II)Landroid/content/pm/ResolveInfo;

    #@44e
    move-result-object v19

    #@44f
    .line 442
    .local v19, _result:Landroid/content/pm/ResolveInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@452
    .line 443
    if-eqz v19, :cond_467

    #@454
    .line 444
    const/4 v3, 0x1

    #@455
    move-object/from16 v0, p3

    #@457
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@45a
    .line 445
    const/4 v3, 0x1

    #@45b
    move-object/from16 v0, v19

    #@45d
    move-object/from16 v1, p3

    #@45f
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ResolveInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@462
    .line 450
    :goto_462
    const/4 v3, 0x1

    #@463
    goto/16 :goto_7

    #@465
    .line 433
    .end local v4           #_arg0:Landroid/content/Intent;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    .end local v19           #_result:Landroid/content/pm/ResolveInfo;
    :cond_465
    const/4 v4, 0x0

    #@466
    .restart local v4       #_arg0:Landroid/content/Intent;
    goto :goto_43d

    #@467
    .line 448
    .restart local v5       #_arg1:Ljava/lang/String;
    .restart local v6       #_arg2:I
    .restart local v7       #_arg3:I
    .restart local v19       #_result:Landroid/content/pm/ResolveInfo;
    :cond_467
    const/4 v3, 0x0

    #@468
    move-object/from16 v0, p3

    #@46a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@46d
    goto :goto_462

    #@46e
    .line 454
    .end local v4           #_arg0:Landroid/content/Intent;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    .end local v19           #_result:Landroid/content/pm/ResolveInfo;
    :sswitch_46e
    const-string v3, "android.content.pm.IPackageManager"

    #@470
    move-object/from16 v0, p2

    #@472
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@475
    .line 456
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@478
    move-result v3

    #@479
    if-eqz v3, :cond_4a4

    #@47b
    .line 457
    sget-object v3, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@47d
    move-object/from16 v0, p2

    #@47f
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@482
    move-result-object v4

    #@483
    check-cast v4, Landroid/content/Intent;

    #@485
    .line 463
    .restart local v4       #_arg0:Landroid/content/Intent;
    :goto_485
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@488
    move-result-object v5

    #@489
    .line 465
    .restart local v5       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@48c
    move-result v6

    #@48d
    .line 467
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@490
    move-result v7

    #@491
    .line 468
    .restart local v7       #_arg3:I
    move-object/from16 v0, p0

    #@493
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/content/pm/IPackageManager$Stub;->queryIntentActivities(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    #@496
    move-result-object v26

    #@497
    .line 469
    .local v26, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@49a
    .line 470
    move-object/from16 v0, p3

    #@49c
    move-object/from16 v1, v26

    #@49e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@4a1
    .line 471
    const/4 v3, 0x1

    #@4a2
    goto/16 :goto_7

    #@4a4
    .line 460
    .end local v4           #_arg0:Landroid/content/Intent;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    .end local v26           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_4a4
    const/4 v4, 0x0

    #@4a5
    .restart local v4       #_arg0:Landroid/content/Intent;
    goto :goto_485

    #@4a6
    .line 475
    .end local v4           #_arg0:Landroid/content/Intent;
    :sswitch_4a6
    const-string v3, "android.content.pm.IPackageManager"

    #@4a8
    move-object/from16 v0, p2

    #@4aa
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4ad
    .line 477
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4b0
    move-result v3

    #@4b1
    if-eqz v3, :cond_4fa

    #@4b3
    .line 478
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4b5
    move-object/from16 v0, p2

    #@4b7
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4ba
    move-result-object v4

    #@4bb
    check-cast v4, Landroid/content/ComponentName;

    #@4bd
    .line 484
    .local v4, _arg0:Landroid/content/ComponentName;
    :goto_4bd
    sget-object v3, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4bf
    move-object/from16 v0, p2

    #@4c1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@4c4
    move-result-object v5

    #@4c5
    check-cast v5, [Landroid/content/Intent;

    #@4c7
    .line 486
    .local v5, _arg1:[Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@4ca
    move-result-object v6

    #@4cb
    .line 488
    .local v6, _arg2:[Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4ce
    move-result v3

    #@4cf
    if-eqz v3, :cond_4fc

    #@4d1
    .line 489
    sget-object v3, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4d3
    move-object/from16 v0, p2

    #@4d5
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4d8
    move-result-object v7

    #@4d9
    check-cast v7, Landroid/content/Intent;

    #@4db
    .line 495
    .local v7, _arg3:Landroid/content/Intent;
    :goto_4db
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4de
    move-result-object v8

    #@4df
    .line 497
    .local v8, _arg4:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4e2
    move-result v9

    #@4e3
    .line 499
    .local v9, _arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4e6
    move-result v10

    #@4e7
    .local v10, _arg6:I
    move-object/from16 v3, p0

    #@4e9
    .line 500
    invoke-virtual/range {v3 .. v10}, Landroid/content/pm/IPackageManager$Stub;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;[Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    #@4ec
    move-result-object v26

    #@4ed
    .line 501
    .restart local v26       #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f0
    .line 502
    move-object/from16 v0, p3

    #@4f2
    move-object/from16 v1, v26

    #@4f4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@4f7
    .line 503
    const/4 v3, 0x1

    #@4f8
    goto/16 :goto_7

    #@4fa
    .line 481
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:[Landroid/content/Intent;
    .end local v6           #_arg2:[Ljava/lang/String;
    .end local v7           #_arg3:Landroid/content/Intent;
    .end local v8           #_arg4:Ljava/lang/String;
    .end local v9           #_arg5:I
    .end local v10           #_arg6:I
    .end local v26           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_4fa
    const/4 v4, 0x0

    #@4fb
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    goto :goto_4bd

    #@4fc
    .line 492
    .restart local v5       #_arg1:[Landroid/content/Intent;
    .restart local v6       #_arg2:[Ljava/lang/String;
    :cond_4fc
    const/4 v7, 0x0

    #@4fd
    .restart local v7       #_arg3:Landroid/content/Intent;
    goto :goto_4db

    #@4fe
    .line 507
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:[Landroid/content/Intent;
    .end local v6           #_arg2:[Ljava/lang/String;
    .end local v7           #_arg3:Landroid/content/Intent;
    :sswitch_4fe
    const-string v3, "android.content.pm.IPackageManager"

    #@500
    move-object/from16 v0, p2

    #@502
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@505
    .line 509
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@508
    move-result v3

    #@509
    if-eqz v3, :cond_534

    #@50b
    .line 510
    sget-object v3, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@50d
    move-object/from16 v0, p2

    #@50f
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@512
    move-result-object v4

    #@513
    check-cast v4, Landroid/content/Intent;

    #@515
    .line 516
    .local v4, _arg0:Landroid/content/Intent;
    :goto_515
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@518
    move-result-object v5

    #@519
    .line 518
    .local v5, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@51c
    move-result v6

    #@51d
    .line 520
    .local v6, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@520
    move-result v7

    #@521
    .line 521
    .local v7, _arg3:I
    move-object/from16 v0, p0

    #@523
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/content/pm/IPackageManager$Stub;->queryIntentReceivers(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    #@526
    move-result-object v26

    #@527
    .line 522
    .restart local v26       #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@52a
    .line 523
    move-object/from16 v0, p3

    #@52c
    move-object/from16 v1, v26

    #@52e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@531
    .line 524
    const/4 v3, 0x1

    #@532
    goto/16 :goto_7

    #@534
    .line 513
    .end local v4           #_arg0:Landroid/content/Intent;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    .end local v26           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_534
    const/4 v4, 0x0

    #@535
    .restart local v4       #_arg0:Landroid/content/Intent;
    goto :goto_515

    #@536
    .line 528
    .end local v4           #_arg0:Landroid/content/Intent;
    :sswitch_536
    const-string v3, "android.content.pm.IPackageManager"

    #@538
    move-object/from16 v0, p2

    #@53a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@53d
    .line 530
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@540
    move-result v3

    #@541
    if-eqz v3, :cond_575

    #@543
    .line 531
    sget-object v3, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@545
    move-object/from16 v0, p2

    #@547
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@54a
    move-result-object v4

    #@54b
    check-cast v4, Landroid/content/Intent;

    #@54d
    .line 537
    .restart local v4       #_arg0:Landroid/content/Intent;
    :goto_54d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@550
    move-result-object v5

    #@551
    .line 539
    .restart local v5       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@554
    move-result v6

    #@555
    .line 541
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@558
    move-result v7

    #@559
    .line 542
    .restart local v7       #_arg3:I
    move-object/from16 v0, p0

    #@55b
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/content/pm/IPackageManager$Stub;->resolveService(Landroid/content/Intent;Ljava/lang/String;II)Landroid/content/pm/ResolveInfo;

    #@55e
    move-result-object v19

    #@55f
    .line 543
    .restart local v19       #_result:Landroid/content/pm/ResolveInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@562
    .line 544
    if-eqz v19, :cond_577

    #@564
    .line 545
    const/4 v3, 0x1

    #@565
    move-object/from16 v0, p3

    #@567
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@56a
    .line 546
    const/4 v3, 0x1

    #@56b
    move-object/from16 v0, v19

    #@56d
    move-object/from16 v1, p3

    #@56f
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ResolveInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@572
    .line 551
    :goto_572
    const/4 v3, 0x1

    #@573
    goto/16 :goto_7

    #@575
    .line 534
    .end local v4           #_arg0:Landroid/content/Intent;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    .end local v19           #_result:Landroid/content/pm/ResolveInfo;
    :cond_575
    const/4 v4, 0x0

    #@576
    .restart local v4       #_arg0:Landroid/content/Intent;
    goto :goto_54d

    #@577
    .line 549
    .restart local v5       #_arg1:Ljava/lang/String;
    .restart local v6       #_arg2:I
    .restart local v7       #_arg3:I
    .restart local v19       #_result:Landroid/content/pm/ResolveInfo;
    :cond_577
    const/4 v3, 0x0

    #@578
    move-object/from16 v0, p3

    #@57a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@57d
    goto :goto_572

    #@57e
    .line 555
    .end local v4           #_arg0:Landroid/content/Intent;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    .end local v19           #_result:Landroid/content/pm/ResolveInfo;
    :sswitch_57e
    const-string v3, "android.content.pm.IPackageManager"

    #@580
    move-object/from16 v0, p2

    #@582
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@585
    .line 557
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@588
    move-result v3

    #@589
    if-eqz v3, :cond_5b4

    #@58b
    .line 558
    sget-object v3, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@58d
    move-object/from16 v0, p2

    #@58f
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@592
    move-result-object v4

    #@593
    check-cast v4, Landroid/content/Intent;

    #@595
    .line 564
    .restart local v4       #_arg0:Landroid/content/Intent;
    :goto_595
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@598
    move-result-object v5

    #@599
    .line 566
    .restart local v5       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@59c
    move-result v6

    #@59d
    .line 568
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5a0
    move-result v7

    #@5a1
    .line 569
    .restart local v7       #_arg3:I
    move-object/from16 v0, p0

    #@5a3
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/content/pm/IPackageManager$Stub;->queryIntentServices(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    #@5a6
    move-result-object v26

    #@5a7
    .line 570
    .restart local v26       #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5aa
    .line 571
    move-object/from16 v0, p3

    #@5ac
    move-object/from16 v1, v26

    #@5ae
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@5b1
    .line 572
    const/4 v3, 0x1

    #@5b2
    goto/16 :goto_7

    #@5b4
    .line 561
    .end local v4           #_arg0:Landroid/content/Intent;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    .end local v26           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_5b4
    const/4 v4, 0x0

    #@5b5
    .restart local v4       #_arg0:Landroid/content/Intent;
    goto :goto_595

    #@5b6
    .line 576
    .end local v4           #_arg0:Landroid/content/Intent;
    :sswitch_5b6
    const-string v3, "android.content.pm.IPackageManager"

    #@5b8
    move-object/from16 v0, p2

    #@5ba
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5bd
    .line 578
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5c0
    move-result v4

    #@5c1
    .line 580
    .local v4, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5c4
    move-result-object v5

    #@5c5
    .line 582
    .restart local v5       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5c8
    move-result v6

    #@5c9
    .line 583
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@5cb
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->getInstalledPackages(ILjava/lang/String;I)Landroid/content/pm/ParceledListSlice;

    #@5ce
    move-result-object v19

    #@5cf
    .line 584
    .local v19, _result:Landroid/content/pm/ParceledListSlice;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5d2
    .line 585
    if-eqz v19, :cond_5e5

    #@5d4
    .line 586
    const/4 v3, 0x1

    #@5d5
    move-object/from16 v0, p3

    #@5d7
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5da
    .line 587
    const/4 v3, 0x1

    #@5db
    move-object/from16 v0, v19

    #@5dd
    move-object/from16 v1, p3

    #@5df
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ParceledListSlice;->writeToParcel(Landroid/os/Parcel;I)V

    #@5e2
    .line 592
    :goto_5e2
    const/4 v3, 0x1

    #@5e3
    goto/16 :goto_7

    #@5e5
    .line 590
    :cond_5e5
    const/4 v3, 0x0

    #@5e6
    move-object/from16 v0, p3

    #@5e8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5eb
    goto :goto_5e2

    #@5ec
    .line 596
    .end local v4           #_arg0:I
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ParceledListSlice;
    :sswitch_5ec
    const-string v3, "android.content.pm.IPackageManager"

    #@5ee
    move-object/from16 v0, p2

    #@5f0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5f3
    .line 598
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5f6
    move-result v4

    #@5f7
    .line 600
    .restart local v4       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5fa
    move-result-object v5

    #@5fb
    .line 602
    .restart local v5       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5fe
    move-result v6

    #@5ff
    .line 603
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@601
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->getInstalledApplications(ILjava/lang/String;I)Landroid/content/pm/ParceledListSlice;

    #@604
    move-result-object v19

    #@605
    .line 604
    .restart local v19       #_result:Landroid/content/pm/ParceledListSlice;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@608
    .line 605
    if-eqz v19, :cond_61b

    #@60a
    .line 606
    const/4 v3, 0x1

    #@60b
    move-object/from16 v0, p3

    #@60d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@610
    .line 607
    const/4 v3, 0x1

    #@611
    move-object/from16 v0, v19

    #@613
    move-object/from16 v1, p3

    #@615
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ParceledListSlice;->writeToParcel(Landroid/os/Parcel;I)V

    #@618
    .line 612
    :goto_618
    const/4 v3, 0x1

    #@619
    goto/16 :goto_7

    #@61b
    .line 610
    :cond_61b
    const/4 v3, 0x0

    #@61c
    move-object/from16 v0, p3

    #@61e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@621
    goto :goto_618

    #@622
    .line 616
    .end local v4           #_arg0:I
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ParceledListSlice;
    :sswitch_622
    const-string v3, "android.content.pm.IPackageManager"

    #@624
    move-object/from16 v0, p2

    #@626
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@629
    .line 618
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@62c
    move-result v4

    #@62d
    .line 619
    .restart local v4       #_arg0:I
    move-object/from16 v0, p0

    #@62f
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->getPersistentApplications(I)Ljava/util/List;

    #@632
    move-result-object v20

    #@633
    .line 620
    .local v20, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@636
    .line 621
    move-object/from16 v0, p3

    #@638
    move-object/from16 v1, v20

    #@63a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@63d
    .line 622
    const/4 v3, 0x1

    #@63e
    goto/16 :goto_7

    #@640
    .line 626
    .end local v4           #_arg0:I
    .end local v20           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    :sswitch_640
    const-string v3, "android.content.pm.IPackageManager"

    #@642
    move-object/from16 v0, p2

    #@644
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@647
    .line 628
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@64a
    move-result-object v4

    #@64b
    .line 630
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@64e
    move-result v5

    #@64f
    .line 632
    .local v5, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@652
    move-result v6

    #@653
    .line 633
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@655
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->resolveContentProvider(Ljava/lang/String;II)Landroid/content/pm/ProviderInfo;

    #@658
    move-result-object v19

    #@659
    .line 634
    .local v19, _result:Landroid/content/pm/ProviderInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@65c
    .line 635
    if-eqz v19, :cond_66f

    #@65e
    .line 636
    const/4 v3, 0x1

    #@65f
    move-object/from16 v0, p3

    #@661
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@664
    .line 637
    const/4 v3, 0x1

    #@665
    move-object/from16 v0, v19

    #@667
    move-object/from16 v1, p3

    #@669
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ProviderInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@66c
    .line 642
    :goto_66c
    const/4 v3, 0x1

    #@66d
    goto/16 :goto_7

    #@66f
    .line 640
    :cond_66f
    const/4 v3, 0x0

    #@670
    move-object/from16 v0, p3

    #@672
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@675
    goto :goto_66c

    #@676
    .line 646
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v19           #_result:Landroid/content/pm/ProviderInfo;
    :sswitch_676
    const-string v3, "android.content.pm.IPackageManager"

    #@678
    move-object/from16 v0, p2

    #@67a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@67d
    .line 648
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@680
    move-result-object v14

    #@681
    .line 650
    .local v14, _arg0:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    sget-object v3, Landroid/content/pm/ProviderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@683
    move-object/from16 v0, p2

    #@685
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@688
    move-result-object v16

    #@689
    .line 651
    .local v16, _arg1:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    move-object/from16 v0, p0

    #@68b
    move-object/from16 v1, v16

    #@68d
    invoke-virtual {v0, v14, v1}, Landroid/content/pm/IPackageManager$Stub;->querySyncProviders(Ljava/util/List;Ljava/util/List;)V

    #@690
    .line 652
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@693
    .line 653
    move-object/from16 v0, p3

    #@695
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@698
    .line 654
    move-object/from16 v0, p3

    #@69a
    move-object/from16 v1, v16

    #@69c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@69f
    .line 655
    const/4 v3, 0x1

    #@6a0
    goto/16 :goto_7

    #@6a2
    .line 659
    .end local v14           #_arg0:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v16           #_arg1:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    :sswitch_6a2
    const-string v3, "android.content.pm.IPackageManager"

    #@6a4
    move-object/from16 v0, p2

    #@6a6
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6a9
    .line 661
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6ac
    move-result-object v4

    #@6ad
    .line 663
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6b0
    move-result v5

    #@6b1
    .line 665
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6b4
    move-result v6

    #@6b5
    .line 666
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@6b7
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->queryContentProviders(Ljava/lang/String;II)Ljava/util/List;

    #@6ba
    move-result-object v25

    #@6bb
    .line 667
    .local v25, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6be
    .line 668
    move-object/from16 v0, p3

    #@6c0
    move-object/from16 v1, v25

    #@6c2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@6c5
    .line 669
    const/4 v3, 0x1

    #@6c6
    goto/16 :goto_7

    #@6c8
    .line 673
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v25           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    :sswitch_6c8
    const-string v3, "android.content.pm.IPackageManager"

    #@6ca
    move-object/from16 v0, p2

    #@6cc
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6cf
    .line 675
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6d2
    move-result v3

    #@6d3
    if-eqz v3, :cond_6ff

    #@6d5
    .line 676
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6d7
    move-object/from16 v0, p2

    #@6d9
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6dc
    move-result-object v4

    #@6dd
    check-cast v4, Landroid/content/ComponentName;

    #@6df
    .line 682
    .local v4, _arg0:Landroid/content/ComponentName;
    :goto_6df
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6e2
    move-result v5

    #@6e3
    .line 683
    .restart local v5       #_arg1:I
    move-object/from16 v0, p0

    #@6e5
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->getInstrumentationInfo(Landroid/content/ComponentName;I)Landroid/content/pm/InstrumentationInfo;

    #@6e8
    move-result-object v19

    #@6e9
    .line 684
    .local v19, _result:Landroid/content/pm/InstrumentationInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6ec
    .line 685
    if-eqz v19, :cond_701

    #@6ee
    .line 686
    const/4 v3, 0x1

    #@6ef
    move-object/from16 v0, p3

    #@6f1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@6f4
    .line 687
    const/4 v3, 0x1

    #@6f5
    move-object/from16 v0, v19

    #@6f7
    move-object/from16 v1, p3

    #@6f9
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/InstrumentationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@6fc
    .line 692
    :goto_6fc
    const/4 v3, 0x1

    #@6fd
    goto/16 :goto_7

    #@6ff
    .line 679
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v19           #_result:Landroid/content/pm/InstrumentationInfo;
    :cond_6ff
    const/4 v4, 0x0

    #@700
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    goto :goto_6df

    #@701
    .line 690
    .restart local v5       #_arg1:I
    .restart local v19       #_result:Landroid/content/pm/InstrumentationInfo;
    :cond_701
    const/4 v3, 0x0

    #@702
    move-object/from16 v0, p3

    #@704
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@707
    goto :goto_6fc

    #@708
    .line 696
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v19           #_result:Landroid/content/pm/InstrumentationInfo;
    :sswitch_708
    const-string v3, "android.content.pm.IPackageManager"

    #@70a
    move-object/from16 v0, p2

    #@70c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70f
    .line 698
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@712
    move-result-object v4

    #@713
    .line 700
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@716
    move-result v5

    #@717
    .line 701
    .restart local v5       #_arg1:I
    move-object/from16 v0, p0

    #@719
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->queryInstrumentation(Ljava/lang/String;I)Ljava/util/List;

    #@71c
    move-result-object v21

    #@71d
    .line 702
    .local v21, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/InstrumentationInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@720
    .line 703
    move-object/from16 v0, p3

    #@722
    move-object/from16 v1, v21

    #@724
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@727
    .line 704
    const/4 v3, 0x1

    #@728
    goto/16 :goto_7

    #@72a
    .line 708
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v21           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/InstrumentationInfo;>;"
    :sswitch_72a
    const-string v3, "android.content.pm.IPackageManager"

    #@72c
    move-object/from16 v0, p2

    #@72e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@731
    .line 710
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@734
    move-result v3

    #@735
    if-eqz v3, :cond_75c

    #@737
    .line 711
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@739
    move-object/from16 v0, p2

    #@73b
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@73e
    move-result-object v4

    #@73f
    check-cast v4, Landroid/net/Uri;

    #@741
    .line 717
    .local v4, _arg0:Landroid/net/Uri;
    :goto_741
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@744
    move-result-object v3

    #@745
    invoke-static {v3}, Landroid/content/pm/IPackageInstallObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageInstallObserver;

    #@748
    move-result-object v5

    #@749
    .line 719
    .local v5, _arg1:Landroid/content/pm/IPackageInstallObserver;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@74c
    move-result v6

    #@74d
    .line 721
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@750
    move-result-object v7

    #@751
    .line 722
    .local v7, _arg3:Ljava/lang/String;
    move-object/from16 v0, p0

    #@753
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/content/pm/IPackageManager$Stub;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    #@756
    .line 723
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@759
    .line 724
    const/4 v3, 0x1

    #@75a
    goto/16 :goto_7

    #@75c
    .line 714
    .end local v4           #_arg0:Landroid/net/Uri;
    .end local v5           #_arg1:Landroid/content/pm/IPackageInstallObserver;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:Ljava/lang/String;
    :cond_75c
    const/4 v4, 0x0

    #@75d
    .restart local v4       #_arg0:Landroid/net/Uri;
    goto :goto_741

    #@75e
    .line 728
    .end local v4           #_arg0:Landroid/net/Uri;
    :sswitch_75e
    const-string v3, "android.content.pm.IPackageManager"

    #@760
    move-object/from16 v0, p2

    #@762
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@765
    .line 730
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@768
    move-result v4

    #@769
    .line 731
    .local v4, _arg0:I
    move-object/from16 v0, p0

    #@76b
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->finishPackageInstall(I)V

    #@76e
    .line 732
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@771
    .line 733
    const/4 v3, 0x1

    #@772
    goto/16 :goto_7

    #@774
    .line 737
    .end local v4           #_arg0:I
    :sswitch_774
    const-string v3, "android.content.pm.IPackageManager"

    #@776
    move-object/from16 v0, p2

    #@778
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@77b
    .line 739
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@77e
    move-result-object v4

    #@77f
    .line 741
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@782
    move-result-object v5

    #@783
    .line 742
    .local v5, _arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@785
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->setInstallerPackageName(Ljava/lang/String;Ljava/lang/String;)V

    #@788
    .line 743
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@78b
    .line 744
    const/4 v3, 0x1

    #@78c
    goto/16 :goto_7

    #@78e
    .line 748
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    :sswitch_78e
    const-string v3, "android.content.pm.IPackageManager"

    #@790
    move-object/from16 v0, p2

    #@792
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@795
    .line 750
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@798
    move-result-object v4

    #@799
    .line 752
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@79c
    move-result-object v3

    #@79d
    invoke-static {v3}, Landroid/content/pm/IPackageDeleteObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageDeleteObserver;

    #@7a0
    move-result-object v5

    #@7a1
    .line 754
    .local v5, _arg1:Landroid/content/pm/IPackageDeleteObserver;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7a4
    move-result v6

    #@7a5
    .line 755
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@7a7
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V

    #@7aa
    .line 756
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7ad
    .line 757
    const/4 v3, 0x1

    #@7ae
    goto/16 :goto_7

    #@7b0
    .line 761
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/content/pm/IPackageDeleteObserver;
    .end local v6           #_arg2:I
    :sswitch_7b0
    const-string v3, "android.content.pm.IPackageManager"

    #@7b2
    move-object/from16 v0, p2

    #@7b4
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7b7
    .line 763
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7ba
    move-result-object v4

    #@7bb
    .line 764
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@7bd
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    #@7c0
    move-result-object v19

    #@7c1
    .line 765
    .local v19, _result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7c4
    .line 766
    move-object/from16 v0, p3

    #@7c6
    move-object/from16 v1, v19

    #@7c8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7cb
    .line 767
    const/4 v3, 0x1

    #@7cc
    goto/16 :goto_7

    #@7ce
    .line 771
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v19           #_result:Ljava/lang/String;
    :sswitch_7ce
    const-string v3, "android.content.pm.IPackageManager"

    #@7d0
    move-object/from16 v0, p2

    #@7d2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7d5
    .line 773
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7d8
    move-result-object v4

    #@7d9
    .line 774
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@7db
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->addPackageToPreferred(Ljava/lang/String;)V

    #@7de
    .line 775
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7e1
    .line 776
    const/4 v3, 0x1

    #@7e2
    goto/16 :goto_7

    #@7e4
    .line 780
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_7e4
    const-string v3, "android.content.pm.IPackageManager"

    #@7e6
    move-object/from16 v0, p2

    #@7e8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7eb
    .line 782
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7ee
    move-result-object v4

    #@7ef
    .line 783
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@7f1
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->removePackageFromPreferred(Ljava/lang/String;)V

    #@7f4
    .line 784
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7f7
    .line 785
    const/4 v3, 0x1

    #@7f8
    goto/16 :goto_7

    #@7fa
    .line 789
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_7fa
    const-string v3, "android.content.pm.IPackageManager"

    #@7fc
    move-object/from16 v0, p2

    #@7fe
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@801
    .line 791
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@804
    move-result v4

    #@805
    .line 792
    .local v4, _arg0:I
    move-object/from16 v0, p0

    #@807
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->getPreferredPackages(I)Ljava/util/List;

    #@80a
    move-result-object v22

    #@80b
    .line 793
    .local v22, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@80e
    .line 794
    move-object/from16 v0, p3

    #@810
    move-object/from16 v1, v22

    #@812
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@815
    .line 795
    const/4 v3, 0x1

    #@816
    goto/16 :goto_7

    #@818
    .line 799
    .end local v4           #_arg0:I
    .end local v22           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :sswitch_818
    const-string v3, "android.content.pm.IPackageManager"

    #@81a
    move-object/from16 v0, p2

    #@81c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@81f
    .line 801
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@822
    move-result v3

    #@823
    if-eqz v3, :cond_85c

    #@825
    .line 802
    sget-object v3, Landroid/content/IntentFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    #@827
    move-object/from16 v0, p2

    #@829
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@82c
    move-result-object v4

    #@82d
    check-cast v4, Landroid/content/IntentFilter;

    #@82f
    .line 808
    .local v4, _arg0:Landroid/content/IntentFilter;
    :goto_82f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@832
    move-result v5

    #@833
    .line 810
    .local v5, _arg1:I
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@835
    move-object/from16 v0, p2

    #@837
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@83a
    move-result-object v6

    #@83b
    check-cast v6, [Landroid/content/ComponentName;

    #@83d
    .line 812
    .local v6, _arg2:[Landroid/content/ComponentName;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@840
    move-result v3

    #@841
    if-eqz v3, :cond_85e

    #@843
    .line 813
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@845
    move-object/from16 v0, p2

    #@847
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@84a
    move-result-object v7

    #@84b
    check-cast v7, Landroid/content/ComponentName;

    #@84d
    .line 819
    .local v7, _arg3:Landroid/content/ComponentName;
    :goto_84d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@850
    move-result v8

    #@851
    .local v8, _arg4:I
    move-object/from16 v3, p0

    #@853
    .line 820
    invoke-virtual/range {v3 .. v8}, Landroid/content/pm/IPackageManager$Stub;->addPreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;I)V

    #@856
    .line 821
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@859
    .line 822
    const/4 v3, 0x1

    #@85a
    goto/16 :goto_7

    #@85c
    .line 805
    .end local v4           #_arg0:Landroid/content/IntentFilter;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:[Landroid/content/ComponentName;
    .end local v7           #_arg3:Landroid/content/ComponentName;
    .end local v8           #_arg4:I
    :cond_85c
    const/4 v4, 0x0

    #@85d
    .restart local v4       #_arg0:Landroid/content/IntentFilter;
    goto :goto_82f

    #@85e
    .line 816
    .restart local v5       #_arg1:I
    .restart local v6       #_arg2:[Landroid/content/ComponentName;
    :cond_85e
    const/4 v7, 0x0

    #@85f
    .restart local v7       #_arg3:Landroid/content/ComponentName;
    goto :goto_84d

    #@860
    .line 826
    .end local v4           #_arg0:Landroid/content/IntentFilter;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:[Landroid/content/ComponentName;
    .end local v7           #_arg3:Landroid/content/ComponentName;
    :sswitch_860
    const-string v3, "android.content.pm.IPackageManager"

    #@862
    move-object/from16 v0, p2

    #@864
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@867
    .line 828
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@86a
    move-result v3

    #@86b
    if-eqz v3, :cond_8a0

    #@86d
    .line 829
    sget-object v3, Landroid/content/IntentFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    #@86f
    move-object/from16 v0, p2

    #@871
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@874
    move-result-object v4

    #@875
    check-cast v4, Landroid/content/IntentFilter;

    #@877
    .line 835
    .restart local v4       #_arg0:Landroid/content/IntentFilter;
    :goto_877
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@87a
    move-result v5

    #@87b
    .line 837
    .restart local v5       #_arg1:I
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@87d
    move-object/from16 v0, p2

    #@87f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@882
    move-result-object v6

    #@883
    check-cast v6, [Landroid/content/ComponentName;

    #@885
    .line 839
    .restart local v6       #_arg2:[Landroid/content/ComponentName;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@888
    move-result v3

    #@889
    if-eqz v3, :cond_8a2

    #@88b
    .line 840
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@88d
    move-object/from16 v0, p2

    #@88f
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@892
    move-result-object v7

    #@893
    check-cast v7, Landroid/content/ComponentName;

    #@895
    .line 845
    .restart local v7       #_arg3:Landroid/content/ComponentName;
    :goto_895
    move-object/from16 v0, p0

    #@897
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/content/pm/IPackageManager$Stub;->replacePreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    #@89a
    .line 846
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@89d
    .line 847
    const/4 v3, 0x1

    #@89e
    goto/16 :goto_7

    #@8a0
    .line 832
    .end local v4           #_arg0:Landroid/content/IntentFilter;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:[Landroid/content/ComponentName;
    .end local v7           #_arg3:Landroid/content/ComponentName;
    :cond_8a0
    const/4 v4, 0x0

    #@8a1
    .restart local v4       #_arg0:Landroid/content/IntentFilter;
    goto :goto_877

    #@8a2
    .line 843
    .restart local v5       #_arg1:I
    .restart local v6       #_arg2:[Landroid/content/ComponentName;
    :cond_8a2
    const/4 v7, 0x0

    #@8a3
    .restart local v7       #_arg3:Landroid/content/ComponentName;
    goto :goto_895

    #@8a4
    .line 851
    .end local v4           #_arg0:Landroid/content/IntentFilter;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:[Landroid/content/ComponentName;
    .end local v7           #_arg3:Landroid/content/ComponentName;
    :sswitch_8a4
    const-string v3, "android.content.pm.IPackageManager"

    #@8a6
    move-object/from16 v0, p2

    #@8a8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8ab
    .line 853
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8ae
    move-result-object v4

    #@8af
    .line 854
    .local v4, _arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@8b1
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->clearPackagePreferredActivities(Ljava/lang/String;)V

    #@8b4
    .line 855
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8b7
    .line 856
    const/4 v3, 0x1

    #@8b8
    goto/16 :goto_7

    #@8ba
    .line 860
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_8ba
    const-string v3, "android.content.pm.IPackageManager"

    #@8bc
    move-object/from16 v0, p2

    #@8be
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8c1
    .line 862
    new-instance v13, Ljava/util/ArrayList;

    #@8c3
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    #@8c6
    .line 864
    .local v13, _arg0:Ljava/util/List;,"Ljava/util/List<Landroid/content/IntentFilter;>;"
    new-instance v15, Ljava/util/ArrayList;

    #@8c8
    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    #@8cb
    .line 866
    .local v15, _arg1:Ljava/util/List;,"Ljava/util/List<Landroid/content/ComponentName;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8ce
    move-result-object v6

    #@8cf
    .line 867
    .local v6, _arg2:Ljava/lang/String;
    move-object/from16 v0, p0

    #@8d1
    invoke-virtual {v0, v13, v15, v6}, Landroid/content/pm/IPackageManager$Stub;->getPreferredActivities(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)I

    #@8d4
    move-result v19

    #@8d5
    .line 868
    .local v19, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8d8
    .line 869
    move-object/from16 v0, p3

    #@8da
    move/from16 v1, v19

    #@8dc
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@8df
    .line 870
    move-object/from16 v0, p3

    #@8e1
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@8e4
    .line 871
    move-object/from16 v0, p3

    #@8e6
    invoke-virtual {v0, v15}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@8e9
    .line 872
    const/4 v3, 0x1

    #@8ea
    goto/16 :goto_7

    #@8ec
    .line 876
    .end local v6           #_arg2:Ljava/lang/String;
    .end local v13           #_arg0:Ljava/util/List;,"Ljava/util/List<Landroid/content/IntentFilter;>;"
    .end local v15           #_arg1:Ljava/util/List;,"Ljava/util/List<Landroid/content/ComponentName;>;"
    .end local v19           #_result:I
    :sswitch_8ec
    const-string v3, "android.content.pm.IPackageManager"

    #@8ee
    move-object/from16 v0, p2

    #@8f0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8f3
    .line 878
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8f6
    move-result v3

    #@8f7
    if-eqz v3, :cond_91a

    #@8f9
    .line 879
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@8fb
    move-object/from16 v0, p2

    #@8fd
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@900
    move-result-object v4

    #@901
    check-cast v4, Landroid/content/ComponentName;

    #@903
    .line 885
    .local v4, _arg0:Landroid/content/ComponentName;
    :goto_903
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@906
    move-result v5

    #@907
    .line 887
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@90a
    move-result v6

    #@90b
    .line 889
    .local v6, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@90e
    move-result v7

    #@90f
    .line 890
    .local v7, _arg3:I
    move-object/from16 v0, p0

    #@911
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/content/pm/IPackageManager$Stub;->setComponentEnabledSetting(Landroid/content/ComponentName;III)V

    #@914
    .line 891
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@917
    .line 892
    const/4 v3, 0x1

    #@918
    goto/16 :goto_7

    #@91a
    .line 882
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    :cond_91a
    const/4 v4, 0x0

    #@91b
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    goto :goto_903

    #@91c
    .line 896
    .end local v4           #_arg0:Landroid/content/ComponentName;
    :sswitch_91c
    const-string v3, "android.content.pm.IPackageManager"

    #@91e
    move-object/from16 v0, p2

    #@920
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@923
    .line 898
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@926
    move-result v3

    #@927
    if-eqz v3, :cond_94a

    #@929
    .line 899
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@92b
    move-object/from16 v0, p2

    #@92d
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@930
    move-result-object v4

    #@931
    check-cast v4, Landroid/content/ComponentName;

    #@933
    .line 905
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    :goto_933
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@936
    move-result v5

    #@937
    .line 906
    .restart local v5       #_arg1:I
    move-object/from16 v0, p0

    #@939
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->getComponentEnabledSetting(Landroid/content/ComponentName;I)I

    #@93c
    move-result v19

    #@93d
    .line 907
    .restart local v19       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@940
    .line 908
    move-object/from16 v0, p3

    #@942
    move/from16 v1, v19

    #@944
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@947
    .line 909
    const/4 v3, 0x1

    #@948
    goto/16 :goto_7

    #@94a
    .line 902
    .end local v4           #_arg0:Landroid/content/ComponentName;
    .end local v5           #_arg1:I
    .end local v19           #_result:I
    :cond_94a
    const/4 v4, 0x0

    #@94b
    .restart local v4       #_arg0:Landroid/content/ComponentName;
    goto :goto_933

    #@94c
    .line 913
    .end local v4           #_arg0:Landroid/content/ComponentName;
    :sswitch_94c
    const-string v3, "android.content.pm.IPackageManager"

    #@94e
    move-object/from16 v0, p2

    #@950
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@953
    .line 915
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@956
    move-result-object v4

    #@957
    .line 917
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@95a
    move-result v5

    #@95b
    .line 919
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@95e
    move-result v6

    #@95f
    .line 921
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@962
    move-result v7

    #@963
    .line 922
    .restart local v7       #_arg3:I
    move-object/from16 v0, p0

    #@965
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/content/pm/IPackageManager$Stub;->setApplicationEnabledSetting(Ljava/lang/String;III)V

    #@968
    .line 923
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@96b
    .line 924
    const/4 v3, 0x1

    #@96c
    goto/16 :goto_7

    #@96e
    .line 928
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    :sswitch_96e
    const-string v3, "android.content.pm.IPackageManager"

    #@970
    move-object/from16 v0, p2

    #@972
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@975
    .line 930
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@978
    move-result-object v4

    #@979
    .line 932
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@97c
    move-result v5

    #@97d
    .line 933
    .restart local v5       #_arg1:I
    move-object/from16 v0, p0

    #@97f
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->getApplicationEnabledSetting(Ljava/lang/String;I)I

    #@982
    move-result v19

    #@983
    .line 934
    .restart local v19       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@986
    .line 935
    move-object/from16 v0, p3

    #@988
    move/from16 v1, v19

    #@98a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@98d
    .line 936
    const/4 v3, 0x1

    #@98e
    goto/16 :goto_7

    #@990
    .line 940
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v19           #_result:I
    :sswitch_990
    const-string v3, "android.content.pm.IPackageManager"

    #@992
    move-object/from16 v0, p2

    #@994
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@997
    .line 942
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@99a
    move-result-object v4

    #@99b
    .line 944
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@99e
    move-result v3

    #@99f
    if-eqz v3, :cond_9b1

    #@9a1
    const/4 v5, 0x1

    #@9a2
    .line 946
    .local v5, _arg1:Z
    :goto_9a2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@9a5
    move-result v6

    #@9a6
    .line 947
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@9a8
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->setPackageStoppedState(Ljava/lang/String;ZI)V

    #@9ab
    .line 948
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9ae
    .line 949
    const/4 v3, 0x1

    #@9af
    goto/16 :goto_7

    #@9b1
    .line 944
    .end local v5           #_arg1:Z
    .end local v6           #_arg2:I
    :cond_9b1
    const/4 v5, 0x0

    #@9b2
    goto :goto_9a2

    #@9b3
    .line 953
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_9b3
    const-string v3, "android.content.pm.IPackageManager"

    #@9b5
    move-object/from16 v0, p2

    #@9b7
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9ba
    .line 955
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@9bd
    move-result-wide v11

    #@9be
    .line 957
    .local v11, _arg0:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@9c1
    move-result-object v3

    #@9c2
    invoke-static {v3}, Landroid/content/pm/IPackageDataObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageDataObserver;

    #@9c5
    move-result-object v5

    #@9c6
    .line 958
    .local v5, _arg1:Landroid/content/pm/IPackageDataObserver;
    move-object/from16 v0, p0

    #@9c8
    invoke-virtual {v0, v11, v12, v5}, Landroid/content/pm/IPackageManager$Stub;->freeStorageAndNotify(JLandroid/content/pm/IPackageDataObserver;)V

    #@9cb
    .line 959
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9ce
    .line 960
    const/4 v3, 0x1

    #@9cf
    goto/16 :goto_7

    #@9d1
    .line 964
    .end local v5           #_arg1:Landroid/content/pm/IPackageDataObserver;
    .end local v11           #_arg0:J
    :sswitch_9d1
    const-string v3, "android.content.pm.IPackageManager"

    #@9d3
    move-object/from16 v0, p2

    #@9d5
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9d8
    .line 966
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@9db
    move-result-wide v11

    #@9dc
    .line 968
    .restart local v11       #_arg0:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@9df
    move-result v3

    #@9e0
    if-eqz v3, :cond_9f7

    #@9e2
    .line 969
    sget-object v3, Landroid/content/IntentSender;->CREATOR:Landroid/os/Parcelable$Creator;

    #@9e4
    move-object/from16 v0, p2

    #@9e6
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@9e9
    move-result-object v5

    #@9ea
    check-cast v5, Landroid/content/IntentSender;

    #@9ec
    .line 974
    .local v5, _arg1:Landroid/content/IntentSender;
    :goto_9ec
    move-object/from16 v0, p0

    #@9ee
    invoke-virtual {v0, v11, v12, v5}, Landroid/content/pm/IPackageManager$Stub;->freeStorage(JLandroid/content/IntentSender;)V

    #@9f1
    .line 975
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9f4
    .line 976
    const/4 v3, 0x1

    #@9f5
    goto/16 :goto_7

    #@9f7
    .line 972
    .end local v5           #_arg1:Landroid/content/IntentSender;
    :cond_9f7
    const/4 v5, 0x0

    #@9f8
    .restart local v5       #_arg1:Landroid/content/IntentSender;
    goto :goto_9ec

    #@9f9
    .line 980
    .end local v5           #_arg1:Landroid/content/IntentSender;
    .end local v11           #_arg0:J
    :sswitch_9f9
    const-string v3, "android.content.pm.IPackageManager"

    #@9fb
    move-object/from16 v0, p2

    #@9fd
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a00
    .line 982
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a03
    move-result-object v4

    #@a04
    .line 984
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a07
    move-result-object v3

    #@a08
    invoke-static {v3}, Landroid/content/pm/IPackageDataObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageDataObserver;

    #@a0b
    move-result-object v5

    #@a0c
    .line 985
    .local v5, _arg1:Landroid/content/pm/IPackageDataObserver;
    move-object/from16 v0, p0

    #@a0e
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->deleteApplicationCacheFiles(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;)V

    #@a11
    .line 986
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a14
    .line 987
    const/4 v3, 0x1

    #@a15
    goto/16 :goto_7

    #@a17
    .line 991
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/content/pm/IPackageDataObserver;
    :sswitch_a17
    const-string v3, "android.content.pm.IPackageManager"

    #@a19
    move-object/from16 v0, p2

    #@a1b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a1e
    .line 993
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a21
    move-result-object v4

    #@a22
    .line 995
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a25
    move-result-object v3

    #@a26
    invoke-static {v3}, Landroid/content/pm/IPackageDataObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageDataObserver;

    #@a29
    move-result-object v5

    #@a2a
    .line 997
    .restart local v5       #_arg1:Landroid/content/pm/IPackageDataObserver;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a2d
    move-result v6

    #@a2e
    .line 998
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@a30
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;I)V

    #@a33
    .line 999
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a36
    .line 1000
    const/4 v3, 0x1

    #@a37
    goto/16 :goto_7

    #@a39
    .line 1004
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/content/pm/IPackageDataObserver;
    .end local v6           #_arg2:I
    :sswitch_a39
    const-string v3, "android.content.pm.IPackageManager"

    #@a3b
    move-object/from16 v0, p2

    #@a3d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a40
    .line 1006
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a43
    move-result-object v4

    #@a44
    .line 1008
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a47
    move-result v5

    #@a48
    .line 1010
    .local v5, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a4b
    move-result-object v3

    #@a4c
    invoke-static {v3}, Landroid/content/pm/IPackageStatsObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageStatsObserver;

    #@a4f
    move-result-object v6

    #@a50
    .line 1011
    .local v6, _arg2:Landroid/content/pm/IPackageStatsObserver;
    move-object/from16 v0, p0

    #@a52
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->getPackageSizeInfo(Ljava/lang/String;ILandroid/content/pm/IPackageStatsObserver;)V

    #@a55
    .line 1012
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a58
    .line 1013
    const/4 v3, 0x1

    #@a59
    goto/16 :goto_7

    #@a5b
    .line 1017
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:Landroid/content/pm/IPackageStatsObserver;
    :sswitch_a5b
    const-string v3, "android.content.pm.IPackageManager"

    #@a5d
    move-object/from16 v0, p2

    #@a5f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a62
    .line 1018
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->getSystemSharedLibraryNames()[Ljava/lang/String;

    #@a65
    move-result-object v19

    #@a66
    .line 1019
    .local v19, _result:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a69
    .line 1020
    move-object/from16 v0, p3

    #@a6b
    move-object/from16 v1, v19

    #@a6d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@a70
    .line 1021
    const/4 v3, 0x1

    #@a71
    goto/16 :goto_7

    #@a73
    .line 1025
    .end local v19           #_result:[Ljava/lang/String;
    :sswitch_a73
    const-string v3, "android.content.pm.IPackageManager"

    #@a75
    move-object/from16 v0, p2

    #@a77
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a7a
    .line 1026
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    #@a7d
    move-result-object v19

    #@a7e
    .line 1027
    .local v19, _result:[Landroid/content/pm/FeatureInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a81
    .line 1028
    const/4 v3, 0x1

    #@a82
    move-object/from16 v0, p3

    #@a84
    move-object/from16 v1, v19

    #@a86
    invoke-virtual {v0, v1, v3}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@a89
    .line 1029
    const/4 v3, 0x1

    #@a8a
    goto/16 :goto_7

    #@a8c
    .line 1033
    .end local v19           #_result:[Landroid/content/pm/FeatureInfo;
    :sswitch_a8c
    const-string v3, "android.content.pm.IPackageManager"

    #@a8e
    move-object/from16 v0, p2

    #@a90
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a93
    .line 1035
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a96
    move-result-object v4

    #@a97
    .line 1036
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a99
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->hasSystemFeature(Ljava/lang/String;)Z

    #@a9c
    move-result v19

    #@a9d
    .line 1037
    .local v19, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@aa0
    .line 1038
    if-eqz v19, :cond_aab

    #@aa2
    const/4 v3, 0x1

    #@aa3
    :goto_aa3
    move-object/from16 v0, p3

    #@aa5
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@aa8
    .line 1039
    const/4 v3, 0x1

    #@aa9
    goto/16 :goto_7

    #@aab
    .line 1038
    :cond_aab
    const/4 v3, 0x0

    #@aac
    goto :goto_aa3

    #@aad
    .line 1043
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v19           #_result:Z
    :sswitch_aad
    const-string v3, "android.content.pm.IPackageManager"

    #@aaf
    move-object/from16 v0, p2

    #@ab1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ab4
    .line 1044
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->enterSafeMode()V

    #@ab7
    .line 1045
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@aba
    .line 1046
    const/4 v3, 0x1

    #@abb
    goto/16 :goto_7

    #@abd
    .line 1050
    :sswitch_abd
    const-string v3, "android.content.pm.IPackageManager"

    #@abf
    move-object/from16 v0, p2

    #@ac1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ac4
    .line 1051
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->isSafeMode()Z

    #@ac7
    move-result v19

    #@ac8
    .line 1052
    .restart local v19       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@acb
    .line 1053
    if-eqz v19, :cond_ad6

    #@acd
    const/4 v3, 0x1

    #@ace
    :goto_ace
    move-object/from16 v0, p3

    #@ad0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@ad3
    .line 1054
    const/4 v3, 0x1

    #@ad4
    goto/16 :goto_7

    #@ad6
    .line 1053
    :cond_ad6
    const/4 v3, 0x0

    #@ad7
    goto :goto_ace

    #@ad8
    .line 1058
    .end local v19           #_result:Z
    :sswitch_ad8
    const-string v3, "android.content.pm.IPackageManager"

    #@ada
    move-object/from16 v0, p2

    #@adc
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@adf
    .line 1059
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->systemReady()V

    #@ae2
    .line 1060
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ae5
    .line 1061
    const/4 v3, 0x1

    #@ae6
    goto/16 :goto_7

    #@ae8
    .line 1065
    :sswitch_ae8
    const-string v3, "android.content.pm.IPackageManager"

    #@aea
    move-object/from16 v0, p2

    #@aec
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@aef
    .line 1066
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->hasSystemUidErrors()Z

    #@af2
    move-result v19

    #@af3
    .line 1067
    .restart local v19       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@af6
    .line 1068
    if-eqz v19, :cond_b01

    #@af8
    const/4 v3, 0x1

    #@af9
    :goto_af9
    move-object/from16 v0, p3

    #@afb
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@afe
    .line 1069
    const/4 v3, 0x1

    #@aff
    goto/16 :goto_7

    #@b01
    .line 1068
    :cond_b01
    const/4 v3, 0x0

    #@b02
    goto :goto_af9

    #@b03
    .line 1073
    .end local v19           #_result:Z
    :sswitch_b03
    const-string v3, "android.content.pm.IPackageManager"

    #@b05
    move-object/from16 v0, p2

    #@b07
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b0a
    .line 1074
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->performBootDexOpt()V

    #@b0d
    .line 1075
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b10
    .line 1076
    const/4 v3, 0x1

    #@b11
    goto/16 :goto_7

    #@b13
    .line 1080
    :sswitch_b13
    const-string v3, "android.content.pm.IPackageManager"

    #@b15
    move-object/from16 v0, p2

    #@b17
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b1a
    .line 1082
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b1d
    move-result-object v4

    #@b1e
    .line 1083
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@b20
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->performDexOpt(Ljava/lang/String;)Z

    #@b23
    move-result v19

    #@b24
    .line 1084
    .restart local v19       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b27
    .line 1085
    if-eqz v19, :cond_b32

    #@b29
    const/4 v3, 0x1

    #@b2a
    :goto_b2a
    move-object/from16 v0, p3

    #@b2c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@b2f
    .line 1086
    const/4 v3, 0x1

    #@b30
    goto/16 :goto_7

    #@b32
    .line 1085
    :cond_b32
    const/4 v3, 0x0

    #@b33
    goto :goto_b2a

    #@b34
    .line 1090
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v19           #_result:Z
    :sswitch_b34
    const-string v3, "android.content.pm.IPackageManager"

    #@b36
    move-object/from16 v0, p2

    #@b38
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b3b
    .line 1092
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b3e
    move-result v3

    #@b3f
    if-eqz v3, :cond_b54

    #@b41
    const/4 v4, 0x1

    #@b42
    .line 1094
    .local v4, _arg0:Z
    :goto_b42
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b45
    move-result v3

    #@b46
    if-eqz v3, :cond_b56

    #@b48
    const/4 v5, 0x1

    #@b49
    .line 1095
    .local v5, _arg1:Z
    :goto_b49
    move-object/from16 v0, p0

    #@b4b
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->updateExternalMediaStatus(ZZ)V

    #@b4e
    .line 1096
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b51
    .line 1097
    const/4 v3, 0x1

    #@b52
    goto/16 :goto_7

    #@b54
    .line 1092
    .end local v4           #_arg0:Z
    .end local v5           #_arg1:Z
    :cond_b54
    const/4 v4, 0x0

    #@b55
    goto :goto_b42

    #@b56
    .line 1094
    .restart local v4       #_arg0:Z
    :cond_b56
    const/4 v5, 0x0

    #@b57
    goto :goto_b49

    #@b58
    .line 1101
    .end local v4           #_arg0:Z
    :sswitch_b58
    const-string v3, "android.content.pm.IPackageManager"

    #@b5a
    move-object/from16 v0, p2

    #@b5c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b5f
    .line 1103
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b62
    move-result v3

    #@b63
    if-eqz v3, :cond_b8b

    #@b65
    .line 1104
    sget-object v3, Landroid/content/pm/PackageCleanItem;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b67
    move-object/from16 v0, p2

    #@b69
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b6c
    move-result-object v4

    #@b6d
    check-cast v4, Landroid/content/pm/PackageCleanItem;

    #@b6f
    .line 1109
    .local v4, _arg0:Landroid/content/pm/PackageCleanItem;
    :goto_b6f
    move-object/from16 v0, p0

    #@b71
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->nextPackageToClean(Landroid/content/pm/PackageCleanItem;)Landroid/content/pm/PackageCleanItem;

    #@b74
    move-result-object v19

    #@b75
    .line 1110
    .local v19, _result:Landroid/content/pm/PackageCleanItem;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b78
    .line 1111
    if-eqz v19, :cond_b8d

    #@b7a
    .line 1112
    const/4 v3, 0x1

    #@b7b
    move-object/from16 v0, p3

    #@b7d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@b80
    .line 1113
    const/4 v3, 0x1

    #@b81
    move-object/from16 v0, v19

    #@b83
    move-object/from16 v1, p3

    #@b85
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageCleanItem;->writeToParcel(Landroid/os/Parcel;I)V

    #@b88
    .line 1118
    :goto_b88
    const/4 v3, 0x1

    #@b89
    goto/16 :goto_7

    #@b8b
    .line 1107
    .end local v4           #_arg0:Landroid/content/pm/PackageCleanItem;
    .end local v19           #_result:Landroid/content/pm/PackageCleanItem;
    :cond_b8b
    const/4 v4, 0x0

    #@b8c
    .restart local v4       #_arg0:Landroid/content/pm/PackageCleanItem;
    goto :goto_b6f

    #@b8d
    .line 1116
    .restart local v19       #_result:Landroid/content/pm/PackageCleanItem;
    :cond_b8d
    const/4 v3, 0x0

    #@b8e
    move-object/from16 v0, p3

    #@b90
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@b93
    goto :goto_b88

    #@b94
    .line 1122
    .end local v4           #_arg0:Landroid/content/pm/PackageCleanItem;
    .end local v19           #_result:Landroid/content/pm/PackageCleanItem;
    :sswitch_b94
    const-string v3, "android.content.pm.IPackageManager"

    #@b96
    move-object/from16 v0, p2

    #@b98
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b9b
    .line 1124
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b9e
    move-result-object v4

    #@b9f
    .line 1126
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ba2
    move-result-object v3

    #@ba3
    invoke-static {v3}, Landroid/content/pm/IPackageMoveObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageMoveObserver;

    #@ba6
    move-result-object v5

    #@ba7
    .line 1128
    .local v5, _arg1:Landroid/content/pm/IPackageMoveObserver;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@baa
    move-result v6

    #@bab
    .line 1129
    .local v6, _arg2:I
    move-object/from16 v0, p0

    #@bad
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/pm/IPackageManager$Stub;->movePackage(Ljava/lang/String;Landroid/content/pm/IPackageMoveObserver;I)V

    #@bb0
    .line 1130
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@bb3
    .line 1131
    const/4 v3, 0x1

    #@bb4
    goto/16 :goto_7

    #@bb6
    .line 1135
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/content/pm/IPackageMoveObserver;
    .end local v6           #_arg2:I
    :sswitch_bb6
    const-string v3, "android.content.pm.IPackageManager"

    #@bb8
    move-object/from16 v0, p2

    #@bba
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bbd
    .line 1137
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@bc0
    move-result v3

    #@bc1
    if-eqz v3, :cond_be1

    #@bc3
    .line 1138
    sget-object v3, Landroid/content/pm/PermissionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@bc5
    move-object/from16 v0, p2

    #@bc7
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@bca
    move-result-object v4

    #@bcb
    check-cast v4, Landroid/content/pm/PermissionInfo;

    #@bcd
    .line 1143
    .local v4, _arg0:Landroid/content/pm/PermissionInfo;
    :goto_bcd
    move-object/from16 v0, p0

    #@bcf
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->addPermissionAsync(Landroid/content/pm/PermissionInfo;)Z

    #@bd2
    move-result v19

    #@bd3
    .line 1144
    .local v19, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@bd6
    .line 1145
    if-eqz v19, :cond_be3

    #@bd8
    const/4 v3, 0x1

    #@bd9
    :goto_bd9
    move-object/from16 v0, p3

    #@bdb
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@bde
    .line 1146
    const/4 v3, 0x1

    #@bdf
    goto/16 :goto_7

    #@be1
    .line 1141
    .end local v4           #_arg0:Landroid/content/pm/PermissionInfo;
    .end local v19           #_result:Z
    :cond_be1
    const/4 v4, 0x0

    #@be2
    .restart local v4       #_arg0:Landroid/content/pm/PermissionInfo;
    goto :goto_bcd

    #@be3
    .line 1145
    .restart local v19       #_result:Z
    :cond_be3
    const/4 v3, 0x0

    #@be4
    goto :goto_bd9

    #@be5
    .line 1150
    .end local v4           #_arg0:Landroid/content/pm/PermissionInfo;
    .end local v19           #_result:Z
    :sswitch_be5
    const-string v3, "android.content.pm.IPackageManager"

    #@be7
    move-object/from16 v0, p2

    #@be9
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bec
    .line 1152
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@bef
    move-result v4

    #@bf0
    .line 1153
    .local v4, _arg0:I
    move-object/from16 v0, p0

    #@bf2
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->setInstallLocation(I)Z

    #@bf5
    move-result v19

    #@bf6
    .line 1154
    .restart local v19       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@bf9
    .line 1155
    if-eqz v19, :cond_c04

    #@bfb
    const/4 v3, 0x1

    #@bfc
    :goto_bfc
    move-object/from16 v0, p3

    #@bfe
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@c01
    .line 1156
    const/4 v3, 0x1

    #@c02
    goto/16 :goto_7

    #@c04
    .line 1155
    :cond_c04
    const/4 v3, 0x0

    #@c05
    goto :goto_bfc

    #@c06
    .line 1160
    .end local v4           #_arg0:I
    .end local v19           #_result:Z
    :sswitch_c06
    const-string v3, "android.content.pm.IPackageManager"

    #@c08
    move-object/from16 v0, p2

    #@c0a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c0d
    .line 1161
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->getInstallLocation()I

    #@c10
    move-result v19

    #@c11
    .line 1162
    .local v19, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@c14
    .line 1163
    move-object/from16 v0, p3

    #@c16
    move/from16 v1, v19

    #@c18
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@c1b
    .line 1164
    const/4 v3, 0x1

    #@c1c
    goto/16 :goto_7

    #@c1e
    .line 1168
    .end local v19           #_result:I
    :sswitch_c1e
    const-string v3, "android.content.pm.IPackageManager"

    #@c20
    move-object/from16 v0, p2

    #@c22
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c25
    .line 1170
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c28
    move-result v3

    #@c29
    if-eqz v3, :cond_c80

    #@c2b
    .line 1171
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c2d
    move-object/from16 v0, p2

    #@c2f
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c32
    move-result-object v4

    #@c33
    check-cast v4, Landroid/net/Uri;

    #@c35
    .line 1177
    .local v4, _arg0:Landroid/net/Uri;
    :goto_c35
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c38
    move-result-object v3

    #@c39
    invoke-static {v3}, Landroid/content/pm/IPackageInstallObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageInstallObserver;

    #@c3c
    move-result-object v5

    #@c3d
    .line 1179
    .local v5, _arg1:Landroid/content/pm/IPackageInstallObserver;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c40
    move-result v6

    #@c41
    .line 1181
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c44
    move-result-object v7

    #@c45
    .line 1183
    .local v7, _arg3:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c48
    move-result v3

    #@c49
    if-eqz v3, :cond_c82

    #@c4b
    .line 1184
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c4d
    move-object/from16 v0, p2

    #@c4f
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c52
    move-result-object v8

    #@c53
    check-cast v8, Landroid/net/Uri;

    #@c55
    .line 1190
    .local v8, _arg4:Landroid/net/Uri;
    :goto_c55
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c58
    move-result v3

    #@c59
    if-eqz v3, :cond_c84

    #@c5b
    .line 1191
    sget-object v3, Landroid/content/pm/ManifestDigest;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c5d
    move-object/from16 v0, p2

    #@c5f
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c62
    move-result-object v9

    #@c63
    check-cast v9, Landroid/content/pm/ManifestDigest;

    #@c65
    .line 1197
    .local v9, _arg5:Landroid/content/pm/ManifestDigest;
    :goto_c65
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c68
    move-result v3

    #@c69
    if-eqz v3, :cond_c86

    #@c6b
    .line 1198
    sget-object v3, Landroid/content/pm/ContainerEncryptionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c6d
    move-object/from16 v0, p2

    #@c6f
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c72
    move-result-object v10

    #@c73
    check-cast v10, Landroid/content/pm/ContainerEncryptionParams;

    #@c75
    .local v10, _arg6:Landroid/content/pm/ContainerEncryptionParams;
    :goto_c75
    move-object/from16 v3, p0

    #@c77
    .line 1203
    invoke-virtual/range {v3 .. v10}, Landroid/content/pm/IPackageManager$Stub;->installPackageWithVerification(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Landroid/net/Uri;Landroid/content/pm/ManifestDigest;Landroid/content/pm/ContainerEncryptionParams;)V

    #@c7a
    .line 1204
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@c7d
    .line 1205
    const/4 v3, 0x1

    #@c7e
    goto/16 :goto_7

    #@c80
    .line 1174
    .end local v4           #_arg0:Landroid/net/Uri;
    .end local v5           #_arg1:Landroid/content/pm/IPackageInstallObserver;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:Ljava/lang/String;
    .end local v8           #_arg4:Landroid/net/Uri;
    .end local v9           #_arg5:Landroid/content/pm/ManifestDigest;
    .end local v10           #_arg6:Landroid/content/pm/ContainerEncryptionParams;
    :cond_c80
    const/4 v4, 0x0

    #@c81
    .restart local v4       #_arg0:Landroid/net/Uri;
    goto :goto_c35

    #@c82
    .line 1187
    .restart local v5       #_arg1:Landroid/content/pm/IPackageInstallObserver;
    .restart local v6       #_arg2:I
    .restart local v7       #_arg3:Ljava/lang/String;
    :cond_c82
    const/4 v8, 0x0

    #@c83
    .restart local v8       #_arg4:Landroid/net/Uri;
    goto :goto_c55

    #@c84
    .line 1194
    :cond_c84
    const/4 v9, 0x0

    #@c85
    .restart local v9       #_arg5:Landroid/content/pm/ManifestDigest;
    goto :goto_c65

    #@c86
    .line 1201
    :cond_c86
    const/4 v10, 0x0

    #@c87
    .restart local v10       #_arg6:Landroid/content/pm/ContainerEncryptionParams;
    goto :goto_c75

    #@c88
    .line 1209
    .end local v4           #_arg0:Landroid/net/Uri;
    .end local v5           #_arg1:Landroid/content/pm/IPackageInstallObserver;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:Ljava/lang/String;
    .end local v8           #_arg4:Landroid/net/Uri;
    .end local v9           #_arg5:Landroid/content/pm/ManifestDigest;
    .end local v10           #_arg6:Landroid/content/pm/ContainerEncryptionParams;
    :sswitch_c88
    const-string v3, "android.content.pm.IPackageManager"

    #@c8a
    move-object/from16 v0, p2

    #@c8c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c8f
    .line 1211
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c92
    move-result v3

    #@c93
    if-eqz v3, :cond_cda

    #@c95
    .line 1212
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c97
    move-object/from16 v0, p2

    #@c99
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c9c
    move-result-object v4

    #@c9d
    check-cast v4, Landroid/net/Uri;

    #@c9f
    .line 1218
    .restart local v4       #_arg0:Landroid/net/Uri;
    :goto_c9f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ca2
    move-result-object v3

    #@ca3
    invoke-static {v3}, Landroid/content/pm/IPackageInstallObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageInstallObserver;

    #@ca6
    move-result-object v5

    #@ca7
    .line 1220
    .restart local v5       #_arg1:Landroid/content/pm/IPackageInstallObserver;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@caa
    move-result v6

    #@cab
    .line 1222
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@cae
    move-result-object v7

    #@caf
    .line 1224
    .restart local v7       #_arg3:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@cb2
    move-result v3

    #@cb3
    if-eqz v3, :cond_cdc

    #@cb5
    .line 1225
    sget-object v3, Landroid/content/pm/VerificationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@cb7
    move-object/from16 v0, p2

    #@cb9
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@cbc
    move-result-object v8

    #@cbd
    check-cast v8, Landroid/content/pm/VerificationParams;

    #@cbf
    .line 1231
    .local v8, _arg4:Landroid/content/pm/VerificationParams;
    :goto_cbf
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@cc2
    move-result v3

    #@cc3
    if-eqz v3, :cond_cde

    #@cc5
    .line 1232
    sget-object v3, Landroid/content/pm/ContainerEncryptionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@cc7
    move-object/from16 v0, p2

    #@cc9
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@ccc
    move-result-object v9

    #@ccd
    check-cast v9, Landroid/content/pm/ContainerEncryptionParams;

    #@ccf
    .local v9, _arg5:Landroid/content/pm/ContainerEncryptionParams;
    :goto_ccf
    move-object/from16 v3, p0

    #@cd1
    .line 1237
    invoke-virtual/range {v3 .. v9}, Landroid/content/pm/IPackageManager$Stub;->installPackageWithVerificationAndEncryption(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Landroid/content/pm/VerificationParams;Landroid/content/pm/ContainerEncryptionParams;)V

    #@cd4
    .line 1238
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@cd7
    .line 1239
    const/4 v3, 0x1

    #@cd8
    goto/16 :goto_7

    #@cda
    .line 1215
    .end local v4           #_arg0:Landroid/net/Uri;
    .end local v5           #_arg1:Landroid/content/pm/IPackageInstallObserver;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:Ljava/lang/String;
    .end local v8           #_arg4:Landroid/content/pm/VerificationParams;
    .end local v9           #_arg5:Landroid/content/pm/ContainerEncryptionParams;
    :cond_cda
    const/4 v4, 0x0

    #@cdb
    .restart local v4       #_arg0:Landroid/net/Uri;
    goto :goto_c9f

    #@cdc
    .line 1228
    .restart local v5       #_arg1:Landroid/content/pm/IPackageInstallObserver;
    .restart local v6       #_arg2:I
    .restart local v7       #_arg3:Ljava/lang/String;
    :cond_cdc
    const/4 v8, 0x0

    #@cdd
    .restart local v8       #_arg4:Landroid/content/pm/VerificationParams;
    goto :goto_cbf

    #@cde
    .line 1235
    :cond_cde
    const/4 v9, 0x0

    #@cdf
    .restart local v9       #_arg5:Landroid/content/pm/ContainerEncryptionParams;
    goto :goto_ccf

    #@ce0
    .line 1243
    .end local v4           #_arg0:Landroid/net/Uri;
    .end local v5           #_arg1:Landroid/content/pm/IPackageInstallObserver;
    .end local v6           #_arg2:I
    .end local v7           #_arg3:Ljava/lang/String;
    .end local v8           #_arg4:Landroid/content/pm/VerificationParams;
    .end local v9           #_arg5:Landroid/content/pm/ContainerEncryptionParams;
    :sswitch_ce0
    const-string v3, "android.content.pm.IPackageManager"

    #@ce2
    move-object/from16 v0, p2

    #@ce4
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ce7
    .line 1245
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@cea
    move-result-object v4

    #@ceb
    .line 1246
    .local v4, _arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@ced
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->installExistingPackage(Ljava/lang/String;)I

    #@cf0
    move-result v19

    #@cf1
    .line 1247
    .restart local v19       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@cf4
    .line 1248
    move-object/from16 v0, p3

    #@cf6
    move/from16 v1, v19

    #@cf8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@cfb
    .line 1249
    const/4 v3, 0x1

    #@cfc
    goto/16 :goto_7

    #@cfe
    .line 1253
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v19           #_result:I
    :sswitch_cfe
    const-string v3, "android.content.pm.IPackageManager"

    #@d00
    move-object/from16 v0, p2

    #@d02
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d05
    .line 1255
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d08
    move-result v4

    #@d09
    .line 1257
    .local v4, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d0c
    move-result v5

    #@d0d
    .line 1258
    .local v5, _arg1:I
    move-object/from16 v0, p0

    #@d0f
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->verifyPendingInstall(II)V

    #@d12
    .line 1259
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d15
    .line 1260
    const/4 v3, 0x1

    #@d16
    goto/16 :goto_7

    #@d18
    .line 1264
    .end local v4           #_arg0:I
    .end local v5           #_arg1:I
    :sswitch_d18
    const-string v3, "android.content.pm.IPackageManager"

    #@d1a
    move-object/from16 v0, p2

    #@d1c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d1f
    .line 1266
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d22
    move-result v4

    #@d23
    .line 1268
    .restart local v4       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d26
    move-result v5

    #@d27
    .line 1270
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@d2a
    move-result-wide v17

    #@d2b
    .line 1271
    .local v17, _arg2:J
    move-object/from16 v0, p0

    #@d2d
    move-wide/from16 v1, v17

    #@d2f
    invoke-virtual {v0, v4, v5, v1, v2}, Landroid/content/pm/IPackageManager$Stub;->extendVerificationTimeout(IIJ)V

    #@d32
    .line 1272
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d35
    .line 1273
    const/4 v3, 0x1

    #@d36
    goto/16 :goto_7

    #@d38
    .line 1277
    .end local v4           #_arg0:I
    .end local v5           #_arg1:I
    .end local v17           #_arg2:J
    :sswitch_d38
    const-string v3, "android.content.pm.IPackageManager"

    #@d3a
    move-object/from16 v0, p2

    #@d3c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d3f
    .line 1278
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->getVerifierDeviceIdentity()Landroid/content/pm/VerifierDeviceIdentity;

    #@d42
    move-result-object v19

    #@d43
    .line 1279
    .local v19, _result:Landroid/content/pm/VerifierDeviceIdentity;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d46
    .line 1280
    if-eqz v19, :cond_d59

    #@d48
    .line 1281
    const/4 v3, 0x1

    #@d49
    move-object/from16 v0, p3

    #@d4b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@d4e
    .line 1282
    const/4 v3, 0x1

    #@d4f
    move-object/from16 v0, v19

    #@d51
    move-object/from16 v1, p3

    #@d53
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/VerifierDeviceIdentity;->writeToParcel(Landroid/os/Parcel;I)V

    #@d56
    .line 1287
    :goto_d56
    const/4 v3, 0x1

    #@d57
    goto/16 :goto_7

    #@d59
    .line 1285
    :cond_d59
    const/4 v3, 0x0

    #@d5a
    move-object/from16 v0, p3

    #@d5c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@d5f
    goto :goto_d56

    #@d60
    .line 1291
    .end local v19           #_result:Landroid/content/pm/VerifierDeviceIdentity;
    :sswitch_d60
    const-string v3, "android.content.pm.IPackageManager"

    #@d62
    move-object/from16 v0, p2

    #@d64
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d67
    .line 1292
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->isFirstBoot()Z

    #@d6a
    move-result v19

    #@d6b
    .line 1293
    .local v19, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d6e
    .line 1294
    if-eqz v19, :cond_d79

    #@d70
    const/4 v3, 0x1

    #@d71
    :goto_d71
    move-object/from16 v0, p3

    #@d73
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@d76
    .line 1295
    const/4 v3, 0x1

    #@d77
    goto/16 :goto_7

    #@d79
    .line 1294
    :cond_d79
    const/4 v3, 0x0

    #@d7a
    goto :goto_d71

    #@d7b
    .line 1299
    .end local v19           #_result:Z
    :sswitch_d7b
    const-string v3, "android.content.pm.IPackageManager"

    #@d7d
    move-object/from16 v0, p2

    #@d7f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d82
    .line 1300
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->isOnlyCoreApps()Z

    #@d85
    move-result v19

    #@d86
    .line 1301
    .restart local v19       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d89
    .line 1302
    if-eqz v19, :cond_d94

    #@d8b
    const/4 v3, 0x1

    #@d8c
    :goto_d8c
    move-object/from16 v0, p3

    #@d8e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@d91
    .line 1303
    const/4 v3, 0x1

    #@d92
    goto/16 :goto_7

    #@d94
    .line 1302
    :cond_d94
    const/4 v3, 0x0

    #@d95
    goto :goto_d8c

    #@d96
    .line 1307
    .end local v19           #_result:Z
    :sswitch_d96
    const-string v3, "android.content.pm.IPackageManager"

    #@d98
    move-object/from16 v0, p2

    #@d9a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d9d
    .line 1309
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@da0
    move-result-object v4

    #@da1
    .line 1311
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@da4
    move-result v3

    #@da5
    if-eqz v3, :cond_db3

    #@da7
    const/4 v5, 0x1

    #@da8
    .line 1312
    .local v5, _arg1:Z
    :goto_da8
    move-object/from16 v0, p0

    #@daa
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/IPackageManager$Stub;->setPermissionEnforced(Ljava/lang/String;Z)V

    #@dad
    .line 1313
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@db0
    .line 1314
    const/4 v3, 0x1

    #@db1
    goto/16 :goto_7

    #@db3
    .line 1311
    .end local v5           #_arg1:Z
    :cond_db3
    const/4 v5, 0x0

    #@db4
    goto :goto_da8

    #@db5
    .line 1318
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_db5
    const-string v3, "android.content.pm.IPackageManager"

    #@db7
    move-object/from16 v0, p2

    #@db9
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dbc
    .line 1320
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@dbf
    move-result-object v4

    #@dc0
    .line 1321
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@dc2
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->isPermissionEnforced(Ljava/lang/String;)Z

    #@dc5
    move-result v19

    #@dc6
    .line 1322
    .restart local v19       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@dc9
    .line 1323
    if-eqz v19, :cond_dd4

    #@dcb
    const/4 v3, 0x1

    #@dcc
    :goto_dcc
    move-object/from16 v0, p3

    #@dce
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@dd1
    .line 1324
    const/4 v3, 0x1

    #@dd2
    goto/16 :goto_7

    #@dd4
    .line 1323
    :cond_dd4
    const/4 v3, 0x0

    #@dd5
    goto :goto_dcc

    #@dd6
    .line 1328
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v19           #_result:Z
    :sswitch_dd6
    const-string v3, "android.content.pm.IPackageManager"

    #@dd8
    move-object/from16 v0, p2

    #@dda
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ddd
    .line 1330
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@de0
    move-result-object v4

    #@de1
    .line 1331
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@de3
    invoke-virtual {v0, v4}, Landroid/content/pm/IPackageManager$Stub;->getOverlayPath(Ljava/lang/String;)Ljava/lang/String;

    #@de6
    move-result-object v19

    #@de7
    .line 1332
    .local v19, _result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@dea
    .line 1333
    move-object/from16 v0, p3

    #@dec
    move-object/from16 v1, v19

    #@dee
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@df1
    .line 1334
    const/4 v3, 0x1

    #@df2
    goto/16 :goto_7

    #@df4
    .line 1338
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v19           #_result:Ljava/lang/String;
    :sswitch_df4
    const-string v3, "android.content.pm.IPackageManager"

    #@df6
    move-object/from16 v0, p2

    #@df8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dfb
    .line 1339
    invoke-virtual/range {p0 .. p0}, Landroid/content/pm/IPackageManager$Stub;->isStorageLow()Z

    #@dfe
    move-result v19

    #@dff
    .line 1340
    .local v19, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@e02
    .line 1341
    if-eqz v19, :cond_e0d

    #@e04
    const/4 v3, 0x1

    #@e05
    :goto_e05
    move-object/from16 v0, p3

    #@e07
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@e0a
    .line 1342
    const/4 v3, 0x1

    #@e0b
    goto/16 :goto_7

    #@e0d
    .line 1341
    :cond_e0d
    const/4 v3, 0x0

    #@e0e
    goto :goto_e05

    #@e0f
    .line 44
    nop

    #@e10
    :sswitch_data_e10
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_46
        0x3 -> :sswitch_67
        0x4 -> :sswitch_84
        0x5 -> :sswitch_a2
        0x6 -> :sswitch_c0
        0x7 -> :sswitch_f2
        0x8 -> :sswitch_114
        0x9 -> :sswitch_146
        0xa -> :sswitch_164
        0xb -> :sswitch_19a
        0xc -> :sswitch_1de
        0xd -> :sswitch_222
        0xe -> :sswitch_266
        0xf -> :sswitch_2aa
        0x10 -> :sswitch_2cc
        0x11 -> :sswitch_2ee
        0x12 -> :sswitch_31d
        0x13 -> :sswitch_333
        0x14 -> :sswitch_34d
        0x15 -> :sswitch_367
        0x16 -> :sswitch_388
        0x17 -> :sswitch_3aa
        0x18 -> :sswitch_3cc
        0x19 -> :sswitch_3ea
        0x1a -> :sswitch_408
        0x1b -> :sswitch_426
        0x1c -> :sswitch_46e
        0x1d -> :sswitch_4a6
        0x1e -> :sswitch_4fe
        0x1f -> :sswitch_536
        0x20 -> :sswitch_57e
        0x21 -> :sswitch_5b6
        0x22 -> :sswitch_5ec
        0x23 -> :sswitch_622
        0x24 -> :sswitch_640
        0x25 -> :sswitch_676
        0x26 -> :sswitch_6a2
        0x27 -> :sswitch_6c8
        0x28 -> :sswitch_708
        0x29 -> :sswitch_72a
        0x2a -> :sswitch_75e
        0x2b -> :sswitch_774
        0x2c -> :sswitch_78e
        0x2d -> :sswitch_7b0
        0x2e -> :sswitch_7ce
        0x2f -> :sswitch_7e4
        0x30 -> :sswitch_7fa
        0x31 -> :sswitch_818
        0x32 -> :sswitch_860
        0x33 -> :sswitch_8a4
        0x34 -> :sswitch_8ba
        0x35 -> :sswitch_8ec
        0x36 -> :sswitch_91c
        0x37 -> :sswitch_94c
        0x38 -> :sswitch_96e
        0x39 -> :sswitch_990
        0x3a -> :sswitch_9b3
        0x3b -> :sswitch_9d1
        0x3c -> :sswitch_9f9
        0x3d -> :sswitch_a17
        0x3e -> :sswitch_a39
        0x3f -> :sswitch_a5b
        0x40 -> :sswitch_a73
        0x41 -> :sswitch_a8c
        0x42 -> :sswitch_aad
        0x43 -> :sswitch_abd
        0x44 -> :sswitch_ad8
        0x45 -> :sswitch_ae8
        0x46 -> :sswitch_b03
        0x47 -> :sswitch_b13
        0x48 -> :sswitch_b34
        0x49 -> :sswitch_b58
        0x4a -> :sswitch_b94
        0x4b -> :sswitch_bb6
        0x4c -> :sswitch_be5
        0x4d -> :sswitch_c06
        0x4e -> :sswitch_c1e
        0x4f -> :sswitch_c88
        0x50 -> :sswitch_ce0
        0x51 -> :sswitch_cfe
        0x52 -> :sswitch_d18
        0x53 -> :sswitch_d38
        0x54 -> :sswitch_d60
        0x55 -> :sswitch_d7b
        0x56 -> :sswitch_d96
        0x57 -> :sswitch_db5
        0x58 -> :sswitch_dd6
        0x59 -> :sswitch_df4
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
