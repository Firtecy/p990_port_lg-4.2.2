.class public Landroid/content/pm/Signature;
.super Ljava/lang/Object;
.source "Signature.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/Signature;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mHashCode:I

.field private mHaveHashCode:Z

.field private final mSignature:[B

.field private mStringRef:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 187
    new-instance v0, Landroid/content/pm/Signature$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/Signature$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/Signature;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 198
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 199
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@9
    .line 200
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/Signature$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/content/pm/Signature;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 14
    .parameter "text"

    #@0
    .prologue
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 68
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@6
    move-result-object v4

    #@7
    .line 69
    .local v4, input:[B
    array-length v0, v4

    #@8
    .line 71
    .local v0, N:I
    rem-int/lit8 v9, v0, 0x2

    #@a
    if-eqz v9, :cond_2c

    #@c
    .line 72
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@e
    new-instance v10, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string/jumbo v11, "text size "

    #@16
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v10

    #@1a
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v10

    #@1e
    const-string v11, " is not even"

    #@20
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v10

    #@24
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v10

    #@28
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v9

    #@2c
    .line 75
    :cond_2c
    div-int/lit8 v9, v0, 0x2

    #@2e
    new-array v6, v9, [B

    #@30
    .line 76
    .local v6, sig:[B
    const/4 v7, 0x0

    #@31
    .line 78
    .local v7, sigIndex:I
    const/4 v2, 0x0

    #@32
    .local v2, i:I
    move v3, v2

    #@33
    .end local v2           #i:I
    .local v3, i:I
    move v8, v7

    #@34
    .end local v7           #sigIndex:I
    .local v8, sigIndex:I
    :goto_34
    if-ge v3, v0, :cond_50

    #@36
    .line 79
    add-int/lit8 v2, v3, 0x1

    #@38
    .end local v3           #i:I
    .restart local v2       #i:I
    aget-byte v9, v4, v3

    #@3a
    invoke-static {v9}, Landroid/content/pm/Signature;->parseHexDigit(I)I

    #@3d
    move-result v1

    #@3e
    .line 80
    .local v1, hi:I
    add-int/lit8 v3, v2, 0x1

    #@40
    .end local v2           #i:I
    .restart local v3       #i:I
    aget-byte v9, v4, v2

    #@42
    invoke-static {v9}, Landroid/content/pm/Signature;->parseHexDigit(I)I

    #@45
    move-result v5

    #@46
    .line 81
    .local v5, lo:I
    add-int/lit8 v7, v8, 0x1

    #@48
    .end local v8           #sigIndex:I
    .restart local v7       #sigIndex:I
    shl-int/lit8 v9, v1, 0x4

    #@4a
    or-int/2addr v9, v5

    #@4b
    int-to-byte v9, v9

    #@4c
    aput-byte v9, v6, v8

    #@4e
    move v8, v7

    #@4f
    .line 82
    .end local v7           #sigIndex:I
    .restart local v8       #sigIndex:I
    goto :goto_34

    #@50
    .line 84
    .end local v1           #hi:I
    .end local v5           #lo:I
    :cond_50
    iput-object v6, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@52
    .line 85
    return-void
.end method

.method public constructor <init>([B)V
    .registers 3
    .parameter "signature"

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 44
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, [B

    #@9
    iput-object v0, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@b
    .line 45
    return-void
.end method

.method private static final parseHexDigit(I)I
    .registers 4
    .parameter "nibble"

    #@0
    .prologue
    .line 48
    const/16 v0, 0x30

    #@2
    if-gt v0, p0, :cond_b

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_b

    #@8
    .line 49
    add-int/lit8 v0, p0, -0x30

    #@a
    .line 53
    :goto_a
    return v0

    #@b
    .line 50
    :cond_b
    const/16 v0, 0x61

    #@d
    if-gt v0, p0, :cond_18

    #@f
    const/16 v0, 0x66

    #@11
    if-gt p0, v0, :cond_18

    #@13
    .line 51
    add-int/lit8 v0, p0, -0x61

    #@15
    add-int/lit8 v0, v0, 0xa

    #@17
    goto :goto_a

    #@18
    .line 52
    :cond_18
    const/16 v0, 0x41

    #@1a
    if-gt v0, p0, :cond_25

    #@1c
    const/16 v0, 0x46

    #@1e
    if-gt p0, v0, :cond_25

    #@20
    .line 53
    add-int/lit8 v0, p0, -0x41

    #@22
    add-int/lit8 v0, v0, 0xa

    #@24
    goto :goto_a

    #@25
    .line 55
    :cond_25
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@27
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "Invalid character "

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    const-string v2, " in hex string"

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@43
    throw v0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 180
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "obj"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 160
    if-eqz p1, :cond_14

    #@3
    .line 161
    :try_start_3
    move-object v0, p1

    #@4
    check-cast v0, Landroid/content/pm/Signature;

    #@6
    move-object v1, v0

    #@7
    .line 162
    .local v1, other:Landroid/content/pm/Signature;
    if-eq p0, v1, :cond_13

    #@9
    iget-object v3, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@b
    iget-object v4, v1, Landroid/content/pm/Signature;->mSignature:[B

    #@d
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_10} :catch_15

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_14

    #@13
    :cond_13
    const/4 v2, 0x1

    #@14
    .line 166
    .end local v1           #other:Landroid/content/pm/Signature;
    :cond_14
    :goto_14
    return v2

    #@15
    .line 164
    :catch_15
    move-exception v3

    #@16
    goto :goto_14
.end method

.method public getPublicKey()Ljava/security/PublicKey;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    #@0
    .prologue
    .line 151
    const-string v3, "X.509"

    #@2
    invoke-static {v3}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@5
    move-result-object v2

    #@6
    .line 152
    .local v2, certFactory:Ljava/security/cert/CertificateFactory;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    #@8
    iget-object v3, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@a
    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@d
    .line 153
    .local v0, bais:Ljava/io/ByteArrayInputStream;
    invoke-virtual {v2, v0}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@10
    move-result-object v1

    #@11
    .line 154
    .local v1, cert:Ljava/security/cert/Certificate;
    invoke-virtual {v1}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    #@14
    move-result-object v3

    #@15
    return-object v3
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 171
    iget-boolean v0, p0, Landroid/content/pm/Signature;->mHaveHashCode:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 172
    iget v0, p0, Landroid/content/pm/Signature;->mHashCode:I

    #@6
    .line 176
    :goto_6
    return v0

    #@7
    .line 174
    :cond_7
    iget-object v0, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@9
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/content/pm/Signature;->mHashCode:I

    #@f
    .line 175
    const/4 v0, 0x1

    #@10
    iput-boolean v0, p0, Landroid/content/pm/Signature;->mHaveHashCode:Z

    #@12
    .line 176
    iget v0, p0, Landroid/content/pm/Signature;->mHashCode:I

    #@14
    goto :goto_6
.end method

.method public toByteArray()[B
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 138
    iget-object v1, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@3
    array-length v1, v1

    #@4
    new-array v0, v1, [B

    #@6
    .line 139
    .local v0, bytes:[B
    iget-object v1, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@8
    iget-object v2, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@a
    array-length v2, v2

    #@b
    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@e
    .line 140
    return-object v0
.end method

.method public toChars()[C
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 91
    invoke-virtual {p0, v0, v0}, Landroid/content/pm/Signature;->toChars([C[I)[C

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public toChars([C[I)[C
    .registers 13
    .parameter "existingArray"
    .parameter "outLen"

    #@0
    .prologue
    const/16 v9, 0xa

    #@2
    .line 105
    iget-object v4, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@4
    .line 106
    .local v4, sig:[B
    array-length v0, v4

    #@5
    .line 107
    .local v0, N:I
    mul-int/lit8 v1, v0, 0x2

    #@7
    .line 108
    .local v1, N2:I
    if-eqz p1, :cond_c

    #@9
    array-length v7, p1

    #@a
    if-le v1, v7, :cond_34

    #@c
    :cond_c
    new-array v5, v1, [C

    #@e
    .line 110
    .local v5, text:[C
    :goto_e
    const/4 v3, 0x0

    #@f
    .local v3, j:I
    :goto_f
    if-ge v3, v0, :cond_3c

    #@11
    .line 111
    aget-byte v6, v4, v3

    #@13
    .line 112
    .local v6, v:B
    shr-int/lit8 v7, v6, 0x4

    #@15
    and-int/lit8 v2, v7, 0xf

    #@17
    .line 113
    .local v2, d:I
    mul-int/lit8 v8, v3, 0x2

    #@19
    if-lt v2, v9, :cond_36

    #@1b
    add-int/lit8 v7, v2, 0x61

    #@1d
    add-int/lit8 v7, v7, -0xa

    #@1f
    :goto_1f
    int-to-char v7, v7

    #@20
    aput-char v7, v5, v8

    #@22
    .line 114
    and-int/lit8 v2, v6, 0xf

    #@24
    .line 115
    mul-int/lit8 v7, v3, 0x2

    #@26
    add-int/lit8 v8, v7, 0x1

    #@28
    if-lt v2, v9, :cond_39

    #@2a
    add-int/lit8 v7, v2, 0x61

    #@2c
    add-int/lit8 v7, v7, -0xa

    #@2e
    :goto_2e
    int-to-char v7, v7

    #@2f
    aput-char v7, v5, v8

    #@31
    .line 110
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_f

    #@34
    .end local v2           #d:I
    .end local v3           #j:I
    .end local v5           #text:[C
    .end local v6           #v:B
    :cond_34
    move-object v5, p1

    #@35
    .line 108
    goto :goto_e

    #@36
    .line 113
    .restart local v2       #d:I
    .restart local v3       #j:I
    .restart local v5       #text:[C
    .restart local v6       #v:B
    :cond_36
    add-int/lit8 v7, v2, 0x30

    #@38
    goto :goto_1f

    #@39
    .line 115
    :cond_39
    add-int/lit8 v7, v2, 0x30

    #@3b
    goto :goto_2e

    #@3c
    .line 117
    .end local v2           #d:I
    .end local v6           #v:B
    :cond_3c
    if-eqz p2, :cond_41

    #@3e
    const/4 v7, 0x0

    #@3f
    aput v0, p2, v7

    #@41
    .line 118
    :cond_41
    return-object v5
.end method

.method public toCharsString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 125
    iget-object v2, p0, Landroid/content/pm/Signature;->mStringRef:Ljava/lang/ref/SoftReference;

    #@2
    if-nez v2, :cond_9

    #@4
    const/4 v0, 0x0

    #@5
    .line 126
    .local v0, str:Ljava/lang/String;
    :goto_5
    if-eqz v0, :cond_13

    #@7
    move-object v1, v0

    #@8
    .line 131
    .end local v0           #str:Ljava/lang/String;
    .local v1, str:Ljava/lang/Object;
    :goto_8
    return-object v1

    #@9
    .line 125
    .end local v1           #str:Ljava/lang/Object;
    :cond_9
    iget-object v2, p0, Landroid/content/pm/Signature;->mStringRef:Ljava/lang/ref/SoftReference;

    #@b
    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljava/lang/String;

    #@11
    move-object v0, v2

    #@12
    goto :goto_5

    #@13
    .line 129
    .restart local v0       #str:Ljava/lang/String;
    :cond_13
    new-instance v0, Ljava/lang/String;

    #@15
    .end local v0           #str:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/pm/Signature;->toChars()[C

    #@18
    move-result-object v2

    #@19
    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    #@1c
    .line 130
    .restart local v0       #str:Ljava/lang/String;
    new-instance v2, Ljava/lang/ref/SoftReference;

    #@1e
    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    #@21
    iput-object v2, p0, Landroid/content/pm/Signature;->mStringRef:Ljava/lang/ref/SoftReference;

    #@23
    move-object v1, v0

    #@24
    .line 131
    .restart local v1       #str:Ljava/lang/Object;
    goto :goto_8
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 184
    iget-object v0, p0, Landroid/content/pm/Signature;->mSignature:[B

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@5
    .line 185
    return-void
.end method
