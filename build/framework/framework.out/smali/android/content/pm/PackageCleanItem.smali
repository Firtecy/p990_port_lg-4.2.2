.class public Landroid/content/pm/PackageCleanItem;
.super Ljava/lang/Object;
.source "PackageCleanItem.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/PackageCleanItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final andCode:Z

.field public final packageName:Ljava/lang/String;

.field public final userId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 69
    new-instance v0, Landroid/content/pm/PackageCleanItem$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/PackageCleanItem$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/PackageCleanItem;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Z)V
    .registers 4
    .parameter "userId"
    .parameter "packageName"
    .parameter "andCode"

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    iput p1, p0, Landroid/content/pm/PackageCleanItem;->userId:I

    #@5
    .line 30
    iput-object p2, p0, Landroid/content/pm/PackageCleanItem;->packageName:Ljava/lang/String;

    #@7
    .line 31
    iput-boolean p3, p0, Landroid/content/pm/PackageCleanItem;->andCode:Z

    #@9
    .line 32
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 80
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/content/pm/PackageCleanItem;->userId:I

    #@9
    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/content/pm/PackageCleanItem;->packageName:Ljava/lang/String;

    #@f
    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_19

    #@15
    const/4 v0, 0x1

    #@16
    :goto_16
    iput-boolean v0, p0, Landroid/content/pm/PackageCleanItem;->andCode:Z

    #@18
    .line 84
    return-void

    #@19
    .line 83
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_16
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/PackageCleanItem$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/content/pm/PackageCleanItem;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 60
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "obj"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 36
    if-ne p0, p1, :cond_5

    #@4
    .line 47
    :cond_4
    :goto_4
    return v2

    #@5
    .line 40
    :cond_5
    if-eqz p1, :cond_24

    #@7
    .line 41
    :try_start_7
    move-object v0, p1

    #@8
    check-cast v0, Landroid/content/pm/PackageCleanItem;

    #@a
    move-object v1, v0

    #@b
    .line 42
    .local v1, other:Landroid/content/pm/PackageCleanItem;
    iget v4, p0, Landroid/content/pm/PackageCleanItem;->userId:I

    #@d
    iget v5, v1, Landroid/content/pm/PackageCleanItem;->userId:I

    #@f
    if-ne v4, v5, :cond_21

    #@11
    iget-object v4, p0, Landroid/content/pm/PackageCleanItem;->packageName:Ljava/lang/String;

    #@13
    iget-object v5, v1, Landroid/content/pm/PackageCleanItem;->packageName:Ljava/lang/String;

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_21

    #@1b
    iget-boolean v4, p0, Landroid/content/pm/PackageCleanItem;->andCode:Z

    #@1d
    iget-boolean v5, v1, Landroid/content/pm/PackageCleanItem;->andCode:Z
    :try_end_1f
    .catch Ljava/lang/ClassCastException; {:try_start_7 .. :try_end_1f} :catch_23

    #@1f
    if-eq v4, v5, :cond_4

    #@21
    :cond_21
    move v2, v3

    #@22
    goto :goto_4

    #@23
    .line 45
    .end local v1           #other:Landroid/content/pm/PackageCleanItem;
    :catch_23
    move-exception v2

    #@24
    :cond_24
    move v2, v3

    #@25
    .line 47
    goto :goto_4
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 52
    const/16 v0, 0x11

    #@2
    .line 53
    .local v0, result:I
    iget v1, p0, Landroid/content/pm/PackageCleanItem;->userId:I

    #@4
    add-int/lit16 v0, v1, 0x20f

    #@6
    .line 54
    mul-int/lit8 v1, v0, 0x1f

    #@8
    iget-object v2, p0, Landroid/content/pm/PackageCleanItem;->packageName:Ljava/lang/String;

    #@a
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    #@d
    move-result v2

    #@e
    add-int v0, v1, v2

    #@10
    .line 55
    mul-int/lit8 v2, v0, 0x1f

    #@12
    iget-boolean v1, p0, Landroid/content/pm/PackageCleanItem;->andCode:Z

    #@14
    if-eqz v1, :cond_1a

    #@16
    const/4 v1, 0x1

    #@17
    :goto_17
    add-int v0, v2, v1

    #@19
    .line 56
    return v0

    #@1a
    .line 55
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_17
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 64
    iget v0, p0, Landroid/content/pm/PackageCleanItem;->userId:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 65
    iget-object v0, p0, Landroid/content/pm/PackageCleanItem;->packageName:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 66
    iget-boolean v0, p0, Landroid/content/pm/PackageCleanItem;->andCode:Z

    #@c
    if-eqz v0, :cond_13

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 67
    return-void

    #@13
    .line 66
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_f
.end method
