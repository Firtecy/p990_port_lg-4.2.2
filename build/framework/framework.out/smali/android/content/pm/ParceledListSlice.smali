.class public Landroid/content/pm/ParceledListSlice;
.super Ljava/lang/Object;
.source "ParceledListSlice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/os/Parcelable;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/ParceledListSlice;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_IPC_SIZE:I = 0x40000


# instance fields
.field private mIsLastSlice:Z

.field private mNumItems:I

.field private mParcel:Landroid/os/Parcel;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 142
    new-instance v0, Landroid/content/pm/ParceledListSlice$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/ParceledListSlice$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/ParceledListSlice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 44
    .local p0, this:Landroid/content/pm/ParceledListSlice;,"Landroid/content/pm/ParceledListSlice<TT;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@9
    .line 46
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;IZ)V
    .registers 4
    .parameter "p"
    .parameter "numItems"
    .parameter "lastSlice"

    #@0
    .prologue
    .line 48
    .local p0, this:Landroid/content/pm/ParceledListSlice;,"Landroid/content/pm/ParceledListSlice<TT;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 49
    iput-object p1, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@5
    .line 50
    iput p2, p0, Landroid/content/pm/ParceledListSlice;->mNumItems:I

    #@7
    .line 51
    iput-boolean p3, p0, Landroid/content/pm/ParceledListSlice;->mIsLastSlice:Z

    #@9
    .line 52
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;IZLandroid/content/pm/ParceledListSlice$1;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 31
    .local p0, this:Landroid/content/pm/ParceledListSlice;,"Landroid/content/pm/ParceledListSlice<TT;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/content/pm/ParceledListSlice;-><init>(Landroid/os/Parcel;IZ)V

    #@3
    return-void
.end method


# virtual methods
.method public append(Landroid/os/Parcelable;)Z
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/content/pm/ParceledListSlice;,"Landroid/content/pm/ParceledListSlice<TT;>;"
    .local p1, item:Landroid/os/Parcelable;,"TT;"
    const/4 v0, 0x1

    #@1
    .line 88
    iget-object v1, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@3
    if-nez v1, :cond_d

    #@5
    .line 89
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string v1, "ParceledListSlice has already been recycled"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 92
    :cond_d
    iget-object v1, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@f
    invoke-interface {p1, v1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    #@12
    .line 93
    iget v1, p0, Landroid/content/pm/ParceledListSlice;->mNumItems:I

    #@14
    add-int/lit8 v1, v1, 0x1

    #@16
    iput v1, p0, Landroid/content/pm/ParceledListSlice;->mNumItems:I

    #@18
    .line 95
    iget-object v1, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@1a
    invoke-virtual {v1}, Landroid/os/Parcel;->dataSize()I

    #@1d
    move-result v1

    #@1e
    const/high16 v2, 0x4

    #@20
    if-le v1, v2, :cond_23

    #@22
    :goto_22
    return v0

    #@23
    :cond_23
    const/4 v0, 0x0

    #@24
    goto :goto_22
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 56
    .local p0, this:Landroid/content/pm/ParceledListSlice;,"Landroid/content/pm/ParceledListSlice<TT;>;"
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isLastSlice()Z
    .registers 2

    #@0
    .prologue
    .line 138
    .local p0, this:Landroid/content/pm/ParceledListSlice;,"Landroid/content/pm/ParceledListSlice<TT;>;"
    iget-boolean v0, p0, Landroid/content/pm/ParceledListSlice;->mIsLastSlice:Z

    #@2
    return v0
.end method

.method public populateList(Ljava/util/List;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;",
            "Landroid/os/Parcelable$Creator",
            "<TT;>;)TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 109
    .local p0, this:Landroid/content/pm/ParceledListSlice;,"Landroid/content/pm/ParceledListSlice<TT;>;"
    .local p1, list:Ljava/util/List;,"Ljava/util/List<TT;>;"
    .local p2, creator:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    iget-object v2, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@2
    const/4 v3, 0x0

    #@3
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    #@6
    .line 111
    const/4 v1, 0x0

    #@7
    .line 112
    .local v1, item:Landroid/os/Parcelable;,"TT;"
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    iget v2, p0, Landroid/content/pm/ParceledListSlice;->mNumItems:I

    #@a
    if-ge v0, v2, :cond_1a

    #@c
    .line 113
    iget-object v2, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@e
    invoke-interface {p2, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    .end local v1           #item:Landroid/os/Parcelable;,"TT;"
    check-cast v1, Landroid/os/Parcelable;

    #@14
    .line 114
    .restart local v1       #item:Landroid/os/Parcelable;,"TT;"
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@17
    .line 112
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 117
    :cond_1a
    iget-object v2, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@1c
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 118
    const/4 v2, 0x0

    #@20
    iput-object v2, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@22
    .line 120
    return-object v1
.end method

.method public setLastSlice(Z)V
    .registers 2
    .parameter "lastSlice"

    #@0
    .prologue
    .line 129
    .local p0, this:Landroid/content/pm/ParceledListSlice;,"Landroid/content/pm/ParceledListSlice<TT;>;"
    iput-boolean p1, p0, Landroid/content/pm/ParceledListSlice;->mIsLastSlice:Z

    #@2
    .line 130
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .local p0, this:Landroid/content/pm/ParceledListSlice;,"Landroid/content/pm/ParceledListSlice<TT;>;"
    const/4 v2, 0x0

    #@1
    .line 66
    iget v1, p0, Landroid/content/pm/ParceledListSlice;->mNumItems:I

    #@3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 67
    iget-boolean v1, p0, Landroid/content/pm/ParceledListSlice;->mIsLastSlice:Z

    #@8
    if-eqz v1, :cond_2b

    #@a
    const/4 v1, 0x1

    #@b
    :goto_b
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 69
    iget v1, p0, Landroid/content/pm/ParceledListSlice;->mNumItems:I

    #@10
    if-lez v1, :cond_20

    #@12
    .line 70
    iget-object v1, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@14
    invoke-virtual {v1}, Landroid/os/Parcel;->dataSize()I

    #@17
    move-result v0

    #@18
    .line 71
    .local v0, parcelSize:I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 72
    iget-object v1, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@1d
    invoke-virtual {p1, v1, v2, v0}, Landroid/os/Parcel;->appendFrom(Landroid/os/Parcel;II)V

    #@20
    .line 75
    .end local v0           #parcelSize:I
    :cond_20
    iput v2, p0, Landroid/content/pm/ParceledListSlice;->mNumItems:I

    #@22
    .line 76
    iget-object v1, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 77
    const/4 v1, 0x0

    #@28
    iput-object v1, p0, Landroid/content/pm/ParceledListSlice;->mParcel:Landroid/os/Parcel;

    #@2a
    .line 78
    return-void

    #@2b
    :cond_2b
    move v1, v2

    #@2c
    .line 67
    goto :goto_b
.end method
