.class public Landroid/content/pm/PackageParser;
.super Ljava/lang/Object;
.source "PackageParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/pm/PackageParser$ServiceIntentInfo;,
        Landroid/content/pm/PackageParser$ActivityIntentInfo;,
        Landroid/content/pm/PackageParser$IntentInfo;,
        Landroid/content/pm/PackageParser$Instrumentation;,
        Landroid/content/pm/PackageParser$Provider;,
        Landroid/content/pm/PackageParser$Service;,
        Landroid/content/pm/PackageParser$Activity;,
        Landroid/content/pm/PackageParser$PermissionGroup;,
        Landroid/content/pm/PackageParser$Permission;,
        Landroid/content/pm/PackageParser$Component;,
        Landroid/content/pm/PackageParser$Package;,
        Landroid/content/pm/PackageParser$PackageLite;,
        Landroid/content/pm/PackageParser$ParseComponentArgs;,
        Landroid/content/pm/PackageParser$ParsePackageItemArgs;,
        Landroid/content/pm/PackageParser$SplitPermissionInfo;,
        Landroid/content/pm/PackageParser$NewPermissionInfo;
    }
.end annotation


# static fields
.field private static final ANDROID_MANIFEST_FILENAME:Ljava/lang/String; = "AndroidManifest.xml"

.field private static final ANDROID_RESOURCES:Ljava/lang/String; = "http://schemas.android.com/apk/res/android"

.field private static final DEBUG_BACKUP:Z = false

.field private static final DEBUG_JAR:Z = false

.field private static final DEBUG_PARSER:Z = false

.field public static final NEW_PERMISSIONS:[Landroid/content/pm/PackageParser$NewPermissionInfo; = null

.field public static final PARSE_CHATTY:I = 0x2

.field private static final PARSE_DEFAULT_INSTALL_LOCATION:I = -0x1

.field public static final PARSE_FORWARD_LOCK:I = 0x10

.field public static final PARSE_IGNORE_PROCESSES:I = 0x8

.field public static final PARSE_IS_SYSTEM:I = 0x1

.field public static final PARSE_IS_SYSTEM_DIR:I = 0x40

.field public static final PARSE_MUST_BE_APK:I = 0x4

.field public static final PARSE_ON_SDCARD:I = 0x20

.field private static final RIGID_PARSER:Z = false

.field private static final SDK_CODENAME:Ljava/lang/String; = null

#the value of this static final field might be set in the static constructor
.field private static final SDK_VERSION:I = 0x0

.field public static final SPLIT_PERMISSIONS:[Landroid/content/pm/PackageParser$SplitPermissionInfo; = null

.field private static final TAG:Ljava/lang/String; = "PackageParser"

.field private static mReadBuffer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<[B>;"
        }
    .end annotation
.end field

.field private static final mSync:Ljava/lang/Object;

.field private static sCompatibilityModeEnabled:Z


# instance fields
.field private mArchiveSourcePath:Ljava/lang/String;

.field private mOnlyCoreApps:Z

.field private mParseActivityAliasArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

.field private mParseActivityArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

.field private mParseError:I

.field private mParseInstrumentationArgs:Landroid/content/pm/PackageParser$ParsePackageItemArgs;

.field private mParseProviderArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

.field private mParseServiceArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

.field private mSeparateProcesses:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/16 v8, 0x10

    #@2
    const/4 v3, 0x4

    #@3
    const/4 v7, 0x2

    #@4
    const/4 v6, 0x1

    #@5
    const/4 v5, 0x0

    #@6
    .line 116
    new-array v0, v7, [Landroid/content/pm/PackageParser$NewPermissionInfo;

    #@8
    new-instance v1, Landroid/content/pm/PackageParser$NewPermissionInfo;

    #@a
    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    #@c
    invoke-direct {v1, v2, v3, v5}, Landroid/content/pm/PackageParser$NewPermissionInfo;-><init>(Ljava/lang/String;II)V

    #@f
    aput-object v1, v0, v5

    #@11
    new-instance v1, Landroid/content/pm/PackageParser$NewPermissionInfo;

    #@13
    const-string v2, "android.permission.READ_PHONE_STATE"

    #@15
    invoke-direct {v1, v2, v3, v5}, Landroid/content/pm/PackageParser$NewPermissionInfo;-><init>(Ljava/lang/String;II)V

    #@18
    aput-object v1, v0, v6

    #@1a
    sput-object v0, Landroid/content/pm/PackageParser;->NEW_PERMISSIONS:[Landroid/content/pm/PackageParser$NewPermissionInfo;

    #@1c
    .line 129
    const/4 v0, 0x3

    #@1d
    new-array v0, v0, [Landroid/content/pm/PackageParser$SplitPermissionInfo;

    #@1f
    new-instance v1, Landroid/content/pm/PackageParser$SplitPermissionInfo;

    #@21
    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    #@23
    new-array v3, v6, [Ljava/lang/String;

    #@25
    const-string v4, "android.permission.READ_EXTERNAL_STORAGE"

    #@27
    aput-object v4, v3, v5

    #@29
    const/16 v4, 0x2711

    #@2b
    invoke-direct {v1, v2, v3, v4}, Landroid/content/pm/PackageParser$SplitPermissionInfo;-><init>(Ljava/lang/String;[Ljava/lang/String;I)V

    #@2e
    aput-object v1, v0, v5

    #@30
    new-instance v1, Landroid/content/pm/PackageParser$SplitPermissionInfo;

    #@32
    const-string v2, "android.permission.READ_CONTACTS"

    #@34
    new-array v3, v6, [Ljava/lang/String;

    #@36
    const-string v4, "android.permission.READ_CALL_LOG"

    #@38
    aput-object v4, v3, v5

    #@3a
    invoke-direct {v1, v2, v3, v8}, Landroid/content/pm/PackageParser$SplitPermissionInfo;-><init>(Ljava/lang/String;[Ljava/lang/String;I)V

    #@3d
    aput-object v1, v0, v6

    #@3f
    new-instance v1, Landroid/content/pm/PackageParser$SplitPermissionInfo;

    #@41
    const-string v2, "android.permission.WRITE_CONTACTS"

    #@43
    new-array v3, v6, [Ljava/lang/String;

    #@45
    const-string v4, "android.permission.WRITE_CALL_LOG"

    #@47
    aput-object v4, v3, v5

    #@49
    invoke-direct {v1, v2, v3, v8}, Landroid/content/pm/PackageParser$SplitPermissionInfo;-><init>(Ljava/lang/String;[Ljava/lang/String;I)V

    #@4c
    aput-object v1, v0, v7

    #@4e
    sput-object v0, Landroid/content/pm/PackageParser;->SPLIT_PERMISSIONS:[Landroid/content/pm/PackageParser$SplitPermissionInfo;

    #@50
    .line 149
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    #@52
    sput v0, Landroid/content/pm/PackageParser;->SDK_VERSION:I

    #@54
    .line 150
    const-string v0, "REL"

    #@56
    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    #@58
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_6b

    #@5e
    const/4 v0, 0x0

    #@5f
    :goto_5f
    sput-object v0, Landroid/content/pm/PackageParser;->SDK_CODENAME:Ljava/lang/String;

    #@61
    .line 155
    new-instance v0, Ljava/lang/Object;

    #@63
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@66
    sput-object v0, Landroid/content/pm/PackageParser;->mSync:Ljava/lang/Object;

    #@68
    .line 158
    sput-boolean v6, Landroid/content/pm/PackageParser;->sCompatibilityModeEnabled:Z

    #@6a
    return-void

    #@6b
    .line 150
    :cond_6b
    sget-object v0, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    #@6d
    goto :goto_5f
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "archiveSourcePath"

    #@0
    .prologue
    .line 234
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 153
    const/4 v0, 0x1

    #@4
    iput v0, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@6
    .line 235
    iput-object p1, p0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@8
    .line 236
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 73
    invoke-static {p0, p1, p2}, Landroid/content/pm/PackageParser;->buildClassName(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 73
    invoke-static/range {p0 .. p5}, Landroid/content/pm/PackageParser;->buildProcessName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static buildClassName(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "pkg"
    .parameter "clsSeq"
    .parameter "outError"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/16 v5, 0x2e

    #@3
    const/4 v6, 0x0

    #@4
    .line 1439
    if-eqz p1, :cond_c

    #@6
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@9
    move-result v4

    #@a
    if-gtz v4, :cond_22

    #@c
    .line 1440
    :cond_c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "Empty class name in package "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    aput-object v4, p2, v6

    #@21
    .line 1458
    :goto_21
    return-object v3

    #@22
    .line 1443
    :cond_22
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    .line 1444
    .local v2, cls:Ljava/lang/String;
    invoke-virtual {v2, v6}, Ljava/lang/String;->charAt(I)C

    #@29
    move-result v1

    #@2a
    .line 1445
    .local v1, c:C
    if-ne v1, v5, :cond_42

    #@2c
    .line 1446
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    goto :goto_21

    #@42
    .line 1448
    :cond_42
    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    #@45
    move-result v4

    #@46
    if-gez v4, :cond_5c

    #@48
    .line 1449
    new-instance v0, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@4d
    .line 1450
    .local v0, b:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@50
    .line 1451
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    .line 1452
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@5a
    move-result-object v3

    #@5b
    goto :goto_21

    #@5c
    .line 1454
    .end local v0           #b:Ljava/lang/StringBuilder;
    :cond_5c
    const/16 v4, 0x61

    #@5e
    if-lt v1, v4, :cond_69

    #@60
    const/16 v4, 0x7a

    #@62
    if-gt v1, v4, :cond_69

    #@64
    .line 1455
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@67
    move-result-object v3

    #@68
    goto :goto_21

    #@69
    .line 1457
    :cond_69
    new-instance v4, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v5, "Bad class name "

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    const-string v5, " in package "

    #@7a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v4

    #@7e
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v4

    #@86
    aput-object v4, p2, v6

    #@88
    goto :goto_21
.end method

.method private static buildCompoundName(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "pkg"
    .parameter "procSeq"
    .parameter "type"
    .parameter "outError"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v7, 0x0

    #@3
    .line 1463
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    .line 1464
    .local v2, proc:Ljava/lang/String;
    invoke-virtual {v2, v7}, Ljava/lang/String;->charAt(I)C

    #@a
    move-result v0

    #@b
    .line 1465
    .local v0, c:C
    if-eqz p0, :cond_9c

    #@d
    const/16 v5, 0x3a

    #@f
    if-ne v0, v5, :cond_9c

    #@11
    .line 1466
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@14
    move-result v5

    #@15
    const/4 v6, 0x2

    #@16
    if-ge v5, v6, :cond_48

    #@18
    .line 1467
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v6, "Bad "

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    const-string v6, " name "

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    const-string v6, " in package "

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    const-string v6, ": must be at least two characters"

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    aput-object v5, p3, v7

    #@47
    .line 1486
    :goto_47
    return-object v4

    #@48
    .line 1471
    :cond_48
    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    .line 1472
    .local v3, subName:Ljava/lang/String;
    invoke-static {v3, v7}, Landroid/content/pm/PackageParser;->validateName(Ljava/lang/String;Z)Ljava/lang/String;

    #@4f
    move-result-object v1

    #@50
    .line 1473
    .local v1, nameError:Ljava/lang/String;
    if-eqz v1, :cond_86

    #@52
    .line 1474
    new-instance v5, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v6, "Invalid "

    #@59
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    const-string v6, " name "

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v5

    #@6b
    const-string v6, " in package "

    #@6d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    const-string v6, ": "

    #@77
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v5

    #@83
    aput-object v5, p3, v7

    #@85
    goto :goto_47

    #@86
    .line 1478
    :cond_86
    new-instance v4, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v4

    #@8f
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v4

    #@93
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v4

    #@97
    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@9a
    move-result-object v4

    #@9b
    goto :goto_47

    #@9c
    .line 1480
    .end local v1           #nameError:Ljava/lang/String;
    .end local v3           #subName:Ljava/lang/String;
    :cond_9c
    invoke-static {v2, v8}, Landroid/content/pm/PackageParser;->validateName(Ljava/lang/String;Z)Ljava/lang/String;

    #@9f
    move-result-object v1

    #@a0
    .line 1481
    .restart local v1       #nameError:Ljava/lang/String;
    if-eqz v1, :cond_e0

    #@a2
    const-string/jumbo v5, "system"

    #@a5
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a8
    move-result v5

    #@a9
    if-nez v5, :cond_e0

    #@ab
    .line 1482
    new-instance v5, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v6, "Invalid "

    #@b2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v5

    #@b6
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v5

    #@ba
    const-string v6, " name "

    #@bc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v5

    #@c0
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v5

    #@c4
    const-string v6, " in package "

    #@c6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v5

    #@ca
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v5

    #@ce
    const-string v6, ": "

    #@d0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v5

    #@d4
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v5

    #@d8
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v5

    #@dc
    aput-object v5, p3, v7

    #@de
    goto/16 :goto_47

    #@e0
    .line 1486
    :cond_e0
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@e3
    move-result-object v4

    #@e4
    goto/16 :goto_47
.end method

.method private static buildProcessName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "pkg"
    .parameter "defProc"
    .parameter "procSeq"
    .parameter "flags"
    .parameter "separateProcesses"
    .parameter "outError"

    #@0
    .prologue
    .line 1492
    and-int/lit8 v2, p3, 0x8

    #@2
    if-eqz v2, :cond_12

    #@4
    const-string/jumbo v2, "system"

    #@7
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_12

    #@d
    .line 1493
    if-eqz p1, :cond_10

    #@f
    .line 1506
    .end local p1
    :cond_f
    :goto_f
    return-object p1

    #@10
    .restart local p1
    :cond_10
    move-object p1, p0

    #@11
    .line 1493
    goto :goto_f

    #@12
    .line 1495
    :cond_12
    if-eqz p4, :cond_32

    #@14
    .line 1496
    array-length v2, p4

    #@15
    add-int/lit8 v0, v2, -0x1

    #@17
    .local v0, i:I
    :goto_17
    if-ltz v0, :cond_32

    #@19
    .line 1497
    aget-object v1, p4, v0

    #@1b
    .line 1498
    .local v1, sp:Ljava/lang/String;
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v2

    #@1f
    if-nez v2, :cond_2d

    #@21
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v2

    #@25
    if-nez v2, :cond_2d

    #@27
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_2f

    #@2d
    :cond_2d
    move-object p1, p0

    #@2e
    .line 1499
    goto :goto_f

    #@2f
    .line 1496
    :cond_2f
    add-int/lit8 v0, v0, -0x1

    #@31
    goto :goto_17

    #@32
    .line 1503
    .end local v0           #i:I
    .end local v1           #sp:Ljava/lang/String;
    :cond_32
    if-eqz p2, :cond_f

    #@34
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    #@37
    move-result v2

    #@38
    if-lez v2, :cond_f

    #@3a
    .line 1506
    const-string/jumbo v2, "process"

    #@3d
    invoke-static {p0, p2, v2, p5}, Landroid/content/pm/PackageParser;->buildCompoundName(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object p1

    #@41
    goto :goto_f
.end method

.method private static buildTaskAffinityName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "pkg"
    .parameter "defProc"
    .parameter "procSeq"
    .parameter "outError"

    #@0
    .prologue
    .line 1511
    if-nez p2, :cond_3

    #@2
    .line 1517
    .end local p1
    :goto_2
    return-object p1

    #@3
    .line 1514
    .restart local p1
    :cond_3
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    #@6
    move-result v0

    #@7
    if-gtz v0, :cond_b

    #@9
    .line 1515
    const/4 p1, 0x0

    #@a
    goto :goto_2

    #@b
    .line 1517
    :cond_b
    const-string/jumbo v0, "taskAffinity"

    #@e
    invoke-static {p0, p2, v0, p3}, Landroid/content/pm/PackageParser;->buildCompoundName(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object p1

    #@12
    goto :goto_2
.end method

.method private static checkUseInstalled(ILandroid/content/pm/PackageUserState;)Z
    .registers 3
    .parameter "flags"
    .parameter "state"

    #@0
    .prologue
    .line 275
    iget-boolean v0, p1, Landroid/content/pm/PackageUserState;->installed:Z

    #@2
    if-nez v0, :cond_8

    #@4
    and-int/lit16 v0, p0, 0x2000

    #@6
    if-eqz v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private static copyNeeded(ILandroid/content/pm/PackageParser$Package;Landroid/content/pm/PackageUserState;Landroid/os/Bundle;I)Z
    .registers 9
    .parameter "flags"
    .parameter "p"
    .parameter "state"
    .parameter "metaData"
    .parameter "userId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 3598
    if-eqz p4, :cond_5

    #@4
    .line 3623
    :cond_4
    :goto_4
    return v1

    #@5
    .line 3603
    :cond_5
    iget v3, p2, Landroid/content/pm/PackageUserState;->enabled:I

    #@7
    if-eqz v3, :cond_14

    #@9
    .line 3604
    iget v3, p2, Landroid/content/pm/PackageUserState;->enabled:I

    #@b
    if-ne v3, v1, :cond_30

    #@d
    move v0, v1

    #@e
    .line 3605
    .local v0, enabled:Z
    :goto_e
    iget-object v3, p1, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@10
    iget-boolean v3, v3, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@12
    if-ne v3, v0, :cond_4

    #@14
    .line 3609
    .end local v0           #enabled:Z
    :cond_14
    iget-boolean v3, p2, Landroid/content/pm/PackageUserState;->installed:Z

    #@16
    if-eqz v3, :cond_4

    #@18
    .line 3612
    iget-boolean v3, p2, Landroid/content/pm/PackageUserState;->stopped:Z

    #@1a
    if-nez v3, :cond_4

    #@1c
    .line 3615
    and-int/lit16 v3, p0, 0x80

    #@1e
    if-eqz v3, :cond_26

    #@20
    if-nez p3, :cond_4

    #@22
    iget-object v3, p1, Landroid/content/pm/PackageParser$Package;->mAppMetaData:Landroid/os/Bundle;

    #@24
    if-nez v3, :cond_4

    #@26
    .line 3619
    :cond_26
    and-int/lit16 v3, p0, 0x400

    #@28
    if-eqz v3, :cond_2e

    #@2a
    iget-object v3, p1, Landroid/content/pm/PackageParser$Package;->usesLibraryFiles:[Ljava/lang/String;

    #@2c
    if-nez v3, :cond_4

    #@2e
    :cond_2e
    move v1, v2

    #@2f
    .line 3623
    goto :goto_4

    #@30
    :cond_30
    move v0, v2

    #@31
    .line 3604
    goto :goto_e
.end method

.method private findOverlayPackage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "apkPath"
    .parameter "packageName"

    #@0
    .prologue
    .line 581
    const/4 v0, 0x0

    #@1
    .line 585
    .local v0, custOverlayPath:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string/jumbo v3, "ro.lge.capp_cupss.rootdir"

    #@9
    const-string v4, "/cust"

    #@b
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, "/overlay/app/"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 586
    .local v1, searchDirPath:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    const-string v3, ".apk"

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    .line 587
    new-instance v2, Ljava/io/File;

    #@36
    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@39
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@3c
    move-result v2

    #@3d
    if-eqz v2, :cond_41

    #@3f
    move-object v2, v0

    #@40
    .line 597
    :goto_40
    return-object v2

    #@41
    .line 592
    :cond_41
    new-instance v2, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    new-instance v3, Ljava/io/File;

    #@4c
    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@4f
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    .line 593
    new-instance v2, Ljava/io/File;

    #@5d
    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@60
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@63
    move-result v2

    #@64
    if-eqz v2, :cond_68

    #@66
    move-object v2, v0

    #@67
    .line 594
    goto :goto_40

    #@68
    .line 597
    :cond_68
    const/4 v2, 0x0

    #@69
    goto :goto_40
.end method

.method public static final generateActivityInfo(Landroid/content/pm/PackageParser$Activity;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ActivityInfo;
    .registers 7
    .parameter "a"
    .parameter "flags"
    .parameter "state"
    .parameter "userId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3737
    if-nez p0, :cond_4

    #@3
    .line 3748
    :cond_3
    :goto_3
    return-object v0

    #@4
    .line 3738
    :cond_4
    invoke-static {p1, p2}, Landroid/content/pm/PackageParser;->checkUseInstalled(ILandroid/content/pm/PackageUserState;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_3

    #@a
    .line 3741
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@c
    iget-object v2, p0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@e
    invoke-static {p1, v1, p2, v2, p3}, Landroid/content/pm/PackageParser;->copyNeeded(ILandroid/content/pm/PackageParser$Package;Landroid/content/pm/PackageUserState;Landroid/os/Bundle;I)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_17

    #@14
    .line 3742
    iget-object v0, p0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@16
    goto :goto_3

    #@17
    .line 3745
    :cond_17
    new-instance v0, Landroid/content/pm/ActivityInfo;

    #@19
    iget-object v1, p0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@1b
    invoke-direct {v0, v1}, Landroid/content/pm/ActivityInfo;-><init>(Landroid/content/pm/ActivityInfo;)V

    #@1e
    .line 3746
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@20
    iput-object v1, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@22
    .line 3747
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@24
    invoke-static {v1, p1, p2, p3}, Landroid/content/pm/PackageParser;->generateApplicationInfo(Landroid/content/pm/PackageParser$Package;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ApplicationInfo;

    #@27
    move-result-object v1

    #@28
    iput-object v1, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2a
    goto :goto_3
.end method

.method public static generateApplicationInfo(Landroid/content/pm/PackageParser$Package;ILandroid/content/pm/PackageUserState;)Landroid/content/pm/ApplicationInfo;
    .registers 4
    .parameter "p"
    .parameter "flags"
    .parameter "state"

    #@0
    .prologue
    .line 3628
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/content/pm/PackageParser;->generateApplicationInfo(Landroid/content/pm/PackageParser$Package;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ApplicationInfo;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static generateApplicationInfo(Landroid/content/pm/PackageParser$Package;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ApplicationInfo;
    .registers 12
    .parameter "p"
    .parameter "flags"
    .parameter "state"
    .parameter "userId"

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v0, 0x0

    #@4
    const/4 v4, 0x1

    #@5
    .line 3633
    if-nez p0, :cond_8

    #@7
    .line 3689
    :cond_7
    :goto_7
    return-object v0

    #@8
    .line 3634
    :cond_8
    invoke-static {p1, p2}, Landroid/content/pm/PackageParser;->checkUseInstalled(ILandroid/content/pm/PackageUserState;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_7

    #@e
    .line 3637
    invoke-static {p1, p0, p2, v0, p3}, Landroid/content/pm/PackageParser;->copyNeeded(ILandroid/content/pm/PackageParser$Package;Landroid/content/pm/PackageUserState;Landroid/os/Bundle;I)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_44

    #@14
    .line 3640
    sget-boolean v1, Landroid/content/pm/PackageParser;->sCompatibilityModeEnabled:Z

    #@16
    if-nez v1, :cond_1d

    #@18
    .line 3641
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1a
    invoke-virtual {v1}, Landroid/content/pm/ApplicationInfo;->disableCompatibilityMode()V

    #@1d
    .line 3646
    :cond_1d
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1f
    iget v2, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@21
    const/high16 v3, 0x80

    #@23
    or-int/2addr v2, v3

    #@24
    iput v2, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@26
    .line 3647
    iget v1, p2, Landroid/content/pm/PackageUserState;->enabled:I

    #@28
    if-ne v1, v4, :cond_37

    #@2a
    .line 3648
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2c
    iput-boolean v4, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@2e
    .line 3653
    :cond_2e
    :goto_2e
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@30
    iget v2, p2, Landroid/content/pm/PackageUserState;->enabled:I

    #@32
    iput v2, v1, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    #@34
    .line 3654
    iget-object v0, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@36
    goto :goto_7

    #@37
    .line 3649
    :cond_37
    iget v1, p2, Landroid/content/pm/PackageUserState;->enabled:I

    #@39
    if-eq v1, v6, :cond_3f

    #@3b
    iget v1, p2, Landroid/content/pm/PackageUserState;->enabled:I

    #@3d
    if-ne v1, v7, :cond_2e

    #@3f
    .line 3651
    :cond_3f
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@41
    iput-boolean v5, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@43
    goto :goto_2e

    #@44
    .line 3658
    :cond_44
    new-instance v0, Landroid/content/pm/ApplicationInfo;

    #@46
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@48
    invoke-direct {v0, v1}, Landroid/content/pm/ApplicationInfo;-><init>(Landroid/content/pm/ApplicationInfo;)V

    #@4b
    .line 3659
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    if-eqz p3, :cond_5d

    #@4d
    .line 3660
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@4f
    invoke-static {p3, v1}, Landroid/os/UserHandle;->getUid(II)I

    #@52
    move-result v1

    #@53
    iput v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@55
    .line 3661
    iget-object v1, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@57
    invoke-static {p3, v1}, Landroid/content/pm/PackageManager;->getDataDirForUser(ILjava/lang/String;)Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    iput-object v1, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@5d
    .line 3663
    :cond_5d
    and-int/lit16 v1, p1, 0x80

    #@5f
    if-eqz v1, :cond_65

    #@61
    .line 3664
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->mAppMetaData:Landroid/os/Bundle;

    #@63
    iput-object v1, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@65
    .line 3666
    :cond_65
    and-int/lit16 v1, p1, 0x400

    #@67
    if-eqz v1, :cond_6d

    #@69
    .line 3667
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->usesLibraryFiles:[Ljava/lang/String;

    #@6b
    iput-object v1, v0, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;

    #@6d
    .line 3669
    :cond_6d
    sget-boolean v1, Landroid/content/pm/PackageParser;->sCompatibilityModeEnabled:Z

    #@6f
    if-nez v1, :cond_74

    #@71
    .line 3670
    invoke-virtual {v0}, Landroid/content/pm/ApplicationInfo;->disableCompatibilityMode()V

    #@74
    .line 3672
    :cond_74
    iget-boolean v1, p2, Landroid/content/pm/PackageUserState;->stopped:Z

    #@76
    if-eqz v1, :cond_96

    #@78
    .line 3673
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@7a
    const/high16 v2, 0x20

    #@7c
    or-int/2addr v1, v2

    #@7d
    iput v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@7f
    .line 3677
    :goto_7f
    iget-boolean v1, p2, Landroid/content/pm/PackageUserState;->installed:Z

    #@81
    if-eqz v1, :cond_9f

    #@83
    .line 3678
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@85
    const/high16 v2, 0x80

    #@87
    or-int/2addr v1, v2

    #@88
    iput v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@8a
    .line 3682
    :goto_8a
    iget v1, p2, Landroid/content/pm/PackageUserState;->enabled:I

    #@8c
    if-ne v1, v4, :cond_a8

    #@8e
    .line 3683
    iput-boolean v4, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@90
    .line 3688
    :cond_90
    :goto_90
    iget v1, p2, Landroid/content/pm/PackageUserState;->enabled:I

    #@92
    iput v1, v0, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    #@94
    goto/16 :goto_7

    #@96
    .line 3675
    :cond_96
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@98
    const v2, -0x200001

    #@9b
    and-int/2addr v1, v2

    #@9c
    iput v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@9e
    goto :goto_7f

    #@9f
    .line 3680
    :cond_9f
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@a1
    const v2, -0x800001

    #@a4
    and-int/2addr v1, v2

    #@a5
    iput v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@a7
    goto :goto_8a

    #@a8
    .line 3684
    :cond_a8
    iget v1, p2, Landroid/content/pm/PackageUserState;->enabled:I

    #@aa
    if-eq v1, v6, :cond_b0

    #@ac
    iget v1, p2, Landroid/content/pm/PackageUserState;->enabled:I

    #@ae
    if-ne v1, v7, :cond_90

    #@b0
    .line 3686
    :cond_b0
    iput-boolean v5, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@b2
    goto :goto_90
.end method

.method public static final generateInstrumentationInfo(Landroid/content/pm/PackageParser$Instrumentation;I)Landroid/content/pm/InstrumentationInfo;
    .registers 4
    .parameter "i"
    .parameter "flags"

    #@0
    .prologue
    .line 3860
    if-nez p0, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    .line 3866
    :goto_3
    return-object v0

    #@4
    .line 3861
    :cond_4
    and-int/lit16 v1, p1, 0x80

    #@6
    if-nez v1, :cond_b

    #@8
    .line 3862
    iget-object v0, p0, Landroid/content/pm/PackageParser$Instrumentation;->info:Landroid/content/pm/InstrumentationInfo;

    #@a
    goto :goto_3

    #@b
    .line 3864
    :cond_b
    new-instance v0, Landroid/content/pm/InstrumentationInfo;

    #@d
    iget-object v1, p0, Landroid/content/pm/PackageParser$Instrumentation;->info:Landroid/content/pm/InstrumentationInfo;

    #@f
    invoke-direct {v0, v1}, Landroid/content/pm/InstrumentationInfo;-><init>(Landroid/content/pm/InstrumentationInfo;)V

    #@12
    .line 3865
    .local v0, ii:Landroid/content/pm/InstrumentationInfo;
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@14
    iput-object v1, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@16
    goto :goto_3
.end method

.method public static generatePackageInfo(Landroid/content/pm/PackageParser$Package;[IIJJLjava/util/HashSet;Landroid/content/pm/PackageUserState;)Landroid/content/pm/PackageInfo;
    .registers 19
    .parameter "p"
    .parameter "gids"
    .parameter "flags"
    .parameter "firstInstallTime"
    .parameter "lastUpdateTime"
    .parameter
    .parameter "state"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageParser$Package;",
            "[IIJJ",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/pm/PackageUserState;",
            ")",
            "Landroid/content/pm/PackageInfo;"
        }
    .end annotation

    #@0
    .prologue
    .line 270
    .local p7, grantedPermissions:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v9

    #@4
    move-object v0, p0

    #@5
    move-object v1, p1

    #@6
    move v2, p2

    #@7
    move-wide v3, p3

    #@8
    move-wide v5, p5

    #@9
    move-object/from16 v7, p7

    #@b
    move-object/from16 v8, p8

    #@d
    invoke-static/range {v0 .. v9}, Landroid/content/pm/PackageParser;->generatePackageInfo(Landroid/content/pm/PackageParser$Package;[IIJJLjava/util/HashSet;Landroid/content/pm/PackageUserState;I)Landroid/content/pm/PackageInfo;

    #@10
    move-result-object v0

    #@11
    return-object v0
.end method

.method public static generatePackageInfo(Landroid/content/pm/PackageParser$Package;[IIJJLjava/util/HashSet;Landroid/content/pm/PackageUserState;I)Landroid/content/pm/PackageInfo;
    .registers 28
    .parameter "p"
    .parameter "gids"
    .parameter "flags"
    .parameter "firstInstallTime"
    .parameter "lastUpdateTime"
    .parameter
    .parameter "state"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageParser$Package;",
            "[IIJJ",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/pm/PackageUserState;",
            "I)",
            "Landroid/content/pm/PackageInfo;"
        }
    .end annotation

    #@0
    .prologue
    .line 282
    .local p7, grantedPermissions:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    move/from16 v0, p2

    #@2
    move-object/from16 v1, p8

    #@4
    invoke-static {v0, v1}, Landroid/content/pm/PackageParser;->checkUseInstalled(ILandroid/content/pm/PackageUserState;)Z

    #@7
    move-result v14

    #@8
    if-nez v14, :cond_c

    #@a
    .line 283
    const/4 v11, 0x0

    #@b
    .line 439
    :cond_b
    :goto_b
    return-object v11

    #@c
    .line 285
    :cond_c
    new-instance v11, Landroid/content/pm/PackageInfo;

    #@e
    invoke-direct {v11}, Landroid/content/pm/PackageInfo;-><init>()V

    #@11
    .line 286
    .local v11, pi:Landroid/content/pm/PackageInfo;
    move-object/from16 v0, p0

    #@13
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@15
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@17
    .line 287
    move-object/from16 v0, p0

    #@19
    iget v14, v0, Landroid/content/pm/PackageParser$Package;->mVersionCode:I

    #@1b
    iput v14, v11, Landroid/content/pm/PackageInfo;->versionCode:I

    #@1d
    .line 288
    move-object/from16 v0, p0

    #@1f
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    #@21
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    #@23
    .line 289
    move-object/from16 v0, p0

    #@25
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->mSharedUserId:Ljava/lang/String;

    #@27
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->sharedUserId:Ljava/lang/String;

    #@29
    .line 290
    move-object/from16 v0, p0

    #@2b
    iget v14, v0, Landroid/content/pm/PackageParser$Package;->mSharedUserLabel:I

    #@2d
    iput v14, v11, Landroid/content/pm/PackageInfo;->sharedUserLabel:I

    #@2f
    .line 291
    move-object/from16 v0, p0

    #@31
    move/from16 v1, p2

    #@33
    move-object/from16 v2, p8

    #@35
    move/from16 v3, p9

    #@37
    invoke-static {v0, v1, v2, v3}, Landroid/content/pm/PackageParser;->generateApplicationInfo(Landroid/content/pm/PackageParser$Package;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ApplicationInfo;

    #@3a
    move-result-object v14

    #@3b
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3d
    .line 292
    move-object/from16 v0, p0

    #@3f
    iget v14, v0, Landroid/content/pm/PackageParser$Package;->installLocation:I

    #@41
    iput v14, v11, Landroid/content/pm/PackageInfo;->installLocation:I

    #@43
    .line 293
    move-wide/from16 v0, p3

    #@45
    iput-wide v0, v11, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    #@47
    .line 294
    move-wide/from16 v0, p5

    #@49
    iput-wide v0, v11, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    #@4b
    .line 295
    move/from16 v0, p2

    #@4d
    and-int/lit16 v14, v0, 0x100

    #@4f
    if-eqz v14, :cond_55

    #@51
    .line 296
    move-object/from16 v0, p1

    #@53
    iput-object v0, v11, Landroid/content/pm/PackageInfo;->gids:[I

    #@55
    .line 298
    :cond_55
    move/from16 v0, p2

    #@57
    and-int/lit16 v14, v0, 0x4000

    #@59
    if-eqz v14, :cond_8f

    #@5b
    .line 299
    move-object/from16 v0, p0

    #@5d
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->configPreferences:Ljava/util/ArrayList;

    #@5f
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@62
    move-result v4

    #@63
    .line 300
    .local v4, N:I
    if-lez v4, :cond_72

    #@65
    .line 301
    new-array v14, v4, [Landroid/content/pm/ConfigurationInfo;

    #@67
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->configPreferences:[Landroid/content/pm/ConfigurationInfo;

    #@69
    .line 302
    move-object/from16 v0, p0

    #@6b
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->configPreferences:Ljava/util/ArrayList;

    #@6d
    iget-object v15, v11, Landroid/content/pm/PackageInfo;->configPreferences:[Landroid/content/pm/ConfigurationInfo;

    #@6f
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@72
    .line 304
    :cond_72
    move-object/from16 v0, p0

    #@74
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->reqFeatures:Ljava/util/ArrayList;

    #@76
    if-eqz v14, :cond_e0

    #@78
    move-object/from16 v0, p0

    #@7a
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->reqFeatures:Ljava/util/ArrayList;

    #@7c
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@7f
    move-result v4

    #@80
    .line 305
    :goto_80
    if-lez v4, :cond_8f

    #@82
    .line 306
    new-array v14, v4, [Landroid/content/pm/FeatureInfo;

    #@84
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->reqFeatures:[Landroid/content/pm/FeatureInfo;

    #@86
    .line 307
    move-object/from16 v0, p0

    #@88
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->reqFeatures:Ljava/util/ArrayList;

    #@8a
    iget-object v15, v11, Landroid/content/pm/PackageInfo;->reqFeatures:[Landroid/content/pm/FeatureInfo;

    #@8c
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@8f
    .line 310
    .end local v4           #N:I
    :cond_8f
    and-int/lit8 v14, p2, 0x1

    #@91
    if-eqz v14, :cond_100

    #@93
    .line 311
    move-object/from16 v0, p0

    #@95
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@97
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@9a
    move-result v4

    #@9b
    .line 312
    .restart local v4       #N:I
    if-lez v4, :cond_100

    #@9d
    .line 313
    move/from16 v0, p2

    #@9f
    and-int/lit16 v14, v0, 0x200

    #@a1
    if-eqz v14, :cond_e2

    #@a3
    .line 314
    new-array v14, v4, [Landroid/content/pm/ActivityInfo;

    #@a5
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    #@a7
    .line 322
    :goto_a7
    const/4 v6, 0x0

    #@a8
    .local v6, i:I
    const/4 v7, 0x0

    #@a9
    .local v7, j:I
    move v8, v7

    #@aa
    .end local v7           #j:I
    .local v8, j:I
    :goto_aa
    if-ge v6, v4, :cond_100

    #@ac
    .line 323
    move-object/from16 v0, p0

    #@ae
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@b0
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b3
    move-result-object v5

    #@b4
    check-cast v5, Landroid/content/pm/PackageParser$Activity;

    #@b6
    .line 324
    .local v5, activity:Landroid/content/pm/PackageParser$Activity;
    iget-object v14, v5, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@b8
    iget-boolean v14, v14, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@ba
    if-nez v14, :cond_c2

    #@bc
    move/from16 v0, p2

    #@be
    and-int/lit16 v14, v0, 0x200

    #@c0
    if-eqz v14, :cond_32b

    #@c2
    .line 326
    :cond_c2
    iget-object v15, v11, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    #@c4
    add-int/lit8 v7, v8, 0x1

    #@c6
    .end local v8           #j:I
    .restart local v7       #j:I
    move-object/from16 v0, p0

    #@c8
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@ca
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@cd
    move-result-object v14

    #@ce
    check-cast v14, Landroid/content/pm/PackageParser$Activity;

    #@d0
    move/from16 v0, p2

    #@d2
    move-object/from16 v1, p8

    #@d4
    move/from16 v2, p9

    #@d6
    invoke-static {v14, v0, v1, v2}, Landroid/content/pm/PackageParser;->generateActivityInfo(Landroid/content/pm/PackageParser$Activity;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ActivityInfo;

    #@d9
    move-result-object v14

    #@da
    aput-object v14, v15, v8

    #@dc
    .line 322
    :goto_dc
    add-int/lit8 v6, v6, 0x1

    #@de
    move v8, v7

    #@df
    .end local v7           #j:I
    .restart local v8       #j:I
    goto :goto_aa

    #@e0
    .line 304
    .end local v5           #activity:Landroid/content/pm/PackageParser$Activity;
    .end local v6           #i:I
    .end local v8           #j:I
    :cond_e0
    const/4 v4, 0x0

    #@e1
    goto :goto_80

    #@e2
    .line 316
    :cond_e2
    const/4 v9, 0x0

    #@e3
    .line 317
    .local v9, num:I
    const/4 v6, 0x0

    #@e4
    .restart local v6       #i:I
    :goto_e4
    if-ge v6, v4, :cond_fb

    #@e6
    .line 318
    move-object/from16 v0, p0

    #@e8
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@ea
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ed
    move-result-object v14

    #@ee
    check-cast v14, Landroid/content/pm/PackageParser$Activity;

    #@f0
    iget-object v14, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@f2
    iget-boolean v14, v14, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@f4
    if-eqz v14, :cond_f8

    #@f6
    add-int/lit8 v9, v9, 0x1

    #@f8
    .line 317
    :cond_f8
    add-int/lit8 v6, v6, 0x1

    #@fa
    goto :goto_e4

    #@fb
    .line 320
    :cond_fb
    new-array v14, v9, [Landroid/content/pm/ActivityInfo;

    #@fd
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    #@ff
    goto :goto_a7

    #@100
    .line 332
    .end local v4           #N:I
    .end local v6           #i:I
    .end local v9           #num:I
    :cond_100
    and-int/lit8 v14, p2, 0x2

    #@102
    if-eqz v14, :cond_16f

    #@104
    .line 333
    move-object/from16 v0, p0

    #@106
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->receivers:Ljava/util/ArrayList;

    #@108
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@10b
    move-result v4

    #@10c
    .line 334
    .restart local v4       #N:I
    if-lez v4, :cond_16f

    #@10e
    .line 335
    move/from16 v0, p2

    #@110
    and-int/lit16 v14, v0, 0x200

    #@112
    if-eqz v14, :cond_151

    #@114
    .line 336
    new-array v14, v4, [Landroid/content/pm/ActivityInfo;

    #@116
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    #@118
    .line 344
    :goto_118
    const/4 v6, 0x0

    #@119
    .restart local v6       #i:I
    const/4 v7, 0x0

    #@11a
    .restart local v7       #j:I
    move v8, v7

    #@11b
    .end local v7           #j:I
    .restart local v8       #j:I
    :goto_11b
    if-ge v6, v4, :cond_16f

    #@11d
    .line 345
    move-object/from16 v0, p0

    #@11f
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->receivers:Ljava/util/ArrayList;

    #@121
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@124
    move-result-object v5

    #@125
    check-cast v5, Landroid/content/pm/PackageParser$Activity;

    #@127
    .line 346
    .restart local v5       #activity:Landroid/content/pm/PackageParser$Activity;
    iget-object v14, v5, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@129
    iget-boolean v14, v14, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@12b
    if-nez v14, :cond_133

    #@12d
    move/from16 v0, p2

    #@12f
    and-int/lit16 v14, v0, 0x200

    #@131
    if-eqz v14, :cond_328

    #@133
    .line 348
    :cond_133
    iget-object v15, v11, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    #@135
    add-int/lit8 v7, v8, 0x1

    #@137
    .end local v8           #j:I
    .restart local v7       #j:I
    move-object/from16 v0, p0

    #@139
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->receivers:Ljava/util/ArrayList;

    #@13b
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13e
    move-result-object v14

    #@13f
    check-cast v14, Landroid/content/pm/PackageParser$Activity;

    #@141
    move/from16 v0, p2

    #@143
    move-object/from16 v1, p8

    #@145
    move/from16 v2, p9

    #@147
    invoke-static {v14, v0, v1, v2}, Landroid/content/pm/PackageParser;->generateActivityInfo(Landroid/content/pm/PackageParser$Activity;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ActivityInfo;

    #@14a
    move-result-object v14

    #@14b
    aput-object v14, v15, v8

    #@14d
    .line 344
    :goto_14d
    add-int/lit8 v6, v6, 0x1

    #@14f
    move v8, v7

    #@150
    .end local v7           #j:I
    .restart local v8       #j:I
    goto :goto_11b

    #@151
    .line 338
    .end local v5           #activity:Landroid/content/pm/PackageParser$Activity;
    .end local v6           #i:I
    .end local v8           #j:I
    :cond_151
    const/4 v9, 0x0

    #@152
    .line 339
    .restart local v9       #num:I
    const/4 v6, 0x0

    #@153
    .restart local v6       #i:I
    :goto_153
    if-ge v6, v4, :cond_16a

    #@155
    .line 340
    move-object/from16 v0, p0

    #@157
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->receivers:Ljava/util/ArrayList;

    #@159
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15c
    move-result-object v14

    #@15d
    check-cast v14, Landroid/content/pm/PackageParser$Activity;

    #@15f
    iget-object v14, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@161
    iget-boolean v14, v14, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@163
    if-eqz v14, :cond_167

    #@165
    add-int/lit8 v9, v9, 0x1

    #@167
    .line 339
    :cond_167
    add-int/lit8 v6, v6, 0x1

    #@169
    goto :goto_153

    #@16a
    .line 342
    :cond_16a
    new-array v14, v9, [Landroid/content/pm/ActivityInfo;

    #@16c
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    #@16e
    goto :goto_118

    #@16f
    .line 354
    .end local v4           #N:I
    .end local v6           #i:I
    .end local v9           #num:I
    :cond_16f
    and-int/lit8 v14, p2, 0x4

    #@171
    if-eqz v14, :cond_1de

    #@173
    .line 355
    move-object/from16 v0, p0

    #@175
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->services:Ljava/util/ArrayList;

    #@177
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@17a
    move-result v4

    #@17b
    .line 356
    .restart local v4       #N:I
    if-lez v4, :cond_1de

    #@17d
    .line 357
    move/from16 v0, p2

    #@17f
    and-int/lit16 v14, v0, 0x200

    #@181
    if-eqz v14, :cond_1c0

    #@183
    .line 358
    new-array v14, v4, [Landroid/content/pm/ServiceInfo;

    #@185
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    #@187
    .line 366
    :goto_187
    const/4 v6, 0x0

    #@188
    .restart local v6       #i:I
    const/4 v7, 0x0

    #@189
    .restart local v7       #j:I
    move v8, v7

    #@18a
    .end local v7           #j:I
    .restart local v8       #j:I
    :goto_18a
    if-ge v6, v4, :cond_1de

    #@18c
    .line 367
    move-object/from16 v0, p0

    #@18e
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->services:Ljava/util/ArrayList;

    #@190
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@193
    move-result-object v13

    #@194
    check-cast v13, Landroid/content/pm/PackageParser$Service;

    #@196
    .line 368
    .local v13, service:Landroid/content/pm/PackageParser$Service;
    iget-object v14, v13, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@198
    iget-boolean v14, v14, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@19a
    if-nez v14, :cond_1a2

    #@19c
    move/from16 v0, p2

    #@19e
    and-int/lit16 v14, v0, 0x200

    #@1a0
    if-eqz v14, :cond_325

    #@1a2
    .line 370
    :cond_1a2
    iget-object v15, v11, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    #@1a4
    add-int/lit8 v7, v8, 0x1

    #@1a6
    .end local v8           #j:I
    .restart local v7       #j:I
    move-object/from16 v0, p0

    #@1a8
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->services:Ljava/util/ArrayList;

    #@1aa
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1ad
    move-result-object v14

    #@1ae
    check-cast v14, Landroid/content/pm/PackageParser$Service;

    #@1b0
    move/from16 v0, p2

    #@1b2
    move-object/from16 v1, p8

    #@1b4
    move/from16 v2, p9

    #@1b6
    invoke-static {v14, v0, v1, v2}, Landroid/content/pm/PackageParser;->generateServiceInfo(Landroid/content/pm/PackageParser$Service;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ServiceInfo;

    #@1b9
    move-result-object v14

    #@1ba
    aput-object v14, v15, v8

    #@1bc
    .line 366
    :goto_1bc
    add-int/lit8 v6, v6, 0x1

    #@1be
    move v8, v7

    #@1bf
    .end local v7           #j:I
    .restart local v8       #j:I
    goto :goto_18a

    #@1c0
    .line 360
    .end local v6           #i:I
    .end local v8           #j:I
    .end local v13           #service:Landroid/content/pm/PackageParser$Service;
    :cond_1c0
    const/4 v9, 0x0

    #@1c1
    .line 361
    .restart local v9       #num:I
    const/4 v6, 0x0

    #@1c2
    .restart local v6       #i:I
    :goto_1c2
    if-ge v6, v4, :cond_1d9

    #@1c4
    .line 362
    move-object/from16 v0, p0

    #@1c6
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->services:Ljava/util/ArrayList;

    #@1c8
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1cb
    move-result-object v14

    #@1cc
    check-cast v14, Landroid/content/pm/PackageParser$Service;

    #@1ce
    iget-object v14, v14, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@1d0
    iget-boolean v14, v14, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@1d2
    if-eqz v14, :cond_1d6

    #@1d4
    add-int/lit8 v9, v9, 0x1

    #@1d6
    .line 361
    :cond_1d6
    add-int/lit8 v6, v6, 0x1

    #@1d8
    goto :goto_1c2

    #@1d9
    .line 364
    :cond_1d9
    new-array v14, v9, [Landroid/content/pm/ServiceInfo;

    #@1db
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    #@1dd
    goto :goto_187

    #@1de
    .line 376
    .end local v4           #N:I
    .end local v6           #i:I
    .end local v9           #num:I
    :cond_1de
    and-int/lit8 v14, p2, 0x8

    #@1e0
    if-eqz v14, :cond_24d

    #@1e2
    .line 377
    move-object/from16 v0, p0

    #@1e4
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->providers:Ljava/util/ArrayList;

    #@1e6
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@1e9
    move-result v4

    #@1ea
    .line 378
    .restart local v4       #N:I
    if-lez v4, :cond_24d

    #@1ec
    .line 379
    move/from16 v0, p2

    #@1ee
    and-int/lit16 v14, v0, 0x200

    #@1f0
    if-eqz v14, :cond_22f

    #@1f2
    .line 380
    new-array v14, v4, [Landroid/content/pm/ProviderInfo;

    #@1f4
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->providers:[Landroid/content/pm/ProviderInfo;

    #@1f6
    .line 388
    :goto_1f6
    const/4 v6, 0x0

    #@1f7
    .restart local v6       #i:I
    const/4 v7, 0x0

    #@1f8
    .restart local v7       #j:I
    move v8, v7

    #@1f9
    .end local v7           #j:I
    .restart local v8       #j:I
    :goto_1f9
    if-ge v6, v4, :cond_24d

    #@1fb
    .line 389
    move-object/from16 v0, p0

    #@1fd
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->providers:Ljava/util/ArrayList;

    #@1ff
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@202
    move-result-object v12

    #@203
    check-cast v12, Landroid/content/pm/PackageParser$Provider;

    #@205
    .line 390
    .local v12, provider:Landroid/content/pm/PackageParser$Provider;
    iget-object v14, v12, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@207
    iget-boolean v14, v14, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@209
    if-nez v14, :cond_211

    #@20b
    move/from16 v0, p2

    #@20d
    and-int/lit16 v14, v0, 0x200

    #@20f
    if-eqz v14, :cond_322

    #@211
    .line 392
    :cond_211
    iget-object v15, v11, Landroid/content/pm/PackageInfo;->providers:[Landroid/content/pm/ProviderInfo;

    #@213
    add-int/lit8 v7, v8, 0x1

    #@215
    .end local v8           #j:I
    .restart local v7       #j:I
    move-object/from16 v0, p0

    #@217
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->providers:Ljava/util/ArrayList;

    #@219
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21c
    move-result-object v14

    #@21d
    check-cast v14, Landroid/content/pm/PackageParser$Provider;

    #@21f
    move/from16 v0, p2

    #@221
    move-object/from16 v1, p8

    #@223
    move/from16 v2, p9

    #@225
    invoke-static {v14, v0, v1, v2}, Landroid/content/pm/PackageParser;->generateProviderInfo(Landroid/content/pm/PackageParser$Provider;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ProviderInfo;

    #@228
    move-result-object v14

    #@229
    aput-object v14, v15, v8

    #@22b
    .line 388
    :goto_22b
    add-int/lit8 v6, v6, 0x1

    #@22d
    move v8, v7

    #@22e
    .end local v7           #j:I
    .restart local v8       #j:I
    goto :goto_1f9

    #@22f
    .line 382
    .end local v6           #i:I
    .end local v8           #j:I
    .end local v12           #provider:Landroid/content/pm/PackageParser$Provider;
    :cond_22f
    const/4 v9, 0x0

    #@230
    .line 383
    .restart local v9       #num:I
    const/4 v6, 0x0

    #@231
    .restart local v6       #i:I
    :goto_231
    if-ge v6, v4, :cond_248

    #@233
    .line 384
    move-object/from16 v0, p0

    #@235
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->providers:Ljava/util/ArrayList;

    #@237
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@23a
    move-result-object v14

    #@23b
    check-cast v14, Landroid/content/pm/PackageParser$Provider;

    #@23d
    iget-object v14, v14, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@23f
    iget-boolean v14, v14, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@241
    if-eqz v14, :cond_245

    #@243
    add-int/lit8 v9, v9, 0x1

    #@245
    .line 383
    :cond_245
    add-int/lit8 v6, v6, 0x1

    #@247
    goto :goto_231

    #@248
    .line 386
    :cond_248
    new-array v14, v9, [Landroid/content/pm/ProviderInfo;

    #@24a
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->providers:[Landroid/content/pm/ProviderInfo;

    #@24c
    goto :goto_1f6

    #@24d
    .line 398
    .end local v4           #N:I
    .end local v6           #i:I
    .end local v9           #num:I
    :cond_24d
    and-int/lit8 v14, p2, 0x10

    #@24f
    if-eqz v14, :cond_279

    #@251
    .line 399
    move-object/from16 v0, p0

    #@253
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->instrumentation:Ljava/util/ArrayList;

    #@255
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@258
    move-result v4

    #@259
    .line 400
    .restart local v4       #N:I
    if-lez v4, :cond_279

    #@25b
    .line 401
    new-array v14, v4, [Landroid/content/pm/InstrumentationInfo;

    #@25d
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->instrumentation:[Landroid/content/pm/InstrumentationInfo;

    #@25f
    .line 402
    const/4 v6, 0x0

    #@260
    .restart local v6       #i:I
    :goto_260
    if-ge v6, v4, :cond_279

    #@262
    .line 403
    iget-object v15, v11, Landroid/content/pm/PackageInfo;->instrumentation:[Landroid/content/pm/InstrumentationInfo;

    #@264
    move-object/from16 v0, p0

    #@266
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->instrumentation:Ljava/util/ArrayList;

    #@268
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26b
    move-result-object v14

    #@26c
    check-cast v14, Landroid/content/pm/PackageParser$Instrumentation;

    #@26e
    move/from16 v0, p2

    #@270
    invoke-static {v14, v0}, Landroid/content/pm/PackageParser;->generateInstrumentationInfo(Landroid/content/pm/PackageParser$Instrumentation;I)Landroid/content/pm/InstrumentationInfo;

    #@273
    move-result-object v14

    #@274
    aput-object v14, v15, v6

    #@276
    .line 402
    add-int/lit8 v6, v6, 0x1

    #@278
    goto :goto_260

    #@279
    .line 408
    .end local v4           #N:I
    .end local v6           #i:I
    :cond_279
    move/from16 v0, p2

    #@27b
    and-int/lit16 v14, v0, 0x1000

    #@27d
    if-eqz v14, :cond_2f7

    #@27f
    .line 409
    move-object/from16 v0, p0

    #@281
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->permissions:Ljava/util/ArrayList;

    #@283
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@286
    move-result v4

    #@287
    .line 410
    .restart local v4       #N:I
    if-lez v4, :cond_2a7

    #@289
    .line 411
    new-array v14, v4, [Landroid/content/pm/PermissionInfo;

    #@28b
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    #@28d
    .line 412
    const/4 v6, 0x0

    #@28e
    .restart local v6       #i:I
    :goto_28e
    if-ge v6, v4, :cond_2a7

    #@290
    .line 413
    iget-object v15, v11, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    #@292
    move-object/from16 v0, p0

    #@294
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->permissions:Ljava/util/ArrayList;

    #@296
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@299
    move-result-object v14

    #@29a
    check-cast v14, Landroid/content/pm/PackageParser$Permission;

    #@29c
    move/from16 v0, p2

    #@29e
    invoke-static {v14, v0}, Landroid/content/pm/PackageParser;->generatePermissionInfo(Landroid/content/pm/PackageParser$Permission;I)Landroid/content/pm/PermissionInfo;

    #@2a1
    move-result-object v14

    #@2a2
    aput-object v14, v15, v6

    #@2a4
    .line 412
    add-int/lit8 v6, v6, 0x1

    #@2a6
    goto :goto_28e

    #@2a7
    .line 416
    .end local v6           #i:I
    :cond_2a7
    move-object/from16 v0, p0

    #@2a9
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@2ab
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@2ae
    move-result v4

    #@2af
    .line 417
    if-lez v4, :cond_2f7

    #@2b1
    .line 418
    new-array v14, v4, [Ljava/lang/String;

    #@2b3
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    #@2b5
    .line 419
    new-array v14, v4, [I

    #@2b7
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->requestedPermissionsFlags:[I

    #@2b9
    .line 420
    const/4 v6, 0x0

    #@2ba
    .restart local v6       #i:I
    :goto_2ba
    if-ge v6, v4, :cond_2f7

    #@2bc
    .line 421
    move-object/from16 v0, p0

    #@2be
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@2c0
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2c3
    move-result-object v10

    #@2c4
    check-cast v10, Ljava/lang/String;

    #@2c6
    .line 422
    .local v10, perm:Ljava/lang/String;
    iget-object v14, v11, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    #@2c8
    aput-object v10, v14, v6

    #@2ca
    .line 423
    move-object/from16 v0, p0

    #@2cc
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->requestedPermissionsRequired:Ljava/util/ArrayList;

    #@2ce
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d1
    move-result-object v14

    #@2d2
    check-cast v14, Ljava/lang/Boolean;

    #@2d4
    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    #@2d7
    move-result v14

    #@2d8
    if-eqz v14, :cond_2e2

    #@2da
    .line 424
    iget-object v14, v11, Landroid/content/pm/PackageInfo;->requestedPermissionsFlags:[I

    #@2dc
    aget v15, v14, v6

    #@2de
    or-int/lit8 v15, v15, 0x1

    #@2e0
    aput v15, v14, v6

    #@2e2
    .line 426
    :cond_2e2
    if-eqz p7, :cond_2f4

    #@2e4
    move-object/from16 v0, p7

    #@2e6
    invoke-virtual {v0, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@2e9
    move-result v14

    #@2ea
    if-eqz v14, :cond_2f4

    #@2ec
    .line 427
    iget-object v14, v11, Landroid/content/pm/PackageInfo;->requestedPermissionsFlags:[I

    #@2ee
    aget v15, v14, v6

    #@2f0
    or-int/lit8 v15, v15, 0x2

    #@2f2
    aput v15, v14, v6

    #@2f4
    .line 420
    :cond_2f4
    add-int/lit8 v6, v6, 0x1

    #@2f6
    goto :goto_2ba

    #@2f7
    .line 432
    .end local v4           #N:I
    .end local v6           #i:I
    .end local v10           #perm:Ljava/lang/String;
    :cond_2f7
    and-int/lit8 v14, p2, 0x40

    #@2f9
    if-eqz v14, :cond_b

    #@2fb
    .line 433
    move-object/from16 v0, p0

    #@2fd
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    #@2ff
    if-eqz v14, :cond_320

    #@301
    move-object/from16 v0, p0

    #@303
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    #@305
    array-length v4, v14

    #@306
    .line 434
    .restart local v4       #N:I
    :goto_306
    if-lez v4, :cond_b

    #@308
    .line 435
    new-array v14, v4, [Landroid/content/pm/Signature;

    #@30a
    iput-object v14, v11, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@30c
    .line 436
    move-object/from16 v0, p0

    #@30e
    iget-object v14, v0, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    #@310
    const/4 v15, 0x0

    #@311
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@313
    move-object/from16 v16, v0

    #@315
    const/16 v17, 0x0

    #@317
    move-object/from16 v0, v16

    #@319
    move/from16 v1, v17

    #@31b
    invoke-static {v14, v15, v0, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@31e
    goto/16 :goto_b

    #@320
    .line 433
    .end local v4           #N:I
    :cond_320
    const/4 v4, 0x0

    #@321
    goto :goto_306

    #@322
    .restart local v4       #N:I
    .restart local v6       #i:I
    .restart local v8       #j:I
    .restart local v12       #provider:Landroid/content/pm/PackageParser$Provider;
    :cond_322
    move v7, v8

    #@323
    .end local v8           #j:I
    .restart local v7       #j:I
    goto/16 :goto_22b

    #@325
    .end local v7           #j:I
    .end local v12           #provider:Landroid/content/pm/PackageParser$Provider;
    .restart local v8       #j:I
    .restart local v13       #service:Landroid/content/pm/PackageParser$Service;
    :cond_325
    move v7, v8

    #@326
    .end local v8           #j:I
    .restart local v7       #j:I
    goto/16 :goto_1bc

    #@328
    .end local v7           #j:I
    .end local v13           #service:Landroid/content/pm/PackageParser$Service;
    .restart local v5       #activity:Landroid/content/pm/PackageParser$Activity;
    .restart local v8       #j:I
    :cond_328
    move v7, v8

    #@329
    .end local v8           #j:I
    .restart local v7       #j:I
    goto/16 :goto_14d

    #@32b
    .end local v7           #j:I
    .restart local v8       #j:I
    :cond_32b
    move v7, v8

    #@32c
    .end local v8           #j:I
    .restart local v7       #j:I
    goto/16 :goto_dc
.end method

.method public static final generatePermissionGroupInfo(Landroid/content/pm/PackageParser$PermissionGroup;I)Landroid/content/pm/PermissionGroupInfo;
    .registers 4
    .parameter "pg"
    .parameter "flags"

    #@0
    .prologue
    .line 3705
    if-nez p0, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    .line 3711
    :goto_3
    return-object v0

    #@4
    .line 3706
    :cond_4
    and-int/lit16 v1, p1, 0x80

    #@6
    if-nez v1, :cond_b

    #@8
    .line 3707
    iget-object v0, p0, Landroid/content/pm/PackageParser$PermissionGroup;->info:Landroid/content/pm/PermissionGroupInfo;

    #@a
    goto :goto_3

    #@b
    .line 3709
    :cond_b
    new-instance v0, Landroid/content/pm/PermissionGroupInfo;

    #@d
    iget-object v1, p0, Landroid/content/pm/PackageParser$PermissionGroup;->info:Landroid/content/pm/PermissionGroupInfo;

    #@f
    invoke-direct {v0, v1}, Landroid/content/pm/PermissionGroupInfo;-><init>(Landroid/content/pm/PermissionGroupInfo;)V

    #@12
    .line 3710
    .local v0, pgi:Landroid/content/pm/PermissionGroupInfo;
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@14
    iput-object v1, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@16
    goto :goto_3
.end method

.method public static final generatePermissionInfo(Landroid/content/pm/PackageParser$Permission;I)Landroid/content/pm/PermissionInfo;
    .registers 4
    .parameter "p"
    .parameter "flags"

    #@0
    .prologue
    .line 3694
    if-nez p0, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    .line 3700
    :goto_3
    return-object v0

    #@4
    .line 3695
    :cond_4
    and-int/lit16 v1, p1, 0x80

    #@6
    if-nez v1, :cond_b

    #@8
    .line 3696
    iget-object v0, p0, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@a
    goto :goto_3

    #@b
    .line 3698
    :cond_b
    new-instance v0, Landroid/content/pm/PermissionInfo;

    #@d
    iget-object v1, p0, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@f
    invoke-direct {v0, v1}, Landroid/content/pm/PermissionInfo;-><init>(Landroid/content/pm/PermissionInfo;)V

    #@12
    .line 3699
    .local v0, pi:Landroid/content/pm/PermissionInfo;
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@14
    iput-object v1, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@16
    goto :goto_3
.end method

.method public static final generateProviderInfo(Landroid/content/pm/PackageParser$Provider;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ProviderInfo;
    .registers 8
    .parameter "p"
    .parameter "flags"
    .parameter "state"
    .parameter "userId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3819
    if-nez p0, :cond_5

    #@3
    move-object v0, v1

    #@4
    .line 3835
    :goto_4
    return-object v0

    #@5
    .line 3820
    :cond_5
    invoke-static {p1, p2}, Landroid/content/pm/PackageParser;->checkUseInstalled(ILandroid/content/pm/PackageUserState;)Z

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_d

    #@b
    move-object v0, v1

    #@c
    .line 3821
    goto :goto_4

    #@d
    .line 3823
    :cond_d
    iget-object v2, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@f
    iget-object v3, p0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@11
    invoke-static {p1, v2, p2, v3, p3}, Landroid/content/pm/PackageParser;->copyNeeded(ILandroid/content/pm/PackageParser$Package;Landroid/content/pm/PackageUserState;Landroid/os/Bundle;I)Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_24

    #@17
    and-int/lit16 v2, p1, 0x800

    #@19
    if-nez v2, :cond_21

    #@1b
    iget-object v2, p0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@1d
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@1f
    if-nez v2, :cond_24

    #@21
    .line 3826
    :cond_21
    iget-object v0, p0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@23
    goto :goto_4

    #@24
    .line 3829
    :cond_24
    new-instance v0, Landroid/content/pm/ProviderInfo;

    #@26
    iget-object v2, p0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@28
    invoke-direct {v0, v2}, Landroid/content/pm/ProviderInfo;-><init>(Landroid/content/pm/ProviderInfo;)V

    #@2b
    .line 3830
    .local v0, pi:Landroid/content/pm/ProviderInfo;
    iget-object v2, p0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@2d
    iput-object v2, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@2f
    .line 3831
    and-int/lit16 v2, p1, 0x800

    #@31
    if-nez v2, :cond_35

    #@33
    .line 3832
    iput-object v1, v0, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@35
    .line 3834
    :cond_35
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@37
    invoke-static {v1, p1, p2, p3}, Landroid/content/pm/PackageParser;->generateApplicationInfo(Landroid/content/pm/PackageParser$Package;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ApplicationInfo;

    #@3a
    move-result-object v1

    #@3b
    iput-object v1, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3d
    goto :goto_4
.end method

.method public static final generateServiceInfo(Landroid/content/pm/PackageParser$Service;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ServiceInfo;
    .registers 7
    .parameter "s"
    .parameter "flags"
    .parameter "state"
    .parameter "userId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3774
    if-nez p0, :cond_4

    #@3
    .line 3785
    :cond_3
    :goto_3
    return-object v0

    #@4
    .line 3775
    :cond_4
    invoke-static {p1, p2}, Landroid/content/pm/PackageParser;->checkUseInstalled(ILandroid/content/pm/PackageUserState;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_3

    #@a
    .line 3778
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@c
    iget-object v2, p0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@e
    invoke-static {p1, v1, p2, v2, p3}, Landroid/content/pm/PackageParser;->copyNeeded(ILandroid/content/pm/PackageParser$Package;Landroid/content/pm/PackageUserState;Landroid/os/Bundle;I)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_17

    #@14
    .line 3779
    iget-object v0, p0, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@16
    goto :goto_3

    #@17
    .line 3782
    :cond_17
    new-instance v0, Landroid/content/pm/ServiceInfo;

    #@19
    iget-object v1, p0, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@1b
    invoke-direct {v0, v1}, Landroid/content/pm/ServiceInfo;-><init>(Landroid/content/pm/ServiceInfo;)V

    #@1e
    .line 3783
    .local v0, si:Landroid/content/pm/ServiceInfo;
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@20
    iput-object v1, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@22
    .line 3784
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@24
    invoke-static {v1, p1, p2, p3}, Landroid/content/pm/PackageParser;->generateApplicationInfo(Landroid/content/pm/PackageParser$Package;ILandroid/content/pm/PackageUserState;I)Landroid/content/pm/ApplicationInfo;

    #@27
    move-result-object v1

    #@28
    iput-object v1, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2a
    goto :goto_3
.end method

.method private static final isPackageFilename(Ljava/lang/String;)Z
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 247
    const-string v0, ".apk"

    #@2
    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private loadCertificates(Ljava/util/jar/JarFile;Ljava/util/jar/JarEntry;[B)[Ljava/security/cert/Certificate;
    .registers 10
    .parameter "jarFile"
    .parameter "je"
    .parameter "readBuffer"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 447
    :try_start_1
    new-instance v1, Ljava/io/BufferedInputStream;

    #@3
    invoke-virtual {p1, p2}, Ljava/util/jar/JarFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    #@6
    move-result-object v3

    #@7
    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@a
    .line 448
    .local v1, is:Ljava/io/InputStream;
    :cond_a
    const/4 v3, 0x0

    #@b
    array-length v4, p3

    #@c
    invoke-virtual {v1, p3, v3, v4}, Ljava/io/FilterInputStream;->read([BII)I

    #@f
    move-result v3

    #@10
    const/4 v4, -0x1

    #@11
    if-ne v3, v4, :cond_a

    #@13
    .line 451
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@16
    .line 452
    if-eqz p2, :cond_1c

    #@18
    invoke-virtual {p2}, Ljava/util/jar/JarEntry;->getCertificates()[Ljava/security/cert/Certificate;
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1b} :catch_1d
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1b} :catch_49

    #@1b
    move-result-object v2

    #@1c
    .line 460
    .end local v1           #is:Ljava/io/InputStream;
    :cond_1c
    :goto_1c
    return-object v2

    #@1d
    .line 453
    :catch_1d
    move-exception v0

    #@1e
    .line 454
    .local v0, e:Ljava/io/IOException;
    const-string v3, "PackageParser"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "Exception reading "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {p2}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, " in "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {p1}, Ljava/util/jar/JarFile;->getName()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    goto :goto_1c

    #@49
    .line 456
    .end local v0           #e:Ljava/io/IOException;
    :catch_49
    move-exception v0

    #@4a
    .line 457
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v3, "PackageParser"

    #@4c
    new-instance v4, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v5, "Exception reading "

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {p2}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    const-string v5, " in "

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {p1}, Ljava/util/jar/JarFile;->getName()Ljava/lang/String;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v4

    #@6d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v4

    #@71
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@74
    goto :goto_1c
.end method

.method private parseActivity(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;ZZ)Landroid/content/pm/PackageParser$Activity;
    .registers 34
    .parameter "owner"
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "flags"
    .parameter "outError"
    .parameter "receiver"
    .parameter "hardwareAccelerated"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2113
    sget-object v2, Lcom/android/internal/R$styleable;->AndroidManifestActivity:[I

    #@2
    move-object/from16 v0, p2

    #@4
    move-object/from16 v1, p4

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v20

    #@a
    .line 2116
    .local v20, sa:Landroid/content/res/TypedArray;
    move-object/from16 v0, p0

    #@c
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@e
    if-nez v2, :cond_2a

    #@10
    .line 2117
    new-instance v2, Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@12
    const/4 v5, 0x3

    #@13
    const/4 v6, 0x1

    #@14
    const/4 v7, 0x2

    #@15
    const/16 v8, 0x17

    #@17
    move-object/from16 v0, p0

    #@19
    iget-object v9, v0, Landroid/content/pm/PackageParser;->mSeparateProcesses:[Ljava/lang/String;

    #@1b
    const/4 v10, 0x7

    #@1c
    const/16 v11, 0x11

    #@1e
    const/4 v12, 0x5

    #@1f
    move-object/from16 v3, p1

    #@21
    move-object/from16 v4, p6

    #@23
    invoke-direct/range {v2 .. v12}, Landroid/content/pm/PackageParser$ParseComponentArgs;-><init>(Landroid/content/pm/PackageParser$Package;[Ljava/lang/String;IIII[Ljava/lang/String;III)V

    #@26
    move-object/from16 v0, p0

    #@28
    iput-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@2a
    .line 2128
    :cond_2a
    move-object/from16 v0, p0

    #@2c
    iget-object v3, v0, Landroid/content/pm/PackageParser;->mParseActivityArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@2e
    if-eqz p7, :cond_5c

    #@30
    const-string v2, "<receiver>"

    #@32
    :goto_32
    iput-object v2, v3, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->tag:Ljava/lang/String;

    #@34
    .line 2129
    move-object/from16 v0, p0

    #@36
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@38
    move-object/from16 v0, v20

    #@3a
    iput-object v0, v2, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@3c
    .line 2130
    move-object/from16 v0, p0

    #@3e
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@40
    move/from16 v0, p5

    #@42
    iput v0, v2, Landroid/content/pm/PackageParser$ParseComponentArgs;->flags:I

    #@44
    .line 2132
    new-instance v14, Landroid/content/pm/PackageParser$Activity;

    #@46
    move-object/from16 v0, p0

    #@48
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@4a
    new-instance v3, Landroid/content/pm/ActivityInfo;

    #@4c
    invoke-direct {v3}, Landroid/content/pm/ActivityInfo;-><init>()V

    #@4f
    invoke-direct {v14, v2, v3}, Landroid/content/pm/PackageParser$Activity;-><init>(Landroid/content/pm/PackageParser$ParseComponentArgs;Landroid/content/pm/ActivityInfo;)V

    #@52
    .line 2133
    .local v14, a:Landroid/content/pm/PackageParser$Activity;
    const/4 v2, 0x0

    #@53
    aget-object v2, p6, v2

    #@55
    if-eqz v2, :cond_5f

    #@57
    .line 2134
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/TypedArray;->recycle()V

    #@5a
    .line 2135
    const/4 v14, 0x0

    #@5b
    .line 2383
    .end local v14           #a:Landroid/content/pm/PackageParser$Activity;
    :cond_5b
    :goto_5b
    return-object v14

    #@5c
    .line 2128
    :cond_5c
    const-string v2, "<activity>"

    #@5e
    goto :goto_32

    #@5f
    .line 2138
    .restart local v14       #a:Landroid/content/pm/PackageParser$Activity;
    :cond_5f
    const/4 v2, 0x6

    #@60
    move-object/from16 v0, v20

    #@62
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@65
    move-result v21

    #@66
    .line 2140
    .local v21, setExported:Z
    if-eqz v21, :cond_74

    #@68
    .line 2141
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@6a
    const/4 v3, 0x6

    #@6b
    const/4 v4, 0x0

    #@6c
    move-object/from16 v0, v20

    #@6e
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@71
    move-result v3

    #@72
    iput-boolean v3, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@74
    .line 2145
    :cond_74
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@76
    const/4 v3, 0x0

    #@77
    const/4 v4, 0x0

    #@78
    move-object/from16 v0, v20

    #@7a
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@7d
    move-result v3

    #@7e
    iput v3, v2, Landroid/content/pm/ActivityInfo;->theme:I

    #@80
    .line 2148
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@82
    const/16 v3, 0x1a

    #@84
    iget-object v4, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@86
    iget-object v4, v4, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@88
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@8a
    move-object/from16 v0, v20

    #@8c
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@8f
    move-result v3

    #@90
    iput v3, v2, Landroid/content/pm/ActivityInfo;->uiOptions:I

    #@92
    .line 2152
    const/16 v2, 0x1b

    #@94
    const/4 v3, 0x0

    #@95
    move-object/from16 v0, v20

    #@97
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@9a
    move-result-object v19

    #@9b
    .line 2154
    .local v19, parentName:Ljava/lang/String;
    if-eqz v19, :cond_b4

    #@9d
    .line 2155
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@9f
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@a1
    move-object/from16 v0, v19

    #@a3
    move-object/from16 v1, p6

    #@a5
    invoke-static {v2, v0, v1}, Landroid/content/pm/PackageParser;->buildClassName(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@a8
    move-result-object v18

    #@a9
    .line 2156
    .local v18, parentClassName:Ljava/lang/String;
    const/4 v2, 0x0

    #@aa
    aget-object v2, p6, v2

    #@ac
    if-nez v2, :cond_2a7

    #@ae
    .line 2157
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@b0
    move-object/from16 v0, v18

    #@b2
    iput-object v0, v2, Landroid/content/pm/ActivityInfo;->parentActivityName:Ljava/lang/String;

    #@b4
    .line 2166
    .end local v18           #parentClassName:Ljava/lang/String;
    :cond_b4
    :goto_b4
    const/4 v2, 0x4

    #@b5
    const/4 v3, 0x0

    #@b6
    move-object/from16 v0, v20

    #@b8
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@bb
    move-result-object v22

    #@bc
    .line 2168
    .local v22, str:Ljava/lang/String;
    if-nez v22, :cond_2d5

    #@be
    .line 2169
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@c0
    move-object/from16 v0, p1

    #@c2
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@c4
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@c6
    iput-object v3, v2, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    #@c8
    .line 2174
    :goto_c8
    const/16 v2, 0x8

    #@ca
    const/4 v3, 0x0

    #@cb
    move-object/from16 v0, v20

    #@cd
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@d0
    move-result-object v22

    #@d1
    .line 2176
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@d3
    move-object/from16 v0, p1

    #@d5
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@d7
    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@d9
    move-object/from16 v0, p1

    #@db
    iget-object v4, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@dd
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->taskAffinity:Ljava/lang/String;

    #@df
    move-object/from16 v0, v22

    #@e1
    move-object/from16 v1, p6

    #@e3
    invoke-static {v3, v4, v0, v1}, Landroid/content/pm/PackageParser;->buildTaskAffinityName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@e6
    move-result-object v3

    #@e7
    iput-object v3, v2, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@e9
    .line 2179
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@eb
    const/4 v3, 0x0

    #@ec
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@ee
    .line 2180
    const/16 v2, 0x9

    #@f0
    const/4 v3, 0x0

    #@f1
    move-object/from16 v0, v20

    #@f3
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@f6
    move-result v2

    #@f7
    if-eqz v2, :cond_101

    #@f9
    .line 2183
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@fb
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@fd
    or-int/lit8 v3, v3, 0x1

    #@ff
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@101
    .line 2186
    :cond_101
    const/16 v2, 0xa

    #@103
    const/4 v3, 0x0

    #@104
    move-object/from16 v0, v20

    #@106
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@109
    move-result v2

    #@10a
    if-eqz v2, :cond_114

    #@10c
    .line 2189
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@10e
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@110
    or-int/lit8 v3, v3, 0x2

    #@112
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@114
    .line 2192
    :cond_114
    const/16 v2, 0xb

    #@116
    const/4 v3, 0x0

    #@117
    move-object/from16 v0, v20

    #@119
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@11c
    move-result v2

    #@11d
    if-eqz v2, :cond_127

    #@11f
    .line 2195
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@121
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@123
    or-int/lit8 v3, v3, 0x4

    #@125
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@127
    .line 2198
    :cond_127
    const/16 v2, 0x15

    #@129
    const/4 v3, 0x0

    #@12a
    move-object/from16 v0, v20

    #@12c
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@12f
    move-result v2

    #@130
    if-eqz v2, :cond_13a

    #@132
    .line 2201
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@134
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@136
    or-int/lit16 v3, v3, 0x80

    #@138
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@13a
    .line 2204
    :cond_13a
    const/16 v2, 0x12

    #@13c
    const/4 v3, 0x0

    #@13d
    move-object/from16 v0, v20

    #@13f
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@142
    move-result v2

    #@143
    if-eqz v2, :cond_14d

    #@145
    .line 2207
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@147
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@149
    or-int/lit8 v3, v3, 0x8

    #@14b
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@14d
    .line 2210
    :cond_14d
    const/16 v2, 0xc

    #@14f
    const/4 v3, 0x0

    #@150
    move-object/from16 v0, v20

    #@152
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@155
    move-result v2

    #@156
    if-eqz v2, :cond_160

    #@158
    .line 2213
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@15a
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@15c
    or-int/lit8 v3, v3, 0x10

    #@15e
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@160
    .line 2216
    :cond_160
    const/16 v2, 0xd

    #@162
    const/4 v3, 0x0

    #@163
    move-object/from16 v0, v20

    #@165
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@168
    move-result v2

    #@169
    if-eqz v2, :cond_173

    #@16b
    .line 2219
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@16d
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@16f
    or-int/lit8 v3, v3, 0x20

    #@171
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@173
    .line 2222
    :cond_173
    const/16 v3, 0x13

    #@175
    move-object/from16 v0, p1

    #@177
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@179
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@17b
    and-int/lit8 v2, v2, 0x20

    #@17d
    if-eqz v2, :cond_2eb

    #@17f
    const/4 v2, 0x1

    #@180
    :goto_180
    move-object/from16 v0, v20

    #@182
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@185
    move-result v2

    #@186
    if-eqz v2, :cond_190

    #@188
    .line 2225
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@18a
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@18c
    or-int/lit8 v3, v3, 0x40

    #@18e
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@190
    .line 2228
    :cond_190
    const/16 v2, 0x16

    #@192
    const/4 v3, 0x0

    #@193
    move-object/from16 v0, v20

    #@195
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@198
    move-result v2

    #@199
    if-eqz v2, :cond_1a3

    #@19b
    .line 2231
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@19d
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@19f
    or-int/lit16 v3, v3, 0x100

    #@1a1
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@1a3
    .line 2234
    :cond_1a3
    const/16 v2, 0x1d

    #@1a5
    const/4 v3, 0x0

    #@1a6
    move-object/from16 v0, v20

    #@1a8
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1ab
    move-result v2

    #@1ac
    if-eqz v2, :cond_1b6

    #@1ae
    .line 2237
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@1b0
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@1b2
    or-int/lit16 v3, v3, 0x400

    #@1b4
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@1b6
    .line 2240
    :cond_1b6
    const/16 v2, 0x18

    #@1b8
    const/4 v3, 0x0

    #@1b9
    move-object/from16 v0, v20

    #@1bb
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1be
    move-result v2

    #@1bf
    if-eqz v2, :cond_1c9

    #@1c1
    .line 2243
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@1c3
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@1c5
    or-int/lit16 v3, v3, 0x800

    #@1c7
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@1c9
    .line 2246
    :cond_1c9
    if-nez p7, :cond_2ee

    #@1cb
    .line 2247
    const/16 v2, 0x19

    #@1cd
    move-object/from16 v0, v20

    #@1cf
    move/from16 v1, p8

    #@1d1
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1d4
    move-result v2

    #@1d5
    if-eqz v2, :cond_1df

    #@1d7
    .line 2250
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@1d9
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@1db
    or-int/lit16 v3, v3, 0x200

    #@1dd
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@1df
    .line 2253
    :cond_1df
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@1e1
    const/16 v3, 0xe

    #@1e3
    const/4 v4, 0x0

    #@1e4
    move-object/from16 v0, v20

    #@1e6
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@1e9
    move-result v3

    #@1ea
    iput v3, v2, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@1ec
    .line 2256
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@1ee
    const/16 v3, 0xf

    #@1f0
    const/4 v4, -0x1

    #@1f1
    move-object/from16 v0, v20

    #@1f3
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@1f6
    move-result v3

    #@1f7
    iput v3, v2, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    #@1f9
    .line 2259
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@1fb
    const/16 v3, 0x10

    #@1fd
    const/4 v4, 0x0

    #@1fe
    move-object/from16 v0, v20

    #@200
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@203
    move-result v3

    #@204
    iput v3, v2, Landroid/content/pm/ActivityInfo;->configChanges:I

    #@206
    .line 2262
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@208
    const/16 v3, 0x14

    #@20a
    const/4 v4, 0x0

    #@20b
    move-object/from16 v0, v20

    #@20d
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@210
    move-result v3

    #@211
    iput v3, v2, Landroid/content/pm/ActivityInfo;->softInputMode:I

    #@213
    .line 2270
    :goto_213
    if-eqz p7, :cond_280

    #@215
    .line 2271
    const/16 v2, 0x1c

    #@217
    const/4 v3, 0x0

    #@218
    move-object/from16 v0, v20

    #@21a
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@21d
    move-result v2

    #@21e
    if-eqz v2, :cond_26c

    #@220
    .line 2274
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@222
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@224
    const/high16 v4, 0x4000

    #@226
    or-int/2addr v3, v4

    #@227
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@229
    .line 2275
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@22b
    iget-boolean v2, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@22d
    if-eqz v2, :cond_26a

    #@22f
    .line 2276
    const-string v2, "PackageParser"

    #@231
    new-instance v3, Ljava/lang/StringBuilder;

    #@233
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@236
    const-string v4, "Activity exported request ignored due to singleUser: "

    #@238
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23b
    move-result-object v3

    #@23c
    iget-object v4, v14, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@23e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@241
    move-result-object v3

    #@242
    const-string v4, " at "

    #@244
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@247
    move-result-object v3

    #@248
    move-object/from16 v0, p0

    #@24a
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@24c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24f
    move-result-object v3

    #@250
    const-string v4, " "

    #@252
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@255
    move-result-object v3

    #@256
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@259
    move-result-object v4

    #@25a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25d
    move-result-object v3

    #@25e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@261
    move-result-object v3

    #@262
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@265
    .line 2279
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@267
    const/4 v3, 0x0

    #@268
    iput-boolean v3, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@26a
    .line 2281
    :cond_26a
    const/16 v21, 0x1

    #@26c
    .line 2283
    :cond_26c
    const/16 v2, 0x1e

    #@26e
    const/4 v3, 0x0

    #@26f
    move-object/from16 v0, v20

    #@271
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@274
    move-result v2

    #@275
    if-eqz v2, :cond_280

    #@277
    .line 2286
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@279
    iget v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@27b
    const/high16 v4, 0x2000

    #@27d
    or-int/2addr v3, v4

    #@27e
    iput v3, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@280
    .line 2290
    :cond_280
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/TypedArray;->recycle()V

    #@283
    .line 2292
    if-eqz p7, :cond_29f

    #@285
    move-object/from16 v0, p1

    #@287
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@289
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@28b
    const/high16 v3, 0x1000

    #@28d
    and-int/2addr v2, v3

    #@28e
    if-eqz v2, :cond_29f

    #@290
    .line 2295
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@292
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@294
    move-object/from16 v0, p1

    #@296
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@298
    if-ne v2, v3, :cond_29f

    #@29a
    .line 2296
    const/4 v2, 0x0

    #@29b
    const-string v3, "Heavy-weight applications can not have receivers in main process"

    #@29d
    aput-object v3, p6, v2

    #@29f
    .line 2300
    :cond_29f
    const/4 v2, 0x0

    #@2a0
    aget-object v2, p6, v2

    #@2a2
    if-eqz v2, :cond_2fa

    #@2a4
    .line 2301
    const/4 v14, 0x0

    #@2a5
    goto/16 :goto_5b

    #@2a7
    .line 2159
    .end local v22           #str:Ljava/lang/String;
    .restart local v18       #parentClassName:Ljava/lang/String;
    :cond_2a7
    const-string v2, "PackageParser"

    #@2a9
    new-instance v3, Ljava/lang/StringBuilder;

    #@2ab
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2ae
    const-string v4, "Activity "

    #@2b0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b3
    move-result-object v3

    #@2b4
    iget-object v4, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@2b6
    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@2b8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v3

    #@2bc
    const-string v4, " specified invalid parentActivityName "

    #@2be
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v3

    #@2c2
    move-object/from16 v0, v19

    #@2c4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c7
    move-result-object v3

    #@2c8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2cb
    move-result-object v3

    #@2cc
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2cf
    .line 2161
    const/4 v2, 0x0

    #@2d0
    const/4 v3, 0x0

    #@2d1
    aput-object v3, p6, v2

    #@2d3
    goto/16 :goto_b4

    #@2d5
    .line 2171
    .end local v18           #parentClassName:Ljava/lang/String;
    .restart local v22       #str:Ljava/lang/String;
    :cond_2d5
    iget-object v3, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@2d7
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    #@2da
    move-result v2

    #@2db
    if-lez v2, :cond_2e9

    #@2dd
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@2e0
    move-result-object v2

    #@2e1
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@2e4
    move-result-object v2

    #@2e5
    :goto_2e5
    iput-object v2, v3, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    #@2e7
    goto/16 :goto_c8

    #@2e9
    :cond_2e9
    const/4 v2, 0x0

    #@2ea
    goto :goto_2e5

    #@2eb
    .line 2222
    :cond_2eb
    const/4 v2, 0x0

    #@2ec
    goto/16 :goto_180

    #@2ee
    .line 2266
    :cond_2ee
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@2f0
    const/4 v3, 0x0

    #@2f1
    iput v3, v2, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@2f3
    .line 2267
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@2f5
    const/4 v3, 0x0

    #@2f6
    iput v3, v2, Landroid/content/pm/ActivityInfo;->configChanges:I

    #@2f8
    goto/16 :goto_213

    #@2fa
    .line 2304
    :cond_2fa
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@2fd
    move-result v17

    #@2fe
    .line 2307
    .local v17, outerDepth:I
    :cond_2fe
    :goto_2fe
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@301
    move-result v24

    #@302
    .local v24, type:I
    const/4 v2, 0x1

    #@303
    move/from16 v0, v24

    #@305
    if-eq v0, v2, :cond_43f

    #@307
    const/4 v2, 0x3

    #@308
    move/from16 v0, v24

    #@30a
    if-ne v0, v2, :cond_314

    #@30c
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@30f
    move-result v2

    #@310
    move/from16 v0, v17

    #@312
    if-le v2, v0, :cond_43f

    #@314
    .line 2309
    :cond_314
    const/4 v2, 0x3

    #@315
    move/from16 v0, v24

    #@317
    if-eq v0, v2, :cond_2fe

    #@319
    const/4 v2, 0x4

    #@31a
    move/from16 v0, v24

    #@31c
    if-eq v0, v2, :cond_2fe

    #@31e
    .line 2313
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@321
    move-result-object v2

    #@322
    const-string v3, "intent-filter"

    #@324
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@327
    move-result v2

    #@328
    if-eqz v2, :cond_381

    #@32a
    .line 2314
    new-instance v7, Landroid/content/pm/PackageParser$ActivityIntentInfo;

    #@32c
    invoke-direct {v7, v14}, Landroid/content/pm/PackageParser$ActivityIntentInfo;-><init>(Landroid/content/pm/PackageParser$Activity;)V

    #@32f
    .line 2315
    .local v7, intent:Landroid/content/pm/PackageParser$ActivityIntentInfo;
    if-nez p7, :cond_347

    #@331
    const/4 v9, 0x1

    #@332
    :goto_332
    move-object/from16 v2, p0

    #@334
    move-object/from16 v3, p2

    #@336
    move-object/from16 v4, p3

    #@338
    move-object/from16 v5, p4

    #@33a
    move/from16 v6, p5

    #@33c
    move-object/from16 v8, p6

    #@33e
    invoke-direct/range {v2 .. v9}, Landroid/content/pm/PackageParser;->parseIntent(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ILandroid/content/pm/PackageParser$IntentInfo;[Ljava/lang/String;Z)Z

    #@341
    move-result v2

    #@342
    if-nez v2, :cond_349

    #@344
    .line 2316
    const/4 v14, 0x0

    #@345
    goto/16 :goto_5b

    #@347
    .line 2315
    :cond_347
    const/4 v9, 0x0

    #@348
    goto :goto_332

    #@349
    .line 2318
    :cond_349
    invoke-virtual {v7}, Landroid/content/pm/PackageParser$ActivityIntentInfo;->countActions()I

    #@34c
    move-result v2

    #@34d
    if-nez v2, :cond_37a

    #@34f
    .line 2319
    const-string v2, "PackageParser"

    #@351
    new-instance v3, Ljava/lang/StringBuilder;

    #@353
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@356
    const-string v4, "No actions in intent filter at "

    #@358
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35b
    move-result-object v3

    #@35c
    move-object/from16 v0, p0

    #@35e
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@360
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@363
    move-result-object v3

    #@364
    const-string v4, " "

    #@366
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@369
    move-result-object v3

    #@36a
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@36d
    move-result-object v4

    #@36e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@371
    move-result-object v3

    #@372
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@375
    move-result-object v3

    #@376
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@379
    goto :goto_2fe

    #@37a
    .line 2323
    :cond_37a
    iget-object v2, v14, Landroid/content/pm/PackageParser$Component;->intents:Ljava/util/ArrayList;

    #@37c
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@37f
    goto/16 :goto_2fe

    #@381
    .line 2325
    .end local v7           #intent:Landroid/content/pm/PackageParser$ActivityIntentInfo;
    :cond_381
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@384
    move-result-object v2

    #@385
    const-string/jumbo v3, "meta-data"

    #@388
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38b
    move-result v2

    #@38c
    if-eqz v2, :cond_3a5

    #@38e
    .line 2326
    iget-object v12, v14, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@390
    move-object/from16 v8, p0

    #@392
    move-object/from16 v9, p2

    #@394
    move-object/from16 v10, p3

    #@396
    move-object/from16 v11, p4

    #@398
    move-object/from16 v13, p6

    #@39a
    invoke-direct/range {v8 .. v13}, Landroid/content/pm/PackageParser;->parseMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;

    #@39d
    move-result-object v2

    #@39e
    iput-object v2, v14, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@3a0
    if-nez v2, :cond_2fe

    #@3a2
    .line 2328
    const/4 v14, 0x0

    #@3a3
    goto/16 :goto_5b

    #@3a5
    .line 2332
    :cond_3a5
    const-string v2, "PackageParser"

    #@3a7
    new-instance v3, Ljava/lang/StringBuilder;

    #@3a9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3ac
    const-string v4, "Problem in package "

    #@3ae
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b1
    move-result-object v3

    #@3b2
    move-object/from16 v0, p0

    #@3b4
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@3b6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b9
    move-result-object v3

    #@3ba
    const-string v4, ":"

    #@3bc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3bf
    move-result-object v3

    #@3c0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c3
    move-result-object v3

    #@3c4
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3c7
    .line 2333
    if-eqz p7, :cond_406

    #@3c9
    .line 2334
    const-string v2, "PackageParser"

    #@3cb
    new-instance v3, Ljava/lang/StringBuilder;

    #@3cd
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3d0
    const-string v4, "Unknown element under <receiver>: "

    #@3d2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d5
    move-result-object v3

    #@3d6
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@3d9
    move-result-object v4

    #@3da
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3dd
    move-result-object v3

    #@3de
    const-string v4, " at "

    #@3e0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e3
    move-result-object v3

    #@3e4
    move-object/from16 v0, p0

    #@3e6
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@3e8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3eb
    move-result-object v3

    #@3ec
    const-string v4, " "

    #@3ee
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f1
    move-result-object v3

    #@3f2
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@3f5
    move-result-object v4

    #@3f6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f9
    move-result-object v3

    #@3fa
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3fd
    move-result-object v3

    #@3fe
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@401
    .line 2342
    :goto_401
    invoke-static/range {p3 .. p3}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@404
    goto/16 :goto_2fe

    #@406
    .line 2338
    :cond_406
    const-string v2, "PackageParser"

    #@408
    new-instance v3, Ljava/lang/StringBuilder;

    #@40a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@40d
    const-string v4, "Unknown element under <activity>: "

    #@40f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@412
    move-result-object v3

    #@413
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@416
    move-result-object v4

    #@417
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41a
    move-result-object v3

    #@41b
    const-string v4, " at "

    #@41d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@420
    move-result-object v3

    #@421
    move-object/from16 v0, p0

    #@423
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@425
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@428
    move-result-object v3

    #@429
    const-string v4, " "

    #@42b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42e
    move-result-object v3

    #@42f
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@432
    move-result-object v4

    #@433
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@436
    move-result-object v3

    #@437
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43a
    move-result-object v3

    #@43b
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@43e
    goto :goto_401

    #@43f
    .line 2355
    :cond_43f
    if-nez v21, :cond_44e

    #@441
    .line 2356
    iget-object v3, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@443
    iget-object v2, v14, Landroid/content/pm/PackageParser$Component;->intents:Ljava/util/ArrayList;

    #@445
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@448
    move-result v2

    #@449
    if-lez v2, :cond_4a5

    #@44b
    const/4 v2, 0x1

    #@44c
    :goto_44c
    iput-boolean v2, v3, Landroid/content/pm/ComponentInfo;->exported:Z

    #@44e
    .line 2360
    :cond_44e
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_RESOURCE:Z

    #@450
    if-eqz v2, :cond_485

    #@452
    .line 2361
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@454
    iget v2, v2, Landroid/content/pm/ActivityInfo;->theme:I

    #@456
    if-nez v2, :cond_485

    #@458
    iget-object v2, v14, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@45a
    if-eqz v2, :cond_485

    #@45c
    .line 2362
    iget-object v2, v14, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@45e
    const-string v3, "com.lge.theme"

    #@460
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@463
    move-result-object v16

    #@464
    .line 2363
    .local v16, lgeTheme:Ljava/lang/String;
    if-eqz v16, :cond_485

    #@466
    .line 2364
    const/16 v2, 0x5f

    #@468
    const/16 v3, 0x2e

    #@46a
    move-object/from16 v0, v16

    #@46c
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@46f
    move-result-object v16

    #@470
    .line 2365
    const-string/jumbo v2, "style"

    #@473
    const-string v3, "com.lge.internal"

    #@475
    move-object/from16 v0, p2

    #@477
    move-object/from16 v1, v16

    #@479
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@47c
    move-result v23

    #@47d
    .line 2366
    .local v23, themeResId:I
    if-eqz v23, :cond_485

    #@47f
    .line 2367
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@481
    move/from16 v0, v23

    #@483
    iput v0, v2, Landroid/content/pm/ActivityInfo;->theme:I

    #@485
    .line 2373
    .end local v16           #lgeTheme:Ljava/lang/String;
    .end local v23           #themeResId:I
    :cond_485
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_THEMEICON:Z

    #@487
    if-eqz v2, :cond_5b

    #@489
    if-nez p7, :cond_5b

    #@48b
    .line 2374
    iget-object v2, v14, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@48d
    if-eqz v2, :cond_5b

    #@48f
    .line 2375
    iget-object v2, v14, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@491
    const-string v3, "com.lge.themeicon.handle_configchange"

    #@493
    const/4 v4, 0x0

    #@494
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@497
    move-result v15

    #@498
    .line 2377
    .local v15, handle_change:Z
    if-eqz v15, :cond_5b

    #@49a
    .line 2378
    iget-object v2, v14, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@49c
    iget v3, v2, Landroid/content/pm/ActivityInfo;->configChanges:I

    #@49e
    const/high16 v4, 0x1000

    #@4a0
    or-int/2addr v3, v4

    #@4a1
    iput v3, v2, Landroid/content/pm/ActivityInfo;->configChanges:I

    #@4a3
    goto/16 :goto_5b

    #@4a5
    .line 2356
    .end local v15           #handle_change:Z
    :cond_4a5
    const/4 v2, 0x0

    #@4a6
    goto :goto_44c
.end method

.method private parseActivityAlias(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$Activity;
    .registers 35
    .parameter "owner"
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "flags"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2389
    sget-object v2, Lcom/android/internal/R$styleable;->AndroidManifestActivityAlias:[I

    #@2
    move-object/from16 v0, p2

    #@4
    move-object/from16 v1, p4

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v21

    #@a
    .line 2392
    .local v21, sa:Landroid/content/res/TypedArray;
    const/4 v2, 0x7

    #@b
    const/4 v3, 0x0

    #@c
    move-object/from16 v0, v21

    #@e
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@11
    move-result-object v26

    #@12
    .line 2394
    .local v26, targetActivity:Ljava/lang/String;
    if-nez v26, :cond_1e

    #@14
    .line 2395
    const/4 v2, 0x0

    #@15
    const-string v3, "<activity-alias> does not specify android:targetActivity"

    #@17
    aput-object v3, p6, v2

    #@19
    .line 2396
    invoke-virtual/range {v21 .. v21}, Landroid/content/res/TypedArray;->recycle()V

    #@1c
    .line 2397
    const/4 v15, 0x0

    #@1d
    .line 2545
    :cond_1d
    :goto_1d
    return-object v15

    #@1e
    .line 2400
    :cond_1e
    move-object/from16 v0, p1

    #@20
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@22
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@24
    move-object/from16 v0, v26

    #@26
    move-object/from16 v1, p6

    #@28
    invoke-static {v2, v0, v1}, Landroid/content/pm/PackageParser;->buildClassName(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v26

    #@2c
    .line 2402
    if-nez v26, :cond_33

    #@2e
    .line 2403
    invoke-virtual/range {v21 .. v21}, Landroid/content/res/TypedArray;->recycle()V

    #@31
    .line 2404
    const/4 v15, 0x0

    #@32
    goto :goto_1d

    #@33
    .line 2407
    :cond_33
    move-object/from16 v0, p0

    #@35
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityAliasArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@37
    if-nez v2, :cond_5a

    #@39
    .line 2408
    new-instance v2, Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@3b
    const/4 v5, 0x2

    #@3c
    const/4 v6, 0x0

    #@3d
    const/4 v7, 0x1

    #@3e
    const/16 v8, 0x8

    #@40
    move-object/from16 v0, p0

    #@42
    iget-object v9, v0, Landroid/content/pm/PackageParser;->mSeparateProcesses:[Ljava/lang/String;

    #@44
    const/4 v10, 0x0

    #@45
    const/4 v11, 0x6

    #@46
    const/4 v12, 0x4

    #@47
    move-object/from16 v3, p1

    #@49
    move-object/from16 v4, p6

    #@4b
    invoke-direct/range {v2 .. v12}, Landroid/content/pm/PackageParser$ParseComponentArgs;-><init>(Landroid/content/pm/PackageParser$Package;[Ljava/lang/String;IIII[Ljava/lang/String;III)V

    #@4e
    move-object/from16 v0, p0

    #@50
    iput-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityAliasArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@52
    .line 2417
    move-object/from16 v0, p0

    #@54
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityAliasArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@56
    const-string v3, "<activity-alias>"

    #@58
    iput-object v3, v2, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->tag:Ljava/lang/String;

    #@5a
    .line 2420
    :cond_5a
    move-object/from16 v0, p0

    #@5c
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityAliasArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@5e
    move-object/from16 v0, v21

    #@60
    iput-object v0, v2, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@62
    .line 2421
    move-object/from16 v0, p0

    #@64
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityAliasArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@66
    move/from16 v0, p5

    #@68
    iput v0, v2, Landroid/content/pm/PackageParser$ParseComponentArgs;->flags:I

    #@6a
    .line 2423
    const/16 v25, 0x0

    #@6c
    .line 2425
    .local v25, target:Landroid/content/pm/PackageParser$Activity;
    move-object/from16 v0, p1

    #@6e
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@70
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@73
    move-result v14

    #@74
    .line 2426
    .local v14, NA:I
    const/16 v16, 0x0

    #@76
    .local v16, i:I
    :goto_76
    move/from16 v0, v16

    #@78
    if-ge v0, v14, :cond_96

    #@7a
    .line 2427
    move-object/from16 v0, p1

    #@7c
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@7e
    move/from16 v0, v16

    #@80
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@83
    move-result-object v24

    #@84
    check-cast v24, Landroid/content/pm/PackageParser$Activity;

    #@86
    .line 2428
    .local v24, t:Landroid/content/pm/PackageParser$Activity;
    move-object/from16 v0, v24

    #@88
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@8a
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@8c
    move-object/from16 v0, v26

    #@8e
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v2

    #@92
    if-eqz v2, :cond_bc

    #@94
    .line 2429
    move-object/from16 v25, v24

    #@96
    .line 2434
    .end local v24           #t:Landroid/content/pm/PackageParser$Activity;
    :cond_96
    if-nez v25, :cond_bf

    #@98
    .line 2435
    const/4 v2, 0x0

    #@99
    new-instance v3, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v4, "<activity-alias> target activity "

    #@a0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v3

    #@a4
    move-object/from16 v0, v26

    #@a6
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v3

    #@aa
    const-string v4, " not found in manifest"

    #@ac
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v3

    #@b0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v3

    #@b4
    aput-object v3, p6, v2

    #@b6
    .line 2437
    invoke-virtual/range {v21 .. v21}, Landroid/content/res/TypedArray;->recycle()V

    #@b9
    .line 2438
    const/4 v15, 0x0

    #@ba
    goto/16 :goto_1d

    #@bc
    .line 2426
    .restart local v24       #t:Landroid/content/pm/PackageParser$Activity;
    :cond_bc
    add-int/lit8 v16, v16, 0x1

    #@be
    goto :goto_76

    #@bf
    .line 2441
    .end local v24           #t:Landroid/content/pm/PackageParser$Activity;
    :cond_bf
    new-instance v17, Landroid/content/pm/ActivityInfo;

    #@c1
    invoke-direct/range {v17 .. v17}, Landroid/content/pm/ActivityInfo;-><init>()V

    #@c4
    .line 2442
    .local v17, info:Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, v26

    #@c6
    move-object/from16 v1, v17

    #@c8
    iput-object v0, v1, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@ca
    .line 2443
    move-object/from16 v0, v25

    #@cc
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@ce
    iget v2, v2, Landroid/content/pm/ActivityInfo;->configChanges:I

    #@d0
    move-object/from16 v0, v17

    #@d2
    iput v2, v0, Landroid/content/pm/ActivityInfo;->configChanges:I

    #@d4
    .line 2444
    move-object/from16 v0, v25

    #@d6
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@d8
    iget v2, v2, Landroid/content/pm/ActivityInfo;->flags:I

    #@da
    move-object/from16 v0, v17

    #@dc
    iput v2, v0, Landroid/content/pm/ActivityInfo;->flags:I

    #@de
    .line 2445
    move-object/from16 v0, v25

    #@e0
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@e2
    iget v2, v2, Landroid/content/pm/PackageItemInfo;->icon:I

    #@e4
    move-object/from16 v0, v17

    #@e6
    iput v2, v0, Landroid/content/pm/PackageItemInfo;->icon:I

    #@e8
    .line 2446
    move-object/from16 v0, v25

    #@ea
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@ec
    iget v2, v2, Landroid/content/pm/PackageItemInfo;->logo:I

    #@ee
    move-object/from16 v0, v17

    #@f0
    iput v2, v0, Landroid/content/pm/PackageItemInfo;->logo:I

    #@f2
    .line 2447
    move-object/from16 v0, v25

    #@f4
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@f6
    iget v2, v2, Landroid/content/pm/PackageItemInfo;->labelRes:I

    #@f8
    move-object/from16 v0, v17

    #@fa
    iput v2, v0, Landroid/content/pm/PackageItemInfo;->labelRes:I

    #@fc
    .line 2448
    move-object/from16 v0, v25

    #@fe
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@100
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@102
    move-object/from16 v0, v17

    #@104
    iput-object v2, v0, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@106
    .line 2449
    move-object/from16 v0, v25

    #@108
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@10a
    iget v2, v2, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@10c
    move-object/from16 v0, v17

    #@10e
    iput v2, v0, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@110
    .line 2450
    move-object/from16 v0, v25

    #@112
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@114
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@116
    move-object/from16 v0, v17

    #@118
    iput-object v2, v0, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@11a
    .line 2451
    move-object/from16 v0, v17

    #@11c
    iget v2, v0, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@11e
    if-nez v2, :cond_12a

    #@120
    .line 2452
    move-object/from16 v0, v25

    #@122
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@124
    iget v2, v2, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@126
    move-object/from16 v0, v17

    #@128
    iput v2, v0, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@12a
    .line 2454
    :cond_12a
    move-object/from16 v0, v25

    #@12c
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@12e
    iget v2, v2, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    #@130
    move-object/from16 v0, v17

    #@132
    iput v2, v0, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    #@134
    .line 2455
    move-object/from16 v0, v25

    #@136
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@138
    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@13a
    move-object/from16 v0, v17

    #@13c
    iput-object v2, v0, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@13e
    .line 2456
    move-object/from16 v0, v25

    #@140
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@142
    iget v2, v2, Landroid/content/pm/ActivityInfo;->theme:I

    #@144
    move-object/from16 v0, v17

    #@146
    iput v2, v0, Landroid/content/pm/ActivityInfo;->theme:I

    #@148
    .line 2457
    move-object/from16 v0, v25

    #@14a
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@14c
    iget v2, v2, Landroid/content/pm/ActivityInfo;->softInputMode:I

    #@14e
    move-object/from16 v0, v17

    #@150
    iput v2, v0, Landroid/content/pm/ActivityInfo;->softInputMode:I

    #@152
    .line 2458
    move-object/from16 v0, v25

    #@154
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@156
    iget v2, v2, Landroid/content/pm/ActivityInfo;->uiOptions:I

    #@158
    move-object/from16 v0, v17

    #@15a
    iput v2, v0, Landroid/content/pm/ActivityInfo;->uiOptions:I

    #@15c
    .line 2459
    move-object/from16 v0, v25

    #@15e
    iget-object v2, v0, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@160
    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->parentActivityName:Ljava/lang/String;

    #@162
    move-object/from16 v0, v17

    #@164
    iput-object v2, v0, Landroid/content/pm/ActivityInfo;->parentActivityName:Ljava/lang/String;

    #@166
    .line 2461
    new-instance v15, Landroid/content/pm/PackageParser$Activity;

    #@168
    move-object/from16 v0, p0

    #@16a
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseActivityAliasArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@16c
    move-object/from16 v0, v17

    #@16e
    invoke-direct {v15, v2, v0}, Landroid/content/pm/PackageParser$Activity;-><init>(Landroid/content/pm/PackageParser$ParseComponentArgs;Landroid/content/pm/ActivityInfo;)V

    #@171
    .line 2462
    .local v15, a:Landroid/content/pm/PackageParser$Activity;
    const/4 v2, 0x0

    #@172
    aget-object v2, p6, v2

    #@174
    if-eqz v2, :cond_17c

    #@176
    .line 2463
    invoke-virtual/range {v21 .. v21}, Landroid/content/res/TypedArray;->recycle()V

    #@179
    .line 2464
    const/4 v15, 0x0

    #@17a
    goto/16 :goto_1d

    #@17c
    .line 2467
    :cond_17c
    const/4 v2, 0x5

    #@17d
    move-object/from16 v0, v21

    #@17f
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@182
    move-result v22

    #@183
    .line 2469
    .local v22, setExported:Z
    if-eqz v22, :cond_191

    #@185
    .line 2470
    iget-object v2, v15, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@187
    const/4 v3, 0x5

    #@188
    const/4 v4, 0x0

    #@189
    move-object/from16 v0, v21

    #@18b
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@18e
    move-result v3

    #@18f
    iput-boolean v3, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@191
    .line 2475
    :cond_191
    const/4 v2, 0x3

    #@192
    const/4 v3, 0x0

    #@193
    move-object/from16 v0, v21

    #@195
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@198
    move-result-object v23

    #@199
    .line 2477
    .local v23, str:Ljava/lang/String;
    if-eqz v23, :cond_1ad

    #@19b
    .line 2478
    iget-object v3, v15, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@19d
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    #@1a0
    move-result v2

    #@1a1
    if-lez v2, :cond_1da

    #@1a3
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@1a6
    move-result-object v2

    #@1a7
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@1aa
    move-result-object v2

    #@1ab
    :goto_1ab
    iput-object v2, v3, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    #@1ad
    .line 2481
    :cond_1ad
    const/16 v2, 0x9

    #@1af
    const/4 v3, 0x0

    #@1b0
    move-object/from16 v0, v21

    #@1b2
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@1b5
    move-result-object v20

    #@1b6
    .line 2484
    .local v20, parentName:Ljava/lang/String;
    if-eqz v20, :cond_1cf

    #@1b8
    .line 2485
    iget-object v2, v15, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@1ba
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@1bc
    move-object/from16 v0, v20

    #@1be
    move-object/from16 v1, p6

    #@1c0
    invoke-static {v2, v0, v1}, Landroid/content/pm/PackageParser;->buildClassName(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@1c3
    move-result-object v19

    #@1c4
    .line 2486
    .local v19, parentClassName:Ljava/lang/String;
    const/4 v2, 0x0

    #@1c5
    aget-object v2, p6, v2

    #@1c7
    if-nez v2, :cond_1dc

    #@1c9
    .line 2487
    iget-object v2, v15, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@1cb
    move-object/from16 v0, v19

    #@1cd
    iput-object v0, v2, Landroid/content/pm/ActivityInfo;->parentActivityName:Ljava/lang/String;

    #@1cf
    .line 2495
    .end local v19           #parentClassName:Ljava/lang/String;
    :cond_1cf
    :goto_1cf
    invoke-virtual/range {v21 .. v21}, Landroid/content/res/TypedArray;->recycle()V

    #@1d2
    .line 2497
    const/4 v2, 0x0

    #@1d3
    aget-object v2, p6, v2

    #@1d5
    if-eqz v2, :cond_209

    #@1d7
    .line 2498
    const/4 v15, 0x0

    #@1d8
    goto/16 :goto_1d

    #@1da
    .line 2478
    .end local v20           #parentName:Ljava/lang/String;
    :cond_1da
    const/4 v2, 0x0

    #@1db
    goto :goto_1ab

    #@1dc
    .line 2489
    .restart local v19       #parentClassName:Ljava/lang/String;
    .restart local v20       #parentName:Ljava/lang/String;
    :cond_1dc
    const-string v2, "PackageParser"

    #@1de
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e3
    const-string v4, "Activity alias "

    #@1e5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v3

    #@1e9
    iget-object v4, v15, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@1eb
    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@1ed
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f0
    move-result-object v3

    #@1f1
    const-string v4, " specified invalid parentActivityName "

    #@1f3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v3

    #@1f7
    move-object/from16 v0, v20

    #@1f9
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v3

    #@1fd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@200
    move-result-object v3

    #@201
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@204
    .line 2491
    const/4 v2, 0x0

    #@205
    const/4 v3, 0x0

    #@206
    aput-object v3, p6, v2

    #@208
    goto :goto_1cf

    #@209
    .line 2501
    .end local v19           #parentClassName:Ljava/lang/String;
    :cond_209
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@20c
    move-result v18

    #@20d
    .line 2504
    .local v18, outerDepth:I
    :cond_20d
    :goto_20d
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@210
    move-result v27

    #@211
    .local v27, type:I
    const/4 v2, 0x1

    #@212
    move/from16 v0, v27

    #@214
    if-eq v0, v2, :cond_2ec

    #@216
    const/4 v2, 0x3

    #@217
    move/from16 v0, v27

    #@219
    if-ne v0, v2, :cond_223

    #@21b
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@21e
    move-result v2

    #@21f
    move/from16 v0, v18

    #@221
    if-le v2, v0, :cond_2ec

    #@223
    .line 2506
    :cond_223
    const/4 v2, 0x3

    #@224
    move/from16 v0, v27

    #@226
    if-eq v0, v2, :cond_20d

    #@228
    const/4 v2, 0x4

    #@229
    move/from16 v0, v27

    #@22b
    if-eq v0, v2, :cond_20d

    #@22d
    .line 2510
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@230
    move-result-object v2

    #@231
    const-string v3, "intent-filter"

    #@233
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@236
    move-result v2

    #@237
    if-eqz v2, :cond_28b

    #@239
    .line 2511
    new-instance v7, Landroid/content/pm/PackageParser$ActivityIntentInfo;

    #@23b
    invoke-direct {v7, v15}, Landroid/content/pm/PackageParser$ActivityIntentInfo;-><init>(Landroid/content/pm/PackageParser$Activity;)V

    #@23e
    .line 2512
    .local v7, intent:Landroid/content/pm/PackageParser$ActivityIntentInfo;
    const/4 v9, 0x1

    #@23f
    move-object/from16 v2, p0

    #@241
    move-object/from16 v3, p2

    #@243
    move-object/from16 v4, p3

    #@245
    move-object/from16 v5, p4

    #@247
    move/from16 v6, p5

    #@249
    move-object/from16 v8, p6

    #@24b
    invoke-direct/range {v2 .. v9}, Landroid/content/pm/PackageParser;->parseIntent(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ILandroid/content/pm/PackageParser$IntentInfo;[Ljava/lang/String;Z)Z

    #@24e
    move-result v2

    #@24f
    if-nez v2, :cond_254

    #@251
    .line 2513
    const/4 v15, 0x0

    #@252
    goto/16 :goto_1d

    #@254
    .line 2515
    :cond_254
    invoke-virtual {v7}, Landroid/content/pm/PackageParser$ActivityIntentInfo;->countActions()I

    #@257
    move-result v2

    #@258
    if-nez v2, :cond_285

    #@25a
    .line 2516
    const-string v2, "PackageParser"

    #@25c
    new-instance v3, Ljava/lang/StringBuilder;

    #@25e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@261
    const-string v4, "No actions in intent filter at "

    #@263
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@266
    move-result-object v3

    #@267
    move-object/from16 v0, p0

    #@269
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@26b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26e
    move-result-object v3

    #@26f
    const-string v4, " "

    #@271
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@274
    move-result-object v3

    #@275
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@278
    move-result-object v4

    #@279
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27c
    move-result-object v3

    #@27d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@280
    move-result-object v3

    #@281
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@284
    goto :goto_20d

    #@285
    .line 2520
    :cond_285
    iget-object v2, v15, Landroid/content/pm/PackageParser$Component;->intents:Ljava/util/ArrayList;

    #@287
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28a
    goto :goto_20d

    #@28b
    .line 2522
    .end local v7           #intent:Landroid/content/pm/PackageParser$ActivityIntentInfo;
    :cond_28b
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@28e
    move-result-object v2

    #@28f
    const-string/jumbo v3, "meta-data"

    #@292
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@295
    move-result v2

    #@296
    if-eqz v2, :cond_2af

    #@298
    .line 2523
    iget-object v12, v15, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@29a
    move-object/from16 v8, p0

    #@29c
    move-object/from16 v9, p2

    #@29e
    move-object/from16 v10, p3

    #@2a0
    move-object/from16 v11, p4

    #@2a2
    move-object/from16 v13, p6

    #@2a4
    invoke-direct/range {v8 .. v13}, Landroid/content/pm/PackageParser;->parseMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;

    #@2a7
    move-result-object v2

    #@2a8
    iput-object v2, v15, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@2aa
    if-nez v2, :cond_20d

    #@2ac
    .line 2525
    const/4 v15, 0x0

    #@2ad
    goto/16 :goto_1d

    #@2af
    .line 2529
    :cond_2af
    const-string v2, "PackageParser"

    #@2b1
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b6
    const-string v4, "Unknown element under <activity-alias>: "

    #@2b8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v3

    #@2bc
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@2bf
    move-result-object v4

    #@2c0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c3
    move-result-object v3

    #@2c4
    const-string v4, " at "

    #@2c6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c9
    move-result-object v3

    #@2ca
    move-object/from16 v0, p0

    #@2cc
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@2ce
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d1
    move-result-object v3

    #@2d2
    const-string v4, " "

    #@2d4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d7
    move-result-object v3

    #@2d8
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@2db
    move-result-object v4

    #@2dc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2df
    move-result-object v3

    #@2e0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e3
    move-result-object v3

    #@2e4
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2e7
    .line 2532
    invoke-static/range {p3 .. p3}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@2ea
    goto/16 :goto_20d

    #@2ec
    .line 2541
    :cond_2ec
    if-nez v22, :cond_1d

    #@2ee
    .line 2542
    iget-object v3, v15, Landroid/content/pm/PackageParser$Activity;->info:Landroid/content/pm/ActivityInfo;

    #@2f0
    iget-object v2, v15, Landroid/content/pm/PackageParser$Component;->intents:Ljava/util/ArrayList;

    #@2f2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@2f5
    move-result v2

    #@2f6
    if-lez v2, :cond_2fd

    #@2f8
    const/4 v2, 0x1

    #@2f9
    :goto_2f9
    iput-boolean v2, v3, Landroid/content/pm/ComponentInfo;->exported:Z

    #@2fb
    goto/16 :goto_1d

    #@2fd
    :cond_2fd
    const/4 v2, 0x0

    #@2fe
    goto :goto_2f9
.end method

.method private parseAllMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Ljava/lang/String;Landroid/content/pm/PackageParser$Component;[Ljava/lang/String;)Z
    .registers 15
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "tag"
    .parameter "outInfo"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2971
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@3
    move-result v6

    #@4
    .line 2974
    .local v6, outerDepth:I
    :cond_4
    :goto_4
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@7
    move-result v7

    #@8
    .local v7, type:I
    const/4 v0, 0x1

    #@9
    if-eq v7, v0, :cond_7c

    #@b
    const/4 v0, 0x3

    #@c
    if-ne v7, v0, :cond_14

    #@e
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@11
    move-result v0

    #@12
    if-le v0, v6, :cond_7c

    #@14
    .line 2976
    :cond_14
    const/4 v0, 0x3

    #@15
    if-eq v7, v0, :cond_4

    #@17
    const/4 v0, 0x4

    #@18
    if-eq v7, v0, :cond_4

    #@1a
    .line 2980
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    const-string/jumbo v1, "meta-data"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_38

    #@27
    .line 2981
    iget-object v4, p5, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@29
    move-object v0, p0

    #@2a
    move-object v1, p1

    #@2b
    move-object v2, p2

    #@2c
    move-object v3, p3

    #@2d
    move-object v5, p6

    #@2e
    invoke-direct/range {v0 .. v5}, Landroid/content/pm/PackageParser;->parseMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;

    #@31
    move-result-object v0

    #@32
    iput-object v0, p5, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@34
    if-nez v0, :cond_4

    #@36
    .line 2983
    const/4 v0, 0x0

    #@37
    .line 2998
    :goto_37
    return v0

    #@38
    .line 2987
    :cond_38
    const-string v0, "PackageParser"

    #@3a
    new-instance v1, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v2, "Unknown element under "

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    const-string v2, ": "

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    const-string v2, " at "

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    iget-object v2, p0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    const-string v2, " "

    #@65
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v1

    #@69
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@6c
    move-result-object v2

    #@6d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v1

    #@75
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 2990
    invoke-static {p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@7b
    goto :goto_4

    #@7c
    .line 2998
    :cond_7c
    const/4 v0, 0x1

    #@7d
    goto :goto_37
.end method

.method private parseApplication(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Z
    .registers 51
    .parameter "owner"
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "flags"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1741
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4
    move-object/from16 v25, v0

    #@6
    .line 1742
    .local v25, ai:Landroid/content/pm/ApplicationInfo;
    move-object/from16 v0, p1

    #@8
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@a
    iget-object v0, v3, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@c
    move-object/from16 v35, v0

    #@e
    .line 1744
    .local v35, pkgName:Ljava/lang/String;
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifestApplication:[I

    #@10
    move-object/from16 v0, p2

    #@12
    move-object/from16 v1, p4

    #@14
    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@17
    move-result-object v38

    #@18
    .line 1747
    .local v38, sa:Landroid/content/res/TypedArray;
    const/4 v3, 0x3

    #@19
    const/4 v4, 0x0

    #@1a
    move-object/from16 v0, v38

    #@1c
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@1f
    move-result-object v33

    #@20
    .line 1749
    .local v33, name:Ljava/lang/String;
    if-eqz v33, :cond_41

    #@22
    .line 1750
    move-object/from16 v0, v35

    #@24
    move-object/from16 v1, v33

    #@26
    move-object/from16 v2, p6

    #@28
    invoke-static {v0, v1, v2}, Landroid/content/pm/PackageParser;->buildClassName(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    move-object/from16 v0, v25

    #@2e
    iput-object v3, v0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@30
    .line 1751
    move-object/from16 v0, v25

    #@32
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@34
    if-nez v3, :cond_41

    #@36
    .line 1752
    invoke-virtual/range {v38 .. v38}, Landroid/content/res/TypedArray;->recycle()V

    #@39
    .line 1753
    const/16 v3, -0x6c

    #@3b
    move-object/from16 v0, p0

    #@3d
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@3f
    .line 1754
    const/4 v3, 0x0

    #@40
    .line 2070
    :goto_40
    return v3

    #@41
    .line 1758
    :cond_41
    const/4 v3, 0x4

    #@42
    const/4 v4, 0x0

    #@43
    move-object/from16 v0, v38

    #@45
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@48
    move-result-object v32

    #@49
    .line 1760
    .local v32, manageSpaceActivity:Ljava/lang/String;
    if-eqz v32, :cond_59

    #@4b
    .line 1761
    move-object/from16 v0, v35

    #@4d
    move-object/from16 v1, v32

    #@4f
    move-object/from16 v2, p6

    #@51
    invoke-static {v0, v1, v2}, Landroid/content/pm/PackageParser;->buildClassName(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    move-object/from16 v0, v25

    #@57
    iput-object v3, v0, Landroid/content/pm/ApplicationInfo;->manageSpaceActivityName:Ljava/lang/String;

    #@59
    .line 1765
    :cond_59
    const/16 v3, 0x11

    #@5b
    const/4 v4, 0x1

    #@5c
    move-object/from16 v0, v38

    #@5e
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@61
    move-result v26

    #@62
    .line 1767
    .local v26, allowBackup:Z
    if-eqz v26, :cond_b5

    #@64
    .line 1768
    move-object/from16 v0, v25

    #@66
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@68
    const v4, 0x8000

    #@6b
    or-int/2addr v3, v4

    #@6c
    move-object/from16 v0, v25

    #@6e
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@70
    .line 1772
    const/16 v3, 0x10

    #@72
    const/4 v4, 0x0

    #@73
    move-object/from16 v0, v38

    #@75
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@78
    move-result-object v27

    #@79
    .line 1774
    .local v27, backupAgent:Ljava/lang/String;
    if-eqz v27, :cond_b5

    #@7b
    .line 1775
    move-object/from16 v0, v35

    #@7d
    move-object/from16 v1, v27

    #@7f
    move-object/from16 v2, p6

    #@81
    invoke-static {v0, v1, v2}, Landroid/content/pm/PackageParser;->buildClassName(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@84
    move-result-object v3

    #@85
    move-object/from16 v0, v25

    #@87
    iput-object v3, v0, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@89
    .line 1781
    const/16 v3, 0x12

    #@8b
    const/4 v4, 0x1

    #@8c
    move-object/from16 v0, v38

    #@8e
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@91
    move-result v3

    #@92
    if-eqz v3, :cond_9f

    #@94
    .line 1784
    move-object/from16 v0, v25

    #@96
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@98
    const/high16 v4, 0x1

    #@9a
    or-int/2addr v3, v4

    #@9b
    move-object/from16 v0, v25

    #@9d
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@9f
    .line 1786
    :cond_9f
    const/16 v3, 0x15

    #@a1
    const/4 v4, 0x0

    #@a2
    move-object/from16 v0, v38

    #@a4
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@a7
    move-result v3

    #@a8
    if-eqz v3, :cond_b5

    #@aa
    .line 1789
    move-object/from16 v0, v25

    #@ac
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@ae
    const/high16 v4, 0x2

    #@b0
    or-int/2addr v3, v4

    #@b1
    move-object/from16 v0, v25

    #@b3
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@b5
    .line 1794
    .end local v27           #backupAgent:Ljava/lang/String;
    :cond_b5
    const/4 v3, 0x1

    #@b6
    move-object/from16 v0, v38

    #@b8
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@bb
    move-result-object v43

    #@bc
    .line 1796
    .local v43, v:Landroid/util/TypedValue;
    if-eqz v43, :cond_d0

    #@be
    move-object/from16 v0, v43

    #@c0
    iget v3, v0, Landroid/util/TypedValue;->resourceId:I

    #@c2
    move-object/from16 v0, v25

    #@c4
    iput v3, v0, Landroid/content/pm/PackageItemInfo;->labelRes:I

    #@c6
    if-nez v3, :cond_d0

    #@c8
    .line 1797
    invoke-virtual/range {v43 .. v43}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@cb
    move-result-object v3

    #@cc
    move-object/from16 v0, v25

    #@ce
    iput-object v3, v0, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@d0
    .line 1800
    :cond_d0
    const/4 v3, 0x2

    #@d1
    const/4 v4, 0x0

    #@d2
    move-object/from16 v0, v38

    #@d4
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@d7
    move-result v3

    #@d8
    move-object/from16 v0, v25

    #@da
    iput v3, v0, Landroid/content/pm/PackageItemInfo;->icon:I

    #@dc
    .line 1802
    const/16 v3, 0x16

    #@de
    const/4 v4, 0x0

    #@df
    move-object/from16 v0, v38

    #@e1
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@e4
    move-result v3

    #@e5
    move-object/from16 v0, v25

    #@e7
    iput v3, v0, Landroid/content/pm/PackageItemInfo;->logo:I

    #@e9
    .line 1804
    const/4 v3, 0x0

    #@ea
    const/4 v4, 0x0

    #@eb
    move-object/from16 v0, v38

    #@ed
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@f0
    move-result v3

    #@f1
    move-object/from16 v0, v25

    #@f3
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->theme:I

    #@f5
    .line 1806
    const/16 v3, 0xd

    #@f7
    const/4 v4, 0x0

    #@f8
    move-object/from16 v0, v38

    #@fa
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@fd
    move-result v3

    #@fe
    move-object/from16 v0, v25

    #@100
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->descriptionRes:I

    #@102
    .line 1809
    and-int/lit8 v3, p5, 0x1

    #@104
    if-eqz v3, :cond_11b

    #@106
    .line 1810
    const/16 v3, 0x8

    #@108
    const/4 v4, 0x0

    #@109
    move-object/from16 v0, v38

    #@10b
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@10e
    move-result v3

    #@10f
    if-eqz v3, :cond_11b

    #@111
    .line 1813
    move-object/from16 v0, v25

    #@113
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@115
    or-int/lit8 v3, v3, 0x8

    #@117
    move-object/from16 v0, v25

    #@119
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@11b
    .line 1817
    :cond_11b
    const/16 v3, 0xa

    #@11d
    const/4 v4, 0x0

    #@11e
    move-object/from16 v0, v38

    #@120
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@123
    move-result v3

    #@124
    if-eqz v3, :cond_130

    #@126
    .line 1820
    move-object/from16 v0, v25

    #@128
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@12a
    or-int/lit8 v3, v3, 0x2

    #@12c
    move-object/from16 v0, v25

    #@12e
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@130
    .line 1823
    :cond_130
    const/16 v3, 0x14

    #@132
    const/4 v4, 0x0

    #@133
    move-object/from16 v0, v38

    #@135
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@138
    move-result v3

    #@139
    if-eqz v3, :cond_145

    #@13b
    .line 1826
    move-object/from16 v0, v25

    #@13d
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@13f
    or-int/lit16 v3, v3, 0x4000

    #@141
    move-object/from16 v0, v25

    #@143
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@145
    .line 1829
    :cond_145
    const/16 v4, 0x17

    #@147
    move-object/from16 v0, p1

    #@149
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@14b
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@14d
    const/16 v6, 0xe

    #@14f
    if-lt v3, v6, :cond_26d

    #@151
    const/4 v3, 0x1

    #@152
    :goto_152
    move-object/from16 v0, v38

    #@154
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@157
    move-result v14

    #@158
    .line 1833
    .local v14, hardwareAccelerated:Z
    const/4 v3, 0x7

    #@159
    const/4 v4, 0x1

    #@15a
    move-object/from16 v0, v38

    #@15c
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@15f
    move-result v3

    #@160
    if-eqz v3, :cond_16c

    #@162
    .line 1836
    move-object/from16 v0, v25

    #@164
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@166
    or-int/lit8 v3, v3, 0x4

    #@168
    move-object/from16 v0, v25

    #@16a
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@16c
    .line 1839
    :cond_16c
    const/16 v3, 0xe

    #@16e
    const/4 v4, 0x0

    #@16f
    move-object/from16 v0, v38

    #@171
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@174
    move-result v3

    #@175
    if-eqz v3, :cond_181

    #@177
    .line 1842
    move-object/from16 v0, v25

    #@179
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@17b
    or-int/lit8 v3, v3, 0x20

    #@17d
    move-object/from16 v0, v25

    #@17f
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@181
    .line 1845
    :cond_181
    const/4 v3, 0x5

    #@182
    const/4 v4, 0x1

    #@183
    move-object/from16 v0, v38

    #@185
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@188
    move-result v3

    #@189
    if-eqz v3, :cond_195

    #@18b
    .line 1848
    move-object/from16 v0, v25

    #@18d
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@18f
    or-int/lit8 v3, v3, 0x40

    #@191
    move-object/from16 v0, v25

    #@193
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@195
    .line 1851
    :cond_195
    const/16 v3, 0xf

    #@197
    const/4 v4, 0x0

    #@198
    move-object/from16 v0, v38

    #@19a
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@19d
    move-result v3

    #@19e
    if-eqz v3, :cond_1aa

    #@1a0
    .line 1854
    move-object/from16 v0, v25

    #@1a2
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1a4
    or-int/lit16 v3, v3, 0x100

    #@1a6
    move-object/from16 v0, v25

    #@1a8
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1aa
    .line 1857
    :cond_1aa
    const/16 v3, 0x18

    #@1ac
    const/4 v4, 0x0

    #@1ad
    move-object/from16 v0, v38

    #@1af
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1b2
    move-result v3

    #@1b3
    if-eqz v3, :cond_1c0

    #@1b5
    .line 1860
    move-object/from16 v0, v25

    #@1b7
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1b9
    const/high16 v4, 0x10

    #@1bb
    or-int/2addr v3, v4

    #@1bc
    move-object/from16 v0, v25

    #@1be
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1c0
    .line 1863
    :cond_1c0
    const/16 v3, 0x1a

    #@1c2
    const/4 v4, 0x0

    #@1c3
    move-object/from16 v0, v38

    #@1c5
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1c8
    move-result v3

    #@1c9
    if-eqz v3, :cond_1d6

    #@1cb
    .line 1866
    move-object/from16 v0, v25

    #@1cd
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1cf
    const/high16 v4, 0x40

    #@1d1
    or-int/2addr v3, v4

    #@1d2
    move-object/from16 v0, v25

    #@1d4
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1d6
    .line 1870
    :cond_1d6
    const/4 v3, 0x6

    #@1d7
    const/4 v4, 0x0

    #@1d8
    move-object/from16 v0, v38

    #@1da
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@1dd
    move-result-object v39

    #@1de
    .line 1872
    .local v39, str:Ljava/lang/String;
    if-eqz v39, :cond_270

    #@1e0
    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->length()I

    #@1e3
    move-result v3

    #@1e4
    if-lez v3, :cond_270

    #@1e6
    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@1e9
    move-result-object v3

    #@1ea
    :goto_1ea
    move-object/from16 v0, v25

    #@1ec
    iput-object v3, v0, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@1ee
    .line 1874
    move-object/from16 v0, p1

    #@1f0
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1f2
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@1f4
    const/16 v4, 0x8

    #@1f6
    if-lt v3, v4, :cond_273

    #@1f8
    .line 1875
    const/16 v3, 0xc

    #@1fa
    const/4 v4, 0x0

    #@1fb
    move-object/from16 v0, v38

    #@1fd
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@200
    move-result-object v39

    #@201
    .line 1884
    :goto_201
    move-object/from16 v0, v25

    #@203
    iget-object v3, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@205
    move-object/from16 v0, v25

    #@207
    iget-object v4, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@209
    move-object/from16 v0, v39

    #@20b
    move-object/from16 v1, p6

    #@20d
    invoke-static {v3, v4, v0, v1}, Landroid/content/pm/PackageParser;->buildTaskAffinityName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@210
    move-result-object v3

    #@211
    move-object/from16 v0, v25

    #@213
    iput-object v3, v0, Landroid/content/pm/ApplicationInfo;->taskAffinity:Ljava/lang/String;

    #@215
    .line 1887
    const/4 v3, 0x0

    #@216
    aget-object v3, p6, v3

    #@218
    if-nez v3, :cond_24f

    #@21a
    .line 1889
    move-object/from16 v0, p1

    #@21c
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@21e
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@220
    const/16 v4, 0x8

    #@222
    if-lt v3, v4, :cond_27c

    #@224
    .line 1890
    const/16 v3, 0xb

    #@226
    const/4 v4, 0x0

    #@227
    move-object/from16 v0, v38

    #@229
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@22c
    move-result-object v5

    #@22d
    .line 1899
    .local v5, pname:Ljava/lang/CharSequence;
    :goto_22d
    move-object/from16 v0, v25

    #@22f
    iget-object v3, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@231
    const/4 v4, 0x0

    #@232
    move-object/from16 v0, p0

    #@234
    iget-object v7, v0, Landroid/content/pm/PackageParser;->mSeparateProcesses:[Ljava/lang/String;

    #@236
    move/from16 v6, p5

    #@238
    move-object/from16 v8, p6

    #@23a
    invoke-static/range {v3 .. v8}, Landroid/content/pm/PackageParser;->buildProcessName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@23d
    move-result-object v3

    #@23e
    move-object/from16 v0, v25

    #@240
    iput-object v3, v0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@242
    .line 1902
    const/16 v3, 0x9

    #@244
    const/4 v4, 0x1

    #@245
    move-object/from16 v0, v38

    #@247
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@24a
    move-result v3

    #@24b
    move-object/from16 v0, v25

    #@24d
    iput-boolean v3, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@24f
    .line 1920
    .end local v5           #pname:Ljava/lang/CharSequence;
    :cond_24f
    const/16 v3, 0x19

    #@251
    const/4 v4, 0x0

    #@252
    move-object/from16 v0, v38

    #@254
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@257
    move-result v3

    #@258
    move-object/from16 v0, v25

    #@25a
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@25c
    .line 1923
    invoke-virtual/range {v38 .. v38}, Landroid/content/res/TypedArray;->recycle()V

    #@25f
    .line 1925
    const/4 v3, 0x0

    #@260
    aget-object v3, p6, v3

    #@262
    if-eqz v3, :cond_285

    #@264
    .line 1926
    const/16 v3, -0x6c

    #@266
    move-object/from16 v0, p0

    #@268
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@26a
    .line 1927
    const/4 v3, 0x0

    #@26b
    goto/16 :goto_40

    #@26d
    .line 1829
    .end local v14           #hardwareAccelerated:Z
    .end local v39           #str:Ljava/lang/String;
    :cond_26d
    const/4 v3, 0x0

    #@26e
    goto/16 :goto_152

    #@270
    .line 1872
    .restart local v14       #hardwareAccelerated:Z
    .restart local v39       #str:Ljava/lang/String;
    :cond_270
    const/4 v3, 0x0

    #@271
    goto/16 :goto_1ea

    #@273
    .line 1881
    :cond_273
    const/16 v3, 0xc

    #@275
    move-object/from16 v0, v38

    #@277
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@27a
    move-result-object v39

    #@27b
    goto :goto_201

    #@27c
    .line 1896
    :cond_27c
    const/16 v3, 0xb

    #@27e
    move-object/from16 v0, v38

    #@280
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@283
    move-result-object v5

    #@284
    .restart local v5       #pname:Ljava/lang/CharSequence;
    goto :goto_22d

    #@285
    .line 1930
    .end local v5           #pname:Ljava/lang/CharSequence;
    :cond_285
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@288
    move-result v28

    #@289
    .line 1934
    .local v28, innerDepth:I
    :cond_289
    :goto_289
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@28c
    move-result v42

    #@28d
    .local v42, type:I
    const/4 v3, 0x1

    #@28e
    move/from16 v0, v42

    #@290
    if-eq v0, v3, :cond_47a

    #@292
    const/4 v3, 0x3

    #@293
    move/from16 v0, v42

    #@295
    if-ne v0, v3, :cond_29f

    #@297
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@29a
    move-result v3

    #@29b
    move/from16 v0, v28

    #@29d
    if-le v3, v0, :cond_47a

    #@29f
    .line 1935
    :cond_29f
    const/4 v3, 0x3

    #@2a0
    move/from16 v0, v42

    #@2a2
    if-eq v0, v3, :cond_289

    #@2a4
    const/4 v3, 0x4

    #@2a5
    move/from16 v0, v42

    #@2a7
    if-eq v0, v3, :cond_289

    #@2a9
    .line 1939
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@2ac
    move-result-object v40

    #@2ad
    .line 1940
    .local v40, tagName:Ljava/lang/String;
    const-string v3, "activity"

    #@2af
    move-object/from16 v0, v40

    #@2b1
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b4
    move-result v3

    #@2b5
    if-eqz v3, :cond_2df

    #@2b7
    .line 1941
    const/4 v13, 0x0

    #@2b8
    move-object/from16 v6, p0

    #@2ba
    move-object/from16 v7, p1

    #@2bc
    move-object/from16 v8, p2

    #@2be
    move-object/from16 v9, p3

    #@2c0
    move-object/from16 v10, p4

    #@2c2
    move/from16 v11, p5

    #@2c4
    move-object/from16 v12, p6

    #@2c6
    invoke-direct/range {v6 .. v14}, Landroid/content/pm/PackageParser;->parseActivity(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;ZZ)Landroid/content/pm/PackageParser$Activity;

    #@2c9
    move-result-object v24

    #@2ca
    .line 1943
    .local v24, a:Landroid/content/pm/PackageParser$Activity;
    if-nez v24, :cond_2d5

    #@2cc
    .line 1944
    const/16 v3, -0x6c

    #@2ce
    move-object/from16 v0, p0

    #@2d0
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@2d2
    .line 1945
    const/4 v3, 0x0

    #@2d3
    goto/16 :goto_40

    #@2d5
    .line 1948
    :cond_2d5
    move-object/from16 v0, p1

    #@2d7
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@2d9
    move-object/from16 v0, v24

    #@2db
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2de
    goto :goto_289

    #@2df
    .line 1950
    .end local v24           #a:Landroid/content/pm/PackageParser$Activity;
    :cond_2df
    const-string/jumbo v3, "receiver"

    #@2e2
    move-object/from16 v0, v40

    #@2e4
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e7
    move-result v3

    #@2e8
    if-eqz v3, :cond_316

    #@2ea
    .line 1951
    const/16 v22, 0x1

    #@2ec
    const/16 v23, 0x0

    #@2ee
    move-object/from16 v15, p0

    #@2f0
    move-object/from16 v16, p1

    #@2f2
    move-object/from16 v17, p2

    #@2f4
    move-object/from16 v18, p3

    #@2f6
    move-object/from16 v19, p4

    #@2f8
    move/from16 v20, p5

    #@2fa
    move-object/from16 v21, p6

    #@2fc
    invoke-direct/range {v15 .. v23}, Landroid/content/pm/PackageParser;->parseActivity(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;ZZ)Landroid/content/pm/PackageParser$Activity;

    #@2ff
    move-result-object v24

    #@300
    .line 1952
    .restart local v24       #a:Landroid/content/pm/PackageParser$Activity;
    if-nez v24, :cond_30b

    #@302
    .line 1953
    const/16 v3, -0x6c

    #@304
    move-object/from16 v0, p0

    #@306
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@308
    .line 1954
    const/4 v3, 0x0

    #@309
    goto/16 :goto_40

    #@30b
    .line 1957
    :cond_30b
    move-object/from16 v0, p1

    #@30d
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->receivers:Ljava/util/ArrayList;

    #@30f
    move-object/from16 v0, v24

    #@311
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@314
    goto/16 :goto_289

    #@316
    .line 1959
    .end local v24           #a:Landroid/content/pm/PackageParser$Activity;
    :cond_316
    const-string/jumbo v3, "service"

    #@319
    move-object/from16 v0, v40

    #@31b
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31e
    move-result v3

    #@31f
    if-eqz v3, :cond_33b

    #@321
    .line 1960
    invoke-direct/range {p0 .. p6}, Landroid/content/pm/PackageParser;->parseService(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$Service;

    #@324
    move-result-object v37

    #@325
    .line 1961
    .local v37, s:Landroid/content/pm/PackageParser$Service;
    if-nez v37, :cond_330

    #@327
    .line 1962
    const/16 v3, -0x6c

    #@329
    move-object/from16 v0, p0

    #@32b
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@32d
    .line 1963
    const/4 v3, 0x0

    #@32e
    goto/16 :goto_40

    #@330
    .line 1966
    :cond_330
    move-object/from16 v0, p1

    #@332
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->services:Ljava/util/ArrayList;

    #@334
    move-object/from16 v0, v37

    #@336
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@339
    goto/16 :goto_289

    #@33b
    .line 1968
    .end local v37           #s:Landroid/content/pm/PackageParser$Service;
    :cond_33b
    const-string/jumbo v3, "provider"

    #@33e
    move-object/from16 v0, v40

    #@340
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@343
    move-result v3

    #@344
    if-eqz v3, :cond_360

    #@346
    .line 1969
    invoke-direct/range {p0 .. p6}, Landroid/content/pm/PackageParser;->parseProvider(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$Provider;

    #@349
    move-result-object v34

    #@34a
    .line 1970
    .local v34, p:Landroid/content/pm/PackageParser$Provider;
    if-nez v34, :cond_355

    #@34c
    .line 1971
    const/16 v3, -0x6c

    #@34e
    move-object/from16 v0, p0

    #@350
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@352
    .line 1972
    const/4 v3, 0x0

    #@353
    goto/16 :goto_40

    #@355
    .line 1975
    :cond_355
    move-object/from16 v0, p1

    #@357
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->providers:Ljava/util/ArrayList;

    #@359
    move-object/from16 v0, v34

    #@35b
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@35e
    goto/16 :goto_289

    #@360
    .line 1977
    .end local v34           #p:Landroid/content/pm/PackageParser$Provider;
    :cond_360
    const-string v3, "activity-alias"

    #@362
    move-object/from16 v0, v40

    #@364
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@367
    move-result v3

    #@368
    if-eqz v3, :cond_384

    #@36a
    .line 1978
    invoke-direct/range {p0 .. p6}, Landroid/content/pm/PackageParser;->parseActivityAlias(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$Activity;

    #@36d
    move-result-object v24

    #@36e
    .line 1979
    .restart local v24       #a:Landroid/content/pm/PackageParser$Activity;
    if-nez v24, :cond_379

    #@370
    .line 1980
    const/16 v3, -0x6c

    #@372
    move-object/from16 v0, p0

    #@374
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@376
    .line 1981
    const/4 v3, 0x0

    #@377
    goto/16 :goto_40

    #@379
    .line 1984
    :cond_379
    move-object/from16 v0, p1

    #@37b
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@37d
    move-object/from16 v0, v24

    #@37f
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@382
    goto/16 :goto_289

    #@384
    .line 1986
    .end local v24           #a:Landroid/content/pm/PackageParser$Activity;
    :cond_384
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@387
    move-result-object v3

    #@388
    const-string/jumbo v4, "meta-data"

    #@38b
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38e
    move-result v3

    #@38f
    if-eqz v3, :cond_3b2

    #@391
    .line 1990
    move-object/from16 v0, p1

    #@393
    iget-object v10, v0, Landroid/content/pm/PackageParser$Package;->mAppMetaData:Landroid/os/Bundle;

    #@395
    move-object/from16 v6, p0

    #@397
    move-object/from16 v7, p2

    #@399
    move-object/from16 v8, p3

    #@39b
    move-object/from16 v9, p4

    #@39d
    move-object/from16 v11, p6

    #@39f
    invoke-direct/range {v6 .. v11}, Landroid/content/pm/PackageParser;->parseMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;

    #@3a2
    move-result-object v3

    #@3a3
    move-object/from16 v0, p1

    #@3a5
    iput-object v3, v0, Landroid/content/pm/PackageParser$Package;->mAppMetaData:Landroid/os/Bundle;

    #@3a7
    if-nez v3, :cond_289

    #@3a9
    .line 1992
    const/16 v3, -0x6c

    #@3ab
    move-object/from16 v0, p0

    #@3ad
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@3af
    .line 1993
    const/4 v3, 0x0

    #@3b0
    goto/16 :goto_40

    #@3b2
    .line 1996
    :cond_3b2
    const-string/jumbo v3, "uses-library"

    #@3b5
    move-object/from16 v0, v40

    #@3b7
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ba
    move-result v3

    #@3bb
    if-eqz v3, :cond_42f

    #@3bd
    .line 1997
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifestUsesLibrary:[I

    #@3bf
    move-object/from16 v0, p2

    #@3c1
    move-object/from16 v1, p4

    #@3c3
    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@3c6
    move-result-object v38

    #@3c7
    .line 2002
    const/4 v3, 0x0

    #@3c8
    move-object/from16 v0, v38

    #@3ca
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@3cd
    move-result-object v31

    #@3ce
    .line 2004
    .local v31, lname:Ljava/lang/String;
    const/4 v3, 0x1

    #@3cf
    const/4 v4, 0x1

    #@3d0
    move-object/from16 v0, v38

    #@3d2
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@3d5
    move-result v36

    #@3d6
    .line 2008
    .local v36, req:Z
    invoke-virtual/range {v38 .. v38}, Landroid/content/res/TypedArray;->recycle()V

    #@3d9
    .line 2010
    if-eqz v31, :cond_403

    #@3db
    .line 2011
    if-eqz v36, :cond_408

    #@3dd
    .line 2012
    move-object/from16 v0, p1

    #@3df
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->usesLibraries:Ljava/util/ArrayList;

    #@3e1
    if-nez v3, :cond_3ec

    #@3e3
    .line 2013
    new-instance v3, Ljava/util/ArrayList;

    #@3e5
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@3e8
    move-object/from16 v0, p1

    #@3ea
    iput-object v3, v0, Landroid/content/pm/PackageParser$Package;->usesLibraries:Ljava/util/ArrayList;

    #@3ec
    .line 2015
    :cond_3ec
    move-object/from16 v0, p1

    #@3ee
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->usesLibraries:Ljava/util/ArrayList;

    #@3f0
    move-object/from16 v0, v31

    #@3f2
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@3f5
    move-result v3

    #@3f6
    if-nez v3, :cond_403

    #@3f8
    .line 2016
    move-object/from16 v0, p1

    #@3fa
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->usesLibraries:Ljava/util/ArrayList;

    #@3fc
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@3ff
    move-result-object v4

    #@400
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@403
    .line 2028
    :cond_403
    :goto_403
    invoke-static/range {p3 .. p3}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@406
    goto/16 :goto_289

    #@408
    .line 2019
    :cond_408
    move-object/from16 v0, p1

    #@40a
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->usesOptionalLibraries:Ljava/util/ArrayList;

    #@40c
    if-nez v3, :cond_417

    #@40e
    .line 2020
    new-instance v3, Ljava/util/ArrayList;

    #@410
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@413
    move-object/from16 v0, p1

    #@415
    iput-object v3, v0, Landroid/content/pm/PackageParser$Package;->usesOptionalLibraries:Ljava/util/ArrayList;

    #@417
    .line 2022
    :cond_417
    move-object/from16 v0, p1

    #@419
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->usesOptionalLibraries:Ljava/util/ArrayList;

    #@41b
    move-object/from16 v0, v31

    #@41d
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@420
    move-result v3

    #@421
    if-nez v3, :cond_403

    #@423
    .line 2023
    move-object/from16 v0, p1

    #@425
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->usesOptionalLibraries:Ljava/util/ArrayList;

    #@427
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@42a
    move-result-object v4

    #@42b
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@42e
    goto :goto_403

    #@42f
    .line 2030
    .end local v31           #lname:Ljava/lang/String;
    .end local v36           #req:Z
    :cond_42f
    const-string/jumbo v3, "uses-package"

    #@432
    move-object/from16 v0, v40

    #@434
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@437
    move-result v3

    #@438
    if-eqz v3, :cond_43f

    #@43a
    .line 2033
    invoke-static/range {p3 .. p3}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@43d
    goto/16 :goto_289

    #@43f
    .line 2037
    :cond_43f
    const-string v3, "PackageParser"

    #@441
    new-instance v4, Ljava/lang/StringBuilder;

    #@443
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@446
    const-string v6, "Unknown element under <application>: "

    #@448
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44b
    move-result-object v4

    #@44c
    move-object/from16 v0, v40

    #@44e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@451
    move-result-object v4

    #@452
    const-string v6, " at "

    #@454
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@457
    move-result-object v4

    #@458
    move-object/from16 v0, p0

    #@45a
    iget-object v6, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@45c
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45f
    move-result-object v4

    #@460
    const-string v6, " "

    #@462
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@465
    move-result-object v4

    #@466
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@469
    move-result-object v6

    #@46a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46d
    move-result-object v4

    #@46e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@471
    move-result-object v4

    #@472
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@475
    .line 2040
    invoke-static/range {p3 .. p3}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@478
    goto/16 :goto_289

    #@47a
    .line 2051
    .end local v40           #tagName:Ljava/lang/String;
    :cond_47a
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_RESOURCE:Z

    #@47c
    if-eqz v3, :cond_4b5

    #@47e
    .line 2052
    move-object/from16 v0, v25

    #@480
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->theme:I

    #@482
    if-nez v3, :cond_4b5

    #@484
    move-object/from16 v0, p1

    #@486
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->mAppMetaData:Landroid/os/Bundle;

    #@488
    if-eqz v3, :cond_4b5

    #@48a
    .line 2053
    move-object/from16 v0, p1

    #@48c
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->mAppMetaData:Landroid/os/Bundle;

    #@48e
    const-string v4, "com.lge.theme"

    #@490
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@493
    move-result-object v29

    #@494
    .line 2054
    .local v29, lgeTheme:Ljava/lang/String;
    if-eqz v29, :cond_4b5

    #@496
    .line 2055
    const/16 v3, 0x5f

    #@498
    const/16 v4, 0x2e

    #@49a
    move-object/from16 v0, v29

    #@49c
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@49f
    move-result-object v29

    #@4a0
    .line 2056
    const-string/jumbo v3, "style"

    #@4a3
    const-string v4, "com.lge.internal"

    #@4a5
    move-object/from16 v0, p2

    #@4a7
    move-object/from16 v1, v29

    #@4a9
    invoke-virtual {v0, v1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@4ac
    move-result v41

    #@4ad
    .line 2057
    .local v41, themeResId:I
    if-eqz v41, :cond_4b5

    #@4af
    .line 2058
    move/from16 v0, v41

    #@4b1
    move-object/from16 v1, v25

    #@4b3
    iput v0, v1, Landroid/content/pm/ApplicationInfo;->theme:I

    #@4b5
    .line 2063
    .end local v29           #lgeTheme:Ljava/lang/String;
    .end local v41           #themeResId:I
    :cond_4b5
    move-object/from16 v0, v25

    #@4b7
    iget-object v3, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@4b9
    if-nez v3, :cond_4e2

    #@4bb
    move-object/from16 v0, p1

    #@4bd
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->mAppMetaData:Landroid/os/Bundle;

    #@4bf
    if-eqz v3, :cond_4e2

    #@4c1
    .line 2064
    move-object/from16 v0, p1

    #@4c3
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->mAppMetaData:Landroid/os/Bundle;

    #@4c5
    const-string v4, "com.lge.layoutdirection"

    #@4c7
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@4ca
    move-result-object v30

    #@4cb
    .line 2065
    .local v30, lgertlmetadata:Ljava/lang/String;
    if-eqz v30, :cond_4e2

    #@4cd
    const-string v3, "Locale"

    #@4cf
    move-object/from16 v0, v30

    #@4d1
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@4d4
    move-result v3

    #@4d5
    if-eqz v3, :cond_4e2

    #@4d7
    .line 2066
    move-object/from16 v0, v25

    #@4d9
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@4db
    const/high16 v4, 0x40

    #@4dd
    or-int/2addr v3, v4

    #@4de
    move-object/from16 v0, v25

    #@4e0
    iput v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@4e2
    .line 2070
    .end local v30           #lgertlmetadata:Ljava/lang/String;
    :cond_4e2
    const/4 v3, 0x1

    #@4e3
    goto/16 :goto_40
.end method

.method private parseInstrumentation(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;[Ljava/lang/String;)Landroid/content/pm/PackageParser$Instrumentation;
    .registers 15
    .parameter "owner"
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1682
    sget-object v0, Lcom/android/internal/R$styleable;->AndroidManifestInstrumentation:[I

    #@2
    invoke-virtual {p2, p4, v0}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@5
    move-result-object v7

    #@6
    .line 1685
    .local v7, sa:Landroid/content/res/TypedArray;
    iget-object v0, p0, Landroid/content/pm/PackageParser;->mParseInstrumentationArgs:Landroid/content/pm/PackageParser$ParsePackageItemArgs;

    #@8
    if-nez v0, :cond_1d

    #@a
    .line 1686
    new-instance v0, Landroid/content/pm/PackageParser$ParsePackageItemArgs;

    #@c
    const/4 v3, 0x2

    #@d
    const/4 v4, 0x0

    #@e
    const/4 v5, 0x1

    #@f
    const/4 v6, 0x6

    #@10
    move-object v1, p1

    #@11
    move-object v2, p5

    #@12
    invoke-direct/range {v0 .. v6}, Landroid/content/pm/PackageParser$ParsePackageItemArgs;-><init>(Landroid/content/pm/PackageParser$Package;[Ljava/lang/String;IIII)V

    #@15
    iput-object v0, p0, Landroid/content/pm/PackageParser;->mParseInstrumentationArgs:Landroid/content/pm/PackageParser$ParsePackageItemArgs;

    #@17
    .line 1691
    iget-object v0, p0, Landroid/content/pm/PackageParser;->mParseInstrumentationArgs:Landroid/content/pm/PackageParser$ParsePackageItemArgs;

    #@19
    const-string v1, "<instrumentation>"

    #@1b
    iput-object v1, v0, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->tag:Ljava/lang/String;

    #@1d
    .line 1694
    :cond_1d
    iget-object v0, p0, Landroid/content/pm/PackageParser;->mParseInstrumentationArgs:Landroid/content/pm/PackageParser$ParsePackageItemArgs;

    #@1f
    iput-object v7, v0, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@21
    .line 1696
    new-instance v5, Landroid/content/pm/PackageParser$Instrumentation;

    #@23
    iget-object v0, p0, Landroid/content/pm/PackageParser;->mParseInstrumentationArgs:Landroid/content/pm/PackageParser$ParsePackageItemArgs;

    #@25
    new-instance v1, Landroid/content/pm/InstrumentationInfo;

    #@27
    invoke-direct {v1}, Landroid/content/pm/InstrumentationInfo;-><init>()V

    #@2a
    invoke-direct {v5, v0, v1}, Landroid/content/pm/PackageParser$Instrumentation;-><init>(Landroid/content/pm/PackageParser$ParsePackageItemArgs;Landroid/content/pm/InstrumentationInfo;)V

    #@2d
    .line 1698
    .local v5, a:Landroid/content/pm/PackageParser$Instrumentation;
    const/4 v0, 0x0

    #@2e
    aget-object v0, p5, v0

    #@30
    if-eqz v0, :cond_3b

    #@32
    .line 1699
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    #@35
    .line 1700
    const/16 v0, -0x6c

    #@37
    iput v0, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@39
    .line 1701
    const/4 v5, 0x0

    #@3a
    .line 1735
    .end local v5           #a:Landroid/content/pm/PackageParser$Instrumentation;
    :goto_3a
    return-object v5

    #@3b
    .line 1707
    .restart local v5       #a:Landroid/content/pm/PackageParser$Instrumentation;
    :cond_3b
    const/4 v0, 0x3

    #@3c
    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@3f
    move-result-object v8

    #@40
    .line 1709
    .local v8, str:Ljava/lang/String;
    iget-object v1, v5, Landroid/content/pm/PackageParser$Instrumentation;->info:Landroid/content/pm/InstrumentationInfo;

    #@42
    if-eqz v8, :cond_72

    #@44
    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@47
    move-result-object v0

    #@48
    :goto_48
    iput-object v0, v1, Landroid/content/pm/InstrumentationInfo;->targetPackage:Ljava/lang/String;

    #@4a
    .line 1711
    iget-object v0, v5, Landroid/content/pm/PackageParser$Instrumentation;->info:Landroid/content/pm/InstrumentationInfo;

    #@4c
    const/4 v1, 0x4

    #@4d
    const/4 v2, 0x0

    #@4e
    invoke-virtual {v7, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@51
    move-result v1

    #@52
    iput-boolean v1, v0, Landroid/content/pm/InstrumentationInfo;->handleProfiling:Z

    #@54
    .line 1715
    iget-object v0, v5, Landroid/content/pm/PackageParser$Instrumentation;->info:Landroid/content/pm/InstrumentationInfo;

    #@56
    const/4 v1, 0x5

    #@57
    const/4 v2, 0x0

    #@58
    invoke-virtual {v7, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@5b
    move-result v1

    #@5c
    iput-boolean v1, v0, Landroid/content/pm/InstrumentationInfo;->functionalTest:Z

    #@5e
    .line 1719
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    #@61
    .line 1721
    iget-object v0, v5, Landroid/content/pm/PackageParser$Instrumentation;->info:Landroid/content/pm/InstrumentationInfo;

    #@63
    iget-object v0, v0, Landroid/content/pm/InstrumentationInfo;->targetPackage:Ljava/lang/String;

    #@65
    if-nez v0, :cond_74

    #@67
    .line 1722
    const/4 v0, 0x0

    #@68
    const-string v1, "<instrumentation> does not specify targetPackage"

    #@6a
    aput-object v1, p5, v0

    #@6c
    .line 1723
    const/16 v0, -0x6c

    #@6e
    iput v0, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@70
    .line 1724
    const/4 v5, 0x0

    #@71
    goto :goto_3a

    #@72
    .line 1709
    :cond_72
    const/4 v0, 0x0

    #@73
    goto :goto_48

    #@74
    .line 1727
    :cond_74
    const-string v4, "<instrumentation>"

    #@76
    move-object v0, p0

    #@77
    move-object v1, p2

    #@78
    move-object v2, p3

    #@79
    move-object v3, p4

    #@7a
    move-object v6, p5

    #@7b
    invoke-direct/range {v0 .. v6}, Landroid/content/pm/PackageParser;->parseAllMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Ljava/lang/String;Landroid/content/pm/PackageParser$Component;[Ljava/lang/String;)Z

    #@7e
    move-result v0

    #@7f
    if-nez v0, :cond_87

    #@81
    .line 1729
    const/16 v0, -0x6c

    #@83
    iput v0, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@85
    .line 1730
    const/4 v5, 0x0

    #@86
    goto :goto_3a

    #@87
    .line 1733
    :cond_87
    iget-object v0, p1, Landroid/content/pm/PackageParser$Package;->instrumentation:Ljava/util/ArrayList;

    #@89
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8c
    goto :goto_3a
.end method

.method private parseIntent(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ILandroid/content/pm/PackageParser$IntentInfo;[Ljava/lang/String;Z)Z
    .registers 24
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "flags"
    .parameter "outInfo"
    .parameter "outError"
    .parameter "isActivity"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3131
    sget-object v13, Lcom/android/internal/R$styleable;->AndroidManifestIntentFilter:[I

    #@2
    move-object/from16 v0, p1

    #@4
    move-object/from16 v1, p3

    #@6
    invoke-virtual {v0, v1, v13}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v8

    #@a
    .line 3134
    .local v8, sa:Landroid/content/res/TypedArray;
    const/4 v13, 0x2

    #@b
    const/4 v14, 0x0

    #@c
    invoke-virtual {v8, v13, v14}, Landroid/content/res/TypedArray;->getInt(II)I

    #@f
    move-result v7

    #@10
    .line 3136
    .local v7, priority:I
    move-object/from16 v0, p5

    #@12
    invoke-virtual {v0, v7}, Landroid/content/pm/PackageParser$IntentInfo;->setPriority(I)V

    #@15
    .line 3138
    const/4 v13, 0x0

    #@16
    invoke-virtual {v8, v13}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@19
    move-result-object v11

    #@1a
    .line 3140
    .local v11, v:Landroid/util/TypedValue;
    if-eqz v11, :cond_2c

    #@1c
    iget v13, v11, Landroid/util/TypedValue;->resourceId:I

    #@1e
    move-object/from16 v0, p5

    #@20
    iput v13, v0, Landroid/content/pm/PackageParser$IntentInfo;->labelRes:I

    #@22
    if-nez v13, :cond_2c

    #@24
    .line 3141
    invoke-virtual {v11}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@27
    move-result-object v13

    #@28
    move-object/from16 v0, p5

    #@2a
    iput-object v13, v0, Landroid/content/pm/PackageParser$IntentInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@2c
    .line 3144
    :cond_2c
    const/4 v13, 0x1

    #@2d
    const/4 v14, 0x0

    #@2e
    invoke-virtual {v8, v13, v14}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@31
    move-result v13

    #@32
    move-object/from16 v0, p5

    #@34
    iput v13, v0, Landroid/content/pm/PackageParser$IntentInfo;->icon:I

    #@36
    .line 3147
    const/4 v13, 0x3

    #@37
    const/4 v14, 0x0

    #@38
    invoke-virtual {v8, v13, v14}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@3b
    move-result v13

    #@3c
    move-object/from16 v0, p5

    #@3e
    iput v13, v0, Landroid/content/pm/PackageParser$IntentInfo;->logo:I

    #@40
    .line 3150
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@43
    .line 3152
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@46
    move-result v5

    #@47
    .line 3155
    .local v5, outerDepth:I
    :cond_47
    :goto_47
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@4a
    move-result v10

    #@4b
    .local v10, type:I
    const/4 v13, 0x1

    #@4c
    if-eq v10, v13, :cond_16f

    #@4e
    const/4 v13, 0x3

    #@4f
    if-ne v10, v13, :cond_57

    #@51
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@54
    move-result v13

    #@55
    if-le v13, v5, :cond_16f

    #@57
    .line 3156
    :cond_57
    const/4 v13, 0x3

    #@58
    if-eq v10, v13, :cond_47

    #@5a
    const/4 v13, 0x4

    #@5b
    if-eq v10, v13, :cond_47

    #@5d
    .line 3160
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@60
    move-result-object v4

    #@61
    .line 3161
    .local v4, nodeName:Ljava/lang/String;
    const-string v13, "action"

    #@63
    invoke-virtual {v4, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v13

    #@67
    if-eqz v13, :cond_8a

    #@69
    .line 3162
    const-string v13, "http://schemas.android.com/apk/res/android"

    #@6b
    const-string/jumbo v14, "name"

    #@6e
    move-object/from16 v0, p3

    #@70
    invoke-interface {v0, v13, v14}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@73
    move-result-object v12

    #@74
    .line 3164
    .local v12, value:Ljava/lang/String;
    if-eqz v12, :cond_7a

    #@76
    const-string v13, ""

    #@78
    if-ne v12, v13, :cond_81

    #@7a
    .line 3165
    :cond_7a
    const/4 v13, 0x0

    #@7b
    const-string v14, "No value supplied for <android:name>"

    #@7d
    aput-object v14, p6, v13

    #@7f
    .line 3166
    const/4 v13, 0x0

    #@80
    .line 3260
    .end local v4           #nodeName:Ljava/lang/String;
    .end local v12           #value:Ljava/lang/String;
    :goto_80
    return v13

    #@81
    .line 3168
    .restart local v4       #nodeName:Ljava/lang/String;
    .restart local v12       #value:Ljava/lang/String;
    :cond_81
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@84
    .line 3170
    move-object/from16 v0, p5

    #@86
    invoke-virtual {v0, v12}, Landroid/content/pm/PackageParser$IntentInfo;->addAction(Ljava/lang/String;)V

    #@89
    goto :goto_47

    #@8a
    .line 3171
    .end local v12           #value:Ljava/lang/String;
    :cond_8a
    const-string v13, "category"

    #@8c
    invoke-virtual {v4, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v13

    #@90
    if-eqz v13, :cond_b3

    #@92
    .line 3172
    const-string v13, "http://schemas.android.com/apk/res/android"

    #@94
    const-string/jumbo v14, "name"

    #@97
    move-object/from16 v0, p3

    #@99
    invoke-interface {v0, v13, v14}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9c
    move-result-object v12

    #@9d
    .line 3174
    .restart local v12       #value:Ljava/lang/String;
    if-eqz v12, :cond_a3

    #@9f
    const-string v13, ""

    #@a1
    if-ne v12, v13, :cond_aa

    #@a3
    .line 3175
    :cond_a3
    const/4 v13, 0x0

    #@a4
    const-string v14, "No value supplied for <android:name>"

    #@a6
    aput-object v14, p6, v13

    #@a8
    .line 3176
    const/4 v13, 0x0

    #@a9
    goto :goto_80

    #@aa
    .line 3178
    :cond_aa
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@ad
    .line 3180
    move-object/from16 v0, p5

    #@af
    invoke-virtual {v0, v12}, Landroid/content/pm/PackageParser$IntentInfo;->addCategory(Ljava/lang/String;)V

    #@b2
    goto :goto_47

    #@b3
    .line 3182
    .end local v12           #value:Ljava/lang/String;
    :cond_b3
    const-string v13, "data"

    #@b5
    invoke-virtual {v4, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b8
    move-result v13

    #@b9
    if-eqz v13, :cond_132

    #@bb
    .line 3183
    sget-object v13, Lcom/android/internal/R$styleable;->AndroidManifestData:[I

    #@bd
    move-object/from16 v0, p1

    #@bf
    move-object/from16 v1, p3

    #@c1
    invoke-virtual {v0, v1, v13}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@c4
    move-result-object v8

    #@c5
    .line 3186
    const/4 v13, 0x0

    #@c6
    const/4 v14, 0x0

    #@c7
    invoke-virtual {v8, v13, v14}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@ca
    move-result-object v9

    #@cb
    .line 3188
    .local v9, str:Ljava/lang/String;
    if-eqz v9, :cond_d2

    #@cd
    .line 3190
    :try_start_cd
    move-object/from16 v0, p5

    #@cf
    invoke-virtual {v0, v9}, Landroid/content/pm/PackageParser$IntentInfo;->addDataType(Ljava/lang/String;)V
    :try_end_d2
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_cd .. :try_end_d2} :catch_124

    #@d2
    .line 3198
    :cond_d2
    const/4 v13, 0x1

    #@d3
    const/4 v14, 0x0

    #@d4
    invoke-virtual {v8, v13, v14}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@d7
    move-result-object v9

    #@d8
    .line 3200
    if-eqz v9, :cond_df

    #@da
    .line 3201
    move-object/from16 v0, p5

    #@dc
    invoke-virtual {v0, v9}, Landroid/content/pm/PackageParser$IntentInfo;->addDataScheme(Ljava/lang/String;)V

    #@df
    .line 3204
    :cond_df
    const/4 v13, 0x2

    #@e0
    const/4 v14, 0x0

    #@e1
    invoke-virtual {v8, v13, v14}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@e4
    move-result-object v3

    #@e5
    .line 3206
    .local v3, host:Ljava/lang/String;
    const/4 v13, 0x3

    #@e6
    const/4 v14, 0x0

    #@e7
    invoke-virtual {v8, v13, v14}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@ea
    move-result-object v6

    #@eb
    .line 3208
    .local v6, port:Ljava/lang/String;
    if-eqz v3, :cond_f2

    #@ed
    .line 3209
    move-object/from16 v0, p5

    #@ef
    invoke-virtual {v0, v3, v6}, Landroid/content/pm/PackageParser$IntentInfo;->addDataAuthority(Ljava/lang/String;Ljava/lang/String;)V

    #@f2
    .line 3212
    :cond_f2
    const/4 v13, 0x4

    #@f3
    const/4 v14, 0x0

    #@f4
    invoke-virtual {v8, v13, v14}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@f7
    move-result-object v9

    #@f8
    .line 3214
    if-eqz v9, :cond_100

    #@fa
    .line 3215
    const/4 v13, 0x0

    #@fb
    move-object/from16 v0, p5

    #@fd
    invoke-virtual {v0, v9, v13}, Landroid/content/pm/PackageParser$IntentInfo;->addDataPath(Ljava/lang/String;I)V

    #@100
    .line 3218
    :cond_100
    const/4 v13, 0x5

    #@101
    const/4 v14, 0x0

    #@102
    invoke-virtual {v8, v13, v14}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@105
    move-result-object v9

    #@106
    .line 3220
    if-eqz v9, :cond_10e

    #@108
    .line 3221
    const/4 v13, 0x1

    #@109
    move-object/from16 v0, p5

    #@10b
    invoke-virtual {v0, v9, v13}, Landroid/content/pm/PackageParser$IntentInfo;->addDataPath(Ljava/lang/String;I)V

    #@10e
    .line 3224
    :cond_10e
    const/4 v13, 0x6

    #@10f
    const/4 v14, 0x0

    #@110
    invoke-virtual {v8, v13, v14}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@113
    move-result-object v9

    #@114
    .line 3226
    if-eqz v9, :cond_11c

    #@116
    .line 3227
    const/4 v13, 0x2

    #@117
    move-object/from16 v0, p5

    #@119
    invoke-virtual {v0, v9, v13}, Landroid/content/pm/PackageParser$IntentInfo;->addDataPath(Ljava/lang/String;I)V

    #@11c
    .line 3230
    :cond_11c
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@11f
    .line 3231
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@122
    goto/16 :goto_47

    #@124
    .line 3191
    .end local v3           #host:Ljava/lang/String;
    .end local v6           #port:Ljava/lang/String;
    :catch_124
    move-exception v2

    #@125
    .line 3192
    .local v2, e:Landroid/content/IntentFilter$MalformedMimeTypeException;
    const/4 v13, 0x0

    #@126
    invoke-virtual {v2}, Landroid/content/IntentFilter$MalformedMimeTypeException;->toString()Ljava/lang/String;

    #@129
    move-result-object v14

    #@12a
    aput-object v14, p6, v13

    #@12c
    .line 3193
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@12f
    .line 3194
    const/4 v13, 0x0

    #@130
    goto/16 :goto_80

    #@132
    .line 3233
    .end local v2           #e:Landroid/content/IntentFilter$MalformedMimeTypeException;
    .end local v9           #str:Ljava/lang/String;
    :cond_132
    const-string v13, "PackageParser"

    #@134
    new-instance v14, Ljava/lang/StringBuilder;

    #@136
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@139
    const-string v15, "Unknown element under <intent-filter>: "

    #@13b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v14

    #@13f
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@142
    move-result-object v15

    #@143
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v14

    #@147
    const-string v15, " at "

    #@149
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v14

    #@14d
    move-object/from16 v0, p0

    #@14f
    iget-object v15, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@151
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v14

    #@155
    const-string v15, " "

    #@157
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v14

    #@15b
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@15e
    move-result-object v15

    #@15f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v14

    #@163
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@166
    move-result-object v14

    #@167
    invoke-static {v13, v14}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16a
    .line 3236
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@16d
    goto/16 :goto_47

    #@16f
    .line 3243
    .end local v4           #nodeName:Ljava/lang/String;
    :cond_16f
    const-string v13, "android.intent.category.DEFAULT"

    #@171
    move-object/from16 v0, p5

    #@173
    invoke-virtual {v0, v13}, Landroid/content/pm/PackageParser$IntentInfo;->hasCategory(Ljava/lang/String;)Z

    #@176
    move-result v13

    #@177
    move-object/from16 v0, p5

    #@179
    iput-boolean v13, v0, Landroid/content/pm/PackageParser$IntentInfo;->hasDefault:Z

    #@17b
    .line 3260
    const/4 v13, 0x1

    #@17c
    goto/16 :goto_80
.end method

.method private parseMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;
    .registers 15
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "data"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v6, 0x0

    #@3
    .line 3006
    sget-object v7, Lcom/android/internal/R$styleable;->AndroidManifestMetaData:[I

    #@5
    invoke-virtual {p1, p3, v7}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@8
    move-result-object v2

    #@9
    .line 3009
    .local v2, sa:Landroid/content/res/TypedArray;
    if-nez p4, :cond_10

    #@b
    .line 3010
    new-instance p4, Landroid/os/Bundle;

    #@d
    .end local p4
    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    #@10
    .line 3013
    .restart local p4
    :cond_10
    invoke-virtual {v2, v6, v6}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 3015
    .local v1, name:Ljava/lang/String;
    if-nez v1, :cond_1e

    #@16
    .line 3016
    const-string v5, "<meta-data> requires an android:name attribute"

    #@18
    aput-object v5, p5, v6

    #@1a
    .line 3017
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    #@1d
    .line 3063
    :goto_1d
    return-object v4

    #@1e
    .line 3021
    :cond_1e
    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    .line 3023
    const/4 v7, 0x2

    #@23
    invoke-virtual {v2, v7}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@26
    move-result-object v3

    #@27
    .line 3025
    .local v3, v:Landroid/util/TypedValue;
    if-eqz v3, :cond_3a

    #@29
    iget v7, v3, Landroid/util/TypedValue;->resourceId:I

    #@2b
    if-eqz v7, :cond_3a

    #@2d
    .line 3027
    iget v4, v3, Landroid/util/TypedValue;->resourceId:I

    #@2f
    invoke-virtual {p4, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@32
    .line 3059
    :goto_32
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    #@35
    .line 3061
    invoke-static {p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@38
    move-object v4, p4

    #@39
    .line 3063
    goto :goto_1d

    #@3a
    .line 3029
    :cond_3a
    invoke-virtual {v2, v5}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@3d
    move-result-object v3

    #@3e
    .line 3032
    if-eqz v3, :cond_bf

    #@40
    .line 3033
    iget v7, v3, Landroid/util/TypedValue;->type:I

    #@42
    const/4 v8, 0x3

    #@43
    if-ne v7, v8, :cond_57

    #@45
    .line 3034
    invoke-virtual {v3}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@48
    move-result-object v0

    #@49
    .line 3035
    .local v0, cs:Ljava/lang/CharSequence;
    if-eqz v0, :cond_53

    #@4b
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    :cond_53
    invoke-virtual {p4, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    goto :goto_32

    #@57
    .line 3036
    .end local v0           #cs:Ljava/lang/CharSequence;
    :cond_57
    iget v4, v3, Landroid/util/TypedValue;->type:I

    #@59
    const/16 v7, 0x12

    #@5b
    if-ne v4, v7, :cond_68

    #@5d
    .line 3037
    iget v4, v3, Landroid/util/TypedValue;->data:I

    #@5f
    if-eqz v4, :cond_66

    #@61
    move v4, v5

    #@62
    :goto_62
    invoke-virtual {p4, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@65
    goto :goto_32

    #@66
    :cond_66
    move v4, v6

    #@67
    goto :goto_62

    #@68
    .line 3038
    :cond_68
    iget v4, v3, Landroid/util/TypedValue;->type:I

    #@6a
    const/16 v5, 0x10

    #@6c
    if-lt v4, v5, :cond_7a

    #@6e
    iget v4, v3, Landroid/util/TypedValue;->type:I

    #@70
    const/16 v5, 0x1f

    #@72
    if-gt v4, v5, :cond_7a

    #@74
    .line 3040
    iget v4, v3, Landroid/util/TypedValue;->data:I

    #@76
    invoke-virtual {p4, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@79
    goto :goto_32

    #@7a
    .line 3041
    :cond_7a
    iget v4, v3, Landroid/util/TypedValue;->type:I

    #@7c
    const/4 v5, 0x4

    #@7d
    if-ne v4, v5, :cond_87

    #@7f
    .line 3042
    invoke-virtual {v3}, Landroid/util/TypedValue;->getFloat()F

    #@82
    move-result v4

    #@83
    invoke-virtual {p4, v1, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@86
    goto :goto_32

    #@87
    .line 3045
    :cond_87
    const-string v4, "PackageParser"

    #@89
    new-instance v5, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v6, "<meta-data> only supports string, integer, float, color, boolean, and resource reference types: "

    #@90
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v5

    #@94
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@97
    move-result-object v6

    #@98
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v5

    #@9c
    const-string v6, " at "

    #@9e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v5

    #@a2
    iget-object v6, p0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@a4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v5

    #@a8
    const-string v6, " "

    #@aa
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v5

    #@ae
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@b1
    move-result-object v6

    #@b2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v5

    #@b6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v5

    #@ba
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@bd
    goto/16 :goto_32

    #@bf
    .line 3054
    :cond_bf
    const-string v4, "<meta-data> requires an android:value or android:resource attribute"

    #@c1
    aput-object v4, p5, v6

    #@c3
    .line 3055
    const/4 p4, 0x0

    #@c4
    goto/16 :goto_32
.end method

.method private parsePackage(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$Package;
    .registers 53
    .parameter "res"
    .parameter "parser"
    .parameter "flags"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 940
    move-object/from16 v7, p2

    #@2
    .line 942
    .local v7, attrs:Landroid/util/AttributeSet;
    const/4 v3, 0x0

    #@3
    move-object/from16 v0, p0

    #@5
    iput-object v3, v0, Landroid/content/pm/PackageParser;->mParseInstrumentationArgs:Landroid/content/pm/PackageParser$ParsePackageItemArgs;

    #@7
    .line 943
    const/4 v3, 0x0

    #@8
    move-object/from16 v0, p0

    #@a
    iput-object v3, v0, Landroid/content/pm/PackageParser;->mParseActivityArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@c
    .line 944
    const/4 v3, 0x0

    #@d
    move-object/from16 v0, p0

    #@f
    iput-object v3, v0, Landroid/content/pm/PackageParser;->mParseServiceArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@11
    .line 945
    const/4 v3, 0x0

    #@12
    move-object/from16 v0, p0

    #@14
    iput-object v3, v0, Landroid/content/pm/PackageParser;->mParseProviderArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@16
    .line 947
    move-object/from16 v0, p2

    #@18
    move/from16 v1, p3

    #@1a
    move-object/from16 v2, p4

    #@1c
    invoke-static {v0, v7, v1, v2}, Landroid/content/pm/PackageParser;->parsePackageName(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v34

    #@20
    .line 948
    .local v34, pkgName:Ljava/lang/String;
    if-nez v34, :cond_2a

    #@22
    .line 949
    const/16 v3, -0x6a

    #@24
    move-object/from16 v0, p0

    #@26
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@28
    .line 950
    const/4 v4, 0x0

    #@29
    .line 1434
    :cond_29
    :goto_29
    return-object v4

    #@2a
    .line 954
    :cond_2a
    move-object/from16 v0, p0

    #@2c
    iget-boolean v3, v0, Landroid/content/pm/PackageParser;->mOnlyCoreApps:Z

    #@2e
    if-eqz v3, :cond_41

    #@30
    .line 955
    const/4 v3, 0x0

    #@31
    const-string v5, "coreApp"

    #@33
    const/4 v6, 0x0

    #@34
    invoke-interface {v7, v3, v5, v6}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@37
    move-result v19

    #@38
    .line 956
    .local v19, core:Z
    if-nez v19, :cond_41

    #@3a
    .line 957
    const/4 v3, 0x1

    #@3b
    move-object/from16 v0, p0

    #@3d
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@3f
    .line 958
    const/4 v4, 0x0

    #@40
    goto :goto_29

    #@41
    .line 962
    .end local v19           #core:Z
    :cond_41
    new-instance v4, Landroid/content/pm/PackageParser$Package;

    #@43
    move-object/from16 v0, v34

    #@45
    invoke-direct {v4, v0}, Landroid/content/pm/PackageParser$Package;-><init>(Ljava/lang/String;)V

    #@48
    .line 963
    .local v4, pkg:Landroid/content/pm/PackageParser$Package;
    const/16 v21, 0x0

    #@4a
    .line 965
    .local v21, foundApp:Z
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifest:[I

    #@4c
    move-object/from16 v0, p1

    #@4e
    invoke-virtual {v0, v7, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@51
    move-result-object v36

    #@52
    .line 967
    .local v36, sa:Landroid/content/res/TypedArray;
    const/4 v3, 0x1

    #@53
    const/4 v5, 0x0

    #@54
    move-object/from16 v0, v36

    #@56
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@59
    move-result v3

    #@5a
    iput v3, v4, Landroid/content/pm/PackageParser$Package;->mVersionCode:I

    #@5c
    .line 969
    const/4 v3, 0x2

    #@5d
    const/4 v5, 0x0

    #@5e
    move-object/from16 v0, v36

    #@60
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    iput-object v3, v4, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    #@66
    .line 971
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    #@68
    if-eqz v3, :cond_72

    #@6a
    .line 972
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    #@6c
    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@6f
    move-result-object v3

    #@70
    iput-object v3, v4, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    #@72
    .line 974
    :cond_72
    const/4 v3, 0x0

    #@73
    const/4 v5, 0x0

    #@74
    move-object/from16 v0, v36

    #@76
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@79
    move-result-object v38

    #@7a
    .line 978
    .local v38, str:Ljava/lang/String;
    const-string v3, "com.felicanetworks.mfc"

    #@7c
    move-object/from16 v0, v34

    #@7e
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@81
    move-result v3

    #@82
    if-eqz v3, :cond_86

    #@84
    .line 979
    const-string v38, "android.uid.felica"

    #@86
    .line 981
    :cond_86
    const-string v3, "com.felicanetworks.mfm"

    #@88
    move-object/from16 v0, v34

    #@8a
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8d
    move-result v3

    #@8e
    if-eqz v3, :cond_92

    #@90
    .line 982
    const-string v38, "android.uid.felica.mfm"

    #@92
    .line 984
    :cond_92
    const-string v3, "com.felicanetworks.mfs"

    #@94
    move-object/from16 v0, v34

    #@96
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@99
    move-result v3

    #@9a
    if-eqz v3, :cond_9e

    #@9c
    .line 985
    const-string v38, "android.uid.felica.mfs"

    #@9e
    .line 987
    :cond_9e
    const-string v3, "com.felicanetworks.mfw.a.boot"

    #@a0
    move-object/from16 v0, v34

    #@a2
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a5
    move-result v3

    #@a6
    if-eqz v3, :cond_aa

    #@a8
    .line 988
    const-string v38, "android.uid.felica.mfw"

    #@aa
    .line 990
    :cond_aa
    const-string v3, "com.nttdocomo.android.felicaremotelock"

    #@ac
    move-object/from16 v0, v34

    #@ae
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b1
    move-result v3

    #@b2
    if-eqz v3, :cond_b6

    #@b4
    .line 991
    const-string v38, "android.uid.felica.felicalock"

    #@b6
    .line 995
    :cond_b6
    if-eqz v38, :cond_10e

    #@b8
    invoke-virtual/range {v38 .. v38}, Ljava/lang/String;->length()I

    #@bb
    move-result v3

    #@bc
    if-lez v3, :cond_10e

    #@be
    .line 996
    const/4 v3, 0x1

    #@bf
    move-object/from16 v0, v38

    #@c1
    invoke-static {v0, v3}, Landroid/content/pm/PackageParser;->validateName(Ljava/lang/String;Z)Ljava/lang/String;

    #@c4
    move-result-object v29

    #@c5
    .line 997
    .local v29, nameError:Ljava/lang/String;
    if-eqz v29, :cond_fe

    #@c7
    const-string v3, "android"

    #@c9
    move-object/from16 v0, v34

    #@cb
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ce
    move-result v3

    #@cf
    if-nez v3, :cond_fe

    #@d1
    .line 998
    const/4 v3, 0x0

    #@d2
    new-instance v5, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v6, "<manifest> specifies bad sharedUserId name \""

    #@d9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v5

    #@dd
    move-object/from16 v0, v38

    #@df
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v5

    #@e3
    const-string v6, "\": "

    #@e5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v5

    #@e9
    move-object/from16 v0, v29

    #@eb
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v5

    #@ef
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v5

    #@f3
    aput-object v5, p4, v3

    #@f5
    .line 1000
    const/16 v3, -0x6b

    #@f7
    move-object/from16 v0, p0

    #@f9
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@fb
    .line 1001
    const/4 v4, 0x0

    #@fc
    goto/16 :goto_29

    #@fe
    .line 1003
    :cond_fe
    invoke-virtual/range {v38 .. v38}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@101
    move-result-object v3

    #@102
    iput-object v3, v4, Landroid/content/pm/PackageParser$Package;->mSharedUserId:Ljava/lang/String;

    #@104
    .line 1004
    const/4 v3, 0x3

    #@105
    const/4 v5, 0x0

    #@106
    move-object/from16 v0, v36

    #@108
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@10b
    move-result v3

    #@10c
    iput v3, v4, Landroid/content/pm/PackageParser$Package;->mSharedUserLabel:I

    #@10e
    .line 1007
    .end local v29           #nameError:Ljava/lang/String;
    :cond_10e
    invoke-virtual/range {v36 .. v36}, Landroid/content/res/TypedArray;->recycle()V

    #@111
    .line 1009
    const/4 v3, 0x4

    #@112
    const/4 v5, -0x1

    #@113
    move-object/from16 v0, v36

    #@115
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@118
    move-result v3

    #@119
    iput v3, v4, Landroid/content/pm/PackageParser$Package;->installLocation:I

    #@11b
    .line 1012
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@11d
    iget v5, v4, Landroid/content/pm/PackageParser$Package;->installLocation:I

    #@11f
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->installLocation:I

    #@121
    .line 1015
    and-int/lit8 v3, p3, 0x10

    #@123
    if-eqz v3, :cond_12e

    #@125
    .line 1016
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@127
    iget v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@129
    const/high16 v6, 0x2000

    #@12b
    or-int/2addr v5, v6

    #@12c
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@12e
    .line 1020
    :cond_12e
    and-int/lit8 v3, p3, 0x20

    #@130
    if-eqz v3, :cond_13b

    #@132
    .line 1021
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@134
    iget v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@136
    const/high16 v6, 0x4

    #@138
    or-int/2addr v5, v6

    #@139
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@13b
    .line 1025
    :cond_13b
    const/16 v41, 0x1

    #@13d
    .line 1026
    .local v41, supportsSmallScreens:I
    const/16 v40, 0x1

    #@13f
    .line 1027
    .local v40, supportsNormalScreens:I
    const/16 v39, 0x1

    #@141
    .line 1028
    .local v39, supportsLargeScreens:I
    const/16 v42, 0x1

    #@143
    .line 1029
    .local v42, supportsXLargeScreens:I
    const/16 v35, 0x1

    #@145
    .line 1030
    .local v35, resizeable:I
    const/16 v17, 0x1

    #@147
    .line 1032
    .local v17, anyDensity:I
    invoke-interface/range {p2 .. p2}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@14a
    move-result v32

    #@14b
    .line 1034
    .local v32, outerDepth:I
    :cond_14b
    :goto_14b
    invoke-interface/range {p2 .. p2}, Landroid/content/res/XmlResourceParser;->next()I

    #@14e
    move-result v46

    #@14f
    .local v46, type:I
    const/4 v3, 0x1

    #@150
    move/from16 v0, v46

    #@152
    if-eq v0, v3, :cond_65d

    #@154
    const/4 v3, 0x3

    #@155
    move/from16 v0, v46

    #@157
    if-ne v0, v3, :cond_161

    #@159
    invoke-interface/range {p2 .. p2}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@15c
    move-result v3

    #@15d
    move/from16 v0, v32

    #@15f
    if-le v3, v0, :cond_65d

    #@161
    .line 1035
    :cond_161
    const/4 v3, 0x3

    #@162
    move/from16 v0, v46

    #@164
    if-eq v0, v3, :cond_14b

    #@166
    const/4 v3, 0x4

    #@167
    move/from16 v0, v46

    #@169
    if-eq v0, v3, :cond_14b

    #@16b
    .line 1039
    invoke-interface/range {p2 .. p2}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@16e
    move-result-object v43

    #@16f
    .line 1040
    .local v43, tagName:Ljava/lang/String;
    const-string v3, "application"

    #@171
    move-object/from16 v0, v43

    #@173
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@176
    move-result v3

    #@177
    if-eqz v3, :cond_19b

    #@179
    .line 1041
    if-eqz v21, :cond_186

    #@17b
    .line 1047
    const-string v3, "PackageParser"

    #@17d
    const-string v5, "<manifest> has more than one <application>"

    #@17f
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@182
    .line 1048
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@185
    goto :goto_14b

    #@186
    .line 1053
    :cond_186
    const/16 v21, 0x1

    #@188
    move-object/from16 v3, p0

    #@18a
    move-object/from16 v5, p1

    #@18c
    move-object/from16 v6, p2

    #@18e
    move/from16 v8, p3

    #@190
    move-object/from16 v9, p4

    #@192
    .line 1054
    invoke-direct/range {v3 .. v9}, Landroid/content/pm/PackageParser;->parseApplication(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Z

    #@195
    move-result v3

    #@196
    if-nez v3, :cond_14b

    #@198
    .line 1055
    const/4 v4, 0x0

    #@199
    goto/16 :goto_29

    #@19b
    .line 1057
    :cond_19b
    const-string/jumbo v3, "permission-group"

    #@19e
    move-object/from16 v0, v43

    #@1a0
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a3
    move-result v3

    #@1a4
    if-eqz v3, :cond_1bb

    #@1a6
    move-object/from16 v8, p0

    #@1a8
    move-object v9, v4

    #@1a9
    move/from16 v10, p3

    #@1ab
    move-object/from16 v11, p1

    #@1ad
    move-object/from16 v12, p2

    #@1af
    move-object v13, v7

    #@1b0
    move-object/from16 v14, p4

    #@1b2
    .line 1058
    invoke-direct/range {v8 .. v14}, Landroid/content/pm/PackageParser;->parsePermissionGroup(Landroid/content/pm/PackageParser$Package;ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;[Ljava/lang/String;)Landroid/content/pm/PackageParser$PermissionGroup;

    #@1b5
    move-result-object v3

    #@1b6
    if-nez v3, :cond_14b

    #@1b8
    .line 1059
    const/4 v4, 0x0

    #@1b9
    goto/16 :goto_29

    #@1bb
    .line 1061
    :cond_1bb
    const-string/jumbo v3, "permission"

    #@1be
    move-object/from16 v0, v43

    #@1c0
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c3
    move-result v3

    #@1c4
    if-eqz v3, :cond_1d7

    #@1c6
    move-object/from16 v3, p0

    #@1c8
    move-object/from16 v5, p1

    #@1ca
    move-object/from16 v6, p2

    #@1cc
    move-object/from16 v8, p4

    #@1ce
    .line 1062
    invoke-direct/range {v3 .. v8}, Landroid/content/pm/PackageParser;->parsePermission(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;[Ljava/lang/String;)Landroid/content/pm/PackageParser$Permission;

    #@1d1
    move-result-object v3

    #@1d2
    if-nez v3, :cond_14b

    #@1d4
    .line 1063
    const/4 v4, 0x0

    #@1d5
    goto/16 :goto_29

    #@1d7
    .line 1065
    :cond_1d7
    const-string/jumbo v3, "permission-tree"

    #@1da
    move-object/from16 v0, v43

    #@1dc
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1df
    move-result v3

    #@1e0
    if-eqz v3, :cond_1f3

    #@1e2
    move-object/from16 v3, p0

    #@1e4
    move-object/from16 v5, p1

    #@1e6
    move-object/from16 v6, p2

    #@1e8
    move-object/from16 v8, p4

    #@1ea
    .line 1066
    invoke-direct/range {v3 .. v8}, Landroid/content/pm/PackageParser;->parsePermissionTree(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;[Ljava/lang/String;)Landroid/content/pm/PackageParser$Permission;

    #@1ed
    move-result-object v3

    #@1ee
    if-nez v3, :cond_14b

    #@1f0
    .line 1067
    const/4 v4, 0x0

    #@1f1
    goto/16 :goto_29

    #@1f3
    .line 1069
    :cond_1f3
    const-string/jumbo v3, "uses-permission"

    #@1f6
    move-object/from16 v0, v43

    #@1f8
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1fb
    move-result v3

    #@1fc
    if-eqz v3, :cond_231

    #@1fe
    .line 1070
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifestUsesPermission:[I

    #@200
    move-object/from16 v0, p1

    #@202
    invoke-virtual {v0, v7, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@205
    move-result-object v36

    #@206
    .line 1075
    const/4 v3, 0x0

    #@207
    move-object/from16 v0, v36

    #@209
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@20c
    move-result-object v28

    #@20d
    .line 1082
    .local v28, name:Ljava/lang/String;
    invoke-virtual/range {v36 .. v36}, Landroid/content/res/TypedArray;->recycle()V

    #@210
    .line 1084
    if-eqz v28, :cond_22c

    #@212
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@214
    move-object/from16 v0, v28

    #@216
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@219
    move-result v3

    #@21a
    if-nez v3, :cond_22c

    #@21c
    .line 1085
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@21e
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@221
    move-result-object v5

    #@222
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@225
    .line 1086
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->requestedPermissionsRequired:Ljava/util/ArrayList;

    #@227
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@229
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@22c
    .line 1089
    :cond_22c
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@22f
    goto/16 :goto_14b

    #@231
    .line 1091
    .end local v28           #name:Ljava/lang/String;
    :cond_231
    const-string/jumbo v3, "uses-configuration"

    #@234
    move-object/from16 v0, v43

    #@236
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@239
    move-result v3

    #@23a
    if-eqz v3, :cond_2a4

    #@23c
    .line 1092
    new-instance v18, Landroid/content/pm/ConfigurationInfo;

    #@23e
    invoke-direct/range {v18 .. v18}, Landroid/content/pm/ConfigurationInfo;-><init>()V

    #@241
    .line 1093
    .local v18, cPref:Landroid/content/pm/ConfigurationInfo;
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifestUsesConfiguration:[I

    #@243
    move-object/from16 v0, p1

    #@245
    invoke-virtual {v0, v7, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@248
    move-result-object v36

    #@249
    .line 1095
    const/4 v3, 0x0

    #@24a
    const/4 v5, 0x0

    #@24b
    move-object/from16 v0, v36

    #@24d
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@250
    move-result v3

    #@251
    move-object/from16 v0, v18

    #@253
    iput v3, v0, Landroid/content/pm/ConfigurationInfo;->reqTouchScreen:I

    #@255
    .line 1098
    const/4 v3, 0x1

    #@256
    const/4 v5, 0x0

    #@257
    move-object/from16 v0, v36

    #@259
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@25c
    move-result v3

    #@25d
    move-object/from16 v0, v18

    #@25f
    iput v3, v0, Landroid/content/pm/ConfigurationInfo;->reqKeyboardType:I

    #@261
    .line 1101
    const/4 v3, 0x2

    #@262
    const/4 v5, 0x0

    #@263
    move-object/from16 v0, v36

    #@265
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@268
    move-result v3

    #@269
    if-eqz v3, :cond_275

    #@26b
    .line 1104
    move-object/from16 v0, v18

    #@26d
    iget v3, v0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@26f
    or-int/lit8 v3, v3, 0x1

    #@271
    move-object/from16 v0, v18

    #@273
    iput v3, v0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@275
    .line 1106
    :cond_275
    const/4 v3, 0x3

    #@276
    const/4 v5, 0x0

    #@277
    move-object/from16 v0, v36

    #@279
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@27c
    move-result v3

    #@27d
    move-object/from16 v0, v18

    #@27f
    iput v3, v0, Landroid/content/pm/ConfigurationInfo;->reqNavigation:I

    #@281
    .line 1109
    const/4 v3, 0x4

    #@282
    const/4 v5, 0x0

    #@283
    move-object/from16 v0, v36

    #@285
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@288
    move-result v3

    #@289
    if-eqz v3, :cond_295

    #@28b
    .line 1112
    move-object/from16 v0, v18

    #@28d
    iget v3, v0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@28f
    or-int/lit8 v3, v3, 0x2

    #@291
    move-object/from16 v0, v18

    #@293
    iput v3, v0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@295
    .line 1114
    :cond_295
    invoke-virtual/range {v36 .. v36}, Landroid/content/res/TypedArray;->recycle()V

    #@298
    .line 1115
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->configPreferences:Ljava/util/ArrayList;

    #@29a
    move-object/from16 v0, v18

    #@29c
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@29f
    .line 1117
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@2a2
    goto/16 :goto_14b

    #@2a4
    .line 1119
    .end local v18           #cPref:Landroid/content/pm/ConfigurationInfo;
    :cond_2a4
    const-string/jumbo v3, "uses-feature"

    #@2a7
    move-object/from16 v0, v43

    #@2a9
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2ac
    move-result v3

    #@2ad
    if-eqz v3, :cond_321

    #@2af
    .line 1120
    new-instance v20, Landroid/content/pm/FeatureInfo;

    #@2b1
    invoke-direct/range {v20 .. v20}, Landroid/content/pm/FeatureInfo;-><init>()V

    #@2b4
    .line 1121
    .local v20, fi:Landroid/content/pm/FeatureInfo;
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifestUsesFeature:[I

    #@2b6
    move-object/from16 v0, p1

    #@2b8
    invoke-virtual {v0, v7, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@2bb
    move-result-object v36

    #@2bc
    .line 1125
    const/4 v3, 0x0

    #@2bd
    move-object/from16 v0, v36

    #@2bf
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@2c2
    move-result-object v3

    #@2c3
    move-object/from16 v0, v20

    #@2c5
    iput-object v3, v0, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    #@2c7
    .line 1127
    move-object/from16 v0, v20

    #@2c9
    iget-object v3, v0, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    #@2cb
    if-nez v3, :cond_2d9

    #@2cd
    .line 1128
    const/4 v3, 0x1

    #@2ce
    const/4 v5, 0x0

    #@2cf
    move-object/from16 v0, v36

    #@2d1
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2d4
    move-result v3

    #@2d5
    move-object/from16 v0, v20

    #@2d7
    iput v3, v0, Landroid/content/pm/FeatureInfo;->reqGlEsVersion:I

    #@2d9
    .line 1132
    :cond_2d9
    const/4 v3, 0x2

    #@2da
    const/4 v5, 0x1

    #@2db
    move-object/from16 v0, v36

    #@2dd
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2e0
    move-result v3

    #@2e1
    if-eqz v3, :cond_2ed

    #@2e3
    .line 1135
    move-object/from16 v0, v20

    #@2e5
    iget v3, v0, Landroid/content/pm/FeatureInfo;->flags:I

    #@2e7
    or-int/lit8 v3, v3, 0x1

    #@2e9
    move-object/from16 v0, v20

    #@2eb
    iput v3, v0, Landroid/content/pm/FeatureInfo;->flags:I

    #@2ed
    .line 1137
    :cond_2ed
    invoke-virtual/range {v36 .. v36}, Landroid/content/res/TypedArray;->recycle()V

    #@2f0
    .line 1138
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->reqFeatures:Ljava/util/ArrayList;

    #@2f2
    if-nez v3, :cond_2fb

    #@2f4
    .line 1139
    new-instance v3, Ljava/util/ArrayList;

    #@2f6
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@2f9
    iput-object v3, v4, Landroid/content/pm/PackageParser$Package;->reqFeatures:Ljava/util/ArrayList;

    #@2fb
    .line 1141
    :cond_2fb
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->reqFeatures:Ljava/util/ArrayList;

    #@2fd
    move-object/from16 v0, v20

    #@2ff
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@302
    .line 1143
    move-object/from16 v0, v20

    #@304
    iget-object v3, v0, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    #@306
    if-nez v3, :cond_31c

    #@308
    .line 1144
    new-instance v18, Landroid/content/pm/ConfigurationInfo;

    #@30a
    invoke-direct/range {v18 .. v18}, Landroid/content/pm/ConfigurationInfo;-><init>()V

    #@30d
    .line 1145
    .restart local v18       #cPref:Landroid/content/pm/ConfigurationInfo;
    move-object/from16 v0, v20

    #@30f
    iget v3, v0, Landroid/content/pm/FeatureInfo;->reqGlEsVersion:I

    #@311
    move-object/from16 v0, v18

    #@313
    iput v3, v0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    #@315
    .line 1146
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->configPreferences:Ljava/util/ArrayList;

    #@317
    move-object/from16 v0, v18

    #@319
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@31c
    .line 1149
    .end local v18           #cPref:Landroid/content/pm/ConfigurationInfo;
    :cond_31c
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@31f
    goto/16 :goto_14b

    #@321
    .line 1151
    .end local v20           #fi:Landroid/content/pm/FeatureInfo;
    :cond_321
    const-string/jumbo v3, "uses-sdk"

    #@324
    move-object/from16 v0, v43

    #@326
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@329
    move-result v3

    #@32a
    if-eqz v3, :cond_4a2

    #@32c
    .line 1152
    sget v3, Landroid/content/pm/PackageParser;->SDK_VERSION:I

    #@32e
    if-lez v3, :cond_496

    #@330
    .line 1153
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifestUsesSdk:[I

    #@332
    move-object/from16 v0, p1

    #@334
    invoke-virtual {v0, v7, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@337
    move-result-object v36

    #@338
    .line 1156
    const/16 v27, 0x0

    #@33a
    .line 1157
    .local v27, minVers:I
    const/16 v26, 0x0

    #@33c
    .line 1158
    .local v26, minCode:Ljava/lang/String;
    const/16 v45, 0x0

    #@33e
    .line 1159
    .local v45, targetVers:I
    const/16 v44, 0x0

    #@340
    .line 1161
    .local v44, targetCode:Ljava/lang/String;
    const/4 v3, 0x0

    #@341
    move-object/from16 v0, v36

    #@343
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@346
    move-result-object v47

    #@347
    .line 1163
    .local v47, val:Landroid/util/TypedValue;
    if-eqz v47, :cond_360

    #@349
    .line 1164
    move-object/from16 v0, v47

    #@34b
    iget v3, v0, Landroid/util/TypedValue;->type:I

    #@34d
    const/4 v5, 0x3

    #@34e
    if-ne v3, v5, :cond_3c6

    #@350
    move-object/from16 v0, v47

    #@352
    iget-object v3, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@354
    if-eqz v3, :cond_3c6

    #@356
    .line 1165
    move-object/from16 v0, v47

    #@358
    iget-object v3, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@35a
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@35d
    move-result-object v26

    #@35e
    move-object/from16 v44, v26

    #@360
    .line 1172
    :cond_360
    :goto_360
    const/4 v3, 0x1

    #@361
    move-object/from16 v0, v36

    #@363
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@366
    move-result-object v47

    #@367
    .line 1174
    if-eqz v47, :cond_380

    #@369
    .line 1175
    move-object/from16 v0, v47

    #@36b
    iget v3, v0, Landroid/util/TypedValue;->type:I

    #@36d
    const/4 v5, 0x3

    #@36e
    if-ne v3, v5, :cond_3cf

    #@370
    move-object/from16 v0, v47

    #@372
    iget-object v3, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@374
    if-eqz v3, :cond_3cf

    #@376
    .line 1176
    move-object/from16 v0, v47

    #@378
    iget-object v3, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@37a
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@37d
    move-result-object v26

    #@37e
    move-object/from16 v44, v26

    #@380
    .line 1183
    :cond_380
    :goto_380
    invoke-virtual/range {v36 .. v36}, Landroid/content/res/TypedArray;->recycle()V

    #@383
    .line 1185
    if-eqz v26, :cond_3f5

    #@385
    .line 1186
    sget-object v3, Landroid/content/pm/PackageParser;->SDK_CODENAME:Ljava/lang/String;

    #@387
    move-object/from16 v0, v26

    #@389
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38c
    move-result v3

    #@38d
    if-nez v3, :cond_42e

    #@38f
    .line 1187
    sget-object v3, Landroid/content/pm/PackageParser;->SDK_CODENAME:Ljava/lang/String;

    #@391
    if-eqz v3, :cond_3d6

    #@393
    .line 1188
    const/4 v3, 0x0

    #@394
    new-instance v5, Ljava/lang/StringBuilder;

    #@396
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@399
    const-string v6, "Requires development platform "

    #@39b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39e
    move-result-object v5

    #@39f
    move-object/from16 v0, v26

    #@3a1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a4
    move-result-object v5

    #@3a5
    const-string v6, " (current platform is "

    #@3a7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3aa
    move-result-object v5

    #@3ab
    sget-object v6, Landroid/content/pm/PackageParser;->SDK_CODENAME:Ljava/lang/String;

    #@3ad
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b0
    move-result-object v5

    #@3b1
    const-string v6, ")"

    #@3b3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b6
    move-result-object v5

    #@3b7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3ba
    move-result-object v5

    #@3bb
    aput-object v5, p4, v3

    #@3bd
    .line 1194
    :goto_3bd
    const/16 v3, -0xc

    #@3bf
    move-object/from16 v0, p0

    #@3c1
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@3c3
    .line 1195
    const/4 v4, 0x0

    #@3c4
    goto/16 :goto_29

    #@3c6
    .line 1168
    :cond_3c6
    move-object/from16 v0, v47

    #@3c8
    iget v0, v0, Landroid/util/TypedValue;->data:I

    #@3ca
    move/from16 v27, v0

    #@3cc
    move/from16 v45, v27

    #@3ce
    goto :goto_360

    #@3cf
    .line 1179
    :cond_3cf
    move-object/from16 v0, v47

    #@3d1
    iget v0, v0, Landroid/util/TypedValue;->data:I

    #@3d3
    move/from16 v45, v0

    #@3d5
    goto :goto_380

    #@3d6
    .line 1191
    :cond_3d6
    const/4 v3, 0x0

    #@3d7
    new-instance v5, Ljava/lang/StringBuilder;

    #@3d9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3dc
    const-string v6, "Requires development platform "

    #@3de
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e1
    move-result-object v5

    #@3e2
    move-object/from16 v0, v26

    #@3e4
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e7
    move-result-object v5

    #@3e8
    const-string v6, " but this is a release platform."

    #@3ea
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ed
    move-result-object v5

    #@3ee
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f1
    move-result-object v5

    #@3f2
    aput-object v5, p4, v3

    #@3f4
    goto :goto_3bd

    #@3f5
    .line 1197
    :cond_3f5
    sget v3, Landroid/content/pm/PackageParser;->SDK_VERSION:I

    #@3f7
    move/from16 v0, v27

    #@3f9
    if-le v0, v3, :cond_42e

    #@3fb
    .line 1198
    const/4 v3, 0x0

    #@3fc
    new-instance v5, Ljava/lang/StringBuilder;

    #@3fe
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@401
    const-string v6, "Requires newer sdk version #"

    #@403
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@406
    move-result-object v5

    #@407
    move/from16 v0, v27

    #@409
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40c
    move-result-object v5

    #@40d
    const-string v6, " (current version is #"

    #@40f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@412
    move-result-object v5

    #@413
    sget v6, Landroid/content/pm/PackageParser;->SDK_VERSION:I

    #@415
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@418
    move-result-object v5

    #@419
    const-string v6, ")"

    #@41b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41e
    move-result-object v5

    #@41f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@422
    move-result-object v5

    #@423
    aput-object v5, p4, v3

    #@425
    .line 1200
    const/16 v3, -0xc

    #@427
    move-object/from16 v0, p0

    #@429
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@42b
    .line 1201
    const/4 v4, 0x0

    #@42c
    goto/16 :goto_29

    #@42e
    .line 1204
    :cond_42e
    if-eqz v44, :cond_49b

    #@430
    .line 1205
    sget-object v3, Landroid/content/pm/PackageParser;->SDK_CODENAME:Ljava/lang/String;

    #@432
    move-object/from16 v0, v44

    #@434
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@437
    move-result v3

    #@438
    if-nez v3, :cond_490

    #@43a
    .line 1206
    sget-object v3, Landroid/content/pm/PackageParser;->SDK_CODENAME:Ljava/lang/String;

    #@43c
    if-eqz v3, :cond_471

    #@43e
    .line 1207
    const/4 v3, 0x0

    #@43f
    new-instance v5, Ljava/lang/StringBuilder;

    #@441
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@444
    const-string v6, "Requires development platform "

    #@446
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@449
    move-result-object v5

    #@44a
    move-object/from16 v0, v44

    #@44c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44f
    move-result-object v5

    #@450
    const-string v6, " (current platform is "

    #@452
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@455
    move-result-object v5

    #@456
    sget-object v6, Landroid/content/pm/PackageParser;->SDK_CODENAME:Ljava/lang/String;

    #@458
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45b
    move-result-object v5

    #@45c
    const-string v6, ")"

    #@45e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@461
    move-result-object v5

    #@462
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@465
    move-result-object v5

    #@466
    aput-object v5, p4, v3

    #@468
    .line 1213
    :goto_468
    const/16 v3, -0xc

    #@46a
    move-object/from16 v0, p0

    #@46c
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@46e
    .line 1214
    const/4 v4, 0x0

    #@46f
    goto/16 :goto_29

    #@471
    .line 1210
    :cond_471
    const/4 v3, 0x0

    #@472
    new-instance v5, Ljava/lang/StringBuilder;

    #@474
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@477
    const-string v6, "Requires development platform "

    #@479
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47c
    move-result-object v5

    #@47d
    move-object/from16 v0, v44

    #@47f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@482
    move-result-object v5

    #@483
    const-string v6, " but this is a release platform."

    #@485
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@488
    move-result-object v5

    #@489
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48c
    move-result-object v5

    #@48d
    aput-object v5, p4, v3

    #@48f
    goto :goto_468

    #@490
    .line 1217
    :cond_490
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@492
    const/16 v5, 0x2710

    #@494
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@496
    .line 1224
    .end local v26           #minCode:Ljava/lang/String;
    .end local v27           #minVers:I
    .end local v44           #targetCode:Ljava/lang/String;
    .end local v45           #targetVers:I
    .end local v47           #val:Landroid/util/TypedValue;
    :cond_496
    :goto_496
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@499
    goto/16 :goto_14b

    #@49b
    .line 1220
    .restart local v26       #minCode:Ljava/lang/String;
    .restart local v27       #minVers:I
    .restart local v44       #targetCode:Ljava/lang/String;
    .restart local v45       #targetVers:I
    .restart local v47       #val:Landroid/util/TypedValue;
    :cond_49b
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@49d
    move/from16 v0, v45

    #@49f
    iput v0, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@4a1
    goto :goto_496

    #@4a2
    .line 1226
    .end local v26           #minCode:Ljava/lang/String;
    .end local v27           #minVers:I
    .end local v44           #targetCode:Ljava/lang/String;
    .end local v45           #targetVers:I
    .end local v47           #val:Landroid/util/TypedValue;
    :cond_4a2
    const-string/jumbo v3, "supports-screens"

    #@4a5
    move-object/from16 v0, v43

    #@4a7
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4aa
    move-result v3

    #@4ab
    if-eqz v3, :cond_518

    #@4ad
    .line 1227
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifestSupportsScreens:[I

    #@4af
    move-object/from16 v0, p1

    #@4b1
    invoke-virtual {v0, v7, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@4b4
    move-result-object v36

    #@4b5
    .line 1230
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4b7
    const/4 v5, 0x6

    #@4b8
    const/4 v6, 0x0

    #@4b9
    move-object/from16 v0, v36

    #@4bb
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@4be
    move-result v5

    #@4bf
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@4c1
    .line 1233
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4c3
    const/4 v5, 0x7

    #@4c4
    const/4 v6, 0x0

    #@4c5
    move-object/from16 v0, v36

    #@4c7
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@4ca
    move-result v5

    #@4cb
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@4cd
    .line 1236
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4cf
    const/16 v5, 0x8

    #@4d1
    const/4 v6, 0x0

    #@4d2
    move-object/from16 v0, v36

    #@4d4
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@4d7
    move-result v5

    #@4d8
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@4da
    .line 1242
    const/4 v3, 0x1

    #@4db
    move-object/from16 v0, v36

    #@4dd
    move/from16 v1, v41

    #@4df
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@4e2
    move-result v41

    #@4e3
    .line 1245
    const/4 v3, 0x2

    #@4e4
    move-object/from16 v0, v36

    #@4e6
    move/from16 v1, v40

    #@4e8
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@4eb
    move-result v40

    #@4ec
    .line 1248
    const/4 v3, 0x3

    #@4ed
    move-object/from16 v0, v36

    #@4ef
    move/from16 v1, v39

    #@4f1
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@4f4
    move-result v39

    #@4f5
    .line 1251
    const/4 v3, 0x5

    #@4f6
    move-object/from16 v0, v36

    #@4f8
    move/from16 v1, v42

    #@4fa
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@4fd
    move-result v42

    #@4fe
    .line 1254
    const/4 v3, 0x4

    #@4ff
    move-object/from16 v0, v36

    #@501
    move/from16 v1, v35

    #@503
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@506
    move-result v35

    #@507
    .line 1257
    const/4 v3, 0x0

    #@508
    move-object/from16 v0, v36

    #@50a
    move/from16 v1, v17

    #@50c
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@50f
    move-result v17

    #@510
    .line 1261
    invoke-virtual/range {v36 .. v36}, Landroid/content/res/TypedArray;->recycle()V

    #@513
    .line 1263
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@516
    goto/16 :goto_14b

    #@518
    .line 1265
    :cond_518
    const-string/jumbo v3, "protected-broadcast"

    #@51b
    move-object/from16 v0, v43

    #@51d
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@520
    move-result v3

    #@521
    if-eqz v3, :cond_55e

    #@523
    .line 1266
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifestProtectedBroadcast:[I

    #@525
    move-object/from16 v0, p1

    #@527
    invoke-virtual {v0, v7, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@52a
    move-result-object v36

    #@52b
    .line 1271
    const/4 v3, 0x0

    #@52c
    move-object/from16 v0, v36

    #@52e
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@531
    move-result-object v28

    #@532
    .line 1274
    .restart local v28       #name:Ljava/lang/String;
    invoke-virtual/range {v36 .. v36}, Landroid/content/res/TypedArray;->recycle()V

    #@535
    .line 1276
    if-eqz v28, :cond_559

    #@537
    and-int/lit8 v3, p3, 0x1

    #@539
    if-eqz v3, :cond_559

    #@53b
    .line 1277
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->protectedBroadcasts:Ljava/util/ArrayList;

    #@53d
    if-nez v3, :cond_546

    #@53f
    .line 1278
    new-instance v3, Ljava/util/ArrayList;

    #@541
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@544
    iput-object v3, v4, Landroid/content/pm/PackageParser$Package;->protectedBroadcasts:Ljava/util/ArrayList;

    #@546
    .line 1280
    :cond_546
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->protectedBroadcasts:Ljava/util/ArrayList;

    #@548
    move-object/from16 v0, v28

    #@54a
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@54d
    move-result v3

    #@54e
    if-nez v3, :cond_559

    #@550
    .line 1281
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->protectedBroadcasts:Ljava/util/ArrayList;

    #@552
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@555
    move-result-object v5

    #@556
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@559
    .line 1285
    :cond_559
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@55c
    goto/16 :goto_14b

    #@55e
    .line 1287
    .end local v28           #name:Ljava/lang/String;
    :cond_55e
    const-string v3, "instrumentation"

    #@560
    move-object/from16 v0, v43

    #@562
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@565
    move-result v3

    #@566
    if-eqz v3, :cond_579

    #@568
    move-object/from16 v3, p0

    #@56a
    move-object/from16 v5, p1

    #@56c
    move-object/from16 v6, p2

    #@56e
    move-object/from16 v8, p4

    #@570
    .line 1288
    invoke-direct/range {v3 .. v8}, Landroid/content/pm/PackageParser;->parseInstrumentation(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;[Ljava/lang/String;)Landroid/content/pm/PackageParser$Instrumentation;

    #@573
    move-result-object v3

    #@574
    if-nez v3, :cond_14b

    #@576
    .line 1289
    const/4 v4, 0x0

    #@577
    goto/16 :goto_29

    #@579
    .line 1292
    :cond_579
    const-string/jumbo v3, "original-package"

    #@57c
    move-object/from16 v0, v43

    #@57e
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@581
    move-result v3

    #@582
    if-eqz v3, :cond_5bc

    #@584
    .line 1293
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifestOriginalPackage:[I

    #@586
    move-object/from16 v0, p1

    #@588
    invoke-virtual {v0, v7, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@58b
    move-result-object v36

    #@58c
    .line 1296
    const/4 v3, 0x0

    #@58d
    const/4 v5, 0x0

    #@58e
    move-object/from16 v0, v36

    #@590
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@593
    move-result-object v31

    #@594
    .line 1298
    .local v31, orig:Ljava/lang/String;
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@596
    move-object/from16 v0, v31

    #@598
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59b
    move-result v3

    #@59c
    if-nez v3, :cond_5b4

    #@59e
    .line 1299
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->mOriginalPackages:Ljava/util/ArrayList;

    #@5a0
    if-nez v3, :cond_5ad

    #@5a2
    .line 1300
    new-instance v3, Ljava/util/ArrayList;

    #@5a4
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@5a7
    iput-object v3, v4, Landroid/content/pm/PackageParser$Package;->mOriginalPackages:Ljava/util/ArrayList;

    #@5a9
    .line 1301
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@5ab
    iput-object v3, v4, Landroid/content/pm/PackageParser$Package;->mRealPackage:Ljava/lang/String;

    #@5ad
    .line 1303
    :cond_5ad
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->mOriginalPackages:Ljava/util/ArrayList;

    #@5af
    move-object/from16 v0, v31

    #@5b1
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5b4
    .line 1306
    :cond_5b4
    invoke-virtual/range {v36 .. v36}, Landroid/content/res/TypedArray;->recycle()V

    #@5b7
    .line 1308
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@5ba
    goto/16 :goto_14b

    #@5bc
    .line 1310
    .end local v31           #orig:Ljava/lang/String;
    :cond_5bc
    const-string v3, "adopt-permissions"

    #@5be
    move-object/from16 v0, v43

    #@5c0
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c3
    move-result v3

    #@5c4
    if-eqz v3, :cond_5f2

    #@5c6
    .line 1311
    sget-object v3, Lcom/android/internal/R$styleable;->AndroidManifestOriginalPackage:[I

    #@5c8
    move-object/from16 v0, p1

    #@5ca
    invoke-virtual {v0, v7, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@5cd
    move-result-object v36

    #@5ce
    .line 1314
    const/4 v3, 0x0

    #@5cf
    const/4 v5, 0x0

    #@5d0
    move-object/from16 v0, v36

    #@5d2
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@5d5
    move-result-object v28

    #@5d6
    .line 1317
    .restart local v28       #name:Ljava/lang/String;
    invoke-virtual/range {v36 .. v36}, Landroid/content/res/TypedArray;->recycle()V

    #@5d9
    .line 1319
    if-eqz v28, :cond_5ed

    #@5db
    .line 1320
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->mAdoptPermissions:Ljava/util/ArrayList;

    #@5dd
    if-nez v3, :cond_5e6

    #@5df
    .line 1321
    new-instance v3, Ljava/util/ArrayList;

    #@5e1
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@5e4
    iput-object v3, v4, Landroid/content/pm/PackageParser$Package;->mAdoptPermissions:Ljava/util/ArrayList;

    #@5e6
    .line 1323
    :cond_5e6
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->mAdoptPermissions:Ljava/util/ArrayList;

    #@5e8
    move-object/from16 v0, v28

    #@5ea
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5ed
    .line 1326
    :cond_5ed
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@5f0
    goto/16 :goto_14b

    #@5f2
    .line 1328
    .end local v28           #name:Ljava/lang/String;
    :cond_5f2
    const-string/jumbo v3, "uses-gl-texture"

    #@5f5
    move-object/from16 v0, v43

    #@5f7
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5fa
    move-result v3

    #@5fb
    if-eqz v3, :cond_602

    #@5fd
    .line 1330
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@600
    goto/16 :goto_14b

    #@602
    .line 1333
    :cond_602
    const-string v3, "compatible-screens"

    #@604
    move-object/from16 v0, v43

    #@606
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@609
    move-result v3

    #@60a
    if-eqz v3, :cond_611

    #@60c
    .line 1335
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@60f
    goto/16 :goto_14b

    #@611
    .line 1338
    :cond_611
    const-string v3, "eat-comment"

    #@613
    move-object/from16 v0, v43

    #@615
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@618
    move-result v3

    #@619
    if-eqz v3, :cond_620

    #@61b
    .line 1340
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@61e
    goto/16 :goto_14b

    #@620
    .line 1350
    :cond_620
    const-string v3, "PackageParser"

    #@622
    new-instance v5, Ljava/lang/StringBuilder;

    #@624
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@627
    const-string v6, "Unknown element under <manifest>: "

    #@629
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62c
    move-result-object v5

    #@62d
    invoke-interface/range {p2 .. p2}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@630
    move-result-object v6

    #@631
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@634
    move-result-object v5

    #@635
    const-string v6, " at "

    #@637
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63a
    move-result-object v5

    #@63b
    move-object/from16 v0, p0

    #@63d
    iget-object v6, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@63f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@642
    move-result-object v5

    #@643
    const-string v6, " "

    #@645
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@648
    move-result-object v5

    #@649
    invoke-interface/range {p2 .. p2}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    #@64c
    move-result-object v6

    #@64d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@650
    move-result-object v5

    #@651
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@654
    move-result-object v5

    #@655
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@658
    .line 1353
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@65b
    goto/16 :goto_14b

    #@65d
    .line 1358
    .end local v43           #tagName:Ljava/lang/String;
    :cond_65d
    if-nez v21, :cond_672

    #@65f
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->instrumentation:Ljava/util/ArrayList;

    #@661
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@664
    move-result v3

    #@665
    if-nez v3, :cond_672

    #@667
    .line 1359
    const/4 v3, 0x0

    #@668
    const-string v5, "<manifest> does not contain an <application> or <instrumentation>"

    #@66a
    aput-object v5, p4, v3

    #@66c
    .line 1360
    const/16 v3, -0x6d

    #@66e
    move-object/from16 v0, p0

    #@670
    iput v3, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@672
    .line 1363
    :cond_672
    sget-object v3, Landroid/content/pm/PackageParser;->NEW_PERMISSIONS:[Landroid/content/pm/PackageParser$NewPermissionInfo;

    #@674
    array-length v15, v3

    #@675
    .line 1364
    .local v15, NP:I
    const/16 v22, 0x0

    #@677
    .line 1365
    .local v22, implicitPerms:Ljava/lang/StringBuilder;
    const/16 v24, 0x0

    #@679
    .local v24, ip:I
    :goto_679
    move/from16 v0, v24

    #@67b
    if-ge v0, v15, :cond_68b

    #@67d
    .line 1366
    sget-object v3, Landroid/content/pm/PackageParser;->NEW_PERMISSIONS:[Landroid/content/pm/PackageParser$NewPermissionInfo;

    #@67f
    aget-object v30, v3, v24

    #@681
    .line 1368
    .local v30, npi:Landroid/content/pm/PackageParser$NewPermissionInfo;
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@683
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@685
    move-object/from16 v0, v30

    #@687
    iget v5, v0, Landroid/content/pm/PackageParser$NewPermissionInfo;->sdkVersion:I

    #@689
    if-lt v3, v5, :cond_6c0

    #@68b
    .line 1384
    .end local v30           #npi:Landroid/content/pm/PackageParser$NewPermissionInfo;
    :cond_68b
    if-eqz v22, :cond_696

    #@68d
    .line 1385
    const-string v3, "PackageParser"

    #@68f
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@692
    move-result-object v5

    #@693
    invoke-static {v3, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@696
    .line 1388
    :cond_696
    sget-object v3, Landroid/content/pm/PackageParser;->SPLIT_PERMISSIONS:[Landroid/content/pm/PackageParser$SplitPermissionInfo;

    #@698
    array-length v0, v3

    #@699
    move/from16 v16, v0

    #@69b
    .line 1389
    .local v16, NS:I
    const/16 v25, 0x0

    #@69d
    .local v25, is:I
    :goto_69d
    move/from16 v0, v25

    #@69f
    move/from16 v1, v16

    #@6a1
    if-ge v0, v1, :cond_736

    #@6a3
    .line 1390
    sget-object v3, Landroid/content/pm/PackageParser;->SPLIT_PERMISSIONS:[Landroid/content/pm/PackageParser$SplitPermissionInfo;

    #@6a5
    aget-object v37, v3, v25

    #@6a7
    .line 1392
    .local v37, spi:Landroid/content/pm/PackageParser$SplitPermissionInfo;
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@6a9
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@6ab
    move-object/from16 v0, v37

    #@6ad
    iget v5, v0, Landroid/content/pm/PackageParser$SplitPermissionInfo;->targetSdk:I

    #@6af
    if-ge v3, v5, :cond_6bd

    #@6b1
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@6b3
    move-object/from16 v0, v37

    #@6b5
    iget-object v5, v0, Landroid/content/pm/PackageParser$SplitPermissionInfo;->rootPerm:Ljava/lang/String;

    #@6b7
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@6ba
    move-result v3

    #@6bb
    if-nez v3, :cond_70a

    #@6bd
    .line 1389
    :cond_6bd
    add-int/lit8 v25, v25, 0x1

    #@6bf
    goto :goto_69d

    #@6c0
    .line 1371
    .end local v16           #NS:I
    .end local v25           #is:I
    .end local v37           #spi:Landroid/content/pm/PackageParser$SplitPermissionInfo;
    .restart local v30       #npi:Landroid/content/pm/PackageParser$NewPermissionInfo;
    :cond_6c0
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@6c2
    move-object/from16 v0, v30

    #@6c4
    iget-object v5, v0, Landroid/content/pm/PackageParser$NewPermissionInfo;->name:Ljava/lang/String;

    #@6c6
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@6c9
    move-result v3

    #@6ca
    if-nez v3, :cond_6fe

    #@6cc
    .line 1372
    if-nez v22, :cond_702

    #@6ce
    .line 1373
    new-instance v22, Ljava/lang/StringBuilder;

    #@6d0
    .end local v22           #implicitPerms:Ljava/lang/StringBuilder;
    const/16 v3, 0x80

    #@6d2
    move-object/from16 v0, v22

    #@6d4
    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@6d7
    .line 1374
    .restart local v22       #implicitPerms:Ljava/lang/StringBuilder;
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@6d9
    move-object/from16 v0, v22

    #@6db
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6de
    .line 1375
    const-string v3, ": compat added "

    #@6e0
    move-object/from16 v0, v22

    #@6e2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e5
    .line 1379
    :goto_6e5
    move-object/from16 v0, v30

    #@6e7
    iget-object v3, v0, Landroid/content/pm/PackageParser$NewPermissionInfo;->name:Ljava/lang/String;

    #@6e9
    move-object/from16 v0, v22

    #@6eb
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6ee
    .line 1380
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@6f0
    move-object/from16 v0, v30

    #@6f2
    iget-object v5, v0, Landroid/content/pm/PackageParser$NewPermissionInfo;->name:Ljava/lang/String;

    #@6f4
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6f7
    .line 1381
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->requestedPermissionsRequired:Ljava/util/ArrayList;

    #@6f9
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@6fb
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6fe
    .line 1365
    :cond_6fe
    add-int/lit8 v24, v24, 0x1

    #@700
    goto/16 :goto_679

    #@702
    .line 1377
    :cond_702
    const/16 v3, 0x20

    #@704
    move-object/from16 v0, v22

    #@706
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@709
    goto :goto_6e5

    #@70a
    .line 1396
    .end local v30           #npi:Landroid/content/pm/PackageParser$NewPermissionInfo;
    .restart local v16       #NS:I
    .restart local v25       #is:I
    .restart local v37       #spi:Landroid/content/pm/PackageParser$SplitPermissionInfo;
    :cond_70a
    const/16 v23, 0x0

    #@70c
    .local v23, in:I
    :goto_70c
    move-object/from16 v0, v37

    #@70e
    iget-object v3, v0, Landroid/content/pm/PackageParser$SplitPermissionInfo;->newPerms:[Ljava/lang/String;

    #@710
    array-length v3, v3

    #@711
    move/from16 v0, v23

    #@713
    if-ge v0, v3, :cond_6bd

    #@715
    .line 1397
    move-object/from16 v0, v37

    #@717
    iget-object v3, v0, Landroid/content/pm/PackageParser$SplitPermissionInfo;->newPerms:[Ljava/lang/String;

    #@719
    aget-object v33, v3, v23

    #@71b
    .line 1398
    .local v33, perm:Ljava/lang/String;
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@71d
    move-object/from16 v0, v33

    #@71f
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@722
    move-result v3

    #@723
    if-nez v3, :cond_733

    #@725
    .line 1399
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@727
    move-object/from16 v0, v33

    #@729
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@72c
    .line 1400
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->requestedPermissionsRequired:Ljava/util/ArrayList;

    #@72e
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@730
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@733
    .line 1396
    :cond_733
    add-int/lit8 v23, v23, 0x1

    #@735
    goto :goto_70c

    #@736
    .line 1405
    .end local v23           #in:I
    .end local v33           #perm:Ljava/lang/String;
    .end local v37           #spi:Landroid/content/pm/PackageParser$SplitPermissionInfo;
    :cond_736
    if-ltz v41, :cond_741

    #@738
    if-lez v41, :cond_749

    #@73a
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@73c
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@73e
    const/4 v5, 0x4

    #@73f
    if-lt v3, v5, :cond_749

    #@741
    .line 1408
    :cond_741
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@743
    iget v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@745
    or-int/lit16 v5, v5, 0x200

    #@747
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@749
    .line 1410
    :cond_749
    if-eqz v40, :cond_753

    #@74b
    .line 1411
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@74d
    iget v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@74f
    or-int/lit16 v5, v5, 0x400

    #@751
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@753
    .line 1413
    :cond_753
    if-ltz v39, :cond_75e

    #@755
    if-lez v39, :cond_766

    #@757
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@759
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@75b
    const/4 v5, 0x4

    #@75c
    if-lt v3, v5, :cond_766

    #@75e
    .line 1416
    :cond_75e
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@760
    iget v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@762
    or-int/lit16 v5, v5, 0x800

    #@764
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@766
    .line 1418
    :cond_766
    if-ltz v42, :cond_772

    #@768
    if-lez v42, :cond_77b

    #@76a
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@76c
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@76e
    const/16 v5, 0x9

    #@770
    if-lt v3, v5, :cond_77b

    #@772
    .line 1421
    :cond_772
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@774
    iget v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@776
    const/high16 v6, 0x8

    #@778
    or-int/2addr v5, v6

    #@779
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@77b
    .line 1423
    :cond_77b
    if-ltz v35, :cond_786

    #@77d
    if-lez v35, :cond_78e

    #@77f
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@781
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@783
    const/4 v5, 0x4

    #@784
    if-lt v3, v5, :cond_78e

    #@786
    .line 1426
    :cond_786
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@788
    iget v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@78a
    or-int/lit16 v5, v5, 0x1000

    #@78c
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@78e
    .line 1428
    :cond_78e
    if-ltz v17, :cond_799

    #@790
    if-lez v17, :cond_29

    #@792
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@794
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@796
    const/4 v5, 0x4

    #@797
    if-lt v3, v5, :cond_29

    #@799
    .line 1431
    :cond_799
    iget-object v3, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@79b
    iget v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@79d
    or-int/lit16 v5, v5, 0x2000

    #@79f
    iput v5, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    #@7a1
    goto/16 :goto_29
.end method

.method private parsePackageItemInfo(Landroid/content/pm/PackageParser$Package;Landroid/content/pm/PackageItemInfo;[Ljava/lang/String;Ljava/lang/String;Landroid/content/res/TypedArray;IIII)Z
    .registers 18
    .parameter "owner"
    .parameter "outInfo"
    .parameter "outError"
    .parameter "tag"
    .parameter "sa"
    .parameter "nameRes"
    .parameter "labelRes"
    .parameter "iconRes"
    .parameter "logoRes"

    #@0
    .prologue
    .line 2076
    const/4 v5, 0x0

    #@1
    invoke-virtual {p5, p6, v5}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@4
    move-result-object v3

    #@5
    .line 2077
    .local v3, name:Ljava/lang/String;
    if-nez v3, :cond_1f

    #@7
    .line 2078
    const/4 v5, 0x0

    #@8
    new-instance v6, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    const-string v7, " does not specify android:name"

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    aput-object v6, p3, v5

    #@1d
    .line 2079
    const/4 v5, 0x0

    #@1e
    .line 2106
    :goto_1e
    return v5

    #@1f
    .line 2082
    :cond_1f
    iget-object v5, p1, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@21
    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@23
    invoke-static {v5, v3, p3}, Landroid/content/pm/PackageParser;->buildClassName(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    iput-object v5, p2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@29
    .line 2084
    iget-object v5, p2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@2b
    if-nez v5, :cond_2f

    #@2d
    .line 2085
    const/4 v5, 0x0

    #@2e
    goto :goto_1e

    #@2f
    .line 2088
    :cond_2f
    const/4 v5, 0x0

    #@30
    move/from16 v0, p8

    #@32
    invoke-virtual {p5, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@35
    move-result v1

    #@36
    .line 2089
    .local v1, iconVal:I
    if-eqz v1, :cond_3d

    #@38
    .line 2090
    iput v1, p2, Landroid/content/pm/PackageItemInfo;->icon:I

    #@3a
    .line 2091
    const/4 v5, 0x0

    #@3b
    iput-object v5, p2, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@3d
    .line 2094
    :cond_3d
    const/4 v5, 0x0

    #@3e
    move/from16 v0, p9

    #@40
    invoke-virtual {p5, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@43
    move-result v2

    #@44
    .line 2095
    .local v2, logoVal:I
    if-eqz v2, :cond_48

    #@46
    .line 2096
    iput v2, p2, Landroid/content/pm/PackageItemInfo;->logo:I

    #@48
    .line 2099
    :cond_48
    invoke-virtual {p5, p7}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@4b
    move-result-object v4

    #@4c
    .line 2100
    .local v4, v:Landroid/util/TypedValue;
    if-eqz v4, :cond_5a

    #@4e
    iget v5, v4, Landroid/util/TypedValue;->resourceId:I

    #@50
    iput v5, p2, Landroid/content/pm/PackageItemInfo;->labelRes:I

    #@52
    if-nez v5, :cond_5a

    #@54
    .line 2101
    invoke-virtual {v4}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@57
    move-result-object v5

    #@58
    iput-object v5, p2, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@5a
    .line 2104
    :cond_5a
    iget-object v5, p1, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@5c
    iput-object v5, p2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@5e
    .line 2106
    const/4 v5, 0x1

    #@5f
    goto :goto_1e
.end method

.method private static parsePackageLite(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$PackageLite;
    .registers 20
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "flags"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 861
    :cond_0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@3
    move-result v8

    #@4
    .local v8, type:I
    const/4 v12, 0x2

    #@5
    if-eq v8, v12, :cond_a

    #@7
    const/4 v12, 0x1

    #@8
    if-ne v8, v12, :cond_0

    #@a
    .line 865
    :cond_a
    const/4 v12, 0x2

    #@b
    if-eq v8, v12, :cond_14

    #@d
    .line 866
    const/4 v12, 0x0

    #@e
    const-string v13, "No start tag found"

    #@10
    aput-object v13, p4, v12

    #@12
    .line 867
    const/4 v12, 0x0

    #@13
    .line 922
    :goto_13
    return-object v12

    #@14
    .line 871
    :cond_14
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@17
    move-result-object v12

    #@18
    const-string/jumbo v13, "manifest"

    #@1b
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v12

    #@1f
    if-nez v12, :cond_28

    #@21
    .line 872
    const/4 v12, 0x0

    #@22
    const-string v13, "No <manifest> tag"

    #@24
    aput-object v13, p4, v12

    #@26
    .line 873
    const/4 v12, 0x0

    #@27
    goto :goto_13

    #@28
    .line 875
    :cond_28
    const/4 v12, 0x0

    #@29
    const-string/jumbo v13, "package"

    #@2c
    move-object/from16 v0, p2

    #@2e
    invoke-interface {v0, v12, v13}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    .line 876
    .local v6, pkgName:Ljava/lang/String;
    if-eqz v6, :cond_3a

    #@34
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@37
    move-result v12

    #@38
    if-nez v12, :cond_41

    #@3a
    .line 877
    :cond_3a
    const/4 v12, 0x0

    #@3b
    const-string v13, "<manifest> does not specify package"

    #@3d
    aput-object v13, p4, v12

    #@3f
    .line 878
    const/4 v12, 0x0

    #@40
    goto :goto_13

    #@41
    .line 880
    :cond_41
    const/4 v12, 0x1

    #@42
    invoke-static {v6, v12}, Landroid/content/pm/PackageParser;->validateName(Ljava/lang/String;Z)Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    .line 881
    .local v4, nameError:Ljava/lang/String;
    if-eqz v4, :cond_72

    #@48
    const-string v12, "android"

    #@4a
    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v12

    #@4e
    if-nez v12, :cond_72

    #@50
    .line 882
    const/4 v12, 0x0

    #@51
    new-instance v13, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v14, "<manifest> specifies bad package name \""

    #@58
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v13

    #@5c
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v13

    #@60
    const-string v14, "\": "

    #@62
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v13

    #@66
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v13

    #@6a
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v13

    #@6e
    aput-object v13, p4, v12

    #@70
    .line 884
    const/4 v12, 0x0

    #@71
    goto :goto_13

    #@72
    .line 886
    :cond_72
    const/4 v3, -0x1

    #@73
    .line 887
    .local v3, installLocation:I
    const/4 v11, 0x0

    #@74
    .line 888
    .local v11, versionCode:I
    const/4 v5, 0x0

    #@75
    .line 889
    .local v5, numFound:I
    const/4 v2, 0x0

    #@76
    .local v2, i:I
    :goto_76
    invoke-interface/range {p2 .. p2}, Landroid/util/AttributeSet;->getAttributeCount()I

    #@79
    move-result v12

    #@7a
    if-ge v2, v12, :cond_96

    #@7c
    .line 890
    move-object/from16 v0, p2

    #@7e
    invoke-interface {v0, v2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    #@81
    move-result-object v1

    #@82
    .line 891
    .local v1, attr:Ljava/lang/String;
    const-string v12, "installLocation"

    #@84
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@87
    move-result v12

    #@88
    if-eqz v12, :cond_d4

    #@8a
    .line 892
    const/4 v12, -0x1

    #@8b
    move-object/from16 v0, p2

    #@8d
    invoke-interface {v0, v2, v12}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    #@90
    move-result v3

    #@91
    .line 894
    add-int/lit8 v5, v5, 0x1

    #@93
    .line 899
    :cond_93
    :goto_93
    const/4 v12, 0x2

    #@94
    if-lt v5, v12, :cond_e7

    #@96
    .line 905
    .end local v1           #attr:Ljava/lang/String;
    :cond_96
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@99
    move-result v12

    #@9a
    add-int/lit8 v7, v12, 0x1

    #@9c
    .line 907
    .local v7, searchDepth:I
    new-instance v10, Ljava/util/ArrayList;

    #@9e
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@a1
    .line 909
    .local v10, verifiers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/VerifierInfo;>;"
    :cond_a1
    :goto_a1
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@a4
    move-result v8

    #@a5
    const/4 v12, 0x1

    #@a6
    if-eq v8, v12, :cond_ea

    #@a8
    const/4 v12, 0x3

    #@a9
    if-ne v8, v12, :cond_b1

    #@ab
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@ae
    move-result v12

    #@af
    if-lt v12, v7, :cond_ea

    #@b1
    .line 910
    :cond_b1
    const/4 v12, 0x3

    #@b2
    if-eq v8, v12, :cond_a1

    #@b4
    const/4 v12, 0x4

    #@b5
    if-eq v8, v12, :cond_a1

    #@b7
    .line 914
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@ba
    move-result v12

    #@bb
    if-ne v12, v7, :cond_a1

    #@bd
    const-string/jumbo v12, "package-verifier"

    #@c0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@c3
    move-result-object v13

    #@c4
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c7
    move-result v12

    #@c8
    if-eqz v12, :cond_a1

    #@ca
    .line 915
    invoke-static/range {p0 .. p4}, Landroid/content/pm/PackageParser;->parseVerifier(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/VerifierInfo;

    #@cd
    move-result-object v9

    #@ce
    .line 916
    .local v9, verifier:Landroid/content/pm/VerifierInfo;
    if-eqz v9, :cond_a1

    #@d0
    .line 917
    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@d3
    goto :goto_a1

    #@d4
    .line 895
    .end local v7           #searchDepth:I
    .end local v9           #verifier:Landroid/content/pm/VerifierInfo;
    .end local v10           #verifiers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/VerifierInfo;>;"
    .restart local v1       #attr:Ljava/lang/String;
    :cond_d4
    const-string/jumbo v12, "versionCode"

    #@d7
    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@da
    move-result v12

    #@db
    if-eqz v12, :cond_93

    #@dd
    .line 896
    const/4 v12, 0x0

    #@de
    move-object/from16 v0, p2

    #@e0
    invoke-interface {v0, v2, v12}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    #@e3
    move-result v11

    #@e4
    .line 897
    add-int/lit8 v5, v5, 0x1

    #@e6
    goto :goto_93

    #@e7
    .line 889
    :cond_e7
    add-int/lit8 v2, v2, 0x1

    #@e9
    goto :goto_76

    #@ea
    .line 922
    .end local v1           #attr:Ljava/lang/String;
    .restart local v7       #searchDepth:I
    .restart local v10       #verifiers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/VerifierInfo;>;"
    :cond_ea
    new-instance v12, Landroid/content/pm/PackageParser$PackageLite;

    #@ec
    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@ef
    move-result-object v13

    #@f0
    invoke-direct {v12, v13, v11, v3, v10}, Landroid/content/pm/PackageParser$PackageLite;-><init>(Ljava/lang/String;IILjava/util/List;)V

    #@f3
    goto/16 :goto_13
.end method

.method public static parsePackageLite(Ljava/lang/String;I)Landroid/content/pm/PackageParser$PackageLite;
    .registers 34
    .parameter "packageFilePath"
    .parameter "flags"

    #@0
    .prologue
    .line 750
    const/16 v23, 0x0

    #@2
    .line 754
    .local v23, assmgr:Landroid/content/res/AssetManager;
    :try_start_2
    new-instance v5, Landroid/content/res/AssetManager;

    #@4
    invoke-direct {v5}, Landroid/content/res/AssetManager;-><init>()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_7} :catch_89

    #@7
    .line 755
    .end local v23           #assmgr:Landroid/content/res/AssetManager;
    .local v5, assmgr:Landroid/content/res/AssetManager;
    const/4 v6, 0x0

    #@8
    const/4 v7, 0x0

    #@9
    const/4 v8, 0x0

    #@a
    const/4 v9, 0x0

    #@b
    const/4 v10, 0x0

    #@c
    const/4 v11, 0x0

    #@d
    const/4 v12, 0x0

    #@e
    const/4 v13, 0x0

    #@f
    const/4 v14, 0x0

    #@10
    const/4 v15, 0x0

    #@11
    const/16 v16, 0x0

    #@13
    const/16 v17, 0x0

    #@15
    const/16 v18, 0x0

    #@17
    const/16 v19, 0x0

    #@19
    const/16 v20, 0x0

    #@1b
    const/16 v21, 0x0

    #@1d
    :try_start_1d
    sget v22, Landroid/os/Build$VERSION;->RESOURCES_SDK_INT:I

    #@1f
    invoke-virtual/range {v5 .. v22}, Landroid/content/res/AssetManager;->setConfiguration(IILjava/lang/String;IIIIIIIIIIIIII)V

    #@22
    .line 758
    move-object/from16 v0, p0

    #@24
    invoke-virtual {v5, v0}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    #@27
    move-result v25

    #@28
    .line 759
    .local v25, cookie:I
    if-nez v25, :cond_2d

    #@2a
    .line 760
    const/16 v29, 0x0

    #@2c
    .line 791
    .end local v25           #cookie:I
    :cond_2c
    :goto_2c
    return-object v29

    #@2d
    .line 763
    .restart local v25       #cookie:I
    :cond_2d
    new-instance v28, Landroid/util/DisplayMetrics;

    #@2f
    invoke-direct/range {v28 .. v28}, Landroid/util/DisplayMetrics;-><init>()V

    #@32
    .line 764
    .local v28, metrics:Landroid/util/DisplayMetrics;
    invoke-virtual/range {v28 .. v28}, Landroid/util/DisplayMetrics;->setToDefaults()V

    #@35
    .line 765
    new-instance v31, Landroid/content/res/Resources;

    #@37
    const/4 v6, 0x0

    #@38
    move-object/from16 v0, v31

    #@3a
    move-object/from16 v1, v28

    #@3c
    invoke-direct {v0, v5, v1, v6}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    #@3f
    .line 766
    .local v31, res:Landroid/content/res/Resources;
    const-string v6, "AndroidManifest.xml"

    #@41
    move/from16 v0, v25

    #@43
    invoke-virtual {v5, v0, v6}, Landroid/content/res/AssetManager;->openXmlResourceParser(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;
    :try_end_46
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_46} :catch_e7

    #@46
    move-result-object v30

    #@47
    .line 774
    .local v30, parser:Landroid/content/res/XmlResourceParser;
    move-object/from16 v24, v30

    #@49
    .line 775
    .local v24, attrs:Landroid/util/AttributeSet;
    const/4 v6, 0x1

    #@4a
    new-array v0, v6, [Ljava/lang/String;

    #@4c
    move-object/from16 v27, v0

    #@4e
    .line 776
    .local v27, errors:[Ljava/lang/String;
    const/16 v29, 0x0

    #@50
    .line 778
    .local v29, packageLite:Landroid/content/pm/PackageParser$PackageLite;
    :try_start_50
    move-object/from16 v0, v31

    #@52
    move-object/from16 v1, v30

    #@54
    move-object/from16 v2, v24

    #@56
    move/from16 v3, p1

    #@58
    move-object/from16 v4, v27

    #@5a
    invoke-static {v0, v1, v2, v3, v4}, Landroid/content/pm/PackageParser;->parsePackageLite(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$PackageLite;
    :try_end_5d
    .catchall {:try_start_50 .. :try_end_5d} :catchall_db
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_5d} :catch_b1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_50 .. :try_end_5d} :catch_c6

    #@5d
    move-result-object v29

    #@5e
    .line 784
    if-eqz v30, :cond_63

    #@60
    invoke-interface/range {v30 .. v30}, Landroid/content/res/XmlResourceParser;->close()V

    #@63
    .line 785
    :cond_63
    if-eqz v5, :cond_68

    #@65
    invoke-virtual {v5}, Landroid/content/res/AssetManager;->close()V

    #@68
    .line 787
    :cond_68
    :goto_68
    if-nez v29, :cond_2c

    #@6a
    .line 788
    const-string v6, "PackageParser"

    #@6c
    new-instance v7, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string/jumbo v8, "parsePackageLite error: "

    #@74
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v7

    #@78
    const/4 v8, 0x0

    #@79
    aget-object v8, v27, v8

    #@7b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v7

    #@7f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v7

    #@83
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 789
    const/16 v29, 0x0

    #@88
    goto :goto_2c

    #@89
    .line 767
    .end local v5           #assmgr:Landroid/content/res/AssetManager;
    .end local v24           #attrs:Landroid/util/AttributeSet;
    .end local v25           #cookie:I
    .end local v27           #errors:[Ljava/lang/String;
    .end local v28           #metrics:Landroid/util/DisplayMetrics;
    .end local v29           #packageLite:Landroid/content/pm/PackageParser$PackageLite;
    .end local v30           #parser:Landroid/content/res/XmlResourceParser;
    .end local v31           #res:Landroid/content/res/Resources;
    .restart local v23       #assmgr:Landroid/content/res/AssetManager;
    :catch_89
    move-exception v26

    #@8a
    move-object/from16 v5, v23

    #@8c
    .line 768
    .end local v23           #assmgr:Landroid/content/res/AssetManager;
    .restart local v5       #assmgr:Landroid/content/res/AssetManager;
    .local v26, e:Ljava/lang/Exception;
    :goto_8c
    if-eqz v5, :cond_91

    #@8e
    invoke-virtual {v5}, Landroid/content/res/AssetManager;->close()V

    #@91
    .line 769
    :cond_91
    const-string v6, "PackageParser"

    #@93
    new-instance v7, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v8, "Unable to read AndroidManifest.xml of "

    #@9a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v7

    #@9e
    move-object/from16 v0, p0

    #@a0
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v7

    #@a4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v7

    #@a8
    move-object/from16 v0, v26

    #@aa
    invoke-static {v6, v7, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ad
    .line 771
    const/16 v29, 0x0

    #@af
    goto/16 :goto_2c

    #@b1
    .line 779
    .end local v26           #e:Ljava/lang/Exception;
    .restart local v24       #attrs:Landroid/util/AttributeSet;
    .restart local v25       #cookie:I
    .restart local v27       #errors:[Ljava/lang/String;
    .restart local v28       #metrics:Landroid/util/DisplayMetrics;
    .restart local v29       #packageLite:Landroid/content/pm/PackageParser$PackageLite;
    .restart local v30       #parser:Landroid/content/res/XmlResourceParser;
    .restart local v31       #res:Landroid/content/res/Resources;
    :catch_b1
    move-exception v26

    #@b2
    .line 780
    .local v26, e:Ljava/io/IOException;
    :try_start_b2
    const-string v6, "PackageParser"

    #@b4
    move-object/from16 v0, p0

    #@b6
    move-object/from16 v1, v26

    #@b8
    invoke-static {v6, v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_bb
    .catchall {:try_start_b2 .. :try_end_bb} :catchall_db

    #@bb
    .line 784
    if-eqz v30, :cond_c0

    #@bd
    invoke-interface/range {v30 .. v30}, Landroid/content/res/XmlResourceParser;->close()V

    #@c0
    .line 785
    :cond_c0
    if-eqz v5, :cond_68

    #@c2
    invoke-virtual {v5}, Landroid/content/res/AssetManager;->close()V

    #@c5
    goto :goto_68

    #@c6
    .line 781
    .end local v26           #e:Ljava/io/IOException;
    :catch_c6
    move-exception v26

    #@c7
    .line 782
    .local v26, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_c7
    const-string v6, "PackageParser"

    #@c9
    move-object/from16 v0, p0

    #@cb
    move-object/from16 v1, v26

    #@cd
    invoke-static {v6, v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d0
    .catchall {:try_start_c7 .. :try_end_d0} :catchall_db

    #@d0
    .line 784
    if-eqz v30, :cond_d5

    #@d2
    invoke-interface/range {v30 .. v30}, Landroid/content/res/XmlResourceParser;->close()V

    #@d5
    .line 785
    :cond_d5
    if-eqz v5, :cond_68

    #@d7
    invoke-virtual {v5}, Landroid/content/res/AssetManager;->close()V

    #@da
    goto :goto_68

    #@db
    .line 784
    .end local v26           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_db
    move-exception v6

    #@dc
    if-eqz v30, :cond_e1

    #@de
    invoke-interface/range {v30 .. v30}, Landroid/content/res/XmlResourceParser;->close()V

    #@e1
    .line 785
    :cond_e1
    if-eqz v5, :cond_e6

    #@e3
    invoke-virtual {v5}, Landroid/content/res/AssetManager;->close()V

    #@e6
    :cond_e6
    throw v6

    #@e7
    .line 767
    .end local v24           #attrs:Landroid/util/AttributeSet;
    .end local v25           #cookie:I
    .end local v27           #errors:[Ljava/lang/String;
    .end local v28           #metrics:Landroid/util/DisplayMetrics;
    .end local v29           #packageLite:Landroid/content/pm/PackageParser$PackageLite;
    .end local v30           #parser:Landroid/content/res/XmlResourceParser;
    .end local v31           #res:Landroid/content/res/Resources;
    :catch_e7
    move-exception v26

    #@e8
    goto :goto_8c
.end method

.method private static parsePackageName(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "parser"
    .parameter "attrs"
    .parameter "flags"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v6, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    .line 826
    :cond_4
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@7
    move-result v2

    #@8
    .local v2, type:I
    if-eq v2, v4, :cond_c

    #@a
    if-ne v2, v7, :cond_4

    #@c
    .line 830
    :cond_c
    if-eq v2, v4, :cond_13

    #@e
    .line 831
    const-string v4, "No start tag found"

    #@10
    aput-object v4, p3, v6

    #@12
    .line 852
    :goto_12
    return-object v3

    #@13
    .line 836
    :cond_13
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    const-string/jumbo v5, "manifest"

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v4

    #@1e
    if-nez v4, :cond_25

    #@20
    .line 837
    const-string v4, "No <manifest> tag"

    #@22
    aput-object v4, p3, v6

    #@24
    goto :goto_12

    #@25
    .line 840
    :cond_25
    const-string/jumbo v4, "package"

    #@28
    invoke-interface {p1, v3, v4}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    .line 841
    .local v1, pkgName:Ljava/lang/String;
    if-eqz v1, :cond_34

    #@2e
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@31
    move-result v4

    #@32
    if-nez v4, :cond_39

    #@34
    .line 842
    :cond_34
    const-string v4, "<manifest> does not specify package"

    #@36
    aput-object v4, p3, v6

    #@38
    goto :goto_12

    #@39
    .line 845
    :cond_39
    invoke-static {v1, v7}, Landroid/content/pm/PackageParser;->validateName(Ljava/lang/String;Z)Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    .line 846
    .local v0, nameError:Ljava/lang/String;
    if-eqz v0, :cond_67

    #@3f
    const-string v4, "android"

    #@41
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v4

    #@45
    if-nez v4, :cond_67

    #@47
    .line 847
    new-instance v4, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v5, "<manifest> specifies bad package name \""

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    const-string v5, "\": "

    #@58
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    aput-object v4, p3, v6

    #@66
    goto :goto_12

    #@67
    .line 852
    :cond_67
    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@6a
    move-result-object v3

    #@6b
    goto :goto_12
.end method

.method private parsePermission(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;[Ljava/lang/String;)Landroid/content/pm/PackageParser$Permission;
    .registers 21
    .parameter "owner"
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1566
    new-instance v13, Landroid/content/pm/PackageParser$Permission;

    #@2
    move-object/from16 v0, p1

    #@4
    invoke-direct {v13, v0}, Landroid/content/pm/PackageParser$Permission;-><init>(Landroid/content/pm/PackageParser$Package;)V

    #@7
    .line 1568
    .local v13, perm:Landroid/content/pm/PackageParser$Permission;
    sget-object v2, Lcom/android/internal/R$styleable;->AndroidManifestPermission:[I

    #@9
    move-object/from16 v0, p2

    #@b
    move-object/from16 v1, p4

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@10
    move-result-object v7

    #@11
    .line 1571
    .local v7, sa:Landroid/content/res/TypedArray;
    iget-object v4, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@13
    const-string v6, "<permission>"

    #@15
    const/4 v8, 0x2

    #@16
    const/4 v9, 0x0

    #@17
    const/4 v10, 0x1

    #@18
    const/4 v11, 0x6

    #@19
    move-object v2, p0

    #@1a
    move-object/from16 v3, p1

    #@1c
    move-object/from16 v5, p5

    #@1e
    invoke-direct/range {v2 .. v11}, Landroid/content/pm/PackageParser;->parsePackageItemInfo(Landroid/content/pm/PackageParser$Package;Landroid/content/pm/PackageItemInfo;[Ljava/lang/String;Ljava/lang/String;Landroid/content/res/TypedArray;IIII)Z

    #@21
    move-result v2

    #@22
    if-nez v2, :cond_2d

    #@24
    .line 1577
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    #@27
    .line 1578
    const/16 v2, -0x6c

    #@29
    iput v2, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@2b
    .line 1579
    const/4 v13, 0x0

    #@2c
    .line 1629
    .end local v13           #perm:Landroid/content/pm/PackageParser$Permission;
    :goto_2c
    return-object v13

    #@2d
    .line 1584
    .restart local v13       #perm:Landroid/content/pm/PackageParser$Permission;
    :cond_2d
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@2f
    const/4 v3, 0x4

    #@30
    invoke-virtual {v7, v3}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    iput-object v3, v2, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@36
    .line 1586
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@38
    iget-object v2, v2, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@3a
    if-eqz v2, :cond_48

    #@3c
    .line 1587
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@3e
    iget-object v3, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@40
    iget-object v3, v3, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@42
    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    iput-object v3, v2, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    #@48
    .line 1590
    :cond_48
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@4a
    const/4 v3, 0x5

    #@4b
    const/4 v4, 0x0

    #@4c
    invoke-virtual {v7, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@4f
    move-result v3

    #@50
    iput v3, v2, Landroid/content/pm/PermissionInfo;->descriptionRes:I

    #@52
    .line 1594
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@54
    const/4 v3, 0x3

    #@55
    const/4 v4, 0x0

    #@56
    invoke-virtual {v7, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@59
    move-result v3

    #@5a
    iput v3, v2, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@5c
    .line 1598
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@5e
    const/4 v3, 0x7

    #@5f
    const/4 v4, 0x0

    #@60
    invoke-virtual {v7, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@63
    move-result v3

    #@64
    iput v3, v2, Landroid/content/pm/PermissionInfo;->flags:I

    #@66
    .line 1601
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    #@69
    .line 1603
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@6b
    iget v2, v2, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@6d
    const/4 v3, -0x1

    #@6e
    if-ne v2, v3, :cond_7b

    #@70
    .line 1604
    const/4 v2, 0x0

    #@71
    const-string v3, "<permission> does not specify protectionLevel"

    #@73
    aput-object v3, p5, v2

    #@75
    .line 1605
    const/16 v2, -0x6c

    #@77
    iput v2, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@79
    .line 1606
    const/4 v13, 0x0

    #@7a
    goto :goto_2c

    #@7b
    .line 1609
    :cond_7b
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@7d
    iget-object v3, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@7f
    iget v3, v3, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@81
    invoke-static {v3}, Landroid/content/pm/PermissionInfo;->fixProtectionLevel(I)I

    #@84
    move-result v3

    #@85
    iput v3, v2, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@87
    .line 1611
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@89
    iget v2, v2, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@8b
    and-int/lit16 v2, v2, 0xf0

    #@8d
    if-eqz v2, :cond_a3

    #@8f
    .line 1612
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@91
    iget v2, v2, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@93
    and-int/lit8 v2, v2, 0xf

    #@95
    const/4 v3, 0x2

    #@96
    if-eq v2, v3, :cond_a3

    #@98
    .line 1614
    const/4 v2, 0x0

    #@99
    const-string v3, "<permission>  protectionLevel specifies a flag but is not based on signature type"

    #@9b
    aput-object v3, p5, v2

    #@9d
    .line 1616
    const/16 v2, -0x6c

    #@9f
    iput v2, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@a1
    .line 1617
    const/4 v13, 0x0

    #@a2
    goto :goto_2c

    #@a3
    .line 1621
    :cond_a3
    const-string v12, "<permission>"

    #@a5
    move-object v8, p0

    #@a6
    move-object/from16 v9, p2

    #@a8
    move-object/from16 v10, p3

    #@aa
    move-object/from16 v11, p4

    #@ac
    move-object/from16 v14, p5

    #@ae
    invoke-direct/range {v8 .. v14}, Landroid/content/pm/PackageParser;->parseAllMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Ljava/lang/String;Landroid/content/pm/PackageParser$Component;[Ljava/lang/String;)Z

    #@b1
    move-result v2

    #@b2
    if-nez v2, :cond_bb

    #@b4
    .line 1623
    const/16 v2, -0x6c

    #@b6
    iput v2, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@b8
    .line 1624
    const/4 v13, 0x0

    #@b9
    goto/16 :goto_2c

    #@bb
    .line 1627
    :cond_bb
    move-object/from16 v0, p1

    #@bd
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->permissions:Ljava/util/ArrayList;

    #@bf
    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c2
    goto/16 :goto_2c
.end method

.method private parsePermissionGroup(Landroid/content/pm/PackageParser$Package;ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;[Ljava/lang/String;)Landroid/content/pm/PackageParser$PermissionGroup;
    .registers 22
    .parameter "owner"
    .parameter "flags"
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1523
    new-instance v13, Landroid/content/pm/PackageParser$PermissionGroup;

    #@2
    move-object/from16 v0, p1

    #@4
    invoke-direct {v13, v0}, Landroid/content/pm/PackageParser$PermissionGroup;-><init>(Landroid/content/pm/PackageParser$Package;)V

    #@7
    .line 1525
    .local v13, perm:Landroid/content/pm/PackageParser$PermissionGroup;
    sget-object v2, Lcom/android/internal/R$styleable;->AndroidManifestPermissionGroup:[I

    #@9
    move-object/from16 v0, p3

    #@b
    move-object/from16 v1, p5

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@10
    move-result-object v7

    #@11
    .line 1528
    .local v7, sa:Landroid/content/res/TypedArray;
    iget-object v4, v13, Landroid/content/pm/PackageParser$PermissionGroup;->info:Landroid/content/pm/PermissionGroupInfo;

    #@13
    const-string v6, "<permission-group>"

    #@15
    const/4 v8, 0x2

    #@16
    const/4 v9, 0x0

    #@17
    const/4 v10, 0x1

    #@18
    const/4 v11, 0x5

    #@19
    move-object v2, p0

    #@1a
    move-object/from16 v3, p1

    #@1c
    move-object/from16 v5, p6

    #@1e
    invoke-direct/range {v2 .. v11}, Landroid/content/pm/PackageParser;->parsePackageItemInfo(Landroid/content/pm/PackageParser$Package;Landroid/content/pm/PackageItemInfo;[Ljava/lang/String;Ljava/lang/String;Landroid/content/res/TypedArray;IIII)Z

    #@21
    move-result v2

    #@22
    if-nez v2, :cond_2d

    #@24
    .line 1534
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    #@27
    .line 1535
    const/16 v2, -0x6c

    #@29
    iput v2, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@2b
    .line 1536
    const/4 v13, 0x0

    #@2c
    .line 1560
    .end local v13           #perm:Landroid/content/pm/PackageParser$PermissionGroup;
    :goto_2c
    return-object v13

    #@2d
    .line 1539
    .restart local v13       #perm:Landroid/content/pm/PackageParser$PermissionGroup;
    :cond_2d
    iget-object v2, v13, Landroid/content/pm/PackageParser$PermissionGroup;->info:Landroid/content/pm/PermissionGroupInfo;

    #@2f
    const/4 v3, 0x4

    #@30
    const/4 v4, 0x0

    #@31
    invoke-virtual {v7, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@34
    move-result v3

    #@35
    iput v3, v2, Landroid/content/pm/PermissionGroupInfo;->descriptionRes:I

    #@37
    .line 1542
    iget-object v2, v13, Landroid/content/pm/PackageParser$PermissionGroup;->info:Landroid/content/pm/PermissionGroupInfo;

    #@39
    const/4 v3, 0x6

    #@3a
    const/4 v4, 0x0

    #@3b
    invoke-virtual {v7, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@3e
    move-result v3

    #@3f
    iput v3, v2, Landroid/content/pm/PermissionGroupInfo;->flags:I

    #@41
    .line 1544
    iget-object v2, v13, Landroid/content/pm/PackageParser$PermissionGroup;->info:Landroid/content/pm/PermissionGroupInfo;

    #@43
    const/4 v3, 0x3

    #@44
    const/4 v4, 0x0

    #@45
    invoke-virtual {v7, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@48
    move-result v3

    #@49
    iput v3, v2, Landroid/content/pm/PermissionGroupInfo;->priority:I

    #@4b
    .line 1546
    iget-object v2, v13, Landroid/content/pm/PackageParser$PermissionGroup;->info:Landroid/content/pm/PermissionGroupInfo;

    #@4d
    iget v2, v2, Landroid/content/pm/PermissionGroupInfo;->priority:I

    #@4f
    if-lez v2, :cond_5a

    #@51
    and-int/lit8 v2, p2, 0x1

    #@53
    if-nez v2, :cond_5a

    #@55
    .line 1547
    iget-object v2, v13, Landroid/content/pm/PackageParser$PermissionGroup;->info:Landroid/content/pm/PermissionGroupInfo;

    #@57
    const/4 v3, 0x0

    #@58
    iput v3, v2, Landroid/content/pm/PermissionGroupInfo;->priority:I

    #@5a
    .line 1550
    :cond_5a
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    #@5d
    .line 1552
    const-string v12, "<permission-group>"

    #@5f
    move-object v8, p0

    #@60
    move-object/from16 v9, p3

    #@62
    move-object/from16 v10, p4

    #@64
    move-object/from16 v11, p5

    #@66
    move-object/from16 v14, p6

    #@68
    invoke-direct/range {v8 .. v14}, Landroid/content/pm/PackageParser;->parseAllMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Ljava/lang/String;Landroid/content/pm/PackageParser$Component;[Ljava/lang/String;)Z

    #@6b
    move-result v2

    #@6c
    if-nez v2, :cond_74

    #@6e
    .line 1554
    const/16 v2, -0x6c

    #@70
    iput v2, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@72
    .line 1555
    const/4 v13, 0x0

    #@73
    goto :goto_2c

    #@74
    .line 1558
    :cond_74
    move-object/from16 v0, p1

    #@76
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->permissionGroups:Ljava/util/ArrayList;

    #@78
    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7b
    goto :goto_2c
.end method

.method private parsePermissionTree(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;[Ljava/lang/String;)Landroid/content/pm/PackageParser$Permission;
    .registers 22
    .parameter "owner"
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1635
    new-instance v13, Landroid/content/pm/PackageParser$Permission;

    #@2
    move-object/from16 v0, p1

    #@4
    invoke-direct {v13, v0}, Landroid/content/pm/PackageParser$Permission;-><init>(Landroid/content/pm/PackageParser$Package;)V

    #@7
    .line 1637
    .local v13, perm:Landroid/content/pm/PackageParser$Permission;
    sget-object v2, Lcom/android/internal/R$styleable;->AndroidManifestPermissionTree:[I

    #@9
    move-object/from16 v0, p2

    #@b
    move-object/from16 v1, p4

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@10
    move-result-object v7

    #@11
    .line 1640
    .local v7, sa:Landroid/content/res/TypedArray;
    iget-object v4, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@13
    const-string v6, "<permission-tree>"

    #@15
    const/4 v8, 0x2

    #@16
    const/4 v9, 0x0

    #@17
    const/4 v10, 0x1

    #@18
    const/4 v11, 0x3

    #@19
    move-object/from16 v2, p0

    #@1b
    move-object/from16 v3, p1

    #@1d
    move-object/from16 v5, p5

    #@1f
    invoke-direct/range {v2 .. v11}, Landroid/content/pm/PackageParser;->parsePackageItemInfo(Landroid/content/pm/PackageParser$Package;Landroid/content/pm/PackageItemInfo;[Ljava/lang/String;Ljava/lang/String;Landroid/content/res/TypedArray;IIII)Z

    #@22
    move-result v2

    #@23
    if-nez v2, :cond_30

    #@25
    .line 1646
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    #@28
    .line 1647
    const/16 v2, -0x6c

    #@2a
    move-object/from16 v0, p0

    #@2c
    iput v2, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@2e
    .line 1648
    const/4 v13, 0x0

    #@2f
    .line 1676
    .end local v13           #perm:Landroid/content/pm/PackageParser$Permission;
    :goto_2f
    return-object v13

    #@30
    .line 1651
    .restart local v13       #perm:Landroid/content/pm/PackageParser$Permission;
    :cond_30
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    #@33
    .line 1653
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@35
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@37
    const/16 v3, 0x2e

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    #@3c
    move-result v15

    #@3d
    .line 1654
    .local v15, index:I
    if-lez v15, :cond_4b

    #@3f
    .line 1655
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@41
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@43
    const/16 v3, 0x2e

    #@45
    add-int/lit8 v4, v15, 0x1

    #@47
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->indexOf(II)I

    #@4a
    move-result v15

    #@4b
    .line 1657
    :cond_4b
    if-gez v15, :cond_6f

    #@4d
    .line 1658
    const/4 v2, 0x0

    #@4e
    new-instance v3, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v4, "<permission-tree> name has less than three segments: "

    #@55
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    iget-object v4, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@5b
    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@5d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    aput-object v3, p5, v2

    #@67
    .line 1660
    const/16 v2, -0x6c

    #@69
    move-object/from16 v0, p0

    #@6b
    iput v2, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@6d
    .line 1661
    const/4 v13, 0x0

    #@6e
    goto :goto_2f

    #@6f
    .line 1664
    :cond_6f
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@71
    const/4 v3, 0x0

    #@72
    iput v3, v2, Landroid/content/pm/PermissionInfo;->descriptionRes:I

    #@74
    .line 1665
    iget-object v2, v13, Landroid/content/pm/PackageParser$Permission;->info:Landroid/content/pm/PermissionInfo;

    #@76
    const/4 v3, 0x0

    #@77
    iput v3, v2, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    #@79
    .line 1666
    const/4 v2, 0x1

    #@7a
    iput-boolean v2, v13, Landroid/content/pm/PackageParser$Permission;->tree:Z

    #@7c
    .line 1668
    const-string v12, "<permission-tree>"

    #@7e
    move-object/from16 v8, p0

    #@80
    move-object/from16 v9, p2

    #@82
    move-object/from16 v10, p3

    #@84
    move-object/from16 v11, p4

    #@86
    move-object/from16 v14, p5

    #@88
    invoke-direct/range {v8 .. v14}, Landroid/content/pm/PackageParser;->parseAllMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Ljava/lang/String;Landroid/content/pm/PackageParser$Component;[Ljava/lang/String;)Z

    #@8b
    move-result v2

    #@8c
    if-nez v2, :cond_96

    #@8e
    .line 1670
    const/16 v2, -0x6c

    #@90
    move-object/from16 v0, p0

    #@92
    iput v2, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@94
    .line 1671
    const/4 v13, 0x0

    #@95
    goto :goto_2f

    #@96
    .line 1674
    :cond_96
    move-object/from16 v0, p1

    #@98
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->permissions:Ljava/util/ArrayList;

    #@9a
    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9d
    goto :goto_2f
.end method

.method private parseProvider(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$Provider;
    .registers 25
    .parameter "owner"
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "flags"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2551
    sget-object v2, Lcom/android/internal/R$styleable;->AndroidManifestProvider:[I

    #@2
    move-object/from16 v0, p2

    #@4
    move-object/from16 v1, p4

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v16

    #@a
    .line 2554
    .local v16, sa:Landroid/content/res/TypedArray;
    move-object/from16 v0, p0

    #@c
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseProviderArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@e
    if-nez v2, :cond_33

    #@10
    .line 2555
    new-instance v2, Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@12
    const/4 v5, 0x2

    #@13
    const/4 v6, 0x0

    #@14
    const/4 v7, 0x1

    #@15
    const/16 v8, 0xf

    #@17
    move-object/from16 v0, p0

    #@19
    iget-object v9, v0, Landroid/content/pm/PackageParser;->mSeparateProcesses:[Ljava/lang/String;

    #@1b
    const/16 v10, 0x8

    #@1d
    const/16 v11, 0xe

    #@1f
    const/4 v12, 0x6

    #@20
    move-object/from16 v3, p1

    #@22
    move-object/from16 v4, p6

    #@24
    invoke-direct/range {v2 .. v12}, Landroid/content/pm/PackageParser$ParseComponentArgs;-><init>(Landroid/content/pm/PackageParser$Package;[Ljava/lang/String;IIII[Ljava/lang/String;III)V

    #@27
    move-object/from16 v0, p0

    #@29
    iput-object v2, v0, Landroid/content/pm/PackageParser;->mParseProviderArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@2b
    .line 2564
    move-object/from16 v0, p0

    #@2d
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseProviderArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@2f
    const-string v3, "<provider>"

    #@31
    iput-object v3, v2, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->tag:Ljava/lang/String;

    #@33
    .line 2567
    :cond_33
    move-object/from16 v0, p0

    #@35
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseProviderArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@37
    move-object/from16 v0, v16

    #@39
    iput-object v0, v2, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@3b
    .line 2568
    move-object/from16 v0, p0

    #@3d
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseProviderArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@3f
    move/from16 v0, p5

    #@41
    iput v0, v2, Landroid/content/pm/PackageParser$ParseComponentArgs;->flags:I

    #@43
    .line 2570
    new-instance v6, Landroid/content/pm/PackageParser$Provider;

    #@45
    move-object/from16 v0, p0

    #@47
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseProviderArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@49
    new-instance v3, Landroid/content/pm/ProviderInfo;

    #@4b
    invoke-direct {v3}, Landroid/content/pm/ProviderInfo;-><init>()V

    #@4e
    invoke-direct {v6, v2, v3}, Landroid/content/pm/PackageParser$Provider;-><init>(Landroid/content/pm/PackageParser$ParseComponentArgs;Landroid/content/pm/ProviderInfo;)V

    #@51
    .line 2571
    .local v6, p:Landroid/content/pm/PackageParser$Provider;
    const/4 v2, 0x0

    #@52
    aget-object v2, p6, v2

    #@54
    if-eqz v2, :cond_5b

    #@56
    .line 2572
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/TypedArray;->recycle()V

    #@59
    .line 2573
    const/4 v6, 0x0

    #@5a
    .line 2668
    .end local v6           #p:Landroid/content/pm/PackageParser$Provider;
    :cond_5a
    :goto_5a
    return-object v6

    #@5b
    .line 2576
    .restart local v6       #p:Landroid/content/pm/PackageParser$Provider;
    :cond_5b
    const/4 v15, 0x0

    #@5c
    .line 2578
    .local v15, providerExportedDefault:Z
    move-object/from16 v0, p1

    #@5e
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@60
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@62
    const/16 v3, 0x11

    #@64
    if-ge v2, v3, :cond_67

    #@66
    .line 2582
    const/4 v15, 0x1

    #@67
    .line 2585
    :cond_67
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@69
    const/4 v3, 0x7

    #@6a
    move-object/from16 v0, v16

    #@6c
    invoke-virtual {v0, v3, v15}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@6f
    move-result v3

    #@70
    iput-boolean v3, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@72
    .line 2589
    const/16 v2, 0xa

    #@74
    const/4 v3, 0x0

    #@75
    move-object/from16 v0, v16

    #@77
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@7a
    move-result-object v13

    #@7b
    .line 2592
    .local v13, cpname:Ljava/lang/String;
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@7d
    const/16 v3, 0xb

    #@7f
    const/4 v4, 0x0

    #@80
    move-object/from16 v0, v16

    #@82
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@85
    move-result v3

    #@86
    iput-boolean v3, v2, Landroid/content/pm/ProviderInfo;->isSyncable:Z

    #@88
    .line 2596
    const/4 v2, 0x3

    #@89
    const/4 v3, 0x0

    #@8a
    move-object/from16 v0, v16

    #@8c
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@8f
    move-result-object v14

    #@90
    .line 2598
    .local v14, permission:Ljava/lang/String;
    const/4 v2, 0x4

    #@91
    const/4 v3, 0x0

    #@92
    move-object/from16 v0, v16

    #@94
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@97
    move-result-object v17

    #@98
    .line 2600
    .local v17, str:Ljava/lang/String;
    if-nez v17, :cond_9c

    #@9a
    .line 2601
    move-object/from16 v17, v14

    #@9c
    .line 2603
    :cond_9c
    if-nez v17, :cond_161

    #@9e
    .line 2604
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@a0
    move-object/from16 v0, p1

    #@a2
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@a4
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@a6
    iput-object v3, v2, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    #@a8
    .line 2609
    :goto_a8
    const/4 v2, 0x5

    #@a9
    const/4 v3, 0x0

    #@aa
    move-object/from16 v0, v16

    #@ac
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@af
    move-result-object v17

    #@b0
    .line 2611
    if-nez v17, :cond_b4

    #@b2
    .line 2612
    move-object/from16 v17, v14

    #@b4
    .line 2614
    :cond_b4
    if-nez v17, :cond_177

    #@b6
    .line 2615
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@b8
    move-object/from16 v0, p1

    #@ba
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@bc
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@be
    iput-object v3, v2, Landroid/content/pm/ProviderInfo;->writePermission:Ljava/lang/String;

    #@c0
    .line 2621
    :goto_c0
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@c2
    const/16 v3, 0xd

    #@c4
    const/4 v4, 0x0

    #@c5
    move-object/from16 v0, v16

    #@c7
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ca
    move-result v3

    #@cb
    iput-boolean v3, v2, Landroid/content/pm/ProviderInfo;->grantUriPermissions:Z

    #@cd
    .line 2625
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@cf
    const/16 v3, 0x9

    #@d1
    const/4 v4, 0x0

    #@d2
    move-object/from16 v0, v16

    #@d4
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@d7
    move-result v3

    #@d8
    iput-boolean v3, v2, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@da
    .line 2629
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@dc
    const/16 v3, 0xc

    #@de
    const/4 v4, 0x0

    #@df
    move-object/from16 v0, v16

    #@e1
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@e4
    move-result v3

    #@e5
    iput v3, v2, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@e7
    .line 2633
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@e9
    const/4 v3, 0x0

    #@ea
    iput v3, v2, Landroid/content/pm/ProviderInfo;->flags:I

    #@ec
    .line 2635
    const/16 v2, 0x10

    #@ee
    const/4 v3, 0x0

    #@ef
    move-object/from16 v0, v16

    #@f1
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@f4
    move-result v2

    #@f5
    if-eqz v2, :cond_141

    #@f7
    .line 2638
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@f9
    iget v3, v2, Landroid/content/pm/ProviderInfo;->flags:I

    #@fb
    const/high16 v4, 0x4000

    #@fd
    or-int/2addr v3, v4

    #@fe
    iput v3, v2, Landroid/content/pm/ProviderInfo;->flags:I

    #@100
    .line 2639
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@102
    iget-boolean v2, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@104
    if-eqz v2, :cond_141

    #@106
    .line 2640
    const-string v2, "PackageParser"

    #@108
    new-instance v3, Ljava/lang/StringBuilder;

    #@10a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10d
    const-string v4, "Provider exported request ignored due to singleUser: "

    #@10f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v3

    #@113
    iget-object v4, v6, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@115
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v3

    #@119
    const-string v4, " at "

    #@11b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v3

    #@11f
    move-object/from16 v0, p0

    #@121
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@123
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v3

    #@127
    const-string v4, " "

    #@129
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v3

    #@12d
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@130
    move-result-object v4

    #@131
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v3

    #@135
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@138
    move-result-object v3

    #@139
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13c
    .line 2643
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@13e
    const/4 v3, 0x0

    #@13f
    iput-boolean v3, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@141
    .line 2647
    :cond_141
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/TypedArray;->recycle()V

    #@144
    .line 2649
    move-object/from16 v0, p1

    #@146
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@148
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@14a
    const/high16 v3, 0x1000

    #@14c
    and-int/2addr v2, v3

    #@14d
    if-eqz v2, :cond_18d

    #@14f
    .line 2652
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@151
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@153
    move-object/from16 v0, p1

    #@155
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@157
    if-ne v2, v3, :cond_18d

    #@159
    .line 2653
    const/4 v2, 0x0

    #@15a
    const-string v3, "Heavy-weight applications can not have providers in main process"

    #@15c
    aput-object v3, p6, v2

    #@15e
    .line 2654
    const/4 v6, 0x0

    #@15f
    goto/16 :goto_5a

    #@161
    .line 2606
    :cond_161
    iget-object v3, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@163
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    #@166
    move-result v2

    #@167
    if-lez v2, :cond_175

    #@169
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@16c
    move-result-object v2

    #@16d
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@170
    move-result-object v2

    #@171
    :goto_171
    iput-object v2, v3, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    #@173
    goto/16 :goto_a8

    #@175
    :cond_175
    const/4 v2, 0x0

    #@176
    goto :goto_171

    #@177
    .line 2617
    :cond_177
    iget-object v3, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@179
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    #@17c
    move-result v2

    #@17d
    if-lez v2, :cond_18b

    #@17f
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@182
    move-result-object v2

    #@183
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@186
    move-result-object v2

    #@187
    :goto_187
    iput-object v2, v3, Landroid/content/pm/ProviderInfo;->writePermission:Ljava/lang/String;

    #@189
    goto/16 :goto_c0

    #@18b
    :cond_18b
    const/4 v2, 0x0

    #@18c
    goto :goto_187

    #@18d
    .line 2658
    :cond_18d
    if-nez v13, :cond_197

    #@18f
    .line 2659
    const/4 v2, 0x0

    #@190
    const-string v3, "<provider> does not include authorities attribute"

    #@192
    aput-object v3, p6, v2

    #@194
    .line 2660
    const/4 v6, 0x0

    #@195
    goto/16 :goto_5a

    #@197
    .line 2662
    :cond_197
    iget-object v2, v6, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@199
    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@19c
    move-result-object v3

    #@19d
    iput-object v3, v2, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@19f
    move-object/from16 v2, p0

    #@1a1
    move-object/from16 v3, p2

    #@1a3
    move-object/from16 v4, p3

    #@1a5
    move-object/from16 v5, p4

    #@1a7
    move-object/from16 v7, p6

    #@1a9
    .line 2664
    invoke-direct/range {v2 .. v7}, Landroid/content/pm/PackageParser;->parseProviderTags(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/pm/PackageParser$Provider;[Ljava/lang/String;)Z

    #@1ac
    move-result v2

    #@1ad
    if-nez v2, :cond_5a

    #@1af
    .line 2665
    const/4 v6, 0x0

    #@1b0
    goto/16 :goto_5a
.end method

.method private parseProviderTags(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/pm/PackageParser$Provider;[Ljava/lang/String;)Z
    .registers 26
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "outInfo"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2675
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@3
    move-result v11

    #@4
    .line 2678
    .local v11, outerDepth:I
    :cond_4
    :goto_4
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@7
    move-result v18

    #@8
    .local v18, type:I
    const/4 v2, 0x1

    #@9
    move/from16 v0, v18

    #@b
    if-eq v0, v2, :cond_295

    #@d
    const/4 v2, 0x3

    #@e
    move/from16 v0, v18

    #@10
    if-ne v0, v2, :cond_18

    #@12
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@15
    move-result v2

    #@16
    if-le v2, v11, :cond_295

    #@18
    .line 2680
    :cond_18
    const/4 v2, 0x3

    #@19
    move/from16 v0, v18

    #@1b
    if-eq v0, v2, :cond_4

    #@1d
    const/4 v2, 0x4

    #@1e
    move/from16 v0, v18

    #@20
    if-eq v0, v2, :cond_4

    #@22
    .line 2684
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    const-string/jumbo v3, "meta-data"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_49

    #@2f
    .line 2685
    move-object/from16 v0, p4

    #@31
    iget-object v6, v0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@33
    move-object/from16 v2, p0

    #@35
    move-object/from16 v3, p1

    #@37
    move-object/from16 v4, p2

    #@39
    move-object/from16 v5, p3

    #@3b
    move-object/from16 v7, p5

    #@3d
    invoke-direct/range {v2 .. v7}, Landroid/content/pm/PackageParser;->parseMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;

    #@40
    move-result-object v2

    #@41
    move-object/from16 v0, p4

    #@43
    iput-object v2, v0, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@45
    if-nez v2, :cond_4

    #@47
    .line 2687
    const/4 v2, 0x0

    #@48
    .line 2844
    :goto_48
    return v2

    #@49
    .line 2690
    :cond_49
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    const-string v3, "grant-uri-permission"

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v2

    #@53
    if-eqz v2, :cond_11d

    #@55
    .line 2691
    sget-object v2, Lcom/android/internal/R$styleable;->AndroidManifestGrantUriPermission:[I

    #@57
    move-object/from16 v0, p1

    #@59
    move-object/from16 v1, p3

    #@5b
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@5e
    move-result-object v16

    #@5f
    .line 2694
    .local v16, sa:Landroid/content/res/TypedArray;
    const/4 v12, 0x0

    #@60
    .line 2696
    .local v12, pa:Landroid/os/PatternMatcher;
    const/4 v2, 0x0

    #@61
    const/4 v3, 0x0

    #@62
    move-object/from16 v0, v16

    #@64
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@67
    move-result-object v17

    #@68
    .line 2698
    .local v17, str:Ljava/lang/String;
    if-eqz v17, :cond_72

    #@6a
    .line 2699
    new-instance v12, Landroid/os/PatternMatcher;

    #@6c
    .end local v12           #pa:Landroid/os/PatternMatcher;
    const/4 v2, 0x0

    #@6d
    move-object/from16 v0, v17

    #@6f
    invoke-direct {v12, v0, v2}, Landroid/os/PatternMatcher;-><init>(Ljava/lang/String;I)V

    #@72
    .line 2702
    .restart local v12       #pa:Landroid/os/PatternMatcher;
    :cond_72
    const/4 v2, 0x1

    #@73
    const/4 v3, 0x0

    #@74
    move-object/from16 v0, v16

    #@76
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@79
    move-result-object v17

    #@7a
    .line 2704
    if-eqz v17, :cond_84

    #@7c
    .line 2705
    new-instance v12, Landroid/os/PatternMatcher;

    #@7e
    .end local v12           #pa:Landroid/os/PatternMatcher;
    const/4 v2, 0x1

    #@7f
    move-object/from16 v0, v17

    #@81
    invoke-direct {v12, v0, v2}, Landroid/os/PatternMatcher;-><init>(Ljava/lang/String;I)V

    #@84
    .line 2708
    .restart local v12       #pa:Landroid/os/PatternMatcher;
    :cond_84
    const/4 v2, 0x2

    #@85
    const/4 v3, 0x0

    #@86
    move-object/from16 v0, v16

    #@88
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@8b
    move-result-object v17

    #@8c
    .line 2710
    if-eqz v17, :cond_96

    #@8e
    .line 2711
    new-instance v12, Landroid/os/PatternMatcher;

    #@90
    .end local v12           #pa:Landroid/os/PatternMatcher;
    const/4 v2, 0x2

    #@91
    move-object/from16 v0, v17

    #@93
    invoke-direct {v12, v0, v2}, Landroid/os/PatternMatcher;-><init>(Ljava/lang/String;I)V

    #@96
    .line 2714
    .restart local v12       #pa:Landroid/os/PatternMatcher;
    :cond_96
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/TypedArray;->recycle()V

    #@99
    .line 2716
    if-eqz v12, :cond_e0

    #@9b
    .line 2717
    move-object/from16 v0, p4

    #@9d
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@9f
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@a1
    if-nez v2, :cond_c1

    #@a3
    .line 2718
    move-object/from16 v0, p4

    #@a5
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@a7
    const/4 v3, 0x1

    #@a8
    new-array v3, v3, [Landroid/os/PatternMatcher;

    #@aa
    iput-object v3, v2, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@ac
    .line 2719
    move-object/from16 v0, p4

    #@ae
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@b0
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@b2
    const/4 v3, 0x0

    #@b3
    aput-object v12, v2, v3

    #@b5
    .line 2727
    :goto_b5
    move-object/from16 v0, p4

    #@b7
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@b9
    const/4 v3, 0x1

    #@ba
    iput-boolean v3, v2, Landroid/content/pm/ProviderInfo;->grantUriPermissions:Z

    #@bc
    .line 2740
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@bf
    goto/16 :goto_4

    #@c1
    .line 2721
    :cond_c1
    move-object/from16 v0, p4

    #@c3
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@c5
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@c7
    array-length v8, v2

    #@c8
    .line 2722
    .local v8, N:I
    add-int/lit8 v2, v8, 0x1

    #@ca
    new-array v10, v2, [Landroid/os/PatternMatcher;

    #@cc
    .line 2723
    .local v10, newp:[Landroid/os/PatternMatcher;
    move-object/from16 v0, p4

    #@ce
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@d0
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@d2
    const/4 v3, 0x0

    #@d3
    const/4 v4, 0x0

    #@d4
    invoke-static {v2, v3, v10, v4, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@d7
    .line 2724
    aput-object v12, v10, v8

    #@d9
    .line 2725
    move-object/from16 v0, p4

    #@db
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@dd
    iput-object v10, v2, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@df
    goto :goto_b5

    #@e0
    .line 2730
    .end local v8           #N:I
    .end local v10           #newp:[Landroid/os/PatternMatcher;
    :cond_e0
    const-string v2, "PackageParser"

    #@e2
    new-instance v3, Ljava/lang/StringBuilder;

    #@e4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e7
    const-string v4, "Unknown element under <path-permission>: "

    #@e9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v3

    #@ed
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@f0
    move-result-object v4

    #@f1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v3

    #@f5
    const-string v4, " at "

    #@f7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v3

    #@fb
    move-object/from16 v0, p0

    #@fd
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@ff
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v3

    #@103
    const-string v4, " "

    #@105
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v3

    #@109
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@10c
    move-result-object v4

    #@10d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v3

    #@111
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@114
    move-result-object v3

    #@115
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@118
    .line 2733
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@11b
    goto/16 :goto_4

    #@11d
    .line 2742
    .end local v12           #pa:Landroid/os/PatternMatcher;
    .end local v16           #sa:Landroid/content/res/TypedArray;
    .end local v17           #str:Ljava/lang/String;
    :cond_11d
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@120
    move-result-object v2

    #@121
    const-string/jumbo v3, "path-permission"

    #@124
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@127
    move-result v2

    #@128
    if-eqz v2, :cond_258

    #@12a
    .line 2743
    sget-object v2, Lcom/android/internal/R$styleable;->AndroidManifestPathPermission:[I

    #@12c
    move-object/from16 v0, p1

    #@12e
    move-object/from16 v1, p3

    #@130
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@133
    move-result-object v16

    #@134
    .line 2746
    .restart local v16       #sa:Landroid/content/res/TypedArray;
    const/4 v12, 0x0

    #@135
    .line 2748
    .local v12, pa:Landroid/content/pm/PathPermission;
    const/4 v2, 0x0

    #@136
    const/4 v3, 0x0

    #@137
    move-object/from16 v0, v16

    #@139
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@13c
    move-result-object v14

    #@13d
    .line 2750
    .local v14, permission:Ljava/lang/String;
    const/4 v2, 0x1

    #@13e
    const/4 v3, 0x0

    #@13f
    move-object/from16 v0, v16

    #@141
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@144
    move-result-object v15

    #@145
    .line 2752
    .local v15, readPermission:Ljava/lang/String;
    if-nez v15, :cond_148

    #@147
    .line 2753
    move-object v15, v14

    #@148
    .line 2755
    :cond_148
    const/4 v2, 0x2

    #@149
    const/4 v3, 0x0

    #@14a
    move-object/from16 v0, v16

    #@14c
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@14f
    move-result-object v19

    #@150
    .line 2757
    .local v19, writePermission:Ljava/lang/String;
    if-nez v19, :cond_154

    #@152
    .line 2758
    move-object/from16 v19, v14

    #@154
    .line 2761
    :cond_154
    const/4 v9, 0x0

    #@155
    .line 2762
    .local v9, havePerm:Z
    if-eqz v15, :cond_15c

    #@157
    .line 2763
    invoke-virtual {v15}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@15a
    move-result-object v15

    #@15b
    .line 2764
    const/4 v9, 0x1

    #@15c
    .line 2766
    :cond_15c
    if-eqz v19, :cond_163

    #@15e
    .line 2767
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@161
    move-result-object v19

    #@162
    .line 2768
    const/4 v9, 0x1

    #@163
    .line 2771
    :cond_163
    if-nez v9, :cond_1a2

    #@165
    .line 2773
    const-string v2, "PackageParser"

    #@167
    new-instance v3, Ljava/lang/StringBuilder;

    #@169
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16c
    const-string v4, "No readPermission or writePermssion for <path-permission>: "

    #@16e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@171
    move-result-object v3

    #@172
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@175
    move-result-object v4

    #@176
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v3

    #@17a
    const-string v4, " at "

    #@17c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v3

    #@180
    move-object/from16 v0, p0

    #@182
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@184
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v3

    #@188
    const-string v4, " "

    #@18a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v3

    #@18e
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@191
    move-result-object v4

    #@192
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@195
    move-result-object v3

    #@196
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@199
    move-result-object v3

    #@19a
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19d
    .line 2776
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1a0
    goto/16 :goto_4

    #@1a2
    .line 2784
    :cond_1a2
    const/4 v2, 0x3

    #@1a3
    const/4 v3, 0x0

    #@1a4
    move-object/from16 v0, v16

    #@1a6
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@1a9
    move-result-object v13

    #@1aa
    .line 2786
    .local v13, path:Ljava/lang/String;
    if-eqz v13, :cond_1b4

    #@1ac
    .line 2787
    new-instance v12, Landroid/content/pm/PathPermission;

    #@1ae
    .end local v12           #pa:Landroid/content/pm/PathPermission;
    const/4 v2, 0x0

    #@1af
    move-object/from16 v0, v19

    #@1b1
    invoke-direct {v12, v13, v2, v15, v0}, Landroid/content/pm/PathPermission;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@1b4
    .line 2791
    .restart local v12       #pa:Landroid/content/pm/PathPermission;
    :cond_1b4
    const/4 v2, 0x4

    #@1b5
    const/4 v3, 0x0

    #@1b6
    move-object/from16 v0, v16

    #@1b8
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@1bb
    move-result-object v13

    #@1bc
    .line 2793
    if-eqz v13, :cond_1c6

    #@1be
    .line 2794
    new-instance v12, Landroid/content/pm/PathPermission;

    #@1c0
    .end local v12           #pa:Landroid/content/pm/PathPermission;
    const/4 v2, 0x1

    #@1c1
    move-object/from16 v0, v19

    #@1c3
    invoke-direct {v12, v13, v2, v15, v0}, Landroid/content/pm/PathPermission;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@1c6
    .line 2798
    .restart local v12       #pa:Landroid/content/pm/PathPermission;
    :cond_1c6
    const/4 v2, 0x5

    #@1c7
    const/4 v3, 0x0

    #@1c8
    move-object/from16 v0, v16

    #@1ca
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@1cd
    move-result-object v13

    #@1ce
    .line 2800
    if-eqz v13, :cond_1d8

    #@1d0
    .line 2801
    new-instance v12, Landroid/content/pm/PathPermission;

    #@1d2
    .end local v12           #pa:Landroid/content/pm/PathPermission;
    const/4 v2, 0x2

    #@1d3
    move-object/from16 v0, v19

    #@1d5
    invoke-direct {v12, v13, v2, v15, v0}, Landroid/content/pm/PathPermission;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@1d8
    .line 2805
    .restart local v12       #pa:Landroid/content/pm/PathPermission;
    :cond_1d8
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/TypedArray;->recycle()V

    #@1db
    .line 2807
    if-eqz v12, :cond_21b

    #@1dd
    .line 2808
    move-object/from16 v0, p4

    #@1df
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@1e1
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@1e3
    if-nez v2, :cond_1fc

    #@1e5
    .line 2809
    move-object/from16 v0, p4

    #@1e7
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@1e9
    const/4 v3, 0x1

    #@1ea
    new-array v3, v3, [Landroid/content/pm/PathPermission;

    #@1ec
    iput-object v3, v2, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@1ee
    .line 2810
    move-object/from16 v0, p4

    #@1f0
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@1f2
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@1f4
    const/4 v3, 0x0

    #@1f5
    aput-object v12, v2, v3

    #@1f7
    .line 2829
    :goto_1f7
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1fa
    goto/16 :goto_4

    #@1fc
    .line 2812
    :cond_1fc
    move-object/from16 v0, p4

    #@1fe
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@200
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@202
    array-length v8, v2

    #@203
    .line 2813
    .restart local v8       #N:I
    add-int/lit8 v2, v8, 0x1

    #@205
    new-array v10, v2, [Landroid/content/pm/PathPermission;

    #@207
    .line 2814
    .local v10, newp:[Landroid/content/pm/PathPermission;
    move-object/from16 v0, p4

    #@209
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@20b
    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@20d
    const/4 v3, 0x0

    #@20e
    const/4 v4, 0x0

    #@20f
    invoke-static {v2, v3, v10, v4, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@212
    .line 2815
    aput-object v12, v10, v8

    #@214
    .line 2816
    move-object/from16 v0, p4

    #@216
    iget-object v2, v0, Landroid/content/pm/PackageParser$Provider;->info:Landroid/content/pm/ProviderInfo;

    #@218
    iput-object v10, v2, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@21a
    goto :goto_1f7

    #@21b
    .line 2820
    .end local v8           #N:I
    .end local v10           #newp:[Landroid/content/pm/PathPermission;
    :cond_21b
    const-string v2, "PackageParser"

    #@21d
    new-instance v3, Ljava/lang/StringBuilder;

    #@21f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@222
    const-string v4, "No path, pathPrefix, or pathPattern for <path-permission>: "

    #@224
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@227
    move-result-object v3

    #@228
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@22b
    move-result-object v4

    #@22c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22f
    move-result-object v3

    #@230
    const-string v4, " at "

    #@232
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@235
    move-result-object v3

    #@236
    move-object/from16 v0, p0

    #@238
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@23a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23d
    move-result-object v3

    #@23e
    const-string v4, " "

    #@240
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@243
    move-result-object v3

    #@244
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@247
    move-result-object v4

    #@248
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24b
    move-result-object v3

    #@24c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24f
    move-result-object v3

    #@250
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@253
    .line 2823
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@256
    goto/16 :goto_4

    #@258
    .line 2833
    .end local v9           #havePerm:Z
    .end local v12           #pa:Landroid/content/pm/PathPermission;
    .end local v13           #path:Ljava/lang/String;
    .end local v14           #permission:Ljava/lang/String;
    .end local v15           #readPermission:Ljava/lang/String;
    .end local v16           #sa:Landroid/content/res/TypedArray;
    .end local v19           #writePermission:Ljava/lang/String;
    :cond_258
    const-string v2, "PackageParser"

    #@25a
    new-instance v3, Ljava/lang/StringBuilder;

    #@25c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25f
    const-string v4, "Unknown element under <provider>: "

    #@261
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@264
    move-result-object v3

    #@265
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@268
    move-result-object v4

    #@269
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26c
    move-result-object v3

    #@26d
    const-string v4, " at "

    #@26f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@272
    move-result-object v3

    #@273
    move-object/from16 v0, p0

    #@275
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@277
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27a
    move-result-object v3

    #@27b
    const-string v4, " "

    #@27d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@280
    move-result-object v3

    #@281
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@284
    move-result-object v4

    #@285
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@288
    move-result-object v3

    #@289
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28c
    move-result-object v3

    #@28d
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@290
    .line 2836
    invoke-static/range {p2 .. p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@293
    goto/16 :goto_4

    #@295
    .line 2844
    :cond_295
    const/4 v2, 0x1

    #@296
    goto/16 :goto_48
.end method

.method private parseService(Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$Service;
    .registers 27
    .parameter "owner"
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "flags"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2850
    sget-object v2, Lcom/android/internal/R$styleable;->AndroidManifestService:[I

    #@2
    move-object/from16 v0, p2

    #@4
    move-object/from16 v1, p4

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v16

    #@a
    .line 2853
    .local v16, sa:Landroid/content/res/TypedArray;
    move-object/from16 v0, p0

    #@c
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseServiceArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@e
    if-nez v2, :cond_31

    #@10
    .line 2854
    new-instance v2, Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@12
    const/4 v5, 0x2

    #@13
    const/4 v6, 0x0

    #@14
    const/4 v7, 0x1

    #@15
    const/16 v8, 0x8

    #@17
    move-object/from16 v0, p0

    #@19
    iget-object v9, v0, Landroid/content/pm/PackageParser;->mSeparateProcesses:[Ljava/lang/String;

    #@1b
    const/4 v10, 0x6

    #@1c
    const/4 v11, 0x7

    #@1d
    const/4 v12, 0x4

    #@1e
    move-object/from16 v3, p1

    #@20
    move-object/from16 v4, p6

    #@22
    invoke-direct/range {v2 .. v12}, Landroid/content/pm/PackageParser$ParseComponentArgs;-><init>(Landroid/content/pm/PackageParser$Package;[Ljava/lang/String;IIII[Ljava/lang/String;III)V

    #@25
    move-object/from16 v0, p0

    #@27
    iput-object v2, v0, Landroid/content/pm/PackageParser;->mParseServiceArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@29
    .line 2863
    move-object/from16 v0, p0

    #@2b
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseServiceArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@2d
    const-string v3, "<service>"

    #@2f
    iput-object v3, v2, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->tag:Ljava/lang/String;

    #@31
    .line 2866
    :cond_31
    move-object/from16 v0, p0

    #@33
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseServiceArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@35
    move-object/from16 v0, v16

    #@37
    iput-object v0, v2, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@39
    .line 2867
    move-object/from16 v0, p0

    #@3b
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseServiceArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@3d
    move/from16 v0, p5

    #@3f
    iput v0, v2, Landroid/content/pm/PackageParser$ParseComponentArgs;->flags:I

    #@41
    .line 2869
    new-instance v15, Landroid/content/pm/PackageParser$Service;

    #@43
    move-object/from16 v0, p0

    #@45
    iget-object v2, v0, Landroid/content/pm/PackageParser;->mParseServiceArgs:Landroid/content/pm/PackageParser$ParseComponentArgs;

    #@47
    new-instance v3, Landroid/content/pm/ServiceInfo;

    #@49
    invoke-direct {v3}, Landroid/content/pm/ServiceInfo;-><init>()V

    #@4c
    invoke-direct {v15, v2, v3}, Landroid/content/pm/PackageParser$Service;-><init>(Landroid/content/pm/PackageParser$ParseComponentArgs;Landroid/content/pm/ServiceInfo;)V

    #@4f
    .line 2870
    .local v15, s:Landroid/content/pm/PackageParser$Service;
    const/4 v2, 0x0

    #@50
    aget-object v2, p6, v2

    #@52
    if-eqz v2, :cond_59

    #@54
    .line 2871
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/TypedArray;->recycle()V

    #@57
    .line 2872
    const/4 v15, 0x0

    #@58
    .line 2964
    .end local v15           #s:Landroid/content/pm/PackageParser$Service;
    :cond_58
    :goto_58
    return-object v15

    #@59
    .line 2875
    .restart local v15       #s:Landroid/content/pm/PackageParser$Service;
    :cond_59
    const/4 v2, 0x5

    #@5a
    move-object/from16 v0, v16

    #@5c
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@5f
    move-result v17

    #@60
    .line 2877
    .local v17, setExported:Z
    if-eqz v17, :cond_6e

    #@62
    .line 2878
    iget-object v2, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@64
    const/4 v3, 0x5

    #@65
    const/4 v4, 0x0

    #@66
    move-object/from16 v0, v16

    #@68
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@6b
    move-result v3

    #@6c
    iput-boolean v3, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@6e
    .line 2882
    :cond_6e
    const/4 v2, 0x3

    #@6f
    const/4 v3, 0x0

    #@70
    move-object/from16 v0, v16

    #@72
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@75
    move-result-object v18

    #@76
    .line 2884
    .local v18, str:Ljava/lang/String;
    if-nez v18, :cond_124

    #@78
    .line 2885
    iget-object v2, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@7a
    move-object/from16 v0, p1

    #@7c
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@7e
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@80
    iput-object v3, v2, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    #@82
    .line 2890
    :goto_82
    iget-object v2, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@84
    const/4 v3, 0x0

    #@85
    iput v3, v2, Landroid/content/pm/ServiceInfo;->flags:I

    #@87
    .line 2891
    const/16 v2, 0x9

    #@89
    const/4 v3, 0x0

    #@8a
    move-object/from16 v0, v16

    #@8c
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@8f
    move-result v2

    #@90
    if-eqz v2, :cond_9a

    #@92
    .line 2894
    iget-object v2, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@94
    iget v3, v2, Landroid/content/pm/ServiceInfo;->flags:I

    #@96
    or-int/lit8 v3, v3, 0x1

    #@98
    iput v3, v2, Landroid/content/pm/ServiceInfo;->flags:I

    #@9a
    .line 2896
    :cond_9a
    const/16 v2, 0xa

    #@9c
    const/4 v3, 0x0

    #@9d
    move-object/from16 v0, v16

    #@9f
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@a2
    move-result v2

    #@a3
    if-eqz v2, :cond_ad

    #@a5
    .line 2899
    iget-object v2, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@a7
    iget v3, v2, Landroid/content/pm/ServiceInfo;->flags:I

    #@a9
    or-int/lit8 v3, v3, 0x2

    #@ab
    iput v3, v2, Landroid/content/pm/ServiceInfo;->flags:I

    #@ad
    .line 2901
    :cond_ad
    const/16 v2, 0xb

    #@af
    const/4 v3, 0x0

    #@b0
    move-object/from16 v0, v16

    #@b2
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@b5
    move-result v2

    #@b6
    if-eqz v2, :cond_104

    #@b8
    .line 2904
    iget-object v2, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@ba
    iget v3, v2, Landroid/content/pm/ServiceInfo;->flags:I

    #@bc
    const/high16 v4, 0x4000

    #@be
    or-int/2addr v3, v4

    #@bf
    iput v3, v2, Landroid/content/pm/ServiceInfo;->flags:I

    #@c1
    .line 2905
    iget-object v2, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@c3
    iget-boolean v2, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@c5
    if-eqz v2, :cond_102

    #@c7
    .line 2906
    const-string v2, "PackageParser"

    #@c9
    new-instance v3, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string v4, "Service exported request ignored due to singleUser: "

    #@d0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v3

    #@d4
    iget-object v4, v15, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@d6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v3

    #@da
    const-string v4, " at "

    #@dc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v3

    #@e0
    move-object/from16 v0, p0

    #@e2
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@e4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v3

    #@e8
    const-string v4, " "

    #@ea
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v3

    #@ee
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@f1
    move-result-object v4

    #@f2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v3

    #@f6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v3

    #@fa
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@fd
    .line 2909
    iget-object v2, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@ff
    const/4 v3, 0x0

    #@100
    iput-boolean v3, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@102
    .line 2911
    :cond_102
    const/16 v17, 0x1

    #@104
    .line 2914
    :cond_104
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/TypedArray;->recycle()V

    #@107
    .line 2916
    move-object/from16 v0, p1

    #@109
    iget-object v2, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@10b
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@10d
    const/high16 v3, 0x1000

    #@10f
    and-int/2addr v2, v3

    #@110
    if-eqz v2, :cond_13a

    #@112
    .line 2919
    iget-object v2, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@114
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@116
    move-object/from16 v0, p1

    #@118
    iget-object v3, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@11a
    if-ne v2, v3, :cond_13a

    #@11c
    .line 2920
    const/4 v2, 0x0

    #@11d
    const-string v3, "Heavy-weight applications can not have services in main process"

    #@11f
    aput-object v3, p6, v2

    #@121
    .line 2921
    const/4 v15, 0x0

    #@122
    goto/16 :goto_58

    #@124
    .line 2887
    :cond_124
    iget-object v3, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@126
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@129
    move-result v2

    #@12a
    if-lez v2, :cond_138

    #@12c
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@12f
    move-result-object v2

    #@130
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@133
    move-result-object v2

    #@134
    :goto_134
    iput-object v2, v3, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    #@136
    goto/16 :goto_82

    #@138
    :cond_138
    const/4 v2, 0x0

    #@139
    goto :goto_134

    #@13a
    .line 2925
    :cond_13a
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@13d
    move-result v14

    #@13e
    .line 2928
    .local v14, outerDepth:I
    :cond_13e
    :goto_13e
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@141
    move-result v19

    #@142
    .local v19, type:I
    const/4 v2, 0x1

    #@143
    move/from16 v0, v19

    #@145
    if-eq v0, v2, :cond_1ea

    #@147
    const/4 v2, 0x3

    #@148
    move/from16 v0, v19

    #@14a
    if-ne v0, v2, :cond_152

    #@14c
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@14f
    move-result v2

    #@150
    if-le v2, v14, :cond_1ea

    #@152
    .line 2930
    :cond_152
    const/4 v2, 0x3

    #@153
    move/from16 v0, v19

    #@155
    if-eq v0, v2, :cond_13e

    #@157
    const/4 v2, 0x4

    #@158
    move/from16 v0, v19

    #@15a
    if-eq v0, v2, :cond_13e

    #@15c
    .line 2934
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@15f
    move-result-object v2

    #@160
    const-string v3, "intent-filter"

    #@162
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@165
    move-result v2

    #@166
    if-eqz v2, :cond_189

    #@168
    .line 2935
    new-instance v7, Landroid/content/pm/PackageParser$ServiceIntentInfo;

    #@16a
    invoke-direct {v7, v15}, Landroid/content/pm/PackageParser$ServiceIntentInfo;-><init>(Landroid/content/pm/PackageParser$Service;)V

    #@16d
    .line 2936
    .local v7, intent:Landroid/content/pm/PackageParser$ServiceIntentInfo;
    const/4 v9, 0x0

    #@16e
    move-object/from16 v2, p0

    #@170
    move-object/from16 v3, p2

    #@172
    move-object/from16 v4, p3

    #@174
    move-object/from16 v5, p4

    #@176
    move/from16 v6, p5

    #@178
    move-object/from16 v8, p6

    #@17a
    invoke-direct/range {v2 .. v9}, Landroid/content/pm/PackageParser;->parseIntent(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ILandroid/content/pm/PackageParser$IntentInfo;[Ljava/lang/String;Z)Z

    #@17d
    move-result v2

    #@17e
    if-nez v2, :cond_183

    #@180
    .line 2937
    const/4 v15, 0x0

    #@181
    goto/16 :goto_58

    #@183
    .line 2940
    :cond_183
    iget-object v2, v15, Landroid/content/pm/PackageParser$Component;->intents:Ljava/util/ArrayList;

    #@185
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@188
    goto :goto_13e

    #@189
    .line 2941
    .end local v7           #intent:Landroid/content/pm/PackageParser$ServiceIntentInfo;
    :cond_189
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@18c
    move-result-object v2

    #@18d
    const-string/jumbo v3, "meta-data"

    #@190
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@193
    move-result v2

    #@194
    if-eqz v2, :cond_1ad

    #@196
    .line 2942
    iget-object v12, v15, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@198
    move-object/from16 v8, p0

    #@19a
    move-object/from16 v9, p2

    #@19c
    move-object/from16 v10, p3

    #@19e
    move-object/from16 v11, p4

    #@1a0
    move-object/from16 v13, p6

    #@1a2
    invoke-direct/range {v8 .. v13}, Landroid/content/pm/PackageParser;->parseMetaData(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;

    #@1a5
    move-result-object v2

    #@1a6
    iput-object v2, v15, Landroid/content/pm/PackageParser$Component;->metaData:Landroid/os/Bundle;

    #@1a8
    if-nez v2, :cond_13e

    #@1aa
    .line 2944
    const/4 v15, 0x0

    #@1ab
    goto/16 :goto_58

    #@1ad
    .line 2948
    :cond_1ad
    const-string v2, "PackageParser"

    #@1af
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b4
    const-string v4, "Unknown element under <service>: "

    #@1b6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v3

    #@1ba
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1bd
    move-result-object v4

    #@1be
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v3

    #@1c2
    const-string v4, " at "

    #@1c4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v3

    #@1c8
    move-object/from16 v0, p0

    #@1ca
    iget-object v4, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@1cc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cf
    move-result-object v3

    #@1d0
    const-string v4, " "

    #@1d2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v3

    #@1d6
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@1d9
    move-result-object v4

    #@1da
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dd
    move-result-object v3

    #@1de
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e1
    move-result-object v3

    #@1e2
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e5
    .line 2951
    invoke-static/range {p3 .. p3}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1e8
    goto/16 :goto_13e

    #@1ea
    .line 2960
    :cond_1ea
    if-nez v17, :cond_58

    #@1ec
    .line 2961
    iget-object v3, v15, Landroid/content/pm/PackageParser$Service;->info:Landroid/content/pm/ServiceInfo;

    #@1ee
    iget-object v2, v15, Landroid/content/pm/PackageParser$Component;->intents:Ljava/util/ArrayList;

    #@1f0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1f3
    move-result v2

    #@1f4
    if-lez v2, :cond_1fb

    #@1f6
    const/4 v2, 0x1

    #@1f7
    :goto_1f7
    iput-boolean v2, v3, Landroid/content/pm/ComponentInfo;->exported:Z

    #@1f9
    goto/16 :goto_58

    #@1fb
    :cond_1fb
    const/4 v2, 0x0

    #@1fc
    goto :goto_1f7
.end method

.method private static parseVerifier(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;I[Ljava/lang/String;)Landroid/content/pm/VerifierInfo;
    .registers 16
    .parameter "res"
    .parameter "parser"
    .parameter "attrs"
    .parameter "flags"
    .parameter "outError"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3069
    sget-object v8, Lcom/android/internal/R$styleable;->AndroidManifestPackageVerifier:[I

    #@2
    invoke-virtual {p0, p2, v8}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@5
    move-result-object v7

    #@6
    .line 3072
    .local v7, sa:Landroid/content/res/TypedArray;
    const/4 v8, 0x0

    #@7
    invoke-virtual {v7, v8}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@a
    move-result-object v5

    #@b
    .line 3075
    .local v5, packageName:Ljava/lang/String;
    const/4 v8, 0x1

    #@c
    invoke-virtual {v7, v8}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    .line 3078
    .local v2, encodedPublicKey:Ljava/lang/String;
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    #@13
    .line 3080
    if-eqz v5, :cond_1b

    #@15
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@18
    move-result v8

    #@19
    if-nez v8, :cond_25

    #@1b
    .line 3081
    :cond_1b
    const-string v8, "PackageParser"

    #@1d
    const-string/jumbo v9, "verifier package name was null; skipping"

    #@20
    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 3082
    const/4 v8, 0x0

    #@24
    .line 3120
    :goto_24
    return-object v8

    #@25
    .line 3083
    :cond_25
    if-nez v2, :cond_46

    #@27
    .line 3084
    const-string v8, "PackageParser"

    #@29
    new-instance v9, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string/jumbo v10, "verifier "

    #@31
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v9

    #@35
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v9

    #@39
    const-string v10, " public key was null; skipping"

    #@3b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v9

    #@3f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v9

    #@43
    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 3089
    :cond_46
    const/4 v8, 0x0

    #@47
    :try_start_47
    invoke-static {v2, v8}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    #@4a
    move-result-object v1

    #@4b
    .line 3090
    .local v1, encoded:[B
    new-instance v4, Ljava/security/spec/X509EncodedKeySpec;

    #@4d
    invoke-direct {v4, v1}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V
    :try_end_50
    .catch Ljava/lang/IllegalArgumentException; {:try_start_47 .. :try_end_50} :catch_6a

    #@50
    .line 3098
    .local v4, keySpec:Ljava/security/spec/EncodedKeySpec;
    :try_start_50
    const-string v8, "RSA"

    #@52
    invoke-static {v8}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    #@55
    move-result-object v3

    #@56
    .line 3099
    .local v3, keyFactory:Ljava/security/KeyFactory;
    invoke-virtual {v3, v4}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    #@59
    move-result-object v6

    #@5a
    .line 3100
    .local v6, publicKey:Ljava/security/PublicKey;
    new-instance v8, Landroid/content/pm/VerifierInfo;

    #@5c
    invoke-direct {v8, v5, v6}, Landroid/content/pm/VerifierInfo;-><init>(Ljava/lang/String;Ljava/security/PublicKey;)V
    :try_end_5f
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_50 .. :try_end_5f} :catch_60
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_50 .. :try_end_5f} :catch_8b

    #@5f
    goto :goto_24

    #@60
    .line 3101
    .end local v3           #keyFactory:Ljava/security/KeyFactory;
    .end local v6           #publicKey:Ljava/security/PublicKey;
    :catch_60
    move-exception v0

    #@61
    .line 3102
    .local v0, e:Ljava/security/NoSuchAlgorithmException;
    const-string v8, "PackageParser"

    #@63
    const-string v9, "Could not parse public key because RSA isn\'t included in build"

    #@65
    invoke-static {v8, v9}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 3103
    const/4 v8, 0x0

    #@69
    goto :goto_24

    #@6a
    .line 3091
    .end local v0           #e:Ljava/security/NoSuchAlgorithmException;
    .end local v1           #encoded:[B
    .end local v4           #keySpec:Ljava/security/spec/EncodedKeySpec;
    :catch_6a
    move-exception v0

    #@6b
    .line 3092
    .local v0, e:Ljava/lang/IllegalArgumentException;
    const-string v8, "PackageParser"

    #@6d
    new-instance v9, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v10, "Could not parse verifier "

    #@74
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v9

    #@78
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v9

    #@7c
    const-string v10, " public key; invalid Base64"

    #@7e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v9

    #@82
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v9

    #@86
    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 3093
    const/4 v8, 0x0

    #@8a
    goto :goto_24

    #@8b
    .line 3104
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    .restart local v1       #encoded:[B
    .restart local v4       #keySpec:Ljava/security/spec/EncodedKeySpec;
    :catch_8b
    move-exception v8

    #@8c
    .line 3110
    :try_start_8c
    const-string v8, "DSA"

    #@8e
    invoke-static {v8}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    #@91
    move-result-object v3

    #@92
    .line 3111
    .restart local v3       #keyFactory:Ljava/security/KeyFactory;
    invoke-virtual {v3, v4}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    #@95
    move-result-object v6

    #@96
    .line 3112
    .restart local v6       #publicKey:Ljava/security/PublicKey;
    new-instance v8, Landroid/content/pm/VerifierInfo;

    #@98
    invoke-direct {v8, v5, v6}, Landroid/content/pm/VerifierInfo;-><init>(Ljava/lang/String;Ljava/security/PublicKey;)V
    :try_end_9b
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_8c .. :try_end_9b} :catch_9c
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_8c .. :try_end_9b} :catch_a7

    #@9b
    goto :goto_24

    #@9c
    .line 3113
    .end local v3           #keyFactory:Ljava/security/KeyFactory;
    .end local v6           #publicKey:Ljava/security/PublicKey;
    :catch_9c
    move-exception v0

    #@9d
    .line 3114
    .local v0, e:Ljava/security/NoSuchAlgorithmException;
    const-string v8, "PackageParser"

    #@9f
    const-string v9, "Could not parse public key because DSA isn\'t included in build"

    #@a1
    invoke-static {v8, v9}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 3115
    const/4 v8, 0x0

    #@a5
    goto/16 :goto_24

    #@a7
    .line 3116
    .end local v0           #e:Ljava/security/NoSuchAlgorithmException;
    :catch_a7
    move-exception v8

    #@a8
    .line 3120
    const/4 v8, 0x0

    #@a9
    goto/16 :goto_24
.end method

.method public static setCompatibilityModeEnabled(Z)V
    .registers 1
    .parameter "compatibilityModeEnabled"

    #@0
    .prologue
    .line 3909
    sput-boolean p0, Landroid/content/pm/PackageParser;->sCompatibilityModeEnabled:Z

    #@2
    .line 3910
    return-void
.end method

.method public static stringToSignature(Ljava/lang/String;)Landroid/content/pm/Signature;
    .registers 5
    .parameter "str"

    #@0
    .prologue
    .line 929
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    .line 930
    .local v0, N:I
    new-array v2, v0, [B

    #@6
    .line 931
    .local v2, sig:[B
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_13

    #@9
    .line 932
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v3

    #@d
    int-to-byte v3, v3

    #@e
    aput-byte v3, v2, v1

    #@10
    .line 931
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_7

    #@13
    .line 934
    :cond_13
    new-instance v3, Landroid/content/pm/Signature;

    #@15
    invoke-direct {v3, v2}, Landroid/content/pm/Signature;-><init>([B)V

    #@18
    return-object v3
.end method

.method private static validateName(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 9
    .parameter "name"
    .parameter "requiresSeparator"

    #@0
    .prologue
    .line 795
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    .line 796
    .local v0, N:I
    const/4 v3, 0x0

    #@5
    .line 797
    .local v3, hasSep:Z
    const/4 v2, 0x1

    #@6
    .line 798
    .local v2, front:Z
    const/4 v4, 0x0

    #@7
    .local v4, i:I
    :goto_7
    if-ge v4, v0, :cond_50

    #@9
    .line 799
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v1

    #@d
    .line 800
    .local v1, c:C
    const/16 v5, 0x61

    #@f
    if-lt v1, v5, :cond_15

    #@11
    const/16 v5, 0x7a

    #@13
    if-le v1, v5, :cond_1d

    #@15
    :cond_15
    const/16 v5, 0x41

    #@17
    if-lt v1, v5, :cond_21

    #@19
    const/16 v5, 0x5a

    #@1b
    if-gt v1, v5, :cond_21

    #@1d
    .line 801
    :cond_1d
    const/4 v2, 0x0

    #@1e
    .line 798
    :cond_1e
    :goto_1e
    add-int/lit8 v4, v4, 0x1

    #@20
    goto :goto_7

    #@21
    .line 804
    :cond_21
    if-nez v2, :cond_2f

    #@23
    .line 805
    const/16 v5, 0x30

    #@25
    if-lt v1, v5, :cond_2b

    #@27
    const/16 v5, 0x39

    #@29
    if-le v1, v5, :cond_1e

    #@2b
    :cond_2b
    const/16 v5, 0x5f

    #@2d
    if-eq v1, v5, :cond_1e

    #@2f
    .line 809
    :cond_2f
    const/16 v5, 0x2e

    #@31
    if-ne v1, v5, :cond_36

    #@33
    .line 810
    const/4 v3, 0x1

    #@34
    .line 811
    const/4 v2, 0x1

    #@35
    .line 812
    goto :goto_1e

    #@36
    .line 814
    :cond_36
    new-instance v5, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v6, "bad character \'"

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    const-string v6, "\'"

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    .line 816
    .end local v1           #c:C
    :goto_4f
    return-object v5

    #@50
    :cond_50
    if-nez v3, :cond_54

    #@52
    if-nez p1, :cond_56

    #@54
    :cond_54
    const/4 v5, 0x0

    #@55
    goto :goto_4f

    #@56
    :cond_56
    const-string/jumbo v5, "must have at least one \'.\' separator"

    #@59
    goto :goto_4f
.end method


# virtual methods
.method public collectCertificates(Landroid/content/pm/PackageParser$Package;I)Z
    .registers 24
    .parameter "pkg"
    .parameter "flags"

    #@0
    .prologue
    .line 602
    const/16 v18, 0x0

    #@2
    move-object/from16 v0, v18

    #@4
    move-object/from16 v1, p1

    #@6
    iput-object v0, v1, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    #@8
    .line 605
    const/16 v16, 0x0

    #@a
    .line 606
    .local v16, readBuffer:[B
    sget-object v19, Landroid/content/pm/PackageParser;->mSync:Ljava/lang/Object;

    #@c
    monitor-enter v19

    #@d
    .line 607
    :try_start_d
    sget-object v17, Landroid/content/pm/PackageParser;->mReadBuffer:Ljava/lang/ref/WeakReference;

    #@f
    .line 608
    .local v17, readBufferRef:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<[B>;"
    if-eqz v17, :cond_1f

    #@11
    .line 609
    const/16 v18, 0x0

    #@13
    sput-object v18, Landroid/content/pm/PackageParser;->mReadBuffer:Ljava/lang/ref/WeakReference;

    #@15
    .line 610
    invoke-virtual/range {v17 .. v17}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@18
    move-result-object v18

    #@19
    move-object/from16 v0, v18

    #@1b
    check-cast v0, [B

    #@1d
    move-object/from16 v16, v0

    #@1f
    .line 612
    :cond_1f
    if-nez v16, :cond_32

    #@21
    .line 613
    const/16 v18, 0x2000

    #@23
    move/from16 v0, v18

    #@25
    new-array v0, v0, [B

    #@27
    move-object/from16 v16, v0

    #@29
    .line 614
    new-instance v17, Ljava/lang/ref/WeakReference;

    #@2b
    .end local v17           #readBufferRef:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<[B>;"
    move-object/from16 v0, v17

    #@2d
    move-object/from16 v1, v16

    #@2f
    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@32
    .line 616
    .restart local v17       #readBufferRef:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<[B>;"
    :cond_32
    monitor-exit v19
    :try_end_33
    .catchall {:try_start_d .. :try_end_33} :catchall_97

    #@33
    .line 619
    :try_start_33
    new-instance v11, Ljava/util/jar/JarFile;

    #@35
    move-object/from16 v0, p0

    #@37
    iget-object v0, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@39
    move-object/from16 v18, v0

    #@3b
    move-object/from16 v0, v18

    #@3d
    invoke-direct {v11, v0}, Ljava/util/jar/JarFile;-><init>(Ljava/lang/String;)V

    #@40
    .line 621
    .local v11, jarFile:Ljava/util/jar/JarFile;
    const/4 v4, 0x0

    #@41
    .line 623
    .local v4, certs:[Ljava/security/cert/Certificate;
    and-int/lit8 v18, p2, 0x1

    #@43
    if-eqz v18, :cond_9a

    #@45
    .line 628
    const-string v18, "AndroidManifest.xml"

    #@47
    move-object/from16 v0, v18

    #@49
    invoke-virtual {v11, v0}, Ljava/util/jar/JarFile;->getJarEntry(Ljava/lang/String;)Ljava/util/jar/JarEntry;

    #@4c
    move-result-object v10

    #@4d
    .line 629
    .local v10, jarEntry:Ljava/util/jar/JarEntry;
    move-object/from16 v0, p0

    #@4f
    move-object/from16 v1, v16

    #@51
    invoke-direct {v0, v11, v10, v1}, Landroid/content/pm/PackageParser;->loadCertificates(Ljava/util/jar/JarFile;Ljava/util/jar/JarEntry;[B)[Ljava/security/cert/Certificate;

    #@54
    move-result-object v4

    #@55
    .line 630
    if-nez v4, :cond_19f

    #@57
    .line 631
    const-string v18, "PackageParser"

    #@59
    new-instance v19, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v20, "Package "

    #@60
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v19

    #@64
    move-object/from16 v0, p1

    #@66
    iget-object v0, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@68
    move-object/from16 v20, v0

    #@6a
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v19

    #@6e
    const-string v20, " has no certificates at entry "

    #@70
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v19

    #@74
    invoke-virtual {v10}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    #@77
    move-result-object v20

    #@78
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v19

    #@7c
    const-string v20, "; ignoring!"

    #@7e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v19

    #@82
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v19

    #@86
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 634
    invoke-virtual {v11}, Ljava/util/jar/JarFile;->close()V

    #@8c
    .line 635
    const/16 v18, -0x67

    #@8e
    move/from16 v0, v18

    #@90
    move-object/from16 v1, p0

    #@92
    iput v0, v1, Landroid/content/pm/PackageParser;->mParseError:I
    :try_end_94
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_33 .. :try_end_94} :catch_1db
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_94} :catch_23a
    .catch Ljava/lang/RuntimeException; {:try_start_33 .. :try_end_94} :catch_269

    #@94
    .line 636
    const/16 v18, 0x0

    #@96
    .line 739
    .end local v4           #certs:[Ljava/security/cert/Certificate;
    .end local v10           #jarEntry:Ljava/util/jar/JarEntry;
    .end local v11           #jarFile:Ljava/util/jar/JarFile;
    :goto_96
    return v18

    #@97
    .line 616
    .end local v17           #readBufferRef:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<[B>;"
    :catchall_97
    move-exception v18

    #@98
    :try_start_98
    monitor-exit v19
    :try_end_99
    .catchall {:try_start_98 .. :try_end_99} :catchall_97

    #@99
    throw v18

    #@9a
    .line 651
    .restart local v4       #certs:[Ljava/security/cert/Certificate;
    .restart local v11       #jarFile:Ljava/util/jar/JarFile;
    .restart local v17       #readBufferRef:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<[B>;"
    :cond_9a
    :try_start_9a
    invoke-virtual {v11}, Ljava/util/jar/JarFile;->entries()Ljava/util/Enumeration;

    #@9d
    move-result-object v6

    #@9e
    .line 652
    .local v6, entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/util/jar/JarEntry;>;"
    invoke-virtual {v11}, Ljava/util/jar/JarFile;->getManifest()Ljava/util/jar/Manifest;

    #@a1
    move-result-object v14

    #@a2
    .line 653
    .local v14, manifest:Ljava/util/jar/Manifest;
    :cond_a2
    :goto_a2
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    #@a5
    move-result v18

    #@a6
    if-eqz v18, :cond_19f

    #@a8
    .line 654
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    #@ab
    move-result-object v12

    #@ac
    check-cast v12, Ljava/util/jar/JarEntry;

    #@ae
    .line 655
    .local v12, je:Ljava/util/jar/JarEntry;
    invoke-virtual {v12}, Ljava/util/jar/JarEntry;->isDirectory()Z

    #@b1
    move-result v18

    #@b2
    if-nez v18, :cond_a2

    #@b4
    .line 657
    invoke-virtual {v12}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    #@b7
    move-result-object v15

    #@b8
    .line 659
    .local v15, name:Ljava/lang/String;
    const-string v18, "META-INF/"

    #@ba
    move-object/from16 v0, v18

    #@bc
    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@bf
    move-result v18

    #@c0
    if-nez v18, :cond_a2

    #@c2
    .line 662
    const-string v18, "AndroidManifest.xml"

    #@c4
    move-object/from16 v0, v18

    #@c6
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c9
    move-result v18

    #@ca
    if-eqz v18, :cond_da

    #@cc
    .line 663
    invoke-virtual {v14, v15}, Ljava/util/jar/Manifest;->getAttributes(Ljava/lang/String;)Ljava/util/jar/Attributes;

    #@cf
    move-result-object v3

    #@d0
    .line 664
    .local v3, attributes:Ljava/util/jar/Attributes;
    invoke-static {v3}, Landroid/content/pm/ManifestDigest;->fromAttributes(Ljava/util/jar/Attributes;)Landroid/content/pm/ManifestDigest;

    #@d3
    move-result-object v18

    #@d4
    move-object/from16 v0, v18

    #@d6
    move-object/from16 v1, p1

    #@d8
    iput-object v0, v1, Landroid/content/pm/PackageParser$Package;->manifestDigest:Landroid/content/pm/ManifestDigest;

    #@da
    .line 667
    .end local v3           #attributes:Ljava/util/jar/Attributes;
    :cond_da
    move-object/from16 v0, p0

    #@dc
    move-object/from16 v1, v16

    #@de
    invoke-direct {v0, v11, v12, v1}, Landroid/content/pm/PackageParser;->loadCertificates(Ljava/util/jar/JarFile;Ljava/util/jar/JarEntry;[B)[Ljava/security/cert/Certificate;

    #@e1
    move-result-object v13

    #@e2
    .line 674
    .local v13, localCerts:[Ljava/security/cert/Certificate;
    if-nez v13, :cond_125

    #@e4
    .line 675
    const-string v18, "PackageParser"

    #@e6
    new-instance v19, Ljava/lang/StringBuilder;

    #@e8
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@eb
    const-string v20, "Package "

    #@ed
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v19

    #@f1
    move-object/from16 v0, p1

    #@f3
    iget-object v0, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@f5
    move-object/from16 v20, v0

    #@f7
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v19

    #@fb
    const-string v20, " has no certificates at entry "

    #@fd
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v19

    #@101
    invoke-virtual {v12}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    #@104
    move-result-object v20

    #@105
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v19

    #@109
    const-string v20, "; ignoring!"

    #@10b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v19

    #@10f
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@112
    move-result-object v19

    #@113
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@116
    .line 678
    invoke-virtual {v11}, Ljava/util/jar/JarFile;->close()V

    #@119
    .line 679
    const/16 v18, -0x67

    #@11b
    move/from16 v0, v18

    #@11d
    move-object/from16 v1, p0

    #@11f
    iput v0, v1, Landroid/content/pm/PackageParser;->mParseError:I

    #@121
    .line 680
    const/16 v18, 0x0

    #@123
    goto/16 :goto_96

    #@125
    .line 681
    :cond_125
    if-nez v4, :cond_12a

    #@127
    .line 682
    move-object v4, v13

    #@128
    goto/16 :goto_a2

    #@12a
    .line 685
    :cond_12a
    const/4 v8, 0x0

    #@12b
    .local v8, i:I
    :goto_12b
    array-length v0, v4

    #@12c
    move/from16 v18, v0

    #@12e
    move/from16 v0, v18

    #@130
    if-ge v8, v0, :cond_a2

    #@132
    .line 686
    const/4 v7, 0x0

    #@133
    .line 687
    .local v7, found:Z
    const/4 v9, 0x0

    #@134
    .local v9, j:I
    :goto_134
    array-length v0, v13

    #@135
    move/from16 v18, v0

    #@137
    move/from16 v0, v18

    #@139
    if-ge v9, v0, :cond_14a

    #@13b
    .line 688
    aget-object v18, v4, v8

    #@13d
    if-eqz v18, :cond_199

    #@13f
    aget-object v18, v4, v8

    #@141
    aget-object v19, v13, v9

    #@143
    invoke-virtual/range {v18 .. v19}, Ljava/security/cert/Certificate;->equals(Ljava/lang/Object;)Z

    #@146
    move-result v18

    #@147
    if-eqz v18, :cond_199

    #@149
    .line 690
    const/4 v7, 0x1

    #@14a
    .line 694
    :cond_14a
    if-eqz v7, :cond_158

    #@14c
    array-length v0, v4

    #@14d
    move/from16 v18, v0

    #@14f
    array-length v0, v13

    #@150
    move/from16 v19, v0

    #@152
    move/from16 v0, v18

    #@154
    move/from16 v1, v19

    #@156
    if-eq v0, v1, :cond_19c

    #@158
    .line 695
    :cond_158
    const-string v18, "PackageParser"

    #@15a
    new-instance v19, Ljava/lang/StringBuilder;

    #@15c
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@15f
    const-string v20, "Package "

    #@161
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v19

    #@165
    move-object/from16 v0, p1

    #@167
    iget-object v0, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@169
    move-object/from16 v20, v0

    #@16b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v19

    #@16f
    const-string v20, " has mismatched certificates at entry "

    #@171
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v19

    #@175
    invoke-virtual {v12}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    #@178
    move-result-object v20

    #@179
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v19

    #@17d
    const-string v20, "; ignoring!"

    #@17f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v19

    #@183
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@186
    move-result-object v19

    #@187
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18a
    .line 698
    invoke-virtual {v11}, Ljava/util/jar/JarFile;->close()V

    #@18d
    .line 699
    const/16 v18, -0x68

    #@18f
    move/from16 v0, v18

    #@191
    move-object/from16 v1, p0

    #@193
    iput v0, v1, Landroid/content/pm/PackageParser;->mParseError:I

    #@195
    .line 700
    const/16 v18, 0x0

    #@197
    goto/16 :goto_96

    #@199
    .line 687
    :cond_199
    add-int/lit8 v9, v9, 0x1

    #@19b
    goto :goto_134

    #@19c
    .line 685
    :cond_19c
    add-int/lit8 v8, v8, 0x1

    #@19e
    goto :goto_12b

    #@19f
    .line 706
    .end local v6           #entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/util/jar/JarEntry;>;"
    .end local v7           #found:Z
    .end local v8           #i:I
    .end local v9           #j:I
    .end local v12           #je:Ljava/util/jar/JarEntry;
    .end local v13           #localCerts:[Ljava/security/cert/Certificate;
    .end local v14           #manifest:Ljava/util/jar/Manifest;
    .end local v15           #name:Ljava/lang/String;
    :cond_19f
    invoke-virtual {v11}, Ljava/util/jar/JarFile;->close()V

    #@1a2
    .line 708
    sget-object v19, Landroid/content/pm/PackageParser;->mSync:Ljava/lang/Object;

    #@1a4
    monitor-enter v19
    :try_end_1a5
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_9a .. :try_end_1a5} :catch_1db
    .catch Ljava/io/IOException; {:try_start_9a .. :try_end_1a5} :catch_23a
    .catch Ljava/lang/RuntimeException; {:try_start_9a .. :try_end_1a5} :catch_269

    #@1a5
    .line 709
    :try_start_1a5
    sput-object v17, Landroid/content/pm/PackageParser;->mReadBuffer:Ljava/lang/ref/WeakReference;

    #@1a7
    .line 710
    monitor-exit v19
    :try_end_1a8
    .catchall {:try_start_1a5 .. :try_end_1a8} :catchall_1d8

    #@1a8
    .line 712
    if-eqz v4, :cond_20a

    #@1aa
    :try_start_1aa
    array-length v0, v4

    #@1ab
    move/from16 v18, v0

    #@1ad
    if-lez v18, :cond_20a

    #@1af
    .line 713
    array-length v2, v4

    #@1b0
    .line 714
    .local v2, N:I
    array-length v0, v4

    #@1b1
    move/from16 v18, v0

    #@1b3
    move/from16 v0, v18

    #@1b5
    new-array v0, v0, [Landroid/content/pm/Signature;

    #@1b7
    move-object/from16 v18, v0

    #@1b9
    move-object/from16 v0, v18

    #@1bb
    move-object/from16 v1, p1

    #@1bd
    iput-object v0, v1, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    #@1bf
    .line 715
    const/4 v8, 0x0

    #@1c0
    .restart local v8       #i:I
    :goto_1c0
    if-ge v8, v2, :cond_298

    #@1c2
    .line 716
    move-object/from16 v0, p1

    #@1c4
    iget-object v0, v0, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    #@1c6
    move-object/from16 v18, v0

    #@1c8
    new-instance v19, Landroid/content/pm/Signature;

    #@1ca
    aget-object v20, v4, v8

    #@1cc
    invoke-virtual/range {v20 .. v20}, Ljava/security/cert/Certificate;->getEncoded()[B

    #@1cf
    move-result-object v20

    #@1d0
    invoke-direct/range {v19 .. v20}, Landroid/content/pm/Signature;-><init>([B)V

    #@1d3
    aput-object v19, v18, v8
    :try_end_1d5
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_1aa .. :try_end_1d5} :catch_1db
    .catch Ljava/io/IOException; {:try_start_1aa .. :try_end_1d5} :catch_23a
    .catch Ljava/lang/RuntimeException; {:try_start_1aa .. :try_end_1d5} :catch_269

    #@1d5
    .line 715
    add-int/lit8 v8, v8, 0x1

    #@1d7
    goto :goto_1c0

    #@1d8
    .line 710
    .end local v2           #N:I
    .end local v8           #i:I
    :catchall_1d8
    move-exception v18

    #@1d9
    :try_start_1d9
    monitor-exit v19
    :try_end_1da
    .catchall {:try_start_1d9 .. :try_end_1da} :catchall_1d8

    #@1da
    :try_start_1da
    throw v18
    :try_end_1db
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_1da .. :try_end_1db} :catch_1db
    .catch Ljava/io/IOException; {:try_start_1da .. :try_end_1db} :catch_23a
    .catch Ljava/lang/RuntimeException; {:try_start_1da .. :try_end_1db} :catch_269

    #@1db
    .line 725
    .end local v4           #certs:[Ljava/security/cert/Certificate;
    .end local v11           #jarFile:Ljava/util/jar/JarFile;
    :catch_1db
    move-exception v5

    #@1dc
    .line 726
    .local v5, e:Ljava/security/cert/CertificateEncodingException;
    const-string v18, "PackageParser"

    #@1de
    new-instance v19, Ljava/lang/StringBuilder;

    #@1e0
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1e3
    const-string v20, "Exception reading "

    #@1e5
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v19

    #@1e9
    move-object/from16 v0, p0

    #@1eb
    iget-object v0, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@1ed
    move-object/from16 v20, v0

    #@1ef
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v19

    #@1f3
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f6
    move-result-object v19

    #@1f7
    move-object/from16 v0, v18

    #@1f9
    move-object/from16 v1, v19

    #@1fb
    invoke-static {v0, v1, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1fe
    .line 727
    const/16 v18, -0x69

    #@200
    move/from16 v0, v18

    #@202
    move-object/from16 v1, p0

    #@204
    iput v0, v1, Landroid/content/pm/PackageParser;->mParseError:I

    #@206
    .line 728
    const/16 v18, 0x0

    #@208
    goto/16 :goto_96

    #@20a
    .line 720
    .end local v5           #e:Ljava/security/cert/CertificateEncodingException;
    .restart local v4       #certs:[Ljava/security/cert/Certificate;
    .restart local v11       #jarFile:Ljava/util/jar/JarFile;
    :cond_20a
    :try_start_20a
    const-string v18, "PackageParser"

    #@20c
    new-instance v19, Ljava/lang/StringBuilder;

    #@20e
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@211
    const-string v20, "Package "

    #@213
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@216
    move-result-object v19

    #@217
    move-object/from16 v0, p1

    #@219
    iget-object v0, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@21b
    move-object/from16 v20, v0

    #@21d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v19

    #@221
    const-string v20, " has no certificates; ignoring!"

    #@223
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v19

    #@227
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22a
    move-result-object v19

    #@22b
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22e
    .line 722
    const/16 v18, -0x67

    #@230
    move/from16 v0, v18

    #@232
    move-object/from16 v1, p0

    #@234
    iput v0, v1, Landroid/content/pm/PackageParser;->mParseError:I
    :try_end_236
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_20a .. :try_end_236} :catch_1db
    .catch Ljava/io/IOException; {:try_start_20a .. :try_end_236} :catch_23a
    .catch Ljava/lang/RuntimeException; {:try_start_20a .. :try_end_236} :catch_269

    #@236
    .line 723
    const/16 v18, 0x0

    #@238
    goto/16 :goto_96

    #@23a
    .line 729
    .end local v4           #certs:[Ljava/security/cert/Certificate;
    .end local v11           #jarFile:Ljava/util/jar/JarFile;
    :catch_23a
    move-exception v5

    #@23b
    .line 730
    .local v5, e:Ljava/io/IOException;
    const-string v18, "PackageParser"

    #@23d
    new-instance v19, Ljava/lang/StringBuilder;

    #@23f
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@242
    const-string v20, "Exception reading "

    #@244
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@247
    move-result-object v19

    #@248
    move-object/from16 v0, p0

    #@24a
    iget-object v0, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@24c
    move-object/from16 v20, v0

    #@24e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@251
    move-result-object v19

    #@252
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@255
    move-result-object v19

    #@256
    move-object/from16 v0, v18

    #@258
    move-object/from16 v1, v19

    #@25a
    invoke-static {v0, v1, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25d
    .line 731
    const/16 v18, -0x69

    #@25f
    move/from16 v0, v18

    #@261
    move-object/from16 v1, p0

    #@263
    iput v0, v1, Landroid/content/pm/PackageParser;->mParseError:I

    #@265
    .line 732
    const/16 v18, 0x0

    #@267
    goto/16 :goto_96

    #@269
    .line 733
    .end local v5           #e:Ljava/io/IOException;
    :catch_269
    move-exception v5

    #@26a
    .line 734
    .local v5, e:Ljava/lang/RuntimeException;
    const-string v18, "PackageParser"

    #@26c
    new-instance v19, Ljava/lang/StringBuilder;

    #@26e
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@271
    const-string v20, "Exception reading "

    #@273
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@276
    move-result-object v19

    #@277
    move-object/from16 v0, p0

    #@279
    iget-object v0, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@27b
    move-object/from16 v20, v0

    #@27d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@280
    move-result-object v19

    #@281
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@284
    move-result-object v19

    #@285
    move-object/from16 v0, v18

    #@287
    move-object/from16 v1, v19

    #@289
    invoke-static {v0, v1, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@28c
    .line 735
    const/16 v18, -0x66

    #@28e
    move/from16 v0, v18

    #@290
    move-object/from16 v1, p0

    #@292
    iput v0, v1, Landroid/content/pm/PackageParser;->mParseError:I

    #@294
    .line 736
    const/16 v18, 0x0

    #@296
    goto/16 :goto_96

    #@298
    .line 739
    .end local v5           #e:Ljava/lang/RuntimeException;
    .restart local v2       #N:I
    .restart local v4       #certs:[Ljava/security/cert/Certificate;
    .restart local v8       #i:I
    .restart local v11       #jarFile:Ljava/util/jar/JarFile;
    :cond_298
    const/16 v18, 0x1

    #@29a
    goto/16 :goto_96
.end method

.method public getParseError()I
    .registers 2

    #@0
    .prologue
    .line 472
    iget v0, p0, Landroid/content/pm/PackageParser;->mParseError:I

    #@2
    return v0
.end method

.method public parsePackage(Ljava/io/File;Ljava/lang/String;Landroid/util/DisplayMetrics;I)Landroid/content/pm/PackageParser$Package;
    .registers 39
    .parameter "sourceFile"
    .parameter "destCodePath"
    .parameter "metrics"
    .parameter "flags"

    #@0
    .prologue
    .line 477
    const/4 v6, 0x1

    #@1
    move-object/from16 v0, p0

    #@3
    iput v6, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@5
    .line 479
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@8
    move-result-object v6

    #@9
    move-object/from16 v0, p0

    #@b
    iput-object v6, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@d
    .line 480
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isFile()Z

    #@10
    move-result v6

    #@11
    if-nez v6, :cond_38

    #@13
    .line 481
    const-string v6, "PackageParser"

    #@15
    new-instance v7, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v8, "Skipping dir: "

    #@1c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v7

    #@20
    move-object/from16 v0, p0

    #@22
    iget-object v8, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@24
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v7

    #@2c
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 482
    const/16 v6, -0x64

    #@31
    move-object/from16 v0, p0

    #@33
    iput v6, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@35
    .line 483
    const/16 v31, 0x0

    #@37
    .line 576
    :goto_37
    return-object v31

    #@38
    .line 485
    :cond_38
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    #@3b
    move-result-object v6

    #@3c
    invoke-static {v6}, Landroid/content/pm/PackageParser;->isPackageFilename(Ljava/lang/String;)Z

    #@3f
    move-result v6

    #@40
    if-nez v6, :cond_6f

    #@42
    and-int/lit8 v6, p4, 0x4

    #@44
    if-eqz v6, :cond_6f

    #@46
    .line 487
    and-int/lit8 v6, p4, 0x1

    #@48
    if-nez v6, :cond_66

    #@4a
    .line 490
    const-string v6, "PackageParser"

    #@4c
    new-instance v7, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v8, "Skipping non-package file: "

    #@53
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v7

    #@57
    move-object/from16 v0, p0

    #@59
    iget-object v8, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@5b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v7

    #@63
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 492
    :cond_66
    const/16 v6, -0x64

    #@68
    move-object/from16 v0, p0

    #@6a
    iput v6, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@6c
    .line 493
    const/16 v31, 0x0

    #@6e
    goto :goto_37

    #@6f
    .line 499
    :cond_6f
    const/16 v30, 0x0

    #@71
    .line 500
    .local v30, parser:Landroid/content/res/XmlResourceParser;
    const/16 v24, 0x0

    #@73
    .line 501
    .local v24, assmgr:Landroid/content/res/AssetManager;
    const/16 v32, 0x0

    #@75
    .line 502
    .local v32, res:Landroid/content/res/Resources;
    const/16 v23, 0x1

    #@77
    .line 504
    .local v23, assetError:Z
    :try_start_77
    new-instance v5, Landroid/content/res/AssetManager;

    #@79
    invoke-direct {v5}, Landroid/content/res/AssetManager;-><init>()V
    :try_end_7c
    .catch Ljava/lang/Exception; {:try_start_77 .. :try_end_7c} :catch_1f2

    #@7c
    .line 505
    .end local v24           #assmgr:Landroid/content/res/AssetManager;
    .local v5, assmgr:Landroid/content/res/AssetManager;
    :try_start_7c
    move-object/from16 v0, p0

    #@7e
    iget-object v6, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@80
    invoke-virtual {v5, v6}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    #@83
    move-result v25

    #@84
    .line 506
    .local v25, cookie:I
    if-eqz v25, :cond_c8

    #@86
    .line 507
    new-instance v33, Landroid/content/res/Resources;

    #@88
    const/4 v6, 0x0

    #@89
    move-object/from16 v0, v33

    #@8b
    move-object/from16 v1, p3

    #@8d
    invoke-direct {v0, v5, v1, v6}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V
    :try_end_90
    .catch Ljava/lang/Exception; {:try_start_7c .. :try_end_90} :catch_e5

    #@90
    .line 508
    .end local v32           #res:Landroid/content/res/Resources;
    .local v33, res:Landroid/content/res/Resources;
    const/4 v6, 0x0

    #@91
    const/4 v7, 0x0

    #@92
    const/4 v8, 0x0

    #@93
    const/4 v9, 0x0

    #@94
    const/4 v10, 0x0

    #@95
    const/4 v11, 0x0

    #@96
    const/4 v12, 0x0

    #@97
    const/4 v13, 0x0

    #@98
    const/4 v14, 0x0

    #@99
    const/4 v15, 0x0

    #@9a
    const/16 v16, 0x0

    #@9c
    const/16 v17, 0x0

    #@9e
    const/16 v18, 0x0

    #@a0
    const/16 v19, 0x0

    #@a2
    const/16 v20, 0x0

    #@a4
    const/16 v21, 0x0

    #@a6
    :try_start_a6
    sget v22, Landroid/os/Build$VERSION;->RESOURCES_SDK_INT:I

    #@a8
    invoke-virtual/range {v5 .. v22}, Landroid/content/res/AssetManager;->setConfiguration(IILjava/lang/String;IIIIIIIIIIIIII)V

    #@ab
    .line 510
    const-string v6, "AndroidManifest.xml"

    #@ad
    move/from16 v0, v25

    #@af
    invoke-virtual {v5, v0, v6}, Landroid/content/res/AssetManager;->openXmlResourceParser(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;
    :try_end_b2
    .catch Ljava/lang/Exception; {:try_start_a6 .. :try_end_b2} :catch_1f7

    #@b2
    move-result-object v30

    #@b3
    .line 511
    const/16 v23, 0x0

    #@b5
    move-object/from16 v32, v33

    #@b7
    .line 519
    .end local v25           #cookie:I
    .end local v33           #res:Landroid/content/res/Resources;
    .restart local v32       #res:Landroid/content/res/Resources;
    :goto_b7
    if-eqz v23, :cond_105

    #@b9
    .line 520
    if-eqz v5, :cond_be

    #@bb
    invoke-virtual {v5}, Landroid/content/res/AssetManager;->close()V

    #@be
    .line 521
    :cond_be
    const/16 v6, -0x65

    #@c0
    move-object/from16 v0, p0

    #@c2
    iput v6, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@c4
    .line 522
    const/16 v31, 0x0

    #@c6
    goto/16 :goto_37

    #@c8
    .line 513
    .restart local v25       #cookie:I
    :cond_c8
    :try_start_c8
    const-string v6, "PackageParser"

    #@ca
    new-instance v7, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    const-string v8, "Failed adding asset path:"

    #@d1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v7

    #@d5
    move-object/from16 v0, p0

    #@d7
    iget-object v8, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@d9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v7

    #@dd
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v7

    #@e1
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e4
    .catch Ljava/lang/Exception; {:try_start_c8 .. :try_end_e4} :catch_e5

    #@e4
    goto :goto_b7

    #@e5
    .line 515
    .end local v25           #cookie:I
    :catch_e5
    move-exception v26

    #@e6
    .line 516
    .local v26, e:Ljava/lang/Exception;
    :goto_e6
    const-string v6, "PackageParser"

    #@e8
    new-instance v7, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v8, "Unable to read AndroidManifest.xml of "

    #@ef
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v7

    #@f3
    move-object/from16 v0, p0

    #@f5
    iget-object v8, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@f7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v7

    #@fb
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fe
    move-result-object v7

    #@ff
    move-object/from16 v0, v26

    #@101
    invoke-static {v6, v7, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@104
    goto :goto_b7

    #@105
    .line 524
    .end local v26           #e:Ljava/lang/Exception;
    :cond_105
    const/4 v6, 0x1

    #@106
    new-array v0, v6, [Ljava/lang/String;

    #@108
    move-object/from16 v28, v0

    #@10a
    .line 525
    .local v28, errorText:[Ljava/lang/String;
    const/16 v31, 0x0

    #@10c
    .line 526
    .local v31, pkg:Landroid/content/pm/PackageParser$Package;
    const/16 v27, 0x0

    #@10e
    .line 529
    .local v27, errorException:Ljava/lang/Exception;
    :try_start_10e
    move-object/from16 v0, p0

    #@110
    move-object/from16 v1, v32

    #@112
    move-object/from16 v2, v30

    #@114
    move/from16 v3, p4

    #@116
    move-object/from16 v4, v28

    #@118
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/pm/PackageParser;->parsePackage(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I[Ljava/lang/String;)Landroid/content/pm/PackageParser$Package;
    :try_end_11b
    .catch Ljava/lang/Exception; {:try_start_10e .. :try_end_11b} :catch_14f

    #@11b
    move-result-object v31

    #@11c
    .line 536
    :goto_11c
    if-nez v31, :cond_18b

    #@11e
    .line 539
    move-object/from16 v0, p0

    #@120
    iget-boolean v6, v0, Landroid/content/pm/PackageParser;->mOnlyCoreApps:Z

    #@122
    if-eqz v6, :cond_12b

    #@124
    move-object/from16 v0, p0

    #@126
    iget v6, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@128
    const/4 v7, 0x1

    #@129
    if-eq v6, v7, :cond_145

    #@12b
    .line 540
    :cond_12b
    if-eqz v27, :cond_159

    #@12d
    .line 541
    const-string v6, "PackageParser"

    #@12f
    move-object/from16 v0, p0

    #@131
    iget-object v7, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@133
    move-object/from16 v0, v27

    #@135
    invoke-static {v6, v7, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@138
    .line 547
    :goto_138
    move-object/from16 v0, p0

    #@13a
    iget v6, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@13c
    const/4 v7, 0x1

    #@13d
    if-ne v6, v7, :cond_145

    #@13f
    .line 548
    const/16 v6, -0x6c

    #@141
    move-object/from16 v0, p0

    #@143
    iput v6, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@145
    .line 551
    :cond_145
    invoke-interface/range {v30 .. v30}, Landroid/content/res/XmlResourceParser;->close()V

    #@148
    .line 552
    invoke-virtual {v5}, Landroid/content/res/AssetManager;->close()V

    #@14b
    .line 553
    const/16 v31, 0x0

    #@14d
    goto/16 :goto_37

    #@14f
    .line 530
    :catch_14f
    move-exception v26

    #@150
    .line 531
    .restart local v26       #e:Ljava/lang/Exception;
    move-object/from16 v27, v26

    #@152
    .line 532
    const/16 v6, -0x66

    #@154
    move-object/from16 v0, p0

    #@156
    iput v6, v0, Landroid/content/pm/PackageParser;->mParseError:I

    #@158
    goto :goto_11c

    #@159
    .line 543
    .end local v26           #e:Ljava/lang/Exception;
    :cond_159
    const-string v6, "PackageParser"

    #@15b
    new-instance v7, Ljava/lang/StringBuilder;

    #@15d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@160
    move-object/from16 v0, p0

    #@162
    iget-object v8, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@164
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v7

    #@168
    const-string v8, " (at "

    #@16a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v7

    #@16e
    invoke-interface/range {v30 .. v30}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    #@171
    move-result-object v8

    #@172
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@175
    move-result-object v7

    #@176
    const-string v8, "): "

    #@178
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v7

    #@17c
    const/4 v8, 0x0

    #@17d
    aget-object v8, v28, v8

    #@17f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v7

    #@183
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@186
    move-result-object v7

    #@187
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18a
    goto :goto_138

    #@18b
    .line 557
    :cond_18b
    move-object/from16 v0, p0

    #@18d
    iget-object v6, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@18f
    move-object/from16 v0, v31

    #@191
    iget-object v7, v0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@193
    move-object/from16 v0, p0

    #@195
    invoke-direct {v0, v6, v7}, Landroid/content/pm/PackageParser;->findOverlayPackage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@198
    move-result-object v29

    #@199
    .line 558
    .local v29, overlayPath:Ljava/lang/String;
    if-eqz v29, :cond_1d7

    #@19b
    new-instance v6, Ljava/io/File;

    #@19d
    move-object/from16 v0, v29

    #@19f
    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1a2
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    #@1a5
    move-result v6

    #@1a6
    if-eqz v6, :cond_1d7

    #@1a8
    .line 559
    move-object/from16 v0, p0

    #@1aa
    iget-object v6, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@1ac
    move-object/from16 v0, v29

    #@1ae
    invoke-virtual {v5, v6, v0, v7}, Landroid/content/res/AssetManager;->addOverlayPath(Ljava/lang/String;Ljava/lang/String;I)I

    #@1b1
    move-result v6

    #@1b2
    const/4 v7, 0x1

    #@1b3
    if-ne v6, v7, :cond_1d7

    #@1b5
    .line 560
    const-string v6, "PackageParser"

    #@1b7
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1bc
    const-string v8, "Added overlay pkg for "

    #@1be
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v7

    #@1c2
    move-object/from16 v0, p0

    #@1c4
    iget-object v8, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@1c6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v7

    #@1ca
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cd
    move-result-object v7

    #@1ce
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d1
    .line 561
    move-object/from16 v0, v29

    #@1d3
    move-object/from16 v1, v31

    #@1d5
    iput-object v0, v1, Landroid/content/pm/PackageParser$Package;->mOverlayPath:Ljava/lang/String;

    #@1d7
    .line 566
    :cond_1d7
    invoke-interface/range {v30 .. v30}, Landroid/content/res/XmlResourceParser;->close()V

    #@1da
    .line 567
    invoke-virtual {v5}, Landroid/content/res/AssetManager;->close()V

    #@1dd
    .line 570
    move-object/from16 v0, p2

    #@1df
    move-object/from16 v1, v31

    #@1e1
    iput-object v0, v1, Landroid/content/pm/PackageParser$Package;->mPath:Ljava/lang/String;

    #@1e3
    .line 571
    move-object/from16 v0, p0

    #@1e5
    iget-object v6, v0, Landroid/content/pm/PackageParser;->mArchiveSourcePath:Ljava/lang/String;

    #@1e7
    move-object/from16 v0, v31

    #@1e9
    iput-object v6, v0, Landroid/content/pm/PackageParser$Package;->mScanPath:Ljava/lang/String;

    #@1eb
    .line 574
    const/4 v6, 0x0

    #@1ec
    move-object/from16 v0, v31

    #@1ee
    iput-object v6, v0, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    #@1f0
    goto/16 :goto_37

    #@1f2
    .line 515
    .end local v5           #assmgr:Landroid/content/res/AssetManager;
    .end local v27           #errorException:Ljava/lang/Exception;
    .end local v28           #errorText:[Ljava/lang/String;
    .end local v29           #overlayPath:Ljava/lang/String;
    .end local v31           #pkg:Landroid/content/pm/PackageParser$Package;
    .restart local v24       #assmgr:Landroid/content/res/AssetManager;
    :catch_1f2
    move-exception v26

    #@1f3
    move-object/from16 v5, v24

    #@1f5
    .end local v24           #assmgr:Landroid/content/res/AssetManager;
    .restart local v5       #assmgr:Landroid/content/res/AssetManager;
    goto/16 :goto_e6

    #@1f7
    .end local v32           #res:Landroid/content/res/Resources;
    .restart local v25       #cookie:I
    .restart local v33       #res:Landroid/content/res/Resources;
    :catch_1f7
    move-exception v26

    #@1f8
    move-object/from16 v32, v33

    #@1fa
    .end local v33           #res:Landroid/content/res/Resources;
    .restart local v32       #res:Landroid/content/res/Resources;
    goto/16 :goto_e6
.end method

.method public setOnlyCoreApps(Z)V
    .registers 2
    .parameter "onlyCoreApps"

    #@0
    .prologue
    .line 243
    iput-boolean p1, p0, Landroid/content/pm/PackageParser;->mOnlyCoreApps:Z

    #@2
    .line 244
    return-void
.end method

.method public setSeparateProcesses([Ljava/lang/String;)V
    .registers 2
    .parameter "procs"

    #@0
    .prologue
    .line 239
    iput-object p1, p0, Landroid/content/pm/PackageParser;->mSeparateProcesses:[Ljava/lang/String;

    #@2
    .line 240
    return-void
.end method