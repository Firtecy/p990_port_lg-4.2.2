.class public Landroid/content/pm/ManifestDigest;
.super Ljava/lang/Object;
.source "ManifestDigest.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/ManifestDigest;",
            ">;"
        }
    .end annotation
.end field

.field private static final DIGEST_TYPES:[Ljava/lang/String; = null

.field private static final TO_STRING_PREFIX:Ljava/lang/String; = "ManifestDigest {mDigest="


# instance fields
.field private final mDigest:[B


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 37
    const/4 v0, 0x3

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "SHA1-Digest"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "SHA-Digest"

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x2

    #@e
    const-string v2, "MD5-Digest"

    #@10
    aput-object v2, v0, v1

    #@12
    sput-object v0, Landroid/content/pm/ManifestDigest;->DIGEST_TYPES:[Ljava/lang/String;

    #@14
    .line 119
    new-instance v0, Landroid/content/pm/ManifestDigest$1;

    #@16
    invoke-direct {v0}, Landroid/content/pm/ManifestDigest$1;-><init>()V

    #@19
    sput-object v0, Landroid/content/pm/ManifestDigest;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1b
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/content/pm/ManifestDigest;->mDigest:[B

    #@9
    .line 50
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/ManifestDigest$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/content/pm/ManifestDigest;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method constructor <init>([B)V
    .registers 2
    .parameter "digest"

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    iput-object p1, p0, Landroid/content/pm/ManifestDigest;->mDigest:[B

    #@5
    .line 46
    return-void
.end method

.method static fromAttributes(Ljava/util/jar/Attributes;)Landroid/content/pm/ManifestDigest;
    .registers 7
    .parameter "attributes"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 53
    if-nez p0, :cond_4

    #@3
    .line 72
    :cond_3
    :goto_3
    return-object v4

    #@4
    .line 57
    :cond_4
    const/4 v1, 0x0

    #@5
    .line 59
    .local v1, encodedDigest:Ljava/lang/String;
    const/4 v2, 0x0

    #@6
    .local v2, i:I
    :goto_6
    sget-object v5, Landroid/content/pm/ManifestDigest;->DIGEST_TYPES:[Ljava/lang/String;

    #@8
    array-length v5, v5

    #@9
    if-ge v2, v5, :cond_16

    #@b
    .line 60
    sget-object v5, Landroid/content/pm/ManifestDigest;->DIGEST_TYPES:[Ljava/lang/String;

    #@d
    aget-object v5, v5, v2

    #@f
    invoke-virtual {p0, v5}, Ljava/util/jar/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    .line 61
    .local v3, value:Ljava/lang/String;
    if-eqz v3, :cond_23

    #@15
    .line 62
    move-object v1, v3

    #@16
    .line 67
    .end local v3           #value:Ljava/lang/String;
    :cond_16
    if-eqz v1, :cond_3

    #@18
    .line 71
    const/4 v4, 0x0

    #@19
    invoke-static {v1, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    #@1c
    move-result-object v0

    #@1d
    .line 72
    .local v0, digest:[B
    new-instance v4, Landroid/content/pm/ManifestDigest;

    #@1f
    invoke-direct {v4, v0}, Landroid/content/pm/ManifestDigest;-><init>([B)V

    #@22
    goto :goto_3

    #@23
    .line 59
    .end local v0           #digest:[B
    .restart local v3       #value:Ljava/lang/String;
    :cond_23
    add-int/lit8 v2, v2, 0x1

    #@25
    goto :goto_6
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 77
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 82
    instance-of v2, p1, Landroid/content/pm/ManifestDigest;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 88
    :cond_5
    :goto_5
    return v1

    #@6
    :cond_6
    move-object v0, p1

    #@7
    .line 86
    check-cast v0, Landroid/content/pm/ManifestDigest;

    #@9
    .line 88
    .local v0, other:Landroid/content/pm/ManifestDigest;
    if-eq p0, v0, :cond_15

    #@b
    iget-object v2, p0, Landroid/content/pm/ManifestDigest;->mDigest:[B

    #@d
    iget-object v3, v0, Landroid/content/pm/ManifestDigest;->mDigest:[B

    #@f
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_5

    #@15
    :cond_15
    const/4 v1, 0x1

    #@16
    goto :goto_5
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/content/pm/ManifestDigest;->mDigest:[B

    #@2
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 98
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    const-string v4, "ManifestDigest {mDigest="

    #@4
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@7
    move-result v4

    #@8
    iget-object v5, p0, Landroid/content/pm/ManifestDigest;->mDigest:[B

    #@a
    array-length v5, v5

    #@b
    mul-int/lit8 v5, v5, 0x3

    #@d
    add-int/2addr v4, v5

    #@e
    add-int/lit8 v4, v4, 0x1

    #@10
    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    #@13
    .line 101
    .local v3, sb:Ljava/lang/StringBuilder;
    const-string v4, "ManifestDigest {mDigest="

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 103
    iget-object v4, p0, Landroid/content/pm/ManifestDigest;->mDigest:[B

    #@1a
    array-length v0, v4

    #@1b
    .line 104
    .local v0, N:I
    const/4 v2, 0x0

    #@1c
    .local v2, i:I
    :goto_1c
    if-ge v2, v0, :cond_2e

    #@1e
    .line 105
    iget-object v4, p0, Landroid/content/pm/ManifestDigest;->mDigest:[B

    #@20
    aget-byte v1, v4, v2

    #@22
    .line 106
    .local v1, b:B
    const/4 v4, 0x0

    #@23
    invoke-static {v3, v1, v4}, Ljava/lang/IntegralToString;->appendByteAsHex(Ljava/lang/StringBuilder;BZ)Ljava/lang/StringBuilder;

    #@26
    .line 107
    const/16 v4, 0x2c

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2b
    .line 104
    add-int/lit8 v2, v2, 0x1

    #@2d
    goto :goto_1c

    #@2e
    .line 109
    .end local v1           #b:B
    :cond_2e
    const/16 v4, 0x7d

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@33
    .line 111
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    return-object v4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Landroid/content/pm/ManifestDigest;->mDigest:[B

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@5
    .line 117
    return-void
.end method
