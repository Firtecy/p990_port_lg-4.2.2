.class public Landroid/content/pm/UserInfo;
.super Ljava/lang/Object;
.source "UserInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final FLAG_ADMIN:I = 0x2

.field public static final FLAG_GUEST:I = 0x4

.field public static final FLAG_INITIALIZED:I = 0x10

.field public static final FLAG_MASK_USER_TYPE:I = 0x3f

.field public static final FLAG_PRIMARY:I = 0x1

.field public static final FLAG_RESTRICTED:I = 0x8


# instance fields
.field public creationTime:J

.field public flags:I

.field public iconPath:Ljava/lang/String;

.field public id:I

.field public lastLoggedInTime:J

.field public name:Ljava/lang/String;

.field public partial:Z

.field public serialNumber:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 138
    new-instance v0, Landroid/content/pm/UserInfo$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/UserInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/UserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 100
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 101
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;I)V
    .registers 5
    .parameter "id"
    .parameter "name"
    .parameter "flags"

    #@0
    .prologue
    .line 78
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0, p3}, Landroid/content/pm/UserInfo;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    #@4
    .line 79
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;I)V
    .registers 5
    .parameter "id"
    .parameter "name"
    .parameter "iconPath"
    .parameter "flags"

    #@0
    .prologue
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 82
    iput p1, p0, Landroid/content/pm/UserInfo;->id:I

    #@5
    .line 83
    iput-object p2, p0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@7
    .line 84
    iput p4, p0, Landroid/content/pm/UserInfo;->flags:I

    #@9
    .line 85
    iput-object p3, p0, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@b
    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/UserInfo;)V
    .registers 4
    .parameter "orig"

    #@0
    .prologue
    .line 103
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 104
    iget-object v0, p1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@5
    iput-object v0, p0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@7
    .line 105
    iget-object v0, p1, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@9
    iput-object v0, p0, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@b
    .line 106
    iget v0, p1, Landroid/content/pm/UserInfo;->id:I

    #@d
    iput v0, p0, Landroid/content/pm/UserInfo;->id:I

    #@f
    .line 107
    iget v0, p1, Landroid/content/pm/UserInfo;->flags:I

    #@11
    iput v0, p0, Landroid/content/pm/UserInfo;->flags:I

    #@13
    .line 108
    iget v0, p1, Landroid/content/pm/UserInfo;->serialNumber:I

    #@15
    iput v0, p0, Landroid/content/pm/UserInfo;->serialNumber:I

    #@17
    .line 109
    iget-wide v0, p1, Landroid/content/pm/UserInfo;->creationTime:J

    #@19
    iput-wide v0, p0, Landroid/content/pm/UserInfo;->creationTime:J

    #@1b
    .line 110
    iget-wide v0, p1, Landroid/content/pm/UserInfo;->lastLoggedInTime:J

    #@1d
    iput-wide v0, p0, Landroid/content/pm/UserInfo;->lastLoggedInTime:J

    #@1f
    .line 111
    iget-boolean v0, p1, Landroid/content/pm/UserInfo;->partial:Z

    #@21
    iput-boolean v0, p0, Landroid/content/pm/UserInfo;->partial:Z

    #@23
    .line 112
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 148
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/content/pm/UserInfo;->id:I

    #@9
    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@f
    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@15
    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/content/pm/UserInfo;->flags:I

    #@1b
    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/content/pm/UserInfo;->serialNumber:I

    #@21
    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@24
    move-result-wide v0

    #@25
    iput-wide v0, p0, Landroid/content/pm/UserInfo;->creationTime:J

    #@27
    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@2a
    move-result-wide v0

    #@2b
    iput-wide v0, p0, Landroid/content/pm/UserInfo;->lastLoggedInTime:J

    #@2d
    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_37

    #@33
    const/4 v0, 0x1

    #@34
    :goto_34
    iput-boolean v0, p0, Landroid/content/pm/UserInfo;->partial:Z

    #@36
    .line 157
    return-void

    #@37
    .line 156
    :cond_37
    const/4 v0, 0x0

    #@38
    goto :goto_34
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/UserInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/content/pm/UserInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 124
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getUserHandle()Landroid/os/UserHandle;
    .registers 3

    #@0
    .prologue
    .line 115
    new-instance v0, Landroid/os/UserHandle;

    #@2
    iget v1, p0, Landroid/content/pm/UserInfo;->id:I

    #@4
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@7
    return-object v0
.end method

.method public isAdmin()Z
    .registers 3

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/content/pm/UserInfo;->flags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    const/4 v1, 0x2

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isGuest()Z
    .registers 3

    #@0
    .prologue
    .line 97
    iget v0, p0, Landroid/content/pm/UserInfo;->flags:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    const/4 v1, 0x4

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isPrimary()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 89
    iget v1, p0, Landroid/content/pm/UserInfo;->flags:I

    #@3
    and-int/lit8 v1, v1, 0x1

    #@5
    if-ne v1, v0, :cond_8

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "UserInfo{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/content/pm/UserInfo;->id:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ":"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ":"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/content/pm/UserInfo;->flags:I

    #@25
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    const-string/jumbo v1, "}"

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 128
    iget v0, p0, Landroid/content/pm/UserInfo;->id:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 129
    iget-object v0, p0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 130
    iget-object v0, p0, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 131
    iget v0, p0, Landroid/content/pm/UserInfo;->flags:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 132
    iget v0, p0, Landroid/content/pm/UserInfo;->serialNumber:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 133
    iget-wide v0, p0, Landroid/content/pm/UserInfo;->creationTime:J

    #@1b
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@1e
    .line 134
    iget-wide v0, p0, Landroid/content/pm/UserInfo;->lastLoggedInTime:J

    #@20
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@23
    .line 135
    iget-boolean v0, p0, Landroid/content/pm/UserInfo;->partial:Z

    #@25
    if-eqz v0, :cond_2c

    #@27
    const/4 v0, 0x1

    #@28
    :goto_28
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 136
    return-void

    #@2c
    .line 135
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_28
.end method
