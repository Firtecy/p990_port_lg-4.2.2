.class public Landroid/content/pm/ResolveInfo;
.super Ljava/lang/Object;
.source "ResolveInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/pm/ResolveInfo$DisplayNameComparator;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public activityInfo:Landroid/content/pm/ActivityInfo;

.field public filter:Landroid/content/IntentFilter;

.field public icon:I

.field public isDefault:Z

.field public labelRes:I

.field public match:I

.field public nonLocalizedLabel:Ljava/lang/CharSequence;

.field public preferredOrder:I

.field public priority:I

.field public resolvePackageName:Ljava/lang/String;

.field public serviceInfo:Landroid/content/pm/ServiceInfo;

.field public specificIndex:I

.field public system:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 287
    new-instance v0, Landroid/content/pm/ResolveInfo$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/ResolveInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/ResolveInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 230
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 83
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@6
    .line 231
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/ResolveInfo;)V
    .registers 3
    .parameter "orig"

    #@0
    .prologue
    .line 233
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 83
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@6
    .line 234
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@8
    iput-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@a
    .line 235
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@c
    iput-object v0, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@e
    .line 236
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    #@10
    iput-object v0, p0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    #@12
    .line 237
    iget v0, p1, Landroid/content/pm/ResolveInfo;->priority:I

    #@14
    iput v0, p0, Landroid/content/pm/ResolveInfo;->priority:I

    #@16
    .line 238
    iget v0, p1, Landroid/content/pm/ResolveInfo;->preferredOrder:I

    #@18
    iput v0, p0, Landroid/content/pm/ResolveInfo;->preferredOrder:I

    #@1a
    .line 239
    iget v0, p1, Landroid/content/pm/ResolveInfo;->match:I

    #@1c
    iput v0, p0, Landroid/content/pm/ResolveInfo;->match:I

    #@1e
    .line 240
    iget v0, p1, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@20
    iput v0, p0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@22
    .line 241
    iget v0, p1, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@24
    iput v0, p0, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@26
    .line 242
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@28
    iput-object v0, p0, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@2a
    .line 243
    iget v0, p1, Landroid/content/pm/ResolveInfo;->icon:I

    #@2c
    iput v0, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@2e
    .line 244
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@30
    iput-object v0, p0, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@32
    .line 245
    iget-boolean v0, p1, Landroid/content/pm/ResolveInfo;->system:Z

    #@34
    iput-boolean v0, p0, Landroid/content/pm/ResolveInfo;->system:Z

    #@36
    .line 246
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 297
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 83
    const/4 v0, -0x1

    #@5
    iput v0, p0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@7
    .line 298
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v0

    #@b
    packed-switch v0, :pswitch_data_7c

    #@e
    .line 308
    iput-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@10
    .line 309
    iput-object v1, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@12
    .line 312
    :goto_12
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_22

    #@18
    .line 313
    sget-object v0, Landroid/content/IntentFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Landroid/content/IntentFilter;

    #@20
    iput-object v0, p0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    #@22
    .line 315
    :cond_22
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v0

    #@26
    iput v0, p0, Landroid/content/pm/ResolveInfo;->priority:I

    #@28
    .line 316
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v0

    #@2c
    iput v0, p0, Landroid/content/pm/ResolveInfo;->preferredOrder:I

    #@2e
    .line 317
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@31
    move-result v0

    #@32
    iput v0, p0, Landroid/content/pm/ResolveInfo;->match:I

    #@34
    .line 318
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v0

    #@38
    iput v0, p0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@3a
    .line 319
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v0

    #@3e
    iput v0, p0, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@40
    .line 320
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@42
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@45
    move-result-object v0

    #@46
    check-cast v0, Ljava/lang/CharSequence;

    #@48
    iput-object v0, p0, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@4a
    .line 322
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v0

    #@4e
    iput v0, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@50
    .line 323
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@53
    move-result-object v0

    #@54
    iput-object v0, p0, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@56
    .line 324
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@59
    move-result v0

    #@5a
    if-eqz v0, :cond_7a

    #@5c
    const/4 v0, 0x1

    #@5d
    :goto_5d
    iput-boolean v0, p0, Landroid/content/pm/ResolveInfo;->system:Z

    #@5f
    .line 325
    return-void

    #@60
    .line 300
    :pswitch_60
    sget-object v0, Landroid/content/pm/ActivityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@62
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@65
    move-result-object v0

    #@66
    check-cast v0, Landroid/content/pm/ActivityInfo;

    #@68
    iput-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@6a
    .line 301
    iput-object v1, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@6c
    goto :goto_12

    #@6d
    .line 304
    :pswitch_6d
    sget-object v0, Landroid/content/pm/ServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6f
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@72
    move-result-object v0

    #@73
    check-cast v0, Landroid/content/pm/ServiceInfo;

    #@75
    iput-object v0, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@77
    .line 305
    iput-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@79
    goto :goto_12

    #@7a
    .line 324
    :cond_7a
    const/4 v0, 0x0

    #@7b
    goto :goto_5d

    #@7c
    .line 298
    :pswitch_data_7c
    .packed-switch 0x1
        :pswitch_60
        :pswitch_6d
    .end packed-switch
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/ResolveInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/content/pm/ResolveInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 257
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    #@2
    if-eqz v0, :cond_32

    #@4
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, "Filter:"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1a
    .line 206
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    #@1c
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, "  "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, p1, v1}, Landroid/content/IntentFilter;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@32
    .line 208
    :cond_32
    new-instance v0, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string/jumbo v1, "priority="

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    iget v1, p0, Landroid/content/pm/ResolveInfo;->priority:I

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v0

    #@48
    const-string v1, " preferredOrder="

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v0

    #@4e
    iget v1, p0, Landroid/content/pm/ResolveInfo;->preferredOrder:I

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    const-string v1, " match=0x"

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v0

    #@5a
    iget v1, p0, Landroid/content/pm/ResolveInfo;->match:I

    #@5c
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    const-string v1, " specificIndex="

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v0

    #@6a
    iget v1, p0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@6c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v0

    #@70
    const-string v1, " isDefault="

    #@72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v0

    #@76
    iget-boolean v1, p0, Landroid/content/pm/ResolveInfo;->isDefault:Z

    #@78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v0

    #@7c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v0

    #@80
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@83
    .line 213
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@85
    if-eqz v0, :cond_a4

    #@87
    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v0

    #@90
    const-string/jumbo v1, "resolvePackageName="

    #@93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v0

    #@97
    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v0

    #@9d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v0

    #@a1
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@a4
    .line 216
    :cond_a4
    iget v0, p0, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@a6
    if-nez v0, :cond_b0

    #@a8
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@aa
    if-nez v0, :cond_b0

    #@ac
    iget v0, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@ae
    if-eqz v0, :cond_ed

    #@b0
    .line 217
    :cond_b0
    new-instance v0, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v0

    #@b9
    const-string/jumbo v1, "labelRes=0x"

    #@bc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v0

    #@c0
    iget v1, p0, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@c2
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@c5
    move-result-object v1

    #@c6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v0

    #@ca
    const-string v1, " nonLocalizedLabel="

    #@cc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v0

    #@d0
    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@d2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v0

    #@d6
    const-string v1, " icon=0x"

    #@d8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v0

    #@dc
    iget v1, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@de
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@e1
    move-result-object v1

    #@e2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v0

    #@e6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e9
    move-result-object v0

    #@ea
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@ed
    .line 221
    :cond_ed
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@ef
    if-eqz v0, :cond_120

    #@f1
    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v0

    #@fa
    const-string v1, "ActivityInfo:"

    #@fc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v0

    #@100
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v0

    #@104
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@107
    .line 223
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@109
    new-instance v1, Ljava/lang/StringBuilder;

    #@10b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10e
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v1

    #@112
    const-string v2, "  "

    #@114
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v1

    #@118
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v1

    #@11c
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/ActivityInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@11f
    .line 228
    :cond_11f
    :goto_11f
    return-void

    #@120
    .line 224
    :cond_120
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@122
    if-eqz v0, :cond_11f

    #@124
    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    #@126
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@129
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v0

    #@12d
    const-string v1, "ServiceInfo:"

    #@12f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v0

    #@133
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@136
    move-result-object v0

    #@137
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@13a
    .line 226
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@13c
    new-instance v1, Ljava/lang/StringBuilder;

    #@13e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@141
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v1

    #@145
    const-string v2, "  "

    #@147
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v1

    #@14b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14e
    move-result-object v1

    #@14f
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/ServiceInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@152
    goto :goto_11f
.end method

.method public final getIconResource()I
    .registers 2

    #@0
    .prologue
    .line 197
    iget v0, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@2
    if-eqz v0, :cond_7

    #@4
    iget v0, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@6
    .line 200
    :goto_6
    return v0

    #@7
    .line 198
    :cond_7
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@9
    if-eqz v0, :cond_12

    #@b
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@d
    invoke-virtual {v0}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    #@10
    move-result v0

    #@11
    goto :goto_6

    #@12
    .line 199
    :cond_12
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@14
    if-eqz v0, :cond_1d

    #@16
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@18
    invoke-virtual {v0}, Landroid/content/pm/ServiceInfo;->getIconResource()I

    #@1b
    move-result v0

    #@1c
    goto :goto_6

    #@1d
    .line 200
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_6
.end method

.method public loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .registers 8
    .parameter "pm"

    #@0
    .prologue
    .line 172
    iget-object v3, p0, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@2
    if-eqz v3, :cond_14

    #@4
    iget v3, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@6
    if-eqz v3, :cond_14

    #@8
    .line 173
    iget-object v3, p0, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@a
    iget v4, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@c
    const/4 v5, 0x0

    #@d
    invoke-virtual {p1, v3, v4, v5}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    #@10
    move-result-object v2

    #@11
    .line 174
    .local v2, dr:Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_14

    #@13
    .line 186
    .end local v2           #dr:Landroid/graphics/drawable/Drawable;
    :cond_13
    :goto_13
    return-object v2

    #@14
    .line 178
    :cond_14
    iget-object v3, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@16
    if-eqz v3, :cond_2f

    #@18
    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@1a
    .line 179
    .local v1, ci:Landroid/content/pm/ComponentInfo;
    :goto_1a
    iget-object v0, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1c
    .line 180
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    iget v3, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@1e
    if-eqz v3, :cond_2a

    #@20
    .line 181
    iget-object v3, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@22
    iget v4, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@24
    invoke-virtual {p1, v3, v4, v0}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    #@27
    move-result-object v2

    #@28
    .line 182
    .restart local v2       #dr:Landroid/graphics/drawable/Drawable;
    if-nez v2, :cond_13

    #@2a
    .line 186
    .end local v2           #dr:Landroid/graphics/drawable/Drawable;
    :cond_2a
    invoke-virtual {v1, p1}, Landroid/content/pm/ComponentInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@2d
    move-result-object v2

    #@2e
    goto :goto_13

    #@2f
    .line 178
    .end local v0           #ai:Landroid/content/pm/ApplicationInfo;
    .end local v1           #ci:Landroid/content/pm/ComponentInfo;
    :cond_2f
    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@31
    goto :goto_1a
.end method

.method public loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 9
    .parameter "pm"

    #@0
    .prologue
    .line 134
    iget-object v4, p0, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@2
    if-eqz v4, :cond_7

    #@4
    .line 135
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@6
    .line 156
    :cond_6
    :goto_6
    return-object v2

    #@7
    .line 138
    :cond_7
    iget-object v4, p0, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@9
    if-eqz v4, :cond_23

    #@b
    iget v4, p0, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@d
    if-eqz v4, :cond_23

    #@f
    .line 139
    iget-object v4, p0, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@11
    iget v5, p0, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@13
    const/4 v6, 0x0

    #@14
    invoke-virtual {p1, v4, v5, v6}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@17
    move-result-object v3

    #@18
    .line 140
    .local v3, label:Ljava/lang/CharSequence;
    if-eqz v3, :cond_23

    #@1a
    .line 141
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    goto :goto_6

    #@23
    .line 144
    .end local v3           #label:Ljava/lang/CharSequence;
    :cond_23
    iget-object v4, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@25
    if-eqz v4, :cond_42

    #@27
    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@29
    .line 145
    .local v1, ci:Landroid/content/pm/ComponentInfo;
    :goto_29
    iget-object v0, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2b
    .line 146
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    iget v4, p0, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@2d
    if-eqz v4, :cond_45

    #@2f
    .line 147
    iget-object v4, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@31
    iget v5, p0, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@33
    invoke-virtual {p1, v4, v5, v0}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@36
    move-result-object v3

    #@37
    .line 148
    .restart local v3       #label:Ljava/lang/CharSequence;
    if-eqz v3, :cond_45

    #@39
    .line 149
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    goto :goto_6

    #@42
    .line 144
    .end local v0           #ai:Landroid/content/pm/ApplicationInfo;
    .end local v1           #ci:Landroid/content/pm/ComponentInfo;
    .end local v3           #label:Ljava/lang/CharSequence;
    :cond_42
    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@44
    goto :goto_29

    #@45
    .line 153
    .restart local v0       #ai:Landroid/content/pm/ApplicationInfo;
    .restart local v1       #ci:Landroid/content/pm/ComponentInfo;
    :cond_45
    invoke-virtual {v1, p1}, Landroid/content/pm/ComponentInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@48
    move-result-object v2

    #@49
    .line 155
    .local v2, data:Ljava/lang/CharSequence;
    if-eqz v2, :cond_6

    #@4b
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    goto :goto_6
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 249
    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@2
    if-eqz v1, :cond_5d

    #@4
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@6
    .line 250
    .local v0, ci:Landroid/content/pm/ComponentInfo;
    :goto_6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "ResolveInfo{"

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@14
    move-result v2

    #@15
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, " "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    iget-object v2, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, " p="

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    iget v2, p0, Landroid/content/pm/ResolveInfo;->priority:I

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    const-string v2, " o="

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    iget v2, p0, Landroid/content/pm/ResolveInfo;->preferredOrder:I

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    const-string v2, " m=0x"

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    iget v2, p0, Landroid/content/pm/ResolveInfo;->match:I

    #@49
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    const-string/jumbo v2, "}"

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v1

    #@5c
    return-object v1

    #@5d
    .line 249
    .end local v0           #ci:Landroid/content/pm/ComponentInfo;
    :cond_5d
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@5f
    goto :goto_6
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 261
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4
    if-eqz v2, :cond_4a

    #@6
    .line 262
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 263
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@b
    invoke-virtual {v2, p1, p2}, Landroid/content/pm/ActivityInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@e
    .line 270
    :goto_e
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    #@10
    if-eqz v2, :cond_5c

    #@12
    .line 271
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 272
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    #@17
    invoke-virtual {v2, p1, p2}, Landroid/content/IntentFilter;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 276
    :goto_1a
    iget v2, p0, Landroid/content/pm/ResolveInfo;->priority:I

    #@1c
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 277
    iget v2, p0, Landroid/content/pm/ResolveInfo;->preferredOrder:I

    #@21
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 278
    iget v2, p0, Landroid/content/pm/ResolveInfo;->match:I

    #@26
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 279
    iget v2, p0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@2b
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 280
    iget v2, p0, Landroid/content/pm/ResolveInfo;->labelRes:I

    #@30
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 281
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@35
    invoke-static {v2, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@38
    .line 282
    iget v2, p0, Landroid/content/pm/ResolveInfo;->icon:I

    #@3a
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    .line 283
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@3f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@42
    .line 284
    iget-boolean v2, p0, Landroid/content/pm/ResolveInfo;->system:Z

    #@44
    if-eqz v2, :cond_60

    #@46
    :goto_46
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@49
    .line 285
    return-void

    #@4a
    .line 264
    :cond_4a
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4c
    if-eqz v2, :cond_58

    #@4e
    .line 265
    const/4 v2, 0x2

    #@4f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@52
    .line 266
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@54
    invoke-virtual {v2, p1, p2}, Landroid/content/pm/ServiceInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@57
    goto :goto_e

    #@58
    .line 268
    :cond_58
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5b
    goto :goto_e

    #@5c
    .line 274
    :cond_5c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5f
    goto :goto_1a

    #@60
    :cond_60
    move v0, v1

    #@61
    .line 284
    goto :goto_46
.end method
