.class public final Landroid/content/pm/ProviderInfo;
.super Landroid/content/pm/ComponentInfo;
.source "ProviderInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/ProviderInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final FLAG_SINGLE_USER:I = 0x40000000


# instance fields
.field public authority:Ljava/lang/String;

.field public flags:I

.field public grantUriPermissions:Z

.field public initOrder:I

.field public isSyncable:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public multiprocess:Z

.field public pathPermissions:[Landroid/content/pm/PathPermission;

.field public readPermission:Ljava/lang/String;

.field public uriPermissionPatterns:[Landroid/os/PatternMatcher;

.field public writePermission:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 134
    new-instance v0, Landroid/content/pm/ProviderInfo$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/ProviderInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/ProviderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 99
    invoke-direct {p0}, Landroid/content/pm/ComponentInfo;-><init>()V

    #@5
    .line 33
    iput-object v1, p0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@7
    .line 37
    iput-object v1, p0, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    #@9
    .line 41
    iput-object v1, p0, Landroid/content/pm/ProviderInfo;->writePermission:Ljava/lang/String;

    #@b
    .line 48
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->grantUriPermissions:Z

    #@d
    .line 58
    iput-object v1, p0, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@f
    .line 66
    iput-object v1, p0, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@11
    .line 71
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@13
    .line 75
    iput v0, p0, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@15
    .line 89
    iput v0, p0, Landroid/content/pm/ProviderInfo;->flags:I

    #@17
    .line 96
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->isSyncable:Z

    #@19
    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/ProviderInfo;)V
    .registers 4
    .parameter "orig"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 103
    invoke-direct {p0, p1}, Landroid/content/pm/ComponentInfo;-><init>(Landroid/content/pm/ComponentInfo;)V

    #@5
    .line 33
    iput-object v1, p0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@7
    .line 37
    iput-object v1, p0, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    #@9
    .line 41
    iput-object v1, p0, Landroid/content/pm/ProviderInfo;->writePermission:Ljava/lang/String;

    #@b
    .line 48
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->grantUriPermissions:Z

    #@d
    .line 58
    iput-object v1, p0, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@f
    .line 66
    iput-object v1, p0, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@11
    .line 71
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@13
    .line 75
    iput v0, p0, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@15
    .line 89
    iput v0, p0, Landroid/content/pm/ProviderInfo;->flags:I

    #@17
    .line 96
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->isSyncable:Z

    #@19
    .line 104
    iget-object v0, p1, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@1b
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@1d
    .line 105
    iget-object v0, p1, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    #@1f
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    #@21
    .line 106
    iget-object v0, p1, Landroid/content/pm/ProviderInfo;->writePermission:Ljava/lang/String;

    #@23
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->writePermission:Ljava/lang/String;

    #@25
    .line 107
    iget-boolean v0, p1, Landroid/content/pm/ProviderInfo;->grantUriPermissions:Z

    #@27
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->grantUriPermissions:Z

    #@29
    .line 108
    iget-object v0, p1, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@2b
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@2d
    .line 109
    iget-object v0, p1, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@2f
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@31
    .line 110
    iget-boolean v0, p1, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@33
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@35
    .line 111
    iget v0, p1, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@37
    iput v0, p0, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@39
    .line 112
    iget v0, p1, Landroid/content/pm/ProviderInfo;->flags:I

    #@3b
    iput v0, p0, Landroid/content/pm/ProviderInfo;->flags:I

    #@3d
    .line 113
    iget-boolean v0, p1, Landroid/content/pm/ProviderInfo;->isSyncable:Z

    #@3f
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->isSyncable:Z

    #@41
    .line 114
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "in"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 150
    invoke-direct {p0, p1}, Landroid/content/pm/ComponentInfo;-><init>(Landroid/os/Parcel;)V

    #@6
    .line 33
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@8
    .line 37
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    #@a
    .line 41
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->writePermission:Ljava/lang/String;

    #@c
    .line 48
    iput-boolean v2, p0, Landroid/content/pm/ProviderInfo;->grantUriPermissions:Z

    #@e
    .line 58
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@10
    .line 66
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@12
    .line 71
    iput-boolean v2, p0, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@14
    .line 75
    iput v2, p0, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@16
    .line 89
    iput v2, p0, Landroid/content/pm/ProviderInfo;->flags:I

    #@18
    .line 96
    iput-boolean v2, p0, Landroid/content/pm/ProviderInfo;->isSyncable:Z

    #@1a
    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@20
    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    #@26
    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->writePermission:Ljava/lang/String;

    #@2c
    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_67

    #@32
    move v0, v1

    #@33
    :goto_33
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->grantUriPermissions:Z

    #@35
    .line 155
    sget-object v0, Landroid/os/PatternMatcher;->CREATOR:Landroid/os/Parcelable$Creator;

    #@37
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@3a
    move-result-object v0

    #@3b
    check-cast v0, [Landroid/os/PatternMatcher;

    #@3d
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@3f
    .line 156
    sget-object v0, Landroid/content/pm/PathPermission;->CREATOR:Landroid/os/Parcelable$Creator;

    #@41
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@44
    move-result-object v0

    #@45
    check-cast v0, [Landroid/content/pm/PathPermission;

    #@47
    iput-object v0, p0, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@49
    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4c
    move-result v0

    #@4d
    if-eqz v0, :cond_69

    #@4f
    move v0, v1

    #@50
    :goto_50
    iput-boolean v0, p0, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@52
    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v0

    #@56
    iput v0, p0, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@58
    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5b
    move-result v0

    #@5c
    iput v0, p0, Landroid/content/pm/ProviderInfo;->flags:I

    #@5e
    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@61
    move-result v0

    #@62
    if-eqz v0, :cond_6b

    #@64
    :goto_64
    iput-boolean v1, p0, Landroid/content/pm/ProviderInfo;->isSyncable:Z

    #@66
    .line 161
    return-void

    #@67
    :cond_67
    move v0, v2

    #@68
    .line 154
    goto :goto_33

    #@69
    :cond_69
    move v0, v2

    #@6a
    .line 157
    goto :goto_50

    #@6b
    :cond_6b
    move v1, v2

    #@6c
    .line 160
    goto :goto_64
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/ProviderInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/content/pm/ProviderInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 117
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ContentProviderInfo{name="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " className="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string/jumbo v1, "}"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "out"
    .parameter "parcelableFlags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 121
    invoke-super {p0, p1, p2}, Landroid/content/pm/ComponentInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@5
    .line 122
    iget-object v0, p0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 123
    iget-object v0, p0, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 124
    iget-object v0, p0, Landroid/content/pm/ProviderInfo;->writePermission:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 125
    iget-boolean v0, p0, Landroid/content/pm/ProviderInfo;->grantUriPermissions:Z

    #@16
    if-eqz v0, :cond_40

    #@18
    move v0, v1

    #@19
    :goto_19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 126
    iget-object v0, p0, Landroid/content/pm/ProviderInfo;->uriPermissionPatterns:[Landroid/os/PatternMatcher;

    #@1e
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@21
    .line 127
    iget-object v0, p0, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@23
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@26
    .line 128
    iget-boolean v0, p0, Landroid/content/pm/ProviderInfo;->multiprocess:Z

    #@28
    if-eqz v0, :cond_42

    #@2a
    move v0, v1

    #@2b
    :goto_2b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 129
    iget v0, p0, Landroid/content/pm/ProviderInfo;->initOrder:I

    #@30
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 130
    iget v0, p0, Landroid/content/pm/ProviderInfo;->flags:I

    #@35
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 131
    iget-boolean v0, p0, Landroid/content/pm/ProviderInfo;->isSyncable:Z

    #@3a
    if-eqz v0, :cond_44

    #@3c
    :goto_3c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3f
    .line 132
    return-void

    #@40
    :cond_40
    move v0, v2

    #@41
    .line 125
    goto :goto_19

    #@42
    :cond_42
    move v0, v2

    #@43
    .line 128
    goto :goto_2b

    #@44
    :cond_44
    move v1, v2

    #@45
    .line 131
    goto :goto_3c
.end method
