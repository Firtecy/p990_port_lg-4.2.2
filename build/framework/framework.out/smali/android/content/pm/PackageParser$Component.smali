.class public Landroid/content/pm/PackageParser$Component;
.super Ljava/lang/Object;
.source "PackageParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/pm/PackageParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Component"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<II:",
        "Landroid/content/pm/PackageParser$IntentInfo;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final className:Ljava/lang/String;

.field componentName:Landroid/content/ComponentName;

.field componentShortName:Ljava/lang/String;

.field public final intents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TII;>;"
        }
    .end annotation
.end field

.field public metaData:Landroid/os/Bundle;

.field public final owner:Landroid/content/pm/PackageParser$Package;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageParser$Component;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageParser$Component",
            "<TII;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3508
    .local p0, this:Landroid/content/pm/PackageParser$Component;,"Landroid/content/pm/PackageParser$Component<TII;>;"
    .local p1, clone:Landroid/content/pm/PackageParser$Component;,"Landroid/content/pm/PackageParser$Component<TII;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3509
    iget-object v0, p1, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@5
    iput-object v0, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@7
    .line 3510
    iget-object v0, p1, Landroid/content/pm/PackageParser$Component;->intents:Ljava/util/ArrayList;

    #@9
    iput-object v0, p0, Landroid/content/pm/PackageParser$Component;->intents:Ljava/util/ArrayList;

    #@b
    .line 3511
    iget-object v0, p1, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@d
    iput-object v0, p0, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@f
    .line 3512
    iget-object v0, p1, Landroid/content/pm/PackageParser$Component;->componentName:Landroid/content/ComponentName;

    #@11
    iput-object v0, p0, Landroid/content/pm/PackageParser$Component;->componentName:Landroid/content/ComponentName;

    #@13
    .line 3513
    iget-object v0, p1, Landroid/content/pm/PackageParser$Component;->componentShortName:Ljava/lang/String;

    #@15
    iput-object v0, p0, Landroid/content/pm/PackageParser$Component;->componentShortName:Ljava/lang/String;

    #@17
    .line 3514
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/PackageParser$Package;)V
    .registers 3
    .parameter "_owner"

    #@0
    .prologue
    .local p0, this:Landroid/content/pm/PackageParser$Component;,"Landroid/content/pm/PackageParser$Component<TII;>;"
    const/4 v0, 0x0

    #@1
    .line 3435
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 3436
    iput-object p1, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@6
    .line 3437
    iput-object v0, p0, Landroid/content/pm/PackageParser$Component;->intents:Ljava/util/ArrayList;

    #@8
    .line 3438
    iput-object v0, p0, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@a
    .line 3439
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/PackageParser$ParseComponentArgs;Landroid/content/pm/ComponentInfo;)V
    .registers 10
    .parameter "args"
    .parameter "outInfo"

    #@0
    .prologue
    .local p0, this:Landroid/content/pm/PackageParser$Component;,"Landroid/content/pm/PackageParser$Component<TII;>;"
    const/4 v6, 0x0

    #@1
    .line 3481
    invoke-direct {p0, p1, p2}, Landroid/content/pm/PackageParser$Component;-><init>(Landroid/content/pm/PackageParser$ParsePackageItemArgs;Landroid/content/pm/PackageItemInfo;)V

    #@4
    .line 3482
    iget-object v0, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->outError:[Ljava/lang/String;

    #@6
    aget-object v0, v0, v6

    #@8
    if-eqz v0, :cond_b

    #@a
    .line 3506
    :goto_a
    return-void

    #@b
    .line 3486
    :cond_b
    iget v0, p1, Landroid/content/pm/PackageParser$ParseComponentArgs;->processRes:I

    #@d
    if-eqz v0, :cond_39

    #@f
    .line 3488
    iget-object v0, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@11
    iget-object v0, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@13
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@15
    const/16 v1, 0x8

    #@17
    if-lt v0, v1, :cond_53

    #@19
    .line 3489
    iget-object v0, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@1b
    iget v1, p1, Landroid/content/pm/PackageParser$ParseComponentArgs;->processRes:I

    #@1d
    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 3496
    .local v2, pname:Ljava/lang/CharSequence;
    :goto_21
    iget-object v0, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@23
    iget-object v0, v0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@25
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@27
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@29
    iget-object v1, v1, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2b
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@2d
    iget v3, p1, Landroid/content/pm/PackageParser$ParseComponentArgs;->flags:I

    #@2f
    iget-object v4, p1, Landroid/content/pm/PackageParser$ParseComponentArgs;->sepProcesses:[Ljava/lang/String;

    #@31
    iget-object v5, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->outError:[Ljava/lang/String;

    #@33
    invoke-static/range {v0 .. v5}, Landroid/content/pm/PackageParser;->access$100(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    iput-object v0, p2, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@39
    .line 3501
    .end local v2           #pname:Ljava/lang/CharSequence;
    :cond_39
    iget v0, p1, Landroid/content/pm/PackageParser$ParseComponentArgs;->descriptionRes:I

    #@3b
    if-eqz v0, :cond_47

    #@3d
    .line 3502
    iget-object v0, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@3f
    iget v1, p1, Landroid/content/pm/PackageParser$ParseComponentArgs;->descriptionRes:I

    #@41
    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@44
    move-result v0

    #@45
    iput v0, p2, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@47
    .line 3505
    :cond_47
    iget-object v0, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@49
    iget v1, p1, Landroid/content/pm/PackageParser$ParseComponentArgs;->enabledRes:I

    #@4b
    const/4 v3, 0x1

    #@4c
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@4f
    move-result v0

    #@50
    iput-boolean v0, p2, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@52
    goto :goto_a

    #@53
    .line 3494
    :cond_53
    iget-object v0, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@55
    iget v1, p1, Landroid/content/pm/PackageParser$ParseComponentArgs;->processRes:I

    #@57
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    .restart local v2       #pname:Ljava/lang/CharSequence;
    goto :goto_21
.end method

.method public constructor <init>(Landroid/content/pm/PackageParser$ParsePackageItemArgs;Landroid/content/pm/PackageItemInfo;)V
    .registers 11
    .parameter "args"
    .parameter "outInfo"

    #@0
    .prologue
    .local p0, this:Landroid/content/pm/PackageParser$Component;,"Landroid/content/pm/PackageParser$Component<TII;>;"
    const/4 v6, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    .line 3441
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 3442
    iget-object v4, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->owner:Landroid/content/pm/PackageParser$Package;

    #@7
    iput-object v4, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@9
    .line 3443
    new-instance v4, Ljava/util/ArrayList;

    #@b
    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    #@e
    iput-object v4, p0, Landroid/content/pm/PackageParser$Component;->intents:Ljava/util/ArrayList;

    #@10
    .line 3444
    iget-object v4, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@12
    iget v5, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->nameRes:I

    #@14
    invoke-virtual {v4, v5, v7}, Landroid/content/res/TypedArray;->getNonConfigurationString(II)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    .line 3445
    .local v2, name:Ljava/lang/String;
    if-nez v2, :cond_36

    #@1a
    .line 3446
    iput-object v6, p0, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@1c
    .line 3447
    iget-object v4, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->outError:[Ljava/lang/String;

    #@1e
    new-instance v5, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    iget-object v6, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->tag:Ljava/lang/String;

    #@25
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    const-string v6, " does not specify android:name"

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    aput-object v5, v4, v7

    #@35
    .line 3478
    :goto_35
    return-void

    #@36
    .line 3451
    :cond_36
    iget-object v4, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@38
    iget-object v4, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3a
    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@3c
    iget-object v5, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->outError:[Ljava/lang/String;

    #@3e
    invoke-static {v4, v2, v5}, Landroid/content/pm/PackageParser;->access$000(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    iput-object v4, p2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@44
    .line 3453
    iget-object v4, p2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@46
    if-nez v4, :cond_64

    #@48
    .line 3454
    iput-object v6, p0, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@4a
    .line 3455
    iget-object v4, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->outError:[Ljava/lang/String;

    #@4c
    new-instance v5, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    iget-object v6, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->tag:Ljava/lang/String;

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    const-string v6, " does not have valid android:name"

    #@59
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    aput-object v5, v4, v7

    #@63
    goto :goto_35

    #@64
    .line 3459
    :cond_64
    iget-object v4, p2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@66
    iput-object v4, p0, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@68
    .line 3461
    iget-object v4, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@6a
    iget v5, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->iconRes:I

    #@6c
    invoke-virtual {v4, v5, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@6f
    move-result v0

    #@70
    .line 3462
    .local v0, iconVal:I
    if-eqz v0, :cond_76

    #@72
    .line 3463
    iput v0, p2, Landroid/content/pm/PackageItemInfo;->icon:I

    #@74
    .line 3464
    iput-object v6, p2, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@76
    .line 3467
    :cond_76
    iget-object v4, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@78
    iget v5, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->logoRes:I

    #@7a
    invoke-virtual {v4, v5, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@7d
    move-result v1

    #@7e
    .line 3468
    .local v1, logoVal:I
    if-eqz v1, :cond_82

    #@80
    .line 3469
    iput v1, p2, Landroid/content/pm/PackageItemInfo;->logo:I

    #@82
    .line 3472
    :cond_82
    iget-object v4, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->sa:Landroid/content/res/TypedArray;

    #@84
    iget v5, p1, Landroid/content/pm/PackageParser$ParsePackageItemArgs;->labelRes:I

    #@86
    invoke-virtual {v4, v5}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@89
    move-result-object v3

    #@8a
    .line 3473
    .local v3, v:Landroid/util/TypedValue;
    if-eqz v3, :cond_98

    #@8c
    iget v4, v3, Landroid/util/TypedValue;->resourceId:I

    #@8e
    iput v4, p2, Landroid/content/pm/PackageItemInfo;->labelRes:I

    #@90
    if-nez v4, :cond_98

    #@92
    .line 3474
    invoke-virtual {v3}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@95
    move-result-object v4

    #@96
    iput-object v4, p2, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@98
    .line 3477
    :cond_98
    iget-object v4, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@9a
    iget-object v4, v4, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@9c
    iput-object v4, p2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@9e
    goto :goto_35
.end method


# virtual methods
.method public getComponentName()Landroid/content/ComponentName;
    .registers 4

    #@0
    .prologue
    .line 3517
    .local p0, this:Landroid/content/pm/PackageParser$Component;,"Landroid/content/pm/PackageParser$Component<TII;>;"
    iget-object v0, p0, Landroid/content/pm/PackageParser$Component;->componentName:Landroid/content/ComponentName;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 3518
    iget-object v0, p0, Landroid/content/pm/PackageParser$Component;->componentName:Landroid/content/ComponentName;

    #@6
    .line 3524
    :goto_6
    return-object v0

    #@7
    .line 3520
    :cond_7
    iget-object v0, p0, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@9
    if-eqz v0, :cond_1a

    #@b
    .line 3521
    new-instance v0, Landroid/content/ComponentName;

    #@d
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->owner:Landroid/content/pm/PackageParser$Package;

    #@f
    iget-object v1, v1, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@11
    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@13
    iget-object v2, p0, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@15
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    iput-object v0, p0, Landroid/content/pm/PackageParser$Component;->componentName:Landroid/content/ComponentName;

    #@1a
    .line 3524
    :cond_1a
    iget-object v0, p0, Landroid/content/pm/PackageParser$Component;->componentName:Landroid/content/ComponentName;

    #@1c
    goto :goto_6
.end method

.method public getComponentShortName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 3528
    .local p0, this:Landroid/content/pm/PackageParser$Component;,"Landroid/content/pm/PackageParser$Component<TII;>;"
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->componentShortName:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 3529
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->componentShortName:Ljava/lang/String;

    #@6
    .line 3535
    :goto_6
    return-object v1

    #@7
    .line 3531
    :cond_7
    invoke-virtual {p0}, Landroid/content/pm/PackageParser$Component;->getComponentName()Landroid/content/ComponentName;

    #@a
    move-result-object v0

    #@b
    .line 3532
    .local v0, component:Landroid/content/ComponentName;
    if-eqz v0, :cond_13

    #@d
    .line 3533
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    iput-object v1, p0, Landroid/content/pm/PackageParser$Component;->componentShortName:Ljava/lang/String;

    #@13
    .line 3535
    :cond_13
    iget-object v1, p0, Landroid/content/pm/PackageParser$Component;->componentShortName:Ljava/lang/String;

    #@15
    goto :goto_6
.end method

.method public setPackageName(Ljava/lang/String;)V
    .registers 3
    .parameter "packageName"

    #@0
    .prologue
    .local p0, this:Landroid/content/pm/PackageParser$Component;,"Landroid/content/pm/PackageParser$Component<TII;>;"
    const/4 v0, 0x0

    #@1
    .line 3539
    iput-object v0, p0, Landroid/content/pm/PackageParser$Component;->componentName:Landroid/content/ComponentName;

    #@3
    .line 3540
    iput-object v0, p0, Landroid/content/pm/PackageParser$Component;->componentShortName:Ljava/lang/String;

    #@5
    .line 3541
    return-void
.end method
