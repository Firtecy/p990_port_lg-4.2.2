.class final Landroid/content/pm/ParceledListSlice$1;
.super Ljava/lang/Object;
.source "ParceledListSlice.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/pm/ParceledListSlice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/content/pm/ParceledListSlice;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 143
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/content/pm/ParceledListSlice;
    .registers 9
    .parameter "in"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5
    move-result v1

    #@6
    .line 146
    .local v1, numItems:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v6

    #@a
    if-ne v6, v0, :cond_2f

    #@c
    .line 148
    .local v0, lastSlice:Z
    :goto_c
    if-lez v1, :cond_31

    #@e
    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v4

    #@12
    .line 152
    .local v4, parcelSize:I
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    #@15
    move-result v2

    #@16
    .line 153
    .local v2, offset:I
    add-int v6, v2, v4

    #@18
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->setDataPosition(I)V

    #@1b
    .line 155
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1e
    move-result-object v3

    #@1f
    .line 156
    .local v3, p:Landroid/os/Parcel;
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    #@22
    .line 157
    invoke-virtual {v3, p1, v2, v4}, Landroid/os/Parcel;->appendFrom(Landroid/os/Parcel;II)V

    #@25
    .line 158
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    #@28
    .line 160
    new-instance v5, Landroid/content/pm/ParceledListSlice;

    #@2a
    const/4 v6, 0x0

    #@2b
    invoke-direct {v5, v3, v1, v0, v6}, Landroid/content/pm/ParceledListSlice;-><init>(Landroid/os/Parcel;IZLandroid/content/pm/ParceledListSlice$1;)V

    #@2e
    .line 162
    .end local v2           #offset:I
    .end local v3           #p:Landroid/os/Parcel;
    .end local v4           #parcelSize:I
    :goto_2e
    return-object v5

    #@2f
    .end local v0           #lastSlice:Z
    :cond_2f
    move v0, v5

    #@30
    .line 146
    goto :goto_c

    #@31
    .line 162
    .restart local v0       #lastSlice:Z
    :cond_31
    new-instance v5, Landroid/content/pm/ParceledListSlice;

    #@33
    invoke-direct {v5}, Landroid/content/pm/ParceledListSlice;-><init>()V

    #@36
    goto :goto_2e
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 143
    invoke-virtual {p0, p1}, Landroid/content/pm/ParceledListSlice$1;->createFromParcel(Landroid/os/Parcel;)Landroid/content/pm/ParceledListSlice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/content/pm/ParceledListSlice;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 167
    new-array v0, p1, [Landroid/content/pm/ParceledListSlice;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 143
    invoke-virtual {p0, p1}, Landroid/content/pm/ParceledListSlice$1;->newArray(I)[Landroid/content/pm/ParceledListSlice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
