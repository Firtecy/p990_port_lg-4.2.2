.class public Landroid/content/pm/ContainerEncryptionParams;
.super Ljava/lang/Object;
.source "ContainerEncryptionParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/ContainerEncryptionParams;",
            ">;"
        }
    .end annotation
.end field

.field private static final ENC_PARAMS_IV_PARAMETERS:I = 0x1

.field private static final MAC_PARAMS_NONE:I = 0x1

.field protected static final TAG:Ljava/lang/String; = "ContainerEncryptionParams"

.field private static final TO_STRING_PREFIX:Ljava/lang/String; = "ContainerEncryptionParams{"


# instance fields
.field private final mAuthenticatedDataStart:J

.field private final mDataEnd:J

.field private final mEncryptedDataStart:J

.field private final mEncryptionAlgorithm:Ljava/lang/String;

.field private final mEncryptionKey:Ljavax/crypto/SecretKey;

.field private final mEncryptionSpec:Ljavax/crypto/spec/IvParameterSpec;

.field private final mMacAlgorithm:Ljava/lang/String;

.field private final mMacKey:Ljavax/crypto/SecretKey;

.field private final mMacSpec:Ljava/security/spec/AlgorithmParameterSpec;

.field private final mMacTag:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 365
    new-instance v0, Landroid/content/pm/ContainerEncryptionParams$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/ContainerEncryptionParams$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/ContainerEncryptionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 8
    .parameter "source"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    #@0
    .prologue
    .line 325
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 326
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v3

    #@7
    iput-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionAlgorithm:Ljava/lang/String;

    #@9
    .line 327
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    .line 328
    .local v0, encParamType:I
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@10
    move-result-object v1

    #@11
    .line 329
    .local v1, encParamsEncoded:[B
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    #@14
    move-result-object v3

    #@15
    check-cast v3, Ljavax/crypto/SecretKey;

    #@17
    iput-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionKey:Ljavax/crypto/SecretKey;

    #@19
    .line 331
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    iput-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacAlgorithm:Ljava/lang/String;

    #@1f
    .line 332
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v2

    #@23
    .line 333
    .local v2, macParamType:I
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@26
    .line 334
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    #@29
    move-result-object v3

    #@2a
    check-cast v3, Ljavax/crypto/SecretKey;

    #@2c
    iput-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacKey:Ljavax/crypto/SecretKey;

    #@2e
    .line 336
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@31
    move-result-object v3

    #@32
    iput-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacTag:[B

    #@34
    .line 338
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@37
    move-result-wide v3

    #@38
    iput-wide v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mAuthenticatedDataStart:J

    #@3a
    .line 339
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@3d
    move-result-wide v3

    #@3e
    iput-wide v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptedDataStart:J

    #@40
    .line 340
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@43
    move-result-wide v3

    #@44
    iput-wide v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mDataEnd:J

    #@46
    .line 342
    packed-switch v0, :pswitch_data_96

    #@49
    .line 347
    new-instance v3, Ljava/security/InvalidAlgorithmParameterException;

    #@4b
    new-instance v4, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v5, "Unknown parameter type "

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v4

    #@5e
    invoke-direct {v3, v4}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    #@61
    throw v3

    #@62
    .line 344
    :pswitch_62
    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    #@64
    invoke-direct {v3, v1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    #@67
    iput-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionSpec:Ljavax/crypto/spec/IvParameterSpec;

    #@69
    .line 351
    packed-switch v2, :pswitch_data_9c

    #@6c
    .line 356
    new-instance v3, Ljava/security/InvalidAlgorithmParameterException;

    #@6e
    new-instance v4, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v5, "Unknown parameter type "

    #@75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v4

    #@81
    invoke-direct {v3, v4}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    #@84
    throw v3

    #@85
    .line 353
    :pswitch_85
    const/4 v3, 0x0

    #@86
    iput-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacSpec:Ljava/security/spec/AlgorithmParameterSpec;

    #@88
    .line 360
    iget-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionKey:Ljavax/crypto/SecretKey;

    #@8a
    if-nez v3, :cond_94

    #@8c
    .line 361
    new-instance v3, Ljava/lang/NullPointerException;

    #@8e
    const-string v4, "encryptionKey == null"

    #@90
    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@93
    throw v3

    #@94
    .line 363
    :cond_94
    return-void

    #@95
    .line 342
    nop

    #@96
    :pswitch_data_96
    .packed-switch 0x1
        :pswitch_62
    .end packed-switch

    #@9c
    .line 351
    :pswitch_data_9c
    .packed-switch 0x1
        :pswitch_85
    .end packed-switch
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/ContainerEncryptionParams$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/content/pm/ContainerEncryptionParams;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;Ljavax/crypto/SecretKey;)V
    .registers 18
    .parameter "encryptionAlgorithm"
    .parameter "encryptionSpec"
    .parameter "encryptionKey"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    #@0
    .prologue
    .line 87
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    const/4 v7, 0x0

    #@4
    const-wide/16 v8, -0x1

    #@6
    const-wide/16 v10, -0x1

    #@8
    const-wide/16 v12, -0x1

    #@a
    move-object v0, p0

    #@b
    move-object v1, p1

    #@c
    move-object/from16 v2, p2

    #@e
    move-object/from16 v3, p3

    #@10
    invoke-direct/range {v0 .. v13}, Landroid/content/pm/ContainerEncryptionParams;-><init>(Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;Ljavax/crypto/SecretKey;Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;Ljavax/crypto/SecretKey;[BJJJ)V

    #@13
    .line 89
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;Ljavax/crypto/SecretKey;Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;Ljavax/crypto/SecretKey;[BJJJ)V
    .registers 16
    .parameter "encryptionAlgorithm"
    .parameter "encryptionSpec"
    .parameter "encryptionKey"
    .parameter "macAlgorithm"
    .parameter "macSpec"
    .parameter "macKey"
    .parameter "macTag"
    .parameter "authenticatedDataStart"
    .parameter "encryptedDataStart"
    .parameter "dataEnd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    #@0
    .prologue
    .line 115
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 116
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_11

    #@9
    .line 117
    new-instance v0, Ljava/lang/NullPointerException;

    #@b
    const-string v1, "algorithm == null"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 118
    :cond_11
    if-nez p2, :cond_1b

    #@13
    .line 119
    new-instance v0, Ljava/lang/NullPointerException;

    #@15
    const-string v1, "encryptionSpec == null"

    #@17
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 120
    :cond_1b
    if-nez p3, :cond_25

    #@1d
    .line 121
    new-instance v0, Ljava/lang/NullPointerException;

    #@1f
    const-string v1, "encryptionKey == null"

    #@21
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 124
    :cond_25
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_36

    #@2b
    .line 125
    if-nez p6, :cond_36

    #@2d
    .line 126
    new-instance v0, Ljava/lang/NullPointerException;

    #@2f
    const-string/jumbo v1, "macKey == null"

    #@32
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@35
    throw v0

    #@36
    .line 130
    :cond_36
    instance-of v0, p2, Ljavax/crypto/spec/IvParameterSpec;

    #@38
    if-nez v0, :cond_42

    #@3a
    .line 131
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    #@3c
    const-string v1, "Unknown parameter spec class; must be IvParameters"

    #@3e
    invoke-direct {v0, v1}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    #@41
    throw v0

    #@42
    .line 135
    :cond_42
    iput-object p1, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionAlgorithm:Ljava/lang/String;

    #@44
    .line 136
    check-cast p2, Ljavax/crypto/spec/IvParameterSpec;

    #@46
    .end local p2
    iput-object p2, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionSpec:Ljavax/crypto/spec/IvParameterSpec;

    #@48
    .line 137
    iput-object p3, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionKey:Ljavax/crypto/SecretKey;

    #@4a
    .line 139
    iput-object p4, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacAlgorithm:Ljava/lang/String;

    #@4c
    .line 140
    iput-object p5, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacSpec:Ljava/security/spec/AlgorithmParameterSpec;

    #@4e
    .line 141
    iput-object p6, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacKey:Ljavax/crypto/SecretKey;

    #@50
    .line 142
    iput-object p7, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacTag:[B

    #@52
    .line 144
    iput-wide p8, p0, Landroid/content/pm/ContainerEncryptionParams;->mAuthenticatedDataStart:J

    #@54
    .line 145
    iput-wide p10, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptedDataStart:J

    #@56
    .line 146
    iput-wide p12, p0, Landroid/content/pm/ContainerEncryptionParams;->mDataEnd:J

    #@58
    .line 147
    return-void
.end method

.method private static final isSecretKeyEqual(Ljavax/crypto/SecretKey;Ljavax/crypto/SecretKey;)Z
    .registers 7
    .parameter "key1"
    .parameter "key2"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 234
    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getFormat()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 235
    .local v0, keyFormat:Ljava/lang/String;
    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getFormat()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 237
    .local v1, otherKeyFormat:Ljava/lang/String;
    if-nez v0, :cond_1a

    #@b
    .line 238
    if-eq v0, v1, :cond_e

    #@d
    .line 255
    :cond_d
    :goto_d
    return v2

    #@e
    .line 242
    :cond_e
    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    #@11
    move-result-object v3

    #@12
    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    #@15
    move-result-object v4

    #@16
    if-ne v3, v4, :cond_d

    #@18
    .line 255
    :cond_18
    const/4 v2, 0x1

    #@19
    goto :goto_d

    #@1a
    .line 246
    :cond_1a
    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getFormat()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_d

    #@24
    .line 250
    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    #@27
    move-result-object v3

    #@28
    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    #@2f
    move-result v3

    #@30
    if-nez v3, :cond_18

    #@32
    goto :goto_d
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 191
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 9
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 196
    if-ne p0, p1, :cond_5

    #@4
    .line 230
    :cond_4
    :goto_4
    return v1

    #@5
    .line 200
    :cond_5
    instance-of v3, p1, Landroid/content/pm/ContainerEncryptionParams;

    #@7
    if-nez v3, :cond_b

    #@9
    move v1, v2

    #@a
    .line 201
    goto :goto_4

    #@b
    :cond_b
    move-object v0, p1

    #@c
    .line 204
    check-cast v0, Landroid/content/pm/ContainerEncryptionParams;

    #@e
    .line 207
    .local v0, other:Landroid/content/pm/ContainerEncryptionParams;
    iget-wide v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mAuthenticatedDataStart:J

    #@10
    iget-wide v5, v0, Landroid/content/pm/ContainerEncryptionParams;->mAuthenticatedDataStart:J

    #@12
    cmp-long v3, v3, v5

    #@14
    if-nez v3, :cond_26

    #@16
    iget-wide v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptedDataStart:J

    #@18
    iget-wide v5, v0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptedDataStart:J

    #@1a
    cmp-long v3, v3, v5

    #@1c
    if-nez v3, :cond_26

    #@1e
    iget-wide v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mDataEnd:J

    #@20
    iget-wide v5, v0, Landroid/content/pm/ContainerEncryptionParams;->mDataEnd:J

    #@22
    cmp-long v3, v3, v5

    #@24
    if-eqz v3, :cond_28

    #@26
    :cond_26
    move v1, v2

    #@27
    .line 210
    goto :goto_4

    #@28
    .line 214
    :cond_28
    iget-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionAlgorithm:Ljava/lang/String;

    #@2a
    iget-object v4, v0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionAlgorithm:Ljava/lang/String;

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v3

    #@30
    if-eqz v3, :cond_3c

    #@32
    iget-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacAlgorithm:Ljava/lang/String;

    #@34
    iget-object v4, v0, Landroid/content/pm/ContainerEncryptionParams;->mMacAlgorithm:Ljava/lang/String;

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v3

    #@3a
    if-nez v3, :cond_3e

    #@3c
    :cond_3c
    move v1, v2

    #@3d
    .line 216
    goto :goto_4

    #@3e
    .line 220
    :cond_3e
    iget-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionKey:Ljavax/crypto/SecretKey;

    #@40
    iget-object v4, v0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionKey:Ljavax/crypto/SecretKey;

    #@42
    invoke-static {v3, v4}, Landroid/content/pm/ContainerEncryptionParams;->isSecretKeyEqual(Ljavax/crypto/SecretKey;Ljavax/crypto/SecretKey;)Z

    #@45
    move-result v3

    #@46
    if-eqz v3, :cond_52

    #@48
    iget-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacKey:Ljavax/crypto/SecretKey;

    #@4a
    iget-object v4, v0, Landroid/content/pm/ContainerEncryptionParams;->mMacKey:Ljavax/crypto/SecretKey;

    #@4c
    invoke-static {v3, v4}, Landroid/content/pm/ContainerEncryptionParams;->isSecretKeyEqual(Ljavax/crypto/SecretKey;Ljavax/crypto/SecretKey;)Z

    #@4f
    move-result v3

    #@50
    if-nez v3, :cond_54

    #@52
    :cond_52
    move v1, v2

    #@53
    .line 222
    goto :goto_4

    #@54
    .line 225
    :cond_54
    iget-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionSpec:Ljavax/crypto/spec/IvParameterSpec;

    #@56
    invoke-virtual {v3}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    #@59
    move-result-object v3

    #@5a
    iget-object v4, v0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionSpec:Ljavax/crypto/spec/IvParameterSpec;

    #@5c
    invoke-virtual {v4}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    #@5f
    move-result-object v4

    #@60
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    #@63
    move-result v3

    #@64
    if-eqz v3, :cond_76

    #@66
    iget-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacTag:[B

    #@68
    iget-object v4, v0, Landroid/content/pm/ContainerEncryptionParams;->mMacTag:[B

    #@6a
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    #@6d
    move-result v3

    #@6e
    if-eqz v3, :cond_76

    #@70
    iget-object v3, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacSpec:Ljava/security/spec/AlgorithmParameterSpec;

    #@72
    iget-object v4, v0, Landroid/content/pm/ContainerEncryptionParams;->mMacSpec:Ljava/security/spec/AlgorithmParameterSpec;

    #@74
    if-eq v3, v4, :cond_4

    #@76
    :cond_76
    move v1, v2

    #@77
    .line 227
    goto :goto_4
.end method

.method public getAuthenticatedDataStart()J
    .registers 3

    #@0
    .prologue
    .line 178
    iget-wide v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mAuthenticatedDataStart:J

    #@2
    return-wide v0
.end method

.method public getDataEnd()J
    .registers 3

    #@0
    .prologue
    .line 186
    iget-wide v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mDataEnd:J

    #@2
    return-wide v0
.end method

.method public getEncryptedDataStart()J
    .registers 3

    #@0
    .prologue
    .line 182
    iget-wide v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptedDataStart:J

    #@2
    return-wide v0
.end method

.method public getEncryptionAlgorithm()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionAlgorithm:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getEncryptionKey()Ljavax/crypto/SecretKey;
    .registers 2

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionKey:Ljavax/crypto/SecretKey;

    #@2
    return-object v0
.end method

.method public getEncryptionSpec()Ljava/security/spec/AlgorithmParameterSpec;
    .registers 2

    #@0
    .prologue
    .line 154
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionSpec:Ljavax/crypto/spec/IvParameterSpec;

    #@2
    return-object v0
.end method

.method public getMacAlgorithm()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 162
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacAlgorithm:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMacKey()Ljavax/crypto/SecretKey;
    .registers 2

    #@0
    .prologue
    .line 170
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacKey:Ljavax/crypto/SecretKey;

    #@2
    return-object v0
.end method

.method public getMacSpec()Ljava/security/spec/AlgorithmParameterSpec;
    .registers 2

    #@0
    .prologue
    .line 166
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacSpec:Ljava/security/spec/AlgorithmParameterSpec;

    #@2
    return-object v0
.end method

.method public getMacTag()[B
    .registers 2

    #@0
    .prologue
    .line 174
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacTag:[B

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 8

    #@0
    .prologue
    .line 260
    const/4 v0, 0x3

    #@1
    .line 262
    .local v0, hash:I
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionAlgorithm:Ljava/lang/String;

    #@3
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@6
    move-result v1

    #@7
    mul-int/lit8 v1, v1, 0x5

    #@9
    add-int/2addr v0, v1

    #@a
    .line 263
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionSpec:Ljavax/crypto/spec/IvParameterSpec;

    #@c
    invoke-virtual {v1}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    #@f
    move-result-object v1

    #@10
    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    #@13
    move-result v1

    #@14
    mul-int/lit8 v1, v1, 0x7

    #@16
    add-int/2addr v0, v1

    #@17
    .line 264
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionKey:Ljavax/crypto/SecretKey;

    #@19
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    #@1c
    move-result v1

    #@1d
    mul-int/lit8 v1, v1, 0xb

    #@1f
    add-int/2addr v0, v1

    #@20
    .line 265
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacAlgorithm:Ljava/lang/String;

    #@22
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@25
    move-result v1

    #@26
    mul-int/lit8 v1, v1, 0xd

    #@28
    add-int/2addr v0, v1

    #@29
    .line 266
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacKey:Ljavax/crypto/SecretKey;

    #@2b
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    #@2e
    move-result v1

    #@2f
    mul-int/lit8 v1, v1, 0x11

    #@31
    add-int/2addr v0, v1

    #@32
    .line 267
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacTag:[B

    #@34
    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    #@37
    move-result v1

    #@38
    mul-int/lit8 v1, v1, 0x13

    #@3a
    add-int/2addr v0, v1

    #@3b
    .line 268
    int-to-long v1, v0

    #@3c
    const-wide/16 v3, 0x17

    #@3e
    iget-wide v5, p0, Landroid/content/pm/ContainerEncryptionParams;->mAuthenticatedDataStart:J

    #@40
    mul-long/2addr v3, v5

    #@41
    add-long/2addr v1, v3

    #@42
    long-to-int v0, v1

    #@43
    .line 269
    int-to-long v1, v0

    #@44
    const-wide/16 v3, 0x1d

    #@46
    iget-wide v5, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptedDataStart:J

    #@48
    mul-long/2addr v3, v5

    #@49
    add-long/2addr v1, v3

    #@4a
    long-to-int v0, v1

    #@4b
    .line 270
    int-to-long v1, v0

    #@4c
    const-wide/16 v3, 0x1f

    #@4e
    iget-wide v5, p0, Landroid/content/pm/ContainerEncryptionParams;->mDataEnd:J

    #@50
    mul-long/2addr v3, v5

    #@51
    add-long/2addr v1, v3

    #@52
    long-to-int v0, v1

    #@53
    .line 272
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "ContainerEncryptionParams{"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 279
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string/jumbo v1, "mEncryptionAlgorithm=\""

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 280
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionAlgorithm:Ljava/lang/String;

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    .line 281
    const-string v1, "\","

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 282
    const-string/jumbo v1, "mEncryptionSpec="

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 283
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionSpec:Ljavax/crypto/spec/IvParameterSpec;

    #@1f
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 284
    const-string/jumbo v1, "mEncryptionKey="

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    .line 285
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionKey:Ljavax/crypto/SecretKey;

    #@2e
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    .line 287
    const-string/jumbo v1, "mMacAlgorithm=\""

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    .line 288
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacAlgorithm:Ljava/lang/String;

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    .line 289
    const-string v1, "\","

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    .line 290
    const-string/jumbo v1, "mMacSpec="

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    .line 291
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacSpec:Ljava/security/spec/AlgorithmParameterSpec;

    #@4d
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    .line 292
    const-string/jumbo v1, "mMacKey="

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    .line 293
    iget-object v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacKey:Ljavax/crypto/SecretKey;

    #@5c
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    .line 295
    const-string v1, ",mAuthenticatedDataStart="

    #@65
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    .line 296
    iget-wide v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mAuthenticatedDataStart:J

    #@6a
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6d
    .line 297
    const-string v1, ",mEncryptedDataStart="

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    .line 298
    iget-wide v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptedDataStart:J

    #@74
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@77
    .line 299
    const-string v1, ",mDataEnd="

    #@79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    .line 300
    iget-wide v1, p0, Landroid/content/pm/ContainerEncryptionParams;->mDataEnd:J

    #@7e
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@81
    .line 301
    const/16 v1, 0x7d

    #@83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@86
    .line 303
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v1

    #@8a
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 308
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionAlgorithm:Ljava/lang/String;

    #@3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@6
    .line 309
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 310
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionSpec:Ljavax/crypto/spec/IvParameterSpec;

    #@b
    invoke-virtual {v0}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    #@e
    move-result-object v0

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@12
    .line 311
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptionKey:Ljavax/crypto/SecretKey;

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    #@17
    .line 313
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacAlgorithm:Ljava/lang/String;

    #@19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 314
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 315
    const/4 v0, 0x0

    #@20
    new-array v0, v0, [B

    #@22
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@25
    .line 316
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacKey:Ljavax/crypto/SecretKey;

    #@27
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    #@2a
    .line 318
    iget-object v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mMacTag:[B

    #@2c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@2f
    .line 320
    iget-wide v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mAuthenticatedDataStart:J

    #@31
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@34
    .line 321
    iget-wide v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mEncryptedDataStart:J

    #@36
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@39
    .line 322
    iget-wide v0, p0, Landroid/content/pm/ContainerEncryptionParams;->mDataEnd:J

    #@3b
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@3e
    .line 323
    return-void
.end method
