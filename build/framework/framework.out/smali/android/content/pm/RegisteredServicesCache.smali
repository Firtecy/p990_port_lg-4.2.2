.class public abstract Landroid/content/pm/RegisteredServicesCache;
.super Ljava/lang/Object;
.source "RegisteredServicesCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,
        Landroid/content/pm/RegisteredServicesCache$UserServices;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "PackageManager"


# instance fields
.field private final mAttributesName:Ljava/lang/String;

.field public final mContext:Landroid/content/Context;

.field private final mExternalReceiver:Landroid/content/BroadcastReceiver;

.field private mHandler:Landroid/os/Handler;

.field private final mInterfaceName:Ljava/lang/String;

.field private mListener:Landroid/content/pm/RegisteredServicesCacheListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/content/pm/RegisteredServicesCacheListener",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final mMetaDataName:Ljava/lang/String;

.field private final mPackageReceiver:Landroid/content/BroadcastReceiver;

.field private final mPersistentServicesFile:Landroid/util/AtomicFile;

.field private mPersistentServicesFileDidNotExist:Z
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mServicesLock"
    .end annotation
.end field

.field private final mSerializerAndParser:Landroid/content/pm/XmlSerializerAndParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/content/pm/XmlSerializerAndParser",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final mServicesLock:Ljava/lang/Object;

.field private final mUserServices:Landroid/util/SparseArray;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mServicesLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/content/pm/RegisteredServicesCache$UserServices",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/XmlSerializerAndParser;)V
    .registers 16
    .parameter "context"
    .parameter "interfaceName"
    .parameter "metaDataName"
    .parameter "attributeName"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/pm/XmlSerializerAndParser",
            "<TV;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    .local p5, serializerAndParser:Landroid/content/pm/XmlSerializerAndParser;,"Landroid/content/pm/XmlSerializerAndParser<TV;>;"
    const/4 v4, 0x0

    #@1
    .line 114
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 80
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/pm/RegisteredServicesCache;->mServicesLock:Ljava/lang/Object;

    #@b
    .line 84
    new-instance v0, Landroid/util/SparseArray;

    #@d
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@10
    iput-object v0, p0, Landroid/content/pm/RegisteredServicesCache;->mUserServices:Landroid/util/SparseArray;

    #@12
    .line 143
    new-instance v0, Landroid/content/pm/RegisteredServicesCache$1;

    #@14
    invoke-direct {v0, p0}, Landroid/content/pm/RegisteredServicesCache$1;-><init>(Landroid/content/pm/RegisteredServicesCache;)V

    #@17
    iput-object v0, p0, Landroid/content/pm/RegisteredServicesCache;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    #@19
    .line 153
    new-instance v0, Landroid/content/pm/RegisteredServicesCache$2;

    #@1b
    invoke-direct {v0, p0}, Landroid/content/pm/RegisteredServicesCache$2;-><init>(Landroid/content/pm/RegisteredServicesCache;)V

    #@1e
    iput-object v0, p0, Landroid/content/pm/RegisteredServicesCache;->mExternalReceiver:Landroid/content/BroadcastReceiver;

    #@20
    .line 115
    iput-object p1, p0, Landroid/content/pm/RegisteredServicesCache;->mContext:Landroid/content/Context;

    #@22
    .line 116
    iput-object p2, p0, Landroid/content/pm/RegisteredServicesCache;->mInterfaceName:Ljava/lang/String;

    #@24
    .line 117
    iput-object p3, p0, Landroid/content/pm/RegisteredServicesCache;->mMetaDataName:Ljava/lang/String;

    #@26
    .line 118
    iput-object p4, p0, Landroid/content/pm/RegisteredServicesCache;->mAttributesName:Ljava/lang/String;

    #@28
    .line 119
    iput-object p5, p0, Landroid/content/pm/RegisteredServicesCache;->mSerializerAndParser:Landroid/content/pm/XmlSerializerAndParser;

    #@2a
    .line 121
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@2d
    move-result-object v6

    #@2e
    .line 122
    .local v6, dataDir:Ljava/io/File;
    new-instance v9, Ljava/io/File;

    #@30
    const-string/jumbo v0, "system"

    #@33
    invoke-direct {v9, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@36
    .line 123
    .local v9, systemDir:Ljava/io/File;
    new-instance v8, Ljava/io/File;

    #@38
    const-string/jumbo v0, "registered_services"

    #@3b
    invoke-direct {v8, v9, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@3e
    .line 124
    .local v8, syncDir:Ljava/io/File;
    new-instance v0, Landroid/util/AtomicFile;

    #@40
    new-instance v1, Ljava/io/File;

    #@42
    new-instance v2, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v5, ".xml"

    #@4d
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-direct {v1, v8, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@58
    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@5b
    iput-object v0, p0, Landroid/content/pm/RegisteredServicesCache;->mPersistentServicesFile:Landroid/util/AtomicFile;

    #@5d
    .line 127
    invoke-direct {p0}, Landroid/content/pm/RegisteredServicesCache;->readPersistentServicesLocked()V

    #@60
    .line 129
    new-instance v3, Landroid/content/IntentFilter;

    #@62
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@65
    .line 130
    .local v3, intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.PACKAGE_ADDED"

    #@67
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6a
    .line 131
    const-string v0, "android.intent.action.PACKAGE_CHANGED"

    #@6c
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6f
    .line 132
    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    #@71
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@74
    .line 133
    const-string/jumbo v0, "package"

    #@77
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@7a
    .line 134
    iget-object v0, p0, Landroid/content/pm/RegisteredServicesCache;->mContext:Landroid/content/Context;

    #@7c
    iget-object v1, p0, Landroid/content/pm/RegisteredServicesCache;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    #@7e
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@80
    move-object v5, v4

    #@81
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@84
    .line 137
    new-instance v7, Landroid/content/IntentFilter;

    #@86
    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    #@89
    .line 138
    .local v7, sdFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    #@8b
    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@8e
    .line 139
    const-string v0, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@90
    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@93
    .line 140
    iget-object v0, p0, Landroid/content/pm/RegisteredServicesCache;->mContext:Landroid/content/Context;

    #@95
    iget-object v1, p0, Landroid/content/pm/RegisteredServicesCache;->mExternalReceiver:Landroid/content/BroadcastReceiver;

    #@97
    invoke-virtual {v0, v1, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@9a
    .line 141
    return-void
.end method

.method static synthetic access$100(Landroid/content/pm/RegisteredServicesCache;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1}, Landroid/content/pm/RegisteredServicesCache;->generateServicesMap(I)V

    #@3
    return-void
.end method

.method private containsType(Ljava/util/ArrayList;Ljava/lang/Object;)Z
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/RegisteredServicesCache$ServiceInfo",
            "<TV;>;>;TV;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 409
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    .local p1, serviceInfos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;>;"
    .local p2, type:Ljava/lang/Object;,"TV;"
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@4
    move-result v0

    #@5
    .local v0, N:I
    :goto_5
    if-ge v1, v0, :cond_1a

    #@7
    .line 410
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v2

    #@b
    check-cast v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@d
    iget-object v2, v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@f
    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_17

    #@15
    .line 411
    const/4 v2, 0x1

    #@16
    .line 415
    :goto_16
    return v2

    #@17
    .line 409
    :cond_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_5

    #@1a
    .line 415
    :cond_1a
    const/4 v2, 0x0

    #@1b
    goto :goto_16
.end method

.method private containsTypeAndUid(Ljava/util/ArrayList;Ljava/lang/Object;I)Z
    .registers 8
    .parameter
    .parameter
    .parameter "uid"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/RegisteredServicesCache$ServiceInfo",
            "<TV;>;>;TV;I)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 419
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    .local p1, serviceInfos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;>;"
    .local p2, type:Ljava/lang/Object;,"TV;"
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@4
    move-result v0

    #@5
    .local v0, N:I
    :goto_5
    if-ge v1, v0, :cond_1e

    #@7
    .line 420
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v2

    #@b
    check-cast v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@d
    .line 421
    .local v2, serviceInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    iget-object v3, v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@f
    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_1b

    #@15
    iget v3, v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->uid:I

    #@17
    if-ne v3, p3, :cond_1b

    #@19
    .line 422
    const/4 v3, 0x1

    #@1a
    .line 426
    .end local v2           #serviceInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    :goto_1a
    return v3

    #@1b
    .line 419
    .restart local v2       #serviceInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_5

    #@1e
    .line 426
    .end local v2           #serviceInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    :cond_1e
    const/4 v3, 0x0

    #@1f
    goto :goto_1a
.end method

.method private findOrCreateUserLocked(I)Landroid/content/pm/RegisteredServicesCache$UserServices;
    .registers 4
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/content/pm/RegisteredServicesCache$UserServices",
            "<TV;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 95
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    iget-object v1, p0, Landroid/content/pm/RegisteredServicesCache;->mUserServices:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/content/pm/RegisteredServicesCache$UserServices;

    #@8
    .line 96
    .local v0, services:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    if-nez v0, :cond_15

    #@a
    .line 97
    new-instance v0, Landroid/content/pm/RegisteredServicesCache$UserServices;

    #@c
    .end local v0           #services:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    const/4 v1, 0x0

    #@d
    invoke-direct {v0, v1}, Landroid/content/pm/RegisteredServicesCache$UserServices;-><init>(Landroid/content/pm/RegisteredServicesCache$1;)V

    #@10
    .line 98
    .restart local v0       #services:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    iget-object v1, p0, Landroid/content/pm/RegisteredServicesCache;->mUserServices:Landroid/util/SparseArray;

    #@12
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@15
    .line 100
    :cond_15
    return-object v0
.end method

.method private generateServicesMap(I)V
    .registers 24
    .parameter "userId"

    #@0
    .prologue
    .line 299
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache;->mContext:Landroid/content/Context;

    #@4
    move-object/from16 v18, v0

    #@6
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@9
    move-result-object v10

    #@a
    .line 300
    .local v10, pm:Landroid/content/pm/PackageManager;
    new-instance v14, Ljava/util/ArrayList;

    #@c
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 301
    .local v14, serviceInfos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;>;"
    new-instance v18, Landroid/content/Intent;

    #@11
    move-object/from16 v0, p0

    #@13
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache;->mInterfaceName:Ljava/lang/String;

    #@15
    move-object/from16 v19, v0

    #@17
    invoke-direct/range {v18 .. v19}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1a
    const/16 v19, 0x80

    #@1c
    move-object/from16 v0, v18

    #@1e
    move/from16 v1, v19

    #@20
    move/from16 v2, p1

    #@22
    invoke-virtual {v10, v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentServicesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    #@25
    move-result-object v13

    #@26
    .line 303
    .local v13, resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@29
    move-result-object v8

    #@2a
    .local v8, i$:Ljava/util/Iterator;
    :goto_2a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@2d
    move-result v18

    #@2e
    if-eqz v18, :cond_a3

    #@30
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@33
    move-result-object v12

    #@34
    check-cast v12, Landroid/content/pm/ResolveInfo;

    #@36
    .line 305
    .local v12, resolveInfo:Landroid/content/pm/ResolveInfo;
    :try_start_36
    move-object/from16 v0, p0

    #@38
    invoke-direct {v0, v12}, Landroid/content/pm/RegisteredServicesCache;->parseServiceInfo(Landroid/content/pm/ResolveInfo;)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@3b
    move-result-object v9

    #@3c
    .line 306
    .local v9, info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    if-nez v9, :cond_7d

    #@3e
    .line 307
    const-string v18, "PackageManager"

    #@40
    new-instance v19, Ljava/lang/StringBuilder;

    #@42
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v20, "Unable to load service info "

    #@47
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v19

    #@4b
    invoke-virtual {v12}, Landroid/content/pm/ResolveInfo;->toString()Ljava/lang/String;

    #@4e
    move-result-object v20

    #@4f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v19

    #@53
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v19

    #@57
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5a
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_36 .. :try_end_5a} :catch_5b
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_5a} :catch_81

    #@5a
    goto :goto_2a

    #@5b
    .line 311
    .end local v9           #info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    :catch_5b
    move-exception v6

    #@5c
    .line 312
    .local v6, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v18, "PackageManager"

    #@5e
    new-instance v19, Ljava/lang/StringBuilder;

    #@60
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v20, "Unable to load service info "

    #@65
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v19

    #@69
    invoke-virtual {v12}, Landroid/content/pm/ResolveInfo;->toString()Ljava/lang/String;

    #@6c
    move-result-object v20

    #@6d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v19

    #@71
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v19

    #@75
    move-object/from16 v0, v18

    #@77
    move-object/from16 v1, v19

    #@79
    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7c
    goto :goto_2a

    #@7d
    .line 310
    .end local v6           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v9       #info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    :cond_7d
    :try_start_7d
    invoke-virtual {v14, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_80
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7d .. :try_end_80} :catch_5b
    .catch Ljava/io/IOException; {:try_start_7d .. :try_end_80} :catch_81

    #@80
    goto :goto_2a

    #@81
    .line 313
    .end local v9           #info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    :catch_81
    move-exception v6

    #@82
    .line 314
    .local v6, e:Ljava/io/IOException;
    const-string v18, "PackageManager"

    #@84
    new-instance v19, Ljava/lang/StringBuilder;

    #@86
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v20, "Unable to load service info "

    #@8b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v19

    #@8f
    invoke-virtual {v12}, Landroid/content/pm/ResolveInfo;->toString()Ljava/lang/String;

    #@92
    move-result-object v20

    #@93
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v19

    #@97
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v19

    #@9b
    move-object/from16 v0, v18

    #@9d
    move-object/from16 v1, v19

    #@9f
    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a2
    goto :goto_2a

    #@a3
    .line 318
    .end local v6           #e:Ljava/io/IOException;
    .end local v12           #resolveInfo:Landroid/content/pm/ResolveInfo;
    :cond_a3
    move-object/from16 v0, p0

    #@a5
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache;->mServicesLock:Ljava/lang/Object;

    #@a7
    move-object/from16 v19, v0

    #@a9
    monitor-enter v19

    #@aa
    .line 319
    :try_start_aa
    invoke-direct/range {p0 .. p1}, Landroid/content/pm/RegisteredServicesCache;->findOrCreateUserLocked(I)Landroid/content/pm/RegisteredServicesCache$UserServices;

    #@ad
    move-result-object v16

    #@ae
    .line 320
    .local v16, user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    move-object/from16 v0, v16

    #@b0
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@b2
    move-object/from16 v18, v0

    #@b4
    if-nez v18, :cond_13b

    #@b6
    const/4 v7, 0x1

    #@b7
    .line 321
    .local v7, firstScan:Z
    :goto_b7
    if-eqz v7, :cond_13e

    #@b9
    .line 322
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@bc
    move-result-object v18

    #@bd
    move-object/from16 v0, v18

    #@bf
    move-object/from16 v1, v16

    #@c1
    iput-object v0, v1, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@c3
    .line 327
    :goto_c3
    new-instance v5, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    .line 328
    .local v5, changes:Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    #@c9
    .line 329
    .local v4, changed:Z
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@cc
    move-result-object v8

    #@cd
    :cond_cd
    :goto_cd
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@d0
    move-result v18

    #@d1
    if-eqz v18, :cond_1cc

    #@d3
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d6
    move-result-object v9

    #@d7
    check-cast v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@d9
    .line 339
    .restart local v9       #info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    move-object/from16 v0, v16

    #@db
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->persistentServices:Ljava/util/Map;

    #@dd
    move-object/from16 v18, v0

    #@df
    iget-object v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@e1
    move-object/from16 v20, v0

    #@e3
    move-object/from16 v0, v18

    #@e5
    move-object/from16 v1, v20

    #@e7
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@ea
    move-result-object v11

    #@eb
    check-cast v11, Ljava/lang/Integer;

    #@ed
    .line 340
    .local v11, previousUid:Ljava/lang/Integer;
    if-nez v11, :cond_149

    #@ef
    .line 344
    const/4 v4, 0x1

    #@f0
    .line 345
    move-object/from16 v0, v16

    #@f2
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@f4
    move-object/from16 v18, v0

    #@f6
    iget-object v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@f8
    move-object/from16 v20, v0

    #@fa
    move-object/from16 v0, v18

    #@fc
    move-object/from16 v1, v20

    #@fe
    invoke-interface {v0, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@101
    .line 346
    move-object/from16 v0, v16

    #@103
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->persistentServices:Ljava/util/Map;

    #@105
    move-object/from16 v18, v0

    #@107
    iget-object v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@109
    move-object/from16 v20, v0

    #@10b
    iget v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->uid:I

    #@10d
    move/from16 v21, v0

    #@10f
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@112
    move-result-object v21

    #@113
    move-object/from16 v0, v18

    #@115
    move-object/from16 v1, v20

    #@117
    move-object/from16 v2, v21

    #@119
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11c
    .line 347
    move-object/from16 v0, p0

    #@11e
    iget-boolean v0, v0, Landroid/content/pm/RegisteredServicesCache;->mPersistentServicesFileDidNotExist:Z

    #@120
    move/from16 v18, v0

    #@122
    if-eqz v18, :cond_126

    #@124
    if-nez v7, :cond_cd

    #@126
    .line 348
    :cond_126
    iget-object v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@128
    move-object/from16 v18, v0

    #@12a
    const/16 v20, 0x0

    #@12c
    move-object/from16 v0, p0

    #@12e
    move-object/from16 v1, v18

    #@130
    move/from16 v2, p1

    #@132
    move/from16 v3, v20

    #@134
    invoke-direct {v0, v1, v2, v3}, Landroid/content/pm/RegisteredServicesCache;->notifyListener(Ljava/lang/Object;IZ)V

    #@137
    goto :goto_cd

    #@138
    .line 405
    .end local v4           #changed:Z
    .end local v5           #changes:Ljava/lang/StringBuilder;
    .end local v7           #firstScan:Z
    .end local v9           #info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    .end local v11           #previousUid:Ljava/lang/Integer;
    .end local v16           #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    :catchall_138
    move-exception v18

    #@139
    monitor-exit v19
    :try_end_13a
    .catchall {:try_start_aa .. :try_end_13a} :catchall_138

    #@13a
    throw v18

    #@13b
    .line 320
    .restart local v16       #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    :cond_13b
    const/4 v7, 0x0

    #@13c
    goto/16 :goto_b7

    #@13e
    .line 324
    .restart local v7       #firstScan:Z
    :cond_13e
    :try_start_13e
    move-object/from16 v0, v16

    #@140
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@142
    move-object/from16 v18, v0

    #@144
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->clear()V

    #@147
    goto/16 :goto_c3

    #@149
    .line 350
    .restart local v4       #changed:Z
    .restart local v5       #changes:Ljava/lang/StringBuilder;
    .restart local v9       #info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    .restart local v11       #previousUid:Ljava/lang/Integer;
    :cond_149
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@14c
    move-result v18

    #@14d
    iget v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->uid:I

    #@14f
    move/from16 v20, v0

    #@151
    move/from16 v0, v18

    #@153
    move/from16 v1, v20

    #@155
    if-ne v0, v1, :cond_16a

    #@157
    .line 354
    move-object/from16 v0, v16

    #@159
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@15b
    move-object/from16 v18, v0

    #@15d
    iget-object v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@15f
    move-object/from16 v20, v0

    #@161
    move-object/from16 v0, v18

    #@163
    move-object/from16 v1, v20

    #@165
    invoke-interface {v0, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@168
    goto/16 :goto_cd

    #@16a
    .line 355
    :cond_16a
    iget v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->uid:I

    #@16c
    move/from16 v18, v0

    #@16e
    move-object/from16 v0, p0

    #@170
    move/from16 v1, v18

    #@172
    invoke-direct {v0, v1}, Landroid/content/pm/RegisteredServicesCache;->inSystemImage(I)Z

    #@175
    move-result v18

    #@176
    if-nez v18, :cond_18c

    #@178
    iget-object v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@17a
    move-object/from16 v18, v0

    #@17c
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@17f
    move-result v20

    #@180
    move-object/from16 v0, p0

    #@182
    move-object/from16 v1, v18

    #@184
    move/from16 v2, v20

    #@186
    invoke-direct {v0, v14, v1, v2}, Landroid/content/pm/RegisteredServicesCache;->containsTypeAndUid(Ljava/util/ArrayList;Ljava/lang/Object;I)Z

    #@189
    move-result v18

    #@18a
    if-nez v18, :cond_cd

    #@18c
    .line 366
    :cond_18c
    const/4 v4, 0x1

    #@18d
    .line 367
    move-object/from16 v0, v16

    #@18f
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@191
    move-object/from16 v18, v0

    #@193
    iget-object v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@195
    move-object/from16 v20, v0

    #@197
    move-object/from16 v0, v18

    #@199
    move-object/from16 v1, v20

    #@19b
    invoke-interface {v0, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19e
    .line 368
    move-object/from16 v0, v16

    #@1a0
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->persistentServices:Ljava/util/Map;

    #@1a2
    move-object/from16 v18, v0

    #@1a4
    iget-object v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@1a6
    move-object/from16 v20, v0

    #@1a8
    iget v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->uid:I

    #@1aa
    move/from16 v21, v0

    #@1ac
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1af
    move-result-object v21

    #@1b0
    move-object/from16 v0, v18

    #@1b2
    move-object/from16 v1, v20

    #@1b4
    move-object/from16 v2, v21

    #@1b6
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b9
    .line 369
    iget-object v0, v9, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@1bb
    move-object/from16 v18, v0

    #@1bd
    const/16 v20, 0x0

    #@1bf
    move-object/from16 v0, p0

    #@1c1
    move-object/from16 v1, v18

    #@1c3
    move/from16 v2, p1

    #@1c5
    move/from16 v3, v20

    #@1c7
    invoke-direct {v0, v1, v2, v3}, Landroid/content/pm/RegisteredServicesCache;->notifyListener(Ljava/lang/Object;IZ)V

    #@1ca
    goto/16 :goto_cd

    #@1cc
    .line 379
    .end local v9           #info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<TV;>;"
    .end local v11           #previousUid:Ljava/lang/Integer;
    :cond_1cc
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@1cf
    move-result-object v15

    #@1d0
    .line 380
    .local v15, toBeRemoved:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TV;>;"
    move-object/from16 v0, v16

    #@1d2
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->persistentServices:Ljava/util/Map;

    #@1d4
    move-object/from16 v18, v0

    #@1d6
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@1d9
    move-result-object v18

    #@1da
    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1dd
    move-result-object v8

    #@1de
    :cond_1de
    :goto_1de
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@1e1
    move-result v18

    #@1e2
    if-eqz v18, :cond_1f8

    #@1e4
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e7
    move-result-object v17

    #@1e8
    .line 381
    .local v17, v1:Ljava/lang/Object;,"TV;"
    move-object/from16 v0, p0

    #@1ea
    move-object/from16 v1, v17

    #@1ec
    invoke-direct {v0, v14, v1}, Landroid/content/pm/RegisteredServicesCache;->containsType(Ljava/util/ArrayList;Ljava/lang/Object;)Z

    #@1ef
    move-result v18

    #@1f0
    if-nez v18, :cond_1de

    #@1f2
    .line 382
    move-object/from16 v0, v17

    #@1f4
    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1f7
    goto :goto_1de

    #@1f8
    .line 385
    .end local v17           #v1:Ljava/lang/Object;,"TV;"
    :cond_1f8
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1fb
    move-result-object v8

    #@1fc
    :goto_1fc
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@1ff
    move-result v18

    #@200
    if-eqz v18, :cond_222

    #@202
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@205
    move-result-object v17

    #@206
    .line 389
    .restart local v17       #v1:Ljava/lang/Object;,"TV;"
    const/4 v4, 0x1

    #@207
    .line 390
    move-object/from16 v0, v16

    #@209
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->persistentServices:Ljava/util/Map;

    #@20b
    move-object/from16 v18, v0

    #@20d
    move-object/from16 v0, v18

    #@20f
    move-object/from16 v1, v17

    #@211
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@214
    .line 391
    const/16 v18, 0x1

    #@216
    move-object/from16 v0, p0

    #@218
    move-object/from16 v1, v17

    #@21a
    move/from16 v2, p1

    #@21c
    move/from16 v3, v18

    #@21e
    invoke-direct {v0, v1, v2, v3}, Landroid/content/pm/RegisteredServicesCache;->notifyListener(Ljava/lang/Object;IZ)V

    #@221
    goto :goto_1fc

    #@222
    .line 402
    .end local v17           #v1:Ljava/lang/Object;,"TV;"
    :cond_222
    if-eqz v4, :cond_227

    #@224
    .line 403
    invoke-direct/range {p0 .. p0}, Landroid/content/pm/RegisteredServicesCache;->writePersistentServicesLocked()V

    #@227
    .line 405
    :cond_227
    monitor-exit v19
    :try_end_228
    .catchall {:try_start_13e .. :try_end_228} :catchall_138

    #@228
    .line 406
    return-void
.end method

.method private inSystemImage(I)Z
    .registers 12
    .parameter "callerUid"

    #@0
    .prologue
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    const/4 v7, 0x0

    #@1
    .line 275
    iget-object v8, p0, Landroid/content/pm/RegisteredServicesCache;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@6
    move-result-object v8

    #@7
    invoke-virtual {v8, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@a
    move-result-object v6

    #@b
    .line 276
    .local v6, packages:[Ljava/lang/String;
    move-object v0, v6

    #@c
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@d
    .local v3, len$:I
    const/4 v2, 0x0

    #@e
    .local v2, i$:I
    :goto_e
    if-ge v2, v3, :cond_26

    #@10
    aget-object v4, v0, v2

    #@12
    .line 278
    .local v4, name:Ljava/lang/String;
    :try_start_12
    iget-object v8, p0, Landroid/content/pm/RegisteredServicesCache;->mContext:Landroid/content/Context;

    #@14
    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@17
    move-result-object v8

    #@18
    const/4 v9, 0x0

    #@19
    invoke-virtual {v8, v4, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@1c
    move-result-object v5

    #@1d
    .line 280
    .local v5, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v8, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1f
    iget v8, v8, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_21
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_12 .. :try_end_21} :catch_27

    #@21
    and-int/lit8 v8, v8, 0x1

    #@23
    if-eqz v8, :cond_29

    #@25
    .line 281
    const/4 v7, 0x1

    #@26
    .line 287
    .end local v4           #name:Ljava/lang/String;
    .end local v5           #packageInfo:Landroid/content/pm/PackageInfo;
    :cond_26
    :goto_26
    return v7

    #@27
    .line 283
    .restart local v4       #name:Ljava/lang/String;
    :catch_27
    move-exception v1

    #@28
    .line 284
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_26

    #@29
    .line 276
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5       #packageInfo:Landroid/content/pm/PackageInfo;
    :cond_29
    add-int/lit8 v2, v2, 0x1

    #@2b
    goto :goto_e
.end method

.method private notifyListener(Ljava/lang/Object;IZ)V
    .registers 12
    .parameter
    .parameter "userId"
    .parameter "removed"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;IZ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 204
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    .local p1, type:Ljava/lang/Object;,"TV;"
    monitor-enter p0

    #@1
    .line 205
    :try_start_1
    iget-object v7, p0, Landroid/content/pm/RegisteredServicesCache;->mListener:Landroid/content/pm/RegisteredServicesCacheListener;

    #@3
    .line 206
    .local v7, listener:Landroid/content/pm/RegisteredServicesCacheListener;,"Landroid/content/pm/RegisteredServicesCacheListener<TV;>;"
    iget-object v6, p0, Landroid/content/pm/RegisteredServicesCache;->mHandler:Landroid/os/Handler;

    #@5
    .line 207
    .local v6, handler:Landroid/os/Handler;
    monitor-exit p0

    #@6
    .line 208
    if-nez v7, :cond_c

    #@8
    .line 218
    :goto_8
    return-void

    #@9
    .line 207
    .end local v6           #handler:Landroid/os/Handler;
    .end local v7           #listener:Landroid/content/pm/RegisteredServicesCacheListener;,"Landroid/content/pm/RegisteredServicesCacheListener<TV;>;"
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_9

    #@b
    throw v0

    #@c
    .line 212
    .restart local v6       #handler:Landroid/os/Handler;
    .restart local v7       #listener:Landroid/content/pm/RegisteredServicesCacheListener;,"Landroid/content/pm/RegisteredServicesCacheListener<TV;>;"
    :cond_c
    move-object v2, v7

    #@d
    .line 213
    .local v2, listener2:Landroid/content/pm/RegisteredServicesCacheListener;,"Landroid/content/pm/RegisteredServicesCacheListener<TV;>;"
    new-instance v0, Landroid/content/pm/RegisteredServicesCache$3;

    #@f
    move-object v1, p0

    #@10
    move-object v3, p1

    #@11
    move v4, p2

    #@12
    move v5, p3

    #@13
    invoke-direct/range {v0 .. v5}, Landroid/content/pm/RegisteredServicesCache$3;-><init>(Landroid/content/pm/RegisteredServicesCache;Landroid/content/pm/RegisteredServicesCacheListener;Ljava/lang/Object;IZ)V

    #@16
    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@19
    goto :goto_8
.end method

.method private parseServiceInfo(Landroid/content/pm/ResolveInfo;)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;
    .registers 18
    .parameter "service"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/ResolveInfo;",
            ")",
            "Landroid/content/pm/RegisteredServicesCache$ServiceInfo",
            "<TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 431
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    move-object/from16 v0, p1

    #@2
    iget-object v9, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4
    .line 432
    .local v9, si:Landroid/content/pm/ServiceInfo;
    new-instance v3, Landroid/content/ComponentName;

    #@6
    iget-object v13, v9, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@8
    iget-object v14, v9, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@a
    invoke-direct {v3, v13, v14}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 434
    .local v3, componentName:Landroid/content/ComponentName;
    move-object/from16 v0, p0

    #@f
    iget-object v13, v0, Landroid/content/pm/RegisteredServicesCache;->mContext:Landroid/content/Context;

    #@11
    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@14
    move-result-object v7

    #@15
    .line 436
    .local v7, pm:Landroid/content/pm/PackageManager;
    const/4 v6, 0x0

    #@16
    .line 438
    .local v6, parser:Landroid/content/res/XmlResourceParser;
    :try_start_16
    move-object/from16 v0, p0

    #@18
    iget-object v13, v0, Landroid/content/pm/RegisteredServicesCache;->mMetaDataName:Ljava/lang/String;

    #@1a
    invoke-virtual {v9, v7, v13}, Landroid/content/pm/ServiceInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@1d
    move-result-object v6

    #@1e
    .line 439
    if-nez v6, :cond_66

    #@20
    .line 440
    new-instance v13, Lorg/xmlpull/v1/XmlPullParserException;

    #@22
    new-instance v14, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v15, "No "

    #@29
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v14

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget-object v15, v0, Landroid/content/pm/RegisteredServicesCache;->mMetaDataName:Ljava/lang/String;

    #@31
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v14

    #@35
    const-string v15, " meta-data"

    #@37
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v14

    #@3b
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v14

    #@3f
    invoke-direct {v13, v14}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@42
    throw v13
    :try_end_43
    .catchall {:try_start_16 .. :try_end_43} :catchall_5f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_16 .. :try_end_43} :catch_43

    #@43
    .line 465
    :catch_43
    move-exception v4

    #@44
    .line 466
    .local v4, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_44
    new-instance v13, Lorg/xmlpull/v1/XmlPullParserException;

    #@46
    new-instance v14, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v15, "Unable to load resources for pacakge "

    #@4d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v14

    #@51
    iget-object v15, v9, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@53
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v14

    #@57
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v14

    #@5b
    invoke-direct {v13, v14}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@5e
    throw v13
    :try_end_5f
    .catchall {:try_start_44 .. :try_end_5f} :catchall_5f

    #@5f
    .line 469
    .end local v4           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_5f
    move-exception v13

    #@60
    if-eqz v6, :cond_65

    #@62
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->close()V

    #@65
    :cond_65
    throw v13

    #@66
    .line 443
    :cond_66
    :try_start_66
    invoke-static {v6}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@69
    move-result-object v2

    #@6a
    .line 447
    .local v2, attrs:Landroid/util/AttributeSet;
    :cond_6a
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->next()I

    #@6d
    move-result v10

    #@6e
    .local v10, type:I
    const/4 v13, 0x1

    #@6f
    if-eq v10, v13, :cond_74

    #@71
    const/4 v13, 0x2

    #@72
    if-ne v10, v13, :cond_6a

    #@74
    .line 450
    :cond_74
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@77
    move-result-object v5

    #@78
    .line 451
    .local v5, nodeName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@7a
    iget-object v13, v0, Landroid/content/pm/RegisteredServicesCache;->mAttributesName:Ljava/lang/String;

    #@7c
    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v13

    #@80
    if-nez v13, :cond_a5

    #@82
    .line 452
    new-instance v13, Lorg/xmlpull/v1/XmlPullParserException;

    #@84
    new-instance v14, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v15, "Meta-data does not start with "

    #@8b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v14

    #@8f
    move-object/from16 v0, p0

    #@91
    iget-object v15, v0, Landroid/content/pm/RegisteredServicesCache;->mAttributesName:Ljava/lang/String;

    #@93
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v14

    #@97
    const-string v15, " tag"

    #@99
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v14

    #@9d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v14

    #@a1
    invoke-direct {v13, v14}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@a4
    throw v13

    #@a5
    .line 456
    :cond_a5
    iget-object v13, v9, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@a7
    invoke-virtual {v7, v13}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    #@aa
    move-result-object v13

    #@ab
    iget-object v14, v9, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@ad
    move-object/from16 v0, p0

    #@af
    invoke-virtual {v0, v13, v14, v2}, Landroid/content/pm/RegisteredServicesCache;->parseServiceAttributes(Landroid/content/res/Resources;Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;
    :try_end_b2
    .catchall {:try_start_66 .. :try_end_b2} :catchall_5f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_66 .. :try_end_b2} :catch_43

    #@b2
    move-result-object v12

    #@b3
    .line 458
    .local v12, v:Ljava/lang/Object;,"TV;"
    if-nez v12, :cond_bc

    #@b5
    .line 459
    const/4 v13, 0x0

    #@b6
    .line 469
    if-eqz v6, :cond_bb

    #@b8
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->close()V

    #@bb
    :cond_bb
    :goto_bb
    return-object v13

    #@bc
    .line 461
    :cond_bc
    :try_start_bc
    move-object/from16 v0, p1

    #@be
    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@c0
    .line 462
    .local v8, serviceInfo:Landroid/content/pm/ServiceInfo;
    iget-object v1, v8, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@c2
    .line 463
    .local v1, applicationInfo:Landroid/content/pm/ApplicationInfo;
    iget v11, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@c4
    .line 464
    .local v11, uid:I
    new-instance v13, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@c6
    invoke-direct {v13, v12, v3, v11}, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;-><init>(Ljava/lang/Object;Landroid/content/ComponentName;I)V
    :try_end_c9
    .catchall {:try_start_bc .. :try_end_c9} :catchall_5f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_bc .. :try_end_c9} :catch_43

    #@c9
    .line 469
    if-eqz v6, :cond_bb

    #@cb
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->close()V

    #@ce
    goto :goto_bb
.end method

.method private readPersistentServicesLocked()V
    .registers 15

    #@0
    .prologue
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    const/4 v11, 0x1

    #@1
    const/4 v13, 0x2

    #@2
    .line 477
    iget-object v10, p0, Landroid/content/pm/RegisteredServicesCache;->mUserServices:Landroid/util/SparseArray;

    #@4
    invoke-virtual {v10}, Landroid/util/SparseArray;->clear()V

    #@7
    .line 478
    iget-object v10, p0, Landroid/content/pm/RegisteredServicesCache;->mSerializerAndParser:Landroid/content/pm/XmlSerializerAndParser;

    #@9
    if-nez v10, :cond_c

    #@b
    .line 525
    :cond_b
    :goto_b
    return-void

    #@c
    .line 481
    :cond_c
    const/4 v2, 0x0

    #@d
    .line 483
    .local v2, fis:Ljava/io/FileInputStream;
    :try_start_d
    iget-object v10, p0, Landroid/content/pm/RegisteredServicesCache;->mPersistentServicesFile:Landroid/util/AtomicFile;

    #@f
    invoke-virtual {v10}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    #@12
    move-result-object v10

    #@13
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    #@16
    move-result v10

    #@17
    if-nez v10, :cond_28

    #@19
    move v10, v11

    #@1a
    :goto_1a
    iput-boolean v10, p0, Landroid/content/pm/RegisteredServicesCache;->mPersistentServicesFileDidNotExist:Z

    #@1c
    .line 484
    iget-boolean v10, p0, Landroid/content/pm/RegisteredServicesCache;->mPersistentServicesFileDidNotExist:Z
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_af
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_1e} :catch_9d

    #@1e
    if-eqz v10, :cond_2a

    #@20
    .line 518
    if-eqz v2, :cond_b

    #@22
    .line 520
    :try_start_22
    #Replaced unresolvable odex instruction with a throw
    throw v2
    #invoke-virtual-quick {v2}, vtable@0xc
    :try_end_25
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_25} :catch_26

    #@25
    goto :goto_b

    #@26
    .line 521
    :catch_26
    move-exception v10

    #@27
    goto :goto_b

    #@28
    .line 483
    :cond_28
    const/4 v10, 0x0

    #@29
    goto :goto_1a

    #@2a
    .line 487
    :cond_2a
    :try_start_2a
    iget-object v10, p0, Landroid/content/pm/RegisteredServicesCache;->mPersistentServicesFile:Landroid/util/AtomicFile;

    #@2c
    invoke-virtual {v10}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@2f
    move-result-object v2

    #@30
    .line 488
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@33
    move-result-object v3

    #@34
    .line 489
    .local v3, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v10, 0x0

    #@35
    invoke-interface {v3, v2, v10}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@38
    .line 490
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@3b
    move-result v1

    #@3c
    .line 491
    .local v1, eventType:I
    :goto_3c
    if-eq v1, v13, :cond_43

    #@3e
    .line 492
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@41
    move-result v1

    #@42
    goto :goto_3c

    #@43
    .line 494
    :cond_43
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    .line 495
    .local v5, tagName:Ljava/lang/String;
    const-string/jumbo v10, "services"

    #@4a
    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v10

    #@4e
    if-eqz v10, :cond_71

    #@50
    .line 496
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@53
    move-result v1

    #@54
    .line 498
    :cond_54
    if-ne v1, v13, :cond_96

    #@56
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@59
    move-result v10

    #@5a
    if-ne v10, v13, :cond_96

    #@5c
    .line 499
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    .line 500
    const-string/jumbo v10, "service"

    #@63
    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v10

    #@67
    if-eqz v10, :cond_96

    #@69
    .line 501
    iget-object v10, p0, Landroid/content/pm/RegisteredServicesCache;->mSerializerAndParser:Landroid/content/pm/XmlSerializerAndParser;

    #@6b
    invoke-interface {v10, v3}, Landroid/content/pm/XmlSerializerAndParser;->createFromXml(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/Object;
    :try_end_6e
    .catchall {:try_start_2a .. :try_end_6e} :catchall_af
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_6e} :catch_9d

    #@6e
    move-result-object v4

    #@6f
    .line 502
    .local v4, service:Ljava/lang/Object;,"TV;"
    if-nez v4, :cond_79

    #@71
    .line 518
    .end local v4           #service:Ljava/lang/Object;,"TV;"
    :cond_71
    :goto_71
    if-eqz v2, :cond_b

    #@73
    .line 520
    :try_start_73
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_76
    .catch Ljava/io/IOException; {:try_start_73 .. :try_end_76} :catch_77

    #@76
    goto :goto_b

    #@77
    .line 521
    :catch_77
    move-exception v10

    #@78
    goto :goto_b

    #@79
    .line 505
    .restart local v4       #service:Ljava/lang/Object;,"TV;"
    :cond_79
    const/4 v10, 0x0

    #@7a
    :try_start_7a
    const-string/jumbo v12, "uid"

    #@7d
    invoke-interface {v3, v10, v12}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@80
    move-result-object v7

    #@81
    .line 506
    .local v7, uidString:Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@84
    move-result v6

    #@85
    .line 507
    .local v6, uid:I
    invoke-static {v6}, Landroid/os/UserHandle;->getUserId(I)I

    #@88
    move-result v9

    #@89
    .line 508
    .local v9, userId:I
    invoke-direct {p0, v9}, Landroid/content/pm/RegisteredServicesCache;->findOrCreateUserLocked(I)Landroid/content/pm/RegisteredServicesCache$UserServices;

    #@8c
    move-result-object v8

    #@8d
    .line 509
    .local v8, user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    iget-object v10, v8, Landroid/content/pm/RegisteredServicesCache$UserServices;->persistentServices:Ljava/util/Map;

    #@8f
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@92
    move-result-object v12

    #@93
    invoke-interface {v10, v4, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@96
    .line 512
    .end local v4           #service:Ljava/lang/Object;,"TV;"
    .end local v6           #uid:I
    .end local v7           #uidString:Ljava/lang/String;
    .end local v8           #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    .end local v9           #userId:I
    :cond_96
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_99
    .catchall {:try_start_7a .. :try_end_99} :catchall_af
    .catch Ljava/lang/Exception; {:try_start_7a .. :try_end_99} :catch_9d

    #@99
    move-result v1

    #@9a
    .line 513
    if-ne v1, v11, :cond_54

    #@9c
    goto :goto_71

    #@9d
    .line 515
    .end local v1           #eventType:I
    .end local v3           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v5           #tagName:Ljava/lang/String;
    :catch_9d
    move-exception v0

    #@9e
    .line 516
    .local v0, e:Ljava/lang/Exception;
    :try_start_9e
    const-string v10, "PackageManager"

    #@a0
    const-string v11, "Error reading persistent services, starting from scratch"

    #@a2
    invoke-static {v10, v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a5
    .catchall {:try_start_9e .. :try_end_a5} :catchall_af

    #@a5
    .line 518
    if-eqz v2, :cond_b

    #@a7
    .line 520
    :try_start_a7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_aa
    .catch Ljava/io/IOException; {:try_start_a7 .. :try_end_aa} :catch_ac

    #@aa
    goto/16 :goto_b

    #@ac
    .line 521
    :catch_ac
    move-exception v10

    #@ad
    goto/16 :goto_b

    #@af
    .line 518
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_af
    move-exception v10

    #@b0
    if-eqz v2, :cond_b5

    #@b2
    .line 520
    :try_start_b2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_b5
    .catch Ljava/io/IOException; {:try_start_b2 .. :try_end_b5} :catch_b6

    #@b5
    .line 522
    :cond_b5
    :goto_b5
    throw v10

    #@b6
    .line 521
    :catch_b6
    move-exception v11

    #@b7
    goto :goto_b5
.end method

.method private writePersistentServicesLocked()V
    .registers 11

    #@0
    .prologue
    .line 531
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    iget-object v7, p0, Landroid/content/pm/RegisteredServicesCache;->mSerializerAndParser:Landroid/content/pm/XmlSerializerAndParser;

    #@2
    if-nez v7, :cond_5

    #@4
    .line 560
    :cond_4
    :goto_4
    return-void

    #@5
    .line 534
    :cond_5
    const/4 v1, 0x0

    #@6
    .line 536
    .local v1, fos:Ljava/io/FileOutputStream;
    :try_start_6
    iget-object v7, p0, Landroid/content/pm/RegisteredServicesCache;->mPersistentServicesFile:Landroid/util/AtomicFile;

    #@8
    invoke-virtual {v7}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@b
    move-result-object v1

    #@c
    .line 537
    new-instance v4, Lcom/android/internal/util/FastXmlSerializer;

    #@e
    invoke-direct {v4}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@11
    .line 538
    .local v4, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string/jumbo v7, "utf-8"

    #@14
    invoke-interface {v4, v1, v7}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@17
    .line 539
    const/4 v7, 0x0

    #@18
    const/4 v8, 0x1

    #@19
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@1c
    move-result-object v8

    #@1d
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@20
    .line 540
    const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@22
    const/4 v8, 0x1

    #@23
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@26
    .line 541
    const/4 v7, 0x0

    #@27
    const-string/jumbo v8, "services"

    #@2a
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2d
    .line 542
    const/4 v2, 0x0

    #@2e
    .local v2, i:I
    :goto_2e
    iget-object v7, p0, Landroid/content/pm/RegisteredServicesCache;->mUserServices:Landroid/util/SparseArray;

    #@30
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    #@33
    move-result v7

    #@34
    if-ge v2, v7, :cond_95

    #@36
    .line 543
    iget-object v7, p0, Landroid/content/pm/RegisteredServicesCache;->mUserServices:Landroid/util/SparseArray;

    #@38
    invoke-virtual {v7, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@3b
    move-result-object v6

    #@3c
    check-cast v6, Landroid/content/pm/RegisteredServicesCache$UserServices;

    #@3e
    .line 544
    .local v6, user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    iget-object v7, v6, Landroid/content/pm/RegisteredServicesCache$UserServices;->persistentServices:Ljava/util/Map;

    #@40
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@43
    move-result-object v7

    #@44
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@47
    move-result-object v3

    #@48
    .local v3, i$:Ljava/util/Iterator;
    :goto_48
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@4b
    move-result v7

    #@4c
    if-eqz v7, :cond_92

    #@4e
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@51
    move-result-object v5

    #@52
    check-cast v5, Ljava/util/Map$Entry;

    #@54
    .line 545
    .local v5, service:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<TV;Ljava/lang/Integer;>;"
    const/4 v7, 0x0

    #@55
    const-string/jumbo v8, "service"

    #@58
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5b
    .line 546
    const/4 v8, 0x0

    #@5c
    const-string/jumbo v9, "uid"

    #@5f
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@62
    move-result-object v7

    #@63
    check-cast v7, Ljava/lang/Integer;

    #@65
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@68
    move-result v7

    #@69
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6c
    move-result-object v7

    #@6d
    invoke-interface {v4, v8, v9, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@70
    .line 547
    iget-object v7, p0, Landroid/content/pm/RegisteredServicesCache;->mSerializerAndParser:Landroid/content/pm/XmlSerializerAndParser;

    #@72
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@75
    move-result-object v8

    #@76
    invoke-interface {v7, v8, v4}, Landroid/content/pm/XmlSerializerAndParser;->writeAsXml(Ljava/lang/Object;Lorg/xmlpull/v1/XmlSerializer;)V

    #@79
    .line 548
    const/4 v7, 0x0

    #@7a
    const-string/jumbo v8, "service"

    #@7d
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_80
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_80} :catch_81

    #@80
    goto :goto_48

    #@81
    .line 554
    .end local v2           #i:I
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #out:Lorg/xmlpull/v1/XmlSerializer;
    .end local v5           #service:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<TV;Ljava/lang/Integer;>;"
    .end local v6           #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    :catch_81
    move-exception v0

    #@82
    .line 555
    .local v0, e1:Ljava/io/IOException;
    const-string v7, "PackageManager"

    #@84
    const-string v8, "Error writing accounts"

    #@86
    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@89
    .line 556
    if-eqz v1, :cond_4

    #@8b
    .line 557
    iget-object v7, p0, Landroid/content/pm/RegisteredServicesCache;->mPersistentServicesFile:Landroid/util/AtomicFile;

    #@8d
    invoke-virtual {v7, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@90
    goto/16 :goto_4

    #@92
    .line 542
    .end local v0           #e1:Ljava/io/IOException;
    .restart local v2       #i:I
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v4       #out:Lorg/xmlpull/v1/XmlSerializer;
    .restart local v6       #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    :cond_92
    add-int/lit8 v2, v2, 0x1

    #@94
    goto :goto_2e

    #@95
    .line 551
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v6           #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    :cond_95
    const/4 v7, 0x0

    #@96
    :try_start_96
    const-string/jumbo v8, "services"

    #@99
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9c
    .line 552
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@9f
    .line 553
    iget-object v7, p0, Landroid/content/pm/RegisteredServicesCache;->mPersistentServicesFile:Landroid/util/AtomicFile;

    #@a1
    invoke-virtual {v7, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_a4
    .catch Ljava/io/IOException; {:try_start_96 .. :try_end_a4} :catch_81

    #@a4
    goto/16 :goto_4
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .registers 11
    .parameter "fd"
    .parameter "fout"
    .parameter "args"
    .parameter "userId"

    #@0
    .prologue
    .line 169
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    iget-object v4, p0, Landroid/content/pm/RegisteredServicesCache;->mServicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 170
    :try_start_3
    invoke-direct {p0, p4}, Landroid/content/pm/RegisteredServicesCache;->findOrCreateUserLocked(I)Landroid/content/pm/RegisteredServicesCache$UserServices;

    #@6
    move-result-object v2

    #@7
    .line 171
    .local v2, user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    iget-object v3, v2, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@9
    if-eqz v3, :cond_5d

    #@b
    .line 172
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "RegisteredServicesCache: "

    #@12
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    iget-object v5, v2, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@18
    invoke-interface {v5}, Ljava/util/Map;->size()I

    #@1b
    move-result v5

    #@1c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v5, " services"

    #@22
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2d
    .line 173
    iget-object v3, v2, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@2f
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@32
    move-result-object v3

    #@33
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@36
    move-result-object v0

    #@37
    .local v0, i$:Ljava/util/Iterator;
    :goto_37
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@3a
    move-result v3

    #@3b
    if-eqz v3, :cond_62

    #@3d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@40
    move-result-object v1

    #@41
    check-cast v1, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@43
    .line 174
    .local v1, info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<*>;"
    new-instance v3, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v5, "  "

    #@4a
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@59
    goto :goto_37

    #@5a
    .line 179
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<*>;"
    .end local v2           #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    :catchall_5a
    move-exception v3

    #@5b
    monitor-exit v4
    :try_end_5c
    .catchall {:try_start_3 .. :try_end_5c} :catchall_5a

    #@5c
    throw v3

    #@5d
    .line 177
    .restart local v2       #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    :cond_5d
    :try_start_5d
    const-string v3, "RegisteredServicesCache: services not loaded"

    #@5f
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@62
    .line 179
    :cond_62
    monitor-exit v4
    :try_end_63
    .catchall {:try_start_5d .. :try_end_63} :catchall_5a

    #@63
    .line 180
    return-void
.end method

.method public getAllServices(I)Ljava/util/Collection;
    .registers 6
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/pm/RegisteredServicesCache$ServiceInfo",
            "<TV;>;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 263
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    iget-object v2, p0, Landroid/content/pm/RegisteredServicesCache;->mServicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 265
    :try_start_3
    invoke-direct {p0, p1}, Landroid/content/pm/RegisteredServicesCache;->findOrCreateUserLocked(I)Landroid/content/pm/RegisteredServicesCache$UserServices;

    #@6
    move-result-object v0

    #@7
    .line 266
    .local v0, user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    iget-object v1, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@9
    if-nez v1, :cond_e

    #@b
    .line 267
    invoke-direct {p0, p1}, Landroid/content/pm/RegisteredServicesCache;->generateServicesMap(I)V

    #@e
    .line 269
    :cond_e
    new-instance v1, Ljava/util/ArrayList;

    #@10
    iget-object v3, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@12
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@15
    move-result-object v3

    #@16
    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@19
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    #@1c
    move-result-object v1

    #@1d
    monitor-exit v2

    #@1e
    return-object v1

    #@1f
    .line 271
    .end local v0           #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v1
.end method

.method public getListener()Landroid/content/pm/RegisteredServicesCacheListener;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/content/pm/RegisteredServicesCacheListener",
            "<TV;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 183
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    monitor-enter p0

    #@1
    .line 184
    :try_start_1
    iget-object v0, p0, Landroid/content/pm/RegisteredServicesCache;->mListener:Landroid/content/pm/RegisteredServicesCacheListener;

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    .line 185
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getServiceInfo(Ljava/lang/Object;I)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;
    .registers 6
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;I)",
            "Landroid/content/pm/RegisteredServicesCache$ServiceInfo",
            "<TV;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 248
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    .local p1, type:Ljava/lang/Object;,"TV;"
    iget-object v2, p0, Landroid/content/pm/RegisteredServicesCache;->mServicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 250
    :try_start_3
    invoke-direct {p0, p2}, Landroid/content/pm/RegisteredServicesCache;->findOrCreateUserLocked(I)Landroid/content/pm/RegisteredServicesCache$UserServices;

    #@6
    move-result-object v0

    #@7
    .line 251
    .local v0, user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    iget-object v1, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@9
    if-nez v1, :cond_e

    #@b
    .line 252
    invoke-direct {p0, p2}, Landroid/content/pm/RegisteredServicesCache;->generateServicesMap(I)V

    #@e
    .line 254
    :cond_e
    iget-object v1, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@10
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@16
    monitor-exit v2

    #@17
    return-object v1

    #@18
    .line 255
    .end local v0           #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    :catchall_18
    move-exception v1

    #@19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v1
.end method

.method public invalidateCache(I)V
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 162
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    iget-object v2, p0, Landroid/content/pm/RegisteredServicesCache;->mServicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 163
    :try_start_3
    invoke-direct {p0, p1}, Landroid/content/pm/RegisteredServicesCache;->findOrCreateUserLocked(I)Landroid/content/pm/RegisteredServicesCache$UserServices;

    #@6
    move-result-object v0

    #@7
    .line 164
    .local v0, user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    const/4 v1, 0x0

    #@8
    iput-object v1, v0, Landroid/content/pm/RegisteredServicesCache$UserServices;->services:Ljava/util/Map;

    #@a
    .line 165
    monitor-exit v2

    #@b
    .line 166
    return-void

    #@c
    .line 165
    .end local v0           #user:Landroid/content/pm/RegisteredServicesCache$UserServices;,"Landroid/content/pm/RegisteredServicesCache$UserServices<TV;>;"
    :catchall_c
    move-exception v1

    #@d
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    #@e
    throw v1
.end method

.method public abstract parseServiceAttributes(Landroid/content/res/Resources;Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/lang/String;",
            "Landroid/util/AttributeSet;",
            ")TV;"
        }
    .end annotation
.end method

.method public setListener(Landroid/content/pm/RegisteredServicesCacheListener;Landroid/os/Handler;)V
    .registers 4
    .parameter
    .parameter "handler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/RegisteredServicesCacheListener",
            "<TV;>;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 189
    .local p0, this:Landroid/content/pm/RegisteredServicesCache;,"Landroid/content/pm/RegisteredServicesCache<TV;>;"
    .local p1, listener:Landroid/content/pm/RegisteredServicesCacheListener;,"Landroid/content/pm/RegisteredServicesCacheListener<TV;>;"
    if-nez p2, :cond_d

    #@2
    .line 190
    new-instance p2, Landroid/os/Handler;

    #@4
    .end local p2
    iget-object v0, p0, Landroid/content/pm/RegisteredServicesCache;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@9
    move-result-object v0

    #@a
    invoke-direct {p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@d
    .line 192
    .restart local p2
    :cond_d
    monitor-enter p0

    #@e
    .line 193
    :try_start_e
    iput-object p2, p0, Landroid/content/pm/RegisteredServicesCache;->mHandler:Landroid/os/Handler;

    #@10
    .line 194
    iput-object p1, p0, Landroid/content/pm/RegisteredServicesCache;->mListener:Landroid/content/pm/RegisteredServicesCacheListener;

    #@12
    .line 195
    monitor-exit p0

    #@13
    .line 196
    return-void

    #@14
    .line 195
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_e .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method
