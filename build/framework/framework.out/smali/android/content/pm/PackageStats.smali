.class public Landroid/content/pm/PackageStats;
.super Ljava/lang/Object;
.source "PackageStats.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/PackageStats;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public cacheSize:J

.field public codeSize:J

.field public dataSize:J

.field public externalCacheSize:J

.field public externalCodeSize:J

.field public externalDataSize:J

.field public externalMediaSize:J

.field public externalObbSize:J

.field public packageName:Ljava/lang/String;

.field public userHandle:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 71
    new-instance v0, Landroid/content/pm/PackageStats$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/PackageStats$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/PackageStats;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/PackageStats;)V
    .registers 4
    .parameter "pStats"

    #@0
    .prologue
    .line 147
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 148
    iget-object v0, p1, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@5
    iput-object v0, p0, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@7
    .line 149
    iget v0, p1, Landroid/content/pm/PackageStats;->userHandle:I

    #@9
    iput v0, p0, Landroid/content/pm/PackageStats;->userHandle:I

    #@b
    .line 150
    iget-wide v0, p1, Landroid/content/pm/PackageStats;->codeSize:J

    #@d
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->codeSize:J

    #@f
    .line 151
    iget-wide v0, p1, Landroid/content/pm/PackageStats;->dataSize:J

    #@11
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->dataSize:J

    #@13
    .line 152
    iget-wide v0, p1, Landroid/content/pm/PackageStats;->cacheSize:J

    #@15
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->cacheSize:J

    #@17
    .line 153
    iget-wide v0, p1, Landroid/content/pm/PackageStats;->externalCodeSize:J

    #@19
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->externalCodeSize:J

    #@1b
    .line 154
    iget-wide v0, p1, Landroid/content/pm/PackageStats;->externalDataSize:J

    #@1d
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->externalDataSize:J

    #@1f
    .line 155
    iget-wide v0, p1, Landroid/content/pm/PackageStats;->externalCacheSize:J

    #@21
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->externalCacheSize:J

    #@23
    .line 156
    iget-wide v0, p1, Landroid/content/pm/PackageStats;->externalMediaSize:J

    #@25
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->externalMediaSize:J

    #@27
    .line 157
    iget-wide v0, p1, Landroid/content/pm/PackageStats;->externalObbSize:J

    #@29
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->externalObbSize:J

    #@2b
    .line 158
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 134
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@9
    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/content/pm/PackageStats;->userHandle:I

    #@f
    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@12
    move-result-wide v0

    #@13
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->codeSize:J

    #@15
    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@18
    move-result-wide v0

    #@19
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->dataSize:J

    #@1b
    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@1e
    move-result-wide v0

    #@1f
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->cacheSize:J

    #@21
    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@24
    move-result-wide v0

    #@25
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->externalCodeSize:J

    #@27
    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@2a
    move-result-wide v0

    #@2b
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->externalDataSize:J

    #@2d
    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@30
    move-result-wide v0

    #@31
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->externalCacheSize:J

    #@33
    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@36
    move-result-wide v0

    #@37
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->externalMediaSize:J

    #@39
    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@3c
    move-result-wide v0

    #@3d
    iput-wide v0, p0, Landroid/content/pm/PackageStats;->externalObbSize:J

    #@3f
    .line 145
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "pkgName"

    #@0
    .prologue
    .line 123
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 124
    iput-object p1, p0, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@5
    .line 125
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@8
    move-result v0

    #@9
    iput v0, p0, Landroid/content/pm/PackageStats;->userHandle:I

    #@b
    .line 126
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter "pkgName"
    .parameter "userHandle"

    #@0
    .prologue
    .line 129
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 130
    iput-object p1, p0, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@5
    .line 131
    iput p2, p0, Landroid/content/pm/PackageStats;->userHandle:I

    #@7
    .line 132
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 161
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const-string v1, "PackageStats{"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 84
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@c
    move-result v1

    #@d
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 85
    const-string v1, " "

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 86
    iget-object v1, p0, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 87
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->codeSize:J

    #@20
    cmp-long v1, v1, v3

    #@22
    if-eqz v1, :cond_2e

    #@24
    .line 88
    const-string v1, " code="

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    .line 89
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->codeSize:J

    #@2b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2e
    .line 91
    :cond_2e
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->dataSize:J

    #@30
    cmp-long v1, v1, v3

    #@32
    if-eqz v1, :cond_3e

    #@34
    .line 92
    const-string v1, " data="

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    .line 93
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->dataSize:J

    #@3b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3e
    .line 95
    :cond_3e
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->cacheSize:J

    #@40
    cmp-long v1, v1, v3

    #@42
    if-eqz v1, :cond_4e

    #@44
    .line 96
    const-string v1, " cache="

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    .line 97
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->cacheSize:J

    #@4b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4e
    .line 99
    :cond_4e
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->externalCodeSize:J

    #@50
    cmp-long v1, v1, v3

    #@52
    if-eqz v1, :cond_5e

    #@54
    .line 100
    const-string v1, " extCode="

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    .line 101
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->externalCodeSize:J

    #@5b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5e
    .line 103
    :cond_5e
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->externalDataSize:J

    #@60
    cmp-long v1, v1, v3

    #@62
    if-eqz v1, :cond_6e

    #@64
    .line 104
    const-string v1, " extData="

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    .line 105
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->externalDataSize:J

    #@6b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6e
    .line 107
    :cond_6e
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->externalCacheSize:J

    #@70
    cmp-long v1, v1, v3

    #@72
    if-eqz v1, :cond_7e

    #@74
    .line 108
    const-string v1, " extCache="

    #@76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    .line 109
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->externalCacheSize:J

    #@7b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@7e
    .line 111
    :cond_7e
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->externalMediaSize:J

    #@80
    cmp-long v1, v1, v3

    #@82
    if-eqz v1, :cond_8e

    #@84
    .line 112
    const-string v1, " media="

    #@86
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    .line 113
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->externalMediaSize:J

    #@8b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8e
    .line 115
    :cond_8e
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->externalObbSize:J

    #@90
    cmp-long v1, v1, v3

    #@92
    if-eqz v1, :cond_9e

    #@94
    .line 116
    const-string v1, " obb="

    #@96
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    .line 117
    iget-wide v1, p0, Landroid/content/pm/PackageStats;->externalObbSize:J

    #@9b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@9e
    .line 119
    :cond_9e
    const-string/jumbo v1, "}"

    #@a1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    .line 120
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v1

    #@a8
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 166
    iget v0, p0, Landroid/content/pm/PackageStats;->userHandle:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 167
    iget-wide v0, p0, Landroid/content/pm/PackageStats;->codeSize:J

    #@c
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@f
    .line 168
    iget-wide v0, p0, Landroid/content/pm/PackageStats;->dataSize:J

    #@11
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@14
    .line 169
    iget-wide v0, p0, Landroid/content/pm/PackageStats;->cacheSize:J

    #@16
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@19
    .line 170
    iget-wide v0, p0, Landroid/content/pm/PackageStats;->externalCodeSize:J

    #@1b
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@1e
    .line 171
    iget-wide v0, p0, Landroid/content/pm/PackageStats;->externalDataSize:J

    #@20
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@23
    .line 172
    iget-wide v0, p0, Landroid/content/pm/PackageStats;->externalCacheSize:J

    #@25
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@28
    .line 173
    iget-wide v0, p0, Landroid/content/pm/PackageStats;->externalMediaSize:J

    #@2a
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@2d
    .line 174
    iget-wide v0, p0, Landroid/content/pm/PackageStats;->externalObbSize:J

    #@2f
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@32
    .line 175
    return-void
.end method
