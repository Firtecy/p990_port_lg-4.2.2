.class public Landroid/content/pm/ConfigurationInfo;
.super Ljava/lang/Object;
.source "ConfigurationInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/ConfigurationInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final GL_ES_VERSION_UNDEFINED:I = 0x0

.field public static final INPUT_FEATURE_FIVE_WAY_NAV:I = 0x2

.field public static final INPUT_FEATURE_HARD_KEYBOARD:I = 0x1


# instance fields
.field public reqGlEsVersion:I

.field public reqInputFeatures:I

.field public reqKeyboardType:I

.field public reqNavigation:I

.field public reqTouchScreen:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 116
    new-instance v0, Landroid/content/pm/ConfigurationInfo$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/ConfigurationInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/ConfigurationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 83
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 71
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@6
    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/ConfigurationInfo;)V
    .registers 3
    .parameter "orig"

    #@0
    .prologue
    .line 86
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 71
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@6
    .line 87
    iget v0, p1, Landroid/content/pm/ConfigurationInfo;->reqTouchScreen:I

    #@8
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqTouchScreen:I

    #@a
    .line 88
    iget v0, p1, Landroid/content/pm/ConfigurationInfo;->reqKeyboardType:I

    #@c
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqKeyboardType:I

    #@e
    .line 89
    iget v0, p1, Landroid/content/pm/ConfigurationInfo;->reqNavigation:I

    #@10
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqNavigation:I

    #@12
    .line 90
    iget v0, p1, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@14
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@16
    .line 91
    iget v0, p1, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    #@18
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    #@1a
    .line 92
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 126
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 71
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@6
    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqTouchScreen:I

    #@c
    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqKeyboardType:I

    #@12
    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqNavigation:I

    #@18
    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@1e
    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v0

    #@22
    iput v0, p0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    #@24
    .line 132
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/ConfigurationInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/content/pm/ConfigurationInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 105
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getGlEsVersion()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 141
    iget v2, p0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    #@2
    const/high16 v3, -0x1

    #@4
    and-int/2addr v2, v3

    #@5
    shr-int/lit8 v0, v2, 0x10

    #@7
    .line 142
    .local v0, major:I
    iget v2, p0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    #@9
    const v3, 0xffff

    #@c
    and-int v1, v2, v3

    #@e
    .line 143
    .local v1, minor:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, "."

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ConfigurationInfo{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " touchscreen = "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    iget v1, p0, Landroid/content/pm/ConfigurationInfo;->reqTouchScreen:I

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string v1, " inputMethod = "

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    iget v1, p0, Landroid/content/pm/ConfigurationInfo;->reqKeyboardType:I

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, " navigation = "

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    iget v1, p0, Landroid/content/pm/ConfigurationInfo;->reqNavigation:I

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, " reqInputFeatures = "

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    iget v1, p0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    const-string v1, " reqGlEsVersion = "

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    iget v1, p0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    const-string/jumbo v1, "}"

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v0

    #@5a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v0

    #@5e
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 109
    iget v0, p0, Landroid/content/pm/ConfigurationInfo;->reqTouchScreen:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 110
    iget v0, p0, Landroid/content/pm/ConfigurationInfo;->reqKeyboardType:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 111
    iget v0, p0, Landroid/content/pm/ConfigurationInfo;->reqNavigation:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 112
    iget v0, p0, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 113
    iget v0, p0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 114
    return-void
.end method
