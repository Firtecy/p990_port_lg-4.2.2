.class public final Landroid/content/pm/PackageParser$Package;
.super Ljava/lang/Object;
.source "PackageParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/pm/PackageParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Package"
.end annotation


# instance fields
.field public final activities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PackageParser$Activity;",
            ">;"
        }
    .end annotation
.end field

.field public final applicationInfo:Landroid/content/pm/ApplicationInfo;

.field public final configPreferences:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/ConfigurationInfo;",
            ">;"
        }
    .end annotation
.end field

.field public installLocation:I

.field public final instrumentation:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PackageParser$Instrumentation;",
            ">;"
        }
    .end annotation
.end field

.field public mAdoptPermissions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mAppMetaData:Landroid/os/Bundle;

.field public mDidDexOpt:Z

.field public mExtras:Ljava/lang/Object;

.field public mOperationPending:Z

.field public mOriginalPackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mOverlayPath:Ljava/lang/String;

.field public mPath:Ljava/lang/String;

.field public mPreferredOrder:I

.field public mRealPackage:Ljava/lang/String;

.field public mScanPath:Ljava/lang/String;

.field public mSharedUserId:Ljava/lang/String;

.field public mSharedUserLabel:I

.field public mSignatures:[Landroid/content/pm/Signature;

.field public mVersionCode:I

.field public mVersionName:Ljava/lang/String;

.field public manifestDigest:Landroid/content/pm/ManifestDigest;

.field public packageName:Ljava/lang/String;

.field public final permissionGroups:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PackageParser$PermissionGroup;",
            ">;"
        }
    .end annotation
.end field

.field public final permissions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PackageParser$Permission;",
            ">;"
        }
    .end annotation
.end field

.field public protectedBroadcasts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final providers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PackageParser$Provider;",
            ">;"
        }
    .end annotation
.end field

.field public final receivers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PackageParser$Activity;",
            ">;"
        }
    .end annotation
.end field

.field public reqFeatures:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/FeatureInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final requestedPermissions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final requestedPermissionsRequired:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final services:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PackageParser$Service;",
            ">;"
        }
    .end annotation
.end field

.field public usesLibraries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public usesLibraryFiles:[Ljava/lang/String;

.field public usesOptionalLibraries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 5
    .parameter "_name"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 3358
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 3267
    new-instance v0, Landroid/content/pm/ApplicationInfo;

    #@7
    invoke-direct {v0}, Landroid/content/pm/ApplicationInfo;-><init>()V

    #@a
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@c
    .line 3269
    new-instance v0, Ljava/util/ArrayList;

    #@e
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@11
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->permissions:Ljava/util/ArrayList;

    #@13
    .line 3270
    new-instance v0, Ljava/util/ArrayList;

    #@15
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@18
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->permissionGroups:Ljava/util/ArrayList;

    #@1a
    .line 3271
    new-instance v0, Ljava/util/ArrayList;

    #@1c
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@1f
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@21
    .line 3272
    new-instance v0, Ljava/util/ArrayList;

    #@23
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@26
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->receivers:Ljava/util/ArrayList;

    #@28
    .line 3273
    new-instance v0, Ljava/util/ArrayList;

    #@2a
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@2d
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->providers:Ljava/util/ArrayList;

    #@2f
    .line 3274
    new-instance v0, Ljava/util/ArrayList;

    #@31
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@34
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->services:Ljava/util/ArrayList;

    #@36
    .line 3275
    new-instance v0, Ljava/util/ArrayList;

    #@38
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@3b
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->instrumentation:Ljava/util/ArrayList;

    #@3d
    .line 3277
    new-instance v0, Ljava/util/ArrayList;

    #@3f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@42
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->requestedPermissions:Ljava/util/ArrayList;

    #@44
    .line 3278
    new-instance v0, Ljava/util/ArrayList;

    #@46
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@49
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->requestedPermissionsRequired:Ljava/util/ArrayList;

    #@4b
    .line 3282
    iput-object v2, p0, Landroid/content/pm/PackageParser$Package;->usesLibraries:Ljava/util/ArrayList;

    #@4d
    .line 3283
    iput-object v2, p0, Landroid/content/pm/PackageParser$Package;->usesOptionalLibraries:Ljava/util/ArrayList;

    #@4f
    .line 3284
    iput-object v2, p0, Landroid/content/pm/PackageParser$Package;->usesLibraryFiles:[Ljava/lang/String;

    #@51
    .line 3286
    iput-object v2, p0, Landroid/content/pm/PackageParser$Package;->mOriginalPackages:Ljava/util/ArrayList;

    #@53
    .line 3287
    iput-object v2, p0, Landroid/content/pm/PackageParser$Package;->mRealPackage:Ljava/lang/String;

    #@55
    .line 3288
    iput-object v2, p0, Landroid/content/pm/PackageParser$Package;->mAdoptPermissions:Ljava/util/ArrayList;

    #@57
    .line 3291
    iput-object v2, p0, Landroid/content/pm/PackageParser$Package;->mAppMetaData:Landroid/os/Bundle;

    #@59
    .line 3313
    iput v1, p0, Landroid/content/pm/PackageParser$Package;->mPreferredOrder:I

    #@5b
    .line 3342
    new-instance v0, Ljava/util/ArrayList;

    #@5d
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@60
    iput-object v0, p0, Landroid/content/pm/PackageParser$Package;->configPreferences:Ljava/util/ArrayList;

    #@62
    .line 3348
    iput-object v2, p0, Landroid/content/pm/PackageParser$Package;->reqFeatures:Ljava/util/ArrayList;

    #@64
    .line 3359
    iput-object p1, p0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@66
    .line 3360
    iget-object v0, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@68
    iput-object p1, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@6a
    .line 3361
    iget-object v0, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@6c
    const/4 v1, -0x1

    #@6d
    iput v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@6f
    .line 3362
    return-void
.end method


# virtual methods
.method public hasComponentClassName(Ljava/lang/String;)Z
    .registers 5
    .parameter "name"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 3391
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    add-int/lit8 v0, v1, -0x1

    #@9
    .local v0, i:I
    :goto_9
    if-ltz v0, :cond_20

    #@b
    .line 3392
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/content/pm/PackageParser$Activity;

    #@13
    iget-object v1, v1, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@15
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_1d

    #@1b
    move v1, v2

    #@1c
    .line 3416
    :goto_1c
    return v1

    #@1d
    .line 3391
    :cond_1d
    add-int/lit8 v0, v0, -0x1

    #@1f
    goto :goto_9

    #@20
    .line 3396
    :cond_20
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->receivers:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@25
    move-result v1

    #@26
    add-int/lit8 v0, v1, -0x1

    #@28
    :goto_28
    if-ltz v0, :cond_3f

    #@2a
    .line 3397
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->receivers:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2f
    move-result-object v1

    #@30
    check-cast v1, Landroid/content/pm/PackageParser$Activity;

    #@32
    iget-object v1, v1, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@34
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v1

    #@38
    if-eqz v1, :cond_3c

    #@3a
    move v1, v2

    #@3b
    .line 3398
    goto :goto_1c

    #@3c
    .line 3396
    :cond_3c
    add-int/lit8 v0, v0, -0x1

    #@3e
    goto :goto_28

    #@3f
    .line 3401
    :cond_3f
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->providers:Ljava/util/ArrayList;

    #@41
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@44
    move-result v1

    #@45
    add-int/lit8 v0, v1, -0x1

    #@47
    :goto_47
    if-ltz v0, :cond_5e

    #@49
    .line 3402
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->providers:Ljava/util/ArrayList;

    #@4b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4e
    move-result-object v1

    #@4f
    check-cast v1, Landroid/content/pm/PackageParser$Provider;

    #@51
    iget-object v1, v1, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@53
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v1

    #@57
    if-eqz v1, :cond_5b

    #@59
    move v1, v2

    #@5a
    .line 3403
    goto :goto_1c

    #@5b
    .line 3401
    :cond_5b
    add-int/lit8 v0, v0, -0x1

    #@5d
    goto :goto_47

    #@5e
    .line 3406
    :cond_5e
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->services:Ljava/util/ArrayList;

    #@60
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@63
    move-result v1

    #@64
    add-int/lit8 v0, v1, -0x1

    #@66
    :goto_66
    if-ltz v0, :cond_7d

    #@68
    .line 3407
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->services:Ljava/util/ArrayList;

    #@6a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6d
    move-result-object v1

    #@6e
    check-cast v1, Landroid/content/pm/PackageParser$Service;

    #@70
    iget-object v1, v1, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@72
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@75
    move-result v1

    #@76
    if-eqz v1, :cond_7a

    #@78
    move v1, v2

    #@79
    .line 3408
    goto :goto_1c

    #@7a
    .line 3406
    :cond_7a
    add-int/lit8 v0, v0, -0x1

    #@7c
    goto :goto_66

    #@7d
    .line 3411
    :cond_7d
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->instrumentation:Ljava/util/ArrayList;

    #@7f
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@82
    move-result v1

    #@83
    add-int/lit8 v0, v1, -0x1

    #@85
    :goto_85
    if-ltz v0, :cond_9c

    #@87
    .line 3412
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->instrumentation:Ljava/util/ArrayList;

    #@89
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8c
    move-result-object v1

    #@8d
    check-cast v1, Landroid/content/pm/PackageParser$Instrumentation;

    #@8f
    iget-object v1, v1, Landroid/content/pm/PackageParser$Component;->className:Ljava/lang/String;

    #@91
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v1

    #@95
    if-eqz v1, :cond_99

    #@97
    move v1, v2

    #@98
    .line 3413
    goto :goto_1c

    #@99
    .line 3411
    :cond_99
    add-int/lit8 v0, v0, -0x1

    #@9b
    goto :goto_85

    #@9c
    .line 3416
    :cond_9c
    const/4 v1, 0x0

    #@9d
    goto/16 :goto_1c
.end method

.method public setPackageName(Ljava/lang/String;)V
    .registers 4
    .parameter "newName"

    #@0
    .prologue
    .line 3365
    iput-object p1, p0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@2
    .line 3366
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4
    iput-object p1, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@6
    .line 3367
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->permissions:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v1

    #@c
    add-int/lit8 v0, v1, -0x1

    #@e
    .local v0, i:I
    :goto_e
    if-ltz v0, :cond_1e

    #@10
    .line 3368
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->permissions:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/content/pm/PackageParser$Permission;

    #@18
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageParser$Permission;->setPackageName(Ljava/lang/String;)V

    #@1b
    .line 3367
    add-int/lit8 v0, v0, -0x1

    #@1d
    goto :goto_e

    #@1e
    .line 3370
    :cond_1e
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->permissionGroups:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@23
    move-result v1

    #@24
    add-int/lit8 v0, v1, -0x1

    #@26
    :goto_26
    if-ltz v0, :cond_36

    #@28
    .line 3371
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->permissionGroups:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v1

    #@2e
    check-cast v1, Landroid/content/pm/PackageParser$PermissionGroup;

    #@30
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageParser$PermissionGroup;->setPackageName(Ljava/lang/String;)V

    #@33
    .line 3370
    add-int/lit8 v0, v0, -0x1

    #@35
    goto :goto_26

    #@36
    .line 3373
    :cond_36
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v1

    #@3c
    add-int/lit8 v0, v1, -0x1

    #@3e
    :goto_3e
    if-ltz v0, :cond_4e

    #@40
    .line 3374
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->activities:Ljava/util/ArrayList;

    #@42
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@45
    move-result-object v1

    #@46
    check-cast v1, Landroid/content/pm/PackageParser$Activity;

    #@48
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageParser$Activity;->setPackageName(Ljava/lang/String;)V

    #@4b
    .line 3373
    add-int/lit8 v0, v0, -0x1

    #@4d
    goto :goto_3e

    #@4e
    .line 3376
    :cond_4e
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->receivers:Ljava/util/ArrayList;

    #@50
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@53
    move-result v1

    #@54
    add-int/lit8 v0, v1, -0x1

    #@56
    :goto_56
    if-ltz v0, :cond_66

    #@58
    .line 3377
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->receivers:Ljava/util/ArrayList;

    #@5a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5d
    move-result-object v1

    #@5e
    check-cast v1, Landroid/content/pm/PackageParser$Activity;

    #@60
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageParser$Activity;->setPackageName(Ljava/lang/String;)V

    #@63
    .line 3376
    add-int/lit8 v0, v0, -0x1

    #@65
    goto :goto_56

    #@66
    .line 3379
    :cond_66
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->providers:Ljava/util/ArrayList;

    #@68
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6b
    move-result v1

    #@6c
    add-int/lit8 v0, v1, -0x1

    #@6e
    :goto_6e
    if-ltz v0, :cond_7e

    #@70
    .line 3380
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->providers:Ljava/util/ArrayList;

    #@72
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@75
    move-result-object v1

    #@76
    check-cast v1, Landroid/content/pm/PackageParser$Provider;

    #@78
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageParser$Provider;->setPackageName(Ljava/lang/String;)V

    #@7b
    .line 3379
    add-int/lit8 v0, v0, -0x1

    #@7d
    goto :goto_6e

    #@7e
    .line 3382
    :cond_7e
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->services:Ljava/util/ArrayList;

    #@80
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@83
    move-result v1

    #@84
    add-int/lit8 v0, v1, -0x1

    #@86
    :goto_86
    if-ltz v0, :cond_96

    #@88
    .line 3383
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->services:Ljava/util/ArrayList;

    #@8a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8d
    move-result-object v1

    #@8e
    check-cast v1, Landroid/content/pm/PackageParser$Service;

    #@90
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageParser$Service;->setPackageName(Ljava/lang/String;)V

    #@93
    .line 3382
    add-int/lit8 v0, v0, -0x1

    #@95
    goto :goto_86

    #@96
    .line 3385
    :cond_96
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->instrumentation:Ljava/util/ArrayList;

    #@98
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@9b
    move-result v1

    #@9c
    add-int/lit8 v0, v1, -0x1

    #@9e
    :goto_9e
    if-ltz v0, :cond_ae

    #@a0
    .line 3386
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->instrumentation:Ljava/util/ArrayList;

    #@a2
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a5
    move-result-object v1

    #@a6
    check-cast v1, Landroid/content/pm/PackageParser$Instrumentation;

    #@a8
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageParser$Instrumentation;->setPackageName(Ljava/lang/String;)V

    #@ab
    .line 3385
    add-int/lit8 v0, v0, -0x1

    #@ad
    goto :goto_9e

    #@ae
    .line 3388
    :cond_ae
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 3420
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Package{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    iget-object v1, p0, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string/jumbo v1, "}"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    return-object v0
.end method
