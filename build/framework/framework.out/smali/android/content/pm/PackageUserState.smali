.class public Landroid/content/pm/PackageUserState;
.super Ljava/lang/Object;
.source "PackageUserState.java"


# instance fields
.field public disabledComponents:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public enabled:I

.field public enabledComponents:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public installed:Z

.field public notLaunched:Z

.field public stopped:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/content/pm/PackageUserState;->installed:Z

    #@6
    .line 38
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/content/pm/PackageUserState;->enabled:I

    #@9
    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/PackageUserState;)V
    .registers 5
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 42
    iget-boolean v0, p1, Landroid/content/pm/PackageUserState;->installed:Z

    #@6
    iput-boolean v0, p0, Landroid/content/pm/PackageUserState;->installed:Z

    #@8
    .line 43
    iget-boolean v0, p1, Landroid/content/pm/PackageUserState;->stopped:Z

    #@a
    iput-boolean v0, p0, Landroid/content/pm/PackageUserState;->stopped:Z

    #@c
    .line 44
    iget-boolean v0, p1, Landroid/content/pm/PackageUserState;->notLaunched:Z

    #@e
    iput-boolean v0, p0, Landroid/content/pm/PackageUserState;->notLaunched:Z

    #@10
    .line 45
    iget v0, p1, Landroid/content/pm/PackageUserState;->enabled:I

    #@12
    iput v0, p0, Landroid/content/pm/PackageUserState;->enabled:I

    #@14
    .line 46
    iget-object v0, p1, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@16
    if-eqz v0, :cond_2f

    #@18
    new-instance v0, Ljava/util/HashSet;

    #@1a
    iget-object v2, p1, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@1c
    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@1f
    :goto_1f
    iput-object v0, p0, Landroid/content/pm/PackageUserState;->disabledComponents:Ljava/util/HashSet;

    #@21
    .line 48
    iget-object v0, p1, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@23
    if-eqz v0, :cond_2c

    #@25
    new-instance v1, Ljava/util/HashSet;

    #@27
    iget-object v0, p1, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@29
    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@2c
    :cond_2c
    iput-object v1, p0, Landroid/content/pm/PackageUserState;->enabledComponents:Ljava/util/HashSet;

    #@2e
    .line 50
    return-void

    #@2f
    :cond_2f
    move-object v0, v1

    #@30
    .line 46
    goto :goto_1f
.end method
