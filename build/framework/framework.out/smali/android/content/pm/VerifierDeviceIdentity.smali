.class public Landroid/content/pm/VerifierDeviceIdentity;
.super Ljava/lang/Object;
.source "VerifierDeviceIdentity.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/VerifierDeviceIdentity;",
            ">;"
        }
    .end annotation
.end field

.field private static final ENCODE:[C = null

.field private static final GROUP_SIZE:I = 0x4

.field private static final LONG_SIZE:I = 0xd

.field private static final SEPARATOR:C = '-'


# instance fields
.field private final mIdentity:J

.field private final mIdentityString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 94
    const/16 v0, 0x20

    #@2
    new-array v0, v0, [C

    #@4
    fill-array-data v0, :array_12

    #@7
    sput-object v0, Landroid/content/pm/VerifierDeviceIdentity;->ENCODE:[C

    #@9
    .line 230
    new-instance v0, Landroid/content/pm/VerifierDeviceIdentity$1;

    #@b
    invoke-direct {v0}, Landroid/content/pm/VerifierDeviceIdentity$1;-><init>()V

    #@e
    sput-object v0, Landroid/content/pm/VerifierDeviceIdentity;->CREATOR:Landroid/os/Parcelable$Creator;

    #@10
    return-void

    #@11
    .line 94
    nop

    #@12
    :array_12
    .array-data 0x2
        0x41t 0x0t
        0x42t 0x0t
        0x43t 0x0t
        0x44t 0x0t
        0x45t 0x0t
        0x46t 0x0t
        0x47t 0x0t
        0x48t 0x0t
        0x49t 0x0t
        0x4at 0x0t
        0x4bt 0x0t
        0x4ct 0x0t
        0x4dt 0x0t
        0x4et 0x0t
        0x4ft 0x0t
        0x50t 0x0t
        0x51t 0x0t
        0x52t 0x0t
        0x53t 0x0t
        0x54t 0x0t
        0x55t 0x0t
        0x56t 0x0t
        0x57t 0x0t
        0x58t 0x0t
        0x59t 0x0t
        0x5at 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
    .end array-data
.end method

.method public constructor <init>(J)V
    .registers 4
    .parameter "identity"

    #@0
    .prologue
    .line 59
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 60
    iput-wide p1, p0, Landroid/content/pm/VerifierDeviceIdentity;->mIdentity:J

    #@5
    .line 61
    invoke-static {p1, p2}, Landroid/content/pm/VerifierDeviceIdentity;->encodeBase32(J)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/content/pm/VerifierDeviceIdentity;->mIdentityString:Ljava/lang/String;

    #@b
    .line 62
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@6
    move-result-wide v0

    #@7
    .line 67
    .local v0, identity:J
    iput-wide v0, p0, Landroid/content/pm/VerifierDeviceIdentity;->mIdentity:J

    #@9
    .line 68
    invoke-static {v0, v1}, Landroid/content/pm/VerifierDeviceIdentity;->encodeBase32(J)Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    iput-object v2, p0, Landroid/content/pm/VerifierDeviceIdentity;->mIdentityString:Ljava/lang/String;

    #@f
    .line 69
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/VerifierDeviceIdentity$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/content/pm/VerifierDeviceIdentity;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private static final decodeBase32([B)J
    .registers 13
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v11, 0xd

    #@2
    .line 138
    const-wide/16 v4, 0x0

    #@4
    .line 139
    .local v4, output:J
    const/4 v3, 0x0

    #@5
    .line 141
    .local v3, numParsed:I
    array-length v0, p0

    #@6
    .line 142
    .local v0, N:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v0, :cond_7b

    #@9
    .line 143
    aget-byte v1, p0, v2

    #@b
    .line 150
    .local v1, group:I
    const/16 v7, 0x41

    #@d
    if-gt v7, v1, :cond_2c

    #@f
    const/16 v7, 0x5a

    #@11
    if-gt v1, v7, :cond_2c

    #@13
    .line 151
    add-int/lit8 v6, v1, -0x41

    #@15
    .line 169
    .local v6, value:I
    :goto_15
    const/4 v7, 0x5

    #@16
    shl-long v7, v4, v7

    #@18
    int-to-long v9, v6

    #@19
    or-long v4, v7, v9

    #@1b
    .line 170
    add-int/lit8 v3, v3, 0x1

    #@1d
    .line 172
    const/4 v7, 0x1

    #@1e
    if-ne v3, v7, :cond_70

    #@20
    .line 173
    and-int/lit8 v7, v6, 0xf

    #@22
    if-eq v7, v6, :cond_3b

    #@24
    .line 174
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@26
    const-string v8, "illegal start character; will overflow"

    #@28
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v7

    #@2c
    .line 152
    .end local v6           #value:I
    :cond_2c
    const/16 v7, 0x32

    #@2e
    if-gt v7, v1, :cond_37

    #@30
    const/16 v7, 0x37

    #@32
    if-gt v1, v7, :cond_37

    #@34
    .line 153
    add-int/lit8 v6, v1, -0x18

    #@36
    .restart local v6       #value:I
    goto :goto_15

    #@37
    .line 154
    .end local v6           #value:I
    :cond_37
    const/16 v7, 0x2d

    #@39
    if-ne v1, v7, :cond_3e

    #@3b
    .line 142
    :cond_3b
    add-int/lit8 v2, v2, 0x1

    #@3d
    goto :goto_7

    #@3e
    .line 156
    :cond_3e
    const/16 v7, 0x61

    #@40
    if-gt v7, v1, :cond_49

    #@42
    const/16 v7, 0x7a

    #@44
    if-gt v1, v7, :cond_49

    #@46
    .line 158
    add-int/lit8 v6, v1, -0x61

    #@48
    .restart local v6       #value:I
    goto :goto_15

    #@49
    .line 159
    .end local v6           #value:I
    :cond_49
    const/16 v7, 0x30

    #@4b
    if-ne v1, v7, :cond_50

    #@4d
    .line 161
    const/16 v6, 0xe

    #@4f
    .restart local v6       #value:I
    goto :goto_15

    #@50
    .line 162
    .end local v6           #value:I
    :cond_50
    const/16 v7, 0x31

    #@52
    if-ne v1, v7, :cond_57

    #@54
    .line 164
    const/16 v6, 0x8

    #@56
    .restart local v6       #value:I
    goto :goto_15

    #@57
    .line 166
    .end local v6           #value:I
    :cond_57
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@59
    new-instance v8, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v9, "base base-32 character: "

    #@60
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v8

    #@64
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v8

    #@68
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v8

    #@6c
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6f
    throw v7

    #@70
    .line 176
    .restart local v6       #value:I
    :cond_70
    if-le v3, v11, :cond_3b

    #@72
    .line 177
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@74
    const-string/jumbo v8, "too long; should have 13 characters"

    #@77
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7a
    throw v7

    #@7b
    .line 181
    .end local v1           #group:I
    .end local v6           #value:I
    :cond_7b
    if-eq v3, v11, :cond_86

    #@7d
    .line 182
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@7f
    const-string/jumbo v8, "too short; should have 13 characters"

    #@82
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@85
    throw v7

    #@86
    .line 185
    :cond_86
    return-wide v4
.end method

.method private static final encodeBase32(J)Ljava/lang/String;
    .registers 9
    .parameter "input"

    #@0
    .prologue
    .line 104
    sget-object v0, Landroid/content/pm/VerifierDeviceIdentity;->ENCODE:[C

    #@2
    .line 110
    .local v0, alphabet:[C
    const/16 v5, 0x10

    #@4
    new-array v1, v5, [C

    #@6
    .line 112
    .local v1, encoded:[C
    array-length v4, v1

    #@7
    .line 113
    .local v4, index:I
    const/4 v3, 0x0

    #@8
    .local v3, i:I
    :goto_8
    const/16 v5, 0xd

    #@a
    if-ge v3, v5, :cond_28

    #@c
    .line 120
    if-lez v3, :cond_19

    #@e
    rem-int/lit8 v5, v3, 0x4

    #@10
    const/4 v6, 0x1

    #@11
    if-ne v5, v6, :cond_19

    #@13
    .line 121
    add-int/lit8 v4, v4, -0x1

    #@15
    const/16 v5, 0x2d

    #@17
    aput-char v5, v1, v4

    #@19
    .line 127
    :cond_19
    const-wide/16 v5, 0x1f

    #@1b
    and-long/2addr v5, p0

    #@1c
    long-to-int v2, v5

    #@1d
    .line 128
    .local v2, group:I
    const/4 v5, 0x5

    #@1e
    ushr-long/2addr p0, v5

    #@1f
    .line 130
    add-int/lit8 v4, v4, -0x1

    #@21
    aget-char v5, v0, v2

    #@23
    aput-char v5, v1, v4

    #@25
    .line 113
    add-int/lit8 v3, v3, 0x1

    #@27
    goto :goto_8

    #@28
    .line 133
    .end local v2           #group:I
    :cond_28
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    return-object v5
.end method

.method public static generate()Landroid/content/pm/VerifierDeviceIdentity;
    .registers 2

    #@0
    .prologue
    .line 77
    new-instance v0, Ljava/security/SecureRandom;

    #@2
    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    #@5
    .line 78
    .local v0, sr:Ljava/security/SecureRandom;
    invoke-static {v0}, Landroid/content/pm/VerifierDeviceIdentity;->generate(Ljava/util/Random;)Landroid/content/pm/VerifierDeviceIdentity;

    #@8
    move-result-object v1

    #@9
    return-object v1
.end method

.method static generate(Ljava/util/Random;)Landroid/content/pm/VerifierDeviceIdentity;
    .registers 4
    .parameter "rng"

    #@0
    .prologue
    .line 90
    invoke-virtual {p0}, Ljava/util/Random;->nextLong()J

    #@3
    move-result-wide v0

    #@4
    .line 91
    .local v0, identity:J
    new-instance v2, Landroid/content/pm/VerifierDeviceIdentity;

    #@6
    invoke-direct {v2, v0, v1}, Landroid/content/pm/VerifierDeviceIdentity;-><init>(J)V

    #@9
    return-object v2
.end method

.method public static parse(Ljava/lang/String;)Landroid/content/pm/VerifierDeviceIdentity;
    .registers 6
    .parameter "deviceIdentity"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 212
    :try_start_0
    const-string v2, "US-ASCII"

    #@2
    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_5} :catch_10

    #@5
    move-result-object v1

    #@6
    .line 217
    .local v1, input:[B
    new-instance v2, Landroid/content/pm/VerifierDeviceIdentity;

    #@8
    invoke-static {v1}, Landroid/content/pm/VerifierDeviceIdentity;->decodeBase32([B)J

    #@b
    move-result-wide v3

    #@c
    invoke-direct {v2, v3, v4}, Landroid/content/pm/VerifierDeviceIdentity;-><init>(J)V

    #@f
    return-object v2

    #@10
    .line 213
    .end local v1           #input:[B
    :catch_10
    move-exception v0

    #@11
    .line 214
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v3, "bad base-32 characters in input"

    #@15
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v2
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 222
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "other"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 195
    instance-of v2, p1, Landroid/content/pm/VerifierDeviceIdentity;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 200
    :cond_5
    :goto_5
    return v1

    #@6
    :cond_6
    move-object v0, p1

    #@7
    .line 199
    check-cast v0, Landroid/content/pm/VerifierDeviceIdentity;

    #@9
    .line 200
    .local v0, o:Landroid/content/pm/VerifierDeviceIdentity;
    iget-wide v2, p0, Landroid/content/pm/VerifierDeviceIdentity;->mIdentity:J

    #@b
    iget-wide v4, v0, Landroid/content/pm/VerifierDeviceIdentity;->mIdentity:J

    #@d
    cmp-long v2, v2, v4

    #@f
    if-nez v2, :cond_5

    #@11
    const/4 v1, 0x1

    #@12
    goto :goto_5
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 190
    iget-wide v0, p0, Landroid/content/pm/VerifierDeviceIdentity;->mIdentity:J

    #@2
    long-to-int v0, v0

    #@3
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 205
    iget-object v0, p0, Landroid/content/pm/VerifierDeviceIdentity;->mIdentityString:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 227
    iget-wide v0, p0, Landroid/content/pm/VerifierDeviceIdentity;->mIdentity:J

    #@2
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@5
    .line 228
    return-void
.end method
