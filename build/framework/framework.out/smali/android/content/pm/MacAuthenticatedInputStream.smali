.class public Landroid/content/pm/MacAuthenticatedInputStream;
.super Ljava/io/FilterInputStream;
.source "MacAuthenticatedInputStream.java"


# instance fields
.field private final mMac:Ljavax/crypto/Mac;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljavax/crypto/Mac;)V
    .registers 3
    .parameter "in"
    .parameter "mac"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    #@3
    .line 38
    iput-object p2, p0, Landroid/content/pm/MacAuthenticatedInputStream;->mMac:Ljavax/crypto/Mac;

    #@5
    .line 39
    return-void
.end method


# virtual methods
.method public isTagEqual([B)Z
    .registers 8
    .parameter "tag"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 42
    iget-object v4, p0, Landroid/content/pm/MacAuthenticatedInputStream;->mMac:Ljavax/crypto/Mac;

    #@3
    invoke-virtual {v4}, Ljavax/crypto/Mac;->doFinal()[B

    #@6
    move-result-object v0

    #@7
    .line 44
    .local v0, actualTag:[B
    if-eqz p1, :cond_f

    #@9
    if-eqz v0, :cond_f

    #@b
    array-length v4, p1

    #@c
    array-length v5, v0

    #@d
    if-eq v4, v5, :cond_10

    #@f
    .line 58
    :cond_f
    :goto_f
    return v3

    #@10
    .line 53
    :cond_10
    const/4 v2, 0x0

    #@11
    .line 54
    .local v2, value:I
    const/4 v1, 0x0

    #@12
    .local v1, i:I
    :goto_12
    array-length v4, p1

    #@13
    if-ge v1, v4, :cond_1e

    #@15
    .line 55
    aget-byte v4, p1, v1

    #@17
    aget-byte v5, v0, v1

    #@19
    xor-int/2addr v4, v5

    #@1a
    or-int/2addr v2, v4

    #@1b
    .line 54
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_12

    #@1e
    .line 58
    :cond_1e
    if-nez v2, :cond_f

    #@20
    const/4 v3, 0x1

    #@21
    goto :goto_f
.end method

.method public read()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 63
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    #@3
    move-result v0

    #@4
    .line 64
    .local v0, b:I
    if-ltz v0, :cond_c

    #@6
    .line 65
    iget-object v1, p0, Landroid/content/pm/MacAuthenticatedInputStream;->mMac:Ljavax/crypto/Mac;

    #@8
    int-to-byte v2, v0

    #@9
    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->update(B)V

    #@c
    .line 67
    :cond_c
    return v0
.end method

.method public read([BII)I
    .registers 6
    .parameter "buffer"
    .parameter "offset"
    .parameter "count"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 72
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    #@3
    move-result v0

    #@4
    .line 73
    .local v0, numRead:I
    if-lez v0, :cond_b

    #@6
    .line 74
    iget-object v1, p0, Landroid/content/pm/MacAuthenticatedInputStream;->mMac:Ljavax/crypto/Mac;

    #@8
    invoke-virtual {v1, p1, p2, v0}, Ljavax/crypto/Mac;->update([BII)V

    #@b
    .line 76
    :cond_b
    return v0
.end method
