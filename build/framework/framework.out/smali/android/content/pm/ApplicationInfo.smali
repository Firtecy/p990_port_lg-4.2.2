.class public Landroid/content/pm/ApplicationInfo;
.super Landroid/content/pm/PackageItemInfo;
.source "ApplicationInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/pm/ApplicationInfo$DisplayNameComparator;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final FLAG_ALLOW_BACKUP:I = 0x8000

.field public static final FLAG_ALLOW_CLEAR_USER_DATA:I = 0x40

.field public static final FLAG_ALLOW_TASK_REPARENTING:I = 0x20

.field public static final FLAG_CANT_SAVE_STATE:I = 0x10000000

.field public static final FLAG_DEBUGGABLE:I = 0x2

.field public static final FLAG_EXTERNAL_STORAGE:I = 0x40000

.field public static final FLAG_FACTORY_TEST:I = 0x10

.field public static final FLAG_FORWARD_LOCK:I = 0x20000000

.field public static final FLAG_HAS_CODE:I = 0x4

.field public static final FLAG_INSTALLED:I = 0x800000

.field public static final FLAG_IS_DATA_ONLY:I = 0x1000000

.field public static final FLAG_KILL_AFTER_RESTORE:I = 0x10000

.field public static final FLAG_LARGE_HEAP:I = 0x100000

.field public static final FLAG_PERSISTENT:I = 0x8

.field public static final FLAG_RESIZEABLE_FOR_SCREENS:I = 0x1000

.field public static final FLAG_RESTORE_ANY_VERSION:I = 0x20000

.field public static final FLAG_STOPPED:I = 0x200000

.field public static final FLAG_SUPPORTS_LARGE_SCREENS:I = 0x800

.field public static final FLAG_SUPPORTS_NORMAL_SCREENS:I = 0x400

.field public static final FLAG_SUPPORTS_RTL:I = 0x400000

.field public static final FLAG_SUPPORTS_SCREEN_DENSITIES:I = 0x2000

.field public static final FLAG_SUPPORTS_SMALL_SCREENS:I = 0x200

.field public static final FLAG_SUPPORTS_XLARGE_SCREENS:I = 0x80000

.field public static final FLAG_SYSTEM:I = 0x1

.field public static final FLAG_TEST_ONLY:I = 0x100

.field public static final FLAG_UPDATED_SYSTEM_APP:I = 0x80

.field public static final FLAG_VM_SAFE_MODE:I = 0x4000


# instance fields
.field public backupAgentName:Ljava/lang/String;

.field public className:Ljava/lang/String;

.field public compatibleWidthLimitDp:I

.field public dataDir:Ljava/lang/String;

.field public descriptionRes:I

.field public enabled:Z

.field public enabledSetting:I

.field public flags:I

.field public installLocation:I

.field public largestWidthLimitDp:I

.field public manageSpaceActivityName:Ljava/lang/String;

.field public nativeLibraryDir:Ljava/lang/String;

.field public permission:Ljava/lang/String;

.field public processName:Ljava/lang/String;

.field public publicSourceDir:Ljava/lang/String;

.field public requiresSmallestWidthDp:I

.field public resourceDirs:[Ljava/lang/String;

.field public sharedLibraryFiles:[Ljava/lang/String;

.field public sourceDir:Ljava/lang/String;

.field public targetSdkVersion:I

.field public taskAffinity:Ljava/lang/String;

.field public theme:I

.field public uiOptions:I

.field public uid:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 599
    new-instance v0, Landroid/content/pm/ApplicationInfo$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/ApplicationInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/ApplicationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 529
    invoke-direct {p0}, Landroid/content/pm/PackageItemInfo;-><init>()V

    #@4
    .line 98
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@6
    .line 353
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@8
    .line 361
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@a
    .line 369
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@c
    .line 377
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@e
    .line 440
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@11
    .line 446
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    #@13
    .line 452
    const/4 v0, -0x1

    #@14
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->installLocation:I

    #@16
    .line 530
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/ApplicationInfo;)V
    .registers 4
    .parameter "orig"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 533
    invoke-direct {p0, p1}, Landroid/content/pm/PackageItemInfo;-><init>(Landroid/content/pm/PackageItemInfo;)V

    #@4
    .line 98
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@6
    .line 353
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@8
    .line 361
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@a
    .line 369
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@c
    .line 377
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@e
    .line 440
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@11
    .line 446
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    #@13
    .line 452
    const/4 v0, -0x1

    #@14
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->installLocation:I

    #@16
    .line 534
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->taskAffinity:Ljava/lang/String;

    #@18
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->taskAffinity:Ljava/lang/String;

    #@1a
    .line 535
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@1c
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@1e
    .line 536
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@20
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@22
    .line 537
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@24
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@26
    .line 538
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->theme:I

    #@28
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->theme:I

    #@2a
    .line 539
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@2c
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@2e
    .line 540
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@30
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@32
    .line 541
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@34
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@36
    .line 542
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@38
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@3a
    .line 543
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@3c
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@3e
    .line 544
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@40
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@42
    .line 545
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@44
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@46
    .line 546
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->resourceDirs:[Ljava/lang/String;

    #@48
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->resourceDirs:[Ljava/lang/String;

    #@4a
    .line 547
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;

    #@4c
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;

    #@4e
    .line 548
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@50
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@52
    .line 549
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@54
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@56
    .line 550
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@58
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@5a
    .line 551
    iget-boolean v0, p1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@5c
    iput-boolean v0, p0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@5e
    .line 552
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    #@60
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    #@62
    .line 553
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->installLocation:I

    #@64
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->installLocation:I

    #@66
    .line 554
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->manageSpaceActivityName:Ljava/lang/String;

    #@68
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->manageSpaceActivityName:Ljava/lang/String;

    #@6a
    .line 555
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->descriptionRes:I

    #@6c
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->descriptionRes:I

    #@6e
    .line 556
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@70
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@72
    .line 557
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@74
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@76
    .line 558
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 610
    invoke-direct {p0, p1}, Landroid/content/pm/PackageItemInfo;-><init>(Landroid/os/Parcel;)V

    #@5
    .line 98
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@7
    .line 353
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@9
    .line 361
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@b
    .line 369
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@d
    .line 377
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@f
    .line 440
    iput-boolean v0, p0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@11
    .line 446
    iput v1, p0, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    #@13
    .line 452
    const/4 v2, -0x1

    #@14
    iput v2, p0, Landroid/content/pm/ApplicationInfo;->installLocation:I

    #@16
    .line 611
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    iput-object v2, p0, Landroid/content/pm/ApplicationInfo;->taskAffinity:Ljava/lang/String;

    #@1c
    .line 612
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    iput-object v2, p0, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@22
    .line 613
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    iput-object v2, p0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@28
    .line 614
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    iput-object v2, p0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@2e
    .line 615
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@31
    move-result v2

    #@32
    iput v2, p0, Landroid/content/pm/ApplicationInfo;->theme:I

    #@34
    .line 616
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v2

    #@38
    iput v2, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@3a
    .line 617
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v2

    #@3e
    iput v2, p0, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@40
    .line 618
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v2

    #@44
    iput v2, p0, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@46
    .line 619
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@49
    move-result v2

    #@4a
    iput v2, p0, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@4c
    .line 620
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    iput-object v2, p0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@52
    .line 621
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    iput-object v2, p0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@58
    .line 622
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    iput-object v2, p0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@5e
    .line 623
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    iput-object v2, p0, Landroid/content/pm/ApplicationInfo;->resourceDirs:[Ljava/lang/String;

    #@64
    .line 624
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@67
    move-result-object v2

    #@68
    iput-object v2, p0, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;

    #@6a
    .line 625
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    iput-object v2, p0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@70
    .line 626
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v2

    #@74
    iput v2, p0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@76
    .line 627
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@79
    move-result v2

    #@7a
    iput v2, p0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@7c
    .line 628
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v2

    #@80
    if-eqz v2, :cond_a9

    #@82
    :goto_82
    iput-boolean v0, p0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@84
    .line 629
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@87
    move-result v0

    #@88
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    #@8a
    .line 630
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8d
    move-result v0

    #@8e
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->installLocation:I

    #@90
    .line 631
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@93
    move-result-object v0

    #@94
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->manageSpaceActivityName:Ljava/lang/String;

    #@96
    .line 632
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@99
    move-result-object v0

    #@9a
    iput-object v0, p0, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@9c
    .line 633
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9f
    move-result v0

    #@a0
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->descriptionRes:I

    #@a2
    .line 634
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a5
    move-result v0

    #@a6
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@a8
    .line 635
    return-void

    #@a9
    :cond_a9
    move v0, v1

    #@aa
    .line 628
    goto :goto_82
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/ApplicationInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/content/pm/ApplicationInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private isPackageUnavailable(Landroid/content/pm/PackageManager;)Z
    .registers 7
    .parameter "pm"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 683
    :try_start_2
    iget-object v3, p0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {p1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_8
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_8} :catch_e

    #@8
    move-result-object v3

    #@9
    if-nez v3, :cond_c

    #@b
    .line 685
    :goto_b
    return v1

    #@c
    :cond_c
    move v1, v2

    #@d
    .line 683
    goto :goto_b

    #@e
    .line 684
    :catch_e
    move-exception v0

    #@f
    .line 685
    .local v0, ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_b
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 568
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public disableCompatibilityMode()V
    .registers 3

    #@0
    .prologue
    .line 664
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@2
    const v1, 0x83e00

    #@5
    or-int/2addr v0, v1

    #@6
    iput v0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@8
    .line 667
    return-void
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 5
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 455
    invoke-super {p0, p1, p2}, Landroid/content/pm/PackageItemInfo;->dumpFront(Landroid/util/Printer;Ljava/lang/String;)V

    #@3
    .line 456
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@5
    if-eqz v0, :cond_23

    #@7
    .line 457
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    const-string v1, "className="

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@23
    .line 459
    :cond_23
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@25
    if-eqz v0, :cond_44

    #@27
    .line 460
    new-instance v0, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    const-string/jumbo v1, "permission="

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@44
    .line 462
    :cond_44
    new-instance v0, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string/jumbo v1, "processName="

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v0

    #@5a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v0

    #@5e
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@61
    .line 463
    new-instance v0, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v0

    #@6a
    const-string/jumbo v1, "taskAffinity="

    #@6d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v0

    #@71
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->taskAffinity:Ljava/lang/String;

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v0

    #@77
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v0

    #@7b
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@7e
    .line 464
    new-instance v0, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v0

    #@87
    const-string/jumbo v1, "uid="

    #@8a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v0

    #@8e
    iget v1, p0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v0

    #@94
    const-string v1, " flags=0x"

    #@96
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v0

    #@9a
    iget v1, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@9c
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@9f
    move-result-object v1

    #@a0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v0

    #@a4
    const-string v1, " theme=0x"

    #@a6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v0

    #@aa
    iget v1, p0, Landroid/content/pm/ApplicationInfo;->theme:I

    #@ac
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@af
    move-result-object v1

    #@b0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v0

    #@b4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v0

    #@b8
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@bb
    .line 466
    new-instance v0, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v0

    #@c4
    const-string/jumbo v1, "requiresSmallestWidthDp="

    #@c7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v0

    #@cb
    iget v1, p0, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@cd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v0

    #@d1
    const-string v1, " compatibleWidthLimitDp="

    #@d3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v0

    #@d7
    iget v1, p0, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@d9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v0

    #@dd
    const-string v1, " largestWidthLimitDp="

    #@df
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v0

    #@e3
    iget v1, p0, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@e5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v0

    #@e9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v0

    #@ed
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@f0
    .line 469
    new-instance v0, Ljava/lang/StringBuilder;

    #@f2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f5
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v0

    #@f9
    const-string/jumbo v1, "sourceDir="

    #@fc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v0

    #@100
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v0

    #@106
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v0

    #@10a
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@10d
    .line 470
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@10f
    if-nez v0, :cond_24a

    #@111
    .line 471
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@113
    if-eqz v0, :cond_132

    #@115
    .line 472
    new-instance v0, Ljava/lang/StringBuilder;

    #@117
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@11a
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v0

    #@11e
    const-string/jumbo v1, "publicSourceDir="

    #@121
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v0

    #@125
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@127
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v0

    #@12b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12e
    move-result-object v0

    #@12f
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@132
    .line 477
    :cond_132
    :goto_132
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->resourceDirs:[Ljava/lang/String;

    #@134
    if-eqz v0, :cond_153

    #@136
    .line 478
    new-instance v0, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v0

    #@13f
    const-string/jumbo v1, "resourceDirs="

    #@142
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v0

    #@146
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->resourceDirs:[Ljava/lang/String;

    #@148
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v0

    #@14c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14f
    move-result-object v0

    #@150
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@153
    .line 480
    :cond_153
    new-instance v0, Ljava/lang/StringBuilder;

    #@155
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@158
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v0

    #@15c
    const-string v1, "dataDir="

    #@15e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v0

    #@162
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@164
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v0

    #@168
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v0

    #@16c
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@16f
    .line 481
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;

    #@171
    if-eqz v0, :cond_190

    #@173
    .line 482
    new-instance v0, Ljava/lang/StringBuilder;

    #@175
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@178
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v0

    #@17c
    const-string/jumbo v1, "sharedLibraryFiles="

    #@17f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v0

    #@183
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;

    #@185
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v0

    #@189
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18c
    move-result-object v0

    #@18d
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@190
    .line 484
    :cond_190
    new-instance v0, Ljava/lang/StringBuilder;

    #@192
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@195
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    move-result-object v0

    #@199
    const-string v1, "enabled="

    #@19b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v0

    #@19f
    iget-boolean v1, p0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@1a1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v0

    #@1a5
    const-string v1, " targetSdkVersion="

    #@1a7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v0

    #@1ab
    iget v1, p0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@1ad
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v0

    #@1b1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b4
    move-result-object v0

    #@1b5
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1b8
    .line 485
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->manageSpaceActivityName:Ljava/lang/String;

    #@1ba
    if-eqz v0, :cond_1d9

    #@1bc
    .line 486
    new-instance v0, Ljava/lang/StringBuilder;

    #@1be
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1c1
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v0

    #@1c5
    const-string/jumbo v1, "manageSpaceActivityName="

    #@1c8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v0

    #@1cc
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->manageSpaceActivityName:Ljava/lang/String;

    #@1ce
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v0

    #@1d2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d5
    move-result-object v0

    #@1d6
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1d9
    .line 488
    :cond_1d9
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->descriptionRes:I

    #@1db
    if-eqz v0, :cond_1fd

    #@1dd
    .line 489
    new-instance v0, Ljava/lang/StringBuilder;

    #@1df
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1e2
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e5
    move-result-object v0

    #@1e6
    const-string v1, "description=0x"

    #@1e8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v0

    #@1ec
    iget v1, p0, Landroid/content/pm/ApplicationInfo;->descriptionRes:I

    #@1ee
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1f1
    move-result-object v1

    #@1f2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v0

    #@1f6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f9
    move-result-object v0

    #@1fa
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1fd
    .line 491
    :cond_1fd
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@1ff
    if-eqz v0, :cond_222

    #@201
    .line 492
    new-instance v0, Ljava/lang/StringBuilder;

    #@203
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@206
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v0

    #@20a
    const-string/jumbo v1, "uiOptions=0x"

    #@20d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v0

    #@211
    iget v1, p0, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@213
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@216
    move-result-object v1

    #@217
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v0

    #@21b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21e
    move-result-object v0

    #@21f
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@222
    .line 494
    :cond_222
    new-instance v0, Ljava/lang/StringBuilder;

    #@224
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@227
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v0

    #@22b
    const-string/jumbo v1, "supportsRtl="

    #@22e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@231
    move-result-object v1

    #@232
    invoke-virtual {p0}, Landroid/content/pm/ApplicationInfo;->hasRtlSupport()Z

    #@235
    move-result v0

    #@236
    if-eqz v0, :cond_273

    #@238
    const-string/jumbo v0, "true"

    #@23b
    :goto_23b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23e
    move-result-object v0

    #@23f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@242
    move-result-object v0

    #@243
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@246
    .line 495
    invoke-super {p0, p1, p2}, Landroid/content/pm/PackageItemInfo;->dumpBack(Landroid/util/Printer;Ljava/lang/String;)V

    #@249
    .line 496
    return-void

    #@24a
    .line 474
    :cond_24a
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@24c
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@24e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@251
    move-result v0

    #@252
    if-nez v0, :cond_132

    #@254
    .line 475
    new-instance v0, Ljava/lang/StringBuilder;

    #@256
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@259
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25c
    move-result-object v0

    #@25d
    const-string/jumbo v1, "publicSourceDir="

    #@260
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@263
    move-result-object v0

    #@264
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@266
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@269
    move-result-object v0

    #@26a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26d
    move-result-object v0

    #@26e
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@271
    goto/16 :goto_132

    #@273
    .line 494
    :cond_273
    const-string v0, "false"

    #@275
    goto :goto_23b
.end method

.method protected getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .registers 1

    #@0
    .prologue
    .line 693
    return-object p0
.end method

.method public hasRtlSupport()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x40

    #@2
    .line 503
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method protected loadDefaultIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter "pm"

    #@0
    .prologue
    .line 673
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@2
    const/high16 v1, 0x4

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_19

    #@7
    invoke-direct {p0, p1}, Landroid/content/pm/ApplicationInfo;->isPackageUnavailable(Landroid/content/pm/PackageManager;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_19

    #@d
    .line 675
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@10
    move-result-object v0

    #@11
    const v1, 0x1080594

    #@14
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@17
    move-result-object v0

    #@18
    .line 678
    :goto_18
    return-object v0

    #@19
    :cond_19
    invoke-virtual {p1}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    #@1c
    move-result-object v0

    #@1d
    goto :goto_18
.end method

.method public loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "pm"

    #@0
    .prologue
    .line 649
    iget v1, p0, Landroid/content/pm/ApplicationInfo;->descriptionRes:I

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 650
    iget-object v1, p0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@6
    iget v2, p0, Landroid/content/pm/ApplicationInfo;->descriptionRes:I

    #@8
    invoke-virtual {p1, v1, v2, p0}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    .line 651
    .local v0, label:Ljava/lang/CharSequence;
    if-eqz v0, :cond_f

    #@e
    .line 655
    .end local v0           #label:Ljava/lang/CharSequence;
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 562
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ApplicationInfo{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    iget-object v1, p0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string/jumbo v1, "}"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 572
    invoke-super {p0, p1, p2}, Landroid/content/pm/PackageItemInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@3
    .line 573
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->taskAffinity:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@8
    .line 574
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->permission:Ljava/lang/String;

    #@a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 575
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 576
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 577
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->theme:I

    #@19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 578
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 579
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@23
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 580
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@28
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 581
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@2d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 582
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@32
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@35
    .line 583
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@37
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3a
    .line 584
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@3c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3f
    .line 585
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->resourceDirs:[Ljava/lang/String;

    #@41
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@44
    .line 586
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;

    #@46
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@49
    .line 587
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@4b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4e
    .line 588
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@50
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@53
    .line 589
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@55
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@58
    .line 590
    iget-boolean v0, p0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@5a
    if-eqz v0, :cond_7f

    #@5c
    const/4 v0, 0x1

    #@5d
    :goto_5d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@60
    .line 591
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    #@62
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@65
    .line 592
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->installLocation:I

    #@67
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6a
    .line 593
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->manageSpaceActivityName:Ljava/lang/String;

    #@6c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@6f
    .line 594
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@71
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@74
    .line 595
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->descriptionRes:I

    #@76
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@79
    .line 596
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->uiOptions:I

    #@7b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7e
    .line 597
    return-void

    #@7f
    .line 590
    :cond_7f
    const/4 v0, 0x0

    #@80
    goto :goto_5d
.end method
