.class public Landroid/content/pm/LimitedLengthInputStream;
.super Ljava/io/FilterInputStream;
.source "LimitedLengthInputStream.java"


# instance fields
.field private final mEnd:J

.field private mOffset:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;JJ)V
    .registers 9
    .parameter "in"
    .parameter "offset"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    .line 33
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    #@5
    .line 35
    if-nez p1, :cond_f

    #@7
    .line 36
    new-instance v0, Ljava/io/IOException;

    #@9
    const-string v1, "in == null"

    #@b
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 39
    :cond_f
    cmp-long v0, p2, v1

    #@11
    if-gez v0, :cond_1c

    #@13
    .line 40
    new-instance v0, Ljava/io/IOException;

    #@15
    const-string/jumbo v1, "offset < 0"

    #@18
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 43
    :cond_1c
    cmp-long v0, p4, v1

    #@1e
    if-gez v0, :cond_29

    #@20
    .line 44
    new-instance v0, Ljava/io/IOException;

    #@22
    const-string/jumbo v1, "length < 0"

    #@25
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 47
    :cond_29
    const-wide v0, 0x7fffffffffffffffL

    #@2e
    sub-long/2addr v0, p2

    #@2f
    cmp-long v0, p4, v0

    #@31
    if-lez v0, :cond_3c

    #@33
    .line 48
    new-instance v0, Ljava/io/IOException;

    #@35
    const-string/jumbo v1, "offset + length > Long.MAX_VALUE"

    #@38
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v0

    #@3c
    .line 51
    :cond_3c
    add-long v0, p2, p4

    #@3e
    iput-wide v0, p0, Landroid/content/pm/LimitedLengthInputStream;->mEnd:J

    #@40
    .line 53
    invoke-virtual {p0, p2, p3}, Landroid/content/pm/LimitedLengthInputStream;->skip(J)J

    #@43
    .line 54
    iput-wide p2, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@45
    .line 55
    return-void
.end method


# virtual methods
.method public declared-synchronized read()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 59
    monitor-enter p0

    #@1
    :try_start_1
    iget-wide v0, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@3
    iget-wide v2, p0, Landroid/content/pm/LimitedLengthInputStream;->mEnd:J
    :try_end_5
    .catchall {:try_start_1 .. :try_end_5} :catchall_18

    #@5
    cmp-long v0, v0, v2

    #@7
    if-ltz v0, :cond_c

    #@9
    .line 60
    const/4 v0, -0x1

    #@a
    .line 64
    :goto_a
    monitor-exit p0

    #@b
    return v0

    #@c
    .line 63
    :cond_c
    :try_start_c
    iget-wide v0, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@e
    const-wide/16 v2, 0x1

    #@10
    add-long/2addr v0, v2

    #@11
    iput-wide v0, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@13
    .line 64
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I
    :try_end_16
    .catchall {:try_start_c .. :try_end_16} :catchall_18

    #@16
    move-result v0

    #@17
    goto :goto_a

    #@18
    .line 59
    :catchall_18
    move-exception v0

    #@19
    monitor-exit p0

    #@1a
    throw v0
.end method

.method public read([B)I
    .registers 4
    .parameter "buffer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 92
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/content/pm/LimitedLengthInputStream;->read([BII)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public read([BII)I
    .registers 12
    .parameter "buffer"
    .parameter "offset"
    .parameter "byteCount"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 69
    iget-wide v2, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@2
    iget-wide v4, p0, Landroid/content/pm/LimitedLengthInputStream;->mEnd:J

    #@4
    cmp-long v2, v2, v4

    #@6
    if-ltz v2, :cond_a

    #@8
    .line 70
    const/4 v1, -0x1

    #@9
    .line 87
    :goto_9
    return v1

    #@a
    .line 73
    :cond_a
    array-length v0, p1

    #@b
    .line 74
    .local v0, arrayLength:I
    invoke-static {v0, p2, p3}, Ljava/util/Arrays;->checkOffsetAndCount(III)V

    #@e
    .line 76
    iget-wide v2, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@10
    const-wide v4, 0x7fffffffffffffffL

    #@15
    int-to-long v6, p3

    #@16
    sub-long/2addr v4, v6

    #@17
    cmp-long v2, v2, v4

    #@19
    if-lez v2, :cond_41

    #@1b
    .line 77
    new-instance v2, Ljava/io/IOException;

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string/jumbo v4, "offset out of bounds: "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    iget-wide v4, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@2b
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v4, " + "

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@40
    throw v2

    #@41
    .line 80
    :cond_41
    iget-wide v2, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@43
    int-to-long v4, p3

    #@44
    add-long/2addr v2, v4

    #@45
    iget-wide v4, p0, Landroid/content/pm/LimitedLengthInputStream;->mEnd:J

    #@47
    cmp-long v2, v2, v4

    #@49
    if-lez v2, :cond_51

    #@4b
    .line 81
    iget-wide v2, p0, Landroid/content/pm/LimitedLengthInputStream;->mEnd:J

    #@4d
    iget-wide v4, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@4f
    sub-long/2addr v2, v4

    #@50
    long-to-int p3, v2

    #@51
    .line 84
    :cond_51
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    #@54
    move-result v1

    #@55
    .line 85
    .local v1, numRead:I
    iget-wide v2, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@57
    int-to-long v4, v1

    #@58
    add-long/2addr v2, v4

    #@59
    iput-wide v2, p0, Landroid/content/pm/LimitedLengthInputStream;->mOffset:J

    #@5b
    goto :goto_9
.end method
