.class public Landroid/content/pm/VerificationParams;
.super Ljava/lang/Object;
.source "VerificationParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/pm/VerificationParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final NO_UID:I = -0x1

.field private static final TO_STRING_PREFIX:Ljava/lang/String; = "VerificationParams{"


# instance fields
.field private mInstallerUid:I

.field private final mManifestDigest:Landroid/content/pm/ManifestDigest;

.field private final mOriginatingURI:Landroid/net/Uri;

.field private final mOriginatingUid:I

.field private final mReferrer:Landroid/net/Uri;

.field private final mVerificationURI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 226
    new-instance v0, Landroid/content/pm/VerificationParams$1;

    #@2
    invoke-direct {v0}, Landroid/content/pm/VerificationParams$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/pm/VerificationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;ILandroid/content/pm/ManifestDigest;)V
    .registers 7
    .parameter "verificationURI"
    .parameter "originatingURI"
    .parameter "referrer"
    .parameter "originatingUid"
    .parameter "manifestDigest"

    #@0
    .prologue
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    iput-object p1, p0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@5
    .line 74
    iput-object p2, p0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@7
    .line 75
    iput-object p3, p0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@9
    .line 76
    iput p4, p0, Landroid/content/pm/VerificationParams;->mOriginatingUid:I

    #@b
    .line 77
    iput-object p5, p0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@d
    .line 78
    const/4 v0, -0x1

    #@e
    iput v0, p0, Landroid/content/pm/VerificationParams;->mInstallerUid:I

    #@10
    .line 79
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 217
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 218
    const-class v0, Landroid/net/Uri;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/net/Uri;

    #@f
    iput-object v0, p0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@11
    .line 219
    const-class v0, Landroid/net/Uri;

    #@13
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Landroid/net/Uri;

    #@1d
    iput-object v0, p0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@1f
    .line 220
    const-class v0, Landroid/net/Uri;

    #@21
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@28
    move-result-object v0

    #@29
    check-cast v0, Landroid/net/Uri;

    #@2b
    iput-object v0, p0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@2d
    .line 221
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v0

    #@31
    iput v0, p0, Landroid/content/pm/VerificationParams;->mOriginatingUid:I

    #@33
    .line 222
    const-class v0, Landroid/content/pm/ManifestDigest;

    #@35
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@3c
    move-result-object v0

    #@3d
    check-cast v0, Landroid/content/pm/ManifestDigest;

    #@3f
    iput-object v0, p0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@41
    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v0

    #@45
    iput v0, p0, Landroid/content/pm/VerificationParams;->mInstallerUid:I

    #@47
    .line 224
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/pm/VerificationParams$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/content/pm/VerificationParams;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 113
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 118
    if-ne p0, p1, :cond_5

    #@4
    .line 168
    :cond_4
    :goto_4
    return v1

    #@5
    .line 122
    :cond_5
    instance-of v3, p1, Landroid/content/pm/VerificationParams;

    #@7
    if-nez v3, :cond_b

    #@9
    move v1, v2

    #@a
    .line 123
    goto :goto_4

    #@b
    :cond_b
    move-object v0, p1

    #@c
    .line 126
    check-cast v0, Landroid/content/pm/VerificationParams;

    #@e
    .line 128
    .local v0, other:Landroid/content/pm/VerificationParams;
    iget-object v3, p0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@10
    if-nez v3, :cond_18

    #@12
    .line 129
    iget-object v3, v0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@14
    if-eqz v3, :cond_24

    #@16
    move v1, v2

    #@17
    .line 130
    goto :goto_4

    #@18
    .line 132
    :cond_18
    iget-object v3, p0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@1a
    iget-object v4, v0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@1c
    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v3

    #@20
    if-nez v3, :cond_24

    #@22
    move v1, v2

    #@23
    .line 133
    goto :goto_4

    #@24
    .line 136
    :cond_24
    iget-object v3, p0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@26
    if-nez v3, :cond_2e

    #@28
    .line 137
    iget-object v3, v0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@2a
    if-eqz v3, :cond_3a

    #@2c
    move v1, v2

    #@2d
    .line 138
    goto :goto_4

    #@2e
    .line 140
    :cond_2e
    iget-object v3, p0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@30
    iget-object v4, v0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@32
    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v3

    #@36
    if-nez v3, :cond_3a

    #@38
    move v1, v2

    #@39
    .line 141
    goto :goto_4

    #@3a
    .line 144
    :cond_3a
    iget-object v3, p0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@3c
    if-nez v3, :cond_44

    #@3e
    .line 145
    iget-object v3, v0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@40
    if-eqz v3, :cond_50

    #@42
    move v1, v2

    #@43
    .line 146
    goto :goto_4

    #@44
    .line 148
    :cond_44
    iget-object v3, p0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@46
    iget-object v4, v0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@48
    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v3

    #@4c
    if-nez v3, :cond_50

    #@4e
    move v1, v2

    #@4f
    .line 149
    goto :goto_4

    #@50
    .line 152
    :cond_50
    iget v3, p0, Landroid/content/pm/VerificationParams;->mOriginatingUid:I

    #@52
    iget v4, v0, Landroid/content/pm/VerificationParams;->mOriginatingUid:I

    #@54
    if-eq v3, v4, :cond_58

    #@56
    move v1, v2

    #@57
    .line 153
    goto :goto_4

    #@58
    .line 156
    :cond_58
    iget-object v3, p0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@5a
    if-nez v3, :cond_62

    #@5c
    .line 157
    iget-object v3, v0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@5e
    if-eqz v3, :cond_6e

    #@60
    move v1, v2

    #@61
    .line 158
    goto :goto_4

    #@62
    .line 160
    :cond_62
    iget-object v3, p0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@64
    iget-object v4, v0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@66
    invoke-virtual {v3, v4}, Landroid/content/pm/ManifestDigest;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v3

    #@6a
    if-nez v3, :cond_6e

    #@6c
    move v1, v2

    #@6d
    .line 161
    goto :goto_4

    #@6e
    .line 164
    :cond_6e
    iget v3, p0, Landroid/content/pm/VerificationParams;->mInstallerUid:I

    #@70
    iget v4, v0, Landroid/content/pm/VerificationParams;->mInstallerUid:I

    #@72
    if-eq v3, v4, :cond_4

    #@74
    move v1, v2

    #@75
    .line 165
    goto :goto_4
.end method

.method public getInstallerUid()I
    .registers 2

    #@0
    .prologue
    .line 104
    iget v0, p0, Landroid/content/pm/VerificationParams;->mInstallerUid:I

    #@2
    return v0
.end method

.method public getManifestDigest()Landroid/content/pm/ManifestDigest;
    .registers 2

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@2
    return-object v0
.end method

.method public getOriginatingURI()Landroid/net/Uri;
    .registers 2

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method public getOriginatingUid()I
    .registers 2

    #@0
    .prologue
    .line 95
    iget v0, p0, Landroid/content/pm/VerificationParams;->mOriginatingUid:I

    #@2
    return v0
.end method

.method public getReferrer()Landroid/net/Uri;
    .registers 2

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method public getVerificationURI()Landroid/net/Uri;
    .registers 2

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 173
    const/4 v0, 0x3

    #@2
    .line 175
    .local v0, hash:I
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@4
    if-nez v1, :cond_2c

    #@6
    move v1, v2

    #@7
    :goto_7
    mul-int/lit8 v1, v1, 0x5

    #@9
    add-int/2addr v0, v1

    #@a
    .line 176
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@c
    if-nez v1, :cond_33

    #@e
    move v1, v2

    #@f
    :goto_f
    mul-int/lit8 v1, v1, 0x7

    #@11
    add-int/2addr v0, v1

    #@12
    .line 177
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@14
    if-nez v1, :cond_3a

    #@16
    move v1, v2

    #@17
    :goto_17
    mul-int/lit8 v1, v1, 0xb

    #@19
    add-int/2addr v0, v1

    #@1a
    .line 178
    iget v1, p0, Landroid/content/pm/VerificationParams;->mOriginatingUid:I

    #@1c
    mul-int/lit8 v1, v1, 0xd

    #@1e
    add-int/2addr v0, v1

    #@1f
    .line 179
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@21
    if-nez v1, :cond_41

    #@23
    :goto_23
    mul-int/lit8 v1, v2, 0x11

    #@25
    add-int/2addr v0, v1

    #@26
    .line 180
    iget v1, p0, Landroid/content/pm/VerificationParams;->mInstallerUid:I

    #@28
    mul-int/lit8 v1, v1, 0x13

    #@2a
    add-int/2addr v0, v1

    #@2b
    .line 182
    return v0

    #@2c
    .line 175
    :cond_2c
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@2e
    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    #@31
    move-result v1

    #@32
    goto :goto_7

    #@33
    .line 176
    :cond_33
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@35
    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    #@38
    move-result v1

    #@39
    goto :goto_f

    #@3a
    .line 177
    :cond_3a
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@3c
    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    #@3f
    move-result v1

    #@40
    goto :goto_17

    #@41
    .line 179
    :cond_41
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@43
    invoke-virtual {v1}, Landroid/content/pm/ManifestDigest;->hashCode()I

    #@46
    move-result v2

    #@47
    goto :goto_23
.end method

.method public setInstallerUid(I)V
    .registers 2
    .parameter "uid"

    #@0
    .prologue
    .line 108
    iput p1, p0, Landroid/content/pm/VerificationParams;->mInstallerUid:I

    #@2
    .line 109
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "VerificationParams{"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 189
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string/jumbo v1, "mVerificationURI="

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 190
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@f
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 191
    const-string v1, ",mOriginatingURI="

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    .line 192
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@1d
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 193
    const-string v1, ",mReferrer="

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    .line 194
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@2b
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    .line 195
    const-string v1, ",mOriginatingUid="

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    .line 196
    iget v1, p0, Landroid/content/pm/VerificationParams;->mOriginatingUid:I

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    .line 197
    const-string v1, ",mManifestDigest="

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    .line 198
    iget-object v1, p0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@43
    invoke-virtual {v1}, Landroid/content/pm/ManifestDigest;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    .line 199
    const-string v1, ",mInstallerUid="

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    .line 200
    iget v1, p0, Landroid/content/pm/VerificationParams;->mInstallerUid:I

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    .line 201
    const/16 v1, 0x7d

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@59
    .line 203
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 208
    iget-object v0, p0, Landroid/content/pm/VerificationParams;->mVerificationURI:Landroid/net/Uri;

    #@3
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@6
    .line 209
    iget-object v0, p0, Landroid/content/pm/VerificationParams;->mOriginatingURI:Landroid/net/Uri;

    #@8
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@b
    .line 210
    iget-object v0, p0, Landroid/content/pm/VerificationParams;->mReferrer:Landroid/net/Uri;

    #@d
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@10
    .line 211
    iget v0, p0, Landroid/content/pm/VerificationParams;->mOriginatingUid:I

    #@12
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 212
    iget-object v0, p0, Landroid/content/pm/VerificationParams;->mManifestDigest:Landroid/content/pm/ManifestDigest;

    #@17
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@1a
    .line 213
    iget v0, p0, Landroid/content/pm/VerificationParams;->mInstallerUid:I

    #@1c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 214
    return-void
.end method
