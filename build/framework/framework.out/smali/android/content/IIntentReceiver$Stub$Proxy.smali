.class Landroid/content/IIntentReceiver$Stub$Proxy;
.super Ljava/lang/Object;
.source "IIntentReceiver.java"

# interfaces
.implements Landroid/content/IIntentReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/IIntentReceiver$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 89
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 90
    iput-object p1, p0, Landroid/content/IIntentReceiver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 91
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Landroid/content/IIntentReceiver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 98
    const-string v0, "android.content.IIntentReceiver"

    #@2
    return-object v0
.end method

.method public performReceive(Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V
    .registers 13
    .parameter "intent"
    .parameter "resultCode"
    .parameter "data"
    .parameter "extras"
    .parameter "ordered"
    .parameter "sticky"
    .parameter "sendingUser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 102
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 104
    .local v0, _data:Landroid/os/Parcel;
    :try_start_6
    const-string v3, "android.content.IIntentReceiver"

    #@8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@b
    .line 105
    if-eqz p1, :cond_3f

    #@d
    .line 106
    const/4 v3, 0x1

    #@e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 107
    const/4 v3, 0x0

    #@12
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@15
    .line 112
    :goto_15
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 113
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 114
    if-eqz p4, :cond_49

    #@1d
    .line 115
    const/4 v3, 0x1

    #@1e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 116
    const/4 v3, 0x0

    #@22
    invoke-virtual {p4, v0, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@25
    .line 121
    :goto_25
    if-eqz p5, :cond_4e

    #@27
    move v3, v1

    #@28
    :goto_28
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 122
    if-eqz p6, :cond_50

    #@2d
    :goto_2d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 123
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 124
    iget-object v1, p0, Landroid/content/IIntentReceiver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@35
    const/4 v2, 0x1

    #@36
    const/4 v3, 0x0

    #@37
    const/4 v4, 0x1

    #@38
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_3b
    .catchall {:try_start_6 .. :try_end_3b} :catchall_44

    #@3b
    .line 127
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    .line 129
    return-void

    #@3f
    .line 110
    :cond_3f
    const/4 v3, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_44

    #@43
    goto :goto_15

    #@44
    .line 127
    :catchall_44
    move-exception v1

    #@45
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@48
    throw v1

    #@49
    .line 119
    :cond_49
    const/4 v3, 0x0

    #@4a
    :try_start_4a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_44

    #@4d
    goto :goto_25

    #@4e
    :cond_4e
    move v3, v2

    #@4f
    .line 121
    goto :goto_28

    #@50
    :cond_50
    move v1, v2

    #@51
    .line 122
    goto :goto_2d
.end method
