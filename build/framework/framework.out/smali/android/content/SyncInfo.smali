.class public Landroid/content/SyncInfo;
.super Ljava/lang/Object;
.source "SyncInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/SyncInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final account:Landroid/accounts/Account;

.field public final authority:Ljava/lang/String;

.field public final authorityId:I

.field public final startTime:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 79
    new-instance v0, Landroid/content/SyncInfo$1;

    #@2
    invoke-direct {v0}, Landroid/content/SyncInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/SyncInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method constructor <init>(ILandroid/accounts/Account;Ljava/lang/String;J)V
    .registers 6
    .parameter "authorityId"
    .parameter "account"
    .parameter "authority"
    .parameter "startTime"

    #@0
    .prologue
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    iput p1, p0, Landroid/content/SyncInfo;->authorityId:I

    #@5
    .line 52
    iput-object p2, p0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    #@7
    .line 53
    iput-object p3, p0, Landroid/content/SyncInfo;->authority:Ljava/lang/String;

    #@9
    .line 54
    iput-wide p4, p0, Landroid/content/SyncInfo;->startTime:J

    #@b
    .line 55
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "parcel"

    #@0
    .prologue
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/content/SyncInfo;->authorityId:I

    #@9
    .line 73
    new-instance v0, Landroid/accounts/Account;

    #@b
    invoke-direct {v0, p1}, Landroid/accounts/Account;-><init>(Landroid/os/Parcel;)V

    #@e
    iput-object v0, p0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    #@10
    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Landroid/content/SyncInfo;->authority:Ljava/lang/String;

    #@16
    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@19
    move-result-wide v0

    #@1a
    iput-wide v0, p0, Landroid/content/SyncInfo;->startTime:J

    #@1c
    .line 76
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 59
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 64
    iget v0, p0, Landroid/content/SyncInfo;->authorityId:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 65
    iget-object v0, p0, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    #@7
    const/4 v1, 0x0

    #@8
    invoke-virtual {v0, p1, v1}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@b
    .line 66
    iget-object v0, p0, Landroid/content/SyncInfo;->authority:Ljava/lang/String;

    #@d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 67
    iget-wide v0, p0, Landroid/content/SyncInfo;->startTime:J

    #@12
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@15
    .line 68
    return-void
.end method
