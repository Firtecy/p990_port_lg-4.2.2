.class public Landroid/content/Intent;
.super Ljava/lang/Object;
.source "Intent.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/Intent$FilterComparison;,
        Landroid/content/Intent$ShortcutIconResource;
    }
.end annotation


# static fields
.field public static final ACTION_ADVANCED_SETTINGS_CHANGED:Ljava/lang/String; = "android.intent.action.ADVANCED_SETTINGS"

.field public static final ACTION_AIRPLANE_MODE_CHANGED:Ljava/lang/String; = "android.intent.action.AIRPLANE_MODE"

.field public static final ACTION_ALARM_CHANGED:Ljava/lang/String; = "android.intent.action.ALARM_CHANGED"

.field public static final ACTION_ALL_APPS:Ljava/lang/String; = "android.intent.action.ALL_APPS"

.field public static final ACTION_ANALOG_AUDIO_DOCK_PLUG:Ljava/lang/String; = "android.intent.action.ANALOG_AUDIO_DOCK_PLUG"

.field public static final ACTION_ANSWER:Ljava/lang/String; = "android.intent.action.ANSWER"

.field public static final ACTION_APP_ERROR:Ljava/lang/String; = "android.intent.action.APP_ERROR"

.field public static final ACTION_ASSIST:Ljava/lang/String; = "android.intent.action.ASSIST"

.field public static final ACTION_ATTACH_DATA:Ljava/lang/String; = "android.intent.action.ATTACH_DATA"

.field public static final ACTION_BATTERY_CHANGED:Ljava/lang/String; = "android.intent.action.BATTERY_CHANGED"

.field public static final ACTION_BATTERY_LOW:Ljava/lang/String; = "android.intent.action.BATTERY_LOW"

.field public static final ACTION_BATTERY_OKAY:Ljava/lang/String; = "android.intent.action.BATTERY_OKAY"

.field public static final ACTION_BOOT_COMPLETED:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final ACTION_BUG_REPORT:Ljava/lang/String; = "android.intent.action.BUG_REPORT"

.field public static final ACTION_CALL:Ljava/lang/String; = "android.intent.action.CALL"

.field public static final ACTION_CALL_BUTTON:Ljava/lang/String; = "android.intent.action.CALL_BUTTON"

.field public static final ACTION_CALL_EMERGENCY:Ljava/lang/String; = "android.intent.action.CALL_EMERGENCY"

.field public static final ACTION_CALL_PRIVILEGED:Ljava/lang/String; = "android.intent.action.CALL_PRIVILEGED"

.field public static final ACTION_CAMERA_BUTTON:Ljava/lang/String; = "android.intent.action.CAMERA_BUTTON"

.field public static final ACTION_CHOOSER:Ljava/lang/String; = "android.intent.action.CHOOSER"

.field public static final ACTION_CLEAR_DNS_CACHE:Ljava/lang/String; = "android.intent.action.CLEAR_DNS_CACHE"

.field public static final ACTION_CLOSE_SYSTEM_DIALOGS:Ljava/lang/String; = "android.intent.action.CLOSE_SYSTEM_DIALOGS"

.field public static final ACTION_CONFIGURATION_CHANGED:Ljava/lang/String; = "android.intent.action.CONFIGURATION_CHANGED"

.field public static final ACTION_CREATE_SHORTCUT:Ljava/lang/String; = "android.intent.action.CREATE_SHORTCUT"

.field public static final ACTION_DATE_CHANGED:Ljava/lang/String; = "android.intent.action.DATE_CHANGED"

.field public static final ACTION_DEFAULT:Ljava/lang/String; = "android.intent.action.VIEW"

.field public static final ACTION_DELETE:Ljava/lang/String; = "android.intent.action.DELETE"

.field public static final ACTION_DEVICE_STORAGE_FULL:Ljava/lang/String; = "android.intent.action.DEVICE_STORAGE_FULL"

.field public static final ACTION_DEVICE_STORAGE_LOW:Ljava/lang/String; = "android.intent.action.DEVICE_STORAGE_LOW"

.field public static final ACTION_DEVICE_STORAGE_NOT_FULL:Ljava/lang/String; = "android.intent.action.DEVICE_STORAGE_NOT_FULL"

.field public static final ACTION_DEVICE_STORAGE_OK:Ljava/lang/String; = "android.intent.action.DEVICE_STORAGE_OK"

.field public static final ACTION_DIAL:Ljava/lang/String; = "android.intent.action.DIAL"

.field public static final ACTION_DIGITAL_AUDIO_DOCK_PLUG:Ljava/lang/String; = "android.intent.action.DIGITAL_AUDIO_DOCK_PLUG"

.field public static final ACTION_DOCK_EVENT:Ljava/lang/String; = "android.intent.action.DOCK_EVENT"

.field public static final ACTION_DREAMING_STARTED:Ljava/lang/String; = "android.intent.action.DREAMING_STARTED"

.field public static final ACTION_DREAMING_STOPPED:Ljava/lang/String; = "android.intent.action.DREAMING_STOPPED"

.field public static final ACTION_EDIT:Ljava/lang/String; = "android.intent.action.EDIT"

.field public static final ACTION_EXTERNAL_APPLICATIONS_AVAILABLE:Ljava/lang/String; = "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

.field public static final ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE:Ljava/lang/String; = "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

.field public static final ACTION_FACTORY_TEST:Ljava/lang/String; = "android.intent.action.FACTORY_TEST"

.field public static final ACTION_FM:Ljava/lang/String; = "qualcomm.intent.action.FM"

.field public static final ACTION_FM_TX:Ljava/lang/String; = "qualcomm.intent.action.FMTX"

.field public static final ACTION_GET_CONTENT:Ljava/lang/String; = "android.intent.action.GET_CONTENT"

.field public static final ACTION_GTALK_SERVICE_CONNECTED:Ljava/lang/String; = "android.intent.action.GTALK_CONNECTED"

.field public static final ACTION_GTALK_SERVICE_DISCONNECTED:Ljava/lang/String; = "android.intent.action.GTALK_DISCONNECTED"

.field public static final ACTION_HDMI_AUDIO_PLUG:Ljava/lang/String; = "android.intent.action.HDMI_AUDIO_PLUG"

.field public static final ACTION_HEADSET_PLUG:Ljava/lang/String; = "android.intent.action.HEADSET_PLUG"

.field public static final ACTION_INPUT_METHOD_CHANGED:Ljava/lang/String; = "android.intent.action.INPUT_METHOD_CHANGED"

.field public static final ACTION_INSERT:Ljava/lang/String; = "android.intent.action.INSERT"

.field public static final ACTION_INSERT_OR_EDIT:Ljava/lang/String; = "android.intent.action.INSERT_OR_EDIT"

.field public static final ACTION_INSTALL_PACKAGE:Ljava/lang/String; = "android.intent.action.INSTALL_PACKAGE"

.field public static final ACTION_LOCALE_CHANGED:Ljava/lang/String; = "android.intent.action.LOCALE_CHANGED"

.field public static final ACTION_MAIN:Ljava/lang/String; = "android.intent.action.MAIN"

.field public static final ACTION_MANAGE_NETWORK_USAGE:Ljava/lang/String; = "android.intent.action.MANAGE_NETWORK_USAGE"

.field public static final ACTION_MANAGE_PACKAGE_STORAGE:Ljava/lang/String; = "android.intent.action.MANAGE_PACKAGE_STORAGE"

.field public static final ACTION_MEDIA_BAD_REMOVAL:Ljava/lang/String; = "android.intent.action.MEDIA_BAD_REMOVAL"

.field public static final ACTION_MEDIA_BUTTON:Ljava/lang/String; = "android.intent.action.MEDIA_BUTTON"

.field public static final ACTION_MEDIA_CHECKING:Ljava/lang/String; = "android.intent.action.MEDIA_CHECKING"

.field public static final ACTION_MEDIA_EJECT:Ljava/lang/String; = "android.intent.action.MEDIA_EJECT"

.field public static final ACTION_MEDIA_MOUNTED:Ljava/lang/String; = "android.intent.action.MEDIA_MOUNTED"

.field public static final ACTION_MEDIA_NOFS:Ljava/lang/String; = "android.intent.action.MEDIA_NOFS"

.field public static final ACTION_MEDIA_PLAYER_STATE_CHANGED:Ljava/lang/String; = "com.lge.intent.action.MEDIA_PLAYER_STATE_CHANGED"

.field public static final ACTION_MEDIA_REMOVED:Ljava/lang/String; = "android.intent.action.MEDIA_REMOVED"

.field public static final ACTION_MEDIA_SCANNER_ERROR:Ljava/lang/String; = "com.lge.intent.action.MEDIA_SCANNER_ERROR"

.field public static final ACTION_MEDIA_SCANNER_FINISHED:Ljava/lang/String; = "android.intent.action.MEDIA_SCANNER_FINISHED"

.field public static final ACTION_MEDIA_SCANNER_PERCENTAGE:Ljava/lang/String; = "com.lge.intent.action.MEDIA_SCANNER_PERCENTAGE"

.field public static final ACTION_MEDIA_SCANNER_SCAN_FILE:Ljava/lang/String; = "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

.field public static final ACTION_MEDIA_SCANNER_STARTED:Ljava/lang/String; = "android.intent.action.MEDIA_SCANNER_STARTED"

.field public static final ACTION_MEDIA_SHARED:Ljava/lang/String; = "android.intent.action.MEDIA_SHARED"

.field public static final ACTION_MEDIA_UNMOUNTABLE:Ljava/lang/String; = "android.intent.action.MEDIA_UNMOUNTABLE"

.field public static final ACTION_MEDIA_UNMOUNTED:Ljava/lang/String; = "android.intent.action.MEDIA_UNMOUNTED"

.field public static final ACTION_MEDIA_UNSHARED:Ljava/lang/String; = "android.intent.action.MEDIA_UNSHARED"

.field public static final ACTION_MY_PACKAGE_REPLACED:Ljava/lang/String; = "android.intent.action.MY_PACKAGE_REPLACED"

.field public static final ACTION_NEW_OUTGOING_CALL:Ljava/lang/String; = "android.intent.action.NEW_OUTGOING_CALL"

.field public static final ACTION_PACKAGE_ADDED:Ljava/lang/String; = "android.intent.action.PACKAGE_ADDED"

.field public static final ACTION_PACKAGE_CHANGED:Ljava/lang/String; = "android.intent.action.PACKAGE_CHANGED"

.field public static final ACTION_PACKAGE_DATA_CLEARED:Ljava/lang/String; = "android.intent.action.PACKAGE_DATA_CLEARED"

.field public static final ACTION_PACKAGE_FIRST_LAUNCH:Ljava/lang/String; = "android.intent.action.PACKAGE_FIRST_LAUNCH"

.field public static final ACTION_PACKAGE_FULLY_REMOVED:Ljava/lang/String; = "android.intent.action.PACKAGE_FULLY_REMOVED"

.field public static final ACTION_PACKAGE_INSTALL:Ljava/lang/String; = "android.intent.action.PACKAGE_INSTALL"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_PACKAGE_NEEDS_VERIFICATION:Ljava/lang/String; = "android.intent.action.PACKAGE_NEEDS_VERIFICATION"

.field public static final ACTION_PACKAGE_REMOVED:Ljava/lang/String; = "android.intent.action.PACKAGE_REMOVED"

.field public static final ACTION_PACKAGE_REPLACED:Ljava/lang/String; = "android.intent.action.PACKAGE_REPLACED"

.field public static final ACTION_PACKAGE_RESTARTED:Ljava/lang/String; = "android.intent.action.PACKAGE_RESTARTED"

.field public static final ACTION_PACKAGE_VERIFIED:Ljava/lang/String; = "android.intent.action.PACKAGE_VERIFIED"

.field public static final ACTION_PASTE:Ljava/lang/String; = "android.intent.action.PASTE"

.field public static final ACTION_PICK:Ljava/lang/String; = "android.intent.action.PICK"

.field public static final ACTION_PICK_ACTIVITY:Ljava/lang/String; = "android.intent.action.PICK_ACTIVITY"

.field public static final ACTION_POWER_CONNECTED:Ljava/lang/String; = "android.intent.action.ACTION_POWER_CONNECTED"

.field public static final ACTION_POWER_DISCONNECTED:Ljava/lang/String; = "android.intent.action.ACTION_POWER_DISCONNECTED"

.field public static final ACTION_POWER_USAGE_SUMMARY:Ljava/lang/String; = "android.intent.action.POWER_USAGE_SUMMARY"

.field public static final ACTION_PRE_BOOT_COMPLETED:Ljava/lang/String; = "android.intent.action.PRE_BOOT_COMPLETED"

.field public static final ACTION_PROVIDER_CHANGED:Ljava/lang/String; = "android.intent.action.PROVIDER_CHANGED"

.field public static final ACTION_QUERY_PACKAGE_RESTART:Ljava/lang/String; = "android.intent.action.QUERY_PACKAGE_RESTART"

.field public static final ACTION_QUICK_CLOCK:Ljava/lang/String; = "android.intent.action.QUICK_CLOCK"

.field public static final ACTION_REBOOT:Ljava/lang/String; = "android.intent.action.REBOOT"

.field public static final ACTION_REMOTE_INTENT:Ljava/lang/String; = "com.google.android.c2dm.intent.RECEIVE"

.field public static final ACTION_REQUEST_SHUTDOWN:Ljava/lang/String; = "android.intent.action.ACTION_REQUEST_SHUTDOWN"

.field public static final ACTION_RUN:Ljava/lang/String; = "android.intent.action.RUN"

.field public static final ACTION_SCREEN_OFF:Ljava/lang/String; = "android.intent.action.SCREEN_OFF"

.field public static final ACTION_SCREEN_ON:Ljava/lang/String; = "android.intent.action.SCREEN_ON"

.field public static final ACTION_SEARCH:Ljava/lang/String; = "android.intent.action.SEARCH"

.field public static final ACTION_SEARCH_LONG_PRESS:Ljava/lang/String; = "android.intent.action.SEARCH_LONG_PRESS"

.field public static final ACTION_SEND:Ljava/lang/String; = "android.intent.action.SEND"

.field public static final ACTION_SENDTO:Ljava/lang/String; = "android.intent.action.SENDTO"

.field public static final ACTION_SEND_MULTIPLE:Ljava/lang/String; = "android.intent.action.SEND_MULTIPLE"

.field public static final ACTION_SET_WALLPAPER:Ljava/lang/String; = "android.intent.action.SET_WALLPAPER"

.field public static final ACTION_SHUTDOWN:Ljava/lang/String; = "android.intent.action.ACTION_SHUTDOWN"

.field public static final ACTION_SYNC:Ljava/lang/String; = "android.intent.action.SYNC"

.field public static final ACTION_SYNC_STATE_CHANGED:Ljava/lang/String; = "android.intent.action.SYNC_STATE_CHANGED"

.field public static final ACTION_SYSTEM_TUTORIAL:Ljava/lang/String; = "android.intent.action.SYSTEM_TUTORIAL"

.field public static final ACTION_THEME_PACKAGE_CHANGED:Ljava/lang/String; = "android.intent.action.THEME_PACKAGE_CHANGED"

.field public static final ACTION_TIMEZONE_CHANGED:Ljava/lang/String; = "android.intent.action.TIMEZONE_CHANGED"

.field public static final ACTION_TIME_CHANGED:Ljava/lang/String; = "android.intent.action.TIME_SET"

.field public static final ACTION_TIME_TICK:Ljava/lang/String; = "android.intent.action.TIME_TICK"

.field public static final ACTION_UID_REMOVED:Ljava/lang/String; = "android.intent.action.UID_REMOVED"

.field public static final ACTION_UMS_CONNECTED:Ljava/lang/String; = "android.intent.action.UMS_CONNECTED"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_UMS_DISCONNECTED:Ljava/lang/String; = "android.intent.action.UMS_DISCONNECTED"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_UNINSTALL_PACKAGE:Ljava/lang/String; = "android.intent.action.UNINSTALL_PACKAGE"

.field public static final ACTION_UPGRADE_SETUP:Ljava/lang/String; = "android.intent.action.UPGRADE_SETUP"

.field public static final ACTION_USB_AUDIO_ACCESSORY_PLUG:Ljava/lang/String; = "android.intent.action.USB_AUDIO_ACCESSORY_PLUG"

.field public static final ACTION_USB_AUDIO_DEVICE_PLUG:Ljava/lang/String; = "android.intent.action.USB_AUDIO_DEVICE_PLUG"

.field public static final ACTION_USER_ADDED:Ljava/lang/String; = "android.intent.action.USER_ADDED"

.field public static final ACTION_USER_BACKGROUND:Ljava/lang/String; = "android.intent.action.USER_BACKGROUND"

.field public static final ACTION_USER_FOREGROUND:Ljava/lang/String; = "android.intent.action.USER_FOREGROUND"

.field public static final ACTION_USER_INFO_CHANGED:Ljava/lang/String; = "android.intent.action.USER_INFO_CHANGED"

.field public static final ACTION_USER_INITIALIZE:Ljava/lang/String; = "android.intent.action.USER_INITIALIZE"

.field public static final ACTION_USER_PRESENT:Ljava/lang/String; = "android.intent.action.USER_PRESENT"

.field public static final ACTION_USER_REMOVED:Ljava/lang/String; = "android.intent.action.USER_REMOVED"

.field public static final ACTION_USER_STARTED:Ljava/lang/String; = "android.intent.action.USER_STARTED"

.field public static final ACTION_USER_STARTING:Ljava/lang/String; = "android.intent.action.USER_STARTING"

.field public static final ACTION_USER_STOPPED:Ljava/lang/String; = "android.intent.action.USER_STOPPED"

.field public static final ACTION_USER_STOPPING:Ljava/lang/String; = "android.intent.action.USER_STOPPING"

.field public static final ACTION_USER_SWITCHED:Ljava/lang/String; = "android.intent.action.USER_SWITCHED"

.field public static final ACTION_VIEW:Ljava/lang/String; = "android.intent.action.VIEW"

.field public static final ACTION_VOICE_COMMAND:Ljava/lang/String; = "android.intent.action.VOICE_COMMAND"

.field public static final ACTION_WALLPAPER_CHANGED:Ljava/lang/String; = "android.intent.action.WALLPAPER_CHANGED"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_WEB_SEARCH:Ljava/lang/String; = "android.intent.action.WEB_SEARCH"

.field public static final ACTION_WIFI_DISPLAY_AUDIO:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_AUDIO"

.field public static final ACTION_WIFI_DISPLAY_VIDEO:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_VIDEO"

.field public static final CATEGORY_ALTERNATIVE:Ljava/lang/String; = "android.intent.category.ALTERNATIVE"

.field public static final CATEGORY_APP_BROWSER:Ljava/lang/String; = "android.intent.category.APP_BROWSER"

.field public static final CATEGORY_APP_CALCULATOR:Ljava/lang/String; = "android.intent.category.APP_CALCULATOR"

.field public static final CATEGORY_APP_CALENDAR:Ljava/lang/String; = "android.intent.category.APP_CALENDAR"

.field public static final CATEGORY_APP_CONTACTS:Ljava/lang/String; = "android.intent.category.APP_CONTACTS"

.field public static final CATEGORY_APP_EMAIL:Ljava/lang/String; = "android.intent.category.APP_EMAIL"

.field public static final CATEGORY_APP_GALLERY:Ljava/lang/String; = "android.intent.category.APP_GALLERY"

.field public static final CATEGORY_APP_MAPS:Ljava/lang/String; = "android.intent.category.APP_MAPS"

.field public static final CATEGORY_APP_MARKET:Ljava/lang/String; = "android.intent.category.APP_MARKET"

.field public static final CATEGORY_APP_MESSAGING:Ljava/lang/String; = "android.intent.category.APP_MESSAGING"

.field public static final CATEGORY_APP_MUSIC:Ljava/lang/String; = "android.intent.category.APP_MUSIC"

.field public static final CATEGORY_BROWSABLE:Ljava/lang/String; = "android.intent.category.BROWSABLE"

.field public static final CATEGORY_CAR_DOCK:Ljava/lang/String; = "android.intent.category.CAR_DOCK"

.field public static final CATEGORY_CAR_MODE:Ljava/lang/String; = "android.intent.category.CAR_MODE"

.field public static final CATEGORY_DEFAULT:Ljava/lang/String; = "android.intent.category.DEFAULT"

.field public static final CATEGORY_DESK_DOCK:Ljava/lang/String; = "android.intent.category.DESK_DOCK"

.field public static final CATEGORY_DEVELOPMENT_PREFERENCE:Ljava/lang/String; = "android.intent.category.DEVELOPMENT_PREFERENCE"

.field public static final CATEGORY_EMBED:Ljava/lang/String; = "android.intent.category.EMBED"

.field public static final CATEGORY_FRAMEWORK_INSTRUMENTATION_TEST:Ljava/lang/String; = "android.intent.category.FRAMEWORK_INSTRUMENTATION_TEST"

.field public static final CATEGORY_HE_DESK_DOCK:Ljava/lang/String; = "android.intent.category.HE_DESK_DOCK"

.field public static final CATEGORY_HOME:Ljava/lang/String; = "android.intent.category.HOME"

.field public static final CATEGORY_INFO:Ljava/lang/String; = "android.intent.category.INFO"

.field public static final CATEGORY_LAUNCHER:Ljava/lang/String; = "android.intent.category.LAUNCHER"

.field public static final CATEGORY_LE_DESK_DOCK:Ljava/lang/String; = "android.intent.category.LE_DESK_DOCK"

.field public static final CATEGORY_MONKEY:Ljava/lang/String; = "android.intent.category.MONKEY"

.field public static final CATEGORY_OPENABLE:Ljava/lang/String; = "android.intent.category.OPENABLE"

.field public static final CATEGORY_PREFERENCE:Ljava/lang/String; = "android.intent.category.PREFERENCE"

.field public static final CATEGORY_SAMPLE_CODE:Ljava/lang/String; = "android.intent.category.SAMPLE_CODE"

.field public static final CATEGORY_SELECTED_ALTERNATIVE:Ljava/lang/String; = "android.intent.category.SELECTED_ALTERNATIVE"

.field public static final CATEGORY_TAB:Ljava/lang/String; = "android.intent.category.TAB"

.field public static final CATEGORY_TEST:Ljava/lang/String; = "android.intent.category.TEST"

.field public static final CATEGORY_UNIT_TEST:Ljava/lang/String; = "android.intent.category.UNIT_TEST"

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXTRA_ALARM_COUNT:Ljava/lang/String; = "android.intent.extra.ALARM_COUNT"

.field public static final EXTRA_ALLOW_REPLACE:Ljava/lang/String; = "android.intent.extra.ALLOW_REPLACE"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final EXTRA_BCC:Ljava/lang/String; = "android.intent.extra.BCC"

.field public static final EXTRA_BUG_REPORT:Ljava/lang/String; = "android.intent.extra.BUG_REPORT"

.field public static final EXTRA_CC:Ljava/lang/String; = "android.intent.extra.CC"

.field public static final EXTRA_CHANGED_COMPONENT_NAME:Ljava/lang/String; = "android.intent.extra.changed_component_name"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final EXTRA_CHANGED_COMPONENT_NAME_LIST:Ljava/lang/String; = "android.intent.extra.changed_component_name_list"

.field public static final EXTRA_CHANGED_PACKAGE_LIST:Ljava/lang/String; = "android.intent.extra.changed_package_list"

.field public static final EXTRA_CHANGED_UID_LIST:Ljava/lang/String; = "android.intent.extra.changed_uid_list"

.field public static final EXTRA_CLIENT_INTENT:Ljava/lang/String; = "android.intent.extra.client_intent"

.field public static final EXTRA_CLIENT_LABEL:Ljava/lang/String; = "android.intent.extra.client_label"

.field public static final EXTRA_DATA_REMOVED:Ljava/lang/String; = "android.intent.extra.DATA_REMOVED"

.field public static final EXTRA_DOCK_STATE:Ljava/lang/String; = "android.intent.extra.DOCK_STATE"

.field public static final EXTRA_DOCK_STATE_CAR:I = 0x2

.field public static final EXTRA_DOCK_STATE_DESK:I = 0x1

.field public static final EXTRA_DOCK_STATE_HE_DESK:I = 0x4

.field public static final EXTRA_DOCK_STATE_LE_DESK:I = 0x3

.field public static final EXTRA_DOCK_STATE_UNDOCKED:I = 0x0

.field public static final EXTRA_DONT_KILL_APP:Ljava/lang/String; = "android.intent.extra.DONT_KILL_APP"

.field public static final EXTRA_EMAIL:Ljava/lang/String; = "android.intent.extra.EMAIL"

.field public static final EXTRA_HTML_TEXT:Ljava/lang/String; = "android.intent.extra.HTML_TEXT"

.field public static final EXTRA_INITIAL_INTENTS:Ljava/lang/String; = "android.intent.extra.INITIAL_INTENTS"

.field public static final EXTRA_INSTALLER_PACKAGE_NAME:Ljava/lang/String; = "android.intent.extra.INSTALLER_PACKAGE_NAME"

.field public static final EXTRA_INSTALL_RESULT:Ljava/lang/String; = "android.intent.extra.INSTALL_RESULT"

.field public static final EXTRA_INTENT:Ljava/lang/String; = "android.intent.extra.INTENT"

.field public static final EXTRA_KEY_CONFIRM:Ljava/lang/String; = "android.intent.extra.KEY_CONFIRM"

.field public static final EXTRA_KEY_EVENT:Ljava/lang/String; = "android.intent.extra.KEY_EVENT"

.field public static final EXTRA_LOCAL_ONLY:Ljava/lang/String; = "android.intent.extra.LOCAL_ONLY"

.field public static final EXTRA_MEDIA_PLAYER_CURRENT_POSITION:Ljava/lang/String; = "com.lge.intent.extra.MEDIA_PLAYER_CURRENT_POSITION"

.field public static final EXTRA_MEDIA_PLAYER_STATE:Ljava/lang/String; = "com.lge.intent.extra.MEDIA_PLAYER_STATE"

.field public static final EXTRA_MEDIA_PLAYER_TOTAL_DURATION:Ljava/lang/String; = "com.lge.intent.extra.MEDIA_PLAYER_TOTAL_DURATION"

.field public static final EXTRA_MEDIA_PLAYER_URI:Ljava/lang/String; = "com.lge.intent.extra.MEDIA_PLAYER_URI"

.field public static final EXTRA_NOT_UNKNOWN_SOURCE:Ljava/lang/String; = "android.intent.extra.NOT_UNKNOWN_SOURCE"

.field public static final EXTRA_ORIGINATING_UID:Ljava/lang/String; = "android.intent.extra.ORIGINATING_UID"

.field public static final EXTRA_ORIGINATING_URI:Ljava/lang/String; = "android.intent.extra.ORIGINATING_URI"

.field public static final EXTRA_PACKAGES:Ljava/lang/String; = "android.intent.extra.PACKAGES"

.field public static final EXTRA_PHONE_NUMBER:Ljava/lang/String; = "android.intent.extra.PHONE_NUMBER"

.field public static final EXTRA_REFERRER:Ljava/lang/String; = "android.intent.extra.REFERRER"

.field public static final EXTRA_REMOTE_INTENT_TOKEN:Ljava/lang/String; = "android.intent.extra.remote_intent_token"

.field public static final EXTRA_REMOVED_FOR_ALL_USERS:Ljava/lang/String; = "android.intent.extra.REMOVED_FOR_ALL_USERS"

.field public static final EXTRA_REPLACING:Ljava/lang/String; = "android.intent.extra.REPLACING"

.field public static final EXTRA_RETURN_RESULT:Ljava/lang/String; = "android.intent.extra.RETURN_RESULT"

.field public static final EXTRA_SHORTCUT_ICON:Ljava/lang/String; = "android.intent.extra.shortcut.ICON"

.field public static final EXTRA_SHORTCUT_ICON_RESOURCE:Ljava/lang/String; = "android.intent.extra.shortcut.ICON_RESOURCE"

.field public static final EXTRA_SHORTCUT_INTENT:Ljava/lang/String; = "android.intent.extra.shortcut.INTENT"

.field public static final EXTRA_SHORTCUT_NAME:Ljava/lang/String; = "android.intent.extra.shortcut.NAME"

.field public static final EXTRA_STREAM:Ljava/lang/String; = "android.intent.extra.STREAM"

.field public static final EXTRA_SUBJECT:Ljava/lang/String; = "android.intent.extra.SUBJECT"

.field public static final EXTRA_TEMPLATE:Ljava/lang/String; = "android.intent.extra.TEMPLATE"

.field public static final EXTRA_TEXT:Ljava/lang/String; = "android.intent.extra.TEXT"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "android.intent.extra.TITLE"

.field public static final EXTRA_UID:Ljava/lang/String; = "android.intent.extra.UID"

.field public static final EXTRA_UNINSTALL_ALL_USERS:Ljava/lang/String; = "android.intent.extra.UNINSTALL_ALL_USERS"

.field public static final EXTRA_USER_HANDLE:Ljava/lang/String; = "android.intent.extra.user_handle"

.field public static final FILL_IN_ACTION:I = 0x1

.field public static final FILL_IN_CATEGORIES:I = 0x4

.field public static final FILL_IN_CLIP_DATA:I = 0x80

.field public static final FILL_IN_COMPONENT:I = 0x8

.field public static final FILL_IN_DATA:I = 0x2

.field public static final FILL_IN_PACKAGE:I = 0x10

.field public static final FILL_IN_SELECTOR:I = 0x40

.field public static final FILL_IN_SOURCE_BOUNDS:I = 0x20

.field public static final FLAG_ACTIVITY_BROUGHT_TO_FRONT:I = 0x400000

.field public static final FLAG_ACTIVITY_CLEAR_TASK:I = 0x8000

.field public static final FLAG_ACTIVITY_CLEAR_TOP:I = 0x4000000

.field public static final FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET:I = 0x80000

.field public static final FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS:I = 0x800000

.field public static final FLAG_ACTIVITY_FORWARD_RESULT:I = 0x2000000

.field public static final FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY:I = 0x100000

.field public static final FLAG_ACTIVITY_MULTIPLE_TASK:I = 0x8000000

.field public static final FLAG_ACTIVITY_NEW_TASK:I = 0x10000000

.field public static final FLAG_ACTIVITY_NO_ANIMATION:I = 0x10000

.field public static final FLAG_ACTIVITY_NO_HISTORY:I = 0x40000000

.field public static final FLAG_ACTIVITY_NO_USER_ACTION:I = 0x40000

.field public static final FLAG_ACTIVITY_PREVIOUS_IS_TOP:I = 0x1000000

.field public static final FLAG_ACTIVITY_REORDER_TO_FRONT:I = 0x20000

.field public static final FLAG_ACTIVITY_RESET_TASK_IF_NEEDED:I = 0x200000

.field public static final FLAG_ACTIVITY_SINGLE_TOP:I = 0x20000000

.field public static final FLAG_ACTIVITY_TASK_ON_HOME:I = 0x4000

.field public static final FLAG_DEBUG_LOG_RESOLUTION:I = 0x8

.field public static final FLAG_EXCLUDE_STOPPED_PACKAGES:I = 0x10

.field public static final FLAG_FROM_BACKGROUND:I = 0x4

.field public static final FLAG_GRANT_READ_URI_PERMISSION:I = 0x1

.field public static final FLAG_GRANT_WRITE_URI_PERMISSION:I = 0x2

.field public static final FLAG_INCLUDE_STOPPED_PACKAGES:I = 0x20

.field public static final FLAG_RECEIVER_BOOT_UPGRADE:I = 0x4000000

.field public static final FLAG_RECEIVER_FOREGROUND:I = 0x10000000

.field public static final FLAG_RECEIVER_REGISTERED_ONLY:I = 0x40000000

.field public static final FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT:I = 0x8000000

.field public static final FLAG_RECEIVER_REPLACE_PENDING:I = 0x20000000

.field public static final IMMUTABLE_FLAGS:I = 0x3

.field public static final METADATA_DOCK_HOME:Ljava/lang/String; = "android.dock_home"

.field public static final METADATA_SETUP_VERSION:Ljava/lang/String; = "android.SETUP_VERSION"

.field public static final URI_INTENT_SCHEME:I = 0x1


# instance fields
.field private mAction:Ljava/lang/String;

.field private mCategories:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mClipData:Landroid/content/ClipData;

.field private mComponent:Landroid/content/ComponentName;

.field private mData:Landroid/net/Uri;

.field private mExtras:Landroid/os/Bundle;

.field private mFlags:I

.field private mPackage:Ljava/lang/String;

.field private mSelector:Landroid/content/Intent;

.field private mSourceBounds:Landroid/graphics/Rect;

.field private mType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 6755
    new-instance v0, Landroid/content/Intent$1;

    #@2
    invoke-direct {v0}, Landroid/content/Intent$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3477
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3478
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .registers 4
    .parameter "packageContext"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3582
    .local p2, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3583
    new-instance v0, Landroid/content/ComponentName;

    #@5
    invoke-direct {v0, p1, p2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@8
    iput-object v0, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@a
    .line 3584
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .registers 4
    .parameter "o"

    #@0
    .prologue
    .line 3483
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3484
    iget-object v0, p1, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@5
    iput-object v0, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@7
    .line 3485
    iget-object v0, p1, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@b
    .line 3486
    iget-object v0, p1, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@d
    iput-object v0, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@f
    .line 3487
    iget-object v0, p1, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@11
    iput-object v0, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@13
    .line 3488
    iget-object v0, p1, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@15
    iput-object v0, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@17
    .line 3489
    iget v0, p1, Landroid/content/Intent;->mFlags:I

    #@19
    iput v0, p0, Landroid/content/Intent;->mFlags:I

    #@1b
    .line 3490
    iget-object v0, p1, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@1d
    if-eqz v0, :cond_28

    #@1f
    .line 3491
    new-instance v0, Ljava/util/HashSet;

    #@21
    iget-object v1, p1, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@23
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@26
    iput-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@28
    .line 3493
    :cond_28
    iget-object v0, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2a
    if-eqz v0, :cond_35

    #@2c
    .line 3494
    new-instance v0, Landroid/os/Bundle;

    #@2e
    iget-object v1, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@30
    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@33
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@35
    .line 3496
    :cond_35
    iget-object v0, p1, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@37
    if-eqz v0, :cond_42

    #@39
    .line 3497
    new-instance v0, Landroid/graphics/Rect;

    #@3b
    iget-object v1, p1, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@3d
    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@40
    iput-object v0, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@42
    .line 3499
    :cond_42
    iget-object v0, p1, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@44
    if-eqz v0, :cond_4f

    #@46
    .line 3500
    new-instance v0, Landroid/content/Intent;

    #@48
    iget-object v1, p1, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@4a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@4d
    iput-object v0, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@4f
    .line 3502
    :cond_4f
    iget-object v0, p1, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@51
    if-eqz v0, :cond_5c

    #@53
    .line 3503
    new-instance v0, Landroid/content/ClipData;

    #@55
    iget-object v1, p1, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@57
    invoke-direct {v0, v1}, Landroid/content/ClipData;-><init>(Landroid/content/ClipData;)V

    #@5a
    iput-object v0, p0, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@5c
    .line 3505
    :cond_5c
    return-void
.end method

.method private constructor <init>(Landroid/content/Intent;Z)V
    .registers 5
    .parameter "o"
    .parameter "all"

    #@0
    .prologue
    .line 3512
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3513
    iget-object v0, p1, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@5
    iput-object v0, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@7
    .line 3514
    iget-object v0, p1, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@b
    .line 3515
    iget-object v0, p1, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@d
    iput-object v0, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@f
    .line 3516
    iget-object v0, p1, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@11
    iput-object v0, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@13
    .line 3517
    iget-object v0, p1, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@15
    iput-object v0, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@17
    .line 3518
    iget-object v0, p1, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@19
    if-eqz v0, :cond_24

    #@1b
    .line 3519
    new-instance v0, Ljava/util/HashSet;

    #@1d
    iget-object v1, p1, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@1f
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@22
    iput-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@24
    .line 3521
    :cond_24
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 6766
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 6767
    invoke-virtual {p0, p1}, Landroid/content/Intent;->readFromParcel(Landroid/os/Parcel;)V

    #@6
    .line 6768
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "action"

    #@0
    .prologue
    .line 3541
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3542
    invoke-virtual {p0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@6
    .line 3543
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;)V
    .registers 3
    .parameter "action"
    .parameter "uri"

    #@0
    .prologue
    .line 3561
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3562
    invoke-virtual {p0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@6
    .line 3563
    iput-object p2, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@8
    .line 3564
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V
    .registers 6
    .parameter "action"
    .parameter "uri"
    .parameter "packageContext"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3610
    .local p4, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3611
    invoke-virtual {p0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@6
    .line 3612
    iput-object p2, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@8
    .line 3613
    new-instance v0, Landroid/content/ComponentName;

    #@a
    invoke-direct {v0, p3, p4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@d
    iput-object v0, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@f
    .line 3614
    return-void
.end method

.method public static createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;
    .registers 9
    .parameter "target"
    .parameter "title"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 853
    new-instance v0, Landroid/content/Intent;

    #@3
    const-string v5, "android.intent.action.CHOOSER"

    #@5
    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@8
    .line 854
    .local v0, intent:Landroid/content/Intent;
    const-string v5, "android.intent.extra.INTENT"

    #@a
    invoke-virtual {v0, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@d
    .line 855
    if-eqz p1, :cond_14

    #@f
    .line 856
    const-string v5, "android.intent.extra.TITLE"

    #@11
    invoke-virtual {v0, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    #@14
    .line 860
    :cond_14
    invoke-virtual {p0}, Landroid/content/Intent;->getFlags()I

    #@17
    move-result v5

    #@18
    and-int/lit8 v3, v5, 0x3

    #@1a
    .line 862
    .local v3, permFlags:I
    if-eqz v3, :cond_4e

    #@1c
    .line 863
    invoke-virtual {p0}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    #@1f
    move-result-object v4

    #@20
    .line 864
    .local v4, targetClipData:Landroid/content/ClipData;
    if-nez v4, :cond_46

    #@22
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@25
    move-result-object v5

    #@26
    if-eqz v5, :cond_46

    #@28
    .line 865
    new-instance v1, Landroid/content/ClipData$Item;

    #@2a
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@2d
    move-result-object v5

    #@2e
    invoke-direct {v1, v5}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    #@31
    .line 867
    .local v1, item:Landroid/content/ClipData$Item;
    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    if-eqz v5, :cond_4f

    #@37
    .line 868
    const/4 v5, 0x1

    #@38
    new-array v2, v5, [Ljava/lang/String;

    #@3a
    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    aput-object v5, v2, v6

    #@40
    .line 872
    .local v2, mimeTypes:[Ljava/lang/String;
    :goto_40
    new-instance v4, Landroid/content/ClipData;

    #@42
    .end local v4           #targetClipData:Landroid/content/ClipData;
    const/4 v5, 0x0

    #@43
    invoke-direct {v4, v5, v2, v1}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@46
    .line 874
    .end local v1           #item:Landroid/content/ClipData$Item;
    .end local v2           #mimeTypes:[Ljava/lang/String;
    .restart local v4       #targetClipData:Landroid/content/ClipData;
    :cond_46
    if-eqz v4, :cond_4e

    #@48
    .line 875
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setClipData(Landroid/content/ClipData;)V

    #@4b
    .line 876
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@4e
    .line 880
    .end local v4           #targetClipData:Landroid/content/ClipData;
    :cond_4e
    return-object v0

    #@4f
    .line 870
    .restart local v1       #item:Landroid/content/ClipData$Item;
    .restart local v4       #targetClipData:Landroid/content/ClipData;
    :cond_4f
    new-array v2, v6, [Ljava/lang/String;

    #@51
    .restart local v2       #mimeTypes:[Ljava/lang/String;
    goto :goto_40
.end method

.method public static getIntent(Ljava/lang/String;)Landroid/content/Intent;
    .registers 2
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 3703
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static getIntentOld(Ljava/lang/String;)Landroid/content/Intent;
    .registers 23
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;
        }
    .end annotation

    #@0
    .prologue
    .line 3867
    const/16 v19, 0x23

    #@2
    move-object/from16 v0, p0

    #@4
    move/from16 v1, v19

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    #@9
    move-result v9

    #@a
    .line 3868
    .local v9, i:I
    if-ltz v9, :cond_2e0

    #@c
    .line 3869
    const/4 v4, 0x0

    #@d
    .line 3870
    .local v4, action:Ljava/lang/String;
    move v11, v9

    #@e
    .line 3871
    .local v11, intentFragmentStart:I
    const/4 v12, 0x0

    #@f
    .line 3873
    .local v12, isIntentFragment:Z
    add-int/lit8 v9, v9, 0x1

    #@11
    .line 3875
    const-string v19, "action("

    #@13
    const/16 v20, 0x0

    #@15
    const/16 v21, 0x7

    #@17
    move-object/from16 v0, p0

    #@19
    move-object/from16 v1, v19

    #@1b
    move/from16 v2, v20

    #@1d
    move/from16 v3, v21

    #@1f
    invoke-virtual {v0, v9, v1, v2, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@22
    move-result v19

    #@23
    if-eqz v19, :cond_3a

    #@25
    .line 3876
    const/4 v12, 0x1

    #@26
    .line 3877
    add-int/lit8 v9, v9, 0x7

    #@28
    .line 3878
    const/16 v19, 0x29

    #@2a
    move-object/from16 v0, p0

    #@2c
    move/from16 v1, v19

    #@2e
    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(II)I

    #@31
    move-result v13

    #@32
    .line 3879
    .local v13, j:I
    move-object/from16 v0, p0

    #@34
    invoke-virtual {v0, v9, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    .line 3880
    add-int/lit8 v9, v13, 0x1

    #@3a
    .line 3883
    .end local v13           #j:I
    :cond_3a
    new-instance v10, Landroid/content/Intent;

    #@3c
    invoke-direct {v10, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3f
    .line 3885
    .local v10, intent:Landroid/content/Intent;
    const-string v19, "categories("

    #@41
    const/16 v20, 0x0

    #@43
    const/16 v21, 0xb

    #@45
    move-object/from16 v0, p0

    #@47
    move-object/from16 v1, v19

    #@49
    move/from16 v2, v20

    #@4b
    move/from16 v3, v21

    #@4d
    invoke-virtual {v0, v9, v1, v2, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@50
    move-result v19

    #@51
    if-eqz v19, :cond_86

    #@53
    .line 3886
    const/4 v12, 0x1

    #@54
    .line 3887
    add-int/lit8 v9, v9, 0xb

    #@56
    .line 3888
    const/16 v19, 0x29

    #@58
    move-object/from16 v0, p0

    #@5a
    move/from16 v1, v19

    #@5c
    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(II)I

    #@5f
    move-result v13

    #@60
    .line 3889
    .restart local v13       #j:I
    :goto_60
    if-ge v9, v13, :cond_84

    #@62
    .line 3890
    const/16 v19, 0x21

    #@64
    move-object/from16 v0, p0

    #@66
    move/from16 v1, v19

    #@68
    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(II)I

    #@6b
    move-result v16

    #@6c
    .line 3891
    .local v16, sep:I
    if-gez v16, :cond_70

    #@6e
    move/from16 v16, v13

    #@70
    .line 3892
    :cond_70
    move/from16 v0, v16

    #@72
    if-ge v9, v0, :cond_81

    #@74
    .line 3893
    move-object/from16 v0, p0

    #@76
    move/from16 v1, v16

    #@78
    invoke-virtual {v0, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7b
    move-result-object v19

    #@7c
    move-object/from16 v0, v19

    #@7e
    invoke-virtual {v10, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@81
    .line 3895
    :cond_81
    add-int/lit8 v9, v16, 0x1

    #@83
    .line 3896
    goto :goto_60

    #@84
    .line 3897
    .end local v16           #sep:I
    :cond_84
    add-int/lit8 v9, v13, 0x1

    #@86
    .line 3900
    .end local v13           #j:I
    :cond_86
    const-string/jumbo v19, "type("

    #@89
    const/16 v20, 0x0

    #@8b
    const/16 v21, 0x5

    #@8d
    move-object/from16 v0, p0

    #@8f
    move-object/from16 v1, v19

    #@91
    move/from16 v2, v20

    #@93
    move/from16 v3, v21

    #@95
    invoke-virtual {v0, v9, v1, v2, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@98
    move-result v19

    #@99
    if-eqz v19, :cond_b4

    #@9b
    .line 3901
    const/4 v12, 0x1

    #@9c
    .line 3902
    add-int/lit8 v9, v9, 0x5

    #@9e
    .line 3903
    const/16 v19, 0x29

    #@a0
    move-object/from16 v0, p0

    #@a2
    move/from16 v1, v19

    #@a4
    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(II)I

    #@a7
    move-result v13

    #@a8
    .line 3904
    .restart local v13       #j:I
    move-object/from16 v0, p0

    #@aa
    invoke-virtual {v0, v9, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ad
    move-result-object v19

    #@ae
    move-object/from16 v0, v19

    #@b0
    iput-object v0, v10, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@b2
    .line 3905
    add-int/lit8 v9, v13, 0x1

    #@b4
    .line 3908
    .end local v13           #j:I
    :cond_b4
    const-string/jumbo v19, "launchFlags("

    #@b7
    const/16 v20, 0x0

    #@b9
    const/16 v21, 0xc

    #@bb
    move-object/from16 v0, p0

    #@bd
    move-object/from16 v1, v19

    #@bf
    move/from16 v2, v20

    #@c1
    move/from16 v3, v21

    #@c3
    invoke-virtual {v0, v9, v1, v2, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@c6
    move-result v19

    #@c7
    if-eqz v19, :cond_ea

    #@c9
    .line 3909
    const/4 v12, 0x1

    #@ca
    .line 3910
    add-int/lit8 v9, v9, 0xc

    #@cc
    .line 3911
    const/16 v19, 0x29

    #@ce
    move-object/from16 v0, p0

    #@d0
    move/from16 v1, v19

    #@d2
    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(II)I

    #@d5
    move-result v13

    #@d6
    .line 3912
    .restart local v13       #j:I
    move-object/from16 v0, p0

    #@d8
    invoke-virtual {v0, v9, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@db
    move-result-object v19

    #@dc
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@df
    move-result-object v19

    #@e0
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    #@e3
    move-result v19

    #@e4
    move/from16 v0, v19

    #@e6
    iput v0, v10, Landroid/content/Intent;->mFlags:I

    #@e8
    .line 3913
    add-int/lit8 v9, v13, 0x1

    #@ea
    .line 3916
    .end local v13           #j:I
    :cond_ea
    const-string v19, "component("

    #@ec
    const/16 v20, 0x0

    #@ee
    const/16 v21, 0xa

    #@f0
    move-object/from16 v0, p0

    #@f2
    move-object/from16 v1, v19

    #@f4
    move/from16 v2, v20

    #@f6
    move/from16 v3, v21

    #@f8
    invoke-virtual {v0, v9, v1, v2, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@fb
    move-result v19

    #@fc
    if-eqz v19, :cond_13a

    #@fe
    .line 3917
    const/4 v12, 0x1

    #@ff
    .line 3918
    add-int/lit8 v9, v9, 0xa

    #@101
    .line 3919
    const/16 v19, 0x29

    #@103
    move-object/from16 v0, p0

    #@105
    move/from16 v1, v19

    #@107
    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(II)I

    #@10a
    move-result v13

    #@10b
    .line 3920
    .restart local v13       #j:I
    const/16 v19, 0x21

    #@10d
    move-object/from16 v0, p0

    #@10f
    move/from16 v1, v19

    #@111
    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(II)I

    #@114
    move-result v16

    #@115
    .line 3921
    .restart local v16       #sep:I
    if-ltz v16, :cond_138

    #@117
    move/from16 v0, v16

    #@119
    if-ge v0, v13, :cond_138

    #@11b
    .line 3922
    move-object/from16 v0, p0

    #@11d
    move/from16 v1, v16

    #@11f
    invoke-virtual {v0, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@122
    move-result-object v15

    #@123
    .line 3923
    .local v15, pkg:Ljava/lang/String;
    add-int/lit8 v19, v16, 0x1

    #@125
    move-object/from16 v0, p0

    #@127
    move/from16 v1, v19

    #@129
    invoke-virtual {v0, v1, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@12c
    move-result-object v7

    #@12d
    .line 3924
    .local v7, cls:Ljava/lang/String;
    new-instance v19, Landroid/content/ComponentName;

    #@12f
    move-object/from16 v0, v19

    #@131
    invoke-direct {v0, v15, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@134
    move-object/from16 v0, v19

    #@136
    iput-object v0, v10, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@138
    .line 3926
    .end local v7           #cls:Ljava/lang/String;
    .end local v15           #pkg:Ljava/lang/String;
    :cond_138
    add-int/lit8 v9, v13, 0x1

    #@13a
    .line 3929
    .end local v13           #j:I
    .end local v16           #sep:I
    :cond_13a
    const-string v19, "extras("

    #@13c
    const/16 v20, 0x0

    #@13e
    const/16 v21, 0x7

    #@140
    move-object/from16 v0, p0

    #@142
    move-object/from16 v1, v19

    #@144
    move/from16 v2, v20

    #@146
    move/from16 v3, v21

    #@148
    invoke-virtual {v0, v9, v1, v2, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@14b
    move-result v19

    #@14c
    if-eqz v19, :cond_217

    #@14e
    .line 3930
    const/4 v12, 0x1

    #@14f
    .line 3931
    add-int/lit8 v9, v9, 0x7

    #@151
    .line 3933
    const/16 v19, 0x29

    #@153
    move-object/from16 v0, p0

    #@155
    move/from16 v1, v19

    #@157
    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(II)I

    #@15a
    move-result v6

    #@15b
    .line 3934
    .local v6, closeParen:I
    const/16 v19, -0x1

    #@15d
    move/from16 v0, v19

    #@15f
    if-ne v6, v0, :cond_171

    #@161
    new-instance v19, Ljava/net/URISyntaxException;

    #@163
    const-string v20, "EXTRA missing trailing \')\'"

    #@165
    move-object/from16 v0, v19

    #@167
    move-object/from16 v1, p0

    #@169
    move-object/from16 v2, v20

    #@16b
    invoke-direct {v0, v1, v2, v9}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    #@16e
    throw v19

    #@16f
    .line 3998
    .local v5, ch:C
    .restart local v13       #j:I
    .local v14, key:Ljava/lang/String;
    .local v17, type:C
    .local v18, value:Ljava/lang/String;
    :cond_16f
    add-int/lit8 v9, v9, 0x1

    #@171
    .line 3937
    .end local v5           #ch:C
    .end local v13           #j:I
    .end local v14           #key:Ljava/lang/String;
    .end local v17           #type:C
    .end local v18           #value:Ljava/lang/String;
    :cond_171
    if-ge v9, v6, :cond_217

    #@173
    .line 3939
    const/16 v19, 0x3d

    #@175
    move-object/from16 v0, p0

    #@177
    move/from16 v1, v19

    #@179
    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(II)I

    #@17c
    move-result v13

    #@17d
    .line 3940
    .restart local v13       #j:I
    add-int/lit8 v19, v9, 0x1

    #@17f
    move/from16 v0, v19

    #@181
    if-le v13, v0, :cond_185

    #@183
    if-lt v9, v6, :cond_193

    #@185
    .line 3941
    :cond_185
    new-instance v19, Ljava/net/URISyntaxException;

    #@187
    const-string v20, "EXTRA missing \'=\'"

    #@189
    move-object/from16 v0, v19

    #@18b
    move-object/from16 v1, p0

    #@18d
    move-object/from16 v2, v20

    #@18f
    invoke-direct {v0, v1, v2, v9}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    #@192
    throw v19

    #@193
    .line 3943
    :cond_193
    move-object/from16 v0, p0

    #@195
    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    #@198
    move-result v17

    #@199
    .line 3944
    .restart local v17       #type:C
    add-int/lit8 v9, v9, 0x1

    #@19b
    .line 3945
    move-object/from16 v0, p0

    #@19d
    invoke-virtual {v0, v9, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1a0
    move-result-object v14

    #@1a1
    .line 3946
    .restart local v14       #key:Ljava/lang/String;
    add-int/lit8 v9, v13, 0x1

    #@1a3
    .line 3949
    const/16 v19, 0x21

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    move/from16 v1, v19

    #@1a9
    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(II)I

    #@1ac
    move-result v13

    #@1ad
    .line 3950
    const/16 v19, -0x1

    #@1af
    move/from16 v0, v19

    #@1b1
    if-eq v13, v0, :cond_1b5

    #@1b3
    if-lt v13, v6, :cond_1b6

    #@1b5
    :cond_1b5
    move v13, v6

    #@1b6
    .line 3951
    :cond_1b6
    if-lt v9, v13, :cond_1c6

    #@1b8
    new-instance v19, Ljava/net/URISyntaxException;

    #@1ba
    const-string v20, "EXTRA missing \'!\'"

    #@1bc
    move-object/from16 v0, v19

    #@1be
    move-object/from16 v1, p0

    #@1c0
    move-object/from16 v2, v20

    #@1c2
    invoke-direct {v0, v1, v2, v9}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    #@1c5
    throw v19

    #@1c6
    .line 3952
    :cond_1c6
    move-object/from16 v0, p0

    #@1c8
    invoke-virtual {v0, v9, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1cb
    move-result-object v18

    #@1cc
    .line 3953
    .restart local v18       #value:Ljava/lang/String;
    move v9, v13

    #@1cd
    .line 3956
    iget-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@1cf
    move-object/from16 v19, v0

    #@1d1
    if-nez v19, :cond_1dc

    #@1d3
    new-instance v19, Landroid/os/Bundle;

    #@1d5
    invoke-direct/range {v19 .. v19}, Landroid/os/Bundle;-><init>()V

    #@1d8
    move-object/from16 v0, v19

    #@1da
    iput-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@1dc
    .line 3960
    :cond_1dc
    sparse-switch v17, :sswitch_data_2f2

    #@1df
    .line 3989
    :try_start_1df
    new-instance v19, Ljava/net/URISyntaxException;

    #@1e1
    const-string v20, "EXTRA has unknown type"

    #@1e3
    move-object/from16 v0, v19

    #@1e5
    move-object/from16 v1, p0

    #@1e7
    move-object/from16 v2, v20

    #@1e9
    invoke-direct {v0, v1, v2, v9}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    #@1ec
    throw v19
    :try_end_1ed
    .catch Ljava/lang/NumberFormatException; {:try_start_1df .. :try_end_1ed} :catch_1ed

    #@1ed
    .line 3991
    :catch_1ed
    move-exception v8

    #@1ee
    .line 3992
    .local v8, e:Ljava/lang/NumberFormatException;
    new-instance v19, Ljava/net/URISyntaxException;

    #@1f0
    const-string v20, "EXTRA value can\'t be parsed"

    #@1f2
    move-object/from16 v0, v19

    #@1f4
    move-object/from16 v1, p0

    #@1f6
    move-object/from16 v2, v20

    #@1f8
    invoke-direct {v0, v1, v2, v9}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    #@1fb
    throw v19

    #@1fc
    .line 3962
    .end local v8           #e:Ljava/lang/NumberFormatException;
    :sswitch_1fc
    :try_start_1fc
    iget-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@1fe
    move-object/from16 v19, v0

    #@200
    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@203
    move-result-object v20

    #@204
    move-object/from16 v0, v19

    #@206
    move-object/from16 v1, v20

    #@208
    invoke-virtual {v0, v14, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_20b
    .catch Ljava/lang/NumberFormatException; {:try_start_1fc .. :try_end_20b} :catch_1ed

    #@20b
    .line 3995
    :goto_20b
    move-object/from16 v0, p0

    #@20d
    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    #@210
    move-result v5

    #@211
    .line 3996
    .restart local v5       #ch:C
    const/16 v19, 0x29

    #@213
    move/from16 v0, v19

    #@215
    if-ne v5, v0, :cond_2c2

    #@217
    .line 4002
    .end local v5           #ch:C
    .end local v6           #closeParen:I
    .end local v13           #j:I
    .end local v14           #key:Ljava/lang/String;
    .end local v17           #type:C
    .end local v18           #value:Ljava/lang/String;
    :cond_217
    if-eqz v12, :cond_2d6

    #@219
    .line 4003
    const/16 v19, 0x0

    #@21b
    move-object/from16 v0, p0

    #@21d
    move/from16 v1, v19

    #@21f
    invoke-virtual {v0, v1, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@222
    move-result-object v19

    #@223
    invoke-static/range {v19 .. v19}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@226
    move-result-object v19

    #@227
    move-object/from16 v0, v19

    #@229
    iput-object v0, v10, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@22b
    .line 4008
    :goto_22b
    iget-object v0, v10, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@22d
    move-object/from16 v19, v0

    #@22f
    if-nez v19, :cond_237

    #@231
    .line 4010
    const-string v19, "android.intent.action.VIEW"

    #@233
    move-object/from16 v0, v19

    #@235
    iput-object v0, v10, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@237
    .line 4017
    .end local v4           #action:Ljava/lang/String;
    .end local v11           #intentFragmentStart:I
    .end local v12           #isIntentFragment:Z
    :cond_237
    :goto_237
    return-object v10

    #@238
    .line 3965
    .restart local v4       #action:Ljava/lang/String;
    .restart local v6       #closeParen:I
    .restart local v11       #intentFragmentStart:I
    .restart local v12       #isIntentFragment:Z
    .restart local v13       #j:I
    .restart local v14       #key:Ljava/lang/String;
    .restart local v17       #type:C
    .restart local v18       #value:Ljava/lang/String;
    :sswitch_238
    :try_start_238
    iget-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@23a
    move-object/from16 v19, v0

    #@23c
    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@23f
    move-result v20

    #@240
    move-object/from16 v0, v19

    #@242
    move/from16 v1, v20

    #@244
    invoke-virtual {v0, v14, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@247
    goto :goto_20b

    #@248
    .line 3968
    :sswitch_248
    iget-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@24a
    move-object/from16 v19, v0

    #@24c
    invoke-static/range {v18 .. v18}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    #@24f
    move-result v20

    #@250
    move-object/from16 v0, v19

    #@252
    move/from16 v1, v20

    #@254
    invoke-virtual {v0, v14, v1}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@257
    goto :goto_20b

    #@258
    .line 3971
    :sswitch_258
    iget-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@25a
    move-object/from16 v19, v0

    #@25c
    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@25f
    move-result-object v20

    #@260
    const/16 v21, 0x0

    #@262
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->charAt(I)C

    #@265
    move-result v20

    #@266
    move-object/from16 v0, v19

    #@268
    move/from16 v1, v20

    #@26a
    invoke-virtual {v0, v14, v1}, Landroid/os/Bundle;->putChar(Ljava/lang/String;C)V

    #@26d
    goto :goto_20b

    #@26e
    .line 3974
    :sswitch_26e
    iget-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@270
    move-object/from16 v19, v0

    #@272
    invoke-static/range {v18 .. v18}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@275
    move-result-wide v20

    #@276
    move-object/from16 v0, v19

    #@278
    move-wide/from16 v1, v20

    #@27a
    invoke-virtual {v0, v14, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    #@27d
    goto :goto_20b

    #@27e
    .line 3977
    :sswitch_27e
    iget-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@280
    move-object/from16 v19, v0

    #@282
    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@285
    move-result v20

    #@286
    move-object/from16 v0, v19

    #@288
    move/from16 v1, v20

    #@28a
    invoke-virtual {v0, v14, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@28d
    goto/16 :goto_20b

    #@28f
    .line 3980
    :sswitch_28f
    iget-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@291
    move-object/from16 v19, v0

    #@293
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@296
    move-result v20

    #@297
    move-object/from16 v0, v19

    #@299
    move/from16 v1, v20

    #@29b
    invoke-virtual {v0, v14, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@29e
    goto/16 :goto_20b

    #@2a0
    .line 3983
    :sswitch_2a0
    iget-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2a2
    move-object/from16 v19, v0

    #@2a4
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@2a7
    move-result-wide v20

    #@2a8
    move-object/from16 v0, v19

    #@2aa
    move-wide/from16 v1, v20

    #@2ac
    invoke-virtual {v0, v14, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@2af
    goto/16 :goto_20b

    #@2b1
    .line 3986
    :sswitch_2b1
    iget-object v0, v10, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2b3
    move-object/from16 v19, v0

    #@2b5
    invoke-static/range {v18 .. v18}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@2b8
    move-result v20

    #@2b9
    move-object/from16 v0, v19

    #@2bb
    move/from16 v1, v20

    #@2bd
    invoke-virtual {v0, v14, v1}, Landroid/os/Bundle;->putShort(Ljava/lang/String;S)V
    :try_end_2c0
    .catch Ljava/lang/NumberFormatException; {:try_start_238 .. :try_end_2c0} :catch_1ed

    #@2c0
    goto/16 :goto_20b

    #@2c2
    .line 3997
    .restart local v5       #ch:C
    :cond_2c2
    const/16 v19, 0x21

    #@2c4
    move/from16 v0, v19

    #@2c6
    if-eq v5, v0, :cond_16f

    #@2c8
    new-instance v19, Ljava/net/URISyntaxException;

    #@2ca
    const-string v20, "EXTRA missing \'!\'"

    #@2cc
    move-object/from16 v0, v19

    #@2ce
    move-object/from16 v1, p0

    #@2d0
    move-object/from16 v2, v20

    #@2d2
    invoke-direct {v0, v1, v2, v9}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    #@2d5
    throw v19

    #@2d6
    .line 4005
    .end local v5           #ch:C
    .end local v6           #closeParen:I
    .end local v13           #j:I
    .end local v14           #key:Ljava/lang/String;
    .end local v17           #type:C
    .end local v18           #value:Ljava/lang/String;
    :cond_2d6
    invoke-static/range {p0 .. p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@2d9
    move-result-object v19

    #@2da
    move-object/from16 v0, v19

    #@2dc
    iput-object v0, v10, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@2de
    goto/16 :goto_22b

    #@2e0
    .line 4014
    .end local v4           #action:Ljava/lang/String;
    .end local v10           #intent:Landroid/content/Intent;
    .end local v11           #intentFragmentStart:I
    .end local v12           #isIntentFragment:Z
    :cond_2e0
    new-instance v10, Landroid/content/Intent;

    #@2e2
    const-string v19, "android.intent.action.VIEW"

    #@2e4
    invoke-static/range {p0 .. p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@2e7
    move-result-object v20

    #@2e8
    move-object/from16 v0, v19

    #@2ea
    move-object/from16 v1, v20

    #@2ec
    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@2ef
    .restart local v10       #intent:Landroid/content/Intent;
    goto/16 :goto_237

    #@2f1
    .line 3960
    nop

    #@2f2
    :sswitch_data_2f2
    .sparse-switch
        0x42 -> :sswitch_238
        0x53 -> :sswitch_1fc
        0x62 -> :sswitch_248
        0x63 -> :sswitch_258
        0x64 -> :sswitch_26e
        0x66 -> :sswitch_27e
        0x69 -> :sswitch_28f
        0x6c -> :sswitch_2a0
        0x73 -> :sswitch_2b1
    .end sparse-switch
.end method

.method private static makeClipItem(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)Landroid/content/ClipData$Item;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter "which"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Landroid/content/ClipData$Item;"
        }
    .end annotation

    #@0
    .prologue
    .local p0, streams:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .local p1, texts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/CharSequence;>;"
    .local p2, htmlTexts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    #@1
    .line 7006
    if-eqz p0, :cond_22

    #@3
    invoke-virtual {p0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v3

    #@7
    check-cast v3, Landroid/net/Uri;

    #@9
    move-object v2, v3

    #@a
    .line 7007
    .local v2, uri:Landroid/net/Uri;
    :goto_a
    if-eqz p1, :cond_24

    #@c
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Ljava/lang/CharSequence;

    #@12
    move-object v1, v3

    #@13
    .line 7008
    .local v1, text:Ljava/lang/CharSequence;
    :goto_13
    if-eqz p2, :cond_26

    #@15
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v3

    #@19
    check-cast v3, Ljava/lang/String;

    #@1b
    move-object v0, v3

    #@1c
    .line 7009
    .local v0, htmlText:Ljava/lang/String;
    :goto_1c
    new-instance v3, Landroid/content/ClipData$Item;

    #@1e
    invoke-direct {v3, v1, v0, v4, v2}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/net/Uri;)V

    #@21
    return-object v3

    #@22
    .end local v0           #htmlText:Ljava/lang/String;
    .end local v1           #text:Ljava/lang/CharSequence;
    .end local v2           #uri:Landroid/net/Uri;
    :cond_22
    move-object v2, v4

    #@23
    .line 7006
    goto :goto_a

    #@24
    .restart local v2       #uri:Landroid/net/Uri;
    :cond_24
    move-object v1, v4

    #@25
    .line 7007
    goto :goto_13

    #@26
    .restart local v1       #text:Ljava/lang/CharSequence;
    :cond_26
    move-object v0, v4

    #@27
    .line 7008
    goto :goto_1c
.end method

.method public static makeMainActivity(Landroid/content/ComponentName;)Landroid/content/Intent;
    .registers 3
    .parameter "mainActivity"

    #@0
    .prologue
    .line 3638
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.MAIN"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 3639
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@a
    .line 3640
    const-string v1, "android.intent.category.LAUNCHER"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@f
    .line 3641
    return-object v0
.end method

.method public static makeMainSelectorActivity(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 5
    .parameter "selectorAction"
    .parameter "selectorCategory"

    #@0
    .prologue
    .line 3668
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v2, "android.intent.action.MAIN"

    #@4
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 3669
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "android.intent.category.LAUNCHER"

    #@9
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 3670
    new-instance v1, Landroid/content/Intent;

    #@e
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@11
    .line 3671
    .local v1, selector:Landroid/content/Intent;
    invoke-virtual {v1, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@14
    .line 3672
    invoke-virtual {v1, p1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@17
    .line 3673
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setSelector(Landroid/content/Intent;)V

    #@1a
    .line 3674
    return-object v0
.end method

.method public static makeRestartActivityTask(Landroid/content/ComponentName;)Landroid/content/Intent;
    .registers 3
    .parameter "mainActivity"

    #@0
    .prologue
    .line 3691
    invoke-static {p0}, Landroid/content/Intent;->makeMainActivity(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    .line 3692
    .local v0, intent:Landroid/content/Intent;
    const v1, 0x10008000

    #@7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@a
    .line 3694
    return-object v0
.end method

.method public static normalizeMimeType(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 6899
    if-nez p0, :cond_4

    #@2
    .line 6900
    const/4 p0, 0x0

    #@3
    .line 6909
    .local v0, semicolonIndex:I
    :cond_3
    :goto_3
    return-object p0

    #@4
    .line 6903
    .end local v0           #semicolonIndex:I
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    #@d
    move-result-object p0

    #@e
    .line 6905
    const/16 v1, 0x3b

    #@10
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    #@13
    move-result v0

    #@14
    .line 6906
    .restart local v0       #semicolonIndex:I
    const/4 v1, -0x1

    #@15
    if-eq v0, v1, :cond_3

    #@17
    .line 6907
    const/4 v1, 0x0

    #@18
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1b
    move-result-object p0

    #@1c
    goto :goto_3
.end method

.method public static parseIntent(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/content/Intent;
    .registers 15
    .parameter "resources"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 6824
    new-instance v3, Landroid/content/Intent;

    #@2
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    #@5
    .line 6826
    .local v3, intent:Landroid/content/Intent;
    sget-object v10, Lcom/android/internal/R$styleable;->Intent:[I

    #@7
    invoke-virtual {p0, p2, v10}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@a
    move-result-object v8

    #@b
    .line 6829
    .local v8, sa:Landroid/content/res/TypedArray;
    const/4 v10, 0x2

    #@c
    invoke-virtual {v8, v10}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@f
    move-result-object v10

    #@10
    invoke-virtual {v3, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@13
    .line 6831
    const/4 v10, 0x3

    #@14
    invoke-virtual {v8, v10}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    .line 6832
    .local v2, data:Ljava/lang/String;
    const/4 v10, 0x1

    #@19
    invoke-virtual {v8, v10}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    .line 6833
    .local v4, mimeType:Ljava/lang/String;
    if-eqz v2, :cond_7c

    #@1f
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@22
    move-result-object v10

    #@23
    :goto_23
    invoke-virtual {v3, v10, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    #@26
    .line 6835
    const/4 v10, 0x0

    #@27
    invoke-virtual {v8, v10}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@2a
    move-result-object v7

    #@2b
    .line 6836
    .local v7, packageName:Ljava/lang/String;
    const/4 v10, 0x4

    #@2c
    invoke-virtual {v8, v10}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    .line 6837
    .local v1, className:Ljava/lang/String;
    if-eqz v7, :cond_3c

    #@32
    if-eqz v1, :cond_3c

    #@34
    .line 6838
    new-instance v10, Landroid/content/ComponentName;

    #@36
    invoke-direct {v10, v7, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    invoke-virtual {v3, v10}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@3c
    .line 6841
    :cond_3c
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@3f
    .line 6843
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@42
    move-result v6

    #@43
    .line 6846
    .local v6, outerDepth:I
    :cond_43
    :goto_43
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@46
    move-result v9

    #@47
    .local v9, type:I
    const/4 v10, 0x1

    #@48
    if-eq v9, v10, :cond_a0

    #@4a
    const/4 v10, 0x3

    #@4b
    if-ne v9, v10, :cond_53

    #@4d
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@50
    move-result v10

    #@51
    if-le v10, v6, :cond_a0

    #@53
    .line 6847
    :cond_53
    const/4 v10, 0x3

    #@54
    if-eq v9, v10, :cond_43

    #@56
    const/4 v10, 0x4

    #@57
    if-eq v9, v10, :cond_43

    #@59
    .line 6851
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@5c
    move-result-object v5

    #@5d
    .line 6852
    .local v5, nodeName:Ljava/lang/String;
    const-string v10, "category"

    #@5f
    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@62
    move-result v10

    #@63
    if-eqz v10, :cond_7e

    #@65
    .line 6853
    sget-object v10, Lcom/android/internal/R$styleable;->IntentCategory:[I

    #@67
    invoke-virtual {p0, p2, v10}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@6a
    move-result-object v8

    #@6b
    .line 6855
    const/4 v10, 0x0

    #@6c
    invoke-virtual {v8, v10}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@6f
    move-result-object v0

    #@70
    .line 6856
    .local v0, cat:Ljava/lang/String;
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@73
    .line 6858
    if-eqz v0, :cond_78

    #@75
    .line 6859
    invoke-virtual {v3, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@78
    .line 6861
    :cond_78
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@7b
    goto :goto_43

    #@7c
    .line 6833
    .end local v0           #cat:Ljava/lang/String;
    .end local v1           #className:Ljava/lang/String;
    .end local v5           #nodeName:Ljava/lang/String;
    .end local v6           #outerDepth:I
    .end local v7           #packageName:Ljava/lang/String;
    .end local v9           #type:I
    :cond_7c
    const/4 v10, 0x0

    #@7d
    goto :goto_23

    #@7e
    .line 6863
    .restart local v1       #className:Ljava/lang/String;
    .restart local v5       #nodeName:Ljava/lang/String;
    .restart local v6       #outerDepth:I
    .restart local v7       #packageName:Ljava/lang/String;
    .restart local v9       #type:I
    :cond_7e
    const-string v10, "extra"

    #@80
    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@83
    move-result v10

    #@84
    if-eqz v10, :cond_9c

    #@86
    .line 6864
    iget-object v10, v3, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@88
    if-nez v10, :cond_91

    #@8a
    .line 6865
    new-instance v10, Landroid/os/Bundle;

    #@8c
    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    #@8f
    iput-object v10, v3, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@91
    .line 6867
    :cond_91
    const-string v10, "extra"

    #@93
    iget-object v11, v3, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@95
    invoke-virtual {p0, v10, p2, v11}, Landroid/content/res/Resources;->parseBundleExtra(Ljava/lang/String;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    #@98
    .line 6868
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@9b
    goto :goto_43

    #@9c
    .line 6871
    :cond_9c
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@9f
    goto :goto_43

    #@a0
    .line 6875
    .end local v5           #nodeName:Ljava/lang/String;
    :cond_a0
    return-object v3
.end method

.method public static parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    .registers 15
    .parameter "uri"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;
        }
    .end annotation

    #@0
    .prologue
    .line 3728
    const/4 v5, 0x0

    #@1
    .line 3731
    .local v5, i:I
    and-int/lit8 v11, p1, 0x1

    #@3
    if-eqz v11, :cond_30

    #@5
    .line 3732
    :try_start_5
    const-string v11, "intent:"

    #@7
    invoke-virtual {p0, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@a
    move-result v11

    #@b
    if-nez v11, :cond_30

    #@d
    .line 3733
    new-instance v6, Landroid/content/Intent;

    #@f
    const-string v11, "android.intent.action.VIEW"

    #@11
    invoke-direct {v6, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5 .. :try_end_14} :catch_27

    #@14
    .line 3735
    .local v6, intent:Landroid/content/Intent;
    :try_start_14
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@17
    move-result-object v11

    #@18
    invoke-virtual {v6, v11}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
    :try_end_1b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_14 .. :try_end_1b} :catch_1c
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_14 .. :try_end_1b} :catch_27

    #@1b
    .line 3857
    .end local v6           #intent:Landroid/content/Intent;
    :cond_1b
    :goto_1b
    return-object v6

    #@1c
    .line 3736
    .restart local v6       #intent:Landroid/content/Intent;
    :catch_1c
    move-exception v3

    #@1d
    .line 3737
    .local v3, e:Ljava/lang/IllegalArgumentException;
    :try_start_1d
    new-instance v11, Ljava/net/URISyntaxException;

    #@1f
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    #@22
    move-result-object v12

    #@23
    invoke-direct {v11, p0, v12}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    throw v11
    :try_end_27
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1d .. :try_end_27} :catch_27

    #@27
    .line 3859
    .end local v3           #e:Ljava/lang/IllegalArgumentException;
    .end local v6           #intent:Landroid/content/Intent;
    :catch_27
    move-exception v3

    #@28
    .line 3860
    .local v3, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v11, Ljava/net/URISyntaxException;

    #@2a
    const-string v12, "illegal Intent URI format"

    #@2c
    invoke-direct {v11, p0, v12, v5}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    #@2f
    throw v11

    #@30
    .line 3744
    .end local v3           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_30
    :try_start_30
    const-string v11, "#"

    #@32
    invoke-virtual {p0, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@35
    move-result v5

    #@36
    .line 3745
    const/4 v11, -0x1

    #@37
    if-ne v5, v11, :cond_45

    #@39
    new-instance v6, Landroid/content/Intent;

    #@3b
    const-string v11, "android.intent.action.VIEW"

    #@3d
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@40
    move-result-object v12

    #@41
    invoke-direct {v6, v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@44
    goto :goto_1b

    #@45
    .line 3748
    :cond_45
    const-string v11, "#Intent;"

    #@47
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@4a
    move-result v11

    #@4b
    if-nez v11, :cond_52

    #@4d
    invoke-static {p0}, Landroid/content/Intent;->getIntentOld(Ljava/lang/String;)Landroid/content/Intent;

    #@50
    move-result-object v6

    #@51
    goto :goto_1b

    #@52
    .line 3751
    :cond_52
    new-instance v6, Landroid/content/Intent;

    #@54
    const-string v11, "android.intent.action.VIEW"

    #@56
    invoke-direct {v6, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@59
    .line 3752
    .restart local v6       #intent:Landroid/content/Intent;
    move-object v1, v6

    #@5a
    .line 3755
    .local v1, baseIntent:Landroid/content/Intent;
    if-ltz v5, :cond_9b

    #@5c
    const/4 v11, 0x0

    #@5d
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    .line 3756
    .local v2, data:Ljava/lang/String;
    :goto_61
    const/4 v8, 0x0

    #@62
    .line 3757
    .local v8, scheme:Ljava/lang/String;
    const-string v11, "#Intent;"

    #@64
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@67
    move-result v11

    #@68
    add-int/2addr v5, v11

    #@69
    .line 3760
    :goto_69
    const-string v11, "end"

    #@6b
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@6e
    move-result v11

    #@6f
    if-nez v11, :cond_1cc

    #@71
    .line 3761
    const/16 v11, 0x3d

    #@73
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->indexOf(II)I

    #@76
    move-result v4

    #@77
    .line 3762
    .local v4, eq:I
    if-gez v4, :cond_7b

    #@79
    add-int/lit8 v4, v5, -0x1

    #@7b
    .line 3763
    :cond_7b
    const/16 v11, 0x3b

    #@7d
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->indexOf(II)I

    #@80
    move-result v9

    #@81
    .line 3764
    .local v9, semi:I
    if-ge v4, v9, :cond_9d

    #@83
    add-int/lit8 v11, v4, 0x1

    #@85
    invoke-virtual {p0, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@88
    move-result-object v11

    #@89
    invoke-static {v11}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@8c
    move-result-object v10

    #@8d
    .line 3767
    .local v10, value:Ljava/lang/String;
    :goto_8d
    const-string v11, "action="

    #@8f
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@92
    move-result v11

    #@93
    if-eqz v11, :cond_a0

    #@95
    .line 3768
    invoke-virtual {v6, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@98
    .line 3831
    :goto_98
    add-int/lit8 v5, v9, 0x1

    #@9a
    .line 3832
    goto :goto_69

    #@9b
    .line 3755
    .end local v2           #data:Ljava/lang/String;
    .end local v4           #eq:I
    .end local v8           #scheme:Ljava/lang/String;
    .end local v9           #semi:I
    .end local v10           #value:Ljava/lang/String;
    :cond_9b
    const/4 v2, 0x0

    #@9c
    goto :goto_61

    #@9d
    .line 3764
    .restart local v2       #data:Ljava/lang/String;
    .restart local v4       #eq:I
    .restart local v8       #scheme:Ljava/lang/String;
    .restart local v9       #semi:I
    :cond_9d
    const-string v10, ""

    #@9f
    goto :goto_8d

    #@a0
    .line 3772
    .restart local v10       #value:Ljava/lang/String;
    :cond_a0
    const-string v11, "category="

    #@a2
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@a5
    move-result v11

    #@a6
    if-eqz v11, :cond_ac

    #@a8
    .line 3773
    invoke-virtual {v6, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@ab
    goto :goto_98

    #@ac
    .line 3777
    :cond_ac
    const-string/jumbo v11, "type="

    #@af
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@b2
    move-result v11

    #@b3
    if-eqz v11, :cond_b8

    #@b5
    .line 3778
    iput-object v10, v6, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@b7
    goto :goto_98

    #@b8
    .line 3782
    :cond_b8
    const-string/jumbo v11, "launchFlags="

    #@bb
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@be
    move-result v11

    #@bf
    if-eqz v11, :cond_cc

    #@c1
    .line 3783
    invoke-static {v10}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@c4
    move-result-object v11

    #@c5
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@c8
    move-result v11

    #@c9
    iput v11, v6, Landroid/content/Intent;->mFlags:I

    #@cb
    goto :goto_98

    #@cc
    .line 3787
    :cond_cc
    const-string/jumbo v11, "package="

    #@cf
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@d2
    move-result v11

    #@d3
    if-eqz v11, :cond_d8

    #@d5
    .line 3788
    iput-object v10, v6, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@d7
    goto :goto_98

    #@d8
    .line 3792
    :cond_d8
    const-string v11, "component="

    #@da
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@dd
    move-result v11

    #@de
    if-eqz v11, :cond_e7

    #@e0
    .line 3793
    invoke-static {v10}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@e3
    move-result-object v11

    #@e4
    iput-object v11, v6, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@e6
    goto :goto_98

    #@e7
    .line 3797
    :cond_e7
    const-string/jumbo v11, "scheme="

    #@ea
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@ed
    move-result v11

    #@ee
    if-eqz v11, :cond_f2

    #@f0
    .line 3798
    move-object v8, v10

    #@f1
    goto :goto_98

    #@f2
    .line 3802
    :cond_f2
    const-string/jumbo v11, "sourceBounds="

    #@f5
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@f8
    move-result v11

    #@f9
    if-eqz v11, :cond_102

    #@fb
    .line 3803
    invoke-static {v10}, Landroid/graphics/Rect;->unflattenFromString(Ljava/lang/String;)Landroid/graphics/Rect;

    #@fe
    move-result-object v11

    #@ff
    iput-object v11, v6, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@101
    goto :goto_98

    #@102
    .line 3807
    :cond_102
    add-int/lit8 v11, v5, 0x3

    #@104
    if-ne v9, v11, :cond_114

    #@106
    const-string v11, "SEL"

    #@108
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@10b
    move-result v11

    #@10c
    if-eqz v11, :cond_114

    #@10e
    .line 3808
    new-instance v6, Landroid/content/Intent;

    #@110
    .end local v6           #intent:Landroid/content/Intent;
    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    #@113
    .restart local v6       #intent:Landroid/content/Intent;
    goto :goto_98

    #@114
    .line 3813
    :cond_114
    add-int/lit8 v11, v5, 0x2

    #@116
    invoke-virtual {p0, v11, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@119
    move-result-object v11

    #@11a
    invoke-static {v11}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@11d
    move-result-object v7

    #@11e
    .line 3815
    .local v7, key:Ljava/lang/String;
    iget-object v11, v6, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@120
    if-nez v11, :cond_129

    #@122
    new-instance v11, Landroid/os/Bundle;

    #@124
    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    #@127
    iput-object v11, v6, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@129
    .line 3816
    :cond_129
    iget-object v0, v6, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@12b
    .line 3818
    .local v0, b:Landroid/os/Bundle;
    const-string v11, "S."

    #@12d
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@130
    move-result v11

    #@131
    if-eqz v11, :cond_138

    #@133
    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@136
    goto/16 :goto_98

    #@138
    .line 3819
    :cond_138
    const-string v11, "B."

    #@13a
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@13d
    move-result v11

    #@13e
    if-eqz v11, :cond_149

    #@140
    invoke-static {v10}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@143
    move-result v11

    #@144
    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@147
    goto/16 :goto_98

    #@149
    .line 3820
    :cond_149
    const-string v11, "b."

    #@14b
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@14e
    move-result v11

    #@14f
    if-eqz v11, :cond_15a

    #@151
    invoke-static {v10}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    #@154
    move-result v11

    #@155
    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@158
    goto/16 :goto_98

    #@15a
    .line 3821
    :cond_15a
    const-string v11, "c."

    #@15c
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@15f
    move-result v11

    #@160
    if-eqz v11, :cond_16c

    #@162
    const/4 v11, 0x0

    #@163
    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    #@166
    move-result v11

    #@167
    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->putChar(Ljava/lang/String;C)V

    #@16a
    goto/16 :goto_98

    #@16c
    .line 3822
    :cond_16c
    const-string v11, "d."

    #@16e
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@171
    move-result v11

    #@172
    if-eqz v11, :cond_17d

    #@174
    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@177
    move-result-wide v11

    #@178
    invoke-virtual {v0, v7, v11, v12}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    #@17b
    goto/16 :goto_98

    #@17d
    .line 3823
    :cond_17d
    const-string v11, "f."

    #@17f
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@182
    move-result v11

    #@183
    if-eqz v11, :cond_18e

    #@185
    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@188
    move-result v11

    #@189
    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@18c
    goto/16 :goto_98

    #@18e
    .line 3824
    :cond_18e
    const-string v11, "i."

    #@190
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@193
    move-result v11

    #@194
    if-eqz v11, :cond_19f

    #@196
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@199
    move-result v11

    #@19a
    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@19d
    goto/16 :goto_98

    #@19f
    .line 3825
    :cond_19f
    const-string/jumbo v11, "l."

    #@1a2
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@1a5
    move-result v11

    #@1a6
    if-eqz v11, :cond_1b1

    #@1a8
    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@1ab
    move-result-wide v11

    #@1ac
    invoke-virtual {v0, v7, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@1af
    goto/16 :goto_98

    #@1b1
    .line 3826
    :cond_1b1
    const-string/jumbo v11, "s."

    #@1b4
    invoke-virtual {p0, v11, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@1b7
    move-result v11

    #@1b8
    if-eqz v11, :cond_1c3

    #@1ba
    invoke-static {v10}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@1bd
    move-result v11

    #@1be
    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->putShort(Ljava/lang/String;S)V

    #@1c1
    goto/16 :goto_98

    #@1c3
    .line 3827
    :cond_1c3
    new-instance v11, Ljava/net/URISyntaxException;

    #@1c5
    const-string/jumbo v12, "unknown EXTRA type"

    #@1c8
    invoke-direct {v11, p0, v12, v5}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    #@1cb
    throw v11

    #@1cc
    .line 3834
    .end local v0           #b:Landroid/os/Bundle;
    .end local v4           #eq:I
    .end local v7           #key:Ljava/lang/String;
    .end local v9           #semi:I
    .end local v10           #value:Ljava/lang/String;
    :cond_1cc
    if-eq v6, v1, :cond_1d2

    #@1ce
    .line 3836
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setSelector(Landroid/content/Intent;)V

    #@1d1
    .line 3837
    move-object v6, v1

    #@1d2
    .line 3840
    :cond_1d2
    if-eqz v2, :cond_1b

    #@1d4
    .line 3841
    const-string v11, "intent:"

    #@1d6
    invoke-virtual {v2, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1d9
    move-result v11

    #@1da
    if-eqz v11, :cond_1fa

    #@1dc
    .line 3842
    const/4 v11, 0x7

    #@1dd
    invoke-virtual {v2, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1e0
    move-result-object v2

    #@1e1
    .line 3843
    if-eqz v8, :cond_1fa

    #@1e3
    .line 3844
    new-instance v11, Ljava/lang/StringBuilder;

    #@1e5
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1e8
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v11

    #@1ec
    const/16 v12, 0x3a

    #@1ee
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v11

    #@1f2
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v11

    #@1f6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f9
    move-result-object v2

    #@1fa
    .line 3848
    :cond_1fa
    invoke-virtual {v2}, Ljava/lang/String;->length()I
    :try_end_1fd
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_30 .. :try_end_1fd} :catch_27

    #@1fd
    move-result v11

    #@1fe
    if-lez v11, :cond_1b

    #@200
    .line 3850
    :try_start_200
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@203
    move-result-object v11

    #@204
    iput-object v11, v6, Landroid/content/Intent;->mData:Landroid/net/Uri;
    :try_end_206
    .catch Ljava/lang/IllegalArgumentException; {:try_start_200 .. :try_end_206} :catch_208
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_200 .. :try_end_206} :catch_27

    #@206
    goto/16 :goto_1b

    #@208
    .line 3851
    :catch_208
    move-exception v3

    #@209
    .line 3852
    .local v3, e:Ljava/lang/IllegalArgumentException;
    :try_start_209
    new-instance v11, Ljava/net/URISyntaxException;

    #@20b
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    #@20e
    move-result-object v12

    #@20f
    invoke-direct {v11, p0, v12}, Ljava/net/URISyntaxException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@212
    throw v11
    :try_end_213
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_209 .. :try_end_213} :catch_27
.end method

.method private toUriInner(Ljava/lang/StringBuilder;Ljava/lang/String;I)V
    .registers 13
    .parameter "uri"
    .parameter "scheme"
    .parameter "flags"

    #@0
    .prologue
    const/16 v8, 0x3b

    #@2
    .line 6654
    if-eqz p2, :cond_12

    #@4
    .line 6655
    const-string/jumbo v5, "scheme="

    #@7
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v5

    #@f
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 6657
    :cond_12
    iget-object v5, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@14
    if-eqz v5, :cond_29

    #@16
    .line 6658
    const-string v5, "action="

    #@18
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v5

    #@1c
    iget-object v6, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@1e
    invoke-static {v6}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@21
    move-result-object v6

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@29
    .line 6660
    :cond_29
    iget-object v5, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@2b
    if-eqz v5, :cond_51

    #@2d
    .line 6661
    iget-object v5, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@2f
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@32
    move-result-object v2

    #@33
    .local v2, i$:Ljava/util/Iterator;
    :goto_33
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@36
    move-result v5

    #@37
    if-eqz v5, :cond_51

    #@39
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3c
    move-result-object v0

    #@3d
    check-cast v0, Ljava/lang/String;

    #@3f
    .line 6662
    .local v0, category:Ljava/lang/String;
    const-string v5, "category="

    #@41
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@50
    goto :goto_33

    #@51
    .line 6665
    .end local v0           #category:Ljava/lang/String;
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_51
    iget-object v5, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@53
    if-eqz v5, :cond_6b

    #@55
    .line 6666
    const-string/jumbo v5, "type="

    #@58
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    iget-object v6, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@5e
    const-string v7, "/"

    #@60
    invoke-static {v6, v7}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@63
    move-result-object v6

    #@64
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6b
    .line 6668
    :cond_6b
    iget v5, p0, Landroid/content/Intent;->mFlags:I

    #@6d
    if-eqz v5, :cond_83

    #@6f
    .line 6669
    const-string/jumbo v5, "launchFlags=0x"

    #@72
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    iget v6, p0, Landroid/content/Intent;->mFlags:I

    #@78
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@83
    .line 6671
    :cond_83
    iget-object v5, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@85
    if-eqz v5, :cond_9b

    #@87
    .line 6672
    const-string/jumbo v5, "package="

    #@8a
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v5

    #@8e
    iget-object v6, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@90
    invoke-static {v6}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@93
    move-result-object v6

    #@94
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v5

    #@98
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@9b
    .line 6674
    :cond_9b
    iget-object v5, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@9d
    if-eqz v5, :cond_b8

    #@9f
    .line 6675
    const-string v5, "component="

    #@a1
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v5

    #@a5
    iget-object v6, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@a7
    invoke-virtual {v6}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@aa
    move-result-object v6

    #@ab
    const-string v7, "/"

    #@ad
    invoke-static {v6, v7}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b0
    move-result-object v6

    #@b1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v5

    #@b5
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b8
    .line 6678
    :cond_b8
    iget-object v5, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@ba
    if-eqz v5, :cond_d4

    #@bc
    .line 6679
    const-string/jumbo v5, "sourceBounds="

    #@bf
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v5

    #@c3
    iget-object v6, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@c5
    invoke-virtual {v6}, Landroid/graphics/Rect;->flattenToString()Ljava/lang/String;

    #@c8
    move-result-object v6

    #@c9
    invoke-static {v6}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@cc
    move-result-object v6

    #@cd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v5

    #@d1
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@d4
    .line 6683
    :cond_d4
    iget-object v5, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d6
    if-eqz v5, :cond_159

    #@d8
    .line 6684
    iget-object v5, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@da
    invoke-virtual {v5}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@dd
    move-result-object v5

    #@de
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@e1
    move-result-object v2

    #@e2
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_e2
    :goto_e2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@e5
    move-result v5

    #@e6
    if-eqz v5, :cond_159

    #@e8
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@eb
    move-result-object v3

    #@ec
    check-cast v3, Ljava/lang/String;

    #@ee
    .line 6685
    .local v3, key:Ljava/lang/String;
    iget-object v5, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@f0
    invoke-virtual {v5, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@f3
    move-result-object v4

    #@f4
    .line 6686
    .local v4, value:Ljava/lang/Object;
    instance-of v5, v4, Ljava/lang/String;

    #@f6
    if-eqz v5, :cond_11f

    #@f8
    const/16 v1, 0x53

    #@fa
    .line 6698
    .local v1, entryType:C
    :goto_fa
    if-eqz v1, :cond_e2

    #@fc
    .line 6699
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@ff
    .line 6700
    const/16 v5, 0x2e

    #@101
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@104
    .line 6701
    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@107
    move-result-object v5

    #@108
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    .line 6702
    const/16 v5, 0x3d

    #@10d
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@110
    .line 6703
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@113
    move-result-object v5

    #@114
    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@117
    move-result-object v5

    #@118
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    .line 6704
    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@11e
    goto :goto_e2

    #@11f
    .line 6686
    .end local v1           #entryType:C
    :cond_11f
    instance-of v5, v4, Ljava/lang/Boolean;

    #@121
    if-eqz v5, :cond_126

    #@123
    const/16 v1, 0x42

    #@125
    goto :goto_fa

    #@126
    :cond_126
    instance-of v5, v4, Ljava/lang/Byte;

    #@128
    if-eqz v5, :cond_12d

    #@12a
    const/16 v1, 0x62

    #@12c
    goto :goto_fa

    #@12d
    :cond_12d
    instance-of v5, v4, Ljava/lang/Character;

    #@12f
    if-eqz v5, :cond_134

    #@131
    const/16 v1, 0x63

    #@133
    goto :goto_fa

    #@134
    :cond_134
    instance-of v5, v4, Ljava/lang/Double;

    #@136
    if-eqz v5, :cond_13b

    #@138
    const/16 v1, 0x64

    #@13a
    goto :goto_fa

    #@13b
    :cond_13b
    instance-of v5, v4, Ljava/lang/Float;

    #@13d
    if-eqz v5, :cond_142

    #@13f
    const/16 v1, 0x66

    #@141
    goto :goto_fa

    #@142
    :cond_142
    instance-of v5, v4, Ljava/lang/Integer;

    #@144
    if-eqz v5, :cond_149

    #@146
    const/16 v1, 0x69

    #@148
    goto :goto_fa

    #@149
    :cond_149
    instance-of v5, v4, Ljava/lang/Long;

    #@14b
    if-eqz v5, :cond_150

    #@14d
    const/16 v1, 0x6c

    #@14f
    goto :goto_fa

    #@150
    :cond_150
    instance-of v5, v4, Ljava/lang/Short;

    #@152
    if-eqz v5, :cond_157

    #@154
    const/16 v1, 0x73

    #@156
    goto :goto_fa

    #@157
    :cond_157
    const/4 v1, 0x0

    #@158
    goto :goto_fa

    #@159
    .line 6708
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #key:Ljava/lang/String;
    .end local v4           #value:Ljava/lang/Object;
    :cond_159
    return-void
.end method


# virtual methods
.method public addCategory(Ljava/lang/String;)Landroid/content/Intent;
    .registers 4
    .parameter "category"

    #@0
    .prologue
    .line 5082
    iget-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5083
    new-instance v0, Ljava/util/HashSet;

    #@6
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@b
    .line 5085
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@d
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@14
    .line 5086
    return-object p0
.end method

.method public addFlags(I)Landroid/content/Intent;
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 5985
    iget v0, p0, Landroid/content/Intent;->mFlags:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/content/Intent;->mFlags:I

    #@5
    .line 5986
    return-object p0
.end method

.method public clone()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 3509
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@5
    return-object v0
.end method

.method public cloneFilter()Landroid/content/Intent;
    .registers 3

    #@0
    .prologue
    .line 3528
    new-instance v0, Landroid/content/Intent;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;Z)V

    #@6
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 6711
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v0}, Landroid/os/Bundle;->describeContents()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public fillIn(Landroid/content/Intent;I)I
    .registers 8
    .parameter "other"
    .parameter "flags"

    #@0
    .prologue
    .line 6211
    const/4 v0, 0x0

    #@1
    .line 6212
    .local v0, changes:I
    iget-object v3, p1, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@3
    if-eqz v3, :cond_13

    #@5
    iget-object v3, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@7
    if-eqz v3, :cond_d

    #@9
    and-int/lit8 v3, p2, 0x1

    #@b
    if-eqz v3, :cond_13

    #@d
    .line 6214
    :cond_d
    iget-object v3, p1, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@f
    iput-object v3, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@11
    .line 6215
    or-int/lit8 v0, v0, 0x1

    #@13
    .line 6217
    :cond_13
    iget-object v3, p1, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@15
    if-nez v3, :cond_1b

    #@17
    iget-object v3, p1, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@19
    if-eqz v3, :cond_31

    #@1b
    :cond_1b
    iget-object v3, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@1d
    if-nez v3, :cond_23

    #@1f
    iget-object v3, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@21
    if-eqz v3, :cond_27

    #@23
    :cond_23
    and-int/lit8 v3, p2, 0x2

    #@25
    if-eqz v3, :cond_31

    #@27
    .line 6220
    :cond_27
    iget-object v3, p1, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@29
    iput-object v3, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@2b
    .line 6221
    iget-object v3, p1, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@2d
    iput-object v3, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@2f
    .line 6222
    or-int/lit8 v0, v0, 0x2

    #@31
    .line 6224
    :cond_31
    iget-object v3, p1, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@33
    if-eqz v3, :cond_4c

    #@35
    iget-object v3, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@37
    if-eqz v3, :cond_3d

    #@39
    and-int/lit8 v3, p2, 0x4

    #@3b
    if-eqz v3, :cond_4c

    #@3d
    .line 6226
    :cond_3d
    iget-object v3, p1, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@3f
    if-eqz v3, :cond_4a

    #@41
    .line 6227
    new-instance v3, Ljava/util/HashSet;

    #@43
    iget-object v4, p1, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@45
    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@48
    iput-object v3, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@4a
    .line 6229
    :cond_4a
    or-int/lit8 v0, v0, 0x4

    #@4c
    .line 6231
    :cond_4c
    iget-object v3, p1, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@4e
    if-eqz v3, :cond_62

    #@50
    iget-object v3, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@52
    if-eqz v3, :cond_58

    #@54
    and-int/lit8 v3, p2, 0x10

    #@56
    if-eqz v3, :cond_62

    #@58
    .line 6234
    :cond_58
    iget-object v3, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@5a
    if-nez v3, :cond_62

    #@5c
    .line 6235
    iget-object v3, p1, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@5e
    iput-object v3, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@60
    .line 6236
    or-int/lit8 v0, v0, 0x10

    #@62
    .line 6241
    :cond_62
    iget-object v3, p1, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@64
    if-eqz v3, :cond_7c

    #@66
    and-int/lit8 v3, p2, 0x40

    #@68
    if-eqz v3, :cond_7c

    #@6a
    .line 6242
    iget-object v3, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@6c
    if-nez v3, :cond_7c

    #@6e
    .line 6243
    new-instance v3, Landroid/content/Intent;

    #@70
    iget-object v4, p1, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@72
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@75
    iput-object v3, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@77
    .line 6244
    const/4 v3, 0x0

    #@78
    iput-object v3, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@7a
    .line 6245
    or-int/lit8 v0, v0, 0x40

    #@7c
    .line 6248
    :cond_7c
    iget-object v3, p1, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@7e
    if-eqz v3, :cond_8e

    #@80
    iget-object v3, p0, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@82
    if-eqz v3, :cond_88

    #@84
    and-int/lit16 v3, p2, 0x80

    #@86
    if-eqz v3, :cond_8e

    #@88
    .line 6250
    :cond_88
    iget-object v3, p1, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@8a
    iput-object v3, p0, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@8c
    .line 6251
    or-int/lit16 v0, v0, 0x80

    #@8e
    .line 6256
    :cond_8e
    iget-object v3, p1, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@90
    if-eqz v3, :cond_9c

    #@92
    and-int/lit8 v3, p2, 0x8

    #@94
    if-eqz v3, :cond_9c

    #@96
    .line 6257
    iget-object v3, p1, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@98
    iput-object v3, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@9a
    .line 6258
    or-int/lit8 v0, v0, 0x8

    #@9c
    .line 6260
    :cond_9c
    iget v3, p0, Landroid/content/Intent;->mFlags:I

    #@9e
    iget v4, p1, Landroid/content/Intent;->mFlags:I

    #@a0
    or-int/2addr v3, v4

    #@a1
    iput v3, p0, Landroid/content/Intent;->mFlags:I

    #@a3
    .line 6261
    iget-object v3, p1, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@a5
    if-eqz v3, :cond_ba

    #@a7
    iget-object v3, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@a9
    if-eqz v3, :cond_af

    #@ab
    and-int/lit8 v3, p2, 0x20

    #@ad
    if-eqz v3, :cond_ba

    #@af
    .line 6263
    :cond_af
    new-instance v3, Landroid/graphics/Rect;

    #@b1
    iget-object v4, p1, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@b3
    invoke-direct {v3, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@b6
    iput-object v3, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@b8
    .line 6264
    or-int/lit8 v0, v0, 0x20

    #@ba
    .line 6266
    :cond_ba
    iget-object v3, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@bc
    if-nez v3, :cond_cc

    #@be
    .line 6267
    iget-object v3, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@c0
    if-eqz v3, :cond_cb

    #@c2
    .line 6268
    new-instance v3, Landroid/os/Bundle;

    #@c4
    iget-object v4, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@c6
    invoke-direct {v3, v4}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@c9
    iput-object v3, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@cb
    .line 6284
    :cond_cb
    :goto_cb
    return v0

    #@cc
    .line 6270
    :cond_cc
    iget-object v3, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@ce
    if-eqz v3, :cond_cb

    #@d0
    .line 6272
    :try_start_d0
    new-instance v2, Landroid/os/Bundle;

    #@d2
    iget-object v3, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d4
    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@d7
    .line 6273
    .local v2, newb:Landroid/os/Bundle;
    iget-object v3, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d9
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    #@dc
    .line 6274
    iput-object v2, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;
    :try_end_de
    .catch Ljava/lang/RuntimeException; {:try_start_d0 .. :try_end_de} :catch_df

    #@de
    goto :goto_cb

    #@df
    .line 6275
    .end local v2           #newb:Landroid/os/Bundle;
    :catch_df
    move-exception v1

    #@e0
    .line 6281
    .local v1, e:Ljava/lang/RuntimeException;
    const-string v3, "Intent"

    #@e2
    const-string v4, "Failure filling in extras"

    #@e4
    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e7
    goto :goto_cb
.end method

.method public filterEquals(Landroid/content/Intent;)Z
    .registers 5
    .parameter "other"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 6340
    if-nez p1, :cond_4

    #@3
    .line 6410
    :cond_3
    :goto_3
    return v0

    #@4
    .line 6343
    :cond_4
    iget-object v1, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@6
    iget-object v2, p1, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@8
    if-eq v1, v2, :cond_18

    #@a
    .line 6344
    iget-object v1, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@c
    if-eqz v1, :cond_7e

    #@e
    .line 6345
    iget-object v1, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@10
    iget-object v2, p1, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_3

    #@18
    .line 6354
    :cond_18
    iget-object v1, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@1a
    iget-object v2, p1, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@1c
    if-eq v1, v2, :cond_2c

    #@1e
    .line 6355
    iget-object v1, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@20
    if-eqz v1, :cond_8a

    #@22
    .line 6356
    iget-object v1, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@24
    iget-object v2, p1, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@26
    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v1

    #@2a
    if-eqz v1, :cond_3

    #@2c
    .line 6365
    :cond_2c
    iget-object v1, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@2e
    iget-object v2, p1, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@30
    if-eq v1, v2, :cond_40

    #@32
    .line 6366
    iget-object v1, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@34
    if-eqz v1, :cond_96

    #@36
    .line 6367
    iget-object v1, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@38
    iget-object v2, p1, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v1

    #@3e
    if-eqz v1, :cond_3

    #@40
    .line 6376
    :cond_40
    iget-object v1, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@42
    iget-object v2, p1, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@44
    if-eq v1, v2, :cond_54

    #@46
    .line 6377
    iget-object v1, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@48
    if-eqz v1, :cond_a2

    #@4a
    .line 6378
    iget-object v1, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@4c
    iget-object v2, p1, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v1

    #@52
    if-eqz v1, :cond_3

    #@54
    .line 6387
    :cond_54
    iget-object v1, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@56
    iget-object v2, p1, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@58
    if-eq v1, v2, :cond_68

    #@5a
    .line 6388
    iget-object v1, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@5c
    if-eqz v1, :cond_ae

    #@5e
    .line 6389
    iget-object v1, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@60
    iget-object v2, p1, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@62
    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v1

    #@66
    if-eqz v1, :cond_3

    #@68
    .line 6398
    :cond_68
    iget-object v1, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@6a
    iget-object v2, p1, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@6c
    if-eq v1, v2, :cond_7c

    #@6e
    .line 6399
    iget-object v1, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@70
    if-eqz v1, :cond_ba

    #@72
    .line 6400
    iget-object v1, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@74
    iget-object v2, p1, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@76
    invoke-virtual {v1, v2}, Ljava/util/HashSet;->equals(Ljava/lang/Object;)Z

    #@79
    move-result v1

    #@7a
    if-eqz v1, :cond_3

    #@7c
    .line 6410
    :cond_7c
    const/4 v0, 0x1

    #@7d
    goto :goto_3

    #@7e
    .line 6349
    :cond_7e
    iget-object v1, p1, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@80
    iget-object v2, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@82
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@85
    move-result v1

    #@86
    if-nez v1, :cond_18

    #@88
    goto/16 :goto_3

    #@8a
    .line 6360
    :cond_8a
    iget-object v1, p1, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@8c
    iget-object v2, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@8e
    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v1

    #@92
    if-nez v1, :cond_2c

    #@94
    goto/16 :goto_3

    #@96
    .line 6371
    :cond_96
    iget-object v1, p1, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@98
    iget-object v2, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@9a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9d
    move-result v1

    #@9e
    if-nez v1, :cond_40

    #@a0
    goto/16 :goto_3

    #@a2
    .line 6382
    :cond_a2
    iget-object v1, p1, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@a4
    iget-object v2, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@a6
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v1

    #@aa
    if-nez v1, :cond_54

    #@ac
    goto/16 :goto_3

    #@ae
    .line 6393
    :cond_ae
    iget-object v1, p1, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@b0
    iget-object v2, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@b2
    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@b5
    move-result v1

    #@b6
    if-nez v1, :cond_68

    #@b8
    goto/16 :goto_3

    #@ba
    .line 6404
    :cond_ba
    iget-object v1, p1, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@bc
    iget-object v2, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@be
    invoke-virtual {v1, v2}, Ljava/util/HashSet;->equals(Ljava/lang/Object;)Z

    #@c1
    move-result v1

    #@c2
    if-nez v1, :cond_7c

    #@c4
    goto/16 :goto_3
.end method

.method public filterHashCode()I
    .registers 3

    #@0
    .prologue
    .line 6422
    const/4 v0, 0x0

    #@1
    .line 6423
    .local v0, code:I
    iget-object v1, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@3
    if-eqz v1, :cond_c

    #@5
    .line 6424
    iget-object v1, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@7
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@a
    move-result v1

    #@b
    add-int/2addr v0, v1

    #@c
    .line 6426
    :cond_c
    iget-object v1, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@e
    if-eqz v1, :cond_17

    #@10
    .line 6427
    iget-object v1, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@12
    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    #@15
    move-result v1

    #@16
    add-int/2addr v0, v1

    #@17
    .line 6429
    :cond_17
    iget-object v1, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@19
    if-eqz v1, :cond_22

    #@1b
    .line 6430
    iget-object v1, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@1d
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@20
    move-result v1

    #@21
    add-int/2addr v0, v1

    #@22
    .line 6432
    :cond_22
    iget-object v1, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@24
    if-eqz v1, :cond_2d

    #@26
    .line 6433
    iget-object v1, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@28
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@2b
    move-result v1

    #@2c
    add-int/2addr v0, v1

    #@2d
    .line 6435
    :cond_2d
    iget-object v1, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@2f
    if-eqz v1, :cond_38

    #@31
    .line 6436
    iget-object v1, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@33
    invoke-virtual {v1}, Landroid/content/ComponentName;->hashCode()I

    #@36
    move-result v1

    #@37
    add-int/2addr v0, v1

    #@38
    .line 6438
    :cond_38
    iget-object v1, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@3a
    if-eqz v1, :cond_43

    #@3c
    .line 6439
    iget-object v1, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@3e
    invoke-virtual {v1}, Ljava/util/HashSet;->hashCode()I

    #@41
    move-result v1

    #@42
    add-int/2addr v0, v1

    #@43
    .line 6441
    :cond_43
    return v0
.end method

.method public getAction()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4031
    iget-object v0, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getBooleanArrayExtra(Ljava/lang/String;)[Z
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4520
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getBooleanExtra(Ljava/lang/String;Z)Z
    .registers 4
    .parameter "name"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 4260
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@a
    move-result p2

    #@b
    goto :goto_4
.end method

.method public getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4660
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getByteArrayExtra(Ljava/lang/String;)[B
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4534
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getByteExtra(Ljava/lang/String;B)B
    .registers 4
    .parameter "name"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 4277
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;B)Ljava/lang/Byte;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    #@e
    move-result p2

    #@f
    goto :goto_4
.end method

.method public getCategories()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4171
    iget-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@2
    return-object v0
.end method

.method public getCharArrayExtra(Ljava/lang/String;)[C
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4562
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getCharExtra(Ljava/lang/String;C)C
    .registers 4
    .parameter "name"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 4311
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getChar(Ljava/lang/String;C)C

    #@a
    move-result p2

    #@b
    goto :goto_4
.end method

.method public getCharSequenceArrayExtra(Ljava/lang/String;)[Ljava/lang/CharSequence;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4646
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getCharSequenceArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 3
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4506
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getCharSequenceArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4408
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getClipData()Landroid/content/ClipData;
    .registers 2

    #@0
    .prologue
    .line 4191
    iget-object v0, p0, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@2
    return-object v0
.end method

.method public getComponent()Landroid/content/ComponentName;
    .registers 2

    #@0
    .prologue
    .line 4766
    iget-object v0, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@2
    return-object v0
.end method

.method public getData()Landroid/net/Uri;
    .registers 2

    #@0
    .prologue
    .line 4046
    iget-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method public getDataString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4054
    iget-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@6
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getDoubleArrayExtra(Ljava/lang/String;)[D
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4618
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getDoubleArray(Ljava/lang/String;)[D

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getDoubleExtra(Ljava/lang/String;D)D
    .registers 5
    .parameter "name"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 4379
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return-wide p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    #@a
    move-result-wide p2

    #@b
    goto :goto_4
.end method

.method public getExtra(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "name"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 4244
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .parameter "name"
    .parameter "defaultValue"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 4698
    move-object v0, p2

    #@1
    .line 4699
    .local v0, result:Ljava/lang/Object;
    iget-object v2, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@3
    if-eqz v2, :cond_e

    #@5
    .line 4700
    iget-object v2, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@7
    invoke-virtual {v2, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@a
    move-result-object v1

    #@b
    .line 4701
    .local v1, result2:Ljava/lang/Object;
    if-eqz v1, :cond_e

    #@d
    .line 4702
    move-object v0, v1

    #@e
    .line 4706
    .end local v1           #result2:Ljava/lang/Object;
    :cond_e
    return-object v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .registers 3

    #@0
    .prologue
    .line 4716
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_c

    #@4
    new-instance v0, Landroid/os/Bundle;

    #@6
    iget-object v1, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@b
    :goto_b
    return-object v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public getFlags()I
    .registers 2

    #@0
    .prologue
    .line 4731
    iget v0, p0, Landroid/content/Intent;->mFlags:I

    #@2
    return v0
.end method

.method public getFloatArrayExtra(Ljava/lang/String;)[F
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4604
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getFloatExtra(Ljava/lang/String;F)F
    .registers 4
    .parameter "name"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 4362
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    #@a
    move-result p2

    #@b
    goto :goto_4
.end method

.method public getIBinderExtra(Ljava/lang/String;)Landroid/os/IBinder;
    .registers 3
    .parameter "name"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 4678
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getIBinder(Ljava/lang/String;)Landroid/os/IBinder;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getIntArrayExtra(Ljava/lang/String;)[I
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4576
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getIntExtra(Ljava/lang/String;I)I
    .registers 4
    .parameter "name"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 4328
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@a
    move-result p2

    #@b
    goto :goto_4
.end method

.method public getIntegerArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 3
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4478
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getLongArrayExtra(Ljava/lang/String;)[J
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4590
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getLongExtra(Ljava/lang/String;J)J
    .registers 5
    .parameter "name"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 4345
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return-wide p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    #@a
    move-result-wide p2

    #@b
    goto :goto_4
.end method

.method public getPackage()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4751
    iget-object v0, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4436
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 3
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 4450
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
    .registers 3
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 4422
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getScheme()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4070
    iget-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@6
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getSelector()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 4181
    iget-object v0, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@2
    return-object v0
.end method

.method public getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4464
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getShortArrayExtra(Ljava/lang/String;)[S
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4548
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getShortArray(Ljava/lang/String;)[S

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getShortExtra(Ljava/lang/String;S)S
    .registers 4
    .parameter "name"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 4294
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getShort(Ljava/lang/String;S)S

    #@a
    move-result p2

    #@b
    goto :goto_4
.end method

.method public getSourceBounds()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 4775
    iget-object v0, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method public getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4632
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 3
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4492
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getStringExtra(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4394
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4084
    iget-object v0, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public hasCategory(Ljava/lang/String;)Z
    .registers 3
    .parameter "category"

    #@0
    .prologue
    .line 4158
    iget-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@6
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public hasExtra(Ljava/lang/String;)Z
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 4213
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public hasFileDescriptors()Z
    .registers 2

    #@0
    .prologue
    .line 4221
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v0}, Landroid/os/Bundle;->hasFileDescriptors()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isExcludingStopped()Z
    .registers 3

    #@0
    .prologue
    .line 4736
    iget v0, p0, Landroid/content/Intent;->mFlags:I

    #@2
    and-int/lit8 v0, v0, 0x30

    #@4
    const/16 v1, 0x10

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public migrateExtraStreamToClipData()Z
    .registers 16

    #@0
    .prologue
    .line 6922
    iget-object v11, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v11, :cond_e

    #@4
    iget-object v11, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v11}, Landroid/os/Bundle;->isParcelled()Z

    #@9
    move-result v11

    #@a
    if-eqz v11, :cond_e

    #@c
    const/4 v11, 0x0

    #@d
    .line 7001
    :goto_d
    return v11

    #@e
    .line 6925
    :cond_e
    invoke-virtual {p0}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    #@11
    move-result-object v11

    #@12
    if-eqz v11, :cond_16

    #@14
    const/4 v11, 0x0

    #@15
    goto :goto_d

    #@16
    .line 6927
    :cond_16
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 6928
    .local v0, action:Ljava/lang/String;
    const-string v11, "android.intent.action.CHOOSER"

    #@1c
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v11

    #@20
    if-eqz v11, :cond_46

    #@22
    .line 6931
    :try_start_22
    const-string v11, "android.intent.extra.INTENT"

    #@24
    invoke-virtual {p0, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@27
    move-result-object v8

    #@28
    check-cast v8, Landroid/content/Intent;

    #@2a
    .line 6932
    .local v8, target:Landroid/content/Intent;
    if-eqz v8, :cond_44

    #@2c
    invoke-virtual {v8}, Landroid/content/Intent;->migrateExtraStreamToClipData()Z

    #@2f
    move-result v11

    #@30
    if-eqz v11, :cond_44

    #@32
    .line 6935
    invoke-virtual {v8}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    #@35
    move-result-object v11

    #@36
    invoke-virtual {p0, v11}, Landroid/content/Intent;->setClipData(Landroid/content/ClipData;)V

    #@39
    .line 6936
    invoke-virtual {v8}, Landroid/content/Intent;->getFlags()I

    #@3c
    move-result v11

    #@3d
    and-int/lit8 v11, v11, 0x3

    #@3f
    invoke-virtual {p0, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
    :try_end_42
    .catch Ljava/lang/ClassCastException; {:try_start_22 .. :try_end_42} :catch_fe

    #@42
    .line 6938
    const/4 v11, 0x1

    #@43
    goto :goto_d

    #@44
    .line 6940
    :cond_44
    const/4 v11, 0x0

    #@45
    goto :goto_d

    #@46
    .line 6945
    .end local v8           #target:Landroid/content/Intent;
    :cond_46
    const-string v11, "android.intent.action.SEND"

    #@48
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v11

    #@4c
    if-eqz v11, :cond_87

    #@4e
    .line 6947
    :try_start_4e
    const-string v11, "android.intent.extra.STREAM"

    #@50
    invoke-virtual {p0, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@53
    move-result-object v6

    #@54
    check-cast v6, Landroid/net/Uri;

    #@56
    .line 6948
    .local v6, stream:Landroid/net/Uri;
    const-string v11, "android.intent.extra.TEXT"

    #@58
    invoke-virtual {p0, v11}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    #@5b
    move-result-object v9

    #@5c
    .line 6949
    .local v9, text:Ljava/lang/CharSequence;
    const-string v11, "android.intent.extra.HTML_TEXT"

    #@5e
    invoke-virtual {p0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    .line 6950
    .local v2, htmlText:Ljava/lang/String;
    if-nez v6, :cond_68

    #@64
    if-nez v9, :cond_68

    #@66
    if-eqz v2, :cond_f9

    #@68
    .line 6951
    :cond_68
    new-instance v1, Landroid/content/ClipData;

    #@6a
    const/4 v11, 0x0

    #@6b
    const/4 v12, 0x1

    #@6c
    new-array v12, v12, [Ljava/lang/String;

    #@6e
    const/4 v13, 0x0

    #@6f
    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@72
    move-result-object v14

    #@73
    aput-object v14, v12, v13

    #@75
    new-instance v13, Landroid/content/ClipData$Item;

    #@77
    const/4 v14, 0x0

    #@78
    invoke-direct {v13, v9, v2, v14, v6}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/net/Uri;)V

    #@7b
    invoke-direct {v1, v11, v12, v13}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@7e
    .line 6954
    .local v1, clipData:Landroid/content/ClipData;
    invoke-virtual {p0, v1}, Landroid/content/Intent;->setClipData(Landroid/content/ClipData;)V

    #@81
    .line 6955
    const/4 v11, 0x1

    #@82
    invoke-virtual {p0, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
    :try_end_85
    .catch Ljava/lang/ClassCastException; {:try_start_4e .. :try_end_85} :catch_fc

    #@85
    .line 6956
    const/4 v11, 0x1

    #@86
    goto :goto_d

    #@87
    .line 6961
    .end local v1           #clipData:Landroid/content/ClipData;
    .end local v2           #htmlText:Ljava/lang/String;
    .end local v6           #stream:Landroid/net/Uri;
    .end local v9           #text:Ljava/lang/CharSequence;
    :cond_87
    const-string v11, "android.intent.action.SEND_MULTIPLE"

    #@89
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v11

    #@8d
    if-eqz v11, :cond_f9

    #@8f
    .line 6963
    :try_start_8f
    const-string v11, "android.intent.extra.STREAM"

    #@91
    invoke-virtual {p0, v11}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    #@94
    move-result-object v7

    #@95
    .line 6964
    .local v7, streams:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v11, "android.intent.extra.TEXT"

    #@97
    invoke-virtual {p0, v11}, Landroid/content/Intent;->getCharSequenceArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9a
    move-result-object v10

    #@9b
    .line 6965
    .local v10, texts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/CharSequence;>;"
    const-string v11, "android.intent.extra.HTML_TEXT"

    #@9d
    invoke-virtual {p0, v11}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a0
    move-result-object v3

    #@a1
    .line 6966
    .local v3, htmlTexts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v5, -0x1

    #@a2
    .line 6967
    .local v5, num:I
    if-eqz v7, :cond_a8

    #@a4
    .line 6968
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@a7
    move-result v5

    #@a8
    .line 6970
    :cond_a8
    if-eqz v10, :cond_b9

    #@aa
    .line 6971
    if-ltz v5, :cond_b5

    #@ac
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@af
    move-result v11

    #@b0
    if-eq v5, v11, :cond_b5

    #@b2
    .line 6973
    const/4 v11, 0x0

    #@b3
    goto/16 :goto_d

    #@b5
    .line 6975
    :cond_b5
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@b8
    move-result v5

    #@b9
    .line 6977
    :cond_b9
    if-eqz v3, :cond_ca

    #@bb
    .line 6978
    if-ltz v5, :cond_c6

    #@bd
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@c0
    move-result v11

    #@c1
    if-eq v5, v11, :cond_c6

    #@c3
    .line 6980
    const/4 v11, 0x0

    #@c4
    goto/16 :goto_d

    #@c6
    .line 6982
    :cond_c6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@c9
    move-result v5

    #@ca
    .line 6984
    :cond_ca
    if-lez v5, :cond_f9

    #@cc
    .line 6985
    new-instance v1, Landroid/content/ClipData;

    #@ce
    const/4 v11, 0x0

    #@cf
    const/4 v12, 0x1

    #@d0
    new-array v12, v12, [Ljava/lang/String;

    #@d2
    const/4 v13, 0x0

    #@d3
    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@d6
    move-result-object v14

    #@d7
    aput-object v14, v12, v13

    #@d9
    const/4 v13, 0x0

    #@da
    invoke-static {v7, v10, v3, v13}, Landroid/content/Intent;->makeClipItem(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)Landroid/content/ClipData$Item;

    #@dd
    move-result-object v13

    #@de
    invoke-direct {v1, v11, v12, v13}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@e1
    .line 6989
    .restart local v1       #clipData:Landroid/content/ClipData;
    const/4 v4, 0x1

    #@e2
    .local v4, i:I
    :goto_e2
    if-ge v4, v5, :cond_ee

    #@e4
    .line 6990
    invoke-static {v7, v10, v3, v4}, Landroid/content/Intent;->makeClipItem(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)Landroid/content/ClipData$Item;

    #@e7
    move-result-object v11

    #@e8
    invoke-virtual {v1, v11}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    #@eb
    .line 6989
    add-int/lit8 v4, v4, 0x1

    #@ed
    goto :goto_e2

    #@ee
    .line 6993
    :cond_ee
    invoke-virtual {p0, v1}, Landroid/content/Intent;->setClipData(Landroid/content/ClipData;)V

    #@f1
    .line 6994
    const/4 v11, 0x1

    #@f2
    invoke-virtual {p0, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
    :try_end_f5
    .catch Ljava/lang/ClassCastException; {:try_start_8f .. :try_end_f5} :catch_f8

    #@f5
    .line 6995
    const/4 v11, 0x1

    #@f6
    goto/16 :goto_d

    #@f8
    .line 6997
    .end local v1           #clipData:Landroid/content/ClipData;
    .end local v3           #htmlTexts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4           #i:I
    .end local v5           #num:I
    .end local v7           #streams:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v10           #texts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/CharSequence;>;"
    :catch_f8
    move-exception v11

    #@f9
    .line 7001
    :cond_f9
    :goto_f9
    const/4 v11, 0x0

    #@fa
    goto/16 :goto_d

    #@fc
    .line 6958
    :catch_fc
    move-exception v11

    #@fd
    goto :goto_f9

    #@fe
    .line 6942
    :catch_fe
    move-exception v11

    #@ff
    goto :goto_f9
.end method

.method public putCharSequenceArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    #@0
    .prologue
    .line 5540
    .local p2, value:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/CharSequence;>;"
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5541
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5543
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putCharSequenceArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@10
    .line 5544
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;B)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5218
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5219
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5221
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@10
    .line 5222
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;C)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5241
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5242
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5244
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putChar(Ljava/lang/String;C)V

    #@10
    .line 5245
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;D)Landroid/content/Intent;
    .registers 5
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5356
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5357
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5359
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    #@10
    .line 5360
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;F)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5333
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5334
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5336
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@10
    .line 5337
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5287
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5288
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5290
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@10
    .line 5291
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;J)Landroid/content/Intent;
    .registers 5
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5310
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5311
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5313
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@10
    .line 5314
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5816
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5817
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5819
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@10
    .line 5820
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;Landroid/os/IBinder;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 5843
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5844
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5846
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putIBinder(Ljava/lang/String;Landroid/os/IBinder;)V

    #@10
    .line 5847
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5425
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5426
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5428
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@10
    .line 5429
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5563
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5564
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5566
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    #@10
    .line 5567
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5402
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5403
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5405
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    #@10
    .line 5406
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5379
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5380
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5382
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 5383
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;S)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5264
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5265
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5267
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putShort(Ljava/lang/String;S)V

    #@10
    .line 5268
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5195
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5196
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5198
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@10
    .line 5199
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[B)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5609
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5610
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5612
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    #@10
    .line 5613
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[C)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5655
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5656
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5658
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putCharArray(Ljava/lang/String;[C)V

    #@10
    .line 5659
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[D)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5747
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5748
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5750
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putDoubleArray(Ljava/lang/String;[D)V

    #@10
    .line 5751
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[F)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5724
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5725
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5727
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    #@10
    .line 5728
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[I)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5678
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5679
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5681
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    #@10
    .line 5682
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[J)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5701
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5702
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5704
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    #@10
    .line 5705
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5448
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5449
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5451
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    #@10
    .line 5452
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[Ljava/lang/CharSequence;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5793
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5794
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5796
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V

    #@10
    .line 5797
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5770
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5771
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5773
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    #@10
    .line 5774
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[S)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5632
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5633
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5635
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putShortArray(Ljava/lang/String;[S)V

    #@10
    .line 5636
    return-object p0
.end method

.method public putExtra(Ljava/lang/String;[Z)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 5586
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5587
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5589
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    #@10
    .line 5590
    return-object p0
.end method

.method public putExtras(Landroid/content/Intent;)Landroid/content/Intent;
    .registers 4
    .parameter "src"

    #@0
    .prologue
    .line 5858
    iget-object v0, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 5859
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@6
    if-nez v0, :cond_12

    #@8
    .line 5860
    new-instance v0, Landroid/os/Bundle;

    #@a
    iget-object v1, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@c
    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@f
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@11
    .line 5865
    :cond_11
    :goto_11
    return-object p0

    #@12
    .line 5862
    :cond_12
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@14
    iget-object v1, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@16
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    #@19
    goto :goto_11
.end method

.method public putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
    .registers 3
    .parameter "extras"

    #@0
    .prologue
    .line 5879
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5880
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5882
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    #@10
    .line 5883
    return-object p0
.end method

.method public putIntegerArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    #@0
    .prologue
    .line 5494
    .local p2, value:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5495
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5497
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@10
    .line 5498
    return-object p0
.end method

.method public putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<+",
            "Landroid/os/Parcelable;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    #@0
    .prologue
    .line 5471
    .local p2, value:Ljava/util/ArrayList;,"Ljava/util/ArrayList<+Landroid/os/Parcelable;>;"
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5472
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5474
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@10
    .line 5475
    return-object p0
.end method

.method public putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
    .registers 4
    .parameter "name"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    #@0
    .prologue
    .line 5517
    .local p2, value:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 5518
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    .line 5520
    :cond_b
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@10
    .line 5521
    return-object p0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 6
    .parameter "in"

    #@0
    .prologue
    .line 6771
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {p0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@7
    .line 6772
    sget-object v2, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@9
    invoke-interface {v2, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    check-cast v2, Landroid/net/Uri;

    #@f
    iput-object v2, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@11
    .line 6773
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    iput-object v2, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@17
    .line 6774
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v2

    #@1b
    iput v2, p0, Landroid/content/Intent;->mFlags:I

    #@1d
    .line 6775
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    iput-object v2, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@23
    .line 6776
    invoke-static {p1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    #@26
    move-result-object v2

    #@27
    iput-object v2, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@29
    .line 6778
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_39

    #@2f
    .line 6779
    sget-object v2, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    #@31
    invoke-interface {v2, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@34
    move-result-object v2

    #@35
    check-cast v2, Landroid/graphics/Rect;

    #@37
    iput-object v2, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@39
    .line 6782
    :cond_39
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v0

    #@3d
    .line 6783
    .local v0, N:I
    if-lez v0, :cond_59

    #@3f
    .line 6784
    new-instance v2, Ljava/util/HashSet;

    #@41
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    #@44
    iput-object v2, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@46
    .line 6786
    const/4 v1, 0x0

    #@47
    .local v1, i:I
    :goto_47
    if-ge v1, v0, :cond_5c

    #@49
    .line 6787
    iget-object v2, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@4b
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@56
    .line 6786
    add-int/lit8 v1, v1, 0x1

    #@58
    goto :goto_47

    #@59
    .line 6790
    .end local v1           #i:I
    :cond_59
    const/4 v2, 0x0

    #@5a
    iput-object v2, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@5c
    .line 6793
    :cond_5c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v2

    #@60
    if-eqz v2, :cond_69

    #@62
    .line 6794
    new-instance v2, Landroid/content/Intent;

    #@64
    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/os/Parcel;)V

    #@67
    iput-object v2, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@69
    .line 6797
    :cond_69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6c
    move-result v2

    #@6d
    if-eqz v2, :cond_76

    #@6f
    .line 6798
    new-instance v2, Landroid/content/ClipData;

    #@71
    invoke-direct {v2, p1}, Landroid/content/ClipData;-><init>(Landroid/os/Parcel;)V

    #@74
    iput-object v2, p0, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@76
    .line 6801
    :cond_76
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@79
    move-result-object v2

    #@7a
    iput-object v2, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@7c
    .line 6802
    return-void
.end method

.method public removeCategory(Ljava/lang/String;)V
    .registers 3
    .parameter "category"

    #@0
    .prologue
    .line 5097
    iget-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@2
    if-eqz v0, :cond_14

    #@4
    .line 5098
    iget-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@6
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@9
    .line 5099
    iget-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@b
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_14

    #@11
    .line 5100
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@14
    .line 5103
    :cond_14
    return-void
.end method

.method public removeExtra(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 5916
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_14

    #@4
    .line 5917
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@9
    .line 5918
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@b
    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_14

    #@11
    .line 5919
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@14
    .line 5922
    :cond_14
    return-void
.end method

.method public replaceExtras(Landroid/content/Intent;)Landroid/content/Intent;
    .registers 4
    .parameter "src"

    #@0
    .prologue
    .line 5894
    iget-object v0, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_e

    #@4
    new-instance v0, Landroid/os/Bundle;

    #@6
    iget-object v1, p1, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@8
    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@b
    :goto_b
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@d
    .line 5895
    return-object p0

    #@e
    .line 5894
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_b
.end method

.method public replaceExtras(Landroid/os/Bundle;)Landroid/content/Intent;
    .registers 3
    .parameter "extras"

    #@0
    .prologue
    .line 5906
    if-eqz p1, :cond_a

    #@2
    new-instance v0, Landroid/os/Bundle;

    #@4
    invoke-direct {v0, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@7
    :goto_7
    iput-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@9
    .line 5907
    return-object p0

    #@a
    .line 5906
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_7
.end method

.method public resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;
    .registers 6
    .parameter "pm"

    #@0
    .prologue
    .line 4826
    iget-object v1, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 4827
    iget-object v1, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@6
    .line 4838
    :goto_6
    return-object v1

    #@7
    .line 4830
    :cond_7
    const/high16 v1, 0x1

    #@9
    invoke-virtual {p1, p0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    #@c
    move-result-object v0

    #@d
    .line 4832
    .local v0, info:Landroid/content/pm/ResolveInfo;
    if-eqz v0, :cond_1f

    #@f
    .line 4833
    new-instance v1, Landroid/content/ComponentName;

    #@11
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@13
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@15
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@17
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@19
    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@1b
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    goto :goto_6

    #@1f
    .line 4838
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_6
.end method

.method public resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;
    .registers 6
    .parameter "pm"
    .parameter "flags"

    #@0
    .prologue
    .line 4858
    const/4 v0, 0x0

    #@1
    .line 4859
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    iget-object v2, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@3
    if-eqz v2, :cond_c

    #@5
    .line 4861
    :try_start_5
    iget-object v2, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@7
    invoke-virtual {p1, v2, p2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_a} :catch_18

    #@a
    move-result-object v0

    #@b
    .line 4873
    :cond_b
    :goto_b
    return-object v0

    #@c
    .line 4866
    :cond_c
    const/high16 v2, 0x1

    #@e
    or-int/2addr v2, p2

    #@f
    invoke-virtual {p1, p0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    #@12
    move-result-object v1

    #@13
    .line 4868
    .local v1, info:Landroid/content/pm/ResolveInfo;
    if-eqz v1, :cond_b

    #@15
    .line 4869
    iget-object v0, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@17
    goto :goto_b

    #@18
    .line 4862
    .end local v1           #info:Landroid/content/pm/ResolveInfo;
    :catch_18
    move-exception v2

    #@19
    goto :goto_b
.end method

.method public resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;
    .registers 4
    .parameter "resolver"

    #@0
    .prologue
    .line 4117
    iget-object v0, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 4118
    iget-object v0, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@6
    .line 4125
    :goto_6
    return-object v0

    #@7
    .line 4120
    :cond_7
    iget-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@9
    if-eqz v0, :cond_20

    #@b
    .line 4121
    const-string v0, "content"

    #@d
    iget-object v1, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@f
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_20

    #@19
    .line 4122
    iget-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@1b
    invoke-virtual {p1, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    goto :goto_6

    #@20
    .line 4125
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_6
.end method

.method public resolveType(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 4099
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/content/Intent;->resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;
    .registers 3
    .parameter "resolver"

    #@0
    .prologue
    .line 4141
    iget-object v0, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 4142
    iget-object v0, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@6
    .line 4144
    :goto_6
    return-object v0

    #@7
    :cond_7
    invoke-virtual {p0, p1}, Landroid/content/Intent;->resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    goto :goto_6
.end method

.method public setAction(Ljava/lang/String;)Landroid/content/Intent;
    .registers 3
    .parameter "action"

    #@0
    .prologue
    .line 4888
    if-eqz p1, :cond_9

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    :goto_6
    iput-object v0, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@8
    .line 4889
    return-object p0

    #@9
    .line 4888
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_6
.end method

.method public setAllowFds(Z)V
    .registers 3
    .parameter "allowFds"

    #@0
    .prologue
    .line 4226
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 4227
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->setAllowFds(Z)Z

    #@9
    .line 4229
    :cond_9
    return-void
.end method

.method public setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .registers 4
    .parameter "packageContext"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    #@0
    .prologue
    .line 6097
    .local p2, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    new-instance v0, Landroid/content/ComponentName;

    #@2
    invoke-direct {v0, p1, p2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@5
    iput-object v0, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@7
    .line 6098
    return-object p0
.end method

.method public setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .registers 4
    .parameter "packageContext"
    .parameter "className"

    #@0
    .prologue
    .line 6058
    new-instance v0, Landroid/content/ComponentName;

    #@2
    invoke-direct {v0, p1, p2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@5
    iput-object v0, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@7
    .line 6059
    return-object p0
.end method

.method public setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 4
    .parameter "packageName"
    .parameter "className"

    #@0
    .prologue
    .line 6078
    new-instance v0, Landroid/content/ComponentName;

    #@2
    invoke-direct {v0, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    iput-object v0, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@7
    .line 6079
    return-object p0
.end method

.method public setClipData(Landroid/content/ClipData;)V
    .registers 2
    .parameter "clip"

    #@0
    .prologue
    .line 5176
    iput-object p1, p0, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@2
    .line 5177
    return-void
.end method

.method public setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
    .registers 2
    .parameter "component"

    #@0
    .prologue
    .line 6038
    iput-object p1, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@2
    .line 6039
    return-object p0
.end method

.method public setData(Landroid/net/Uri;)Landroid/content/Intent;
    .registers 3
    .parameter "data"

    #@0
    .prologue
    .line 4914
    iput-object p1, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@2
    .line 4915
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@5
    .line 4916
    return-object p0
.end method

.method public setDataAndNormalize(Landroid/net/Uri;)Landroid/content/Intent;
    .registers 3
    .parameter "data"

    #@0
    .prologue
    .line 4942
    invoke-virtual {p1}, Landroid/net/Uri;->normalizeScheme()Landroid/net/Uri;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .registers 3
    .parameter "data"
    .parameter "type"

    #@0
    .prologue
    .line 5031
    iput-object p1, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@2
    .line 5032
    iput-object p2, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@4
    .line 5033
    return-object p0
.end method

.method public setDataAndTypeAndNormalize(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .registers 5
    .parameter "data"
    .parameter "type"

    #@0
    .prologue
    .line 5062
    invoke-virtual {p1}, Landroid/net/Uri;->normalizeScheme()Landroid/net/Uri;

    #@3
    move-result-object v0

    #@4
    invoke-static {p2}, Landroid/content/Intent;->normalizeMimeType(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public setExtrasClassLoader(Ljava/lang/ClassLoader;)V
    .registers 3
    .parameter "loader"

    #@0
    .prologue
    .line 4202
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 4203
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    #@9
    .line 4205
    :cond_9
    return-void
.end method

.method public setFlags(I)Landroid/content/Intent;
    .registers 2
    .parameter "flags"

    #@0
    .prologue
    .line 5969
    iput p1, p0, Landroid/content/Intent;->mFlags:I

    #@2
    .line 5970
    return-object p0
.end method

.method public setPackage(Ljava/lang/String;)Landroid/content/Intent;
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 6006
    if-eqz p1, :cond_e

    #@2
    iget-object v0, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 6007
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Can\'t set package name when selector is already set"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 6010
    :cond_e
    iput-object p1, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@10
    .line 6011
    return-object p0
.end method

.method public setSelector(Landroid/content/Intent;)V
    .registers 4
    .parameter "selector"

    #@0
    .prologue
    .line 5137
    if-ne p1, p0, :cond_a

    #@2
    .line 5138
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Intent being set as a selector of itself"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 5141
    :cond_a
    if-eqz p1, :cond_18

    #@c
    iget-object v0, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@e
    if-eqz v0, :cond_18

    #@10
    .line 5142
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "Can\'t set selector when package name is already set"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 5145
    :cond_18
    iput-object p1, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@1a
    .line 5146
    return-void
.end method

.method public setSourceBounds(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 6107
    if-eqz p1, :cond_a

    #@2
    .line 6108
    new-instance v0, Landroid/graphics/Rect;

    #@4
    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@7
    iput-object v0, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@9
    .line 6112
    :goto_9
    return-void

    #@a
    .line 6110
    :cond_a
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@d
    goto :goto_9
.end method

.method public setType(Ljava/lang/String;)Landroid/content/Intent;
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 4971
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@3
    .line 4972
    iput-object p1, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@5
    .line 4973
    return-object p0
.end method

.method public setTypeAndNormalize(Ljava/lang/String;)Landroid/content/Intent;
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 5002
    invoke-static {p1}, Landroid/content/Intent;->normalizeMimeType(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public toInsecureString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 6457
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    const/16 v0, 0x80

    #@6
    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@9
    .line 6459
    .local v1, b:Ljava/lang/StringBuilder;
    const-string v0, "Intent { "

    #@b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-object v0, p0

    #@f
    move v4, v3

    #@10
    move v5, v2

    #@11
    .line 6460
    invoke-virtual/range {v0 .. v5}, Landroid/content/Intent;->toShortString(Ljava/lang/StringBuilder;ZZZZ)V

    #@14
    .line 6461
    const-string v0, " }"

    #@16
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 6463
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method

.method public toInsecureStringWithClip()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 6468
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    const/16 v0, 0x80

    #@5
    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@8
    .line 6470
    .local v1, b:Ljava/lang/StringBuilder;
    const-string v0, "Intent { "

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 6471
    const/4 v2, 0x0

    #@e
    move-object v0, p0

    #@f
    move v4, v3

    #@10
    move v5, v3

    #@11
    invoke-virtual/range {v0 .. v5}, Landroid/content/Intent;->toShortString(Ljava/lang/StringBuilder;ZZZZ)V

    #@14
    .line 6472
    const-string v0, " }"

    #@16
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 6474
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method

.method public toShortString(ZZZZ)Ljava/lang/String;
    .registers 11
    .parameter "secure"
    .parameter "comp"
    .parameter "extras"
    .parameter "clip"

    #@0
    .prologue
    .line 6479
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    const/16 v0, 0x80

    #@4
    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .local v1, b:Ljava/lang/StringBuilder;
    move-object v0, p0

    #@8
    move v2, p1

    #@9
    move v3, p2

    #@a
    move v4, p3

    #@b
    move v5, p4

    #@c
    .line 6480
    invoke-virtual/range {v0 .. v5}, Landroid/content/Intent;->toShortString(Ljava/lang/StringBuilder;ZZZZ)V

    #@f
    .line 6481
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    return-object v0
.end method

.method public toShortString(Ljava/lang/StringBuilder;ZZZZ)V
    .registers 15
    .parameter "b"
    .parameter "secure"
    .parameter "comp"
    .parameter "extras"
    .parameter "clip"

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 6487
    const/4 v7, 0x1

    #@3
    .line 6488
    .local v7, first:Z
    iget-object v0, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@5
    if-eqz v0, :cond_13

    #@7
    .line 6489
    const-string v0, "act="

    #@9
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    iget-object v1, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    .line 6490
    const/4 v7, 0x0

    #@13
    .line 6492
    :cond_13
    iget-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@15
    if-eqz v0, :cond_46

    #@17
    .line 6493
    if-nez v7, :cond_1c

    #@19
    .line 6494
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1c
    .line 6496
    :cond_1c
    const/4 v7, 0x0

    #@1d
    .line 6497
    const-string v0, "cat=["

    #@1f
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 6498
    iget-object v0, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@24
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@27
    move-result-object v8

    #@28
    .line 6499
    .local v8, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    const/4 v6, 0x0

    #@29
    .line 6500
    .local v6, didone:Z
    :goto_29
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_41

    #@2f
    .line 6501
    if-eqz v6, :cond_36

    #@31
    const-string v0, ","

    #@33
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 6502
    :cond_36
    const/4 v6, 0x1

    #@37
    .line 6503
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3a
    move-result-object v0

    #@3b
    check-cast v0, Ljava/lang/String;

    #@3d
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    goto :goto_29

    #@41
    .line 6505
    :cond_41
    const-string v0, "]"

    #@43
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    .line 6507
    .end local v6           #didone:Z
    .end local v8           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_46
    iget-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@48
    if-eqz v0, :cond_60

    #@4a
    .line 6508
    if-nez v7, :cond_4f

    #@4c
    .line 6509
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4f
    .line 6511
    :cond_4f
    const/4 v7, 0x0

    #@50
    .line 6512
    const-string v0, "dat="

    #@52
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 6513
    if-eqz p2, :cond_11f

    #@57
    .line 6514
    iget-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@59
    invoke-virtual {v0}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    .line 6519
    :cond_60
    :goto_60
    iget-object v0, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@62
    if-eqz v0, :cond_76

    #@64
    .line 6520
    if-nez v7, :cond_69

    #@66
    .line 6521
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@69
    .line 6523
    :cond_69
    const/4 v7, 0x0

    #@6a
    .line 6524
    const-string/jumbo v0, "typ="

    #@6d
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v0

    #@71
    iget-object v1, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    .line 6526
    :cond_76
    iget v0, p0, Landroid/content/Intent;->mFlags:I

    #@78
    if-eqz v0, :cond_8f

    #@7a
    .line 6527
    if-nez v7, :cond_7f

    #@7c
    .line 6528
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@7f
    .line 6530
    :cond_7f
    const/4 v7, 0x0

    #@80
    .line 6531
    const-string v0, "flg=0x"

    #@82
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v0

    #@86
    iget v1, p0, Landroid/content/Intent;->mFlags:I

    #@88
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@8b
    move-result-object v1

    #@8c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    .line 6533
    :cond_8f
    iget-object v0, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@91
    if-eqz v0, :cond_a5

    #@93
    .line 6534
    if-nez v7, :cond_98

    #@95
    .line 6535
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@98
    .line 6537
    :cond_98
    const/4 v7, 0x0

    #@99
    .line 6538
    const-string/jumbo v0, "pkg="

    #@9c
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v0

    #@a0
    iget-object v1, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@a2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    .line 6540
    :cond_a5
    if-eqz p3, :cond_c0

    #@a7
    iget-object v0, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@a9
    if-eqz v0, :cond_c0

    #@ab
    .line 6541
    if-nez v7, :cond_b0

    #@ad
    .line 6542
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b0
    .line 6544
    :cond_b0
    const/4 v7, 0x0

    #@b1
    .line 6545
    const-string v0, "cmp="

    #@b3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v0

    #@b7
    iget-object v1, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@b9
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@bc
    move-result-object v1

    #@bd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    .line 6547
    :cond_c0
    iget-object v0, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@c2
    if-eqz v0, :cond_d9

    #@c4
    .line 6548
    if-nez v7, :cond_c9

    #@c6
    .line 6549
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c9
    .line 6551
    :cond_c9
    const/4 v7, 0x0

    #@ca
    .line 6552
    const-string v0, "bnds="

    #@cc
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v0

    #@d0
    iget-object v1, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@d2
    invoke-virtual {v1}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    #@d5
    move-result-object v1

    #@d6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    .line 6554
    :cond_d9
    iget-object v0, p0, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@db
    if-eqz v0, :cond_f4

    #@dd
    .line 6555
    if-nez v7, :cond_e2

    #@df
    .line 6556
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@e2
    .line 6558
    :cond_e2
    const/4 v7, 0x0

    #@e3
    .line 6559
    if-eqz p5, :cond_126

    #@e5
    .line 6560
    const-string v0, "clip={"

    #@e7
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    .line 6561
    iget-object v0, p0, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@ec
    invoke-virtual {v0, p1}, Landroid/content/ClipData;->toShortString(Ljava/lang/StringBuilder;)V

    #@ef
    .line 6562
    const/16 v0, 0x7d

    #@f1
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@f4
    .line 6567
    :cond_f4
    :goto_f4
    if-eqz p4, :cond_105

    #@f6
    iget-object v0, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@f8
    if-eqz v0, :cond_105

    #@fa
    .line 6568
    if-nez v7, :cond_ff

    #@fc
    .line 6569
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@ff
    .line 6571
    :cond_ff
    const/4 v7, 0x0

    #@100
    .line 6572
    const-string v0, "(has extras)"

    #@102
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    .line 6574
    :cond_105
    iget-object v0, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@107
    if-eqz v0, :cond_11e

    #@109
    .line 6575
    const-string v0, " sel={"

    #@10b
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    .line 6576
    iget-object v0, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@110
    move-object v1, p1

    #@111
    move v2, p2

    #@112
    move v3, p3

    #@113
    move v4, p4

    #@114
    move v5, p5

    #@115
    invoke-virtual/range {v0 .. v5}, Landroid/content/Intent;->toShortString(Ljava/lang/StringBuilder;ZZZZ)V

    #@118
    .line 6577
    const-string/jumbo v0, "}"

    #@11b
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    .line 6579
    :cond_11e
    return-void

    #@11f
    .line 6516
    :cond_11f
    iget-object v0, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@121
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@124
    goto/16 :goto_60

    #@126
    .line 6564
    :cond_126
    const-string v0, "(has clip)"

    #@128
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    goto :goto_f4
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 6446
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    const/16 v0, 0x80

    #@5
    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@8
    .line 6448
    .local v1, b:Ljava/lang/StringBuilder;
    const-string v0, "Intent { "

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 6449
    const/4 v5, 0x0

    #@e
    move-object v0, p0

    #@f
    move v3, v2

    #@10
    move v4, v2

    #@11
    invoke-virtual/range {v0 .. v5}, Landroid/content/Intent;->toShortString(Ljava/lang/StringBuilder;ZZZZ)V

    #@14
    .line 6450
    const-string v0, " }"

    #@16
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 6452
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method

.method public toURI()Ljava/lang/String;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 6587
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public toUri(I)Ljava/lang/String;
    .registers 10
    .parameter "flags"

    #@0
    .prologue
    .line 6607
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    const/16 v6, 0x80

    #@4
    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 6608
    .local v5, uri:Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    #@8
    .line 6609
    .local v4, scheme:Ljava/lang/String;
    iget-object v6, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@a
    if-eqz v6, :cond_76

    #@c
    .line 6610
    iget-object v6, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@e
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    .line 6611
    .local v2, data:Ljava/lang/String;
    and-int/lit8 v6, p1, 0x1

    #@14
    if-eqz v6, :cond_52

    #@16
    .line 6612
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@19
    move-result v0

    #@1a
    .line 6613
    .local v0, N:I
    const/4 v3, 0x0

    #@1b
    .local v3, i:I
    :goto_1b
    if-ge v3, v0, :cond_52

    #@1d
    .line 6614
    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    #@20
    move-result v1

    #@21
    .line 6615
    .local v1, c:C
    const/16 v6, 0x61

    #@23
    if-lt v1, v6, :cond_29

    #@25
    const/16 v6, 0x7a

    #@27
    if-le v1, v6, :cond_39

    #@29
    :cond_29
    const/16 v6, 0x41

    #@2b
    if-lt v1, v6, :cond_31

    #@2d
    const/16 v6, 0x5a

    #@2f
    if-le v1, v6, :cond_39

    #@31
    :cond_31
    const/16 v6, 0x2e

    #@33
    if-eq v1, v6, :cond_39

    #@35
    const/16 v6, 0x2d

    #@37
    if-ne v1, v6, :cond_3c

    #@39
    .line 6613
    :cond_39
    add-int/lit8 v3, v3, 0x1

    #@3b
    goto :goto_1b

    #@3c
    .line 6619
    :cond_3c
    const/16 v6, 0x3a

    #@3e
    if-ne v1, v6, :cond_52

    #@40
    if-lez v3, :cond_52

    #@42
    .line 6621
    const/4 v6, 0x0

    #@43
    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    .line 6622
    const-string v6, "intent:"

    #@49
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    .line 6623
    add-int/lit8 v6, v3, 0x1

    #@4e
    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    .line 6631
    .end local v0           #N:I
    .end local v1           #c:C
    .end local v3           #i:I
    :cond_52
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 6637
    .end local v2           #data:Ljava/lang/String;
    :cond_55
    :goto_55
    const-string v6, "#Intent;"

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    .line 6639
    invoke-direct {p0, v5, v4, p1}, Landroid/content/Intent;->toUriInner(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    #@5d
    .line 6640
    iget-object v6, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@5f
    if-eqz v6, :cond_6c

    #@61
    .line 6641
    const-string v6, "SEL;"

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    .line 6645
    iget-object v6, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@68
    const/4 v7, 0x0

    #@69
    invoke-direct {v6, v5, v7, p1}, Landroid/content/Intent;->toUriInner(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    #@6c
    .line 6648
    :cond_6c
    const-string v6, "end"

    #@6e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    .line 6650
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v6

    #@75
    return-object v6

    #@76
    .line 6633
    :cond_76
    and-int/lit8 v6, p1, 0x1

    #@78
    if-eqz v6, :cond_55

    #@7a
    .line 6634
    const-string v6, "intent:"

    #@7c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    goto :goto_55
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 6715
    iget-object v2, p0, Landroid/content/Intent;->mAction:Ljava/lang/String;

    #@4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7
    .line 6716
    iget-object v2, p0, Landroid/content/Intent;->mData:Landroid/net/Uri;

    #@9
    invoke-static {p1, v2}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;Landroid/net/Uri;)V

    #@c
    .line 6717
    iget-object v2, p0, Landroid/content/Intent;->mType:Ljava/lang/String;

    #@e
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 6718
    iget v2, p0, Landroid/content/Intent;->mFlags:I

    #@13
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 6719
    iget-object v2, p0, Landroid/content/Intent;->mPackage:Ljava/lang/String;

    #@18
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 6720
    iget-object v2, p0, Landroid/content/Intent;->mComponent:Landroid/content/ComponentName;

    #@1d
    invoke-static {v2, p1}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    #@20
    .line 6722
    iget-object v2, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@22
    if-eqz v2, :cond_4f

    #@24
    .line 6723
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 6724
    iget-object v2, p0, Landroid/content/Intent;->mSourceBounds:Landroid/graphics/Rect;

    #@29
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@2c
    .line 6729
    :goto_2c
    iget-object v2, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@2e
    if-eqz v2, :cond_53

    #@30
    .line 6730
    iget-object v2, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@32
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@35
    move-result v2

    #@36
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@39
    .line 6731
    iget-object v2, p0, Landroid/content/Intent;->mCategories:Ljava/util/HashSet;

    #@3b
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@3e
    move-result-object v1

    #@3f
    .local v1, i$:Ljava/util/Iterator;
    :goto_3f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@42
    move-result v2

    #@43
    if-eqz v2, :cond_56

    #@45
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@48
    move-result-object v0

    #@49
    check-cast v0, Ljava/lang/String;

    #@4b
    .line 6732
    .local v0, category:Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4e
    goto :goto_3f

    #@4f
    .line 6726
    .end local v0           #category:Ljava/lang/String;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_4f
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@52
    goto :goto_2c

    #@53
    .line 6735
    :cond_53
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@56
    .line 6738
    :cond_56
    iget-object v2, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@58
    if-eqz v2, :cond_74

    #@5a
    .line 6739
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5d
    .line 6740
    iget-object v2, p0, Landroid/content/Intent;->mSelector:Landroid/content/Intent;

    #@5f
    invoke-virtual {v2, p1, p2}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@62
    .line 6745
    :goto_62
    iget-object v2, p0, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@64
    if-eqz v2, :cond_78

    #@66
    .line 6746
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@69
    .line 6747
    iget-object v2, p0, Landroid/content/Intent;->mClipData:Landroid/content/ClipData;

    #@6b
    invoke-virtual {v2, p1, p2}, Landroid/content/ClipData;->writeToParcel(Landroid/os/Parcel;I)V

    #@6e
    .line 6752
    :goto_6e
    iget-object v2, p0, Landroid/content/Intent;->mExtras:Landroid/os/Bundle;

    #@70
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@73
    .line 6753
    return-void

    #@74
    .line 6742
    :cond_74
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@77
    goto :goto_62

    #@78
    .line 6749
    :cond_78
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@7b
    goto :goto_6e
.end method
