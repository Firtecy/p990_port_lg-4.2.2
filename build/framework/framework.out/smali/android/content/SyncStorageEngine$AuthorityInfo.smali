.class public Landroid/content/SyncStorageEngine$AuthorityInfo;
.super Ljava/lang/Object;
.source "SyncStorageEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/SyncStorageEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AuthorityInfo"
.end annotation


# instance fields
.field final account:Landroid/accounts/Account;

.field final authority:Ljava/lang/String;

.field backoffDelay:J

.field backoffTime:J

.field delayUntil:J

.field enabled:Z

.field final ident:I

.field final periodicSyncs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/os/Bundle;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field syncable:I

.field final userId:I


# direct methods
.method constructor <init>(Landroid/accounts/Account;ILjava/lang/String;I)V
    .registers 9
    .parameter "account"
    .parameter "userId"
    .parameter "authority"
    .parameter "ident"

    #@0
    .prologue
    const-wide/16 v1, -0x1

    #@2
    .line 225
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 226
    iput-object p1, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@7
    .line 227
    iput p2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@9
    .line 228
    iput-object p3, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@b
    .line 229
    iput p4, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@d
    .line 230
    const/4 v0, 0x0

    #@e
    iput-boolean v0, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@10
    .line 231
    const/4 v0, -0x1

    #@11
    iput v0, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@13
    .line 232
    iput-wide v1, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@15
    .line 233
    iput-wide v1, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@17
    .line 234
    new-instance v0, Ljava/util/ArrayList;

    #@19
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1c
    iput-object v0, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@1e
    .line 235
    iget-object v0, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@20
    new-instance v1, Landroid/os/Bundle;

    #@22
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@25
    const-wide/32 v2, 0x15180

    #@28
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@33
    .line 236
    return-void
.end method

.method constructor <init>(Landroid/content/SyncStorageEngine$AuthorityInfo;)V
    .registers 7
    .parameter "toCopy"

    #@0
    .prologue
    .line 208
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 209
    iget-object v2, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@5
    iput-object v2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@7
    .line 210
    iget v2, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@9
    iput v2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@b
    .line 211
    iget-object v2, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@d
    iput-object v2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@f
    .line 212
    iget v2, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@11
    iput v2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@13
    .line 213
    iget-boolean v2, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@15
    iput-boolean v2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@17
    .line 214
    iget v2, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@19
    iput v2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@1b
    .line 215
    iget-wide v2, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@1d
    iput-wide v2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@1f
    .line 216
    iget-wide v2, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@21
    iput-wide v2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@23
    .line 217
    iget-wide v2, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->delayUntil:J

    #@25
    iput-wide v2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->delayUntil:J

    #@27
    .line 218
    new-instance v2, Ljava/util/ArrayList;

    #@29
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@2c
    iput-object v2, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@2e
    .line 219
    iget-object v2, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@33
    move-result-object v0

    #@34
    .local v0, i$:Ljava/util/Iterator;
    :goto_34
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@37
    move-result v2

    #@38
    if-eqz v2, :cond_55

    #@3a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3d
    move-result-object v1

    #@3e
    check-cast v1, Landroid/util/Pair;

    #@40
    .line 221
    .local v1, sync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    iget-object v3, p0, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@42
    new-instance v4, Landroid/os/Bundle;

    #@44
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@46
    check-cast v2, Landroid/os/Bundle;

    #@48
    invoke-direct {v4, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@4b
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@4d
    invoke-static {v4, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@54
    goto :goto_34

    #@55
    .line 223
    .end local v1           #sync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :cond_55
    return-void
.end method
