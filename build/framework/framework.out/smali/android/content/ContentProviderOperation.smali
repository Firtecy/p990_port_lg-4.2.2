.class public Landroid/content/ContentProviderOperation;
.super Ljava/lang/Object;
.source "ContentProviderOperation.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/ContentProviderOperation$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ContentProviderOperation"

.field public static final TYPE_ASSERT:I = 0x4

.field public static final TYPE_DELETE:I = 0x3

.field public static final TYPE_INSERT:I = 0x1

.field public static final TYPE_UPDATE:I = 0x2


# instance fields
.field private final mExpectedCount:Ljava/lang/Integer;

.field private final mSelection:Ljava/lang/String;

.field private final mSelectionArgs:[Ljava/lang/String;

.field private final mSelectionArgsBackReferences:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mType:I

.field private final mUri:Landroid/net/Uri;

.field private final mValues:Landroid/content/ContentValues;

.field private final mValuesBackReferences:Landroid/content/ContentValues;

.field private final mYieldAllowed:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 379
    new-instance v0, Landroid/content/ContentProviderOperation$1;

    #@2
    invoke-direct {v0}, Landroid/content/ContentProviderOperation$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/ContentProviderOperation;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/content/ContentProviderOperation$Builder;)V
    .registers 3
    .parameter "builder"

    #@0
    .prologue
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 57
    invoke-static {p1}, Landroid/content/ContentProviderOperation$Builder;->access$000(Landroid/content/ContentProviderOperation$Builder;)I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/content/ContentProviderOperation;->mType:I

    #@9
    .line 58
    invoke-static {p1}, Landroid/content/ContentProviderOperation$Builder;->access$100(Landroid/content/ContentProviderOperation$Builder;)Landroid/net/Uri;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/content/ContentProviderOperation;->mUri:Landroid/net/Uri;

    #@f
    .line 59
    invoke-static {p1}, Landroid/content/ContentProviderOperation$Builder;->access$200(Landroid/content/ContentProviderOperation$Builder;)Landroid/content/ContentValues;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Landroid/content/ContentProviderOperation;->mValues:Landroid/content/ContentValues;

    #@15
    .line 60
    invoke-static {p1}, Landroid/content/ContentProviderOperation$Builder;->access$300(Landroid/content/ContentProviderOperation$Builder;)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/content/ContentProviderOperation;->mSelection:Ljava/lang/String;

    #@1b
    .line 61
    invoke-static {p1}, Landroid/content/ContentProviderOperation$Builder;->access$400(Landroid/content/ContentProviderOperation$Builder;)[Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Landroid/content/ContentProviderOperation;->mSelectionArgs:[Ljava/lang/String;

    #@21
    .line 62
    invoke-static {p1}, Landroid/content/ContentProviderOperation$Builder;->access$500(Landroid/content/ContentProviderOperation$Builder;)Ljava/lang/Integer;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Landroid/content/ContentProviderOperation;->mExpectedCount:Ljava/lang/Integer;

    #@27
    .line 63
    invoke-static {p1}, Landroid/content/ContentProviderOperation$Builder;->access$600(Landroid/content/ContentProviderOperation$Builder;)Ljava/util/Map;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Landroid/content/ContentProviderOperation;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@2d
    .line 64
    invoke-static {p1}, Landroid/content/ContentProviderOperation$Builder;->access$700(Landroid/content/ContentProviderOperation$Builder;)Landroid/content/ContentValues;

    #@30
    move-result-object v0

    #@31
    iput-object v0, p0, Landroid/content/ContentProviderOperation;->mValuesBackReferences:Landroid/content/ContentValues;

    #@33
    .line 65
    invoke-static {p1}, Landroid/content/ContentProviderOperation$Builder;->access$800(Landroid/content/ContentProviderOperation$Builder;)Z

    #@36
    move-result v0

    #@37
    iput-boolean v0, p0, Landroid/content/ContentProviderOperation;->mYieldAllowed:Z

    #@39
    .line 66
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/ContentProviderOperation$Builder;Landroid/content/ContentProviderOperation$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/content/ContentProviderOperation;-><init>(Landroid/content/ContentProviderOperation$Builder;)V

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 7
    .parameter "source"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 68
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7
    move-result v2

    #@8
    iput v2, p0, Landroid/content/ContentProviderOperation;->mType:I

    #@a
    .line 70
    sget-object v2, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c
    invoke-interface {v2, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/net/Uri;

    #@12
    iput-object v2, p0, Landroid/content/ContentProviderOperation;->mUri:Landroid/net/Uri;

    #@14
    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_8c

    #@1a
    sget-object v2, Landroid/content/ContentValues;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v2, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Landroid/content/ContentValues;

    #@22
    :goto_22
    iput-object v2, p0, Landroid/content/ContentProviderOperation;->mValues:Landroid/content/ContentValues;

    #@24
    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v2

    #@28
    if-eqz v2, :cond_8e

    #@2a
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    :goto_2e
    iput-object v2, p0, Landroid/content/ContentProviderOperation;->mSelection:Ljava/lang/String;

    #@30
    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v2

    #@34
    if-eqz v2, :cond_90

    #@36
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    :goto_3a
    iput-object v2, p0, Landroid/content/ContentProviderOperation;->mSelectionArgs:[Ljava/lang/String;

    #@3c
    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3f
    move-result v2

    #@40
    if-eqz v2, :cond_92

    #@42
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v2

    #@46
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@49
    move-result-object v2

    #@4a
    :goto_4a
    iput-object v2, p0, Landroid/content/ContentProviderOperation;->mExpectedCount:Ljava/lang/Integer;

    #@4c
    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v2

    #@50
    if-eqz v2, :cond_94

    #@52
    sget-object v2, Landroid/content/ContentValues;->CREATOR:Landroid/os/Parcelable$Creator;

    #@54
    invoke-interface {v2, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@57
    move-result-object v2

    #@58
    check-cast v2, Landroid/content/ContentValues;

    #@5a
    :goto_5a
    iput-object v2, p0, Landroid/content/ContentProviderOperation;->mValuesBackReferences:Landroid/content/ContentValues;

    #@5c
    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v2

    #@60
    if-eqz v2, :cond_67

    #@62
    new-instance v3, Ljava/util/HashMap;

    #@64
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    #@67
    :cond_67
    iput-object v3, p0, Landroid/content/ContentProviderOperation;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@69
    .line 81
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@6b
    if-eqz v2, :cond_96

    #@6d
    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@70
    move-result v0

    #@71
    .line 83
    .local v0, count:I
    const/4 v1, 0x0

    #@72
    .local v1, i:I
    :goto_72
    if-ge v1, v0, :cond_96

    #@74
    .line 84
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@79
    move-result v3

    #@7a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7d
    move-result-object v3

    #@7e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@81
    move-result v4

    #@82
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@85
    move-result-object v4

    #@86
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@89
    .line 83
    add-int/lit8 v1, v1, 0x1

    #@8b
    goto :goto_72

    #@8c
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_8c
    move-object v2, v3

    #@8d
    .line 71
    goto :goto_22

    #@8e
    :cond_8e
    move-object v2, v3

    #@8f
    .line 72
    goto :goto_2e

    #@90
    :cond_90
    move-object v2, v3

    #@91
    .line 73
    goto :goto_3a

    #@92
    :cond_92
    move-object v2, v3

    #@93
    .line 74
    goto :goto_4a

    #@94
    :cond_94
    move-object v2, v3

    #@95
    .line 75
    goto :goto_5a

    #@96
    .line 87
    :cond_96
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@99
    move-result v2

    #@9a
    if-eqz v2, :cond_a0

    #@9c
    const/4 v2, 0x1

    #@9d
    :goto_9d
    iput-boolean v2, p0, Landroid/content/ContentProviderOperation;->mYieldAllowed:Z

    #@9f
    .line 88
    return-void

    #@a0
    .line 87
    :cond_a0
    const/4 v2, 0x0

    #@a1
    goto :goto_9d
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/ContentProviderOperation$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/content/ContentProviderOperation;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private backRefToValue([Landroid/content/ContentProviderResult;ILjava/lang/Integer;)J
    .registers 10
    .parameter "backRefs"
    .parameter "numBackRefs"
    .parameter "backRefIndex"

    #@0
    .prologue
    .line 360
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    #@3
    move-result v3

    #@4
    if-lt v3, p2, :cond_38

    #@6
    .line 361
    const-string v3, "ContentProviderOperation"

    #@8
    invoke-virtual {p0}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    #@b
    move-result-object v4

    #@c
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 362
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "asked for back ref "

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    const-string v5, " but there are only "

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    const-string v5, " back refs"

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    invoke-direct {v3, v4}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@37
    throw v3

    #@38
    .line 365
    :cond_38
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    #@3b
    move-result v3

    #@3c
    aget-object v0, p1, v3

    #@3e
    .line 367
    .local v0, backRef:Landroid/content/ContentProviderResult;
    iget-object v3, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    #@40
    if-eqz v3, :cond_49

    #@42
    .line 368
    iget-object v3, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    #@44
    invoke-static {v3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    #@47
    move-result-wide v1

    #@48
    .line 372
    .local v1, backRefValue:J
    :goto_48
    return-wide v1

    #@49
    .line 370
    .end local v1           #backRefValue:J
    :cond_49
    iget-object v3, v0, Landroid/content/ContentProviderResult;->count:Ljava/lang/Integer;

    #@4b
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@4e
    move-result v3

    #@4f
    int-to-long v1, v3

    #@50
    .restart local v1       #backRefValue:J
    goto :goto_48
.end method

.method public static newAssertQuery(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 169
    new-instance v0, Landroid/content/ContentProviderOperation$Builder;

    #@2
    const/4 v1, 0x4

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, v1, p0, v2}, Landroid/content/ContentProviderOperation$Builder;-><init>(ILandroid/net/Uri;Landroid/content/ContentProviderOperation$1;)V

    #@7
    return-object v0
.end method

.method public static newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 160
    new-instance v0, Landroid/content/ContentProviderOperation$Builder;

    #@2
    const/4 v1, 0x3

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, v1, p0, v2}, Landroid/content/ContentProviderOperation$Builder;-><init>(ILandroid/net/Uri;Landroid/content/ContentProviderOperation$1;)V

    #@7
    return-object v0
.end method

.method public static newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 142
    new-instance v0, Landroid/content/ContentProviderOperation$Builder;

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, v1, p0, v2}, Landroid/content/ContentProviderOperation$Builder;-><init>(ILandroid/net/Uri;Landroid/content/ContentProviderOperation$1;)V

    #@7
    return-object v0
.end method

.method public static newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 151
    new-instance v0, Landroid/content/ContentProviderOperation$Builder;

    #@2
    const/4 v1, 0x2

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, v1, p0, v2}, Landroid/content/ContentProviderOperation$Builder;-><init>(ILandroid/net/Uri;Landroid/content/ContentProviderOperation$1;)V

    #@7
    return-object v0
.end method


# virtual methods
.method public apply(Landroid/content/ContentProvider;[Landroid/content/ContentProviderResult;I)Landroid/content/ContentProviderResult;
    .registers 23
    .parameter "provider"
    .parameter "backRefs"
    .parameter "numBackRefs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    #@0
    .prologue
    .line 209
    move-object/from16 v0, p0

    #@2
    move-object/from16 v1, p2

    #@4
    move/from16 v2, p3

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation;->resolveValueBackReferences([Landroid/content/ContentProviderResult;I)Landroid/content/ContentValues;

    #@9
    move-result-object v18

    #@a
    .line 210
    .local v18, values:Landroid/content/ContentValues;
    move-object/from16 v0, p0

    #@c
    move-object/from16 v1, p2

    #@e
    move/from16 v2, p3

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation;->resolveSelectionArgsBackReferences([Landroid/content/ContentProviderResult;I)[Ljava/lang/String;

    #@13
    move-result-object v7

    #@14
    .line 213
    .local v7, selectionArgs:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@16
    iget v3, v0, Landroid/content/ContentProviderOperation;->mType:I

    #@18
    const/4 v4, 0x1

    #@19
    if-ne v3, v4, :cond_37

    #@1b
    .line 214
    move-object/from16 v0, p0

    #@1d
    iget-object v3, v0, Landroid/content/ContentProviderOperation;->mUri:Landroid/net/Uri;

    #@1f
    move-object/from16 v0, p1

    #@21
    move-object/from16 v1, v18

    #@23
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@26
    move-result-object v15

    #@27
    .line 215
    .local v15, newUri:Landroid/net/Uri;
    if-nez v15, :cond_31

    #@29
    .line 216
    new-instance v3, Landroid/content/OperationApplicationException;

    #@2b
    const-string v4, "insert failed"

    #@2d
    invoke-direct {v3, v4}, Landroid/content/OperationApplicationException;-><init>(Ljava/lang/String;)V

    #@30
    throw v3

    #@31
    .line 218
    :cond_31
    new-instance v3, Landroid/content/ContentProviderResult;

    #@33
    invoke-direct {v3, v15}, Landroid/content/ContentProviderResult;-><init>(Landroid/net/Uri;)V

    #@36
    .line 268
    .end local v15           #newUri:Landroid/net/Uri;
    :goto_36
    return-object v3

    #@37
    .line 222
    :cond_37
    move-object/from16 v0, p0

    #@39
    iget v3, v0, Landroid/content/ContentProviderOperation;->mType:I

    #@3b
    const/4 v4, 0x3

    #@3c
    if-ne v3, v4, :cond_83

    #@3e
    .line 223
    move-object/from16 v0, p0

    #@40
    iget-object v3, v0, Landroid/content/ContentProviderOperation;->mUri:Landroid/net/Uri;

    #@42
    move-object/from16 v0, p0

    #@44
    iget-object v4, v0, Landroid/content/ContentProviderOperation;->mSelection:Ljava/lang/String;

    #@46
    move-object/from16 v0, p1

    #@48
    invoke-virtual {v0, v3, v4, v7}, Landroid/content/ContentProvider;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@4b
    move-result v16

    #@4c
    .line 263
    .local v16, numRows:I
    :goto_4c
    move-object/from16 v0, p0

    #@4e
    iget-object v3, v0, Landroid/content/ContentProviderOperation;->mExpectedCount:Ljava/lang/Integer;

    #@50
    if-eqz v3, :cond_172

    #@52
    move-object/from16 v0, p0

    #@54
    iget-object v3, v0, Landroid/content/ContentProviderOperation;->mExpectedCount:Ljava/lang/Integer;

    #@56
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@59
    move-result v3

    #@5a
    move/from16 v0, v16

    #@5c
    if-eq v3, v0, :cond_172

    #@5e
    .line 264
    const-string v3, "ContentProviderOperation"

    #@60
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 265
    new-instance v3, Landroid/content/OperationApplicationException;

    #@69
    new-instance v4, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string/jumbo v6, "wrong number of rows: "

    #@71
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v4

    #@75
    move/from16 v0, v16

    #@77
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v4

    #@7b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v4

    #@7f
    invoke-direct {v3, v4}, Landroid/content/OperationApplicationException;-><init>(Ljava/lang/String;)V

    #@82
    throw v3

    #@83
    .line 224
    .end local v16           #numRows:I
    :cond_83
    move-object/from16 v0, p0

    #@85
    iget v3, v0, Landroid/content/ContentProviderOperation;->mType:I

    #@87
    const/4 v4, 0x2

    #@88
    if-ne v3, v4, :cond_9b

    #@8a
    .line 225
    move-object/from16 v0, p0

    #@8c
    iget-object v3, v0, Landroid/content/ContentProviderOperation;->mUri:Landroid/net/Uri;

    #@8e
    move-object/from16 v0, p0

    #@90
    iget-object v4, v0, Landroid/content/ContentProviderOperation;->mSelection:Ljava/lang/String;

    #@92
    move-object/from16 v0, p1

    #@94
    move-object/from16 v1, v18

    #@96
    invoke-virtual {v0, v3, v1, v4, v7}, Landroid/content/ContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@99
    move-result v16

    #@9a
    .restart local v16       #numRows:I
    goto :goto_4c

    #@9b
    .line 226
    .end local v16           #numRows:I
    :cond_9b
    move-object/from16 v0, p0

    #@9d
    iget v3, v0, Landroid/content/ContentProviderOperation;->mType:I

    #@9f
    const/4 v4, 0x4

    #@a0
    if-ne v3, v4, :cond_14c

    #@a2
    .line 228
    const/4 v5, 0x0

    #@a3
    .line 229
    .local v5, projection:[Ljava/lang/String;
    if-eqz v18, :cond_d6

    #@a5
    .line 231
    new-instance v17, Ljava/util/ArrayList;

    #@a7
    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    #@aa
    .line 232
    .local v17, projectionList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {v18 .. v18}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    #@ad
    move-result-object v3

    #@ae
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@b1
    move-result-object v14

    #@b2
    .local v14, i$:Ljava/util/Iterator;
    :goto_b2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    #@b5
    move-result v3

    #@b6
    if-eqz v3, :cond_c8

    #@b8
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@bb
    move-result-object v11

    #@bc
    check-cast v11, Ljava/util/Map$Entry;

    #@be
    .line 233
    .local v11, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@c1
    move-result-object v3

    #@c2
    move-object/from16 v0, v17

    #@c4
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c7
    goto :goto_b2

    #@c8
    .line 235
    .end local v11           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_c8
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@cb
    move-result v3

    #@cc
    new-array v3, v3, [Ljava/lang/String;

    #@ce
    move-object/from16 v0, v17

    #@d0
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@d3
    move-result-object v5

    #@d4
    .end local v5           #projection:[Ljava/lang/String;
    check-cast v5, [Ljava/lang/String;

    #@d6
    .line 237
    .end local v14           #i$:Ljava/util/Iterator;
    .end local v17           #projectionList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5       #projection:[Ljava/lang/String;
    :cond_d6
    move-object/from16 v0, p0

    #@d8
    iget-object v4, v0, Landroid/content/ContentProviderOperation;->mUri:Landroid/net/Uri;

    #@da
    move-object/from16 v0, p0

    #@dc
    iget-object v6, v0, Landroid/content/ContentProviderOperation;->mSelection:Ljava/lang/String;

    #@de
    const/4 v8, 0x0

    #@df
    move-object/from16 v3, p1

    #@e1
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e4
    move-result-object v9

    #@e5
    .line 239
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_e5
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    #@e8
    move-result v16

    #@e9
    .line 240
    .restart local v16       #numRows:I
    if-eqz v5, :cond_147

    #@eb
    .line 241
    :cond_eb
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@ee
    move-result v3

    #@ef
    if-eqz v3, :cond_147

    #@f1
    .line 242
    const/4 v13, 0x0

    #@f2
    .local v13, i:I
    :goto_f2
    array-length v3, v5

    #@f3
    if-ge v13, v3, :cond_eb

    #@f5
    .line 243
    invoke-interface {v9, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@f8
    move-result-object v10

    #@f9
    .line 244
    .local v10, cursorValue:Ljava/lang/String;
    aget-object v3, v5, v13

    #@fb
    move-object/from16 v0, v18

    #@fd
    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@100
    move-result-object v12

    #@101
    .line 245
    .local v12, expectedValue:Ljava/lang/String;
    invoke-static {v10, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@104
    move-result v3

    #@105
    if-nez v3, :cond_144

    #@107
    .line 247
    const-string v3, "ContentProviderOperation"

    #@109
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    #@10c
    move-result-object v4

    #@10d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    .line 248
    new-instance v3, Landroid/content/OperationApplicationException;

    #@112
    new-instance v4, Ljava/lang/StringBuilder;

    #@114
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@117
    const-string v6, "Found value "

    #@119
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v4

    #@11d
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v4

    #@121
    const-string v6, " when expected "

    #@123
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v4

    #@127
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v4

    #@12b
    const-string v6, " for column "

    #@12d
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v4

    #@131
    aget-object v6, v5, v13

    #@133
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v4

    #@137
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13a
    move-result-object v4

    #@13b
    invoke-direct {v3, v4}, Landroid/content/OperationApplicationException;-><init>(Ljava/lang/String;)V

    #@13e
    throw v3
    :try_end_13f
    .catchall {:try_start_e5 .. :try_end_13f} :catchall_13f

    #@13f
    .line 256
    .end local v10           #cursorValue:Ljava/lang/String;
    .end local v12           #expectedValue:Ljava/lang/String;
    .end local v13           #i:I
    .end local v16           #numRows:I
    :catchall_13f
    move-exception v3

    #@140
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@143
    throw v3

    #@144
    .line 242
    .restart local v10       #cursorValue:Ljava/lang/String;
    .restart local v12       #expectedValue:Ljava/lang/String;
    .restart local v13       #i:I
    .restart local v16       #numRows:I
    :cond_144
    add-int/lit8 v13, v13, 0x1

    #@146
    goto :goto_f2

    #@147
    .line 256
    .end local v10           #cursorValue:Ljava/lang/String;
    .end local v12           #expectedValue:Ljava/lang/String;
    .end local v13           #i:I
    :cond_147
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@14a
    goto/16 :goto_4c

    #@14c
    .line 259
    .end local v5           #projection:[Ljava/lang/String;
    .end local v9           #cursor:Landroid/database/Cursor;
    .end local v16           #numRows:I
    :cond_14c
    const-string v3, "ContentProviderOperation"

    #@14e
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    #@151
    move-result-object v4

    #@152
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@155
    .line 260
    new-instance v3, Ljava/lang/IllegalStateException;

    #@157
    new-instance v4, Ljava/lang/StringBuilder;

    #@159
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15c
    const-string v6, "bad type, "

    #@15e
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v4

    #@162
    move-object/from16 v0, p0

    #@164
    iget v6, v0, Landroid/content/ContentProviderOperation;->mType:I

    #@166
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@169
    move-result-object v4

    #@16a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16d
    move-result-object v4

    #@16e
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@171
    throw v3

    #@172
    .line 268
    .restart local v16       #numRows:I
    :cond_172
    new-instance v3, Landroid/content/ContentProviderResult;

    #@174
    move/from16 v0, v16

    #@176
    invoke-direct {v3, v0}, Landroid/content/ContentProviderResult;-><init>(I)V

    #@179
    goto/16 :goto_36
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 376
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 182
    iget v0, p0, Landroid/content/ContentProviderOperation;->mType:I

    #@2
    return v0
.end method

.method public getUri()Landroid/net/Uri;
    .registers 2

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Landroid/content/ContentProviderOperation;->mUri:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method public isReadOperation()Z
    .registers 3

    #@0
    .prologue
    .line 190
    iget v0, p0, Landroid/content/ContentProviderOperation;->mType:I

    #@2
    const/4 v1, 0x4

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public isWriteOperation()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 186
    iget v1, p0, Landroid/content/ContentProviderOperation;->mType:I

    #@3
    const/4 v2, 0x3

    #@4
    if-eq v1, v2, :cond_f

    #@6
    iget v1, p0, Landroid/content/ContentProviderOperation;->mType:I

    #@8
    if-eq v1, v0, :cond_f

    #@a
    iget v1, p0, Landroid/content/ContentProviderOperation;->mType:I

    #@c
    const/4 v2, 0x2

    #@d
    if-ne v1, v2, :cond_10

    #@f
    :cond_f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public isYieldAllowed()Z
    .registers 2

    #@0
    .prologue
    .line 177
    iget-boolean v0, p0, Landroid/content/ContentProviderOperation;->mYieldAllowed:Z

    #@2
    return v0
.end method

.method public resolveSelectionArgsBackReferences([Landroid/content/ContentProviderResult;I)[Ljava/lang/String;
    .registers 11
    .parameter "backRefs"
    .parameter "numBackRefs"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 323
    iget-object v5, p0, Landroid/content/ContentProviderOperation;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@3
    if-nez v5, :cond_8

    #@5
    .line 324
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mSelectionArgs:[Ljava/lang/String;

    #@7
    .line 335
    :cond_7
    return-object v2

    #@8
    .line 326
    :cond_8
    iget-object v5, p0, Landroid/content/ContentProviderOperation;->mSelectionArgs:[Ljava/lang/String;

    #@a
    array-length v5, v5

    #@b
    new-array v2, v5, [Ljava/lang/String;

    #@d
    .line 327
    .local v2, newArgs:[Ljava/lang/String;
    iget-object v5, p0, Landroid/content/ContentProviderOperation;->mSelectionArgs:[Ljava/lang/String;

    #@f
    iget-object v6, p0, Landroid/content/ContentProviderOperation;->mSelectionArgs:[Ljava/lang/String;

    #@11
    array-length v6, v6

    #@12
    invoke-static {v5, v7, v2, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@15
    .line 329
    iget-object v5, p0, Landroid/content/ContentProviderOperation;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@17
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@1a
    move-result-object v5

    #@1b
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1e
    move-result-object v1

    #@1f
    .local v1, i$:Ljava/util/Iterator;
    :goto_1f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_7

    #@25
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@28
    move-result-object v3

    #@29
    check-cast v3, Ljava/util/Map$Entry;

    #@2b
    .line 330
    .local v3, selectionArgBackRef:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2e
    move-result-object v4

    #@2f
    check-cast v4, Ljava/lang/Integer;

    #@31
    .line 331
    .local v4, selectionArgIndex:Ljava/lang/Integer;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@34
    move-result-object v5

    #@35
    check-cast v5, Ljava/lang/Integer;

    #@37
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@3a
    move-result v0

    #@3b
    .line 332
    .local v0, backRefIndex:I
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@3e
    move-result v5

    #@3f
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@42
    move-result-object v6

    #@43
    invoke-direct {p0, p1, p2, v6}, Landroid/content/ContentProviderOperation;->backRefToValue([Landroid/content/ContentProviderResult;ILjava/lang/Integer;)J

    #@46
    move-result-wide v6

    #@47
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    aput-object v6, v2, v5

    #@4d
    goto :goto_1f
.end method

.method public resolveValueBackReferences([Landroid/content/ContentProviderResult;I)Landroid/content/ContentValues;
    .registers 11
    .parameter "backRefs"
    .parameter "numBackRefs"

    #@0
    .prologue
    .line 286
    iget-object v5, p0, Landroid/content/ContentProviderOperation;->mValuesBackReferences:Landroid/content/ContentValues;

    #@2
    if-nez v5, :cond_7

    #@4
    .line 287
    iget-object v4, p0, Landroid/content/ContentProviderOperation;->mValues:Landroid/content/ContentValues;

    #@6
    .line 304
    :cond_6
    return-object v4

    #@7
    .line 290
    :cond_7
    iget-object v5, p0, Landroid/content/ContentProviderOperation;->mValues:Landroid/content/ContentValues;

    #@9
    if-nez v5, :cond_5d

    #@b
    .line 291
    new-instance v4, Landroid/content/ContentValues;

    #@d
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@10
    .line 295
    .local v4, values:Landroid/content/ContentValues;
    :goto_10
    iget-object v5, p0, Landroid/content/ContentProviderOperation;->mValuesBackReferences:Landroid/content/ContentValues;

    #@12
    invoke-virtual {v5}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    #@15
    move-result-object v5

    #@16
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v2

    #@1a
    .local v2, i$:Ljava/util/Iterator;
    :goto_1a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v5

    #@1e
    if-eqz v5, :cond_6

    #@20
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Ljava/util/Map$Entry;

    #@26
    .line 296
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@29
    move-result-object v3

    #@2a
    check-cast v3, Ljava/lang/String;

    #@2c
    .line 297
    .local v3, key:Ljava/lang/String;
    iget-object v5, p0, Landroid/content/ContentProviderOperation;->mValuesBackReferences:Landroid/content/ContentValues;

    #@2e
    invoke-virtual {v5, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    #@31
    move-result-object v0

    #@32
    .line 298
    .local v0, backRefIndex:Ljava/lang/Integer;
    if-nez v0, :cond_65

    #@34
    .line 299
    const-string v5, "ContentProviderOperation"

    #@36
    invoke-virtual {p0}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    #@39
    move-result-object v6

    #@3a
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 300
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@3f
    new-instance v6, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string/jumbo v7, "values backref "

    #@47
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    const-string v7, " is not an integer"

    #@51
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v6

    #@59
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5c
    throw v5

    #@5d
    .line 293
    .end local v0           #backRefIndex:Ljava/lang/Integer;
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #key:Ljava/lang/String;
    .end local v4           #values:Landroid/content/ContentValues;
    :cond_5d
    new-instance v4, Landroid/content/ContentValues;

    #@5f
    iget-object v5, p0, Landroid/content/ContentProviderOperation;->mValues:Landroid/content/ContentValues;

    #@61
    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    #@64
    .restart local v4       #values:Landroid/content/ContentValues;
    goto :goto_10

    #@65
    .line 302
    .restart local v0       #backRefIndex:Ljava/lang/Integer;
    .restart local v1       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #key:Ljava/lang/String;
    :cond_65
    invoke-direct {p0, p1, p2, v0}, Landroid/content/ContentProviderOperation;->backRefToValue([Landroid/content/ContentProviderResult;ILjava/lang/Integer;)J

    #@68
    move-result-wide v5

    #@69
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@70
    goto :goto_1a
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 340
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v1, "mType: "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    iget v1, p0, Landroid/content/ContentProviderOperation;->mType:I

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, ", mUri: "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    iget-object v1, p0, Landroid/content/ContentProviderOperation;->mUri:Landroid/net/Uri;

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, ", mSelection: "

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    iget-object v1, p0, Landroid/content/ContentProviderOperation;->mSelection:Ljava/lang/String;

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    const-string v1, ", mExpectedCount: "

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    iget-object v1, p0, Landroid/content/ContentProviderOperation;->mExpectedCount:Ljava/lang/Integer;

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    const-string v1, ", mYieldAllowed: "

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    iget-boolean v1, p0, Landroid/content/ContentProviderOperation;->mYieldAllowed:Z

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    const-string v1, ", mValues: "

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v0

    #@48
    iget-object v1, p0, Landroid/content/ContentProviderOperation;->mValues:Landroid/content/ContentValues;

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v0

    #@4e
    const-string v1, ", mValuesBackReferences: "

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    iget-object v1, p0, Landroid/content/ContentProviderOperation;->mValuesBackReferences:Landroid/content/ContentValues;

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v0

    #@5a
    const-string v1, ", mSelectionArgsBackReferences: "

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v0

    #@60
    iget-object v1, p0, Landroid/content/ContentProviderOperation;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v0

    #@66
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v0

    #@6a
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 91
    iget v2, p0, Landroid/content/ContentProviderOperation;->mType:I

    #@4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 92
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mUri:Landroid/net/Uri;

    #@9
    invoke-static {p1, v2}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;Landroid/net/Uri;)V

    #@c
    .line 93
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mValues:Landroid/content/ContentValues;

    #@e
    if-eqz v2, :cond_8d

    #@10
    .line 94
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 95
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mValues:Landroid/content/ContentValues;

    #@15
    invoke-virtual {v2, p1, v4}, Landroid/content/ContentValues;->writeToParcel(Landroid/os/Parcel;I)V

    #@18
    .line 99
    :goto_18
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mSelection:Ljava/lang/String;

    #@1a
    if-eqz v2, :cond_91

    #@1c
    .line 100
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 101
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mSelection:Ljava/lang/String;

    #@21
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@24
    .line 105
    :goto_24
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mSelectionArgs:[Ljava/lang/String;

    #@26
    if-eqz v2, :cond_95

    #@28
    .line 106
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 107
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mSelectionArgs:[Ljava/lang/String;

    #@2d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@30
    .line 111
    :goto_30
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mExpectedCount:Ljava/lang/Integer;

    #@32
    if-eqz v2, :cond_99

    #@34
    .line 112
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    .line 113
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mExpectedCount:Ljava/lang/Integer;

    #@39
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@3c
    move-result v2

    #@3d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@40
    .line 117
    :goto_40
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mValuesBackReferences:Landroid/content/ContentValues;

    #@42
    if-eqz v2, :cond_9d

    #@44
    .line 118
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@47
    .line 119
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mValuesBackReferences:Landroid/content/ContentValues;

    #@49
    invoke-virtual {v2, p1, v4}, Landroid/content/ContentValues;->writeToParcel(Landroid/os/Parcel;I)V

    #@4c
    .line 123
    :goto_4c
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@4e
    if-eqz v2, :cond_a1

    #@50
    .line 124
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@53
    .line 125
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@55
    invoke-interface {v2}, Ljava/util/Map;->size()I

    #@58
    move-result v2

    #@59
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@5c
    .line 126
    iget-object v2, p0, Landroid/content/ContentProviderOperation;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@5e
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@61
    move-result-object v2

    #@62
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@65
    move-result-object v1

    #@66
    .local v1, i$:Ljava/util/Iterator;
    :goto_66
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@69
    move-result v2

    #@6a
    if-eqz v2, :cond_a4

    #@6c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6f
    move-result-object v0

    #@70
    check-cast v0, Ljava/util/Map$Entry;

    #@72
    .line 127
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@75
    move-result-object v2

    #@76
    check-cast v2, Ljava/lang/Integer;

    #@78
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@7b
    move-result v2

    #@7c
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@7f
    .line 128
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@82
    move-result-object v2

    #@83
    check-cast v2, Ljava/lang/Integer;

    #@85
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@88
    move-result v2

    #@89
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@8c
    goto :goto_66

    #@8d
    .line 97
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_8d
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@90
    goto :goto_18

    #@91
    .line 103
    :cond_91
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@94
    goto :goto_24

    #@95
    .line 109
    :cond_95
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@98
    goto :goto_30

    #@99
    .line 115
    :cond_99
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@9c
    goto :goto_40

    #@9d
    .line 121
    :cond_9d
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@a0
    goto :goto_4c

    #@a1
    .line 131
    :cond_a1
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@a4
    .line 133
    :cond_a4
    iget-boolean v2, p0, Landroid/content/ContentProviderOperation;->mYieldAllowed:Z

    #@a6
    if-eqz v2, :cond_ad

    #@a8
    move v2, v3

    #@a9
    :goto_a9
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@ac
    .line 134
    return-void

    #@ad
    :cond_ad
    move v2, v4

    #@ae
    .line 133
    goto :goto_a9
.end method
