.class public Landroid/content/SyncManager;
.super Ljava/lang/Object;
.source "SyncManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/SyncManager$SyncHandler;,
        Landroid/content/SyncManager$ServiceConnectionData;,
        Landroid/content/SyncManager$SyncTimeTracker;,
        Landroid/content/SyncManager$AccountSyncStats;,
        Landroid/content/SyncManager$AuthoritySyncStats;,
        Landroid/content/SyncManager$ActiveSyncContext;,
        Landroid/content/SyncManager$SyncAlarmIntentReceiver;,
        Landroid/content/SyncManager$SyncHandlerMessagePayload;
    }
.end annotation


# static fields
.field private static final ACTION_SYNC_ALARM:Ljava/lang/String; = "android.content.syncmanager.SYNC_ALARM"

.field private static final DEFAULT_MAX_SYNC_RETRY_TIME_IN_SECONDS:J = 0xe10L

.field private static final DELAY_RETRY_SYNC_IN_PROGRESS_IN_SECONDS:I = 0xa

.field private static final HANDLE_SYNC_ALARM_WAKE_LOCK:Ljava/lang/String; = "SyncManagerHandleSyncAlarm"

.field private static final INITIALIZATION_UNBIND_DELAY_MS:I = 0x1388

.field private static final INITIAL_ACCOUNTS_ARRAY:[Landroid/accounts/AccountAndUser; = null

.field private static final INITIAL_SYNC_RETRY_TIME_IN_MS:J = 0x7530L

#the value of this static final field might be set in the static constructor
.field private static final LOCAL_SYNC_DELAY:J = 0x0L

#the value of this static final field might be set in the static constructor
.field private static final MAX_SIMULTANEOUS_INITIALIZATION_SYNCS:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final MAX_SIMULTANEOUS_REGULAR_SYNCS:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final MAX_TIME_PER_SYNC:J = 0x0L

.field private static final SYNC_ALARM_TIMEOUT_MAX:J = 0x6ddd00L

.field private static final SYNC_ALARM_TIMEOUT_MIN:J = 0x7530L

.field private static final SYNC_LOOP_WAKE_LOCK:Ljava/lang/String; = "SyncLoopWakeLock"

#the value of this static final field might be set in the static constructor
.field private static final SYNC_NOTIFICATION_DELAY:J = 0x0L

.field private static final SYNC_WAKE_LOCK_PREFIX:Ljava/lang/String; = "*sync*"

.field private static final TAG:Ljava/lang/String; = "SyncManager"


# instance fields
.field private mAccountsUpdatedReceiver:Landroid/content/BroadcastReceiver;

.field protected final mActiveSyncContexts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/SyncManager$ActiveSyncContext;",
            ">;"
        }
    .end annotation
.end field

.field private mAlarmService:Landroid/app/AlarmManager;

.field private mBackgroundDataSettingChanged:Landroid/content/BroadcastReceiver;

.field private volatile mBootCompleted:Z

.field private mBootCompletedReceiver:Landroid/content/BroadcastReceiver;

.field private mConnManagerDoNotUseDirectly:Landroid/net/ConnectivityManager;

.field private mConnectivityIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private volatile mDataConnectionIsConnected:Z

.field private volatile mHandleAlarmWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mNeedSyncActiveNotification:Z

.field private final mNotificationMgr:Landroid/app/NotificationManager;

.field private final mPowerManager:Landroid/os/PowerManager;

.field private volatile mRunningAccounts:[Landroid/accounts/AccountAndUser;

.field private mShutdownIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mStorageIntentReceiver:Landroid/content/BroadcastReceiver;

.field private volatile mStorageIsLow:Z

.field protected mSyncAdapters:Landroid/content/SyncAdaptersCache;

.field private final mSyncAlarmIntent:Landroid/app/PendingIntent;

.field private final mSyncHandler:Landroid/content/SyncManager$SyncHandler;

.field private volatile mSyncManagerWakeLock:Landroid/os/PowerManager$WakeLock;

.field private final mSyncQueue:Landroid/content/SyncQueue;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mSyncQueue"
    .end annotation
.end field

.field private mSyncRandomOffsetMillis:I

.field private mSyncStorageEngine:Landroid/content/SyncStorageEngine;

.field private mUserIntentReceiver:Landroid/content/BroadcastReceiver;

.field private final mUserManager:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const-wide/16 v6, 0x7530

    #@2
    const/4 v1, 0x2

    #@3
    .line 99
    invoke-static {}, Landroid/app/ActivityManager;->isLargeRAM()Z

    #@6
    move-result v2

    #@7
    .line 100
    .local v2, isLargeRAM:Z
    if-eqz v2, :cond_42

    #@9
    const/4 v0, 0x5

    #@a
    .line 101
    .local v0, defaultMaxInitSyncs:I
    :goto_a
    if-eqz v2, :cond_44

    #@c
    .line 102
    .local v1, defaultMaxRegularSyncs:I
    :goto_c
    const-string/jumbo v3, "sync.max_init_syncs"

    #@f
    invoke-static {v3, v0}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@12
    move-result v3

    #@13
    sput v3, Landroid/content/SyncManager;->MAX_SIMULTANEOUS_INITIALIZATION_SYNCS:I

    #@15
    .line 104
    const-string/jumbo v3, "sync.max_regular_syncs"

    #@18
    invoke-static {v3, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@1b
    move-result v3

    #@1c
    sput v3, Landroid/content/SyncManager;->MAX_SIMULTANEOUS_REGULAR_SYNCS:I

    #@1e
    .line 106
    const-string/jumbo v3, "sync.local_sync_delay"

    #@21
    invoke-static {v3, v6, v7}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    #@24
    move-result-wide v3

    #@25
    sput-wide v3, Landroid/content/SyncManager;->LOCAL_SYNC_DELAY:J

    #@27
    .line 108
    const-string/jumbo v3, "sync.max_time_per_sync"

    #@2a
    const-wide/32 v4, 0x493e0

    #@2d
    invoke-static {v3, v4, v5}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    #@30
    move-result-wide v3

    #@31
    sput-wide v3, Landroid/content/SyncManager;->MAX_TIME_PER_SYNC:J

    #@33
    .line 110
    const-string/jumbo v3, "sync.notification_delay"

    #@36
    invoke-static {v3, v6, v7}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    #@39
    move-result-wide v3

    #@3a
    sput-wide v3, Landroid/content/SyncManager;->SYNC_NOTIFICATION_DELAY:J

    #@3c
    .line 144
    const/4 v3, 0x0

    #@3d
    new-array v3, v3, [Landroid/accounts/AccountAndUser;

    #@3f
    sput-object v3, Landroid/content/SyncManager;->INITIAL_ACCOUNTS_ARRAY:[Landroid/accounts/AccountAndUser;

    #@41
    return-void

    #@42
    .end local v0           #defaultMaxInitSyncs:I
    .end local v1           #defaultMaxRegularSyncs:I
    :cond_42
    move v0, v1

    #@43
    .line 100
    goto :goto_a

    #@44
    .line 101
    .restart local v0       #defaultMaxInitSyncs:I
    :cond_44
    const/4 v1, 0x1

    #@45
    goto :goto_c
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .registers 15
    .parameter "context"
    .parameter "factoryTest"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    .line 346
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 147
    sget-object v0, Landroid/content/SyncManager;->INITIAL_ACCOUNTS_ARRAY:[Landroid/accounts/AccountAndUser;

    #@8
    iput-object v0, p0, Landroid/content/SyncManager;->mRunningAccounts:[Landroid/accounts/AccountAndUser;

    #@a
    .line 151
    iput-boolean v6, p0, Landroid/content/SyncManager;->mDataConnectionIsConnected:Z

    #@c
    .line 152
    iput-boolean v6, p0, Landroid/content/SyncManager;->mStorageIsLow:Z

    #@e
    .line 155
    iput-object v4, p0, Landroid/content/SyncManager;->mAlarmService:Landroid/app/AlarmManager;

    #@10
    .line 162
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@16
    .line 165
    iput-boolean v6, p0, Landroid/content/SyncManager;->mNeedSyncActiveNotification:Z

    #@18
    .line 174
    new-instance v0, Landroid/content/SyncManager$1;

    #@1a
    invoke-direct {v0, p0}, Landroid/content/SyncManager$1;-><init>(Landroid/content/SyncManager;)V

    #@1d
    iput-object v0, p0, Landroid/content/SyncManager;->mStorageIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1f
    .line 195
    new-instance v0, Landroid/content/SyncManager$2;

    #@21
    invoke-direct {v0, p0}, Landroid/content/SyncManager$2;-><init>(Landroid/content/SyncManager;)V

    #@24
    iput-object v0, p0, Landroid/content/SyncManager;->mBootCompletedReceiver:Landroid/content/BroadcastReceiver;

    #@26
    .line 201
    new-instance v0, Landroid/content/SyncManager$3;

    #@28
    invoke-direct {v0, p0}, Landroid/content/SyncManager$3;-><init>(Landroid/content/SyncManager;)V

    #@2b
    iput-object v0, p0, Landroid/content/SyncManager;->mBackgroundDataSettingChanged:Landroid/content/BroadcastReceiver;

    #@2d
    .line 211
    new-instance v0, Landroid/content/SyncManager$4;

    #@2f
    invoke-direct {v0, p0}, Landroid/content/SyncManager$4;-><init>(Landroid/content/SyncManager;)V

    #@32
    iput-object v0, p0, Landroid/content/SyncManager;->mAccountsUpdatedReceiver:Landroid/content/BroadcastReceiver;

    #@34
    .line 277
    new-instance v0, Landroid/content/SyncManager$5;

    #@36
    invoke-direct {v0, p0}, Landroid/content/SyncManager$5;-><init>(Landroid/content/SyncManager;)V

    #@39
    iput-object v0, p0, Landroid/content/SyncManager;->mConnectivityIntentReceiver:Landroid/content/BroadcastReceiver;

    #@3b
    .line 302
    new-instance v0, Landroid/content/SyncManager$6;

    #@3d
    invoke-direct {v0, p0}, Landroid/content/SyncManager$6;-><init>(Landroid/content/SyncManager;)V

    #@40
    iput-object v0, p0, Landroid/content/SyncManager;->mShutdownIntentReceiver:Landroid/content/BroadcastReceiver;

    #@42
    .line 310
    new-instance v0, Landroid/content/SyncManager$7;

    #@44
    invoke-direct {v0, p0}, Landroid/content/SyncManager$7;-><init>(Landroid/content/SyncManager;)V

    #@47
    iput-object v0, p0, Landroid/content/SyncManager;->mUserIntentReceiver:Landroid/content/BroadcastReceiver;

    #@49
    .line 330
    iput-boolean v6, p0, Landroid/content/SyncManager;->mBootCompleted:Z

    #@4b
    .line 349
    iput-object p1, p0, Landroid/content/SyncManager;->mContext:Landroid/content/Context;

    #@4d
    .line 351
    invoke-static {p1}, Landroid/content/SyncStorageEngine;->init(Landroid/content/Context;)V

    #@50
    .line 352
    invoke-static {}, Landroid/content/SyncStorageEngine;->getSingleton()Landroid/content/SyncStorageEngine;

    #@53
    move-result-object v0

    #@54
    iput-object v0, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@56
    .line 353
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@58
    new-instance v1, Landroid/content/SyncManager$8;

    #@5a
    invoke-direct {v1, p0}, Landroid/content/SyncManager$8;-><init>(Landroid/content/SyncManager;)V

    #@5d
    invoke-virtual {v0, v1}, Landroid/content/SyncStorageEngine;->setOnSyncRequestListener(Landroid/content/SyncStorageEngine$OnSyncRequestListener;)V

    #@60
    .line 360
    new-instance v0, Landroid/content/SyncAdaptersCache;

    #@62
    iget-object v1, p0, Landroid/content/SyncManager;->mContext:Landroid/content/Context;

    #@64
    invoke-direct {v0, v1}, Landroid/content/SyncAdaptersCache;-><init>(Landroid/content/Context;)V

    #@67
    iput-object v0, p0, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@69
    .line 361
    new-instance v0, Landroid/content/SyncQueue;

    #@6b
    iget-object v1, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@6d
    iget-object v2, p0, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@6f
    invoke-direct {v0, v1, v2}, Landroid/content/SyncQueue;-><init>(Landroid/content/SyncStorageEngine;Landroid/content/SyncAdaptersCache;)V

    #@72
    iput-object v0, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@74
    .line 363
    new-instance v11, Landroid/os/HandlerThread;

    #@76
    const-string v0, "SyncHandlerThread"

    #@78
    const/16 v1, 0xa

    #@7a
    invoke-direct {v11, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    #@7d
    .line 365
    .local v11, syncThread:Landroid/os/HandlerThread;
    invoke-virtual {v11}, Landroid/os/HandlerThread;->start()V

    #@80
    .line 366
    new-instance v0, Landroid/content/SyncManager$SyncHandler;

    #@82
    invoke-virtual {v11}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@85
    move-result-object v1

    #@86
    invoke-direct {v0, p0, v1}, Landroid/content/SyncManager$SyncHandler;-><init>(Landroid/content/SyncManager;Landroid/os/Looper;)V

    #@89
    iput-object v0, p0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@8b
    .line 368
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@8d
    new-instance v1, Landroid/content/SyncManager$9;

    #@8f
    invoke-direct {v1, p0}, Landroid/content/SyncManager$9;-><init>(Landroid/content/SyncManager;)V

    #@92
    iget-object v2, p0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@94
    invoke-virtual {v0, v1, v2}, Landroid/content/SyncAdaptersCache;->setListener(Landroid/content/pm/RegisteredServicesCacheListener;Landroid/os/Handler;)V

    #@97
    .line 378
    iget-object v0, p0, Landroid/content/SyncManager;->mContext:Landroid/content/Context;

    #@99
    new-instance v1, Landroid/content/Intent;

    #@9b
    const-string v2, "android.content.syncmanager.SYNC_ALARM"

    #@9d
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a0
    invoke-static {v0, v6, v1, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@a3
    move-result-object v0

    #@a4
    iput-object v0, p0, Landroid/content/SyncManager;->mSyncAlarmIntent:Landroid/app/PendingIntent;

    #@a6
    .line 381
    new-instance v3, Landroid/content/IntentFilter;

    #@a8
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    #@aa
    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@ad
    .line 382
    .local v3, intentFilter:Landroid/content/IntentFilter;
    iget-object v0, p0, Landroid/content/SyncManager;->mConnectivityIntentReceiver:Landroid/content/BroadcastReceiver;

    #@af
    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@b2
    .line 384
    if-nez p2, :cond_c0

    #@b4
    .line 385
    new-instance v3, Landroid/content/IntentFilter;

    #@b6
    .end local v3           #intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    #@b8
    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@bb
    .line 386
    .restart local v3       #intentFilter:Landroid/content/IntentFilter;
    iget-object v0, p0, Landroid/content/SyncManager;->mBootCompletedReceiver:Landroid/content/BroadcastReceiver;

    #@bd
    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@c0
    .line 389
    :cond_c0
    new-instance v3, Landroid/content/IntentFilter;

    #@c2
    .end local v3           #intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"

    #@c4
    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@c7
    .line 390
    .restart local v3       #intentFilter:Landroid/content/IntentFilter;
    iget-object v0, p0, Landroid/content/SyncManager;->mBackgroundDataSettingChanged:Landroid/content/BroadcastReceiver;

    #@c9
    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@cc
    .line 392
    new-instance v3, Landroid/content/IntentFilter;

    #@ce
    .end local v3           #intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.DEVICE_STORAGE_LOW"

    #@d0
    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@d3
    .line 393
    .restart local v3       #intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.DEVICE_STORAGE_OK"

    #@d5
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d8
    .line 394
    iget-object v0, p0, Landroid/content/SyncManager;->mStorageIntentReceiver:Landroid/content/BroadcastReceiver;

    #@da
    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@dd
    .line 396
    new-instance v3, Landroid/content/IntentFilter;

    #@df
    .end local v3           #intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.ACTION_SHUTDOWN"

    #@e1
    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@e4
    .line 397
    .restart local v3       #intentFilter:Landroid/content/IntentFilter;
    const/16 v0, 0x64

    #@e6
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->setPriority(I)V

    #@e9
    .line 398
    iget-object v0, p0, Landroid/content/SyncManager;->mShutdownIntentReceiver:Landroid/content/BroadcastReceiver;

    #@eb
    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@ee
    .line 400
    new-instance v3, Landroid/content/IntentFilter;

    #@f0
    .end local v3           #intentFilter:Landroid/content/IntentFilter;
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@f3
    .line 401
    .restart local v3       #intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.USER_REMOVED"

    #@f5
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f8
    .line 402
    const-string v0, "android.intent.action.USER_STARTING"

    #@fa
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@fd
    .line 403
    const-string v0, "android.intent.action.USER_STOPPING"

    #@ff
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@102
    .line 404
    iget-object v0, p0, Landroid/content/SyncManager;->mContext:Landroid/content/Context;

    #@104
    iget-object v1, p0, Landroid/content/SyncManager;->mUserIntentReceiver:Landroid/content/BroadcastReceiver;

    #@106
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@108
    move-object v5, v4

    #@109
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@10c
    .line 407
    if-nez p2, :cond_187

    #@10e
    .line 408
    const-string/jumbo v0, "notification"

    #@111
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@114
    move-result-object v0

    #@115
    check-cast v0, Landroid/app/NotificationManager;

    #@117
    iput-object v0, p0, Landroid/content/SyncManager;->mNotificationMgr:Landroid/app/NotificationManager;

    #@119
    .line 410
    new-instance v0, Landroid/content/SyncManager$SyncAlarmIntentReceiver;

    #@11b
    invoke-direct {v0, p0}, Landroid/content/SyncManager$SyncAlarmIntentReceiver;-><init>(Landroid/content/SyncManager;)V

    #@11e
    new-instance v1, Landroid/content/IntentFilter;

    #@120
    const-string v2, "android.content.syncmanager.SYNC_ALARM"

    #@122
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@125
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@128
    .line 415
    :goto_128
    const-string/jumbo v0, "power"

    #@12b
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@12e
    move-result-object v0

    #@12f
    check-cast v0, Landroid/os/PowerManager;

    #@131
    iput-object v0, p0, Landroid/content/SyncManager;->mPowerManager:Landroid/os/PowerManager;

    #@133
    .line 416
    iget-object v0, p0, Landroid/content/SyncManager;->mContext:Landroid/content/Context;

    #@135
    const-string/jumbo v1, "user"

    #@138
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13b
    move-result-object v0

    #@13c
    check-cast v0, Landroid/os/UserManager;

    #@13e
    iput-object v0, p0, Landroid/content/SyncManager;->mUserManager:Landroid/os/UserManager;

    #@140
    .line 422
    iget-object v0, p0, Landroid/content/SyncManager;->mPowerManager:Landroid/os/PowerManager;

    #@142
    const-string v1, "SyncManagerHandleSyncAlarm"

    #@144
    invoke-virtual {v0, v7, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@147
    move-result-object v0

    #@148
    iput-object v0, p0, Landroid/content/SyncManager;->mHandleAlarmWakeLock:Landroid/os/PowerManager$WakeLock;

    #@14a
    .line 424
    iget-object v0, p0, Landroid/content/SyncManager;->mHandleAlarmWakeLock:Landroid/os/PowerManager$WakeLock;

    #@14c
    invoke-virtual {v0, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@14f
    .line 431
    iget-object v0, p0, Landroid/content/SyncManager;->mPowerManager:Landroid/os/PowerManager;

    #@151
    const-string v1, "SyncLoopWakeLock"

    #@153
    invoke-virtual {v0, v7, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@156
    move-result-object v0

    #@157
    iput-object v0, p0, Landroid/content/SyncManager;->mSyncManagerWakeLock:Landroid/os/PowerManager$WakeLock;

    #@159
    .line 433
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncManagerWakeLock:Landroid/os/PowerManager$WakeLock;

    #@15b
    invoke-virtual {v0, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@15e
    .line 435
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@160
    new-instance v1, Landroid/content/SyncManager$10;

    #@162
    invoke-direct {v1, p0}, Landroid/content/SyncManager$10;-><init>(Landroid/content/SyncManager;)V

    #@165
    invoke-virtual {v0, v7, v1}, Landroid/content/SyncStorageEngine;->addStatusChangeListener(ILandroid/content/ISyncStatusObserver;)V

    #@168
    .line 443
    if-nez p2, :cond_17c

    #@16a
    .line 445
    iget-object v5, p0, Landroid/content/SyncManager;->mContext:Landroid/content/Context;

    #@16c
    iget-object v6, p0, Landroid/content/SyncManager;->mAccountsUpdatedReceiver:Landroid/content/BroadcastReceiver;

    #@16e
    sget-object v7, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@170
    new-instance v8, Landroid/content/IntentFilter;

    #@172
    const-string v0, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    #@174
    invoke-direct {v8, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@177
    move-object v9, v4

    #@178
    move-object v10, v4

    #@179
    invoke-virtual/range {v5 .. v10}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@17c
    .line 452
    :cond_17c
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@17e
    invoke-virtual {v0}, Landroid/content/SyncStorageEngine;->getSyncRandomOffset()I

    #@181
    move-result v0

    #@182
    mul-int/lit16 v0, v0, 0x3e8

    #@184
    iput v0, p0, Landroid/content/SyncManager;->mSyncRandomOffsetMillis:I

    #@186
    .line 453
    return-void

    #@187
    .line 413
    :cond_187
    iput-object v4, p0, Landroid/content/SyncManager;->mNotificationMgr:Landroid/app/NotificationManager;

    #@189
    goto :goto_128
.end method

.method static synthetic access$000(Landroid/content/SyncManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-boolean v0, p0, Landroid/content/SyncManager;->mStorageIsLow:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Landroid/content/SyncManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-boolean p1, p0, Landroid/content/SyncManager;->mStorageIsLow:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Landroid/content/SyncManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 86
    invoke-direct {p0}, Landroid/content/SyncManager;->sendCheckAlarmsMessage()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Landroid/content/SyncManager;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1}, Landroid/content/SyncManager;->onUserStopping(I)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Landroid/content/SyncManager;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mHandleAlarmWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Landroid/content/SyncManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 86
    invoke-direct {p0}, Landroid/content/SyncManager;->sendSyncAlarmMessage()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Landroid/content/SyncManager;Landroid/content/SyncManager$ActiveSyncContext;Landroid/content/SyncResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Landroid/content/SyncManager;->sendSyncFinishedOrCanceledMessage(Landroid/content/SyncManager$ActiveSyncContext;Landroid/content/SyncResult;)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Landroid/content/SyncManager;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/content/SyncManager;)Landroid/content/SyncManager$SyncHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$2002(Landroid/content/SyncManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-boolean p1, p0, Landroid/content/SyncManager;->mBootCompleted:Z

    #@2
    return p1
.end method

.method static synthetic access$2100(Landroid/content/SyncManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 86
    invoke-direct {p0}, Landroid/content/SyncManager;->doDatabaseCleanup()V

    #@3
    return-void
.end method

.method static synthetic access$2200(Landroid/content/SyncManager;)Landroid/os/PowerManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mPowerManager:Landroid/os/PowerManager;

    #@2
    return-object v0
.end method

.method static synthetic access$2300(Landroid/content/SyncManager;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncManagerWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Landroid/content/SyncManager;Landroid/content/SyncManager$ActiveSyncContext;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1}, Landroid/content/SyncManager;->isSyncStillActive(Landroid/content/SyncManager$ActiveSyncContext;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2500(Landroid/content/SyncManager;)[Landroid/accounts/AccountAndUser;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mRunningAccounts:[Landroid/accounts/AccountAndUser;

    #@2
    return-object v0
.end method

.method static synthetic access$2600(Landroid/content/SyncManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget v0, p0, Landroid/content/SyncManager;->mSyncRandomOffsetMillis:I

    #@2
    return v0
.end method

.method static synthetic access$2700(Landroid/content/SyncManager;[Landroid/accounts/AccountAndUser;Landroid/accounts/Account;I)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1, p2, p3}, Landroid/content/SyncManager;->containsAccountAndUser([Landroid/accounts/AccountAndUser;Landroid/accounts/Account;I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2800()[Landroid/accounts/AccountAndUser;
    .registers 1

    #@0
    .prologue
    .line 86
    sget-object v0, Landroid/content/SyncManager;->INITIAL_ACCOUNTS_ARRAY:[Landroid/accounts/AccountAndUser;

    #@2
    return-object v0
.end method

.method static synthetic access$2900(Landroid/content/SyncManager;)Landroid/os/UserManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mUserManager:Landroid/os/UserManager;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/content/SyncManager;)Landroid/net/ConnectivityManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    invoke-direct {p0}, Landroid/content/SyncManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$3000()J
    .registers 2

    #@0
    .prologue
    .line 86
    sget-wide v0, Landroid/content/SyncManager;->MAX_TIME_PER_SYNC:J

    #@2
    return-wide v0
.end method

.method static synthetic access$3100()I
    .registers 1

    #@0
    .prologue
    .line 86
    sget v0, Landroid/content/SyncManager;->MAX_SIMULTANEOUS_INITIALIZATION_SYNCS:I

    #@2
    return v0
.end method

.method static synthetic access$3200()I
    .registers 1

    #@0
    .prologue
    .line 86
    sget v0, Landroid/content/SyncManager;->MAX_SIMULTANEOUS_REGULAR_SYNCS:I

    #@2
    return v0
.end method

.method static synthetic access$3300(Landroid/content/SyncManager;Landroid/content/SyncOperation;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1}, Landroid/content/SyncManager;->increaseBackoffSetting(Landroid/content/SyncOperation;)V

    #@3
    return-void
.end method

.method static synthetic access$3400(Landroid/content/SyncManager;Landroid/content/SyncOperation;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1}, Landroid/content/SyncManager;->clearBackoffSetting(Landroid/content/SyncOperation;)V

    #@3
    return-void
.end method

.method static synthetic access$3500(Landroid/content/SyncManager;Landroid/content/SyncOperation;J)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1, p2, p3}, Landroid/content/SyncManager;->setDelayUntilTime(Landroid/content/SyncOperation;J)V

    #@3
    return-void
.end method

.method static synthetic access$3600(Landroid/content/SyncManager;)Landroid/app/NotificationManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mNotificationMgr:Landroid/app/NotificationManager;

    #@2
    return-object v0
.end method

.method static synthetic access$3700()J
    .registers 2

    #@0
    .prologue
    .line 86
    sget-wide v0, Landroid/content/SyncManager;->SYNC_NOTIFICATION_DELAY:J

    #@2
    return-wide v0
.end method

.method static synthetic access$3800(Landroid/content/SyncManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-boolean v0, p0, Landroid/content/SyncManager;->mNeedSyncActiveNotification:Z

    #@2
    return v0
.end method

.method static synthetic access$3802(Landroid/content/SyncManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-boolean p1, p0, Landroid/content/SyncManager;->mNeedSyncActiveNotification:Z

    #@2
    return p1
.end method

.method static synthetic access$3900(Landroid/content/SyncManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 86
    invoke-direct {p0}, Landroid/content/SyncManager;->ensureAlarmService()V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/content/SyncManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-boolean v0, p0, Landroid/content/SyncManager;->mDataConnectionIsConnected:Z

    #@2
    return v0
.end method

.method static synthetic access$4000(Landroid/content/SyncManager;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncAlarmIntent:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Landroid/content/SyncManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-boolean p1, p0, Landroid/content/SyncManager;->mDataConnectionIsConnected:Z

    #@2
    return p1
.end method

.method static synthetic access$4100(Landroid/content/SyncManager;)Landroid/app/AlarmManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mAlarmService:Landroid/app/AlarmManager;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/content/SyncManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    invoke-direct {p0}, Landroid/content/SyncManager;->readDataConnectionState()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(Landroid/content/SyncManager;)Landroid/content/SyncQueue;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/content/SyncManager;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1}, Landroid/content/SyncManager;->onUserRemoved(I)V

    #@3
    return-void
.end method

.method static synthetic access$900(Landroid/content/SyncManager;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1}, Landroid/content/SyncManager;->onUserStarting(I)V

    #@3
    return-void
.end method

.method private clearBackoffSetting(Landroid/content/SyncOperation;)V
    .registers 10
    .parameter "op"

    #@0
    .prologue
    const-wide/16 v4, -0x1

    #@2
    .line 722
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@4
    iget-object v1, p1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@6
    iget v2, p1, Landroid/content/SyncOperation;->userId:I

    #@8
    iget-object v3, p1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@a
    move-wide v6, v4

    #@b
    invoke-virtual/range {v0 .. v7}, Landroid/content/SyncStorageEngine;->setBackoff(Landroid/accounts/Account;ILjava/lang/String;JJ)V

    #@e
    .line 724
    iget-object v6, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@10
    monitor-enter v6

    #@11
    .line 725
    :try_start_11
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@13
    iget-object v1, p1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@15
    iget v2, p1, Landroid/content/SyncOperation;->userId:I

    #@17
    iget-object v3, p1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@19
    const-wide/16 v4, 0x0

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/content/SyncQueue;->onBackoffChanged(Landroid/accounts/Account;ILjava/lang/String;J)V

    #@1e
    .line 726
    monitor-exit v6

    #@1f
    .line 727
    return-void

    #@20
    .line 726
    :catchall_20
    move-exception v0

    #@21
    monitor-exit v6
    :try_end_22
    .catchall {:try_start_11 .. :try_end_22} :catchall_20

    #@22
    throw v0
.end method

.method private containsAccountAndUser([Landroid/accounts/AccountAndUser;Landroid/accounts/Account;I)Z
    .registers 7
    .parameter "accounts"
    .parameter "account"
    .parameter "userId"

    #@0
    .prologue
    .line 235
    const/4 v0, 0x0

    #@1
    .line 236
    .local v0, found:Z
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    array-length v2, p1

    #@3
    if-ge v1, v2, :cond_16

    #@5
    .line 237
    aget-object v2, p1, v1

    #@7
    iget v2, v2, Landroid/accounts/AccountAndUser;->userId:I

    #@9
    if-ne v2, p3, :cond_17

    #@b
    aget-object v2, p1, v1

    #@d
    iget-object v2, v2, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@f
    invoke-virtual {v2, p2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_17

    #@15
    .line 239
    const/4 v0, 0x1

    #@16
    .line 243
    :cond_16
    return v0

    #@17
    .line 236
    :cond_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_2
.end method

.method private doDatabaseCleanup()V
    .registers 6

    #@0
    .prologue
    .line 269
    iget-object v3, p0, Landroid/content/SyncManager;->mUserManager:Landroid/os/UserManager;

    #@2
    const/4 v4, 0x1

    #@3
    invoke-virtual {v3, v4}, Landroid/os/UserManager;->getUsers(Z)Ljava/util/List;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_2d

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/content/pm/UserInfo;

    #@17
    .line 271
    .local v2, user:Landroid/content/pm/UserInfo;
    iget-boolean v3, v2, Landroid/content/pm/UserInfo;->partial:Z

    #@19
    if-nez v3, :cond_b

    #@1b
    .line 272
    invoke-static {}, Landroid/accounts/AccountManagerService;->getSingleton()Landroid/accounts/AccountManagerService;

    #@1e
    move-result-object v3

    #@1f
    iget v4, v2, Landroid/content/pm/UserInfo;->id:I

    #@21
    invoke-virtual {v3, v4}, Landroid/accounts/AccountManagerService;->getAccounts(I)[Landroid/accounts/Account;

    #@24
    move-result-object v0

    #@25
    .line 273
    .local v0, accountsForUser:[Landroid/accounts/Account;
    iget-object v3, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@27
    iget v4, v2, Landroid/content/pm/UserInfo;->id:I

    #@29
    invoke-virtual {v3, v0, v4}, Landroid/content/SyncStorageEngine;->doDatabaseCleanup([Landroid/accounts/Account;I)V

    #@2c
    goto :goto_b

    #@2d
    .line 275
    .end local v0           #accountsForUser:[Landroid/accounts/Account;
    .end local v2           #user:Landroid/content/pm/UserInfo;
    :cond_2d
    return-void
.end method

.method private dumpDayStatistic(Ljava/io/PrintWriter;Landroid/content/SyncStorageEngine$DayStats;)V
    .registers 7
    .parameter "pw"
    .parameter "ds"

    #@0
    .prologue
    .line 1262
    const-string v0, "Success ("

    #@2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    iget v0, p2, Landroid/content/SyncStorageEngine$DayStats;->successCount:I

    #@7
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@a
    .line 1263
    iget v0, p2, Landroid/content/SyncStorageEngine$DayStats;->successCount:I

    #@c
    if-lez v0, :cond_26

    #@e
    .line 1264
    const-string v0, " for "

    #@10
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13
    iget-wide v0, p2, Landroid/content/SyncStorageEngine$DayStats;->successTime:J

    #@15
    invoke-direct {p0, p1, v0, v1}, Landroid/content/SyncManager;->dumpTimeSec(Ljava/io/PrintWriter;J)V

    #@18
    .line 1265
    const-string v0, " avg="

    #@1a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d
    iget-wide v0, p2, Landroid/content/SyncStorageEngine$DayStats;->successTime:J

    #@1f
    iget v2, p2, Landroid/content/SyncStorageEngine$DayStats;->successCount:I

    #@21
    int-to-long v2, v2

    #@22
    div-long/2addr v0, v2

    #@23
    invoke-direct {p0, p1, v0, v1}, Landroid/content/SyncManager;->dumpTimeSec(Ljava/io/PrintWriter;J)V

    #@26
    .line 1267
    :cond_26
    const-string v0, ") Failure ("

    #@28
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b
    iget v0, p2, Landroid/content/SyncStorageEngine$DayStats;->failureCount:I

    #@2d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@30
    .line 1268
    iget v0, p2, Landroid/content/SyncStorageEngine$DayStats;->failureCount:I

    #@32
    if-lez v0, :cond_4c

    #@34
    .line 1269
    const-string v0, " for "

    #@36
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@39
    iget-wide v0, p2, Landroid/content/SyncStorageEngine$DayStats;->failureTime:J

    #@3b
    invoke-direct {p0, p1, v0, v1}, Landroid/content/SyncManager;->dumpTimeSec(Ljava/io/PrintWriter;J)V

    #@3e
    .line 1270
    const-string v0, " avg="

    #@40
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@43
    iget-wide v0, p2, Landroid/content/SyncStorageEngine$DayStats;->failureTime:J

    #@45
    iget v2, p2, Landroid/content/SyncStorageEngine$DayStats;->failureCount:I

    #@47
    int-to-long v2, v2

    #@48
    div-long/2addr v0, v2

    #@49
    invoke-direct {p0, p1, v0, v1}, Landroid/content/SyncManager;->dumpTimeSec(Ljava/io/PrintWriter;J)V

    #@4c
    .line 1272
    :cond_4c
    const-string v0, ")"

    #@4e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@51
    .line 1273
    return-void
.end method

.method private dumpDayStatistics(Ljava/io/PrintWriter;)V
    .registers 14
    .parameter "pw"

    #@0
    .prologue
    const/4 v11, 0x6

    #@1
    const/4 v8, 0x0

    #@2
    .line 1481
    iget-object v7, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@4
    invoke-virtual {v7}, Landroid/content/SyncStorageEngine;->getDayStatistics()[Landroid/content/SyncStorageEngine$DayStats;

    #@7
    move-result-object v3

    #@8
    .line 1482
    .local v3, dses:[Landroid/content/SyncStorageEngine$DayStats;
    if-eqz v3, :cond_99

    #@a
    aget-object v7, v3, v8

    #@c
    if-eqz v7, :cond_99

    #@e
    .line 1483
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@11
    .line 1484
    const-string v7, "Sync Statistics"

    #@13
    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@16
    .line 1485
    const-string v7, "  Today:  "

    #@18
    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b
    aget-object v7, v3, v8

    #@1d
    invoke-direct {p0, p1, v7}, Landroid/content/SyncManager;->dumpDayStatistic(Ljava/io/PrintWriter;Landroid/content/SyncStorageEngine$DayStats;)V

    #@20
    .line 1486
    aget-object v7, v3, v8

    #@22
    iget v5, v7, Landroid/content/SyncStorageEngine$DayStats;->day:I

    #@24
    .line 1491
    .local v5, today:I
    const/4 v4, 0x1

    #@25
    .local v4, i:I
    :goto_25
    if-gt v4, v11, :cond_2e

    #@27
    array-length v7, v3

    #@28
    if-ge v4, v7, :cond_2e

    #@2a
    .line 1492
    aget-object v2, v3, v4

    #@2c
    .line 1493
    .local v2, ds:Landroid/content/SyncStorageEngine$DayStats;
    if-nez v2, :cond_54

    #@2e
    .line 1502
    .end local v2           #ds:Landroid/content/SyncStorageEngine$DayStats;
    :cond_2e
    move v6, v5

    #@2f
    .line 1503
    .local v6, weekDay:I
    :cond_2f
    :goto_2f
    array-length v7, v3

    #@30
    if-ge v4, v7, :cond_99

    #@32
    .line 1504
    const/4 v0, 0x0

    #@33
    .line 1505
    .local v0, aggr:Landroid/content/SyncStorageEngine$DayStats;
    add-int/lit8 v6, v6, -0x7

    #@35
    .line 1506
    :goto_35
    array-length v7, v3

    #@36
    if-ge v4, v7, :cond_3d

    #@38
    .line 1507
    aget-object v2, v3, v4

    #@3a
    .line 1508
    .restart local v2       #ds:Landroid/content/SyncStorageEngine$DayStats;
    if-nez v2, :cond_6d

    #@3c
    .line 1509
    array-length v4, v3

    #@3d
    .line 1524
    .end local v2           #ds:Landroid/content/SyncStorageEngine$DayStats;
    :cond_3d
    if-eqz v0, :cond_2f

    #@3f
    .line 1525
    const-string v7, "  Week-"

    #@41
    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@44
    sub-int v7, v5, v6

    #@46
    div-int/lit8 v7, v7, 0x7

    #@48
    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(I)V

    #@4b
    const-string v7, ": "

    #@4d
    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@50
    .line 1526
    invoke-direct {p0, p1, v0}, Landroid/content/SyncManager;->dumpDayStatistic(Ljava/io/PrintWriter;Landroid/content/SyncStorageEngine$DayStats;)V

    #@53
    goto :goto_2f

    #@54
    .line 1494
    .end local v0           #aggr:Landroid/content/SyncStorageEngine$DayStats;
    .end local v6           #weekDay:I
    .restart local v2       #ds:Landroid/content/SyncStorageEngine$DayStats;
    :cond_54
    iget v7, v2, Landroid/content/SyncStorageEngine$DayStats;->day:I

    #@56
    sub-int v1, v5, v7

    #@58
    .line 1495
    .local v1, delta:I
    if-gt v1, v11, :cond_2e

    #@5a
    .line 1497
    const-string v7, "  Day-"

    #@5c
    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5f
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(I)V

    #@62
    const-string v7, ":  "

    #@64
    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@67
    .line 1498
    invoke-direct {p0, p1, v2}, Landroid/content/SyncManager;->dumpDayStatistic(Ljava/io/PrintWriter;Landroid/content/SyncStorageEngine$DayStats;)V

    #@6a
    .line 1491
    add-int/lit8 v4, v4, 0x1

    #@6c
    goto :goto_25

    #@6d
    .line 1512
    .end local v1           #delta:I
    .restart local v0       #aggr:Landroid/content/SyncStorageEngine$DayStats;
    .restart local v6       #weekDay:I
    :cond_6d
    iget v7, v2, Landroid/content/SyncStorageEngine$DayStats;->day:I

    #@6f
    sub-int v1, v6, v7

    #@71
    .line 1513
    .restart local v1       #delta:I
    if-gt v1, v11, :cond_3d

    #@73
    .line 1514
    add-int/lit8 v4, v4, 0x1

    #@75
    .line 1516
    if-nez v0, :cond_7c

    #@77
    .line 1517
    new-instance v0, Landroid/content/SyncStorageEngine$DayStats;

    #@79
    .end local v0           #aggr:Landroid/content/SyncStorageEngine$DayStats;
    invoke-direct {v0, v6}, Landroid/content/SyncStorageEngine$DayStats;-><init>(I)V

    #@7c
    .line 1519
    .restart local v0       #aggr:Landroid/content/SyncStorageEngine$DayStats;
    :cond_7c
    iget v7, v0, Landroid/content/SyncStorageEngine$DayStats;->successCount:I

    #@7e
    iget v8, v2, Landroid/content/SyncStorageEngine$DayStats;->successCount:I

    #@80
    add-int/2addr v7, v8

    #@81
    iput v7, v0, Landroid/content/SyncStorageEngine$DayStats;->successCount:I

    #@83
    .line 1520
    iget-wide v7, v0, Landroid/content/SyncStorageEngine$DayStats;->successTime:J

    #@85
    iget-wide v9, v2, Landroid/content/SyncStorageEngine$DayStats;->successTime:J

    #@87
    add-long/2addr v7, v9

    #@88
    iput-wide v7, v0, Landroid/content/SyncStorageEngine$DayStats;->successTime:J

    #@8a
    .line 1521
    iget v7, v0, Landroid/content/SyncStorageEngine$DayStats;->failureCount:I

    #@8c
    iget v8, v2, Landroid/content/SyncStorageEngine$DayStats;->failureCount:I

    #@8e
    add-int/2addr v7, v8

    #@8f
    iput v7, v0, Landroid/content/SyncStorageEngine$DayStats;->failureCount:I

    #@91
    .line 1522
    iget-wide v7, v0, Landroid/content/SyncStorageEngine$DayStats;->failureTime:J

    #@93
    iget-wide v9, v2, Landroid/content/SyncStorageEngine$DayStats;->failureTime:J

    #@95
    add-long/2addr v7, v9

    #@96
    iput-wide v7, v0, Landroid/content/SyncStorageEngine$DayStats;->failureTime:J

    #@98
    goto :goto_35

    #@99
    .line 1530
    .end local v0           #aggr:Landroid/content/SyncStorageEngine$DayStats;
    .end local v1           #delta:I
    .end local v2           #ds:Landroid/content/SyncStorageEngine$DayStats;
    .end local v4           #i:I
    .end local v5           #today:I
    .end local v6           #weekDay:I
    :cond_99
    return-void
.end method

.method private dumpRecentHistory(Ljava/io/PrintWriter;)V
    .registers 57
    .parameter "pw"

    #@0
    .prologue
    .line 1281
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@4
    move-object/from16 v50, v0

    #@6
    invoke-virtual/range {v50 .. v50}, Landroid/content/SyncStorageEngine;->getSyncHistory()Ljava/util/ArrayList;

    #@9
    move-result-object v26

    #@a
    .line 1283
    .local v26, items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStorageEngine$SyncHistoryItem;>;"
    if-eqz v26, :cond_537

    #@c
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v50

    #@10
    if-lez v50, :cond_537

    #@12
    .line 1284
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@15
    move-result-object v10

    #@16
    .line 1285
    .local v10, authorityMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Landroid/content/SyncManager$AuthoritySyncStats;>;"
    const-wide/16 v46, 0x0

    #@18
    .line 1286
    .local v46, totalElapsedTime:J
    const-wide/16 v48, 0x0

    #@1a
    .line 1287
    .local v48, totalTimes:J
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    #@1d
    move-result v3

    #@1e
    .line 1289
    .local v3, N:I
    const/16 v32, 0x0

    #@20
    .line 1290
    .local v32, maxAuthority:I
    const/16 v31, 0x0

    #@22
    .line 1291
    .local v31, maxAccount:I
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@25
    move-result-object v23

    #@26
    .local v23, i$:Ljava/util/Iterator;
    :goto_26
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    #@29
    move-result v50

    #@2a
    if-eqz v50, :cond_fc

    #@2c
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f
    move-result-object v25

    #@30
    check-cast v25, Landroid/content/SyncStorageEngine$SyncHistoryItem;

    #@32
    .line 1292
    .local v25, item:Landroid/content/SyncStorageEngine$SyncHistoryItem;
    move-object/from16 v0, p0

    #@34
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@36
    move-object/from16 v50, v0

    #@38
    move-object/from16 v0, v25

    #@3a
    iget v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->authorityId:I

    #@3c
    move/from16 v51, v0

    #@3e
    invoke-virtual/range {v50 .. v51}, Landroid/content/SyncStorageEngine;->getAuthority(I)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@41
    move-result-object v8

    #@42
    .line 1296
    .local v8, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v8, :cond_f7

    #@44
    .line 1297
    iget-object v11, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@46
    .line 1298
    .local v11, authorityName:Ljava/lang/String;
    new-instance v50, Ljava/lang/StringBuilder;

    #@48
    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    iget-object v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@4d
    move-object/from16 v51, v0

    #@4f
    move-object/from16 v0, v51

    #@51
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@53
    move-object/from16 v51, v0

    #@55
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v50

    #@59
    const-string v51, "/"

    #@5b
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v50

    #@5f
    iget-object v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@61
    move-object/from16 v51, v0

    #@63
    move-object/from16 v0, v51

    #@65
    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@67
    move-object/from16 v51, v0

    #@69
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v50

    #@6d
    const-string v51, " u"

    #@6f
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v50

    #@73
    iget v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@75
    move/from16 v51, v0

    #@77
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v50

    #@7b
    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    .line 1305
    .local v5, accountKey:Ljava/lang/String;
    :goto_7f
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@82
    move-result v30

    #@83
    .line 1306
    .local v30, length:I
    move/from16 v0, v30

    #@85
    move/from16 v1, v32

    #@87
    if-le v0, v1, :cond_8b

    #@89
    .line 1307
    move/from16 v32, v30

    #@8b
    .line 1309
    :cond_8b
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@8e
    move-result v30

    #@8f
    .line 1310
    move/from16 v0, v30

    #@91
    move/from16 v1, v31

    #@93
    if-le v0, v1, :cond_97

    #@95
    .line 1311
    move/from16 v31, v30

    #@97
    .line 1314
    :cond_97
    move-object/from16 v0, v25

    #@99
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->elapsedTime:J

    #@9b
    move-wide/from16 v17, v0

    #@9d
    .line 1315
    .local v17, elapsedTime:J
    add-long v46, v46, v17

    #@9f
    .line 1316
    const-wide/16 v50, 0x1

    #@a1
    add-long v48, v48, v50

    #@a3
    .line 1317
    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a6
    move-result-object v12

    #@a7
    check-cast v12, Landroid/content/SyncManager$AuthoritySyncStats;

    #@a9
    .line 1318
    .local v12, authoritySyncStats:Landroid/content/SyncManager$AuthoritySyncStats;
    if-nez v12, :cond_b7

    #@ab
    .line 1319
    new-instance v12, Landroid/content/SyncManager$AuthoritySyncStats;

    #@ad
    .end local v12           #authoritySyncStats:Landroid/content/SyncManager$AuthoritySyncStats;
    const/16 v50, 0x0

    #@af
    move-object/from16 v0, v50

    #@b1
    invoke-direct {v12, v11, v0}, Landroid/content/SyncManager$AuthoritySyncStats;-><init>(Ljava/lang/String;Landroid/content/SyncManager$1;)V

    #@b4
    .line 1320
    .restart local v12       #authoritySyncStats:Landroid/content/SyncManager$AuthoritySyncStats;
    invoke-interface {v10, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b7
    .line 1322
    :cond_b7
    iget-wide v0, v12, Landroid/content/SyncManager$AuthoritySyncStats;->elapsedTime:J

    #@b9
    move-wide/from16 v50, v0

    #@bb
    add-long v50, v50, v17

    #@bd
    move-wide/from16 v0, v50

    #@bf
    iput-wide v0, v12, Landroid/content/SyncManager$AuthoritySyncStats;->elapsedTime:J

    #@c1
    .line 1323
    iget v0, v12, Landroid/content/SyncManager$AuthoritySyncStats;->times:I

    #@c3
    move/from16 v50, v0

    #@c5
    add-int/lit8 v50, v50, 0x1

    #@c7
    move/from16 v0, v50

    #@c9
    iput v0, v12, Landroid/content/SyncManager$AuthoritySyncStats;->times:I

    #@cb
    .line 1324
    iget-object v6, v12, Landroid/content/SyncManager$AuthoritySyncStats;->accountMap:Ljava/util/Map;

    #@cd
    .line 1325
    .local v6, accountMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Landroid/content/SyncManager$AccountSyncStats;>;"
    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d0
    move-result-object v7

    #@d1
    check-cast v7, Landroid/content/SyncManager$AccountSyncStats;

    #@d3
    .line 1326
    .local v7, accountSyncStats:Landroid/content/SyncManager$AccountSyncStats;
    if-nez v7, :cond_e1

    #@d5
    .line 1327
    new-instance v7, Landroid/content/SyncManager$AccountSyncStats;

    #@d7
    .end local v7           #accountSyncStats:Landroid/content/SyncManager$AccountSyncStats;
    const/16 v50, 0x0

    #@d9
    move-object/from16 v0, v50

    #@db
    invoke-direct {v7, v5, v0}, Landroid/content/SyncManager$AccountSyncStats;-><init>(Ljava/lang/String;Landroid/content/SyncManager$1;)V

    #@de
    .line 1328
    .restart local v7       #accountSyncStats:Landroid/content/SyncManager$AccountSyncStats;
    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e1
    .line 1330
    :cond_e1
    iget-wide v0, v7, Landroid/content/SyncManager$AccountSyncStats;->elapsedTime:J

    #@e3
    move-wide/from16 v50, v0

    #@e5
    add-long v50, v50, v17

    #@e7
    move-wide/from16 v0, v50

    #@e9
    iput-wide v0, v7, Landroid/content/SyncManager$AccountSyncStats;->elapsedTime:J

    #@eb
    .line 1331
    iget v0, v7, Landroid/content/SyncManager$AccountSyncStats;->times:I

    #@ed
    move/from16 v50, v0

    #@ef
    add-int/lit8 v50, v50, 0x1

    #@f1
    move/from16 v0, v50

    #@f3
    iput v0, v7, Landroid/content/SyncManager$AccountSyncStats;->times:I

    #@f5
    goto/16 :goto_26

    #@f7
    .line 1301
    .end local v5           #accountKey:Ljava/lang/String;
    .end local v6           #accountMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Landroid/content/SyncManager$AccountSyncStats;>;"
    .end local v7           #accountSyncStats:Landroid/content/SyncManager$AccountSyncStats;
    .end local v11           #authorityName:Ljava/lang/String;
    .end local v12           #authoritySyncStats:Landroid/content/SyncManager$AuthoritySyncStats;
    .end local v17           #elapsedTime:J
    .end local v30           #length:I
    :cond_f7
    const-string v11, "Unknown"

    #@f9
    .line 1302
    .restart local v11       #authorityName:Ljava/lang/String;
    const-string v5, "Unknown"

    #@fb
    .restart local v5       #accountKey:Ljava/lang/String;
    goto :goto_7f

    #@fc
    .line 1335
    .end local v5           #accountKey:Ljava/lang/String;
    .end local v8           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v11           #authorityName:Ljava/lang/String;
    .end local v25           #item:Landroid/content/SyncStorageEngine$SyncHistoryItem;
    :cond_fc
    const-wide/16 v50, 0x0

    #@fe
    cmp-long v50, v46, v50

    #@100
    if-lez v50, :cond_2e0

    #@102
    .line 1336
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@105
    .line 1337
    const-string v50, "Detailed Statistics (Recent history):  %d (# of times) %ds (sync time)\n"

    #@107
    const/16 v51, 0x2

    #@109
    move/from16 v0, v51

    #@10b
    new-array v0, v0, [Ljava/lang/Object;

    #@10d
    move-object/from16 v51, v0

    #@10f
    const/16 v52, 0x0

    #@111
    invoke-static/range {v48 .. v49}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@114
    move-result-object v53

    #@115
    aput-object v53, v51, v52

    #@117
    const/16 v52, 0x1

    #@119
    const-wide/16 v53, 0x3e8

    #@11b
    div-long v53, v46, v53

    #@11d
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@120
    move-result-object v53

    #@121
    aput-object v53, v51, v52

    #@123
    move-object/from16 v0, p1

    #@125
    move-object/from16 v1, v50

    #@127
    move-object/from16 v2, v51

    #@129
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@12c
    .line 1341
    new-instance v40, Ljava/util/ArrayList;

    #@12e
    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@131
    move-result-object v50

    #@132
    move-object/from16 v0, v40

    #@134
    move-object/from16 v1, v50

    #@136
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@139
    .line 1343
    .local v40, sortedAuthorities:Ljava/util/List;,"Ljava/util/List<Landroid/content/SyncManager$AuthoritySyncStats;>;"
    new-instance v50, Landroid/content/SyncManager$11;

    #@13b
    move-object/from16 v0, v50

    #@13d
    move-object/from16 v1, p0

    #@13f
    invoke-direct {v0, v1}, Landroid/content/SyncManager$11;-><init>(Landroid/content/SyncManager;)V

    #@142
    move-object/from16 v0, v40

    #@144
    move-object/from16 v1, v50

    #@146
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@149
    .line 1355
    add-int/lit8 v50, v31, 0x3

    #@14b
    move/from16 v0, v32

    #@14d
    move/from16 v1, v50

    #@14f
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@152
    move-result v33

    #@153
    .line 1356
    .local v33, maxLength:I
    add-int/lit8 v50, v33, 0x4

    #@155
    add-int/lit8 v50, v50, 0x2

    #@157
    add-int/lit8 v50, v50, 0xa

    #@159
    add-int/lit8 v35, v50, 0xb

    #@15b
    .line 1357
    .local v35, padLength:I
    move/from16 v0, v35

    #@15d
    new-array v13, v0, [C

    #@15f
    .line 1358
    .local v13, chars:[C
    const/16 v50, 0x2d

    #@161
    move/from16 v0, v50

    #@163
    invoke-static {v13, v0}, Ljava/util/Arrays;->fill([CC)V

    #@166
    .line 1359
    new-instance v38, Ljava/lang/String;

    #@168
    move-object/from16 v0, v38

    #@16a
    invoke-direct {v0, v13}, Ljava/lang/String;-><init>([C)V

    #@16d
    .line 1361
    .local v38, separator:Ljava/lang/String;
    const-string v50, "  %%-%ds: %%-9s  %%-11s\n"

    #@16f
    const/16 v51, 0x1

    #@171
    move/from16 v0, v51

    #@173
    new-array v0, v0, [Ljava/lang/Object;

    #@175
    move-object/from16 v51, v0

    #@177
    const/16 v52, 0x0

    #@179
    add-int/lit8 v53, v33, 0x2

    #@17b
    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17e
    move-result-object v53

    #@17f
    aput-object v53, v51, v52

    #@181
    invoke-static/range {v50 .. v51}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@184
    move-result-object v9

    #@185
    .line 1363
    .local v9, authorityFormat:Ljava/lang/String;
    const-string v50, "    %%-%ds:   %%-9s  %%-11s\n"

    #@187
    const/16 v51, 0x1

    #@189
    move/from16 v0, v51

    #@18b
    new-array v0, v0, [Ljava/lang/Object;

    #@18d
    move-object/from16 v51, v0

    #@18f
    const/16 v52, 0x0

    #@191
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@194
    move-result-object v53

    #@195
    aput-object v53, v51, v52

    #@197
    invoke-static/range {v50 .. v51}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@19a
    move-result-object v4

    #@19b
    .line 1366
    .local v4, accountFormat:Ljava/lang/String;
    move-object/from16 v0, p1

    #@19d
    move-object/from16 v1, v38

    #@19f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a2
    .line 1367
    invoke-interface/range {v40 .. v40}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1a5
    move-result-object v23

    #@1a6
    .end local v23           #i$:Ljava/util/Iterator;
    :goto_1a6
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    #@1a9
    move-result v50

    #@1aa
    if-eqz v50, :cond_2e0

    #@1ac
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1af
    move-result-object v12

    #@1b0
    check-cast v12, Landroid/content/SyncManager$AuthoritySyncStats;

    #@1b2
    .line 1368
    .restart local v12       #authoritySyncStats:Landroid/content/SyncManager$AuthoritySyncStats;
    iget-object v0, v12, Landroid/content/SyncManager$AuthoritySyncStats;->name:Ljava/lang/String;

    #@1b4
    move-object/from16 v34, v0

    #@1b6
    .line 1374
    .local v34, name:Ljava/lang/String;
    iget-wide v0, v12, Landroid/content/SyncManager$AuthoritySyncStats;->elapsedTime:J

    #@1b8
    move-wide/from16 v17, v0

    #@1ba
    .line 1375
    .restart local v17       #elapsedTime:J
    iget v0, v12, Landroid/content/SyncManager$AuthoritySyncStats;->times:I

    #@1bc
    move/from16 v44, v0

    #@1be
    .line 1376
    .local v44, times:I
    const-string v50, "%ds/%d%%"

    #@1c0
    const/16 v51, 0x2

    #@1c2
    move/from16 v0, v51

    #@1c4
    new-array v0, v0, [Ljava/lang/Object;

    #@1c6
    move-object/from16 v51, v0

    #@1c8
    const/16 v52, 0x0

    #@1ca
    const-wide/16 v53, 0x3e8

    #@1cc
    div-long v53, v17, v53

    #@1ce
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1d1
    move-result-object v53

    #@1d2
    aput-object v53, v51, v52

    #@1d4
    const/16 v52, 0x1

    #@1d6
    const-wide/16 v53, 0x64

    #@1d8
    mul-long v53, v53, v17

    #@1da
    div-long v53, v53, v46

    #@1dc
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1df
    move-result-object v53

    #@1e0
    aput-object v53, v51, v52

    #@1e2
    invoke-static/range {v50 .. v51}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1e5
    move-result-object v43

    #@1e6
    .line 1379
    .local v43, timeStr:Ljava/lang/String;
    const-string v50, "%d/%d%%"

    #@1e8
    const/16 v51, 0x2

    #@1ea
    move/from16 v0, v51

    #@1ec
    new-array v0, v0, [Ljava/lang/Object;

    #@1ee
    move-object/from16 v51, v0

    #@1f0
    const/16 v52, 0x0

    #@1f2
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f5
    move-result-object v53

    #@1f6
    aput-object v53, v51, v52

    #@1f8
    const/16 v52, 0x1

    #@1fa
    mul-int/lit8 v53, v44, 0x64

    #@1fc
    move/from16 v0, v53

    #@1fe
    int-to-long v0, v0

    #@1ff
    move-wide/from16 v53, v0

    #@201
    div-long v53, v53, v48

    #@203
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@206
    move-result-object v53

    #@207
    aput-object v53, v51, v52

    #@209
    invoke-static/range {v50 .. v51}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@20c
    move-result-object v45

    #@20d
    .line 1382
    .local v45, timesStr:Ljava/lang/String;
    const/16 v50, 0x3

    #@20f
    move/from16 v0, v50

    #@211
    new-array v0, v0, [Ljava/lang/Object;

    #@213
    move-object/from16 v50, v0

    #@215
    const/16 v51, 0x0

    #@217
    aput-object v34, v50, v51

    #@219
    const/16 v51, 0x1

    #@21b
    aput-object v45, v50, v51

    #@21d
    const/16 v51, 0x2

    #@21f
    aput-object v43, v50, v51

    #@221
    move-object/from16 v0, p1

    #@223
    move-object/from16 v1, v50

    #@225
    invoke-virtual {v0, v9, v1}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@228
    .line 1384
    new-instance v39, Ljava/util/ArrayList;

    #@22a
    iget-object v0, v12, Landroid/content/SyncManager$AuthoritySyncStats;->accountMap:Ljava/util/Map;

    #@22c
    move-object/from16 v50, v0

    #@22e
    invoke-interface/range {v50 .. v50}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@231
    move-result-object v50

    #@232
    move-object/from16 v0, v39

    #@234
    move-object/from16 v1, v50

    #@236
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@239
    .line 1387
    .local v39, sortedAccounts:Ljava/util/List;,"Ljava/util/List<Landroid/content/SyncManager$AccountSyncStats;>;"
    new-instance v50, Landroid/content/SyncManager$12;

    #@23b
    move-object/from16 v0, v50

    #@23d
    move-object/from16 v1, p0

    #@23f
    invoke-direct {v0, v1}, Landroid/content/SyncManager$12;-><init>(Landroid/content/SyncManager;)V

    #@242
    move-object/from16 v0, v39

    #@244
    move-object/from16 v1, v50

    #@246
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@249
    .line 1398
    invoke-interface/range {v39 .. v39}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@24c
    move-result-object v24

    #@24d
    .local v24, i$:Ljava/util/Iterator;
    :goto_24d
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    #@250
    move-result v50

    #@251
    if-eqz v50, :cond_2d7

    #@253
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@256
    move-result-object v41

    #@257
    check-cast v41, Landroid/content/SyncManager$AccountSyncStats;

    #@259
    .line 1399
    .local v41, stats:Landroid/content/SyncManager$AccountSyncStats;
    move-object/from16 v0, v41

    #@25b
    iget-wide v0, v0, Landroid/content/SyncManager$AccountSyncStats;->elapsedTime:J

    #@25d
    move-wide/from16 v17, v0

    #@25f
    .line 1400
    move-object/from16 v0, v41

    #@261
    iget v0, v0, Landroid/content/SyncManager$AccountSyncStats;->times:I

    #@263
    move/from16 v44, v0

    #@265
    .line 1401
    const-string v50, "%ds/%d%%"

    #@267
    const/16 v51, 0x2

    #@269
    move/from16 v0, v51

    #@26b
    new-array v0, v0, [Ljava/lang/Object;

    #@26d
    move-object/from16 v51, v0

    #@26f
    const/16 v52, 0x0

    #@271
    const-wide/16 v53, 0x3e8

    #@273
    div-long v53, v17, v53

    #@275
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@278
    move-result-object v53

    #@279
    aput-object v53, v51, v52

    #@27b
    const/16 v52, 0x1

    #@27d
    const-wide/16 v53, 0x64

    #@27f
    mul-long v53, v53, v17

    #@281
    div-long v53, v53, v46

    #@283
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@286
    move-result-object v53

    #@287
    aput-object v53, v51, v52

    #@289
    invoke-static/range {v50 .. v51}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@28c
    move-result-object v43

    #@28d
    .line 1404
    const-string v50, "%d/%d%%"

    #@28f
    const/16 v51, 0x2

    #@291
    move/from16 v0, v51

    #@293
    new-array v0, v0, [Ljava/lang/Object;

    #@295
    move-object/from16 v51, v0

    #@297
    const/16 v52, 0x0

    #@299
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29c
    move-result-object v53

    #@29d
    aput-object v53, v51, v52

    #@29f
    const/16 v52, 0x1

    #@2a1
    mul-int/lit8 v53, v44, 0x64

    #@2a3
    move/from16 v0, v53

    #@2a5
    int-to-long v0, v0

    #@2a6
    move-wide/from16 v53, v0

    #@2a8
    div-long v53, v53, v48

    #@2aa
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2ad
    move-result-object v53

    #@2ae
    aput-object v53, v51, v52

    #@2b0
    invoke-static/range {v50 .. v51}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2b3
    move-result-object v45

    #@2b4
    .line 1407
    const/16 v50, 0x3

    #@2b6
    move/from16 v0, v50

    #@2b8
    new-array v0, v0, [Ljava/lang/Object;

    #@2ba
    move-object/from16 v50, v0

    #@2bc
    const/16 v51, 0x0

    #@2be
    move-object/from16 v0, v41

    #@2c0
    iget-object v0, v0, Landroid/content/SyncManager$AccountSyncStats;->name:Ljava/lang/String;

    #@2c2
    move-object/from16 v52, v0

    #@2c4
    aput-object v52, v50, v51

    #@2c6
    const/16 v51, 0x1

    #@2c8
    aput-object v45, v50, v51

    #@2ca
    const/16 v51, 0x2

    #@2cc
    aput-object v43, v50, v51

    #@2ce
    move-object/from16 v0, p1

    #@2d0
    move-object/from16 v1, v50

    #@2d2
    invoke-virtual {v0, v4, v1}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@2d5
    goto/16 :goto_24d

    #@2d7
    .line 1409
    .end local v41           #stats:Landroid/content/SyncManager$AccountSyncStats;
    :cond_2d7
    move-object/from16 v0, p1

    #@2d9
    move-object/from16 v1, v38

    #@2db
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2de
    goto/16 :goto_1a6

    #@2e0
    .line 1413
    .end local v4           #accountFormat:Ljava/lang/String;
    .end local v9           #authorityFormat:Ljava/lang/String;
    .end local v12           #authoritySyncStats:Landroid/content/SyncManager$AuthoritySyncStats;
    .end local v13           #chars:[C
    .end local v17           #elapsedTime:J
    .end local v24           #i$:Ljava/util/Iterator;
    .end local v33           #maxLength:I
    .end local v34           #name:Ljava/lang/String;
    .end local v35           #padLength:I
    .end local v38           #separator:Ljava/lang/String;
    .end local v39           #sortedAccounts:Ljava/util/List;,"Ljava/util/List<Landroid/content/SyncManager$AccountSyncStats;>;"
    .end local v40           #sortedAuthorities:Ljava/util/List;,"Ljava/util/List<Landroid/content/SyncManager$AuthoritySyncStats;>;"
    .end local v43           #timeStr:Ljava/lang/String;
    .end local v44           #times:I
    .end local v45           #timesStr:Ljava/lang/String;
    :cond_2e0
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@2e3
    .line 1414
    const-string v50, "Recent Sync History"

    #@2e5
    move-object/from16 v0, p1

    #@2e7
    move-object/from16 v1, v50

    #@2e9
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2ec
    .line 1415
    new-instance v50, Ljava/lang/StringBuilder;

    #@2ee
    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    #@2f1
    const-string v51, "  %-"

    #@2f3
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f6
    move-result-object v50

    #@2f7
    move-object/from16 v0, v50

    #@2f9
    move/from16 v1, v31

    #@2fb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2fe
    move-result-object v50

    #@2ff
    const-string/jumbo v51, "s  %s\n"

    #@302
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@305
    move-result-object v50

    #@306
    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@309
    move-result-object v21

    #@30a
    .line 1416
    .local v21, format:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@30d
    move-result-object v29

    #@30e
    .line 1418
    .local v29, lastTimeMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const/16 v22, 0x0

    #@310
    .local v22, i:I
    :goto_310
    move/from16 v0, v22

    #@312
    if-ge v0, v3, :cond_537

    #@314
    .line 1419
    move-object/from16 v0, v26

    #@316
    move/from16 v1, v22

    #@318
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@31b
    move-result-object v25

    #@31c
    check-cast v25, Landroid/content/SyncStorageEngine$SyncHistoryItem;

    #@31e
    .line 1420
    .restart local v25       #item:Landroid/content/SyncStorageEngine$SyncHistoryItem;
    move-object/from16 v0, p0

    #@320
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@322
    move-object/from16 v50, v0

    #@324
    move-object/from16 v0, v25

    #@326
    iget v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->authorityId:I

    #@328
    move/from16 v51, v0

    #@32a
    invoke-virtual/range {v50 .. v51}, Landroid/content/SyncStorageEngine;->getAuthority(I)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@32d
    move-result-object v8

    #@32e
    .line 1424
    .restart local v8       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v8, :cond_4b5

    #@330
    .line 1425
    iget-object v11, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@332
    .line 1426
    .restart local v11       #authorityName:Ljava/lang/String;
    new-instance v50, Ljava/lang/StringBuilder;

    #@334
    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    #@337
    iget-object v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@339
    move-object/from16 v51, v0

    #@33b
    move-object/from16 v0, v51

    #@33d
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@33f
    move-object/from16 v51, v0

    #@341
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@344
    move-result-object v50

    #@345
    const-string v51, "/"

    #@347
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34a
    move-result-object v50

    #@34b
    iget-object v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@34d
    move-object/from16 v51, v0

    #@34f
    move-object/from16 v0, v51

    #@351
    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@353
    move-object/from16 v51, v0

    #@355
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@358
    move-result-object v50

    #@359
    const-string v51, " u"

    #@35b
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35e
    move-result-object v50

    #@35f
    iget v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@361
    move/from16 v51, v0

    #@363
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@366
    move-result-object v50

    #@367
    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36a
    move-result-object v5

    #@36b
    .line 1432
    .restart local v5       #accountKey:Ljava/lang/String;
    :goto_36b
    move-object/from16 v0, v25

    #@36d
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->elapsedTime:J

    #@36f
    move-wide/from16 v17, v0

    #@371
    .line 1433
    .restart local v17       #elapsedTime:J
    new-instance v42, Landroid/text/format/Time;

    #@373
    invoke-direct/range {v42 .. v42}, Landroid/text/format/Time;-><init>()V

    #@376
    .line 1434
    .local v42, time:Landroid/text/format/Time;
    move-object/from16 v0, v25

    #@378
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->eventTime:J

    #@37a
    move-wide/from16 v19, v0

    #@37c
    .line 1435
    .local v19, eventTime:J
    move-object/from16 v0, v42

    #@37e
    move-wide/from16 v1, v19

    #@380
    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    #@383
    .line 1437
    new-instance v50, Ljava/lang/StringBuilder;

    #@385
    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    #@388
    move-object/from16 v0, v50

    #@38a
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38d
    move-result-object v50

    #@38e
    const-string v51, "/"

    #@390
    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@393
    move-result-object v50

    #@394
    move-object/from16 v0, v50

    #@396
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@399
    move-result-object v50

    #@39a
    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39d
    move-result-object v27

    #@39e
    .line 1438
    .local v27, key:Ljava/lang/String;
    move-object/from16 v0, v29

    #@3a0
    move-object/from16 v1, v27

    #@3a2
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3a5
    move-result-object v28

    #@3a6
    check-cast v28, Ljava/lang/Long;

    #@3a8
    .line 1440
    .local v28, lastEventTime:Ljava/lang/Long;
    if-nez v28, :cond_4bb

    #@3aa
    .line 1441
    const-string v16, ""

    #@3ac
    .line 1454
    .local v16, diffString:Ljava/lang/String;
    :goto_3ac
    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3af
    move-result-object v50

    #@3b0
    move-object/from16 v0, v29

    #@3b2
    move-object/from16 v1, v27

    #@3b4
    move-object/from16 v2, v50

    #@3b6
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3b9
    .line 1456
    const-string v50, "  #%-3d: %s %8s  %5.1fs  %8s"

    #@3bb
    const/16 v51, 0x5

    #@3bd
    move/from16 v0, v51

    #@3bf
    new-array v0, v0, [Ljava/lang/Object;

    #@3c1
    move-object/from16 v51, v0

    #@3c3
    const/16 v52, 0x0

    #@3c5
    add-int/lit8 v53, v22, 0x1

    #@3c7
    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3ca
    move-result-object v53

    #@3cb
    aput-object v53, v51, v52

    #@3cd
    const/16 v52, 0x1

    #@3cf
    invoke-static/range {v19 .. v20}, Landroid/content/SyncManager;->formatTime(J)Ljava/lang/String;

    #@3d2
    move-result-object v53

    #@3d3
    aput-object v53, v51, v52

    #@3d5
    const/16 v52, 0x2

    #@3d7
    sget-object v53, Landroid/content/SyncStorageEngine;->SOURCES:[Ljava/lang/String;

    #@3d9
    move-object/from16 v0, v25

    #@3db
    iget v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->source:I

    #@3dd
    move/from16 v54, v0

    #@3df
    aget-object v53, v53, v54

    #@3e1
    aput-object v53, v51, v52

    #@3e3
    const/16 v52, 0x3

    #@3e5
    move-wide/from16 v0, v17

    #@3e7
    long-to-float v0, v0

    #@3e8
    move/from16 v53, v0

    #@3ea
    const/high16 v54, 0x447a

    #@3ec
    div-float v53, v53, v54

    #@3ee
    invoke-static/range {v53 .. v53}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@3f1
    move-result-object v53

    #@3f2
    aput-object v53, v51, v52

    #@3f4
    const/16 v52, 0x4

    #@3f6
    aput-object v16, v51, v52

    #@3f8
    move-object/from16 v0, p1

    #@3fa
    move-object/from16 v1, v50

    #@3fc
    move-object/from16 v2, v51

    #@3fe
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@401
    .line 1462
    const/16 v50, 0x2

    #@403
    move/from16 v0, v50

    #@405
    new-array v0, v0, [Ljava/lang/Object;

    #@407
    move-object/from16 v50, v0

    #@409
    const/16 v51, 0x0

    #@40b
    aput-object v5, v50, v51

    #@40d
    const/16 v51, 0x1

    #@40f
    aput-object v11, v50, v51

    #@411
    move-object/from16 v0, p1

    #@413
    move-object/from16 v1, v21

    #@415
    move-object/from16 v2, v50

    #@417
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@41a
    .line 1464
    move-object/from16 v0, v25

    #@41c
    iget v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->event:I

    #@41e
    move/from16 v50, v0

    #@420
    const/16 v51, 0x1

    #@422
    move/from16 v0, v50

    #@424
    move/from16 v1, v51

    #@426
    if-ne v0, v1, :cond_440

    #@428
    move-object/from16 v0, v25

    #@42a
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->upstreamActivity:J

    #@42c
    move-wide/from16 v50, v0

    #@42e
    const-wide/16 v52, 0x0

    #@430
    cmp-long v50, v50, v52

    #@432
    if-nez v50, :cond_440

    #@434
    move-object/from16 v0, v25

    #@436
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->downstreamActivity:J

    #@438
    move-wide/from16 v50, v0

    #@43a
    const-wide/16 v52, 0x0

    #@43c
    cmp-long v50, v50, v52

    #@43e
    if-eqz v50, :cond_47d

    #@440
    .line 1467
    :cond_440
    const-string v50, "    event=%d upstreamActivity=%d downstreamActivity=%d\n"

    #@442
    const/16 v51, 0x3

    #@444
    move/from16 v0, v51

    #@446
    new-array v0, v0, [Ljava/lang/Object;

    #@448
    move-object/from16 v51, v0

    #@44a
    const/16 v52, 0x0

    #@44c
    move-object/from16 v0, v25

    #@44e
    iget v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->event:I

    #@450
    move/from16 v53, v0

    #@452
    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@455
    move-result-object v53

    #@456
    aput-object v53, v51, v52

    #@458
    const/16 v52, 0x1

    #@45a
    move-object/from16 v0, v25

    #@45c
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->upstreamActivity:J

    #@45e
    move-wide/from16 v53, v0

    #@460
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@463
    move-result-object v53

    #@464
    aput-object v53, v51, v52

    #@466
    const/16 v52, 0x2

    #@468
    move-object/from16 v0, v25

    #@46a
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->downstreamActivity:J

    #@46c
    move-wide/from16 v53, v0

    #@46e
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@471
    move-result-object v53

    #@472
    aput-object v53, v51, v52

    #@474
    move-object/from16 v0, p1

    #@476
    move-object/from16 v1, v50

    #@478
    move-object/from16 v2, v51

    #@47a
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@47d
    .line 1472
    :cond_47d
    move-object/from16 v0, v25

    #@47f
    iget-object v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->mesg:Ljava/lang/String;

    #@481
    move-object/from16 v50, v0

    #@483
    if-eqz v50, :cond_4b1

    #@485
    const-string/jumbo v50, "success"

    #@488
    move-object/from16 v0, v25

    #@48a
    iget-object v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->mesg:Ljava/lang/String;

    #@48c
    move-object/from16 v51, v0

    #@48e
    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@491
    move-result v50

    #@492
    if-nez v50, :cond_4b1

    #@494
    .line 1474
    const-string v50, "    mesg=%s\n"

    #@496
    const/16 v51, 0x1

    #@498
    move/from16 v0, v51

    #@49a
    new-array v0, v0, [Ljava/lang/Object;

    #@49c
    move-object/from16 v51, v0

    #@49e
    const/16 v52, 0x0

    #@4a0
    move-object/from16 v0, v25

    #@4a2
    iget-object v0, v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;->mesg:Ljava/lang/String;

    #@4a4
    move-object/from16 v53, v0

    #@4a6
    aput-object v53, v51, v52

    #@4a8
    move-object/from16 v0, p1

    #@4aa
    move-object/from16 v1, v50

    #@4ac
    move-object/from16 v2, v51

    #@4ae
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@4b1
    .line 1418
    :cond_4b1
    add-int/lit8 v22, v22, 0x1

    #@4b3
    goto/16 :goto_310

    #@4b5
    .line 1429
    .end local v5           #accountKey:Ljava/lang/String;
    .end local v11           #authorityName:Ljava/lang/String;
    .end local v16           #diffString:Ljava/lang/String;
    .end local v17           #elapsedTime:J
    .end local v19           #eventTime:J
    .end local v27           #key:Ljava/lang/String;
    .end local v28           #lastEventTime:Ljava/lang/Long;
    .end local v42           #time:Landroid/text/format/Time;
    :cond_4b5
    const-string v11, "Unknown"

    #@4b7
    .line 1430
    .restart local v11       #authorityName:Ljava/lang/String;
    const-string v5, "Unknown"

    #@4b9
    .restart local v5       #accountKey:Ljava/lang/String;
    goto/16 :goto_36b

    #@4bb
    .line 1443
    .restart local v17       #elapsedTime:J
    .restart local v19       #eventTime:J
    .restart local v27       #key:Ljava/lang/String;
    .restart local v28       #lastEventTime:Ljava/lang/Long;
    .restart local v42       #time:Landroid/text/format/Time;
    :cond_4bb
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Long;->longValue()J

    #@4be
    move-result-wide v50

    #@4bf
    sub-long v50, v50, v19

    #@4c1
    const-wide/16 v52, 0x3e8

    #@4c3
    div-long v14, v50, v52

    #@4c5
    .line 1444
    .local v14, diff:J
    const-wide/16 v50, 0x3c

    #@4c7
    cmp-long v50, v14, v50

    #@4c9
    if-gez v50, :cond_4d1

    #@4cb
    .line 1445
    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@4ce
    move-result-object v16

    #@4cf
    .restart local v16       #diffString:Ljava/lang/String;
    goto/16 :goto_3ac

    #@4d1
    .line 1446
    .end local v16           #diffString:Ljava/lang/String;
    :cond_4d1
    const-wide/16 v50, 0xe10

    #@4d3
    cmp-long v50, v14, v50

    #@4d5
    if-gez v50, :cond_4ff

    #@4d7
    .line 1447
    const-string v50, "%02d:%02d"

    #@4d9
    const/16 v51, 0x2

    #@4db
    move/from16 v0, v51

    #@4dd
    new-array v0, v0, [Ljava/lang/Object;

    #@4df
    move-object/from16 v51, v0

    #@4e1
    const/16 v52, 0x0

    #@4e3
    const-wide/16 v53, 0x3c

    #@4e5
    div-long v53, v14, v53

    #@4e7
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4ea
    move-result-object v53

    #@4eb
    aput-object v53, v51, v52

    #@4ed
    const/16 v52, 0x1

    #@4ef
    const-wide/16 v53, 0x3c

    #@4f1
    rem-long v53, v14, v53

    #@4f3
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4f6
    move-result-object v53

    #@4f7
    aput-object v53, v51, v52

    #@4f9
    invoke-static/range {v50 .. v51}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@4fc
    move-result-object v16

    #@4fd
    .restart local v16       #diffString:Ljava/lang/String;
    goto/16 :goto_3ac

    #@4ff
    .line 1449
    .end local v16           #diffString:Ljava/lang/String;
    :cond_4ff
    const-wide/16 v50, 0xe10

    #@501
    rem-long v36, v14, v50

    #@503
    .line 1450
    .local v36, sec:J
    const-string v50, "%02d:%02d:%02d"

    #@505
    const/16 v51, 0x3

    #@507
    move/from16 v0, v51

    #@509
    new-array v0, v0, [Ljava/lang/Object;

    #@50b
    move-object/from16 v51, v0

    #@50d
    const/16 v52, 0x0

    #@50f
    const-wide/16 v53, 0xe10

    #@511
    div-long v53, v14, v53

    #@513
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@516
    move-result-object v53

    #@517
    aput-object v53, v51, v52

    #@519
    const/16 v52, 0x1

    #@51b
    const-wide/16 v53, 0x3c

    #@51d
    div-long v53, v36, v53

    #@51f
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@522
    move-result-object v53

    #@523
    aput-object v53, v51, v52

    #@525
    const/16 v52, 0x2

    #@527
    const-wide/16 v53, 0x3c

    #@529
    rem-long v53, v36, v53

    #@52b
    invoke-static/range {v53 .. v54}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@52e
    move-result-object v53

    #@52f
    aput-object v53, v51, v52

    #@531
    invoke-static/range {v50 .. v51}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@534
    move-result-object v16

    #@535
    .restart local v16       #diffString:Ljava/lang/String;
    goto/16 :goto_3ac

    #@537
    .line 1478
    .end local v3           #N:I
    .end local v5           #accountKey:Ljava/lang/String;
    .end local v8           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v10           #authorityMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Landroid/content/SyncManager$AuthoritySyncStats;>;"
    .end local v11           #authorityName:Ljava/lang/String;
    .end local v14           #diff:J
    .end local v16           #diffString:Ljava/lang/String;
    .end local v17           #elapsedTime:J
    .end local v19           #eventTime:J
    .end local v21           #format:Ljava/lang/String;
    .end local v22           #i:I
    .end local v25           #item:Landroid/content/SyncStorageEngine$SyncHistoryItem;
    .end local v27           #key:Ljava/lang/String;
    .end local v28           #lastEventTime:Ljava/lang/Long;
    .end local v29           #lastTimeMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v31           #maxAccount:I
    .end local v32           #maxAuthority:I
    .end local v36           #sec:J
    .end local v42           #time:Landroid/text/format/Time;
    .end local v46           #totalElapsedTime:J
    .end local v48           #totalTimes:J
    :cond_537
    return-void
.end method

.method private dumpSyncAdapters(Lcom/android/internal/util/IndentingPrintWriter;)V
    .registers 9
    .parameter "pw"

    #@0
    .prologue
    .line 1533
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@3
    .line 1534
    invoke-direct {p0}, Landroid/content/SyncManager;->getAllUsers()Ljava/util/List;

    #@6
    move-result-object v4

    #@7
    .line 1535
    .local v4, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    if-eqz v4, :cond_5b

    #@9
    .line 1536
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v0

    #@d
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v5

    #@11
    if-eqz v5, :cond_5b

    #@13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v3

    #@17
    check-cast v3, Landroid/content/pm/UserInfo;

    #@19
    .line 1537
    .local v3, user:Landroid/content/pm/UserInfo;
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v6, "Sync adapters for "

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    const-string v6, ":"

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {p1, v5}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@35
    .line 1538
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@38
    .line 1540
    iget-object v5, p0, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@3a
    iget v6, v3, Landroid/content/pm/UserInfo;->id:I

    #@3c
    invoke-virtual {v5, v6}, Landroid/content/SyncAdaptersCache;->getAllServices(I)Ljava/util/Collection;

    #@3f
    move-result-object v5

    #@40
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@43
    move-result-object v1

    #@44
    .local v1, i$:Ljava/util/Iterator;
    :goto_44
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@47
    move-result v5

    #@48
    if-eqz v5, :cond_54

    #@4a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4d
    move-result-object v2

    #@4e
    check-cast v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@50
    .line 1541
    .local v2, info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<*>;"
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/Object;)V

    #@53
    goto :goto_44

    #@54
    .line 1543
    .end local v2           #info:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<*>;"
    :cond_54
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@57
    .line 1544
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@5a
    goto :goto_d

    #@5b
    .line 1547
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #user:Landroid/content/pm/UserInfo;
    :cond_5b
    return-void
.end method

.method private dumpTimeSec(Ljava/io/PrintWriter;J)V
    .registers 8
    .parameter "pw"
    .parameter "time"

    #@0
    .prologue
    .line 1257
    const-wide/16 v0, 0x3e8

    #@2
    div-long v0, p2, v0

    #@4
    invoke-virtual {p1, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    #@7
    const/16 v0, 0x2e

    #@9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V

    #@c
    const-wide/16 v0, 0x64

    #@e
    div-long v0, p2, v0

    #@10
    const-wide/16 v2, 0xa

    #@12
    rem-long/2addr v0, v2

    #@13
    invoke-virtual {p1, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    #@16
    .line 1258
    const/16 v0, 0x73

    #@18
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V

    #@1b
    .line 1259
    return-void
.end method

.method private ensureAlarmService()V
    .registers 3

    #@0
    .prologue
    .line 474
    iget-object v0, p0, Landroid/content/SyncManager;->mAlarmService:Landroid/app/AlarmManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 475
    iget-object v0, p0, Landroid/content/SyncManager;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "alarm"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/app/AlarmManager;

    #@e
    iput-object v0, p0, Landroid/content/SyncManager;->mAlarmService:Landroid/app/AlarmManager;

    #@10
    .line 477
    :cond_10
    return-void
.end method

.method static formatTime(J)Ljava/lang/String;
    .registers 4
    .parameter "time"

    #@0
    .prologue
    .line 1075
    new-instance v0, Landroid/text/format/Time;

    #@2
    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    #@5
    .line 1076
    .local v0, tobj:Landroid/text/format/Time;
    invoke-virtual {v0, p0, p1}, Landroid/text/format/Time;->set(J)V

    #@8
    .line 1077
    const-string v1, "%Y-%m-%d %H:%M:%S"

    #@a
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method

.method private getAllUsers()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Landroid/content/SyncManager;->mUserManager:Landroid/os/UserManager;

    #@2
    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private getConnectivityManager()Landroid/net/ConnectivityManager;
    .registers 3

    #@0
    .prologue
    .line 333
    monitor-enter p0

    #@1
    .line 334
    :try_start_1
    iget-object v0, p0, Landroid/content/SyncManager;->mConnManagerDoNotUseDirectly:Landroid/net/ConnectivityManager;

    #@3
    if-nez v0, :cond_11

    #@5
    .line 335
    iget-object v0, p0, Landroid/content/SyncManager;->mContext:Landroid/content/Context;

    #@7
    const-string v1, "connectivity"

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/net/ConnectivityManager;

    #@f
    iput-object v0, p0, Landroid/content/SyncManager;->mConnManagerDoNotUseDirectly:Landroid/net/ConnectivityManager;

    #@11
    .line 338
    :cond_11
    iget-object v0, p0, Landroid/content/SyncManager;->mConnManagerDoNotUseDirectly:Landroid/net/ConnectivityManager;

    #@13
    monitor-exit p0

    #@14
    return-object v0

    #@15
    .line 339
    :catchall_15
    move-exception v0

    #@16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method private getLastFailureMessage(I)Ljava/lang/String;
    .registers 3
    .parameter "code"

    #@0
    .prologue
    .line 1226
    packed-switch p1, :pswitch_data_24

    #@3
    .line 1252
    const-string/jumbo v0, "unknown"

    #@6
    :goto_6
    return-object v0

    #@7
    .line 1228
    :pswitch_7
    const-string/jumbo v0, "sync already in progress"

    #@a
    goto :goto_6

    #@b
    .line 1231
    :pswitch_b
    const-string v0, "authentication error"

    #@d
    goto :goto_6

    #@e
    .line 1234
    :pswitch_e
    const-string v0, "I/O error"

    #@10
    goto :goto_6

    #@11
    .line 1237
    :pswitch_11
    const-string/jumbo v0, "parse error"

    #@14
    goto :goto_6

    #@15
    .line 1240
    :pswitch_15
    const-string v0, "conflict error"

    #@17
    goto :goto_6

    #@18
    .line 1243
    :pswitch_18
    const-string/jumbo v0, "too many deletions error"

    #@1b
    goto :goto_6

    #@1c
    .line 1246
    :pswitch_1c
    const-string/jumbo v0, "too many retries error"

    #@1f
    goto :goto_6

    #@20
    .line 1249
    :pswitch_20
    const-string v0, "internal error"

    #@22
    goto :goto_6

    #@23
    .line 1226
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_7
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_15
        :pswitch_18
        :pswitch_1c
        :pswitch_20
    .end packed-switch
.end method

.method private increaseBackoffSetting(Landroid/content/SyncOperation;)V
    .registers 20
    .parameter "op"

    #@0
    .prologue
    .line 732
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v11

    #@4
    .line 734
    .local v11, now:J
    move-object/from16 v0, p0

    #@6
    iget-object v1, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@8
    move-object/from16 v0, p1

    #@a
    iget-object v2, v0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@c
    move-object/from16 v0, p1

    #@e
    iget v3, v0, Landroid/content/SyncOperation;->userId:I

    #@10
    move-object/from16 v0, p1

    #@12
    iget-object v4, v0, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@14
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/SyncStorageEngine;->getBackoff(Landroid/accounts/Account;ILjava/lang/String;)Landroid/util/Pair;

    #@17
    move-result-object v13

    #@18
    .line 736
    .local v13, previousSettings:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    const-wide/16 v7, -0x1

    #@1a
    .line 737
    .local v7, newDelayInMs:J
    if-eqz v13, :cond_69

    #@1c
    .line 740
    iget-object v1, v13, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@1e
    check-cast v1, Ljava/lang/Long;

    #@20
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    #@23
    move-result-wide v1

    #@24
    cmp-long v1, v11, v1

    #@26
    if-gez v1, :cond_5d

    #@28
    .line 741
    const-string v1, "SyncManager"

    #@2a
    const/4 v2, 0x2

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_5c

    #@31
    .line 742
    const-string v2, "SyncManager"

    #@33
    new-instance v1, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "Still in backoff, do not increase it. Remaining: "

    #@3a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    iget-object v1, v13, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@40
    check-cast v1, Ljava/lang/Long;

    #@42
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    #@45
    move-result-wide v14

    #@46
    sub-long/2addr v14, v11

    #@47
    const-wide/16 v16, 0x3e8

    #@49
    div-long v14, v14, v16

    #@4b
    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    const-string v3, " seconds."

    #@51
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 775
    :cond_5c
    :goto_5c
    return-void

    #@5d
    .line 748
    :cond_5d
    iget-object v1, v13, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@5f
    check-cast v1, Ljava/lang/Long;

    #@61
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    #@64
    move-result-wide v1

    #@65
    const-wide/16 v3, 0x2

    #@67
    mul-long v7, v1, v3

    #@69
    .line 750
    :cond_69
    const-wide/16 v1, 0x0

    #@6b
    cmp-long v1, v7, v1

    #@6d
    if-gtz v1, :cond_7a

    #@6f
    .line 752
    const-wide/16 v1, 0x7530

    #@71
    const-wide/32 v3, 0x80e8

    #@74
    move-object/from16 v0, p0

    #@76
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/SyncManager;->jitterize(JJ)J

    #@79
    move-result-wide v7

    #@7a
    .line 757
    :cond_7a
    move-object/from16 v0, p0

    #@7c
    iget-object v1, v0, Landroid/content/SyncManager;->mContext:Landroid/content/Context;

    #@7e
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@81
    move-result-object v1

    #@82
    const-string/jumbo v2, "sync_max_retry_delay_in_seconds"

    #@85
    const-wide/16 v3, 0xe10

    #@87
    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@8a
    move-result-wide v9

    #@8b
    .line 760
    .local v9, maxSyncRetryTimeInSeconds:J
    const-wide/16 v1, 0x3e8

    #@8d
    mul-long/2addr v1, v9

    #@8e
    cmp-long v1, v7, v1

    #@90
    if-lez v1, :cond_96

    #@92
    .line 761
    const-wide/16 v1, 0x3e8

    #@94
    mul-long v7, v9, v1

    #@96
    .line 764
    :cond_96
    add-long v5, v11, v7

    #@98
    .line 766
    .local v5, backoff:J
    move-object/from16 v0, p0

    #@9a
    iget-object v1, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@9c
    move-object/from16 v0, p1

    #@9e
    iget-object v2, v0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@a0
    move-object/from16 v0, p1

    #@a2
    iget v3, v0, Landroid/content/SyncOperation;->userId:I

    #@a4
    move-object/from16 v0, p1

    #@a6
    iget-object v4, v0, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@a8
    invoke-virtual/range {v1 .. v8}, Landroid/content/SyncStorageEngine;->setBackoff(Landroid/accounts/Account;ILjava/lang/String;JJ)V

    #@ab
    .line 769
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@ae
    move-result-object v1

    #@af
    move-object/from16 v0, p1

    #@b1
    iput-object v1, v0, Landroid/content/SyncOperation;->backoff:Ljava/lang/Long;

    #@b3
    .line 770
    invoke-virtual/range {p1 .. p1}, Landroid/content/SyncOperation;->updateEffectiveRunTime()V

    #@b6
    .line 772
    move-object/from16 v0, p0

    #@b8
    iget-object v14, v0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@ba
    monitor-enter v14

    #@bb
    .line 773
    :try_start_bb
    move-object/from16 v0, p0

    #@bd
    iget-object v1, v0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@bf
    move-object/from16 v0, p1

    #@c1
    iget-object v2, v0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@c3
    move-object/from16 v0, p1

    #@c5
    iget v3, v0, Landroid/content/SyncOperation;->userId:I

    #@c7
    move-object/from16 v0, p1

    #@c9
    iget-object v4, v0, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@cb
    invoke-virtual/range {v1 .. v6}, Landroid/content/SyncQueue;->onBackoffChanged(Landroid/accounts/Account;ILjava/lang/String;J)V

    #@ce
    .line 774
    monitor-exit v14

    #@cf
    goto :goto_5c

    #@d0
    :catchall_d0
    move-exception v1

    #@d1
    monitor-exit v14
    :try_end_d2
    .catchall {:try_start_bb .. :try_end_d2} :catchall_d0

    #@d2
    throw v1
.end method

.method private isSyncStillActive(Landroid/content/SyncManager$ActiveSyncContext;)Z
    .registers 5
    .parameter "activeSyncContext"

    #@0
    .prologue
    .line 2625
    iget-object v2, p0, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_16

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/content/SyncManager$ActiveSyncContext;

    #@12
    .line 2626
    .local v1, sync:Landroid/content/SyncManager$ActiveSyncContext;
    if-ne v1, p1, :cond_6

    #@14
    .line 2627
    const/4 v2, 0x1

    #@15
    .line 2630
    .end local v1           #sync:Landroid/content/SyncManager$ActiveSyncContext;
    :goto_15
    return v2

    #@16
    :cond_16
    const/4 v2, 0x0

    #@17
    goto :goto_15
.end method

.method private jitterize(JJ)J
    .registers 10
    .parameter "minValue"
    .parameter "maxValue"

    #@0
    .prologue
    .line 460
    new-instance v0, Ljava/util/Random;

    #@2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5
    move-result-wide v3

    #@6
    invoke-direct {v0, v3, v4}, Ljava/util/Random;-><init>(J)V

    #@9
    .line 461
    .local v0, random:Ljava/util/Random;
    sub-long v1, p3, p1

    #@b
    .line 462
    .local v1, spread:J
    const-wide/32 v3, 0x7fffffff

    #@e
    cmp-long v3, v1, v3

    #@10
    if-lez v3, :cond_1b

    #@12
    .line 463
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@14
    const-string/jumbo v4, "the difference between the maxValue and the minValue must be less than 2147483647"

    #@17
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v3

    #@1b
    .line 466
    :cond_1b
    long-to-int v3, v1

    #@1c
    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    #@1f
    move-result v3

    #@20
    int-to-long v3, v3

    #@21
    add-long/2addr v3, p1

    #@22
    return-wide v3
.end method

.method private onUserRemoved(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 932
    invoke-virtual {p0}, Landroid/content/SyncManager;->updateRunningAccounts()V

    #@3
    .line 935
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@5
    const/4 v1, 0x0

    #@6
    new-array v1, v1, [Landroid/accounts/Account;

    #@8
    invoke-virtual {v0, v1, p1}, Landroid/content/SyncStorageEngine;->doDatabaseCleanup([Landroid/accounts/Account;I)V

    #@b
    .line 936
    iget-object v1, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@d
    monitor-enter v1

    #@e
    .line 937
    :try_start_e
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@10
    invoke-virtual {v0, p1}, Landroid/content/SyncQueue;->removeUser(I)V

    #@13
    .line 938
    monitor-exit v1

    #@14
    .line 939
    return-void

    #@15
    .line 938
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_e .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method private onUserStarting(I)V
    .registers 14
    .parameter "userId"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 902
    invoke-static {}, Landroid/accounts/AccountManagerService;->getSingleton()Landroid/accounts/AccountManagerService;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {v0, p1}, Landroid/accounts/AccountManagerService;->validateAccounts(I)V

    #@8
    .line 904
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@a
    invoke-virtual {v0, p1}, Landroid/content/SyncAdaptersCache;->invalidateCache(I)V

    #@d
    .line 906
    invoke-virtual {p0}, Landroid/content/SyncManager;->updateRunningAccounts()V

    #@10
    .line 908
    iget-object v2, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@12
    monitor-enter v2

    #@13
    .line 909
    :try_start_13
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@15
    invoke-virtual {v0, p1}, Landroid/content/SyncQueue;->addPendingOperations(I)V

    #@18
    .line 910
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_13 .. :try_end_19} :catchall_34

    #@19
    .line 913
    invoke-static {}, Landroid/accounts/AccountManagerService;->getSingleton()Landroid/accounts/AccountManagerService;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0, p1}, Landroid/accounts/AccountManagerService;->getAccounts(I)[Landroid/accounts/Account;

    #@20
    move-result-object v8

    #@21
    .line 914
    .local v8, accounts:[Landroid/accounts/Account;
    move-object v9, v8

    #@22
    .local v9, arr$:[Landroid/accounts/Account;
    array-length v11, v9

    #@23
    .local v11, len$:I
    const/4 v10, 0x0

    #@24
    .local v10, i$:I
    :goto_24
    if-ge v10, v11, :cond_37

    #@26
    aget-object v1, v9, v10

    #@28
    .line 915
    .local v1, account:Landroid/accounts/Account;
    const-wide/16 v5, 0x0

    #@2a
    const/4 v7, 0x1

    #@2b
    move-object v0, p0

    #@2c
    move v2, p1

    #@2d
    move-object v4, v3

    #@2e
    invoke-virtual/range {v0 .. v7}, Landroid/content/SyncManager;->scheduleSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;JZ)V

    #@31
    .line 914
    add-int/lit8 v10, v10, 0x1

    #@33
    goto :goto_24

    #@34
    .line 910
    .end local v1           #account:Landroid/accounts/Account;
    .end local v8           #accounts:[Landroid/accounts/Account;
    .end local v9           #arr$:[Landroid/accounts/Account;
    .end local v10           #i$:I
    .end local v11           #len$:I
    :catchall_34
    move-exception v0

    #@35
    :try_start_35
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    #@36
    throw v0

    #@37
    .line 919
    .restart local v8       #accounts:[Landroid/accounts/Account;
    .restart local v9       #arr$:[Landroid/accounts/Account;
    .restart local v10       #i$:I
    .restart local v11       #len$:I
    :cond_37
    invoke-direct {p0}, Landroid/content/SyncManager;->sendCheckAlarmsMessage()V

    #@3a
    .line 920
    return-void
.end method

.method private onUserStopping(I)V
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 923
    invoke-virtual {p0}, Landroid/content/SyncManager;->updateRunningAccounts()V

    #@4
    .line 925
    invoke-virtual {p0, v0, p1, v0}, Landroid/content/SyncManager;->cancelActiveSync(Landroid/accounts/Account;ILjava/lang/String;)V

    #@7
    .line 929
    return-void
.end method

.method private readDataConnectionState()Z
    .registers 3

    #@0
    .prologue
    .line 298
    invoke-direct {p0}, Landroid/content/SyncManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    #@7
    move-result-object v0

    #@8
    .line 299
    .local v0, networkInfo:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_12

    #@a
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    const/4 v1, 0x1

    #@11
    :goto_11
    return v1

    #@12
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_11
.end method

.method private sendCancelSyncsMessage(Landroid/accounts/Account;ILjava/lang/String;)V
    .registers 7
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    .line 696
    const-string v1, "SyncManager"

    #@2
    const/4 v2, 0x2

    #@3
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_11

    #@9
    const-string v1, "SyncManager"

    #@b
    const-string/jumbo v2, "sending MESSAGE_CANCEL"

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 697
    :cond_11
    iget-object v1, p0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@13
    invoke-virtual {v1}, Landroid/content/SyncManager$SyncHandler;->obtainMessage()Landroid/os/Message;

    #@16
    move-result-object v0

    #@17
    .line 698
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x6

    #@18
    iput v1, v0, Landroid/os/Message;->what:I

    #@1a
    .line 699
    invoke-static {p1, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@1d
    move-result-object v1

    #@1e
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20
    .line 700
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@22
    .line 701
    iget-object v1, p0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@24
    invoke-virtual {v1, v0}, Landroid/content/SyncManager$SyncHandler;->sendMessage(Landroid/os/Message;)Z

    #@27
    .line 702
    return-void
.end method

.method private sendCheckAlarmsMessage()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    .line 680
    const-string v0, "SyncManager"

    #@3
    const/4 v1, 0x2

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_12

    #@a
    const-string v0, "SyncManager"

    #@c
    const-string/jumbo v1, "sending MESSAGE_CHECK_ALARMS"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 681
    :cond_12
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@14
    invoke-virtual {v0, v2}, Landroid/content/SyncManager$SyncHandler;->removeMessages(I)V

    #@17
    .line 682
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@19
    invoke-virtual {v0, v2}, Landroid/content/SyncManager$SyncHandler;->sendEmptyMessage(I)Z

    #@1c
    .line 683
    return-void
.end method

.method private sendSyncAlarmMessage()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 675
    const-string v0, "SyncManager"

    #@3
    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_11

    #@9
    const-string v0, "SyncManager"

    #@b
    const-string/jumbo v1, "sending MESSAGE_SYNC_ALARM"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 676
    :cond_11
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@13
    invoke-virtual {v0, v2}, Landroid/content/SyncManager$SyncHandler;->sendEmptyMessage(I)Z

    #@16
    .line 677
    return-void
.end method

.method private sendSyncFinishedOrCanceledMessage(Landroid/content/SyncManager$ActiveSyncContext;Landroid/content/SyncResult;)V
    .registers 6
    .parameter "syncContext"
    .parameter "syncResult"

    #@0
    .prologue
    .line 687
    const-string v1, "SyncManager"

    #@2
    const/4 v2, 0x2

    #@3
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_11

    #@9
    const-string v1, "SyncManager"

    #@b
    const-string/jumbo v2, "sending MESSAGE_SYNC_FINISHED"

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 688
    :cond_11
    iget-object v1, p0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@13
    invoke-virtual {v1}, Landroid/content/SyncManager$SyncHandler;->obtainMessage()Landroid/os/Message;

    #@16
    move-result-object v0

    #@17
    .line 689
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@18
    iput v1, v0, Landroid/os/Message;->what:I

    #@1a
    .line 690
    new-instance v1, Landroid/content/SyncManager$SyncHandlerMessagePayload;

    #@1c
    invoke-direct {v1, p0, p1, p2}, Landroid/content/SyncManager$SyncHandlerMessagePayload;-><init>(Landroid/content/SyncManager;Landroid/content/SyncManager$ActiveSyncContext;Landroid/content/SyncResult;)V

    #@1f
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    .line 691
    iget-object v1, p0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@23
    invoke-virtual {v1, v0}, Landroid/content/SyncManager$SyncHandler;->sendMessage(Landroid/os/Message;)Z

    #@26
    .line 692
    return-void
.end method

.method private setDelayUntilTime(Landroid/content/SyncOperation;J)V
    .registers 14
    .parameter "op"
    .parameter "delayUntilSeconds"

    #@0
    .prologue
    .line 778
    const-wide/16 v0, 0x3e8

    #@2
    mul-long v8, p2, v0

    #@4
    .line 779
    .local v8, delayUntil:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@7
    move-result-wide v6

    #@8
    .line 781
    .local v6, absoluteNow:J
    cmp-long v0, v8, v6

    #@a
    if-lez v0, :cond_2d

    #@c
    .line 782
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@f
    move-result-wide v0

    #@10
    sub-long v2, v8, v6

    #@12
    add-long v4, v0, v2

    #@14
    .line 786
    .local v4, newDelayUntilTime:J
    :goto_14
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@16
    iget-object v1, p1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@18
    iget v2, p1, Landroid/content/SyncOperation;->userId:I

    #@1a
    iget-object v3, p1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@1c
    invoke-virtual/range {v0 .. v5}, Landroid/content/SyncStorageEngine;->setDelayUntilTime(Landroid/accounts/Account;ILjava/lang/String;J)V

    #@1f
    .line 788
    iget-object v1, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@21
    monitor-enter v1

    #@22
    .line 789
    :try_start_22
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@24
    iget-object v2, p1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@26
    iget-object v3, p1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@28
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/content/SyncQueue;->onDelayUntilTimeChanged(Landroid/accounts/Account;Ljava/lang/String;J)V

    #@2b
    .line 790
    monitor-exit v1

    #@2c
    .line 791
    return-void

    #@2d
    .line 784
    .end local v4           #newDelayUntilTime:J
    :cond_2d
    const-wide/16 v4, 0x0

    #@2f
    .restart local v4       #newDelayUntilTime:J
    goto :goto_14

    #@30
    .line 790
    :catchall_30
    move-exception v0

    #@31
    monitor-exit v1
    :try_end_32
    .catchall {:try_start_22 .. :try_end_32} :catchall_30

    #@32
    throw v0
.end method


# virtual methods
.method public cancelActiveSync(Landroid/accounts/Account;ILjava/lang/String;)V
    .registers 4
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    .line 799
    invoke-direct {p0, p1, p2, p3}, Landroid/content/SyncManager;->sendCancelSyncsMessage(Landroid/accounts/Account;ILjava/lang/String;)V

    #@3
    .line 800
    return-void
.end method

.method public clearScheduledSyncOperations(Landroid/accounts/Account;ILjava/lang/String;)V
    .registers 12
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    const-wide/16 v4, -0x1

    #@2
    .line 832
    iget-object v1, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@4
    monitor-enter v1

    #@5
    .line 833
    :try_start_5
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@7
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/SyncQueue;->remove(Landroid/accounts/Account;ILjava/lang/String;)V

    #@a
    .line 834
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_5 .. :try_end_b} :catchall_15

    #@b
    .line 835
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@d
    move-object v1, p1

    #@e
    move v2, p2

    #@f
    move-object v3, p3

    #@10
    move-wide v6, v4

    #@11
    invoke-virtual/range {v0 .. v7}, Landroid/content/SyncStorageEngine;->setBackoff(Landroid/accounts/Account;ILjava/lang/String;JJ)V

    #@14
    .line 837
    return-void

    #@15
    .line 834
    :catchall_15
    move-exception v0

    #@16
    :try_start_16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .registers 5
    .parameter "fd"
    .parameter "pw"

    #@0
    .prologue
    .line 1068
    new-instance v0, Lcom/android/internal/util/IndentingPrintWriter;

    #@2
    const-string v1, "  "

    #@4
    invoke-direct {v0, p2, v1}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    #@7
    .line 1069
    .local v0, ipw:Lcom/android/internal/util/IndentingPrintWriter;
    invoke-virtual {p0, v0}, Landroid/content/SyncManager;->dumpSyncState(Ljava/io/PrintWriter;)V

    #@a
    .line 1070
    invoke-virtual {p0, v0}, Landroid/content/SyncManager;->dumpSyncHistory(Ljava/io/PrintWriter;)V

    #@d
    .line 1071
    invoke-direct {p0, v0}, Landroid/content/SyncManager;->dumpSyncAdapters(Lcom/android/internal/util/IndentingPrintWriter;)V

    #@10
    .line 1072
    return-void
.end method

.method protected dumpSyncHistory(Ljava/io/PrintWriter;)V
    .registers 2
    .parameter "pw"

    #@0
    .prologue
    .line 1276
    invoke-direct {p0, p1}, Landroid/content/SyncManager;->dumpRecentHistory(Ljava/io/PrintWriter;)V

    #@3
    .line 1277
    invoke-direct {p0, p1}, Landroid/content/SyncManager;->dumpDayStatistics(Ljava/io/PrintWriter;)V

    #@6
    .line 1278
    return-void
.end method

.method protected dumpSyncState(Ljava/io/PrintWriter;)V
    .registers 35
    .parameter "pw"

    #@0
    .prologue
    .line 1081
    const-string v28, "data connected: "

    #@2
    move-object/from16 v0, p1

    #@4
    move-object/from16 v1, v28

    #@6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9
    move-object/from16 v0, p0

    #@b
    iget-boolean v0, v0, Landroid/content/SyncManager;->mDataConnectionIsConnected:Z

    #@d
    move/from16 v28, v0

    #@f
    move-object/from16 v0, p1

    #@11
    move/from16 v1, v28

    #@13
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@16
    .line 1082
    const-string v28, "auto sync: "

    #@18
    move-object/from16 v0, p1

    #@1a
    move-object/from16 v1, v28

    #@1c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f
    .line 1083
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager;->getAllUsers()Ljava/util/List;

    #@22
    move-result-object v27

    #@23
    .line 1084
    .local v27, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    if-eqz v27, :cond_7a

    #@25
    .line 1085
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@28
    move-result-object v11

    #@29
    .local v11, i$:Ljava/util/Iterator;
    :goto_29
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@2c
    move-result v28

    #@2d
    if-eqz v28, :cond_77

    #@2f
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@32
    move-result-object v26

    #@33
    check-cast v26, Landroid/content/pm/UserInfo;

    #@35
    .line 1086
    .local v26, user:Landroid/content/pm/UserInfo;
    new-instance v28, Ljava/lang/StringBuilder;

    #@37
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string/jumbo v29, "u"

    #@3d
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v28

    #@41
    move-object/from16 v0, v26

    #@43
    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    #@45
    move/from16 v29, v0

    #@47
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v28

    #@4b
    const-string v29, "="

    #@4d
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v28

    #@51
    move-object/from16 v0, p0

    #@53
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@55
    move-object/from16 v29, v0

    #@57
    move-object/from16 v0, v26

    #@59
    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    #@5b
    move/from16 v30, v0

    #@5d
    invoke-virtual/range {v29 .. v30}, Landroid/content/SyncStorageEngine;->getMasterSyncAutomatically(I)Z

    #@60
    move-result v29

    #@61
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@64
    move-result-object v28

    #@65
    const-string v29, " "

    #@67
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v28

    #@6b
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v28

    #@6f
    move-object/from16 v0, p1

    #@71
    move-object/from16 v1, v28

    #@73
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@76
    goto :goto_29

    #@77
    .line 1089
    .end local v26           #user:Landroid/content/pm/UserInfo;
    :cond_77
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@7a
    .line 1091
    .end local v11           #i$:Ljava/util/Iterator;
    :cond_7a
    const-string/jumbo v28, "memory low: "

    #@7d
    move-object/from16 v0, p1

    #@7f
    move-object/from16 v1, v28

    #@81
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@84
    move-object/from16 v0, p0

    #@86
    iget-boolean v0, v0, Landroid/content/SyncManager;->mStorageIsLow:Z

    #@88
    move/from16 v28, v0

    #@8a
    move-object/from16 v0, p1

    #@8c
    move/from16 v1, v28

    #@8e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@91
    .line 1093
    invoke-static {}, Landroid/accounts/AccountManagerService;->getSingleton()Landroid/accounts/AccountManagerService;

    #@94
    move-result-object v28

    #@95
    invoke-virtual/range {v28 .. v28}, Landroid/accounts/AccountManagerService;->getAllAccounts()[Landroid/accounts/AccountAndUser;

    #@98
    move-result-object v5

    #@99
    .line 1095
    .local v5, accounts:[Landroid/accounts/AccountAndUser;
    const-string v28, "accounts: "

    #@9b
    move-object/from16 v0, p1

    #@9d
    move-object/from16 v1, v28

    #@9f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a2
    .line 1096
    sget-object v28, Landroid/content/SyncManager;->INITIAL_ACCOUNTS_ARRAY:[Landroid/accounts/AccountAndUser;

    #@a4
    move-object/from16 v0, v28

    #@a6
    if-eq v5, v0, :cond_296

    #@a8
    .line 1097
    array-length v0, v5

    #@a9
    move/from16 v28, v0

    #@ab
    move-object/from16 v0, p1

    #@ad
    move/from16 v1, v28

    #@af
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(I)V

    #@b2
    .line 1101
    :goto_b2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@b5
    move-result-wide v19

    #@b6
    .line 1102
    .local v19, now:J
    const-string/jumbo v28, "now: "

    #@b9
    move-object/from16 v0, p1

    #@bb
    move-object/from16 v1, v28

    #@bd
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c0
    move-object/from16 v0, p1

    #@c2
    move-wide/from16 v1, v19

    #@c4
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@c7
    .line 1103
    new-instance v28, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v29, " ("

    #@ce
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v28

    #@d2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@d5
    move-result-wide v29

    #@d6
    invoke-static/range {v29 .. v30}, Landroid/content/SyncManager;->formatTime(J)Ljava/lang/String;

    #@d9
    move-result-object v29

    #@da
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v28

    #@de
    const-string v29, ")"

    #@e0
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v28

    #@e4
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e7
    move-result-object v28

    #@e8
    move-object/from16 v0, p1

    #@ea
    move-object/from16 v1, v28

    #@ec
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ef
    .line 1104
    const-string/jumbo v28, "offset: "

    #@f2
    move-object/from16 v0, p1

    #@f4
    move-object/from16 v1, v28

    #@f6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f9
    move-object/from16 v0, p0

    #@fb
    iget v0, v0, Landroid/content/SyncManager;->mSyncRandomOffsetMillis:I

    #@fd
    move/from16 v28, v0

    #@ff
    move/from16 v0, v28

    #@101
    div-int/lit16 v0, v0, 0x3e8

    #@103
    move/from16 v28, v0

    #@105
    move/from16 v0, v28

    #@107
    int-to-long v0, v0

    #@108
    move-wide/from16 v28, v0

    #@10a
    invoke-static/range {v28 .. v29}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    #@10d
    move-result-object v28

    #@10e
    move-object/from16 v0, p1

    #@110
    move-object/from16 v1, v28

    #@112
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@115
    .line 1105
    const-string v28, " (HH:MM:SS)"

    #@117
    move-object/from16 v0, p1

    #@119
    move-object/from16 v1, v28

    #@11b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@11e
    .line 1106
    const-string/jumbo v28, "uptime: "

    #@121
    move-object/from16 v0, p1

    #@123
    move-object/from16 v1, v28

    #@125
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@128
    const-wide/16 v28, 0x3e8

    #@12a
    div-long v28, v19, v28

    #@12c
    invoke-static/range {v28 .. v29}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    #@12f
    move-result-object v28

    #@130
    move-object/from16 v0, p1

    #@132
    move-object/from16 v1, v28

    #@134
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@137
    .line 1107
    const-string v28, " (HH:MM:SS)"

    #@139
    move-object/from16 v0, p1

    #@13b
    move-object/from16 v1, v28

    #@13d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@140
    .line 1108
    const-string/jumbo v28, "time spent syncing: "

    #@143
    move-object/from16 v0, p1

    #@145
    move-object/from16 v1, v28

    #@147
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14a
    .line 1109
    move-object/from16 v0, p0

    #@14c
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@14e
    move-object/from16 v28, v0

    #@150
    move-object/from16 v0, v28

    #@152
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->mSyncTimeTracker:Landroid/content/SyncManager$SyncTimeTracker;

    #@154
    move-object/from16 v28, v0

    #@156
    invoke-virtual/range {v28 .. v28}, Landroid/content/SyncManager$SyncTimeTracker;->timeSpentSyncing()J

    #@159
    move-result-wide v28

    #@15a
    const-wide/16 v30, 0x3e8

    #@15c
    div-long v28, v28, v30

    #@15e
    invoke-static/range {v28 .. v29}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    #@161
    move-result-object v28

    #@162
    move-object/from16 v0, p1

    #@164
    move-object/from16 v1, v28

    #@166
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@169
    .line 1111
    const-string v28, " (HH:MM:SS), sync "

    #@16b
    move-object/from16 v0, p1

    #@16d
    move-object/from16 v1, v28

    #@16f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@172
    .line 1112
    move-object/from16 v0, p0

    #@174
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@176
    move-object/from16 v28, v0

    #@178
    move-object/from16 v0, v28

    #@17a
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->mSyncTimeTracker:Landroid/content/SyncManager$SyncTimeTracker;

    #@17c
    move-object/from16 v28, v0

    #@17e
    move-object/from16 v0, v28

    #@180
    iget-boolean v0, v0, Landroid/content/SyncManager$SyncTimeTracker;->mLastWasSyncing:Z

    #@182
    move/from16 v28, v0

    #@184
    if-eqz v28, :cond_2a2

    #@186
    const-string v28, ""

    #@188
    :goto_188
    move-object/from16 v0, p1

    #@18a
    move-object/from16 v1, v28

    #@18c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18f
    .line 1113
    const-string v28, "in progress"

    #@191
    move-object/from16 v0, p1

    #@193
    move-object/from16 v1, v28

    #@195
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@198
    .line 1114
    move-object/from16 v0, p0

    #@19a
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@19c
    move-object/from16 v28, v0

    #@19e
    invoke-static/range {v28 .. v28}, Landroid/content/SyncManager$SyncHandler;->access$1600(Landroid/content/SyncManager$SyncHandler;)Ljava/lang/Long;

    #@1a1
    move-result-object v28

    #@1a2
    if-eqz v28, :cond_2a7

    #@1a4
    .line 1115
    const-string/jumbo v28, "next alarm time: "

    #@1a7
    move-object/from16 v0, p1

    #@1a9
    move-object/from16 v1, v28

    #@1ab
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ae
    move-object/from16 v0, p0

    #@1b0
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@1b2
    move-object/from16 v28, v0

    #@1b4
    invoke-static/range {v28 .. v28}, Landroid/content/SyncManager$SyncHandler;->access$1600(Landroid/content/SyncManager$SyncHandler;)Ljava/lang/Long;

    #@1b7
    move-result-object v28

    #@1b8
    move-object/from16 v0, p1

    #@1ba
    move-object/from16 v1, v28

    #@1bc
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@1bf
    .line 1116
    const-string v28, " ("

    #@1c1
    move-object/from16 v0, p1

    #@1c3
    move-object/from16 v1, v28

    #@1c5
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c8
    .line 1117
    move-object/from16 v0, p0

    #@1ca
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@1cc
    move-object/from16 v28, v0

    #@1ce
    invoke-static/range {v28 .. v28}, Landroid/content/SyncManager$SyncHandler;->access$1600(Landroid/content/SyncManager$SyncHandler;)Ljava/lang/Long;

    #@1d1
    move-result-object v28

    #@1d2
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Long;->longValue()J

    #@1d5
    move-result-wide v28

    #@1d6
    sub-long v28, v28, v19

    #@1d8
    const-wide/16 v30, 0x3e8

    #@1da
    div-long v28, v28, v30

    #@1dc
    invoke-static/range {v28 .. v29}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    #@1df
    move-result-object v28

    #@1e0
    move-object/from16 v0, p1

    #@1e2
    move-object/from16 v1, v28

    #@1e4
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e7
    .line 1118
    const-string v28, " (HH:MM:SS) from now)"

    #@1e9
    move-object/from16 v0, p1

    #@1eb
    move-object/from16 v1, v28

    #@1ed
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1f0
    .line 1123
    :goto_1f0
    const-string/jumbo v28, "notification info: "

    #@1f3
    move-object/from16 v0, p1

    #@1f5
    move-object/from16 v1, v28

    #@1f7
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1fa
    .line 1124
    new-instance v22, Ljava/lang/StringBuilder;

    #@1fc
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@1ff
    .line 1125
    .local v22, sb:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    #@201
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncHandler:Landroid/content/SyncManager$SyncHandler;

    #@203
    move-object/from16 v28, v0

    #@205
    move-object/from16 v0, v28

    #@207
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@209
    move-object/from16 v28, v0

    #@20b
    move-object/from16 v0, v28

    #@20d
    move-object/from16 v1, v22

    #@20f
    invoke-virtual {v0, v1}, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->toString(Ljava/lang/StringBuilder;)V

    #@212
    .line 1126
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@215
    move-result-object v28

    #@216
    move-object/from16 v0, p1

    #@218
    move-object/from16 v1, v28

    #@21a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@21d
    .line 1128
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@220
    .line 1129
    new-instance v28, Ljava/lang/StringBuilder;

    #@222
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@225
    const-string v29, "Active Syncs: "

    #@227
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v28

    #@22b
    move-object/from16 v0, p0

    #@22d
    iget-object v0, v0, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@22f
    move-object/from16 v29, v0

    #@231
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    #@234
    move-result v29

    #@235
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@238
    move-result-object v28

    #@239
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23c
    move-result-object v28

    #@23d
    move-object/from16 v0, p1

    #@23f
    move-object/from16 v1, v28

    #@241
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@244
    .line 1130
    move-object/from16 v0, p0

    #@246
    iget-object v0, v0, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@248
    move-object/from16 v28, v0

    #@24a
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@24d
    move-result-object v11

    #@24e
    .restart local v11       #i$:Ljava/util/Iterator;
    :goto_24e
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@251
    move-result v28

    #@252
    if-eqz v28, :cond_2b3

    #@254
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@257
    move-result-object v6

    #@258
    check-cast v6, Landroid/content/SyncManager$ActiveSyncContext;

    #@25a
    .line 1131
    .local v6, activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    iget-wide v0, v6, Landroid/content/SyncManager$ActiveSyncContext;->mStartTime:J

    #@25c
    move-wide/from16 v28, v0

    #@25e
    sub-long v28, v19, v28

    #@260
    const-wide/16 v30, 0x3e8

    #@262
    div-long v8, v28, v30

    #@264
    .line 1132
    .local v8, durationInSeconds:J
    const-string v28, "  "

    #@266
    move-object/from16 v0, p1

    #@268
    move-object/from16 v1, v28

    #@26a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@26d
    .line 1133
    invoke-static {v8, v9}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    #@270
    move-result-object v28

    #@271
    move-object/from16 v0, p1

    #@273
    move-object/from16 v1, v28

    #@275
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@278
    .line 1134
    const-string v28, " - "

    #@27a
    move-object/from16 v0, p1

    #@27c
    move-object/from16 v1, v28

    #@27e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@281
    .line 1135
    iget-object v0, v6, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@283
    move-object/from16 v28, v0

    #@285
    const/16 v29, 0x0

    #@287
    invoke-virtual/range {v28 .. v29}, Landroid/content/SyncOperation;->dump(Z)Ljava/lang/String;

    #@28a
    move-result-object v28

    #@28b
    move-object/from16 v0, p1

    #@28d
    move-object/from16 v1, v28

    #@28f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@292
    .line 1136
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@295
    goto :goto_24e

    #@296
    .line 1099
    .end local v6           #activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    .end local v8           #durationInSeconds:J
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v19           #now:J
    .end local v22           #sb:Ljava/lang/StringBuilder;
    :cond_296
    const-string/jumbo v28, "not known yet"

    #@299
    move-object/from16 v0, p1

    #@29b
    move-object/from16 v1, v28

    #@29d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2a0
    goto/16 :goto_b2

    #@2a2
    .line 1112
    .restart local v19       #now:J
    :cond_2a2
    const-string/jumbo v28, "not "

    #@2a5
    goto/16 :goto_188

    #@2a7
    .line 1120
    :cond_2a7
    const-string/jumbo v28, "no alarm is scheduled (there had better not be any pending syncs)"

    #@2aa
    move-object/from16 v0, p1

    #@2ac
    move-object/from16 v1, v28

    #@2ae
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2b1
    goto/16 :goto_1f0

    #@2b3
    .line 1139
    .restart local v11       #i$:Ljava/util/Iterator;
    .restart local v22       #sb:Ljava/lang/StringBuilder;
    :cond_2b3
    move-object/from16 v0, p0

    #@2b5
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@2b7
    move-object/from16 v29, v0

    #@2b9
    monitor-enter v29

    #@2ba
    .line 1140
    const/16 v28, 0x0

    #@2bc
    :try_start_2bc
    move-object/from16 v0, v22

    #@2be
    move/from16 v1, v28

    #@2c0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    #@2c3
    .line 1141
    move-object/from16 v0, p0

    #@2c5
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@2c7
    move-object/from16 v28, v0

    #@2c9
    move-object/from16 v0, v28

    #@2cb
    move-object/from16 v1, v22

    #@2cd
    invoke-virtual {v0, v1}, Landroid/content/SyncQueue;->dump(Ljava/lang/StringBuilder;)V

    #@2d0
    .line 1142
    monitor-exit v29
    :try_end_2d1
    .catchall {:try_start_2bc .. :try_end_2d1} :catchall_54f

    #@2d1
    .line 1143
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@2d4
    .line 1144
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d7
    move-result-object v28

    #@2d8
    move-object/from16 v0, p1

    #@2da
    move-object/from16 v1, v28

    #@2dc
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2df
    .line 1147
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@2e2
    .line 1148
    const-string v28, "Sync Status"

    #@2e4
    move-object/from16 v0, p1

    #@2e6
    move-object/from16 v1, v28

    #@2e8
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2eb
    .line 1149
    move-object v7, v5

    #@2ec
    .local v7, arr$:[Landroid/accounts/AccountAndUser;
    array-length v0, v7

    #@2ed
    move/from16 v16, v0

    #@2ef
    .local v16, len$:I
    const/4 v11, 0x0

    #@2f0
    .local v11, i$:I
    move v12, v11

    #@2f1
    .end local v11           #i$:I
    .local v12, i$:I
    :goto_2f1
    move/from16 v0, v16

    #@2f3
    if-ge v12, v0, :cond_6ed

    #@2f5
    aget-object v4, v7, v12

    #@2f7
    .line 1150
    .local v4, account:Landroid/accounts/AccountAndUser;
    const-string v28, "  Account "

    #@2f9
    move-object/from16 v0, p1

    #@2fb
    move-object/from16 v1, v28

    #@2fd
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@300
    iget-object v0, v4, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@302
    move-object/from16 v28, v0

    #@304
    move-object/from16 v0, v28

    #@306
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@308
    move-object/from16 v28, v0

    #@30a
    move-object/from16 v0, p1

    #@30c
    move-object/from16 v1, v28

    #@30e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@311
    .line 1151
    const-string v28, " u"

    #@313
    move-object/from16 v0, p1

    #@315
    move-object/from16 v1, v28

    #@317
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@31a
    iget v0, v4, Landroid/accounts/AccountAndUser;->userId:I

    #@31c
    move/from16 v28, v0

    #@31e
    move-object/from16 v0, p1

    #@320
    move/from16 v1, v28

    #@322
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@325
    .line 1152
    const-string v28, " "

    #@327
    move-object/from16 v0, p1

    #@329
    move-object/from16 v1, v28

    #@32b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@32e
    iget-object v0, v4, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@330
    move-object/from16 v28, v0

    #@332
    move-object/from16 v0, v28

    #@334
    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@336
    move-object/from16 v28, v0

    #@338
    move-object/from16 v0, p1

    #@33a
    move-object/from16 v1, v28

    #@33c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@33f
    .line 1153
    const-string v28, ":"

    #@341
    move-object/from16 v0, p1

    #@343
    move-object/from16 v1, v28

    #@345
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@348
    .line 1155
    move-object/from16 v0, p0

    #@34a
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@34c
    move-object/from16 v28, v0

    #@34e
    iget v0, v4, Landroid/accounts/AccountAndUser;->userId:I

    #@350
    move/from16 v29, v0

    #@352
    invoke-virtual/range {v28 .. v29}, Landroid/content/SyncAdaptersCache;->getAllServices(I)Ljava/util/Collection;

    #@355
    move-result-object v28

    #@356
    invoke-interface/range {v28 .. v28}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@359
    move-result-object v11

    #@35a
    .end local v12           #i$:I
    .local v11, i$:Ljava/util/Iterator;
    :cond_35a
    :goto_35a
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@35d
    move-result v28

    #@35e
    if-eqz v28, :cond_6e8

    #@360
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@363
    move-result-object v25

    #@364
    check-cast v25, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@366
    .line 1156
    .local v25, syncAdapterType:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    move-object/from16 v0, v25

    #@368
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@36a
    move-object/from16 v28, v0

    #@36c
    check-cast v28, Landroid/content/SyncAdapterType;

    #@36e
    move-object/from16 v0, v28

    #@370
    iget-object v0, v0, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    #@372
    move-object/from16 v28, v0

    #@374
    iget-object v0, v4, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@376
    move-object/from16 v29, v0

    #@378
    move-object/from16 v0, v29

    #@37a
    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@37c
    move-object/from16 v29, v0

    #@37e
    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@381
    move-result v28

    #@382
    if-eqz v28, :cond_35a

    #@384
    .line 1160
    move-object/from16 v0, p0

    #@386
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@388
    move-object/from16 v29, v0

    #@38a
    iget-object v0, v4, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@38c
    move-object/from16 v30, v0

    #@38e
    iget v0, v4, Landroid/accounts/AccountAndUser;->userId:I

    #@390
    move/from16 v31, v0

    #@392
    move-object/from16 v0, v25

    #@394
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@396
    move-object/from16 v28, v0

    #@398
    check-cast v28, Landroid/content/SyncAdapterType;

    #@39a
    move-object/from16 v0, v28

    #@39c
    iget-object v0, v0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@39e
    move-object/from16 v28, v0

    #@3a0
    move-object/from16 v0, v29

    #@3a2
    move-object/from16 v1, v30

    #@3a4
    move/from16 v2, v31

    #@3a6
    move-object/from16 v3, v28

    #@3a8
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/SyncStorageEngine;->getOrCreateAuthority(Landroid/accounts/Account;ILjava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@3ab
    move-result-object v23

    #@3ac
    .line 1163
    .local v23, settings:Landroid/content/SyncStorageEngine$AuthorityInfo;
    move-object/from16 v0, p0

    #@3ae
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@3b0
    move-object/from16 v28, v0

    #@3b2
    move-object/from16 v0, v28

    #@3b4
    move-object/from16 v1, v23

    #@3b6
    invoke-virtual {v0, v1}, Landroid/content/SyncStorageEngine;->getOrCreateSyncStatus(Landroid/content/SyncStorageEngine$AuthorityInfo;)Landroid/content/SyncStatusInfo;

    #@3b9
    move-result-object v24

    #@3ba
    .line 1164
    .local v24, status:Landroid/content/SyncStatusInfo;
    const-string v28, "    "

    #@3bc
    move-object/from16 v0, p1

    #@3be
    move-object/from16 v1, v28

    #@3c0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3c3
    move-object/from16 v0, v23

    #@3c5
    iget-object v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@3c7
    move-object/from16 v28, v0

    #@3c9
    move-object/from16 v0, p1

    #@3cb
    move-object/from16 v1, v28

    #@3cd
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3d0
    .line 1165
    const-string v28, ":"

    #@3d2
    move-object/from16 v0, p1

    #@3d4
    move-object/from16 v1, v28

    #@3d6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3d9
    .line 1166
    const-string v28, "      settings:"

    #@3db
    move-object/from16 v0, p1

    #@3dd
    move-object/from16 v1, v28

    #@3df
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3e2
    .line 1167
    new-instance v28, Ljava/lang/StringBuilder;

    #@3e4
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@3e7
    const-string v29, " "

    #@3e9
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ec
    move-result-object v29

    #@3ed
    move-object/from16 v0, v23

    #@3ef
    iget v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@3f1
    move/from16 v28, v0

    #@3f3
    if-lez v28, :cond_552

    #@3f5
    const-string/jumbo v28, "syncable"

    #@3f8
    :goto_3f8
    move-object/from16 v0, v29

    #@3fa
    move-object/from16 v1, v28

    #@3fc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ff
    move-result-object v28

    #@400
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@403
    move-result-object v28

    #@404
    move-object/from16 v0, p1

    #@406
    move-object/from16 v1, v28

    #@408
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@40b
    .line 1170
    new-instance v28, Ljava/lang/StringBuilder;

    #@40d
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@410
    const-string v29, ", "

    #@412
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@415
    move-result-object v29

    #@416
    move-object/from16 v0, v23

    #@418
    iget-boolean v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@41a
    move/from16 v28, v0

    #@41c
    if-eqz v28, :cond_564

    #@41e
    const-string v28, "enabled"

    #@420
    :goto_420
    move-object/from16 v0, v29

    #@422
    move-object/from16 v1, v28

    #@424
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@427
    move-result-object v28

    #@428
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42b
    move-result-object v28

    #@42c
    move-object/from16 v0, p1

    #@42e
    move-object/from16 v1, v28

    #@430
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@433
    .line 1171
    move-object/from16 v0, v23

    #@435
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->delayUntil:J

    #@437
    move-wide/from16 v28, v0

    #@439
    cmp-long v28, v28, v19

    #@43b
    if-lez v28, :cond_469

    #@43d
    .line 1172
    new-instance v28, Ljava/lang/StringBuilder;

    #@43f
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@442
    const-string v29, ", delay for "

    #@444
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@447
    move-result-object v28

    #@448
    move-object/from16 v0, v23

    #@44a
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->delayUntil:J

    #@44c
    move-wide/from16 v29, v0

    #@44e
    sub-long v29, v29, v19

    #@450
    const-wide/16 v31, 0x3e8

    #@452
    div-long v29, v29, v31

    #@454
    invoke-virtual/range {v28 .. v30}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@457
    move-result-object v28

    #@458
    const-string v29, " sec"

    #@45a
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45d
    move-result-object v28

    #@45e
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@461
    move-result-object v28

    #@462
    move-object/from16 v0, p1

    #@464
    move-object/from16 v1, v28

    #@466
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@469
    .line 1175
    :cond_469
    move-object/from16 v0, v23

    #@46b
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@46d
    move-wide/from16 v28, v0

    #@46f
    cmp-long v28, v28, v19

    #@471
    if-lez v28, :cond_49f

    #@473
    .line 1176
    new-instance v28, Ljava/lang/StringBuilder;

    #@475
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@478
    const-string v29, ", backoff for "

    #@47a
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47d
    move-result-object v28

    #@47e
    move-object/from16 v0, v23

    #@480
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@482
    move-wide/from16 v29, v0

    #@484
    sub-long v29, v29, v19

    #@486
    const-wide/16 v31, 0x3e8

    #@488
    div-long v29, v29, v31

    #@48a
    invoke-virtual/range {v28 .. v30}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@48d
    move-result-object v28

    #@48e
    const-string v29, " sec"

    #@490
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@493
    move-result-object v28

    #@494
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@497
    move-result-object v28

    #@498
    move-object/from16 v0, p1

    #@49a
    move-object/from16 v1, v28

    #@49c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@49f
    .line 1179
    :cond_49f
    move-object/from16 v0, v23

    #@4a1
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@4a3
    move-wide/from16 v28, v0

    #@4a5
    const-wide/16 v30, 0x0

    #@4a7
    cmp-long v28, v28, v30

    #@4a9
    if-lez v28, :cond_4d5

    #@4ab
    .line 1180
    new-instance v28, Ljava/lang/StringBuilder;

    #@4ad
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@4b0
    const-string v29, ", the backoff increment is "

    #@4b2
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b5
    move-result-object v28

    #@4b6
    move-object/from16 v0, v23

    #@4b8
    iget-wide v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@4ba
    move-wide/from16 v29, v0

    #@4bc
    const-wide/16 v31, 0x3e8

    #@4be
    div-long v29, v29, v31

    #@4c0
    invoke-virtual/range {v28 .. v30}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4c3
    move-result-object v28

    #@4c4
    const-string v29, " sec"

    #@4c6
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c9
    move-result-object v28

    #@4ca
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4cd
    move-result-object v28

    #@4ce
    move-object/from16 v0, p1

    #@4d0
    move-object/from16 v1, v28

    #@4d2
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4d5
    .line 1183
    :cond_4d5
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@4d8
    .line 1184
    const/16 v21, 0x0

    #@4da
    .line 1185
    .local v21, periodicIndex:I
    :goto_4da
    move-object/from16 v0, v23

    #@4dc
    iget-object v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@4de
    move-object/from16 v28, v0

    #@4e0
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    #@4e3
    move-result v28

    #@4e4
    move/from16 v0, v21

    #@4e6
    move/from16 v1, v28

    #@4e8
    if-ge v0, v1, :cond_568

    #@4ea
    .line 1187
    move-object/from16 v0, v23

    #@4ec
    iget-object v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@4ee
    move-object/from16 v28, v0

    #@4f0
    move-object/from16 v0, v28

    #@4f2
    move/from16 v1, v21

    #@4f4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4f7
    move-result-object v13

    #@4f8
    check-cast v13, Landroid/util/Pair;

    #@4fa
    .line 1188
    .local v13, info:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    move-object/from16 v0, v24

    #@4fc
    move/from16 v1, v21

    #@4fe
    invoke-virtual {v0, v1}, Landroid/content/SyncStatusInfo;->getPeriodicSyncTime(I)J

    #@501
    move-result-wide v14

    #@502
    .line 1189
    .local v14, lastPeriodicTime:J
    iget-object v0, v13, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@504
    move-object/from16 v28, v0

    #@506
    check-cast v28, Ljava/lang/Long;

    #@508
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Long;->longValue()J

    #@50b
    move-result-wide v28

    #@50c
    const-wide/16 v30, 0x3e8

    #@50e
    mul-long v28, v28, v30

    #@510
    add-long v17, v14, v28

    #@512
    .line 1190
    .local v17, nextPeriodicTime:J
    new-instance v28, Ljava/lang/StringBuilder;

    #@514
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@517
    const-string v29, "      periodic period="

    #@519
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51c
    move-result-object v28

    #@51d
    iget-object v0, v13, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@51f
    move-object/from16 v29, v0

    #@521
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@524
    move-result-object v28

    #@525
    const-string v29, ", extras="

    #@527
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52a
    move-result-object v28

    #@52b
    iget-object v0, v13, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@52d
    move-object/from16 v29, v0

    #@52f
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@532
    move-result-object v28

    #@533
    const-string v29, ", next="

    #@535
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@538
    move-result-object v28

    #@539
    invoke-static/range {v17 .. v18}, Landroid/content/SyncManager;->formatTime(J)Ljava/lang/String;

    #@53c
    move-result-object v29

    #@53d
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@540
    move-result-object v28

    #@541
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@544
    move-result-object v28

    #@545
    move-object/from16 v0, p1

    #@547
    move-object/from16 v1, v28

    #@549
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@54c
    .line 1186
    add-int/lit8 v21, v21, 0x1

    #@54e
    goto :goto_4da

    #@54f
    .line 1142
    .end local v4           #account:Landroid/accounts/AccountAndUser;
    .end local v7           #arr$:[Landroid/accounts/AccountAndUser;
    .end local v13           #info:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    .end local v14           #lastPeriodicTime:J
    .end local v16           #len$:I
    .end local v17           #nextPeriodicTime:J
    .end local v21           #periodicIndex:I
    .end local v23           #settings:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v24           #status:Landroid/content/SyncStatusInfo;
    .end local v25           #syncAdapterType:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    :catchall_54f
    move-exception v28

    #@550
    :try_start_550
    monitor-exit v29
    :try_end_551
    .catchall {:try_start_550 .. :try_end_551} :catchall_54f

    #@551
    throw v28

    #@552
    .line 1167
    .restart local v4       #account:Landroid/accounts/AccountAndUser;
    .restart local v7       #arr$:[Landroid/accounts/AccountAndUser;
    .restart local v16       #len$:I
    .restart local v23       #settings:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v24       #status:Landroid/content/SyncStatusInfo;
    .restart local v25       #syncAdapterType:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    :cond_552
    move-object/from16 v0, v23

    #@554
    iget v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@556
    move/from16 v28, v0

    #@558
    if-nez v28, :cond_55f

    #@55a
    const-string/jumbo v28, "not syncable"

    #@55d
    goto/16 :goto_3f8

    #@55f
    :cond_55f
    const-string/jumbo v28, "not initialized"

    #@562
    goto/16 :goto_3f8

    #@564
    .line 1170
    :cond_564
    const-string v28, "disabled"

    #@566
    goto/16 :goto_420

    #@568
    .line 1194
    .restart local v21       #periodicIndex:I
    :cond_568
    const-string v28, "      count: local="

    #@56a
    move-object/from16 v0, p1

    #@56c
    move-object/from16 v1, v28

    #@56e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@571
    move-object/from16 v0, v24

    #@573
    iget v0, v0, Landroid/content/SyncStatusInfo;->numSourceLocal:I

    #@575
    move/from16 v28, v0

    #@577
    move-object/from16 v0, p1

    #@579
    move/from16 v1, v28

    #@57b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@57e
    .line 1195
    const-string v28, " poll="

    #@580
    move-object/from16 v0, p1

    #@582
    move-object/from16 v1, v28

    #@584
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@587
    move-object/from16 v0, v24

    #@589
    iget v0, v0, Landroid/content/SyncStatusInfo;->numSourcePoll:I

    #@58b
    move/from16 v28, v0

    #@58d
    move-object/from16 v0, p1

    #@58f
    move/from16 v1, v28

    #@591
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@594
    .line 1196
    const-string v28, " periodic="

    #@596
    move-object/from16 v0, p1

    #@598
    move-object/from16 v1, v28

    #@59a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@59d
    move-object/from16 v0, v24

    #@59f
    iget v0, v0, Landroid/content/SyncStatusInfo;->numSourcePeriodic:I

    #@5a1
    move/from16 v28, v0

    #@5a3
    move-object/from16 v0, p1

    #@5a5
    move/from16 v1, v28

    #@5a7
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@5aa
    .line 1197
    const-string v28, " server="

    #@5ac
    move-object/from16 v0, p1

    #@5ae
    move-object/from16 v1, v28

    #@5b0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5b3
    move-object/from16 v0, v24

    #@5b5
    iget v0, v0, Landroid/content/SyncStatusInfo;->numSourceServer:I

    #@5b7
    move/from16 v28, v0

    #@5b9
    move-object/from16 v0, p1

    #@5bb
    move/from16 v1, v28

    #@5bd
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@5c0
    .line 1198
    const-string v28, " user="

    #@5c2
    move-object/from16 v0, p1

    #@5c4
    move-object/from16 v1, v28

    #@5c6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5c9
    move-object/from16 v0, v24

    #@5cb
    iget v0, v0, Landroid/content/SyncStatusInfo;->numSourceUser:I

    #@5cd
    move/from16 v28, v0

    #@5cf
    move-object/from16 v0, p1

    #@5d1
    move/from16 v1, v28

    #@5d3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@5d6
    .line 1199
    const-string v28, " total="

    #@5d8
    move-object/from16 v0, p1

    #@5da
    move-object/from16 v1, v28

    #@5dc
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5df
    move-object/from16 v0, v24

    #@5e1
    iget v0, v0, Landroid/content/SyncStatusInfo;->numSyncs:I

    #@5e3
    move/from16 v28, v0

    #@5e5
    move-object/from16 v0, p1

    #@5e7
    move/from16 v1, v28

    #@5e9
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@5ec
    .line 1200
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@5ef
    .line 1201
    const-string v28, "      total duration: "

    #@5f1
    move-object/from16 v0, p1

    #@5f3
    move-object/from16 v1, v28

    #@5f5
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5f8
    .line 1202
    move-object/from16 v0, v24

    #@5fa
    iget-wide v0, v0, Landroid/content/SyncStatusInfo;->totalElapsedTime:J

    #@5fc
    move-wide/from16 v28, v0

    #@5fe
    const-wide/16 v30, 0x3e8

    #@600
    div-long v28, v28, v30

    #@602
    invoke-static/range {v28 .. v29}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    #@605
    move-result-object v28

    #@606
    move-object/from16 v0, p1

    #@608
    move-object/from16 v1, v28

    #@60a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@60d
    .line 1203
    move-object/from16 v0, v24

    #@60f
    iget-wide v0, v0, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    #@611
    move-wide/from16 v28, v0

    #@613
    const-wide/16 v30, 0x0

    #@615
    cmp-long v28, v28, v30

    #@617
    if-eqz v28, :cond_64d

    #@619
    .line 1204
    const-string v28, "      SUCCESS: source="

    #@61b
    move-object/from16 v0, p1

    #@61d
    move-object/from16 v1, v28

    #@61f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@622
    .line 1205
    sget-object v28, Landroid/content/SyncStorageEngine;->SOURCES:[Ljava/lang/String;

    #@624
    move-object/from16 v0, v24

    #@626
    iget v0, v0, Landroid/content/SyncStatusInfo;->lastSuccessSource:I

    #@628
    move/from16 v29, v0

    #@62a
    aget-object v28, v28, v29

    #@62c
    move-object/from16 v0, p1

    #@62e
    move-object/from16 v1, v28

    #@630
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@633
    .line 1206
    const-string v28, " time="

    #@635
    move-object/from16 v0, p1

    #@637
    move-object/from16 v1, v28

    #@639
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@63c
    .line 1207
    move-object/from16 v0, v24

    #@63e
    iget-wide v0, v0, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    #@640
    move-wide/from16 v28, v0

    #@642
    invoke-static/range {v28 .. v29}, Landroid/content/SyncManager;->formatTime(J)Ljava/lang/String;

    #@645
    move-result-object v28

    #@646
    move-object/from16 v0, p1

    #@648
    move-object/from16 v1, v28

    #@64a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@64d
    .line 1209
    :cond_64d
    move-object/from16 v0, v24

    #@64f
    iget-wide v0, v0, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    #@651
    move-wide/from16 v28, v0

    #@653
    const-wide/16 v30, 0x0

    #@655
    cmp-long v28, v28, v30

    #@657
    if-eqz v28, :cond_35a

    #@659
    .line 1210
    const-string v28, "      FAILURE: source="

    #@65b
    move-object/from16 v0, p1

    #@65d
    move-object/from16 v1, v28

    #@65f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@662
    .line 1211
    sget-object v28, Landroid/content/SyncStorageEngine;->SOURCES:[Ljava/lang/String;

    #@664
    move-object/from16 v0, v24

    #@666
    iget v0, v0, Landroid/content/SyncStatusInfo;->lastFailureSource:I

    #@668
    move/from16 v29, v0

    #@66a
    aget-object v28, v28, v29

    #@66c
    move-object/from16 v0, p1

    #@66e
    move-object/from16 v1, v28

    #@670
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@673
    .line 1213
    const-string v28, " initialTime="

    #@675
    move-object/from16 v0, p1

    #@677
    move-object/from16 v1, v28

    #@679
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@67c
    .line 1214
    move-object/from16 v0, v24

    #@67e
    iget-wide v0, v0, Landroid/content/SyncStatusInfo;->initialFailureTime:J

    #@680
    move-wide/from16 v28, v0

    #@682
    invoke-static/range {v28 .. v29}, Landroid/content/SyncManager;->formatTime(J)Ljava/lang/String;

    #@685
    move-result-object v28

    #@686
    move-object/from16 v0, p1

    #@688
    move-object/from16 v1, v28

    #@68a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@68d
    .line 1215
    const-string v28, " lastTime="

    #@68f
    move-object/from16 v0, p1

    #@691
    move-object/from16 v1, v28

    #@693
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@696
    .line 1216
    move-object/from16 v0, v24

    #@698
    iget-wide v0, v0, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    #@69a
    move-wide/from16 v28, v0

    #@69c
    invoke-static/range {v28 .. v29}, Landroid/content/SyncManager;->formatTime(J)Ljava/lang/String;

    #@69f
    move-result-object v28

    #@6a0
    move-object/from16 v0, p1

    #@6a2
    move-object/from16 v1, v28

    #@6a4
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6a7
    .line 1217
    const/16 v28, 0x0

    #@6a9
    move-object/from16 v0, v24

    #@6ab
    move/from16 v1, v28

    #@6ad
    invoke-virtual {v0, v1}, Landroid/content/SyncStatusInfo;->getLastFailureMesgAsInt(I)I

    #@6b0
    move-result v10

    #@6b1
    .line 1218
    .local v10, errCode:I
    const-string v28, "      message: "

    #@6b3
    move-object/from16 v0, p1

    #@6b5
    move-object/from16 v1, v28

    #@6b7
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6ba
    new-instance v28, Ljava/lang/StringBuilder;

    #@6bc
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@6bf
    move-object/from16 v0, p0

    #@6c1
    invoke-direct {v0, v10}, Landroid/content/SyncManager;->getLastFailureMessage(I)Ljava/lang/String;

    #@6c4
    move-result-object v29

    #@6c5
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c8
    move-result-object v28

    #@6c9
    const-string v29, " ("

    #@6cb
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6ce
    move-result-object v28

    #@6cf
    move-object/from16 v0, v28

    #@6d1
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d4
    move-result-object v28

    #@6d5
    const-string v29, ")"

    #@6d7
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6da
    move-result-object v28

    #@6db
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6de
    move-result-object v28

    #@6df
    move-object/from16 v0, p1

    #@6e1
    move-object/from16 v1, v28

    #@6e3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6e6
    goto/16 :goto_35a

    #@6e8
    .line 1149
    .end local v10           #errCode:I
    .end local v21           #periodicIndex:I
    .end local v23           #settings:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v24           #status:Landroid/content/SyncStatusInfo;
    .end local v25           #syncAdapterType:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    :cond_6e8
    add-int/lit8 v11, v12, 0x1

    #@6ea
    .local v11, i$:I
    move v12, v11

    #@6eb
    .end local v11           #i$:I
    .restart local v12       #i$:I
    goto/16 :goto_2f1

    #@6ed
    .line 1223
    .end local v4           #account:Landroid/accounts/AccountAndUser;
    :cond_6ed
    return-void
.end method

.method public getSyncAdapterTypes(I)[Landroid/content/SyncAdapterType;
    .registers 8
    .parameter "userId"

    #@0
    .prologue
    .line 664
    iget-object v5, p0, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@2
    invoke-virtual {v5, p1}, Landroid/content/SyncAdaptersCache;->getAllServices(I)Ljava/util/Collection;

    #@5
    move-result-object v3

    #@6
    .line 665
    .local v3, serviceInfos:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;>;"
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    #@9
    move-result v5

    #@a
    new-array v4, v5, [Landroid/content/SyncAdapterType;

    #@c
    .line 666
    .local v4, types:[Landroid/content/SyncAdapterType;
    const/4 v0, 0x0

    #@d
    .line 667
    .local v0, i:I
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v1

    #@11
    .local v1, i$:Ljava/util/Iterator;
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v5

    #@15
    if-eqz v5, :cond_26

    #@17
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@1d
    .line 668
    .local v2, serviceInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    iget-object v5, v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@1f
    check-cast v5, Landroid/content/SyncAdapterType;

    #@21
    aput-object v5, v4, v0

    #@23
    .line 669
    add-int/lit8 v0, v0, 0x1

    #@25
    goto :goto_11

    #@26
    .line 671
    .end local v2           #serviceInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    :cond_26
    return-object v4
.end method

.method public getSyncStorageEngine()Landroid/content/SyncStorageEngine;
    .registers 2

    #@0
    .prologue
    .line 470
    iget-object v0, p0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@2
    return-object v0
.end method

.method maybeRescheduleSync(Landroid/content/SyncResult;Landroid/content/SyncOperation;)V
    .registers 19
    .parameter "syncResult"
    .parameter "operation"

    #@0
    .prologue
    .line 840
    const-string v1, "SyncManager"

    #@2
    const/4 v2, 0x3

    #@3
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v14

    #@7
    .line 841
    .local v14, isLoggable:Z
    if-eqz v14, :cond_2f

    #@9
    .line 842
    const-string v1, "SyncManager"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "encountered error(s) during the sync: "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    move-object/from16 v0, p1

    #@18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, ", "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    move-object/from16 v0, p2

    #@24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 845
    :cond_2f
    new-instance v15, Landroid/content/SyncOperation;

    #@31
    move-object/from16 v0, p2

    #@33
    invoke-direct {v15, v0}, Landroid/content/SyncOperation;-><init>(Landroid/content/SyncOperation;)V

    #@36
    .line 850
    .end local p2
    .local v15, operation:Landroid/content/SyncOperation;
    iget-object v1, v15, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@38
    const-string v2, "ignore_backoff"

    #@3a
    const/4 v3, 0x0

    #@3b
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@3e
    move-result v1

    #@3f
    if-eqz v1, :cond_48

    #@41
    .line 851
    iget-object v1, v15, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@43
    const-string v2, "ignore_backoff"

    #@45
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@48
    .line 860
    :cond_48
    iget-object v1, v15, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@4a
    const-string v2, "do_not_retry"

    #@4c
    const/4 v3, 0x0

    #@4d
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@50
    move-result v1

    #@51
    if-eqz v1, :cond_6d

    #@53
    .line 861
    const-string v1, "SyncManager"

    #@55
    new-instance v2, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string/jumbo v3, "not retrying sync operation because SYNC_EXTRAS_DO_NOT_RETRY was specified "

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v2

    #@69
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 898
    :goto_6c
    return-void

    #@6d
    .line 863
    :cond_6d
    iget-object v1, v15, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@6f
    const-string/jumbo v2, "upload"

    #@72
    const/4 v3, 0x0

    #@73
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@76
    move-result v1

    #@77
    if-eqz v1, :cond_a6

    #@79
    move-object/from16 v0, p1

    #@7b
    iget-boolean v1, v0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@7d
    if-nez v1, :cond_a6

    #@7f
    .line 865
    iget-object v1, v15, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@81
    const-string/jumbo v2, "upload"

    #@84
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@87
    .line 866
    const-string v1, "SyncManager"

    #@89
    new-instance v2, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string/jumbo v3, "retrying sync operation as a two-way sync because an upload-only sync encountered an error: "

    #@91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v2

    #@95
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v2

    #@99
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v2

    #@9d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    .line 868
    move-object/from16 v0, p0

    #@a2
    invoke-virtual {v0, v15}, Landroid/content/SyncManager;->scheduleSyncOperation(Landroid/content/SyncOperation;)V

    #@a5
    goto :goto_6c

    #@a6
    .line 869
    :cond_a6
    move-object/from16 v0, p1

    #@a8
    iget-boolean v1, v0, Landroid/content/SyncResult;->tooManyRetries:Z

    #@aa
    if-eqz v1, :cond_c6

    #@ac
    .line 870
    const-string v1, "SyncManager"

    #@ae
    new-instance v2, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string/jumbo v3, "not retrying sync operation because it retried too many times: "

    #@b6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v2

    #@ba
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v2

    #@be
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v2

    #@c2
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    goto :goto_6c

    #@c6
    .line 872
    :cond_c6
    invoke-virtual/range {p1 .. p1}, Landroid/content/SyncResult;->madeSomeProgress()Z

    #@c9
    move-result v1

    #@ca
    if-eqz v1, :cond_dc

    #@cc
    .line 873
    if-eqz v14, :cond_d6

    #@ce
    .line 874
    const-string v1, "SyncManager"

    #@d0
    const-string/jumbo v2, "retrying sync operation because even though it had an error it achieved some success"

    #@d3
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    .line 877
    :cond_d6
    move-object/from16 v0, p0

    #@d8
    invoke-virtual {v0, v15}, Landroid/content/SyncManager;->scheduleSyncOperation(Landroid/content/SyncOperation;)V

    #@db
    goto :goto_6c

    #@dc
    .line 878
    :cond_dc
    move-object/from16 v0, p1

    #@de
    iget-boolean v1, v0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@e0
    if-eqz v1, :cond_11f

    #@e2
    .line 879
    if-eqz v14, :cond_fd

    #@e4
    .line 880
    const-string v1, "SyncManager"

    #@e6
    new-instance v2, Ljava/lang/StringBuilder;

    #@e8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@eb
    const-string/jumbo v3, "retrying sync operation that failed because there was already a sync in progress: "

    #@ee
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v2

    #@f2
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v2

    #@f6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v2

    #@fa
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fd
    .line 883
    :cond_fd
    new-instance v1, Landroid/content/SyncOperation;

    #@ff
    iget-object v2, v15, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@101
    iget v3, v15, Landroid/content/SyncOperation;->userId:I

    #@103
    iget v4, v15, Landroid/content/SyncOperation;->syncSource:I

    #@105
    iget-object v5, v15, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@107
    iget-object v6, v15, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@109
    const-wide/16 v7, 0x2710

    #@10b
    iget-object v9, v15, Landroid/content/SyncOperation;->backoff:Ljava/lang/Long;

    #@10d
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    #@110
    move-result-wide v9

    #@111
    iget-wide v11, v15, Landroid/content/SyncOperation;->delayUntil:J

    #@113
    iget-boolean v13, v15, Landroid/content/SyncOperation;->allowParallelSyncs:Z

    #@115
    invoke-direct/range {v1 .. v13}, Landroid/content/SyncOperation;-><init>(Landroid/accounts/Account;IILjava/lang/String;Landroid/os/Bundle;JJJZ)V

    #@118
    move-object/from16 v0, p0

    #@11a
    invoke-virtual {v0, v1}, Landroid/content/SyncManager;->scheduleSyncOperation(Landroid/content/SyncOperation;)V

    #@11d
    goto/16 :goto_6c

    #@11f
    .line 888
    :cond_11f
    invoke-virtual/range {p1 .. p1}, Landroid/content/SyncResult;->hasSoftError()Z

    #@122
    move-result v1

    #@123
    if-eqz v1, :cond_147

    #@125
    .line 889
    if-eqz v14, :cond_140

    #@127
    .line 890
    const-string v1, "SyncManager"

    #@129
    new-instance v2, Ljava/lang/StringBuilder;

    #@12b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12e
    const-string/jumbo v3, "retrying sync operation because it encountered a soft error: "

    #@131
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v2

    #@135
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v2

    #@139
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13c
    move-result-object v2

    #@13d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@140
    .line 893
    :cond_140
    move-object/from16 v0, p0

    #@142
    invoke-virtual {v0, v15}, Landroid/content/SyncManager;->scheduleSyncOperation(Landroid/content/SyncOperation;)V

    #@145
    goto/16 :goto_6c

    #@147
    .line 895
    :cond_147
    const-string v1, "SyncManager"

    #@149
    new-instance v2, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    const-string/jumbo v3, "not retrying sync operation because the error is a hard error: "

    #@151
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v2

    #@155
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v2

    #@159
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v2

    #@15d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@160
    goto/16 :goto_6c
.end method

.method public scheduleLocalSync(Landroid/accounts/Account;ILjava/lang/String;)V
    .registers 12
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    .line 656
    new-instance v4, Landroid/os/Bundle;

    #@2
    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 657
    .local v4, extras:Landroid/os/Bundle;
    const-string/jumbo v0, "upload"

    #@8
    const/4 v1, 0x1

    #@9
    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@c
    .line 658
    sget-wide v5, Landroid/content/SyncManager;->LOCAL_SYNC_DELAY:J

    #@e
    const/4 v7, 0x0

    #@f
    move-object v0, p0

    #@10
    move-object v1, p1

    #@11
    move v2, p2

    #@12
    move-object v3, p3

    #@13
    invoke-virtual/range {v0 .. v7}, Landroid/content/SyncManager;->scheduleSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;JZ)V

    #@16
    .line 660
    return-void
.end method

.method public scheduleSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;JZ)V
    .registers 56
    .parameter "requestedAccount"
    .parameter "userId"
    .parameter "requestedAuthority"
    .parameter "extras"
    .parameter "delay"
    .parameter "onlyThoseWithUnkownSyncableState"

    #@0
    .prologue
    .line 510
    const-string v2, "SyncManager"

    #@2
    const/4 v3, 0x2

    #@3
    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v39

    #@7
    .line 512
    .local v39, isLoggable:Z
    move-object/from16 v0, p0

    #@9
    iget-boolean v2, v0, Landroid/content/SyncManager;->mBootCompleted:Z

    #@b
    if-eqz v2, :cond_17

    #@d
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_c0

    #@17
    :cond_17
    const/16 v31, 0x1

    #@19
    .line 515
    .local v31, backgroundDataUsageAllowed:Z
    :goto_19
    if-nez p4, :cond_20

    #@1b
    new-instance p4, Landroid/os/Bundle;

    #@1d
    .end local p4
    invoke-direct/range {p4 .. p4}, Landroid/os/Bundle;-><init>()V

    #@20
    .line 517
    .restart local p4
    :cond_20
    const-string v2, "expedited"

    #@22
    const/4 v3, 0x0

    #@23
    move-object/from16 v0, p4

    #@25
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@28
    move-result v2

    #@29
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@2c
    move-result-object v33

    #@2d
    .line 518
    .local v33, expedited:Ljava/lang/Boolean;
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Boolean;->booleanValue()Z

    #@30
    move-result v2

    #@31
    if-eqz v2, :cond_35

    #@33
    .line 519
    const-wide/16 p5, -0x1

    #@35
    .line 523
    :cond_35
    if-eqz p1, :cond_c4

    #@37
    const/4 v2, -0x1

    #@38
    move/from16 v0, p2

    #@3a
    if-eq v0, v2, :cond_c4

    #@3c
    .line 524
    const/4 v2, 0x1

    #@3d
    new-array v0, v2, [Landroid/accounts/AccountAndUser;

    #@3f
    move-object/from16 v29, v0

    #@41
    const/4 v2, 0x0

    #@42
    new-instance v3, Landroid/accounts/AccountAndUser;

    #@44
    move-object/from16 v0, p1

    #@46
    move/from16 v1, p2

    #@48
    invoke-direct {v3, v0, v1}, Landroid/accounts/AccountAndUser;-><init>(Landroid/accounts/Account;I)V

    #@4b
    aput-object v3, v29, v2

    #@4d
    .line 537
    .local v29, accounts:[Landroid/accounts/AccountAndUser;
    :cond_4d
    const-string/jumbo v2, "upload"

    #@50
    const/4 v3, 0x0

    #@51
    move-object/from16 v0, p4

    #@53
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@56
    move-result v47

    #@57
    .line 538
    .local v47, uploadOnly:Z
    const-string v2, "force"

    #@59
    const/4 v3, 0x0

    #@5a
    move-object/from16 v0, p4

    #@5c
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@5f
    move-result v42

    #@60
    .line 539
    .local v42, manualSync:Z
    if-eqz v42, :cond_72

    #@62
    .line 540
    const-string v2, "ignore_backoff"

    #@64
    const/4 v3, 0x1

    #@65
    move-object/from16 v0, p4

    #@67
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@6a
    .line 541
    const-string v2, "ignore_settings"

    #@6c
    const/4 v3, 0x1

    #@6d
    move-object/from16 v0, p4

    #@6f
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@72
    .line 543
    :cond_72
    const-string v2, "ignore_settings"

    #@74
    const/4 v3, 0x0

    #@75
    move-object/from16 v0, p4

    #@77
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@7a
    move-result v37

    #@7b
    .line 547
    .local v37, ignoreSettings:Z
    if-eqz v47, :cond_da

    #@7d
    .line 548
    const/4 v5, 0x1

    #@7e
    .line 559
    .local v5, source:I
    :goto_7e
    move-object/from16 v30, v29

    #@80
    .local v30, arr$:[Landroid/accounts/AccountAndUser;
    move-object/from16 v0, v30

    #@82
    array-length v0, v0

    #@83
    move/from16 v41, v0

    #@85
    .local v41, len$:I
    const/16 v35, 0x0

    #@87
    .local v35, i$:I
    move/from16 v36, v35

    #@89
    .end local v35           #i$:I
    .local v36, i$:I
    :goto_89
    move/from16 v0, v36

    #@8b
    move/from16 v1, v41

    #@8d
    if-ge v0, v1, :cond_d9

    #@8f
    aget-object v28, v30, v36

    #@91
    .line 562
    .local v28, account:Landroid/accounts/AccountAndUser;
    new-instance v46, Ljava/util/HashSet;

    #@93
    invoke-direct/range {v46 .. v46}, Ljava/util/HashSet;-><init>()V

    #@96
    .line 564
    .local v46, syncableAuthorities:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    #@98
    iget-object v2, v0, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@9a
    move-object/from16 v0, v28

    #@9c
    iget v3, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@9e
    invoke-virtual {v2, v3}, Landroid/content/SyncAdaptersCache;->getAllServices(I)Ljava/util/Collection;

    #@a1
    move-result-object v2

    #@a2
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a5
    move-result-object v35

    #@a6
    .end local v36           #i$:I
    .local v35, i$:Ljava/util/Iterator;
    :goto_a6
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->hasNext()Z

    #@a9
    move-result v2

    #@aa
    if-eqz v2, :cond_e4

    #@ac
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@af
    move-result-object v43

    #@b0
    check-cast v43, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@b2
    .line 565
    .local v43, syncAdapter:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    move-object/from16 v0, v43

    #@b4
    iget-object v2, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@b6
    check-cast v2, Landroid/content/SyncAdapterType;

    #@b8
    iget-object v2, v2, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@ba
    move-object/from16 v0, v46

    #@bc
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@bf
    goto :goto_a6

    #@c0
    .line 512
    .end local v5           #source:I
    .end local v28           #account:Landroid/accounts/AccountAndUser;
    .end local v29           #accounts:[Landroid/accounts/AccountAndUser;
    .end local v30           #arr$:[Landroid/accounts/AccountAndUser;
    .end local v31           #backgroundDataUsageAllowed:Z
    .end local v33           #expedited:Ljava/lang/Boolean;
    .end local v35           #i$:Ljava/util/Iterator;
    .end local v37           #ignoreSettings:Z
    .end local v41           #len$:I
    .end local v42           #manualSync:Z
    .end local v43           #syncAdapter:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    .end local v46           #syncableAuthorities:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v47           #uploadOnly:Z
    :cond_c0
    const/16 v31, 0x0

    #@c2
    goto/16 :goto_19

    #@c4
    .line 528
    .restart local v31       #backgroundDataUsageAllowed:Z
    .restart local v33       #expedited:Ljava/lang/Boolean;
    :cond_c4
    move-object/from16 v0, p0

    #@c6
    iget-object v0, v0, Landroid/content/SyncManager;->mRunningAccounts:[Landroid/accounts/AccountAndUser;

    #@c8
    move-object/from16 v29, v0

    #@ca
    .line 529
    .restart local v29       #accounts:[Landroid/accounts/AccountAndUser;
    move-object/from16 v0, v29

    #@cc
    array-length v2, v0

    #@cd
    if-nez v2, :cond_4d

    #@cf
    .line 530
    if-eqz v39, :cond_d9

    #@d1
    .line 531
    const-string v2, "SyncManager"

    #@d3
    const-string/jumbo v3, "scheduleSync: no accounts configured, dropping"

    #@d6
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    .line 653
    :cond_d9
    return-void

    #@da
    .line 549
    .restart local v37       #ignoreSettings:Z
    .restart local v42       #manualSync:Z
    .restart local v47       #uploadOnly:Z
    :cond_da
    if-eqz v42, :cond_de

    #@dc
    .line 550
    const/4 v5, 0x3

    #@dd
    .restart local v5       #source:I
    goto :goto_7e

    #@de
    .line 551
    .end local v5           #source:I
    :cond_de
    if-nez p3, :cond_e2

    #@e0
    .line 552
    const/4 v5, 0x2

    #@e1
    .restart local v5       #source:I
    goto :goto_7e

    #@e2
    .line 556
    .end local v5           #source:I
    :cond_e2
    const/4 v5, 0x0

    #@e3
    .restart local v5       #source:I
    goto :goto_7e

    #@e4
    .line 571
    .restart local v28       #account:Landroid/accounts/AccountAndUser;
    .restart local v30       #arr$:[Landroid/accounts/AccountAndUser;
    .restart local v35       #i$:Ljava/util/Iterator;
    .restart local v41       #len$:I
    .restart local v46       #syncableAuthorities:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_e4
    if-eqz p3, :cond_fa

    #@e6
    .line 572
    move-object/from16 v0, v46

    #@e8
    move-object/from16 v1, p3

    #@ea
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@ed
    move-result v34

    #@ee
    .line 573
    .local v34, hasSyncAdapter:Z
    invoke-virtual/range {v46 .. v46}, Ljava/util/HashSet;->clear()V

    #@f1
    .line 574
    if-eqz v34, :cond_fa

    #@f3
    move-object/from16 v0, v46

    #@f5
    move-object/from16 v1, p3

    #@f7
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@fa
    .line 577
    .end local v34           #hasSyncAdapter:Z
    :cond_fa
    invoke-virtual/range {v46 .. v46}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@fd
    move-result-object v35

    #@fe
    :cond_fe
    :goto_fe
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->hasNext()Z

    #@101
    move-result v2

    #@102
    if-eqz v2, :cond_2d5

    #@104
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@107
    move-result-object v6

    #@108
    check-cast v6, Ljava/lang/String;

    #@10a
    .line 578
    .local v6, authority:Ljava/lang/String;
    move-object/from16 v0, p0

    #@10c
    iget-object v2, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@10e
    move-object/from16 v0, v28

    #@110
    iget-object v3, v0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@112
    move-object/from16 v0, v28

    #@114
    iget v4, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@116
    invoke-virtual {v2, v3, v4, v6}, Landroid/content/SyncStorageEngine;->getIsSyncable(Landroid/accounts/Account;ILjava/lang/String;)I

    #@119
    move-result v40

    #@11a
    .line 580
    .local v40, isSyncable:I
    if-eqz v40, :cond_fe

    #@11c
    .line 584
    move-object/from16 v0, p0

    #@11e
    iget-object v2, v0, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@120
    move-object/from16 v0, v28

    #@122
    iget-object v3, v0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@124
    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@126
    invoke-static {v6, v3}, Landroid/content/SyncAdapterType;->newKey(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SyncAdapterType;

    #@129
    move-result-object v3

    #@12a
    move-object/from16 v0, v28

    #@12c
    iget v4, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@12e
    invoke-virtual {v2, v3, v4}, Landroid/content/SyncAdaptersCache;->getServiceInfo(Ljava/lang/Object;I)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@131
    move-result-object v44

    #@132
    .line 586
    .local v44, syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    if-eqz v44, :cond_fe

    #@134
    .line 589
    move-object/from16 v0, v44

    #@136
    iget-object v2, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@138
    check-cast v2, Landroid/content/SyncAdapterType;

    #@13a
    invoke-virtual {v2}, Landroid/content/SyncAdapterType;->allowParallelSyncs()Z

    #@13d
    move-result v14

    #@13e
    .line 590
    .local v14, allowParallelSyncs:Z
    move-object/from16 v0, v44

    #@140
    iget-object v2, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@142
    check-cast v2, Landroid/content/SyncAdapterType;

    #@144
    invoke-virtual {v2}, Landroid/content/SyncAdapterType;->isAlwaysSyncable()Z

    #@147
    move-result v38

    #@148
    .line 591
    .local v38, isAlwaysSyncable:Z
    if-gez v40, :cond_15e

    #@14a
    if-eqz v38, :cond_15e

    #@14c
    .line 592
    move-object/from16 v0, p0

    #@14e
    iget-object v2, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@150
    move-object/from16 v0, v28

    #@152
    iget-object v3, v0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@154
    move-object/from16 v0, v28

    #@156
    iget v4, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@158
    const/4 v8, 0x1

    #@159
    invoke-virtual {v2, v3, v4, v6, v8}, Landroid/content/SyncStorageEngine;->setIsSyncable(Landroid/accounts/Account;ILjava/lang/String;I)V

    #@15c
    .line 593
    const/16 v40, 0x1

    #@15e
    .line 595
    :cond_15e
    if-eqz p7, :cond_162

    #@160
    if-gez v40, :cond_fe

    #@162
    .line 598
    :cond_162
    move-object/from16 v0, v44

    #@164
    iget-object v2, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@166
    check-cast v2, Landroid/content/SyncAdapterType;

    #@168
    invoke-virtual {v2}, Landroid/content/SyncAdapterType;->supportsUploading()Z

    #@16b
    move-result v2

    #@16c
    if-nez v2, :cond_170

    #@16e
    if-nez v47, :cond_fe

    #@170
    .line 603
    :cond_170
    if-ltz v40, :cond_196

    #@172
    if-nez v37, :cond_196

    #@174
    if-eqz v31, :cond_1c9

    #@176
    move-object/from16 v0, p0

    #@178
    iget-object v2, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@17a
    move-object/from16 v0, v28

    #@17c
    iget v3, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@17e
    invoke-virtual {v2, v3}, Landroid/content/SyncStorageEngine;->getMasterSyncAutomatically(I)Z

    #@181
    move-result v2

    #@182
    if-eqz v2, :cond_1c9

    #@184
    move-object/from16 v0, p0

    #@186
    iget-object v2, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@188
    move-object/from16 v0, v28

    #@18a
    iget-object v3, v0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@18c
    move-object/from16 v0, v28

    #@18e
    iget v4, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@190
    invoke-virtual {v2, v3, v4, v6}, Landroid/content/SyncStorageEngine;->getSyncAutomatically(Landroid/accounts/Account;ILjava/lang/String;)Z

    #@193
    move-result v2

    #@194
    if-eqz v2, :cond_1c9

    #@196
    :cond_196
    const/16 v45, 0x1

    #@198
    .line 610
    .local v45, syncAllowed:Z
    :goto_198
    if-nez v45, :cond_1cc

    #@19a
    .line 611
    if-eqz v39, :cond_fe

    #@19c
    .line 612
    const-string v2, "SyncManager"

    #@19e
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a3
    const-string/jumbo v4, "scheduleSync: sync of "

    #@1a6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a9
    move-result-object v3

    #@1aa
    move-object/from16 v0, v28

    #@1ac
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v3

    #@1b0
    const-string v4, ", "

    #@1b2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v3

    #@1b6
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v3

    #@1ba
    const-string v4, " is not allowed, dropping request"

    #@1bc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v3

    #@1c0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c3
    move-result-object v3

    #@1c4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c7
    goto/16 :goto_fe

    #@1c9
    .line 603
    .end local v45           #syncAllowed:Z
    :cond_1c9
    const/16 v45, 0x0

    #@1cb
    goto :goto_198

    #@1cc
    .line 618
    .restart local v45       #syncAllowed:Z
    :cond_1cc
    move-object/from16 v0, p0

    #@1ce
    iget-object v2, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@1d0
    move-object/from16 v0, v28

    #@1d2
    iget-object v3, v0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@1d4
    move-object/from16 v0, v28

    #@1d6
    iget v4, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@1d8
    invoke-virtual {v2, v3, v4, v6}, Landroid/content/SyncStorageEngine;->getBackoff(Landroid/accounts/Account;ILjava/lang/String;)Landroid/util/Pair;

    #@1db
    move-result-object v32

    #@1dc
    .line 620
    .local v32, backoff:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    #@1de
    iget-object v2, v0, Landroid/content/SyncManager;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@1e0
    move-object/from16 v0, v28

    #@1e2
    iget-object v3, v0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@1e4
    move-object/from16 v0, v28

    #@1e6
    iget v4, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@1e8
    invoke-virtual {v2, v3, v4, v6}, Landroid/content/SyncStorageEngine;->getDelayUntilTime(Landroid/accounts/Account;ILjava/lang/String;)J

    #@1eb
    move-result-wide v12

    #@1ec
    .line 622
    .local v12, delayUntil:J
    if-eqz v32, :cond_2d1

    #@1ee
    move-object/from16 v0, v32

    #@1f0
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@1f2
    check-cast v2, Ljava/lang/Long;

    #@1f4
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@1f7
    move-result-wide v10

    #@1f8
    .line 623
    .local v10, backoffTime:J
    :goto_1f8
    if-gez v40, :cond_260

    #@1fa
    .line 624
    new-instance v7, Landroid/os/Bundle;

    #@1fc
    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    #@1ff
    .line 625
    .local v7, newExtras:Landroid/os/Bundle;
    const-string v2, "initialize"

    #@201
    const/4 v3, 0x1

    #@202
    invoke-virtual {v7, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@205
    .line 626
    if-eqz v39, :cond_24c

    #@207
    .line 627
    const-string v2, "SyncManager"

    #@209
    new-instance v3, Ljava/lang/StringBuilder;

    #@20b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20e
    const-string/jumbo v4, "scheduleSync: delay "

    #@211
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v3

    #@215
    move-wide/from16 v0, p5

    #@217
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v3

    #@21b
    const-string v4, ", source "

    #@21d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v3

    #@221
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@224
    move-result-object v3

    #@225
    const-string v4, ", account "

    #@227
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v3

    #@22b
    move-object/from16 v0, v28

    #@22d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@230
    move-result-object v3

    #@231
    const-string v4, ", authority "

    #@233
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@236
    move-result-object v3

    #@237
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v3

    #@23b
    const-string v4, ", extras "

    #@23d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@240
    move-result-object v3

    #@241
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@244
    move-result-object v3

    #@245
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@248
    move-result-object v3

    #@249
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@24c
    .line 634
    :cond_24c
    new-instance v2, Landroid/content/SyncOperation;

    #@24e
    move-object/from16 v0, v28

    #@250
    iget-object v3, v0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@252
    move-object/from16 v0, v28

    #@254
    iget v4, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@256
    const-wide/16 v8, 0x0

    #@258
    invoke-direct/range {v2 .. v14}, Landroid/content/SyncOperation;-><init>(Landroid/accounts/Account;IILjava/lang/String;Landroid/os/Bundle;JJJZ)V

    #@25b
    move-object/from16 v0, p0

    #@25d
    invoke-virtual {v0, v2}, Landroid/content/SyncManager;->scheduleSyncOperation(Landroid/content/SyncOperation;)V

    #@260
    .line 638
    .end local v7           #newExtras:Landroid/os/Bundle;
    :cond_260
    if-nez p7, :cond_fe

    #@262
    .line 639
    if-eqz v39, :cond_2ab

    #@264
    .line 640
    const-string v2, "SyncManager"

    #@266
    new-instance v3, Ljava/lang/StringBuilder;

    #@268
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@26b
    const-string/jumbo v4, "scheduleSync: delay "

    #@26e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@271
    move-result-object v3

    #@272
    move-wide/from16 v0, p5

    #@274
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@277
    move-result-object v3

    #@278
    const-string v4, ", source "

    #@27a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27d
    move-result-object v3

    #@27e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@281
    move-result-object v3

    #@282
    const-string v4, ", account "

    #@284
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@287
    move-result-object v3

    #@288
    move-object/from16 v0, v28

    #@28a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v3

    #@28e
    const-string v4, ", authority "

    #@290
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@293
    move-result-object v3

    #@294
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@297
    move-result-object v3

    #@298
    const-string v4, ", extras "

    #@29a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29d
    move-result-object v3

    #@29e
    move-object/from16 v0, p4

    #@2a0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a3
    move-result-object v3

    #@2a4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a7
    move-result-object v3

    #@2a8
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2ab
    .line 647
    :cond_2ab
    new-instance v15, Landroid/content/SyncOperation;

    #@2ad
    move-object/from16 v0, v28

    #@2af
    iget-object v0, v0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@2b1
    move-object/from16 v16, v0

    #@2b3
    move-object/from16 v0, v28

    #@2b5
    iget v0, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@2b7
    move/from16 v17, v0

    #@2b9
    move/from16 v18, v5

    #@2bb
    move-object/from16 v19, v6

    #@2bd
    move-object/from16 v20, p4

    #@2bf
    move-wide/from16 v21, p5

    #@2c1
    move-wide/from16 v23, v10

    #@2c3
    move-wide/from16 v25, v12

    #@2c5
    move/from16 v27, v14

    #@2c7
    invoke-direct/range {v15 .. v27}, Landroid/content/SyncOperation;-><init>(Landroid/accounts/Account;IILjava/lang/String;Landroid/os/Bundle;JJJZ)V

    #@2ca
    move-object/from16 v0, p0

    #@2cc
    invoke-virtual {v0, v15}, Landroid/content/SyncManager;->scheduleSyncOperation(Landroid/content/SyncOperation;)V

    #@2cf
    goto/16 :goto_fe

    #@2d1
    .line 622
    .end local v10           #backoffTime:J
    :cond_2d1
    const-wide/16 v10, 0x0

    #@2d3
    goto/16 :goto_1f8

    #@2d5
    .line 559
    .end local v6           #authority:Ljava/lang/String;
    .end local v12           #delayUntil:J
    .end local v14           #allowParallelSyncs:Z
    .end local v32           #backoff:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    .end local v38           #isAlwaysSyncable:Z
    .end local v40           #isSyncable:I
    .end local v44           #syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    .end local v45           #syncAllowed:Z
    :cond_2d5
    add-int/lit8 v35, v36, 0x1

    #@2d7
    .local v35, i$:I
    move/from16 v36, v35

    #@2d9
    .end local v35           #i$:I
    .restart local v36       #i$:I
    goto/16 :goto_89
.end method

.method public scheduleSyncOperation(Landroid/content/SyncOperation;)V
    .registers 6
    .parameter "syncOperation"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 809
    iget-object v2, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@3
    monitor-enter v2

    #@4
    .line 810
    :try_start_4
    iget-object v1, p0, Landroid/content/SyncManager;->mSyncQueue:Landroid/content/SyncQueue;

    #@6
    invoke-virtual {v1, p1}, Landroid/content/SyncQueue;->add(Landroid/content/SyncOperation;)Z

    #@9
    move-result v0

    #@a
    .line 811
    .local v0, queueChanged:Z
    monitor-exit v2
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_32

    #@b
    .line 813
    if-eqz v0, :cond_35

    #@d
    .line 814
    const-string v1, "SyncManager"

    #@f
    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_2e

    #@15
    .line 815
    const-string v1, "SyncManager"

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string/jumbo v3, "scheduleSyncOperation: enqueued "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 817
    :cond_2e
    invoke-direct {p0}, Landroid/content/SyncManager;->sendCheckAlarmsMessage()V

    #@31
    .line 824
    :cond_31
    :goto_31
    return-void

    #@32
    .line 811
    .end local v0           #queueChanged:Z
    :catchall_32
    move-exception v1

    #@33
    :try_start_33
    monitor-exit v2
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    #@34
    throw v1

    #@35
    .line 819
    .restart local v0       #queueChanged:Z
    :cond_35
    const-string v1, "SyncManager"

    #@37
    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@3a
    move-result v1

    #@3b
    if-eqz v1, :cond_31

    #@3d
    .line 820
    const-string v1, "SyncManager"

    #@3f
    new-instance v2, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string/jumbo v3, "scheduleSyncOperation: dropping duplicate sync operation "

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_31
.end method

.method public updateRunningAccounts()V
    .registers 6

    #@0
    .prologue
    .line 247
    invoke-static {}, Landroid/accounts/AccountManagerService;->getSingleton()Landroid/accounts/AccountManagerService;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Landroid/accounts/AccountManagerService;->getRunningAccounts()[Landroid/accounts/AccountAndUser;

    #@7
    move-result-object v2

    #@8
    iput-object v2, p0, Landroid/content/SyncManager;->mRunningAccounts:[Landroid/accounts/AccountAndUser;

    #@a
    .line 249
    iget-boolean v2, p0, Landroid/content/SyncManager;->mBootCompleted:Z

    #@c
    if-eqz v2, :cond_11

    #@e
    .line 250
    invoke-direct {p0}, Landroid/content/SyncManager;->doDatabaseCleanup()V

    #@11
    .line 253
    :cond_11
    iget-object v2, p0, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v1

    #@17
    .local v1, i$:Ljava/util/Iterator;
    :cond_17
    :goto_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_3f

    #@1d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/content/SyncManager$ActiveSyncContext;

    #@23
    .line 254
    .local v0, currentSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    iget-object v2, p0, Landroid/content/SyncManager;->mRunningAccounts:[Landroid/accounts/AccountAndUser;

    #@25
    iget-object v3, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@27
    iget-object v3, v3, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@29
    iget-object v4, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@2b
    iget v4, v4, Landroid/content/SyncOperation;->userId:I

    #@2d
    invoke-direct {p0, v2, v3, v4}, Landroid/content/SyncManager;->containsAccountAndUser([Landroid/accounts/AccountAndUser;Landroid/accounts/Account;I)Z

    #@30
    move-result v2

    #@31
    if-nez v2, :cond_17

    #@33
    .line 257
    const-string v2, "SyncManager"

    #@35
    const-string v3, "canceling sync since the account is no longer running"

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 258
    const/4 v2, 0x0

    #@3b
    invoke-direct {p0, v0, v2}, Landroid/content/SyncManager;->sendSyncFinishedOrCanceledMessage(Landroid/content/SyncManager$ActiveSyncContext;Landroid/content/SyncResult;)V

    #@3e
    goto :goto_17

    #@3f
    .line 265
    .end local v0           #currentSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    :cond_3f
    invoke-direct {p0}, Landroid/content/SyncManager;->sendCheckAlarmsMessage()V

    #@42
    .line 266
    return-void
.end method
