.class public final Landroid/content/ContentValues;
.super Ljava/lang/Object;
.source "ContentValues.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field

.field public static final TAG:Ljava/lang/String; = "ContentValues"


# instance fields
.field private mValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 460
    new-instance v0, Landroid/content/ContentValues$1;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/ContentValues;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 44
    new-instance v0, Ljava/util/HashMap;

    #@5
    const/16 v1, 0x8

    #@7
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    #@a
    iput-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@c
    .line 45
    return-void
.end method

.method public constructor <init>(I)V
    .registers 4
    .parameter "size"

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    new-instance v0, Ljava/util/HashMap;

    #@5
    const/high16 v1, 0x3f80

    #@7
    invoke-direct {v0, p1, v1}, Ljava/util/HashMap;-><init>(IF)V

    #@a
    iput-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@c
    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/ContentValues;)V
    .registers 4
    .parameter "from"

    #@0
    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 62
    new-instance v0, Ljava/util/HashMap;

    #@5
    iget-object v1, p1, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@7
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@a
    iput-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@c
    .line 63
    return-void
.end method

.method private constructor <init>(Ljava/util/HashMap;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 72
    .local p1, values:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    iput-object p1, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@5
    .line 74
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/HashMap;Landroid/content/ContentValues$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/content/ContentValues;-><init>(Ljava/util/HashMap;)V

    #@3
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 219
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 220
    return-void
.end method

.method public containsKey(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 229
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 475
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "object"

    #@0
    .prologue
    .line 78
    instance-of v0, p1, Landroid/content/ContentValues;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 79
    const/4 v0, 0x0

    #@5
    .line 81
    .end local p1
    :goto_5
    return v0

    #@6
    .restart local p1
    :cond_6
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@8
    check-cast p1, Landroid/content/ContentValues;

    #@a
    .end local p1
    iget-object v1, p1, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@c
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    goto :goto_5
.end method

.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 240
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;
    .registers 7
    .parameter "key"

    #@0
    .prologue
    .line 411
    iget-object v2, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    .line 413
    .local v1, value:Ljava/lang/Object;
    :try_start_6
    check-cast v1, Ljava/lang/Boolean;
    :try_end_8
    .catch Ljava/lang/ClassCastException; {:try_start_6 .. :try_end_8} :catch_9

    #@8
    .line 421
    .end local v1           #value:Ljava/lang/Object;
    :goto_8
    return-object v1

    #@9
    .line 414
    .restart local v1       #value:Ljava/lang/Object;
    :catch_9
    move-exception v0

    #@a
    .line 415
    .local v0, e:Ljava/lang/ClassCastException;
    instance-of v2, v1, Ljava/lang/CharSequence;

    #@c
    if-eqz v2, :cond_17

    #@e
    .line 416
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    #@15
    move-result-object v1

    #@16
    goto :goto_8

    #@17
    .line 417
    :cond_17
    instance-of v2, v1, Ljava/lang/Number;

    #@19
    if-eqz v2, :cond_2b

    #@1b
    .line 418
    check-cast v1, Ljava/lang/Number;

    #@1d
    .end local v1           #value:Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_29

    #@23
    const/4 v2, 0x1

    #@24
    :goto_24
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@27
    move-result-object v1

    #@28
    goto :goto_8

    #@29
    :cond_29
    const/4 v2, 0x0

    #@2a
    goto :goto_24

    #@2b
    .line 420
    .restart local v1       #value:Ljava/lang/Object;
    :cond_2b
    const-string v2, "ContentValues"

    #@2d
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v4, "Cannot cast value for "

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    const-string v4, " to a Boolean: "

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4d
    .line 421
    const/4 v1, 0x0

    #@4e
    goto :goto_8
.end method

.method public getAsByte(Ljava/lang/String;)Ljava/lang/Byte;
    .registers 10
    .parameter "key"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 336
    iget-object v4, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@3
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v3

    #@7
    .line 338
    .local v3, value:Ljava/lang/Object;
    if-eqz v3, :cond_17

    #@9
    :try_start_9
    move-object v0, v3

    #@a
    check-cast v0, Ljava/lang/Number;

    #@c
    move-object v4, v0

    #@d
    invoke-virtual {v4}, Ljava/lang/Number;->byteValue()B

    #@10
    move-result v4

    #@11
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_14} :catch_19

    #@14
    move-result-object v4

    #@15
    :goto_15
    move-object v5, v4

    #@16
    .line 349
    :goto_16
    return-object v5

    #@17
    :cond_17
    move-object v4, v5

    #@18
    .line 338
    goto :goto_15

    #@19
    .line 339
    :catch_19
    move-exception v1

    #@1a
    .line 340
    .local v1, e:Ljava/lang/ClassCastException;
    instance-of v4, v3, Ljava/lang/CharSequence;

    #@1c
    if-eqz v4, :cond_4b

    #@1e
    .line 342
    :try_start_1e
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(Ljava/lang/String;)Ljava/lang/Byte;
    :try_end_25
    .catch Ljava/lang/NumberFormatException; {:try_start_1e .. :try_end_25} :catch_27

    #@25
    move-result-object v5

    #@26
    goto :goto_16

    #@27
    .line 343
    :catch_27
    move-exception v2

    #@28
    .line 344
    .local v2, e2:Ljava/lang/NumberFormatException;
    const-string v4, "ContentValues"

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "Cannot parse Byte value for "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    const-string v7, " at key "

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_16

    #@4b
    .line 348
    .end local v2           #e2:Ljava/lang/NumberFormatException;
    :cond_4b
    const-string v4, "ContentValues"

    #@4d
    new-instance v6, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v7, "Cannot cast value for "

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    const-string v7, " to a Byte: "

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-static {v4, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d
    goto :goto_16
.end method

.method public getAsByteArray(Ljava/lang/String;)[B
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 434
    iget-object v1, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    .line 435
    .local v0, value:Ljava/lang/Object;
    instance-of v1, v0, [B

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 436
    check-cast v0, [B

    #@c
    .end local v0           #value:Ljava/lang/Object;
    check-cast v0, [B

    #@e
    .line 438
    :goto_e
    return-object v0

    #@f
    .restart local v0       #value:Ljava/lang/Object;
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public getAsDouble(Ljava/lang/String;)Ljava/lang/Double;
    .registers 10
    .parameter "key"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 361
    iget-object v4, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@3
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v3

    #@7
    .line 363
    .local v3, value:Ljava/lang/Object;
    if-eqz v3, :cond_17

    #@9
    :try_start_9
    move-object v0, v3

    #@a
    check-cast v0, Ljava/lang/Number;

    #@c
    move-object v4, v0

    #@d
    invoke-virtual {v4}, Ljava/lang/Number;->doubleValue()D

    #@10
    move-result-wide v6

    #@11
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_14} :catch_19

    #@14
    move-result-object v4

    #@15
    :goto_15
    move-object v5, v4

    #@16
    .line 374
    :goto_16
    return-object v5

    #@17
    :cond_17
    move-object v4, v5

    #@18
    .line 363
    goto :goto_15

    #@19
    .line 364
    :catch_19
    move-exception v1

    #@1a
    .line 365
    .local v1, e:Ljava/lang/ClassCastException;
    instance-of v4, v3, Ljava/lang/CharSequence;

    #@1c
    if-eqz v4, :cond_4b

    #@1e
    .line 367
    :try_start_1e
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v4}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;
    :try_end_25
    .catch Ljava/lang/NumberFormatException; {:try_start_1e .. :try_end_25} :catch_27

    #@25
    move-result-object v5

    #@26
    goto :goto_16

    #@27
    .line 368
    :catch_27
    move-exception v2

    #@28
    .line 369
    .local v2, e2:Ljava/lang/NumberFormatException;
    const-string v4, "ContentValues"

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "Cannot parse Double value for "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    const-string v7, " at key "

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_16

    #@4b
    .line 373
    .end local v2           #e2:Ljava/lang/NumberFormatException;
    :cond_4b
    const-string v4, "ContentValues"

    #@4d
    new-instance v6, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v7, "Cannot cast value for "

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    const-string v7, " to a Double: "

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-static {v4, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d
    goto :goto_16
.end method

.method public getAsFloat(Ljava/lang/String;)Ljava/lang/Float;
    .registers 10
    .parameter "key"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 386
    iget-object v4, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@3
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v3

    #@7
    .line 388
    .local v3, value:Ljava/lang/Object;
    if-eqz v3, :cond_17

    #@9
    :try_start_9
    move-object v0, v3

    #@a
    check-cast v0, Ljava/lang/Number;

    #@c
    move-object v4, v0

    #@d
    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    #@10
    move-result v4

    #@11
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_14} :catch_19

    #@14
    move-result-object v4

    #@15
    :goto_15
    move-object v5, v4

    #@16
    .line 399
    :goto_16
    return-object v5

    #@17
    :cond_17
    move-object v4, v5

    #@18
    .line 388
    goto :goto_15

    #@19
    .line 389
    :catch_19
    move-exception v1

    #@1a
    .line 390
    .local v1, e:Ljava/lang/ClassCastException;
    instance-of v4, v3, Ljava/lang/CharSequence;

    #@1c
    if-eqz v4, :cond_4b

    #@1e
    .line 392
    :try_start_1e
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v4}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;
    :try_end_25
    .catch Ljava/lang/NumberFormatException; {:try_start_1e .. :try_end_25} :catch_27

    #@25
    move-result-object v5

    #@26
    goto :goto_16

    #@27
    .line 393
    :catch_27
    move-exception v2

    #@28
    .line 394
    .local v2, e2:Ljava/lang/NumberFormatException;
    const-string v4, "ContentValues"

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "Cannot parse Float value for "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    const-string v7, " at key "

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_16

    #@4b
    .line 398
    .end local v2           #e2:Ljava/lang/NumberFormatException;
    :cond_4b
    const-string v4, "ContentValues"

    #@4d
    new-instance v6, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v7, "Cannot cast value for "

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    const-string v7, " to a Float: "

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-static {v4, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d
    goto :goto_16
.end method

.method public getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;
    .registers 10
    .parameter "key"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 286
    iget-object v4, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@3
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v3

    #@7
    .line 288
    .local v3, value:Ljava/lang/Object;
    if-eqz v3, :cond_17

    #@9
    :try_start_9
    move-object v0, v3

    #@a
    check-cast v0, Ljava/lang/Number;

    #@c
    move-object v4, v0

    #@d
    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    #@10
    move-result v4

    #@11
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_14} :catch_19

    #@14
    move-result-object v4

    #@15
    :goto_15
    move-object v5, v4

    #@16
    .line 299
    :goto_16
    return-object v5

    #@17
    :cond_17
    move-object v4, v5

    #@18
    .line 288
    goto :goto_15

    #@19
    .line 289
    :catch_19
    move-exception v1

    #@1a
    .line 290
    .local v1, e:Ljava/lang/ClassCastException;
    instance-of v4, v3, Ljava/lang/CharSequence;

    #@1c
    if-eqz v4, :cond_4b

    #@1e
    .line 292
    :try_start_1e
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_25
    .catch Ljava/lang/NumberFormatException; {:try_start_1e .. :try_end_25} :catch_27

    #@25
    move-result-object v5

    #@26
    goto :goto_16

    #@27
    .line 293
    :catch_27
    move-exception v2

    #@28
    .line 294
    .local v2, e2:Ljava/lang/NumberFormatException;
    const-string v4, "ContentValues"

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "Cannot parse Integer value for "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    const-string v7, " at key "

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_16

    #@4b
    .line 298
    .end local v2           #e2:Ljava/lang/NumberFormatException;
    :cond_4b
    const-string v4, "ContentValues"

    #@4d
    new-instance v6, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v7, "Cannot cast value for "

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    const-string v7, " to a Integer: "

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-static {v4, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d
    goto :goto_16
.end method

.method public getAsLong(Ljava/lang/String;)Ljava/lang/Long;
    .registers 10
    .parameter "key"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 261
    iget-object v4, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@3
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v3

    #@7
    .line 263
    .local v3, value:Ljava/lang/Object;
    if-eqz v3, :cond_17

    #@9
    :try_start_9
    move-object v0, v3

    #@a
    check-cast v0, Ljava/lang/Number;

    #@c
    move-object v4, v0

    #@d
    invoke-virtual {v4}, Ljava/lang/Number;->longValue()J

    #@10
    move-result-wide v6

    #@11
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_14} :catch_19

    #@14
    move-result-object v4

    #@15
    :goto_15
    move-object v5, v4

    #@16
    .line 274
    :goto_16
    return-object v5

    #@17
    :cond_17
    move-object v4, v5

    #@18
    .line 263
    goto :goto_15

    #@19
    .line 264
    :catch_19
    move-exception v1

    #@1a
    .line 265
    .local v1, e:Ljava/lang/ClassCastException;
    instance-of v4, v3, Ljava/lang/CharSequence;

    #@1c
    if-eqz v4, :cond_4b

    #@1e
    .line 267
    :try_start_1e
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_25
    .catch Ljava/lang/NumberFormatException; {:try_start_1e .. :try_end_25} :catch_27

    #@25
    move-result-object v5

    #@26
    goto :goto_16

    #@27
    .line 268
    :catch_27
    move-exception v2

    #@28
    .line 269
    .local v2, e2:Ljava/lang/NumberFormatException;
    const-string v4, "ContentValues"

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "Cannot parse Long value for "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    const-string v7, " at key "

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_16

    #@4b
    .line 273
    .end local v2           #e2:Ljava/lang/NumberFormatException;
    :cond_4b
    const-string v4, "ContentValues"

    #@4d
    new-instance v6, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v7, "Cannot cast value for "

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    const-string v7, " to a Long: "

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-static {v4, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d
    goto :goto_16
.end method

.method public getAsShort(Ljava/lang/String;)Ljava/lang/Short;
    .registers 10
    .parameter "key"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 311
    iget-object v4, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@3
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v3

    #@7
    .line 313
    .local v3, value:Ljava/lang/Object;
    if-eqz v3, :cond_17

    #@9
    :try_start_9
    move-object v0, v3

    #@a
    check-cast v0, Ljava/lang/Number;

    #@c
    move-object v4, v0

    #@d
    invoke-virtual {v4}, Ljava/lang/Number;->shortValue()S

    #@10
    move-result v4

    #@11
    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_14} :catch_19

    #@14
    move-result-object v4

    #@15
    :goto_15
    move-object v5, v4

    #@16
    .line 324
    :goto_16
    return-object v5

    #@17
    :cond_17
    move-object v4, v5

    #@18
    .line 313
    goto :goto_15

    #@19
    .line 314
    :catch_19
    move-exception v1

    #@1a
    .line 315
    .local v1, e:Ljava/lang/ClassCastException;
    instance-of v4, v3, Ljava/lang/CharSequence;

    #@1c
    if-eqz v4, :cond_4b

    #@1e
    .line 317
    :try_start_1e
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v4}, Ljava/lang/Short;->valueOf(Ljava/lang/String;)Ljava/lang/Short;
    :try_end_25
    .catch Ljava/lang/NumberFormatException; {:try_start_1e .. :try_end_25} :catch_27

    #@25
    move-result-object v5

    #@26
    goto :goto_16

    #@27
    .line 318
    :catch_27
    move-exception v2

    #@28
    .line 319
    .local v2, e2:Ljava/lang/NumberFormatException;
    const-string v4, "ContentValues"

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "Cannot parse Short value for "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    const-string v7, " at key "

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_16

    #@4b
    .line 323
    .end local v2           #e2:Ljava/lang/NumberFormatException;
    :cond_4b
    const-string v4, "ContentValues"

    #@4d
    new-instance v6, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v7, "Cannot cast value for "

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    const-string v7, " to a Short: "

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-static {v4, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d
    goto :goto_16
.end method

.method public getAsString(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 250
    iget-object v1, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    .line 251
    .local v0, value:Ljava/lang/Object;
    if-eqz v0, :cond_d

    #@8
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method public getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 3
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 499
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/util/ArrayList;

    #@8
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public keySet()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 457
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public put(Ljava/lang/String;Ljava/lang/Boolean;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 175
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 176
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/Byte;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 116
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/Double;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 166
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/Float;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 155
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 156
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/Integer;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 136
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/Long;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 145
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 146
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/Short;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 126
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 97
    return-void
.end method

.method public put(Ljava/lang/String;[B)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 185
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 186
    return-void
.end method

.method public putAll(Landroid/content/ContentValues;)V
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 105
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    iget-object v1, p1, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@4
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    #@7
    .line 106
    return-void
.end method

.method public putNull(Ljava/lang/String;)V
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    .line 195
    return-void
.end method

.method public putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .registers 4
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 489
    .local p2, value:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 490
    return-void
.end method

.method public remove(Ljava/lang/String;)V
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 212
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 213
    return-void
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 508
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 509
    .local v2, sb:Ljava/lang/StringBuilder;
    iget-object v4, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@7
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@a
    move-result-object v4

    #@b
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v0

    #@f
    .local v0, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_45

    #@15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Ljava/lang/String;

    #@1b
    .line 510
    .local v1, name:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    .line 511
    .local v3, value:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    #@22
    move-result v4

    #@23
    if-lez v4, :cond_2a

    #@25
    const-string v4, " "

    #@27
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 512
    :cond_2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, "="

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    goto :goto_f

    #@45
    .line 514
    .end local v1           #name:Ljava/lang/String;
    .end local v3           #value:Ljava/lang/String;
    :cond_45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    return-object v4
.end method

.method public valueSet()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 448
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 480
    iget-object v0, p0, Landroid/content/ContentValues;->mValues:Ljava/util/HashMap;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    #@5
    .line 481
    return-void
.end method
