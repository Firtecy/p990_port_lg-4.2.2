.class Landroid/content/SyncManager$SyncHandler;
.super Landroid/os/Handler;
.source "SyncManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/SyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SyncHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;
    }
.end annotation


# static fields
.field private static final MESSAGE_CANCEL:I = 0x6

.field private static final MESSAGE_CHECK_ALARMS:I = 0x3

.field private static final MESSAGE_SERVICE_CONNECTED:I = 0x4

.field private static final MESSAGE_SERVICE_DISCONNECTED:I = 0x5

.field private static final MESSAGE_SYNC_ALARM:I = 0x2

.field private static final MESSAGE_SYNC_FINISHED:I = 0x1


# instance fields
.field private mAlarmScheduleTime:Ljava/lang/Long;

.field private volatile mReadyToRunLatch:Ljava/util/concurrent/CountDownLatch;

.field public final mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

.field public final mSyncTimeTracker:Landroid/content/SyncManager$SyncTimeTracker;

.field private final mWakeLocks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/PowerManager$WakeLock;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/content/SyncManager;


# direct methods
.method public constructor <init>(Landroid/content/SyncManager;Landroid/os/Looper;)V
    .registers 6
    .parameter
    .parameter "looper"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1692
    iput-object p1, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@3
    .line 1693
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@6
    .line 1625
    new-instance v0, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@8
    invoke-direct {v0, p0}, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;-><init>(Landroid/content/SyncManager$SyncHandler;)V

    #@b
    iput-object v0, p0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@d
    .line 1626
    iput-object v2, p0, Landroid/content/SyncManager$SyncHandler;->mAlarmScheduleTime:Ljava/lang/Long;

    #@f
    .line 1627
    new-instance v0, Landroid/content/SyncManager$SyncTimeTracker;

    #@11
    iget-object v1, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@13
    invoke-direct {v0, v1, v2}, Landroid/content/SyncManager$SyncTimeTracker;-><init>(Landroid/content/SyncManager;Landroid/content/SyncManager$1;)V

    #@16
    iput-object v0, p0, Landroid/content/SyncManager$SyncHandler;->mSyncTimeTracker:Landroid/content/SyncManager$SyncTimeTracker;

    #@18
    .line 1628
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/content/SyncManager$SyncHandler;->mWakeLocks:Ljava/util/HashMap;

    #@1e
    .line 1631
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    #@20
    const/4 v1, 0x1

    #@21
    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@24
    iput-object v0, p0, Landroid/content/SyncManager$SyncHandler;->mReadyToRunLatch:Ljava/util/concurrent/CountDownLatch;

    #@26
    .line 1694
    return-void
.end method

.method static synthetic access$1300(Landroid/content/SyncManager$SyncHandler;Landroid/accounts/Account;Ljava/lang/String;)Landroid/os/PowerManager$WakeLock;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 1616
    invoke-direct {p0, p1, p2}, Landroid/content/SyncManager$SyncHandler;->getSyncWakeLock(Landroid/accounts/Account;Ljava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1600(Landroid/content/SyncManager$SyncHandler;)Ljava/lang/Long;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1616
    iget-object v0, p0, Landroid/content/SyncManager$SyncHandler;->mAlarmScheduleTime:Ljava/lang/Long;

    #@2
    return-object v0
.end method

.method private cancelActiveSyncLocked(Landroid/accounts/Account;ILjava/lang/String;)V
    .registers 8
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    .line 2228
    new-instance v1, Ljava/util/ArrayList;

    #@2
    iget-object v3, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@4
    iget-object v3, v3, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@6
    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@9
    .line 2230
    .local v1, activeSyncs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncManager$ActiveSyncContext;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v2

    #@d
    .local v2, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_41

    #@13
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/content/SyncManager$ActiveSyncContext;

    #@19
    .line 2231
    .local v0, activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    if-eqz v0, :cond_d

    #@1b
    .line 2233
    if-eqz p1, :cond_27

    #@1d
    .line 2234
    iget-object v3, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@1f
    iget-object v3, v3, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@21
    invoke-virtual {p1, v3}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_d

    #@27
    .line 2239
    :cond_27
    if-eqz p3, :cond_33

    #@29
    .line 2240
    iget-object v3, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@2b
    iget-object v3, v3, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@2d
    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_d

    #@33
    .line 2245
    :cond_33
    const/4 v3, -0x1

    #@34
    if-eq p2, v3, :cond_3c

    #@36
    iget-object v3, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@38
    iget v3, v3, Landroid/content/SyncOperation;->userId:I

    #@3a
    if-ne p2, v3, :cond_d

    #@3c
    .line 2249
    :cond_3c
    const/4 v3, 0x0

    #@3d
    invoke-direct {p0, v3, v0}, Landroid/content/SyncManager$SyncHandler;->runSyncFinishedOrCanceledLocked(Landroid/content/SyncResult;Landroid/content/SyncManager$ActiveSyncContext;)V

    #@40
    goto :goto_d

    #@41
    .line 2253
    .end local v0           #activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    :cond_41
    return-void
.end method

.method private closeActiveSyncContext(Landroid/content/SyncManager$ActiveSyncContext;)V
    .registers 5
    .parameter "activeSyncContext"

    #@0
    .prologue
    .line 2338
    invoke-virtual {p1}, Landroid/content/SyncManager$ActiveSyncContext;->close()V

    #@3
    .line 2339
    iget-object v0, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@5
    iget-object v0, v0, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@a
    .line 2340
    iget-object v0, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@c
    invoke-static {v0}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p1, Landroid/content/SyncManager$ActiveSyncContext;->mSyncInfo:Landroid/content/SyncInfo;

    #@12
    iget-object v2, p1, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@14
    iget v2, v2, Landroid/content/SyncOperation;->userId:I

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/content/SyncStorageEngine;->removeActiveSync(Landroid/content/SyncInfo;I)V

    #@19
    .line 2342
    return-void
.end method

.method private dispatchSyncOperation(Landroid/content/SyncOperation;)Z
    .registers 14
    .parameter "op"

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/4 v10, 0x0

    #@2
    .line 2171
    const-string v1, "SyncManager"

    #@4
    invoke-static {v1, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_61

    #@a
    .line 2172
    const-string v1, "SyncManager"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "dispatchSyncOperation: we are going to sync "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 2173
    const-string v1, "SyncManager"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string/jumbo v3, "num active syncs: "

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    iget-object v3, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@32
    iget-object v3, v3, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@37
    move-result v3

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 2174
    iget-object v1, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@45
    iget-object v1, v1, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@47
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@4a
    move-result-object v6

    #@4b
    .local v6, i$:Ljava/util/Iterator;
    :goto_4b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@4e
    move-result v1

    #@4f
    if-eqz v1, :cond_61

    #@51
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@54
    move-result-object v9

    #@55
    check-cast v9, Landroid/content/SyncManager$ActiveSyncContext;

    #@57
    .line 2175
    .local v9, syncContext:Landroid/content/SyncManager$ActiveSyncContext;
    const-string v1, "SyncManager"

    #@59
    invoke-virtual {v9}, Landroid/content/SyncManager$ActiveSyncContext;->toString()Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    goto :goto_4b

    #@61
    .line 2180
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v9           #syncContext:Landroid/content/SyncManager$ActiveSyncContext;
    :cond_61
    iget-object v1, p1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@63
    iget-object v2, p1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@65
    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@67
    invoke-static {v1, v2}, Landroid/content/SyncAdapterType;->newKey(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SyncAdapterType;

    #@6a
    move-result-object v8

    #@6b
    .line 2182
    .local v8, syncAdapterType:Landroid/content/SyncAdapterType;
    iget-object v1, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@6d
    iget-object v1, v1, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@6f
    iget v2, p1, Landroid/content/SyncOperation;->userId:I

    #@71
    invoke-virtual {v1, v8, v2}, Landroid/content/SyncAdaptersCache;->getServiceInfo(Ljava/lang/Object;I)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@74
    move-result-object v7

    #@75
    .line 2183
    .local v7, syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    if-nez v7, :cond_a6

    #@77
    .line 2184
    const-string v1, "SyncManager"

    #@79
    new-instance v2, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v3, "can\'t find a sync adapter for "

    #@80
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v2

    #@88
    const-string v3, ", removing settings for it"

    #@8a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v2

    #@8e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v2

    #@92
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 2186
    iget-object v1, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@97
    invoke-static {v1}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@9a
    move-result-object v1

    #@9b
    iget-object v2, p1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@9d
    iget v3, p1, Landroid/content/SyncOperation;->userId:I

    #@9f
    iget-object v4, p1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@a1
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/SyncStorageEngine;->removeAuthority(Landroid/accounts/Account;ILjava/lang/String;)V

    #@a4
    move v1, v10

    #@a5
    .line 2203
    :goto_a5
    return v1

    #@a6
    .line 2190
    :cond_a6
    new-instance v0, Landroid/content/SyncManager$ActiveSyncContext;

    #@a8
    iget-object v1, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@aa
    invoke-virtual {p0, p1}, Landroid/content/SyncManager$SyncHandler;->insertStartSyncEvent(Landroid/content/SyncOperation;)J

    #@ad
    move-result-wide v3

    #@ae
    iget v5, v7, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->uid:I

    #@b0
    move-object v2, p1

    #@b1
    invoke-direct/range {v0 .. v5}, Landroid/content/SyncManager$ActiveSyncContext;-><init>(Landroid/content/SyncManager;Landroid/content/SyncOperation;JI)V

    #@b4
    .line 2192
    .local v0, activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    iget-object v1, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@b6
    invoke-static {v1}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@b9
    move-result-object v1

    #@ba
    invoke-virtual {v1, v0}, Landroid/content/SyncStorageEngine;->addActiveSync(Landroid/content/SyncManager$ActiveSyncContext;)Landroid/content/SyncInfo;

    #@bd
    move-result-object v1

    #@be
    iput-object v1, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncInfo:Landroid/content/SyncInfo;

    #@c0
    .line 2193
    iget-object v1, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@c2
    iget-object v1, v1, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@c4
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c7
    .line 2194
    const-string v1, "SyncManager"

    #@c9
    invoke-static {v1, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@cc
    move-result v1

    #@cd
    if-eqz v1, :cond_e7

    #@cf
    .line 2195
    const-string v1, "SyncManager"

    #@d1
    new-instance v2, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    const-string v3, "dispatchSyncOperation: starting "

    #@d8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v2

    #@dc
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v2

    #@e0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v2

    #@e4
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e7
    .line 2197
    :cond_e7
    iget v1, p1, Landroid/content/SyncOperation;->userId:I

    #@e9
    invoke-virtual {v0, v7, v1}, Landroid/content/SyncManager$ActiveSyncContext;->bindToSyncAdapter(Landroid/content/pm/RegisteredServicesCache$ServiceInfo;I)Z

    #@ec
    move-result v1

    #@ed
    if-nez v1, :cond_10c

    #@ef
    .line 2198
    const-string v1, "SyncManager"

    #@f1
    new-instance v2, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v3, "Bind attempt failed to "

    #@f8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v2

    #@fc
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v2

    #@100
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v2

    #@104
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@107
    .line 2199
    invoke-direct {p0, v0}, Landroid/content/SyncManager$SyncHandler;->closeActiveSyncContext(Landroid/content/SyncManager$ActiveSyncContext;)V

    #@10a
    move v1, v10

    #@10b
    .line 2200
    goto :goto_a5

    #@10c
    .line 2203
    :cond_10c
    const/4 v1, 0x1

    #@10d
    goto :goto_a5
.end method

.method private getSyncWakeLock(Landroid/accounts/Account;Ljava/lang/String;)Landroid/os/PowerManager$WakeLock;
    .registers 8
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 1644
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@3
    move-result-object v2

    #@4
    .line 1645
    .local v2, wakeLockKey:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/accounts/Account;Ljava/lang/String;>;"
    iget-object v3, p0, Landroid/content/SyncManager$SyncHandler;->mWakeLocks:Ljava/util/HashMap;

    #@6
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/os/PowerManager$WakeLock;

    #@c
    .line 1646
    .local v1, wakeLock:Landroid/os/PowerManager$WakeLock;
    if-nez v1, :cond_3f

    #@e
    .line 1647
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "*sync*_"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, "_"

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    .line 1648
    .local v0, name:Ljava/lang/String;
    iget-object v3, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2d
    invoke-static {v3}, Landroid/content/SyncManager;->access$2200(Landroid/content/SyncManager;)Landroid/os/PowerManager;

    #@30
    move-result-object v3

    #@31
    const/4 v4, 0x1

    #@32
    invoke-virtual {v3, v4, v0}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@35
    move-result-object v1

    #@36
    .line 1649
    const/4 v3, 0x0

    #@37
    invoke-virtual {v1, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@3a
    .line 1650
    iget-object v3, p0, Landroid/content/SyncManager$SyncHandler;->mWakeLocks:Ljava/util/HashMap;

    #@3c
    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 1652
    .end local v0           #name:Ljava/lang/String;
    :cond_3f
    return-object v1
.end method

.method private installHandleTooManyDeletesNotification(Landroid/accounts/Account;Ljava/lang/String;JI)V
    .registers 19
    .parameter "account"
    .parameter "authority"
    .parameter "numDeletes"
    .parameter "userId"

    #@0
    .prologue
    .line 2539
    iget-object v2, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2
    invoke-static {v2}, Landroid/content/SyncManager;->access$3600(Landroid/content/SyncManager;)Landroid/app/NotificationManager;

    #@5
    move-result-object v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 2577
    :cond_8
    :goto_8
    return-void

    #@9
    .line 2541
    :cond_9
    iget-object v2, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@b
    invoke-static {v2}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@12
    move-result-object v2

    #@13
    const/4 v3, 0x0

    #@14
    invoke-virtual {v2, p2, v3}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    #@17
    move-result-object v11

    #@18
    .line 2543
    .local v11, providerInfo:Landroid/content/pm/ProviderInfo;
    if-eqz v11, :cond_8

    #@1a
    .line 2546
    iget-object v2, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@1c
    invoke-static {v2}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v11, v2}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@27
    move-result-object v8

    #@28
    .line 2548
    .local v8, authorityName:Ljava/lang/CharSequence;
    new-instance v4, Landroid/content/Intent;

    #@2a
    iget-object v2, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2c
    invoke-static {v2}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@2f
    move-result-object v2

    #@30
    const-class v3, Landroid/content/SyncActivityTooManyDeletes;

    #@32
    invoke-direct {v4, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@35
    .line 2549
    .local v4, clickIntent:Landroid/content/Intent;
    const-string v2, "account"

    #@37
    invoke-virtual {v4, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@3a
    .line 2550
    const-string v2, "authority"

    #@3c
    invoke-virtual {v4, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3f
    .line 2551
    const-string/jumbo v2, "provider"

    #@42
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@49
    .line 2552
    const-string/jumbo v2, "numDeletes"

    #@4c
    move-wide/from16 v0, p3

    #@4e
    invoke-virtual {v4, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@51
    .line 2554
    invoke-direct {p0, v4}, Landroid/content/SyncManager$SyncHandler;->isActivityAvailable(Landroid/content/Intent;)Z

    #@54
    move-result v2

    #@55
    if-nez v2, :cond_5f

    #@57
    .line 2555
    const-string v2, "SyncManager"

    #@59
    const-string v3, "No activity found to handle too many deletes."

    #@5b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    goto :goto_8

    #@5f
    .line 2559
    :cond_5f
    iget-object v2, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@61
    invoke-static {v2}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@64
    move-result-object v2

    #@65
    const/4 v3, 0x0

    #@66
    const/high16 v5, 0x1000

    #@68
    const/4 v6, 0x0

    #@69
    new-instance v7, Landroid/os/UserHandle;

    #@6b
    move/from16 v0, p5

    #@6d
    invoke-direct {v7, v0}, Landroid/os/UserHandle;-><init>(I)V

    #@70
    invoke-static/range {v2 .. v7}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@73
    move-result-object v10

    #@74
    .line 2563
    .local v10, pendingIntent:Landroid/app/PendingIntent;
    iget-object v2, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@76
    invoke-static {v2}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@79
    move-result-object v2

    #@7a
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7d
    move-result-object v2

    #@7e
    const v3, 0x10400ef

    #@81
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@84
    move-result-object v12

    #@85
    .line 2566
    .local v12, tooManyDeletesDescFormat:Ljava/lang/CharSequence;
    new-instance v9, Landroid/app/Notification;

    #@87
    const v2, 0x108052c

    #@8a
    iget-object v3, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@8c
    invoke-static {v3}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@8f
    move-result-object v3

    #@90
    const v5, 0x10400ed

    #@93
    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@96
    move-result-object v3

    #@97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9a
    move-result-wide v5

    #@9b
    invoke-direct {v9, v2, v3, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@9e
    .line 2570
    .local v9, notification:Landroid/app/Notification;
    iget-object v2, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@a0
    invoke-static {v2}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@a3
    move-result-object v2

    #@a4
    iget-object v3, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@a6
    invoke-static {v3}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@a9
    move-result-object v3

    #@aa
    const v5, 0x10400ee

    #@ad
    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@b0
    move-result-object v3

    #@b1
    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@b4
    move-result-object v5

    #@b5
    const/4 v6, 0x1

    #@b6
    new-array v6, v6, [Ljava/lang/Object;

    #@b8
    const/4 v7, 0x0

    #@b9
    aput-object v8, v6, v7

    #@bb
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@be
    move-result-object v5

    #@bf
    invoke-virtual {v9, v2, v3, v5, v10}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@c2
    .line 2574
    iget v2, v9, Landroid/app/Notification;->flags:I

    #@c4
    or-int/lit8 v2, v2, 0x2

    #@c6
    iput v2, v9, Landroid/app/Notification;->flags:I

    #@c8
    .line 2575
    iget-object v2, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@ca
    invoke-static {v2}, Landroid/content/SyncManager;->access$3600(Landroid/content/SyncManager;)Landroid/app/NotificationManager;

    #@cd
    move-result-object v2

    #@ce
    const/4 v3, 0x0

    #@cf
    invoke-virtual {p1}, Landroid/accounts/Account;->hashCode()I

    #@d2
    move-result v5

    #@d3
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    #@d6
    move-result v6

    #@d7
    xor-int/2addr v5, v6

    #@d8
    new-instance v6, Landroid/os/UserHandle;

    #@da
    move/from16 v0, p5

    #@dc
    invoke-direct {v6, v0}, Landroid/os/UserHandle;-><init>(I)V

    #@df
    invoke-virtual {v2, v3, v5, v9, v6}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@e2
    goto/16 :goto_8
.end method

.method private isActivityAvailable(Landroid/content/Intent;)Z
    .registers 9
    .parameter "intent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2586
    iget-object v6, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@3
    invoke-static {v6}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@6
    move-result-object v6

    #@7
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@a
    move-result-object v3

    #@b
    .line 2587
    .local v3, pm:Landroid/content/pm/PackageManager;
    invoke-virtual {v3, p1, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@e
    move-result-object v1

    #@f
    .line 2588
    .local v1, list:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@12
    move-result v2

    #@13
    .line 2589
    .local v2, listSize:I
    const/4 v0, 0x0

    #@14
    .local v0, i:I
    :goto_14
    if-ge v0, v2, :cond_27

    #@16
    .line 2590
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v4

    #@1a
    check-cast v4, Landroid/content/pm/ResolveInfo;

    #@1c
    .line 2591
    .local v4, resolveInfo:Landroid/content/pm/ResolveInfo;
    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@1e
    iget-object v6, v6, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@20
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->flags:I

    #@22
    and-int/lit8 v6, v6, 0x1

    #@24
    if-eqz v6, :cond_28

    #@26
    .line 2593
    const/4 v5, 0x1

    #@27
    .line 2597
    .end local v4           #resolveInfo:Landroid/content/pm/ResolveInfo;
    :cond_27
    return v5

    #@28
    .line 2589
    .restart local v4       #resolveInfo:Landroid/content/pm/ResolveInfo;
    :cond_28
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_14
.end method

.method private manageSyncAlarmLocked(JJ)V
    .registers 30
    .parameter "nextPeriodicEventElapsedTime"
    .parameter "nextPendingEventElapsedTime"

    #@0
    .prologue
    .line 2438
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@4
    move-object/from16 v19, v0

    #@6
    invoke-static/range {v19 .. v19}, Landroid/content/SyncManager;->access$400(Landroid/content/SyncManager;)Z

    #@9
    move-result v19

    #@a
    if-nez v19, :cond_d

    #@c
    .line 2527
    :cond_c
    :goto_c
    return-void

    #@d
    .line 2439
    :cond_d
    move-object/from16 v0, p0

    #@f
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@11
    move-object/from16 v19, v0

    #@13
    invoke-static/range {v19 .. v19}, Landroid/content/SyncManager;->access$000(Landroid/content/SyncManager;)Z

    #@16
    move-result v19

    #@17
    if-nez v19, :cond_c

    #@19
    .line 2442
    move-object/from16 v0, p0

    #@1b
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@1d
    move-object/from16 v19, v0

    #@1f
    invoke-static/range {v19 .. v19}, Landroid/content/SyncManager;->access$200(Landroid/content/SyncManager;)Landroid/content/SyncManager$SyncHandler;

    #@22
    move-result-object v19

    #@23
    move-object/from16 v0, v19

    #@25
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@27
    move-object/from16 v19, v0

    #@29
    move-object/from16 v0, v19

    #@2b
    iget-boolean v0, v0, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->isActive:Z

    #@2d
    move/from16 v19, v0

    #@2f
    if-nez v19, :cond_bf

    #@31
    move-object/from16 v0, p0

    #@33
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@35
    move-object/from16 v19, v0

    #@37
    invoke-static/range {v19 .. v19}, Landroid/content/SyncManager;->access$200(Landroid/content/SyncManager;)Landroid/content/SyncManager$SyncHandler;

    #@3a
    move-result-object v19

    #@3b
    move-object/from16 v0, v19

    #@3d
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@3f
    move-object/from16 v19, v0

    #@41
    move-object/from16 v0, v19

    #@43
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->startTime:Ljava/lang/Long;

    #@45
    move-object/from16 v19, v0

    #@47
    if-eqz v19, :cond_bf

    #@49
    move-object/from16 v0, p0

    #@4b
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@4d
    move-object/from16 v19, v0

    #@4f
    invoke-static/range {v19 .. v19}, Landroid/content/SyncManager;->access$200(Landroid/content/SyncManager;)Landroid/content/SyncManager$SyncHandler;

    #@52
    move-result-object v19

    #@53
    move-object/from16 v0, v19

    #@55
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@57
    move-object/from16 v19, v0

    #@59
    move-object/from16 v0, v19

    #@5b
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->startTime:Ljava/lang/Long;

    #@5d
    move-object/from16 v19, v0

    #@5f
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    #@62
    move-result-wide v19

    #@63
    invoke-static {}, Landroid/content/SyncManager;->access$3700()J

    #@66
    move-result-wide v21

    #@67
    add-long v13, v19, v21

    #@69
    .line 2449
    .local v13, notificationTime:J
    :goto_69
    const-wide v9, 0x7fffffffffffffffL

    #@6e
    .line 2450
    .local v9, earliestTimeoutTime:J
    move-object/from16 v0, p0

    #@70
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@72
    move-object/from16 v19, v0

    #@74
    move-object/from16 v0, v19

    #@76
    iget-object v0, v0, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@78
    move-object/from16 v19, v0

    #@7a
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@7d
    move-result-object v11

    #@7e
    .local v11, i$:Ljava/util/Iterator;
    :cond_7e
    :goto_7e
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@81
    move-result v19

    #@82
    if-eqz v19, :cond_c5

    #@84
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@87
    move-result-object v6

    #@88
    check-cast v6, Landroid/content/SyncManager$ActiveSyncContext;

    #@8a
    .line 2451
    .local v6, currentSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    iget-wide v0, v6, Landroid/content/SyncManager$ActiveSyncContext;->mTimeoutStartTime:J

    #@8c
    move-wide/from16 v19, v0

    #@8e
    invoke-static {}, Landroid/content/SyncManager;->access$3000()J

    #@91
    move-result-wide v21

    #@92
    add-long v7, v19, v21

    #@94
    .line 2453
    .local v7, currentSyncTimeoutTime:J
    const-string v19, "SyncManager"

    #@96
    const/16 v20, 0x2

    #@98
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@9b
    move-result v19

    #@9c
    if-eqz v19, :cond_b9

    #@9e
    .line 2454
    const-string v19, "SyncManager"

    #@a0
    new-instance v20, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string/jumbo v21, "manageSyncAlarm: active sync, mTimeoutStartTime + MAX is "

    #@a8
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v20

    #@ac
    move-object/from16 v0, v20

    #@ae
    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v20

    #@b2
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v20

    #@b6
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    .line 2457
    :cond_b9
    cmp-long v19, v9, v7

    #@bb
    if-lez v19, :cond_7e

    #@bd
    .line 2458
    move-wide v9, v7

    #@be
    goto :goto_7e

    #@bf
    .line 2442
    .end local v6           #currentSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    .end local v7           #currentSyncTimeoutTime:J
    .end local v9           #earliestTimeoutTime:J
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v13           #notificationTime:J
    :cond_bf
    const-wide v13, 0x7fffffffffffffffL

    #@c4
    goto :goto_69

    #@c5
    .line 2462
    .restart local v9       #earliestTimeoutTime:J
    .restart local v11       #i$:Ljava/util/Iterator;
    .restart local v13       #notificationTime:J
    :cond_c5
    const-string v19, "SyncManager"

    #@c7
    const/16 v20, 0x2

    #@c9
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@cc
    move-result v19

    #@cd
    if-eqz v19, :cond_ea

    #@cf
    .line 2463
    const-string v19, "SyncManager"

    #@d1
    new-instance v20, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    const-string/jumbo v21, "manageSyncAlarm: notificationTime is "

    #@d9
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v20

    #@dd
    move-object/from16 v0, v20

    #@df
    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v20

    #@e3
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v20

    #@e7
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 2466
    :cond_ea
    const-string v19, "SyncManager"

    #@ec
    const/16 v20, 0x2

    #@ee
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@f1
    move-result v19

    #@f2
    if-eqz v19, :cond_10f

    #@f4
    .line 2467
    const-string v19, "SyncManager"

    #@f6
    new-instance v20, Ljava/lang/StringBuilder;

    #@f8
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@fb
    const-string/jumbo v21, "manageSyncAlarm: earliestTimeoutTime is "

    #@fe
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v20

    #@102
    move-object/from16 v0, v20

    #@104
    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@107
    move-result-object v20

    #@108
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b
    move-result-object v20

    #@10c
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10f
    .line 2470
    :cond_10f
    const-string v19, "SyncManager"

    #@111
    const/16 v20, 0x2

    #@113
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@116
    move-result v19

    #@117
    if-eqz v19, :cond_136

    #@119
    .line 2471
    const-string v19, "SyncManager"

    #@11b
    new-instance v20, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    const-string/jumbo v21, "manageSyncAlarm: nextPeriodicEventElapsedTime is "

    #@123
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v20

    #@127
    move-object/from16 v0, v20

    #@129
    move-wide/from16 v1, p1

    #@12b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v20

    #@12f
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@132
    move-result-object v20

    #@133
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@136
    .line 2474
    :cond_136
    const-string v19, "SyncManager"

    #@138
    const/16 v20, 0x2

    #@13a
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@13d
    move-result v19

    #@13e
    if-eqz v19, :cond_15d

    #@140
    .line 2475
    const-string v19, "SyncManager"

    #@142
    new-instance v20, Ljava/lang/StringBuilder;

    #@144
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@147
    const-string/jumbo v21, "manageSyncAlarm: nextPendingEventElapsedTime is "

    #@14a
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v20

    #@14e
    move-object/from16 v0, v20

    #@150
    move-wide/from16 v1, p3

    #@152
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@155
    move-result-object v20

    #@156
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@159
    move-result-object v20

    #@15a
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@15d
    .line 2479
    :cond_15d
    invoke-static {v13, v14, v9, v10}, Ljava/lang/Math;->min(JJ)J

    #@160
    move-result-wide v4

    #@161
    .line 2480
    .local v4, alarmTime:J
    move-wide/from16 v0, p1

    #@163
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    #@166
    move-result-wide v4

    #@167
    .line 2481
    move-wide/from16 v0, p3

    #@169
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    #@16c
    move-result-wide v4

    #@16d
    .line 2484
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@170
    move-result-wide v15

    #@171
    .line 2485
    .local v15, now:J
    const-wide/16 v19, 0x7530

    #@173
    add-long v19, v19, v15

    #@175
    cmp-long v19, v4, v19

    #@177
    if-gez v19, :cond_259

    #@179
    .line 2486
    const-string v19, "SyncManager"

    #@17b
    const/16 v20, 0x2

    #@17d
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@180
    move-result v19

    #@181
    if-eqz v19, :cond_1ac

    #@183
    .line 2487
    const-string v19, "SyncManager"

    #@185
    new-instance v20, Ljava/lang/StringBuilder;

    #@187
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@18a
    const-string/jumbo v21, "manageSyncAlarm: the alarmTime is too small, "

    #@18d
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    move-result-object v20

    #@191
    move-object/from16 v0, v20

    #@193
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@196
    move-result-object v20

    #@197
    const-string v21, ", setting to "

    #@199
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v20

    #@19d
    const-wide/16 v21, 0x7530

    #@19f
    add-long v21, v21, v15

    #@1a1
    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v20

    #@1a5
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a8
    move-result-object v20

    #@1a9
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1ac
    .line 2490
    :cond_1ac
    const-wide/16 v19, 0x7530

    #@1ae
    add-long v4, v15, v19

    #@1b0
    .line 2500
    :cond_1b0
    :goto_1b0
    const/16 v18, 0x0

    #@1b2
    .line 2501
    .local v18, shouldSet:Z
    const/16 v17, 0x0

    #@1b4
    .line 2502
    .local v17, shouldCancel:Z
    move-object/from16 v0, p0

    #@1b6
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->mAlarmScheduleTime:Ljava/lang/Long;

    #@1b8
    move-object/from16 v19, v0

    #@1ba
    if-eqz v19, :cond_29c

    #@1bc
    const/4 v3, 0x1

    #@1bd
    .line 2503
    .local v3, alarmIsActive:Z
    :goto_1bd
    const-wide v19, 0x7fffffffffffffffL

    #@1c2
    cmp-long v19, v4, v19

    #@1c4
    if-eqz v19, :cond_29f

    #@1c6
    const/4 v12, 0x1

    #@1c7
    .line 2504
    .local v12, needAlarm:Z
    :goto_1c7
    if-eqz v12, :cond_2a2

    #@1c9
    .line 2505
    if-eqz v3, :cond_1d9

    #@1cb
    move-object/from16 v0, p0

    #@1cd
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->mAlarmScheduleTime:Ljava/lang/Long;

    #@1cf
    move-object/from16 v19, v0

    #@1d1
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    #@1d4
    move-result-wide v19

    #@1d5
    cmp-long v19, v4, v19

    #@1d7
    if-gez v19, :cond_1db

    #@1d9
    .line 2506
    :cond_1d9
    const/16 v18, 0x1

    #@1db
    .line 2513
    :cond_1db
    :goto_1db
    move-object/from16 v0, p0

    #@1dd
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@1df
    move-object/from16 v19, v0

    #@1e1
    invoke-static/range {v19 .. v19}, Landroid/content/SyncManager;->access$3900(Landroid/content/SyncManager;)V

    #@1e4
    .line 2514
    if-eqz v18, :cond_2a6

    #@1e6
    .line 2515
    const-string v19, "SyncManager"

    #@1e8
    const/16 v20, 0x2

    #@1ea
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@1ed
    move-result v19

    #@1ee
    if-eqz v19, :cond_22e

    #@1f0
    .line 2516
    const-string v19, "SyncManager"

    #@1f2
    new-instance v20, Ljava/lang/StringBuilder;

    #@1f4
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@1f7
    const-string/jumbo v21, "requesting that the alarm manager wake us up at elapsed time "

    #@1fa
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fd
    move-result-object v20

    #@1fe
    move-object/from16 v0, v20

    #@200
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@203
    move-result-object v20

    #@204
    const-string v21, ", now is "

    #@206
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v20

    #@20a
    move-object/from16 v0, v20

    #@20c
    move-wide v1, v15

    #@20d
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@210
    move-result-object v20

    #@211
    const-string v21, ", "

    #@213
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@216
    move-result-object v20

    #@217
    sub-long v21, v4, v15

    #@219
    const-wide/16 v23, 0x3e8

    #@21b
    div-long v21, v21, v23

    #@21d
    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@220
    move-result-object v20

    #@221
    const-string v21, " secs from now"

    #@223
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v20

    #@227
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22a
    move-result-object v20

    #@22b
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22e
    .line 2520
    :cond_22e
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@231
    move-result-object v19

    #@232
    move-object/from16 v0, v19

    #@234
    move-object/from16 v1, p0

    #@236
    iput-object v0, v1, Landroid/content/SyncManager$SyncHandler;->mAlarmScheduleTime:Ljava/lang/Long;

    #@238
    .line 2521
    move-object/from16 v0, p0

    #@23a
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@23c
    move-object/from16 v19, v0

    #@23e
    invoke-static/range {v19 .. v19}, Landroid/content/SyncManager;->access$4100(Landroid/content/SyncManager;)Landroid/app/AlarmManager;

    #@241
    move-result-object v19

    #@242
    const/16 v20, 0x2

    #@244
    move-object/from16 v0, p0

    #@246
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@248
    move-object/from16 v21, v0

    #@24a
    invoke-static/range {v21 .. v21}, Landroid/content/SyncManager;->access$4000(Landroid/content/SyncManager;)Landroid/app/PendingIntent;

    #@24d
    move-result-object v21

    #@24e
    move-object/from16 v0, v19

    #@250
    move/from16 v1, v20

    #@252
    move-object/from16 v2, v21

    #@254
    invoke-virtual {v0, v1, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@257
    goto/16 :goto_c

    #@259
    .line 2491
    .end local v3           #alarmIsActive:Z
    .end local v12           #needAlarm:Z
    .end local v17           #shouldCancel:Z
    .end local v18           #shouldSet:Z
    :cond_259
    const-wide/32 v19, 0x6ddd00

    #@25c
    add-long v19, v19, v15

    #@25e
    cmp-long v19, v4, v19

    #@260
    if-lez v19, :cond_1b0

    #@262
    .line 2492
    const-string v19, "SyncManager"

    #@264
    const/16 v20, 0x2

    #@266
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@269
    move-result v19

    #@26a
    if-eqz v19, :cond_295

    #@26c
    .line 2493
    const-string v19, "SyncManager"

    #@26e
    new-instance v20, Ljava/lang/StringBuilder;

    #@270
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@273
    const-string/jumbo v21, "manageSyncAlarm: the alarmTime is too large, "

    #@276
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@279
    move-result-object v20

    #@27a
    move-object/from16 v0, v20

    #@27c
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@27f
    move-result-object v20

    #@280
    const-string v21, ", setting to "

    #@282
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@285
    move-result-object v20

    #@286
    const-wide/16 v21, 0x7530

    #@288
    add-long v21, v21, v15

    #@28a
    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v20

    #@28e
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@291
    move-result-object v20

    #@292
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@295
    .line 2496
    :cond_295
    const-wide/32 v19, 0x6ddd00

    #@298
    add-long v4, v15, v19

    #@29a
    goto/16 :goto_1b0

    #@29c
    .line 2502
    .restart local v17       #shouldCancel:Z
    .restart local v18       #shouldSet:Z
    :cond_29c
    const/4 v3, 0x0

    #@29d
    goto/16 :goto_1bd

    #@29f
    .line 2503
    .restart local v3       #alarmIsActive:Z
    :cond_29f
    const/4 v12, 0x0

    #@2a0
    goto/16 :goto_1c7

    #@2a2
    .line 2509
    .restart local v12       #needAlarm:Z
    :cond_2a2
    move/from16 v17, v3

    #@2a4
    goto/16 :goto_1db

    #@2a6
    .line 2523
    :cond_2a6
    if-eqz v17, :cond_c

    #@2a8
    .line 2524
    const/16 v19, 0x0

    #@2aa
    move-object/from16 v0, v19

    #@2ac
    move-object/from16 v1, p0

    #@2ae
    iput-object v0, v1, Landroid/content/SyncManager$SyncHandler;->mAlarmScheduleTime:Ljava/lang/Long;

    #@2b0
    .line 2525
    move-object/from16 v0, p0

    #@2b2
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2b4
    move-object/from16 v19, v0

    #@2b6
    invoke-static/range {v19 .. v19}, Landroid/content/SyncManager;->access$4100(Landroid/content/SyncManager;)Landroid/app/AlarmManager;

    #@2b9
    move-result-object v19

    #@2ba
    move-object/from16 v0, p0

    #@2bc
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2be
    move-object/from16 v20, v0

    #@2c0
    invoke-static/range {v20 .. v20}, Landroid/content/SyncManager;->access$4000(Landroid/content/SyncManager;)Landroid/app/PendingIntent;

    #@2c3
    move-result-object v20

    #@2c4
    invoke-virtual/range {v19 .. v20}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@2c7
    goto/16 :goto_c
.end method

.method private manageSyncNotificationLocked()V
    .registers 15

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 2377
    iget-object v10, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@4
    iget-object v10, v10, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    #@9
    move-result v10

    #@a
    if-eqz v10, :cond_35

    #@c
    .line 2378
    iget-object v10, p0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@e
    const/4 v11, 0x0

    #@f
    iput-object v11, v10, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->startTime:Ljava/lang/Long;

    #@11
    .line 2382
    iget-object v10, p0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@13
    iget-boolean v5, v10, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->isActive:Z

    #@15
    .line 2383
    .local v5, shouldCancel:Z
    const/4 v6, 0x0

    #@16
    .line 2421
    :cond_16
    :goto_16
    if-eqz v5, :cond_26

    #@18
    if-nez v6, :cond_26

    #@1a
    .line 2422
    iget-object v10, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@1c
    invoke-static {v10, v9}, Landroid/content/SyncManager;->access$3802(Landroid/content/SyncManager;Z)Z

    #@1f
    .line 2423
    invoke-direct {p0}, Landroid/content/SyncManager$SyncHandler;->sendSyncStateIntent()V

    #@22
    .line 2424
    iget-object v10, p0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@24
    iput-boolean v9, v10, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->isActive:Z

    #@26
    .line 2427
    :cond_26
    if-eqz v6, :cond_34

    #@28
    .line 2428
    iget-object v9, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2a
    invoke-static {v9, v8}, Landroid/content/SyncManager;->access$3802(Landroid/content/SyncManager;Z)Z

    #@2d
    .line 2429
    invoke-direct {p0}, Landroid/content/SyncManager$SyncHandler;->sendSyncStateIntent()V

    #@30
    .line 2430
    iget-object v9, p0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@32
    iput-boolean v8, v9, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->isActive:Z

    #@34
    .line 2432
    :cond_34
    return-void

    #@35
    .line 2386
    .end local v5           #shouldCancel:Z
    :cond_35
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@38
    move-result-wide v3

    #@39
    .line 2387
    .local v3, now:J
    iget-object v10, p0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@3b
    iget-object v10, v10, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->startTime:Ljava/lang/Long;

    #@3d
    if-nez v10, :cond_47

    #@3f
    .line 2388
    iget-object v10, p0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@41
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@44
    move-result-object v11

    #@45
    iput-object v11, v10, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->startTime:Ljava/lang/Long;

    #@47
    .line 2396
    :cond_47
    iget-object v10, p0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@49
    iget-boolean v10, v10, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->isActive:Z

    #@4b
    if-eqz v10, :cond_50

    #@4d
    .line 2397
    const/4 v5, 0x0

    #@4e
    .restart local v5       #shouldCancel:Z
    move v6, v5

    #@4f
    .local v6, shouldInstall:I
    goto :goto_16

    #@50
    .line 2400
    .end local v5           #shouldCancel:Z
    .end local v6           #shouldInstall:I
    :cond_50
    const/4 v5, 0x0

    #@51
    .line 2402
    .restart local v5       #shouldCancel:Z
    iget-object v10, p0, Landroid/content/SyncManager$SyncHandler;->mSyncNotificationInfo:Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;

    #@53
    iget-object v10, v10, Landroid/content/SyncManager$SyncHandler$SyncNotificationInfo;->startTime:Ljava/lang/Long;

    #@55
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    #@58
    move-result-wide v10

    #@59
    invoke-static {}, Landroid/content/SyncManager;->access$3700()J

    #@5c
    move-result-wide v12

    #@5d
    add-long/2addr v10, v12

    #@5e
    cmp-long v10, v3, v10

    #@60
    if-lez v10, :cond_67

    #@62
    move v7, v8

    #@63
    .line 2404
    .local v7, timeToShowNotification:Z
    :goto_63
    if-eqz v7, :cond_69

    #@65
    .line 2405
    const/4 v6, 0x1

    #@66
    .local v6, shouldInstall:Z
    goto :goto_16

    #@67
    .end local v6           #shouldInstall:Z
    .end local v7           #timeToShowNotification:Z
    :cond_67
    move v7, v9

    #@68
    .line 2402
    goto :goto_63

    #@69
    .line 2408
    .restart local v7       #timeToShowNotification:Z
    :cond_69
    const/4 v6, 0x0

    #@6a
    .line 2409
    .restart local v6       #shouldInstall:Z
    iget-object v10, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@6c
    iget-object v10, v10, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@6e
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@71
    move-result-object v1

    #@72
    .local v1, i$:Ljava/util/Iterator;
    :cond_72
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@75
    move-result v10

    #@76
    if-eqz v10, :cond_16

    #@78
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7b
    move-result-object v0

    #@7c
    check-cast v0, Landroid/content/SyncManager$ActiveSyncContext;

    #@7e
    .line 2410
    .local v0, activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    iget-object v10, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@80
    iget-object v10, v10, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@82
    const-string v11, "force"

    #@84
    invoke-virtual {v10, v11, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@87
    move-result v2

    #@88
    .line 2412
    .local v2, manualSync:Z
    if-eqz v2, :cond_72

    #@8a
    .line 2413
    const/4 v6, 0x1

    #@8b
    .line 2414
    goto :goto_16
.end method

.method private maybeStartNextSyncLocked()J
    .registers 42

    #@0
    .prologue
    .line 1921
    const-string v36, "SyncManager"

    #@2
    const/16 v37, 0x2

    #@4
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@7
    move-result v15

    #@8
    .line 1922
    .local v15, isLoggable:Z
    if-eqz v15, :cond_12

    #@a
    const-string v36, "SyncManager"

    #@c
    const-string/jumbo v37, "maybeStartNextSync"

    #@f
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 1925
    :cond_12
    move-object/from16 v0, p0

    #@14
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@16
    move-object/from16 v36, v0

    #@18
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$400(Landroid/content/SyncManager;)Z

    #@1b
    move-result v36

    #@1c
    if-nez v36, :cond_2e

    #@1e
    .line 1926
    if-eqz v15, :cond_28

    #@20
    .line 1927
    const-string v36, "SyncManager"

    #@22
    const-string/jumbo v37, "maybeStartNextSync: no data connection, skipping"

    #@25
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1929
    :cond_28
    const-wide v18, 0x7fffffffffffffffL

    #@2d
    .line 2167
    :cond_2d
    :goto_2d
    return-wide v18

    #@2e
    .line 1932
    :cond_2e
    move-object/from16 v0, p0

    #@30
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@32
    move-object/from16 v36, v0

    #@34
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$000(Landroid/content/SyncManager;)Z

    #@37
    move-result v36

    #@38
    if-eqz v36, :cond_4a

    #@3a
    .line 1933
    if-eqz v15, :cond_44

    #@3c
    .line 1934
    const-string v36, "SyncManager"

    #@3e
    const-string/jumbo v37, "maybeStartNextSync: memory low, skipping"

    #@41
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 1936
    :cond_44
    const-wide v18, 0x7fffffffffffffffL

    #@49
    goto :goto_2d

    #@4a
    .line 1941
    :cond_4a
    move-object/from16 v0, p0

    #@4c
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@4e
    move-object/from16 v36, v0

    #@50
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$2500(Landroid/content/SyncManager;)[Landroid/accounts/AccountAndUser;

    #@53
    move-result-object v5

    #@54
    .line 1942
    .local v5, accounts:[Landroid/accounts/AccountAndUser;
    invoke-static {}, Landroid/content/SyncManager;->access$2800()[Landroid/accounts/AccountAndUser;

    #@57
    move-result-object v36

    #@58
    move-object/from16 v0, v36

    #@5a
    if-ne v5, v0, :cond_6c

    #@5c
    .line 1943
    if-eqz v15, :cond_66

    #@5e
    .line 1944
    const-string v36, "SyncManager"

    #@60
    const-string/jumbo v37, "maybeStartNextSync: accounts not known, skipping"

    #@63
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 1946
    :cond_66
    const-wide v18, 0x7fffffffffffffffL

    #@6b
    goto :goto_2d

    #@6c
    .line 1952
    :cond_6c
    move-object/from16 v0, p0

    #@6e
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@70
    move-object/from16 v36, v0

    #@72
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$300(Landroid/content/SyncManager;)Landroid/net/ConnectivityManager;

    #@75
    move-result-object v36

    #@76
    invoke-virtual/range {v36 .. v36}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    #@79
    move-result v9

    #@7a
    .line 1955
    .local v9, backgroundDataUsageAllowed:Z
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@7d
    move-result-wide v20

    #@7e
    .line 1958
    .local v20, now:J
    const-wide v18, 0x7fffffffffffffffL

    #@83
    .line 1961
    .local v18, nextReadyToRunTime:J
    new-instance v27, Ljava/util/ArrayList;

    #@85
    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    #@88
    .line 1962
    .local v27, operations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncOperation;>;"
    move-object/from16 v0, p0

    #@8a
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@8c
    move-object/from16 v36, v0

    #@8e
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$600(Landroid/content/SyncManager;)Landroid/content/SyncQueue;

    #@91
    move-result-object v37

    #@92
    monitor-enter v37

    #@93
    .line 1963
    if-eqz v15, :cond_c3

    #@95
    .line 1964
    :try_start_95
    const-string v36, "SyncManager"

    #@97
    new-instance v38, Ljava/lang/StringBuilder;

    #@99
    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v39, "build the operation array, syncQueue size is "

    #@9e
    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v38

    #@a2
    move-object/from16 v0, p0

    #@a4
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@a6
    move-object/from16 v39, v0

    #@a8
    invoke-static/range {v39 .. v39}, Landroid/content/SyncManager;->access$600(Landroid/content/SyncManager;)Landroid/content/SyncQueue;

    #@ab
    move-result-object v39

    #@ac
    invoke-virtual/range {v39 .. v39}, Landroid/content/SyncQueue;->getOperations()Ljava/util/Collection;

    #@af
    move-result-object v39

    #@b0
    invoke-interface/range {v39 .. v39}, Ljava/util/Collection;->size()I

    #@b3
    move-result v39

    #@b4
    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v38

    #@b8
    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v38

    #@bc
    move-object/from16 v0, v36

    #@be
    move-object/from16 v1, v38

    #@c0
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    .line 1967
    :cond_c3
    move-object/from16 v0, p0

    #@c5
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@c7
    move-object/from16 v36, v0

    #@c9
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$600(Landroid/content/SyncManager;)Landroid/content/SyncQueue;

    #@cc
    move-result-object v36

    #@cd
    invoke-virtual/range {v36 .. v36}, Landroid/content/SyncQueue;->getOperations()Ljava/util/Collection;

    #@d0
    move-result-object v36

    #@d1
    invoke-interface/range {v36 .. v36}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@d4
    move-result-object v26

    #@d5
    .line 1970
    .local v26, operationIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/SyncOperation;>;"
    move-object/from16 v0, p0

    #@d7
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@d9
    move-object/from16 v36, v0

    #@db
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@de
    move-result-object v36

    #@df
    const-string v38, "activity"

    #@e1
    move-object/from16 v0, v36

    #@e3
    move-object/from16 v1, v38

    #@e5
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e8
    move-result-object v8

    #@e9
    check-cast v8, Landroid/app/ActivityManager;

    #@eb
    .line 1972
    .local v8, activityManager:Landroid/app/ActivityManager;
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@ee
    move-result-object v28

    #@ef
    .line 1973
    .local v28, removedUsers:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_ef
    :goto_ef
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    #@f2
    move-result v36

    #@f3
    if-eqz v36, :cond_2b6

    #@f5
    .line 1974
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f8
    move-result-object v25

    #@f9
    check-cast v25, Landroid/content/SyncOperation;

    #@fb
    .line 1977
    .local v25, op:Landroid/content/SyncOperation;
    move-object/from16 v0, p0

    #@fd
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@ff
    move-object/from16 v36, v0

    #@101
    move-object/from16 v0, v25

    #@103
    iget-object v0, v0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@105
    move-object/from16 v38, v0

    #@107
    move-object/from16 v0, v25

    #@109
    iget v0, v0, Landroid/content/SyncOperation;->userId:I

    #@10b
    move/from16 v39, v0

    #@10d
    move-object/from16 v0, v36

    #@10f
    move-object/from16 v1, v38

    #@111
    move/from16 v2, v39

    #@113
    invoke-static {v0, v5, v1, v2}, Landroid/content/SyncManager;->access$2700(Landroid/content/SyncManager;[Landroid/accounts/AccountAndUser;Landroid/accounts/Account;I)Z

    #@116
    move-result v36

    #@117
    if-nez v36, :cond_137

    #@119
    .line 1978
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->remove()V

    #@11c
    .line 1979
    move-object/from16 v0, p0

    #@11e
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@120
    move-object/from16 v36, v0

    #@122
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@125
    move-result-object v36

    #@126
    move-object/from16 v0, v25

    #@128
    iget-object v0, v0, Landroid/content/SyncOperation;->pendingOperation:Landroid/content/SyncStorageEngine$PendingOperation;

    #@12a
    move-object/from16 v38, v0

    #@12c
    move-object/from16 v0, v36

    #@12e
    move-object/from16 v1, v38

    #@130
    invoke-virtual {v0, v1}, Landroid/content/SyncStorageEngine;->deleteFromPending(Landroid/content/SyncStorageEngine$PendingOperation;)Z

    #@133
    goto :goto_ef

    #@134
    .line 2047
    .end local v8           #activityManager:Landroid/app/ActivityManager;
    .end local v25           #op:Landroid/content/SyncOperation;
    .end local v26           #operationIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/SyncOperation;>;"
    .end local v28           #removedUsers:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Integer;>;"
    :catchall_134
    move-exception v36

    #@135
    monitor-exit v37
    :try_end_136
    .catchall {:try_start_95 .. :try_end_136} :catchall_134

    #@136
    throw v36

    #@137
    .line 1984
    .restart local v8       #activityManager:Landroid/app/ActivityManager;
    .restart local v25       #op:Landroid/content/SyncOperation;
    .restart local v26       #operationIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/SyncOperation;>;"
    .restart local v28       #removedUsers:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_137
    :try_start_137
    move-object/from16 v0, p0

    #@139
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@13b
    move-object/from16 v36, v0

    #@13d
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@140
    move-result-object v36

    #@141
    move-object/from16 v0, v25

    #@143
    iget-object v0, v0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@145
    move-object/from16 v38, v0

    #@147
    move-object/from16 v0, v25

    #@149
    iget v0, v0, Landroid/content/SyncOperation;->userId:I

    #@14b
    move/from16 v39, v0

    #@14d
    move-object/from16 v0, v25

    #@14f
    iget-object v0, v0, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@151
    move-object/from16 v40, v0

    #@153
    move-object/from16 v0, v36

    #@155
    move-object/from16 v1, v38

    #@157
    move/from16 v2, v39

    #@159
    move-object/from16 v3, v40

    #@15b
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/SyncStorageEngine;->getIsSyncable(Landroid/accounts/Account;ILjava/lang/String;)I

    #@15e
    move-result v31

    #@15f
    .line 1986
    .local v31, syncableState:I
    if-nez v31, :cond_17d

    #@161
    .line 1987
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->remove()V

    #@164
    .line 1988
    move-object/from16 v0, p0

    #@166
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@168
    move-object/from16 v36, v0

    #@16a
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@16d
    move-result-object v36

    #@16e
    move-object/from16 v0, v25

    #@170
    iget-object v0, v0, Landroid/content/SyncOperation;->pendingOperation:Landroid/content/SyncStorageEngine$PendingOperation;

    #@172
    move-object/from16 v38, v0

    #@174
    move-object/from16 v0, v36

    #@176
    move-object/from16 v1, v38

    #@178
    invoke-virtual {v0, v1}, Landroid/content/SyncStorageEngine;->deleteFromPending(Landroid/content/SyncStorageEngine$PendingOperation;)Z

    #@17b
    goto/16 :goto_ef

    #@17d
    .line 1993
    :cond_17d
    move-object/from16 v0, v25

    #@17f
    iget v0, v0, Landroid/content/SyncOperation;->userId:I

    #@181
    move/from16 v36, v0

    #@183
    move/from16 v0, v36

    #@185
    invoke-virtual {v8, v0}, Landroid/app/ActivityManager;->isUserRunning(I)Z

    #@188
    move-result v36

    #@189
    if-nez v36, :cond_1b8

    #@18b
    .line 1994
    move-object/from16 v0, p0

    #@18d
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@18f
    move-object/from16 v36, v0

    #@191
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$2900(Landroid/content/SyncManager;)Landroid/os/UserManager;

    #@194
    move-result-object v36

    #@195
    move-object/from16 v0, v25

    #@197
    iget v0, v0, Landroid/content/SyncOperation;->userId:I

    #@199
    move/from16 v38, v0

    #@19b
    move-object/from16 v0, v36

    #@19d
    move/from16 v1, v38

    #@19f
    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    #@1a2
    move-result-object v35

    #@1a3
    .line 1995
    .local v35, userInfo:Landroid/content/pm/UserInfo;
    if-nez v35, :cond_ef

    #@1a5
    .line 1996
    move-object/from16 v0, v25

    #@1a7
    iget v0, v0, Landroid/content/SyncOperation;->userId:I

    #@1a9
    move/from16 v36, v0

    #@1ab
    invoke-static/range {v36 .. v36}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ae
    move-result-object v36

    #@1af
    move-object/from16 v0, v28

    #@1b1
    move-object/from16 v1, v36

    #@1b3
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@1b6
    goto/16 :goto_ef

    #@1b8
    .line 2003
    .end local v35           #userInfo:Landroid/content/pm/UserInfo;
    :cond_1b8
    move-object/from16 v0, v25

    #@1ba
    iget-wide v0, v0, Landroid/content/SyncOperation;->effectiveRunTime:J

    #@1bc
    move-wide/from16 v38, v0

    #@1be
    cmp-long v36, v38, v20

    #@1c0
    if-lez v36, :cond_1d4

    #@1c2
    .line 2004
    move-object/from16 v0, v25

    #@1c4
    iget-wide v0, v0, Landroid/content/SyncOperation;->effectiveRunTime:J

    #@1c6
    move-wide/from16 v38, v0

    #@1c8
    cmp-long v36, v18, v38

    #@1ca
    if-lez v36, :cond_ef

    #@1cc
    .line 2005
    move-object/from16 v0, v25

    #@1ce
    iget-wide v0, v0, Landroid/content/SyncOperation;->effectiveRunTime:J

    #@1d0
    move-wide/from16 v18, v0

    #@1d2
    goto/16 :goto_ef

    #@1d4
    .line 2011
    :cond_1d4
    move-object/from16 v0, p0

    #@1d6
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@1d8
    move-object/from16 v36, v0

    #@1da
    move-object/from16 v0, v36

    #@1dc
    iget-object v0, v0, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@1de
    move-object/from16 v36, v0

    #@1e0
    move-object/from16 v0, v25

    #@1e2
    iget-object v0, v0, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@1e4
    move-object/from16 v38, v0

    #@1e6
    move-object/from16 v0, v25

    #@1e8
    iget-object v0, v0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@1ea
    move-object/from16 v39, v0

    #@1ec
    move-object/from16 v0, v39

    #@1ee
    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@1f0
    move-object/from16 v39, v0

    #@1f2
    invoke-static/range {v38 .. v39}, Landroid/content/SyncAdapterType;->newKey(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SyncAdapterType;

    #@1f5
    move-result-object v38

    #@1f6
    move-object/from16 v0, v25

    #@1f8
    iget v0, v0, Landroid/content/SyncOperation;->userId:I

    #@1fa
    move/from16 v39, v0

    #@1fc
    move-object/from16 v0, v36

    #@1fe
    move-object/from16 v1, v38

    #@200
    move/from16 v2, v39

    #@202
    invoke-virtual {v0, v1, v2}, Landroid/content/SyncAdaptersCache;->getServiceInfo(Ljava/lang/Object;I)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@205
    move-result-object v30

    #@206
    .line 2016
    .local v30, syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    if-eqz v30, :cond_2a9

    #@208
    .line 2017
    move-object/from16 v0, p0

    #@20a
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@20c
    move-object/from16 v36, v0

    #@20e
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$300(Landroid/content/SyncManager;)Landroid/net/ConnectivityManager;

    #@211
    move-result-object v36

    #@212
    move-object/from16 v0, v30

    #@214
    iget v0, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->uid:I

    #@216
    move/from16 v38, v0

    #@218
    move-object/from16 v0, v36

    #@21a
    move/from16 v1, v38

    #@21c
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfoForUid(I)Landroid/net/NetworkInfo;

    #@21f
    move-result-object v17

    #@220
    .line 2019
    .local v17, networkInfo:Landroid/net/NetworkInfo;
    if-eqz v17, :cond_2a6

    #@222
    invoke-virtual/range {v17 .. v17}, Landroid/net/NetworkInfo;->isConnected()Z

    #@225
    move-result v36

    #@226
    if-eqz v36, :cond_2a6

    #@228
    const/16 v33, 0x1

    #@22a
    .line 2027
    .end local v17           #networkInfo:Landroid/net/NetworkInfo;
    .local v33, uidNetworkConnected:Z
    :goto_22a
    move-object/from16 v0, v25

    #@22c
    iget-object v0, v0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@22e
    move-object/from16 v36, v0

    #@230
    const-string v38, "ignore_settings"

    #@232
    const/16 v39, 0x0

    #@234
    move-object/from16 v0, v36

    #@236
    move-object/from16 v1, v38

    #@238
    move/from16 v2, v39

    #@23a
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@23d
    move-result v36

    #@23e
    if-nez v36, :cond_2ad

    #@240
    if-lez v31, :cond_2ad

    #@242
    move-object/from16 v0, p0

    #@244
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@246
    move-object/from16 v36, v0

    #@248
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@24b
    move-result-object v36

    #@24c
    move-object/from16 v0, v25

    #@24e
    iget v0, v0, Landroid/content/SyncOperation;->userId:I

    #@250
    move/from16 v38, v0

    #@252
    move-object/from16 v0, v36

    #@254
    move/from16 v1, v38

    #@256
    invoke-virtual {v0, v1}, Landroid/content/SyncStorageEngine;->getMasterSyncAutomatically(I)Z

    #@259
    move-result v36

    #@25a
    if-eqz v36, :cond_28a

    #@25c
    if-eqz v9, :cond_28a

    #@25e
    if-eqz v33, :cond_28a

    #@260
    move-object/from16 v0, p0

    #@262
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@264
    move-object/from16 v36, v0

    #@266
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@269
    move-result-object v36

    #@26a
    move-object/from16 v0, v25

    #@26c
    iget-object v0, v0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@26e
    move-object/from16 v38, v0

    #@270
    move-object/from16 v0, v25

    #@272
    iget v0, v0, Landroid/content/SyncOperation;->userId:I

    #@274
    move/from16 v39, v0

    #@276
    move-object/from16 v0, v25

    #@278
    iget-object v0, v0, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@27a
    move-object/from16 v40, v0

    #@27c
    move-object/from16 v0, v36

    #@27e
    move-object/from16 v1, v38

    #@280
    move/from16 v2, v39

    #@282
    move-object/from16 v3, v40

    #@284
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/SyncStorageEngine;->getSyncAutomatically(Landroid/accounts/Account;ILjava/lang/String;)Z

    #@287
    move-result v36

    #@288
    if-nez v36, :cond_2ad

    #@28a
    .line 2034
    :cond_28a
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->remove()V

    #@28d
    .line 2035
    move-object/from16 v0, p0

    #@28f
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@291
    move-object/from16 v36, v0

    #@293
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@296
    move-result-object v36

    #@297
    move-object/from16 v0, v25

    #@299
    iget-object v0, v0, Landroid/content/SyncOperation;->pendingOperation:Landroid/content/SyncStorageEngine$PendingOperation;

    #@29b
    move-object/from16 v38, v0

    #@29d
    move-object/from16 v0, v36

    #@29f
    move-object/from16 v1, v38

    #@2a1
    invoke-virtual {v0, v1}, Landroid/content/SyncStorageEngine;->deleteFromPending(Landroid/content/SyncStorageEngine$PendingOperation;)Z

    #@2a4
    goto/16 :goto_ef

    #@2a6
    .line 2019
    .end local v33           #uidNetworkConnected:Z
    .restart local v17       #networkInfo:Landroid/net/NetworkInfo;
    :cond_2a6
    const/16 v33, 0x0

    #@2a8
    goto :goto_22a

    #@2a9
    .line 2021
    .end local v17           #networkInfo:Landroid/net/NetworkInfo;
    :cond_2a9
    const/16 v33, 0x0

    #@2ab
    .restart local v33       #uidNetworkConnected:Z
    goto/16 :goto_22a

    #@2ad
    .line 2039
    :cond_2ad
    move-object/from16 v0, v27

    #@2af
    move-object/from16 v1, v25

    #@2b1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2b4
    goto/16 :goto_ef

    #@2b6
    .line 2041
    .end local v25           #op:Landroid/content/SyncOperation;
    .end local v30           #syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    .end local v31           #syncableState:I
    .end local v33           #uidNetworkConnected:Z
    :cond_2b6
    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2b9
    move-result-object v14

    #@2ba
    .local v14, i$:Ljava/util/Iterator;
    :cond_2ba
    :goto_2ba
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    #@2bd
    move-result v36

    #@2be
    if-eqz v36, :cond_2f0

    #@2c0
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c3
    move-result-object v34

    #@2c4
    check-cast v34, Ljava/lang/Integer;

    #@2c6
    .line 2043
    .local v34, user:Ljava/lang/Integer;
    move-object/from16 v0, p0

    #@2c8
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2ca
    move-object/from16 v36, v0

    #@2cc
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$2900(Landroid/content/SyncManager;)Landroid/os/UserManager;

    #@2cf
    move-result-object v36

    #@2d0
    invoke-virtual/range {v34 .. v34}, Ljava/lang/Integer;->intValue()I

    #@2d3
    move-result v38

    #@2d4
    move-object/from16 v0, v36

    #@2d6
    move/from16 v1, v38

    #@2d8
    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    #@2db
    move-result-object v36

    #@2dc
    if-nez v36, :cond_2ba

    #@2de
    .line 2044
    move-object/from16 v0, p0

    #@2e0
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2e2
    move-object/from16 v36, v0

    #@2e4
    invoke-virtual/range {v34 .. v34}, Ljava/lang/Integer;->intValue()I

    #@2e7
    move-result v38

    #@2e8
    move-object/from16 v0, v36

    #@2ea
    move/from16 v1, v38

    #@2ec
    invoke-static {v0, v1}, Landroid/content/SyncManager;->access$800(Landroid/content/SyncManager;I)V

    #@2ef
    goto :goto_2ba

    #@2f0
    .line 2047
    .end local v34           #user:Ljava/lang/Integer;
    :cond_2f0
    monitor-exit v37
    :try_end_2f1
    .catchall {:try_start_137 .. :try_end_2f1} :catchall_134

    #@2f1
    .line 2054
    if-eqz v15, :cond_310

    #@2f3
    const-string v36, "SyncManager"

    #@2f5
    new-instance v37, Ljava/lang/StringBuilder;

    #@2f7
    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    #@2fa
    const-string/jumbo v38, "sort the candidate operations, size "

    #@2fd
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@300
    move-result-object v37

    #@301
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@304
    move-result v38

    #@305
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@308
    move-result-object v37

    #@309
    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30c
    move-result-object v37

    #@30d
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@310
    .line 2055
    :cond_310
    invoke-static/range {v27 .. v27}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@313
    .line 2056
    if-eqz v15, :cond_31c

    #@315
    const-string v36, "SyncManager"

    #@317
    const-string v37, "dispatch all ready sync operations"

    #@319
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@31c
    .line 2057
    :cond_31c
    const/4 v13, 0x0

    #@31d
    .local v13, i:I
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@320
    move-result v4

    #@321
    .local v4, N:I
    :goto_321
    if-ge v13, v4, :cond_2d

    #@323
    .line 2058
    move-object/from16 v0, v27

    #@325
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@328
    move-result-object v10

    #@329
    check-cast v10, Landroid/content/SyncOperation;

    #@32b
    .line 2059
    .local v10, candidate:Landroid/content/SyncOperation;
    invoke-virtual {v10}, Landroid/content/SyncOperation;->isInitialization()Z

    #@32e
    move-result v11

    #@32f
    .line 2061
    .local v11, candidateIsInitialization:Z
    const/16 v22, 0x0

    #@331
    .line 2062
    .local v22, numInit:I
    const/16 v23, 0x0

    #@333
    .line 2063
    .local v23, numRegular:I
    const/4 v12, 0x0

    #@334
    .line 2064
    .local v12, conflict:Landroid/content/SyncManager$ActiveSyncContext;
    const/16 v16, 0x0

    #@336
    .line 2065
    .local v16, longRunning:Landroid/content/SyncManager$ActiveSyncContext;
    const/16 v32, 0x0

    #@338
    .line 2066
    .local v32, toReschedule:Landroid/content/SyncManager$ActiveSyncContext;
    const/16 v24, 0x0

    #@33a
    .line 2068
    .local v24, oldestNonExpeditedRegular:Landroid/content/SyncManager$ActiveSyncContext;
    move-object/from16 v0, p0

    #@33c
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@33e
    move-object/from16 v36, v0

    #@340
    move-object/from16 v0, v36

    #@342
    iget-object v0, v0, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@344
    move-object/from16 v36, v0

    #@346
    invoke-virtual/range {v36 .. v36}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@349
    move-result-object v14

    #@34a
    :cond_34a
    :goto_34a
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    #@34d
    move-result v36

    #@34e
    if-eqz v36, :cond_3ed

    #@350
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@353
    move-result-object v7

    #@354
    check-cast v7, Landroid/content/SyncManager$ActiveSyncContext;

    #@356
    .line 2069
    .local v7, activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    iget-object v6, v7, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@358
    .line 2070
    .local v6, activeOp:Landroid/content/SyncOperation;
    invoke-virtual {v6}, Landroid/content/SyncOperation;->isInitialization()Z

    #@35b
    move-result v36

    #@35c
    if-eqz v36, :cond_3b8

    #@35e
    .line 2071
    add-int/lit8 v22, v22, 0x1

    #@360
    .line 2082
    :cond_360
    :goto_360
    iget-object v0, v6, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@362
    move-object/from16 v36, v0

    #@364
    move-object/from16 v0, v36

    #@366
    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@368
    move-object/from16 v36, v0

    #@36a
    iget-object v0, v10, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@36c
    move-object/from16 v37, v0

    #@36e
    move-object/from16 v0, v37

    #@370
    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@372
    move-object/from16 v37, v0

    #@374
    invoke-virtual/range {v36 .. v37}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@377
    move-result v36

    #@378
    if-eqz v36, :cond_3d3

    #@37a
    iget-object v0, v6, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@37c
    move-object/from16 v36, v0

    #@37e
    iget-object v0, v10, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@380
    move-object/from16 v37, v0

    #@382
    invoke-virtual/range {v36 .. v37}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@385
    move-result v36

    #@386
    if-eqz v36, :cond_3d3

    #@388
    iget v0, v6, Landroid/content/SyncOperation;->userId:I

    #@38a
    move/from16 v36, v0

    #@38c
    iget v0, v10, Landroid/content/SyncOperation;->userId:I

    #@38e
    move/from16 v37, v0

    #@390
    move/from16 v0, v36

    #@392
    move/from16 v1, v37

    #@394
    if-ne v0, v1, :cond_3d3

    #@396
    iget-boolean v0, v6, Landroid/content/SyncOperation;->allowParallelSyncs:Z

    #@398
    move/from16 v36, v0

    #@39a
    if-eqz v36, :cond_3b6

    #@39c
    iget-object v0, v6, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@39e
    move-object/from16 v36, v0

    #@3a0
    move-object/from16 v0, v36

    #@3a2
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@3a4
    move-object/from16 v36, v0

    #@3a6
    iget-object v0, v10, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@3a8
    move-object/from16 v37, v0

    #@3aa
    move-object/from16 v0, v37

    #@3ac
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@3ae
    move-object/from16 v37, v0

    #@3b0
    invoke-virtual/range {v36 .. v37}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b3
    move-result v36

    #@3b4
    if-eqz v36, :cond_3d3

    #@3b6
    .line 2087
    :cond_3b6
    move-object v12, v7

    #@3b7
    goto :goto_34a

    #@3b8
    .line 2073
    :cond_3b8
    add-int/lit8 v23, v23, 0x1

    #@3ba
    .line 2074
    invoke-virtual {v6}, Landroid/content/SyncOperation;->isExpedited()Z

    #@3bd
    move-result v36

    #@3be
    if-nez v36, :cond_360

    #@3c0
    .line 2075
    if-eqz v24, :cond_3d0

    #@3c2
    move-object/from16 v0, v24

    #@3c4
    iget-wide v0, v0, Landroid/content/SyncManager$ActiveSyncContext;->mStartTime:J

    #@3c6
    move-wide/from16 v36, v0

    #@3c8
    iget-wide v0, v7, Landroid/content/SyncManager$ActiveSyncContext;->mStartTime:J

    #@3ca
    move-wide/from16 v38, v0

    #@3cc
    cmp-long v36, v36, v38

    #@3ce
    if-lez v36, :cond_360

    #@3d0
    .line 2078
    :cond_3d0
    move-object/from16 v24, v7

    #@3d2
    goto :goto_360

    #@3d3
    .line 2090
    :cond_3d3
    invoke-virtual {v6}, Landroid/content/SyncOperation;->isInitialization()Z

    #@3d6
    move-result v36

    #@3d7
    move/from16 v0, v36

    #@3d9
    if-ne v11, v0, :cond_34a

    #@3db
    iget-wide v0, v7, Landroid/content/SyncManager$ActiveSyncContext;->mStartTime:J

    #@3dd
    move-wide/from16 v36, v0

    #@3df
    invoke-static {}, Landroid/content/SyncManager;->access$3000()J

    #@3e2
    move-result-wide v38

    #@3e3
    add-long v36, v36, v38

    #@3e5
    cmp-long v36, v36, v20

    #@3e7
    if-gez v36, :cond_34a

    #@3e9
    .line 2092
    move-object/from16 v16, v7

    #@3eb
    goto/16 :goto_34a

    #@3ed
    .line 2098
    .end local v6           #activeOp:Landroid/content/SyncOperation;
    .end local v7           #activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    :cond_3ed
    if-eqz v15, :cond_49d

    #@3ef
    .line 2099
    const-string v36, "SyncManager"

    #@3f1
    new-instance v37, Ljava/lang/StringBuilder;

    #@3f3
    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    #@3f6
    const-string v38, "candidate "

    #@3f8
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3fb
    move-result-object v37

    #@3fc
    add-int/lit8 v38, v13, 0x1

    #@3fe
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@401
    move-result-object v37

    #@402
    const-string v38, " of "

    #@404
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@407
    move-result-object v37

    #@408
    move-object/from16 v0, v37

    #@40a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40d
    move-result-object v37

    #@40e
    const-string v38, ": "

    #@410
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@413
    move-result-object v37

    #@414
    move-object/from16 v0, v37

    #@416
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@419
    move-result-object v37

    #@41a
    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41d
    move-result-object v37

    #@41e
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@421
    .line 2100
    const-string v36, "SyncManager"

    #@423
    new-instance v37, Ljava/lang/StringBuilder;

    #@425
    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    #@428
    const-string v38, "  numActiveInit="

    #@42a
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42d
    move-result-object v37

    #@42e
    move-object/from16 v0, v37

    #@430
    move/from16 v1, v22

    #@432
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@435
    move-result-object v37

    #@436
    const-string v38, ", numActiveRegular="

    #@438
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43b
    move-result-object v37

    #@43c
    move-object/from16 v0, v37

    #@43e
    move/from16 v1, v23

    #@440
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@443
    move-result-object v37

    #@444
    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@447
    move-result-object v37

    #@448
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44b
    .line 2101
    const-string v36, "SyncManager"

    #@44d
    new-instance v37, Ljava/lang/StringBuilder;

    #@44f
    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    #@452
    const-string v38, "  longRunning: "

    #@454
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@457
    move-result-object v37

    #@458
    move-object/from16 v0, v37

    #@45a
    move-object/from16 v1, v16

    #@45c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45f
    move-result-object v37

    #@460
    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@463
    move-result-object v37

    #@464
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@467
    .line 2102
    const-string v36, "SyncManager"

    #@469
    new-instance v37, Ljava/lang/StringBuilder;

    #@46b
    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    #@46e
    const-string v38, "  conflict: "

    #@470
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@473
    move-result-object v37

    #@474
    move-object/from16 v0, v37

    #@476
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@479
    move-result-object v37

    #@47a
    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47d
    move-result-object v37

    #@47e
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@481
    .line 2103
    const-string v36, "SyncManager"

    #@483
    new-instance v37, Ljava/lang/StringBuilder;

    #@485
    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    #@488
    const-string v38, "  oldestNonExpeditedRegular: "

    #@48a
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48d
    move-result-object v37

    #@48e
    move-object/from16 v0, v37

    #@490
    move-object/from16 v1, v24

    #@492
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@495
    move-result-object v37

    #@496
    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@499
    move-result-object v37

    #@49a
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@49d
    .line 2106
    :cond_49d
    if-eqz v11, :cond_52c

    #@49f
    invoke-static {}, Landroid/content/SyncManager;->access$3100()I

    #@4a2
    move-result v36

    #@4a3
    move/from16 v0, v22

    #@4a5
    move/from16 v1, v36

    #@4a7
    if-ge v0, v1, :cond_529

    #@4a9
    const/16 v29, 0x1

    #@4ab
    .line 2110
    .local v29, roomAvailable:Z
    :goto_4ab
    if-eqz v12, :cond_584

    #@4ad
    .line 2111
    if-eqz v11, :cond_53e

    #@4af
    iget-object v0, v12, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@4b1
    move-object/from16 v36, v0

    #@4b3
    invoke-virtual/range {v36 .. v36}, Landroid/content/SyncOperation;->isInitialization()Z

    #@4b6
    move-result v36

    #@4b7
    if-nez v36, :cond_53e

    #@4b9
    invoke-static {}, Landroid/content/SyncManager;->access$3100()I

    #@4bc
    move-result v36

    #@4bd
    move/from16 v0, v22

    #@4bf
    move/from16 v1, v36

    #@4c1
    if-ge v0, v1, :cond_53e

    #@4c3
    .line 2113
    move-object/from16 v32, v12

    #@4c5
    .line 2114
    const-string v36, "SyncManager"

    #@4c7
    const/16 v37, 0x2

    #@4c9
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@4cc
    move-result v36

    #@4cd
    if-eqz v36, :cond_4e9

    #@4cf
    .line 2115
    const-string v36, "SyncManager"

    #@4d1
    new-instance v37, Ljava/lang/StringBuilder;

    #@4d3
    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    #@4d6
    const-string v38, "canceling and rescheduling sync since an initialization takes higher priority, "

    #@4d8
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4db
    move-result-object v37

    #@4dc
    move-object/from16 v0, v37

    #@4de
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e1
    move-result-object v37

    #@4e2
    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e5
    move-result-object v37

    #@4e6
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4e9
    .line 2157
    :cond_4e9
    :goto_4e9
    if-eqz v32, :cond_505

    #@4eb
    .line 2158
    const/16 v36, 0x0

    #@4ed
    move-object/from16 v0, p0

    #@4ef
    move-object/from16 v1, v36

    #@4f1
    move-object/from16 v2, v32

    #@4f3
    invoke-direct {v0, v1, v2}, Landroid/content/SyncManager$SyncHandler;->runSyncFinishedOrCanceledLocked(Landroid/content/SyncResult;Landroid/content/SyncManager$ActiveSyncContext;)V

    #@4f6
    .line 2159
    move-object/from16 v0, p0

    #@4f8
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@4fa
    move-object/from16 v36, v0

    #@4fc
    move-object/from16 v0, v32

    #@4fe
    iget-object v0, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@500
    move-object/from16 v37, v0

    #@502
    invoke-virtual/range {v36 .. v37}, Landroid/content/SyncManager;->scheduleSyncOperation(Landroid/content/SyncOperation;)V

    #@505
    .line 2161
    :cond_505
    move-object/from16 v0, p0

    #@507
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@509
    move-object/from16 v36, v0

    #@50b
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$600(Landroid/content/SyncManager;)Landroid/content/SyncQueue;

    #@50e
    move-result-object v37

    #@50f
    monitor-enter v37

    #@510
    .line 2162
    :try_start_510
    move-object/from16 v0, p0

    #@512
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@514
    move-object/from16 v36, v0

    #@516
    invoke-static/range {v36 .. v36}, Landroid/content/SyncManager;->access$600(Landroid/content/SyncManager;)Landroid/content/SyncQueue;

    #@519
    move-result-object v36

    #@51a
    move-object/from16 v0, v36

    #@51c
    invoke-virtual {v0, v10}, Landroid/content/SyncQueue;->remove(Landroid/content/SyncOperation;)V

    #@51f
    .line 2163
    monitor-exit v37
    :try_end_520
    .catchall {:try_start_510 .. :try_end_520} :catchall_5f4

    #@520
    .line 2164
    move-object/from16 v0, p0

    #@522
    invoke-direct {v0, v10}, Landroid/content/SyncManager$SyncHandler;->dispatchSyncOperation(Landroid/content/SyncOperation;)Z

    #@525
    .line 2057
    :cond_525
    add-int/lit8 v13, v13, 0x1

    #@527
    goto/16 :goto_321

    #@529
    .line 2106
    .end local v29           #roomAvailable:Z
    :cond_529
    const/16 v29, 0x0

    #@52b
    goto :goto_4ab

    #@52c
    :cond_52c
    invoke-static {}, Landroid/content/SyncManager;->access$3200()I

    #@52f
    move-result v36

    #@530
    move/from16 v0, v23

    #@532
    move/from16 v1, v36

    #@534
    if-ge v0, v1, :cond_53a

    #@536
    const/16 v29, 0x1

    #@538
    goto/16 :goto_4ab

    #@53a
    :cond_53a
    const/16 v29, 0x0

    #@53c
    goto/16 :goto_4ab

    #@53e
    .line 2118
    .restart local v29       #roomAvailable:Z
    :cond_53e
    iget-boolean v0, v10, Landroid/content/SyncOperation;->expedited:Z

    #@540
    move/from16 v36, v0

    #@542
    if-eqz v36, :cond_525

    #@544
    iget-object v0, v12, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@546
    move-object/from16 v36, v0

    #@548
    move-object/from16 v0, v36

    #@54a
    iget-boolean v0, v0, Landroid/content/SyncOperation;->expedited:Z

    #@54c
    move/from16 v36, v0

    #@54e
    if-nez v36, :cond_525

    #@550
    iget-object v0, v12, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@552
    move-object/from16 v36, v0

    #@554
    invoke-virtual/range {v36 .. v36}, Landroid/content/SyncOperation;->isInitialization()Z

    #@557
    move-result v36

    #@558
    move/from16 v0, v36

    #@55a
    if-ne v11, v0, :cond_525

    #@55c
    .line 2121
    move-object/from16 v32, v12

    #@55e
    .line 2122
    const-string v36, "SyncManager"

    #@560
    const/16 v37, 0x2

    #@562
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@565
    move-result v36

    #@566
    if-eqz v36, :cond_4e9

    #@568
    .line 2123
    const-string v36, "SyncManager"

    #@56a
    new-instance v37, Ljava/lang/StringBuilder;

    #@56c
    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    #@56f
    const-string v38, "canceling and rescheduling sync since an expedited takes higher priority, "

    #@571
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@574
    move-result-object v37

    #@575
    move-object/from16 v0, v37

    #@577
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@57a
    move-result-object v37

    #@57b
    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57e
    move-result-object v37

    #@57f
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@582
    goto/16 :goto_4e9

    #@584
    .line 2129
    :cond_584
    if-nez v29, :cond_4e9

    #@586
    .line 2131
    invoke-virtual {v10}, Landroid/content/SyncOperation;->isExpedited()Z

    #@589
    move-result v36

    #@58a
    if-eqz v36, :cond_5ba

    #@58c
    if-eqz v24, :cond_5ba

    #@58e
    if-nez v11, :cond_5ba

    #@590
    .line 2136
    move-object/from16 v32, v24

    #@592
    .line 2137
    const-string v36, "SyncManager"

    #@594
    const/16 v37, 0x2

    #@596
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@599
    move-result v36

    #@59a
    if-eqz v36, :cond_4e9

    #@59c
    .line 2138
    const-string v36, "SyncManager"

    #@59e
    new-instance v37, Ljava/lang/StringBuilder;

    #@5a0
    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    #@5a3
    const-string v38, "canceling and rescheduling sync since an expedited is ready to run, "

    #@5a5
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a8
    move-result-object v37

    #@5a9
    move-object/from16 v0, v37

    #@5ab
    move-object/from16 v1, v24

    #@5ad
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b0
    move-result-object v37

    #@5b1
    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b4
    move-result-object v37

    #@5b5
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5b8
    goto/16 :goto_4e9

    #@5ba
    .line 2141
    :cond_5ba
    if-eqz v16, :cond_525

    #@5bc
    move-object/from16 v0, v16

    #@5be
    iget-object v0, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@5c0
    move-object/from16 v36, v0

    #@5c2
    invoke-virtual/range {v36 .. v36}, Landroid/content/SyncOperation;->isInitialization()Z

    #@5c5
    move-result v36

    #@5c6
    move/from16 v0, v36

    #@5c8
    if-ne v11, v0, :cond_525

    #@5ca
    .line 2146
    move-object/from16 v32, v16

    #@5cc
    .line 2147
    const-string v36, "SyncManager"

    #@5ce
    const/16 v37, 0x2

    #@5d0
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@5d3
    move-result v36

    #@5d4
    if-eqz v36, :cond_4e9

    #@5d6
    .line 2148
    const-string v36, "SyncManager"

    #@5d8
    new-instance v37, Ljava/lang/StringBuilder;

    #@5da
    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    #@5dd
    const-string v38, "canceling and rescheduling sync since it ran roo long, "

    #@5df
    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e2
    move-result-object v37

    #@5e3
    move-object/from16 v0, v37

    #@5e5
    move-object/from16 v1, v16

    #@5e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5ea
    move-result-object v37

    #@5eb
    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5ee
    move-result-object v37

    #@5ef
    invoke-static/range {v36 .. v37}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5f2
    goto/16 :goto_4e9

    #@5f4
    .line 2163
    :catchall_5f4
    move-exception v36

    #@5f5
    :try_start_5f5
    monitor-exit v37
    :try_end_5f6
    .catchall {:try_start_5f5 .. :try_end_5f6} :catchall_5f4

    #@5f6
    throw v36
.end method

.method private runBoundToSyncAdapter(Landroid/content/SyncManager$ActiveSyncContext;Landroid/content/ISyncAdapter;)V
    .registers 9
    .parameter "activeSyncContext"
    .parameter "syncAdapter"

    #@0
    .prologue
    .line 2208
    iput-object p2, p1, Landroid/content/SyncManager$ActiveSyncContext;->mSyncAdapter:Landroid/content/ISyncAdapter;

    #@2
    .line 2209
    iget-object v2, p1, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@4
    .line 2211
    .local v2, syncOperation:Landroid/content/SyncOperation;
    const/4 v3, 0x1

    #@5
    :try_start_5
    iput-boolean v3, p1, Landroid/content/SyncManager$ActiveSyncContext;->mIsLinkedToDeath:Z

    #@7
    .line 2212
    invoke-interface {p2}, Landroid/content/ISyncAdapter;->asBinder()Landroid/os/IBinder;

    #@a
    move-result-object v3

    #@b
    const/4 v4, 0x0

    #@c
    invoke-interface {v3, p1, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@f
    .line 2214
    iget-object v3, v2, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@11
    iget-object v4, v2, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@13
    iget-object v5, v2, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@15
    invoke-interface {p2, p1, v3, v4, v5}, Landroid/content/ISyncAdapter;->startSync(Landroid/content/ISyncContext;Ljava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_18} :catch_19
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_18} :catch_35

    #@18
    .line 2225
    :goto_18
    return-void

    #@19
    .line 2216
    :catch_19
    move-exception v1

    #@1a
    .line 2217
    .local v1, remoteExc:Landroid/os/RemoteException;
    const-string v3, "SyncManager"

    #@1c
    const-string/jumbo v4, "maybeStartNextSync: caught a RemoteException, rescheduling"

    #@1f
    invoke-static {v3, v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@22
    .line 2218
    invoke-direct {p0, p1}, Landroid/content/SyncManager$SyncHandler;->closeActiveSyncContext(Landroid/content/SyncManager$ActiveSyncContext;)V

    #@25
    .line 2219
    iget-object v3, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@27
    invoke-static {v3, v2}, Landroid/content/SyncManager;->access$3300(Landroid/content/SyncManager;Landroid/content/SyncOperation;)V

    #@2a
    .line 2220
    iget-object v3, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2c
    new-instance v4, Landroid/content/SyncOperation;

    #@2e
    invoke-direct {v4, v2}, Landroid/content/SyncOperation;-><init>(Landroid/content/SyncOperation;)V

    #@31
    invoke-virtual {v3, v4}, Landroid/content/SyncManager;->scheduleSyncOperation(Landroid/content/SyncOperation;)V

    #@34
    goto :goto_18

    #@35
    .line 2221
    .end local v1           #remoteExc:Landroid/os/RemoteException;
    :catch_35
    move-exception v0

    #@36
    .line 2222
    .local v0, exc:Ljava/lang/RuntimeException;
    invoke-direct {p0, p1}, Landroid/content/SyncManager$SyncHandler;->closeActiveSyncContext(Landroid/content/SyncManager$ActiveSyncContext;)V

    #@39
    .line 2223
    const-string v3, "SyncManager"

    #@3b
    new-instance v4, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v5, "Caught RuntimeException while starting the sync "

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@51
    goto :goto_18
.end method

.method private runSyncFinishedOrCanceledLocked(Landroid/content/SyncResult;Landroid/content/SyncManager$ActiveSyncContext;)V
    .registers 28
    .parameter "syncResult"
    .parameter "activeSyncContext"

    #@0
    .prologue
    .line 2257
    const-string v2, "SyncManager"

    #@2
    const/4 v3, 0x2

    #@3
    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v24

    #@7
    .line 2259
    .local v24, isLoggable:Z
    move-object/from16 v0, p2

    #@9
    iget-boolean v2, v0, Landroid/content/SyncManager$ActiveSyncContext;->mIsLinkedToDeath:Z

    #@b
    if-eqz v2, :cond_20

    #@d
    .line 2260
    move-object/from16 v0, p2

    #@f
    iget-object v2, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncAdapter:Landroid/content/ISyncAdapter;

    #@11
    invoke-interface {v2}, Landroid/content/ISyncAdapter;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v2

    #@15
    const/4 v3, 0x0

    #@16
    move-object/from16 v0, p2

    #@18
    invoke-interface {v2, v0, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@1b
    .line 2261
    const/4 v2, 0x0

    #@1c
    move-object/from16 v0, p2

    #@1e
    iput-boolean v2, v0, Landroid/content/SyncManager$ActiveSyncContext;->mIsLinkedToDeath:Z

    #@20
    .line 2263
    :cond_20
    move-object/from16 v0, p0

    #@22
    move-object/from16 v1, p2

    #@24
    invoke-direct {v0, v1}, Landroid/content/SyncManager$SyncHandler;->closeActiveSyncContext(Landroid/content/SyncManager$ActiveSyncContext;)V

    #@27
    .line 2265
    move-object/from16 v0, p2

    #@29
    iget-object v5, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@2b
    .line 2267
    .local v5, syncOperation:Landroid/content/SyncOperation;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@2e
    move-result-wide v2

    #@2f
    move-object/from16 v0, p2

    #@31
    iget-wide v11, v0, Landroid/content/SyncManager$ActiveSyncContext;->mStartTime:J

    #@33
    sub-long v9, v2, v11

    #@35
    .line 2272
    .local v9, elapsedTime:J
    if-eqz p1, :cond_117

    #@37
    .line 2273
    if-eqz v24, :cond_5e

    #@39
    .line 2274
    const-string v2, "SyncManager"

    #@3b
    new-instance v3, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string/jumbo v4, "runSyncFinishedOrCanceled [finished]: "

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    const-string v4, ", result "

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    move-object/from16 v0, p1

    #@53
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v3

    #@5b
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 2278
    :cond_5e
    invoke-virtual/range {p1 .. p1}, Landroid/content/SyncResult;->hasError()Z

    #@61
    move-result v2

    #@62
    if-nez v2, :cond_d1

    #@64
    .line 2279
    const-string/jumbo v6, "success"

    #@67
    .line 2281
    .local v6, historyMessage:Ljava/lang/String;
    const/4 v8, 0x0

    #@68
    .line 2282
    .local v8, downstreamActivity:I
    const/4 v7, 0x0

    #@69
    .line 2283
    .local v7, upstreamActivity:I
    move-object/from16 v0, p0

    #@6b
    iget-object v2, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@6d
    invoke-static {v2, v5}, Landroid/content/SyncManager;->access$3400(Landroid/content/SyncManager;Landroid/content/SyncOperation;)V

    #@70
    .line 2298
    :goto_70
    move-object/from16 v0, p0

    #@72
    iget-object v2, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@74
    move-object/from16 v0, p1

    #@76
    iget-wide v3, v0, Landroid/content/SyncResult;->delayUntil:J

    #@78
    invoke-static {v2, v5, v3, v4}, Landroid/content/SyncManager;->access$3500(Landroid/content/SyncManager;Landroid/content/SyncOperation;J)V

    #@7b
    .line 2315
    :goto_7b
    move-object/from16 v0, p2

    #@7d
    iget-wide v3, v0, Landroid/content/SyncManager$ActiveSyncContext;->mHistoryRowId:J

    #@7f
    move-object/from16 v2, p0

    #@81
    invoke-virtual/range {v2 .. v10}, Landroid/content/SyncManager$SyncHandler;->stopSyncEvent(JLandroid/content/SyncOperation;Ljava/lang/String;IIJ)V

    #@84
    .line 2318
    if-eqz p1, :cond_147

    #@86
    move-object/from16 v0, p1

    #@88
    iget-boolean v2, v0, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@8a
    if-eqz v2, :cond_147

    #@8c
    .line 2319
    iget-object v12, v5, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@8e
    iget-object v13, v5, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@90
    move-object/from16 v0, p1

    #@92
    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@94
    iget-wide v14, v2, Landroid/content/SyncStats;->numDeletes:J

    #@96
    iget v0, v5, Landroid/content/SyncOperation;->userId:I

    #@98
    move/from16 v16, v0

    #@9a
    move-object/from16 v11, p0

    #@9c
    invoke-direct/range {v11 .. v16}, Landroid/content/SyncManager$SyncHandler;->installHandleTooManyDeletesNotification(Landroid/accounts/Account;Ljava/lang/String;JI)V

    #@9f
    .line 2328
    :goto_9f
    if-eqz p1, :cond_d0

    #@a1
    move-object/from16 v0, p1

    #@a3
    iget-boolean v2, v0, Landroid/content/SyncResult;->fullSyncRequested:Z

    #@a5
    if-eqz v2, :cond_d0

    #@a7
    .line 2329
    move-object/from16 v0, p0

    #@a9
    iget-object v2, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@ab
    new-instance v11, Landroid/content/SyncOperation;

    #@ad
    iget-object v12, v5, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@af
    iget v13, v5, Landroid/content/SyncOperation;->userId:I

    #@b1
    iget v14, v5, Landroid/content/SyncOperation;->syncSource:I

    #@b3
    iget-object v15, v5, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@b5
    new-instance v16, Landroid/os/Bundle;

    #@b7
    invoke-direct/range {v16 .. v16}, Landroid/os/Bundle;-><init>()V

    #@ba
    const-wide/16 v17, 0x0

    #@bc
    iget-object v3, v5, Landroid/content/SyncOperation;->backoff:Ljava/lang/Long;

    #@be
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@c1
    move-result-wide v19

    #@c2
    iget-wide v0, v5, Landroid/content/SyncOperation;->delayUntil:J

    #@c4
    move-wide/from16 v21, v0

    #@c6
    iget-boolean v0, v5, Landroid/content/SyncOperation;->allowParallelSyncs:Z

    #@c8
    move/from16 v23, v0

    #@ca
    invoke-direct/range {v11 .. v23}, Landroid/content/SyncOperation;-><init>(Landroid/accounts/Account;IILjava/lang/String;Landroid/os/Bundle;JJJZ)V

    #@cd
    invoke-virtual {v2, v11}, Landroid/content/SyncManager;->scheduleSyncOperation(Landroid/content/SyncOperation;)V

    #@d0
    .line 2335
    :cond_d0
    return-void

    #@d1
    .line 2285
    .end local v6           #historyMessage:Ljava/lang/String;
    .end local v7           #upstreamActivity:I
    .end local v8           #downstreamActivity:I
    :cond_d1
    const-string v2, "SyncManager"

    #@d3
    new-instance v3, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v4, "failed sync operation "

    #@da
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v3

    #@de
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v3

    #@e2
    const-string v4, ", "

    #@e4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v3

    #@e8
    move-object/from16 v0, p1

    #@ea
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v3

    #@ee
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v3

    #@f2
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f5
    .line 2287
    move-object/from16 v0, p1

    #@f7
    iget-boolean v2, v0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@f9
    if-nez v2, :cond_102

    #@fb
    .line 2288
    move-object/from16 v0, p0

    #@fd
    iget-object v2, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@ff
    invoke-static {v2, v5}, Landroid/content/SyncManager;->access$3300(Landroid/content/SyncManager;Landroid/content/SyncOperation;)V

    #@102
    .line 2291
    :cond_102
    move-object/from16 v0, p0

    #@104
    iget-object v2, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@106
    move-object/from16 v0, p1

    #@108
    invoke-virtual {v2, v0, v5}, Landroid/content/SyncManager;->maybeRescheduleSync(Landroid/content/SyncResult;Landroid/content/SyncOperation;)V

    #@10b
    .line 2292
    invoke-direct/range {p0 .. p1}, Landroid/content/SyncManager$SyncHandler;->syncResultToErrorNumber(Landroid/content/SyncResult;)I

    #@10e
    move-result v2

    #@10f
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@112
    move-result-object v6

    #@113
    .line 2294
    .restart local v6       #historyMessage:Ljava/lang/String;
    const/4 v8, 0x0

    #@114
    .line 2295
    .restart local v8       #downstreamActivity:I
    const/4 v7, 0x0

    #@115
    .restart local v7       #upstreamActivity:I
    goto/16 :goto_70

    #@117
    .line 2300
    .end local v6           #historyMessage:Ljava/lang/String;
    .end local v7           #upstreamActivity:I
    .end local v8           #downstreamActivity:I
    :cond_117
    if-eqz v24, :cond_132

    #@119
    .line 2301
    const-string v2, "SyncManager"

    #@11b
    new-instance v3, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    const-string/jumbo v4, "runSyncFinishedOrCanceled [canceled]: "

    #@123
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v3

    #@127
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v3

    #@12b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12e
    move-result-object v3

    #@12f
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@132
    .line 2303
    :cond_132
    move-object/from16 v0, p2

    #@134
    iget-object v2, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncAdapter:Landroid/content/ISyncAdapter;

    #@136
    if-eqz v2, :cond_141

    #@138
    .line 2305
    :try_start_138
    move-object/from16 v0, p2

    #@13a
    iget-object v2, v0, Landroid/content/SyncManager$ActiveSyncContext;->mSyncAdapter:Landroid/content/ISyncAdapter;

    #@13c
    move-object/from16 v0, p2

    #@13e
    invoke-interface {v2, v0}, Landroid/content/ISyncAdapter;->cancelSync(Landroid/content/ISyncContext;)V
    :try_end_141
    .catch Landroid/os/RemoteException; {:try_start_138 .. :try_end_141} :catch_169

    #@141
    .line 2310
    :cond_141
    :goto_141
    const-string v6, "canceled"

    #@143
    .line 2311
    .restart local v6       #historyMessage:Ljava/lang/String;
    const/4 v8, 0x0

    #@144
    .line 2312
    .restart local v8       #downstreamActivity:I
    const/4 v7, 0x0

    #@145
    .restart local v7       #upstreamActivity:I
    goto/16 :goto_7b

    #@147
    .line 2323
    :cond_147
    move-object/from16 v0, p0

    #@149
    iget-object v2, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@14b
    invoke-static {v2}, Landroid/content/SyncManager;->access$3600(Landroid/content/SyncManager;)Landroid/app/NotificationManager;

    #@14e
    move-result-object v2

    #@14f
    const/4 v3, 0x0

    #@150
    iget-object v4, v5, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@152
    invoke-virtual {v4}, Landroid/accounts/Account;->hashCode()I

    #@155
    move-result v4

    #@156
    iget-object v11, v5, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@158
    invoke-virtual {v11}, Ljava/lang/String;->hashCode()I

    #@15b
    move-result v11

    #@15c
    xor-int/2addr v4, v11

    #@15d
    new-instance v11, Landroid/os/UserHandle;

    #@15f
    iget v12, v5, Landroid/content/SyncOperation;->userId:I

    #@161
    invoke-direct {v11, v12}, Landroid/os/UserHandle;-><init>(I)V

    #@164
    invoke-virtual {v2, v3, v4, v11}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@167
    goto/16 :goto_9f

    #@169
    .line 2306
    .end local v6           #historyMessage:Ljava/lang/String;
    .end local v7           #upstreamActivity:I
    .end local v8           #downstreamActivity:I
    :catch_169
    move-exception v2

    #@16a
    goto :goto_141
.end method

.method private scheduleReadyPeriodicSyncs()J
    .registers 43

    #@0
    .prologue
    .line 1820
    move-object/from16 v0, p0

    #@2
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@4
    invoke-static {v4}, Landroid/content/SyncManager;->access$300(Landroid/content/SyncManager;)Landroid/net/ConnectivityManager;

    #@7
    move-result-object v4

    #@8
    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    #@b
    move-result v19

    #@c
    .line 1822
    .local v19, backgroundDataUsageAllowed:Z
    const-wide v21, 0x7fffffffffffffffL

    #@11
    .line 1823
    .local v21, earliestFuturePollTime:J
    if-nez v19, :cond_16

    #@13
    move-wide/from16 v4, v21

    #@15
    .line 1914
    :goto_15
    return-wide v4

    #@16
    .line 1827
    :cond_16
    move-object/from16 v0, p0

    #@18
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@1a
    invoke-static {v4}, Landroid/content/SyncManager;->access$2500(Landroid/content/SyncManager;)[Landroid/accounts/AccountAndUser;

    #@1d
    move-result-object v18

    #@1e
    .line 1829
    .local v18, accounts:[Landroid/accounts/AccountAndUser;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@21
    move-result-wide v31

    #@22
    .line 1830
    .local v31, nowAbsolute:J
    const-wide/16 v4, 0x0

    #@24
    move-object/from16 v0, p0

    #@26
    iget-object v6, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@28
    invoke-static {v6}, Landroid/content/SyncManager;->access$2600(Landroid/content/SyncManager;)I

    #@2b
    move-result v6

    #@2c
    int-to-long v6, v6

    #@2d
    sub-long v6, v31, v6

    #@2f
    cmp-long v4, v4, v6

    #@31
    if-gez v4, :cond_161

    #@33
    move-object/from16 v0, p0

    #@35
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@37
    invoke-static {v4}, Landroid/content/SyncManager;->access$2600(Landroid/content/SyncManager;)I

    #@3a
    move-result v4

    #@3b
    int-to-long v4, v4

    #@3c
    sub-long v36, v31, v4

    #@3e
    .line 1833
    .local v36, shiftedNowAbsolute:J
    :goto_3e
    move-object/from16 v0, p0

    #@40
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@42
    invoke-static {v4}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Landroid/content/SyncStorageEngine;->getAuthorities()Ljava/util/ArrayList;

    #@49
    move-result-object v26

    #@4a
    .line 1834
    .local v26, infos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStorageEngine$AuthorityInfo;>;"
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@4d
    move-result-object v24

    #@4e
    .local v24, i$:Ljava/util/Iterator;
    :cond_4e
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    #@51
    move-result v4

    #@52
    if-eqz v4, :cond_1d1

    #@54
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@57
    move-result-object v25

    #@58
    check-cast v25, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@5a
    .line 1836
    .local v25, info:Landroid/content/SyncStorageEngine$AuthorityInfo;
    move-object/from16 v0, p0

    #@5c
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@5e
    move-object/from16 v0, v25

    #@60
    iget-object v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@62
    move-object/from16 v0, v25

    #@64
    iget v6, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@66
    move-object/from16 v0, v18

    #@68
    invoke-static {v4, v0, v5, v6}, Landroid/content/SyncManager;->access$2700(Landroid/content/SyncManager;[Landroid/accounts/AccountAndUser;Landroid/accounts/Account;I)Z

    #@6b
    move-result v4

    #@6c
    if-eqz v4, :cond_4e

    #@6e
    .line 1840
    move-object/from16 v0, p0

    #@70
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@72
    invoke-static {v4}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@75
    move-result-object v4

    #@76
    move-object/from16 v0, v25

    #@78
    iget v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@7a
    invoke-virtual {v4, v5}, Landroid/content/SyncStorageEngine;->getMasterSyncAutomatically(I)Z

    #@7d
    move-result v4

    #@7e
    if-eqz v4, :cond_4e

    #@80
    move-object/from16 v0, p0

    #@82
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@84
    invoke-static {v4}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@87
    move-result-object v4

    #@88
    move-object/from16 v0, v25

    #@8a
    iget-object v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@8c
    move-object/from16 v0, v25

    #@8e
    iget v6, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@90
    move-object/from16 v0, v25

    #@92
    iget-object v7, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@94
    invoke-virtual {v4, v5, v6, v7}, Landroid/content/SyncStorageEngine;->getSyncAutomatically(Landroid/accounts/Account;ILjava/lang/String;)Z

    #@97
    move-result v4

    #@98
    if-eqz v4, :cond_4e

    #@9a
    .line 1846
    move-object/from16 v0, p0

    #@9c
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@9e
    invoke-static {v4}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@a1
    move-result-object v4

    #@a2
    move-object/from16 v0, v25

    #@a4
    iget-object v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@a6
    move-object/from16 v0, v25

    #@a8
    iget v6, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@aa
    move-object/from16 v0, v25

    #@ac
    iget-object v7, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@ae
    invoke-virtual {v4, v5, v6, v7}, Landroid/content/SyncStorageEngine;->getIsSyncable(Landroid/accounts/Account;ILjava/lang/String;)I

    #@b1
    move-result v4

    #@b2
    if-eqz v4, :cond_4e

    #@b4
    .line 1851
    move-object/from16 v0, p0

    #@b6
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@b8
    invoke-static {v4}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@bb
    move-result-object v4

    #@bc
    move-object/from16 v0, v25

    #@be
    invoke-virtual {v4, v0}, Landroid/content/SyncStorageEngine;->getOrCreateSyncStatus(Landroid/content/SyncStorageEngine$AuthorityInfo;)Landroid/content/SyncStatusInfo;

    #@c1
    move-result-object v38

    #@c2
    .line 1852
    .local v38, status:Landroid/content/SyncStatusInfo;
    const/16 v23, 0x0

    #@c4
    .local v23, i:I
    move-object/from16 v0, v25

    #@c6
    iget-object v4, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@c8
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@cb
    move-result v17

    #@cc
    .local v17, N:I
    :goto_cc
    move/from16 v0, v23

    #@ce
    move/from16 v1, v17

    #@d0
    if-ge v0, v1, :cond_4e

    #@d2
    .line 1853
    move-object/from16 v0, v25

    #@d4
    iget-object v4, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@d6
    move/from16 v0, v23

    #@d8
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@db
    move-result-object v4

    #@dc
    check-cast v4, Landroid/util/Pair;

    #@de
    iget-object v9, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@e0
    check-cast v9, Landroid/os/Bundle;

    #@e2
    .line 1854
    .local v9, extras:Landroid/os/Bundle;
    move-object/from16 v0, v25

    #@e4
    iget-object v4, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@e6
    move/from16 v0, v23

    #@e8
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@eb
    move-result-object v4

    #@ec
    check-cast v4, Landroid/util/Pair;

    #@ee
    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@f0
    check-cast v4, Ljava/lang/Long;

    #@f2
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    #@f5
    move-result-wide v4

    #@f6
    const-wide/16 v6, 0x3e8

    #@f8
    mul-long/2addr v4, v6

    #@f9
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@fc
    move-result-object v33

    #@fd
    .line 1856
    .local v33, periodInMillis:Ljava/lang/Long;
    move-object/from16 v0, v38

    #@ff
    move/from16 v1, v23

    #@101
    invoke-virtual {v0, v1}, Landroid/content/SyncStatusInfo;->getPeriodicSyncTime(I)J

    #@104
    move-result-wide v27

    #@105
    .line 1858
    .local v27, lastPollTimeAbsolute:J
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Long;->longValue()J

    #@108
    move-result-wide v4

    #@109
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Long;->longValue()J

    #@10c
    move-result-wide v6

    #@10d
    rem-long v6, v36, v6

    #@10f
    sub-long v34, v4, v6

    #@111
    .line 1875
    .local v34, remainingMillis:J
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Long;->longValue()J

    #@114
    move-result-wide v4

    #@115
    cmp-long v4, v34, v4

    #@117
    if-eqz v4, :cond_127

    #@119
    cmp-long v4, v27, v31

    #@11b
    if-gtz v4, :cond_127

    #@11d
    sub-long v4, v31, v27

    #@11f
    invoke-virtual/range {v33 .. v33}, Ljava/lang/Long;->longValue()J

    #@122
    move-result-wide v6

    #@123
    cmp-long v4, v4, v6

    #@125
    if-ltz v4, :cond_1c5

    #@127
    .line 1880
    :cond_127
    move-object/from16 v0, p0

    #@129
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@12b
    invoke-static {v4}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@12e
    move-result-object v4

    #@12f
    move-object/from16 v0, v25

    #@131
    iget-object v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@133
    move-object/from16 v0, v25

    #@135
    iget v6, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@137
    move-object/from16 v0, v25

    #@139
    iget-object v7, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@13b
    invoke-virtual {v4, v5, v6, v7}, Landroid/content/SyncStorageEngine;->getBackoff(Landroid/accounts/Account;ILjava/lang/String;)Landroid/util/Pair;

    #@13e
    move-result-object v20

    #@13f
    .line 1883
    .local v20, backoff:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    #@141
    iget-object v4, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@143
    iget-object v4, v4, Landroid/content/SyncManager;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@145
    move-object/from16 v0, v25

    #@147
    iget-object v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@149
    move-object/from16 v0, v25

    #@14b
    iget-object v6, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@14d
    iget-object v6, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@14f
    invoke-static {v5, v6}, Landroid/content/SyncAdapterType;->newKey(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SyncAdapterType;

    #@152
    move-result-object v5

    #@153
    move-object/from16 v0, v25

    #@155
    iget v6, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@157
    invoke-virtual {v4, v5, v6}, Landroid/content/SyncAdaptersCache;->getServiceInfo(Ljava/lang/Object;I)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@15a
    move-result-object v39

    #@15b
    .line 1886
    .local v39, syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    if-nez v39, :cond_165

    #@15d
    .line 1852
    .end local v20           #backoff:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    .end local v39           #syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    :cond_15d
    :goto_15d
    add-int/lit8 v23, v23, 0x1

    #@15f
    goto/16 :goto_cc

    #@161
    .line 1830
    .end local v9           #extras:Landroid/os/Bundle;
    .end local v17           #N:I
    .end local v23           #i:I
    .end local v24           #i$:Ljava/util/Iterator;
    .end local v25           #info:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v26           #infos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStorageEngine$AuthorityInfo;>;"
    .end local v27           #lastPollTimeAbsolute:J
    .end local v33           #periodInMillis:Ljava/lang/Long;
    .end local v34           #remainingMillis:J
    .end local v36           #shiftedNowAbsolute:J
    .end local v38           #status:Landroid/content/SyncStatusInfo;
    :cond_161
    const-wide/16 v36, 0x0

    #@163
    goto/16 :goto_3e

    #@165
    .line 1889
    .restart local v9       #extras:Landroid/os/Bundle;
    .restart local v17       #N:I
    .restart local v20       #backoff:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    .restart local v23       #i:I
    .restart local v24       #i$:Ljava/util/Iterator;
    .restart local v25       #info:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v26       #infos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStorageEngine$AuthorityInfo;>;"
    .restart local v27       #lastPollTimeAbsolute:J
    .restart local v33       #periodInMillis:Ljava/lang/Long;
    .restart local v34       #remainingMillis:J
    .restart local v36       #shiftedNowAbsolute:J
    .restart local v38       #status:Landroid/content/SyncStatusInfo;
    .restart local v39       #syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    :cond_165
    move-object/from16 v0, p0

    #@167
    iget-object v0, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@169
    move-object/from16 v40, v0

    #@16b
    new-instance v4, Landroid/content/SyncOperation;

    #@16d
    move-object/from16 v0, v25

    #@16f
    iget-object v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@171
    move-object/from16 v0, v25

    #@173
    iget v6, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@175
    const/4 v7, 0x4

    #@176
    move-object/from16 v0, v25

    #@178
    iget-object v8, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@17a
    const-wide/16 v10, 0x0

    #@17c
    if-eqz v20, :cond_1ce

    #@17e
    move-object/from16 v0, v20

    #@180
    iget-object v12, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@182
    check-cast v12, Ljava/lang/Long;

    #@184
    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    #@187
    move-result-wide v12

    #@188
    :goto_188
    move-object/from16 v0, p0

    #@18a
    iget-object v14, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@18c
    invoke-static {v14}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@18f
    move-result-object v14

    #@190
    move-object/from16 v0, v25

    #@192
    iget-object v15, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@194
    move-object/from16 v0, v25

    #@196
    iget v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@198
    move/from16 v16, v0

    #@19a
    move-object/from16 v0, v25

    #@19c
    iget-object v0, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@19e
    move-object/from16 v41, v0

    #@1a0
    move/from16 v0, v16

    #@1a2
    move-object/from16 v1, v41

    #@1a4
    invoke-virtual {v14, v15, v0, v1}, Landroid/content/SyncStorageEngine;->getDelayUntilTime(Landroid/accounts/Account;ILjava/lang/String;)J

    #@1a7
    move-result-wide v14

    #@1a8
    move-object/from16 v0, v39

    #@1aa
    iget-object v0, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@1ac
    move-object/from16 v16, v0

    #@1ae
    check-cast v16, Landroid/content/SyncAdapterType;

    #@1b0
    invoke-virtual/range {v16 .. v16}, Landroid/content/SyncAdapterType;->allowParallelSyncs()Z

    #@1b3
    move-result v16

    #@1b4
    invoke-direct/range {v4 .. v16}, Landroid/content/SyncOperation;-><init>(Landroid/accounts/Account;IILjava/lang/String;Landroid/os/Bundle;JJJZ)V

    #@1b7
    move-object/from16 v0, v40

    #@1b9
    invoke-virtual {v0, v4}, Landroid/content/SyncManager;->scheduleSyncOperation(Landroid/content/SyncOperation;)V

    #@1bc
    .line 1897
    move-object/from16 v0, v38

    #@1be
    move/from16 v1, v23

    #@1c0
    move-wide/from16 v2, v31

    #@1c2
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/SyncStatusInfo;->setPeriodicSyncTime(IJ)V

    #@1c5
    .line 1900
    .end local v20           #backoff:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    .end local v39           #syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    :cond_1c5
    add-long v29, v31, v34

    #@1c7
    .line 1903
    .local v29, nextPollTimeAbsolute:J
    cmp-long v4, v29, v21

    #@1c9
    if-gez v4, :cond_15d

    #@1cb
    .line 1904
    move-wide/from16 v21, v29

    #@1cd
    goto :goto_15d

    #@1ce
    .line 1889
    .end local v29           #nextPollTimeAbsolute:J
    .restart local v20       #backoff:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    .restart local v39       #syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    :cond_1ce
    const-wide/16 v12, 0x0

    #@1d0
    goto :goto_188

    #@1d1
    .line 1909
    .end local v9           #extras:Landroid/os/Bundle;
    .end local v17           #N:I
    .end local v20           #backoff:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    .end local v23           #i:I
    .end local v25           #info:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v27           #lastPollTimeAbsolute:J
    .end local v33           #periodInMillis:Ljava/lang/Long;
    .end local v34           #remainingMillis:J
    .end local v38           #status:Landroid/content/SyncStatusInfo;
    .end local v39           #syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    :cond_1d1
    const-wide v4, 0x7fffffffffffffffL

    #@1d6
    cmp-long v4, v21, v4

    #@1d8
    if-nez v4, :cond_1e1

    #@1da
    .line 1910
    const-wide v4, 0x7fffffffffffffffL

    #@1df
    goto/16 :goto_15

    #@1e1
    .line 1914
    :cond_1e1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1e4
    move-result-wide v6

    #@1e5
    cmp-long v4, v21, v31

    #@1e7
    if-gez v4, :cond_1ee

    #@1e9
    const-wide/16 v4, 0x0

    #@1eb
    :goto_1eb
    add-long/2addr v4, v6

    #@1ec
    goto/16 :goto_15

    #@1ee
    :cond_1ee
    sub-long v4, v21, v31

    #@1f0
    goto :goto_1eb
.end method

.method private sendSyncStateIntent()V
    .registers 4

    #@0
    .prologue
    .line 2530
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.SYNC_STATE_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2531
    .local v0, syncStateIntent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 2532
    const-string v1, "active"

    #@e
    iget-object v2, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@10
    invoke-static {v2}, Landroid/content/SyncManager;->access$3800(Landroid/content/SyncManager;)Z

    #@13
    move-result v2

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@17
    .line 2533
    const-string v1, "failing"

    #@19
    const/4 v2, 0x0

    #@1a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1d
    .line 2534
    iget-object v1, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@1f
    invoke-static {v1}, Landroid/content/SyncManager;->access$1500(Landroid/content/SyncManager;)Landroid/content/Context;

    #@22
    move-result-object v1

    #@23
    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    #@25
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@28
    .line 2535
    return-void
.end method

.method private syncResultToErrorNumber(Landroid/content/SyncResult;)I
    .registers 6
    .parameter "syncResult"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 2354
    iget-boolean v0, p1, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 2355
    const/4 v0, 0x1

    #@7
    .line 2369
    :goto_7
    return v0

    #@8
    .line 2356
    :cond_8
    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@a
    iget-wide v0, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    #@c
    cmp-long v0, v0, v2

    #@e
    if-lez v0, :cond_12

    #@10
    .line 2357
    const/4 v0, 0x2

    #@11
    goto :goto_7

    #@12
    .line 2358
    :cond_12
    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@14
    iget-wide v0, v0, Landroid/content/SyncStats;->numIoExceptions:J

    #@16
    cmp-long v0, v0, v2

    #@18
    if-lez v0, :cond_1c

    #@1a
    .line 2359
    const/4 v0, 0x3

    #@1b
    goto :goto_7

    #@1c
    .line 2360
    :cond_1c
    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@1e
    iget-wide v0, v0, Landroid/content/SyncStats;->numParseExceptions:J

    #@20
    cmp-long v0, v0, v2

    #@22
    if-lez v0, :cond_26

    #@24
    .line 2361
    const/4 v0, 0x4

    #@25
    goto :goto_7

    #@26
    .line 2362
    :cond_26
    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@28
    iget-wide v0, v0, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    #@2a
    cmp-long v0, v0, v2

    #@2c
    if-lez v0, :cond_30

    #@2e
    .line 2363
    const/4 v0, 0x5

    #@2f
    goto :goto_7

    #@30
    .line 2364
    :cond_30
    iget-boolean v0, p1, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@32
    if-eqz v0, :cond_36

    #@34
    .line 2365
    const/4 v0, 0x6

    #@35
    goto :goto_7

    #@36
    .line 2366
    :cond_36
    iget-boolean v0, p1, Landroid/content/SyncResult;->tooManyRetries:Z

    #@38
    if-eqz v0, :cond_3c

    #@3a
    .line 2367
    const/4 v0, 0x7

    #@3b
    goto :goto_7

    #@3c
    .line 2368
    :cond_3c
    iget-boolean v0, p1, Landroid/content/SyncResult;->databaseError:Z

    #@3e
    if-eqz v0, :cond_43

    #@40
    .line 2369
    const/16 v0, 0x8

    #@42
    goto :goto_7

    #@43
    .line 2370
    :cond_43
    new-instance v0, Ljava/lang/IllegalStateException;

    #@45
    new-instance v1, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string/jumbo v2, "we are not in an error state, "

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@5c
    throw v0
.end method

.method private waitUntilReadyToRun()V
    .registers 4

    #@0
    .prologue
    .line 1656
    iget-object v1, p0, Landroid/content/SyncManager$SyncHandler;->mReadyToRunLatch:Ljava/util/concurrent/CountDownLatch;

    #@2
    .line 1657
    .local v1, latch:Ljava/util/concurrent/CountDownLatch;
    if-eqz v1, :cond_a

    #@4
    .line 1660
    :goto_4
    :try_start_4
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V

    #@7
    .line 1661
    const/4 v2, 0x0

    #@8
    iput-object v2, p0, Landroid/content/SyncManager$SyncHandler;->mReadyToRunLatch:Ljava/util/concurrent/CountDownLatch;
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_a} :catch_b

    #@a
    .line 1668
    :cond_a
    return-void

    #@b
    .line 1663
    :catch_b
    move-exception v0

    #@c
    .line 1664
    .local v0, e:Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    #@13
    goto :goto_4
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 18
    .parameter "msg"

    #@0
    .prologue
    .line 1697
    const-wide v2, 0x7fffffffffffffffL

    #@5
    .line 1698
    .local v2, earliestFuturePollTime:J
    const-wide v6, 0x7fffffffffffffffL

    #@a
    .line 1703
    .local v6, nextPendingSyncTime:J
    :try_start_a
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager$SyncHandler;->waitUntilReadyToRun()V

    #@d
    .line 1704
    move-object/from16 v0, p0

    #@f
    iget-object v11, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@11
    move-object/from16 v0, p0

    #@13
    iget-object v12, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@15
    invoke-static {v12}, Landroid/content/SyncManager;->access$500(Landroid/content/SyncManager;)Z

    #@18
    move-result v12

    #@19
    invoke-static {v11, v12}, Landroid/content/SyncManager;->access$402(Landroid/content/SyncManager;Z)Z

    #@1c
    .line 1705
    move-object/from16 v0, p0

    #@1e
    iget-object v11, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@20
    invoke-static {v11}, Landroid/content/SyncManager;->access$2300(Landroid/content/SyncManager;)Landroid/os/PowerManager$WakeLock;

    #@23
    move-result-object v11

    #@24
    invoke-virtual {v11}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@27
    .line 1710
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager$SyncHandler;->scheduleReadyPeriodicSyncs()J

    #@2a
    move-result-wide v2

    #@2b
    .line 1711
    move-object/from16 v0, p1

    #@2d
    iget v11, v0, Landroid/os/Message;->what:I
    :try_end_2f
    .catchall {:try_start_a .. :try_end_2f} :catchall_d8

    #@2f
    packed-switch v11, :pswitch_data_1e4

    #@32
    .line 1807
    :cond_32
    :goto_32
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager$SyncHandler;->manageSyncNotificationLocked()V

    #@35
    .line 1808
    move-object/from16 v0, p0

    #@37
    invoke-direct {v0, v2, v3, v6, v7}, Landroid/content/SyncManager$SyncHandler;->manageSyncAlarmLocked(JJ)V

    #@3a
    .line 1809
    move-object/from16 v0, p0

    #@3c
    iget-object v11, v0, Landroid/content/SyncManager$SyncHandler;->mSyncTimeTracker:Landroid/content/SyncManager$SyncTimeTracker;

    #@3e
    invoke-virtual {v11}, Landroid/content/SyncManager$SyncTimeTracker;->update()V

    #@41
    .line 1810
    move-object/from16 v0, p0

    #@43
    iget-object v11, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@45
    invoke-static {v11}, Landroid/content/SyncManager;->access$2300(Landroid/content/SyncManager;)Landroid/os/PowerManager$WakeLock;

    #@48
    move-result-object v11

    #@49
    invoke-virtual {v11}, Landroid/os/PowerManager$WakeLock;->release()V

    #@4c
    .line 1812
    return-void

    #@4d
    .line 1713
    :pswitch_4d
    :try_start_4d
    move-object/from16 v0, p1

    #@4f
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@51
    check-cast v9, Landroid/util/Pair;

    #@53
    .line 1714
    .local v9, payload:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/accounts/Account;Ljava/lang/String;>;"
    const-string v11, "SyncManager"

    #@55
    const/4 v12, 0x2

    #@56
    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@59
    move-result v11

    #@5a
    if-eqz v11, :cond_84

    #@5c
    .line 1715
    const-string v12, "SyncManager"

    #@5e
    new-instance v11, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v13, "handleSyncHandlerMessage: MESSAGE_SERVICE_CANCEL: "

    #@65
    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v11

    #@69
    iget-object v13, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@6b
    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v11

    #@6f
    const-string v13, ", "

    #@71
    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v13

    #@75
    iget-object v11, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@77
    check-cast v11, Ljava/lang/String;

    #@79
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v11

    #@7d
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v11

    #@81
    invoke-static {v12, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 1718
    :cond_84
    iget-object v11, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@86
    check-cast v11, Landroid/accounts/Account;

    #@88
    move-object/from16 v0, p1

    #@8a
    iget v13, v0, Landroid/os/Message;->arg1:I

    #@8c
    iget-object v12, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@8e
    check-cast v12, Ljava/lang/String;

    #@90
    move-object/from16 v0, p0

    #@92
    invoke-direct {v0, v11, v13, v12}, Landroid/content/SyncManager$SyncHandler;->cancelActiveSyncLocked(Landroid/accounts/Account;ILjava/lang/String;)V

    #@95
    .line 1719
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager$SyncHandler;->maybeStartNextSyncLocked()J

    #@98
    move-result-wide v6

    #@99
    .line 1720
    goto :goto_32

    #@9a
    .line 1724
    .end local v9           #payload:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/accounts/Account;Ljava/lang/String;>;"
    :pswitch_9a
    const-string v11, "SyncManager"

    #@9c
    const/4 v12, 0x2

    #@9d
    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@a0
    move-result v11

    #@a1
    if-eqz v11, :cond_aa

    #@a3
    .line 1725
    const-string v11, "SyncManager"

    #@a5
    const-string v12, "handleSyncHandlerMessage: MESSAGE_SYNC_FINISHED"

    #@a7
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 1727
    :cond_aa
    move-object/from16 v0, p1

    #@ac
    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ae
    check-cast v8, Landroid/content/SyncManager$SyncHandlerMessagePayload;

    #@b0
    .line 1728
    .local v8, payload:Landroid/content/SyncManager$SyncHandlerMessagePayload;
    move-object/from16 v0, p0

    #@b2
    iget-object v11, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@b4
    iget-object v12, v8, Landroid/content/SyncManager$SyncHandlerMessagePayload;->activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;

    #@b6
    invoke-static {v11, v12}, Landroid/content/SyncManager;->access$2400(Landroid/content/SyncManager;Landroid/content/SyncManager$ActiveSyncContext;)Z

    #@b9
    move-result v11

    #@ba
    if-nez v11, :cond_f4

    #@bc
    .line 1729
    const-string v11, "SyncManager"

    #@be
    new-instance v12, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v13, "handleSyncHandlerMessage: dropping since the sync is no longer active: "

    #@c5
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v12

    #@c9
    iget-object v13, v8, Landroid/content/SyncManager$SyncHandlerMessagePayload;->activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;

    #@cb
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v12

    #@cf
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v12

    #@d3
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d6
    .catchall {:try_start_4d .. :try_end_d6} :catchall_d8

    #@d6
    goto/16 :goto_32

    #@d8
    .line 1807
    .end local v8           #payload:Landroid/content/SyncManager$SyncHandlerMessagePayload;
    :catchall_d8
    move-exception v11

    #@d9
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager$SyncHandler;->manageSyncNotificationLocked()V

    #@dc
    .line 1808
    move-object/from16 v0, p0

    #@de
    invoke-direct {v0, v2, v3, v6, v7}, Landroid/content/SyncManager$SyncHandler;->manageSyncAlarmLocked(JJ)V

    #@e1
    .line 1809
    move-object/from16 v0, p0

    #@e3
    iget-object v12, v0, Landroid/content/SyncManager$SyncHandler;->mSyncTimeTracker:Landroid/content/SyncManager$SyncTimeTracker;

    #@e5
    invoke-virtual {v12}, Landroid/content/SyncManager$SyncTimeTracker;->update()V

    #@e8
    .line 1810
    move-object/from16 v0, p0

    #@ea
    iget-object v12, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@ec
    invoke-static {v12}, Landroid/content/SyncManager;->access$2300(Landroid/content/SyncManager;)Landroid/os/PowerManager$WakeLock;

    #@ef
    move-result-object v12

    #@f0
    invoke-virtual {v12}, Landroid/os/PowerManager$WakeLock;->release()V

    #@f3
    .line 1807
    throw v11

    #@f4
    .line 1734
    .restart local v8       #payload:Landroid/content/SyncManager$SyncHandlerMessagePayload;
    :cond_f4
    :try_start_f4
    iget-object v11, v8, Landroid/content/SyncManager$SyncHandlerMessagePayload;->syncResult:Landroid/content/SyncResult;

    #@f6
    iget-object v12, v8, Landroid/content/SyncManager$SyncHandlerMessagePayload;->activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;

    #@f8
    move-object/from16 v0, p0

    #@fa
    invoke-direct {v0, v11, v12}, Landroid/content/SyncManager$SyncHandler;->runSyncFinishedOrCanceledLocked(Landroid/content/SyncResult;Landroid/content/SyncManager$ActiveSyncContext;)V

    #@fd
    .line 1737
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager$SyncHandler;->maybeStartNextSyncLocked()J

    #@100
    move-result-wide v6

    #@101
    .line 1738
    goto/16 :goto_32

    #@103
    .line 1741
    .end local v8           #payload:Landroid/content/SyncManager$SyncHandlerMessagePayload;
    :pswitch_103
    move-object/from16 v0, p1

    #@105
    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@107
    check-cast v5, Landroid/content/SyncManager$ServiceConnectionData;

    #@109
    .line 1742
    .local v5, msgData:Landroid/content/SyncManager$ServiceConnectionData;
    const-string v11, "SyncManager"

    #@10b
    const/4 v12, 0x2

    #@10c
    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@10f
    move-result v11

    #@110
    if-eqz v11, :cond_12c

    #@112
    .line 1743
    const-string v11, "SyncManager"

    #@114
    new-instance v12, Ljava/lang/StringBuilder;

    #@116
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@119
    const-string v13, "handleSyncHandlerMessage: MESSAGE_SERVICE_CONNECTED: "

    #@11b
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v12

    #@11f
    iget-object v13, v5, Landroid/content/SyncManager$ServiceConnectionData;->activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;

    #@121
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v12

    #@125
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@128
    move-result-object v12

    #@129
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12c
    .line 1747
    :cond_12c
    move-object/from16 v0, p0

    #@12e
    iget-object v11, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@130
    iget-object v12, v5, Landroid/content/SyncManager$ServiceConnectionData;->activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;

    #@132
    invoke-static {v11, v12}, Landroid/content/SyncManager;->access$2400(Landroid/content/SyncManager;Landroid/content/SyncManager$ActiveSyncContext;)Z

    #@135
    move-result v11

    #@136
    if-eqz v11, :cond_32

    #@138
    .line 1748
    iget-object v11, v5, Landroid/content/SyncManager$ServiceConnectionData;->activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;

    #@13a
    iget-object v12, v5, Landroid/content/SyncManager$ServiceConnectionData;->syncAdapter:Landroid/content/ISyncAdapter;

    #@13c
    move-object/from16 v0, p0

    #@13e
    invoke-direct {v0, v11, v12}, Landroid/content/SyncManager$SyncHandler;->runBoundToSyncAdapter(Landroid/content/SyncManager$ActiveSyncContext;Landroid/content/ISyncAdapter;)V

    #@141
    goto/16 :goto_32

    #@143
    .line 1754
    .end local v5           #msgData:Landroid/content/SyncManager$ServiceConnectionData;
    :pswitch_143
    move-object/from16 v0, p1

    #@145
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@147
    check-cast v11, Landroid/content/SyncManager$ServiceConnectionData;

    #@149
    iget-object v1, v11, Landroid/content/SyncManager$ServiceConnectionData;->activeSyncContext:Landroid/content/SyncManager$ActiveSyncContext;

    #@14b
    .line 1756
    .local v1, currentSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    const-string v11, "SyncManager"

    #@14d
    const/4 v12, 0x2

    #@14e
    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@151
    move-result v11

    #@152
    if-eqz v11, :cond_16c

    #@154
    .line 1757
    const-string v11, "SyncManager"

    #@156
    new-instance v12, Ljava/lang/StringBuilder;

    #@158
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@15b
    const-string v13, "handleSyncHandlerMessage: MESSAGE_SERVICE_DISCONNECTED: "

    #@15d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v12

    #@161
    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v12

    #@165
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@168
    move-result-object v12

    #@169
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16c
    .line 1761
    :cond_16c
    move-object/from16 v0, p0

    #@16e
    iget-object v11, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@170
    invoke-static {v11, v1}, Landroid/content/SyncManager;->access$2400(Landroid/content/SyncManager;Landroid/content/SyncManager$ActiveSyncContext;)Z

    #@173
    move-result v11

    #@174
    if-eqz v11, :cond_32

    #@176
    .line 1764
    iget-object v11, v1, Landroid/content/SyncManager$ActiveSyncContext;->mSyncAdapter:Landroid/content/ISyncAdapter;
    :try_end_178
    .catchall {:try_start_f4 .. :try_end_178} :catchall_d8

    #@178
    if-eqz v11, :cond_17f

    #@17a
    .line 1766
    :try_start_17a
    iget-object v11, v1, Landroid/content/SyncManager$ActiveSyncContext;->mSyncAdapter:Landroid/content/ISyncAdapter;

    #@17c
    invoke-interface {v11, v1}, Landroid/content/ISyncAdapter;->cancelSync(Landroid/content/ISyncContext;)V
    :try_end_17f
    .catchall {:try_start_17a .. :try_end_17f} :catchall_d8
    .catch Landroid/os/RemoteException; {:try_start_17a .. :try_end_17f} :catch_1e1

    #@17f
    .line 1774
    :cond_17f
    :goto_17f
    :try_start_17f
    new-instance v10, Landroid/content/SyncResult;

    #@181
    invoke-direct {v10}, Landroid/content/SyncResult;-><init>()V

    #@184
    .line 1775
    .local v10, syncResult:Landroid/content/SyncResult;
    iget-object v11, v10, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@186
    iget-wide v12, v11, Landroid/content/SyncStats;->numIoExceptions:J

    #@188
    const-wide/16 v14, 0x1

    #@18a
    add-long/2addr v12, v14

    #@18b
    iput-wide v12, v11, Landroid/content/SyncStats;->numIoExceptions:J

    #@18d
    .line 1776
    move-object/from16 v0, p0

    #@18f
    invoke-direct {v0, v10, v1}, Landroid/content/SyncManager$SyncHandler;->runSyncFinishedOrCanceledLocked(Landroid/content/SyncResult;Landroid/content/SyncManager$ActiveSyncContext;)V

    #@192
    .line 1779
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager$SyncHandler;->maybeStartNextSyncLocked()J

    #@195
    move-result-wide v6

    #@196
    .line 1780
    goto/16 :goto_32

    #@198
    .line 1786
    .end local v1           #currentSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    .end local v10           #syncResult:Landroid/content/SyncResult;
    :pswitch_198
    const-string v11, "SyncManager"

    #@19a
    const/4 v12, 0x2

    #@19b
    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@19e
    move-result v4

    #@19f
    .line 1787
    .local v4, isLoggable:Z
    if-eqz v4, :cond_1a8

    #@1a1
    .line 1788
    const-string v11, "SyncManager"

    #@1a3
    const-string v12, "handleSyncHandlerMessage: MESSAGE_SYNC_ALARM"

    #@1a5
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a8
    .line 1790
    :cond_1a8
    const/4 v11, 0x0

    #@1a9
    move-object/from16 v0, p0

    #@1ab
    iput-object v11, v0, Landroid/content/SyncManager$SyncHandler;->mAlarmScheduleTime:Ljava/lang/Long;
    :try_end_1ad
    .catchall {:try_start_17f .. :try_end_1ad} :catchall_d8

    #@1ad
    .line 1792
    :try_start_1ad
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager$SyncHandler;->maybeStartNextSyncLocked()J
    :try_end_1b0
    .catchall {:try_start_1ad .. :try_end_1b0} :catchall_1be

    #@1b0
    move-result-wide v6

    #@1b1
    .line 1794
    :try_start_1b1
    move-object/from16 v0, p0

    #@1b3
    iget-object v11, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@1b5
    invoke-static {v11}, Landroid/content/SyncManager;->access$1100(Landroid/content/SyncManager;)Landroid/os/PowerManager$WakeLock;

    #@1b8
    move-result-object v11

    #@1b9
    invoke-virtual {v11}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1bc
    goto/16 :goto_32

    #@1be
    :catchall_1be
    move-exception v11

    #@1bf
    move-object/from16 v0, p0

    #@1c1
    iget-object v12, v0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@1c3
    invoke-static {v12}, Landroid/content/SyncManager;->access$1100(Landroid/content/SyncManager;)Landroid/os/PowerManager$WakeLock;

    #@1c6
    move-result-object v12

    #@1c7
    invoke-virtual {v12}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1ca
    throw v11

    #@1cb
    .line 1800
    .end local v4           #isLoggable:Z
    :pswitch_1cb
    const-string v11, "SyncManager"

    #@1cd
    const/4 v12, 0x2

    #@1ce
    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@1d1
    move-result v11

    #@1d2
    if-eqz v11, :cond_1db

    #@1d4
    .line 1801
    const-string v11, "SyncManager"

    #@1d6
    const-string v12, "handleSyncHandlerMessage: MESSAGE_CHECK_ALARMS"

    #@1d8
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1db
    .line 1803
    :cond_1db
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncManager$SyncHandler;->maybeStartNextSyncLocked()J
    :try_end_1de
    .catchall {:try_start_1b1 .. :try_end_1de} :catchall_d8

    #@1de
    move-result-wide v6

    #@1df
    goto/16 :goto_32

    #@1e1
    .line 1767
    .restart local v1       #currentSyncContext:Landroid/content/SyncManager$ActiveSyncContext;
    :catch_1e1
    move-exception v11

    #@1e2
    goto :goto_17f

    #@1e3
    .line 1711
    nop

    #@1e4
    :pswitch_data_1e4
    .packed-switch 0x1
        :pswitch_9a
        :pswitch_198
        :pswitch_1cb
        :pswitch_103
        :pswitch_143
        :pswitch_4d
    .end packed-switch
.end method

.method public insertStartSyncEvent(Landroid/content/SyncOperation;)J
    .registers 10
    .parameter "syncOperation"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2601
    iget v6, p1, Landroid/content/SyncOperation;->syncSource:I

    #@3
    .line 2602
    .local v6, source:I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@6
    move-result-wide v4

    #@7
    .line 2604
    .local v4, now:J
    const/16 v0, 0xaa0

    #@9
    const/4 v1, 0x4

    #@a
    new-array v1, v1, [Ljava/lang/Object;

    #@c
    iget-object v2, p1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@e
    aput-object v2, v1, v3

    #@10
    const/4 v2, 0x1

    #@11
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v3

    #@15
    aput-object v3, v1, v2

    #@17
    const/4 v2, 0x2

    #@18
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v3

    #@1c
    aput-object v3, v1, v2

    #@1e
    const/4 v2, 0x3

    #@1f
    iget-object v3, p1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@21
    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@23
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    #@26
    move-result v3

    #@27
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v3

    #@2b
    aput-object v3, v1, v2

    #@2d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@30
    .line 2608
    iget-object v0, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@32
    invoke-static {v0}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@35
    move-result-object v0

    #@36
    iget-object v1, p1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@38
    iget v2, p1, Landroid/content/SyncOperation;->userId:I

    #@3a
    iget-object v3, p1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@3c
    invoke-virtual {p1}, Landroid/content/SyncOperation;->isInitialization()Z

    #@3f
    move-result v7

    #@40
    invoke-virtual/range {v0 .. v7}, Landroid/content/SyncStorageEngine;->insertStartSyncEvent(Landroid/accounts/Account;ILjava/lang/String;JIZ)J

    #@43
    move-result-wide v0

    #@44
    return-wide v0
.end method

.method public onBootCompleted()V
    .registers 3

    #@0
    .prologue
    .line 1634
    iget-object v0, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {v0, v1}, Landroid/content/SyncManager;->access$2002(Landroid/content/SyncManager;Z)Z

    #@6
    .line 1636
    iget-object v0, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@8
    invoke-static {v0}, Landroid/content/SyncManager;->access$2100(Landroid/content/SyncManager;)V

    #@b
    .line 1638
    iget-object v0, p0, Landroid/content/SyncManager$SyncHandler;->mReadyToRunLatch:Ljava/util/concurrent/CountDownLatch;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 1639
    iget-object v0, p0, Landroid/content/SyncManager$SyncHandler;->mReadyToRunLatch:Ljava/util/concurrent/CountDownLatch;

    #@11
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@14
    .line 1641
    :cond_14
    return-void
.end method

.method public stopSyncEvent(JLandroid/content/SyncOperation;Ljava/lang/String;IIJ)V
    .registers 20
    .parameter "rowId"
    .parameter "syncOperation"
    .parameter "resultMessage"
    .parameter "upstreamActivity"
    .parameter "downstreamActivity"
    .parameter "elapsedTime"

    #@0
    .prologue
    .line 2615
    const/16 v1, 0xaa0

    #@2
    const/4 v2, 0x4

    #@3
    new-array v2, v2, [Ljava/lang/Object;

    #@5
    const/4 v3, 0x0

    #@6
    iget-object v4, p3, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@8
    aput-object v4, v2, v3

    #@a
    const/4 v3, 0x1

    #@b
    const/4 v4, 0x1

    #@c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v4

    #@10
    aput-object v4, v2, v3

    #@12
    const/4 v3, 0x2

    #@13
    iget v4, p3, Landroid/content/SyncOperation;->syncSource:I

    #@15
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v4

    #@19
    aput-object v4, v2, v3

    #@1b
    const/4 v3, 0x3

    #@1c
    iget-object v4, p3, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@1e
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@20
    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    #@23
    move-result v4

    #@24
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v4

    #@28
    aput-object v4, v2, v3

    #@2a
    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@2d
    .line 2619
    iget-object v1, p0, Landroid/content/SyncManager$SyncHandler;->this$0:Landroid/content/SyncManager;

    #@2f
    invoke-static {v1}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@32
    move-result-object v1

    #@33
    move/from16 v0, p6

    #@35
    int-to-long v7, v0

    #@36
    move/from16 v0, p5

    #@38
    int-to-long v9, v0

    #@39
    move-wide v2, p1

    #@3a
    move-wide/from16 v4, p7

    #@3c
    move-object v6, p4

    #@3d
    invoke-virtual/range {v1 .. v10}, Landroid/content/SyncStorageEngine;->stopSyncEvent(JJLjava/lang/String;JJ)V

    #@40
    .line 2621
    return-void
.end method
