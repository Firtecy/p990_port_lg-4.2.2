.class Landroid/content/AbstractThreadedSyncAdapter$SyncThread;
.super Ljava/lang/Thread;
.source "AbstractThreadedSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/AbstractThreadedSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncThread"
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mAuthority:Ljava/lang/String;

.field private final mExtras:Landroid/os/Bundle;

.field private final mSyncContext:Landroid/content/SyncContext;

.field private final mThreadsKey:Landroid/accounts/Account;

.field final synthetic this$0:Landroid/content/AbstractThreadedSyncAdapter;


# direct methods
.method private constructor <init>(Landroid/content/AbstractThreadedSyncAdapter;Ljava/lang/String;Landroid/content/SyncContext;Ljava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;)V
    .registers 8
    .parameter
    .parameter "name"
    .parameter "syncContext"
    .parameter "authority"
    .parameter "account"
    .parameter "extras"

    #@0
    .prologue
    .line 228
    iput-object p1, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@2
    .line 229
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@5
    .line 230
    iput-object p3, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mSyncContext:Landroid/content/SyncContext;

    #@7
    .line 231
    iput-object p4, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mAuthority:Ljava/lang/String;

    #@9
    .line 232
    iput-object p5, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mAccount:Landroid/accounts/Account;

    #@b
    .line 233
    iput-object p6, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mExtras:Landroid/os/Bundle;

    #@d
    .line 234
    invoke-static {p1, p5}, Landroid/content/AbstractThreadedSyncAdapter;->access$100(Landroid/content/AbstractThreadedSyncAdapter;Landroid/accounts/Account;)Landroid/accounts/Account;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mThreadsKey:Landroid/accounts/Account;

    #@13
    .line 235
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/AbstractThreadedSyncAdapter;Ljava/lang/String;Landroid/content/SyncContext;Ljava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/AbstractThreadedSyncAdapter$1;)V
    .registers 8
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"

    #@0
    .prologue
    .line 220
    invoke-direct/range {p0 .. p6}, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;-><init>(Landroid/content/AbstractThreadedSyncAdapter;Ljava/lang/String;Landroid/content/SyncContext;Ljava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/content/AbstractThreadedSyncAdapter$SyncThread;)Landroid/content/SyncContext;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mSyncContext:Landroid/content/SyncContext;

    #@2
    return-object v0
.end method

.method private isCanceled()Z
    .registers 2

    #@0
    .prologue
    .line 277
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    #@7
    move-result v0

    #@8
    return v0
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    const-wide/16 v6, 0x80

    #@2
    .line 239
    const/16 v0, 0xa

    #@4
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@7
    .line 244
    iget-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mAuthority:Ljava/lang/String;

    #@9
    invoke-static {v6, v7, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@c
    .line 246
    new-instance v5, Landroid/content/SyncResult;

    #@e
    invoke-direct {v5}, Landroid/content/SyncResult;-><init>()V

    #@11
    .line 247
    .local v5, syncResult:Landroid/content/SyncResult;
    const/4 v4, 0x0

    #@12
    .line 249
    .local v4, provider:Landroid/content/ContentProviderClient;
    :try_start_12
    invoke-direct {p0}, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->isCanceled()Z
    :try_end_15
    .catchall {:try_start_12 .. :try_end_15} :catchall_8a

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_3f

    #@18
    .line 260
    invoke-static {v6, v7}, Landroid/os/Trace;->traceEnd(J)V

    #@1b
    .line 262
    if-eqz v4, :cond_20

    #@1d
    .line 263
    #Replaced unresolvable odex instruction with a throw
    throw v4
    #invoke-virtual-quick {v4}, vtable@0x18

    #@20
    .line 265
    :cond_20
    invoke-direct {p0}, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->isCanceled()Z

    #@23
    move-result v0

    #@24
    if-nez v0, :cond_2b

    #@26
    .line 266
    iget-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mSyncContext:Landroid/content/SyncContext;

    #@28
    invoke-virtual {v0, v5}, Landroid/content/SyncContext;->onFinished(Landroid/content/SyncResult;)V

    #@2b
    .line 270
    :cond_2b
    iget-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@2d
    invoke-static {v0}, Landroid/content/AbstractThreadedSyncAdapter;->access$200(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/lang/Object;

    #@30
    move-result-object v1

    #@31
    monitor-enter v1

    #@32
    .line 271
    :try_start_32
    iget-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@34
    invoke-static {v0}, Landroid/content/AbstractThreadedSyncAdapter;->access$300(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/util/HashMap;

    #@37
    move-result-object v0

    #@38
    iget-object v2, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mThreadsKey:Landroid/accounts/Account;

    #@3a
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 272
    monitor-exit v1
    :try_end_3e
    .catchall {:try_start_32 .. :try_end_3e} :catchall_b5

    #@3e
    .line 274
    :goto_3e
    return-void

    #@3f
    .line 252
    :cond_3f
    :try_start_3f
    iget-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@41
    invoke-static {v0}, Landroid/content/AbstractThreadedSyncAdapter;->access$900(Landroid/content/AbstractThreadedSyncAdapter;)Landroid/content/Context;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@48
    move-result-object v0

    #@49
    iget-object v1, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mAuthority:Ljava/lang/String;

    #@4b
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    #@4e
    move-result-object v4

    #@4f
    .line 253
    if-eqz v4, :cond_86

    #@51
    .line 254
    iget-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@53
    iget-object v1, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mAccount:Landroid/accounts/Account;

    #@55
    iget-object v2, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mExtras:Landroid/os/Bundle;

    #@57
    iget-object v3, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mAuthority:Ljava/lang/String;

    #@59
    invoke-virtual/range {v0 .. v5}, Landroid/content/AbstractThreadedSyncAdapter;->onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    :try_end_5c
    .catchall {:try_start_3f .. :try_end_5c} :catchall_8a

    #@5c
    .line 260
    :goto_5c
    invoke-static {v6, v7}, Landroid/os/Trace;->traceEnd(J)V

    #@5f
    .line 262
    if-eqz v4, :cond_64

    #@61
    .line 263
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    #@64
    .line 265
    :cond_64
    invoke-direct {p0}, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->isCanceled()Z

    #@67
    move-result v0

    #@68
    if-nez v0, :cond_6f

    #@6a
    .line 266
    iget-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mSyncContext:Landroid/content/SyncContext;

    #@6c
    invoke-virtual {v0, v5}, Landroid/content/SyncContext;->onFinished(Landroid/content/SyncResult;)V

    #@6f
    .line 270
    :cond_6f
    iget-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@71
    invoke-static {v0}, Landroid/content/AbstractThreadedSyncAdapter;->access$200(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/lang/Object;

    #@74
    move-result-object v1

    #@75
    monitor-enter v1

    #@76
    .line 271
    :try_start_76
    iget-object v0, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@78
    invoke-static {v0}, Landroid/content/AbstractThreadedSyncAdapter;->access$300(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/util/HashMap;

    #@7b
    move-result-object v0

    #@7c
    iget-object v2, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mThreadsKey:Landroid/accounts/Account;

    #@7e
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@81
    .line 272
    monitor-exit v1

    #@82
    goto :goto_3e

    #@83
    :catchall_83
    move-exception v0

    #@84
    monitor-exit v1
    :try_end_85
    .catchall {:try_start_76 .. :try_end_85} :catchall_83

    #@85
    throw v0

    #@86
    .line 257
    :cond_86
    const/4 v0, 0x1

    #@87
    :try_start_87
    iput-boolean v0, v5, Landroid/content/SyncResult;->databaseError:Z
    :try_end_89
    .catchall {:try_start_87 .. :try_end_89} :catchall_8a

    #@89
    goto :goto_5c

    #@8a
    .line 260
    :catchall_8a
    move-exception v0

    #@8b
    invoke-static {v6, v7}, Landroid/os/Trace;->traceEnd(J)V

    #@8e
    .line 262
    if-eqz v4, :cond_93

    #@90
    .line 263
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    #@93
    .line 265
    :cond_93
    invoke-direct {p0}, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->isCanceled()Z

    #@96
    move-result v1

    #@97
    if-nez v1, :cond_9e

    #@99
    .line 266
    iget-object v1, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mSyncContext:Landroid/content/SyncContext;

    #@9b
    invoke-virtual {v1, v5}, Landroid/content/SyncContext;->onFinished(Landroid/content/SyncResult;)V

    #@9e
    .line 270
    :cond_9e
    iget-object v1, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@a0
    invoke-static {v1}, Landroid/content/AbstractThreadedSyncAdapter;->access$200(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/lang/Object;

    #@a3
    move-result-object v1

    #@a4
    monitor-enter v1

    #@a5
    .line 271
    :try_start_a5
    iget-object v2, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@a7
    invoke-static {v2}, Landroid/content/AbstractThreadedSyncAdapter;->access$300(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/util/HashMap;

    #@aa
    move-result-object v2

    #@ab
    iget-object v3, p0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->mThreadsKey:Landroid/accounts/Account;

    #@ad
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@b0
    .line 272
    monitor-exit v1
    :try_end_b1
    .catchall {:try_start_a5 .. :try_end_b1} :catchall_b2

    #@b1
    .line 260
    throw v0

    #@b2
    .line 272
    :catchall_b2
    move-exception v0

    #@b3
    :try_start_b3
    monitor-exit v1
    :try_end_b4
    .catchall {:try_start_b3 .. :try_end_b4} :catchall_b2

    #@b4
    throw v0

    #@b5
    :catchall_b5
    move-exception v0

    #@b6
    :try_start_b6
    monitor-exit v1
    :try_end_b7
    .catchall {:try_start_b6 .. :try_end_b7} :catchall_b5

    #@b7
    throw v0
.end method
