.class Landroid/content/SyncManager$8;
.super Ljava/lang/Object;
.source "SyncManager.java"

# interfaces
.implements Landroid/content/SyncStorageEngine$OnSyncRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/content/SyncManager;-><init>(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/content/SyncManager;


# direct methods
.method constructor <init>(Landroid/content/SyncManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 353
    iput-object p1, p0, Landroid/content/SyncManager$8;->this$0:Landroid/content/SyncManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onSyncRequest(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 13
    .parameter "account"
    .parameter "userId"
    .parameter "authority"
    .parameter "extras"

    #@0
    .prologue
    .line 356
    iget-object v0, p0, Landroid/content/SyncManager$8;->this$0:Landroid/content/SyncManager;

    #@2
    const-wide/16 v5, 0x0

    #@4
    const/4 v7, 0x0

    #@5
    move-object v1, p1

    #@6
    move v2, p2

    #@7
    move-object v3, p3

    #@8
    move-object v4, p4

    #@9
    invoke-virtual/range {v0 .. v7}, Landroid/content/SyncManager;->scheduleSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;JZ)V

    #@c
    .line 357
    return-void
.end method
