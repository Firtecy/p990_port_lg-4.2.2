.class public final Landroid/content/Entity;
.super Ljava/lang/Object;
.source "Entity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/Entity$NamedContentValues;
    }
.end annotation


# instance fields
.field private final mSubValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;"
        }
    .end annotation
.end field

.field private final mValues:Landroid/content/ContentValues;


# direct methods
.method public constructor <init>(Landroid/content/ContentValues;)V
    .registers 3
    .parameter "values"

    #@0
    .prologue
    .line 37
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 38
    iput-object p1, p0, Landroid/content/Entity;->mValues:Landroid/content/ContentValues;

    #@5
    .line 39
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/content/Entity;->mSubValues:Ljava/util/ArrayList;

    #@c
    .line 40
    return-void
.end method


# virtual methods
.method public addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .registers 5
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Landroid/content/Entity;->mSubValues:Ljava/util/ArrayList;

    #@2
    new-instance v1, Landroid/content/Entity$NamedContentValues;

    #@4
    invoke-direct {v1, p1, p2}, Landroid/content/Entity$NamedContentValues;-><init>(Landroid/net/Uri;Landroid/content/ContentValues;)V

    #@7
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a
    .line 52
    return-void
.end method

.method public getEntityValues()Landroid/content/ContentValues;
    .registers 2

    #@0
    .prologue
    .line 43
    iget-object v0, p0, Landroid/content/Entity;->mValues:Landroid/content/ContentValues;

    #@2
    return-object v0
.end method

.method public getSubValues()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Landroid/content/Entity;->mSubValues:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 66
    .local v2, sb:Ljava/lang/StringBuilder;
    const-string v3, "Entity: "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p0}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    .line 67
    invoke-virtual {p0}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v0

    #@1a
    .local v0, i$:Ljava/util/Iterator;
    :goto_1a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_3d

    #@20
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Landroid/content/Entity$NamedContentValues;

    #@26
    .line 68
    .local v1, namedValue:Landroid/content/Entity$NamedContentValues;
    const-string v3, "\n  "

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    iget-object v4, v1, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    .line 69
    const-string v3, "\n  -> "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    iget-object v4, v1, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    goto :goto_1a

    #@3d
    .line 71
    .end local v1           #namedValue:Landroid/content/Entity$NamedContentValues;
    :cond_3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    return-object v3
.end method
