.class Landroid/content/ISyncContext$Stub$Proxy;
.super Ljava/lang/Object;
.source "ISyncContext.java"

# interfaces
.implements Landroid/content/ISyncContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/ISyncContext$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 77
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 78
    iput-object p1, p0, Landroid/content/ISyncContext$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 79
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/content/ISyncContext$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 86
    const-string v0, "android.content.ISyncContext"

    #@2
    return-object v0
.end method

.method public onFinished(Landroid/content/SyncResult;)V
    .registers 7
    .parameter "result"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 113
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 114
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 116
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.content.ISyncContext"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 117
    if-eqz p1, :cond_28

    #@f
    .line 118
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 119
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/content/SyncResult;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 124
    :goto_17
    iget-object v2, p0, Landroid/content/ISyncContext$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v3, 0x2

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 125
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2d

    #@21
    .line 128
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 129
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 131
    return-void

    #@28
    .line 122
    :cond_28
    const/4 v2, 0x0

    #@29
    :try_start_29
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_2d

    #@2c
    goto :goto_17

    #@2d
    .line 128
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 129
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method

.method public sendHeartbeat()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 95
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 96
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 98
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.content.ISyncContext"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 99
    iget-object v2, p0, Landroid/content/ISyncContext$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x1

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 100
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 103
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 104
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 106
    return-void

    #@1e
    .line 103
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 104
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method
