.class public abstract Landroid/content/ContentProvider;
.super Ljava/lang/Object;
.source "ContentProvider.java"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/ContentProvider$PipeDataWriter;,
        Landroid/content/ContentProvider$Transport;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ContentProvider"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mExported:Z

.field private mMyUid:I

.field private mPathPermissions:[Landroid/content/pm/PathPermission;

.field private mReadPermission:Ljava/lang/String;

.field private mTransport:Landroid/content/ContentProvider$Transport;

.field private mWritePermission:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 121
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 97
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/content/ContentProvider;->mContext:Landroid/content/Context;

    #@6
    .line 104
    new-instance v0, Landroid/content/ContentProvider$Transport;

    #@8
    invoke-direct {v0, p0}, Landroid/content/ContentProvider$Transport;-><init>(Landroid/content/ContentProvider;)V

    #@b
    iput-object v0, p0, Landroid/content/ContentProvider;->mTransport:Landroid/content/ContentProvider$Transport;

    #@d
    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Landroid/content/pm/PathPermission;)V
    .registers 6
    .parameter "context"
    .parameter "readPermission"
    .parameter "writePermission"
    .parameter "pathPermissions"

    #@0
    .prologue
    .line 141
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 97
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/content/ContentProvider;->mContext:Landroid/content/Context;

    #@6
    .line 104
    new-instance v0, Landroid/content/ContentProvider$Transport;

    #@8
    invoke-direct {v0, p0}, Landroid/content/ContentProvider$Transport;-><init>(Landroid/content/ContentProvider;)V

    #@b
    iput-object v0, p0, Landroid/content/ContentProvider;->mTransport:Landroid/content/ContentProvider$Transport;

    #@d
    .line 142
    iput-object p1, p0, Landroid/content/ContentProvider;->mContext:Landroid/content/Context;

    #@f
    .line 143
    iput-object p2, p0, Landroid/content/ContentProvider;->mReadPermission:Ljava/lang/String;

    #@11
    .line 144
    iput-object p3, p0, Landroid/content/ContentProvider;->mWritePermission:Ljava/lang/String;

    #@13
    .line 145
    iput-object p4, p0, Landroid/content/ContentProvider;->mPathPermissions:[Landroid/content/pm/PathPermission;

    #@15
    .line 146
    return-void
.end method

.method static synthetic access$000(Landroid/content/ContentProvider;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget v0, p0, Landroid/content/ContentProvider;->mMyUid:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/content/ContentProvider;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-boolean v0, p0, Landroid/content/ContentProvider;->mExported:Z

    #@2
    return v0
.end method

.method public static coerceToLocalContentProvider(Landroid/content/IContentProvider;)Landroid/content/ContentProvider;
    .registers 2
    .parameter "abstractInterface"

    #@0
    .prologue
    .line 163
    instance-of v0, p0, Landroid/content/ContentProvider$Transport;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 164
    check-cast p0, Landroid/content/ContentProvider$Transport;

    #@6
    .end local p0
    invoke-virtual {p0}, Landroid/content/ContentProvider$Transport;->getContentProvider()Landroid/content/ContentProvider;

    #@9
    move-result-object v0

    #@a
    .line 166
    :goto_a
    return-object v0

    #@b
    .restart local p0
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    #@0
    .prologue
    .line 1081
    .local p1, operations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v1

    #@4
    .line 1082
    .local v1, numOperations:I
    new-array v2, v1, [Landroid/content/ContentProviderResult;

    #@6
    .line 1083
    .local v2, results:[Landroid/content/ContentProviderResult;
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_18

    #@9
    .line 1084
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Landroid/content/ContentProviderOperation;

    #@f
    invoke-virtual {v3, p0, v2, v0}, Landroid/content/ContentProviderOperation;->apply(Landroid/content/ContentProvider;[Landroid/content/ContentProviderResult;I)Landroid/content/ContentProviderResult;

    #@12
    move-result-object v3

    #@13
    aput-object v3, v2, v0

    #@15
    .line 1083
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_7

    #@18
    .line 1086
    :cond_18
    return-object v2
.end method

.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .registers 4
    .parameter "context"
    .parameter "info"

    #@0
    .prologue
    .line 1043
    invoke-static {}, Landroid/os/AsyncTask;->init()V

    #@3
    .line 1049
    iget-object v0, p0, Landroid/content/ContentProvider;->mContext:Landroid/content/Context;

    #@5
    if-nez v0, :cond_27

    #@7
    .line 1050
    iput-object p1, p0, Landroid/content/ContentProvider;->mContext:Landroid/content/Context;

    #@9
    .line 1051
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/content/ContentProvider;->mMyUid:I

    #@f
    .line 1052
    if-eqz p2, :cond_24

    #@11
    .line 1053
    iget-object v0, p2, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    #@13
    invoke-virtual {p0, v0}, Landroid/content/ContentProvider;->setReadPermission(Ljava/lang/String;)V

    #@16
    .line 1054
    iget-object v0, p2, Landroid/content/pm/ProviderInfo;->writePermission:Ljava/lang/String;

    #@18
    invoke-virtual {p0, v0}, Landroid/content/ContentProvider;->setWritePermission(Ljava/lang/String;)V

    #@1b
    .line 1055
    iget-object v0, p2, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    #@1d
    invoke-virtual {p0, v0}, Landroid/content/ContentProvider;->setPathPermissions([Landroid/content/pm/PathPermission;)V

    #@20
    .line 1056
    iget-boolean v0, p2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@22
    iput-boolean v0, p0, Landroid/content/ContentProvider;->mExported:Z

    #@24
    .line 1058
    :cond_24
    invoke-virtual {p0}, Landroid/content/ContentProvider;->onCreate()Z

    #@27
    .line 1060
    :cond_27
    return-void
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .registers 6
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    .line 703
    array-length v1, p2

    #@1
    .line 704
    .local v1, numValues:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, v1, :cond_c

    #@4
    .line 705
    aget-object v2, p2, v0

    #@6
    invoke-virtual {p0, p1, v2}, Landroid/content/ContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@9
    .line 704
    add-int/lit8 v0, v0, 0x1

    #@b
    goto :goto_2

    #@c
    .line 707
    :cond_c
    return v1
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 5
    .parameter "method"
    .parameter "arg"
    .parameter "extras"

    #@0
    .prologue
    .line 1101
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public abstract delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 5
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 1143
    const-string/jumbo v0, "nothing to dump"

    #@3
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6
    .line 1144
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 405
    iget-object v0, p0, Landroid/content/ContentProvider;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getIContentProvider()Landroid/content/IContentProvider;
    .registers 2

    #@0
    .prologue
    .line 1028
    iget-object v0, p0, Landroid/content/ContentProvider;->mTransport:Landroid/content/ContentProvider$Transport;

    #@2
    return-object v0
.end method

.method public final getPathPermissions()[Landroid/content/pm/PathPermission;
    .registers 2

    #@0
    .prologue
    .line 471
    iget-object v0, p0, Landroid/content/ContentProvider;->mPathPermissions:[Landroid/content/pm/PathPermission;

    #@2
    return-object v0
.end method

.method public final getReadPermission()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 427
    iget-object v0, p0, Landroid/content/ContentProvider;->mReadPermission:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;
    .registers 4
    .parameter "uri"
    .parameter "mimeTypeFilter"

    #@0
    .prologue
    .line 895
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public abstract getType(Landroid/net/Uri;)Ljava/lang/String;
.end method

.method public final getWritePermission()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 449
    iget-object v0, p0, Landroid/content/ContentProvider;->mWritePermission:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public abstract insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
.end method

.method protected isTemporary()Z
    .registers 2

    #@0
    .prologue
    .line 1018
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter "newConfig"

    #@0
    .prologue
    .line 512
    return-void
.end method

.method public abstract onCreate()Z
.end method

.method public onLowMemory()V
    .registers 1

    #@0
    .prologue
    .line 523
    return-void
.end method

.method public onTrimMemory(I)V
    .registers 2
    .parameter "level"

    #@0
    .prologue
    .line 526
    return-void
.end method

.method public openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .registers 9
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 829
    invoke-virtual {p0, p1, p2}, Landroid/content/ContentProvider;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@3
    move-result-object v1

    #@4
    .line 830
    .local v1, fd:Landroid/os/ParcelFileDescriptor;
    if-eqz v1, :cond_10

    #@6
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    #@8
    const-wide/16 v2, 0x0

    #@a
    const-wide/16 v4, -0x1

    #@c
    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V

    #@f
    :goto_f
    return-object v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 6
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 785
    new-instance v0, Ljava/io/FileNotFoundException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "No files supported by provider at "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0
.end method

.method protected final openFileHelper(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 15
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 849
    new-array v2, v11, [Ljava/lang/String;

    #@5
    const-string v0, "_data"

    #@7
    aput-object v0, v2, v7

    #@9
    move-object v0, p0

    #@a
    move-object v1, p1

    #@b
    move-object v4, v3

    #@c
    move-object v5, v3

    #@d
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@10
    move-result-object v6

    #@11
    .line 850
    .local v6, c:Landroid/database/Cursor;
    if-eqz v6, :cond_17

    #@13
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@16
    move-result v7

    #@17
    .line 851
    .local v7, count:I
    :cond_17
    if-eq v7, v11, :cond_52

    #@19
    .line 854
    if-eqz v6, :cond_1e

    #@1b
    .line 855
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@1e
    .line 857
    :cond_1e
    if-nez v7, :cond_39

    #@20
    .line 858
    new-instance v0, Ljava/io/FileNotFoundException;

    #@22
    new-instance v1, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v2, "No entry for "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@38
    throw v0

    #@39
    .line 860
    :cond_39
    new-instance v0, Ljava/io/FileNotFoundException;

    #@3b
    new-instance v1, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v2, "Multiple items at "

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@51
    throw v0

    #@52
    .line 863
    :cond_52
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@55
    .line 864
    const-string v0, "_data"

    #@57
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@5a
    move-result v8

    #@5b
    .line 865
    .local v8, i:I
    if-ltz v8, :cond_6e

    #@5d
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@60
    move-result-object v10

    #@61
    .line 866
    .local v10, path:Ljava/lang/String;
    :goto_61
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@64
    .line 867
    if-nez v10, :cond_70

    #@66
    .line 868
    new-instance v0, Ljava/io/FileNotFoundException;

    #@68
    const-string v1, "Column _data not found."

    #@6a
    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@6d
    throw v0

    #@6e
    .end local v10           #path:Ljava/lang/String;
    :cond_6e
    move-object v10, v3

    #@6f
    .line 865
    goto :goto_61

    #@70
    .line 871
    .restart local v10       #path:Ljava/lang/String;
    :cond_70
    invoke-static {p1, p2}, Landroid/content/ContentResolver;->modeToMode(Landroid/net/Uri;Ljava/lang/String;)I

    #@73
    move-result v9

    #@74
    .line 872
    .local v9, modeBits:I
    new-instance v0, Ljava/io/File;

    #@76
    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@79
    invoke-static {v0, v9}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@7c
    move-result-object v0

    #@7d
    return-object v0
.end method

.method public openPipeHelper(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Object;Landroid/content/ContentProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    .registers 15
    .parameter "uri"
    .parameter "mimeType"
    .parameter "opts"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "TT;",
            "Landroid/content/ContentProvider$PipeDataWriter",
            "<TT;>;)",
            "Landroid/os/ParcelFileDescriptor;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 991
    .local p4, args:Ljava/lang/Object;,"TT;"
    .local p5, func:Landroid/content/ContentProvider$PipeDataWriter;,"Landroid/content/ContentProvider$PipeDataWriter<TT;>;"
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    #@3
    move-result-object v3

    #@4
    .line 993
    .local v3, fds:[Landroid/os/ParcelFileDescriptor;
    new-instance v0, Landroid/content/ContentProvider$1;

    #@6
    move-object v1, p0

    #@7
    move-object v2, p5

    #@8
    move-object v4, p1

    #@9
    move-object v5, p2

    #@a
    move-object v6, p3

    #@b
    move-object v7, p4

    #@c
    invoke-direct/range {v0 .. v7}, Landroid/content/ContentProvider$1;-><init>(Landroid/content/ContentProvider;Landroid/content/ContentProvider$PipeDataWriter;[Landroid/os/ParcelFileDescriptor;Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Object;)V

    #@f
    .line 1005
    .local v0, task:Landroid/os/AsyncTask;,"Landroid/os/AsyncTask<Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;>;"
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    #@11
    const/4 v1, 0x0

    #@12
    check-cast v1, [Ljava/lang/Object;

    #@14
    invoke-virtual {v0, v2, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@17
    .line 1007
    const/4 v1, 0x0

    #@18
    aget-object v1, v3, v1
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_1a} :catch_1b

    #@1a
    return-object v1

    #@1b
    .line 1008
    .end local v0           #task:Landroid/os/AsyncTask;,"Landroid/os/AsyncTask<Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;>;"
    .end local v3           #fds:[Landroid/os/ParcelFileDescriptor;
    :catch_1b
    move-exception v8

    #@1c
    .line 1009
    .local v8, e:Ljava/io/IOException;
    new-instance v1, Ljava/io/FileNotFoundException;

    #@1e
    const-string v2, "failure making pipe"

    #@20
    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@23
    throw v1
.end method

.method public openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
    .registers 8
    .parameter "uri"
    .parameter "mimeTypeFilter"
    .parameter "opts"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 935
    const-string v1, "*/*"

    #@2
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_10

    #@8
    .line 937
    const-string/jumbo v1, "r"

    #@b
    invoke-virtual {p0, p1, v1}, Landroid/content/ContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@e
    move-result-object v1

    #@f
    .line 943
    :goto_f
    return-object v1

    #@10
    .line 939
    :cond_10
    invoke-virtual {p0, p1}, Landroid/content/ContentProvider;->getType(Landroid/net/Uri;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 940
    .local v0, baseType:Ljava/lang/String;
    if-eqz v0, :cond_24

    #@16
    invoke-static {v0, p2}, Landroid/content/ClipDescription;->compareMimeTypes(Ljava/lang/String;Ljava/lang/String;)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_24

    #@1c
    .line 943
    const-string/jumbo v1, "r"

    #@1f
    invoke-virtual {p0, p1, v1}, Landroid/content/ContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@22
    move-result-object v1

    #@23
    goto :goto_f

    #@24
    .line 945
    :cond_24
    new-instance v1, Ljava/io/FileNotFoundException;

    #@26
    new-instance v2, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v3, "Can\'t open "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    const-string v3, " as type "

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@46
    throw v1
.end method

.method public abstract query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .registers 8
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"
    .parameter "cancellationSignal"

    #@0
    .prologue
    .line 652
    invoke-virtual/range {p0 .. p5}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected final setPathPermissions([Landroid/content/pm/PathPermission;)V
    .registers 2
    .parameter "permissions"

    #@0
    .prologue
    .line 460
    iput-object p1, p0, Landroid/content/ContentProvider;->mPathPermissions:[Landroid/content/pm/PathPermission;

    #@2
    .line 461
    return-void
.end method

.method protected final setReadPermission(Ljava/lang/String;)V
    .registers 2
    .parameter "permission"

    #@0
    .prologue
    .line 416
    iput-object p1, p0, Landroid/content/ContentProvider;->mReadPermission:Ljava/lang/String;

    #@2
    .line 417
    return-void
.end method

.method protected final setWritePermission(Ljava/lang/String;)V
    .registers 2
    .parameter "permission"

    #@0
    .prologue
    .line 438
    iput-object p1, p0, Landroid/content/ContentProvider;->mWritePermission:Ljava/lang/String;

    #@2
    .line 439
    return-void
.end method

.method public shutdown()V
    .registers 3

    #@0
    .prologue
    .line 1127
    const-string v0, "ContentProvider"

    #@2
    const-string v1, "implement ContentProvider shutdown() to make sure all database connections are gracefully shutdown"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1129
    return-void
.end method

.method public abstract update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
.end method
