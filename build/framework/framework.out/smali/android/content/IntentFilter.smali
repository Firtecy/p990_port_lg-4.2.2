.class public Landroid/content/IntentFilter;
.super Ljava/lang/Object;
.source "IntentFilter.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/IntentFilter$AuthorityEntry;,
        Landroid/content/IntentFilter$MalformedMimeTypeException;
    }
.end annotation


# static fields
.field private static final ACTION_STR:Ljava/lang/String; = "action"

.field private static final AUTH_STR:Ljava/lang/String; = "auth"

.field private static final CAT_STR:Ljava/lang/String; = "cat"

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field

.field private static final HOST_STR:Ljava/lang/String; = "host"

.field private static final LITERAL_STR:Ljava/lang/String; = "literal"

.field public static final MATCH_ADJUSTMENT_MASK:I = 0xffff

.field public static final MATCH_ADJUSTMENT_NORMAL:I = 0x8000

.field public static final MATCH_CATEGORY_EMPTY:I = 0x100000

.field public static final MATCH_CATEGORY_HOST:I = 0x300000

.field public static final MATCH_CATEGORY_MASK:I = 0xfff0000

.field public static final MATCH_CATEGORY_PATH:I = 0x500000

.field public static final MATCH_CATEGORY_PORT:I = 0x400000

.field public static final MATCH_CATEGORY_SCHEME:I = 0x200000

.field public static final MATCH_CATEGORY_TYPE:I = 0x600000

.field private static final NAME_STR:Ljava/lang/String; = "name"

.field public static final NO_MATCH_ACTION:I = -0x3

.field public static final NO_MATCH_CATEGORY:I = -0x4

.field public static final NO_MATCH_DATA:I = -0x2

.field public static final NO_MATCH_TYPE:I = -0x1

.field private static final PATH_STR:Ljava/lang/String; = "path"

.field private static final PORT_STR:Ljava/lang/String; = "port"

.field private static final PREFIX_STR:Ljava/lang/String; = "prefix"

.field private static final SCHEME_STR:Ljava/lang/String; = "scheme"

.field private static final SGLOB_STR:Ljava/lang/String; = "sglob"

.field public static final SYSTEM_HIGH_PRIORITY:I = 0x3e8

.field public static final SYSTEM_LOW_PRIORITY:I = -0x3e8

.field private static final TYPE_STR:Ljava/lang/String; = "type"


# instance fields
.field private final mActions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCategories:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDataAuthorities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter$AuthorityEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mDataPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/PatternMatcher;",
            ">;"
        }
    .end annotation
.end field

.field private mDataSchemes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDataTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHasPartialTypes:Z

.field private mPriority:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1330
    new-instance v0, Landroid/content/IntentFilter$1;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/IntentFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 338
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 236
    iput-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@7
    .line 237
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@9
    .line 238
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@b
    .line 239
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@d
    .line 240
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@f
    .line 241
    iput-boolean v1, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@11
    .line 339
    iput v1, p0, Landroid/content/IntentFilter;->mPriority:I

    #@13
    .line 340
    new-instance v0, Ljava/util/ArrayList;

    #@15
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@18
    iput-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@1a
    .line 341
    return-void
.end method

.method public constructor <init>(Landroid/content/IntentFilter;)V
    .registers 4
    .parameter "o"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 385
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 236
    iput-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@6
    .line 237
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@8
    .line 238
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@a
    .line 239
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@c
    .line 240
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@e
    .line 241
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@11
    .line 386
    iget v0, p1, Landroid/content/IntentFilter;->mPriority:I

    #@13
    iput v0, p0, Landroid/content/IntentFilter;->mPriority:I

    #@15
    .line 387
    new-instance v0, Ljava/util/ArrayList;

    #@17
    iget-object v1, p1, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@19
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@1c
    iput-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@1e
    .line 388
    iget-object v0, p1, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@20
    if-eqz v0, :cond_2b

    #@22
    .line 389
    new-instance v0, Ljava/util/ArrayList;

    #@24
    iget-object v1, p1, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@26
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@29
    iput-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@2b
    .line 391
    :cond_2b
    iget-object v0, p1, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@2d
    if-eqz v0, :cond_38

    #@2f
    .line 392
    new-instance v0, Ljava/util/ArrayList;

    #@31
    iget-object v1, p1, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@33
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@36
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@38
    .line 394
    :cond_38
    iget-object v0, p1, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@3a
    if-eqz v0, :cond_45

    #@3c
    .line 395
    new-instance v0, Ljava/util/ArrayList;

    #@3e
    iget-object v1, p1, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@40
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@43
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@45
    .line 397
    :cond_45
    iget-object v0, p1, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@47
    if-eqz v0, :cond_52

    #@49
    .line 398
    new-instance v0, Ljava/util/ArrayList;

    #@4b
    iget-object v1, p1, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@4d
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@50
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@52
    .line 400
    :cond_52
    iget-object v0, p1, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@54
    if-eqz v0, :cond_5f

    #@56
    .line 401
    new-instance v0, Ljava/util/ArrayList;

    #@58
    iget-object v1, p1, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@5a
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@5d
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@5f
    .line 403
    :cond_5f
    iget-boolean v0, p1, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@61
    iput-boolean v0, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@63
    .line 404
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 7
    .parameter "source"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 1413
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 236
    iput-object v3, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@7
    .line 237
    iput-object v3, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@9
    .line 238
    iput-object v3, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@b
    .line 239
    iput-object v3, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@d
    .line 240
    iput-object v3, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@f
    .line 241
    iput-boolean v2, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@11
    .line 1414
    new-instance v3, Ljava/util/ArrayList;

    #@13
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v3, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@18
    .line 1415
    iget-object v3, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    #@1d
    .line 1416
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_2f

    #@23
    .line 1417
    new-instance v3, Ljava/util/ArrayList;

    #@25
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@28
    iput-object v3, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@2a
    .line 1418
    iget-object v3, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    #@2f
    .line 1420
    :cond_2f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_41

    #@35
    .line 1421
    new-instance v3, Ljava/util/ArrayList;

    #@37
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@3a
    iput-object v3, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@3c
    .line 1422
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@3e
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    #@41
    .line 1424
    :cond_41
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v3

    #@45
    if-eqz v3, :cond_53

    #@47
    .line 1425
    new-instance v3, Ljava/util/ArrayList;

    #@49
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@4c
    iput-object v3, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@4e
    .line 1426
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@50
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    #@53
    .line 1428
    :cond_53
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@56
    move-result v0

    #@57
    .line 1429
    .local v0, N:I
    if-lez v0, :cond_70

    #@59
    .line 1430
    new-instance v3, Ljava/util/ArrayList;

    #@5b
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@5e
    iput-object v3, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@60
    .line 1431
    const/4 v1, 0x0

    #@61
    .local v1, i:I
    :goto_61
    if-ge v1, v0, :cond_70

    #@63
    .line 1432
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@65
    new-instance v4, Landroid/content/IntentFilter$AuthorityEntry;

    #@67
    invoke-direct {v4, p1}, Landroid/content/IntentFilter$AuthorityEntry;-><init>(Landroid/os/Parcel;)V

    #@6a
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6d
    .line 1431
    add-int/lit8 v1, v1, 0x1

    #@6f
    goto :goto_61

    #@70
    .line 1435
    .end local v1           #i:I
    :cond_70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v0

    #@74
    .line 1436
    if-lez v0, :cond_8d

    #@76
    .line 1437
    new-instance v3, Ljava/util/ArrayList;

    #@78
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@7b
    iput-object v3, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@7d
    .line 1438
    const/4 v1, 0x0

    #@7e
    .restart local v1       #i:I
    :goto_7e
    if-ge v1, v0, :cond_8d

    #@80
    .line 1439
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@82
    new-instance v4, Landroid/os/PatternMatcher;

    #@84
    invoke-direct {v4, p1}, Landroid/os/PatternMatcher;-><init>(Landroid/os/Parcel;)V

    #@87
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8a
    .line 1438
    add-int/lit8 v1, v1, 0x1

    #@8c
    goto :goto_7e

    #@8d
    .line 1442
    .end local v1           #i:I
    :cond_8d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@90
    move-result v3

    #@91
    iput v3, p0, Landroid/content/IntentFilter;->mPriority:I

    #@93
    .line 1443
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@96
    move-result v3

    #@97
    if-lez v3, :cond_9a

    #@99
    const/4 v2, 0x1

    #@9a
    :cond_9a
    iput-boolean v2, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@9c
    .line 1444
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/IntentFilter$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 129
    invoke-direct {p0, p1}, Landroid/content/IntentFilter;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "action"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 350
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 236
    iput-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@7
    .line 237
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@9
    .line 238
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@b
    .line 239
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@d
    .line 240
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@f
    .line 241
    iput-boolean v1, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@11
    .line 351
    iput v1, p0, Landroid/content/IntentFilter;->mPriority:I

    #@13
    .line 352
    new-instance v0, Ljava/util/ArrayList;

    #@15
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@18
    iput-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@1a
    .line 353
    invoke-virtual {p0, p1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d
    .line 354
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "action"
    .parameter "dataType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentFilter$MalformedMimeTypeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 373
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 236
    iput-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@7
    .line 237
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@9
    .line 238
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@b
    .line 239
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@d
    .line 240
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@f
    .line 241
    iput-boolean v1, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@11
    .line 374
    iput v1, p0, Landroid/content/IntentFilter;->mPriority:I

    #@13
    .line 375
    new-instance v0, Ljava/util/ArrayList;

    #@15
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@18
    iput-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@1a
    .line 376
    invoke-virtual {p0, p1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d
    .line 377
    invoke-virtual {p0, p2}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    #@20
    .line 378
    return-void
.end method

.method private static addStringToSet([Ljava/lang/String;Ljava/lang/String;[II)[Ljava/lang/String;
    .registers 8
    .parameter "set"
    .parameter "string"
    .parameter "lengths"
    .parameter "lenPos"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 258
    invoke-static {p0, p1, p2, p3}, Landroid/content/IntentFilter;->findStringInSet([Ljava/lang/String;Ljava/lang/String;[II)I

    #@4
    move-result v2

    #@5
    if-ltz v2, :cond_8

    #@7
    .line 277
    :goto_7
    return-object p0

    #@8
    .line 259
    :cond_8
    if-nez p0, :cond_13

    #@a
    .line 260
    const/4 v2, 0x2

    #@b
    new-array p0, v2, [Ljava/lang/String;

    #@d
    .line 261
    aput-object p1, p0, v3

    #@f
    .line 262
    const/4 v2, 0x1

    #@10
    aput v2, p2, p3

    #@12
    goto :goto_7

    #@13
    .line 265
    :cond_13
    aget v0, p2, p3

    #@15
    .line 266
    .local v0, N:I
    array-length v2, p0

    #@16
    if-ge v0, v2, :cond_1f

    #@18
    .line 267
    aput-object p1, p0, v0

    #@1a
    .line 268
    add-int/lit8 v2, v0, 0x1

    #@1c
    aput v2, p2, p3

    #@1e
    goto :goto_7

    #@1f
    .line 272
    :cond_1f
    mul-int/lit8 v2, v0, 0x3

    #@21
    div-int/lit8 v2, v2, 0x2

    #@23
    add-int/lit8 v2, v2, 0x2

    #@25
    new-array v1, v2, [Ljava/lang/String;

    #@27
    .line 273
    .local v1, newSet:[Ljava/lang/String;
    invoke-static {p0, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2a
    .line 274
    move-object p0, v1

    #@2b
    .line 275
    aput-object p1, p0, v0

    #@2d
    .line 276
    add-int/lit8 v2, v0, 0x1

    #@2f
    aput v2, p2, p3

    #@31
    goto :goto_7
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;)Landroid/content/IntentFilter;
    .registers 5
    .parameter "action"
    .parameter "dataType"

    #@0
    .prologue
    .line 329
    :try_start_0
    new-instance v1, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v1, p0, p1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    return-object v1

    #@6
    .line 330
    :catch_6
    move-exception v0

    #@7
    .line 331
    .local v0, e:Landroid/content/IntentFilter$MalformedMimeTypeException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@9
    const-string v2, "Bad MIME type"

    #@b
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@e
    throw v1
.end method

.method private final findMimeType(Ljava/lang/String;)Z
    .registers 12
    .parameter "type"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 1447
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@4
    .line 1449
    .local v3, t:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_8

    #@6
    move v6, v7

    #@7
    .line 1486
    :cond_7
    :goto_7
    return v6

    #@8
    .line 1453
    :cond_8
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@b
    move-result v8

    #@c
    if-nez v8, :cond_7

    #@e
    .line 1458
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@11
    move-result v4

    #@12
    .line 1459
    .local v4, typeLength:I
    const/4 v8, 0x3

    #@13
    if-ne v4, v8, :cond_25

    #@15
    const-string v8, "*/*"

    #@17
    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v8

    #@1b
    if-eqz v8, :cond_25

    #@1d
    .line 1460
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@20
    move-result v8

    #@21
    if-eqz v8, :cond_7

    #@23
    move v6, v7

    #@24
    goto :goto_7

    #@25
    .line 1464
    :cond_25
    iget-boolean v8, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@27
    if-eqz v8, :cond_31

    #@29
    const-string v8, "*"

    #@2b
    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@2e
    move-result v8

    #@2f
    if-nez v8, :cond_7

    #@31
    .line 1468
    :cond_31
    const/16 v8, 0x2f

    #@33
    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    #@36
    move-result v2

    #@37
    .line 1469
    .local v2, slashpos:I
    if-lez v2, :cond_6d

    #@39
    .line 1470
    iget-boolean v8, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@3b
    if-eqz v8, :cond_47

    #@3d
    invoke-virtual {p1, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@44
    move-result v8

    #@45
    if-nez v8, :cond_7

    #@47
    .line 1473
    :cond_47
    add-int/lit8 v8, v2, 0x2

    #@49
    if-ne v4, v8, :cond_6d

    #@4b
    add-int/lit8 v8, v2, 0x1

    #@4d
    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    #@50
    move-result v8

    #@51
    const/16 v9, 0x2a

    #@53
    if-ne v8, v9, :cond_6d

    #@55
    .line 1476
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@58
    move-result v1

    #@59
    .line 1477
    .local v1, numTypes:I
    const/4 v0, 0x0

    #@5a
    .local v0, i:I
    :goto_5a
    if-ge v0, v1, :cond_6d

    #@5c
    .line 1478
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5f
    move-result-object v5

    #@60
    check-cast v5, Ljava/lang/String;

    #@62
    .line 1479
    .local v5, v:Ljava/lang/String;
    add-int/lit8 v8, v2, 0x1

    #@64
    invoke-virtual {p1, v7, v5, v7, v8}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@67
    move-result v8

    #@68
    if-nez v8, :cond_7

    #@6a
    .line 1477
    add-int/lit8 v0, v0, 0x1

    #@6c
    goto :goto_5a

    #@6d
    .end local v0           #i:I
    .end local v1           #numTypes:I
    .end local v5           #v:Ljava/lang/String;
    :cond_6d
    move v6, v7

    #@6e
    .line 1486
    goto :goto_7
.end method

.method private static findStringInSet([Ljava/lang/String;Ljava/lang/String;[II)I
    .registers 8
    .parameter "set"
    .parameter "string"
    .parameter "lengths"
    .parameter "lenPos"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 248
    if-nez p0, :cond_5

    #@3
    move v1, v2

    #@4
    .line 253
    :cond_4
    :goto_4
    return v1

    #@5
    .line 249
    :cond_5
    aget v0, p2, p3

    #@7
    .line 250
    .local v0, N:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_15

    #@a
    .line 251
    aget-object v3, p0, v1

    #@c
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v3

    #@10
    if-nez v3, :cond_4

    #@12
    .line 250
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_8

    #@15
    :cond_15
    move v1, v2

    #@16
    .line 253
    goto :goto_4
.end method

.method private static removeStringFromSet([Ljava/lang/String;Ljava/lang/String;[II)[Ljava/lang/String;
    .registers 10
    .parameter "set"
    .parameter "string"
    .parameter "lengths"
    .parameter "lenPos"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 282
    invoke-static {p0, p1, p2, p3}, Landroid/content/IntentFilter;->findStringInSet([Ljava/lang/String;Ljava/lang/String;[II)I

    #@4
    move-result v3

    #@5
    .line 283
    .local v3, pos:I
    if-gez v3, :cond_8

    #@7
    .line 298
    .end local p0
    :goto_7
    return-object p0

    #@8
    .line 284
    .restart local p0
    :cond_8
    aget v0, p2, p3

    #@a
    .line 285
    .local v0, N:I
    array-length v4, p0

    #@b
    div-int/lit8 v4, v4, 0x4

    #@d
    if-le v0, v4, :cond_24

    #@f
    .line 286
    add-int/lit8 v4, v3, 0x1

    #@11
    sub-int v1, v0, v4

    #@13
    .line 287
    .local v1, copyLen:I
    if-lez v1, :cond_1a

    #@15
    .line 288
    add-int/lit8 v4, v3, 0x1

    #@17
    invoke-static {p0, v4, p0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1a
    .line 290
    :cond_1a
    add-int/lit8 v4, v0, -0x1

    #@1c
    const/4 v5, 0x0

    #@1d
    aput-object v5, p0, v4

    #@1f
    .line 291
    add-int/lit8 v4, v0, -0x1

    #@21
    aput v4, p2, p3

    #@23
    goto :goto_7

    #@24
    .line 295
    .end local v1           #copyLen:I
    :cond_24
    array-length v4, p0

    #@25
    div-int/lit8 v4, v4, 0x3

    #@27
    new-array v2, v4, [Ljava/lang/String;

    #@29
    .line 296
    .local v2, newSet:[Ljava/lang/String;
    if-lez v3, :cond_2e

    #@2b
    invoke-static {p0, v5, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2e
    .line 297
    :cond_2e
    add-int/lit8 v4, v3, 0x1

    #@30
    if-ge v4, v0, :cond_3b

    #@32
    add-int/lit8 v4, v3, 0x1

    #@34
    add-int/lit8 v5, v3, 0x1

    #@36
    sub-int v5, v0, v5

    #@38
    invoke-static {p0, v4, v2, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3b
    :cond_3b
    move-object p0, v2

    #@3c
    .line 298
    goto :goto_7
.end method


# virtual methods
.method public final actionsIterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 490
    iget-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public final addAction(Ljava/lang/String;)V
    .registers 4
    .parameter "action"

    #@0
    .prologue
    .line 442
    iget-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_11

    #@8
    .line 443
    iget-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@a
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11
    .line 445
    :cond_11
    return-void
.end method

.method public final addCategory(Ljava/lang/String;)V
    .registers 4
    .parameter "category"

    #@0
    .prologue
    .line 974
    iget-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_b

    #@4
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@b
    .line 975
    :cond_b
    iget-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_1c

    #@13
    .line 976
    iget-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@15
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 978
    :cond_1c
    return-void
.end method

.method public final addDataAuthority(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "host"
    .parameter "port"

    #@0
    .prologue
    .line 722
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_b

    #@4
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@b
    .line 724
    :cond_b
    if-eqz p2, :cond_11

    #@d
    invoke-virtual {p2}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@10
    move-result-object p2

    #@11
    .line 725
    :cond_11
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@13
    new-instance v1, Landroid/content/IntentFilter$AuthorityEntry;

    #@15
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-direct {v1, v2, p2}, Landroid/content/IntentFilter$AuthorityEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1f
    .line 726
    return-void
.end method

.method public final addDataPath(Ljava/lang/String;I)V
    .registers 6
    .parameter "path"
    .parameter "type"

    #@0
    .prologue
    .line 790
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_b

    #@4
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@b
    .line 791
    :cond_b
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@d
    new-instance v1, Landroid/os/PatternMatcher;

    #@f
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-direct {v1, v2, p2}, Landroid/os/PatternMatcher;-><init>(Ljava/lang/String;I)V

    #@16
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@19
    .line 792
    return-void
.end method

.method public final addDataScheme(Ljava/lang/String;)V
    .registers 4
    .parameter "scheme"

    #@0
    .prologue
    .line 585
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_b

    #@4
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@b
    .line 586
    :cond_b
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_1c

    #@13
    .line 587
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@15
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 589
    :cond_1c
    return-void
.end method

.method public final addDataType(Ljava/lang/String;)V
    .registers 7
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentFilter$MalformedMimeTypeException;
        }
    .end annotation

    #@0
    .prologue
    .line 514
    const/16 v3, 0x2f

    #@2
    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    #@5
    move-result v0

    #@6
    .line 515
    .local v0, slashpos:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@9
    move-result v2

    #@a
    .line 516
    .local v2, typelen:I
    if-lez v0, :cond_55

    #@c
    add-int/lit8 v3, v0, 0x2

    #@e
    if-lt v2, v3, :cond_55

    #@10
    .line 517
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@12
    if-nez v3, :cond_1b

    #@14
    new-instance v3, Ljava/util/ArrayList;

    #@16
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@19
    iput-object v3, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@1b
    .line 518
    :cond_1b
    add-int/lit8 v3, v0, 0x2

    #@1d
    if-ne v2, v3, :cond_43

    #@1f
    add-int/lit8 v3, v0, 0x1

    #@21
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@24
    move-result v3

    #@25
    const/16 v4, 0x2a

    #@27
    if-ne v3, v4, :cond_43

    #@29
    .line 519
    const/4 v3, 0x0

    #@2a
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    .line 520
    .local v1, str:Ljava/lang/String;
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@33
    move-result v3

    #@34
    if-nez v3, :cond_3f

    #@36
    .line 521
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3f
    .line 523
    :cond_3f
    const/4 v3, 0x1

    #@40
    iput-boolean v3, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@42
    .line 529
    .end local v1           #str:Ljava/lang/String;
    :cond_42
    :goto_42
    return-void

    #@43
    .line 525
    :cond_43
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@45
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@48
    move-result v3

    #@49
    if-nez v3, :cond_42

    #@4b
    .line 526
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@4d
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@54
    goto :goto_42

    #@55
    .line 532
    :cond_55
    new-instance v3, Landroid/content/IntentFilter$MalformedMimeTypeException;

    #@57
    invoke-direct {v3, p1}, Landroid/content/IntentFilter$MalformedMimeTypeException;-><init>(Ljava/lang/String;)V

    #@5a
    throw v3
.end method

.method public final authoritiesIterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Landroid/content/IntentFilter$AuthorityEntry;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 760
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public final categoriesIterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1011
    iget-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public final countActions()I
    .registers 2

    #@0
    .prologue
    .line 451
    iget-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final countCategories()I
    .registers 2

    #@0
    .prologue
    .line 984
    iget-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public final countDataAuthorities()I
    .registers 2

    #@0
    .prologue
    .line 732
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public final countDataPaths()I
    .registers 2

    #@0
    .prologue
    .line 798
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public final countDataSchemes()I
    .registers 2

    #@0
    .prologue
    .line 595
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public final countDataTypes()I
    .registers 2

    #@0
    .prologue
    .line 551
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public debugCheck()Z
    .registers 2

    #@0
    .prologue
    .line 1394
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public final describeContents()I
    .registers 2

    #@0
    .prologue
    .line 1342
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 11
    .parameter "du"
    .parameter "prefix"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1263
    new-instance v5, Ljava/lang/StringBuilder;

    #@3
    const/16 v6, 0x100

    #@5
    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    #@8
    .line 1264
    .local v5, sb:Ljava/lang/StringBuilder;
    iget-object v6, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v6

    #@e
    if-lez v6, :cond_3d

    #@10
    .line 1265
    iget-object v6, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v3

    #@16
    .line 1266
    .local v3, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_16
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v6

    #@1a
    if-eqz v6, :cond_3d

    #@1c
    .line 1267
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@1f
    .line 1268
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    const-string v6, "Action: \""

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    .line 1269
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v6

    #@2b
    check-cast v6, Ljava/lang/String;

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    const-string v6, "\""

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    .line 1270
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@3c
    goto :goto_16

    #@3d
    .line 1273
    .end local v3           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_3d
    iget-object v6, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@3f
    if-eqz v6, :cond_6e

    #@41
    .line 1274
    iget-object v6, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@43
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@46
    move-result-object v3

    #@47
    .line 1275
    .restart local v3       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_47
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@4a
    move-result v6

    #@4b
    if-eqz v6, :cond_6e

    #@4d
    .line 1276
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@50
    .line 1277
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    const-string v6, "Category: \""

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 1278
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5b
    move-result-object v6

    #@5c
    check-cast v6, Ljava/lang/String;

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    const-string v6, "\""

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    .line 1279
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@6d
    goto :goto_47

    #@6e
    .line 1282
    .end local v3           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_6e
    iget-object v6, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@70
    if-eqz v6, :cond_9f

    #@72
    .line 1283
    iget-object v6, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@74
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@77
    move-result-object v3

    #@78
    .line 1284
    .restart local v3       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_78
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@7b
    move-result v6

    #@7c
    if-eqz v6, :cond_9f

    #@7e
    .line 1285
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@81
    .line 1286
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    const-string v6, "Scheme: \""

    #@86
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    .line 1287
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8c
    move-result-object v6

    #@8d
    check-cast v6, Ljava/lang/String;

    #@8f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    const-string v6, "\""

    #@94
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    .line 1288
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v6

    #@9b
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@9e
    goto :goto_78

    #@9f
    .line 1291
    .end local v3           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_9f
    iget-object v6, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@a1
    if-eqz v6, :cond_e6

    #@a3
    .line 1292
    iget-object v6, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@a5
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a8
    move-result-object v1

    #@a9
    .line 1293
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/IntentFilter$AuthorityEntry;>;"
    :goto_a9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@ac
    move-result v6

    #@ad
    if-eqz v6, :cond_e6

    #@af
    .line 1294
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b2
    move-result-object v0

    #@b3
    check-cast v0, Landroid/content/IntentFilter$AuthorityEntry;

    #@b5
    .line 1295
    .local v0, ae:Landroid/content/IntentFilter$AuthorityEntry;
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@b8
    .line 1296
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    const-string v6, "Authority: \""

    #@bd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    .line 1297
    invoke-static {v0}, Landroid/content/IntentFilter$AuthorityEntry;->access$000(Landroid/content/IntentFilter$AuthorityEntry;)Ljava/lang/String;

    #@c3
    move-result-object v6

    #@c4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    const-string v6, "\": "

    #@c9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    .line 1298
    invoke-static {v0}, Landroid/content/IntentFilter$AuthorityEntry;->access$100(Landroid/content/IntentFilter$AuthorityEntry;)I

    #@cf
    move-result v6

    #@d0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d3
    .line 1299
    invoke-static {v0}, Landroid/content/IntentFilter$AuthorityEntry;->access$200(Landroid/content/IntentFilter$AuthorityEntry;)Z

    #@d6
    move-result v6

    #@d7
    if-eqz v6, :cond_de

    #@d9
    const-string v6, " WILD"

    #@db
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    .line 1300
    :cond_de
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v6

    #@e2
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@e5
    goto :goto_a9

    #@e6
    .line 1303
    .end local v0           #ae:Landroid/content/IntentFilter$AuthorityEntry;
    .end local v1           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/IntentFilter$AuthorityEntry;>;"
    :cond_e6
    iget-object v6, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@e8
    if-eqz v6, :cond_117

    #@ea
    .line 1304
    iget-object v6, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@ec
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@ef
    move-result-object v2

    #@f0
    .line 1305
    .local v2, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/os/PatternMatcher;>;"
    :goto_f0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@f3
    move-result v6

    #@f4
    if-eqz v6, :cond_117

    #@f6
    .line 1306
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f9
    move-result-object v4

    #@fa
    check-cast v4, Landroid/os/PatternMatcher;

    #@fc
    .line 1307
    .local v4, pe:Landroid/os/PatternMatcher;
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@ff
    .line 1308
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    const-string v6, "Path: \""

    #@104
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    .line 1309
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10a
    const-string v6, "\""

    #@10c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    .line 1310
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@112
    move-result-object v6

    #@113
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@116
    goto :goto_f0

    #@117
    .line 1313
    .end local v2           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/os/PatternMatcher;>;"
    .end local v4           #pe:Landroid/os/PatternMatcher;
    :cond_117
    iget-object v6, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@119
    if-eqz v6, :cond_148

    #@11b
    .line 1314
    iget-object v6, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@11d
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@120
    move-result-object v3

    #@121
    .line 1315
    .restart local v3       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_121
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@124
    move-result v6

    #@125
    if-eqz v6, :cond_148

    #@127
    .line 1316
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@12a
    .line 1317
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    const-string v6, "Type: \""

    #@12f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    .line 1318
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@135
    move-result-object v6

    #@136
    check-cast v6, Ljava/lang/String;

    #@138
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    const-string v6, "\""

    #@13d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    .line 1319
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@143
    move-result-object v6

    #@144
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@147
    goto :goto_121

    #@148
    .line 1322
    .end local v3           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_148
    iget v6, p0, Landroid/content/IntentFilter;->mPriority:I

    #@14a
    if-nez v6, :cond_150

    #@14c
    iget-boolean v6, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@14e
    if-eqz v6, :cond_172

    #@150
    .line 1323
    :cond_150
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@153
    .line 1324
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    const-string/jumbo v6, "mPriority="

    #@159
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    iget v6, p0, Landroid/content/IntentFilter;->mPriority:I

    #@15e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@161
    .line 1325
    const-string v6, ", mHasPartialTypes="

    #@163
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    iget-boolean v6, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@168
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16b
    .line 1326
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v6

    #@16f
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@172
    .line 1328
    :cond_172
    return-void
.end method

.method public final getAction(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 458
    iget-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public final getCategory(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 991
    iget-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public final getDataAuthority(I)Landroid/content/IntentFilter$AuthorityEntry;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 739
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/content/IntentFilter$AuthorityEntry;

    #@8
    return-object v0
.end method

.method public final getDataPath(I)Landroid/os/PatternMatcher;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 805
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/PatternMatcher;

    #@8
    return-object v0
.end method

.method public final getDataScheme(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 602
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public final getDataType(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 558
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public final getPriority()I
    .registers 2

    #@0
    .prologue
    .line 431
    iget v0, p0, Landroid/content/IntentFilter;->mPriority:I

    #@2
    return v0
.end method

.method public final hasAction(Ljava/lang/String;)Z
    .registers 3
    .parameter "action"

    #@0
    .prologue
    .line 470
    if-eqz p1, :cond_c

    #@2
    iget-object v0, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public final hasCategory(Ljava/lang/String;)Z
    .registers 3
    .parameter "category"

    #@0
    .prologue
    .line 1002
    iget-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public final hasDataAuthority(Landroid/net/Uri;)Z
    .registers 3
    .parameter "data"

    #@0
    .prologue
    .line 753
    invoke-virtual {p0, p1}, Landroid/content/IntentFilter;->matchDataAuthority(Landroid/net/Uri;)I

    #@3
    move-result v0

    #@4
    if-ltz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final hasDataPath(Ljava/lang/String;)Z
    .registers 7
    .parameter "data"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 820
    iget-object v4, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@3
    if-nez v4, :cond_6

    #@5
    .line 830
    :cond_5
    :goto_5
    return v3

    #@6
    .line 823
    :cond_6
    iget-object v4, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v1

    #@c
    .line 824
    .local v1, numDataPaths:I
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    if-ge v0, v1, :cond_5

    #@f
    .line 825
    iget-object v4, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/os/PatternMatcher;

    #@17
    .line 826
    .local v2, pe:Landroid/os/PatternMatcher;
    invoke-virtual {v2, p1}, Landroid/os/PatternMatcher;->match(Ljava/lang/String;)Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_1f

    #@1d
    .line 827
    const/4 v3, 0x1

    #@1e
    goto :goto_5

    #@1f
    .line 824
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_d
.end method

.method public final hasDataScheme(Ljava/lang/String;)Z
    .registers 3
    .parameter "scheme"

    #@0
    .prologue
    .line 615
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public final hasDataType(Ljava/lang/String;)Z
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 544
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_c

    #@4
    invoke-direct {p0, p1}, Landroid/content/IntentFilter;->findMimeType(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public final match(Landroid/content/ContentResolver;Landroid/content/Intent;ZLjava/lang/String;)I
    .registers 12
    .parameter "resolver"
    .parameter "intent"
    .parameter "resolve"
    .parameter "logTag"

    #@0
    .prologue
    .line 1070
    if-eqz p3, :cond_1d

    #@2
    invoke-virtual {p2, p1}, Landroid/content/Intent;->resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    .line 1071
    .local v2, type:Ljava/lang/String;
    :goto_6
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {p2}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {p2}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    #@15
    move-result-object v5

    #@16
    move-object v0, p0

    #@17
    move-object v6, p4

    #@18
    invoke-virtual/range {v0 .. v6}, Landroid/content/IntentFilter;->match(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Set;Ljava/lang/String;)I

    #@1b
    move-result v0

    #@1c
    return v0

    #@1d
    .line 1070
    .end local v2           #type:Ljava/lang/String;
    :cond_1d
    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    goto :goto_6
.end method

.method public final match(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Set;Ljava/lang/String;)I
    .registers 10
    .parameter "action"
    .parameter "type"
    .parameter "scheme"
    .parameter "data"
    .parameter
    .parameter "logTag"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    #@0
    .prologue
    .line 1105
    .local p5, categories:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz p1, :cond_a

    #@2
    invoke-virtual {p0, p1}, Landroid/content/IntentFilter;->matchAction(Ljava/lang/String;)Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_a

    #@8
    .line 1108
    const/4 v1, -0x3

    #@9
    .line 1142
    :cond_9
    :goto_9
    return v1

    #@a
    .line 1111
    :cond_a
    invoke-virtual {p0, p2, p3, p4}, Landroid/content/IntentFilter;->matchData(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)I

    #@d
    move-result v1

    #@e
    .line 1112
    .local v1, dataMatch:I
    if-ltz v1, :cond_9

    #@10
    .line 1126
    invoke-virtual {p0, p5}, Landroid/content/IntentFilter;->matchCategories(Ljava/util/Set;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 1127
    .local v0, categoryMismatch:Ljava/lang/String;
    if-eqz v0, :cond_9

    #@16
    .line 1131
    const/4 v1, -0x4

    #@17
    goto :goto_9
.end method

.method public final matchAction(Ljava/lang/String;)Z
    .registers 3
    .parameter "action"

    #@0
    .prologue
    .line 482
    invoke-virtual {p0, p1}, Landroid/content/IntentFilter;->hasAction(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public final matchCategories(Ljava/util/Set;)Ljava/lang/String;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .local p1, categories:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v2, 0x0

    #@1
    .line 1026
    if-nez p1, :cond_4

    #@3
    .line 1043
    :cond_3
    :goto_3
    return-object v2

    #@4
    .line 1030
    :cond_4
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .line 1032
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    iget-object v3, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@a
    if-nez v3, :cond_19

    #@c
    .line 1033
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_3

    #@12
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Ljava/lang/String;

    #@18
    goto :goto_3

    #@19
    .line 1036
    :cond_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_3

    #@1f
    .line 1037
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Ljava/lang/String;

    #@25
    .line 1038
    .local v0, category:Ljava/lang/String;
    iget-object v3, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@2a
    move-result v3

    #@2b
    if-nez v3, :cond_19

    #@2d
    move-object v2, v0

    #@2e
    .line 1039
    goto :goto_3
.end method

.method public final matchData(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)I
    .registers 13
    .parameter "type"
    .parameter "scheme"
    .parameter "data"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v6, -0x2

    #@2
    .line 900
    iget-object v5, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@4
    .line 901
    .local v5, types:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@6
    .line 902
    .local v4, schemes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@8
    .line 903
    .local v1, authorities:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/IntentFilter$AuthorityEntry;>;"
    iget-object v3, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@a
    .line 905
    .local v3, paths:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/PatternMatcher;>;"
    const/high16 v2, 0x10

    #@c
    .line 907
    .local v2, match:I
    if-nez v5, :cond_18

    #@e
    if-nez v4, :cond_18

    #@10
    .line 908
    if-nez p1, :cond_17

    #@12
    if-nez p3, :cond_17

    #@14
    const v6, 0x108000

    #@17
    .line 960
    .end local p2
    :cond_17
    :goto_17
    return v6

    #@18
    .line 912
    .restart local p2
    :cond_18
    if-eqz v4, :cond_4e

    #@1a
    .line 913
    if-eqz p2, :cond_3e

    #@1c
    .end local p2
    :goto_1c
    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@1f
    move-result v8

    #@20
    if-eqz v8, :cond_17

    #@22
    .line 914
    const/high16 v2, 0x20

    #@24
    .line 919
    if-eqz v1, :cond_2f

    #@26
    .line 920
    invoke-virtual {p0, p3}, Landroid/content/IntentFilter;->matchDataAuthority(Landroid/net/Uri;)I

    #@29
    move-result v0

    #@2a
    .line 921
    .local v0, authMatch:I
    if-ltz v0, :cond_17

    #@2c
    .line 922
    if-nez v3, :cond_41

    #@2e
    .line 923
    move v2, v0

    #@2f
    .line 946
    .end local v0           #authMatch:I
    :cond_2f
    :goto_2f
    if-eqz v5, :cond_6b

    #@31
    .line 947
    invoke-direct {p0, p1}, Landroid/content/IntentFilter;->findMimeType(Ljava/lang/String;)Z

    #@34
    move-result v6

    #@35
    if-eqz v6, :cond_69

    #@37
    .line 948
    const/high16 v2, 0x60

    #@39
    .line 960
    :cond_39
    const v6, 0x8000

    #@3c
    add-int/2addr v6, v2

    #@3d
    goto :goto_17

    #@3e
    .line 913
    .restart local p2
    :cond_3e
    const-string p2, ""

    #@40
    goto :goto_1c

    #@41
    .line 924
    .end local p2
    .restart local v0       #authMatch:I
    :cond_41
    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@44
    move-result-object v8

    #@45
    invoke-virtual {p0, v8}, Landroid/content/IntentFilter;->hasDataPath(Ljava/lang/String;)Z

    #@48
    move-result v8

    #@49
    if-eqz v8, :cond_17

    #@4b
    .line 925
    const/high16 v2, 0x50

    #@4d
    goto :goto_2f

    #@4e
    .line 939
    .end local v0           #authMatch:I
    .restart local p2
    :cond_4e
    if-eqz p2, :cond_2f

    #@50
    const-string v8, ""

    #@52
    invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v8

    #@56
    if-nez v8, :cond_2f

    #@58
    const-string v8, "content"

    #@5a
    invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v8

    #@5e
    if-nez v8, :cond_2f

    #@60
    const-string v8, "file"

    #@62
    invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v8

    #@66
    if-nez v8, :cond_2f

    #@68
    goto :goto_17

    #@69
    .end local p2
    :cond_69
    move v6, v7

    #@6a
    .line 950
    goto :goto_17

    #@6b
    .line 955
    :cond_6b
    if-eqz p1, :cond_39

    #@6d
    move v6, v7

    #@6e
    .line 956
    goto :goto_17
.end method

.method public final matchDataAuthority(Landroid/net/Uri;)I
    .registers 8
    .parameter "data"

    #@0
    .prologue
    const/4 v4, -0x2

    #@1
    .line 851
    iget-object v5, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@3
    if-nez v5, :cond_7

    #@5
    move v2, v4

    #@6
    .line 862
    :cond_6
    :goto_6
    return v2

    #@7
    .line 854
    :cond_7
    iget-object v5, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v3

    #@d
    .line 855
    .local v3, numDataAuthorities:I
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v3, :cond_21

    #@10
    .line 856
    iget-object v5, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/content/IntentFilter$AuthorityEntry;

    #@18
    .line 857
    .local v0, ae:Landroid/content/IntentFilter$AuthorityEntry;
    invoke-virtual {v0, p1}, Landroid/content/IntentFilter$AuthorityEntry;->match(Landroid/net/Uri;)I

    #@1b
    move-result v2

    #@1c
    .line 858
    .local v2, match:I
    if-gez v2, :cond_6

    #@1e
    .line 855
    add-int/lit8 v1, v1, 0x1

    #@20
    goto :goto_e

    #@21
    .end local v0           #ae:Landroid/content/IntentFilter$AuthorityEntry;
    .end local v2           #match:I
    :cond_21
    move v2, v4

    #@22
    .line 862
    goto :goto_6
.end method

.method public final pathsIterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Landroid/os/PatternMatcher;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 837
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public readFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 15
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v12, 0x3

    #@1
    const/4 v11, 0x1

    #@2
    const/4 v10, 0x0

    #@3
    .line 1206
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@6
    move-result v2

    #@7
    .line 1209
    .local v2, outerDepth:I
    :cond_7
    :goto_7
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@a
    move-result v6

    #@b
    .local v6, type:I
    if-eq v6, v11, :cond_e2

    #@d
    if-ne v6, v12, :cond_15

    #@f
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@12
    move-result v7

    #@13
    if-le v7, v2, :cond_e2

    #@15
    .line 1211
    :cond_15
    if-eq v6, v12, :cond_7

    #@17
    const/4 v7, 0x4

    #@18
    if-eq v6, v7, :cond_7

    #@1a
    .line 1216
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    .line 1217
    .local v5, tagName:Ljava/lang/String;
    const-string v7, "action"

    #@20
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v7

    #@24
    if-eqz v7, :cond_36

    #@26
    .line 1218
    const-string/jumbo v7, "name"

    #@29
    invoke-interface {p1, v10, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    .line 1219
    .local v1, name:Ljava/lang/String;
    if-eqz v1, :cond_32

    #@2f
    .line 1220
    invoke-virtual {p0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@32
    .line 1258
    .end local v1           #name:Ljava/lang/String;
    :cond_32
    :goto_32
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@35
    goto :goto_7

    #@36
    .line 1222
    :cond_36
    const-string v7, "cat"

    #@38
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v7

    #@3c
    if-eqz v7, :cond_4b

    #@3e
    .line 1223
    const-string/jumbo v7, "name"

    #@41
    invoke-interface {p1, v10, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    .line 1224
    .restart local v1       #name:Ljava/lang/String;
    if-eqz v1, :cond_32

    #@47
    .line 1225
    invoke-virtual {p0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    #@4a
    goto :goto_32

    #@4b
    .line 1227
    .end local v1           #name:Ljava/lang/String;
    :cond_4b
    const-string/jumbo v7, "type"

    #@4e
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v7

    #@52
    if-eqz v7, :cond_63

    #@54
    .line 1228
    const-string/jumbo v7, "name"

    #@57
    invoke-interface {p1, v10, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    .line 1229
    .restart local v1       #name:Ljava/lang/String;
    if-eqz v1, :cond_32

    #@5d
    .line 1231
    :try_start_5d
    invoke-virtual {p0, v1}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_60
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_5d .. :try_end_60} :catch_61

    #@60
    goto :goto_32

    #@61
    .line 1232
    :catch_61
    move-exception v7

    #@62
    goto :goto_32

    #@63
    .line 1235
    .end local v1           #name:Ljava/lang/String;
    :cond_63
    const-string/jumbo v7, "scheme"

    #@66
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v7

    #@6a
    if-eqz v7, :cond_79

    #@6c
    .line 1236
    const-string/jumbo v7, "name"

    #@6f
    invoke-interface {p1, v10, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    .line 1237
    .restart local v1       #name:Ljava/lang/String;
    if-eqz v1, :cond_32

    #@75
    .line 1238
    invoke-virtual {p0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@78
    goto :goto_32

    #@79
    .line 1240
    .end local v1           #name:Ljava/lang/String;
    :cond_79
    const-string v7, "auth"

    #@7b
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7e
    move-result v7

    #@7f
    if-eqz v7, :cond_94

    #@81
    .line 1241
    const-string v7, "host"

    #@83
    invoke-interface {p1, v10, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@86
    move-result-object v0

    #@87
    .line 1242
    .local v0, host:Ljava/lang/String;
    const-string/jumbo v7, "port"

    #@8a
    invoke-interface {p1, v10, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8d
    move-result-object v4

    #@8e
    .line 1243
    .local v4, port:Ljava/lang/String;
    if-eqz v0, :cond_32

    #@90
    .line 1244
    invoke-virtual {p0, v0, v4}, Landroid/content/IntentFilter;->addDataAuthority(Ljava/lang/String;Ljava/lang/String;)V

    #@93
    goto :goto_32

    #@94
    .line 1246
    .end local v0           #host:Ljava/lang/String;
    .end local v4           #port:Ljava/lang/String;
    :cond_94
    const-string/jumbo v7, "path"

    #@97
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v7

    #@9b
    if-eqz v7, :cond_c8

    #@9d
    .line 1247
    const-string/jumbo v7, "literal"

    #@a0
    invoke-interface {p1, v10, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a3
    move-result-object v3

    #@a4
    .line 1248
    .local v3, path:Ljava/lang/String;
    if-eqz v3, :cond_ab

    #@a6
    .line 1249
    const/4 v7, 0x0

    #@a7
    invoke-virtual {p0, v3, v7}, Landroid/content/IntentFilter;->addDataPath(Ljava/lang/String;I)V

    #@aa
    goto :goto_32

    #@ab
    .line 1250
    :cond_ab
    const-string/jumbo v7, "prefix"

    #@ae
    invoke-interface {p1, v10, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b1
    move-result-object v3

    #@b2
    if-eqz v3, :cond_b9

    #@b4
    .line 1251
    invoke-virtual {p0, v3, v11}, Landroid/content/IntentFilter;->addDataPath(Ljava/lang/String;I)V

    #@b7
    goto/16 :goto_32

    #@b9
    .line 1252
    :cond_b9
    const-string/jumbo v7, "sglob"

    #@bc
    invoke-interface {p1, v10, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@bf
    move-result-object v3

    #@c0
    if-eqz v3, :cond_32

    #@c2
    .line 1253
    const/4 v7, 0x2

    #@c3
    invoke-virtual {p0, v3, v7}, Landroid/content/IntentFilter;->addDataPath(Ljava/lang/String;I)V

    #@c6
    goto/16 :goto_32

    #@c8
    .line 1256
    .end local v3           #path:Ljava/lang/String;
    :cond_c8
    const-string v7, "IntentFilter"

    #@ca
    new-instance v8, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    const-string v9, "Unknown tag parsing IntentFilter: "

    #@d1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v8

    #@d5
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v8

    #@d9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v8

    #@dd
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    goto/16 :goto_32

    #@e2
    .line 1260
    .end local v5           #tagName:Ljava/lang/String;
    :cond_e2
    return-void
.end method

.method public final schemesIterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 622
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public final setPriority(I)V
    .registers 2
    .parameter "priority"

    #@0
    .prologue
    .line 420
    iput p1, p0, Landroid/content/IntentFilter;->mPriority:I

    #@2
    .line 421
    return-void
.end method

.method public final typesIterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 565
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1346
    iget-object v2, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@7
    .line 1347
    iget-object v2, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@9
    if-eqz v2, :cond_49

    #@b
    .line 1348
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 1349
    iget-object v2, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@10
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@13
    .line 1353
    :goto_13
    iget-object v2, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@15
    if-eqz v2, :cond_4d

    #@17
    .line 1354
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 1355
    iget-object v2, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@1f
    .line 1359
    :goto_1f
    iget-object v2, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@21
    if-eqz v2, :cond_51

    #@23
    .line 1360
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 1361
    iget-object v2, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@28
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@2b
    .line 1365
    :goto_2b
    iget-object v2, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@2d
    if-eqz v2, :cond_55

    #@2f
    .line 1366
    iget-object v2, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@34
    move-result v0

    #@35
    .line 1367
    .local v0, N:I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 1368
    const/4 v1, 0x0

    #@39
    .local v1, i:I
    :goto_39
    if-ge v1, v0, :cond_58

    #@3b
    .line 1369
    iget-object v2, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@3d
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@40
    move-result-object v2

    #@41
    check-cast v2, Landroid/content/IntentFilter$AuthorityEntry;

    #@43
    invoke-virtual {v2, p1}, Landroid/content/IntentFilter$AuthorityEntry;->writeToParcel(Landroid/os/Parcel;)V

    #@46
    .line 1368
    add-int/lit8 v1, v1, 0x1

    #@48
    goto :goto_39

    #@49
    .line 1351
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_49
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@4c
    goto :goto_13

    #@4d
    .line 1357
    :cond_4d
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@50
    goto :goto_1f

    #@51
    .line 1363
    :cond_51
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@54
    goto :goto_2b

    #@55
    .line 1372
    :cond_55
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@58
    .line 1374
    :cond_58
    iget-object v2, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@5a
    if-eqz v2, :cond_76

    #@5c
    .line 1375
    iget-object v2, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@5e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@61
    move-result v0

    #@62
    .line 1376
    .restart local v0       #N:I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@65
    .line 1377
    const/4 v1, 0x0

    #@66
    .restart local v1       #i:I
    :goto_66
    if-ge v1, v0, :cond_79

    #@68
    .line 1378
    iget-object v2, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@6a
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6d
    move-result-object v2

    #@6e
    check-cast v2, Landroid/os/PatternMatcher;

    #@70
    invoke-virtual {v2, p1, v4}, Landroid/os/PatternMatcher;->writeToParcel(Landroid/os/Parcel;I)V

    #@73
    .line 1377
    add-int/lit8 v1, v1, 0x1

    #@75
    goto :goto_66

    #@76
    .line 1381
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_76
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@79
    .line 1383
    :cond_79
    iget v2, p0, Landroid/content/IntentFilter;->mPriority:I

    #@7b
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@7e
    .line 1384
    iget-boolean v2, p0, Landroid/content/IntentFilter;->mHasPartialTypes:Z

    #@80
    if-eqz v2, :cond_87

    #@82
    move v2, v3

    #@83
    :goto_83
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@86
    .line 1385
    return-void

    #@87
    :cond_87
    move v2, v4

    #@88
    .line 1384
    goto :goto_83
.end method

.method public writeToXml(Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 10
    .parameter "serializer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1149
    invoke-virtual {p0}, Landroid/content/IntentFilter;->countActions()I

    #@4
    move-result v0

    #@5
    .line 1150
    .local v0, N:I
    const/4 v2, 0x0

    #@6
    .local v2, i:I
    :goto_6
    if-ge v2, v0, :cond_23

    #@8
    .line 1151
    const-string v5, "action"

    #@a
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d
    .line 1152
    const-string/jumbo v6, "name"

    #@10
    iget-object v5, p0, Landroid/content/IntentFilter;->mActions:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v5

    #@16
    check-cast v5, Ljava/lang/String;

    #@18
    invoke-interface {p1, v7, v6, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1b
    .line 1153
    const-string v5, "action"

    #@1d
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@20
    .line 1150
    add-int/lit8 v2, v2, 0x1

    #@22
    goto :goto_6

    #@23
    .line 1155
    :cond_23
    invoke-virtual {p0}, Landroid/content/IntentFilter;->countCategories()I

    #@26
    move-result v0

    #@27
    .line 1156
    const/4 v2, 0x0

    #@28
    :goto_28
    if-ge v2, v0, :cond_45

    #@2a
    .line 1157
    const-string v5, "cat"

    #@2c
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2f
    .line 1158
    const-string/jumbo v6, "name"

    #@32
    iget-object v5, p0, Landroid/content/IntentFilter;->mCategories:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@37
    move-result-object v5

    #@38
    check-cast v5, Ljava/lang/String;

    #@3a
    invoke-interface {p1, v7, v6, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3d
    .line 1159
    const-string v5, "cat"

    #@3f
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@42
    .line 1156
    add-int/lit8 v2, v2, 0x1

    #@44
    goto :goto_28

    #@45
    .line 1161
    :cond_45
    invoke-virtual {p0}, Landroid/content/IntentFilter;->countDataTypes()I

    #@48
    move-result v0

    #@49
    .line 1162
    const/4 v2, 0x0

    #@4a
    :goto_4a
    if-ge v2, v0, :cond_84

    #@4c
    .line 1163
    const-string/jumbo v5, "type"

    #@4f
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@52
    .line 1164
    iget-object v5, p0, Landroid/content/IntentFilter;->mDataTypes:Ljava/util/ArrayList;

    #@54
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@57
    move-result-object v4

    #@58
    check-cast v4, Ljava/lang/String;

    #@5a
    .line 1165
    .local v4, type:Ljava/lang/String;
    const/16 v5, 0x2f

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    #@5f
    move-result v5

    #@60
    if-gez v5, :cond_75

    #@62
    new-instance v5, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v5

    #@6b
    const-string v6, "/*"

    #@6d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    .line 1166
    :cond_75
    const-string/jumbo v5, "name"

    #@78
    invoke-interface {p1, v7, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7b
    .line 1167
    const-string/jumbo v5, "type"

    #@7e
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@81
    .line 1162
    add-int/lit8 v2, v2, 0x1

    #@83
    goto :goto_4a

    #@84
    .line 1169
    .end local v4           #type:Ljava/lang/String;
    :cond_84
    invoke-virtual {p0}, Landroid/content/IntentFilter;->countDataSchemes()I

    #@87
    move-result v0

    #@88
    .line 1170
    const/4 v2, 0x0

    #@89
    :goto_89
    if-ge v2, v0, :cond_a8

    #@8b
    .line 1171
    const-string/jumbo v5, "scheme"

    #@8e
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@91
    .line 1172
    const-string/jumbo v6, "name"

    #@94
    iget-object v5, p0, Landroid/content/IntentFilter;->mDataSchemes:Ljava/util/ArrayList;

    #@96
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@99
    move-result-object v5

    #@9a
    check-cast v5, Ljava/lang/String;

    #@9c
    invoke-interface {p1, v7, v6, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9f
    .line 1173
    const-string/jumbo v5, "scheme"

    #@a2
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@a5
    .line 1170
    add-int/lit8 v2, v2, 0x1

    #@a7
    goto :goto_89

    #@a8
    .line 1175
    :cond_a8
    invoke-virtual {p0}, Landroid/content/IntentFilter;->countDataAuthorities()I

    #@ab
    move-result v0

    #@ac
    .line 1176
    const/4 v2, 0x0

    #@ad
    :goto_ad
    if-ge v2, v0, :cond_e1

    #@af
    .line 1177
    const-string v5, "auth"

    #@b1
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@b4
    .line 1178
    iget-object v5, p0, Landroid/content/IntentFilter;->mDataAuthorities:Ljava/util/ArrayList;

    #@b6
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b9
    move-result-object v1

    #@ba
    check-cast v1, Landroid/content/IntentFilter$AuthorityEntry;

    #@bc
    .line 1179
    .local v1, ae:Landroid/content/IntentFilter$AuthorityEntry;
    const-string v5, "host"

    #@be
    invoke-virtual {v1}, Landroid/content/IntentFilter$AuthorityEntry;->getHost()Ljava/lang/String;

    #@c1
    move-result-object v6

    #@c2
    invoke-interface {p1, v7, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c5
    .line 1180
    invoke-virtual {v1}, Landroid/content/IntentFilter$AuthorityEntry;->getPort()I

    #@c8
    move-result v5

    #@c9
    if-ltz v5, :cond_d9

    #@cb
    .line 1181
    const-string/jumbo v5, "port"

    #@ce
    invoke-virtual {v1}, Landroid/content/IntentFilter$AuthorityEntry;->getPort()I

    #@d1
    move-result v6

    #@d2
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@d5
    move-result-object v6

    #@d6
    invoke-interface {p1, v7, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d9
    .line 1183
    :cond_d9
    const-string v5, "auth"

    #@db
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@de
    .line 1176
    add-int/lit8 v2, v2, 0x1

    #@e0
    goto :goto_ad

    #@e1
    .line 1185
    .end local v1           #ae:Landroid/content/IntentFilter$AuthorityEntry;
    :cond_e1
    invoke-virtual {p0}, Landroid/content/IntentFilter;->countDataPaths()I

    #@e4
    move-result v0

    #@e5
    .line 1186
    const/4 v2, 0x0

    #@e6
    :goto_e6
    if-ge v2, v0, :cond_127

    #@e8
    .line 1187
    const-string/jumbo v5, "path"

    #@eb
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@ee
    .line 1188
    iget-object v5, p0, Landroid/content/IntentFilter;->mDataPaths:Ljava/util/ArrayList;

    #@f0
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f3
    move-result-object v3

    #@f4
    check-cast v3, Landroid/os/PatternMatcher;

    #@f6
    .line 1189
    .local v3, pe:Landroid/os/PatternMatcher;
    invoke-virtual {v3}, Landroid/os/PatternMatcher;->getType()I

    #@f9
    move-result v5

    #@fa
    packed-switch v5, :pswitch_data_128

    #@fd
    .line 1200
    :goto_fd
    const-string/jumbo v5, "path"

    #@100
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@103
    .line 1186
    add-int/lit8 v2, v2, 0x1

    #@105
    goto :goto_e6

    #@106
    .line 1191
    :pswitch_106
    const-string/jumbo v5, "literal"

    #@109
    invoke-virtual {v3}, Landroid/os/PatternMatcher;->getPath()Ljava/lang/String;

    #@10c
    move-result-object v6

    #@10d
    invoke-interface {p1, v7, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@110
    goto :goto_fd

    #@111
    .line 1194
    :pswitch_111
    const-string/jumbo v5, "prefix"

    #@114
    invoke-virtual {v3}, Landroid/os/PatternMatcher;->getPath()Ljava/lang/String;

    #@117
    move-result-object v6

    #@118
    invoke-interface {p1, v7, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@11b
    goto :goto_fd

    #@11c
    .line 1197
    :pswitch_11c
    const-string/jumbo v5, "sglob"

    #@11f
    invoke-virtual {v3}, Landroid/os/PatternMatcher;->getPath()Ljava/lang/String;

    #@122
    move-result-object v6

    #@123
    invoke-interface {p1, v7, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@126
    goto :goto_fd

    #@127
    .line 1202
    .end local v3           #pe:Landroid/os/PatternMatcher;
    :cond_127
    return-void

    #@128
    .line 1189
    :pswitch_data_128
    .packed-switch 0x0
        :pswitch_106
        :pswitch_111
        :pswitch_11c
    .end packed-switch
.end method
