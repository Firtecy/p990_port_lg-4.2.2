.class public abstract Landroid/content/IIntentReceiver$Stub;
.super Landroid/os/Binder;
.source "IIntentReceiver.java"

# interfaces
.implements Landroid/content/IIntentReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/IIntentReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/IIntentReceiver$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.content.IIntentReceiver"

.field static final TRANSACTION_performReceive:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 22
    const-string v0, "android.content.IIntentReceiver"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/content/IIntentReceiver$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 23
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/content/IIntentReceiver;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 30
    if-nez p0, :cond_4

    #@2
    .line 31
    const/4 v0, 0x0

    #@3
    .line 37
    :goto_3
    return-object v0

    #@4
    .line 33
    :cond_4
    const-string v1, "android.content.IIntentReceiver"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 34
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/content/IIntentReceiver;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 35
    check-cast v0, Landroid/content/IIntentReceiver;

    #@12
    goto :goto_3

    #@13
    .line 37
    :cond_13
    new-instance v0, Landroid/content/IIntentReceiver$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/content/IIntentReceiver$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 41
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 15
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 45
    sparse-switch p1, :sswitch_data_58

    #@5
    .line 83
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v8

    #@9
    :goto_9
    return v8

    #@a
    .line 49
    :sswitch_a
    const-string v0, "android.content.IIntentReceiver"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 54
    :sswitch_10
    const-string v9, "android.content.IIntentReceiver"

    #@12
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v9

    #@19
    if-eqz v9, :cond_50

    #@1b
    .line 57
    sget-object v9, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Landroid/content/Intent;

    #@23
    .line 63
    .local v1, _arg0:Landroid/content/Intent;
    :goto_23
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@26
    move-result v2

    #@27
    .line 65
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    .line 67
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2e
    move-result v9

    #@2f
    if-eqz v9, :cond_52

    #@31
    .line 68
    sget-object v9, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@33
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@36
    move-result-object v4

    #@37
    check-cast v4, Landroid/os/Bundle;

    #@39
    .line 74
    .local v4, _arg3:Landroid/os/Bundle;
    :goto_39
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v9

    #@3d
    if-eqz v9, :cond_54

    #@3f
    move v5, v8

    #@40
    .line 76
    .local v5, _arg4:Z
    :goto_40
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v9

    #@44
    if-eqz v9, :cond_56

    #@46
    move v6, v8

    #@47
    .line 78
    .local v6, _arg5:Z
    :goto_47
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4a
    move-result v7

    #@4b
    .local v7, _arg6:I
    move-object v0, p0

    #@4c
    .line 79
    invoke-virtual/range {v0 .. v7}, Landroid/content/IIntentReceiver$Stub;->performReceive(Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V

    #@4f
    goto :goto_9

    #@50
    .line 60
    .end local v1           #_arg0:Landroid/content/Intent;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Landroid/os/Bundle;
    .end local v5           #_arg4:Z
    .end local v6           #_arg5:Z
    .end local v7           #_arg6:I
    :cond_50
    const/4 v1, 0x0

    #@51
    .restart local v1       #_arg0:Landroid/content/Intent;
    goto :goto_23

    #@52
    .line 71
    .restart local v2       #_arg1:I
    .restart local v3       #_arg2:Ljava/lang/String;
    :cond_52
    const/4 v4, 0x0

    #@53
    .restart local v4       #_arg3:Landroid/os/Bundle;
    goto :goto_39

    #@54
    :cond_54
    move v5, v0

    #@55
    .line 74
    goto :goto_40

    #@56
    .restart local v5       #_arg4:Z
    :cond_56
    move v6, v0

    #@57
    .line 76
    goto :goto_47

    #@58
    .line 45
    :sswitch_data_58
    .sparse-switch
        0x1 -> :sswitch_10
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
