.class Landroid/content/ContentService$ObserverNode$ObserverEntry;
.super Ljava/lang/Object;
.source "ContentService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/ContentService$ObserverNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ObserverEntry"
.end annotation


# instance fields
.field public final notifyForDescendants:Z

.field public final observer:Landroid/database/IContentObserver;

.field private final observersLock:Ljava/lang/Object;

.field public final pid:I

.field final synthetic this$0:Landroid/content/ContentService$ObserverNode;

.field public final uid:I

.field private final userHandle:I


# direct methods
.method public constructor <init>(Landroid/content/ContentService$ObserverNode;Landroid/database/IContentObserver;ZLjava/lang/Object;III)V
    .registers 11
    .parameter
    .parameter "o"
    .parameter "n"
    .parameter "observersLock"
    .parameter "_uid"
    .parameter "_pid"
    .parameter "_userHandle"

    #@0
    .prologue
    .line 625
    iput-object p1, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->this$0:Landroid/content/ContentService$ObserverNode;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 626
    iput-object p4, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observersLock:Ljava/lang/Object;

    #@7
    .line 627
    iput-object p2, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observer:Landroid/database/IContentObserver;

    #@9
    .line 628
    iput p5, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->uid:I

    #@b
    .line 629
    iput p6, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->pid:I

    #@d
    .line 630
    iput p7, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->userHandle:I

    #@f
    .line 631
    iput-boolean p3, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->notifyForDescendants:Z

    #@11
    .line 633
    :try_start_11
    iget-object v1, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observer:Landroid/database/IContentObserver;

    #@13
    invoke-interface {v1}, Landroid/database/IContentObserver;->asBinder()Landroid/os/IBinder;

    #@16
    move-result-object v1

    #@17
    const/4 v2, 0x0

    #@18
    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_1b} :catch_1c

    #@1b
    .line 637
    :goto_1b
    return-void

    #@1c
    .line 634
    :catch_1c
    move-exception v0

    #@1d
    .line 635
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0}, Landroid/content/ContentService$ObserverNode$ObserverEntry;->binderDied()V

    #@20
    goto :goto_1b
.end method

.method static synthetic access$100(Landroid/content/ContentService$ObserverNode$ObserverEntry;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 616
    iget v0, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->userHandle:I

    #@2
    return v0
.end method


# virtual methods
.method public binderDied()V
    .registers 4

    #@0
    .prologue
    .line 640
    iget-object v1, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observersLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 641
    :try_start_3
    iget-object v0, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->this$0:Landroid/content/ContentService$ObserverNode;

    #@5
    iget-object v2, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observer:Landroid/database/IContentObserver;

    #@7
    invoke-virtual {v0, v2}, Landroid/content/ContentService$ObserverNode;->removeObserverLocked(Landroid/database/IContentObserver;)Z

    #@a
    .line 642
    monitor-exit v1

    #@b
    .line 643
    return-void

    #@c
    .line 642
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public dumpLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/SparseIntArray;)V
    .registers 9
    .parameter "fd"
    .parameter "pw"
    .parameter "args"
    .parameter "name"
    .parameter "prefix"
    .parameter "pidCounts"

    #@0
    .prologue
    .line 647
    iget v0, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->pid:I

    #@2
    iget v1, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->pid:I

    #@4
    invoke-virtual {p6, v1}, Landroid/util/SparseIntArray;->get(I)I

    #@7
    move-result v1

    #@8
    add-int/lit8 v1, v1, 0x1

    #@a
    invoke-virtual {p6, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    #@d
    .line 648
    invoke-virtual {p2, p5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10
    invoke-virtual {p2, p4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13
    const-string v0, ": pid="

    #@15
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18
    .line 649
    iget v0, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->pid:I

    #@1a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1d
    const-string v0, " uid="

    #@1f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22
    .line 650
    iget v0, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->uid:I

    #@24
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@27
    const-string v0, " user="

    #@29
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c
    .line 651
    iget v0, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->userHandle:I

    #@2e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@31
    const-string v0, " target="

    #@33
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@36
    .line 652
    iget-object v0, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observer:Landroid/database/IContentObserver;

    #@38
    if-eqz v0, :cond_4c

    #@3a
    iget-object v0, p0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observer:Landroid/database/IContentObserver;

    #@3c
    invoke-interface {v0}, Landroid/database/IContentObserver;->asBinder()Landroid/os/IBinder;

    #@3f
    move-result-object v0

    #@40
    :goto_40
    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@43
    move-result v0

    #@44
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@47
    move-result-object v0

    #@48
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4b
    .line 654
    return-void

    #@4c
    .line 652
    :cond_4c
    const/4 v0, 0x0

    #@4d
    goto :goto_40
.end method
