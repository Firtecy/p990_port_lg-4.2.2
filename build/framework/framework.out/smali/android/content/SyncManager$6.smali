.class Landroid/content/SyncManager$6;
.super Landroid/content/BroadcastReceiver;
.source "SyncManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/SyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/content/SyncManager;


# direct methods
.method constructor <init>(Landroid/content/SyncManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 303
    iput-object p1, p0, Landroid/content/SyncManager$6;->this$0:Landroid/content/SyncManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 5
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 305
    const-string v0, "SyncManager"

    #@2
    const-string v1, "Writing sync state before shutdown..."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 306
    iget-object v0, p0, Landroid/content/SyncManager$6;->this$0:Landroid/content/SyncManager;

    #@9
    invoke-virtual {v0}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0}, Landroid/content/SyncStorageEngine;->writeAllState()V

    #@10
    .line 307
    return-void
.end method
