.class public Landroid/content/SyncStatusInfo;
.super Ljava/lang/Object;
.source "SyncStatusInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/SyncStatusInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "Sync"

.field static final VERSION:I = 0x2


# instance fields
.field public final authorityId:I

.field public initialFailureTime:J

.field public initialize:Z

.field public lastFailureMesg:Ljava/lang/String;

.field public lastFailureSource:I

.field public lastFailureTime:J

.field public lastSuccessSource:I

.field public lastSuccessTime:J

.field public numSourceLocal:I

.field public numSourcePeriodic:I

.field public numSourcePoll:I

.field public numSourceServer:I

.field public numSourceUser:I

.field public numSyncs:I

.field public pending:Z

.field public periodicSyncTimes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public totalElapsedTime:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 167
    new-instance v0, Landroid/content/SyncStatusInfo$1;

    #@2
    invoke-direct {v0}, Landroid/content/SyncStatusInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/SyncStatusInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method constructor <init>(I)V
    .registers 2
    .parameter "authorityId"

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 50
    iput p1, p0, Landroid/content/SyncStatusInfo;->authorityId:I

    #@5
    .line 51
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 11
    .parameter "parcel"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v4, 0x1

    #@3
    .line 95
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v2

    #@a
    .line 97
    .local v2, version:I
    const/4 v3, 0x2

    #@b
    if-eq v2, v3, :cond_27

    #@d
    if-eq v2, v4, :cond_27

    #@f
    .line 98
    const-string v3, "SyncStatusInfo"

    #@11
    new-instance v6, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v7, "Unknown version: "

    #@18
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v6

    #@1c
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v6

    #@24
    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 100
    :cond_27
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v3

    #@2b
    iput v3, p0, Landroid/content/SyncStatusInfo;->authorityId:I

    #@2d
    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@30
    move-result-wide v6

    #@31
    iput-wide v6, p0, Landroid/content/SyncStatusInfo;->totalElapsedTime:J

    #@33
    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@36
    move-result v3

    #@37
    iput v3, p0, Landroid/content/SyncStatusInfo;->numSyncs:I

    #@39
    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v3

    #@3d
    iput v3, p0, Landroid/content/SyncStatusInfo;->numSourcePoll:I

    #@3f
    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@42
    move-result v3

    #@43
    iput v3, p0, Landroid/content/SyncStatusInfo;->numSourceServer:I

    #@45
    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@48
    move-result v3

    #@49
    iput v3, p0, Landroid/content/SyncStatusInfo;->numSourceLocal:I

    #@4b
    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4e
    move-result v3

    #@4f
    iput v3, p0, Landroid/content/SyncStatusInfo;->numSourceUser:I

    #@51
    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@54
    move-result-wide v6

    #@55
    iput-wide v6, p0, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    #@57
    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5a
    move-result v3

    #@5b
    iput v3, p0, Landroid/content/SyncStatusInfo;->lastSuccessSource:I

    #@5d
    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@60
    move-result-wide v6

    #@61
    iput-wide v6, p0, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    #@63
    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@66
    move-result v3

    #@67
    iput v3, p0, Landroid/content/SyncStatusInfo;->lastFailureSource:I

    #@69
    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    iput-object v3, p0, Landroid/content/SyncStatusInfo;->lastFailureMesg:Ljava/lang/String;

    #@6f
    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@72
    move-result-wide v6

    #@73
    iput-wide v6, p0, Landroid/content/SyncStatusInfo;->initialFailureTime:J

    #@75
    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@78
    move-result v3

    #@79
    if-eqz v3, :cond_8c

    #@7b
    move v3, v4

    #@7c
    :goto_7c
    iput-boolean v3, p0, Landroid/content/SyncStatusInfo;->pending:Z

    #@7e
    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@81
    move-result v3

    #@82
    if-eqz v3, :cond_85

    #@84
    move v5, v4

    #@85
    :cond_85
    iput-boolean v5, p0, Landroid/content/SyncStatusInfo;->initialize:Z

    #@87
    .line 115
    if-ne v2, v4, :cond_8e

    #@89
    .line 116
    iput-object v8, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@8b
    .line 128
    :cond_8b
    :goto_8b
    return-void

    #@8c
    :cond_8c
    move v3, v5

    #@8d
    .line 113
    goto :goto_7c

    #@8e
    .line 118
    :cond_8e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@91
    move-result v0

    #@92
    .line 119
    .local v0, N:I
    if-gez v0, :cond_97

    #@94
    .line 120
    iput-object v8, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@96
    goto :goto_8b

    #@97
    .line 122
    :cond_97
    new-instance v3, Ljava/util/ArrayList;

    #@99
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@9c
    iput-object v3, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@9e
    .line 123
    const/4 v1, 0x0

    #@9f
    .local v1, i:I
    :goto_9f
    if-ge v1, v0, :cond_8b

    #@a1
    .line 124
    iget-object v3, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@a3
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@a6
    move-result-wide v4

    #@a7
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@aa
    move-result-object v4

    #@ab
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ae
    .line 123
    add-int/lit8 v1, v1, 0x1

    #@b0
    goto :goto_9f
.end method

.method private ensurePeriodicSyncTimeSize(I)V
    .registers 7
    .parameter "index"

    #@0
    .prologue
    .line 143
    iget-object v2, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@2
    if-nez v2, :cond_c

    #@4
    .line 144
    new-instance v2, Ljava/util/ArrayList;

    #@6
    const/4 v3, 0x0

    #@7
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    #@a
    iput-object v2, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@c
    .line 147
    :cond_c
    add-int/lit8 v1, p1, 0x1

    #@e
    .line 148
    .local v1, requiredSize:I
    iget-object v2, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v2

    #@14
    if-ge v2, v1, :cond_2c

    #@16
    .line 149
    iget-object v2, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1b
    move-result v0

    #@1c
    .local v0, i:I
    :goto_1c
    if-ge v0, v1, :cond_2c

    #@1e
    .line 150
    iget-object v2, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@20
    const-wide/16 v3, 0x0

    #@22
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@29
    .line 149
    add-int/lit8 v0, v0, 0x1

    #@2b
    goto :goto_1c

    #@2c
    .line 153
    .end local v0           #i:I
    :cond_2c
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 65
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getLastFailureMesgAsInt(I)I
    .registers 6
    .parameter "def"

    #@0
    .prologue
    .line 55
    :try_start_0
    iget-object v1, p0, Landroid/content/SyncStatusInfo;->lastFailureMesg:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_a

    #@4
    .line 56
    iget-object v1, p0, Landroid/content/SyncStatusInfo;->lastFailureMesg:Ljava/lang/String;

    #@6
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result p1

    #@a
    .line 61
    .end local p1
    :cond_a
    :goto_a
    return p1

    #@b
    .line 58
    .restart local p1
    :catch_b
    move-exception v0

    #@c
    .line 59
    .local v0, e:Ljava/lang/NumberFormatException;
    const-string v1, "Sync"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "error parsing lastFailureMesg of "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    iget-object v3, p0, Landroid/content/SyncStatusInfo;->lastFailureMesg:Ljava/lang/String;

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    goto :goto_a
.end method

.method public getPeriodicSyncTime(I)J
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    add-int/lit8 v1, p1, 0x1

    #@c
    if-ge v0, v1, :cond_11

    #@e
    .line 157
    :cond_e
    const-wide/16 v0, 0x0

    #@10
    .line 159
    :goto_10
    return-wide v0

    #@11
    :cond_11
    iget-object v0, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Ljava/lang/Long;

    #@19
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@1c
    move-result-wide v0

    #@1d
    goto :goto_10
.end method

.method public removePeriodicSyncTime(I)V
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 163
    invoke-direct {p0, p1}, Landroid/content/SyncStatusInfo;->ensurePeriodicSyncTimeSize(I)V

    #@3
    .line 164
    iget-object v0, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@8
    .line 165
    return-void
.end method

.method public setPeriodicSyncTime(IJ)V
    .registers 7
    .parameter "index"
    .parameter "when"

    #@0
    .prologue
    .line 134
    :try_start_0
    invoke-direct {p0, p1}, Landroid/content/SyncStatusInfo;->ensurePeriodicSyncTimeSize(I)V

    #@3
    .line 135
    iget-object v1, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@5
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v1, p1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_c
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_c} :catch_d

    #@c
    .line 140
    :goto_c
    return-void

    #@d
    .line 136
    :catch_d
    move-exception v0

    #@e
    .line 137
    .local v0, e:Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    #@11
    goto :goto_c
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 11
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 69
    const/4 v3, 0x2

    #@3
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 70
    iget v3, p0, Landroid/content/SyncStatusInfo;->authorityId:I

    #@8
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@b
    .line 71
    iget-wide v6, p0, Landroid/content/SyncStatusInfo;->totalElapsedTime:J

    #@d
    invoke-virtual {p1, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    #@10
    .line 72
    iget v3, p0, Landroid/content/SyncStatusInfo;->numSyncs:I

    #@12
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 73
    iget v3, p0, Landroid/content/SyncStatusInfo;->numSourcePoll:I

    #@17
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 74
    iget v3, p0, Landroid/content/SyncStatusInfo;->numSourceServer:I

    #@1c
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 75
    iget v3, p0, Landroid/content/SyncStatusInfo;->numSourceLocal:I

    #@21
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 76
    iget v3, p0, Landroid/content/SyncStatusInfo;->numSourceUser:I

    #@26
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 77
    iget-wide v6, p0, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    #@2b
    invoke-virtual {p1, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    #@2e
    .line 78
    iget v3, p0, Landroid/content/SyncStatusInfo;->lastSuccessSource:I

    #@30
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 79
    iget-wide v6, p0, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    #@35
    invoke-virtual {p1, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    #@38
    .line 80
    iget v3, p0, Landroid/content/SyncStatusInfo;->lastFailureSource:I

    #@3a
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    .line 81
    iget-object v3, p0, Landroid/content/SyncStatusInfo;->lastFailureMesg:Ljava/lang/String;

    #@3f
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@42
    .line 82
    iget-wide v6, p0, Landroid/content/SyncStatusInfo;->initialFailureTime:J

    #@44
    invoke-virtual {p1, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    #@47
    .line 83
    iget-boolean v3, p0, Landroid/content/SyncStatusInfo;->pending:Z

    #@49
    if-eqz v3, :cond_7d

    #@4b
    move v3, v4

    #@4c
    :goto_4c
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4f
    .line 84
    iget-boolean v3, p0, Landroid/content/SyncStatusInfo;->initialize:Z

    #@51
    if-eqz v3, :cond_7f

    #@53
    :goto_53
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@56
    .line 85
    iget-object v3, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@58
    if-eqz v3, :cond_81

    #@5a
    .line 86
    iget-object v3, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@5c
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5f
    move-result v3

    #@60
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@63
    .line 87
    iget-object v3, p0, Landroid/content/SyncStatusInfo;->periodicSyncTimes:Ljava/util/ArrayList;

    #@65
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@68
    move-result-object v0

    #@69
    .local v0, i$:Ljava/util/Iterator;
    :goto_69
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@6c
    move-result v3

    #@6d
    if-eqz v3, :cond_85

    #@6f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@72
    move-result-object v3

    #@73
    check-cast v3, Ljava/lang/Long;

    #@75
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@78
    move-result-wide v1

    #@79
    .line 88
    .local v1, periodicSyncTime:J
    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@7c
    goto :goto_69

    #@7d
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #periodicSyncTime:J
    :cond_7d
    move v3, v5

    #@7e
    .line 83
    goto :goto_4c

    #@7f
    :cond_7f
    move v4, v5

    #@80
    .line 84
    goto :goto_53

    #@81
    .line 91
    :cond_81
    const/4 v3, -0x1

    #@82
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@85
    .line 93
    :cond_85
    return-void
.end method
