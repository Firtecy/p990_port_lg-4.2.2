.class public Landroid/content/BroadcastReceiver$PendingResult;
.super Ljava/lang/Object;
.source "BroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/BroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PendingResult"
.end annotation


# static fields
.field public static final TYPE_COMPONENT:I = 0x0

.field public static final TYPE_REGISTERED:I = 0x1

.field public static final TYPE_UNREGISTERED:I = 0x2


# instance fields
.field mAbortBroadcast:Z

.field mFinished:Z

.field final mInitialStickyHint:Z

.field final mOrderedHint:Z

.field mResultCode:I

.field mResultData:Ljava/lang/String;

.field mResultExtras:Landroid/os/Bundle;

.field final mSendingUser:I

.field final mToken:Landroid/os/IBinder;

.field final mType:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/os/Bundle;IZZLandroid/os/IBinder;I)V
    .registers 9
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "resultExtras"
    .parameter "type"
    .parameter "ordered"
    .parameter "sticky"
    .parameter "token"
    .parameter "userId"

    #@0
    .prologue
    .line 250
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 251
    iput p1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultCode:I

    #@5
    .line 252
    iput-object p2, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultData:Ljava/lang/String;

    #@7
    .line 253
    iput-object p3, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@9
    .line 254
    iput p4, p0, Landroid/content/BroadcastReceiver$PendingResult;->mType:I

    #@b
    .line 255
    iput-boolean p5, p0, Landroid/content/BroadcastReceiver$PendingResult;->mOrderedHint:Z

    #@d
    .line 256
    iput-boolean p6, p0, Landroid/content/BroadcastReceiver$PendingResult;->mInitialStickyHint:Z

    #@f
    .line 257
    iput-object p7, p0, Landroid/content/BroadcastReceiver$PendingResult;->mToken:Landroid/os/IBinder;

    #@11
    .line 258
    iput p8, p0, Landroid/content/BroadcastReceiver$PendingResult;->mSendingUser:I

    #@13
    .line 259
    return-void
.end method


# virtual methods
.method public final abortBroadcast()V
    .registers 2

    #@0
    .prologue
    .line 348
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver$PendingResult;->checkSynchronousHint()V

    #@3
    .line 349
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mAbortBroadcast:Z

    #@6
    .line 350
    return-void
.end method

.method checkSynchronousHint()V
    .registers 4

    #@0
    .prologue
    .line 440
    iget-boolean v1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mOrderedHint:Z

    #@2
    if-nez v1, :cond_8

    #@4
    iget-boolean v1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mInitialStickyHint:Z

    #@6
    if-eqz v1, :cond_9

    #@8
    .line 447
    :cond_8
    :goto_8
    return-void

    #@9
    .line 443
    :cond_9
    new-instance v0, Ljava/lang/RuntimeException;

    #@b
    const-string v1, "BroadcastReceiver trying to return result during a non-ordered broadcast"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@10
    .line 445
    .local v0, e:Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    #@13
    .line 446
    const-string v1, "BroadcastReceiver"

    #@15
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_8
.end method

.method public final clearAbortBroadcast()V
    .registers 2

    #@0
    .prologue
    .line 358
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mAbortBroadcast:Z

    #@3
    .line 359
    return-void
.end method

.method public final finish()V
    .registers 4

    #@0
    .prologue
    .line 366
    iget v1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mType:I

    #@2
    if-nez v1, :cond_1f

    #@4
    .line 367
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@7
    move-result-object v0

    #@8
    .line 368
    .local v0, mgr:Landroid/app/IActivityManager;
    invoke-static {}, Landroid/app/QueuedWork;->hasPendingWork()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_1b

    #@e
    .line 379
    invoke-static {}, Landroid/app/QueuedWork;->singleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    #@11
    move-result-object v1

    #@12
    new-instance v2, Landroid/content/BroadcastReceiver$PendingResult$1;

    #@14
    invoke-direct {v2, p0, v0}, Landroid/content/BroadcastReceiver$PendingResult$1;-><init>(Landroid/content/BroadcastReceiver$PendingResult;Landroid/app/IActivityManager;)V

    #@17
    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    #@1a
    .line 397
    .end local v0           #mgr:Landroid/app/IActivityManager;
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 389
    .restart local v0       #mgr:Landroid/app/IActivityManager;
    :cond_1b
    invoke-virtual {p0, v0}, Landroid/content/BroadcastReceiver$PendingResult;->sendFinished(Landroid/app/IActivityManager;)V

    #@1e
    goto :goto_1a

    #@1f
    .line 391
    .end local v0           #mgr:Landroid/app/IActivityManager;
    :cond_1f
    iget-boolean v1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mOrderedHint:Z

    #@21
    if-eqz v1, :cond_1a

    #@23
    iget v1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mType:I

    #@25
    const/4 v2, 0x2

    #@26
    if-eq v1, v2, :cond_1a

    #@28
    .line 394
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@2b
    move-result-object v0

    #@2c
    .line 395
    .restart local v0       #mgr:Landroid/app/IActivityManager;
    invoke-virtual {p0, v0}, Landroid/content/BroadcastReceiver$PendingResult;->sendFinished(Landroid/app/IActivityManager;)V

    #@2f
    goto :goto_1a
.end method

.method public final getAbortBroadcast()Z
    .registers 2

    #@0
    .prologue
    .line 339
    iget-boolean v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mAbortBroadcast:Z

    #@2
    return v0
.end method

.method public final getResultCode()I
    .registers 2

    #@0
    .prologue
    .line 277
    iget v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultCode:I

    #@2
    return v0
.end method

.method public final getResultData()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 296
    iget-object v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultData:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public final getResultExtras(Z)Landroid/os/Bundle;
    .registers 4
    .parameter "makeMap"

    #@0
    .prologue
    .line 315
    iget-object v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@2
    .line 316
    .local v0, e:Landroid/os/Bundle;
    if-nez p1, :cond_6

    #@4
    move-object v1, v0

    #@5
    .line 318
    .end local v0           #e:Landroid/os/Bundle;
    .local v1, e:Landroid/os/Bundle;
    :goto_5
    return-object v1

    #@6
    .line 317
    .end local v1           #e:Landroid/os/Bundle;
    .restart local v0       #e:Landroid/os/Bundle;
    :cond_6
    if-nez v0, :cond_f

    #@8
    new-instance v0, Landroid/os/Bundle;

    #@a
    .end local v0           #e:Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@d
    .restart local v0       #e:Landroid/os/Bundle;
    iput-object v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@f
    :cond_f
    move-object v1, v0

    #@10
    .line 318
    .end local v0           #e:Landroid/os/Bundle;
    .restart local v1       #e:Landroid/os/Bundle;
    goto :goto_5
.end method

.method public getSendingUserId()I
    .registers 2

    #@0
    .prologue
    .line 433
    iget v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mSendingUser:I

    #@2
    return v0
.end method

.method public sendFinished(Landroid/app/IActivityManager;)V
    .registers 8
    .parameter "am"

    #@0
    .prologue
    .line 408
    monitor-enter p0

    #@1
    .line 409
    :try_start_1
    iget-boolean v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mFinished:Z

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 410
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string v1, "Broadcast already finished"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 428
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0

    #@10
    .line 412
    :cond_10
    const/4 v0, 0x1

    #@11
    :try_start_11
    iput-boolean v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mFinished:Z
    :try_end_13
    .catchall {:try_start_11 .. :try_end_13} :catchall_d

    #@13
    .line 415
    :try_start_13
    iget-object v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@15
    if-eqz v0, :cond_1d

    #@17
    .line 416
    iget-object v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@19
    const/4 v1, 0x0

    #@1a
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setAllowFds(Z)Z

    #@1d
    .line 418
    :cond_1d
    iget-boolean v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mOrderedHint:Z

    #@1f
    if-eqz v0, :cond_31

    #@21
    .line 419
    iget-object v1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mToken:Landroid/os/IBinder;

    #@23
    iget v2, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultCode:I

    #@25
    iget-object v3, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultData:Ljava/lang/String;

    #@27
    iget-object v4, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@29
    iget-boolean v5, p0, Landroid/content/BroadcastReceiver$PendingResult;->mAbortBroadcast:Z

    #@2b
    move-object v0, p1

    #@2c
    invoke-interface/range {v0 .. v5}, Landroid/app/IActivityManager;->finishReceiver(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/Bundle;Z)V
    :try_end_2f
    .catchall {:try_start_13 .. :try_end_2f} :catchall_d
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_2f} :catch_3c

    #@2f
    .line 428
    :goto_2f
    :try_start_2f
    monitor-exit p0
    :try_end_30
    .catchall {:try_start_2f .. :try_end_30} :catchall_d

    #@30
    .line 429
    return-void

    #@31
    .line 424
    :cond_31
    :try_start_31
    iget-object v1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mToken:Landroid/os/IBinder;

    #@33
    const/4 v2, 0x0

    #@34
    const/4 v3, 0x0

    #@35
    const/4 v4, 0x0

    #@36
    const/4 v5, 0x0

    #@37
    move-object v0, p1

    #@38
    invoke-interface/range {v0 .. v5}, Landroid/app/IActivityManager;->finishReceiver(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/Bundle;Z)V
    :try_end_3b
    .catchall {:try_start_31 .. :try_end_3b} :catchall_d
    .catch Landroid/os/RemoteException; {:try_start_31 .. :try_end_3b} :catch_3c

    #@3b
    goto :goto_2f

    #@3c
    .line 426
    :catch_3c
    move-exception v0

    #@3d
    goto :goto_2f
.end method

.method public setExtrasClassLoader(Ljava/lang/ClassLoader;)V
    .registers 3
    .parameter "cl"

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 402
    iget-object v0, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    #@9
    .line 404
    :cond_9
    return-void
.end method

.method public final setResult(ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 4
    .parameter "code"
    .parameter "data"
    .parameter "extras"

    #@0
    .prologue
    .line 327
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver$PendingResult;->checkSynchronousHint()V

    #@3
    .line 328
    iput p1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultCode:I

    #@5
    .line 329
    iput-object p2, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultData:Ljava/lang/String;

    #@7
    .line 330
    iput-object p3, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@9
    .line 331
    return-void
.end method

.method public final setResultCode(I)V
    .registers 2
    .parameter "code"

    #@0
    .prologue
    .line 267
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver$PendingResult;->checkSynchronousHint()V

    #@3
    .line 268
    iput p1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultCode:I

    #@5
    .line 269
    return-void
.end method

.method public final setResultData(Ljava/lang/String;)V
    .registers 2
    .parameter "data"

    #@0
    .prologue
    .line 286
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver$PendingResult;->checkSynchronousHint()V

    #@3
    .line 287
    iput-object p1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultData:Ljava/lang/String;

    #@5
    .line 288
    return-void
.end method

.method public final setResultExtras(Landroid/os/Bundle;)V
    .registers 2
    .parameter "extras"

    #@0
    .prologue
    .line 305
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver$PendingResult;->checkSynchronousHint()V

    #@3
    .line 306
    iput-object p1, p0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@5
    .line 307
    return-void
.end method
