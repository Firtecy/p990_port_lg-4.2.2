.class public Landroid/content/ContentProviderOperation$Builder;
.super Ljava/lang/Object;
.source "ContentProviderOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/ContentProviderOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mExpectedCount:Ljava/lang/Integer;

.field private mSelection:Ljava/lang/String;

.field private mSelectionArgs:[Ljava/lang/String;

.field private mSelectionArgsBackReferences:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mType:I

.field private final mUri:Landroid/net/Uri;

.field private mValues:Landroid/content/ContentValues;

.field private mValuesBackReferences:Landroid/content/ContentValues;

.field private mYieldAllowed:Z


# direct methods
.method private constructor <init>(ILandroid/net/Uri;)V
    .registers 5
    .parameter "type"
    .parameter "uri"

    #@0
    .prologue
    .line 413
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 414
    if-nez p2, :cond_e

    #@5
    .line 415
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string/jumbo v1, "uri must not be null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 417
    :cond_e
    iput p1, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@10
    .line 418
    iput-object p2, p0, Landroid/content/ContentProviderOperation$Builder;->mUri:Landroid/net/Uri;

    #@12
    .line 419
    return-void
.end method

.method synthetic constructor <init>(ILandroid/net/Uri;Landroid/content/ContentProviderOperation$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 401
    invoke-direct {p0, p1, p2}, Landroid/content/ContentProviderOperation$Builder;-><init>(ILandroid/net/Uri;)V

    #@3
    return-void
.end method

.method static synthetic access$000(Landroid/content/ContentProviderOperation$Builder;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 401
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/content/ContentProviderOperation$Builder;)Landroid/net/Uri;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mUri:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/content/ContentProviderOperation$Builder;)Landroid/content/ContentValues;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/content/ContentProviderOperation$Builder;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mSelection:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/content/ContentProviderOperation$Builder;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mSelectionArgs:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/content/ContentProviderOperation$Builder;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mExpectedCount:Ljava/lang/Integer;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/content/ContentProviderOperation$Builder;)Ljava/util/Map;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/content/ContentProviderOperation$Builder;)Landroid/content/ContentValues;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValuesBackReferences:Landroid/content/ContentValues;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/content/ContentProviderOperation$Builder;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 401
    iget-boolean v0, p0, Landroid/content/ContentProviderOperation$Builder;->mYieldAllowed:Z

    #@2
    return v0
.end method


# virtual methods
.method public build()Landroid/content/ContentProviderOperation;
    .registers 3

    #@0
    .prologue
    .line 423
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_25

    #@5
    .line 424
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@7
    if-eqz v0, :cond_11

    #@9
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@b
    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_25

    #@11
    :cond_11
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValuesBackReferences:Landroid/content/ContentValues;

    #@13
    if-eqz v0, :cond_1d

    #@15
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValuesBackReferences:Landroid/content/ContentValues;

    #@17
    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    #@1a
    move-result v0

    #@1b
    if-nez v0, :cond_25

    #@1d
    .line 426
    :cond_1d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string v1, "Empty values"

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 429
    :cond_25
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@27
    const/4 v1, 0x4

    #@28
    if-ne v0, v1, :cond_4e

    #@2a
    .line 430
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@2c
    if-eqz v0, :cond_36

    #@2e
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@30
    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    #@33
    move-result v0

    #@34
    if-nez v0, :cond_4e

    #@36
    :cond_36
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValuesBackReferences:Landroid/content/ContentValues;

    #@38
    if-eqz v0, :cond_42

    #@3a
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValuesBackReferences:Landroid/content/ContentValues;

    #@3c
    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    #@3f
    move-result v0

    #@40
    if-nez v0, :cond_4e

    #@42
    :cond_42
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mExpectedCount:Ljava/lang/Integer;

    #@44
    if-nez v0, :cond_4e

    #@46
    .line 433
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@48
    const-string v1, "Empty values"

    #@4a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4d
    throw v0

    #@4e
    .line 436
    :cond_4e
    new-instance v0, Landroid/content/ContentProviderOperation;

    #@50
    const/4 v1, 0x0

    #@51
    invoke-direct {v0, p0, v1}, Landroid/content/ContentProviderOperation;-><init>(Landroid/content/ContentProviderOperation$Builder;Landroid/content/ContentProviderOperation$1;)V

    #@54
    return-object v0
.end method

.method public withExpectedCount(I)Landroid/content/ContentProviderOperation$Builder;
    .registers 4
    .parameter "count"

    #@0
    .prologue
    .line 585
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@2
    const/4 v1, 0x2

    #@3
    if-eq v0, v1, :cond_18

    #@5
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@7
    const/4 v1, 0x3

    #@8
    if-eq v0, v1, :cond_18

    #@a
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@c
    const/4 v1, 0x4

    #@d
    if-eq v0, v1, :cond_18

    #@f
    .line 586
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string/jumbo v1, "only updates, deletes, and asserts can have expected counts"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 589
    :cond_18
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mExpectedCount:Ljava/lang/Integer;

    #@1e
    .line 590
    return-object p0
.end method

.method public withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;
    .registers 6
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 564
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@3
    const/4 v1, 0x2

    #@4
    if-eq v0, v1, :cond_19

    #@6
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@8
    const/4 v1, 0x3

    #@9
    if-eq v0, v1, :cond_19

    #@b
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@d
    const/4 v1, 0x4

    #@e
    if-eq v0, v1, :cond_19

    #@10
    .line 565
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string/jumbo v1, "only updates, deletes, and asserts can have selections"

    #@15
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0

    #@19
    .line 568
    :cond_19
    iput-object p1, p0, Landroid/content/ContentProviderOperation$Builder;->mSelection:Ljava/lang/String;

    #@1b
    .line 569
    if-nez p2, :cond_21

    #@1d
    .line 570
    const/4 v0, 0x0

    #@1e
    iput-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mSelectionArgs:[Ljava/lang/String;

    #@20
    .line 575
    :goto_20
    return-object p0

    #@21
    .line 572
    :cond_21
    array-length v0, p2

    #@22
    new-array v0, v0, [Ljava/lang/String;

    #@24
    iput-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mSelectionArgs:[Ljava/lang/String;

    #@26
    .line 573
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mSelectionArgs:[Ljava/lang/String;

    #@28
    array-length v1, p2

    #@29
    invoke-static {p2, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2c
    goto :goto_20
.end method

.method public withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;
    .registers 6
    .parameter "selectionArgIndex"
    .parameter "previousResult"

    #@0
    .prologue
    .line 483
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@2
    const/4 v1, 0x2

    #@3
    if-eq v0, v1, :cond_18

    #@5
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@7
    const/4 v1, 0x3

    #@8
    if-eq v0, v1, :cond_18

    #@a
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@c
    const/4 v1, 0x4

    #@d
    if-eq v0, v1, :cond_18

    #@f
    .line 484
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string/jumbo v1, "only updates, deletes, and asserts can have selection back-references"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 487
    :cond_18
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@1a
    if-nez v0, :cond_23

    #@1c
    .line 488
    new-instance v0, Ljava/util/HashMap;

    #@1e
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@21
    iput-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@23
    .line 490
    :cond_23
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mSelectionArgsBackReferences:Ljava/util/Map;

    #@25
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@28
    move-result-object v1

    #@29
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v2

    #@2d
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    .line 491
    return-object p0
.end method

.method public withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    .registers 6
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 523
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_18

    #@5
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@7
    const/4 v1, 0x2

    #@8
    if-eq v0, v1, :cond_18

    #@a
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@c
    const/4 v1, 0x4

    #@d
    if-eq v0, v1, :cond_18

    #@f
    .line 524
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string/jumbo v1, "only inserts and updates can have values"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 526
    :cond_18
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@1a
    if-nez v0, :cond_23

    #@1c
    .line 527
    new-instance v0, Landroid/content/ContentValues;

    #@1e
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@21
    iput-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@23
    .line 529
    :cond_23
    if-nez p2, :cond_2b

    #@25
    .line 530
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@27
    invoke-virtual {v0, p1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    #@2a
    .line 552
    .end local p2
    :goto_2a
    return-object p0

    #@2b
    .line 531
    .restart local p2
    :cond_2b
    instance-of v0, p2, Ljava/lang/String;

    #@2d
    if-eqz v0, :cond_37

    #@2f
    .line 532
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@31
    check-cast p2, Ljava/lang/String;

    #@33
    .end local p2
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    goto :goto_2a

    #@37
    .line 533
    .restart local p2
    :cond_37
    instance-of v0, p2, Ljava/lang/Byte;

    #@39
    if-eqz v0, :cond_43

    #@3b
    .line 534
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@3d
    check-cast p2, Ljava/lang/Byte;

    #@3f
    .end local p2
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    #@42
    goto :goto_2a

    #@43
    .line 535
    .restart local p2
    :cond_43
    instance-of v0, p2, Ljava/lang/Short;

    #@45
    if-eqz v0, :cond_4f

    #@47
    .line 536
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@49
    check-cast p2, Ljava/lang/Short;

    #@4b
    .end local p2
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    #@4e
    goto :goto_2a

    #@4f
    .line 537
    .restart local p2
    :cond_4f
    instance-of v0, p2, Ljava/lang/Integer;

    #@51
    if-eqz v0, :cond_5b

    #@53
    .line 538
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@55
    check-cast p2, Ljava/lang/Integer;

    #@57
    .end local p2
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5a
    goto :goto_2a

    #@5b
    .line 539
    .restart local p2
    :cond_5b
    instance-of v0, p2, Ljava/lang/Long;

    #@5d
    if-eqz v0, :cond_67

    #@5f
    .line 540
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@61
    check-cast p2, Ljava/lang/Long;

    #@63
    .end local p2
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@66
    goto :goto_2a

    #@67
    .line 541
    .restart local p2
    :cond_67
    instance-of v0, p2, Ljava/lang/Float;

    #@69
    if-eqz v0, :cond_73

    #@6b
    .line 542
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@6d
    check-cast p2, Ljava/lang/Float;

    #@6f
    .end local p2
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    #@72
    goto :goto_2a

    #@73
    .line 543
    .restart local p2
    :cond_73
    instance-of v0, p2, Ljava/lang/Double;

    #@75
    if-eqz v0, :cond_7f

    #@77
    .line 544
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@79
    check-cast p2, Ljava/lang/Double;

    #@7b
    .end local p2
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    #@7e
    goto :goto_2a

    #@7f
    .line 545
    .restart local p2
    :cond_7f
    instance-of v0, p2, Ljava/lang/Boolean;

    #@81
    if-eqz v0, :cond_8b

    #@83
    .line 546
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@85
    check-cast p2, Ljava/lang/Boolean;

    #@87
    .end local p2
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@8a
    goto :goto_2a

    #@8b
    .line 547
    .restart local p2
    :cond_8b
    instance-of v0, p2, [B

    #@8d
    if-eqz v0, :cond_99

    #@8f
    .line 548
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@91
    check-cast p2, [B

    #@93
    .end local p2
    check-cast p2, [B

    #@95
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@98
    goto :goto_2a

    #@99
    .line 550
    .restart local p2
    :cond_99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9b
    new-instance v1, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v2, "bad value type: "

    #@a2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v1

    #@a6
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a9
    move-result-object v2

    #@aa
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@ad
    move-result-object v2

    #@ae
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v1

    #@b2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v1

    #@b6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b9
    throw v0
.end method

.method public withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;
    .registers 5
    .parameter "key"
    .parameter "previousResult"

    #@0
    .prologue
    .line 465
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_18

    #@5
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@7
    const/4 v1, 0x2

    #@8
    if-eq v0, v1, :cond_18

    #@a
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@c
    const/4 v1, 0x4

    #@d
    if-eq v0, v1, :cond_18

    #@f
    .line 466
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string/jumbo v1, "only inserts, updates, and asserts can have value back-references"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 469
    :cond_18
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValuesBackReferences:Landroid/content/ContentValues;

    #@1a
    if-nez v0, :cond_23

    #@1c
    .line 470
    new-instance v0, Landroid/content/ContentValues;

    #@1e
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@21
    iput-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValuesBackReferences:Landroid/content/ContentValues;

    #@23
    .line 472
    :cond_23
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValuesBackReferences:Landroid/content/ContentValues;

    #@25
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2c
    .line 473
    return-object p0
.end method

.method public withValueBackReferences(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;
    .registers 4
    .parameter "backReferences"

    #@0
    .prologue
    .line 449
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_18

    #@5
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@7
    const/4 v1, 0x2

    #@8
    if-eq v0, v1, :cond_18

    #@a
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@c
    const/4 v1, 0x4

    #@d
    if-eq v0, v1, :cond_18

    #@f
    .line 450
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string/jumbo v1, "only inserts, updates, and asserts can have value back-references"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 453
    :cond_18
    iput-object p1, p0, Landroid/content/ContentProviderOperation$Builder;->mValuesBackReferences:Landroid/content/ContentValues;

    #@1a
    .line 454
    return-object p0
.end method

.method public withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;
    .registers 4
    .parameter "values"

    #@0
    .prologue
    .line 502
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_18

    #@5
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@7
    const/4 v1, 0x2

    #@8
    if-eq v0, v1, :cond_18

    #@a
    iget v0, p0, Landroid/content/ContentProviderOperation$Builder;->mType:I

    #@c
    const/4 v1, 0x4

    #@d
    if-eq v0, v1, :cond_18

    #@f
    .line 503
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string/jumbo v1, "only inserts, updates, and asserts can have values"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 506
    :cond_18
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@1a
    if-nez v0, :cond_23

    #@1c
    .line 507
    new-instance v0, Landroid/content/ContentValues;

    #@1e
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@21
    iput-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@23
    .line 509
    :cond_23
    iget-object v0, p0, Landroid/content/ContentProviderOperation$Builder;->mValues:Landroid/content/ContentValues;

    #@25
    invoke-virtual {v0, p1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    #@28
    .line 510
    return-object p0
.end method

.method public withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;
    .registers 2
    .parameter "yieldAllowed"

    #@0
    .prologue
    .line 594
    iput-boolean p1, p0, Landroid/content/ContentProviderOperation$Builder;->mYieldAllowed:Z

    #@2
    .line 595
    return-object p0
.end method
