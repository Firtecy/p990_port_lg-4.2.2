.class public abstract Landroid/content/BroadcastReceiver;
.super Ljava/lang/Object;
.source "BroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/BroadcastReceiver$PendingResult;
    }
.end annotation


# instance fields
.field private mDebugUnregister:Z

.field private mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 450
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 451
    return-void
.end method


# virtual methods
.method public final abortBroadcast()V
    .registers 3

    #@0
    .prologue
    .line 689
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->checkSynchronousHint()V

    #@3
    .line 690
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@5
    const/4 v1, 0x1

    #@6
    iput-boolean v1, v0, Landroid/content/BroadcastReceiver$PendingResult;->mAbortBroadcast:Z

    #@8
    .line 691
    return-void
.end method

.method checkSynchronousHint()V
    .registers 4

    #@0
    .prologue
    .line 771
    iget-object v1, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 772
    new-instance v1, Ljava/lang/IllegalStateException;

    #@6
    const-string v2, "Call while result is not pending"

    #@8
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1

    #@c
    .line 778
    :cond_c
    iget-object v1, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@e
    iget-boolean v1, v1, Landroid/content/BroadcastReceiver$PendingResult;->mOrderedHint:Z

    #@10
    if-nez v1, :cond_18

    #@12
    iget-object v1, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@14
    iget-boolean v1, v1, Landroid/content/BroadcastReceiver$PendingResult;->mInitialStickyHint:Z

    #@16
    if-eqz v1, :cond_19

    #@18
    .line 785
    :cond_18
    :goto_18
    return-void

    #@19
    .line 781
    :cond_19
    new-instance v0, Ljava/lang/RuntimeException;

    #@1b
    const-string v1, "BroadcastReceiver trying to return result during a non-ordered broadcast"

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@20
    .line 783
    .local v0, e:Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    #@23
    .line 784
    const-string v1, "BroadcastReceiver"

    #@25
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2c
    goto :goto_18
.end method

.method public final clearAbortBroadcast()V
    .registers 3

    #@0
    .prologue
    .line 698
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 699
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@6
    const/4 v1, 0x0

    #@7
    iput-boolean v1, v0, Landroid/content/BroadcastReceiver$PendingResult;->mAbortBroadcast:Z

    #@9
    .line 701
    :cond_9
    return-void
.end method

.method public final getAbortBroadcast()Z
    .registers 2

    #@0
    .prologue
    .line 671
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@6
    iget-boolean v0, v0, Landroid/content/BroadcastReceiver$PendingResult;->mAbortBroadcast:Z

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public final getDebugUnregister()Z
    .registers 2

    #@0
    .prologue
    .line 767
    iget-boolean v0, p0, Landroid/content/BroadcastReceiver;->mDebugUnregister:Z

    #@2
    return v0
.end method

.method public final getPendingResult()Landroid/content/BroadcastReceiver$PendingResult;
    .registers 2

    #@0
    .prologue
    .line 740
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    return-object v0
.end method

.method public final getResultCode()I
    .registers 2

    #@0
    .prologue
    .line 559
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@6
    iget v0, v0, Landroid/content/BroadcastReceiver$PendingResult;->mResultCode:I

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public final getResultData()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 589
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@6
    iget-object v0, v0, Landroid/content/BroadcastReceiver$PendingResult;->mResultData:Ljava/lang/String;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public final getResultExtras(Z)Landroid/os/Bundle;
    .registers 4
    .parameter "makeMap"

    #@0
    .prologue
    .line 626
    iget-object v1, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    if-nez v1, :cond_6

    #@4
    .line 627
    const/4 v0, 0x0

    #@5
    .line 632
    :cond_5
    :goto_5
    return-object v0

    #@6
    .line 629
    :cond_6
    iget-object v1, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@8
    iget-object v0, v1, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@a
    .line 630
    .local v0, e:Landroid/os/Bundle;
    if-eqz p1, :cond_5

    #@c
    .line 631
    if-nez v0, :cond_5

    #@e
    iget-object v1, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@10
    new-instance v0, Landroid/os/Bundle;

    #@12
    .end local v0           #e:Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@15
    .restart local v0       #e:Landroid/os/Bundle;
    iput-object v0, v1, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@17
    goto :goto_5
.end method

.method public getSendingUserId()I
    .registers 2

    #@0
    .prologue
    .line 745
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    iget v0, v0, Landroid/content/BroadcastReceiver$PendingResult;->mSendingUser:I

    #@4
    return v0
.end method

.method public final goAsync()Landroid/content/BroadcastReceiver$PendingResult;
    .registers 3

    #@0
    .prologue
    .line 505
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    .line 506
    .local v0, res:Landroid/content/BroadcastReceiver$PendingResult;
    const/4 v1, 0x0

    #@3
    iput-object v1, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@5
    .line 507
    return-object v0
.end method

.method public final isInitialStickyBroadcast()Z
    .registers 2

    #@0
    .prologue
    .line 718
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@6
    iget-boolean v0, v0, Landroid/content/BroadcastReceiver$PendingResult;->mInitialStickyHint:Z

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public final isOrderedBroadcast()Z
    .registers 2

    #@0
    .prologue
    .line 708
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@6
    iget-boolean v0, v0, Landroid/content/BroadcastReceiver$PendingResult;->mOrderedHint:Z

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public abstract onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end method

.method public peekService(Landroid/content/Context;Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 6
    .parameter "myContext"
    .parameter "service"

    #@0
    .prologue
    .line 520
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    .line 521
    .local v0, am:Landroid/app/IActivityManager;
    const/4 v1, 0x0

    #@5
    .line 523
    .local v1, binder:Landroid/os/IBinder;
    const/4 v2, 0x0

    #@6
    :try_start_6
    invoke-virtual {p2, v2}, Landroid/content/Intent;->setAllowFds(Z)V

    #@9
    .line 524
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {p2, v2}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-interface {v0, p2, v2}, Landroid/app/IActivityManager;->peekService(Landroid/content/Intent;Ljava/lang/String;)Landroid/os/IBinder;
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_14} :catch_16

    #@14
    move-result-object v1

    #@15
    .line 528
    :goto_15
    return-object v1

    #@16
    .line 526
    :catch_16
    move-exception v2

    #@17
    goto :goto_15
.end method

.method public final setDebugUnregister(Z)V
    .registers 2
    .parameter "debug"

    #@0
    .prologue
    .line 760
    iput-boolean p1, p0, Landroid/content/BroadcastReceiver;->mDebugUnregister:Z

    #@2
    .line 761
    return-void
.end method

.method public final setOrderedHint(Z)V
    .registers 2
    .parameter "isOrdered"

    #@0
    .prologue
    .line 727
    return-void
.end method

.method public final setPendingResult(Landroid/content/BroadcastReceiver$PendingResult;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 733
    iput-object p1, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@2
    .line 734
    return-void
.end method

.method public final setResult(ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 5
    .parameter "code"
    .parameter "data"
    .parameter "extras"

    #@0
    .prologue
    .line 658
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->checkSynchronousHint()V

    #@3
    .line 659
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@5
    iput p1, v0, Landroid/content/BroadcastReceiver$PendingResult;->mResultCode:I

    #@7
    .line 660
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@9
    iput-object p2, v0, Landroid/content/BroadcastReceiver$PendingResult;->mResultData:Ljava/lang/String;

    #@b
    .line 661
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@d
    iput-object p3, v0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@f
    .line 662
    return-void
.end method

.method public final setResultCode(I)V
    .registers 3
    .parameter "code"

    #@0
    .prologue
    .line 549
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->checkSynchronousHint()V

    #@3
    .line 550
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@5
    iput p1, v0, Landroid/content/BroadcastReceiver$PendingResult;->mResultCode:I

    #@7
    .line 551
    return-void
.end method

.method public final setResultData(Ljava/lang/String;)V
    .registers 3
    .parameter "data"

    #@0
    .prologue
    .line 578
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->checkSynchronousHint()V

    #@3
    .line 579
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@5
    iput-object p1, v0, Landroid/content/BroadcastReceiver$PendingResult;->mResultData:Ljava/lang/String;

    #@7
    .line 580
    return-void
.end method

.method public final setResultExtras(Landroid/os/Bundle;)V
    .registers 3
    .parameter "extras"

    #@0
    .prologue
    .line 610
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->checkSynchronousHint()V

    #@3
    .line 611
    iget-object v0, p0, Landroid/content/BroadcastReceiver;->mPendingResult:Landroid/content/BroadcastReceiver$PendingResult;

    #@5
    iput-object p1, v0, Landroid/content/BroadcastReceiver$PendingResult;->mResultExtras:Landroid/os/Bundle;

    #@7
    .line 612
    return-void
.end method
