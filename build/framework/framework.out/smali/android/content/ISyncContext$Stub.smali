.class public abstract Landroid/content/ISyncContext$Stub;
.super Landroid/os/Binder;
.source "ISyncContext.java"

# interfaces
.implements Landroid/content/ISyncContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/ISyncContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/ISyncContext$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.content.ISyncContext"

.field static final TRANSACTION_onFinished:I = 0x2

.field static final TRANSACTION_sendHeartbeat:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "android.content.ISyncContext"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/content/ISyncContext$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/content/ISyncContext;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "android.content.ISyncContext"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/content/ISyncContext;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Landroid/content/ISyncContext;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Landroid/content/ISyncContext$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/content/ISyncContext$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 42
    sparse-switch p1, :sswitch_data_38

    #@4
    .line 71
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v1

    #@8
    :goto_8
    return v1

    #@9
    .line 46
    :sswitch_9
    const-string v2, "android.content.ISyncContext"

    #@b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 51
    :sswitch_f
    const-string v2, "android.content.ISyncContext"

    #@11
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 52
    invoke-virtual {p0}, Landroid/content/ISyncContext$Stub;->sendHeartbeat()V

    #@17
    .line 53
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a
    goto :goto_8

    #@1b
    .line 58
    :sswitch_1b
    const-string v2, "android.content.ISyncContext"

    #@1d
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20
    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_35

    #@26
    .line 61
    sget-object v2, Landroid/content/SyncResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@28
    invoke-interface {v2, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    check-cast v0, Landroid/content/SyncResult;

    #@2e
    .line 66
    .local v0, _arg0:Landroid/content/SyncResult;
    :goto_2e
    invoke-virtual {p0, v0}, Landroid/content/ISyncContext$Stub;->onFinished(Landroid/content/SyncResult;)V

    #@31
    .line 67
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34
    goto :goto_8

    #@35
    .line 64
    .end local v0           #_arg0:Landroid/content/SyncResult;
    :cond_35
    const/4 v0, 0x0

    #@36
    .restart local v0       #_arg0:Landroid/content/SyncResult;
    goto :goto_2e

    #@37
    .line 42
    nop

    #@38
    :sswitch_data_38
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1b
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
