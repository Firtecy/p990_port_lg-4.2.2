.class public Landroid/content/ClipboardManager;
.super Landroid/text/ClipboardManager;
.source "ClipboardManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;
    }
.end annotation


# static fields
.field static final MSG_REPORT_PRIMARY_CLIP_CHANGED:I = 0x1

.field private static sService:Landroid/content/IClipboard;

.field private static final sStaticLock:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mPrimaryClipChangedListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mPrimaryClipChangedServiceListener:Landroid/content/IOnPrimaryClipChangedListener$Stub;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 72
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/content/ClipboardManager;->sStaticLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 4
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    .line 128
    invoke-direct {p0}, Landroid/text/ClipboardManager;-><init>()V

    #@3
    .line 77
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedListeners:Ljava/util/ArrayList;

    #@a
    .line 80
    new-instance v0, Landroid/content/ClipboardManager$1;

    #@c
    invoke-direct {v0, p0}, Landroid/content/ClipboardManager$1;-><init>(Landroid/content/ClipboardManager;)V

    #@f
    iput-object v0, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedServiceListener:Landroid/content/IOnPrimaryClipChangedListener$Stub;

    #@11
    .line 89
    new-instance v0, Landroid/content/ClipboardManager$2;

    #@13
    invoke-direct {v0, p0}, Landroid/content/ClipboardManager$2;-><init>(Landroid/content/ClipboardManager;)V

    #@16
    iput-object v0, p0, Landroid/content/ClipboardManager;->mHandler:Landroid/os/Handler;

    #@18
    .line 129
    iput-object p1, p0, Landroid/content/ClipboardManager;->mContext:Landroid/content/Context;

    #@1a
    .line 130
    return-void
.end method

.method static synthetic access$000(Landroid/content/ClipboardManager;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/content/ClipboardManager;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private static getService()Landroid/content/IClipboard;
    .registers 3

    #@0
    .prologue
    .line 117
    sget-object v2, Landroid/content/ClipboardManager;->sStaticLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 118
    :try_start_3
    sget-object v1, Landroid/content/ClipboardManager;->sService:Landroid/content/IClipboard;

    #@5
    if-eqz v1, :cond_b

    #@7
    .line 119
    sget-object v1, Landroid/content/ClipboardManager;->sService:Landroid/content/IClipboard;

    #@9
    monitor-exit v2

    #@a
    .line 123
    .local v0, b:Landroid/os/IBinder;
    :goto_a
    return-object v1

    #@b
    .line 121
    .end local v0           #b:Landroid/os/IBinder;
    :cond_b
    const-string v1, "clipboard"

    #@d
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v0

    #@11
    .line 122
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/content/IClipboard$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IClipboard;

    #@14
    move-result-object v1

    #@15
    sput-object v1, Landroid/content/ClipboardManager;->sService:Landroid/content/IClipboard;

    #@17
    .line 123
    sget-object v1, Landroid/content/ClipboardManager;->sService:Landroid/content/IClipboard;

    #@19
    monitor-exit v2

    #@1a
    goto :goto_a

    #@1b
    .line 124
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    #@1d
    throw v1
.end method


# virtual methods
.method public addPrimaryClipChangedListener(Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;)V
    .registers 5
    .parameter "what"

    #@0
    .prologue
    .line 180
    iget-object v1, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 181
    :try_start_3
    iget-object v0, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedListeners:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_1b

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_14

    #@b
    .line 183
    :try_start_b
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@e
    move-result-object v0

    #@f
    iget-object v2, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedServiceListener:Landroid/content/IOnPrimaryClipChangedListener$Stub;

    #@11
    invoke-interface {v0, v2}, Landroid/content/IClipboard;->addPrimaryClipChangedListener(Landroid/content/IOnPrimaryClipChangedListener;)V
    :try_end_14
    .catchall {:try_start_b .. :try_end_14} :catchall_1b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_14} :catch_1e

    #@14
    .line 188
    :cond_14
    :goto_14
    :try_start_14
    iget-object v0, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedListeners:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@19
    .line 189
    monitor-exit v1

    #@1a
    .line 190
    return-void

    #@1b
    .line 189
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_14 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0

    #@1e
    .line 185
    :catch_1e
    move-exception v0

    #@1f
    goto :goto_14
.end method

.method public getInitialPrimaryClipAt(I)Landroid/content/ClipData;
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 334
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/content/ClipboardManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v1, v2, p1}, Landroid/content/IClipboard;->getInitialPrimaryClipAt(Ljava/lang/String;I)Landroid/content/ClipData;
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result-object v1

    #@e
    .line 336
    :goto_e
    return-object v1

    #@f
    .line 335
    :catch_f
    move-exception v0

    #@10
    .line 336
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@11
    goto :goto_e
.end method

.method public getPrimaryClip()Landroid/content/ClipData;
    .registers 4

    #@0
    .prologue
    .line 150
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/content/ClipboardManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v1, v2}, Landroid/content/IClipboard;->getPrimaryClip(Ljava/lang/String;)Landroid/content/ClipData;
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result-object v1

    #@e
    .line 152
    :goto_e
    return-object v1

    #@f
    .line 151
    :catch_f
    move-exception v0

    #@10
    .line 152
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@11
    goto :goto_e
.end method

.method public getPrimaryClipAt(I)Landroid/content/ClipData;
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 260
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/content/ClipboardManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v1, v2, p1}, Landroid/content/IClipboard;->getPrimaryClipAt(Ljava/lang/String;I)Landroid/content/ClipData;
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result-object v1

    #@e
    .line 262
    :goto_e
    return-object v1

    #@f
    .line 261
    :catch_f
    move-exception v0

    #@10
    .line 262
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@11
    goto :goto_e
.end method

.method public getPrimaryClipCount()I
    .registers 3

    #@0
    .prologue
    .line 285
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/content/IClipboard;->getPrimaryClipCount()I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 287
    :goto_8
    return v1

    #@9
    .line 286
    :catch_9
    move-exception v0

    #@a
    .line 287
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getPrimaryClipDescription()Landroid/content/ClipDescription;
    .registers 3

    #@0
    .prologue
    .line 162
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/content/IClipboard;->getPrimaryClipDescription()Landroid/content/ClipDescription;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 164
    :goto_8
    return-object v1

    #@9
    .line 163
    :catch_9
    move-exception v0

    #@a
    .line 164
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getPrimaryClipDescriptionAt(I)Landroid/content/ClipDescription;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 273
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Landroid/content/IClipboard;->getPrimaryClipDescriptionAt(I)Landroid/content/ClipDescription;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 275
    :goto_8
    return-object v1

    #@9
    .line 274
    :catch_9
    move-exception v0

    #@a
    .line 275
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getText()Ljava/lang/CharSequence;
    .registers 4

    #@0
    .prologue
    .line 210
    invoke-virtual {p0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    #@3
    move-result-object v0

    #@4
    .line 211
    .local v0, clip:Landroid/content/ClipData;
    if-eqz v0, :cond_18

    #@6
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    #@9
    move-result v1

    #@a
    if-lez v1, :cond_18

    #@c
    .line 212
    const/4 v1, 0x0

    #@d
    invoke-virtual {v0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@10
    move-result-object v1

    #@11
    iget-object v2, p0, Landroid/content/ClipboardManager;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v1, v2}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@16
    move-result-object v1

    #@17
    .line 214
    :goto_17
    return-object v1

    #@18
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_17
.end method

.method public hasPrimaryClip()Z
    .registers 3

    #@0
    .prologue
    .line 173
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/content/IClipboard;->hasPrimaryClip()Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 175
    :goto_8
    return v1

    #@9
    .line 174
    :catch_9
    move-exception v0

    #@a
    .line 175
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public hasText()Z
    .registers 3

    #@0
    .prologue
    .line 231
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/content/IClipboard;->hasClipboardText()Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 233
    :goto_8
    return v1

    #@9
    .line 232
    :catch_9
    move-exception v0

    #@a
    .line 233
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public isUidSecure()Z
    .registers 3

    #@0
    .prologue
    .line 321
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/content/IClipboard;->isUidSecure()Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 323
    :goto_8
    return v1

    #@9
    .line 322
    :catch_9
    move-exception v0

    #@a
    .line 323
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public removeAllPrimaryClips()Z
    .registers 4

    #@0
    .prologue
    .line 309
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/content/ClipboardManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v1, v2}, Landroid/content/IClipboard;->removeAllPrimaryClips(Ljava/lang/String;)Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 311
    :goto_e
    return v1

    #@f
    .line 310
    :catch_f
    move-exception v0

    #@10
    .line 311
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@11
    goto :goto_e
.end method

.method public removePrimaryClipAt(I)Z
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 297
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/content/ClipboardManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v1, v2, p1}, Landroid/content/IClipboard;->removePrimaryClipAt(Ljava/lang/String;I)Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 299
    :goto_e
    return v1

    #@f
    .line 298
    :catch_f
    move-exception v0

    #@10
    .line 299
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@11
    goto :goto_e
.end method

.method public removePrimaryClipChangedListener(Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;)V
    .registers 5
    .parameter "what"

    #@0
    .prologue
    .line 193
    iget-object v1, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 194
    :try_start_3
    iget-object v0, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedListeners:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8
    .line 195
    iget-object v0, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedListeners:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_1b

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_19

    #@10
    .line 197
    :try_start_10
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@13
    move-result-object v0

    #@14
    iget-object v2, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedServiceListener:Landroid/content/IOnPrimaryClipChangedListener$Stub;

    #@16
    invoke-interface {v0, v2}, Landroid/content/IClipboard;->removePrimaryClipChangedListener(Landroid/content/IOnPrimaryClipChangedListener;)V
    :try_end_19
    .catchall {:try_start_10 .. :try_end_19} :catchall_1b
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_19} :catch_1e

    #@19
    .line 202
    :cond_19
    :goto_19
    :try_start_19
    monitor-exit v1

    #@1a
    .line 203
    return-void

    #@1b
    .line 202
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_19 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0

    #@1e
    .line 199
    :catch_1e
    move-exception v0

    #@1f
    goto :goto_19
.end method

.method reportPrimaryClipChanged()V
    .registers 6

    #@0
    .prologue
    .line 240
    iget-object v4, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 241
    :try_start_3
    iget-object v3, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedListeners:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    .line 242
    .local v0, N:I
    if-gtz v0, :cond_d

    #@b
    .line 243
    monitor-exit v4

    #@c
    .line 251
    :cond_c
    return-void

    #@d
    .line 245
    :cond_d
    iget-object v3, p0, Landroid/content/ClipboardManager;->mPrimaryClipChangedListeners:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    .line 246
    .local v2, listeners:[Ljava/lang/Object;
    monitor-exit v4
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_22

    #@14
    .line 248
    const/4 v1, 0x0

    #@15
    .local v1, i:I
    :goto_15
    array-length v3, v2

    #@16
    if-ge v1, v3, :cond_c

    #@18
    .line 249
    aget-object v3, v2, v1

    #@1a
    check-cast v3, Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;

    #@1c
    invoke-interface {v3}, Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;->onPrimaryClipChanged()V

    #@1f
    .line 248
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_15

    #@22
    .line 246
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #listeners:[Ljava/lang/Object;
    :catchall_22
    move-exception v3

    #@23
    :try_start_23
    monitor-exit v4
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_22

    #@24
    throw v3
.end method

.method public setPrimaryClip(Landroid/content/ClipData;)V
    .registers 3
    .parameter "clip"

    #@0
    .prologue
    .line 140
    :try_start_0
    invoke-static {}, Landroid/content/ClipboardManager;->getService()Landroid/content/IClipboard;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p1}, Landroid/content/IClipboard;->setPrimaryClip(Landroid/content/ClipData;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 143
    :goto_7
    return-void

    #@8
    .line 141
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 223
    const/4 v0, 0x0

    #@1
    invoke-static {v0, p1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {p0, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@8
    .line 224
    return-void
.end method
