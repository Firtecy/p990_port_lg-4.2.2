.class Landroid/content/SyncManager$SyncTimeTracker;
.super Ljava/lang/Object;
.source "SyncManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/SyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncTimeTracker"
.end annotation


# instance fields
.field mLastWasSyncing:Z

.field private mTimeSpentSyncing:J

.field mWhenSyncStarted:J

.field final synthetic this$0:Landroid/content/SyncManager;


# direct methods
.method private constructor <init>(Landroid/content/SyncManager;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 1573
    iput-object p1, p0, Landroid/content/SyncManager$SyncTimeTracker;->this$0:Landroid/content/SyncManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1575
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/content/SyncManager$SyncTimeTracker;->mLastWasSyncing:Z

    #@8
    .line 1577
    const-wide/16 v0, 0x0

    #@a
    iput-wide v0, p0, Landroid/content/SyncManager$SyncTimeTracker;->mWhenSyncStarted:J

    #@c
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/SyncManager;Landroid/content/SyncManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1573
    invoke-direct {p0, p1}, Landroid/content/SyncManager$SyncTimeTracker;-><init>(Landroid/content/SyncManager;)V

    #@3
    return-void
.end method


# virtual methods
.method public declared-synchronized timeSpentSyncing()J
    .registers 7

    #@0
    .prologue
    .line 1596
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v2, p0, Landroid/content/SyncManager$SyncTimeTracker;->mLastWasSyncing:Z

    #@3
    if-nez v2, :cond_9

    #@5
    iget-wide v2, p0, Landroid/content/SyncManager$SyncTimeTracker;->mTimeSpentSyncing:J
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_15

    #@7
    .line 1599
    :goto_7
    monitor-exit p0

    #@8
    return-wide v2

    #@9
    .line 1598
    :cond_9
    :try_start_9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@c
    move-result-wide v0

    #@d
    .line 1599
    .local v0, now:J
    iget-wide v2, p0, Landroid/content/SyncManager$SyncTimeTracker;->mTimeSpentSyncing:J

    #@f
    iget-wide v4, p0, Landroid/content/SyncManager$SyncTimeTracker;->mWhenSyncStarted:J
    :try_end_11
    .catchall {:try_start_9 .. :try_end_11} :catchall_15

    #@11
    sub-long v4, v0, v4

    #@13
    add-long/2addr v2, v4

    #@14
    goto :goto_7

    #@15
    .line 1596
    .end local v0           #now:J
    :catchall_15
    move-exception v2

    #@16
    monitor-exit p0

    #@17
    throw v2
.end method

.method public declared-synchronized update()V
    .registers 8

    #@0
    .prologue
    .line 1583
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v3, p0, Landroid/content/SyncManager$SyncTimeTracker;->this$0:Landroid/content/SyncManager;

    #@3
    iget-object v3, v3, Landroid/content/SyncManager;->mActiveSyncContexts:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@8
    move-result v3

    #@9
    if-nez v3, :cond_12

    #@b
    const/4 v0, 0x1

    #@c
    .line 1584
    .local v0, isSyncInProgress:Z
    :goto_c
    iget-boolean v3, p0, Landroid/content/SyncManager$SyncTimeTracker;->mLastWasSyncing:Z
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_1f

    #@e
    if-ne v0, v3, :cond_14

    #@10
    .line 1592
    :goto_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 1583
    .end local v0           #isSyncInProgress:Z
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_c

    #@14
    .line 1585
    .restart local v0       #isSyncInProgress:Z
    :cond_14
    :try_start_14
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@17
    move-result-wide v1

    #@18
    .line 1586
    .local v1, now:J
    if-eqz v0, :cond_22

    #@1a
    .line 1587
    iput-wide v1, p0, Landroid/content/SyncManager$SyncTimeTracker;->mWhenSyncStarted:J

    #@1c
    .line 1591
    :goto_1c
    iput-boolean v0, p0, Landroid/content/SyncManager$SyncTimeTracker;->mLastWasSyncing:Z
    :try_end_1e
    .catchall {:try_start_14 .. :try_end_1e} :catchall_1f

    #@1e
    goto :goto_10

    #@1f
    .line 1583
    .end local v0           #isSyncInProgress:Z
    .end local v1           #now:J
    :catchall_1f
    move-exception v3

    #@20
    monitor-exit p0

    #@21
    throw v3

    #@22
    .line 1589
    .restart local v0       #isSyncInProgress:Z
    .restart local v1       #now:J
    :cond_22
    :try_start_22
    iget-wide v3, p0, Landroid/content/SyncManager$SyncTimeTracker;->mTimeSpentSyncing:J

    #@24
    iget-wide v5, p0, Landroid/content/SyncManager$SyncTimeTracker;->mWhenSyncStarted:J

    #@26
    sub-long v5, v1, v5

    #@28
    add-long/2addr v3, v5

    #@29
    iput-wide v3, p0, Landroid/content/SyncManager$SyncTimeTracker;->mTimeSpentSyncing:J
    :try_end_2b
    .catchall {:try_start_22 .. :try_end_2b} :catchall_1f

    #@2b
    goto :goto_1c
.end method
