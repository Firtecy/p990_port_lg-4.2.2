.class public final Landroid/content/Intent$FilterComparison;
.super Ljava/lang/Object;
.source "Intent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/Intent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FilterComparison"
.end annotation


# instance fields
.field private final mHashCode:I

.field private final mIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 6299
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 6300
    iput-object p1, p0, Landroid/content/Intent$FilterComparison;->mIntent:Landroid/content/Intent;

    #@5
    .line 6301
    invoke-virtual {p1}, Landroid/content/Intent;->filterHashCode()I

    #@8
    move-result v0

    #@9
    iput v0, p0, Landroid/content/Intent$FilterComparison;->mHashCode:I

    #@b
    .line 6302
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "obj"

    #@0
    .prologue
    .line 6315
    instance-of v1, p1, Landroid/content/Intent$FilterComparison;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 6316
    check-cast p1, Landroid/content/Intent$FilterComparison;

    #@6
    .end local p1
    iget-object v0, p1, Landroid/content/Intent$FilterComparison;->mIntent:Landroid/content/Intent;

    #@8
    .line 6317
    .local v0, other:Landroid/content/Intent;
    iget-object v1, p0, Landroid/content/Intent$FilterComparison;->mIntent:Landroid/content/Intent;

    #@a
    invoke-virtual {v1, v0}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    #@d
    move-result v1

    #@e
    .line 6319
    .end local v0           #other:Landroid/content/Intent;
    :goto_e
    return v1

    #@f
    .restart local p1
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public getIntent()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 6310
    iget-object v0, p0, Landroid/content/Intent$FilterComparison;->mIntent:Landroid/content/Intent;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 6324
    iget v0, p0, Landroid/content/Intent$FilterComparison;->mHashCode:I

    #@2
    return v0
.end method
