.class public Landroid/content/ClipData$Item;
.super Ljava/lang/Object;
.source "ClipData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/ClipData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Item"
.end annotation


# instance fields
.field final mHtmlText:Ljava/lang/String;

.field final mIntent:Landroid/content/Intent;

.field final mText:Ljava/lang/CharSequence;

.field final mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 216
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 217
    iput-object v0, p0, Landroid/content/ClipData$Item;->mText:Ljava/lang/CharSequence;

    #@6
    .line 218
    iput-object v0, p0, Landroid/content/ClipData$Item;->mHtmlText:Ljava/lang/String;

    #@8
    .line 219
    iput-object p1, p0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@a
    .line 220
    iput-object v0, p0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@c
    .line 221
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .registers 3
    .parameter "uri"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 226
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 227
    iput-object v0, p0, Landroid/content/ClipData$Item;->mText:Ljava/lang/CharSequence;

    #@6
    .line 228
    iput-object v0, p0, Landroid/content/ClipData$Item;->mHtmlText:Ljava/lang/String;

    #@8
    .line 229
    iput-object v0, p0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@a
    .line 230
    iput-object p1, p0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@c
    .line 231
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "text"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 193
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 194
    iput-object p1, p0, Landroid/content/ClipData$Item;->mText:Ljava/lang/CharSequence;

    #@6
    .line 195
    iput-object v0, p0, Landroid/content/ClipData$Item;->mHtmlText:Ljava/lang/String;

    #@8
    .line 196
    iput-object v0, p0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@a
    .line 197
    iput-object v0, p0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@c
    .line 198
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/content/Intent;Landroid/net/Uri;)V
    .registers 5
    .parameter "text"
    .parameter "intent"
    .parameter "uri"

    #@0
    .prologue
    .line 237
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 238
    iput-object p1, p0, Landroid/content/ClipData$Item;->mText:Ljava/lang/CharSequence;

    #@5
    .line 239
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/content/ClipData$Item;->mHtmlText:Ljava/lang/String;

    #@8
    .line 240
    iput-object p2, p0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@a
    .line 241
    iput-object p3, p0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@c
    .line 242
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .registers 4
    .parameter "text"
    .parameter "htmlText"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 206
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 207
    iput-object p1, p0, Landroid/content/ClipData$Item;->mText:Ljava/lang/CharSequence;

    #@6
    .line 208
    iput-object p2, p0, Landroid/content/ClipData$Item;->mHtmlText:Ljava/lang/String;

    #@8
    .line 209
    iput-object v0, p0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@a
    .line 210
    iput-object v0, p0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@c
    .line 211
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/net/Uri;)V
    .registers 7
    .parameter "text"
    .parameter "htmlText"
    .parameter "intent"
    .parameter "uri"

    #@0
    .prologue
    .line 250
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 251
    if-eqz p2, :cond_f

    #@5
    if-nez p1, :cond_f

    #@7
    .line 252
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v1, "Plain text must be supplied if HTML text is supplied"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 255
    :cond_f
    iput-object p1, p0, Landroid/content/ClipData$Item;->mText:Ljava/lang/CharSequence;

    #@11
    .line 256
    iput-object p2, p0, Landroid/content/ClipData$Item;->mHtmlText:Ljava/lang/String;

    #@13
    .line 257
    iput-object p3, p0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@15
    .line 258
    iput-object p4, p0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@17
    .line 259
    return-void
.end method

.method private coerceToHtmlOrStyledText(Landroid/content/Context;Z)Ljava/lang/CharSequence;
    .registers 27
    .parameter "context"
    .parameter "styled"

    #@0
    .prologue
    .line 457
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@4
    move-object/from16 v20, v0

    #@6
    if-eqz v20, :cond_12d

    #@8
    .line 462
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v20

    #@c
    move-object/from16 v0, p0

    #@e
    iget-object v0, v0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@10
    move-object/from16 v21, v0

    #@12
    const-string/jumbo v22, "text/*"

    #@15
    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentResolver;->getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;

    #@18
    move-result-object v19

    #@19
    .line 463
    .local v19, types:[Ljava/lang/String;
    const/4 v9, 0x0

    #@1a
    .line 464
    .local v9, hasHtml:Z
    const/4 v10, 0x0

    #@1b
    .line 465
    .local v10, hasText:Z
    if-eqz v19, :cond_45

    #@1d
    .line 466
    move-object/from16 v4, v19

    #@1f
    .local v4, arr$:[Ljava/lang/String;
    array-length v13, v4

    #@20
    .local v13, len$:I
    const/4 v11, 0x0

    #@21
    .local v11, i$:I
    :goto_21
    if-ge v11, v13, :cond_45

    #@23
    aget-object v18, v4, v11

    #@25
    .line 467
    .local v18, type:Ljava/lang/String;
    const-string/jumbo v20, "text/html"

    #@28
    move-object/from16 v0, v20

    #@2a
    move-object/from16 v1, v18

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v20

    #@30
    if-eqz v20, :cond_36

    #@32
    .line 468
    const/4 v9, 0x1

    #@33
    .line 466
    :cond_33
    :goto_33
    add-int/lit8 v11, v11, 0x1

    #@35
    goto :goto_21

    #@36
    .line 469
    :cond_36
    const-string/jumbo v20, "text/"

    #@39
    move-object/from16 v0, v18

    #@3b
    move-object/from16 v1, v20

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@40
    move-result v20

    #@41
    if-eqz v20, :cond_33

    #@43
    .line 470
    const/4 v10, 0x1

    #@44
    goto :goto_33

    #@45
    .line 476
    .end local v4           #arr$:[Ljava/lang/String;
    .end local v11           #i$:I
    .end local v13           #len$:I
    .end local v18           #type:Ljava/lang/String;
    :cond_45
    if-nez v9, :cond_49

    #@47
    if-eqz v10, :cond_9a

    #@49
    .line 477
    :cond_49
    const/16 v16, 0x0

    #@4b
    .line 480
    .local v16, stream:Ljava/io/FileInputStream;
    :try_start_4b
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4e
    move-result-object v21

    #@4f
    move-object/from16 v0, p0

    #@51
    iget-object v0, v0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@53
    move-object/from16 v22, v0

    #@55
    if-eqz v9, :cond_af

    #@57
    const-string/jumbo v20, "text/html"

    #@5a
    :goto_5a
    const/16 v23, 0x0

    #@5c
    move-object/from16 v0, v21

    #@5e
    move-object/from16 v1, v22

    #@60
    move-object/from16 v2, v20

    #@62
    move-object/from16 v3, v23

    #@64
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->openTypedAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;

    #@67
    move-result-object v7

    #@68
    .line 483
    .local v7, descr:Landroid/content/res/AssetFileDescriptor;
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    #@6b
    move-result-object v16

    #@6c
    .line 484
    new-instance v15, Ljava/io/InputStreamReader;

    #@6e
    const-string v20, "UTF-8"

    #@70
    move-object/from16 v0, v16

    #@72
    move-object/from16 v1, v20

    #@74
    invoke-direct {v15, v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    #@77
    .line 487
    .local v15, reader:Ljava/io/InputStreamReader;
    new-instance v6, Ljava/lang/StringBuilder;

    #@79
    const/16 v20, 0x80

    #@7b
    move/from16 v0, v20

    #@7d
    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@80
    .line 488
    .local v6, builder:Ljava/lang/StringBuilder;
    const/16 v20, 0x2000

    #@82
    move/from16 v0, v20

    #@84
    new-array v5, v0, [C

    #@86
    .line 490
    .local v5, buffer:[C
    :goto_86
    invoke-virtual {v15, v5}, Ljava/io/InputStreamReader;->read([C)I

    #@89
    move-result v12

    #@8a
    .local v12, len:I
    if-lez v12, :cond_b3

    #@8c
    .line 491
    const/16 v20, 0x0

    #@8e
    move/from16 v0, v20

    #@90
    invoke-virtual {v6, v5, v0, v12}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_93
    .catchall {:try_start_4b .. :try_end_93} :catchall_113
    .catch Ljava/io/FileNotFoundException; {:try_start_4b .. :try_end_93} :catch_94
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_93} :catch_f7

    #@93
    goto :goto_86

    #@94
    .line 520
    .end local v5           #buffer:[C
    .end local v6           #builder:Ljava/lang/StringBuilder;
    .end local v7           #descr:Landroid/content/res/AssetFileDescriptor;
    .end local v12           #len:I
    .end local v15           #reader:Ljava/io/InputStreamReader;
    :catch_94
    move-exception v20

    #@95
    .line 530
    if-eqz v16, :cond_9a

    #@97
    .line 532
    :try_start_97
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_9a
    .catch Ljava/io/IOException; {:try_start_97 .. :try_end_9a} :catch_16a

    #@9a
    .line 542
    .end local v16           #stream:Ljava/io/FileInputStream;
    :cond_9a
    :goto_9a
    if-eqz p2, :cond_11a

    #@9c
    .line 543
    move-object/from16 v0, p0

    #@9e
    iget-object v0, v0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@a0
    move-object/from16 v20, v0

    #@a2
    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@a5
    move-result-object v20

    #@a6
    move-object/from16 v0, p0

    #@a8
    move-object/from16 v1, v20

    #@aa
    invoke-direct {v0, v1}, Landroid/content/ClipData$Item;->uriToStyledText(Ljava/lang/String;)Ljava/lang/CharSequence;

    #@ad
    move-result-object v17

    #@ae
    .line 560
    .end local v9           #hasHtml:Z
    .end local v10           #hasText:Z
    .end local v19           #types:[Ljava/lang/String;
    :cond_ae
    :goto_ae
    return-object v17

    #@af
    .line 480
    .restart local v9       #hasHtml:Z
    .restart local v10       #hasText:Z
    .restart local v16       #stream:Ljava/io/FileInputStream;
    .restart local v19       #types:[Ljava/lang/String;
    :cond_af
    :try_start_af
    const-string/jumbo v20, "text/plain"

    #@b2
    goto :goto_5a

    #@b3
    .line 493
    .restart local v5       #buffer:[C
    .restart local v6       #builder:Ljava/lang/StringBuilder;
    .restart local v7       #descr:Landroid/content/res/AssetFileDescriptor;
    .restart local v12       #len:I
    .restart local v15       #reader:Ljava/io/InputStreamReader;
    :cond_b3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_b6
    .catchall {:try_start_af .. :try_end_b6} :catchall_113
    .catch Ljava/io/FileNotFoundException; {:try_start_af .. :try_end_b6} :catch_94
    .catch Ljava/io/IOException; {:try_start_af .. :try_end_b6} :catch_f7

    #@b6
    move-result-object v17

    #@b7
    .line 494
    .local v17, text:Ljava/lang/String;
    if-eqz v9, :cond_e1

    #@b9
    .line 495
    if-eqz p2, :cond_d5

    #@bb
    .line 499
    :try_start_bb
    invoke-static/range {v17 .. v17}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;
    :try_end_be
    .catchall {:try_start_bb .. :try_end_be} :catchall_113
    .catch Ljava/lang/RuntimeException; {:try_start_bb .. :try_end_be} :catch_cc
    .catch Ljava/io/FileNotFoundException; {:try_start_bb .. :try_end_be} :catch_94
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_be} :catch_f7

    #@be
    move-result-object v14

    #@bf
    .line 500
    .local v14, newText:Ljava/lang/CharSequence;
    if-eqz v14, :cond_c9

    #@c1
    .line 530
    .end local v14           #newText:Ljava/lang/CharSequence;
    :goto_c1
    if-eqz v16, :cond_c6

    #@c3
    .line 532
    :try_start_c3
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_c6
    .catch Ljava/io/IOException; {:try_start_c3 .. :try_end_c6} :catch_167

    #@c6
    :cond_c6
    :goto_c6
    move-object/from16 v17, v14

    #@c8
    .line 534
    goto :goto_ae

    #@c9
    .restart local v14       #newText:Ljava/lang/CharSequence;
    :cond_c9
    move-object/from16 v14, v17

    #@cb
    .line 500
    goto :goto_c1

    #@cc
    .line 501
    .end local v14           #newText:Ljava/lang/CharSequence;
    :catch_cc
    move-exception v8

    #@cd
    .line 530
    .local v8, e:Ljava/lang/RuntimeException;
    if-eqz v16, :cond_ae

    #@cf
    .line 532
    :try_start_cf
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_d2
    .catch Ljava/io/IOException; {:try_start_cf .. :try_end_d2} :catch_d3

    #@d2
    goto :goto_ae

    #@d3
    .line 533
    :catch_d3
    move-exception v20

    #@d4
    goto :goto_ae

    #@d5
    .line 507
    .end local v8           #e:Ljava/lang/RuntimeException;
    :cond_d5
    :try_start_d5
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_d8
    .catchall {:try_start_d5 .. :try_end_d8} :catchall_113
    .catch Ljava/io/FileNotFoundException; {:try_start_d5 .. :try_end_d8} :catch_94
    .catch Ljava/io/IOException; {:try_start_d5 .. :try_end_d8} :catch_f7

    #@d8
    move-result-object v17

    #@d9
    .line 530
    .end local v17           #text:Ljava/lang/String;
    if-eqz v16, :cond_ae

    #@db
    .line 532
    :try_start_db
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_de
    .catch Ljava/io/IOException; {:try_start_db .. :try_end_de} :catch_df

    #@de
    goto :goto_ae

    #@df
    .line 533
    :catch_df
    move-exception v20

    #@e0
    goto :goto_ae

    #@e1
    .line 510
    .restart local v17       #text:Ljava/lang/String;
    :cond_e1
    if-eqz p2, :cond_eb

    #@e3
    .line 530
    if-eqz v16, :cond_ae

    #@e5
    .line 532
    :try_start_e5
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_e8
    .catch Ljava/io/IOException; {:try_start_e5 .. :try_end_e8} :catch_e9

    #@e8
    goto :goto_ae

    #@e9
    .line 533
    :catch_e9
    move-exception v20

    #@ea
    goto :goto_ae

    #@eb
    .line 517
    :cond_eb
    :try_start_eb
    invoke-static/range {v17 .. v17}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_ee
    .catchall {:try_start_eb .. :try_end_ee} :catchall_113
    .catch Ljava/io/FileNotFoundException; {:try_start_eb .. :try_end_ee} :catch_94
    .catch Ljava/io/IOException; {:try_start_eb .. :try_end_ee} :catch_f7

    #@ee
    move-result-object v17

    #@ef
    .line 530
    .end local v17           #text:Ljava/lang/String;
    if-eqz v16, :cond_ae

    #@f1
    .line 532
    :try_start_f1
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_f4
    .catch Ljava/io/IOException; {:try_start_f1 .. :try_end_f4} :catch_f5

    #@f4
    goto :goto_ae

    #@f5
    .line 533
    :catch_f5
    move-exception v20

    #@f6
    goto :goto_ae

    #@f7
    .line 524
    .end local v5           #buffer:[C
    .end local v6           #builder:Ljava/lang/StringBuilder;
    .end local v7           #descr:Landroid/content/res/AssetFileDescriptor;
    .end local v12           #len:I
    .end local v15           #reader:Ljava/io/InputStreamReader;
    :catch_f7
    move-exception v8

    #@f8
    .line 526
    .local v8, e:Ljava/io/IOException;
    :try_start_f8
    const-string v20, "ClippedData"

    #@fa
    const-string v21, "Failure loading text"

    #@fc
    move-object/from16 v0, v20

    #@fe
    move-object/from16 v1, v21

    #@100
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@103
    .line 527
    invoke-virtual {v8}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@106
    move-result-object v20

    #@107
    invoke-static/range {v20 .. v20}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_10a
    .catchall {:try_start_f8 .. :try_end_10a} :catchall_113

    #@10a
    move-result-object v17

    #@10b
    .line 530
    if-eqz v16, :cond_ae

    #@10d
    .line 532
    :try_start_10d
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_110
    .catch Ljava/io/IOException; {:try_start_10d .. :try_end_110} :catch_111

    #@110
    goto :goto_ae

    #@111
    .line 533
    :catch_111
    move-exception v20

    #@112
    goto :goto_ae

    #@113
    .line 530
    .end local v8           #e:Ljava/io/IOException;
    :catchall_113
    move-exception v20

    #@114
    if-eqz v16, :cond_119

    #@116
    .line 532
    :try_start_116
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_119
    .catch Ljava/io/IOException; {:try_start_116 .. :try_end_119} :catch_16d

    #@119
    .line 534
    :cond_119
    :goto_119
    throw v20

    #@11a
    .line 545
    .end local v16           #stream:Ljava/io/FileInputStream;
    :cond_11a
    move-object/from16 v0, p0

    #@11c
    iget-object v0, v0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@11e
    move-object/from16 v20, v0

    #@120
    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@123
    move-result-object v20

    #@124
    move-object/from16 v0, p0

    #@126
    move-object/from16 v1, v20

    #@128
    invoke-direct {v0, v1}, Landroid/content/ClipData$Item;->uriToHtml(Ljava/lang/String;)Ljava/lang/String;

    #@12b
    move-result-object v17

    #@12c
    goto :goto_ae

    #@12d
    .line 551
    .end local v9           #hasHtml:Z
    .end local v10           #hasText:Z
    .end local v19           #types:[Ljava/lang/String;
    :cond_12d
    move-object/from16 v0, p0

    #@12f
    iget-object v0, v0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@131
    move-object/from16 v20, v0

    #@133
    if-eqz v20, :cond_163

    #@135
    .line 552
    if-eqz p2, :cond_14d

    #@137
    .line 553
    move-object/from16 v0, p0

    #@139
    iget-object v0, v0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@13b
    move-object/from16 v20, v0

    #@13d
    const/16 v21, 0x1

    #@13f
    invoke-virtual/range {v20 .. v21}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    #@142
    move-result-object v20

    #@143
    move-object/from16 v0, p0

    #@145
    move-object/from16 v1, v20

    #@147
    invoke-direct {v0, v1}, Landroid/content/ClipData$Item;->uriToStyledText(Ljava/lang/String;)Ljava/lang/CharSequence;

    #@14a
    move-result-object v17

    #@14b
    goto/16 :goto_ae

    #@14d
    .line 555
    :cond_14d
    move-object/from16 v0, p0

    #@14f
    iget-object v0, v0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@151
    move-object/from16 v20, v0

    #@153
    const/16 v21, 0x1

    #@155
    invoke-virtual/range {v20 .. v21}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    #@158
    move-result-object v20

    #@159
    move-object/from16 v0, p0

    #@15b
    move-object/from16 v1, v20

    #@15d
    invoke-direct {v0, v1}, Landroid/content/ClipData$Item;->uriToHtml(Ljava/lang/String;)Ljava/lang/String;

    #@160
    move-result-object v17

    #@161
    goto/16 :goto_ae

    #@163
    .line 560
    :cond_163
    const-string v17, ""

    #@165
    goto/16 :goto_ae

    #@167
    .line 533
    .restart local v5       #buffer:[C
    .restart local v6       #builder:Ljava/lang/StringBuilder;
    .restart local v7       #descr:Landroid/content/res/AssetFileDescriptor;
    .restart local v9       #hasHtml:Z
    .restart local v10       #hasText:Z
    .restart local v12       #len:I
    .restart local v15       #reader:Ljava/io/InputStreamReader;
    .restart local v16       #stream:Ljava/io/FileInputStream;
    .restart local v17       #text:Ljava/lang/String;
    .restart local v19       #types:[Ljava/lang/String;
    :catch_167
    move-exception v20

    #@168
    goto/16 :goto_c6

    #@16a
    .end local v5           #buffer:[C
    .end local v6           #builder:Ljava/lang/StringBuilder;
    .end local v7           #descr:Landroid/content/res/AssetFileDescriptor;
    .end local v12           #len:I
    .end local v15           #reader:Ljava/io/InputStreamReader;
    .end local v17           #text:Ljava/lang/String;
    :catch_16a
    move-exception v20

    #@16b
    goto/16 :goto_9a

    #@16d
    :catch_16d
    move-exception v21

    #@16e
    goto :goto_119
.end method

.method private uriToHtml(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 564
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x100

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 565
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "<a href=\""

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 566
    invoke-static {p1}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 567
    const-string v1, "\">"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 568
    invoke-static {p1}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    .line 569
    const-string v1, "</a>"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 570
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    return-object v1
.end method

.method private uriToStyledText(Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 7
    .parameter "uri"

    #@0
    .prologue
    .line 574
    new-instance v0, Landroid/text/SpannableStringBuilder;

    #@2
    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    #@5
    .line 575
    .local v0, builder:Landroid/text/SpannableStringBuilder;
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@8
    .line 576
    new-instance v1, Landroid/text/style/URLSpan;

    #@a
    invoke-direct {v1, p1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    #@d
    const/4 v2, 0x0

    #@e
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    #@11
    move-result v3

    #@12
    const/16 v4, 0x21

    #@14
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@17
    .line 578
    return-object v0
.end method


# virtual methods
.method public coerceToHtmlText(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 437
    invoke-virtual {p0}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 438
    .local v0, htmlText:Ljava/lang/String;
    if-eqz v0, :cond_7

    #@6
    .line 452
    .end local v0           #htmlText:Ljava/lang/String;
    :goto_6
    return-object v0

    #@7
    .line 443
    .restart local v0       #htmlText:Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@a
    move-result-object v1

    #@b
    .line 444
    .local v1, text:Ljava/lang/CharSequence;
    if-eqz v1, :cond_1d

    #@d
    .line 445
    instance-of v2, v1, Landroid/text/Spanned;

    #@f
    if-eqz v2, :cond_18

    #@11
    .line 446
    check-cast v1, Landroid/text/Spanned;

    #@13
    .end local v1           #text:Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    goto :goto_6

    #@18
    .line 448
    .restart local v1       #text:Ljava/lang/CharSequence;
    :cond_18
    invoke-static {v1}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    goto :goto_6

    #@1d
    .line 451
    :cond_1d
    const/4 v2, 0x0

    #@1e
    invoke-direct {p0, p1, v2}, Landroid/content/ClipData$Item;->coerceToHtmlOrStyledText(Landroid/content/Context;Z)Ljava/lang/CharSequence;

    #@21
    move-result-object v1

    #@22
    .line 452
    if-eqz v1, :cond_2a

    #@24
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    :goto_28
    move-object v0, v2

    #@29
    goto :goto_6

    #@2a
    :cond_2a
    const/4 v2, 0x0

    #@2b
    goto :goto_28
.end method

.method public coerceToStyledText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 386
    invoke-virtual {p0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@3
    move-result-object v2

    #@4
    .line 387
    .local v2, text:Ljava/lang/CharSequence;
    instance-of v3, v2, Landroid/text/Spanned;

    #@6
    if-eqz v3, :cond_9

    #@8
    .line 405
    .end local v2           #text:Ljava/lang/CharSequence;
    :cond_8
    :goto_8
    return-object v2

    #@9
    .line 390
    .restart local v2       #text:Ljava/lang/CharSequence;
    :cond_9
    invoke-virtual {p0}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 391
    .local v0, htmlText:Ljava/lang/String;
    if-eqz v0, :cond_18

    #@f
    .line 393
    :try_start_f
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;
    :try_end_12
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_12} :catch_17

    #@12
    move-result-object v1

    #@13
    .line 394
    .local v1, newText:Ljava/lang/CharSequence;
    if-eqz v1, :cond_18

    #@15
    move-object v2, v1

    #@16
    .line 395
    goto :goto_8

    #@17
    .line 397
    .end local v1           #newText:Ljava/lang/CharSequence;
    :catch_17
    move-exception v3

    #@18
    .line 402
    :cond_18
    if-nez v2, :cond_8

    #@1a
    .line 405
    const/4 v3, 0x1

    #@1b
    invoke-direct {p0, p1, v3}, Landroid/content/ClipData$Item;->coerceToHtmlOrStyledText(Landroid/content/Context;Z)Ljava/lang/CharSequence;

    #@1e
    move-result-object v2

    #@1f
    goto :goto_8
.end method

.method public coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .registers 15
    .parameter "context"

    #@0
    .prologue
    .line 313
    invoke-virtual {p0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@3
    move-result-object v8

    #@4
    .line 314
    .local v8, text:Ljava/lang/CharSequence;
    if-eqz v8, :cond_7

    #@6
    .line 373
    .end local v8           #text:Ljava/lang/CharSequence;
    :cond_6
    :goto_6
    return-object v8

    #@7
    .line 319
    .restart local v8       #text:Ljava/lang/CharSequence;
    :cond_7
    invoke-virtual {p0}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@a
    move-result-object v9

    #@b
    .line 320
    .local v9, uri:Landroid/net/Uri;
    if-eqz v9, :cond_6d

    #@d
    .line 325
    const/4 v7, 0x0

    #@e
    .line 328
    .local v7, stream:Ljava/io/FileInputStream;
    :try_start_e
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11
    move-result-object v10

    #@12
    const-string/jumbo v11, "text/*"

    #@15
    const/4 v12, 0x0

    #@16
    invoke-virtual {v10, v9, v11, v12}, Landroid/content/ContentResolver;->openTypedAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;

    #@19
    move-result-object v2

    #@1a
    .line 330
    .local v2, descr:Landroid/content/res/AssetFileDescriptor;
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    #@1d
    move-result-object v7

    #@1e
    .line 331
    new-instance v6, Ljava/io/InputStreamReader;

    #@20
    const-string v10, "UTF-8"

    #@22
    invoke-direct {v6, v7, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    #@25
    .line 334
    .local v6, reader:Ljava/io/InputStreamReader;
    new-instance v1, Ljava/lang/StringBuilder;

    #@27
    const/16 v10, 0x80

    #@29
    invoke-direct {v1, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    #@2c
    .line 335
    .local v1, builder:Ljava/lang/StringBuilder;
    const/16 v10, 0x2000

    #@2e
    new-array v0, v10, [C

    #@30
    .line 337
    .local v0, buffer:[C
    :goto_30
    invoke-virtual {v6, v0}, Ljava/io/InputStreamReader;->read([C)I

    #@33
    move-result v5

    #@34
    .local v5, len:I
    if-lez v5, :cond_46

    #@36
    .line 338
    const/4 v10, 0x0

    #@37
    invoke-virtual {v1, v0, v10, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_3a
    .catchall {:try_start_e .. :try_end_3a} :catchall_66
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_3a} :catch_3b
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_3a} :catch_52

    #@3a
    goto :goto_30

    #@3b
    .line 342
    .end local v0           #buffer:[C
    .end local v1           #builder:Ljava/lang/StringBuilder;
    .end local v2           #descr:Landroid/content/res/AssetFileDescriptor;
    .end local v5           #len:I
    .end local v6           #reader:Ljava/io/InputStreamReader;
    :catch_3b
    move-exception v10

    #@3c
    .line 352
    if-eqz v7, :cond_41

    #@3e
    .line 354
    :try_start_3e
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_41} :catch_7c

    #@41
    .line 362
    :cond_41
    :goto_41
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@44
    move-result-object v8

    #@45
    goto :goto_6

    #@46
    .line 340
    .restart local v0       #buffer:[C
    .restart local v1       #builder:Ljava/lang/StringBuilder;
    .restart local v2       #descr:Landroid/content/res/AssetFileDescriptor;
    .restart local v5       #len:I
    .restart local v6       #reader:Ljava/io/InputStreamReader;
    :cond_46
    :try_start_46
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_49
    .catchall {:try_start_46 .. :try_end_49} :catchall_66
    .catch Ljava/io/FileNotFoundException; {:try_start_46 .. :try_end_49} :catch_3b
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_49} :catch_52

    #@49
    move-result-object v8

    #@4a
    .line 352
    .end local v8           #text:Ljava/lang/CharSequence;
    if-eqz v7, :cond_6

    #@4c
    .line 354
    :try_start_4c
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_4f
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_4f} :catch_50

    #@4f
    goto :goto_6

    #@50
    .line 355
    :catch_50
    move-exception v10

    #@51
    goto :goto_6

    #@52
    .line 346
    .end local v0           #buffer:[C
    .end local v1           #builder:Ljava/lang/StringBuilder;
    .end local v2           #descr:Landroid/content/res/AssetFileDescriptor;
    .end local v5           #len:I
    .end local v6           #reader:Ljava/io/InputStreamReader;
    .restart local v8       #text:Ljava/lang/CharSequence;
    :catch_52
    move-exception v3

    #@53
    .line 348
    .local v3, e:Ljava/io/IOException;
    :try_start_53
    const-string v10, "ClippedData"

    #@55
    const-string v11, "Failure loading text"

    #@57
    invoke-static {v10, v11, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5a
    .line 349
    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;
    :try_end_5d
    .catchall {:try_start_53 .. :try_end_5d} :catchall_66

    #@5d
    move-result-object v8

    #@5e
    .line 352
    .end local v8           #text:Ljava/lang/CharSequence;
    if-eqz v7, :cond_6

    #@60
    .line 354
    :try_start_60
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_63
    .catch Ljava/io/IOException; {:try_start_60 .. :try_end_63} :catch_64

    #@63
    goto :goto_6

    #@64
    .line 355
    :catch_64
    move-exception v10

    #@65
    goto :goto_6

    #@66
    .line 352
    .end local v3           #e:Ljava/io/IOException;
    .restart local v8       #text:Ljava/lang/CharSequence;
    :catchall_66
    move-exception v10

    #@67
    if-eqz v7, :cond_6c

    #@69
    .line 354
    :try_start_69
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_6c
    .catch Ljava/io/IOException; {:try_start_69 .. :try_end_6c} :catch_7e

    #@6c
    .line 356
    :cond_6c
    :goto_6c
    throw v10

    #@6d
    .line 367
    .end local v7           #stream:Ljava/io/FileInputStream;
    :cond_6d
    invoke-virtual {p0}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    #@70
    move-result-object v4

    #@71
    .line 368
    .local v4, intent:Landroid/content/Intent;
    if-eqz v4, :cond_79

    #@73
    .line 369
    const/4 v10, 0x1

    #@74
    invoke-virtual {v4, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    #@77
    move-result-object v8

    #@78
    goto :goto_6

    #@79
    .line 373
    :cond_79
    const-string v8, ""

    #@7b
    goto :goto_6

    #@7c
    .line 355
    .end local v4           #intent:Landroid/content/Intent;
    .restart local v7       #stream:Ljava/io/FileInputStream;
    :catch_7c
    move-exception v10

    #@7d
    goto :goto_41

    #@7e
    :catch_7e
    move-exception v11

    #@7f
    goto :goto_6c
.end method

.method public getHtmlText()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 272
    iget-object v0, p0, Landroid/content/ClipData$Item;->mHtmlText:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 279
    iget-object v0, p0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@2
    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 265
    iget-object v0, p0, Landroid/content/ClipData$Item;->mText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .registers 2

    #@0
    .prologue
    .line 286
    iget-object v0, p0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method public toShortString(Ljava/lang/StringBuilder;)V
    .registers 8
    .parameter "b"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 594
    iget-object v0, p0, Landroid/content/ClipData$Item;->mHtmlText:Ljava/lang/String;

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 595
    const-string v0, "H:"

    #@7
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 596
    iget-object v0, p0, Landroid/content/ClipData$Item;->mHtmlText:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 609
    :goto_f
    return-void

    #@10
    .line 597
    :cond_10
    iget-object v0, p0, Landroid/content/ClipData$Item;->mText:Ljava/lang/CharSequence;

    #@12
    if-eqz v0, :cond_1f

    #@14
    .line 598
    const-string v0, "T:"

    #@16
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 599
    iget-object v0, p0, Landroid/content/ClipData$Item;->mText:Ljava/lang/CharSequence;

    #@1b
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@1e
    goto :goto_f

    #@1f
    .line 600
    :cond_1f
    iget-object v0, p0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@21
    if-eqz v0, :cond_2e

    #@23
    .line 601
    const-string v0, "U:"

    #@25
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    .line 602
    iget-object v0, p0, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@2a
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    goto :goto_f

    #@2e
    .line 603
    :cond_2e
    iget-object v0, p0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@30
    if-eqz v0, :cond_41

    #@32
    .line 604
    const-string v0, "I:"

    #@34
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    .line 605
    iget-object v0, p0, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@39
    move-object v1, p1

    #@3a
    move v3, v2

    #@3b
    move v4, v2

    #@3c
    move v5, v2

    #@3d
    invoke-virtual/range {v0 .. v5}, Landroid/content/Intent;->toShortString(Ljava/lang/StringBuilder;ZZZZ)V

    #@40
    goto :goto_f

    #@41
    .line 607
    :cond_41
    const-string v0, "NULL"

    #@43
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    goto :goto_f
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 583
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 585
    .local v0, b:Ljava/lang/StringBuilder;
    const-string v1, "ClipData.Item { "

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 586
    invoke-virtual {p0, v0}, Landroid/content/ClipData$Item;->toShortString(Ljava/lang/StringBuilder;)V

    #@f
    .line 587
    const-string v1, " }"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 589
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    return-object v1
.end method
