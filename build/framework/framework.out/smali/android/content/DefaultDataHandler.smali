.class public Landroid/content/DefaultDataHandler;
.super Ljava/lang/Object;
.source "DefaultDataHandler.java"

# interfaces
.implements Landroid/content/ContentInsertHandler;


# static fields
.field private static final ARG:Ljava/lang/String; = "arg"

.field private static final COL:Ljava/lang/String; = "col"

.field private static final DEL:Ljava/lang/String; = "del"

.field private static final POSTFIX:Ljava/lang/String; = "postfix"

.field private static final ROW:Ljava/lang/String; = "row"

.field private static final SELECT:Ljava/lang/String; = "select"

.field private static final URI_STR:Ljava/lang/String; = "uri"


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mUris:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mValues:Landroid/content/ContentValues;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 83
    new-instance v0, Ljava/util/Stack;

    #@5
    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    #@8
    iput-object v0, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@a
    return-void
.end method

.method private insertRow()Landroid/net/Uri;
    .registers 5

    #@0
    .prologue
    .line 128
    iget-object v2, p0, Landroid/content/DefaultDataHandler;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    iget-object v1, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@4
    invoke-virtual {v1}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Landroid/net/Uri;

    #@a
    iget-object v3, p0, Landroid/content/DefaultDataHandler;->mValues:Landroid/content/ContentValues;

    #@c
    invoke-virtual {v2, v1, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@f
    move-result-object v0

    #@10
    .line 129
    .local v0, u:Landroid/net/Uri;
    const/4 v1, 0x0

    #@11
    iput-object v1, p0, Landroid/content/DefaultDataHandler;->mValues:Landroid/content/ContentValues;

    #@13
    .line 130
    return-object v0
.end method

.method private parseRow(Lorg/xml/sax/Attributes;)V
    .registers 8
    .parameter "atts"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 100
    const-string/jumbo v3, "uri"

    #@3
    invoke-interface {p1, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    .line 102
    .local v2, uriStr:Ljava/lang/String;
    if-eqz v2, :cond_35

    #@9
    .line 104
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@c
    move-result-object v1

    #@d
    .line 105
    .local v1, uri:Landroid/net/Uri;
    if-nez v1, :cond_52

    #@f
    .line 106
    new-instance v3, Lorg/xml/sax/SAXException;

    #@11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "attribute "

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    const-string/jumbo v5, "uri"

    #@1f
    invoke-interface {p1, v5}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, " parsing failure"

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-direct {v3, v4}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    #@34
    throw v3

    #@35
    .line 110
    .end local v1           #uri:Landroid/net/Uri;
    :cond_35
    iget-object v3, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@37
    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    #@3a
    move-result v3

    #@3b
    if-lez v3, :cond_61

    #@3d
    .line 112
    const-string/jumbo v3, "postfix"

    #@40
    invoke-interface {p1, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@43
    move-result-object v0

    #@44
    .line 113
    .local v0, postfix:Ljava/lang/String;
    if-eqz v0, :cond_58

    #@46
    .line 114
    iget-object v3, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@48
    invoke-virtual {v3}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    #@4b
    move-result-object v3

    #@4c
    check-cast v3, Landroid/net/Uri;

    #@4e
    invoke-static {v3, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@51
    move-result-object v1

    #@52
    .line 123
    .end local v0           #postfix:Ljava/lang/String;
    .restart local v1       #uri:Landroid/net/Uri;
    :cond_52
    :goto_52
    iget-object v3, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@54
    invoke-virtual {v3, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    .line 125
    return-void

    #@58
    .line 117
    .end local v1           #uri:Landroid/net/Uri;
    .restart local v0       #postfix:Ljava/lang/String;
    :cond_58
    iget-object v3, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@5a
    invoke-virtual {v3}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    #@5d
    move-result-object v1

    #@5e
    check-cast v1, Landroid/net/Uri;

    #@60
    .restart local v1       #uri:Landroid/net/Uri;
    goto :goto_52

    #@61
    .line 120
    .end local v0           #postfix:Ljava/lang/String;
    .end local v1           #uri:Landroid/net/Uri;
    :cond_61
    new-instance v3, Lorg/xml/sax/SAXException;

    #@63
    const-string v4, "attribute parsing failure"

    #@65
    invoke-direct {v3, v4}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    #@68
    throw v3
.end method


# virtual methods
.method public characters([CII)V
    .registers 4
    .parameter "ch"
    .parameter "start"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 217
    return-void
.end method

.method public endDocument()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 222
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "uri"
    .parameter "localName"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 201
    const-string/jumbo v0, "row"

    #@3
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_26

    #@9
    .line 202
    iget-object v0, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@b
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1a

    #@11
    .line 203
    new-instance v0, Lorg/xml/sax/SAXException;

    #@13
    const-string/jumbo v1, "uri mismatch"

    #@16
    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 205
    :cond_1a
    iget-object v0, p0, Landroid/content/DefaultDataHandler;->mValues:Landroid/content/ContentValues;

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 206
    invoke-direct {p0}, Landroid/content/DefaultDataHandler;->insertRow()Landroid/net/Uri;

    #@21
    .line 208
    :cond_21
    iget-object v0, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@23
    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    #@26
    .line 210
    :cond_26
    return-void
.end method

.method public endPrefixMapping(Ljava/lang/String;)V
    .registers 2
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 227
    return-void
.end method

.method public ignorableWhitespace([CII)V
    .registers 4
    .parameter "ch"
    .parameter "start"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 233
    return-void
.end method

.method public insert(Landroid/content/ContentResolver;Ljava/io/InputStream;)V
    .registers 4
    .parameter "contentResolver"
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    iput-object p1, p0, Landroid/content/DefaultDataHandler;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    .line 90
    sget-object v0, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    #@4
    invoke-static {p2, v0, p0}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V

    #@7
    .line 91
    return-void
.end method

.method public insert(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .registers 3
    .parameter "contentResolver"
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 95
    iput-object p1, p0, Landroid/content/DefaultDataHandler;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    .line 96
    invoke-static {p2, p0}, Landroid/util/Xml;->parse(Ljava/lang/String;Lorg/xml/sax/ContentHandler;)V

    #@5
    .line 97
    return-void
.end method

.method public processingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "target"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 239
    return-void
.end method

.method public setDocumentLocator(Lorg/xml/sax/Locator;)V
    .registers 2
    .parameter "locator"

    #@0
    .prologue
    .line 244
    return-void
.end method

.method public skippedEntity(Ljava/lang/String;)V
    .registers 2
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 249
    return-void
.end method

.method public startDocument()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 254
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .registers 15
    .parameter "uri"
    .parameter "localName"
    .parameter "name"
    .parameter "atts"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 135
    const-string/jumbo v7, "row"

    #@5
    invoke-virtual {v7, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v7

    #@9
    if-eqz v7, :cond_75

    #@b
    .line 136
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mValues:Landroid/content/ContentValues;

    #@d
    if-eqz v7, :cond_5f

    #@f
    .line 138
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@11
    invoke-virtual {v7}, Ljava/util/Stack;->empty()Z

    #@14
    move-result v7

    #@15
    if-eqz v7, :cond_20

    #@17
    .line 139
    new-instance v7, Lorg/xml/sax/SAXException;

    #@19
    const-string/jumbo v8, "uri is empty"

    #@1c
    invoke-direct {v7, v8}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v7

    #@20
    .line 141
    :cond_20
    invoke-direct {p0}, Landroid/content/DefaultDataHandler;->insertRow()Landroid/net/Uri;

    #@23
    move-result-object v3

    #@24
    .line 142
    .local v3, nextUri:Landroid/net/Uri;
    if-nez v3, :cond_51

    #@26
    .line 143
    new-instance v8, Lorg/xml/sax/SAXException;

    #@28
    new-instance v7, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v9, "insert to uri "

    #@2f
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v9

    #@33
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@35
    invoke-virtual {v7}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    #@38
    move-result-object v7

    #@39
    check-cast v7, Landroid/net/Uri;

    #@3b
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@3e
    move-result-object v7

    #@3f
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    const-string v9, " failure"

    #@45
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v7

    #@49
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v7

    #@4d
    invoke-direct {v8, v7}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    #@50
    throw v8

    #@51
    .line 147
    :cond_51
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@53
    invoke-virtual {v7}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    #@56
    .line 148
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@58
    invoke-virtual {v7, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    .line 149
    invoke-direct {p0, p4}, Landroid/content/DefaultDataHandler;->parseRow(Lorg/xml/sax/Attributes;)V

    #@5e
    .line 197
    .end local v3           #nextUri:Landroid/net/Uri;
    :goto_5e
    return-void

    #@5f
    .line 152
    :cond_5f
    invoke-interface {p4}, Lorg/xml/sax/Attributes;->getLength()I

    #@62
    move-result v0

    #@63
    .line 153
    .local v0, attrLen:I
    if-nez v0, :cond_71

    #@65
    .line 155
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@67
    iget-object v8, p0, Landroid/content/DefaultDataHandler;->mUris:Ljava/util/Stack;

    #@69
    invoke-virtual {v8}, Ljava/util/Stack;->lastElement()Ljava/lang/Object;

    #@6c
    move-result-object v8

    #@6d
    invoke-virtual {v7, v8}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    #@70
    goto :goto_5e

    #@71
    .line 157
    :cond_71
    invoke-direct {p0, p4}, Landroid/content/DefaultDataHandler;->parseRow(Lorg/xml/sax/Attributes;)V

    #@74
    goto :goto_5e

    #@75
    .line 160
    .end local v0           #attrLen:I
    :cond_75
    const-string v7, "col"

    #@77
    invoke-virtual {v7, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7a
    move-result v7

    #@7b
    if-eqz v7, :cond_cf

    #@7d
    .line 161
    invoke-interface {p4}, Lorg/xml/sax/Attributes;->getLength()I

    #@80
    move-result v0

    #@81
    .line 162
    .restart local v0       #attrLen:I
    const/4 v7, 0x2

    #@82
    if-eq v0, v7, :cond_9d

    #@84
    .line 163
    new-instance v7, Lorg/xml/sax/SAXException;

    #@86
    new-instance v8, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v9, "illegal attributes number "

    #@8d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v8

    #@91
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v8

    #@95
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v8

    #@99
    invoke-direct {v7, v8}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    #@9c
    throw v7

    #@9d
    .line 165
    :cond_9d
    const/4 v7, 0x0

    #@9e
    invoke-interface {p4, v7}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    #@a1
    move-result-object v2

    #@a2
    .line 166
    .local v2, key:Ljava/lang/String;
    invoke-interface {p4, v8}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    #@a5
    move-result-object v6

    #@a6
    .line 167
    .local v6, value:Ljava/lang/String;
    if-eqz v2, :cond_c7

    #@a8
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@ab
    move-result v7

    #@ac
    if-lez v7, :cond_c7

    #@ae
    if-eqz v6, :cond_c7

    #@b0
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@b3
    move-result v7

    #@b4
    if-lez v7, :cond_c7

    #@b6
    .line 168
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mValues:Landroid/content/ContentValues;

    #@b8
    if-nez v7, :cond_c1

    #@ba
    .line 169
    new-instance v7, Landroid/content/ContentValues;

    #@bc
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    #@bf
    iput-object v7, p0, Landroid/content/DefaultDataHandler;->mValues:Landroid/content/ContentValues;

    #@c1
    .line 171
    :cond_c1
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mValues:Landroid/content/ContentValues;

    #@c3
    invoke-virtual {v7, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c6
    goto :goto_5e

    #@c7
    .line 173
    :cond_c7
    new-instance v7, Lorg/xml/sax/SAXException;

    #@c9
    const-string v8, "illegal attributes value"

    #@cb
    invoke-direct {v7, v8}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    #@ce
    throw v7

    #@cf
    .line 175
    .end local v0           #attrLen:I
    .end local v2           #key:Ljava/lang/String;
    .end local v6           #value:Ljava/lang/String;
    :cond_cf
    const-string v7, "del"

    #@d1
    invoke-virtual {v7, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d4
    move-result v7

    #@d5
    if-eqz v7, :cond_141

    #@d7
    .line 176
    const-string/jumbo v7, "uri"

    #@da
    invoke-interface {p4, v7}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@dd
    move-result-object v7

    #@de
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@e1
    move-result-object v5

    #@e2
    .line 177
    .local v5, u:Landroid/net/Uri;
    if-nez v5, :cond_10a

    #@e4
    .line 178
    new-instance v7, Lorg/xml/sax/SAXException;

    #@e6
    new-instance v8, Ljava/lang/StringBuilder;

    #@e8
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@eb
    const-string v9, "attribute "

    #@ed
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v8

    #@f1
    const-string/jumbo v9, "uri"

    #@f4
    invoke-interface {p4, v9}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    #@f7
    move-result-object v9

    #@f8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v8

    #@fc
    const-string v9, " parsing failure"

    #@fe
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v8

    #@102
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v8

    #@106
    invoke-direct {v7, v8}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    #@109
    throw v7

    #@10a
    .line 181
    :cond_10a
    invoke-interface {p4}, Lorg/xml/sax/Attributes;->getLength()I

    #@10d
    move-result v7

    #@10e
    add-int/lit8 v0, v7, -0x2

    #@110
    .line 182
    .restart local v0       #attrLen:I
    if-lez v0, :cond_12d

    #@112
    .line 183
    new-array v4, v0, [Ljava/lang/String;

    #@114
    .line 184
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v1, 0x0

    #@115
    .local v1, i:I
    :goto_115
    if-ge v1, v0, :cond_122

    #@117
    .line 185
    add-int/lit8 v7, v1, 0x2

    #@119
    invoke-interface {p4, v7}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    #@11c
    move-result-object v7

    #@11d
    aput-object v7, v4, v1

    #@11f
    .line 184
    add-int/lit8 v1, v1, 0x1

    #@121
    goto :goto_115

    #@122
    .line 187
    :cond_122
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mContentResolver:Landroid/content/ContentResolver;

    #@124
    invoke-interface {p4, v8}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    #@127
    move-result-object v8

    #@128
    invoke-virtual {v7, v5, v8, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@12b
    goto/16 :goto_5e

    #@12d
    .line 188
    .end local v1           #i:I
    .end local v4           #selectionArgs:[Ljava/lang/String;
    :cond_12d
    if-nez v0, :cond_13a

    #@12f
    .line 189
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mContentResolver:Landroid/content/ContentResolver;

    #@131
    invoke-interface {p4, v8}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    #@134
    move-result-object v8

    #@135
    invoke-virtual {v7, v5, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@138
    goto/16 :goto_5e

    #@13a
    .line 191
    :cond_13a
    iget-object v7, p0, Landroid/content/DefaultDataHandler;->mContentResolver:Landroid/content/ContentResolver;

    #@13c
    invoke-virtual {v7, v5, v9, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@13f
    goto/16 :goto_5e

    #@141
    .line 195
    .end local v0           #attrLen:I
    .end local v5           #u:Landroid/net/Uri;
    :cond_141
    new-instance v7, Lorg/xml/sax/SAXException;

    #@143
    new-instance v8, Ljava/lang/StringBuilder;

    #@145
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@148
    const-string/jumbo v9, "unknown element: "

    #@14b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v8

    #@14f
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v8

    #@153
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@156
    move-result-object v8

    #@157
    invoke-direct {v7, v8}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    #@15a
    throw v7
.end method

.method public startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "prefix"
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 260
    return-void
.end method
