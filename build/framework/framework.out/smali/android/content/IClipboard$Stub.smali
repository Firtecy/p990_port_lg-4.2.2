.class public abstract Landroid/content/IClipboard$Stub;
.super Landroid/os/Binder;
.source "IClipboard.java"

# interfaces
.implements Landroid/content/IClipboard;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/IClipboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/IClipboard$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.content.IClipboard"

.field static final TRANSACTION_addPrimaryClipChangedListener:I = 0x5

.field static final TRANSACTION_getInitialPrimaryClipAt:I = 0xd

.field static final TRANSACTION_getPrimaryClip:I = 0x2

.field static final TRANSACTION_getPrimaryClipAt:I = 0x7

.field static final TRANSACTION_getPrimaryClipCount:I = 0x9

.field static final TRANSACTION_getPrimaryClipDescription:I = 0x3

.field static final TRANSACTION_getPrimaryClipDescriptionAt:I = 0x8

.field static final TRANSACTION_hasClipboardText:I = 0xe

.field static final TRANSACTION_hasPrimaryClip:I = 0x4

.field static final TRANSACTION_isUidSecure:I = 0xc

.field static final TRANSACTION_removeAllPrimaryClips:I = 0xb

.field static final TRANSACTION_removePrimaryClipAt:I = 0xa

.field static final TRANSACTION_removePrimaryClipChangedListener:I = 0x6

.field static final TRANSACTION_setPrimaryClip:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.content.IClipboard"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/content/IClipboard$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/content/IClipboard;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.content.IClipboard"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/content/IClipboard;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/content/IClipboard;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/content/IClipboard$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/content/IClipboard$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_170

    #@5
    .line 219
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 47
    :sswitch_a
    const-string v3, "android.content.IClipboard"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v3, "android.content.IClipboard"

    #@12
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_2a

    #@1b
    .line 55
    sget-object v3, Landroid/content/ClipData;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/content/ClipData;

    #@23
    .line 60
    .local v0, _arg0:Landroid/content/ClipData;
    :goto_23
    invoke-virtual {p0, v0}, Landroid/content/IClipboard$Stub;->setPrimaryClip(Landroid/content/ClipData;)V

    #@26
    .line 61
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@29
    goto :goto_9

    #@2a
    .line 58
    .end local v0           #_arg0:Landroid/content/ClipData;
    :cond_2a
    const/4 v0, 0x0

    #@2b
    .restart local v0       #_arg0:Landroid/content/ClipData;
    goto :goto_23

    #@2c
    .line 66
    .end local v0           #_arg0:Landroid/content/ClipData;
    :sswitch_2c
    const-string v5, "android.content.IClipboard"

    #@2e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    .line 69
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/content/IClipboard$Stub;->getPrimaryClip(Ljava/lang/String;)Landroid/content/ClipData;

    #@38
    move-result-object v2

    #@39
    .line 70
    .local v2, _result:Landroid/content/ClipData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c
    .line 71
    if-eqz v2, :cond_45

    #@3e
    .line 72
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@41
    .line 73
    invoke-virtual {v2, p3, v4}, Landroid/content/ClipData;->writeToParcel(Landroid/os/Parcel;I)V

    #@44
    goto :goto_9

    #@45
    .line 76
    :cond_45
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@48
    goto :goto_9

    #@49
    .line 82
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:Landroid/content/ClipData;
    :sswitch_49
    const-string v5, "android.content.IClipboard"

    #@4b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e
    .line 83
    invoke-virtual {p0}, Landroid/content/IClipboard$Stub;->getPrimaryClipDescription()Landroid/content/ClipDescription;

    #@51
    move-result-object v2

    #@52
    .line 84
    .local v2, _result:Landroid/content/ClipDescription;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@55
    .line 85
    if-eqz v2, :cond_5e

    #@57
    .line 86
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5a
    .line 87
    invoke-virtual {v2, p3, v4}, Landroid/content/ClipDescription;->writeToParcel(Landroid/os/Parcel;I)V

    #@5d
    goto :goto_9

    #@5e
    .line 90
    :cond_5e
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@61
    goto :goto_9

    #@62
    .line 96
    .end local v2           #_result:Landroid/content/ClipDescription;
    :sswitch_62
    const-string v5, "android.content.IClipboard"

    #@64
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@67
    .line 97
    invoke-virtual {p0}, Landroid/content/IClipboard$Stub;->hasPrimaryClip()Z

    #@6a
    move-result v2

    #@6b
    .line 98
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e
    .line 99
    if-eqz v2, :cond_71

    #@70
    move v3, v4

    #@71
    :cond_71
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@74
    goto :goto_9

    #@75
    .line 104
    .end local v2           #_result:Z
    :sswitch_75
    const-string v3, "android.content.IClipboard"

    #@77
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7a
    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7d
    move-result-object v3

    #@7e
    invoke-static {v3}, Landroid/content/IOnPrimaryClipChangedListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IOnPrimaryClipChangedListener;

    #@81
    move-result-object v0

    #@82
    .line 107
    .local v0, _arg0:Landroid/content/IOnPrimaryClipChangedListener;
    invoke-virtual {p0, v0}, Landroid/content/IClipboard$Stub;->addPrimaryClipChangedListener(Landroid/content/IOnPrimaryClipChangedListener;)V

    #@85
    .line 108
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@88
    goto :goto_9

    #@89
    .line 113
    .end local v0           #_arg0:Landroid/content/IOnPrimaryClipChangedListener;
    :sswitch_89
    const-string v3, "android.content.IClipboard"

    #@8b
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8e
    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@91
    move-result-object v3

    #@92
    invoke-static {v3}, Landroid/content/IOnPrimaryClipChangedListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IOnPrimaryClipChangedListener;

    #@95
    move-result-object v0

    #@96
    .line 116
    .restart local v0       #_arg0:Landroid/content/IOnPrimaryClipChangedListener;
    invoke-virtual {p0, v0}, Landroid/content/IClipboard$Stub;->removePrimaryClipChangedListener(Landroid/content/IOnPrimaryClipChangedListener;)V

    #@99
    .line 117
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9c
    goto/16 :goto_9

    #@9e
    .line 122
    .end local v0           #_arg0:Landroid/content/IOnPrimaryClipChangedListener;
    :sswitch_9e
    const-string v5, "android.content.IClipboard"

    #@a0
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a3
    .line 124
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a6
    move-result-object v0

    #@a7
    .line 126
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@aa
    move-result v1

    #@ab
    .line 127
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/content/IClipboard$Stub;->getPrimaryClipAt(Ljava/lang/String;I)Landroid/content/ClipData;

    #@ae
    move-result-object v2

    #@af
    .line 128
    .local v2, _result:Landroid/content/ClipData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b2
    .line 129
    if-eqz v2, :cond_bc

    #@b4
    .line 130
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@b7
    .line 131
    invoke-virtual {v2, p3, v4}, Landroid/content/ClipData;->writeToParcel(Landroid/os/Parcel;I)V

    #@ba
    goto/16 :goto_9

    #@bc
    .line 134
    :cond_bc
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@bf
    goto/16 :goto_9

    #@c1
    .line 140
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v2           #_result:Landroid/content/ClipData;
    :sswitch_c1
    const-string v5, "android.content.IClipboard"

    #@c3
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c6
    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c9
    move-result v0

    #@ca
    .line 143
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/content/IClipboard$Stub;->getPrimaryClipDescriptionAt(I)Landroid/content/ClipDescription;

    #@cd
    move-result-object v2

    #@ce
    .line 144
    .local v2, _result:Landroid/content/ClipDescription;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d1
    .line 145
    if-eqz v2, :cond_db

    #@d3
    .line 146
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@d6
    .line 147
    invoke-virtual {v2, p3, v4}, Landroid/content/ClipDescription;->writeToParcel(Landroid/os/Parcel;I)V

    #@d9
    goto/16 :goto_9

    #@db
    .line 150
    :cond_db
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@de
    goto/16 :goto_9

    #@e0
    .line 156
    .end local v0           #_arg0:I
    .end local v2           #_result:Landroid/content/ClipDescription;
    :sswitch_e0
    const-string v3, "android.content.IClipboard"

    #@e2
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e5
    .line 157
    invoke-virtual {p0}, Landroid/content/IClipboard$Stub;->getPrimaryClipCount()I

    #@e8
    move-result v2

    #@e9
    .line 158
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ec
    .line 159
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@ef
    goto/16 :goto_9

    #@f1
    .line 164
    .end local v2           #_result:I
    :sswitch_f1
    const-string v5, "android.content.IClipboard"

    #@f3
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f6
    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f9
    move-result-object v0

    #@fa
    .line 168
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@fd
    move-result v1

    #@fe
    .line 169
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/content/IClipboard$Stub;->removePrimaryClipAt(Ljava/lang/String;I)Z

    #@101
    move-result v2

    #@102
    .line 170
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@105
    .line 171
    if-eqz v2, :cond_108

    #@107
    move v3, v4

    #@108
    :cond_108
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@10b
    goto/16 :goto_9

    #@10d
    .line 176
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v2           #_result:Z
    :sswitch_10d
    const-string v5, "android.content.IClipboard"

    #@10f
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@112
    .line 178
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@115
    move-result-object v0

    #@116
    .line 179
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/content/IClipboard$Stub;->removeAllPrimaryClips(Ljava/lang/String;)Z

    #@119
    move-result v2

    #@11a
    .line 180
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11d
    .line 181
    if-eqz v2, :cond_120

    #@11f
    move v3, v4

    #@120
    :cond_120
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@123
    goto/16 :goto_9

    #@125
    .line 186
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:Z
    :sswitch_125
    const-string v5, "android.content.IClipboard"

    #@127
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12a
    .line 187
    invoke-virtual {p0}, Landroid/content/IClipboard$Stub;->isUidSecure()Z

    #@12d
    move-result v2

    #@12e
    .line 188
    .restart local v2       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@131
    .line 189
    if-eqz v2, :cond_134

    #@133
    move v3, v4

    #@134
    :cond_134
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@137
    goto/16 :goto_9

    #@139
    .line 194
    .end local v2           #_result:Z
    :sswitch_139
    const-string v5, "android.content.IClipboard"

    #@13b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13e
    .line 196
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@141
    move-result-object v0

    #@142
    .line 198
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@145
    move-result v1

    #@146
    .line 199
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/content/IClipboard$Stub;->getInitialPrimaryClipAt(Ljava/lang/String;I)Landroid/content/ClipData;

    #@149
    move-result-object v2

    #@14a
    .line 200
    .local v2, _result:Landroid/content/ClipData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@14d
    .line 201
    if-eqz v2, :cond_157

    #@14f
    .line 202
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@152
    .line 203
    invoke-virtual {v2, p3, v4}, Landroid/content/ClipData;->writeToParcel(Landroid/os/Parcel;I)V

    #@155
    goto/16 :goto_9

    #@157
    .line 206
    :cond_157
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@15a
    goto/16 :goto_9

    #@15c
    .line 212
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v2           #_result:Landroid/content/ClipData;
    :sswitch_15c
    const-string v5, "android.content.IClipboard"

    #@15e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@161
    .line 213
    invoke-virtual {p0}, Landroid/content/IClipboard$Stub;->hasClipboardText()Z

    #@164
    move-result v2

    #@165
    .line 214
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@168
    .line 215
    if-eqz v2, :cond_16b

    #@16a
    move v3, v4

    #@16b
    :cond_16b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@16e
    goto/16 :goto_9

    #@170
    .line 43
    :sswitch_data_170
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_49
        0x4 -> :sswitch_62
        0x5 -> :sswitch_75
        0x6 -> :sswitch_89
        0x7 -> :sswitch_9e
        0x8 -> :sswitch_c1
        0x9 -> :sswitch_e0
        0xa -> :sswitch_f1
        0xb -> :sswitch_10d
        0xc -> :sswitch_125
        0xd -> :sswitch_139
        0xe -> :sswitch_15c
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
