.class public Landroid/content/SyncStats;
.super Ljava/lang/Object;
.source "SyncStats.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/SyncStats;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public numAuthExceptions:J

.field public numConflictDetectedExceptions:J

.field public numDeletes:J

.field public numEntries:J

.field public numInserts:J

.field public numIoExceptions:J

.field public numParseExceptions:J

.field public numSkippedEntries:J

.field public numUpdates:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 169
    new-instance v0, Landroid/content/SyncStats$1;

    #@2
    invoke-direct {v0}, Landroid/content/SyncStats$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/SyncStats;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 96
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 97
    iput-wide v0, p0, Landroid/content/SyncStats;->numAuthExceptions:J

    #@7
    .line 98
    iput-wide v0, p0, Landroid/content/SyncStats;->numIoExceptions:J

    #@9
    .line 99
    iput-wide v0, p0, Landroid/content/SyncStats;->numParseExceptions:J

    #@b
    .line 100
    iput-wide v0, p0, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    #@d
    .line 101
    iput-wide v0, p0, Landroid/content/SyncStats;->numInserts:J

    #@f
    .line 102
    iput-wide v0, p0, Landroid/content/SyncStats;->numUpdates:J

    #@11
    .line 103
    iput-wide v0, p0, Landroid/content/SyncStats;->numDeletes:J

    #@13
    .line 104
    iput-wide v0, p0, Landroid/content/SyncStats;->numEntries:J

    #@15
    .line 105
    iput-wide v0, p0, Landroid/content/SyncStats;->numSkippedEntries:J

    #@17
    .line 106
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 108
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@6
    move-result-wide v0

    #@7
    iput-wide v0, p0, Landroid/content/SyncStats;->numAuthExceptions:J

    #@9
    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@c
    move-result-wide v0

    #@d
    iput-wide v0, p0, Landroid/content/SyncStats;->numIoExceptions:J

    #@f
    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@12
    move-result-wide v0

    #@13
    iput-wide v0, p0, Landroid/content/SyncStats;->numParseExceptions:J

    #@15
    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@18
    move-result-wide v0

    #@19
    iput-wide v0, p0, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    #@1b
    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@1e
    move-result-wide v0

    #@1f
    iput-wide v0, p0, Landroid/content/SyncStats;->numInserts:J

    #@21
    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@24
    move-result-wide v0

    #@25
    iput-wide v0, p0, Landroid/content/SyncStats;->numUpdates:J

    #@27
    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@2a
    move-result-wide v0

    #@2b
    iput-wide v0, p0, Landroid/content/SyncStats;->numDeletes:J

    #@2d
    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@30
    move-result-wide v0

    #@31
    iput-wide v0, p0, Landroid/content/SyncStats;->numEntries:J

    #@33
    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@36
    move-result-wide v0

    #@37
    iput-wide v0, p0, Landroid/content/SyncStats;->numSkippedEntries:J

    #@39
    .line 118
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 3

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 142
    iput-wide v0, p0, Landroid/content/SyncStats;->numAuthExceptions:J

    #@4
    .line 143
    iput-wide v0, p0, Landroid/content/SyncStats;->numIoExceptions:J

    #@6
    .line 144
    iput-wide v0, p0, Landroid/content/SyncStats;->numParseExceptions:J

    #@8
    .line 145
    iput-wide v0, p0, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    #@a
    .line 146
    iput-wide v0, p0, Landroid/content/SyncStats;->numInserts:J

    #@c
    .line 147
    iput-wide v0, p0, Landroid/content/SyncStats;->numUpdates:J

    #@e
    .line 148
    iput-wide v0, p0, Landroid/content/SyncStats;->numDeletes:J

    #@10
    .line 149
    iput-wide v0, p0, Landroid/content/SyncStats;->numEntries:J

    #@12
    .line 150
    iput-wide v0, p0, Landroid/content/SyncStats;->numSkippedEntries:J

    #@14
    .line 151
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 154
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 123
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, " stats ["

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 124
    iget-wide v1, p0, Landroid/content/SyncStats;->numAuthExceptions:J

    #@e
    cmp-long v1, v1, v4

    #@10
    if-lez v1, :cond_1d

    #@12
    const-string v1, " numAuthExceptions: "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    iget-wide v2, p0, Landroid/content/SyncStats;->numAuthExceptions:J

    #@1a
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1d
    .line 125
    :cond_1d
    iget-wide v1, p0, Landroid/content/SyncStats;->numIoExceptions:J

    #@1f
    cmp-long v1, v1, v4

    #@21
    if-lez v1, :cond_2e

    #@23
    const-string v1, " numIoExceptions: "

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    iget-wide v2, p0, Landroid/content/SyncStats;->numIoExceptions:J

    #@2b
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2e
    .line 126
    :cond_2e
    iget-wide v1, p0, Landroid/content/SyncStats;->numParseExceptions:J

    #@30
    cmp-long v1, v1, v4

    #@32
    if-lez v1, :cond_3f

    #@34
    const-string v1, " numParseExceptions: "

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    iget-wide v2, p0, Landroid/content/SyncStats;->numParseExceptions:J

    #@3c
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3f
    .line 127
    :cond_3f
    iget-wide v1, p0, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    #@41
    cmp-long v1, v1, v4

    #@43
    if-lez v1, :cond_50

    #@45
    .line 128
    const-string v1, " numConflictDetectedExceptions: "

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    iget-wide v2, p0, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    #@4d
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@50
    .line 129
    :cond_50
    iget-wide v1, p0, Landroid/content/SyncStats;->numInserts:J

    #@52
    cmp-long v1, v1, v4

    #@54
    if-lez v1, :cond_61

    #@56
    const-string v1, " numInserts: "

    #@58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v1

    #@5c
    iget-wide v2, p0, Landroid/content/SyncStats;->numInserts:J

    #@5e
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@61
    .line 130
    :cond_61
    iget-wide v1, p0, Landroid/content/SyncStats;->numUpdates:J

    #@63
    cmp-long v1, v1, v4

    #@65
    if-lez v1, :cond_72

    #@67
    const-string v1, " numUpdates: "

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v1

    #@6d
    iget-wide v2, p0, Landroid/content/SyncStats;->numUpdates:J

    #@6f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@72
    .line 131
    :cond_72
    iget-wide v1, p0, Landroid/content/SyncStats;->numDeletes:J

    #@74
    cmp-long v1, v1, v4

    #@76
    if-lez v1, :cond_83

    #@78
    const-string v1, " numDeletes: "

    #@7a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v1

    #@7e
    iget-wide v2, p0, Landroid/content/SyncStats;->numDeletes:J

    #@80
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@83
    .line 132
    :cond_83
    iget-wide v1, p0, Landroid/content/SyncStats;->numEntries:J

    #@85
    cmp-long v1, v1, v4

    #@87
    if-lez v1, :cond_94

    #@89
    const-string v1, " numEntries: "

    #@8b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v1

    #@8f
    iget-wide v2, p0, Landroid/content/SyncStats;->numEntries:J

    #@91
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@94
    .line 133
    :cond_94
    iget-wide v1, p0, Landroid/content/SyncStats;->numSkippedEntries:J

    #@96
    cmp-long v1, v1, v4

    #@98
    if-lez v1, :cond_a5

    #@9a
    const-string v1, " numSkippedEntries: "

    #@9c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v1

    #@a0
    iget-wide v2, p0, Landroid/content/SyncStats;->numSkippedEntries:J

    #@a2
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@a5
    .line 134
    :cond_a5
    const-string v1, "]"

    #@a7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    .line 135
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v1

    #@ae
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 158
    iget-wide v0, p0, Landroid/content/SyncStats;->numAuthExceptions:J

    #@2
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@5
    .line 159
    iget-wide v0, p0, Landroid/content/SyncStats;->numIoExceptions:J

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@a
    .line 160
    iget-wide v0, p0, Landroid/content/SyncStats;->numParseExceptions:J

    #@c
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@f
    .line 161
    iget-wide v0, p0, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    #@11
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@14
    .line 162
    iget-wide v0, p0, Landroid/content/SyncStats;->numInserts:J

    #@16
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@19
    .line 163
    iget-wide v0, p0, Landroid/content/SyncStats;->numUpdates:J

    #@1b
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@1e
    .line 164
    iget-wide v0, p0, Landroid/content/SyncStats;->numDeletes:J

    #@20
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@23
    .line 165
    iget-wide v0, p0, Landroid/content/SyncStats;->numEntries:J

    #@25
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@28
    .line 166
    iget-wide v0, p0, Landroid/content/SyncStats;->numSkippedEntries:J

    #@2a
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@2d
    .line 167
    return-void
.end method
