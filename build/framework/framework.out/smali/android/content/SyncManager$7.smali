.class Landroid/content/SyncManager$7;
.super Landroid/content/BroadcastReceiver;
.source "SyncManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/SyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/content/SyncManager;


# direct methods
.method constructor <init>(Landroid/content/SyncManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 310
    iput-object p1, p0, Landroid/content/SyncManager$7;->this$0:Landroid/content/SyncManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/16 v3, -0x2710

    #@2
    .line 313
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 314
    .local v0, action:Ljava/lang/String;
    const-string v2, "android.intent.extra.user_handle"

    #@8
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@b
    move-result v1

    #@c
    .line 315
    .local v1, userId:I
    if-ne v1, v3, :cond_f

    #@e
    .line 324
    :cond_e
    :goto_e
    return-void

    #@f
    .line 317
    :cond_f
    const-string v2, "android.intent.action.USER_REMOVED"

    #@11
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_1d

    #@17
    .line 318
    iget-object v2, p0, Landroid/content/SyncManager$7;->this$0:Landroid/content/SyncManager;

    #@19
    invoke-static {v2, v1}, Landroid/content/SyncManager;->access$800(Landroid/content/SyncManager;I)V

    #@1c
    goto :goto_e

    #@1d
    .line 319
    :cond_1d
    const-string v2, "android.intent.action.USER_STARTING"

    #@1f
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v2

    #@23
    if-eqz v2, :cond_2b

    #@25
    .line 320
    iget-object v2, p0, Landroid/content/SyncManager$7;->this$0:Landroid/content/SyncManager;

    #@27
    invoke-static {v2, v1}, Landroid/content/SyncManager;->access$900(Landroid/content/SyncManager;I)V

    #@2a
    goto :goto_e

    #@2b
    .line 321
    :cond_2b
    const-string v2, "android.intent.action.USER_STOPPING"

    #@2d
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v2

    #@31
    if-eqz v2, :cond_e

    #@33
    .line 322
    iget-object v2, p0, Landroid/content/SyncManager$7;->this$0:Landroid/content/SyncManager;

    #@35
    invoke-static {v2, v1}, Landroid/content/SyncManager;->access$1000(Landroid/content/SyncManager;I)V

    #@38
    goto :goto_e
.end method
