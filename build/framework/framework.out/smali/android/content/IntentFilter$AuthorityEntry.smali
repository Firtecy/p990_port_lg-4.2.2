.class public final Landroid/content/IntentFilter$AuthorityEntry;
.super Ljava/lang/Object;
.source "IntentFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/IntentFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AuthorityEntry"
.end annotation


# instance fields
.field private final mHost:Ljava/lang/String;

.field private final mOrigHost:Ljava/lang/String;

.field private final mPort:I

.field private final mWild:Z


# direct methods
.method constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 642
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 643
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mOrigHost:Ljava/lang/String;

    #@9
    .line 644
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mHost:Ljava/lang/String;

    #@f
    .line 645
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1f

    #@15
    const/4 v0, 0x1

    #@16
    :goto_16
    iput-boolean v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mWild:Z

    #@18
    .line 646
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mPort:I

    #@1e
    .line 647
    return-void

    #@1f
    .line 645
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_16
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "host"
    .parameter "port"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 635
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 636
    iput-object p1, p0, Landroid/content/IntentFilter$AuthorityEntry;->mOrigHost:Ljava/lang/String;

    #@7
    .line 637
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@a
    move-result v2

    #@b
    if-lez v2, :cond_16

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@10
    move-result v2

    #@11
    const/16 v3, 0x2a

    #@13
    if-ne v2, v3, :cond_16

    #@15
    move v0, v1

    #@16
    :cond_16
    iput-boolean v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mWild:Z

    #@18
    .line 638
    iget-boolean v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mWild:Z

    #@1a
    if-eqz v0, :cond_24

    #@1c
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@23
    move-result-object p1

    #@24
    .end local p1
    :cond_24
    iput-object p1, p0, Landroid/content/IntentFilter$AuthorityEntry;->mHost:Ljava/lang/String;

    #@26
    .line 639
    if-eqz p2, :cond_2f

    #@28
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2b
    move-result v0

    #@2c
    :goto_2c
    iput v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mPort:I

    #@2e
    .line 640
    return-void

    #@2f
    .line 639
    :cond_2f
    const/4 v0, -0x1

    #@30
    goto :goto_2c
.end method

.method static synthetic access$000(Landroid/content/IntentFilter$AuthorityEntry;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 629
    iget-object v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mHost:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/content/IntentFilter$AuthorityEntry;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 629
    iget v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mPort:I

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/content/IntentFilter$AuthorityEntry;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 629
    iget-boolean v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mWild:Z

    #@2
    return v0
.end method


# virtual methods
.method public getHost()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 657
    iget-object v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mOrigHost:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPort()I
    .registers 2

    #@0
    .prologue
    .line 661
    iget v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mPort:I

    #@2
    return v0
.end method

.method public match(Landroid/net/Uri;)I
    .registers 6
    .parameter "data"

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    .line 675
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 676
    .local v0, host:Ljava/lang/String;
    if-nez v0, :cond_8

    #@7
    .line 696
    :cond_7
    :goto_7
    return v1

    #@8
    .line 681
    :cond_8
    iget-boolean v2, p0, Landroid/content/IntentFilter$AuthorityEntry;->mWild:Z

    #@a
    if-eqz v2, :cond_27

    #@c
    .line 682
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@f
    move-result v2

    #@10
    iget-object v3, p0, Landroid/content/IntentFilter$AuthorityEntry;->mHost:Ljava/lang/String;

    #@12
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@15
    move-result v3

    #@16
    if-lt v2, v3, :cond_7

    #@18
    .line 685
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@1b
    move-result v2

    #@1c
    iget-object v3, p0, Landroid/content/IntentFilter$AuthorityEntry;->mHost:Ljava/lang/String;

    #@1e
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@21
    move-result v3

    #@22
    sub-int/2addr v2, v3

    #@23
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 687
    :cond_27
    iget-object v2, p0, Landroid/content/IntentFilter$AuthorityEntry;->mHost:Ljava/lang/String;

    #@29
    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@2c
    move-result v2

    #@2d
    if-nez v2, :cond_7

    #@2f
    .line 690
    iget v2, p0, Landroid/content/IntentFilter$AuthorityEntry;->mPort:I

    #@31
    if-ltz v2, :cond_3e

    #@33
    .line 691
    iget v2, p0, Landroid/content/IntentFilter$AuthorityEntry;->mPort:I

    #@35
    invoke-virtual {p1}, Landroid/net/Uri;->getPort()I

    #@38
    move-result v3

    #@39
    if-ne v2, v3, :cond_7

    #@3b
    .line 694
    const/high16 v1, 0x40

    #@3d
    goto :goto_7

    #@3e
    .line 696
    :cond_3e
    const/high16 v1, 0x30

    #@40
    goto :goto_7
.end method

.method writeToParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "dest"

    #@0
    .prologue
    .line 650
    iget-object v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mOrigHost:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 651
    iget-object v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mHost:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 652
    iget-boolean v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mWild:Z

    #@c
    if-eqz v0, :cond_18

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 653
    iget v0, p0, Landroid/content/IntentFilter$AuthorityEntry;->mPort:I

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 654
    return-void

    #@18
    .line 652
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_f
.end method
