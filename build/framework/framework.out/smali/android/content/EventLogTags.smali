.class public Landroid/content/EventLogTags;
.super Ljava/lang/Object;
.source "EventLogTags.java"


# static fields
.field public static final BINDER_SAMPLE:I = 0xcb24

.field public static final CONTENT_QUERY_SAMPLE:I = 0xcb22

.field public static final CONTENT_UPDATE_SAMPLE:I = 0xcb23


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static writeBinderSample(Ljava/lang/String;IILjava/lang/String;I)V
    .registers 9
    .parameter "descriptor"
    .parameter "methodNum"
    .parameter "time"
    .parameter "blockingPackage"
    .parameter "samplePercent"

    #@0
    .prologue
    .line 31
    const v0, 0xcb24

    #@3
    const/4 v1, 0x5

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    aput-object p0, v1, v2

    #@9
    const/4 v2, 0x1

    #@a
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d
    move-result-object v3

    #@e
    aput-object v3, v1, v2

    #@10
    const/4 v2, 0x2

    #@11
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v3

    #@15
    aput-object v3, v1, v2

    #@17
    const/4 v2, 0x3

    #@18
    aput-object p3, v1, v2

    #@1a
    const/4 v2, 0x4

    #@1b
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    aput-object v3, v1, v2

    #@21
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@24
    .line 32
    return-void
.end method

.method public static writeContentQuerySample(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V
    .registers 11
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "sortorder"
    .parameter "time"
    .parameter "blockingPackage"
    .parameter "samplePercent"

    #@0
    .prologue
    .line 23
    const v0, 0xcb22

    #@3
    const/4 v1, 0x7

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    aput-object p0, v1, v2

    #@9
    const/4 v2, 0x1

    #@a
    aput-object p1, v1, v2

    #@c
    const/4 v2, 0x2

    #@d
    aput-object p2, v1, v2

    #@f
    const/4 v2, 0x3

    #@10
    aput-object p3, v1, v2

    #@12
    const/4 v2, 0x4

    #@13
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v3

    #@17
    aput-object v3, v1, v2

    #@19
    const/4 v2, 0x5

    #@1a
    aput-object p5, v1, v2

    #@1c
    const/4 v2, 0x6

    #@1d
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    aput-object v3, v1, v2

    #@23
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@26
    .line 24
    return-void
.end method

.method public static writeContentUpdateSample(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V
    .registers 10
    .parameter "uri"
    .parameter "operation"
    .parameter "selection"
    .parameter "time"
    .parameter "blockingPackage"
    .parameter "samplePercent"

    #@0
    .prologue
    .line 27
    const v0, 0xcb23

    #@3
    const/4 v1, 0x6

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    aput-object p0, v1, v2

    #@9
    const/4 v2, 0x1

    #@a
    aput-object p1, v1, v2

    #@c
    const/4 v2, 0x2

    #@d
    aput-object p2, v1, v2

    #@f
    const/4 v2, 0x3

    #@10
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v3

    #@14
    aput-object v3, v1, v2

    #@16
    const/4 v2, 0x4

    #@17
    aput-object p4, v1, v2

    #@19
    const/4 v2, 0x5

    #@1a
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v3

    #@1e
    aput-object v3, v1, v2

    #@20
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@23
    .line 28
    return-void
.end method
