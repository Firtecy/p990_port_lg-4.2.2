.class public final Landroid/content/SyncResult;
.super Ljava/lang/Object;
.source "SyncResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ALREADY_IN_PROGRESS:Landroid/content/SyncResult;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/SyncResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public databaseError:Z

.field public delayUntil:J

.field public fullSyncRequested:Z

.field public moreRecordsToGet:Z

.field public partialSyncUnavailable:Z

.field public final stats:Landroid/content/SyncStats;

.field public final syncAlreadyInProgress:Z

.field public tooManyDeletions:Z

.field public tooManyRetries:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 101
    new-instance v0, Landroid/content/SyncResult;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-direct {v0, v1}, Landroid/content/SyncResult;-><init>(Z)V

    #@6
    sput-object v0, Landroid/content/SyncResult;->ALREADY_IN_PROGRESS:Landroid/content/SyncResult;

    #@8
    .line 223
    new-instance v0, Landroid/content/SyncResult$1;

    #@a
    invoke-direct {v0}, Landroid/content/SyncResult$1;-><init>()V

    #@d
    sput-object v0, Landroid/content/SyncResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 119
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/content/SyncResult;-><init>(Z)V

    #@4
    .line 120
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "parcel"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 138
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_51

    #@b
    move v0, v1

    #@c
    :goto_c
    iput-boolean v0, p0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@e
    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_53

    #@14
    move v0, v1

    #@15
    :goto_15
    iput-boolean v0, p0, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@17
    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_55

    #@1d
    move v0, v1

    #@1e
    :goto_1e
    iput-boolean v0, p0, Landroid/content/SyncResult;->tooManyRetries:Z

    #@20
    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_57

    #@26
    move v0, v1

    #@27
    :goto_27
    iput-boolean v0, p0, Landroid/content/SyncResult;->databaseError:Z

    #@29
    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_59

    #@2f
    move v0, v1

    #@30
    :goto_30
    iput-boolean v0, p0, Landroid/content/SyncResult;->fullSyncRequested:Z

    #@32
    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_5b

    #@38
    move v0, v1

    #@39
    :goto_39
    iput-boolean v0, p0, Landroid/content/SyncResult;->partialSyncUnavailable:Z

    #@3b
    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3e
    move-result v0

    #@3f
    if-eqz v0, :cond_5d

    #@41
    :goto_41
    iput-boolean v1, p0, Landroid/content/SyncResult;->moreRecordsToGet:Z

    #@43
    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@46
    move-result-wide v0

    #@47
    iput-wide v0, p0, Landroid/content/SyncResult;->delayUntil:J

    #@49
    .line 147
    new-instance v0, Landroid/content/SyncStats;

    #@4b
    invoke-direct {v0, p1}, Landroid/content/SyncStats;-><init>(Landroid/os/Parcel;)V

    #@4e
    iput-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@50
    .line 148
    return-void

    #@51
    :cond_51
    move v0, v2

    #@52
    .line 139
    goto :goto_c

    #@53
    :cond_53
    move v0, v2

    #@54
    .line 140
    goto :goto_15

    #@55
    :cond_55
    move v0, v2

    #@56
    .line 141
    goto :goto_1e

    #@57
    :cond_57
    move v0, v2

    #@58
    .line 142
    goto :goto_27

    #@59
    :cond_59
    move v0, v2

    #@5a
    .line 143
    goto :goto_30

    #@5b
    :cond_5b
    move v0, v2

    #@5c
    .line 144
    goto :goto_39

    #@5d
    :cond_5d
    move v1, v2

    #@5e
    .line 145
    goto :goto_41
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/SyncResult$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/content/SyncResult;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private constructor <init>(Z)V
    .registers 4
    .parameter "syncAlreadyInProgress"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 127
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 128
    iput-boolean p1, p0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@6
    .line 129
    iput-boolean v0, p0, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@8
    .line 130
    iput-boolean v0, p0, Landroid/content/SyncResult;->tooManyRetries:Z

    #@a
    .line 131
    iput-boolean v0, p0, Landroid/content/SyncResult;->fullSyncRequested:Z

    #@c
    .line 132
    iput-boolean v0, p0, Landroid/content/SyncResult;->partialSyncUnavailable:Z

    #@e
    .line 133
    iput-boolean v0, p0, Landroid/content/SyncResult;->moreRecordsToGet:Z

    #@10
    .line 134
    const-wide/16 v0, 0x0

    #@12
    iput-wide v0, p0, Landroid/content/SyncResult;->delayUntil:J

    #@14
    .line 135
    new-instance v0, Landroid/content/SyncStats;

    #@16
    invoke-direct {v0}, Landroid/content/SyncStats;-><init>()V

    #@19
    iput-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@1b
    .line 136
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 209
    iget-boolean v0, p0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@3
    if-eqz v0, :cond_e

    #@5
    .line 210
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@7
    const-string/jumbo v1, "you are not allowed to clear the ALREADY_IN_PROGRESS SyncStats"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 213
    :cond_e
    iput-boolean v1, p0, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@10
    .line 214
    iput-boolean v1, p0, Landroid/content/SyncResult;->tooManyRetries:Z

    #@12
    .line 215
    iput-boolean v1, p0, Landroid/content/SyncResult;->databaseError:Z

    #@14
    .line 216
    iput-boolean v1, p0, Landroid/content/SyncResult;->fullSyncRequested:Z

    #@16
    .line 217
    iput-boolean v1, p0, Landroid/content/SyncResult;->partialSyncUnavailable:Z

    #@18
    .line 218
    iput-boolean v1, p0, Landroid/content/SyncResult;->moreRecordsToGet:Z

    #@1a
    .line 219
    const-wide/16 v0, 0x0

    #@1c
    iput-wide v0, p0, Landroid/content/SyncResult;->delayUntil:J

    #@1e
    .line 220
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@20
    invoke-virtual {v0}, Landroid/content/SyncStats;->clear()V

    #@23
    .line 221
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 234
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public hasError()Z
    .registers 2

    #@0
    .prologue
    .line 195
    invoke-virtual {p0}, Landroid/content/SyncResult;->hasSoftError()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-virtual {p0}, Landroid/content/SyncResult;->hasHardError()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public hasHardError()Z
    .registers 5

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 166
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@4
    iget-wide v0, v0, Landroid/content/SyncStats;->numParseExceptions:J

    #@6
    cmp-long v0, v0, v2

    #@8
    if-gtz v0, :cond_26

    #@a
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@c
    iget-wide v0, v0, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    #@e
    cmp-long v0, v0, v2

    #@10
    if-gtz v0, :cond_26

    #@12
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@14
    iget-wide v0, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    #@16
    cmp-long v0, v0, v2

    #@18
    if-gtz v0, :cond_26

    #@1a
    iget-boolean v0, p0, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@1c
    if-nez v0, :cond_26

    #@1e
    iget-boolean v0, p0, Landroid/content/SyncResult;->tooManyRetries:Z

    #@20
    if-nez v0, :cond_26

    #@22
    iget-boolean v0, p0, Landroid/content/SyncResult;->databaseError:Z

    #@24
    if-eqz v0, :cond_28

    #@26
    :cond_26
    const/4 v0, 0x1

    #@27
    :goto_27
    return v0

    #@28
    :cond_28
    const/4 v0, 0x0

    #@29
    goto :goto_27
.end method

.method public hasSoftError()Z
    .registers 5

    #@0
    .prologue
    .line 187
    iget-boolean v0, p0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@2
    if-nez v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@6
    iget-wide v0, v0, Landroid/content/SyncStats;->numIoExceptions:J

    #@8
    const-wide/16 v2, 0x0

    #@a
    cmp-long v0, v0, v2

    #@c
    if-lez v0, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public madeSomeProgress()Z
    .registers 5

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 199
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@4
    iget-wide v0, v0, Landroid/content/SyncStats;->numDeletes:J

    #@6
    cmp-long v0, v0, v2

    #@8
    if-lez v0, :cond_e

    #@a
    iget-boolean v0, p0, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@c
    if-eqz v0, :cond_1e

    #@e
    :cond_e
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@10
    iget-wide v0, v0, Landroid/content/SyncStats;->numInserts:J

    #@12
    cmp-long v0, v0, v2

    #@14
    if-gtz v0, :cond_1e

    #@16
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@18
    iget-wide v0, v0, Landroid/content/SyncStats;->numUpdates:J

    #@1a
    cmp-long v0, v0, v2

    #@1c
    if-lez v0, :cond_20

    #@1e
    :cond_1e
    const/4 v0, 0x1

    #@1f
    :goto_1f
    return v0

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_1f
.end method

.method public toDebugString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 280
    new-instance v0, Ljava/lang/StringBuffer;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@7
    .line 282
    .local v0, sb:Ljava/lang/StringBuffer;
    iget-boolean v1, p0, Landroid/content/SyncResult;->fullSyncRequested:Z

    #@9
    if-eqz v1, :cond_10

    #@b
    .line 283
    const-string v1, "f1"

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@10
    .line 285
    :cond_10
    iget-boolean v1, p0, Landroid/content/SyncResult;->partialSyncUnavailable:Z

    #@12
    if-eqz v1, :cond_1a

    #@14
    .line 286
    const-string/jumbo v1, "r1"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1a
    .line 288
    :cond_1a
    invoke-virtual {p0}, Landroid/content/SyncResult;->hasHardError()Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_25

    #@20
    .line 289
    const-string v1, "X1"

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@25
    .line 291
    :cond_25
    iget-object v1, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@27
    iget-wide v1, v1, Landroid/content/SyncStats;->numParseExceptions:J

    #@29
    cmp-long v1, v1, v4

    #@2b
    if-lez v1, :cond_3a

    #@2d
    .line 292
    const-string v1, "e"

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@32
    move-result-object v1

    #@33
    iget-object v2, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@35
    iget-wide v2, v2, Landroid/content/SyncStats;->numParseExceptions:J

    #@37
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    #@3a
    .line 294
    :cond_3a
    iget-object v1, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@3c
    iget-wide v1, v1, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    #@3e
    cmp-long v1, v1, v4

    #@40
    if-lez v1, :cond_4f

    #@42
    .line 295
    const-string v1, "c"

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@47
    move-result-object v1

    #@48
    iget-object v2, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@4a
    iget-wide v2, v2, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    #@4c
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    #@4f
    .line 297
    :cond_4f
    iget-object v1, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@51
    iget-wide v1, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    #@53
    cmp-long v1, v1, v4

    #@55
    if-lez v1, :cond_64

    #@57
    .line 298
    const-string v1, "a"

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@5c
    move-result-object v1

    #@5d
    iget-object v2, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@5f
    iget-wide v2, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    #@61
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    #@64
    .line 300
    :cond_64
    iget-boolean v1, p0, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@66
    if-eqz v1, :cond_6d

    #@68
    .line 301
    const-string v1, "D1"

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@6d
    .line 303
    :cond_6d
    iget-boolean v1, p0, Landroid/content/SyncResult;->tooManyRetries:Z

    #@6f
    if-eqz v1, :cond_76

    #@71
    .line 304
    const-string v1, "R1"

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@76
    .line 306
    :cond_76
    iget-boolean v1, p0, Landroid/content/SyncResult;->databaseError:Z

    #@78
    if-eqz v1, :cond_7f

    #@7a
    .line 307
    const-string v1, "b1"

    #@7c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@7f
    .line 309
    :cond_7f
    invoke-virtual {p0}, Landroid/content/SyncResult;->hasSoftError()Z

    #@82
    move-result v1

    #@83
    if-eqz v1, :cond_8b

    #@85
    .line 310
    const-string/jumbo v1, "x1"

    #@88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@8b
    .line 312
    :cond_8b
    iget-boolean v1, p0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@8d
    if-eqz v1, :cond_95

    #@8f
    .line 313
    const-string/jumbo v1, "l1"

    #@92
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@95
    .line 315
    :cond_95
    iget-object v1, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@97
    iget-wide v1, v1, Landroid/content/SyncStats;->numIoExceptions:J

    #@99
    cmp-long v1, v1, v4

    #@9b
    if-lez v1, :cond_aa

    #@9d
    .line 316
    const-string v1, "I"

    #@9f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a2
    move-result-object v1

    #@a3
    iget-object v2, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@a5
    iget-wide v2, v2, Landroid/content/SyncStats;->numIoExceptions:J

    #@a7
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    #@aa
    .line 318
    :cond_aa
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@ad
    move-result-object v1

    #@ae
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 252
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "SyncResult:"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 253
    iget-boolean v1, p0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@c
    if-eqz v1, :cond_19

    #@e
    .line 254
    const-string v1, " syncAlreadyInProgress: "

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget-boolean v2, p0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19
    .line 256
    :cond_19
    iget-boolean v1, p0, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@1b
    if-eqz v1, :cond_28

    #@1d
    const-string v1, " tooManyDeletions: "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    iget-boolean v2, p0, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@28
    .line 257
    :cond_28
    iget-boolean v1, p0, Landroid/content/SyncResult;->tooManyRetries:Z

    #@2a
    if-eqz v1, :cond_37

    #@2c
    const-string v1, " tooManyRetries: "

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    iget-boolean v2, p0, Landroid/content/SyncResult;->tooManyRetries:Z

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@37
    .line 258
    :cond_37
    iget-boolean v1, p0, Landroid/content/SyncResult;->databaseError:Z

    #@39
    if-eqz v1, :cond_46

    #@3b
    const-string v1, " databaseError: "

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    iget-boolean v2, p0, Landroid/content/SyncResult;->databaseError:Z

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@46
    .line 259
    :cond_46
    iget-boolean v1, p0, Landroid/content/SyncResult;->fullSyncRequested:Z

    #@48
    if-eqz v1, :cond_55

    #@4a
    const-string v1, " fullSyncRequested: "

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    iget-boolean v2, p0, Landroid/content/SyncResult;->fullSyncRequested:Z

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@55
    .line 260
    :cond_55
    iget-boolean v1, p0, Landroid/content/SyncResult;->partialSyncUnavailable:Z

    #@57
    if-eqz v1, :cond_64

    #@59
    .line 261
    const-string v1, " partialSyncUnavailable: "

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    iget-boolean v2, p0, Landroid/content/SyncResult;->partialSyncUnavailable:Z

    #@61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@64
    .line 263
    :cond_64
    iget-boolean v1, p0, Landroid/content/SyncResult;->moreRecordsToGet:Z

    #@66
    if-eqz v1, :cond_73

    #@68
    const-string v1, " moreRecordsToGet: "

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v1

    #@6e
    iget-boolean v2, p0, Landroid/content/SyncResult;->moreRecordsToGet:Z

    #@70
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@73
    .line 264
    :cond_73
    iget-wide v1, p0, Landroid/content/SyncResult;->delayUntil:J

    #@75
    const-wide/16 v3, 0x0

    #@77
    cmp-long v1, v1, v3

    #@79
    if-lez v1, :cond_86

    #@7b
    const-string v1, " delayUntil: "

    #@7d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    iget-wide v2, p0, Landroid/content/SyncResult;->delayUntil:J

    #@83
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@86
    .line 265
    :cond_86
    iget-object v1, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    .line 266
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v1

    #@8f
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 238
    iget-boolean v0, p0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    #@4
    if-eqz v0, :cond_44

    #@6
    move v0, v1

    #@7
    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 239
    iget-boolean v0, p0, Landroid/content/SyncResult;->tooManyDeletions:Z

    #@c
    if-eqz v0, :cond_46

    #@e
    move v0, v1

    #@f
    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 240
    iget-boolean v0, p0, Landroid/content/SyncResult;->tooManyRetries:Z

    #@14
    if-eqz v0, :cond_48

    #@16
    move v0, v1

    #@17
    :goto_17
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 241
    iget-boolean v0, p0, Landroid/content/SyncResult;->databaseError:Z

    #@1c
    if-eqz v0, :cond_4a

    #@1e
    move v0, v1

    #@1f
    :goto_1f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 242
    iget-boolean v0, p0, Landroid/content/SyncResult;->fullSyncRequested:Z

    #@24
    if-eqz v0, :cond_4c

    #@26
    move v0, v1

    #@27
    :goto_27
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 243
    iget-boolean v0, p0, Landroid/content/SyncResult;->partialSyncUnavailable:Z

    #@2c
    if-eqz v0, :cond_4e

    #@2e
    move v0, v1

    #@2f
    :goto_2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 244
    iget-boolean v0, p0, Landroid/content/SyncResult;->moreRecordsToGet:Z

    #@34
    if-eqz v0, :cond_50

    #@36
    :goto_36
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@39
    .line 245
    iget-wide v0, p0, Landroid/content/SyncResult;->delayUntil:J

    #@3b
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@3e
    .line 246
    iget-object v0, p0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    #@40
    invoke-virtual {v0, p1, p2}, Landroid/content/SyncStats;->writeToParcel(Landroid/os/Parcel;I)V

    #@43
    .line 247
    return-void

    #@44
    :cond_44
    move v0, v2

    #@45
    .line 238
    goto :goto_7

    #@46
    :cond_46
    move v0, v2

    #@47
    .line 239
    goto :goto_f

    #@48
    :cond_48
    move v0, v2

    #@49
    .line 240
    goto :goto_17

    #@4a
    :cond_4a
    move v0, v2

    #@4b
    .line 241
    goto :goto_1f

    #@4c
    :cond_4c
    move v0, v2

    #@4d
    .line 242
    goto :goto_27

    #@4e
    :cond_4e
    move v0, v2

    #@4f
    .line 243
    goto :goto_2f

    #@50
    :cond_50
    move v1, v2

    #@51
    .line 244
    goto :goto_36
.end method
