.class public Landroid/content/SyncStorageEngine$PendingOperation;
.super Ljava/lang/Object;
.source "SyncStorageEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/SyncStorageEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PendingOperation"
.end annotation


# instance fields
.field final account:Landroid/accounts/Account;

.field final authority:Ljava/lang/String;

.field authorityId:I

.field final expedited:Z

.field final extras:Landroid/os/Bundle;

.field flatExtras:[B

.field final syncSource:I

.field final userId:I


# direct methods
.method constructor <init>(Landroid/accounts/Account;IILjava/lang/String;Landroid/os/Bundle;Z)V
    .registers 8
    .parameter "account"
    .parameter "userId"
    .parameter "source"
    .parameter "authority"
    .parameter "extras"
    .parameter "expedited"

    #@0
    .prologue
    .line 159
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 160
    iput-object p1, p0, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@5
    .line 161
    iput p2, p0, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@7
    .line 162
    iput p3, p0, Landroid/content/SyncStorageEngine$PendingOperation;->syncSource:I

    #@9
    .line 163
    iput-object p4, p0, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@b
    .line 164
    if-eqz p5, :cond_13

    #@d
    new-instance v0, Landroid/os/Bundle;

    #@f
    invoke-direct {v0, p5}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@12
    move-object p5, v0

    #@13
    .end local p5
    :cond_13
    iput-object p5, p0, Landroid/content/SyncStorageEngine$PendingOperation;->extras:Landroid/os/Bundle;

    #@15
    .line 165
    iput-boolean p6, p0, Landroid/content/SyncStorageEngine$PendingOperation;->expedited:Z

    #@17
    .line 166
    const/4 v0, -0x1

    #@18
    iput v0, p0, Landroid/content/SyncStorageEngine$PendingOperation;->authorityId:I

    #@1a
    .line 167
    return-void
.end method

.method constructor <init>(Landroid/content/SyncStorageEngine$PendingOperation;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 169
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 170
    iget-object v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@5
    iput-object v0, p0, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@7
    .line 171
    iget v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@9
    iput v0, p0, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@b
    .line 172
    iget v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->syncSource:I

    #@d
    iput v0, p0, Landroid/content/SyncStorageEngine$PendingOperation;->syncSource:I

    #@f
    .line 173
    iget-object v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@11
    iput-object v0, p0, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@13
    .line 174
    iget-object v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->extras:Landroid/os/Bundle;

    #@15
    iput-object v0, p0, Landroid/content/SyncStorageEngine$PendingOperation;->extras:Landroid/os/Bundle;

    #@17
    .line 175
    iget v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->authorityId:I

    #@19
    iput v0, p0, Landroid/content/SyncStorageEngine$PendingOperation;->authorityId:I

    #@1b
    .line 176
    iget-boolean v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->expedited:Z

    #@1d
    iput-boolean v0, p0, Landroid/content/SyncStorageEngine$PendingOperation;->expedited:Z

    #@1f
    .line 177
    return-void
.end method
