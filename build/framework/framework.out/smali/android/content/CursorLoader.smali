.class public Landroid/content/CursorLoader;
.super Landroid/content/AsyncTaskLoader;
.source "CursorLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/AsyncTaskLoader",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field mCancellationSignal:Landroid/os/CancellationSignal;

.field mCursor:Landroid/database/Cursor;

.field final mObserver:Landroid/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field mProjection:[Ljava/lang/String;

.field mSelection:Ljava/lang/String;

.field mSelectionArgs:[Ljava/lang/String;

.field mSortOrder:Ljava/lang/String;

.field mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 127
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    #@3
    .line 128
    new-instance v0, Landroid/content/Loader$ForceLoadContentObserver;

    #@5
    invoke-direct {v0, p0}, Landroid/content/Loader$ForceLoadContentObserver;-><init>(Landroid/content/Loader;)V

    #@8
    iput-object v0, p0, Landroid/content/CursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    #@a
    .line 129
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "context"
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    .line 139
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    #@3
    .line 140
    new-instance v0, Landroid/content/Loader$ForceLoadContentObserver;

    #@5
    invoke-direct {v0, p0}, Landroid/content/Loader$ForceLoadContentObserver;-><init>(Landroid/content/Loader;)V

    #@8
    iput-object v0, p0, Landroid/content/CursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    #@a
    .line 141
    iput-object p2, p0, Landroid/content/CursorLoader;->mUri:Landroid/net/Uri;

    #@c
    .line 142
    iput-object p3, p0, Landroid/content/CursorLoader;->mProjection:[Ljava/lang/String;

    #@e
    .line 143
    iput-object p4, p0, Landroid/content/CursorLoader;->mSelection:Ljava/lang/String;

    #@10
    .line 144
    iput-object p5, p0, Landroid/content/CursorLoader;->mSelectionArgs:[Ljava/lang/String;

    #@12
    .line 145
    iput-object p6, p0, Landroid/content/CursorLoader;->mSortOrder:Ljava/lang/String;

    #@14
    .line 146
    return-void
.end method


# virtual methods
.method public cancelLoadInBackground()V
    .registers 2

    #@0
    .prologue
    .line 82
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->cancelLoadInBackground()V

    #@3
    .line 84
    monitor-enter p0

    #@4
    .line 85
    :try_start_4
    iget-object v0, p0, Landroid/content/CursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 86
    iget-object v0, p0, Landroid/content/CursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    #@a
    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    #@d
    .line 88
    :cond_d
    monitor-exit p0

    #@e
    .line 89
    return-void

    #@f
    .line 88
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public deliverResult(Landroid/database/Cursor;)V
    .registers 4
    .parameter "cursor"

    #@0
    .prologue
    .line 102
    invoke-virtual {p0}, Landroid/content/CursorLoader;->isReset()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_c

    #@6
    .line 104
    if-eqz p1, :cond_b

    #@8
    .line 105
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    #@b
    .line 119
    :cond_b
    :goto_b
    return-void

    #@c
    .line 109
    :cond_c
    iget-object v0, p0, Landroid/content/CursorLoader;->mCursor:Landroid/database/Cursor;

    #@e
    .line 110
    .local v0, oldCursor:Landroid/database/Cursor;
    iput-object p1, p0, Landroid/content/CursorLoader;->mCursor:Landroid/database/Cursor;

    #@10
    .line 112
    invoke-virtual {p0}, Landroid/content/CursorLoader;->isStarted()Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_19

    #@16
    .line 113
    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    #@19
    .line 116
    :cond_19
    if-eqz v0, :cond_b

    #@1b
    if-eq v0, p1, :cond_b

    #@1d
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_b

    #@23
    .line 117
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@26
    goto :goto_b
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 43
    check-cast p1, Landroid/database/Cursor;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/content/CursorLoader;->deliverResult(Landroid/database/Cursor;)V

    #@5
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "prefix"
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 236
    invoke-super {p0, p1, p2, p3, p4}, Landroid/content/AsyncTaskLoader;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@3
    .line 237
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6
    const-string/jumbo v0, "mUri="

    #@9
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c
    iget-object v0, p0, Landroid/content/CursorLoader;->mUri:Landroid/net/Uri;

    #@e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@11
    .line 238
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14
    const-string/jumbo v0, "mProjection="

    #@17
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a
    .line 239
    iget-object v0, p0, Landroid/content/CursorLoader;->mProjection:[Ljava/lang/String;

    #@1c
    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@23
    .line 240
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@26
    const-string/jumbo v0, "mSelection="

    #@29
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c
    iget-object v0, p0, Landroid/content/CursorLoader;->mSelection:Ljava/lang/String;

    #@2e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@31
    .line 241
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@34
    const-string/jumbo v0, "mSelectionArgs="

    #@37
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a
    .line 242
    iget-object v0, p0, Landroid/content/CursorLoader;->mSelectionArgs:[Ljava/lang/String;

    #@3c
    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@43
    .line 243
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@46
    const-string/jumbo v0, "mSortOrder="

    #@49
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c
    iget-object v0, p0, Landroid/content/CursorLoader;->mSortOrder:Ljava/lang/String;

    #@4e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@51
    .line 244
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@54
    const-string/jumbo v0, "mCursor="

    #@57
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5a
    iget-object v0, p0, Landroid/content/CursorLoader;->mCursor:Landroid/database/Cursor;

    #@5c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@5f
    .line 245
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@62
    const-string/jumbo v0, "mContentChanged="

    #@65
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@68
    iget-boolean v0, p0, Landroid/content/Loader;->mContentChanged:Z

    #@6a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@6d
    .line 246
    return-void
.end method

.method public getProjection()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Landroid/content/CursorLoader;->mProjection:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSelection()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Landroid/content/CursorLoader;->mSelection:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSelectionArgs()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 219
    iget-object v0, p0, Landroid/content/CursorLoader;->mSelectionArgs:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSortOrder()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Landroid/content/CursorLoader;->mSortOrder:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .registers 2

    #@0
    .prologue
    .line 195
    iget-object v0, p0, Landroid/content/CursorLoader;->mUri:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method public loadInBackground()Landroid/database/Cursor;
    .registers 9

    #@0
    .prologue
    .line 58
    monitor-enter p0

    #@1
    .line 59
    :try_start_1
    invoke-virtual {p0}, Landroid/content/CursorLoader;->isLoadInBackgroundCanceled()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_10

    #@7
    .line 60
    new-instance v0, Landroid/os/OperationCanceledException;

    #@9
    invoke-direct {v0}, Landroid/os/OperationCanceledException;-><init>()V

    #@c
    throw v0

    #@d
    .line 63
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0

    #@10
    .line 62
    :cond_10
    :try_start_10
    new-instance v0, Landroid/os/CancellationSignal;

    #@12
    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    #@15
    iput-object v0, p0, Landroid/content/CursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    #@17
    .line 63
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_10 .. :try_end_18} :catchall_d

    #@18
    .line 65
    :try_start_18
    invoke-virtual {p0}, Landroid/content/CursorLoader;->getContext()Landroid/content/Context;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1f
    move-result-object v0

    #@20
    iget-object v1, p0, Landroid/content/CursorLoader;->mUri:Landroid/net/Uri;

    #@22
    iget-object v2, p0, Landroid/content/CursorLoader;->mProjection:[Ljava/lang/String;

    #@24
    iget-object v3, p0, Landroid/content/CursorLoader;->mSelection:Ljava/lang/String;

    #@26
    iget-object v4, p0, Landroid/content/CursorLoader;->mSelectionArgs:[Ljava/lang/String;

    #@28
    iget-object v5, p0, Landroid/content/CursorLoader;->mSortOrder:Ljava/lang/String;

    #@2a
    iget-object v6, p0, Landroid/content/CursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    #@2c
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    #@2f
    move-result-object v7

    #@30
    .line 67
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_3a

    #@32
    .line 69
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@35
    .line 70
    iget-object v0, p0, Landroid/content/CursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    #@37
    invoke-virtual {p0, v7, v0}, Landroid/content/CursorLoader;->registerContentObserver(Landroid/database/Cursor;Landroid/database/ContentObserver;)V
    :try_end_3a
    .catchall {:try_start_18 .. :try_end_3a} :catchall_40

    #@3a
    .line 74
    :cond_3a
    monitor-enter p0

    #@3b
    .line 75
    const/4 v0, 0x0

    #@3c
    :try_start_3c
    iput-object v0, p0, Landroid/content/CursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    #@3e
    .line 76
    monitor-exit p0
    :try_end_3f
    .catchall {:try_start_3c .. :try_end_3f} :catchall_4a

    #@3f
    .line 72
    return-object v7

    #@40
    .line 74
    .end local v7           #cursor:Landroid/database/Cursor;
    :catchall_40
    move-exception v0

    #@41
    monitor-enter p0

    #@42
    .line 75
    const/4 v1, 0x0

    #@43
    :try_start_43
    iput-object v1, p0, Landroid/content/CursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    #@45
    .line 76
    monitor-exit p0
    :try_end_46
    .catchall {:try_start_43 .. :try_end_46} :catchall_47

    #@46
    .line 74
    throw v0

    #@47
    .line 76
    :catchall_47
    move-exception v0

    #@48
    :try_start_48
    monitor-exit p0
    :try_end_49
    .catchall {:try_start_48 .. :try_end_49} :catchall_47

    #@49
    throw v0

    #@4a
    .restart local v7       #cursor:Landroid/database/Cursor;
    :catchall_4a
    move-exception v0

    #@4b
    :try_start_4b
    monitor-exit p0
    :try_end_4c
    .catchall {:try_start_4b .. :try_end_4c} :catchall_4a

    #@4c
    throw v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 43
    invoke-virtual {p0}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public onCanceled(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    #@0
    .prologue
    .line 176
    if-eqz p1, :cond_b

    #@2
    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_b

    #@8
    .line 177
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    #@b
    .line 179
    :cond_b
    return-void
.end method

.method public bridge synthetic onCanceled(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 43
    check-cast p1, Landroid/database/Cursor;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/content/CursorLoader;->onCanceled(Landroid/database/Cursor;)V

    #@5
    return-void
.end method

.method protected onReset()V
    .registers 2

    #@0
    .prologue
    .line 183
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onReset()V

    #@3
    .line 186
    invoke-virtual {p0}, Landroid/content/CursorLoader;->onStopLoading()V

    #@6
    .line 188
    iget-object v0, p0, Landroid/content/CursorLoader;->mCursor:Landroid/database/Cursor;

    #@8
    if-eqz v0, :cond_17

    #@a
    iget-object v0, p0, Landroid/content/CursorLoader;->mCursor:Landroid/database/Cursor;

    #@c
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_17

    #@12
    .line 189
    iget-object v0, p0, Landroid/content/CursorLoader;->mCursor:Landroid/database/Cursor;

    #@14
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@17
    .line 191
    :cond_17
    const/4 v0, 0x0

    #@18
    iput-object v0, p0, Landroid/content/CursorLoader;->mCursor:Landroid/database/Cursor;

    #@1a
    .line 192
    return-void
.end method

.method protected onStartLoading()V
    .registers 2

    #@0
    .prologue
    .line 157
    iget-object v0, p0, Landroid/content/CursorLoader;->mCursor:Landroid/database/Cursor;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 158
    iget-object v0, p0, Landroid/content/CursorLoader;->mCursor:Landroid/database/Cursor;

    #@6
    invoke-virtual {p0, v0}, Landroid/content/CursorLoader;->deliverResult(Landroid/database/Cursor;)V

    #@9
    .line 160
    :cond_9
    invoke-virtual {p0}, Landroid/content/CursorLoader;->takeContentChanged()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_13

    #@f
    iget-object v0, p0, Landroid/content/CursorLoader;->mCursor:Landroid/database/Cursor;

    #@11
    if-nez v0, :cond_16

    #@13
    .line 161
    :cond_13
    invoke-virtual {p0}, Landroid/content/CursorLoader;->forceLoad()V

    #@16
    .line 163
    :cond_16
    return-void
.end method

.method protected onStopLoading()V
    .registers 1

    #@0
    .prologue
    .line 171
    invoke-virtual {p0}, Landroid/content/CursorLoader;->cancelLoad()Z

    #@3
    .line 172
    return-void
.end method

.method registerContentObserver(Landroid/database/Cursor;Landroid/database/ContentObserver;)V
    .registers 4
    .parameter "cursor"
    .parameter "observer"

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Landroid/content/CursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    #@2
    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    #@5
    .line 97
    return-void
.end method

.method public setProjection([Ljava/lang/String;)V
    .registers 2
    .parameter "projection"

    #@0
    .prologue
    .line 207
    iput-object p1, p0, Landroid/content/CursorLoader;->mProjection:[Ljava/lang/String;

    #@2
    .line 208
    return-void
.end method

.method public setSelection(Ljava/lang/String;)V
    .registers 2
    .parameter "selection"

    #@0
    .prologue
    .line 215
    iput-object p1, p0, Landroid/content/CursorLoader;->mSelection:Ljava/lang/String;

    #@2
    .line 216
    return-void
.end method

.method public setSelectionArgs([Ljava/lang/String;)V
    .registers 2
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 223
    iput-object p1, p0, Landroid/content/CursorLoader;->mSelectionArgs:[Ljava/lang/String;

    #@2
    .line 224
    return-void
.end method

.method public setSortOrder(Ljava/lang/String;)V
    .registers 2
    .parameter "sortOrder"

    #@0
    .prologue
    .line 231
    iput-object p1, p0, Landroid/content/CursorLoader;->mSortOrder:Ljava/lang/String;

    #@2
    .line 232
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .registers 2
    .parameter "uri"

    #@0
    .prologue
    .line 199
    iput-object p1, p0, Landroid/content/CursorLoader;->mUri:Landroid/net/Uri;

    #@2
    .line 200
    return-void
.end method
