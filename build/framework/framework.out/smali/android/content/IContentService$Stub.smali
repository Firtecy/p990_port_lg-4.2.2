.class public abstract Landroid/content/IContentService$Stub;
.super Landroid/os/Binder;
.source "IContentService.java"

# interfaces
.implements Landroid/content/IContentService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/IContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/IContentService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.content.IContentService"

.field static final TRANSACTION_addPeriodicSync:I = 0x9

.field static final TRANSACTION_addStatusChangeListener:I = 0x14

.field static final TRANSACTION_cancelSync:I = 0x5

.field static final TRANSACTION_getCurrentSyncs:I = 0x10

.field static final TRANSACTION_getIsSyncable:I = 0xb

.field static final TRANSACTION_getMasterSyncAutomatically:I = 0xe

.field static final TRANSACTION_getPeriodicSyncs:I = 0x8

.field static final TRANSACTION_getSyncAdapterTypes:I = 0x11

.field static final TRANSACTION_getSyncAutomatically:I = 0x6

.field static final TRANSACTION_getSyncStatus:I = 0x12

.field static final TRANSACTION_isSyncActive:I = 0xf

.field static final TRANSACTION_isSyncPending:I = 0x13

.field static final TRANSACTION_notifyChange:I = 0x3

.field static final TRANSACTION_registerContentObserver:I = 0x2

.field static final TRANSACTION_removePeriodicSync:I = 0xa

.field static final TRANSACTION_removeStatusChangeListener:I = 0x15

.field static final TRANSACTION_requestSync:I = 0x4

.field static final TRANSACTION_setIsSyncable:I = 0xc

.field static final TRANSACTION_setMasterSyncAutomatically:I = 0xd

.field static final TRANSACTION_setSyncAutomatically:I = 0x7

.field static final TRANSACTION_unregisterContentObserver:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.content.IContentService"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/content/IContentService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/content/IContentService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.content.IContentService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/content/IContentService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/content/IContentService;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/content/IContentService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/content/IContentService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 21
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 41
    sparse-switch p1, :sswitch_data_3ac

    #@3
    .line 384
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v1

    #@7
    :goto_7
    return v1

    #@8
    .line 45
    :sswitch_8
    const-string v1, "android.content.IContentService"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 46
    const/4 v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 50
    :sswitch_11
    const-string v1, "android.content.IContentService"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 52
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v1}, Landroid/database/IContentObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/database/IContentObserver;

    #@1f
    move-result-object v2

    #@20
    .line 53
    .local v2, _arg0:Landroid/database/IContentObserver;
    move-object/from16 v0, p0

    #@22
    invoke-virtual {v0, v2}, Landroid/content/IContentService$Stub;->unregisterContentObserver(Landroid/database/IContentObserver;)V

    #@25
    .line 54
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@28
    .line 55
    const/4 v1, 0x1

    #@29
    goto :goto_7

    #@2a
    .line 59
    .end local v2           #_arg0:Landroid/database/IContentObserver;
    :sswitch_2a
    const-string v1, "android.content.IContentService"

    #@2c
    move-object/from16 v0, p2

    #@2e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 61
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_5e

    #@37
    .line 62
    sget-object v1, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@39
    move-object/from16 v0, p2

    #@3b
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3e
    move-result-object v2

    #@3f
    check-cast v2, Landroid/net/Uri;

    #@41
    .line 68
    .local v2, _arg0:Landroid/net/Uri;
    :goto_41
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v1

    #@45
    if-eqz v1, :cond_60

    #@47
    const/4 v3, 0x1

    #@48
    .line 70
    .local v3, _arg1:Z
    :goto_48
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4b
    move-result-object v1

    #@4c
    invoke-static {v1}, Landroid/database/IContentObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/database/IContentObserver;

    #@4f
    move-result-object v4

    #@50
    .line 72
    .local v4, _arg2:Landroid/database/IContentObserver;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@53
    move-result v5

    #@54
    .line 73
    .local v5, _arg3:I
    move-object/from16 v0, p0

    #@56
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/content/IContentService$Stub;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/IContentObserver;I)V

    #@59
    .line 74
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c
    .line 75
    const/4 v1, 0x1

    #@5d
    goto :goto_7

    #@5e
    .line 65
    .end local v2           #_arg0:Landroid/net/Uri;
    .end local v3           #_arg1:Z
    .end local v4           #_arg2:Landroid/database/IContentObserver;
    .end local v5           #_arg3:I
    :cond_5e
    const/4 v2, 0x0

    #@5f
    .restart local v2       #_arg0:Landroid/net/Uri;
    goto :goto_41

    #@60
    .line 68
    :cond_60
    const/4 v3, 0x0

    #@61
    goto :goto_48

    #@62
    .line 79
    .end local v2           #_arg0:Landroid/net/Uri;
    :sswitch_62
    const-string v1, "android.content.IContentService"

    #@64
    move-object/from16 v0, p2

    #@66
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@69
    .line 81
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6c
    move-result v1

    #@6d
    if-eqz v1, :cond_9e

    #@6f
    .line 82
    sget-object v1, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@71
    move-object/from16 v0, p2

    #@73
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@76
    move-result-object v2

    #@77
    check-cast v2, Landroid/net/Uri;

    #@79
    .line 88
    .restart local v2       #_arg0:Landroid/net/Uri;
    :goto_79
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7c
    move-result-object v1

    #@7d
    invoke-static {v1}, Landroid/database/IContentObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/database/IContentObserver;

    #@80
    move-result-object v3

    #@81
    .line 90
    .local v3, _arg1:Landroid/database/IContentObserver;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@84
    move-result v1

    #@85
    if-eqz v1, :cond_a0

    #@87
    const/4 v4, 0x1

    #@88
    .line 92
    .local v4, _arg2:Z
    :goto_88
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8b
    move-result v1

    #@8c
    if-eqz v1, :cond_a2

    #@8e
    const/4 v5, 0x1

    #@8f
    .line 94
    .local v5, _arg3:Z
    :goto_8f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@92
    move-result v6

    #@93
    .local v6, _arg4:I
    move-object/from16 v1, p0

    #@95
    .line 95
    invoke-virtual/range {v1 .. v6}, Landroid/content/IContentService$Stub;->notifyChange(Landroid/net/Uri;Landroid/database/IContentObserver;ZZI)V

    #@98
    .line 96
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9b
    .line 97
    const/4 v1, 0x1

    #@9c
    goto/16 :goto_7

    #@9e
    .line 85
    .end local v2           #_arg0:Landroid/net/Uri;
    .end local v3           #_arg1:Landroid/database/IContentObserver;
    .end local v4           #_arg2:Z
    .end local v5           #_arg3:Z
    .end local v6           #_arg4:I
    :cond_9e
    const/4 v2, 0x0

    #@9f
    .restart local v2       #_arg0:Landroid/net/Uri;
    goto :goto_79

    #@a0
    .line 90
    .restart local v3       #_arg1:Landroid/database/IContentObserver;
    :cond_a0
    const/4 v4, 0x0

    #@a1
    goto :goto_88

    #@a2
    .line 92
    .restart local v4       #_arg2:Z
    :cond_a2
    const/4 v5, 0x0

    #@a3
    goto :goto_8f

    #@a4
    .line 101
    .end local v2           #_arg0:Landroid/net/Uri;
    .end local v3           #_arg1:Landroid/database/IContentObserver;
    .end local v4           #_arg2:Z
    :sswitch_a4
    const-string v1, "android.content.IContentService"

    #@a6
    move-object/from16 v0, p2

    #@a8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ab
    .line 103
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ae
    move-result v1

    #@af
    if-eqz v1, :cond_da

    #@b1
    .line 104
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b3
    move-object/from16 v0, p2

    #@b5
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b8
    move-result-object v2

    #@b9
    check-cast v2, Landroid/accounts/Account;

    #@bb
    .line 110
    .local v2, _arg0:Landroid/accounts/Account;
    :goto_bb
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@be
    move-result-object v3

    #@bf
    .line 112
    .local v3, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c2
    move-result v1

    #@c3
    if-eqz v1, :cond_dc

    #@c5
    .line 113
    sget-object v1, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c7
    move-object/from16 v0, p2

    #@c9
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@cc
    move-result-object v4

    #@cd
    check-cast v4, Landroid/os/Bundle;

    #@cf
    .line 118
    .local v4, _arg2:Landroid/os/Bundle;
    :goto_cf
    move-object/from16 v0, p0

    #@d1
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/IContentService$Stub;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    #@d4
    .line 119
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d7
    .line 120
    const/4 v1, 0x1

    #@d8
    goto/16 :goto_7

    #@da
    .line 107
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Landroid/os/Bundle;
    :cond_da
    const/4 v2, 0x0

    #@db
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_bb

    #@dc
    .line 116
    .restart local v3       #_arg1:Ljava/lang/String;
    :cond_dc
    const/4 v4, 0x0

    #@dd
    .restart local v4       #_arg2:Landroid/os/Bundle;
    goto :goto_cf

    #@de
    .line 124
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Landroid/os/Bundle;
    :sswitch_de
    const-string v1, "android.content.IContentService"

    #@e0
    move-object/from16 v0, p2

    #@e2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e5
    .line 126
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e8
    move-result v1

    #@e9
    if-eqz v1, :cond_104

    #@eb
    .line 127
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ed
    move-object/from16 v0, p2

    #@ef
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f2
    move-result-object v2

    #@f3
    check-cast v2, Landroid/accounts/Account;

    #@f5
    .line 133
    .restart local v2       #_arg0:Landroid/accounts/Account;
    :goto_f5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f8
    move-result-object v3

    #@f9
    .line 134
    .restart local v3       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@fb
    invoke-virtual {v0, v2, v3}, Landroid/content/IContentService$Stub;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    #@fe
    .line 135
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@101
    .line 136
    const/4 v1, 0x1

    #@102
    goto/16 :goto_7

    #@104
    .line 130
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    :cond_104
    const/4 v2, 0x0

    #@105
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_f5

    #@106
    .line 140
    .end local v2           #_arg0:Landroid/accounts/Account;
    :sswitch_106
    const-string v1, "android.content.IContentService"

    #@108
    move-object/from16 v0, p2

    #@10a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10d
    .line 142
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@110
    move-result v1

    #@111
    if-eqz v1, :cond_135

    #@113
    .line 143
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@115
    move-object/from16 v0, p2

    #@117
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@11a
    move-result-object v2

    #@11b
    check-cast v2, Landroid/accounts/Account;

    #@11d
    .line 149
    .restart local v2       #_arg0:Landroid/accounts/Account;
    :goto_11d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@120
    move-result-object v3

    #@121
    .line 150
    .restart local v3       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@123
    invoke-virtual {v0, v2, v3}, Landroid/content/IContentService$Stub;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    #@126
    move-result v13

    #@127
    .line 151
    .local v13, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@12a
    .line 152
    if-eqz v13, :cond_137

    #@12c
    const/4 v1, 0x1

    #@12d
    :goto_12d
    move-object/from16 v0, p3

    #@12f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@132
    .line 153
    const/4 v1, 0x1

    #@133
    goto/16 :goto_7

    #@135
    .line 146
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v13           #_result:Z
    :cond_135
    const/4 v2, 0x0

    #@136
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_11d

    #@137
    .line 152
    .restart local v3       #_arg1:Ljava/lang/String;
    .restart local v13       #_result:Z
    :cond_137
    const/4 v1, 0x0

    #@138
    goto :goto_12d

    #@139
    .line 157
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v13           #_result:Z
    :sswitch_139
    const-string v1, "android.content.IContentService"

    #@13b
    move-object/from16 v0, p2

    #@13d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@140
    .line 159
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@143
    move-result v1

    #@144
    if-eqz v1, :cond_166

    #@146
    .line 160
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@148
    move-object/from16 v0, p2

    #@14a
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@14d
    move-result-object v2

    #@14e
    check-cast v2, Landroid/accounts/Account;

    #@150
    .line 166
    .restart local v2       #_arg0:Landroid/accounts/Account;
    :goto_150
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@153
    move-result-object v3

    #@154
    .line 168
    .restart local v3       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@157
    move-result v1

    #@158
    if-eqz v1, :cond_168

    #@15a
    const/4 v4, 0x1

    #@15b
    .line 169
    .local v4, _arg2:Z
    :goto_15b
    move-object/from16 v0, p0

    #@15d
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/IContentService$Stub;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    #@160
    .line 170
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@163
    .line 171
    const/4 v1, 0x1

    #@164
    goto/16 :goto_7

    #@166
    .line 163
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Z
    :cond_166
    const/4 v2, 0x0

    #@167
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_150

    #@168
    .line 168
    .restart local v3       #_arg1:Ljava/lang/String;
    :cond_168
    const/4 v4, 0x0

    #@169
    goto :goto_15b

    #@16a
    .line 175
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    :sswitch_16a
    const-string v1, "android.content.IContentService"

    #@16c
    move-object/from16 v0, p2

    #@16e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@171
    .line 177
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@174
    move-result v1

    #@175
    if-eqz v1, :cond_196

    #@177
    .line 178
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@179
    move-object/from16 v0, p2

    #@17b
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@17e
    move-result-object v2

    #@17f
    check-cast v2, Landroid/accounts/Account;

    #@181
    .line 184
    .restart local v2       #_arg0:Landroid/accounts/Account;
    :goto_181
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@184
    move-result-object v3

    #@185
    .line 185
    .restart local v3       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@187
    invoke-virtual {v0, v2, v3}, Landroid/content/IContentService$Stub;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    #@18a
    move-result-object v14

    #@18b
    .line 186
    .local v14, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/PeriodicSync;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@18e
    .line 187
    move-object/from16 v0, p3

    #@190
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@193
    .line 188
    const/4 v1, 0x1

    #@194
    goto/16 :goto_7

    #@196
    .line 181
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v14           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/PeriodicSync;>;"
    :cond_196
    const/4 v2, 0x0

    #@197
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_181

    #@198
    .line 192
    .end local v2           #_arg0:Landroid/accounts/Account;
    :sswitch_198
    const-string v1, "android.content.IContentService"

    #@19a
    move-object/from16 v0, p2

    #@19c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19f
    .line 194
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1a2
    move-result v1

    #@1a3
    if-eqz v1, :cond_1d5

    #@1a5
    .line 195
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a7
    move-object/from16 v0, p2

    #@1a9
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1ac
    move-result-object v2

    #@1ad
    check-cast v2, Landroid/accounts/Account;

    #@1af
    .line 201
    .restart local v2       #_arg0:Landroid/accounts/Account;
    :goto_1af
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b2
    move-result-object v3

    #@1b3
    .line 203
    .restart local v3       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b6
    move-result v1

    #@1b7
    if-eqz v1, :cond_1d7

    #@1b9
    .line 204
    sget-object v1, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1bb
    move-object/from16 v0, p2

    #@1bd
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1c0
    move-result-object v4

    #@1c1
    check-cast v4, Landroid/os/Bundle;

    #@1c3
    .line 210
    .local v4, _arg2:Landroid/os/Bundle;
    :goto_1c3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@1c6
    move-result-wide v11

    #@1c7
    .local v11, _arg3:J
    move-object/from16 v7, p0

    #@1c9
    move-object v8, v2

    #@1ca
    move-object v9, v3

    #@1cb
    move-object v10, v4

    #@1cc
    .line 211
    invoke-virtual/range {v7 .. v12}, Landroid/content/IContentService$Stub;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    #@1cf
    .line 212
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d2
    .line 213
    const/4 v1, 0x1

    #@1d3
    goto/16 :goto_7

    #@1d5
    .line 198
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Landroid/os/Bundle;
    .end local v11           #_arg3:J
    :cond_1d5
    const/4 v2, 0x0

    #@1d6
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_1af

    #@1d7
    .line 207
    .restart local v3       #_arg1:Ljava/lang/String;
    :cond_1d7
    const/4 v4, 0x0

    #@1d8
    .restart local v4       #_arg2:Landroid/os/Bundle;
    goto :goto_1c3

    #@1d9
    .line 217
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Landroid/os/Bundle;
    :sswitch_1d9
    const-string v1, "android.content.IContentService"

    #@1db
    move-object/from16 v0, p2

    #@1dd
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e0
    .line 219
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1e3
    move-result v1

    #@1e4
    if-eqz v1, :cond_20f

    #@1e6
    .line 220
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1e8
    move-object/from16 v0, p2

    #@1ea
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1ed
    move-result-object v2

    #@1ee
    check-cast v2, Landroid/accounts/Account;

    #@1f0
    .line 226
    .restart local v2       #_arg0:Landroid/accounts/Account;
    :goto_1f0
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1f3
    move-result-object v3

    #@1f4
    .line 228
    .restart local v3       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1f7
    move-result v1

    #@1f8
    if-eqz v1, :cond_211

    #@1fa
    .line 229
    sget-object v1, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1fc
    move-object/from16 v0, p2

    #@1fe
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@201
    move-result-object v4

    #@202
    check-cast v4, Landroid/os/Bundle;

    #@204
    .line 234
    .restart local v4       #_arg2:Landroid/os/Bundle;
    :goto_204
    move-object/from16 v0, p0

    #@206
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/IContentService$Stub;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    #@209
    .line 235
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@20c
    .line 236
    const/4 v1, 0x1

    #@20d
    goto/16 :goto_7

    #@20f
    .line 223
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Landroid/os/Bundle;
    :cond_20f
    const/4 v2, 0x0

    #@210
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_1f0

    #@211
    .line 232
    .restart local v3       #_arg1:Ljava/lang/String;
    :cond_211
    const/4 v4, 0x0

    #@212
    .restart local v4       #_arg2:Landroid/os/Bundle;
    goto :goto_204

    #@213
    .line 240
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Landroid/os/Bundle;
    :sswitch_213
    const-string v1, "android.content.IContentService"

    #@215
    move-object/from16 v0, p2

    #@217
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@21a
    .line 242
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@21d
    move-result v1

    #@21e
    if-eqz v1, :cond_23f

    #@220
    .line 243
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@222
    move-object/from16 v0, p2

    #@224
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@227
    move-result-object v2

    #@228
    check-cast v2, Landroid/accounts/Account;

    #@22a
    .line 249
    .restart local v2       #_arg0:Landroid/accounts/Account;
    :goto_22a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@22d
    move-result-object v3

    #@22e
    .line 250
    .restart local v3       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@230
    invoke-virtual {v0, v2, v3}, Landroid/content/IContentService$Stub;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    #@233
    move-result v13

    #@234
    .line 251
    .local v13, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@237
    .line 252
    move-object/from16 v0, p3

    #@239
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@23c
    .line 253
    const/4 v1, 0x1

    #@23d
    goto/16 :goto_7

    #@23f
    .line 246
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v13           #_result:I
    :cond_23f
    const/4 v2, 0x0

    #@240
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_22a

    #@241
    .line 257
    .end local v2           #_arg0:Landroid/accounts/Account;
    :sswitch_241
    const-string v1, "android.content.IContentService"

    #@243
    move-object/from16 v0, p2

    #@245
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@248
    .line 259
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@24b
    move-result v1

    #@24c
    if-eqz v1, :cond_26b

    #@24e
    .line 260
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@250
    move-object/from16 v0, p2

    #@252
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@255
    move-result-object v2

    #@256
    check-cast v2, Landroid/accounts/Account;

    #@258
    .line 266
    .restart local v2       #_arg0:Landroid/accounts/Account;
    :goto_258
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@25b
    move-result-object v3

    #@25c
    .line 268
    .restart local v3       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@25f
    move-result v4

    #@260
    .line 269
    .local v4, _arg2:I
    move-object/from16 v0, p0

    #@262
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/IContentService$Stub;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    #@265
    .line 270
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@268
    .line 271
    const/4 v1, 0x1

    #@269
    goto/16 :goto_7

    #@26b
    .line 263
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:I
    :cond_26b
    const/4 v2, 0x0

    #@26c
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_258

    #@26d
    .line 275
    .end local v2           #_arg0:Landroid/accounts/Account;
    :sswitch_26d
    const-string v1, "android.content.IContentService"

    #@26f
    move-object/from16 v0, p2

    #@271
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@274
    .line 277
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@277
    move-result v1

    #@278
    if-eqz v1, :cond_286

    #@27a
    const/4 v2, 0x1

    #@27b
    .line 278
    .local v2, _arg0:Z
    :goto_27b
    move-object/from16 v0, p0

    #@27d
    invoke-virtual {v0, v2}, Landroid/content/IContentService$Stub;->setMasterSyncAutomatically(Z)V

    #@280
    .line 279
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@283
    .line 280
    const/4 v1, 0x1

    #@284
    goto/16 :goto_7

    #@286
    .line 277
    .end local v2           #_arg0:Z
    :cond_286
    const/4 v2, 0x0

    #@287
    goto :goto_27b

    #@288
    .line 284
    :sswitch_288
    const-string v1, "android.content.IContentService"

    #@28a
    move-object/from16 v0, p2

    #@28c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28f
    .line 285
    invoke-virtual/range {p0 .. p0}, Landroid/content/IContentService$Stub;->getMasterSyncAutomatically()Z

    #@292
    move-result v13

    #@293
    .line 286
    .local v13, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@296
    .line 287
    if-eqz v13, :cond_2a1

    #@298
    const/4 v1, 0x1

    #@299
    :goto_299
    move-object/from16 v0, p3

    #@29b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@29e
    .line 288
    const/4 v1, 0x1

    #@29f
    goto/16 :goto_7

    #@2a1
    .line 287
    :cond_2a1
    const/4 v1, 0x0

    #@2a2
    goto :goto_299

    #@2a3
    .line 292
    .end local v13           #_result:Z
    :sswitch_2a3
    const-string v1, "android.content.IContentService"

    #@2a5
    move-object/from16 v0, p2

    #@2a7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2aa
    .line 294
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2ad
    move-result v1

    #@2ae
    if-eqz v1, :cond_2d2

    #@2b0
    .line 295
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2b2
    move-object/from16 v0, p2

    #@2b4
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2b7
    move-result-object v2

    #@2b8
    check-cast v2, Landroid/accounts/Account;

    #@2ba
    .line 301
    .local v2, _arg0:Landroid/accounts/Account;
    :goto_2ba
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2bd
    move-result-object v3

    #@2be
    .line 302
    .restart local v3       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2c0
    invoke-virtual {v0, v2, v3}, Landroid/content/IContentService$Stub;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    #@2c3
    move-result v13

    #@2c4
    .line 303
    .restart local v13       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c7
    .line 304
    if-eqz v13, :cond_2d4

    #@2c9
    const/4 v1, 0x1

    #@2ca
    :goto_2ca
    move-object/from16 v0, p3

    #@2cc
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2cf
    .line 305
    const/4 v1, 0x1

    #@2d0
    goto/16 :goto_7

    #@2d2
    .line 298
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v13           #_result:Z
    :cond_2d2
    const/4 v2, 0x0

    #@2d3
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_2ba

    #@2d4
    .line 304
    .restart local v3       #_arg1:Ljava/lang/String;
    .restart local v13       #_result:Z
    :cond_2d4
    const/4 v1, 0x0

    #@2d5
    goto :goto_2ca

    #@2d6
    .line 309
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v13           #_result:Z
    :sswitch_2d6
    const-string v1, "android.content.IContentService"

    #@2d8
    move-object/from16 v0, p2

    #@2da
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2dd
    .line 310
    invoke-virtual/range {p0 .. p0}, Landroid/content/IContentService$Stub;->getCurrentSyncs()Ljava/util/List;

    #@2e0
    move-result-object v15

    #@2e1
    .line 311
    .local v15, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/SyncInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e4
    .line 312
    move-object/from16 v0, p3

    #@2e6
    invoke-virtual {v0, v15}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@2e9
    .line 313
    const/4 v1, 0x1

    #@2ea
    goto/16 :goto_7

    #@2ec
    .line 317
    .end local v15           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/SyncInfo;>;"
    :sswitch_2ec
    const-string v1, "android.content.IContentService"

    #@2ee
    move-object/from16 v0, p2

    #@2f0
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f3
    .line 318
    invoke-virtual/range {p0 .. p0}, Landroid/content/IContentService$Stub;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    #@2f6
    move-result-object v13

    #@2f7
    .line 319
    .local v13, _result:[Landroid/content/SyncAdapterType;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2fa
    .line 320
    const/4 v1, 0x1

    #@2fb
    move-object/from16 v0, p3

    #@2fd
    invoke-virtual {v0, v13, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@300
    .line 321
    const/4 v1, 0x1

    #@301
    goto/16 :goto_7

    #@303
    .line 325
    .end local v13           #_result:[Landroid/content/SyncAdapterType;
    :sswitch_303
    const-string v1, "android.content.IContentService"

    #@305
    move-object/from16 v0, p2

    #@307
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30a
    .line 327
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@30d
    move-result v1

    #@30e
    if-eqz v1, :cond_338

    #@310
    .line 328
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@312
    move-object/from16 v0, p2

    #@314
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@317
    move-result-object v2

    #@318
    check-cast v2, Landroid/accounts/Account;

    #@31a
    .line 334
    .restart local v2       #_arg0:Landroid/accounts/Account;
    :goto_31a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@31d
    move-result-object v3

    #@31e
    .line 335
    .restart local v3       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@320
    invoke-virtual {v0, v2, v3}, Landroid/content/IContentService$Stub;->getSyncStatus(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/SyncStatusInfo;

    #@323
    move-result-object v13

    #@324
    .line 336
    .local v13, _result:Landroid/content/SyncStatusInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@327
    .line 337
    if-eqz v13, :cond_33a

    #@329
    .line 338
    const/4 v1, 0x1

    #@32a
    move-object/from16 v0, p3

    #@32c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@32f
    .line 339
    const/4 v1, 0x1

    #@330
    move-object/from16 v0, p3

    #@332
    invoke-virtual {v13, v0, v1}, Landroid/content/SyncStatusInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@335
    .line 344
    :goto_335
    const/4 v1, 0x1

    #@336
    goto/16 :goto_7

    #@338
    .line 331
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v13           #_result:Landroid/content/SyncStatusInfo;
    :cond_338
    const/4 v2, 0x0

    #@339
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_31a

    #@33a
    .line 342
    .restart local v3       #_arg1:Ljava/lang/String;
    .restart local v13       #_result:Landroid/content/SyncStatusInfo;
    :cond_33a
    const/4 v1, 0x0

    #@33b
    move-object/from16 v0, p3

    #@33d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@340
    goto :goto_335

    #@341
    .line 348
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v13           #_result:Landroid/content/SyncStatusInfo;
    :sswitch_341
    const-string v1, "android.content.IContentService"

    #@343
    move-object/from16 v0, p2

    #@345
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@348
    .line 350
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@34b
    move-result v1

    #@34c
    if-eqz v1, :cond_370

    #@34e
    .line 351
    sget-object v1, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@350
    move-object/from16 v0, p2

    #@352
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@355
    move-result-object v2

    #@356
    check-cast v2, Landroid/accounts/Account;

    #@358
    .line 357
    .restart local v2       #_arg0:Landroid/accounts/Account;
    :goto_358
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35b
    move-result-object v3

    #@35c
    .line 358
    .restart local v3       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@35e
    invoke-virtual {v0, v2, v3}, Landroid/content/IContentService$Stub;->isSyncPending(Landroid/accounts/Account;Ljava/lang/String;)Z

    #@361
    move-result v13

    #@362
    .line 359
    .local v13, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@365
    .line 360
    if-eqz v13, :cond_372

    #@367
    const/4 v1, 0x1

    #@368
    :goto_368
    move-object/from16 v0, p3

    #@36a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@36d
    .line 361
    const/4 v1, 0x1

    #@36e
    goto/16 :goto_7

    #@370
    .line 354
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v13           #_result:Z
    :cond_370
    const/4 v2, 0x0

    #@371
    .restart local v2       #_arg0:Landroid/accounts/Account;
    goto :goto_358

    #@372
    .line 360
    .restart local v3       #_arg1:Ljava/lang/String;
    .restart local v13       #_result:Z
    :cond_372
    const/4 v1, 0x0

    #@373
    goto :goto_368

    #@374
    .line 365
    .end local v2           #_arg0:Landroid/accounts/Account;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v13           #_result:Z
    :sswitch_374
    const-string v1, "android.content.IContentService"

    #@376
    move-object/from16 v0, p2

    #@378
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@37b
    .line 367
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@37e
    move-result v2

    #@37f
    .line 369
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@382
    move-result-object v1

    #@383
    invoke-static {v1}, Landroid/content/ISyncStatusObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/ISyncStatusObserver;

    #@386
    move-result-object v3

    #@387
    .line 370
    .local v3, _arg1:Landroid/content/ISyncStatusObserver;
    move-object/from16 v0, p0

    #@389
    invoke-virtual {v0, v2, v3}, Landroid/content/IContentService$Stub;->addStatusChangeListener(ILandroid/content/ISyncStatusObserver;)V

    #@38c
    .line 371
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@38f
    .line 372
    const/4 v1, 0x1

    #@390
    goto/16 :goto_7

    #@392
    .line 376
    .end local v2           #_arg0:I
    .end local v3           #_arg1:Landroid/content/ISyncStatusObserver;
    :sswitch_392
    const-string v1, "android.content.IContentService"

    #@394
    move-object/from16 v0, p2

    #@396
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@399
    .line 378
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@39c
    move-result-object v1

    #@39d
    invoke-static {v1}, Landroid/content/ISyncStatusObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/ISyncStatusObserver;

    #@3a0
    move-result-object v2

    #@3a1
    .line 379
    .local v2, _arg0:Landroid/content/ISyncStatusObserver;
    move-object/from16 v0, p0

    #@3a3
    invoke-virtual {v0, v2}, Landroid/content/IContentService$Stub;->removeStatusChangeListener(Landroid/content/ISyncStatusObserver;)V

    #@3a6
    .line 380
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a9
    .line 381
    const/4 v1, 0x1

    #@3aa
    goto/16 :goto_7

    #@3ac
    .line 41
    :sswitch_data_3ac
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_2a
        0x3 -> :sswitch_62
        0x4 -> :sswitch_a4
        0x5 -> :sswitch_de
        0x6 -> :sswitch_106
        0x7 -> :sswitch_139
        0x8 -> :sswitch_16a
        0x9 -> :sswitch_198
        0xa -> :sswitch_1d9
        0xb -> :sswitch_213
        0xc -> :sswitch_241
        0xd -> :sswitch_26d
        0xe -> :sswitch_288
        0xf -> :sswitch_2a3
        0x10 -> :sswitch_2d6
        0x11 -> :sswitch_2ec
        0x12 -> :sswitch_303
        0x13 -> :sswitch_341
        0x14 -> :sswitch_374
        0x15 -> :sswitch_392
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
