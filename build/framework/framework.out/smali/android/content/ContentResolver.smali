.class public abstract Landroid/content/ContentResolver;
.super Ljava/lang/Object;
.source "ContentResolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/ContentResolver$ParcelFileDescriptorInner;,
        Landroid/content/ContentResolver$CursorWrapperInner;,
        Landroid/content/ContentResolver$OpenResourceIdResult;
    }
.end annotation


# static fields
.field public static final CONTENT_SERVICE_NAME:Ljava/lang/String; = "content"

.field public static final CURSOR_DIR_BASE_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir"

.field public static final CURSOR_ITEM_BASE_TYPE:Ljava/lang/String; = "vnd.android.cursor.item"

.field public static final SCHEME_ANDROID_RESOURCE:Ljava/lang/String; = "android.resource"

.field public static final SCHEME_CONTENT:Ljava/lang/String; = "content"

.field public static final SCHEME_FILE:Ljava/lang/String; = "file"

.field private static final SLOW_THRESHOLD_MILLIS:I = 0x1f4

.field public static final SYNC_ERROR_AUTHENTICATION:I = 0x2

.field public static final SYNC_ERROR_CONFLICT:I = 0x5

.field public static final SYNC_ERROR_INTERNAL:I = 0x8

.field public static final SYNC_ERROR_IO:I = 0x3

.field public static final SYNC_ERROR_PARSE:I = 0x4

.field public static final SYNC_ERROR_SYNC_ALREADY_IN_PROGRESS:I = 0x1

.field public static final SYNC_ERROR_TOO_MANY_DELETIONS:I = 0x6

.field public static final SYNC_ERROR_TOO_MANY_RETRIES:I = 0x7

.field public static final SYNC_EXTRAS_ACCOUNT:Ljava/lang/String; = "account"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SYNC_EXTRAS_DISCARD_LOCAL_DELETIONS:Ljava/lang/String; = "discard_deletions"

.field public static final SYNC_EXTRAS_DO_NOT_RETRY:Ljava/lang/String; = "do_not_retry"

.field public static final SYNC_EXTRAS_EXPEDITED:Ljava/lang/String; = "expedited"

.field public static final SYNC_EXTRAS_FORCE:Ljava/lang/String; = "force"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SYNC_EXTRAS_IGNORE_BACKOFF:Ljava/lang/String; = "ignore_backoff"

.field public static final SYNC_EXTRAS_IGNORE_SETTINGS:Ljava/lang/String; = "ignore_settings"

.field public static final SYNC_EXTRAS_INITIALIZE:Ljava/lang/String; = "initialize"

.field public static final SYNC_EXTRAS_MANUAL:Ljava/lang/String; = "force"

.field public static final SYNC_EXTRAS_OVERRIDE_TOO_MANY_DELETIONS:Ljava/lang/String; = "deletions_override"

.field public static final SYNC_EXTRAS_UPLOAD:Ljava/lang/String; = "upload"

.field public static final SYNC_OBSERVER_TYPE_ACTIVE:I = 0x4

.field public static final SYNC_OBSERVER_TYPE_ALL:I = 0x7fffffff

.field public static final SYNC_OBSERVER_TYPE_PENDING:I = 0x2

.field public static final SYNC_OBSERVER_TYPE_SETTINGS:I = 0x1

.field public static final SYNC_OBSERVER_TYPE_STATUS:I = 0x8

.field private static final TAG:Ljava/lang/String; = "ContentResolver"

.field private static sContentService:Landroid/content/IContentService;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mRandom:Ljava/util/Random;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 185
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 183
    new-instance v0, Ljava/util/Random;

    #@5
    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    #@8
    iput-object v0, p0, Landroid/content/ContentResolver;->mRandom:Ljava/util/Random;

    #@a
    .line 186
    iput-object p1, p0, Landroid/content/ContentResolver;->mContext:Landroid/content/Context;

    #@c
    .line 187
    return-void
.end method

.method public static addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V
    .registers 11
    .parameter "account"
    .parameter "authority"
    .parameter "extras"
    .parameter "pollFrequency"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1500
    invoke-static {p2}, Landroid/content/ContentResolver;->validateSyncExtrasBundle(Landroid/os/Bundle;)V

    #@4
    .line 1501
    if-nez p0, :cond_e

    #@6
    .line 1502
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "account must not be null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 1504
    :cond_e
    if-nez p1, :cond_18

    #@10
    .line 1505
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "authority must not be null"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 1507
    :cond_18
    const-string v0, "force"

    #@1a
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_50

    #@20
    const-string v0, "do_not_retry"

    #@22
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_50

    #@28
    const-string v0, "ignore_backoff"

    #@2a
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@2d
    move-result v0

    #@2e
    if-nez v0, :cond_50

    #@30
    const-string v0, "ignore_settings"

    #@32
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@35
    move-result v0

    #@36
    if-nez v0, :cond_50

    #@38
    const-string v0, "initialize"

    #@3a
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@3d
    move-result v0

    #@3e
    if-nez v0, :cond_50

    #@40
    const-string v0, "force"

    #@42
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@45
    move-result v0

    #@46
    if-nez v0, :cond_50

    #@48
    const-string v0, "expedited"

    #@4a
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@4d
    move-result v0

    #@4e
    if-eqz v0, :cond_58

    #@50
    .line 1514
    :cond_50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@52
    const-string v1, "illegal extras were set"

    #@54
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@57
    throw v0

    #@58
    .line 1517
    :cond_58
    :try_start_58
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@5b
    move-result-object v0

    #@5c
    move-object v1, p0

    #@5d
    move-object v2, p1

    #@5e
    move-object v3, p2

    #@5f
    move-wide v4, p3

    #@60
    invoke-interface/range {v0 .. v5}, Landroid/content/IContentService;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V
    :try_end_63
    .catch Landroid/os/RemoteException; {:try_start_58 .. :try_end_63} :catch_64

    #@63
    .line 1522
    :goto_63
    return-void

    #@64
    .line 1518
    :catch_64
    move-exception v0

    #@65
    goto :goto_63
.end method

.method public static addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;
    .registers 6
    .parameter "mask"
    .parameter "callback"

    #@0
    .prologue
    .line 1740
    if-nez p1, :cond_b

    #@2
    .line 1741
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v3, "you passed in a null callback"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1744
    :cond_b
    :try_start_b
    new-instance v1, Landroid/content/ContentResolver$1;

    #@d
    invoke-direct {v1, p1}, Landroid/content/ContentResolver$1;-><init>(Landroid/content/SyncStatusObserver;)V

    #@10
    .line 1749
    .local v1, observer:Landroid/content/ISyncStatusObserver$Stub;
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@13
    move-result-object v2

    #@14
    invoke-interface {v2, p0, v1}, Landroid/content/IContentService;->addStatusChangeListener(ILandroid/content/ISyncStatusObserver;)V
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_17} :catch_18

    #@17
    .line 1750
    return-object v1

    #@18
    .line 1751
    .end local v1           #observer:Landroid/content/ISyncStatusObserver$Stub;
    :catch_18
    move-exception v0

    #@19
    .line 1752
    .local v0, e:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@1b
    const-string/jumbo v3, "the ContentService should always be reachable"

    #@1e
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@21
    throw v2
.end method

.method public static cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V
    .registers 3
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 1419
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p0, p1}, Landroid/content/IContentService;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 1422
    :goto_7
    return-void

    #@8
    .line 1420
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public static getContentService()Landroid/content/IContentService;
    .registers 2

    #@0
    .prologue
    .line 1919
    sget-object v1, Landroid/content/ContentResolver;->sContentService:Landroid/content/IContentService;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 1920
    sget-object v1, Landroid/content/ContentResolver;->sContentService:Landroid/content/IContentService;

    #@6
    .line 1926
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v1

    #@7
    .line 1922
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v1, "content"

    #@9
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 1924
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/content/IContentService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IContentService;

    #@10
    move-result-object v1

    #@11
    sput-object v1, Landroid/content/ContentResolver;->sContentService:Landroid/content/IContentService;

    #@13
    .line 1926
    sget-object v1, Landroid/content/ContentResolver;->sContentService:Landroid/content/IContentService;

    #@15
    goto :goto_6
.end method

.method public static getCurrentSync()Landroid/content/SyncInfo;
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1667
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v2

    #@4
    invoke-interface {v2}, Landroid/content/IContentService;->getCurrentSyncs()Ljava/util/List;

    #@7
    move-result-object v1

    #@8
    .line 1668
    .local v1, syncs:Ljava/util/List;,"Ljava/util/List<Landroid/content/SyncInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_10

    #@e
    .line 1669
    const/4 v2, 0x0

    #@f
    .line 1671
    :goto_f
    return-object v2

    #@10
    :cond_10
    const/4 v2, 0x0

    #@11
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/content/SyncInfo;
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_17} :catch_18

    #@17
    goto :goto_f

    #@18
    .line 1672
    :catch_18
    move-exception v0

    #@19
    .line 1673
    .local v0, e:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@1b
    const-string/jumbo v3, "the ContentService should always be reachable"

    #@1e
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@21
    throw v2
.end method

.method public static getCurrentSyncs()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/SyncInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1688
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/content/IContentService;->getCurrentSyncs()Ljava/util/List;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    return-object v1

    #@9
    .line 1689
    :catch_9
    move-exception v0

    #@a
    .line 1690
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@c
    const-string/jumbo v2, "the ContentService should always be reachable"

    #@f
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@12
    throw v1
.end method

.method public static getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I
    .registers 5
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 1580
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p0, p1}, Landroid/content/IContentService;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    return v1

    #@9
    .line 1581
    :catch_9
    move-exception v0

    #@a
    .line 1582
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@c
    const-string/jumbo v2, "the ContentService should always be reachable"

    #@f
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@12
    throw v1
.end method

.method public static getMasterSyncAutomatically()Z
    .registers 3

    #@0
    .prologue
    .line 1611
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/content/IContentService;->getMasterSyncAutomatically()Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    return v1

    #@9
    .line 1612
    :catch_9
    move-exception v0

    #@a
    .line 1613
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@c
    const-string/jumbo v2, "the ContentService should always be reachable"

    #@f
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@12
    throw v1
.end method

.method public static getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;
    .registers 5
    .parameter "account"
    .parameter "authority"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/PeriodicSync;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1559
    if-nez p0, :cond_a

    #@2
    .line 1560
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "account must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 1562
    :cond_a
    if-nez p1, :cond_14

    #@c
    .line 1563
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v2, "authority must not be null"

    #@10
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 1566
    :cond_14
    :try_start_14
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@17
    move-result-object v1

    #@18
    invoke-interface {v1, p0, p1}, Landroid/content/IContentService;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_1b} :catch_1d

    #@1b
    move-result-object v1

    #@1c
    return-object v1

    #@1d
    .line 1567
    :catch_1d
    move-exception v0

    #@1e
    .line 1568
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@20
    const-string/jumbo v2, "the ContentService should always be reachable"

    #@23
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@26
    throw v1
.end method

.method public static getSyncAdapterTypes()[Landroid/content/SyncAdapterType;
    .registers 3

    #@0
    .prologue
    .line 1430
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/content/IContentService;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    return-object v1

    #@9
    .line 1431
    :catch_9
    move-exception v0

    #@a
    .line 1432
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@c
    const-string/jumbo v2, "the ContentService should always be reachable"

    #@f
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@12
    throw v1
.end method

.method public static getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z
    .registers 5
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 1447
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p0, p1}, Landroid/content/IContentService;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    return v1

    #@9
    .line 1448
    :catch_9
    move-exception v0

    #@a
    .line 1449
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@c
    const-string/jumbo v2, "the ContentService should always be reachable"

    #@f
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@12
    throw v1
.end method

.method public static getSyncStatus(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/SyncStatusInfo;
    .registers 5
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 1703
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p0, p1}, Landroid/content/IContentService;->getSyncStatus(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/SyncStatusInfo;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    return-object v1

    #@9
    .line 1704
    :catch_9
    move-exception v0

    #@a
    .line 1705
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@c
    const-string/jumbo v2, "the ContentService should always be reachable"

    #@f
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@12
    throw v1
.end method

.method public static isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z
    .registers 5
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 1645
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p0, p1}, Landroid/content/IContentService;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    return v1

    #@9
    .line 1646
    :catch_9
    move-exception v0

    #@a
    .line 1647
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@c
    const-string/jumbo v2, "the ContentService should always be reachable"

    #@f
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@12
    throw v1
.end method

.method public static isSyncPending(Landroid/accounts/Account;Ljava/lang/String;)Z
    .registers 5
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 1719
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p0, p1}, Landroid/content/IContentService;->isSyncPending(Landroid/accounts/Account;Ljava/lang/String;)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    return v1

    #@9
    .line 1720
    :catch_9
    move-exception v0

    #@a
    .line 1721
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@c
    const-string/jumbo v2, "the ContentService should always be reachable"

    #@f
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@12
    throw v1
.end method

.method private maybeLogQueryToEventLog(JLandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "durationMillis"
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "sortOrder"

    #@0
    .prologue
    const/16 v7, 0x64

    #@2
    .line 1787
    invoke-direct {p0, p1, p2}, Landroid/content/ContentResolver;->samplePercentForDuration(J)I

    #@5
    move-result v3

    #@6
    .line 1788
    .local v3, samplePercent:I
    if-ge v3, v7, :cond_18

    #@8
    .line 1789
    iget-object v5, p0, Landroid/content/ContentResolver;->mRandom:Ljava/util/Random;

    #@a
    monitor-enter v5

    #@b
    .line 1790
    :try_start_b
    iget-object v4, p0, Landroid/content/ContentResolver;->mRandom:Ljava/util/Random;

    #@d
    const/16 v6, 0x64

    #@f
    invoke-virtual {v4, v6}, Ljava/util/Random;->nextInt(I)I

    #@12
    move-result v4

    #@13
    if-lt v4, v3, :cond_17

    #@15
    .line 1791
    monitor-exit v5

    #@16
    .line 1822
    .end local p5
    .end local p6
    :goto_16
    return-void

    #@17
    .line 1793
    .restart local p5
    .restart local p6
    :cond_17
    monitor-exit v5
    :try_end_18
    .catchall {:try_start_b .. :try_end_18} :catchall_32

    #@18
    .line 1796
    :cond_18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    #@1d
    .line 1797
    .local v2, projectionBuffer:Ljava/lang/StringBuilder;
    if-eqz p4, :cond_35

    #@1f
    .line 1798
    const/4 v1, 0x0

    #@20
    .local v1, i:I
    :goto_20
    array-length v4, p4

    #@21
    if-ge v1, v4, :cond_35

    #@23
    .line 1803
    if-eqz v1, :cond_2a

    #@25
    const/16 v4, 0x2f

    #@27
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2a
    .line 1804
    :cond_2a
    aget-object v4, p4, v1

    #@2c
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    .line 1798
    add-int/lit8 v1, v1, 0x1

    #@31
    goto :goto_20

    #@32
    .line 1793
    .end local v1           #i:I
    .end local v2           #projectionBuffer:Ljava/lang/StringBuilder;
    :catchall_32
    move-exception v4

    #@33
    :try_start_33
    monitor-exit v5
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    #@34
    throw v4

    #@35
    .line 1811
    .restart local v2       #projectionBuffer:Ljava/lang/StringBuilder;
    :cond_35
    invoke-static {}, Landroid/app/AppGlobals;->getInitialPackage()Ljava/lang/String;

    #@38
    move-result-object v0

    #@39
    .line 1813
    .local v0, blockingPackage:Ljava/lang/String;
    const v4, 0xcb22

    #@3c
    const/4 v5, 0x7

    #@3d
    new-array v5, v5, [Ljava/lang/Object;

    #@3f
    const/4 v6, 0x0

    #@40
    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@43
    move-result-object v7

    #@44
    aput-object v7, v5, v6

    #@46
    const/4 v6, 0x1

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v7

    #@4b
    aput-object v7, v5, v6

    #@4d
    const/4 v6, 0x2

    #@4e
    if-eqz p5, :cond_6e

    #@50
    .end local p5
    :goto_50
    aput-object p5, v5, v6

    #@52
    const/4 v6, 0x3

    #@53
    if-eqz p6, :cond_71

    #@55
    .end local p6
    :goto_55
    aput-object p6, v5, v6

    #@57
    const/4 v6, 0x4

    #@58
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5b
    move-result-object v7

    #@5c
    aput-object v7, v5, v6

    #@5e
    const/4 v6, 0x5

    #@5f
    if-eqz v0, :cond_74

    #@61
    .end local v0           #blockingPackage:Ljava/lang/String;
    :goto_61
    aput-object v0, v5, v6

    #@63
    const/4 v6, 0x6

    #@64
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@67
    move-result-object v7

    #@68
    aput-object v7, v5, v6

    #@6a
    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@6d
    goto :goto_16

    #@6e
    .restart local v0       #blockingPackage:Ljava/lang/String;
    .restart local p5
    .restart local p6
    :cond_6e
    const-string p5, ""

    #@70
    goto :goto_50

    #@71
    .end local p5
    :cond_71
    const-string p6, ""

    #@73
    goto :goto_55

    #@74
    .end local p6
    :cond_74
    const-string v0, ""

    #@76
    goto :goto_61
.end method

.method private maybeLogUpdateToEventLog(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "durationMillis"
    .parameter "uri"
    .parameter "operation"
    .parameter "selection"

    #@0
    .prologue
    const/16 v2, 0x64

    #@2
    .line 1826
    invoke-direct {p0, p1, p2}, Landroid/content/ContentResolver;->samplePercentForDuration(J)I

    #@5
    move-result v1

    #@6
    .line 1827
    .local v1, samplePercent:I
    if-ge v1, v2, :cond_18

    #@8
    .line 1828
    iget-object v3, p0, Landroid/content/ContentResolver;->mRandom:Ljava/util/Random;

    #@a
    monitor-enter v3

    #@b
    .line 1829
    :try_start_b
    iget-object v2, p0, Landroid/content/ContentResolver;->mRandom:Ljava/util/Random;

    #@d
    const/16 v4, 0x64

    #@f
    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    #@12
    move-result v2

    #@13
    if-lt v2, v1, :cond_17

    #@15
    .line 1830
    monitor-exit v3

    #@16
    .line 1843
    .end local p5
    :goto_16
    return-void

    #@17
    .line 1832
    .restart local p5
    :cond_17
    monitor-exit v3
    :try_end_18
    .catchall {:try_start_b .. :try_end_18} :catchall_48

    #@18
    .line 1834
    :cond_18
    invoke-static {}, Landroid/app/AppGlobals;->getInitialPackage()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 1835
    .local v0, blockingPackage:Ljava/lang/String;
    const v2, 0xcb23

    #@1f
    const/4 v3, 0x6

    #@20
    new-array v3, v3, [Ljava/lang/Object;

    #@22
    const/4 v4, 0x0

    #@23
    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    aput-object v5, v3, v4

    #@29
    const/4 v4, 0x1

    #@2a
    aput-object p4, v3, v4

    #@2c
    const/4 v4, 0x2

    #@2d
    if-eqz p5, :cond_4b

    #@2f
    .end local p5
    :goto_2f
    aput-object p5, v3, v4

    #@31
    const/4 v4, 0x3

    #@32
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@35
    move-result-object v5

    #@36
    aput-object v5, v3, v4

    #@38
    const/4 v4, 0x4

    #@39
    if-eqz v0, :cond_4e

    #@3b
    .end local v0           #blockingPackage:Ljava/lang/String;
    :goto_3b
    aput-object v0, v3, v4

    #@3d
    const/4 v4, 0x5

    #@3e
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v5

    #@42
    aput-object v5, v3, v4

    #@44
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@47
    goto :goto_16

    #@48
    .line 1832
    .restart local p5
    :catchall_48
    move-exception v2

    #@49
    :try_start_49
    monitor-exit v3
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_48

    #@4a
    throw v2

    #@4b
    .line 1835
    .restart local v0       #blockingPackage:Ljava/lang/String;
    :cond_4b
    const-string p5, ""

    #@4d
    goto :goto_2f

    #@4e
    .end local p5
    :cond_4e
    const-string v0, ""

    #@50
    goto :goto_3b
.end method

.method public static modeToMode(Landroid/net/Uri;Ljava/lang/String;)I
    .registers 6
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 824
    const-string/jumbo v1, "r"

    #@3
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_c

    #@9
    .line 825
    const/high16 v0, 0x1000

    #@b
    .line 845
    .local v0, modeBits:I
    :goto_b
    return v0

    #@c
    .line 826
    .end local v0           #modeBits:I
    :cond_c
    const-string/jumbo v1, "w"

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_1e

    #@15
    const-string/jumbo v1, "wt"

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_21

    #@1e
    .line 827
    :cond_1e
    const/high16 v0, 0x2c00

    #@20
    .restart local v0       #modeBits:I
    goto :goto_b

    #@21
    .line 830
    .end local v0           #modeBits:I
    :cond_21
    const-string/jumbo v1, "wa"

    #@24
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v1

    #@28
    if-eqz v1, :cond_2d

    #@2a
    .line 831
    const/high16 v0, 0x2a00

    #@2c
    .restart local v0       #modeBits:I
    goto :goto_b

    #@2d
    .line 834
    .end local v0           #modeBits:I
    :cond_2d
    const-string/jumbo v1, "rw"

    #@30
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v1

    #@34
    if-eqz v1, :cond_39

    #@36
    .line 835
    const/high16 v0, 0x3800

    #@38
    .restart local v0       #modeBits:I
    goto :goto_b

    #@39
    .line 837
    .end local v0           #modeBits:I
    :cond_39
    const-string/jumbo v1, "rwt"

    #@3c
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v1

    #@40
    if-eqz v1, :cond_45

    #@42
    .line 838
    const/high16 v0, 0x3c00

    #@44
    .restart local v0       #modeBits:I
    goto :goto_b

    #@45
    .line 842
    .end local v0           #modeBits:I
    :cond_45
    new-instance v1, Ljava/io/FileNotFoundException;

    #@47
    new-instance v2, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v3, "Bad mode for "

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    const-string v3, ": "

    #@58
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v2

    #@60
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v2

    #@64
    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@67
    throw v1
.end method

.method public static removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 6
    .parameter "account"
    .parameter "authority"
    .parameter "extras"

    #@0
    .prologue
    .line 1535
    invoke-static {p2}, Landroid/content/ContentResolver;->validateSyncExtrasBundle(Landroid/os/Bundle;)V

    #@3
    .line 1536
    if-nez p0, :cond_d

    #@5
    .line 1537
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v2, "account must not be null"

    #@9
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1

    #@d
    .line 1539
    :cond_d
    if-nez p1, :cond_17

    #@f
    .line 1540
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@11
    const-string v2, "authority must not be null"

    #@13
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16
    throw v1

    #@17
    .line 1543
    :cond_17
    :try_start_17
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@1a
    move-result-object v1

    #@1b
    invoke-interface {v1, p0, p1, p2}, Landroid/content/IContentService;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_1e} :catch_1f

    #@1e
    .line 1547
    return-void

    #@1f
    .line 1544
    :catch_1f
    move-exception v0

    #@20
    .line 1545
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@22
    const-string/jumbo v2, "the ContentService should always be reachable"

    #@25
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@28
    throw v1
.end method

.method public static removeStatusChangeListener(Ljava/lang/Object;)V
    .registers 3
    .parameter "handle"

    #@0
    .prologue
    .line 1761
    if-nez p0, :cond_b

    #@2
    .line 1762
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "you passed in a null handle"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 1765
    :cond_b
    :try_start_b
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@e
    move-result-object v0

    #@f
    check-cast p0, Landroid/content/ISyncStatusObserver$Stub;

    #@11
    .end local p0
    invoke-interface {v0, p0}, Landroid/content/IContentService;->removeStatusChangeListener(Landroid/content/ISyncStatusObserver;)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_14} :catch_15

    #@14
    .line 1770
    :goto_14
    return-void

    #@15
    .line 1766
    :catch_15
    move-exception v0

    #@16
    goto :goto_14
.end method

.method public static requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 4
    .parameter "account"
    .parameter "authority"
    .parameter "extras"

    #@0
    .prologue
    .line 1354
    invoke-static {p2}, Landroid/content/ContentResolver;->validateSyncExtrasBundle(Landroid/os/Bundle;)V

    #@3
    .line 1356
    :try_start_3
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@6
    move-result-object v0

    #@7
    invoke-interface {v0, p0, p1, p2}, Landroid/content/IContentService;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_a} :catch_b

    #@a
    .line 1359
    :goto_a
    return-void

    #@b
    .line 1357
    :catch_b
    move-exception v0

    #@c
    goto :goto_a
.end method

.method private samplePercentForDuration(J)I
    .registers 7
    .parameter "durationMillis"

    #@0
    .prologue
    const-wide/16 v2, 0x1f4

    #@2
    .line 1778
    cmp-long v0, p1, v2

    #@4
    if-ltz v0, :cond_9

    #@6
    .line 1779
    const/16 v0, 0x64

    #@8
    .line 1781
    :goto_8
    return v0

    #@9
    :cond_9
    const-wide/16 v0, 0x64

    #@b
    mul-long/2addr v0, p1

    #@c
    div-long/2addr v0, v2

    #@d
    long-to-int v0, v0

    #@e
    add-int/lit8 v0, v0, 0x1

    #@10
    goto :goto_8
.end method

.method public static setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    .registers 4
    .parameter "account"
    .parameter "authority"
    .parameter "syncable"

    #@0
    .prologue
    .line 1594
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p0, p1, p2}, Landroid/content/IContentService;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 1599
    :goto_7
    return-void

    #@8
    .line 1595
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public static setMasterSyncAutomatically(Z)V
    .registers 2
    .parameter "sync"

    #@0
    .prologue
    .line 1627
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p0}, Landroid/content/IContentService;->setMasterSyncAutomatically(Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 1632
    :goto_7
    return-void

    #@8
    .line 1628
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public static setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V
    .registers 4
    .parameter "account"
    .parameter "authority"
    .parameter "sync"

    #@0
    .prologue
    .line 1464
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p0, p1, p2}, Landroid/content/IContentService;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 1469
    :goto_7
    return-void

    #@8
    .line 1465
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public static validateSyncExtrasBundle(Landroid/os/Bundle;)V
    .registers 9
    .parameter "extras"

    #@0
    .prologue
    .line 1377
    :try_start_0
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@3
    move-result-object v5

    #@4
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v2

    #@8
    .local v2, i$:Ljava/util/Iterator;
    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v5

    #@c
    if-eqz v5, :cond_63

    #@e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v3

    #@12
    check-cast v3, Ljava/lang/String;

    #@14
    .line 1378
    .local v3, key:Ljava/lang/String;
    invoke-virtual {p0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@17
    move-result-object v4

    #@18
    .line 1379
    .local v4, value:Ljava/lang/Object;
    if-eqz v4, :cond_8

    #@1a
    .line 1380
    instance-of v5, v4, Ljava/lang/Long;

    #@1c
    if-nez v5, :cond_8

    #@1e
    .line 1381
    instance-of v5, v4, Ljava/lang/Integer;

    #@20
    if-nez v5, :cond_8

    #@22
    .line 1382
    instance-of v5, v4, Ljava/lang/Boolean;

    #@24
    if-nez v5, :cond_8

    #@26
    .line 1383
    instance-of v5, v4, Ljava/lang/Float;

    #@28
    if-nez v5, :cond_8

    #@2a
    .line 1384
    instance-of v5, v4, Ljava/lang/Double;

    #@2c
    if-nez v5, :cond_8

    #@2e
    .line 1385
    instance-of v5, v4, Ljava/lang/String;

    #@30
    if-nez v5, :cond_8

    #@32
    .line 1386
    instance-of v5, v4, Landroid/accounts/Account;

    #@34
    if-nez v5, :cond_8

    #@36
    .line 1387
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@38
    new-instance v6, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string/jumbo v7, "unexpected value type: "

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@47
    move-result-object v7

    #@48
    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v6

    #@50
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v6

    #@54
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@57
    throw v5
    :try_end_58
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_58} :catch_58
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_58} :catch_5a

    #@58
    .line 1390
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #key:Ljava/lang/String;
    .end local v4           #value:Ljava/lang/Object;
    :catch_58
    move-exception v0

    #@59
    .line 1391
    .local v0, e:Ljava/lang/IllegalArgumentException;
    throw v0

    #@5a
    .line 1392
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    :catch_5a
    move-exception v1

    #@5b
    .line 1393
    .local v1, exc:Ljava/lang/RuntimeException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@5d
    const-string v6, "error unparceling Bundle"

    #@5f
    invoke-direct {v5, v6, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@62
    throw v5

    #@63
    .line 1395
    .end local v1           #exc:Ljava/lang/RuntimeException;
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_63
    return-void
.end method


# virtual methods
.method public final acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    .line 1128
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@3
    move-result-object v0

    #@4
    .line 1129
    .local v0, provider:Landroid/content/IContentProvider;
    if-eqz v0, :cond_d

    #@6
    .line 1130
    new-instance v1, Landroid/content/ContentProviderClient;

    #@8
    const/4 v2, 0x1

    #@9
    invoke-direct {v1, p0, v0, v2}, Landroid/content/ContentProviderClient;-><init>(Landroid/content/ContentResolver;Landroid/content/IContentProvider;Z)V

    #@c
    .line 1133
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method public final acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;
    .registers 5
    .parameter "name"

    #@0
    .prologue
    .line 1148
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Ljava/lang/String;)Landroid/content/IContentProvider;

    #@3
    move-result-object v0

    #@4
    .line 1149
    .local v0, provider:Landroid/content/IContentProvider;
    if-eqz v0, :cond_d

    #@6
    .line 1150
    new-instance v1, Landroid/content/ContentProviderClient;

    #@8
    const/4 v2, 0x1

    #@9
    invoke-direct {v1, p0, v0, v2}, Landroid/content/ContentProviderClient;-><init>(Landroid/content/ContentResolver;Landroid/content/IContentProvider;Z)V

    #@c
    .line 1153
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method protected acquireExistingProvider(Landroid/content/Context;Ljava/lang/String;)Landroid/content/IContentProvider;
    .registers 4
    .parameter "c"
    .parameter "name"

    #@0
    .prologue
    .line 195
    invoke-virtual {p0, p1, p2}, Landroid/content/ContentResolver;->acquireProvider(Landroid/content/Context;Ljava/lang/String;)Landroid/content/IContentProvider;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public final acquireExistingProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;
    .registers 6
    .parameter "uri"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1068
    const-string v2, "content"

    #@3
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_e

    #@d
    .line 1075
    :cond_d
    :goto_d
    return-object v1

    #@e
    .line 1071
    :cond_e
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 1072
    .local v0, auth:Ljava/lang/String;
    if-eqz v0, :cond_d

    #@14
    .line 1073
    iget-object v1, p0, Landroid/content/ContentResolver;->mContext:Landroid/content/Context;

    #@16
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {p0, v1, v2}, Landroid/content/ContentResolver;->acquireExistingProvider(Landroid/content/Context;Ljava/lang/String;)Landroid/content/IContentProvider;

    #@1d
    move-result-object v1

    #@1e
    goto :goto_d
.end method

.method protected abstract acquireProvider(Landroid/content/Context;Ljava/lang/String;)Landroid/content/IContentProvider;
.end method

.method public final acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;
    .registers 6
    .parameter "uri"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1049
    const-string v2, "content"

    #@3
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_e

    #@d
    .line 1056
    :cond_d
    :goto_d
    return-object v1

    #@e
    .line 1052
    :cond_e
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 1053
    .local v0, auth:Ljava/lang/String;
    if-eqz v0, :cond_d

    #@14
    .line 1054
    iget-object v1, p0, Landroid/content/ContentResolver;->mContext:Landroid/content/Context;

    #@16
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {p0, v1, v2}, Landroid/content/ContentResolver;->acquireProvider(Landroid/content/Context;Ljava/lang/String;)Landroid/content/IContentProvider;

    #@1d
    move-result-object v1

    #@1e
    goto :goto_d
.end method

.method public final acquireProvider(Ljava/lang/String;)Landroid/content/IContentProvider;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 1082
    if-nez p1, :cond_4

    #@2
    .line 1083
    const/4 v0, 0x0

    #@3
    .line 1085
    :goto_3
    return-object v0

    #@4
    :cond_4
    iget-object v0, p0, Landroid/content/ContentResolver;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {p0, v0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/content/Context;Ljava/lang/String;)Landroid/content/IContentProvider;

    #@9
    move-result-object v0

    #@a
    goto :goto_3
.end method

.method public final acquireUnstableContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    .line 1173
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireUnstableProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@3
    move-result-object v0

    #@4
    .line 1174
    .local v0, provider:Landroid/content/IContentProvider;
    if-eqz v0, :cond_d

    #@6
    .line 1175
    new-instance v1, Landroid/content/ContentProviderClient;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, p0, v0, v2}, Landroid/content/ContentProviderClient;-><init>(Landroid/content/ContentResolver;Landroid/content/IContentProvider;Z)V

    #@c
    .line 1178
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method public final acquireUnstableContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;
    .registers 5
    .parameter "name"

    #@0
    .prologue
    .line 1198
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireUnstableProvider(Ljava/lang/String;)Landroid/content/IContentProvider;

    #@3
    move-result-object v0

    #@4
    .line 1199
    .local v0, provider:Landroid/content/IContentProvider;
    if-eqz v0, :cond_d

    #@6
    .line 1200
    new-instance v1, Landroid/content/ContentProviderClient;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, p0, v0, v2}, Landroid/content/ContentProviderClient;-><init>(Landroid/content/ContentResolver;Landroid/content/IContentProvider;Z)V

    #@c
    .line 1203
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method protected abstract acquireUnstableProvider(Landroid/content/Context;Ljava/lang/String;)Landroid/content/IContentProvider;
.end method

.method public final acquireUnstableProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;
    .registers 6
    .parameter "uri"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1096
    const-string v2, "content"

    #@3
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_e

    #@d
    .line 1103
    :cond_d
    :goto_d
    return-object v1

    #@e
    .line 1099
    :cond_e
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 1100
    .local v0, auth:Ljava/lang/String;
    if-eqz v0, :cond_d

    #@14
    .line 1101
    iget-object v1, p0, Landroid/content/ContentResolver;->mContext:Landroid/content/Context;

    #@16
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {p0, v1, v2}, Landroid/content/ContentResolver;->acquireUnstableProvider(Landroid/content/Context;Ljava/lang/String;)Landroid/content/IContentProvider;

    #@1d
    move-result-object v1

    #@1e
    goto :goto_d
.end method

.method public final acquireUnstableProvider(Ljava/lang/String;)Landroid/content/IContentProvider;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 1110
    if-nez p1, :cond_4

    #@2
    .line 1111
    const/4 v0, 0x0

    #@3
    .line 1113
    :goto_3
    return-object v0

    #@4
    :cond_4
    iget-object v0, p0, Landroid/content/ContentResolver;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {p0, v0, p1}, Landroid/content/ContentResolver;->acquireUnstableProvider(Landroid/content/Context;Ljava/lang/String;)Landroid/content/IContentProvider;

    #@9
    move-result-object v0

    #@a
    goto :goto_3
.end method

.method public applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .registers 7
    .parameter "authority"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    #@0
    .prologue
    .line 898
    .local p2, operations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    #@3
    move-result-object v0

    #@4
    .line 899
    .local v0, provider:Landroid/content/ContentProviderClient;
    if-nez v0, :cond_1f

    #@6
    .line 900
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Unknown authority "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1

    #@1f
    .line 903
    :cond_1f
    :try_start_1f
    invoke-virtual {v0, p2}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_22
    .catchall {:try_start_1f .. :try_end_22} :catchall_27

    #@22
    move-result-object v1

    #@23
    .line 905
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    #@26
    return-object v1

    #@27
    :catchall_27
    move-exception v1

    #@28
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    #@2b
    throw v1
.end method

.method public final bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .registers 14
    .parameter "url"
    .parameter "values"

    #@0
    .prologue
    .line 921
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@3
    move-result-object v7

    #@4
    .line 922
    .local v7, provider:Landroid/content/IContentProvider;
    if-nez v7, :cond_1f

    #@6
    .line 923
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "Unknown URL "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 926
    :cond_1f
    :try_start_1f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@22
    move-result-wide v9

    #@23
    .line 927
    .local v9, startTime:J
    invoke-interface {v7, p1, p2}, Landroid/content/IContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    #@26
    move-result v8

    #@27
    .line 928
    .local v8, rowsCreated:I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@2a
    move-result-wide v3

    #@2b
    sub-long v1, v3, v9

    #@2d
    .line 929
    .local v1, durationMillis:J
    const-string v4, "bulkinsert"

    #@2f
    const/4 v5, 0x0

    #@30
    move-object v0, p0

    #@31
    move-object v3, p1

    #@32
    invoke-direct/range {v0 .. v5}, Landroid/content/ContentResolver;->maybeLogUpdateToEventLog(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_35
    .catchall {:try_start_1f .. :try_end_35} :catchall_3f
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_35} :catch_39

    #@35
    .line 936
    invoke-virtual {p0, v7}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@38
    .end local v1           #durationMillis:J
    .end local v8           #rowsCreated:I
    .end local v9           #startTime:J
    :goto_38
    return v8

    #@39
    .line 931
    :catch_39
    move-exception v6

    #@3a
    .line 934
    .local v6, e:Landroid/os/RemoteException;
    const/4 v8, 0x0

    #@3b
    .line 936
    invoke-virtual {p0, v7}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@3e
    goto :goto_38

    #@3f
    .end local v6           #e:Landroid/os/RemoteException;
    :catchall_3f
    move-exception v0

    #@40
    invoke-virtual {p0, v7}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@43
    throw v0
.end method

.method public final call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 10
    .parameter "uri"
    .parameter "method"
    .parameter "arg"
    .parameter "extras"

    #@0
    .prologue
    .line 1020
    if-nez p1, :cond_b

    #@2
    .line 1021
    new-instance v2, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v3, "uri == null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1023
    :cond_b
    if-nez p2, :cond_16

    #@d
    .line 1024
    new-instance v2, Ljava/lang/NullPointerException;

    #@f
    const-string/jumbo v3, "method == null"

    #@12
    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@15
    throw v2

    #@16
    .line 1026
    :cond_16
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@19
    move-result-object v1

    #@1a
    .line 1027
    .local v1, provider:Landroid/content/IContentProvider;
    if-nez v1, :cond_35

    #@1c
    .line 1028
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "Unknown URI "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@34
    throw v2

    #@35
    .line 1031
    :cond_35
    :try_start_35
    invoke-interface {v1, p2, p3, p4}, Landroid/content/IContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_43
    .catch Landroid/os/RemoteException; {:try_start_35 .. :try_end_38} :catch_3d

    #@38
    move-result-object v2

    #@39
    .line 1037
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@3c
    :goto_3c
    return-object v2

    #@3d
    .line 1032
    :catch_3d
    move-exception v0

    #@3e
    .line 1035
    .local v0, e:Landroid/os/RemoteException;
    const/4 v2, 0x0

    #@3f
    .line 1037
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@42
    goto :goto_3c

    #@43
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_43
    move-exception v2

    #@44
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@47
    throw v2
.end method

.method public cancelSync(Landroid/net/Uri;)V
    .registers 4
    .parameter "uri"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1406
    if-eqz p1, :cond_b

    #@3
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    :goto_7
    invoke-static {v1, v0}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    #@a
    .line 1407
    return-void

    #@b
    :cond_b
    move-object v0, v1

    #@c
    .line 1406
    goto :goto_7
.end method

.method public final delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 15
    .parameter "url"
    .parameter "where"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 952
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@3
    move-result-object v7

    #@4
    .line 953
    .local v7, provider:Landroid/content/IContentProvider;
    if-nez v7, :cond_1f

    #@6
    .line 954
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "Unknown URL "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 957
    :cond_1f
    :try_start_1f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@22
    move-result-wide v9

    #@23
    .line 958
    .local v9, startTime:J
    invoke-interface {v7, p1, p2, p3}, Landroid/content/IContentProvider;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@26
    move-result v8

    #@27
    .line 959
    .local v8, rowsDeleted:I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@2a
    move-result-wide v3

    #@2b
    sub-long v1, v3, v9

    #@2d
    .line 960
    .local v1, durationMillis:J
    const-string v4, "delete"

    #@2f
    move-object v0, p0

    #@30
    move-object v3, p1

    #@31
    move-object v5, p2

    #@32
    invoke-direct/range {v0 .. v5}, Landroid/content/ContentResolver;->maybeLogUpdateToEventLog(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_35
    .catchall {:try_start_1f .. :try_end_35} :catchall_3f
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_35} :catch_39

    #@35
    .line 967
    invoke-virtual {p0, v7}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@38
    .end local v1           #durationMillis:J
    .end local v8           #rowsDeleted:I
    .end local v9           #startTime:J
    :goto_38
    return v8

    #@39
    .line 962
    :catch_39
    move-exception v6

    #@3a
    .line 965
    .local v6, e:Landroid/os/RemoteException;
    const/4 v8, -0x1

    #@3b
    .line 967
    invoke-virtual {p0, v7}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@3e
    goto :goto_38

    #@3f
    .end local v6           #e:Landroid/os/RemoteException;
    :catchall_3f
    move-exception v0

    #@40
    invoke-virtual {p0, v7}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@43
    throw v0
.end method

.method public getResourceId(Landroid/net/Uri;)Landroid/content/ContentResolver$OpenResourceIdResult;
    .registers 13
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 784
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 786
    .local v0, authority:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9
    move-result v8

    #@a
    if-eqz v8, :cond_25

    #@c
    .line 787
    new-instance v8, Ljava/io/FileNotFoundException;

    #@e
    new-instance v9, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v10, "No authority: "

    #@15
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v9

    #@19
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v9

    #@1d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v9

    #@21
    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@24
    throw v8

    #@25
    .line 790
    :cond_25
    :try_start_25
    iget-object v8, p0, Landroid/content/ContentResolver;->mContext:Landroid/content/Context;

    #@27
    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@2a
    move-result-object v8

    #@2b
    invoke-virtual {v8, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_2e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_25 .. :try_end_2e} :catch_4e

    #@2e
    move-result-object v6

    #@2f
    .line 795
    .local v6, r:Landroid/content/res/Resources;
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@32
    move-result-object v5

    #@33
    .line 796
    .local v5, path:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez v5, :cond_68

    #@35
    .line 797
    new-instance v8, Ljava/io/FileNotFoundException;

    #@37
    new-instance v9, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v10, "No path: "

    #@3e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v9

    #@42
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v9

    #@46
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v9

    #@4a
    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@4d
    throw v8

    #@4e
    .line 791
    .end local v5           #path:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v6           #r:Landroid/content/res/Resources;
    :catch_4e
    move-exception v2

    #@4f
    .line 792
    .local v2, ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v8, Ljava/io/FileNotFoundException;

    #@51
    new-instance v9, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v10, "No package found for authority: "

    #@58
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v9

    #@5c
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v9

    #@60
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v9

    #@64
    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@67
    throw v8

    #@68
    .line 799
    .end local v2           #ex:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5       #path:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v6       #r:Landroid/content/res/Resources;
    :cond_68
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@6b
    move-result v4

    #@6c
    .line 801
    .local v4, len:I
    if-ne v4, v10, :cond_ae

    #@6e
    .line 803
    const/4 v8, 0x0

    #@6f
    :try_start_6f
    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@72
    move-result-object v8

    #@73
    check-cast v8, Ljava/lang/String;

    #@75
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_78
    .catch Ljava/lang/NumberFormatException; {:try_start_6f .. :try_end_78} :catch_94

    #@78
    move-result v3

    #@79
    .line 812
    .local v3, id:I
    :goto_79
    if-nez v3, :cond_db

    #@7b
    .line 813
    new-instance v8, Ljava/io/FileNotFoundException;

    #@7d
    new-instance v9, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v10, "No resource found for: "

    #@84
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v9

    #@88
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v9

    #@8c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v9

    #@90
    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@93
    throw v8

    #@94
    .line 804
    .end local v3           #id:I
    :catch_94
    move-exception v1

    #@95
    .line 805
    .local v1, e:Ljava/lang/NumberFormatException;
    new-instance v8, Ljava/io/FileNotFoundException;

    #@97
    new-instance v9, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v10, "Single path segment is not a resource ID: "

    #@9e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v9

    #@a2
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v9

    #@a6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v9

    #@aa
    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@ad
    throw v8

    #@ae
    .line 807
    .end local v1           #e:Ljava/lang/NumberFormatException;
    :cond_ae
    const/4 v8, 0x2

    #@af
    if-ne v4, v8, :cond_c2

    #@b1
    .line 808
    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@b4
    move-result-object v8

    #@b5
    check-cast v8, Ljava/lang/String;

    #@b7
    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@ba
    move-result-object v9

    #@bb
    check-cast v9, Ljava/lang/String;

    #@bd
    invoke-virtual {v6, v8, v9, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    move-result v3

    #@c1
    .restart local v3       #id:I
    goto :goto_79

    #@c2
    .line 810
    .end local v3           #id:I
    :cond_c2
    new-instance v8, Ljava/io/FileNotFoundException;

    #@c4
    new-instance v9, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v10, "More than two path segments: "

    #@cb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v9

    #@cf
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v9

    #@d3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v9

    #@d7
    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@da
    throw v8

    #@db
    .line 815
    .restart local v3       #id:I
    :cond_db
    new-instance v7, Landroid/content/ContentResolver$OpenResourceIdResult;

    #@dd
    invoke-direct {v7, p0}, Landroid/content/ContentResolver$OpenResourceIdResult;-><init>(Landroid/content/ContentResolver;)V

    #@e0
    .line 816
    .local v7, res:Landroid/content/ContentResolver$OpenResourceIdResult;
    iput-object v6, v7, Landroid/content/ContentResolver$OpenResourceIdResult;->r:Landroid/content/res/Resources;

    #@e2
    .line 817
    iput v3, v7, Landroid/content/ContentResolver$OpenResourceIdResult;->id:I

    #@e4
    .line 818
    return-object v7
.end method

.method public getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;
    .registers 6
    .parameter "url"
    .parameter "mimeTypeFilter"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 265
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@4
    move-result-object v1

    #@5
    .line 266
    .local v1, provider:Landroid/content/IContentProvider;
    if-nez v1, :cond_8

    #@7
    .line 277
    :goto_7
    return-object v2

    #@8
    .line 271
    :cond_8
    :try_start_8
    invoke-interface {v1, p1, p2}, Landroid/content/IContentProvider;->getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_15
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_b} :catch_10

    #@b
    move-result-object v2

    #@c
    .line 277
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@f
    goto :goto_7

    #@10
    .line 272
    :catch_10
    move-exception v0

    #@11
    .line 277
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@14
    goto :goto_7

    #@15
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_15
    move-exception v2

    #@16
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@19
    throw v2
.end method

.method public final getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 8
    .parameter "url"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 215
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireExistingProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@4
    move-result-object v1

    #@5
    .line 216
    .local v1, provider:Landroid/content/IContentProvider;
    if-eqz v1, :cond_4a

    #@7
    .line 218
    :try_start_7
    invoke-interface {v1, p1}, Landroid/content/IContentProvider;->getType(Landroid/net/Uri;)Ljava/lang/String;
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_45
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_f
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_a} :catch_14

    #@a
    move-result-object v2

    #@b
    .line 225
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@e
    .line 243
    :cond_e
    :goto_e
    return-object v2

    #@f
    .line 219
    :catch_f
    move-exception v0

    #@10
    .line 225
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@13
    goto :goto_e

    #@14
    .line 221
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_14
    move-exception v0

    #@15
    .line 222
    .local v0, e:Ljava/lang/Exception;
    :try_start_15
    const-string v3, "ContentResolver"

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "Failed to get type for: "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    const-string v5, " ("

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    const-string v5, ")"

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_41
    .catchall {:try_start_15 .. :try_end_41} :catchall_45

    #@41
    .line 225
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@44
    goto :goto_e

    #@45
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_45
    move-exception v3

    #@46
    invoke-virtual {p0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@49
    throw v3

    #@4a
    .line 229
    :cond_4a
    const-string v3, "content"

    #@4c
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v3

    #@54
    if-eqz v3, :cond_e

    #@56
    .line 234
    :try_start_56
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@59
    move-result-object v3

    #@5a
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@5d
    move-result v4

    #@5e
    invoke-interface {v3, p1, v4}, Landroid/app/IActivityManager;->getProviderMimeType(Landroid/net/Uri;I)Ljava/lang/String;
    :try_end_61
    .catch Landroid/os/RemoteException; {:try_start_56 .. :try_end_61} :catch_63
    .catch Ljava/lang/Exception; {:try_start_56 .. :try_end_61} :catch_65

    #@61
    move-result-object v2

    #@62
    .line 236
    .local v2, type:Ljava/lang/String;
    goto :goto_e

    #@63
    .line 237
    .end local v2           #type:Ljava/lang/String;
    :catch_63
    move-exception v0

    #@64
    .line 240
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_e

    #@65
    .line 241
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_65
    move-exception v0

    #@66
    .line 242
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "ContentResolver"

    #@68
    new-instance v4, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v5, "Failed to get type for: "

    #@6f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v4

    #@73
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v4

    #@77
    const-string v5, " ("

    #@79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@80
    move-result-object v5

    #@81
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v4

    #@85
    const-string v5, ")"

    #@87
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v4

    #@8b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v4

    #@8f
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    goto/16 :goto_e
.end method

.method public final insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 15
    .parameter "url"
    .parameter "values"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 860
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@4
    move-result-object v8

    #@5
    .line 861
    .local v8, provider:Landroid/content/IContentProvider;
    if-nez v8, :cond_20

    #@7
    .line 862
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "Unknown URL "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 865
    :cond_20
    :try_start_20
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@23
    move-result-wide v9

    #@24
    .line 866
    .local v9, startTime:J
    invoke-interface {v8, p1, p2}, Landroid/content/IContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@27
    move-result-object v6

    #@28
    .line 867
    .local v6, createdRow:Landroid/net/Uri;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@2b
    move-result-wide v3

    #@2c
    sub-long v1, v3, v9

    #@2e
    .line 868
    .local v1, durationMillis:J
    const-string v4, "insert"

    #@30
    const/4 v5, 0x0

    #@31
    move-object v0, p0

    #@32
    move-object v3, p1

    #@33
    invoke-direct/range {v0 .. v5}, Landroid/content/ContentResolver;->maybeLogUpdateToEventLog(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_36
    .catchall {:try_start_20 .. :try_end_36} :catchall_40
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_36} :catch_3a

    #@36
    .line 875
    invoke-virtual {p0, v8}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@39
    .end local v1           #durationMillis:J
    .end local v6           #createdRow:Landroid/net/Uri;
    .end local v9           #startTime:J
    :goto_39
    return-object v6

    #@3a
    .line 870
    :catch_3a
    move-exception v7

    #@3b
    .line 875
    .local v7, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v8}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@3e
    move-object v6, v11

    #@3f
    goto :goto_39

    #@40
    .end local v7           #e:Landroid/os/RemoteException;
    :catchall_40
    move-exception v0

    #@41
    invoke-virtual {p0, v8}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@44
    throw v0
.end method

.method public notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    .registers 4
    .parameter "uri"
    .parameter "observer"

    #@0
    .prologue
    .line 1266
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    #@4
    .line 1267
    return-void
.end method

.method public notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V
    .registers 5
    .parameter "uri"
    .parameter "observer"
    .parameter "syncToNetwork"

    #@0
    .prologue
    .line 1286
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V

    #@7
    .line 1287
    return-void
.end method

.method public notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;ZI)V
    .registers 11
    .parameter "uri"
    .parameter "observer"
    .parameter "syncToNetwork"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1297
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v0

    #@4
    if-nez p2, :cond_17

    #@6
    const/4 v2, 0x0

    #@7
    :goto_7
    if-eqz p2, :cond_1c

    #@9
    invoke-virtual {p2}, Landroid/database/ContentObserver;->deliverSelfNotifications()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_1c

    #@f
    const/4 v3, 0x1

    #@10
    :goto_10
    move-object v1, p1

    #@11
    move v4, p3

    #@12
    move v5, p4

    #@13
    invoke-interface/range {v0 .. v5}, Landroid/content/IContentService;->notifyChange(Landroid/net/Uri;Landroid/database/IContentObserver;ZZI)V

    #@16
    .line 1303
    :goto_16
    return-void

    #@17
    .line 1297
    :cond_17
    invoke-virtual {p2}, Landroid/database/ContentObserver;->getContentObserver()Landroid/database/IContentObserver;
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_1a} :catch_1e

    #@1a
    move-result-object v2

    #@1b
    goto :goto_7

    #@1c
    :cond_1c
    const/4 v3, 0x0

    #@1d
    goto :goto_10

    #@1e
    .line 1301
    :catch_1e
    move-exception v0

    #@1f
    goto :goto_16
.end method

.method public final openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .registers 16
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 597
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@4
    move-result-object v10

    #@5
    .line 598
    .local v10, scheme:Ljava/lang/String;
    const-string v2, "android.resource"

    #@7
    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_56

    #@d
    .line 599
    const-string/jumbo v0, "r"

    #@10
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_2f

    #@16
    .line 600
    new-instance v0, Ljava/io/FileNotFoundException;

    #@18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "Can\'t write resources: "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-direct {v0, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v0

    #@2f
    .line 602
    :cond_2f
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->getResourceId(Landroid/net/Uri;)Landroid/content/ContentResolver$OpenResourceIdResult;

    #@32
    move-result-object v9

    #@33
    .line 604
    .local v9, r:Landroid/content/ContentResolver$OpenResourceIdResult;
    :try_start_33
    iget-object v0, v9, Landroid/content/ContentResolver$OpenResourceIdResult;->r:Landroid/content/res/Resources;

    #@35
    iget v2, v9, Landroid/content/ContentResolver$OpenResourceIdResult;->id:I

    #@37
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;
    :try_end_3a
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_33 .. :try_end_3a} :catch_3c

    #@3a
    move-result-object v0

    #@3b
    .line 671
    .end local v9           #r:Landroid/content/ContentResolver$OpenResourceIdResult;
    :cond_3b
    :goto_3b
    return-object v0

    #@3c
    .line 605
    .restart local v9       #r:Landroid/content/ContentResolver$OpenResourceIdResult;
    :catch_3c
    move-exception v7

    #@3d
    .line 606
    .local v7, ex:Landroid/content/res/Resources$NotFoundException;
    new-instance v0, Ljava/io/FileNotFoundException;

    #@3f
    new-instance v2, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v3, "Resource does not exist: "

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-direct {v0, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@55
    throw v0

    #@56
    .line 608
    .end local v7           #ex:Landroid/content/res/Resources$NotFoundException;
    .end local v9           #r:Landroid/content/ContentResolver$OpenResourceIdResult;
    :cond_56
    const-string v2, "file"

    #@58
    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v2

    #@5c
    if-eqz v2, :cond_79

    #@5e
    .line 609
    new-instance v0, Ljava/io/File;

    #@60
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@63
    move-result-object v2

    #@64
    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@67
    invoke-static {p1, p2}, Landroid/content/ContentResolver;->modeToMode(Landroid/net/Uri;Ljava/lang/String;)I

    #@6a
    move-result v2

    #@6b
    invoke-static {v0, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@6e
    move-result-object v1

    #@6f
    .line 611
    .local v1, pfd:Landroid/os/ParcelFileDescriptor;
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    #@71
    const-wide/16 v2, 0x0

    #@73
    const-wide/16 v4, -0x1

    #@75
    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V

    #@78
    goto :goto_3b

    #@79
    .line 613
    .end local v1           #pfd:Landroid/os/ParcelFileDescriptor;
    :cond_79
    const-string/jumbo v2, "r"

    #@7c
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v2

    #@80
    if-eqz v2, :cond_89

    #@82
    .line 614
    const-string v2, "*/*"

    #@84
    invoke-virtual {p0, p1, v2, v0}, Landroid/content/ContentResolver;->openTypedAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;

    #@87
    move-result-object v0

    #@88
    goto :goto_3b

    #@89
    .line 616
    :cond_89
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireUnstableProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@8c
    move-result-object v12

    #@8d
    .line 617
    .local v12, unstableProvider:Landroid/content/IContentProvider;
    if-nez v12, :cond_a8

    #@8f
    .line 618
    new-instance v0, Ljava/io/FileNotFoundException;

    #@91
    new-instance v2, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v3, "No content provider: "

    #@98
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v2

    #@9c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v2

    #@a0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v2

    #@a4
    invoke-direct {v0, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@a7
    throw v0

    #@a8
    .line 620
    :cond_a8
    const/4 v11, 0x0

    #@a9
    .line 621
    .local v11, stableProvider:Landroid/content/IContentProvider;
    const/4 v8, 0x0

    #@aa
    .line 625
    .local v8, fd:Landroid/content/res/AssetFileDescriptor;
    :try_start_aa
    invoke-interface {v12, p1, p2}, Landroid/content/IContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_ad
    .catchall {:try_start_aa .. :try_end_ad} :catchall_f8
    .catch Landroid/os/DeadObjectException; {:try_start_aa .. :try_end_ad} :catch_bb
    .catch Landroid/os/RemoteException; {:try_start_aa .. :try_end_ad} :catch_de
    .catch Ljava/io/FileNotFoundException; {:try_start_aa .. :try_end_ad} :catch_142

    #@ad
    move-result-object v8

    #@ae
    .line 626
    if-nez v8, :cond_116

    #@b0
    .line 667
    if-eqz v11, :cond_b5

    #@b2
    .line 668
    invoke-virtual {p0, v11}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@b5
    .line 670
    :cond_b5
    if-eqz v12, :cond_3b

    #@b7
    .line 671
    invoke-virtual {p0, v12}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@ba
    goto :goto_3b

    #@bb
    .line 630
    :catch_bb
    move-exception v6

    #@bc
    .line 634
    .local v6, e:Landroid/os/DeadObjectException;
    :try_start_bc
    invoke-virtual {p0, v12}, Landroid/content/ContentResolver;->unstableProviderDied(Landroid/content/IContentProvider;)V

    #@bf
    .line 635
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@c2
    move-result-object v11

    #@c3
    .line 636
    if-nez v11, :cond_104

    #@c5
    .line 637
    new-instance v0, Ljava/io/FileNotFoundException;

    #@c7
    new-instance v2, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v3, "No content provider: "

    #@ce
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v2

    #@d2
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v2

    #@d6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v2

    #@da
    invoke-direct {v0, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@dd
    throw v0
    :try_end_de
    .catchall {:try_start_bc .. :try_end_de} :catchall_f8
    .catch Landroid/os/RemoteException; {:try_start_bc .. :try_end_de} :catch_de
    .catch Ljava/io/FileNotFoundException; {:try_start_bc .. :try_end_de} :catch_142

    #@de
    .line 660
    .end local v6           #e:Landroid/os/DeadObjectException;
    :catch_de
    move-exception v6

    #@df
    .line 662
    .local v6, e:Landroid/os/RemoteException;
    :try_start_df
    new-instance v0, Ljava/io/FileNotFoundException;

    #@e1
    new-instance v2, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v3, "Failed opening content provider: "

    #@e8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v2

    #@ec
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v2

    #@f0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f3
    move-result-object v2

    #@f4
    invoke-direct {v0, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@f7
    throw v0
    :try_end_f8
    .catchall {:try_start_df .. :try_end_f8} :catchall_f8

    #@f8
    .line 667
    .end local v6           #e:Landroid/os/RemoteException;
    :catchall_f8
    move-exception v0

    #@f9
    if-eqz v11, :cond_fe

    #@fb
    .line 668
    invoke-virtual {p0, v11}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@fe
    .line 670
    :cond_fe
    if-eqz v12, :cond_103

    #@100
    .line 671
    invoke-virtual {p0, v12}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@103
    :cond_103
    throw v0

    #@104
    .line 639
    .local v6, e:Landroid/os/DeadObjectException;
    :cond_104
    :try_start_104
    invoke-interface {v11, p1, p2}, Landroid/content/IContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_107
    .catchall {:try_start_104 .. :try_end_107} :catchall_f8
    .catch Landroid/os/RemoteException; {:try_start_104 .. :try_end_107} :catch_de
    .catch Ljava/io/FileNotFoundException; {:try_start_104 .. :try_end_107} :catch_142

    #@107
    move-result-object v8

    #@108
    .line 640
    if-nez v8, :cond_116

    #@10a
    .line 667
    if-eqz v11, :cond_10f

    #@10c
    .line 668
    invoke-virtual {p0, v11}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@10f
    .line 670
    :cond_10f
    if-eqz v12, :cond_3b

    #@111
    .line 671
    invoke-virtual {p0, v12}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@114
    goto/16 :goto_3b

    #@116
    .line 646
    .end local v6           #e:Landroid/os/DeadObjectException;
    :cond_116
    if-nez v11, :cond_11c

    #@118
    .line 647
    :try_start_118
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@11b
    move-result-object v11

    #@11c
    .line 649
    :cond_11c
    invoke-virtual {p0, v12}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@11f
    .line 650
    new-instance v1, Landroid/content/ContentResolver$ParcelFileDescriptorInner;

    #@121
    invoke-virtual {v8}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@124
    move-result-object v0

    #@125
    invoke-direct {v1, p0, v0, v11}, Landroid/content/ContentResolver$ParcelFileDescriptorInner;-><init>(Landroid/content/ContentResolver;Landroid/os/ParcelFileDescriptor;Landroid/content/IContentProvider;)V

    #@128
    .line 655
    .restart local v1       #pfd:Landroid/os/ParcelFileDescriptor;
    const/4 v11, 0x0

    #@129
    .line 657
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    #@12b
    invoke-virtual {v8}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@12e
    move-result-wide v2

    #@12f
    invoke-virtual {v8}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    #@132
    move-result-wide v4

    #@133
    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V
    :try_end_136
    .catchall {:try_start_118 .. :try_end_136} :catchall_f8
    .catch Landroid/os/RemoteException; {:try_start_118 .. :try_end_136} :catch_de
    .catch Ljava/io/FileNotFoundException; {:try_start_118 .. :try_end_136} :catch_142

    #@136
    .line 667
    if-eqz v11, :cond_13b

    #@138
    .line 668
    invoke-virtual {p0, v11}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@13b
    .line 670
    :cond_13b
    if-eqz v12, :cond_3b

    #@13d
    .line 671
    invoke-virtual {p0, v12}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@140
    goto/16 :goto_3b

    #@142
    .line 664
    .end local v1           #pfd:Landroid/os/ParcelFileDescriptor;
    :catch_142
    move-exception v6

    #@143
    .line 665
    .local v6, e:Ljava/io/FileNotFoundException;
    :try_start_143
    throw v6
    :try_end_144
    .catchall {:try_start_143 .. :try_end_144} :catchall_f8
.end method

.method public final openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 8
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 527
    invoke-virtual {p0, p1, p2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@3
    move-result-object v0

    #@4
    .line 528
    .local v0, afd:Landroid/content/res/AssetFileDescriptor;
    if-nez v0, :cond_8

    #@6
    .line 529
    const/4 v1, 0x0

    #@7
    .line 534
    :goto_7
    return-object v1

    #@8
    .line 532
    :cond_8
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    #@b
    move-result-wide v1

    #@c
    const-wide/16 v3, 0x0

    #@e
    cmp-long v1, v1, v3

    #@10
    if-gez v1, :cond_17

    #@12
    .line 534
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@15
    move-result-object v1

    #@16
    goto :goto_7

    #@17
    .line 540
    :cond_17
    :try_start_17
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_1a} :catch_22

    #@1a
    .line 544
    :goto_1a
    new-instance v1, Ljava/io/FileNotFoundException;

    #@1c
    const-string v2, "Not a whole file"

    #@1e
    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 541
    :catch_22
    move-exception v1

    #@23
    goto :goto_1a
.end method

.method public final openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    .registers 11
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 433
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@3
    move-result-object v4

    #@4
    .line 434
    .local v4, scheme:Ljava/lang/String;
    const-string v6, "android.resource"

    #@6
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v6

    #@a
    if-eqz v6, :cond_33

    #@c
    .line 437
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->getResourceId(Landroid/net/Uri;)Landroid/content/ContentResolver$OpenResourceIdResult;

    #@f
    move-result-object v3

    #@10
    .line 439
    .local v3, r:Landroid/content/ContentResolver$OpenResourceIdResult;
    :try_start_10
    iget-object v6, v3, Landroid/content/ContentResolver$OpenResourceIdResult;->r:Landroid/content/res/Resources;

    #@12
    iget v7, v3, Landroid/content/ContentResolver$OpenResourceIdResult;->id:I

    #@14
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_17
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_10 .. :try_end_17} :catch_19

    #@17
    move-result-object v5

    #@18
    .line 451
    .end local v3           #r:Landroid/content/ContentResolver$OpenResourceIdResult;
    :goto_18
    return-object v5

    #@19
    .line 441
    .restart local v3       #r:Landroid/content/ContentResolver$OpenResourceIdResult;
    :catch_19
    move-exception v1

    #@1a
    .line 442
    .local v1, ex:Landroid/content/res/Resources$NotFoundException;
    new-instance v6, Ljava/io/FileNotFoundException;

    #@1c
    new-instance v7, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v8, "Resource does not exist: "

    #@23
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v7

    #@2f
    invoke-direct {v6, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@32
    throw v6

    #@33
    .line 444
    .end local v1           #ex:Landroid/content/res/Resources$NotFoundException;
    .end local v3           #r:Landroid/content/ContentResolver$OpenResourceIdResult;
    :cond_33
    const-string v6, "file"

    #@35
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v6

    #@39
    if-eqz v6, :cond_45

    #@3b
    .line 447
    new-instance v5, Ljava/io/FileInputStream;

    #@3d
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@40
    move-result-object v6

    #@41
    invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    #@44
    goto :goto_18

    #@45
    .line 449
    :cond_45
    const-string/jumbo v6, "r"

    #@48
    invoke-virtual {p0, p1, v6}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@4b
    move-result-object v2

    #@4c
    .line 451
    .local v2, fd:Landroid/content/res/AssetFileDescriptor;
    if-eqz v2, :cond_54

    #@4e
    :try_start_4e
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_51} :catch_56

    #@51
    move-result-object v6

    #@52
    :goto_52
    move-object v5, v6

    #@53
    goto :goto_18

    #@54
    :cond_54
    const/4 v6, 0x0

    #@55
    goto :goto_52

    #@56
    .line 452
    :catch_56
    move-exception v0

    #@57
    .line 453
    .local v0, e:Ljava/io/IOException;
    new-instance v6, Ljava/io/FileNotFoundException;

    #@59
    const-string v7, "Unable to create stream"

    #@5b
    invoke-direct {v6, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@5e
    throw v6
.end method

.method public final openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;
    .registers 3
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 465
    const-string/jumbo v0, "w"

    #@3
    invoke-virtual {p0, p1, v0}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/OutputStream;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public final openOutputStream(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/OutputStream;
    .registers 7
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 489
    invoke-virtual {p0, p1, p2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@3
    move-result-object v1

    #@4
    .line 491
    .local v1, fd:Landroid/content/res/AssetFileDescriptor;
    if-eqz v1, :cond_b

    #@6
    :try_start_6
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->createOutputStream()Ljava/io/FileOutputStream;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_9} :catch_d

    #@9
    move-result-object v2

    #@a
    :goto_a
    return-object v2

    #@b
    :cond_b
    const/4 v2, 0x0

    #@c
    goto :goto_a

    #@d
    .line 492
    :catch_d
    move-exception v0

    #@e
    .line 493
    .local v0, e:Ljava/io/IOException;
    new-instance v2, Ljava/io/FileNotFoundException;

    #@10
    const-string v3, "Unable to create stream"

    #@12
    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@15
    throw v2
.end method

.method public final openTypedAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
    .registers 14
    .parameter "uri"
    .parameter "mimeType"
    .parameter "opts"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 708
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireUnstableProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@4
    move-result-object v9

    #@5
    .line 709
    .local v9, unstableProvider:Landroid/content/IContentProvider;
    if-nez v9, :cond_20

    #@7
    .line 710
    new-instance v0, Ljava/io/FileNotFoundException;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "No content provider: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-direct {v0, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 712
    :cond_20
    const/4 v8, 0x0

    #@21
    .line 713
    .local v8, stableProvider:Landroid/content/IContentProvider;
    const/4 v7, 0x0

    #@22
    .line 717
    .local v7, fd:Landroid/content/res/AssetFileDescriptor;
    :try_start_22
    invoke-interface {v9, p1, p2, p3}, Landroid/content/IContentProvider;->openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_70
    .catch Landroid/os/DeadObjectException; {:try_start_22 .. :try_end_25} :catch_33
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_25} :catch_56
    .catch Ljava/io/FileNotFoundException; {:try_start_22 .. :try_end_25} :catch_b9

    #@25
    move-result-object v7

    #@26
    .line 718
    if-nez v7, :cond_8d

    #@28
    .line 759
    if-eqz v8, :cond_2d

    #@2a
    .line 760
    invoke-virtual {p0, v8}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@2d
    .line 762
    :cond_2d
    if-eqz v9, :cond_32

    #@2f
    .line 763
    invoke-virtual {p0, v9}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@32
    :cond_32
    :goto_32
    return-object v0

    #@33
    .line 722
    :catch_33
    move-exception v6

    #@34
    .line 726
    .local v6, e:Landroid/os/DeadObjectException;
    :try_start_34
    invoke-virtual {p0, v9}, Landroid/content/ContentResolver;->unstableProviderDied(Landroid/content/IContentProvider;)V

    #@37
    .line 727
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@3a
    move-result-object v8

    #@3b
    .line 728
    if-nez v8, :cond_7c

    #@3d
    .line 729
    new-instance v0, Ljava/io/FileNotFoundException;

    #@3f
    new-instance v2, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v3, "No content provider: "

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-direct {v0, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@55
    throw v0
    :try_end_56
    .catchall {:try_start_34 .. :try_end_56} :catchall_70
    .catch Landroid/os/RemoteException; {:try_start_34 .. :try_end_56} :catch_56
    .catch Ljava/io/FileNotFoundException; {:try_start_34 .. :try_end_56} :catch_b9

    #@56
    .line 752
    .end local v6           #e:Landroid/os/DeadObjectException;
    :catch_56
    move-exception v6

    #@57
    .line 754
    .local v6, e:Landroid/os/RemoteException;
    :try_start_57
    new-instance v0, Ljava/io/FileNotFoundException;

    #@59
    new-instance v2, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v3, "Failed opening content provider: "

    #@60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v2

    #@64
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v2

    #@68
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v2

    #@6c
    invoke-direct {v0, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@6f
    throw v0
    :try_end_70
    .catchall {:try_start_57 .. :try_end_70} :catchall_70

    #@70
    .line 759
    .end local v6           #e:Landroid/os/RemoteException;
    :catchall_70
    move-exception v0

    #@71
    if-eqz v8, :cond_76

    #@73
    .line 760
    invoke-virtual {p0, v8}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@76
    .line 762
    :cond_76
    if-eqz v9, :cond_7b

    #@78
    .line 763
    invoke-virtual {p0, v9}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@7b
    :cond_7b
    throw v0

    #@7c
    .line 731
    .local v6, e:Landroid/os/DeadObjectException;
    :cond_7c
    :try_start_7c
    invoke-interface {v8, p1, p2, p3}, Landroid/content/IContentProvider;->openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
    :try_end_7f
    .catchall {:try_start_7c .. :try_end_7f} :catchall_70
    .catch Landroid/os/RemoteException; {:try_start_7c .. :try_end_7f} :catch_56
    .catch Ljava/io/FileNotFoundException; {:try_start_7c .. :try_end_7f} :catch_b9

    #@7f
    move-result-object v7

    #@80
    .line 732
    if-nez v7, :cond_8d

    #@82
    .line 759
    if-eqz v8, :cond_87

    #@84
    .line 760
    invoke-virtual {p0, v8}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@87
    .line 762
    :cond_87
    if-eqz v9, :cond_32

    #@89
    .line 763
    invoke-virtual {p0, v9}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@8c
    goto :goto_32

    #@8d
    .line 738
    .end local v6           #e:Landroid/os/DeadObjectException;
    :cond_8d
    if-nez v8, :cond_93

    #@8f
    .line 739
    :try_start_8f
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@92
    move-result-object v8

    #@93
    .line 741
    :cond_93
    invoke-virtual {p0, v9}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@96
    .line 742
    new-instance v1, Landroid/content/ContentResolver$ParcelFileDescriptorInner;

    #@98
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@9b
    move-result-object v0

    #@9c
    invoke-direct {v1, p0, v0, v8}, Landroid/content/ContentResolver$ParcelFileDescriptorInner;-><init>(Landroid/content/ContentResolver;Landroid/os/ParcelFileDescriptor;Landroid/content/IContentProvider;)V

    #@9f
    .line 747
    .local v1, pfd:Landroid/os/ParcelFileDescriptor;
    const/4 v8, 0x0

    #@a0
    .line 749
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    #@a2
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@a5
    move-result-wide v2

    #@a6
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    #@a9
    move-result-wide v4

    #@aa
    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V
    :try_end_ad
    .catchall {:try_start_8f .. :try_end_ad} :catchall_70
    .catch Landroid/os/RemoteException; {:try_start_8f .. :try_end_ad} :catch_56
    .catch Ljava/io/FileNotFoundException; {:try_start_8f .. :try_end_ad} :catch_b9

    #@ad
    .line 759
    if-eqz v8, :cond_b2

    #@af
    .line 760
    invoke-virtual {p0, v8}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@b2
    .line 762
    :cond_b2
    if-eqz v9, :cond_32

    #@b4
    .line 763
    invoke-virtual {p0, v9}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@b7
    goto/16 :goto_32

    #@b9
    .line 756
    .end local v1           #pfd:Landroid/os/ParcelFileDescriptor;
    :catch_b9
    move-exception v6

    #@ba
    .line 757
    .local v6, e:Ljava/io/FileNotFoundException;
    :try_start_ba
    throw v6
    :try_end_bb
    .catchall {:try_start_ba .. :try_end_bb} :catchall_70
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 13
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    .line 315
    const/4 v6, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object v5, p5

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .registers 30
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"
    .parameter "cancellationSignal"

    #@0
    .prologue
    .line 356
    invoke-virtual/range {p0 .. p1}, Landroid/content/ContentResolver;->acquireUnstableProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@3
    move-result-object v3

    #@4
    .line 357
    .local v3, unstableProvider:Landroid/content/IContentProvider;
    if-nez v3, :cond_9

    #@6
    .line 358
    const/16 v22, 0x0

    #@8
    .line 407
    :cond_8
    :goto_8
    return-object v22

    #@9
    .line 360
    :cond_9
    const/4 v10, 0x0

    #@a
    .line 362
    .local v10, stableProvider:Landroid/content/IContentProvider;
    :try_start_a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@d
    move-result-wide v20

    #@e
    .line 364
    .local v20, startTime:J
    const/4 v9, 0x0

    #@f
    .line 365
    .local v9, remoteCancellationSignal:Landroid/os/ICancellationSignal;
    if-eqz p6, :cond_1d

    #@11
    .line 366
    invoke-virtual/range {p6 .. p6}, Landroid/os/CancellationSignal;->throwIfCanceled()V

    #@14
    .line 367
    invoke-interface {v3}, Landroid/content/IContentProvider;->createCancellationSignal()Landroid/os/ICancellationSignal;

    #@17
    move-result-object v9

    #@18
    .line 368
    move-object/from16 v0, p6

    #@1a
    invoke-virtual {v0, v9}, Landroid/os/CancellationSignal;->setRemote(Landroid/os/ICancellationSignal;)V
    :try_end_1d
    .catchall {:try_start_a .. :try_end_1d} :catchall_b9
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_1d} :catch_a6

    #@1d
    :cond_1d
    move-object/from16 v4, p1

    #@1f
    move-object/from16 v5, p2

    #@21
    move-object/from16 v6, p3

    #@23
    move-object/from16 v7, p4

    #@25
    move-object/from16 v8, p5

    #@27
    .line 372
    :try_start_27
    invoke-interface/range {v3 .. v9}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;
    :try_end_2a
    .catchall {:try_start_27 .. :try_end_2a} :catchall_b9
    .catch Landroid/os/DeadObjectException; {:try_start_27 .. :try_end_2a} :catch_3e
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_2a} :catch_a6

    #@2a
    move-result-object v19

    #@2b
    .line 386
    .local v19, qCursor:Landroid/database/Cursor;
    :goto_2b
    if-nez v19, :cond_6c

    #@2d
    .line 387
    const/16 v22, 0x0

    #@2f
    .line 403
    if-eqz v3, :cond_36

    #@31
    .line 404
    move-object/from16 v0, p0

    #@33
    invoke-virtual {v0, v3}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@36
    .line 406
    :cond_36
    if-eqz v10, :cond_8

    #@38
    .line 407
    move-object/from16 v0, p0

    #@3a
    invoke-virtual {v0, v10}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@3d
    goto :goto_8

    #@3e
    .line 374
    .end local v19           #qCursor:Landroid/database/Cursor;
    :catch_3e
    move-exception v18

    #@3f
    .line 378
    .local v18, e:Landroid/os/DeadObjectException;
    :try_start_3f
    move-object/from16 v0, p0

    #@41
    invoke-virtual {v0, v3}, Landroid/content/ContentResolver;->unstableProviderDied(Landroid/content/IContentProvider;)V

    #@44
    .line 379
    invoke-virtual/range {p0 .. p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;
    :try_end_47
    .catchall {:try_start_3f .. :try_end_47} :catchall_b9
    .catch Landroid/os/RemoteException; {:try_start_3f .. :try_end_47} :catch_a6

    #@47
    move-result-object v10

    #@48
    .line 380
    if-nez v10, :cond_5b

    #@4a
    .line 381
    const/16 v22, 0x0

    #@4c
    .line 403
    if-eqz v3, :cond_53

    #@4e
    .line 404
    move-object/from16 v0, p0

    #@50
    invoke-virtual {v0, v3}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@53
    .line 406
    :cond_53
    if-eqz v10, :cond_8

    #@55
    .line 407
    move-object/from16 v0, p0

    #@57
    invoke-virtual {v0, v10}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@5a
    goto :goto_8

    #@5b
    :cond_5b
    move-object/from16 v11, p1

    #@5d
    move-object/from16 v12, p2

    #@5f
    move-object/from16 v13, p3

    #@61
    move-object/from16 v14, p4

    #@63
    move-object/from16 v15, p5

    #@65
    move-object/from16 v16, v9

    #@67
    .line 383
    :try_start_67
    invoke-interface/range {v10 .. v16}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@6a
    move-result-object v19

    #@6b
    .restart local v19       #qCursor:Landroid/database/Cursor;
    goto :goto_2b

    #@6c
    .line 390
    .end local v18           #e:Landroid/os/DeadObjectException;
    :cond_6c
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    #@6f
    .line 391
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@72
    move-result-wide v4

    #@73
    sub-long v12, v4, v20

    #@75
    .local v12, durationMillis:J
    move-object/from16 v11, p0

    #@77
    move-object/from16 v14, p1

    #@79
    move-object/from16 v15, p2

    #@7b
    move-object/from16 v16, p3

    #@7d
    move-object/from16 v17, p5

    #@7f
    .line 392
    invoke-direct/range {v11 .. v17}, Landroid/content/ContentResolver;->maybeLogQueryToEventLog(JLandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@82
    .line 394
    new-instance v22, Landroid/content/ContentResolver$CursorWrapperInner;

    #@84
    if-eqz v10, :cond_a1

    #@86
    move-object v4, v10

    #@87
    :goto_87
    move-object/from16 v0, v22

    #@89
    move-object/from16 v1, p0

    #@8b
    move-object/from16 v2, v19

    #@8d
    invoke-direct {v0, v1, v2, v4}, Landroid/content/ContentResolver$CursorWrapperInner;-><init>(Landroid/content/ContentResolver;Landroid/database/Cursor;Landroid/content/IContentProvider;)V
    :try_end_90
    .catchall {:try_start_67 .. :try_end_90} :catchall_b9
    .catch Landroid/os/RemoteException; {:try_start_67 .. :try_end_90} :catch_a6

    #@90
    .line 396
    .local v22, wrapper:Landroid/content/ContentResolver$CursorWrapperInner;
    const/4 v10, 0x0

    #@91
    .line 403
    if-eqz v3, :cond_98

    #@93
    .line 404
    move-object/from16 v0, p0

    #@95
    invoke-virtual {v0, v3}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@98
    .line 406
    :cond_98
    if-eqz v10, :cond_8

    #@9a
    .line 407
    move-object/from16 v0, p0

    #@9c
    invoke-virtual {v0, v10}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@9f
    goto/16 :goto_8

    #@a1
    .line 394
    .end local v22           #wrapper:Landroid/content/ContentResolver$CursorWrapperInner;
    :cond_a1
    :try_start_a1
    invoke-virtual/range {p0 .. p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;
    :try_end_a4
    .catchall {:try_start_a1 .. :try_end_a4} :catchall_b9
    .catch Landroid/os/RemoteException; {:try_start_a1 .. :try_end_a4} :catch_a6

    #@a4
    move-result-object v4

    #@a5
    goto :goto_87

    #@a6
    .line 398
    .end local v9           #remoteCancellationSignal:Landroid/os/ICancellationSignal;
    .end local v12           #durationMillis:J
    .end local v19           #qCursor:Landroid/database/Cursor;
    .end local v20           #startTime:J
    :catch_a6
    move-exception v18

    #@a7
    .line 401
    .local v18, e:Landroid/os/RemoteException;
    const/16 v22, 0x0

    #@a9
    .line 403
    if-eqz v3, :cond_b0

    #@ab
    .line 404
    move-object/from16 v0, p0

    #@ad
    invoke-virtual {v0, v3}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@b0
    .line 406
    :cond_b0
    if-eqz v10, :cond_8

    #@b2
    .line 407
    move-object/from16 v0, p0

    #@b4
    invoke-virtual {v0, v10}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@b7
    goto/16 :goto_8

    #@b9
    .line 403
    .end local v18           #e:Landroid/os/RemoteException;
    :catchall_b9
    move-exception v4

    #@ba
    if-eqz v3, :cond_c1

    #@bc
    .line 404
    move-object/from16 v0, p0

    #@be
    invoke-virtual {v0, v3}, Landroid/content/ContentResolver;->releaseUnstableProvider(Landroid/content/IContentProvider;)Z

    #@c1
    .line 406
    :cond_c1
    if-eqz v10, :cond_c8

    #@c3
    .line 407
    move-object/from16 v0, p0

    #@c5
    invoke-virtual {v0, v10}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@c8
    :cond_c8
    throw v4
.end method

.method public final registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
    .registers 5
    .parameter "uri"
    .parameter "notifyForDescendents"
    .parameter "observer"

    #@0
    .prologue
    .line 1222
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@7
    .line 1223
    return-void
.end method

.method public final registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
    .registers 7
    .parameter "uri"
    .parameter "notifyForDescendents"
    .parameter "observer"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1230
    :try_start_0
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p3}, Landroid/database/ContentObserver;->getContentObserver()Landroid/database/IContentObserver;

    #@7
    move-result-object v1

    #@8
    invoke-interface {v0, p1, p2, v1, p4}, Landroid/content/IContentService;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/IContentObserver;I)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 1234
    :goto_b
    return-void

    #@c
    .line 1232
    :catch_c
    move-exception v0

    #@d
    goto :goto_b
.end method

.method public abstract releaseProvider(Landroid/content/IContentProvider;)Z
.end method

.method public abstract releaseUnstableProvider(Landroid/content/IContentProvider;)Z
.end method

.method public startSync(Landroid/net/Uri;Landroid/os/Bundle;)V
    .registers 6
    .parameter "uri"
    .parameter "extras"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1325
    const/4 v0, 0x0

    #@1
    .line 1326
    .local v0, account:Landroid/accounts/Account;
    if-eqz p2, :cond_1b

    #@3
    .line 1327
    const-string v2, "account"

    #@5
    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 1328
    .local v1, accountName:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_16

    #@f
    .line 1329
    new-instance v0, Landroid/accounts/Account;

    #@11
    .end local v0           #account:Landroid/accounts/Account;
    const-string v2, "com.google"

    #@13
    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 1331
    .restart local v0       #account:Landroid/accounts/Account;
    :cond_16
    const-string v2, "account"

    #@18
    invoke-virtual {p2, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@1b
    .line 1333
    .end local v1           #accountName:Ljava/lang/String;
    :cond_1b
    if-eqz p1, :cond_25

    #@1d
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    :goto_21
    invoke-static {v0, v2, p2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    #@24
    .line 1334
    return-void

    #@25
    .line 1333
    :cond_25
    const/4 v2, 0x0

    #@26
    goto :goto_21
.end method

.method public final unregisterContentObserver(Landroid/database/ContentObserver;)V
    .registers 4
    .parameter "observer"

    #@0
    .prologue
    .line 1244
    :try_start_0
    invoke-virtual {p1}, Landroid/database/ContentObserver;->releaseContentObserver()Landroid/database/IContentObserver;

    #@3
    move-result-object v0

    #@4
    .line 1245
    .local v0, contentObserver:Landroid/database/IContentObserver;
    if-eqz v0, :cond_d

    #@6
    .line 1246
    invoke-static {}, Landroid/content/ContentResolver;->getContentService()Landroid/content/IContentService;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1, v0}, Landroid/content/IContentService;->unregisterContentObserver(Landroid/database/IContentObserver;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_e

    #@d
    .line 1251
    .end local v0           #contentObserver:Landroid/database/IContentObserver;
    :cond_d
    :goto_d
    return-void

    #@e
    .line 1249
    :catch_e
    move-exception v1

    #@f
    goto :goto_d
.end method

.method public abstract unstableProviderDied(Landroid/content/IContentProvider;)V
.end method

.method public final update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 16
    .parameter "uri"
    .parameter "values"
    .parameter "where"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 986
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireProvider(Landroid/net/Uri;)Landroid/content/IContentProvider;

    #@3
    move-result-object v7

    #@4
    .line 987
    .local v7, provider:Landroid/content/IContentProvider;
    if-nez v7, :cond_1f

    #@6
    .line 988
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "Unknown URI "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 991
    :cond_1f
    :try_start_1f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@22
    move-result-wide v9

    #@23
    .line 992
    .local v9, startTime:J
    invoke-interface {v7, p1, p2, p3, p4}, Landroid/content/IContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@26
    move-result v8

    #@27
    .line 993
    .local v8, rowsUpdated:I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@2a
    move-result-wide v3

    #@2b
    sub-long v1, v3, v9

    #@2d
    .line 994
    .local v1, durationMillis:J
    const-string/jumbo v4, "update"

    #@30
    move-object v0, p0

    #@31
    move-object v3, p1

    #@32
    move-object v5, p3

    #@33
    invoke-direct/range {v0 .. v5}, Landroid/content/ContentResolver;->maybeLogUpdateToEventLog(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_36
    .catchall {:try_start_1f .. :try_end_36} :catchall_40
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_36} :catch_3a

    #@36
    .line 1001
    invoke-virtual {p0, v7}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@39
    .end local v1           #durationMillis:J
    .end local v8           #rowsUpdated:I
    .end local v9           #startTime:J
    :goto_39
    return v8

    #@3a
    .line 996
    :catch_3a
    move-exception v6

    #@3b
    .line 999
    .local v6, e:Landroid/os/RemoteException;
    const/4 v8, -0x1

    #@3c
    .line 1001
    invoke-virtual {p0, v7}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@3f
    goto :goto_39

    #@40
    .end local v6           #e:Landroid/os/RemoteException;
    :catchall_40
    move-exception v0

    #@41
    invoke-virtual {p0, v7}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@44
    throw v0
.end method
