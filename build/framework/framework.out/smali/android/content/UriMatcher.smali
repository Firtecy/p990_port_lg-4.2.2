.class public Landroid/content/UriMatcher;
.super Ljava/lang/Object;
.source "UriMatcher.java"


# static fields
.field private static final EXACT:I = 0x0

.field public static final NO_MATCH:I = -0x1

.field private static final NUMBER:I = 0x1

.field static final PATH_SPLIT_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final TEXT:I = 0x2


# instance fields
.field private mChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/UriMatcher;",
            ">;"
        }
    .end annotation
.end field

.field private mCode:I

.field private mText:Ljava/lang/String;

.field private mWhich:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 192
    const-string v0, "/"

    #@2
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/content/UriMatcher;->PATH_SPLIT_PATTERN:Ljava/util/regex/Pattern;

    #@8
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 134
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 135
    iput v0, p0, Landroid/content/UriMatcher;->mCode:I

    #@6
    .line 136
    iput v0, p0, Landroid/content/UriMatcher;->mWhich:I

    #@8
    .line 137
    new-instance v0, Ljava/util/ArrayList;

    #@a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v0, p0, Landroid/content/UriMatcher;->mChildren:Ljava/util/ArrayList;

    #@f
    .line 138
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Landroid/content/UriMatcher;->mText:Ljava/lang/String;

    #@12
    .line 139
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "code"

    #@0
    .prologue
    .line 126
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 127
    iput p1, p0, Landroid/content/UriMatcher;->mCode:I

    #@5
    .line 128
    const/4 v0, -0x1

    #@6
    iput v0, p0, Landroid/content/UriMatcher;->mWhich:I

    #@8
    .line 129
    new-instance v0, Ljava/util/ArrayList;

    #@a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v0, p0, Landroid/content/UriMatcher;->mChildren:Ljava/util/ArrayList;

    #@f
    .line 130
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Landroid/content/UriMatcher;->mText:Ljava/lang/String;

    #@12
    .line 131
    return-void
.end method


# virtual methods
.method public addURI(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 16
    .parameter "authority"
    .parameter "path"
    .parameter "code"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 155
    if-gez p3, :cond_22

    #@3
    .line 156
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v10, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v11, "code "

    #@c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v10

    #@10
    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v10

    #@14
    const-string v11, " is invalid: it must be positive"

    #@16
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v10

    #@1a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v10

    #@1e
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v9

    #@22
    .line 158
    :cond_22
    if-eqz p2, :cond_69

    #@24
    sget-object v10, Landroid/content/UriMatcher;->PATH_SPLIT_PATTERN:Ljava/util/regex/Pattern;

    #@26
    invoke-virtual {v10, p2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    .line 159
    .local v8, tokens:[Ljava/lang/String;
    :goto_2a
    if-eqz v8, :cond_6b

    #@2c
    array-length v6, v8

    #@2d
    .line 160
    .local v6, numTokens:I
    :goto_2d
    move-object v4, p0

    #@2e
    .line 161
    .local v4, node:Landroid/content/UriMatcher;
    const/4 v2, -0x1

    #@2f
    .local v2, i:I
    :goto_2f
    if-ge v2, v6, :cond_82

    #@31
    .line 162
    if-gez v2, :cond_6d

    #@33
    move-object v7, p1

    #@34
    .line 163
    .local v7, token:Ljava/lang/String;
    :goto_34
    iget-object v1, v4, Landroid/content/UriMatcher;->mChildren:Ljava/util/ArrayList;

    #@36
    .line 164
    .local v1, children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/UriMatcher;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@39
    move-result v5

    #@3a
    .line 167
    .local v5, numChildren:I
    const/4 v3, 0x0

    #@3b
    .local v3, j:I
    :goto_3b
    if-ge v3, v5, :cond_4c

    #@3d
    .line 168
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@40
    move-result-object v0

    #@41
    check-cast v0, Landroid/content/UriMatcher;

    #@43
    .line 169
    .local v0, child:Landroid/content/UriMatcher;
    iget-object v10, v0, Landroid/content/UriMatcher;->mText:Ljava/lang/String;

    #@45
    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v10

    #@49
    if-eqz v10, :cond_70

    #@4b
    .line 170
    move-object v4, v0

    #@4c
    .line 174
    .end local v0           #child:Landroid/content/UriMatcher;
    :cond_4c
    if-ne v3, v5, :cond_66

    #@4e
    .line 176
    new-instance v0, Landroid/content/UriMatcher;

    #@50
    invoke-direct {v0}, Landroid/content/UriMatcher;-><init>()V

    #@53
    .line 177
    .restart local v0       #child:Landroid/content/UriMatcher;
    const-string v10, "#"

    #@55
    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@58
    move-result v10

    #@59
    if-eqz v10, :cond_73

    #@5b
    .line 178
    const/4 v10, 0x1

    #@5c
    iput v10, v0, Landroid/content/UriMatcher;->mWhich:I

    #@5e
    .line 184
    :goto_5e
    iput-object v7, v0, Landroid/content/UriMatcher;->mText:Ljava/lang/String;

    #@60
    .line 185
    iget-object v10, v4, Landroid/content/UriMatcher;->mChildren:Ljava/util/ArrayList;

    #@62
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@65
    .line 186
    move-object v4, v0

    #@66
    .line 161
    .end local v0           #child:Landroid/content/UriMatcher;
    :cond_66
    add-int/lit8 v2, v2, 0x1

    #@68
    goto :goto_2f

    #@69
    .line 158
    .end local v1           #children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/UriMatcher;>;"
    .end local v2           #i:I
    .end local v3           #j:I
    .end local v4           #node:Landroid/content/UriMatcher;
    .end local v5           #numChildren:I
    .end local v6           #numTokens:I
    .end local v7           #token:Ljava/lang/String;
    .end local v8           #tokens:[Ljava/lang/String;
    :cond_69
    const/4 v8, 0x0

    #@6a
    goto :goto_2a

    #@6b
    .restart local v8       #tokens:[Ljava/lang/String;
    :cond_6b
    move v6, v9

    #@6c
    .line 159
    goto :goto_2d

    #@6d
    .line 162
    .restart local v2       #i:I
    .restart local v4       #node:Landroid/content/UriMatcher;
    .restart local v6       #numTokens:I
    :cond_6d
    aget-object v7, v8, v2

    #@6f
    goto :goto_34

    #@70
    .line 167
    .restart local v0       #child:Landroid/content/UriMatcher;
    .restart local v1       #children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/UriMatcher;>;"
    .restart local v3       #j:I
    .restart local v5       #numChildren:I
    .restart local v7       #token:Ljava/lang/String;
    :cond_70
    add-int/lit8 v3, v3, 0x1

    #@72
    goto :goto_3b

    #@73
    .line 179
    :cond_73
    const-string v10, "*"

    #@75
    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@78
    move-result v10

    #@79
    if-eqz v10, :cond_7f

    #@7b
    .line 180
    const/4 v10, 0x2

    #@7c
    iput v10, v0, Landroid/content/UriMatcher;->mWhich:I

    #@7e
    goto :goto_5e

    #@7f
    .line 182
    :cond_7f
    iput v9, v0, Landroid/content/UriMatcher;->mWhich:I

    #@81
    goto :goto_5e

    #@82
    .line 189
    .end local v0           #child:Landroid/content/UriMatcher;
    .end local v1           #children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/UriMatcher;>;"
    .end local v3           #j:I
    .end local v5           #numChildren:I
    .end local v7           #token:Ljava/lang/String;
    :cond_82
    iput p3, v4, Landroid/content/UriMatcher;->mCode:I

    #@84
    .line 190
    return-void
.end method

.method public match(Landroid/net/Uri;)I
    .registers 15
    .parameter "uri"

    #@0
    .prologue
    .line 204
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@3
    move-result-object v10

    #@4
    .line 205
    .local v10, pathSegments:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@7
    move-result v4

    #@8
    .line 207
    .local v4, li:I
    move-object v9, p0

    #@9
    .line 209
    .local v9, node:Landroid/content/UriMatcher;
    if-nez v4, :cond_14

    #@b
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@e
    move-result-object v12

    #@f
    if-nez v12, :cond_14

    #@11
    .line 210
    iget v12, p0, Landroid/content/UriMatcher;->mCode:I

    #@13
    .line 253
    :goto_13
    return v12

    #@14
    .line 213
    :cond_14
    const/4 v1, -0x1

    #@15
    .local v1, i:I
    :goto_15
    if-ge v1, v4, :cond_21

    #@17
    .line 214
    if-gez v1, :cond_24

    #@19
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@1c
    move-result-object v11

    #@1d
    .line 215
    .local v11, u:Ljava/lang/String;
    :goto_1d
    iget-object v5, v9, Landroid/content/UriMatcher;->mChildren:Ljava/util/ArrayList;

    #@1f
    .line 216
    .local v5, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/UriMatcher;>;"
    if-nez v5, :cond_2c

    #@21
    .line 253
    .end local v5           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/UriMatcher;>;"
    .end local v11           #u:Ljava/lang/String;
    :cond_21
    iget v12, v9, Landroid/content/UriMatcher;->mCode:I

    #@23
    goto :goto_13

    #@24
    .line 214
    :cond_24
    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@27
    move-result-object v12

    #@28
    check-cast v12, Ljava/lang/String;

    #@2a
    move-object v11, v12

    #@2b
    goto :goto_1d

    #@2c
    .line 219
    .restart local v5       #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/UriMatcher;>;"
    .restart local v11       #u:Ljava/lang/String;
    :cond_2c
    const/4 v9, 0x0

    #@2d
    .line 220
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@30
    move-result v6

    #@31
    .line 221
    .local v6, lj:I
    const/4 v2, 0x0

    #@32
    .local v2, j:I
    :goto_32
    if-ge v2, v6, :cond_41

    #@34
    .line 222
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@37
    move-result-object v8

    #@38
    check-cast v8, Landroid/content/UriMatcher;

    #@3a
    .line 224
    .local v8, n:Landroid/content/UriMatcher;
    iget v12, v8, Landroid/content/UriMatcher;->mWhich:I

    #@3c
    packed-switch v12, :pswitch_data_70

    #@3f
    .line 244
    :cond_3f
    :goto_3f
    if-eqz v9, :cond_69

    #@41
    .line 248
    .end local v8           #n:Landroid/content/UriMatcher;
    :cond_41
    if-nez v9, :cond_6c

    #@43
    .line 249
    const/4 v12, -0x1

    #@44
    goto :goto_13

    #@45
    .line 226
    .restart local v8       #n:Landroid/content/UriMatcher;
    :pswitch_45
    iget-object v12, v8, Landroid/content/UriMatcher;->mText:Ljava/lang/String;

    #@47
    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v12

    #@4b
    if-eqz v12, :cond_3f

    #@4d
    .line 227
    move-object v9, v8

    #@4e
    goto :goto_3f

    #@4f
    .line 231
    :pswitch_4f
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@52
    move-result v7

    #@53
    .line 232
    .local v7, lk:I
    const/4 v3, 0x0

    #@54
    .local v3, k:I
    :goto_54
    if-ge v3, v7, :cond_65

    #@56
    .line 233
    invoke-virtual {v11, v3}, Ljava/lang/String;->charAt(I)C

    #@59
    move-result v0

    #@5a
    .line 234
    .local v0, c:C
    const/16 v12, 0x30

    #@5c
    if-lt v0, v12, :cond_3f

    #@5e
    const/16 v12, 0x39

    #@60
    if-gt v0, v12, :cond_3f

    #@62
    .line 232
    add-int/lit8 v3, v3, 0x1

    #@64
    goto :goto_54

    #@65
    .line 238
    .end local v0           #c:C
    :cond_65
    move-object v9, v8

    #@66
    .line 239
    goto :goto_3f

    #@67
    .line 241
    .end local v3           #k:I
    .end local v7           #lk:I
    :pswitch_67
    move-object v9, v8

    #@68
    goto :goto_3f

    #@69
    .line 221
    :cond_69
    add-int/lit8 v2, v2, 0x1

    #@6b
    goto :goto_32

    #@6c
    .line 213
    .end local v8           #n:Landroid/content/UriMatcher;
    :cond_6c
    add-int/lit8 v1, v1, 0x1

    #@6e
    goto :goto_15

    #@6f
    .line 224
    nop

    #@70
    :pswitch_data_70
    .packed-switch 0x0
        :pswitch_45
        :pswitch_4f
        :pswitch_67
    .end packed-switch
.end method
