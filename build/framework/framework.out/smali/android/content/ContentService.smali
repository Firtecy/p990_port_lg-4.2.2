.class public final Landroid/content/ContentService;
.super Landroid/content/IContentService$Stub;
.source "ContentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/ContentService$ObserverNode;,
        Landroid/content/ContentService$ObserverCall;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ContentService"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFactoryTest:Z

.field private final mRootNode:Landroid/content/ContentService$ObserverNode;

.field private mSyncManager:Landroid/content/SyncManager;

.field private final mSyncManagerLock:Ljava/lang/Object;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .registers 5
    .parameter "context"
    .parameter "factoryTest"

    #@0
    .prologue
    .line 134
    invoke-direct {p0}, Landroid/content/IContentService$Stub;-><init>()V

    #@3
    .line 50
    new-instance v0, Landroid/content/ContentService$ObserverNode;

    #@5
    const-string v1, ""

    #@7
    invoke-direct {v0, v1}, Landroid/content/ContentService$ObserverNode;-><init>(Ljava/lang/String;)V

    #@a
    iput-object v0, p0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@c
    .line 51
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Landroid/content/ContentService;->mSyncManager:Landroid/content/SyncManager;

    #@f
    .line 52
    new-instance v0, Ljava/lang/Object;

    #@11
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@14
    iput-object v0, p0, Landroid/content/ContentService;->mSyncManagerLock:Ljava/lang/Object;

    #@16
    .line 135
    iput-object p1, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@18
    .line 136
    iput-boolean p2, p0, Landroid/content/ContentService;->mFactoryTest:Z

    #@1a
    .line 137
    return-void
.end method

.method private getSyncManager()Landroid/content/SyncManager;
    .registers 6

    #@0
    .prologue
    .line 55
    iget-object v2, p0, Landroid/content/ContentService;->mSyncManagerLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 58
    :try_start_3
    iget-object v1, p0, Landroid/content/ContentService;->mSyncManager:Landroid/content/SyncManager;

    #@5
    if-nez v1, :cond_12

    #@7
    new-instance v1, Landroid/content/SyncManager;

    #@9
    iget-object v3, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@b
    iget-boolean v4, p0, Landroid/content/ContentService;->mFactoryTest:Z

    #@d
    invoke-direct {v1, v3, v4}, Landroid/content/SyncManager;-><init>(Landroid/content/Context;Z)V

    #@10
    iput-object v1, p0, Landroid/content/ContentService;->mSyncManager:Landroid/content/SyncManager;
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_1f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_12} :catch_16

    #@12
    .line 62
    :cond_12
    :goto_12
    :try_start_12
    iget-object v1, p0, Landroid/content/ContentService;->mSyncManager:Landroid/content/SyncManager;

    #@14
    monitor-exit v2

    #@15
    return-object v1

    #@16
    .line 59
    :catch_16
    move-exception v0

    #@17
    .line 60
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    const-string v1, "ContentService"

    #@19
    const-string v3, "Can\'t create SyncManager"

    #@1b
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    goto :goto_12

    #@1f
    .line 63
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_12 .. :try_end_21} :catchall_1f

    #@21
    throw v1
.end method

.method public static main(Landroid/content/Context;Z)Landroid/content/ContentService;
    .registers 4
    .parameter "context"
    .parameter "factoryTest"

    #@0
    .prologue
    .line 605
    new-instance v0, Landroid/content/ContentService;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/content/ContentService;-><init>(Landroid/content/Context;Z)V

    #@5
    .line 606
    .local v0, service:Landroid/content/ContentService;
    const-string v1, "content"

    #@7
    invoke-static {v1, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@a
    .line 607
    return-object v0
.end method


# virtual methods
.method public addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V
    .registers 15
    .parameter "account"
    .parameter "authority"
    .parameter "extras"
    .parameter "pollFrequency"

    #@0
    .prologue
    .line 395
    iget-object v0, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.WRITE_SYNC_SETTINGS"

    #@4
    const-string/jumbo v3, "no permission to write the sync settings"

    #@7
    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 397
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v2

    #@e
    .line 399
    .local v2, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v7

    #@12
    .line 401
    .local v7, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@19
    move-result-object v0

    #@1a
    move-object v1, p1

    #@1b
    move-object v3, p2

    #@1c
    move-object v4, p3

    #@1d
    move-wide v5, p4

    #@1e
    invoke-virtual/range {v0 .. v6}, Landroid/content/SyncStorageEngine;->addPeriodicSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;J)V
    :try_end_21
    .catchall {:try_start_12 .. :try_end_21} :catchall_25

    #@21
    .line 404
    invoke-static {v7, v8}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@24
    .line 406
    return-void

    #@25
    .line 404
    :catchall_25
    move-exception v0

    #@26
    invoke-static {v7, v8}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@29
    throw v0
.end method

.method public addStatusChangeListener(ILandroid/content/ISyncStatusObserver;)V
    .registers 7
    .parameter "mask"
    .parameter "callback"

    #@0
    .prologue
    .line 581
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 583
    .local v0, identityToken:J
    :try_start_4
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@7
    move-result-object v2

    #@8
    .line 584
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_13

    #@a
    if-eqz p2, :cond_13

    #@c
    .line 585
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3, p1, p2}, Landroid/content/SyncStorageEngine;->addStatusChangeListener(ILandroid/content/ISyncStatusObserver;)V
    :try_end_13
    .catchall {:try_start_4 .. :try_end_13} :catchall_17

    #@13
    .line 588
    :cond_13
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@16
    .line 590
    return-void

    #@17
    .line 588
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_17
    move-exception v3

    #@18
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@1b
    throw v3
.end method

.method public cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V
    .registers 8
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 325
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v3

    #@4
    .line 329
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@7
    move-result-wide v0

    #@8
    .line 331
    .local v0, identityToken:J
    :try_start_8
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@b
    move-result-object v2

    #@c
    .line 332
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_14

    #@e
    .line 333
    invoke-virtual {v2, p1, v3, p2}, Landroid/content/SyncManager;->clearScheduledSyncOperations(Landroid/accounts/Account;ILjava/lang/String;)V

    #@11
    .line 334
    invoke-virtual {v2, p1, v3, p2}, Landroid/content/SyncManager;->cancelActiveSync(Landroid/accounts/Account;ILjava/lang/String;)V
    :try_end_14
    .catchall {:try_start_8 .. :try_end_14} :catchall_18

    #@14
    .line 337
    :cond_14
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@17
    .line 339
    return-void

    #@18
    .line 337
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_18
    move-exception v4

    #@19
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@1c
    throw v4
.end method

.method protected declared-synchronized dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 20
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 68
    monitor-enter p0

    #@1
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v2, v0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@5
    const-string v3, "android.permission.DUMP"

    #@7
    const-string v4, "caller doesn\'t have the DUMP permission"

    #@9
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 73
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_73

    #@f
    move-result-wide v11

    #@10
    .line 75
    .local v11, identityToken:J
    :try_start_10
    move-object/from16 v0, p0

    #@12
    iget-object v2, v0, Landroid/content/ContentService;->mSyncManager:Landroid/content/SyncManager;

    #@14
    if-nez v2, :cond_62

    #@16
    .line 76
    const-string v2, "No SyncManager created!  (Disk full?)"

    #@18
    move-object/from16 v0, p2

    #@1a
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d
    .line 80
    :goto_1d
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@20
    .line 81
    const-string v2, "Observer tree:"

    #@22
    move-object/from16 v0, p2

    #@24
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@27
    .line 82
    move-object/from16 v0, p0

    #@29
    iget-object v15, v0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@2b
    monitor-enter v15
    :try_end_2c
    .catchall {:try_start_10 .. :try_end_2c} :catchall_6e

    #@2c
    .line 83
    const/4 v2, 0x2

    #@2d
    :try_start_2d
    new-array v8, v2, [I

    #@2f
    .line 84
    .local v8, counts:[I
    new-instance v9, Landroid/util/SparseIntArray;

    #@31
    invoke-direct {v9}, Landroid/util/SparseIntArray;-><init>()V

    #@34
    .line 85
    .local v9, pidCounts:Landroid/util/SparseIntArray;
    move-object/from16 v0, p0

    #@36
    iget-object v2, v0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@38
    const-string v6, ""

    #@3a
    const-string v7, "  "

    #@3c
    move-object/from16 v3, p1

    #@3e
    move-object/from16 v4, p2

    #@40
    move-object/from16 v5, p3

    #@42
    invoke-virtual/range {v2 .. v9}, Landroid/content/ContentService$ObserverNode;->dumpLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[ILandroid/util/SparseIntArray;)V

    #@45
    .line 86
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@48
    .line 87
    new-instance v14, Ljava/util/ArrayList;

    #@4a
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    #@4d
    .line 88
    .local v14, sorted:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v10, 0x0

    #@4e
    .local v10, i:I
    :goto_4e
    invoke-virtual {v9}, Landroid/util/SparseIntArray;->size()I

    #@51
    move-result v2

    #@52
    if-ge v10, v2, :cond_76

    #@54
    .line 89
    invoke-virtual {v9, v10}, Landroid/util/SparseIntArray;->keyAt(I)I

    #@57
    move-result v2

    #@58
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5f
    .catchall {:try_start_2d .. :try_end_5f} :catchall_de

    #@5f
    .line 88
    add-int/lit8 v10, v10, 0x1

    #@61
    goto :goto_4e

    #@62
    .line 78
    .end local v8           #counts:[I
    .end local v9           #pidCounts:Landroid/util/SparseIntArray;
    .end local v10           #i:I
    .end local v14           #sorted:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_62
    :try_start_62
    move-object/from16 v0, p0

    #@64
    iget-object v2, v0, Landroid/content/ContentService;->mSyncManager:Landroid/content/SyncManager;

    #@66
    move-object/from16 v0, p1

    #@68
    move-object/from16 v1, p2

    #@6a
    invoke-virtual {v2, v0, v1}, Landroid/content/SyncManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    :try_end_6d
    .catchall {:try_start_62 .. :try_end_6d} :catchall_6e

    #@6d
    goto :goto_1d

    #@6e
    .line 115
    :catchall_6e
    move-exception v2

    #@6f
    :try_start_6f
    invoke-static {v11, v12}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@72
    throw v2
    :try_end_73
    .catchall {:try_start_6f .. :try_end_73} :catchall_73

    #@73
    .line 68
    .end local v11           #identityToken:J
    :catchall_73
    move-exception v2

    #@74
    monitor-exit p0

    #@75
    throw v2

    #@76
    .line 91
    .restart local v8       #counts:[I
    .restart local v9       #pidCounts:Landroid/util/SparseIntArray;
    .restart local v10       #i:I
    .restart local v11       #identityToken:J
    .restart local v14       #sorted:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_76
    :try_start_76
    new-instance v2, Landroid/content/ContentService$1;

    #@78
    move-object/from16 v0, p0

    #@7a
    invoke-direct {v2, v0, v9}, Landroid/content/ContentService$1;-><init>(Landroid/content/ContentService;Landroid/util/SparseIntArray;)V

    #@7d
    invoke-static {v14, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@80
    .line 105
    const/4 v10, 0x0

    #@81
    :goto_81
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@84
    move-result v2

    #@85
    if-ge v10, v2, :cond_b7

    #@87
    .line 106
    invoke-virtual {v14, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8a
    move-result-object v2

    #@8b
    check-cast v2, Ljava/lang/Integer;

    #@8d
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@90
    move-result v13

    #@91
    .line 107
    .local v13, pid:I
    const-string v2, "  pid "

    #@93
    move-object/from16 v0, p2

    #@95
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@98
    move-object/from16 v0, p2

    #@9a
    invoke-virtual {v0, v13}, Ljava/io/PrintWriter;->print(I)V

    #@9d
    const-string v2, ": "

    #@9f
    move-object/from16 v0, p2

    #@a1
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a4
    .line 108
    invoke-virtual {v9, v13}, Landroid/util/SparseIntArray;->get(I)I

    #@a7
    move-result v2

    #@a8
    move-object/from16 v0, p2

    #@aa
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(I)V

    #@ad
    const-string v2, " observers"

    #@af
    move-object/from16 v0, p2

    #@b1
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b4
    .line 105
    add-int/lit8 v10, v10, 0x1

    #@b6
    goto :goto_81

    #@b7
    .line 110
    .end local v13           #pid:I
    :cond_b7
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    #@ba
    .line 111
    const-string v2, " Total number of nodes: "

    #@bc
    move-object/from16 v0, p2

    #@be
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c1
    const/4 v2, 0x0

    #@c2
    aget v2, v8, v2

    #@c4
    move-object/from16 v0, p2

    #@c6
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(I)V

    #@c9
    .line 112
    const-string v2, " Total number of observers: "

    #@cb
    move-object/from16 v0, p2

    #@cd
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d0
    const/4 v2, 0x1

    #@d1
    aget v2, v8, v2

    #@d3
    move-object/from16 v0, p2

    #@d5
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(I)V

    #@d8
    .line 113
    monitor-exit v15
    :try_end_d9
    .catchall {:try_start_76 .. :try_end_d9} :catchall_de

    #@d9
    .line 115
    :try_start_d9
    invoke-static {v11, v12}, Landroid/content/ContentService;->restoreCallingIdentity(J)V
    :try_end_dc
    .catchall {:try_start_d9 .. :try_end_dc} :catchall_73

    #@dc
    .line 117
    monitor-exit p0

    #@dd
    return-void

    #@de
    .line 113
    .end local v8           #counts:[I
    .end local v9           #pidCounts:Landroid/util/SparseIntArray;
    .end local v10           #i:I
    .end local v14           #sorted:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catchall_de
    move-exception v2

    #@df
    :try_start_df
    monitor-exit v15
    :try_end_e0
    .catchall {:try_start_df .. :try_end_e0} :catchall_de

    #@e0
    :try_start_e0
    throw v2
    :try_end_e1
    .catchall {:try_start_e0 .. :try_end_e1} :catchall_6e
.end method

.method public getCurrentSyncs()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/SyncInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 533
    iget-object v3, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.READ_SYNC_STATS"

    #@4
    const-string/jumbo v5, "no permission to read the sync stats"

    #@7
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 535
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v2

    #@e
    .line 537
    .local v2, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 539
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, v2}, Landroid/content/SyncStorageEngine;->getCurrentSyncs(I)Ljava/util/List;
    :try_end_1d
    .catchall {:try_start_12 .. :try_end_1d} :catchall_22

    #@1d
    move-result-object v3

    #@1e
    .line 541
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@21
    return-object v3

    #@22
    :catchall_22
    move-exception v3

    #@23
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@26
    throw v3
.end method

.method public getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I
    .registers 10
    .parameter "account"
    .parameter "providerName"

    #@0
    .prologue
    .line 437
    iget-object v4, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.READ_SYNC_SETTINGS"

    #@4
    const-string/jumbo v6, "no permission to read the sync settings"

    #@7
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 439
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v3

    #@e
    .line 441
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 443
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v2

    #@16
    .line 444
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_24

    #@18
    .line 445
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p1, v3, p2}, Landroid/content/SyncStorageEngine;->getIsSyncable(Landroid/accounts/Account;ILjava/lang/String;)I
    :try_end_1f
    .catchall {:try_start_12 .. :try_end_1f} :catchall_29

    #@1f
    move-result v4

    #@20
    .line 449
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@23
    .line 451
    :goto_23
    return v4

    #@24
    .line 449
    :cond_24
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@27
    .line 451
    const/4 v4, -0x1

    #@28
    goto :goto_23

    #@29
    .line 449
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_29
    move-exception v4

    #@2a
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@2d
    throw v4
.end method

.method public getMasterSyncAutomatically()Z
    .registers 8

    #@0
    .prologue
    .line 472
    iget-object v4, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.READ_SYNC_SETTINGS"

    #@4
    const-string/jumbo v6, "no permission to read the sync settings"

    #@7
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 474
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v3

    #@e
    .line 476
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 478
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v2

    #@16
    .line 479
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_24

    #@18
    .line 480
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, v3}, Landroid/content/SyncStorageEngine;->getMasterSyncAutomatically(I)Z
    :try_end_1f
    .catchall {:try_start_12 .. :try_end_1f} :catchall_29

    #@1f
    move-result v4

    #@20
    .line 483
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@23
    .line 485
    :goto_23
    return v4

    #@24
    .line 483
    :cond_24
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@27
    .line 485
    const/4 v4, 0x0

    #@28
    goto :goto_23

    #@29
    .line 483
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_29
    move-exception v4

    #@2a
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@2d
    throw v4
.end method

.method public getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;
    .registers 9
    .parameter "account"
    .parameter "providerName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/PeriodicSync;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 423
    iget-object v3, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.READ_SYNC_SETTINGS"

    #@4
    const-string/jumbo v5, "no permission to read the sync settings"

    #@7
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 425
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v2

    #@e
    .line 427
    .local v2, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 429
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1, v2, p2}, Landroid/content/SyncStorageEngine;->getPeriodicSyncs(Landroid/accounts/Account;ILjava/lang/String;)Ljava/util/List;
    :try_end_1d
    .catchall {:try_start_12 .. :try_end_1d} :catchall_22

    #@1d
    move-result-object v3

    #@1e
    .line 432
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@21
    return-object v3

    #@22
    :catchall_22
    move-exception v3

    #@23
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@26
    throw v3
.end method

.method public getSyncAdapterTypes()[Landroid/content/SyncAdapterType;
    .registers 6

    #@0
    .prologue
    .line 348
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v3

    #@4
    .line 349
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@7
    move-result-wide v0

    #@8
    .line 351
    .local v0, identityToken:J
    :try_start_8
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@b
    move-result-object v2

    #@c
    .line 352
    .local v2, syncManager:Landroid/content/SyncManager;
    invoke-virtual {v2, v3}, Landroid/content/SyncManager;->getSyncAdapterTypes(I)[Landroid/content/SyncAdapterType;
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_14

    #@f
    move-result-object v4

    #@10
    .line 354
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@13
    return-object v4

    #@14
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_14
    move-exception v4

    #@15
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@18
    throw v4
.end method

.method public getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z
    .registers 10
    .parameter "account"
    .parameter "providerName"

    #@0
    .prologue
    .line 359
    iget-object v4, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.READ_SYNC_SETTINGS"

    #@4
    const-string/jumbo v6, "no permission to read the sync settings"

    #@7
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 361
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v3

    #@e
    .line 363
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 365
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v2

    #@16
    .line 366
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_24

    #@18
    .line 367
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p1, v3, p2}, Landroid/content/SyncStorageEngine;->getSyncAutomatically(Landroid/accounts/Account;ILjava/lang/String;)Z
    :try_end_1f
    .catchall {:try_start_12 .. :try_end_1f} :catchall_29

    #@1f
    move-result v4

    #@20
    .line 371
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@23
    .line 373
    :goto_23
    return v4

    #@24
    .line 371
    :cond_24
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@27
    .line 373
    const/4 v4, 0x0

    #@28
    goto :goto_23

    #@29
    .line 371
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_29
    move-exception v4

    #@2a
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@2d
    throw v4
.end method

.method public getSyncStatus(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/SyncStatusInfo;
    .registers 10
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 546
    iget-object v4, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.READ_SYNC_STATS"

    #@4
    const-string/jumbo v6, "no permission to read the sync stats"

    #@7
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 548
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v3

    #@e
    .line 550
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 552
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v2

    #@16
    .line 553
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_24

    #@18
    .line 554
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p1, v3, p2}, Landroid/content/SyncStorageEngine;->getStatusByAccountAndAuthority(Landroid/accounts/Account;ILjava/lang/String;)Landroid/content/SyncStatusInfo;
    :try_end_1f
    .catchall {:try_start_12 .. :try_end_1f} :catchall_29

    #@1f
    move-result-object v4

    #@20
    .line 558
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@23
    .line 560
    :goto_23
    return-object v4

    #@24
    .line 558
    :cond_24
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@27
    .line 560
    const/4 v4, 0x0

    #@28
    goto :goto_23

    #@29
    .line 558
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_29
    move-exception v4

    #@2a
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@2d
    throw v4
.end method

.method public isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z
    .registers 10
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 515
    iget-object v4, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.READ_SYNC_STATS"

    #@4
    const-string/jumbo v6, "no permission to read the sync stats"

    #@7
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 517
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v3

    #@e
    .line 519
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 521
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v2

    #@16
    .line 522
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_24

    #@18
    .line 523
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p1, v3, p2}, Landroid/content/SyncStorageEngine;->isSyncActive(Landroid/accounts/Account;ILjava/lang/String;)Z
    :try_end_1f
    .catchall {:try_start_12 .. :try_end_1f} :catchall_29

    #@1f
    move-result v4

    #@20
    .line 527
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@23
    .line 529
    :goto_23
    return v4

    #@24
    .line 527
    :cond_24
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@27
    .line 529
    const/4 v4, 0x0

    #@28
    goto :goto_23

    #@29
    .line 527
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_29
    move-exception v4

    #@2a
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@2d
    throw v4
.end method

.method public isSyncPending(Landroid/accounts/Account;Ljava/lang/String;)Z
    .registers 10
    .parameter "account"
    .parameter "authority"

    #@0
    .prologue
    .line 564
    iget-object v4, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.READ_SYNC_STATS"

    #@4
    const-string/jumbo v6, "no permission to read the sync stats"

    #@7
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 566
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v3

    #@e
    .line 568
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 570
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v2

    #@16
    .line 571
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_24

    #@18
    .line 572
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p1, v3, p2}, Landroid/content/SyncStorageEngine;->isSyncPending(Landroid/accounts/Account;ILjava/lang/String;)Z
    :try_end_1f
    .catchall {:try_start_12 .. :try_end_1f} :catchall_29

    #@1f
    move-result v4

    #@20
    .line 575
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@23
    .line 577
    :goto_23
    return v4

    #@24
    .line 575
    :cond_24
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@27
    .line 577
    const/4 v4, 0x0

    #@28
    goto :goto_23

    #@29
    .line 575
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_29
    move-exception v4

    #@2a
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@2d
    throw v4
.end method

.method public notifyChange(Landroid/net/Uri;Landroid/database/IContentObserver;ZZ)V
    .registers 11
    .parameter "uri"
    .parameter "observer"
    .parameter "observerWantsSelfNotifications"
    .parameter "syncToNetwork"

    #@0
    .prologue
    .line 278
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v5

    #@4
    move-object v0, p0

    #@5
    move-object v1, p1

    #@6
    move-object v2, p2

    #@7
    move v3, p3

    #@8
    move v4, p4

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentService;->notifyChange(Landroid/net/Uri;Landroid/database/IContentObserver;ZZI)V

    #@c
    .line 280
    return-void
.end method

.method public notifyChange(Landroid/net/Uri;Landroid/database/IContentObserver;ZZI)V
    .registers 28
    .parameter "uri"
    .parameter "observer"
    .parameter "observerWantsSelfNotifications"
    .parameter "syncToNetwork"
    .parameter "userHandle"

    #@0
    .prologue
    .line 207
    const-string v1, "ContentService"

    #@2
    const/4 v2, 0x2

    #@3
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_47

    #@9
    .line 208
    const-string v1, "ContentService"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Notifying update of "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    move-object/from16 v0, p1

    #@18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, " for user "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    move/from16 v0, p5

    #@24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    const-string v3, " from observer "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    move-object/from16 v0, p2

    #@30
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    const-string v3, ", syncToNetwork "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    move/from16 v0, p4

    #@3c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 213
    :cond_47
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@4a
    move-result v9

    #@4b
    .line 214
    .local v9, callingUserHandle:I
    move/from16 v0, p5

    #@4d
    if-eq v0, v9, :cond_5b

    #@4f
    .line 215
    move-object/from16 v0, p0

    #@51
    iget-object v1, v0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@53
    const-string v2, "android.permission.INTERACT_ACROSS_USERS_FULL"

    #@55
    const-string/jumbo v3, "no permission to notify other users"

    #@58
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    .line 220
    :cond_5b
    if-gez p5, :cond_66

    #@5d
    .line 221
    const/4 v1, -0x2

    #@5e
    move/from16 v0, p5

    #@60
    if-ne v0, v1, :cond_de

    #@62
    .line 222
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@65
    move-result p5

    #@66
    .line 231
    :cond_66
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@69
    move-result-wide v12

    #@6a
    .line 233
    .local v12, identityToken:J
    :try_start_6a
    new-instance v7, Ljava/util/ArrayList;

    #@6c
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@6f
    .line 234
    .local v7, calls:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentService$ObserverCall;>;"
    move-object/from16 v0, p0

    #@71
    iget-object v0, v0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@73
    move-object/from16 v21, v0

    #@75
    monitor-enter v21
    :try_end_76
    .catchall {:try_start_6a .. :try_end_76} :catchall_101

    #@76
    .line 235
    :try_start_76
    move-object/from16 v0, p0

    #@78
    iget-object v1, v0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@7a
    const/4 v3, 0x0

    #@7b
    move-object/from16 v2, p1

    #@7d
    move-object/from16 v4, p2

    #@7f
    move/from16 v5, p3

    #@81
    move/from16 v6, p5

    #@83
    invoke-virtual/range {v1 .. v7}, Landroid/content/ContentService$ObserverNode;->collectObserversLocked(Landroid/net/Uri;ILandroid/database/IContentObserver;ZILjava/util/ArrayList;)V

    #@86
    .line 237
    monitor-exit v21
    :try_end_87
    .catchall {:try_start_76 .. :try_end_87} :catchall_fe

    #@87
    .line 238
    :try_start_87
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@8a
    move-result v16

    #@8b
    .line 239
    .local v16, numCalls:I
    const/4 v11, 0x0

    #@8c
    .local v11, i:I
    :goto_8c
    move/from16 v0, v16

    #@8e
    if-ge v11, v0, :cond_14b

    #@90
    .line 240
    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@93
    move-result-object v18

    #@94
    check-cast v18, Landroid/content/ContentService$ObserverCall;
    :try_end_96
    .catchall {:try_start_87 .. :try_end_96} :catchall_101

    #@96
    .line 242
    .local v18, oc:Landroid/content/ContentService$ObserverCall;
    :try_start_96
    move-object/from16 v0, v18

    #@98
    iget-object v1, v0, Landroid/content/ContentService$ObserverCall;->mObserver:Landroid/database/IContentObserver;

    #@9a
    move-object/from16 v0, v18

    #@9c
    iget-boolean v2, v0, Landroid/content/ContentService$ObserverCall;->mSelfChange:Z

    #@9e
    move-object/from16 v0, p1

    #@a0
    invoke-interface {v1, v2, v0}, Landroid/database/IContentObserver;->onChange(ZLandroid/net/Uri;)V

    #@a3
    .line 243
    const-string v1, "ContentService"

    #@a5
    const/4 v2, 0x2

    #@a6
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@a9
    move-result v1

    #@aa
    if-eqz v1, :cond_db

    #@ac
    .line 244
    const-string v1, "ContentService"

    #@ae
    new-instance v2, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v3, "Notified "

    #@b5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v2

    #@b9
    move-object/from16 v0, v18

    #@bb
    iget-object v3, v0, Landroid/content/ContentService$ObserverCall;->mObserver:Landroid/database/IContentObserver;

    #@bd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v2

    #@c1
    const-string v3, " of "

    #@c3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v2

    #@c7
    const-string/jumbo v3, "update at "

    #@ca
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v2

    #@ce
    move-object/from16 v0, p1

    #@d0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v2

    #@d4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v2

    #@d8
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_db
    .catchall {:try_start_96 .. :try_end_db} :catchall_101
    .catch Landroid/os/RemoteException; {:try_start_96 .. :try_end_db} :catch_106

    #@db
    .line 239
    :cond_db
    :goto_db
    add-int/lit8 v11, v11, 0x1

    #@dd
    goto :goto_8c

    #@de
    .line 223
    .end local v7           #calls:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentService$ObserverCall;>;"
    .end local v11           #i:I
    .end local v12           #identityToken:J
    .end local v16           #numCalls:I
    .end local v18           #oc:Landroid/content/ContentService$ObserverCall;
    :cond_de
    const/4 v1, -0x1

    #@df
    move/from16 v0, p5

    #@e1
    if-eq v0, v1, :cond_66

    #@e3
    .line 224
    new-instance v1, Ljava/security/InvalidParameterException;

    #@e5
    new-instance v2, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v3, "Bad user handle for notifyChange: "

    #@ec
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v2

    #@f0
    move/from16 v0, p5

    #@f2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v2

    #@f6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v2

    #@fa
    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    #@fd
    throw v1

    #@fe
    .line 237
    .restart local v7       #calls:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentService$ObserverCall;>;"
    .restart local v12       #identityToken:J
    :catchall_fe
    move-exception v1

    #@ff
    :try_start_ff
    monitor-exit v21
    :try_end_100
    .catchall {:try_start_ff .. :try_end_100} :catchall_fe

    #@100
    :try_start_100
    throw v1
    :try_end_101
    .catchall {:try_start_100 .. :try_end_101} :catchall_101

    #@101
    .line 272
    .end local v7           #calls:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentService$ObserverCall;>;"
    :catchall_101
    move-exception v1

    #@102
    invoke-static {v12, v13}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@105
    throw v1

    #@106
    .line 246
    .restart local v7       #calls:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentService$ObserverCall;>;"
    .restart local v11       #i:I
    .restart local v16       #numCalls:I
    .restart local v18       #oc:Landroid/content/ContentService$ObserverCall;
    :catch_106
    move-exception v10

    #@107
    .line 247
    .local v10, ex:Landroid/os/RemoteException;
    :try_start_107
    move-object/from16 v0, p0

    #@109
    iget-object v2, v0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@10b
    monitor-enter v2
    :try_end_10c
    .catchall {:try_start_107 .. :try_end_10c} :catchall_101

    #@10c
    .line 248
    :try_start_10c
    const-string v1, "ContentService"

    #@10e
    const-string v3, "Found dead observer, removing"

    #@110
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@113
    .line 249
    move-object/from16 v0, v18

    #@115
    iget-object v1, v0, Landroid/content/ContentService$ObserverCall;->mObserver:Landroid/database/IContentObserver;

    #@117
    invoke-interface {v1}, Landroid/database/IContentObserver;->asBinder()Landroid/os/IBinder;

    #@11a
    move-result-object v8

    #@11b
    .line 250
    .local v8, binder:Landroid/os/IBinder;
    move-object/from16 v0, v18

    #@11d
    iget-object v1, v0, Landroid/content/ContentService$ObserverCall;->mNode:Landroid/content/ContentService$ObserverNode;

    #@11f
    invoke-static {v1}, Landroid/content/ContentService$ObserverNode;->access$000(Landroid/content/ContentService$ObserverNode;)Ljava/util/ArrayList;

    #@122
    move-result-object v15

    #@123
    .line 252
    .local v15, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentService$ObserverNode$ObserverEntry;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    #@126
    move-result v17

    #@127
    .line 253
    .local v17, numList:I
    const/4 v14, 0x0

    #@128
    .local v14, j:I
    :goto_128
    move/from16 v0, v17

    #@12a
    if-ge v14, v0, :cond_146

    #@12c
    .line 254
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12f
    move-result-object v19

    #@130
    check-cast v19, Landroid/content/ContentService$ObserverNode$ObserverEntry;

    #@132
    .line 255
    .local v19, oe:Landroid/content/ContentService$ObserverNode$ObserverEntry;
    move-object/from16 v0, v19

    #@134
    iget-object v1, v0, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observer:Landroid/database/IContentObserver;

    #@136
    invoke-interface {v1}, Landroid/database/IContentObserver;->asBinder()Landroid/os/IBinder;

    #@139
    move-result-object v1

    #@13a
    if-ne v1, v8, :cond_143

    #@13c
    .line 256
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@13f
    .line 257
    add-int/lit8 v14, v14, -0x1

    #@141
    .line 258
    add-int/lit8 v17, v17, -0x1

    #@143
    .line 253
    :cond_143
    add-int/lit8 v14, v14, 0x1

    #@145
    goto :goto_128

    #@146
    .line 261
    .end local v19           #oe:Landroid/content/ContentService$ObserverNode$ObserverEntry;
    :cond_146
    monitor-exit v2

    #@147
    goto :goto_db

    #@148
    .end local v8           #binder:Landroid/os/IBinder;
    .end local v14           #j:I
    .end local v15           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentService$ObserverNode$ObserverEntry;>;"
    .end local v17           #numList:I
    :catchall_148
    move-exception v1

    #@149
    monitor-exit v2
    :try_end_14a
    .catchall {:try_start_10c .. :try_end_14a} :catchall_148

    #@14a
    :try_start_14a
    throw v1

    #@14b
    .line 264
    .end local v10           #ex:Landroid/os/RemoteException;
    .end local v18           #oc:Landroid/content/ContentService$ObserverCall;
    :cond_14b
    if-eqz p4, :cond_15d

    #@14d
    .line 265
    invoke-direct/range {p0 .. p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@150
    move-result-object v20

    #@151
    .line 266
    .local v20, syncManager:Landroid/content/SyncManager;
    if-eqz v20, :cond_15d

    #@153
    .line 267
    const/4 v1, 0x0

    #@154
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@157
    move-result-object v2

    #@158
    move-object/from16 v0, v20

    #@15a
    invoke-virtual {v0, v1, v9, v2}, Landroid/content/SyncManager;->scheduleLocalSync(Landroid/accounts/Account;ILjava/lang/String;)V
    :try_end_15d
    .catchall {:try_start_14a .. :try_end_15d} :catchall_101

    #@15d
    .line 272
    .end local v20           #syncManager:Landroid/content/SyncManager;
    :cond_15d
    invoke-static {v12, v13}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@160
    .line 274
    return-void
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 123
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/content/IContentService$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result v1

    #@4
    return v1

    #@5
    .line 124
    :catch_5
    move-exception v0

    #@6
    .line 127
    .local v0, e:Ljava/lang/RuntimeException;
    instance-of v1, v0, Ljava/lang/SecurityException;

    #@8
    if-nez v1, :cond_11

    #@a
    .line 128
    const-string v1, "ContentService"

    #@c
    const-string v2, "Content Service Crash"

    #@e
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 130
    :cond_11
    throw v0
.end method

.method public registerContentObserver(Landroid/net/Uri;ZLandroid/database/IContentObserver;)V
    .registers 5
    .parameter "uri"
    .parameter "notifyForDescendants"
    .parameter "observer"

    #@0
    .prologue
    .line 182
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/content/ContentService;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/IContentObserver;I)V

    #@7
    .line 184
    return-void
.end method

.method public registerContentObserver(Landroid/net/Uri;ZLandroid/database/IContentObserver;I)V
    .registers 15
    .parameter "uri"
    .parameter "notifyForDescendants"
    .parameter "observer"
    .parameter "userHandle"

    #@0
    .prologue
    .line 153
    if-eqz p3, :cond_4

    #@2
    if-nez p1, :cond_c

    #@4
    .line 154
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v1, "You must pass a valid uri and observer"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 157
    :cond_c
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@f
    move-result v8

    #@10
    .line 158
    .local v8, callingUser:I
    if-eq v8, p4, :cond_1c

    #@12
    .line 159
    iget-object v0, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@14
    const-string v1, "android.permission.INTERACT_ACROSS_USERS_FULL"

    #@16
    const-string/jumbo v2, "no permission to observe other users\' provider view"

    #@19
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 163
    :cond_1c
    if-gez p4, :cond_25

    #@1e
    .line 164
    const/4 v0, -0x2

    #@1f
    if-ne p4, v0, :cond_3d

    #@21
    .line 165
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@24
    move-result p4

    #@25
    .line 172
    :cond_25
    iget-object v9, p0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@27
    monitor-enter v9

    #@28
    .line 173
    :try_start_28
    iget-object v0, p0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@2a
    iget-object v4, p0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@2c
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2f
    move-result v5

    #@30
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@33
    move-result v6

    #@34
    move-object v1, p1

    #@35
    move-object v2, p3

    #@36
    move v3, p2

    #@37
    move v7, p4

    #@38
    invoke-virtual/range {v0 .. v7}, Landroid/content/ContentService$ObserverNode;->addObserverLocked(Landroid/net/Uri;Landroid/database/IContentObserver;ZLjava/lang/Object;III)V

    #@3b
    .line 177
    monitor-exit v9
    :try_end_3c
    .catchall {:try_start_28 .. :try_end_3c} :catchall_59

    #@3c
    .line 178
    return-void

    #@3d
    .line 166
    :cond_3d
    const/4 v0, -0x1

    #@3e
    if-eq p4, v0, :cond_25

    #@40
    .line 167
    new-instance v0, Ljava/security/InvalidParameterException;

    #@42
    new-instance v1, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v2, "Bad user handle for registerContentObserver: "

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    #@58
    throw v0

    #@59
    .line 177
    :catchall_59
    move-exception v0

    #@5a
    :try_start_5a
    monitor-exit v9
    :try_end_5b
    .catchall {:try_start_5a .. :try_end_5b} :catchall_59

    #@5b
    throw v0
.end method

.method public removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 10
    .parameter "account"
    .parameter "authority"
    .parameter "extras"

    #@0
    .prologue
    .line 409
    iget-object v3, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.WRITE_SYNC_SETTINGS"

    #@4
    const-string/jumbo v5, "no permission to write the sync settings"

    #@7
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 411
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v2

    #@e
    .line 413
    .local v2, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 415
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1, v2, p2, p3}, Landroid/content/SyncStorageEngine;->removePeriodicSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V
    :try_end_1d
    .catchall {:try_start_12 .. :try_end_1d} :catchall_21

    #@1d
    .line 418
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@20
    .line 420
    return-void

    #@21
    .line 418
    :catchall_21
    move-exception v3

    #@22
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@25
    throw v3
.end method

.method public removeStatusChangeListener(Landroid/content/ISyncStatusObserver;)V
    .registers 6
    .parameter "callback"

    #@0
    .prologue
    .line 593
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 595
    .local v0, identityToken:J
    :try_start_4
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@7
    move-result-object v2

    #@8
    .line 596
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_13

    #@a
    if-eqz p1, :cond_13

    #@c
    .line 597
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3, p1}, Landroid/content/SyncStorageEngine;->removeStatusChangeListener(Landroid/content/ISyncStatusObserver;)V
    :try_end_13
    .catchall {:try_start_4 .. :try_end_13} :catchall_17

    #@13
    .line 600
    :cond_13
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@16
    .line 602
    return-void

    #@17
    .line 600
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_17
    move-exception v3

    #@18
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@1b
    throw v3
.end method

.method public requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 14
    .parameter "account"
    .parameter "authority"
    .parameter "extras"

    #@0
    .prologue
    .line 301
    invoke-static {p3}, Landroid/content/ContentResolver;->validateSyncExtrasBundle(Landroid/os/Bundle;)V

    #@3
    .line 302
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@6
    move-result v2

    #@7
    .line 306
    .local v2, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@a
    move-result-wide v8

    #@b
    .line 308
    .local v8, identityToken:J
    :try_start_b
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@e
    move-result-object v0

    #@f
    .line 309
    .local v0, syncManager:Landroid/content/SyncManager;
    if-eqz v0, :cond_1a

    #@11
    .line 310
    const-wide/16 v5, 0x0

    #@13
    const/4 v7, 0x0

    #@14
    move-object v1, p1

    #@15
    move-object v3, p2

    #@16
    move-object v4, p3

    #@17
    invoke-virtual/range {v0 .. v7}, Landroid/content/SyncManager;->scheduleSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;JZ)V
    :try_end_1a
    .catchall {:try_start_b .. :try_end_1a} :catchall_1e

    #@1a
    .line 314
    :cond_1a
    invoke-static {v8, v9}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@1d
    .line 316
    return-void

    #@1e
    .line 314
    .end local v0           #syncManager:Landroid/content/SyncManager;
    :catchall_1e
    move-exception v1

    #@1f
    invoke-static {v8, v9}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@22
    throw v1
.end method

.method public setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    .registers 11
    .parameter "account"
    .parameter "providerName"
    .parameter "syncable"

    #@0
    .prologue
    .line 455
    iget-object v4, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.WRITE_SYNC_SETTINGS"

    #@4
    const-string/jumbo v6, "no permission to write the sync settings"

    #@7
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 457
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v3

    #@e
    .line 459
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 461
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v2

    #@16
    .line 462
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_1f

    #@18
    .line 463
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p1, v3, p2, p3}, Landroid/content/SyncStorageEngine;->setIsSyncable(Landroid/accounts/Account;ILjava/lang/String;I)V
    :try_end_1f
    .catchall {:try_start_12 .. :try_end_1f} :catchall_23

    #@1f
    .line 467
    :cond_1f
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@22
    .line 469
    return-void

    #@23
    .line 467
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_23
    move-exception v4

    #@24
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@27
    throw v4
.end method

.method public setMasterSyncAutomatically(Z)V
    .registers 9
    .parameter "flag"

    #@0
    .prologue
    .line 489
    iget-object v4, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.WRITE_SYNC_SETTINGS"

    #@4
    const-string/jumbo v6, "no permission to write the sync settings"

    #@7
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 493
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@d
    move-result-object v4

    #@e
    if-eqz v4, :cond_20

    #@10
    if-eqz p1, :cond_20

    #@12
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@15
    move-result-object v4

    #@16
    const/4 v5, 0x0

    #@17
    const-string v6, "LGMDMEmailUIAdapter"

    #@19
    invoke-interface {v4, v5, v6}, Lcom/lge/cappuccino/IMdm;->checkDisabledSystemService(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_20

    #@1f
    .line 512
    :goto_1f
    return-void

    #@20
    .line 501
    :cond_20
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@23
    move-result v3

    #@24
    .line 503
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@27
    move-result-wide v0

    #@28
    .line 505
    .local v0, identityToken:J
    :try_start_28
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@2b
    move-result-object v2

    #@2c
    .line 506
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_35

    #@2e
    .line 507
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4, p1, v3}, Landroid/content/SyncStorageEngine;->setMasterSyncAutomatically(ZI)V
    :try_end_35
    .catchall {:try_start_28 .. :try_end_35} :catchall_39

    #@35
    .line 510
    :cond_35
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@38
    goto :goto_1f

    #@39
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_39
    move-exception v4

    #@3a
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@3d
    throw v4
.end method

.method public setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V
    .registers 11
    .parameter "account"
    .parameter "providerName"
    .parameter "sync"

    #@0
    .prologue
    .line 377
    iget-object v4, p0, Landroid/content/ContentService;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "android.permission.WRITE_SYNC_SETTINGS"

    #@4
    const-string/jumbo v6, "no permission to write the sync settings"

    #@7
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 379
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@d
    move-result v3

    #@e
    .line 381
    .local v3, userId:I
    invoke-static {}, Landroid/content/ContentService;->clearCallingIdentity()J

    #@11
    move-result-wide v0

    #@12
    .line 383
    .local v0, identityToken:J
    :try_start_12
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@15
    move-result-object v2

    #@16
    .line 384
    .local v2, syncManager:Landroid/content/SyncManager;
    if-eqz v2, :cond_1f

    #@18
    .line 385
    invoke-virtual {v2}, Landroid/content/SyncManager;->getSyncStorageEngine()Landroid/content/SyncStorageEngine;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p1, v3, p2, p3}, Landroid/content/SyncStorageEngine;->setSyncAutomatically(Landroid/accounts/Account;ILjava/lang/String;Z)V
    :try_end_1f
    .catchall {:try_start_12 .. :try_end_1f} :catchall_23

    #@1f
    .line 389
    :cond_1f
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@22
    .line 391
    return-void

    #@23
    .line 389
    .end local v2           #syncManager:Landroid/content/SyncManager;
    :catchall_23
    move-exception v4

    #@24
    invoke-static {v0, v1}, Landroid/content/ContentService;->restoreCallingIdentity(J)V

    #@27
    throw v4
.end method

.method public systemReady()V
    .registers 1

    #@0
    .prologue
    .line 140
    invoke-direct {p0}, Landroid/content/ContentService;->getSyncManager()Landroid/content/SyncManager;

    #@3
    .line 141
    return-void
.end method

.method public unregisterContentObserver(Landroid/database/IContentObserver;)V
    .registers 4
    .parameter "observer"

    #@0
    .prologue
    .line 187
    if-nez p1, :cond_a

    #@2
    .line 188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "You must pass a valid observer"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 190
    :cond_a
    iget-object v1, p0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@c
    monitor-enter v1

    #@d
    .line 191
    :try_start_d
    iget-object v0, p0, Landroid/content/ContentService;->mRootNode:Landroid/content/ContentService$ObserverNode;

    #@f
    invoke-virtual {v0, p1}, Landroid/content/ContentService$ObserverNode;->removeObserverLocked(Landroid/database/IContentObserver;)Z

    #@12
    .line 193
    monitor-exit v1

    #@13
    .line 194
    return-void

    #@14
    .line 193
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_d .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method
