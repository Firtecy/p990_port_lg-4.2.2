.class public Landroid/content/SearchRecentSuggestionsProvider;
.super Landroid/content/ContentProvider;
.source "SearchRecentSuggestionsProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/SearchRecentSuggestionsProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field public static final DATABASE_MODE_2LINES:I = 0x2

.field public static final DATABASE_MODE_QUERIES:I = 0x1

.field private static final DATABASE_VERSION:I = 0x200

.field private static final NULL_COLUMN:Ljava/lang/String; = "query"

.field private static final ORDER_BY:Ljava/lang/String; = "date DESC"

.field private static final TAG:Ljava/lang/String; = "SuggestionsProvider"

.field private static final URI_MATCH_SUGGEST:I = 0x1

.field private static final sDatabaseName:Ljava/lang/String; = "suggestions.db"

.field private static final sSuggestions:Ljava/lang/String; = "suggestions"


# instance fields
.field private mAuthority:Ljava/lang/String;

.field private mMode:I

.field private mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

.field private mSuggestSuggestionClause:Ljava/lang/String;

.field private mSuggestionProjection:[Ljava/lang/String;

.field private mSuggestionsUri:Landroid/net/Uri;

.field private mTwoLineDisplay:Z

.field private mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    .line 127
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 10
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 226
    iget-object v4, p0, Landroid/content/SearchRecentSuggestionsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    #@2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v2

    #@6
    .line 228
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@9
    move-result-object v4

    #@a
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@d
    move-result v3

    #@e
    .line 229
    .local v3, length:I
    const/4 v4, 0x1

    #@f
    if-eq v3, v4, :cond_19

    #@11
    .line 230
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v5, "Unknown Uri"

    #@15
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v4

    #@19
    .line 233
    :cond_19
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@1c
    move-result-object v4

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Ljava/lang/String;

    #@24
    .line 234
    .local v0, base:Ljava/lang/String;
    const/4 v1, 0x0

    #@25
    .line 235
    .local v1, count:I
    const-string/jumbo v4, "suggestions"

    #@28
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_42

    #@2e
    .line 236
    const-string/jumbo v4, "suggestions"

    #@31
    invoke-virtual {v2, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@34
    move-result v1

    #@35
    .line 240
    invoke-virtual {p0}, Landroid/content/SearchRecentSuggestionsProvider;->getContext()Landroid/content/Context;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3c
    move-result-object v4

    #@3d
    const/4 v5, 0x0

    #@3e
    invoke-virtual {v4, p1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@41
    .line 241
    return v1

    #@42
    .line 238
    :cond_42
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@44
    const-string v5, "Unknown Uri"

    #@46
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@49
    throw v4
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 7
    .parameter "uri"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 250
    iget-object v2, p0, Landroid/content/SearchRecentSuggestionsProvider;->mUriMatcher:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v2

    #@7
    if-ne v2, v4, :cond_d

    #@9
    .line 251
    const-string/jumbo v2, "vnd.android.cursor.dir/vnd.android.search.suggest"

    #@c
    .line 260
    :goto_c
    return-object v2

    #@d
    .line 253
    :cond_d
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@10
    move-result-object v2

    #@11
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@14
    move-result v1

    #@15
    .line 254
    .local v1, length:I
    if-lt v1, v4, :cond_38

    #@17
    .line 255
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@1a
    move-result-object v2

    #@1b
    const/4 v3, 0x0

    #@1c
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Ljava/lang/String;

    #@22
    .line 256
    .local v0, base:Ljava/lang/String;
    const-string/jumbo v2, "suggestions"

    #@25
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v2

    #@29
    if-eqz v2, :cond_38

    #@2b
    .line 257
    if-ne v1, v4, :cond_31

    #@2d
    .line 258
    const-string/jumbo v2, "vnd.android.cursor.dir/suggestion"

    #@30
    goto :goto_c

    #@31
    .line 259
    :cond_31
    const/4 v2, 0x2

    #@32
    if-ne v1, v2, :cond_38

    #@34
    .line 260
    const-string/jumbo v2, "vnd.android.cursor.item/suggestion"

    #@37
    goto :goto_c

    #@38
    .line 264
    .end local v0           #base:Ljava/lang/String;
    :cond_38
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@3a
    const-string v3, "Unknown Uri"

    #@3c
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v2
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 14
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    const-wide/16 v9, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    .line 273
    iget-object v6, p0, Landroid/content/SearchRecentSuggestionsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    #@5
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@8
    move-result-object v1

    #@9
    .line 275
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@c
    move-result-object v6

    #@d
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@10
    move-result v2

    #@11
    .line 276
    .local v2, length:I
    if-ge v2, v8, :cond_1b

    #@13
    .line 277
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@15
    const-string v7, "Unknown Uri"

    #@17
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v6

    #@1b
    .line 280
    :cond_1b
    const-wide/16 v4, -0x1

    #@1d
    .line 281
    .local v4, rowID:J
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@20
    move-result-object v6

    #@21
    const/4 v7, 0x0

    #@22
    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v0

    #@26
    check-cast v0, Ljava/lang/String;

    #@28
    .line 282
    .local v0, base:Ljava/lang/String;
    const/4 v3, 0x0

    #@29
    .line 283
    .local v3, newUri:Landroid/net/Uri;
    const-string/jumbo v6, "suggestions"

    #@2c
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v6

    #@30
    if-eqz v6, :cond_4c

    #@32
    .line 284
    if-ne v2, v8, :cond_4c

    #@34
    .line 285
    const-string/jumbo v6, "suggestions"

    #@37
    const-string/jumbo v7, "query"

    #@3a
    invoke-virtual {v1, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@3d
    move-result-wide v4

    #@3e
    .line 286
    cmp-long v6, v4, v9

    #@40
    if-lez v6, :cond_4c

    #@42
    .line 287
    iget-object v6, p0, Landroid/content/SearchRecentSuggestionsProvider;->mSuggestionsUri:Landroid/net/Uri;

    #@44
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@47
    move-result-object v7

    #@48
    invoke-static {v6, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@4b
    move-result-object v3

    #@4c
    .line 291
    :cond_4c
    cmp-long v6, v4, v9

    #@4e
    if-gez v6, :cond_58

    #@50
    .line 292
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@52
    const-string v7, "Unknown Uri"

    #@54
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@57
    throw v6

    #@58
    .line 294
    :cond_58
    invoke-virtual {p0}, Landroid/content/SearchRecentSuggestionsProvider;->getContext()Landroid/content/Context;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5f
    move-result-object v6

    #@60
    const/4 v7, 0x0

    #@61
    invoke-virtual {v6, v3, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@64
    .line 295
    return-object v3
.end method

.method public onCreate()Z
    .registers 4

    #@0
    .prologue
    .line 304
    iget-object v1, p0, Landroid/content/SearchRecentSuggestionsProvider;->mAuthority:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_8

    #@4
    iget v1, p0, Landroid/content/SearchRecentSuggestionsProvider;->mMode:I

    #@6
    if-nez v1, :cond_10

    #@8
    .line 305
    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v2, "Provider not configured"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 307
    :cond_10
    iget v1, p0, Landroid/content/SearchRecentSuggestionsProvider;->mMode:I

    #@12
    add-int/lit16 v0, v1, 0x200

    #@14
    .line 308
    .local v0, mWorkingDbVersion:I
    new-instance v1, Landroid/content/SearchRecentSuggestionsProvider$DatabaseHelper;

    #@16
    invoke-virtual {p0}, Landroid/content/SearchRecentSuggestionsProvider;->getContext()Landroid/content/Context;

    #@19
    move-result-object v2

    #@1a
    invoke-direct {v1, v2, v0}, Landroid/content/SearchRecentSuggestionsProvider$DatabaseHelper;-><init>(Landroid/content/Context;I)V

    #@1d
    iput-object v1, p0, Landroid/content/SearchRecentSuggestionsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    #@1f
    .line 310
    const/4 v1, 0x1

    #@20
    return v1
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 26
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    .line 321
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Landroid/content/SearchRecentSuggestionsProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    #@4
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@7
    move-result-object v2

    #@8
    .line 324
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, p0

    #@a
    iget-object v3, v0, Landroid/content/SearchRecentSuggestionsProvider;->mUriMatcher:Landroid/content/UriMatcher;

    #@c
    move-object/from16 v0, p1

    #@e
    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@11
    move-result v3

    #@12
    const/4 v4, 0x1

    #@13
    if-ne v3, v4, :cond_77

    #@15
    .line 327
    const/4 v3, 0x0

    #@16
    aget-object v3, p4, v3

    #@18
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_40

    #@1e
    .line 328
    const/4 v5, 0x0

    #@1f
    .line 329
    .local v5, suggestSelection:Ljava/lang/String;
    const/4 v6, 0x0

    #@20
    .line 340
    .local v6, myArgs:[Ljava/lang/String;
    :goto_20
    const-string/jumbo v3, "suggestions"

    #@23
    move-object/from16 v0, p0

    #@25
    iget-object v4, v0, Landroid/content/SearchRecentSuggestionsProvider;->mSuggestionProjection:[Ljava/lang/String;

    #@27
    const/4 v7, 0x0

    #@28
    const/4 v8, 0x0

    #@29
    const-string v9, "date DESC"

    #@2b
    const/4 v10, 0x0

    #@2c
    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2f
    move-result-object v16

    #@30
    .line 342
    .local v16, c:Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Landroid/content/SearchRecentSuggestionsProvider;->getContext()Landroid/content/Context;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@37
    move-result-object v3

    #@38
    move-object/from16 v0, v16

    #@3a
    move-object/from16 v1, p1

    #@3c
    invoke-interface {v0, v3, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    #@3f
    .line 385
    .end local v5           #suggestSelection:Ljava/lang/String;
    .end local v6           #myArgs:[Ljava/lang/String;
    :goto_3f
    return-object v16

    #@40
    .line 331
    .end local v16           #c:Landroid/database/Cursor;
    :cond_40
    new-instance v3, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v4, "%"

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    const/4 v4, 0x0

    #@4c
    aget-object v4, p4, v4

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    const-string v4, "%"

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v18

    #@5c
    .line 332
    .local v18, like:Ljava/lang/String;
    move-object/from16 v0, p0

    #@5e
    iget-boolean v3, v0, Landroid/content/SearchRecentSuggestionsProvider;->mTwoLineDisplay:Z

    #@60
    if-eqz v3, :cond_70

    #@62
    .line 333
    const/4 v3, 0x2

    #@63
    new-array v6, v3, [Ljava/lang/String;

    #@65
    const/4 v3, 0x0

    #@66
    aput-object v18, v6, v3

    #@68
    const/4 v3, 0x1

    #@69
    aput-object v18, v6, v3

    #@6b
    .line 337
    .restart local v6       #myArgs:[Ljava/lang/String;
    :goto_6b
    move-object/from16 v0, p0

    #@6d
    iget-object v5, v0, Landroid/content/SearchRecentSuggestionsProvider;->mSuggestSuggestionClause:Ljava/lang/String;

    #@6f
    .restart local v5       #suggestSelection:Ljava/lang/String;
    goto :goto_20

    #@70
    .line 335
    .end local v5           #suggestSelection:Ljava/lang/String;
    .end local v6           #myArgs:[Ljava/lang/String;
    :cond_70
    const/4 v3, 0x1

    #@71
    new-array v6, v3, [Ljava/lang/String;

    #@73
    const/4 v3, 0x0

    #@74
    aput-object v18, v6, v3

    #@76
    .restart local v6       #myArgs:[Ljava/lang/String;
    goto :goto_6b

    #@77
    .line 347
    .end local v6           #myArgs:[Ljava/lang/String;
    .end local v18           #like:Ljava/lang/String;
    :cond_77
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@7a
    move-result-object v3

    #@7b
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@7e
    move-result v17

    #@7f
    .line 348
    .local v17, length:I
    const/4 v3, 0x1

    #@80
    move/from16 v0, v17

    #@82
    if-eq v0, v3, :cond_91

    #@84
    const/4 v3, 0x2

    #@85
    move/from16 v0, v17

    #@87
    if-eq v0, v3, :cond_91

    #@89
    .line 349
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@8b
    const-string v4, "Unknown Uri"

    #@8d
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@90
    throw v3

    #@91
    .line 352
    :cond_91
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@94
    move-result-object v3

    #@95
    const/4 v4, 0x0

    #@96
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@99
    move-result-object v8

    #@9a
    check-cast v8, Ljava/lang/String;

    #@9c
    .line 353
    .local v8, base:Ljava/lang/String;
    const-string/jumbo v3, "suggestions"

    #@9f
    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a2
    move-result v3

    #@a3
    if-nez v3, :cond_ad

    #@a5
    .line 354
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@a7
    const-string v4, "Unknown Uri"

    #@a9
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@ac
    throw v3

    #@ad
    .line 357
    :cond_ad
    const/4 v9, 0x0

    #@ae
    .line 358
    .local v9, useProjection:[Ljava/lang/String;
    if-eqz p2, :cond_cd

    #@b0
    move-object/from16 v0, p2

    #@b2
    array-length v3, v0

    #@b3
    if-lez v3, :cond_cd

    #@b5
    .line 359
    move-object/from16 v0, p2

    #@b7
    array-length v3, v0

    #@b8
    add-int/lit8 v3, v3, 0x1

    #@ba
    new-array v9, v3, [Ljava/lang/String;

    #@bc
    .line 360
    const/4 v3, 0x0

    #@bd
    const/4 v4, 0x0

    #@be
    move-object/from16 v0, p2

    #@c0
    array-length v7, v0

    #@c1
    move-object/from16 v0, p2

    #@c3
    invoke-static {v0, v3, v9, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@c6
    .line 361
    move-object/from16 v0, p2

    #@c8
    array-length v3, v0

    #@c9
    const-string v4, "_id AS _id"

    #@cb
    aput-object v4, v9, v3

    #@cd
    .line 364
    :cond_cd
    new-instance v19, Ljava/lang/StringBuilder;

    #@cf
    const/16 v3, 0x100

    #@d1
    move-object/from16 v0, v19

    #@d3
    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d6
    .line 365
    .local v19, whereClause:Ljava/lang/StringBuilder;
    const/4 v3, 0x2

    #@d7
    move/from16 v0, v17

    #@d9
    if-ne v0, v3, :cond_f7

    #@db
    .line 366
    const-string v3, "(_id = "

    #@dd
    move-object/from16 v0, v19

    #@df
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v4

    #@e3
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@e6
    move-result-object v3

    #@e7
    const/4 v7, 0x1

    #@e8
    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@eb
    move-result-object v3

    #@ec
    check-cast v3, Ljava/lang/String;

    #@ee
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v3

    #@f2
    const-string v4, ")"

    #@f4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    .line 370
    :cond_f7
    if-eqz p3, :cond_121

    #@f9
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    #@fc
    move-result v3

    #@fd
    if-lez v3, :cond_121

    #@ff
    .line 371
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->length()I

    #@102
    move-result v3

    #@103
    if-lez v3, :cond_10c

    #@105
    .line 372
    const-string v3, " AND "

    #@107
    move-object/from16 v0, v19

    #@109
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    .line 375
    :cond_10c
    const/16 v3, 0x28

    #@10e
    move-object/from16 v0, v19

    #@110
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@113
    .line 376
    move-object/from16 v0, v19

    #@115
    move-object/from16 v1, p3

    #@117
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    .line 377
    const/16 v3, 0x29

    #@11c
    move-object/from16 v0, v19

    #@11e
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@121
    .line 381
    :cond_121
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v10

    #@125
    const/4 v12, 0x0

    #@126
    const/4 v13, 0x0

    #@127
    const/4 v15, 0x0

    #@128
    move-object v7, v2

    #@129
    move-object/from16 v11, p4

    #@12b
    move-object/from16 v14, p5

    #@12d
    invoke-virtual/range {v7 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@130
    move-result-object v16

    #@131
    .line 384
    .restart local v16       #c:Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Landroid/content/SearchRecentSuggestionsProvider;->getContext()Landroid/content/Context;

    #@134
    move-result-object v3

    #@135
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@138
    move-result-object v3

    #@139
    move-object/from16 v0, v16

    #@13b
    move-object/from16 v1, p1

    #@13d
    invoke-interface {v0, v3, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    #@140
    goto/16 :goto_3f
.end method

.method protected setupSuggestions(Ljava/lang/String;I)V
    .registers 11
    .parameter "authority"
    .parameter "mode"

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v1, 0x1

    #@5
    .line 174
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_f

    #@b
    and-int/lit8 v0, p2, 0x1

    #@d
    if-nez v0, :cond_15

    #@f
    .line 176
    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@14
    throw v0

    #@15
    .line 179
    :cond_15
    and-int/lit8 v0, p2, 0x2

    #@17
    if-eqz v0, :cond_80

    #@19
    move v0, v1

    #@1a
    :goto_1a
    iput-boolean v0, p0, Landroid/content/SearchRecentSuggestionsProvider;->mTwoLineDisplay:Z

    #@1c
    .line 182
    new-instance v0, Ljava/lang/String;

    #@1e
    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@21
    iput-object v0, p0, Landroid/content/SearchRecentSuggestionsProvider;->mAuthority:Ljava/lang/String;

    #@23
    .line 183
    iput p2, p0, Landroid/content/SearchRecentSuggestionsProvider;->mMode:I

    #@25
    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v3, "content://"

    #@2c
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    iget-object v3, p0, Landroid/content/SearchRecentSuggestionsProvider;->mAuthority:Ljava/lang/String;

    #@32
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    const-string v3, "/suggestions"

    #@38
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@43
    move-result-object v0

    #@44
    iput-object v0, p0, Landroid/content/SearchRecentSuggestionsProvider;->mSuggestionsUri:Landroid/net/Uri;

    #@46
    .line 187
    new-instance v0, Landroid/content/UriMatcher;

    #@48
    const/4 v3, -0x1

    #@49
    invoke-direct {v0, v3}, Landroid/content/UriMatcher;-><init>(I)V

    #@4c
    iput-object v0, p0, Landroid/content/SearchRecentSuggestionsProvider;->mUriMatcher:Landroid/content/UriMatcher;

    #@4e
    .line 188
    iget-object v0, p0, Landroid/content/SearchRecentSuggestionsProvider;->mUriMatcher:Landroid/content/UriMatcher;

    #@50
    iget-object v3, p0, Landroid/content/SearchRecentSuggestionsProvider;->mAuthority:Ljava/lang/String;

    #@52
    const-string/jumbo v4, "search_suggest_query"

    #@55
    invoke-virtual {v0, v3, v4, v1}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@58
    .line 190
    iget-boolean v0, p0, Landroid/content/SearchRecentSuggestionsProvider;->mTwoLineDisplay:Z

    #@5a
    if-eqz v0, :cond_82

    #@5c
    .line 191
    const-string v0, "display1 LIKE ? OR display2 LIKE ?"

    #@5e
    iput-object v0, p0, Landroid/content/SearchRecentSuggestionsProvider;->mSuggestSuggestionClause:Ljava/lang/String;

    #@60
    .line 193
    const/4 v0, 0x6

    #@61
    new-array v0, v0, [Ljava/lang/String;

    #@63
    const-string v3, "0 AS suggest_format"

    #@65
    aput-object v3, v0, v2

    #@67
    const-string v2, "\'android.resource://system/17301578\' AS suggest_icon_1"

    #@69
    aput-object v2, v0, v1

    #@6b
    const-string v1, "display1 AS suggest_text_1"

    #@6d
    aput-object v1, v0, v5

    #@6f
    const-string v1, "display2 AS suggest_text_2"

    #@71
    aput-object v1, v0, v6

    #@73
    const-string/jumbo v1, "query AS suggest_intent_query"

    #@76
    aput-object v1, v0, v7

    #@78
    const/4 v1, 0x5

    #@79
    const-string v2, "_id"

    #@7b
    aput-object v2, v0, v1

    #@7d
    iput-object v0, p0, Landroid/content/SearchRecentSuggestionsProvider;->mSuggestionProjection:[Ljava/lang/String;

    #@7f
    .line 218
    :goto_7f
    return-void

    #@80
    :cond_80
    move v0, v2

    #@81
    .line 179
    goto :goto_1a

    #@82
    .line 204
    :cond_82
    const-string v0, "display1 LIKE ?"

    #@84
    iput-object v0, p0, Landroid/content/SearchRecentSuggestionsProvider;->mSuggestSuggestionClause:Ljava/lang/String;

    #@86
    .line 206
    const/4 v0, 0x5

    #@87
    new-array v0, v0, [Ljava/lang/String;

    #@89
    const-string v3, "0 AS suggest_format"

    #@8b
    aput-object v3, v0, v2

    #@8d
    const-string v2, "\'android.resource://system/17301578\' AS suggest_icon_1"

    #@8f
    aput-object v2, v0, v1

    #@91
    const-string v1, "display1 AS suggest_text_1"

    #@93
    aput-object v1, v0, v5

    #@95
    const-string/jumbo v1, "query AS suggest_intent_query"

    #@98
    aput-object v1, v0, v6

    #@9a
    const-string v1, "_id"

    #@9c
    aput-object v1, v0, v7

    #@9e
    iput-object v0, p0, Landroid/content/SearchRecentSuggestionsProvider;->mSuggestionProjection:[Ljava/lang/String;

    #@a0
    goto :goto_7f
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 7
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 394
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Not implemented"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method
