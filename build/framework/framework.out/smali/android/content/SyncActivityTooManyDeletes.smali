.class public Landroid/content/SyncActivityTooManyDeletes;
.super Landroid/app/Activity;
.source "SyncActivityTooManyDeletes.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mNumDeletes:J

.field private mProvider:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    return-void
.end method

.method private startSyncReallyDelete()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 120
    new-instance v0, Landroid/os/Bundle;

    #@3
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@6
    .line 121
    .local v0, extras:Landroid/os/Bundle;
    const-string v1, "deletions_override"

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@b
    .line 122
    const-string v1, "force"

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@10
    .line 123
    const-string v1, "expedited"

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@15
    .line 124
    const-string/jumbo v1, "upload"

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@1b
    .line 125
    iget-object v1, p0, Landroid/content/SyncActivityTooManyDeletes;->mAccount:Landroid/accounts/Account;

    #@1d
    iget-object v2, p0, Landroid/content/SyncActivityTooManyDeletes;->mAuthority:Ljava/lang/String;

    #@1f
    invoke-static {v1, v2, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    #@22
    .line 126
    return-void
.end method

.method private startSyncUndoDeletes()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 129
    new-instance v0, Landroid/os/Bundle;

    #@3
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@6
    .line 130
    .local v0, extras:Landroid/os/Bundle;
    const-string v1, "discard_deletions"

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@b
    .line 131
    const-string v1, "force"

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@10
    .line 132
    const-string v1, "expedited"

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@15
    .line 133
    const-string/jumbo v1, "upload"

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@1b
    .line 134
    iget-object v1, p0, Landroid/content/SyncActivityTooManyDeletes;->mAccount:Landroid/accounts/Account;

    #@1d
    iget-object v2, p0, Landroid/content/SyncActivityTooManyDeletes;->mAuthority:Ljava/lang/String;

    #@1f
    invoke-static {v1, v2, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    #@22
    .line 135
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 15
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 50
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_RESOURCE:Z

    #@5
    if-eqz v8, :cond_d

    #@7
    .line 51
    const v8, 0x20a01c5

    #@a
    invoke-virtual {p0, v8}, Landroid/content/SyncActivityTooManyDeletes;->setTheme(I)V

    #@d
    .line 53
    :cond_d
    invoke-virtual {p0}, Landroid/content/SyncActivityTooManyDeletes;->getIntent()Landroid/content/Intent;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@14
    move-result-object v1

    #@15
    .line 54
    .local v1, extras:Landroid/os/Bundle;
    if-nez v1, :cond_1b

    #@17
    .line 55
    invoke-virtual {p0}, Landroid/content/SyncActivityTooManyDeletes;->finish()V

    #@1a
    .line 110
    :goto_1a
    return-void

    #@1b
    .line 59
    :cond_1b
    const-string/jumbo v8, "numDeletes"

    #@1e
    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    #@21
    move-result-wide v8

    #@22
    iput-wide v8, p0, Landroid/content/SyncActivityTooManyDeletes;->mNumDeletes:J

    #@24
    .line 60
    const-string v8, "account"

    #@26
    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@29
    move-result-object v8

    #@2a
    check-cast v8, Landroid/accounts/Account;

    #@2c
    iput-object v8, p0, Landroid/content/SyncActivityTooManyDeletes;->mAccount:Landroid/accounts/Account;

    #@2e
    .line 61
    const-string v8, "authority"

    #@30
    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v8

    #@34
    iput-object v8, p0, Landroid/content/SyncActivityTooManyDeletes;->mAuthority:Ljava/lang/String;

    #@36
    .line 62
    const-string/jumbo v8, "provider"

    #@39
    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v8

    #@3d
    iput-object v8, p0, Landroid/content/SyncActivityTooManyDeletes;->mProvider:Ljava/lang/String;

    #@3f
    .line 65
    const/4 v8, 0x3

    #@40
    new-array v5, v8, [Ljava/lang/CharSequence;

    #@42
    const/4 v8, 0x0

    #@43
    invoke-virtual {p0}, Landroid/content/SyncActivityTooManyDeletes;->getResources()Landroid/content/res/Resources;

    #@46
    move-result-object v9

    #@47
    const v10, 0x10404dd

    #@4a
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@4d
    move-result-object v9

    #@4e
    aput-object v9, v5, v8

    #@50
    const/4 v8, 0x1

    #@51
    invoke-virtual {p0}, Landroid/content/SyncActivityTooManyDeletes;->getResources()Landroid/content/res/Resources;

    #@54
    move-result-object v9

    #@55
    const v10, 0x10404de

    #@58
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@5b
    move-result-object v9

    #@5c
    aput-object v9, v5, v8

    #@5e
    const/4 v8, 0x2

    #@5f
    invoke-virtual {p0}, Landroid/content/SyncActivityTooManyDeletes;->getResources()Landroid/content/res/Resources;

    #@62
    move-result-object v9

    #@63
    const v10, 0x10404df

    #@66
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@69
    move-result-object v9

    #@6a
    aput-object v9, v5, v8

    #@6c
    .line 71
    .local v5, options:[Ljava/lang/CharSequence;
    new-instance v0, Landroid/widget/ArrayAdapter;

    #@6e
    const v8, 0x1090003

    #@71
    const v9, 0x1020014

    #@74
    invoke-direct {v0, p0, v8, v9, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    #@77
    .line 76
    .local v0, adapter:Landroid/widget/ListAdapter;
    new-instance v2, Landroid/widget/ListView;

    #@79
    invoke-direct {v2, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    #@7c
    .line 77
    .local v2, listView:Landroid/widget/ListView;
    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@7f
    .line 78
    const/4 v8, 0x1

    #@80
    invoke-virtual {v2, v8}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    #@83
    .line 79
    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@86
    .line 81
    new-instance v6, Landroid/widget/TextView;

    #@88
    invoke-direct {v6, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    #@8b
    .line 82
    .local v6, textView:Landroid/widget/TextView;
    invoke-virtual {p0}, Landroid/content/SyncActivityTooManyDeletes;->getResources()Landroid/content/res/Resources;

    #@8e
    move-result-object v8

    #@8f
    const v9, 0x10404dc

    #@92
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@95
    move-result-object v7

    #@96
    .line 84
    .local v7, tooManyDeletesDescFormat:Ljava/lang/CharSequence;
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@99
    move-result-object v8

    #@9a
    const/4 v9, 0x3

    #@9b
    new-array v9, v9, [Ljava/lang/Object;

    #@9d
    const/4 v10, 0x0

    #@9e
    iget-wide v11, p0, Landroid/content/SyncActivityTooManyDeletes;->mNumDeletes:J

    #@a0
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@a3
    move-result-object v11

    #@a4
    aput-object v11, v9, v10

    #@a6
    const/4 v10, 0x1

    #@a7
    iget-object v11, p0, Landroid/content/SyncActivityTooManyDeletes;->mProvider:Ljava/lang/String;

    #@a9
    aput-object v11, v9, v10

    #@ab
    const/4 v10, 0x2

    #@ac
    iget-object v11, p0, Landroid/content/SyncActivityTooManyDeletes;->mAccount:Landroid/accounts/Account;

    #@ae
    iget-object v11, v11, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@b0
    aput-object v11, v9, v10

    #@b2
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@b5
    move-result-object v8

    #@b6
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@b9
    .line 87
    new-instance v3, Landroid/widget/LinearLayout;

    #@bb
    invoke-direct {v3, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@be
    .line 88
    .local v3, ll:Landroid/widget/LinearLayout;
    const/4 v8, 0x1

    #@bf
    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    #@c2
    .line 89
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    #@c4
    const/4 v8, -0x1

    #@c5
    const/4 v9, -0x2

    #@c6
    const/4 v10, 0x0

    #@c7
    invoke-direct {v4, v8, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    #@ca
    .line 91
    .local v4, lp:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v6, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@cd
    .line 92
    invoke-virtual {v3, v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@d0
    .line 109
    invoke-virtual {p0, v3}, Landroid/content/SyncActivityTooManyDeletes;->setContentView(Landroid/view/View;)V

    #@d3
    goto/16 :goto_1a
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 114
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    if-nez p3, :cond_9

    #@2
    invoke-direct {p0}, Landroid/content/SyncActivityTooManyDeletes;->startSyncReallyDelete()V

    #@5
    .line 116
    :cond_5
    :goto_5
    invoke-virtual {p0}, Landroid/content/SyncActivityTooManyDeletes;->finish()V

    #@8
    .line 117
    return-void

    #@9
    .line 115
    :cond_9
    const/4 v0, 0x1

    #@a
    if-ne p3, v0, :cond_5

    #@c
    invoke-direct {p0}, Landroid/content/SyncActivityTooManyDeletes;->startSyncUndoDeletes()V

    #@f
    goto :goto_5
.end method
