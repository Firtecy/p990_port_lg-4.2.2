.class public Landroid/content/SyncOperation;
.super Ljava/lang/Object;
.source "SyncOperation.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final account:Landroid/accounts/Account;

.field public final allowParallelSyncs:Z

.field public authority:Ljava/lang/String;

.field public backoff:Ljava/lang/Long;

.field public delayUntil:J

.field public earliestRunTime:J

.field public effectiveRunTime:J

.field public expedited:Z

.field public extras:Landroid/os/Bundle;

.field public final key:Ljava/lang/String;

.field public pendingOperation:Landroid/content/SyncStorageEngine$PendingOperation;

.field public syncSource:I

.field public final userId:I


# direct methods
.method public constructor <init>(Landroid/accounts/Account;IILjava/lang/String;Landroid/os/Bundle;JJJZ)V
    .registers 18
    .parameter "account"
    .parameter "userId"
    .parameter "source"
    .parameter "authority"
    .parameter "extras"
    .parameter "delayInMs"
    .parameter "backoff"
    .parameter "delayUntil"
    .parameter "allowParallelSyncs"

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 44
    iput-object p1, p0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@5
    .line 45
    iput p2, p0, Landroid/content/SyncOperation;->userId:I

    #@7
    .line 46
    iput p3, p0, Landroid/content/SyncOperation;->syncSource:I

    #@9
    .line 47
    iput-object p4, p0, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@b
    .line 48
    move/from16 v0, p12

    #@d
    iput-boolean v0, p0, Landroid/content/SyncOperation;->allowParallelSyncs:Z

    #@f
    .line 49
    new-instance v3, Landroid/os/Bundle;

    #@11
    invoke-direct {v3, p5}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@14
    iput-object v3, p0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@16
    .line 50
    const-string/jumbo v3, "upload"

    #@19
    invoke-direct {p0, v3}, Landroid/content/SyncOperation;->removeFalseExtra(Ljava/lang/String;)V

    #@1c
    .line 51
    const-string v3, "force"

    #@1e
    invoke-direct {p0, v3}, Landroid/content/SyncOperation;->removeFalseExtra(Ljava/lang/String;)V

    #@21
    .line 52
    const-string v3, "ignore_settings"

    #@23
    invoke-direct {p0, v3}, Landroid/content/SyncOperation;->removeFalseExtra(Ljava/lang/String;)V

    #@26
    .line 53
    const-string v3, "ignore_backoff"

    #@28
    invoke-direct {p0, v3}, Landroid/content/SyncOperation;->removeFalseExtra(Ljava/lang/String;)V

    #@2b
    .line 54
    const-string v3, "do_not_retry"

    #@2d
    invoke-direct {p0, v3}, Landroid/content/SyncOperation;->removeFalseExtra(Ljava/lang/String;)V

    #@30
    .line 55
    const-string v3, "discard_deletions"

    #@32
    invoke-direct {p0, v3}, Landroid/content/SyncOperation;->removeFalseExtra(Ljava/lang/String;)V

    #@35
    .line 56
    const-string v3, "expedited"

    #@37
    invoke-direct {p0, v3}, Landroid/content/SyncOperation;->removeFalseExtra(Ljava/lang/String;)V

    #@3a
    .line 57
    const-string v3, "deletions_override"

    #@3c
    invoke-direct {p0, v3}, Landroid/content/SyncOperation;->removeFalseExtra(Ljava/lang/String;)V

    #@3f
    .line 58
    iput-wide p10, p0, Landroid/content/SyncOperation;->delayUntil:J

    #@41
    .line 59
    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@44
    move-result-object v3

    #@45
    iput-object v3, p0, Landroid/content/SyncOperation;->backoff:Ljava/lang/Long;

    #@47
    .line 60
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@4a
    move-result-wide v1

    #@4b
    .line 61
    .local v1, now:J
    const-wide/16 v3, 0x0

    #@4d
    cmp-long v3, p6, v3

    #@4f
    if-gez v3, :cond_60

    #@51
    .line 62
    const/4 v3, 0x1

    #@52
    iput-boolean v3, p0, Landroid/content/SyncOperation;->expedited:Z

    #@54
    .line 63
    iput-wide v1, p0, Landroid/content/SyncOperation;->earliestRunTime:J

    #@56
    .line 68
    :goto_56
    invoke-virtual {p0}, Landroid/content/SyncOperation;->updateEffectiveRunTime()V

    #@59
    .line 69
    invoke-direct {p0}, Landroid/content/SyncOperation;->toKey()Ljava/lang/String;

    #@5c
    move-result-object v3

    #@5d
    iput-object v3, p0, Landroid/content/SyncOperation;->key:Ljava/lang/String;

    #@5f
    .line 70
    return-void

    #@60
    .line 65
    :cond_60
    const/4 v3, 0x0

    #@61
    iput-boolean v3, p0, Landroid/content/SyncOperation;->expedited:Z

    #@63
    .line 66
    add-long v3, v1, p6

    #@65
    iput-wide v3, p0, Landroid/content/SyncOperation;->earliestRunTime:J

    #@67
    goto :goto_56
.end method

.method constructor <init>(Landroid/content/SyncOperation;)V
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 79
    iget-object v0, p1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@5
    iput-object v0, p0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@7
    .line 80
    iget v0, p1, Landroid/content/SyncOperation;->userId:I

    #@9
    iput v0, p0, Landroid/content/SyncOperation;->userId:I

    #@b
    .line 81
    iget v0, p1, Landroid/content/SyncOperation;->syncSource:I

    #@d
    iput v0, p0, Landroid/content/SyncOperation;->syncSource:I

    #@f
    .line 82
    iget-object v0, p1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@11
    iput-object v0, p0, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@13
    .line 83
    new-instance v0, Landroid/os/Bundle;

    #@15
    iget-object v1, p1, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@17
    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@1a
    iput-object v0, p0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@1c
    .line 84
    iget-boolean v0, p1, Landroid/content/SyncOperation;->expedited:Z

    #@1e
    iput-boolean v0, p0, Landroid/content/SyncOperation;->expedited:Z

    #@20
    .line 85
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@23
    move-result-wide v0

    #@24
    iput-wide v0, p0, Landroid/content/SyncOperation;->earliestRunTime:J

    #@26
    .line 86
    iget-object v0, p1, Landroid/content/SyncOperation;->backoff:Ljava/lang/Long;

    #@28
    iput-object v0, p0, Landroid/content/SyncOperation;->backoff:Ljava/lang/Long;

    #@2a
    .line 87
    iget-wide v0, p1, Landroid/content/SyncOperation;->delayUntil:J

    #@2c
    iput-wide v0, p0, Landroid/content/SyncOperation;->delayUntil:J

    #@2e
    .line 88
    iget-boolean v0, p1, Landroid/content/SyncOperation;->allowParallelSyncs:Z

    #@30
    iput-boolean v0, p0, Landroid/content/SyncOperation;->allowParallelSyncs:Z

    #@32
    .line 89
    invoke-virtual {p0}, Landroid/content/SyncOperation;->updateEffectiveRunTime()V

    #@35
    .line 90
    invoke-direct {p0}, Landroid/content/SyncOperation;->toKey()Ljava/lang/String;

    #@38
    move-result-object v0

    #@39
    iput-object v0, p0, Landroid/content/SyncOperation;->key:Ljava/lang/String;

    #@3b
    .line 91
    return-void
.end method

.method public static extrasToStringBuilder(Landroid/os/Bundle;Ljava/lang/StringBuilder;)V
    .registers 6
    .parameter "bundle"
    .parameter "sb"

    #@0
    .prologue
    .line 143
    const-string v2, "["

    #@2
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5
    .line 144
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@8
    move-result-object v2

    #@9
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v0

    #@d
    .local v0, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_31

    #@13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Ljava/lang/String;

    #@19
    .line 145
    .local v1, key:Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "="

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {p0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, " "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    goto :goto_d

    #@31
    .line 147
    .end local v1           #key:Ljava/lang/String;
    :cond_31
    const-string v2, "]"

    #@33
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 148
    return-void
.end method

.method private removeFalseExtra(Ljava/lang/String;)V
    .registers 4
    .parameter "extraName"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_e

    #@9
    .line 74
    iget-object v0, p0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@b
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@e
    .line 76
    :cond_e
    return-void
.end method

.method private toKey()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 134
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "authority: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget-object v2, p0, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, " account {name="

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    iget-object v2, p0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@1d
    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, ", user="

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    iget v2, p0, Landroid/content/SyncOperation;->userId:I

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    const-string v2, ", type="

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    iget-object v2, p0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@37
    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string/jumbo v2, "}"

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    .line 137
    const-string v1, " extras: "

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    .line 138
    iget-object v1, p0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@52
    invoke-static {v1, v0}, Landroid/content/SyncOperation;->extrasToStringBuilder(Landroid/os/Bundle;Ljava/lang/StringBuilder;)V

    #@55
    .line 139
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    return-object v1
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .registers 9
    .parameter "o"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, -0x1

    #@2
    .line 159
    move-object v0, p1

    #@3
    check-cast v0, Landroid/content/SyncOperation;

    #@5
    .line 161
    .local v0, other:Landroid/content/SyncOperation;
    iget-boolean v3, p0, Landroid/content/SyncOperation;->expedited:Z

    #@7
    iget-boolean v4, v0, Landroid/content/SyncOperation;->expedited:Z

    #@9
    if-eq v3, v4, :cond_12

    #@b
    .line 162
    iget-boolean v3, p0, Landroid/content/SyncOperation;->expedited:Z

    #@d
    if-eqz v3, :cond_10

    #@f
    .line 169
    :cond_f
    :goto_f
    return v1

    #@10
    :cond_10
    move v1, v2

    #@11
    .line 162
    goto :goto_f

    #@12
    .line 165
    :cond_12
    iget-wide v3, p0, Landroid/content/SyncOperation;->effectiveRunTime:J

    #@14
    iget-wide v5, v0, Landroid/content/SyncOperation;->effectiveRunTime:J

    #@16
    cmp-long v3, v3, v5

    #@18
    if-nez v3, :cond_1c

    #@1a
    .line 166
    const/4 v1, 0x0

    #@1b
    goto :goto_f

    #@1c
    .line 169
    :cond_1c
    iget-wide v3, p0, Landroid/content/SyncOperation;->effectiveRunTime:J

    #@1e
    iget-wide v5, v0, Landroid/content/SyncOperation;->effectiveRunTime:J

    #@20
    cmp-long v3, v3, v5

    #@22
    if-ltz v3, :cond_f

    #@24
    move v1, v2

    #@25
    goto :goto_f
.end method

.method public dump(Z)Ljava/lang/String;
    .registers 6
    .parameter "useOneLine"

    #@0
    .prologue
    .line 98
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    iget-object v2, p0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@7
    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, " u"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget v2, p0, Landroid/content/SyncOperation;->userId:I

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " ("

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    iget-object v2, p0, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@21
    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, ")"

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    const-string v2, ", "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    iget-object v2, p0, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    const-string v2, ", "

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    sget-object v2, Landroid/content/SyncStorageEngine;->SOURCES:[Ljava/lang/String;

    #@41
    iget v3, p0, Landroid/content/SyncOperation;->syncSource:I

    #@43
    aget-object v2, v2, v3

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    const-string v2, ", earliestRunTime "

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    iget-wide v2, p0, Landroid/content/SyncOperation;->earliestRunTime:J

    #@51
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    .line 110
    .local v0, sb:Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Landroid/content/SyncOperation;->expedited:Z

    #@57
    if-eqz v1, :cond_5e

    #@59
    .line 111
    const-string v1, ", EXPEDITED"

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    .line 113
    :cond_5e
    if-nez p1, :cond_76

    #@60
    iget-object v1, p0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@62
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@65
    move-result-object v1

    #@66
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    #@69
    move-result v1

    #@6a
    if-nez v1, :cond_76

    #@6c
    .line 114
    const-string v1, "\n    "

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    .line 115
    iget-object v1, p0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@73
    invoke-static {v1, v0}, Landroid/content/SyncOperation;->extrasToStringBuilder(Landroid/os/Bundle;Ljava/lang/StringBuilder;)V

    #@76
    .line 117
    :cond_76
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v1

    #@7a
    return-object v1
.end method

.method public ignoreBackoff()Z
    .registers 4

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@2
    const-string v1, "ignore_backoff"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public isExpedited()Z
    .registers 4

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@2
    const-string v1, "expedited"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public isInitialization()Z
    .registers 4

    #@0
    .prologue
    .line 121
    iget-object v0, p0, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@2
    const-string v1, "initialize"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 94
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/content/SyncOperation;->dump(Z)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public updateEffectiveRunTime()V
    .registers 5

    #@0
    .prologue
    .line 151
    invoke-virtual {p0}, Landroid/content/SyncOperation;->ignoreBackoff()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    iget-wide v0, p0, Landroid/content/SyncOperation;->earliestRunTime:J

    #@8
    :goto_8
    iput-wide v0, p0, Landroid/content/SyncOperation;->effectiveRunTime:J

    #@a
    .line 156
    return-void

    #@b
    .line 151
    :cond_b
    iget-wide v0, p0, Landroid/content/SyncOperation;->earliestRunTime:J

    #@d
    iget-wide v2, p0, Landroid/content/SyncOperation;->delayUntil:J

    #@f
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    #@12
    move-result-wide v0

    #@13
    iget-object v2, p0, Landroid/content/SyncOperation;->backoff:Ljava/lang/Long;

    #@15
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@18
    move-result-wide v2

    #@19
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    #@1c
    move-result-wide v0

    #@1d
    goto :goto_8
.end method
