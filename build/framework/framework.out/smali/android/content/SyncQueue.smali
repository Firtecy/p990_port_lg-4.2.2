.class public Landroid/content/SyncQueue;
.super Ljava/lang/Object;
.source "SyncQueue.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SyncManager"


# instance fields
.field private final mOperationsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/SyncOperation;",
            ">;"
        }
    .end annotation
.end field

.field private final mSyncAdapters:Landroid/content/SyncAdaptersCache;

.field private final mSyncStorageEngine:Landroid/content/SyncStorageEngine;


# direct methods
.method public constructor <init>(Landroid/content/SyncStorageEngine;Landroid/content/SyncAdaptersCache;)V
    .registers 4
    .parameter "syncStorageEngine"
    .parameter "syncAdapters"

    #@0
    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 49
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@9
    .line 52
    iput-object p1, p0, Landroid/content/SyncQueue;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@b
    .line 53
    iput-object p2, p0, Landroid/content/SyncQueue;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@d
    .line 54
    return-void
.end method

.method private add(Landroid/content/SyncOperation;Landroid/content/SyncStorageEngine$PendingOperation;)Z
    .registers 16
    .parameter "operation"
    .parameter "pop"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    .line 91
    iget-object v11, p1, Landroid/content/SyncOperation;->key:Ljava/lang/String;

    #@3
    .line 92
    .local v11, operationKey:Ljava/lang/String;
    iget-object v0, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@5
    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v8

    #@9
    check-cast v8, Landroid/content/SyncOperation;

    #@b
    .line 94
    .local v8, existingOperation:Landroid/content/SyncOperation;
    if-eqz v8, :cond_2e

    #@d
    .line 95
    const/4 v7, 0x0

    #@e
    .line 96
    .local v7, changed:Z
    iget-boolean v0, v8, Landroid/content/SyncOperation;->expedited:Z

    #@10
    iget-boolean v1, p1, Landroid/content/SyncOperation;->expedited:Z

    #@12
    if-ne v0, v1, :cond_26

    #@14
    .line 97
    iget-wide v0, v8, Landroid/content/SyncOperation;->earliestRunTime:J

    #@16
    iget-wide v2, p1, Landroid/content/SyncOperation;->earliestRunTime:J

    #@18
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    #@1b
    move-result-wide v9

    #@1c
    .line 99
    .local v9, newRunTime:J
    iget-wide v0, v8, Landroid/content/SyncOperation;->earliestRunTime:J

    #@1e
    cmp-long v0, v0, v9

    #@20
    if-eqz v0, :cond_25

    #@22
    .line 100
    iput-wide v9, v8, Landroid/content/SyncOperation;->earliestRunTime:J

    #@24
    .line 101
    const/4 v7, 0x1

    #@25
    .line 126
    .end local v7           #changed:Z
    .end local v9           #newRunTime:J
    :cond_25
    :goto_25
    return v7

    #@26
    .line 104
    .restart local v7       #changed:Z
    :cond_26
    iget-boolean v0, p1, Landroid/content/SyncOperation;->expedited:Z

    #@28
    if-eqz v0, :cond_25

    #@2a
    .line 105
    iput-boolean v12, v8, Landroid/content/SyncOperation;->expedited:Z

    #@2c
    .line 106
    const/4 v7, 0x1

    #@2d
    goto :goto_25

    #@2e
    .line 112
    .end local v7           #changed:Z
    :cond_2e
    iput-object p2, p1, Landroid/content/SyncOperation;->pendingOperation:Landroid/content/SyncStorageEngine$PendingOperation;

    #@30
    .line 113
    iget-object v0, p1, Landroid/content/SyncOperation;->pendingOperation:Landroid/content/SyncStorageEngine$PendingOperation;

    #@32
    if-nez v0, :cond_69

    #@34
    .line 114
    new-instance p2, Landroid/content/SyncStorageEngine$PendingOperation;

    #@36
    .end local p2
    iget-object v1, p1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@38
    iget v2, p1, Landroid/content/SyncOperation;->userId:I

    #@3a
    iget v3, p1, Landroid/content/SyncOperation;->syncSource:I

    #@3c
    iget-object v4, p1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@3e
    iget-object v5, p1, Landroid/content/SyncOperation;->extras:Landroid/os/Bundle;

    #@40
    iget-boolean v6, p1, Landroid/content/SyncOperation;->expedited:Z

    #@42
    move-object v0, p2

    #@43
    invoke-direct/range {v0 .. v6}, Landroid/content/SyncStorageEngine$PendingOperation;-><init>(Landroid/accounts/Account;IILjava/lang/String;Landroid/os/Bundle;Z)V

    #@46
    .line 117
    .restart local p2
    iget-object v0, p0, Landroid/content/SyncQueue;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@48
    invoke-virtual {v0, p2}, Landroid/content/SyncStorageEngine;->insertIntoPending(Landroid/content/SyncStorageEngine$PendingOperation;)Landroid/content/SyncStorageEngine$PendingOperation;

    #@4b
    move-result-object p2

    #@4c
    .line 118
    if-nez p2, :cond_67

    #@4e
    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    #@50
    new-instance v1, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v2, "error adding pending sync operation "

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v1

    #@63
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@66
    throw v0

    #@67
    .line 122
    :cond_67
    iput-object p2, p1, Landroid/content/SyncOperation;->pendingOperation:Landroid/content/SyncStorageEngine$PendingOperation;

    #@69
    .line 125
    :cond_69
    iget-object v0, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@6b
    invoke-virtual {v0, v11, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6e
    move v7, v12

    #@6f
    .line 126
    goto :goto_25
.end method


# virtual methods
.method public add(Landroid/content/SyncOperation;)Z
    .registers 3
    .parameter "operation"

    #@0
    .prologue
    .line 81
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/content/SyncQueue;->add(Landroid/content/SyncOperation;Landroid/content/SyncStorageEngine$PendingOperation;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public addPendingOperations(I)V
    .registers 22
    .parameter "userId"

    #@0
    .prologue
    .line 57
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Landroid/content/SyncQueue;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@4
    invoke-virtual {v3}, Landroid/content/SyncStorageEngine;->getPendingOperations()Ljava/util/ArrayList;

    #@7
    move-result-object v3

    #@8
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@b
    move-result-object v16

    #@c
    .local v16, i$:Ljava/util/Iterator;
    :cond_c
    :goto_c
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_d8

    #@12
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15
    move-result-object v17

    #@16
    check-cast v17, Landroid/content/SyncStorageEngine$PendingOperation;

    #@18
    .line 58
    .local v17, op:Landroid/content/SyncStorageEngine$PendingOperation;
    move-object/from16 v0, v17

    #@1a
    iget v3, v0, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@1c
    move/from16 v0, p1

    #@1e
    if-ne v3, v0, :cond_c

    #@20
    .line 60
    move-object/from16 v0, p0

    #@22
    iget-object v3, v0, Landroid/content/SyncQueue;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@24
    move-object/from16 v0, v17

    #@26
    iget-object v4, v0, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@28
    move-object/from16 v0, v17

    #@2a
    iget v5, v0, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@2c
    move-object/from16 v0, v17

    #@2e
    iget-object v6, v0, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@30
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/SyncStorageEngine;->getBackoff(Landroid/accounts/Account;ILjava/lang/String;)Landroid/util/Pair;

    #@33
    move-result-object v15

    #@34
    .line 62
    .local v15, backoff:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    #@36
    iget-object v3, v0, Landroid/content/SyncQueue;->mSyncAdapters:Landroid/content/SyncAdaptersCache;

    #@38
    move-object/from16 v0, v17

    #@3a
    iget-object v4, v0, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@3c
    move-object/from16 v0, v17

    #@3e
    iget-object v5, v0, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@40
    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@42
    invoke-static {v4, v5}, Landroid/content/SyncAdapterType;->newKey(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SyncAdapterType;

    #@45
    move-result-object v4

    #@46
    move-object/from16 v0, v17

    #@48
    iget v5, v0, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@4a
    invoke-virtual {v3, v4, v5}, Landroid/content/SyncAdaptersCache;->getServiceInfo(Ljava/lang/Object;I)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@4d
    move-result-object v18

    #@4e
    .line 64
    .local v18, syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    if-nez v18, :cond_7b

    #@50
    .line 65
    const-string v3, "SyncManager"

    #@52
    new-instance v4, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v5, "Missing sync adapter info for authority "

    #@59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    move-object/from16 v0, v17

    #@5f
    iget-object v5, v0, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    const-string v5, ", userId "

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    move-object/from16 v0, v17

    #@6d
    iget v5, v0, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@6f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v4

    #@73
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v4

    #@77
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    goto :goto_c

    #@7b
    .line 69
    :cond_7b
    new-instance v2, Landroid/content/SyncOperation;

    #@7d
    move-object/from16 v0, v17

    #@7f
    iget-object v3, v0, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@81
    move-object/from16 v0, v17

    #@83
    iget v4, v0, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@85
    move-object/from16 v0, v17

    #@87
    iget v5, v0, Landroid/content/SyncStorageEngine$PendingOperation;->syncSource:I

    #@89
    move-object/from16 v0, v17

    #@8b
    iget-object v6, v0, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@8d
    move-object/from16 v0, v17

    #@8f
    iget-object v7, v0, Landroid/content/SyncStorageEngine$PendingOperation;->extras:Landroid/os/Bundle;

    #@91
    const-wide/16 v8, 0x0

    #@93
    if-eqz v15, :cond_d5

    #@95
    iget-object v10, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@97
    check-cast v10, Ljava/lang/Long;

    #@99
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    #@9c
    move-result-wide v10

    #@9d
    :goto_9d
    move-object/from16 v0, p0

    #@9f
    iget-object v12, v0, Landroid/content/SyncQueue;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@a1
    move-object/from16 v0, v17

    #@a3
    iget-object v13, v0, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@a5
    move-object/from16 v0, v17

    #@a7
    iget v14, v0, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@a9
    move-object/from16 v0, v17

    #@ab
    iget-object v0, v0, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@ad
    move-object/from16 v19, v0

    #@af
    move-object/from16 v0, v19

    #@b1
    invoke-virtual {v12, v13, v14, v0}, Landroid/content/SyncStorageEngine;->getDelayUntilTime(Landroid/accounts/Account;ILjava/lang/String;)J

    #@b4
    move-result-wide v12

    #@b5
    move-object/from16 v0, v18

    #@b7
    iget-object v14, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@b9
    check-cast v14, Landroid/content/SyncAdapterType;

    #@bb
    invoke-virtual {v14}, Landroid/content/SyncAdapterType;->allowParallelSyncs()Z

    #@be
    move-result v14

    #@bf
    invoke-direct/range {v2 .. v14}, Landroid/content/SyncOperation;-><init>(Landroid/accounts/Account;IILjava/lang/String;Landroid/os/Bundle;JJJZ)V

    #@c2
    .line 74
    .local v2, syncOperation:Landroid/content/SyncOperation;
    move-object/from16 v0, v17

    #@c4
    iget-boolean v3, v0, Landroid/content/SyncStorageEngine$PendingOperation;->expedited:Z

    #@c6
    iput-boolean v3, v2, Landroid/content/SyncOperation;->expedited:Z

    #@c8
    .line 75
    move-object/from16 v0, v17

    #@ca
    iput-object v0, v2, Landroid/content/SyncOperation;->pendingOperation:Landroid/content/SyncStorageEngine$PendingOperation;

    #@cc
    .line 76
    move-object/from16 v0, p0

    #@ce
    move-object/from16 v1, v17

    #@d0
    invoke-direct {v0, v2, v1}, Landroid/content/SyncQueue;->add(Landroid/content/SyncOperation;Landroid/content/SyncStorageEngine$PendingOperation;)Z

    #@d3
    goto/16 :goto_c

    #@d5
    .line 69
    .end local v2           #syncOperation:Landroid/content/SyncOperation;
    :cond_d5
    const-wide/16 v10, 0x0

    #@d7
    goto :goto_9d

    #@d8
    .line 78
    .end local v15           #backoff:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    .end local v17           #op:Landroid/content/SyncStorageEngine$PendingOperation;
    .end local v18           #syncAdapterInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/content/SyncAdapterType;>;"
    :cond_d8
    return-void
.end method

.method public dump(Ljava/lang/StringBuilder;)V
    .registers 10
    .parameter "sb"

    #@0
    .prologue
    .line 207
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v1

    #@4
    .line 208
    .local v1, now:J
    const-string v4, "SyncQueue: "

    #@6
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9
    move-result-object v4

    #@a
    iget-object v5, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@c
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@f
    move-result v5

    #@10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    const-string v5, " operation(s)\n"

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 209
    iget-object v4, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@1b
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@1e
    move-result-object v4

    #@1f
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@22
    move-result-object v0

    #@23
    .local v0, i$:Ljava/util/Iterator;
    :goto_23
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_61

    #@29
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Landroid/content/SyncOperation;

    #@2f
    .line 210
    .local v3, operation:Landroid/content/SyncOperation;
    const-string v4, "  "

    #@31
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 211
    iget-wide v4, v3, Landroid/content/SyncOperation;->effectiveRunTime:J

    #@36
    cmp-long v4, v4, v1

    #@38
    if-gtz v4, :cond_53

    #@3a
    .line 212
    const-string v4, "READY"

    #@3c
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    .line 216
    :goto_3f
    const-string v4, " - "

    #@41
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    .line 217
    const/4 v4, 0x0

    #@45
    invoke-virtual {v3, v4}, Landroid/content/SyncOperation;->dump(Z)Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    const-string v5, "\n"

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    goto :goto_23

    #@53
    .line 214
    :cond_53
    iget-wide v4, v3, Landroid/content/SyncOperation;->effectiveRunTime:J

    #@55
    sub-long/2addr v4, v1

    #@56
    const-wide/16 v6, 0x3e8

    #@58
    div-long/2addr v4, v6

    #@59
    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    goto :goto_3f

    #@61
    .line 219
    .end local v3           #operation:Landroid/content/SyncOperation;
    :cond_61
    return-void
.end method

.method public getOperations()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/SyncOperation;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public onBackoffChanged(Landroid/accounts/Account;ILjava/lang/String;J)V
    .registers 9
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"
    .parameter "backoff"

    #@0
    .prologue
    .line 160
    iget-object v2, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_34

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/content/SyncOperation;

    #@16
    .line 161
    .local v1, op:Landroid/content/SyncOperation;
    iget-object v2, v1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@18
    invoke-virtual {v2, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_a

    #@1e
    iget-object v2, v1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@20
    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_a

    #@26
    iget v2, v1, Landroid/content/SyncOperation;->userId:I

    #@28
    if-ne v2, p2, :cond_a

    #@2a
    .line 163
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2d
    move-result-object v2

    #@2e
    iput-object v2, v1, Landroid/content/SyncOperation;->backoff:Ljava/lang/Long;

    #@30
    .line 164
    invoke-virtual {v1}, Landroid/content/SyncOperation;->updateEffectiveRunTime()V

    #@33
    goto :goto_a

    #@34
    .line 167
    .end local v1           #op:Landroid/content/SyncOperation;
    :cond_34
    return-void
.end method

.method public onDelayUntilTimeChanged(Landroid/accounts/Account;Ljava/lang/String;J)V
    .registers 8
    .parameter "account"
    .parameter "providerName"
    .parameter "delayUntil"

    #@0
    .prologue
    .line 172
    iget-object v2, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_2c

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/content/SyncOperation;

    #@16
    .line 173
    .local v1, op:Landroid/content/SyncOperation;
    iget-object v2, v1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@18
    invoke-virtual {v2, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_a

    #@1e
    iget-object v2, v1, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@20
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_a

    #@26
    .line 174
    iput-wide p3, v1, Landroid/content/SyncOperation;->delayUntil:J

    #@28
    .line 175
    invoke-virtual {v1}, Landroid/content/SyncOperation;->updateEffectiveRunTime()V

    #@2b
    goto :goto_a

    #@2c
    .line 178
    .end local v1           #op:Landroid/content/SyncOperation;
    :cond_2c
    return-void
.end method

.method public remove(Landroid/accounts/Account;ILjava/lang/String;)V
    .registers 10
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    .line 181
    iget-object v4, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@5
    move-result-object v4

    #@6
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .line 182
    .local v0, entries:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Landroid/content/SyncOperation;>;>;"
    :cond_a
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_60

    #@10
    .line 183
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Ljava/util/Map$Entry;

    #@16
    .line 184
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Landroid/content/SyncOperation;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    check-cast v3, Landroid/content/SyncOperation;

    #@1c
    .line 185
    .local v3, syncOperation:Landroid/content/SyncOperation;
    if-eqz p1, :cond_26

    #@1e
    iget-object v4, v3, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@20
    invoke-virtual {v4, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_a

    #@26
    .line 188
    :cond_26
    if-eqz p3, :cond_30

    #@28
    iget-object v4, v3, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@2a
    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_a

    #@30
    .line 191
    :cond_30
    iget v4, v3, Landroid/content/SyncOperation;->userId:I

    #@32
    if-ne p2, v4, :cond_a

    #@34
    .line 194
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@37
    .line 195
    iget-object v4, p0, Landroid/content/SyncQueue;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@39
    iget-object v5, v3, Landroid/content/SyncOperation;->pendingOperation:Landroid/content/SyncStorageEngine$PendingOperation;

    #@3b
    invoke-virtual {v4, v5}, Landroid/content/SyncStorageEngine;->deleteFromPending(Landroid/content/SyncStorageEngine$PendingOperation;)Z

    #@3e
    move-result v4

    #@3f
    if-nez v4, :cond_a

    #@41
    .line 196
    new-instance v4, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string/jumbo v5, "unable to find pending row for "

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    .line 197
    .local v2, errorMessage:Ljava/lang/String;
    const-string v4, "SyncManager"

    #@57
    new-instance v5, Ljava/lang/IllegalStateException;

    #@59
    invoke-direct {v5, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@5c
    invoke-static {v4, v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5f
    goto :goto_a

    #@60
    .line 200
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Landroid/content/SyncOperation;>;"
    .end local v2           #errorMessage:Ljava/lang/String;
    .end local v3           #syncOperation:Landroid/content/SyncOperation;
    :cond_60
    return-void
.end method

.method public remove(Landroid/content/SyncOperation;)V
    .registers 6
    .parameter "operation"

    #@0
    .prologue
    .line 147
    iget-object v2, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@2
    iget-object v3, p1, Landroid/content/SyncOperation;->key:Ljava/lang/String;

    #@4
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Landroid/content/SyncOperation;

    #@a
    .line 148
    .local v1, operationToRemove:Landroid/content/SyncOperation;
    if-nez v1, :cond_d

    #@c
    .line 155
    :cond_c
    :goto_c
    return-void

    #@d
    .line 151
    :cond_d
    iget-object v2, p0, Landroid/content/SyncQueue;->mSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@f
    iget-object v3, v1, Landroid/content/SyncOperation;->pendingOperation:Landroid/content/SyncStorageEngine$PendingOperation;

    #@11
    invoke-virtual {v2, v3}, Landroid/content/SyncStorageEngine;->deleteFromPending(Landroid/content/SyncStorageEngine$PendingOperation;)Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_c

    #@17
    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string/jumbo v3, "unable to find pending row for "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    .line 153
    .local v0, errorMessage:Ljava/lang/String;
    const-string v2, "SyncManager"

    #@2d
    new-instance v3, Ljava/lang/IllegalStateException;

    #@2f
    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@32
    invoke-static {v2, v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@35
    goto :goto_c
.end method

.method public removeUser(I)V
    .registers 6
    .parameter "userId"

    #@0
    .prologue
    .line 130
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 131
    .local v2, opsToRemove:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncOperation;>;"
    iget-object v3, p0, Landroid/content/SyncQueue;->mOperationsMap:Ljava/util/HashMap;

    #@7
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@a
    move-result-object v3

    #@b
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v0

    #@f
    .local v0, i$:Ljava/util/Iterator;
    :cond_f
    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_23

    #@15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/content/SyncOperation;

    #@1b
    .line 132
    .local v1, op:Landroid/content/SyncOperation;
    iget v3, v1, Landroid/content/SyncOperation;->userId:I

    #@1d
    if-ne v3, p1, :cond_f

    #@1f
    .line 133
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@22
    goto :goto_f

    #@23
    .line 137
    .end local v1           #op:Landroid/content/SyncOperation;
    :cond_23
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@26
    move-result-object v0

    #@27
    :goto_27
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@2a
    move-result v3

    #@2b
    if-eqz v3, :cond_37

    #@2d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@30
    move-result-object v1

    #@31
    check-cast v1, Landroid/content/SyncOperation;

    #@33
    .line 138
    .restart local v1       #op:Landroid/content/SyncOperation;
    invoke-virtual {p0, v1}, Landroid/content/SyncQueue;->remove(Landroid/content/SyncOperation;)V

    #@36
    goto :goto_27

    #@37
    .line 140
    .end local v1           #op:Landroid/content/SyncOperation;
    :cond_37
    return-void
.end method
