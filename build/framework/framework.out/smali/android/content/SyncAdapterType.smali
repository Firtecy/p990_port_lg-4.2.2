.class public Landroid/content/SyncAdapterType;
.super Ljava/lang/Object;
.source "SyncAdapterType.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/SyncAdapterType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final accountType:Ljava/lang/String;

.field private final allowParallelSyncs:Z

.field public final authority:Ljava/lang/String;

.field private final isAlwaysSyncable:Z

.field public final isKey:Z

.field private final settingsActivity:Ljava/lang/String;

.field private final supportsUploading:Z

.field private final userVisible:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 217
    new-instance v0, Landroid/content/SyncAdapterType$1;

    #@2
    invoke-direct {v0}, Landroid/content/SyncAdapterType$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/SyncAdapterType;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 10
    .parameter "source"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 207
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_2f

    #@10
    move v3, v0

    #@11
    :goto_11
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_31

    #@17
    move v4, v0

    #@18
    :goto_18
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v5

    #@1c
    if-eqz v5, :cond_33

    #@1e
    move v5, v0

    #@1f
    :goto_1f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v7

    #@23
    if-eqz v7, :cond_26

    #@25
    move v6, v0

    #@26
    :cond_26
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@29
    move-result-object v7

    #@2a
    move-object v0, p0

    #@2b
    invoke-direct/range {v0 .. v7}, Landroid/content/SyncAdapterType;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;)V

    #@2e
    .line 215
    return-void

    #@2f
    :cond_2f
    move v3, v6

    #@30
    .line 207
    goto :goto_11

    #@31
    :cond_31
    move v4, v6

    #@32
    goto :goto_18

    #@33
    :cond_33
    move v5, v6

    #@34
    goto :goto_1f
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "authority"
    .parameter "accountType"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 77
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 78
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_25

    #@b
    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string/jumbo v2, "the authority must not be empty: "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 81
    :cond_25
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_45

    #@2b
    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2d
    new-instance v1, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string/jumbo v2, "the accountType must not be empty: "

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@44
    throw v0

    #@45
    .line 84
    :cond_45
    iput-object p1, p0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@47
    .line 85
    iput-object p2, p0, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    #@49
    .line 86
    iput-boolean v1, p0, Landroid/content/SyncAdapterType;->userVisible:Z

    #@4b
    .line 87
    iput-boolean v1, p0, Landroid/content/SyncAdapterType;->supportsUploading:Z

    #@4d
    .line 88
    iput-boolean v2, p0, Landroid/content/SyncAdapterType;->isAlwaysSyncable:Z

    #@4f
    .line 89
    iput-boolean v2, p0, Landroid/content/SyncAdapterType;->allowParallelSyncs:Z

    #@51
    .line 90
    const/4 v0, 0x0

    #@52
    iput-object v0, p0, Landroid/content/SyncAdapterType;->settingsActivity:Ljava/lang/String;

    #@54
    .line 91
    iput-boolean v1, p0, Landroid/content/SyncAdapterType;->isKey:Z

    #@56
    .line 92
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .registers 8
    .parameter "authority"
    .parameter "accountType"
    .parameter "userVisible"
    .parameter "supportsUploading"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 39
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_24

    #@a
    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string/jumbo v2, "the authority must not be empty: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 42
    :cond_24
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_44

    #@2a
    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2c
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string/jumbo v2, "the accountType must not be empty: "

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@43
    throw v0

    #@44
    .line 45
    :cond_44
    iput-object p1, p0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@46
    .line 46
    iput-object p2, p0, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    #@48
    .line 47
    iput-boolean p3, p0, Landroid/content/SyncAdapterType;->userVisible:Z

    #@4a
    .line 48
    iput-boolean p4, p0, Landroid/content/SyncAdapterType;->supportsUploading:Z

    #@4c
    .line 49
    iput-boolean v1, p0, Landroid/content/SyncAdapterType;->isAlwaysSyncable:Z

    #@4e
    .line 50
    iput-boolean v1, p0, Landroid/content/SyncAdapterType;->allowParallelSyncs:Z

    #@50
    .line 51
    const/4 v0, 0x0

    #@51
    iput-object v0, p0, Landroid/content/SyncAdapterType;->settingsActivity:Ljava/lang/String;

    #@53
    .line 52
    iput-boolean v1, p0, Landroid/content/SyncAdapterType;->isKey:Z

    #@55
    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;)V
    .registers 11
    .parameter "authority"
    .parameter "accountType"
    .parameter "userVisible"
    .parameter "supportsUploading"
    .parameter "isAlwaysSyncable"
    .parameter "allowParallelSyncs"
    .parameter "settingsActivity"

    #@0
    .prologue
    .line 60
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 61
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_23

    #@9
    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v2, "the authority must not be empty: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 64
    :cond_23
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_43

    #@29
    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2b
    new-instance v1, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string/jumbo v2, "the accountType must not be empty: "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@42
    throw v0

    #@43
    .line 67
    :cond_43
    iput-object p1, p0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@45
    .line 68
    iput-object p2, p0, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    #@47
    .line 69
    iput-boolean p3, p0, Landroid/content/SyncAdapterType;->userVisible:Z

    #@49
    .line 70
    iput-boolean p4, p0, Landroid/content/SyncAdapterType;->supportsUploading:Z

    #@4b
    .line 71
    iput-boolean p5, p0, Landroid/content/SyncAdapterType;->isAlwaysSyncable:Z

    #@4d
    .line 72
    iput-boolean p6, p0, Landroid/content/SyncAdapterType;->allowParallelSyncs:Z

    #@4f
    .line 73
    iput-object p7, p0, Landroid/content/SyncAdapterType;->settingsActivity:Ljava/lang/String;

    #@51
    .line 74
    const/4 v0, 0x0

    #@52
    iput-boolean v0, p0, Landroid/content/SyncAdapterType;->isKey:Z

    #@54
    .line 75
    return-void
.end method

.method public static newKey(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SyncAdapterType;
    .registers 3
    .parameter "authority"
    .parameter "accountType"

    #@0
    .prologue
    .line 152
    new-instance v0, Landroid/content/SyncAdapterType;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/content/SyncAdapterType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    return-object v0
.end method


# virtual methods
.method public allowParallelSyncs()Z
    .registers 3

    #@0
    .prologue
    .line 116
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->isKey:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 117
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "this method is not allowed to be called when this is a key"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 120
    :cond_d
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->allowParallelSyncs:Z

    #@f
    return v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 189
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 156
    if-ne p1, p0, :cond_5

    #@4
    .line 160
    :cond_4
    :goto_4
    return v1

    #@5
    .line 157
    :cond_5
    instance-of v3, p1, Landroid/content/SyncAdapterType;

    #@7
    if-nez v3, :cond_b

    #@9
    move v1, v2

    #@a
    goto :goto_4

    #@b
    :cond_b
    move-object v0, p1

    #@c
    .line 158
    check-cast v0, Landroid/content/SyncAdapterType;

    #@e
    .line 160
    .local v0, other:Landroid/content/SyncAdapterType;
    iget-object v3, p0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@10
    iget-object v4, v0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_22

    #@18
    iget-object v3, p0, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    #@1a
    iget-object v4, v0, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v3

    #@20
    if-nez v3, :cond_4

    #@22
    :cond_22
    move v1, v2

    #@23
    goto :goto_4
.end method

.method public getSettingsActivity()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 144
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->isKey:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 145
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "this method is not allowed to be called when this is a key"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 148
    :cond_d
    iget-object v0, p0, Landroid/content/SyncAdapterType;->settingsActivity:Ljava/lang/String;

    #@f
    return-object v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 164
    const/16 v0, 0x11

    #@2
    .line 165
    .local v0, result:I
    iget-object v1, p0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@4
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@7
    move-result v1

    #@8
    add-int/lit16 v0, v1, 0x20f

    #@a
    .line 166
    mul-int/lit8 v1, v0, 0x1f

    #@c
    iget-object v2, p0, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    #@e
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    #@11
    move-result v2

    #@12
    add-int v0, v1, v2

    #@14
    .line 168
    return v0
.end method

.method public isAlwaysSyncable()Z
    .registers 3

    #@0
    .prologue
    .line 132
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->isKey:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 133
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "this method is not allowed to be called when this is a key"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 136
    :cond_d
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->isAlwaysSyncable:Z

    #@f
    return v0
.end method

.method public isUserVisible()Z
    .registers 3

    #@0
    .prologue
    .line 103
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->isKey:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 104
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "this method is not allowed to be called when this is a key"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 107
    :cond_d
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->userVisible:Z

    #@f
    return v0
.end method

.method public supportsUploading()Z
    .registers 3

    #@0
    .prologue
    .line 95
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->isKey:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 96
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "this method is not allowed to be called when this is a key"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 99
    :cond_d
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->supportsUploading:Z

    #@f
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 172
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->isKey:Z

    #@2
    if-eqz v0, :cond_2d

    #@4
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v1, "SyncAdapterType Key {name="

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    iget-object v1, p0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, ", type="

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget-object v1, p0, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string/jumbo v1, "}"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    .line 177
    :goto_2c
    return-object v0

    #@2d
    :cond_2d
    new-instance v0, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v1, "SyncAdapterType {name="

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    iget-object v1, p0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v0

    #@3e
    const-string v1, ", type="

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    iget-object v1, p0, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v0

    #@4a
    const-string v1, ", userVisible="

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v0

    #@50
    iget-boolean v1, p0, Landroid/content/SyncAdapterType;->userVisible:Z

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@55
    move-result-object v0

    #@56
    const-string v1, ", supportsUploading="

    #@58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v0

    #@5c
    iget-boolean v1, p0, Landroid/content/SyncAdapterType;->supportsUploading:Z

    #@5e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@61
    move-result-object v0

    #@62
    const-string v1, ", isAlwaysSyncable="

    #@64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v0

    #@68
    iget-boolean v1, p0, Landroid/content/SyncAdapterType;->isAlwaysSyncable:Z

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    const-string v1, ", allowParallelSyncs="

    #@70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v0

    #@74
    iget-boolean v1, p0, Landroid/content/SyncAdapterType;->allowParallelSyncs:Z

    #@76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@79
    move-result-object v0

    #@7a
    const-string v1, ", settingsActivity="

    #@7c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v0

    #@80
    iget-object v1, p0, Landroid/content/SyncAdapterType;->settingsActivity:Ljava/lang/String;

    #@82
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v0

    #@86
    const-string/jumbo v1, "}"

    #@89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v0

    #@8d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v0

    #@91
    goto :goto_2c
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 193
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->isKey:Z

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 194
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string/jumbo v1, "keys aren\'t parcelable"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 197
    :cond_f
    iget-object v0, p0, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 198
    iget-object v0, p0, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 199
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->userVisible:Z

    #@1b
    if-eqz v0, :cond_3e

    #@1d
    move v0, v1

    #@1e
    :goto_1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 200
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->supportsUploading:Z

    #@23
    if-eqz v0, :cond_40

    #@25
    move v0, v1

    #@26
    :goto_26
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 201
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->isAlwaysSyncable:Z

    #@2b
    if-eqz v0, :cond_42

    #@2d
    move v0, v1

    #@2e
    :goto_2e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 202
    iget-boolean v0, p0, Landroid/content/SyncAdapterType;->allowParallelSyncs:Z

    #@33
    if-eqz v0, :cond_44

    #@35
    :goto_35
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 203
    iget-object v0, p0, Landroid/content/SyncAdapterType;->settingsActivity:Ljava/lang/String;

    #@3a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3d
    .line 204
    return-void

    #@3e
    :cond_3e
    move v0, v2

    #@3f
    .line 199
    goto :goto_1e

    #@40
    :cond_40
    move v0, v2

    #@41
    .line 200
    goto :goto_26

    #@42
    :cond_42
    move v0, v2

    #@43
    .line 201
    goto :goto_2e

    #@44
    :cond_44
    move v1, v2

    #@45
    .line 202
    goto :goto_35
.end method
