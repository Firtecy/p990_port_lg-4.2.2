.class public Landroid/content/thm/ThemePackageInfo;
.super Ljava/lang/Object;
.source "ThemePackageInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/thm/ThemePackageInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public applicationInfo:Landroid/content/pm/ApplicationInfo;

.field public bgIcons:[I

.field public componentToResMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public heightAlpha:I

.field public packageName:Ljava/lang/String;

.field public resizeRate:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 51
    new-instance v0, Landroid/content/thm/ThemePackageInfo$1;

    #@2
    invoke-direct {v0}, Landroid/content/thm/ThemePackageInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/thm/ThemePackageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 22
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/content/thm/ThemePackageInfo;->componentToResMap:Ljava/util/HashMap;

    #@a
    .line 31
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 22
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/content/thm/ThemePackageInfo;->componentToResMap:Ljava/util/HashMap;

    #@a
    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Landroid/content/thm/ThemePackageInfo;->packageName:Ljava/lang/String;

    #@10
    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_20

    #@16
    .line 64
    sget-object v0, Landroid/content/pm/ApplicationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@18
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/content/pm/ApplicationInfo;

    #@1e
    iput-object v0, p0, Landroid/content/thm/ThemePackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@20
    .line 67
    :cond_20
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    #@23
    move-result-object v0

    #@24
    iput-object v0, p0, Landroid/content/thm/ThemePackageInfo;->bgIcons:[I

    #@26
    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@29
    move-result v0

    #@2a
    iput v0, p0, Landroid/content/thm/ThemePackageInfo;->heightAlpha:I

    #@2c
    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@2f
    move-result v0

    #@30
    iput v0, p0, Landroid/content/thm/ThemePackageInfo;->resizeRate:F

    #@32
    .line 70
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/thm/ThemePackageInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/content/thm/ThemePackageInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 34
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Landroid/content/thm/ThemePackageInfo;->packageName:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 39
    iget-object v0, p0, Landroid/content/thm/ThemePackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@7
    if-eqz v0, :cond_22

    #@9
    .line 40
    const/4 v0, 0x1

    #@a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 41
    iget-object v0, p0, Landroid/content/thm/ThemePackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@f
    invoke-virtual {v0, p1, p2}, Landroid/content/pm/ApplicationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@12
    .line 46
    :goto_12
    iget-object v0, p0, Landroid/content/thm/ThemePackageInfo;->bgIcons:[I

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    #@17
    .line 47
    iget v0, p0, Landroid/content/thm/ThemePackageInfo;->heightAlpha:I

    #@19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 48
    iget v0, p0, Landroid/content/thm/ThemePackageInfo;->resizeRate:F

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@21
    .line 49
    return-void

    #@22
    .line 43
    :cond_22
    const/4 v0, 0x0

    #@23
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    goto :goto_12
.end method
