.class public abstract Landroid/content/thm/IThemeIconManager$Stub;
.super Landroid/os/Binder;
.source "IThemeIconManager.java"

# interfaces
.implements Landroid/content/thm/IThemeIconManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/thm/IThemeIconManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/thm/IThemeIconManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.content.thm.IThemeIconManager"

.field static final TRANSACTION_getPackageRedirectionMap:I = 0x4

.field static final TRANSACTION_getThemePackageInfo:I = 0x3

.field static final TRANSACTION_removePackageRedirectionMap:I = 0x2

.field static final TRANSACTION_removeThemePackage:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.content.thm.IThemeIconManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/content/thm/IThemeIconManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/content/thm/IThemeIconManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.content.thm.IThemeIconManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/content/thm/IThemeIconManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/content/thm/IThemeIconManager;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/content/thm/IThemeIconManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/content/thm/IThemeIconManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_6e

    #@5
    .line 103
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v3

    #@9
    :goto_9
    return v3

    #@a
    .line 47
    :sswitch_a
    const-string v4, "android.content.thm.IThemeIconManager"

    #@c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v4, "android.content.thm.IThemeIconManager"

    #@12
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 55
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/content/thm/IThemeIconManager$Stub;->removeThemePackage(Ljava/lang/String;)V

    #@1c
    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f
    goto :goto_9

    #@20
    .line 61
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_20
    const-string v4, "android.content.thm.IThemeIconManager"

    #@22
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 64
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/content/thm/IThemeIconManager$Stub;->removePackageRedirectionMap(Ljava/lang/String;)V

    #@2c
    .line 65
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f
    goto :goto_9

    #@30
    .line 70
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_30
    const-string v4, "android.content.thm.IThemeIconManager"

    #@32
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@38
    move-result-object v0

    #@39
    .line 73
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/content/thm/IThemeIconManager$Stub;->getThemePackageInfo(Ljava/lang/String;)Landroid/content/thm/ThemePackageInfo;

    #@3c
    move-result-object v2

    #@3d
    .line 74
    .local v2, _result:Landroid/content/thm/ThemePackageInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@40
    .line 75
    if-eqz v2, :cond_49

    #@42
    .line 76
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@45
    .line 77
    invoke-virtual {v2, p3, v3}, Landroid/content/thm/ThemePackageInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@48
    goto :goto_9

    #@49
    .line 80
    :cond_49
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@4c
    goto :goto_9

    #@4d
    .line 86
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:Landroid/content/thm/ThemePackageInfo;
    :sswitch_4d
    const-string v4, "android.content.thm.IThemeIconManager"

    #@4f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@52
    .line 88
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@55
    move-result-object v0

    #@56
    .line 90
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@59
    move-result-object v1

    #@5a
    .line 91
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/content/thm/IThemeIconManager$Stub;->getPackageRedirectionMap(Ljava/lang/String;Ljava/lang/String;)Landroid/content/thm/ThemeIconRedirectionMap;

    #@5d
    move-result-object v2

    #@5e
    .line 92
    .local v2, _result:Landroid/content/thm/ThemeIconRedirectionMap;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@61
    .line 93
    if-eqz v2, :cond_6a

    #@63
    .line 94
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@66
    .line 95
    invoke-virtual {v2, p3, v3}, Landroid/content/thm/ThemeIconRedirectionMap;->writeToParcel(Landroid/os/Parcel;I)V

    #@69
    goto :goto_9

    #@6a
    .line 98
    :cond_6a
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@6d
    goto :goto_9

    #@6e
    .line 43
    :sswitch_data_6e
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_20
        0x3 -> :sswitch_30
        0x4 -> :sswitch_4d
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
