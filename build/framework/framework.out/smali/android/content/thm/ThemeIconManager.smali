.class public Landroid/content/thm/ThemeIconManager;
.super Ljava/lang/Object;
.source "ThemeIconManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/thm/ThemeIconManager$ThemeResources;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final TAG:Ljava/lang/String; = "ThemeIconManager"

.field private static final VENDOR_LAUNCHERS:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sGlobals:Landroid/content/thm/ThemeIconManager$ThemeResources;

.field private static final sSync:Ljava/lang/Object;


# instance fields
.field private mAppIconSize:I

.field private mIconMap:Landroid/content/thm/ThemeIconRedirectionMap;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 40
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/content/thm/ThemeIconManager;->sSync:Ljava/lang/Object;

    #@7
    .line 42
    new-instance v0, Landroid/content/thm/ThemeIconManager$ThemeResources;

    #@9
    invoke-direct {v0}, Landroid/content/thm/ThemeIconManager$ThemeResources;-><init>()V

    #@c
    sput-object v0, Landroid/content/thm/ThemeIconManager;->sGlobals:Landroid/content/thm/ThemeIconManager$ThemeResources;

    #@e
    .line 44
    new-instance v0, Landroid/content/thm/ThemeIconManager$1;

    #@10
    invoke-direct {v0}, Landroid/content/thm/ThemeIconManager$1;-><init>()V

    #@13
    sput-object v0, Landroid/content/thm/ThemeIconManager;->VENDOR_LAUNCHERS:Ljava/util/HashSet;

    #@15
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 7
    .parameter "r"

    #@0
    .prologue
    .line 200
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 201
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@6
    move-result-object v1

    #@7
    .line 202
    .local v1, config:Landroid/content/res/Configuration;
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@a
    move-result-object v2

    #@b
    .line 203
    .local v2, dm:Landroid/util/DisplayMetrics;
    invoke-virtual {p1}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@e
    move-result-object v0

    #@f
    .line 204
    .local v0, compat:Landroid/content/res/CompatibilityInfo;
    sget-object v4, Landroid/content/thm/ThemeIconManager;->sSync:Ljava/lang/Object;

    #@11
    monitor-enter v4

    #@12
    .line 205
    :try_start_12
    sget-object v3, Landroid/content/thm/ThemeIconManager;->sGlobals:Landroid/content/thm/ThemeIconManager$ThemeResources;

    #@14
    invoke-virtual {v3, v1, v2, v0}, Landroid/content/thm/ThemeIconManager$ThemeResources;->init(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@17
    .line 206
    invoke-direct {p0, p1}, Landroid/content/thm/ThemeIconManager;->initDefaultValuesLocked(Landroid/content/res/Resources;)V

    #@1a
    .line 207
    invoke-direct {p0, p1}, Landroid/content/thm/ThemeIconManager;->generateRedirectionMapLocked(Landroid/content/res/Resources;)V

    #@1d
    .line 208
    monitor-exit v4

    #@1e
    .line 209
    return-void

    #@1f
    .line 208
    :catchall_1f
    move-exception v3

    #@20
    monitor-exit v4
    :try_end_21
    .catchall {:try_start_12 .. :try_end_21} :catchall_1f

    #@21
    throw v3
.end method

.method private generateRedirectionMapLocked(Landroid/content/res/Resources;)V
    .registers 8
    .parameter "r"

    #@0
    .prologue
    .line 221
    const/4 v5, 0x0

    #@1
    iput-object v5, p0, Landroid/content/thm/ThemeIconManager;->mIconMap:Landroid/content/thm/ThemeIconRedirectionMap;

    #@3
    .line 223
    invoke-virtual {p1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@6
    move-result-object v1

    #@7
    .line 224
    .local v1, assets:Landroid/content/res/AssetManager;
    invoke-virtual {v1}, Landroid/content/res/AssetManager;->getBasePackageCount()I

    #@a
    move-result v0

    #@b
    .line 225
    .local v0, N:I
    add-int/lit8 v2, v0, -0x1

    #@d
    .local v2, i:I
    :goto_d
    if-ltz v2, :cond_23

    #@f
    .line 226
    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->getBasePackageName(I)Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    .line 227
    .local v4, packageName:Ljava/lang/String;
    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->getBasePackageId(I)I

    #@16
    move-result v3

    #@17
    .line 230
    .local v3, packageId:I
    const/16 v5, 0x7f

    #@19
    if-ne v3, v5, :cond_24

    #@1b
    .line 235
    sget-object v5, Landroid/content/thm/ThemeIconManager;->sGlobals:Landroid/content/thm/ThemeIconManager$ThemeResources;

    #@1d
    invoke-virtual {v5, v4}, Landroid/content/thm/ThemeIconManager$ThemeResources;->getPackageRedirectionMap(Ljava/lang/String;)Landroid/content/thm/ThemeIconRedirectionMap;

    #@20
    move-result-object v5

    #@21
    iput-object v5, p0, Landroid/content/thm/ThemeIconManager;->mIconMap:Landroid/content/thm/ThemeIconRedirectionMap;

    #@23
    .line 239
    .end local v3           #packageId:I
    .end local v4           #packageName:Ljava/lang/String;
    :cond_23
    return-void

    #@24
    .line 225
    .restart local v3       #packageId:I
    .restart local v4       #packageName:Ljava/lang/String;
    :cond_24
    add-int/lit8 v2, v2, -0x1

    #@26
    goto :goto_d
.end method

.method private initDefaultValuesLocked(Landroid/content/res/Resources;)V
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 214
    const v1, 0x20c0046

    #@3
    :try_start_3
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    #@6
    move-result v1

    #@7
    float-to-int v1, v1

    #@8
    iput v1, p0, Landroid/content/thm/ThemeIconManager;->mAppIconSize:I
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_a} :catch_b

    #@a
    .line 218
    :goto_a
    return-void

    #@b
    .line 215
    :catch_b
    move-exception v0

    #@c
    .line 216
    .local v0, e:Ljava/lang/Exception;
    const/4 v1, 0x0

    #@d
    iput v1, p0, Landroid/content/thm/ThemeIconManager;->mAppIconSize:I

    #@f
    goto :goto_a
.end method

.method public static isUseThemeIcon(Landroid/content/pm/IPackageManager;Ljava/lang/String;Z)Z
    .registers 12
    .parameter "pm"
    .parameter "packageName"
    .parameter "system"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 294
    if-eqz p2, :cond_5

    #@4
    .line 321
    :cond_4
    :goto_4
    return v4

    #@5
    .line 297
    :cond_5
    if-eqz p0, :cond_4

    #@7
    if-eqz p1, :cond_4

    #@9
    .line 298
    sget-object v6, Landroid/content/thm/ThemeIconManager;->VENDOR_LAUNCHERS:Ljava/util/HashSet;

    #@b
    invoke-virtual {v6, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@e
    move-result v6

    #@f
    if-nez v6, :cond_4

    #@11
    .line 300
    new-instance v0, Landroid/content/Intent;

    #@13
    const-string v6, "android.intent.action.MAIN"

    #@15
    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@18
    .line 301
    .local v0, homeIntent:Landroid/content/Intent;
    const-string v6, "android.intent.category.HOME"

    #@1a
    invoke-virtual {v0, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@1d
    .line 302
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@20
    .line 304
    const/4 v6, 0x0

    #@21
    const/16 v7, 0x80

    #@23
    :try_start_23
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@26
    move-result v8

    #@27
    invoke-interface {p0, v0, v6, v7, v8}, Landroid/content/pm/IPackageManager;->queryIntentActivities(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;

    #@2a
    move-result-object v2

    #@2b
    .line 306
    .local v2, results:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_4

    #@2d
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@30
    move-result v6

    #@31
    if-lez v6, :cond_4

    #@33
    .line 307
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@36
    move-result-object v1

    #@37
    .local v1, i$:Ljava/util/Iterator;
    :cond_37
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@3a
    move-result v6

    #@3b
    if-eqz v6, :cond_5b

    #@3d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@40
    move-result-object v3

    #@41
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@43
    .line 308
    .local v3, ri:Landroid/content/pm/ResolveInfo;
    iget-object v6, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@45
    if-eqz v6, :cond_37

    #@47
    iget-object v6, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@49
    iget-object v6, v6, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@4b
    if-eqz v6, :cond_37

    #@4d
    iget-object v6, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4f
    iget-object v6, v6, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@51
    const-string v7, "com.lge.themeicon"

    #@53
    const/4 v8, 0x0

    #@54
    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_57
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_57} :catch_5d

    #@57
    move-result v6

    #@58
    if-eqz v6, :cond_37

    #@5a
    goto :goto_4

    #@5b
    .end local v3           #ri:Landroid/content/pm/ResolveInfo;
    :cond_5b
    move v4, v5

    #@5c
    .line 314
    goto :goto_4

    #@5d
    .line 316
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #results:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :catch_5d
    move-exception v5

    #@5e
    goto :goto_4
.end method

.method public static updateThemeConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V
    .registers 5
    .parameter "config"
    .parameter "dm"
    .parameter "compat"

    #@0
    .prologue
    .line 278
    sget-object v1, Landroid/content/thm/ThemeIconManager;->sSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 279
    :try_start_3
    sget-object v0, Landroid/content/thm/ThemeIconManager;->sGlobals:Landroid/content/thm/ThemeIconManager$ThemeResources;

    #@5
    invoke-virtual {v0, p0, p1, p2}, Landroid/content/thm/ThemeIconManager$ThemeResources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@8
    .line 280
    monitor-exit v1

    #@9
    .line 281
    return-void

    #@a
    .line 280
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method


# virtual methods
.method public doPostProcessing(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;
    .registers 14
    .parameter "src"
    .parameter "id"
    .parameter "density"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 253
    if-nez p1, :cond_5

    #@3
    move-object p1, v0

    #@4
    .line 272
    .end local p1
    :goto_4
    return-object p1

    #@5
    .line 256
    .restart local p1
    :cond_5
    sget-object v9, Landroid/content/thm/ThemeIconManager;->sSync:Ljava/lang/Object;

    #@7
    monitor-enter v9

    #@8
    .line 258
    :try_start_8
    iget-object v2, p0, Landroid/content/thm/ThemeIconManager;->mIconMap:Landroid/content/thm/ThemeIconRedirectionMap;

    #@a
    if-eqz v2, :cond_50

    #@c
    iget-object v0, p0, Landroid/content/thm/ThemeIconManager;->mIconMap:Landroid/content/thm/ThemeIconRedirectionMap;

    #@e
    iget-object v0, v0, Landroid/content/thm/ThemeIconRedirectionMap;->backgroundMap:Landroid/util/SparseArray;

    #@10
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Ljava/lang/Integer;

    #@16
    move-object v6, v0

    #@17
    .line 259
    .local v6, bgIcon:Ljava/lang/Integer;
    :goto_17
    if-eqz v6, :cond_52

    #@19
    .line 260
    sget-object v0, Landroid/content/thm/ThemeIconManager;->sGlobals:Landroid/content/thm/ThemeIconManager$ThemeResources;

    #@1b
    invoke-virtual {v0}, Landroid/content/thm/ThemeIconManager$ThemeResources;->getThemePackageInfo()Landroid/content/thm/ThemePackageInfo;

    #@1e
    move-result-object v8

    #@1f
    .line 261
    .local v8, info:Landroid/content/thm/ThemePackageInfo;
    if-eqz v8, :cond_52

    #@21
    .line 262
    sget-object v0, Landroid/content/thm/ThemeIconManager;->sGlobals:Landroid/content/thm/ThemeIconManager$ThemeResources;

    #@23
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@26
    move-result v2

    #@27
    invoke-virtual {v0, v2, p3}, Landroid/content/thm/ThemeIconManager$ThemeResources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;

    #@2a
    move-result-object v1

    #@2b
    .line 263
    .local v1, drBack:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_52

    #@2d
    iget v0, p0, Landroid/content/thm/ThemeIconManager;->mAppIconSize:I

    #@2f
    if-lez v0, :cond_52

    #@31
    .line 264
    iget v2, p0, Landroid/content/thm/ThemeIconManager;->mAppIconSize:I

    #@33
    iget v3, p0, Landroid/content/thm/ThemeIconManager;->mAppIconSize:I

    #@35
    iget v4, v8, Landroid/content/thm/ThemePackageInfo;->resizeRate:F

    #@37
    iget v5, v8, Landroid/content/thm/ThemePackageInfo;->heightAlpha:I

    #@39
    move-object v0, p1

    #@3a
    invoke-static/range {v0 .. v5}, Landroid/content/thm/ThemeIconManagerHelper;->drawBackground(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;IIFI)Landroid/graphics/Bitmap;

    #@3d
    move-result-object v7

    #@3e
    .line 266
    .local v7, bm:Landroid/graphics/Bitmap;
    if-eqz v7, :cond_52

    #@40
    .line 267
    new-instance p1, Landroid/graphics/drawable/BitmapDrawable;

    #@42
    .end local p1
    sget-object v0, Landroid/content/thm/ThemeIconManager;->sGlobals:Landroid/content/thm/ThemeIconManager$ThemeResources;

    #@44
    invoke-virtual {v0}, Landroid/content/thm/ThemeIconManager$ThemeResources;->getResources()Landroid/content/res/Resources;

    #@47
    move-result-object v0

    #@48
    invoke-direct {p1, v0, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    #@4b
    monitor-exit v9

    #@4c
    goto :goto_4

    #@4d
    .line 273
    .end local v1           #drBack:Landroid/graphics/drawable/Drawable;
    .end local v6           #bgIcon:Ljava/lang/Integer;
    .end local v7           #bm:Landroid/graphics/Bitmap;
    .end local v8           #info:Landroid/content/thm/ThemePackageInfo;
    :catchall_4d
    move-exception v0

    #@4e
    monitor-exit v9
    :try_end_4f
    .catchall {:try_start_8 .. :try_end_4f} :catchall_4d

    #@4f
    throw v0

    #@50
    .restart local p1
    :cond_50
    move-object v6, v0

    #@51
    .line 258
    goto :goto_17

    #@52
    .line 272
    .restart local v6       #bgIcon:Ljava/lang/Integer;
    :cond_52
    :try_start_52
    monitor-exit v9
    :try_end_53
    .catchall {:try_start_52 .. :try_end_53} :catchall_4d

    #@53
    goto :goto_4
.end method

.method public getThemeIcon(II)Landroid/graphics/drawable/Drawable;
    .registers 7
    .parameter "id"
    .parameter "density"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 242
    sget-object v3, Landroid/content/thm/ThemeIconManager;->sSync:Ljava/lang/Object;

    #@3
    monitor-enter v3

    #@4
    .line 244
    :try_start_4
    iget-object v1, p0, Landroid/content/thm/ThemeIconManager;->mIconMap:Landroid/content/thm/ThemeIconRedirectionMap;

    #@6
    if-eqz v1, :cond_21

    #@8
    iget-object v1, p0, Landroid/content/thm/ThemeIconManager;->mIconMap:Landroid/content/thm/ThemeIconRedirectionMap;

    #@a
    iget-object v1, v1, Landroid/content/thm/ThemeIconRedirectionMap;->redirectionMap:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Ljava/lang/Integer;

    #@12
    move-object v0, v1

    #@13
    .line 245
    .local v0, themeIcon:Ljava/lang/Integer;
    :goto_13
    if-eqz v0, :cond_23

    #@15
    .line 246
    sget-object v1, Landroid/content/thm/ThemeIconManager;->sGlobals:Landroid/content/thm/ThemeIconManager$ThemeResources;

    #@17
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@1a
    move-result v2

    #@1b
    invoke-virtual {v1, v2, p2}, Landroid/content/thm/ThemeIconManager$ThemeResources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;

    #@1e
    move-result-object v1

    #@1f
    monitor-exit v3

    #@20
    .line 248
    :goto_20
    return-object v1

    #@21
    .end local v0           #themeIcon:Ljava/lang/Integer;
    :cond_21
    move-object v0, v2

    #@22
    .line 244
    goto :goto_13

    #@23
    .line 248
    .restart local v0       #themeIcon:Ljava/lang/Integer;
    :cond_23
    monitor-exit v3

    #@24
    move-object v1, v2

    #@25
    goto :goto_20

    #@26
    .line 249
    .end local v0           #themeIcon:Ljava/lang/Integer;
    :catchall_26
    move-exception v1

    #@27
    monitor-exit v3
    :try_end_28
    .catchall {:try_start_4 .. :try_end_28} :catchall_26

    #@28
    throw v1
.end method

.method public updateConfiguration(Landroid/content/res/Resources;ZLandroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V
    .registers 8
    .parameter "res"
    .parameter "themeChanged"
    .parameter "config"
    .parameter "dm"
    .parameter "compat"

    #@0
    .prologue
    .line 285
    invoke-direct {p0, p1}, Landroid/content/thm/ThemeIconManager;->initDefaultValuesLocked(Landroid/content/res/Resources;)V

    #@3
    .line 286
    sget-object v1, Landroid/content/thm/ThemeIconManager;->sSync:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 287
    if-eqz p2, :cond_b

    #@8
    .line 288
    :try_start_8
    invoke-direct {p0, p1}, Landroid/content/thm/ThemeIconManager;->generateRedirectionMapLocked(Landroid/content/res/Resources;)V

    #@b
    .line 290
    :cond_b
    monitor-exit v1

    #@c
    .line 291
    return-void

    #@d
    .line 290
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method
