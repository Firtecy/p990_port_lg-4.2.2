.class public Landroid/content/thm/ThemeIconManagerHelper;
.super Ljava/lang/Object;
.source "ThemeIconManagerHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ThemeIconManager"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 21
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static changeDrawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "source"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 50
    if-nez p0, :cond_6

    #@4
    move-object v0, v4

    #@5
    .line 67
    .local v2, height:I
    .local v3, width:I
    :goto_5
    return-object v0

    #@6
    .line 52
    .end local v2           #height:I
    .end local v3           #width:I
    :cond_6
    instance-of v5, p0, Landroid/graphics/drawable/BitmapDrawable;

    #@8
    if-eqz v5, :cond_11

    #@a
    .line 53
    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    #@c
    .end local p0
    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    #@f
    move-result-object v0

    #@10
    goto :goto_5

    #@11
    .line 55
    .restart local p0
    :cond_11
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@14
    move-result v3

    #@15
    .line 56
    .restart local v3       #width:I
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@18
    move-result v2

    #@19
    .line 57
    .restart local v2       #height:I
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@1b
    invoke-static {v3, v2, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@1e
    move-result-object v0

    #@1f
    .line 59
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_23

    #@21
    move-object v0, v4

    #@22
    .line 60
    goto :goto_5

    #@23
    .line 62
    :cond_23
    new-instance v1, Landroid/graphics/Canvas;

    #@25
    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@28
    .line 63
    .local v1, canvas:Landroid/graphics/Canvas;
    invoke-virtual {p0, v6, v6, v3, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@2b
    .line 64
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@2e
    .line 65
    invoke-virtual {v1, v4}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@31
    goto :goto_5
.end method

.method public static drawBackground(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;IIFI)Landroid/graphics/Bitmap;
    .registers 21
    .parameter "icon"
    .parameter "icon_bg"
    .parameter "width"
    .parameter "height"
    .parameter "resizeRate"
    .parameter "heightAlpha"

    #@0
    .prologue
    .line 72
    invoke-static {p0}, Landroid/content/thm/ThemeIconManagerHelper;->changeDrawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    #@3
    move-result-object v5

    #@4
    .line 73
    .local v5, iconBitmap:Landroid/graphics/Bitmap;
    if-nez v5, :cond_8

    #@6
    .line 74
    const/4 v3, 0x0

    #@7
    .line 100
    :goto_7
    return-object v3

    #@8
    .line 77
    :cond_8
    invoke-static/range {p1 .. p1}, Landroid/content/thm/ThemeIconManagerHelper;->changeDrawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    #@b
    move-result-object v4

    #@c
    .line 78
    .local v4, iconBgBitmap:Landroid/graphics/Bitmap;
    if-nez v4, :cond_10

    #@e
    .line 79
    const/4 v3, 0x0

    #@f
    goto :goto_7

    #@10
    .line 82
    :cond_10
    sget-object v13, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@12
    move/from16 v0, p2

    #@14
    move/from16 v1, p3

    #@16
    invoke-static {v0, v1, v13}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@19
    move-result-object v3

    #@1a
    .line 84
    .local v3, canvasBitmap:Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Rect;

    #@1c
    const/4 v13, 0x0

    #@1d
    const/4 v14, 0x0

    #@1e
    move/from16 v0, p2

    #@20
    move/from16 v1, p3

    #@22
    invoke-direct {v6, v13, v14, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    #@25
    .line 86
    .local v6, iconBounds:Landroid/graphics/Rect;
    new-instance v7, Landroid/graphics/Paint;

    #@27
    const/4 v13, 0x3

    #@28
    invoke-direct {v7, v13}, Landroid/graphics/Paint;-><init>(I)V

    #@2b
    .line 88
    .local v7, p:Landroid/graphics/Paint;
    new-instance v2, Landroid/graphics/Canvas;

    #@2d
    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@30
    .line 89
    .local v2, canvas:Landroid/graphics/Canvas;
    const/4 v13, 0x0

    #@31
    invoke-virtual {v2, v4, v13, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@34
    .line 91
    move/from16 v0, p2

    #@36
    int-to-float v13, v0

    #@37
    mul-float v9, v13, p4

    #@39
    .line 92
    .local v9, paddedWidth:F
    move/from16 v0, p3

    #@3b
    int-to-float v13, v0

    #@3c
    mul-float v8, v13, p4

    #@3e
    .line 93
    .local v8, paddedHeight:F
    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterX()F

    #@41
    move-result v13

    #@42
    const/high16 v14, 0x4000

    #@44
    div-float v14, v9, v14

    #@46
    sub-float v11, v13, v14

    #@48
    .line 94
    .local v11, x:F
    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterY()F

    #@4b
    move-result v13

    #@4c
    const/high16 v14, 0x4000

    #@4e
    div-float v14, v8, v14

    #@50
    sub-float/2addr v13, v14

    #@51
    move/from16 v0, p5

    #@53
    int-to-float v14, v0

    #@54
    add-float v12, v13, v14

    #@56
    .line 96
    .local v12, y:F
    new-instance v10, Landroid/graphics/RectF;

    #@58
    add-float v13, v11, v9

    #@5a
    add-float v14, v12, v8

    #@5c
    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    #@5f
    .line 97
    .local v10, r:Landroid/graphics/RectF;
    const/4 v13, 0x0

    #@60
    invoke-virtual {v2, v5, v13, v10, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@63
    .line 98
    const/4 v13, 0x0

    #@64
    invoke-virtual {v2, v13}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@67
    goto :goto_7
.end method

.method public static getIcon(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;
    .registers 5
    .parameter "res"
    .parameter "resId"
    .parameter "density"

    #@0
    .prologue
    .line 27
    if-nez p2, :cond_7

    #@2
    .line 28
    :try_start_2
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v1

    #@6
    .line 35
    .local v1, result:Landroid/graphics/drawable/Drawable;
    :goto_6
    return-object v1

    #@7
    .line 30
    .end local v1           #result:Landroid/graphics/drawable/Drawable;
    :cond_7
    invoke-virtual {p0, p1, p2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_a
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_a} :catch_c

    #@a
    move-result-object v1

    #@b
    .restart local v1       #result:Landroid/graphics/drawable/Drawable;
    goto :goto_6

    #@c
    .line 32
    .end local v1           #result:Landroid/graphics/drawable/Drawable;
    :catch_c
    move-exception v0

    #@d
    .line 33
    .local v0, e:Landroid/content/res/Resources$NotFoundException;
    const/4 v1, 0x0

    #@e
    .restart local v1       #result:Landroid/graphics/drawable/Drawable;
    goto :goto_6
.end method

.method public static getResourcesForApplication(Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    .registers 4
    .parameter "pm"
    .parameter "appInfo"

    #@0
    .prologue
    .line 42
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result-object v1

    #@4
    .line 46
    .local v1, result:Landroid/content/res/Resources;
    :goto_4
    return-object v1

    #@5
    .line 43
    .end local v1           #result:Landroid/content/res/Resources;
    :catch_5
    move-exception v0

    #@6
    .line 44
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    #@7
    .restart local v1       #result:Landroid/content/res/Resources;
    goto :goto_4
.end method
