.class Landroid/content/thm/ThemeIconManager$ThemeResources;
.super Ljava/lang/Object;
.source "ThemeIconManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/thm/ThemeIconManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ThemeResources"
.end annotation


# instance fields
.field private mCompatibility:Landroid/content/res/CompatibilityInfo;

.field private mConfiguration:Landroid/content/res/Configuration;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mService:Landroid/content/thm/IThemeIconManager;

.field private mThemePackageInfo:Landroid/content/thm/ThemePackageInfo;

.field private mThemeRes:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 66
    return-void
.end method

.method private getThemeIconManager()Landroid/content/thm/IThemeIconManager;
    .registers 2

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mService:Landroid/content/thm/IThemeIconManager;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 70
    iget-object v0, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mService:Landroid/content/thm/IThemeIconManager;

    #@6
    .line 74
    :goto_6
    return-object v0

    #@7
    .line 72
    :cond_7
    const-string/jumbo v0, "themeicon"

    #@a
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v0

    #@e
    invoke-static {v0}, Landroid/content/thm/IThemeIconManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/thm/IThemeIconManager;

    #@11
    move-result-object v0

    #@12
    iput-object v0, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mService:Landroid/content/thm/IThemeIconManager;

    #@14
    .line 74
    iget-object v0, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mService:Landroid/content/thm/IThemeIconManager;

    #@16
    goto :goto_6
.end method

.method private isEmpty(Ljava/lang/String;)Z
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 147
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 148
    :cond_8
    const/4 v0, 0x1

    #@9
    .line 150
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private peekResources()Landroid/content/res/Resources;
    .registers 2

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemeRes:Ljava/lang/ref/WeakReference;

    #@2
    if-eqz v0, :cond_d

    #@4
    iget-object v0, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemeRes:Ljava/lang/ref/WeakReference;

    #@6
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/content/res/Resources;

    #@c
    :goto_c
    return-object v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method


# virtual methods
.method getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    .registers 5
    .parameter "id"
    .parameter "density"

    #@0
    .prologue
    .line 187
    :try_start_0
    invoke-virtual {p0}, Landroid/content/thm/ThemeIconManager$ThemeResources;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    .line 188
    .local v0, r:Landroid/content/res/Resources;
    if-eqz v0, :cond_13

    #@6
    .line 189
    if-nez p2, :cond_d

    #@8
    .line 190
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@b
    move-result-object v1

    #@c
    .line 196
    .end local v0           #r:Landroid/content/res/Resources;
    :goto_c
    return-object v1

    #@d
    .line 192
    .restart local v0       #r:Landroid/content/res/Resources;
    :cond_d
    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_10
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_10} :catch_12

    #@10
    move-result-object v1

    #@11
    goto :goto_c

    #@12
    .line 194
    .end local v0           #r:Landroid/content/res/Resources;
    :catch_12
    move-exception v1

    #@13
    .line 196
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_c
.end method

.method getPackageRedirectionMap(Ljava/lang/String;)Landroid/content/thm/ThemeIconRedirectionMap;
    .registers 5
    .parameter "targetPackageName"

    #@0
    .prologue
    .line 170
    iget-object v2, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@2
    if-eqz v2, :cond_1a

    #@4
    .line 171
    iget-object v2, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@6
    iget-object v0, v2, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@8
    .line 172
    .local v0, themePackage:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/content/thm/ThemeIconManager$ThemeResources;->isEmpty(Ljava/lang/String;)Z

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_1a

    #@e
    .line 174
    :try_start_e
    invoke-direct {p0}, Landroid/content/thm/ThemeIconManager$ThemeResources;->getThemeIconManager()Landroid/content/thm/IThemeIconManager;

    #@11
    move-result-object v1

    #@12
    .line 175
    .local v1, tm:Landroid/content/thm/IThemeIconManager;
    if-eqz v1, :cond_1a

    #@14
    .line 176
    invoke-interface {v1, v0, p1}, Landroid/content/thm/IThemeIconManager;->getPackageRedirectionMap(Ljava/lang/String;Ljava/lang/String;)Landroid/content/thm/ThemeIconRedirectionMap;
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_17} :catch_19

    #@17
    move-result-object v2

    #@18
    .line 182
    .end local v0           #themePackage:Ljava/lang/String;
    .end local v1           #tm:Landroid/content/thm/IThemeIconManager;
    :goto_18
    return-object v2

    #@19
    .line 178
    .restart local v0       #themePackage:Ljava/lang/String;
    :catch_19
    move-exception v2

    #@1a
    .line 182
    .end local v0           #themePackage:Ljava/lang/String;
    :cond_1a
    const/4 v2, 0x0

    #@1b
    goto :goto_18
.end method

.method getResources()Landroid/content/res/Resources;
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 83
    iget-object v4, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemeRes:Ljava/lang/ref/WeakReference;

    #@3
    if-eqz v4, :cond_1b

    #@5
    iget-object v4, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemeRes:Ljava/lang/ref/WeakReference;

    #@7
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@a
    move-result-object v4

    #@b
    check-cast v4, Landroid/content/res/Resources;

    #@d
    move-object v1, v4

    #@e
    .line 84
    .local v1, existing:Landroid/content/res/Resources;
    :goto_e
    if-eqz v1, :cond_24

    #@10
    .line 85
    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4}, Landroid/content/res/AssetManager;->isUpToDate()Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_1d

    #@1a
    .line 103
    .end local v1           #existing:Landroid/content/res/Resources;
    :goto_1a
    return-object v1

    #@1b
    :cond_1b
    move-object v1, v3

    #@1c
    .line 83
    goto :goto_e

    #@1d
    .line 88
    .restart local v1       #existing:Landroid/content/res/Resources;
    :cond_1d
    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4}, Landroid/content/res/AssetManager;->close()V

    #@24
    .line 91
    :cond_24
    invoke-virtual {p0}, Landroid/content/thm/ThemeIconManager$ThemeResources;->getThemePackageInfo()Landroid/content/thm/ThemePackageInfo;

    #@27
    move-result-object v2

    #@28
    .line 92
    .local v2, info:Landroid/content/thm/ThemePackageInfo;
    if-nez v2, :cond_2c

    #@2a
    move-object v1, v3

    #@2b
    .line 93
    goto :goto_1a

    #@2c
    .line 96
    :cond_2c
    new-instance v0, Landroid/content/res/AssetManager;

    #@2e
    invoke-direct {v0}, Landroid/content/res/AssetManager;-><init>()V

    #@31
    .line 97
    .local v0, assets:Landroid/content/res/AssetManager;
    iget-object v4, v2, Landroid/content/thm/ThemePackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@33
    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@35
    invoke-virtual {v0, v4}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    #@38
    move-result v4

    #@39
    if-nez v4, :cond_3d

    #@3b
    move-object v1, v3

    #@3c
    .line 98
    goto :goto_1a

    #@3d
    .line 101
    :cond_3d
    new-instance v3, Landroid/content/res/Resources;

    #@3f
    iget-object v4, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@41
    iget-object v5, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@43
    iget-object v6, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mCompatibility:Landroid/content/res/CompatibilityInfo;

    #@45
    invoke-direct {v3, v0, v4, v5, v6}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V

    #@48
    .line 102
    .local v3, r:Landroid/content/res/Resources;
    new-instance v4, Ljava/lang/ref/WeakReference;

    #@4a
    invoke-direct {v4, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@4d
    iput-object v4, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemeRes:Ljava/lang/ref/WeakReference;

    #@4f
    move-object v1, v3

    #@50
    .line 103
    goto :goto_1a
.end method

.method getThemePackageInfo()Landroid/content/thm/ThemePackageInfo;
    .registers 3

    #@0
    .prologue
    .line 154
    iget-object v1, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemePackageInfo:Landroid/content/thm/ThemePackageInfo;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 155
    iget-object v1, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemePackageInfo:Landroid/content/thm/ThemePackageInfo;

    #@6
    .line 166
    :goto_6
    return-object v1

    #@7
    .line 157
    :cond_7
    iget-object v1, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@9
    if-eqz v1, :cond_25

    #@b
    iget-object v1, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@d
    iget-object v1, v1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@f
    invoke-direct {p0, v1}, Landroid/content/thm/ThemeIconManager$ThemeResources;->isEmpty(Ljava/lang/String;)Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_25

    #@15
    .line 159
    :try_start_15
    invoke-direct {p0}, Landroid/content/thm/ThemeIconManager$ThemeResources;->getThemeIconManager()Landroid/content/thm/IThemeIconManager;

    #@18
    move-result-object v0

    #@19
    .line 160
    .local v0, tm:Landroid/content/thm/IThemeIconManager;
    if-eqz v0, :cond_25

    #@1b
    .line 161
    iget-object v1, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@1d
    iget-object v1, v1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@1f
    invoke-interface {v0, v1}, Landroid/content/thm/IThemeIconManager;->getThemePackageInfo(Ljava/lang/String;)Landroid/content/thm/ThemePackageInfo;

    #@22
    move-result-object v1

    #@23
    iput-object v1, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemePackageInfo:Landroid/content/thm/ThemePackageInfo;
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_25} :catch_28

    #@25
    .line 166
    .end local v0           #tm:Landroid/content/thm/IThemeIconManager;
    :cond_25
    :goto_25
    iget-object v1, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemePackageInfo:Landroid/content/thm/ThemePackageInfo;

    #@27
    goto :goto_6

    #@28
    .line 163
    :catch_28
    move-exception v1

    #@29
    goto :goto_25
.end method

.method init(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V
    .registers 5
    .parameter "config"
    .parameter "dm"
    .parameter "compat"

    #@0
    .prologue
    .line 107
    iget-object v0, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@2
    if-nez v0, :cond_14

    #@4
    .line 108
    new-instance v0, Landroid/content/res/Configuration;

    #@6
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@b
    .line 109
    iget-object v0, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@d
    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    #@10
    .line 110
    iput-object p2, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@12
    .line 111
    iput-object p3, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mCompatibility:Landroid/content/res/CompatibilityInfo;

    #@14
    .line 113
    :cond_14
    return-void
.end method

.method updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V
    .registers 10
    .parameter "config"
    .parameter "dm"
    .parameter "compat"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 116
    iget-object v4, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@3
    if-nez v4, :cond_6

    #@5
    .line 144
    :cond_5
    :goto_5
    return-void

    #@6
    .line 120
    :cond_6
    iget-object v4, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mConfiguration:Landroid/content/res/Configuration;

    #@8
    invoke-virtual {v4, p1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    #@b
    move-result v1

    #@c
    .line 121
    .local v1, changes:I
    iput-object p2, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@e
    .line 122
    iput-object p3, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mCompatibility:Landroid/content/res/CompatibilityInfo;

    #@10
    .line 124
    const/high16 v4, 0x1000

    #@12
    and-int/2addr v4, v1

    #@13
    if-eqz v4, :cond_34

    #@15
    .line 125
    iget-object v4, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemeRes:Ljava/lang/ref/WeakReference;

    #@17
    if-eqz v4, :cond_32

    #@19
    iget-object v4, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemeRes:Ljava/lang/ref/WeakReference;

    #@1b
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1e
    move-result-object v4

    #@1f
    check-cast v4, Landroid/content/res/Resources;

    #@21
    move-object v2, v4

    #@22
    .line 126
    .local v2, old:Landroid/content/res/Resources;
    :goto_22
    if-eqz v2, :cond_2d

    #@24
    .line 127
    invoke-virtual {v2}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@27
    move-result-object v0

    #@28
    .line 128
    .local v0, assets:Landroid/content/res/AssetManager;
    if-eqz v0, :cond_2d

    #@2a
    .line 129
    invoke-virtual {v0}, Landroid/content/res/AssetManager;->close()V

    #@2d
    .line 132
    .end local v0           #assets:Landroid/content/res/AssetManager;
    :cond_2d
    iput-object v5, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemePackageInfo:Landroid/content/thm/ThemePackageInfo;

    #@2f
    .line 133
    iput-object v5, p0, Landroid/content/thm/ThemeIconManager$ThemeResources;->mThemeRes:Ljava/lang/ref/WeakReference;

    #@31
    goto :goto_5

    #@32
    .end local v2           #old:Landroid/content/res/Resources;
    :cond_32
    move-object v2, v5

    #@33
    .line 125
    goto :goto_22

    #@34
    .line 139
    :cond_34
    invoke-direct {p0}, Landroid/content/thm/ThemeIconManager$ThemeResources;->peekResources()Landroid/content/res/Resources;

    #@37
    move-result-object v3

    #@38
    .line 140
    .local v3, r:Landroid/content/res/Resources;
    if-eqz v3, :cond_5

    #@3a
    .line 141
    invoke-virtual {v3, p1, p2, p3}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@3d
    goto :goto_5
.end method
