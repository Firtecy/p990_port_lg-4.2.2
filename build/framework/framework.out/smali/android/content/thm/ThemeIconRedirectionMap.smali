.class public Landroid/content/thm/ThemeIconRedirectionMap;
.super Ljava/lang/Object;
.source "ThemeIconRedirectionMap.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/thm/ThemeIconRedirectionMap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public backgroundMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public redirectionMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public targetPackageName:Ljava/lang/String;

.field public themePackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 57
    new-instance v0, Landroid/content/thm/ThemeIconRedirectionMap$1;

    #@2
    invoke-direct {v0}, Landroid/content/thm/ThemeIconRedirectionMap$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/thm/ThemeIconRedirectionMap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 16
    new-instance v0, Landroid/util/SparseArray;

    #@5
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@8
    iput-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->redirectionMap:Landroid/util/SparseArray;

    #@a
    .line 17
    new-instance v0, Landroid/util/SparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@f
    iput-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->backgroundMap:Landroid/util/SparseArray;

    #@11
    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->themePackageName:Ljava/lang/String;

    #@17
    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->targetPackageName:Ljava/lang/String;

    #@1d
    .line 70
    iget-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->redirectionMap:Landroid/util/SparseArray;

    #@1f
    invoke-direct {p0, p1, v0}, Landroid/content/thm/ThemeIconRedirectionMap;->readMap(Landroid/os/Parcel;Landroid/util/SparseArray;)V

    #@22
    .line 71
    iget-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->backgroundMap:Landroid/util/SparseArray;

    #@24
    invoke-direct {p0, p1, v0}, Landroid/content/thm/ThemeIconRedirectionMap;->readMap(Landroid/os/Parcel;Landroid/util/SparseArray;)V

    #@27
    .line 72
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/thm/ThemeIconRedirectionMap$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 13
    invoke-direct {p0, p1}, Landroid/content/thm/ThemeIconRedirectionMap;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "themePackageName"
    .parameter "targetPackageName"

    #@0
    .prologue
    .line 19
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 16
    new-instance v0, Landroid/util/SparseArray;

    #@5
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@8
    iput-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->redirectionMap:Landroid/util/SparseArray;

    #@a
    .line 17
    new-instance v0, Landroid/util/SparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@f
    iput-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->backgroundMap:Landroid/util/SparseArray;

    #@11
    .line 20
    iput-object p1, p0, Landroid/content/thm/ThemeIconRedirectionMap;->themePackageName:Ljava/lang/String;

    #@13
    .line 21
    iput-object p2, p0, Landroid/content/thm/ThemeIconRedirectionMap;->targetPackageName:Ljava/lang/String;

    #@15
    .line 22
    return-void
.end method

.method private readMap(Landroid/os/Parcel;Landroid/util/SparseArray;)V
    .registers 8
    .parameter "source"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 36
    .local p2, map:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 37
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_1a

    #@7
    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v2

    #@b
    .line 39
    .local v2, key:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@e
    move-result v3

    #@f
    .line 40
    .local v3, value:I
    new-instance v4, Ljava/lang/Integer;

    #@11
    invoke-direct {v4, v3}, Ljava/lang/Integer;-><init>(I)V

    #@14
    invoke-virtual {p2, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@17
    .line 37
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_5

    #@1a
    .line 42
    .end local v2           #key:I
    .end local v3           #value:I
    :cond_1a
    return-void
.end method

.method private writeMap(Landroid/os/Parcel;Landroid/util/SparseArray;)V
    .registers 8
    .parameter "dest"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 25
    .local p2, map:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/lang/Integer;>;"
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    #@3
    move-result v0

    #@4
    .line 26
    .local v0, N:I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 27
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_21

    #@a
    .line 28
    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@d
    move-result v2

    #@e
    .line 29
    .local v2, key:I
    invoke-virtual {p2, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v3

    #@12
    check-cast v3, Ljava/lang/Integer;

    #@14
    .line 30
    .local v3, value:Ljava/lang/Integer;
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 31
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@1a
    move-result v4

    #@1b
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 27
    add-int/lit8 v1, v1, 0x1

    #@20
    goto :goto_8

    #@21
    .line 33
    .end local v2           #key:I
    .end local v3           #value:Ljava/lang/Integer;
    :cond_21
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 46
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->themePackageName:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 52
    iget-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->targetPackageName:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 53
    iget-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->redirectionMap:Landroid/util/SparseArray;

    #@c
    invoke-direct {p0, p1, v0}, Landroid/content/thm/ThemeIconRedirectionMap;->writeMap(Landroid/os/Parcel;Landroid/util/SparseArray;)V

    #@f
    .line 54
    iget-object v0, p0, Landroid/content/thm/ThemeIconRedirectionMap;->backgroundMap:Landroid/util/SparseArray;

    #@11
    invoke-direct {p0, p1, v0}, Landroid/content/thm/ThemeIconRedirectionMap;->writeMap(Landroid/os/Parcel;Landroid/util/SparseArray;)V

    #@14
    .line 55
    return-void
.end method
