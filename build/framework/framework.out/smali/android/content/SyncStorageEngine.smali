.class public Landroid/content/SyncStorageEngine;
.super Landroid/os/Handler;
.source "SyncStorageEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/SyncStorageEngine$OnSyncRequestListener;,
        Landroid/content/SyncStorageEngine$DayStats;,
        Landroid/content/SyncStorageEngine$SyncHistoryItem;,
        Landroid/content/SyncStorageEngine$AuthorityInfo;,
        Landroid/content/SyncStorageEngine$AccountInfo;,
        Landroid/content/SyncStorageEngine$PendingOperation;
    }
.end annotation


# static fields
.field private static final ACCOUNTS_VERSION:I = 0x2

.field private static final DEBUG:Z = false

.field private static final DEBUG_FILE:Z = false

.field private static final DEFAULT_POLL_FREQUENCY_SECONDS:J = 0x15180L

.field public static final EVENTS:[Ljava/lang/String; = null

.field public static final EVENT_START:I = 0x0

.field public static final EVENT_STOP:I = 0x1

.field public static final MAX_HISTORY:I = 0x64

.field public static final MESG_CANCELED:Ljava/lang/String; = "canceled"

.field public static final MESG_SUCCESS:Ljava/lang/String; = "success"

.field static final MILLIS_IN_4WEEKS:J = 0x90321000L

.field private static final MSG_WRITE_STATISTICS:I = 0x2

.field private static final MSG_WRITE_STATUS:I = 0x1

.field public static final NOT_IN_BACKOFF_MODE:J = -0x1L

.field private static final PENDING_FINISH_TO_WRITE:I = 0x4

.field public static final PENDING_OPERATION_VERSION:I = 0x2

.field public static final SOURCES:[Ljava/lang/String; = null

.field public static final SOURCE_LOCAL:I = 0x1

.field public static final SOURCE_PERIODIC:I = 0x4

.field public static final SOURCE_POLL:I = 0x2

.field public static final SOURCE_SERVER:I = 0x0

.field public static final SOURCE_USER:I = 0x3

.field public static final STATISTICS_FILE_END:I = 0x0

.field public static final STATISTICS_FILE_ITEM:I = 0x65

.field public static final STATISTICS_FILE_ITEM_OLD:I = 0x64

.field public static final STATUS_FILE_END:I = 0x0

.field public static final STATUS_FILE_ITEM:I = 0x64

.field public static final SYNC_CONNECTION_SETTING_CHANGED_INTENT:Landroid/content/Intent; = null

.field private static final SYNC_ENABLED_DEFAULT:Z = false

.field private static final TAG:Ljava/lang/String; = "SyncManager"

.field private static final WRITE_STATISTICS_DELAY:J = 0x1b7740L

.field private static final WRITE_STATUS_DELAY:J = 0x927c0L

.field private static final XML_ATTR_ENABLED:Ljava/lang/String; = "enabled"

.field private static final XML_ATTR_LISTEN_FOR_TICKLES:Ljava/lang/String; = "listen-for-tickles"

.field private static final XML_ATTR_NEXT_AUTHORITY_ID:Ljava/lang/String; = "nextAuthorityId"

.field private static final XML_ATTR_SYNC_RANDOM_OFFSET:Ljava/lang/String; = "offsetInSeconds"

.field private static final XML_ATTR_USER:Ljava/lang/String; = "user"

.field private static final XML_TAG_LISTEN_FOR_TICKLES:Ljava/lang/String; = "listenForTickles"

.field private static sAuthorityRenames:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sSyncStorageEngine:Landroid/content/SyncStorageEngine;


# instance fields
.field private final mAccountInfoFile:Landroid/util/AtomicFile;

.field private final mAccounts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/accounts/AccountAndUser;",
            "Landroid/content/SyncStorageEngine$AccountInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mAuthorities:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/content/SyncStorageEngine$AuthorityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mCal:Ljava/util/Calendar;

.field private final mChangeListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/content/ISyncStatusObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mCurrentSyncs:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/SyncInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

.field private mDefaultMasterSyncAutomatically:Z

.field private mMasterSyncAutomatically:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mNextAuthorityId:I

.field private mNextHistoryId:I

.field private mNumPendingFinished:I

.field private final mPendingFile:Landroid/util/AtomicFile;

.field private final mPendingOperations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/SyncStorageEngine$PendingOperation;",
            ">;"
        }
    .end annotation
.end field

.field private final mStatisticsFile:Landroid/util/AtomicFile;

.field private final mStatusFile:Landroid/util/AtomicFile;

.field private final mSyncHistory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/SyncStorageEngine$SyncHistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSyncRandomOffset:I

.field private mSyncRequestListener:Landroid/content/SyncStorageEngine$OnSyncRequestListener;

.field private final mSyncStatus:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/content/SyncStatusInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mYear:I

.field private mYearInDays:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 90
    new-array v0, v4, [Ljava/lang/String;

    #@5
    const-string v1, "START"

    #@7
    aput-object v1, v0, v2

    #@9
    const-string v1, "STOP"

    #@b
    aput-object v1, v0, v3

    #@d
    sput-object v0, Landroid/content/SyncStorageEngine;->EVENTS:[Ljava/lang/String;

    #@f
    .line 111
    new-instance v0, Landroid/content/Intent;

    #@11
    const-string v1, "com.android.sync.SYNC_CONN_STATUS_CHANGED"

    #@13
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@16
    sput-object v0, Landroid/content/SyncStorageEngine;->SYNC_CONNECTION_SETTING_CHANGED_INTENT:Landroid/content/Intent;

    #@18
    .line 116
    const/4 v0, 0x5

    #@19
    new-array v0, v0, [Ljava/lang/String;

    #@1b
    const-string v1, "SERVER"

    #@1d
    aput-object v1, v0, v2

    #@1f
    const-string v1, "LOCAL"

    #@21
    aput-object v1, v0, v3

    #@23
    const-string v1, "POLL"

    #@25
    aput-object v1, v0, v4

    #@27
    const/4 v1, 0x3

    #@28
    const-string v2, "USER"

    #@2a
    aput-object v2, v0, v1

    #@2c
    const/4 v1, 0x4

    #@2d
    const-string v2, "PERIODIC"

    #@2f
    aput-object v2, v0, v1

    #@31
    sput-object v0, Landroid/content/SyncStorageEngine;->SOURCES:[Ljava/lang/String;

    #@33
    .line 142
    new-instance v0, Ljava/util/HashMap;

    #@35
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@38
    sput-object v0, Landroid/content/SyncStorageEngine;->sAuthorityRenames:Ljava/util/HashMap;

    #@3a
    .line 143
    sget-object v0, Landroid/content/SyncStorageEngine;->sAuthorityRenames:Ljava/util/HashMap;

    #@3c
    const-string v1, "contacts"

    #@3e
    const-string v2, "com.android.contacts"

    #@40
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@43
    .line 144
    sget-object v0, Landroid/content/SyncStorageEngine;->sAuthorityRenames:Ljava/util/HashMap;

    #@45
    const-string v1, "calendar"

    #@47
    const-string v2, "com.android.calendar"

    #@49
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4c
    .line 307
    const/4 v0, 0x0

    #@4d
    sput-object v0, Landroid/content/SyncStorageEngine;->sSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@4f
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/io/File;)V
    .registers 8
    .parameter "context"
    .parameter "dataDir"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 346
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@4
    .line 276
    new-instance v2, Landroid/util/SparseArray;

    #@6
    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    #@9
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@b
    .line 279
    new-instance v2, Ljava/util/HashMap;

    #@d
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@10
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mAccounts:Ljava/util/HashMap;

    #@12
    .line 282
    new-instance v2, Ljava/util/ArrayList;

    #@14
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@17
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@19
    .line 285
    new-instance v2, Landroid/util/SparseArray;

    #@1b
    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    #@1e
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mCurrentSyncs:Landroid/util/SparseArray;

    #@20
    .line 288
    new-instance v2, Landroid/util/SparseArray;

    #@22
    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    #@25
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@27
    .line 291
    new-instance v2, Ljava/util/ArrayList;

    #@29
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@2c
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@2e
    .line 294
    new-instance v2, Landroid/os/RemoteCallbackList;

    #@30
    invoke-direct {v2}, Landroid/os/RemoteCallbackList;-><init>()V

    #@33
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mChangeListeners:Landroid/os/RemoteCallbackList;

    #@35
    .line 297
    iput v3, p0, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@37
    .line 300
    const/16 v2, 0x1c

    #@39
    new-array v2, v2, [Landroid/content/SyncStorageEngine$DayStats;

    #@3b
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@3d
    .line 338
    iput v3, p0, Landroid/content/SyncStorageEngine;->mNumPendingFinished:I

    #@3f
    .line 340
    iput v3, p0, Landroid/content/SyncStorageEngine;->mNextHistoryId:I

    #@41
    .line 341
    new-instance v2, Landroid/util/SparseArray;

    #@43
    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    #@46
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mMasterSyncAutomatically:Landroid/util/SparseArray;

    #@48
    .line 347
    iput-object p1, p0, Landroid/content/SyncStorageEngine;->mContext:Landroid/content/Context;

    #@4a
    .line 348
    sput-object p0, Landroid/content/SyncStorageEngine;->sSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@4c
    .line 350
    const-string v2, "GMT+0"

    #@4e
    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@51
    move-result-object v2

    #@52
    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    #@55
    move-result-object v2

    #@56
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mCal:Ljava/util/Calendar;

    #@58
    .line 352
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mContext:Landroid/content/Context;

    #@5a
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5d
    move-result-object v2

    #@5e
    const v3, 0x1110046

    #@61
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@64
    move-result v2

    #@65
    iput-boolean v2, p0, Landroid/content/SyncStorageEngine;->mDefaultMasterSyncAutomatically:Z

    #@67
    .line 355
    new-instance v1, Ljava/io/File;

    #@69
    const-string/jumbo v2, "system"

    #@6c
    invoke-direct {v1, p2, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@6f
    .line 356
    .local v1, systemDir:Ljava/io/File;
    new-instance v0, Ljava/io/File;

    #@71
    const-string/jumbo v2, "sync"

    #@74
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@77
    .line 357
    .local v0, syncDir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@7a
    .line 358
    new-instance v2, Landroid/util/AtomicFile;

    #@7c
    new-instance v3, Ljava/io/File;

    #@7e
    const-string v4, "accounts.xml"

    #@80
    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@83
    invoke-direct {v2, v3}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@86
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mAccountInfoFile:Landroid/util/AtomicFile;

    #@88
    .line 359
    new-instance v2, Landroid/util/AtomicFile;

    #@8a
    new-instance v3, Ljava/io/File;

    #@8c
    const-string/jumbo v4, "status.bin"

    #@8f
    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@92
    invoke-direct {v2, v3}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@95
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mStatusFile:Landroid/util/AtomicFile;

    #@97
    .line 360
    new-instance v2, Landroid/util/AtomicFile;

    #@99
    new-instance v3, Ljava/io/File;

    #@9b
    const-string/jumbo v4, "pending.bin"

    #@9e
    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@a1
    invoke-direct {v2, v3}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@a4
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mPendingFile:Landroid/util/AtomicFile;

    #@a6
    .line 361
    new-instance v2, Landroid/util/AtomicFile;

    #@a8
    new-instance v3, Ljava/io/File;

    #@aa
    const-string/jumbo v4, "stats.bin"

    #@ad
    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b0
    invoke-direct {v2, v3}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    #@b3
    iput-object v2, p0, Landroid/content/SyncStorageEngine;->mStatisticsFile:Landroid/util/AtomicFile;

    #@b5
    .line 363
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->readAccountInfoLocked()V

    #@b8
    .line 364
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->readStatusLocked()V

    #@bb
    .line 365
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->readPendingOperationsLocked()V

    #@be
    .line 366
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->readStatisticsLocked()V

    #@c1
    .line 367
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->readAndDeleteLegacyAccountInfoLocked()V

    #@c4
    .line 368
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@c7
    .line 369
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeStatusLocked()V

    #@ca
    .line 370
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writePendingOperationsLocked()V

    #@cd
    .line 371
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeStatisticsLocked()V

    #@d0
    .line 372
    return-void
.end method

.method private appendPendingOperationLocked(Landroid/content/SyncStorageEngine$PendingOperation;)V
    .registers 8
    .parameter "op"

    #@0
    .prologue
    .line 2156
    const/4 v2, 0x0

    #@1
    .line 2158
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mPendingFile:Landroid/util/AtomicFile;

    #@3
    invoke-virtual {v4}, Landroid/util/AtomicFile;->openAppend()Ljava/io/FileOutputStream;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_1c

    #@6
    move-result-object v2

    #@7
    .line 2166
    :try_start_7
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v3

    #@b
    .line 2167
    .local v3, out:Landroid/os/Parcel;
    invoke-direct {p0, p1, v3}, Landroid/content/SyncStorageEngine;->writePendingOperationLocked(Landroid/content/SyncStorageEngine$PendingOperation;Landroid/os/Parcel;)V

    #@e
    .line 2168
    invoke-virtual {v3}, Landroid/os/Parcel;->marshall()[B

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v2, v4}, Ljava/io/FileOutputStream;->write([B)V

    #@15
    .line 2169
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_2f
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_18} :catch_21

    #@18
    .line 2174
    :try_start_18
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_1b} :catch_2d

    #@1b
    .line 2178
    .end local v3           #out:Landroid/os/Parcel;
    :goto_1b
    return-void

    #@1c
    .line 2159
    :catch_1c
    move-exception v0

    #@1d
    .line 2161
    .local v0, e:Ljava/io/IOException;
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writePendingOperationsLocked()V

    #@20
    goto :goto_1b

    #@21
    .line 2170
    .end local v0           #e:Ljava/io/IOException;
    :catch_21
    move-exception v1

    #@22
    .line 2171
    .local v1, e1:Ljava/io/IOException;
    :try_start_22
    const-string v4, "SyncManager"

    #@24
    const-string v5, "Error writing pending operations"

    #@26
    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_29
    .catchall {:try_start_22 .. :try_end_29} :catchall_2f

    #@29
    .line 2174
    :try_start_29
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2c
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_1b

    #@2d
    .line 2175
    .end local v1           #e1:Ljava/io/IOException;
    :catch_2d
    move-exception v4

    #@2e
    goto :goto_1b

    #@2f
    .line 2173
    :catchall_2f
    move-exception v4

    #@30
    .line 2174
    :try_start_30
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_33
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_33} :catch_34

    #@33
    .line 2173
    :goto_33
    throw v4

    #@34
    .line 2175
    :catch_34
    move-exception v5

    #@35
    goto :goto_33
.end method

.method public static equals(Landroid/os/Bundle;Landroid/os/Bundle;)Z
    .registers 8
    .parameter "b1"
    .parameter "b2"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1088
    invoke-virtual {p0}, Landroid/os/Bundle;->size()I

    #@5
    move-result v4

    #@6
    invoke-virtual {p1}, Landroid/os/Bundle;->size()I

    #@9
    move-result v5

    #@a
    if-eq v4, v5, :cond_d

    #@c
    .line 1102
    :cond_c
    :goto_c
    return v2

    #@d
    .line 1091
    :cond_d
    invoke-virtual {p0}, Landroid/os/Bundle;->isEmpty()Z

    #@10
    move-result v4

    #@11
    if-eqz v4, :cond_15

    #@13
    move v2, v3

    #@14
    .line 1092
    goto :goto_c

    #@15
    .line 1094
    :cond_15
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@18
    move-result-object v4

    #@19
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1c
    move-result-object v0

    #@1d
    .local v0, i$:Ljava/util/Iterator;
    :cond_1d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@20
    move-result v4

    #@21
    if-eqz v4, :cond_3e

    #@23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Ljava/lang/String;

    #@29
    .line 1095
    .local v1, key:Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@2c
    move-result v4

    #@2d
    if-eqz v4, :cond_c

    #@2f
    .line 1098
    invoke-virtual {p0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v4

    #@3b
    if-nez v4, :cond_1d

    #@3d
    goto :goto_c

    #@3e
    .end local v1           #key:Ljava/lang/String;
    :cond_3e
    move v2, v3

    #@3f
    .line 1102
    goto :goto_c
.end method

.method private static flattenBundle(Landroid/os/Bundle;)[B
    .registers 4
    .parameter "bundle"

    #@0
    .prologue
    .line 2181
    const/4 v0, 0x0

    #@1
    .line 2182
    .local v0, flatData:[B
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v1

    #@5
    .line 2184
    .local v1, parcel:Landroid/os/Parcel;
    const/4 v2, 0x0

    #@6
    :try_start_6
    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@9
    .line 2185
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B
    :try_end_c
    .catchall {:try_start_6 .. :try_end_c} :catchall_11

    #@c
    move-result-object v0

    #@d
    .line 2187
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@10
    .line 2189
    return-object v0

    #@11
    .line 2187
    :catchall_11
    move-exception v2

    #@12
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@15
    throw v2
.end method

.method private getAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;
    .registers 10
    .parameter "accountName"
    .parameter "userId"
    .parameter "authorityName"
    .parameter "tag"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1366
    new-instance v1, Landroid/accounts/AccountAndUser;

    #@3
    invoke-direct {v1, p1, p2}, Landroid/accounts/AccountAndUser;-><init>(Landroid/accounts/Account;I)V

    #@6
    .line 1367
    .local v1, au:Landroid/accounts/AccountAndUser;
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mAccounts:Ljava/util/HashMap;

    #@8
    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/content/SyncStorageEngine$AccountInfo;

    #@e
    .line 1368
    .local v0, accountInfo:Landroid/content/SyncStorageEngine$AccountInfo;
    if-nez v0, :cond_14

    #@10
    .line 1369
    if-eqz p4, :cond_12

    #@12
    :cond_12
    move-object v2, v3

    #@13
    .line 1386
    :cond_13
    :goto_13
    return-object v2

    #@14
    .line 1376
    :cond_14
    iget-object v4, v0, Landroid/content/SyncStorageEngine$AccountInfo;->authorities:Ljava/util/HashMap;

    #@16
    invoke-virtual {v4, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@1c
    .line 1377
    .local v2, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-nez v2, :cond_13

    #@1e
    .line 1378
    if-eqz p4, :cond_20

    #@20
    :cond_20
    move-object v2, v3

    #@21
    .line 1383
    goto :goto_13
.end method

.method private getCurrentDayLocked()I
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1345
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mCal:Ljava/util/Calendar;

    #@3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@6
    move-result-wide v2

    #@7
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@a
    .line 1346
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mCal:Ljava/util/Calendar;

    #@c
    const/4 v2, 0x6

    #@d
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    #@10
    move-result v0

    #@11
    .line 1347
    .local v0, dayOfYear:I
    iget v1, p0, Landroid/content/SyncStorageEngine;->mYear:I

    #@13
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mCal:Ljava/util/Calendar;

    #@15
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    #@18
    move-result v2

    #@19
    if-eq v1, v2, :cond_3c

    #@1b
    .line 1348
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mCal:Ljava/util/Calendar;

    #@1d
    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    #@20
    move-result v1

    #@21
    iput v1, p0, Landroid/content/SyncStorageEngine;->mYear:I

    #@23
    .line 1349
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mCal:Ljava/util/Calendar;

    #@25
    invoke-virtual {v1}, Ljava/util/Calendar;->clear()V

    #@28
    .line 1350
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mCal:Ljava/util/Calendar;

    #@2a
    iget v2, p0, Landroid/content/SyncStorageEngine;->mYear:I

    #@2c
    invoke-virtual {v1, v4, v2}, Ljava/util/Calendar;->set(II)V

    #@2f
    .line 1351
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mCal:Ljava/util/Calendar;

    #@31
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@34
    move-result-wide v1

    #@35
    const-wide/32 v3, 0x5265c00

    #@38
    div-long/2addr v1, v3

    #@39
    long-to-int v1, v1

    #@3a
    iput v1, p0, Landroid/content/SyncStorageEngine;->mYearInDays:I

    #@3c
    .line 1353
    :cond_3c
    iget v1, p0, Landroid/content/SyncStorageEngine;->mYearInDays:I

    #@3e
    add-int/2addr v1, v0

    #@3f
    return v1
.end method

.method static getIntColumn(Landroid/database/Cursor;Ljava/lang/String;)I
    .registers 3
    .parameter "c"
    .parameter "name"

    #@0
    .prologue
    .line 1849
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method static getLongColumn(Landroid/database/Cursor;Ljava/lang/String;)J
    .registers 4
    .parameter "c"
    .parameter "name"

    #@0
    .prologue
    .line 1853
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method private getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;
    .registers 10
    .parameter "accountName"
    .parameter "userId"
    .parameter "authorityName"
    .parameter "ident"
    .parameter "doWrite"

    #@0
    .prologue
    .line 1391
    new-instance v1, Landroid/accounts/AccountAndUser;

    #@2
    invoke-direct {v1, p1, p2}, Landroid/accounts/AccountAndUser;-><init>(Landroid/accounts/Account;I)V

    #@5
    .line 1392
    .local v1, au:Landroid/accounts/AccountAndUser;
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mAccounts:Ljava/util/HashMap;

    #@7
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/content/SyncStorageEngine$AccountInfo;

    #@d
    .line 1393
    .local v0, account:Landroid/content/SyncStorageEngine$AccountInfo;
    if-nez v0, :cond_19

    #@f
    .line 1394
    new-instance v0, Landroid/content/SyncStorageEngine$AccountInfo;

    #@11
    .end local v0           #account:Landroid/content/SyncStorageEngine$AccountInfo;
    invoke-direct {v0, v1}, Landroid/content/SyncStorageEngine$AccountInfo;-><init>(Landroid/accounts/AccountAndUser;)V

    #@14
    .line 1395
    .restart local v0       #account:Landroid/content/SyncStorageEngine$AccountInfo;
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mAccounts:Ljava/util/HashMap;

    #@16
    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 1397
    :cond_19
    iget-object v3, v0, Landroid/content/SyncStorageEngine$AccountInfo;->authorities:Ljava/util/HashMap;

    #@1b
    invoke-virtual {v3, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@21
    .line 1398
    .local v2, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-nez v2, :cond_42

    #@23
    .line 1399
    if-gez p4, :cond_2e

    #@25
    .line 1400
    iget p4, p0, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@27
    .line 1401
    iget v3, p0, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@29
    add-int/lit8 v3, v3, 0x1

    #@2b
    iput v3, p0, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@2d
    .line 1402
    const/4 p5, 0x1

    #@2e
    .line 1409
    :cond_2e
    new-instance v2, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@30
    .end local v2           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    invoke-direct {v2, p1, p2, p3, p4}, Landroid/content/SyncStorageEngine$AuthorityInfo;-><init>(Landroid/accounts/Account;ILjava/lang/String;I)V

    #@33
    .line 1410
    .restart local v2       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    iget-object v3, v0, Landroid/content/SyncStorageEngine$AccountInfo;->authorities:Ljava/util/HashMap;

    #@35
    invoke-virtual {v3, p3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    .line 1411
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@3a
    invoke-virtual {v3, p4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@3d
    .line 1412
    if-eqz p5, :cond_42

    #@3f
    .line 1413
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@42
    .line 1417
    :cond_42
    return-object v2
.end method

.method private getOrCreateSyncStatusLocked(I)Landroid/content/SyncStatusInfo;
    .registers 4
    .parameter "authorityId"

    #@0
    .prologue
    .line 1441
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/content/SyncStatusInfo;

    #@8
    .line 1442
    .local v0, status:Landroid/content/SyncStatusInfo;
    if-nez v0, :cond_14

    #@a
    .line 1443
    new-instance v0, Landroid/content/SyncStatusInfo;

    #@c
    .end local v0           #status:Landroid/content/SyncStatusInfo;
    invoke-direct {v0, p1}, Landroid/content/SyncStatusInfo;-><init>(I)V

    #@f
    .line 1444
    .restart local v0       #status:Landroid/content/SyncStatusInfo;
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@14
    .line 1446
    :cond_14
    return-object v0
.end method

.method public static getSingleton()Landroid/content/SyncStorageEngine;
    .registers 2

    #@0
    .prologue
    .line 389
    sget-object v0, Landroid/content/SyncStorageEngine;->sSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 390
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "not initialized"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 392
    :cond_d
    sget-object v0, Landroid/content/SyncStorageEngine;->sSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@f
    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 379
    sget-object v1, Landroid/content/SyncStorageEngine;->sSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@2
    if-eqz v1, :cond_5

    #@4
    .line 386
    :goto_4
    return-void

    #@5
    .line 384
    :cond_5
    invoke-static {}, Landroid/os/Environment;->getSecureDataDirectory()Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    .line 385
    .local v0, dataDir:Ljava/io/File;
    new-instance v1, Landroid/content/SyncStorageEngine;

    #@b
    invoke-direct {v1, p0, v0}, Landroid/content/SyncStorageEngine;-><init>(Landroid/content/Context;Ljava/io/File;)V

    #@e
    sput-object v1, Landroid/content/SyncStorageEngine;->sSyncStorageEngine:Landroid/content/SyncStorageEngine;

    #@10
    goto :goto_4
.end method

.method private maybeMigrateSettingsForRenamedAuthorities()Z
    .registers 15

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1584
    const/4 v13, 0x0

    #@2
    .line 1586
    .local v13, writeNeeded:Z
    new-instance v7, Ljava/util/ArrayList;

    #@4
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@7
    .line 1587
    .local v7, authoritiesToRemove:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStorageEngine$AuthorityInfo;>;"
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@9
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    #@c
    move-result v6

    #@d
    .line 1588
    .local v6, N:I
    const/4 v10, 0x0

    #@e
    .local v10, i:I
    :goto_e
    if-ge v10, v6, :cond_49

    #@10
    .line 1589
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@12
    invoke-virtual {v0, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@15
    move-result-object v8

    #@16
    check-cast v8, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@18
    .line 1591
    .local v8, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    sget-object v0, Landroid/content/SyncStorageEngine;->sAuthorityRenames:Ljava/util/HashMap;

    #@1a
    iget-object v1, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@1c
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    check-cast v3, Ljava/lang/String;

    #@22
    .line 1592
    .local v3, newAuthorityName:Ljava/lang/String;
    if-nez v3, :cond_27

    #@24
    .line 1588
    :cond_24
    :goto_24
    add-int/lit8 v10, v10, 0x1

    #@26
    goto :goto_e

    #@27
    .line 1598
    :cond_27
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2a
    .line 1602
    iget-boolean v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@2c
    if-eqz v0, :cond_24

    #@2e
    .line 1607
    iget-object v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@30
    iget v1, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@32
    const-string v2, "cleanup"

    #@34
    invoke-direct {p0, v0, v1, v3, v2}, Landroid/content/SyncStorageEngine;->getAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@37
    move-result-object v0

    #@38
    if-nez v0, :cond_24

    #@3a
    .line 1612
    iget-object v1, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@3c
    iget v2, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@3e
    const/4 v4, -0x1

    #@3f
    move-object v0, p0

    #@40
    invoke-direct/range {v0 .. v5}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@43
    move-result-object v12

    #@44
    .line 1614
    .local v12, newAuthority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    const/4 v0, 0x1

    #@45
    iput-boolean v0, v12, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@47
    .line 1615
    const/4 v13, 0x1

    #@48
    goto :goto_24

    #@49
    .line 1618
    .end local v3           #newAuthorityName:Ljava/lang/String;
    .end local v8           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v12           #newAuthority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_49
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@4c
    move-result-object v11

    #@4d
    .local v11, i$:Ljava/util/Iterator;
    :goto_4d
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@50
    move-result v0

    #@51
    if-eqz v0, :cond_64

    #@53
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@56
    move-result-object v9

    #@57
    check-cast v9, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@59
    .line 1619
    .local v9, authorityInfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    iget-object v0, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@5b
    iget v1, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@5d
    iget-object v2, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@5f
    invoke-direct {p0, v0, v1, v2, v5}, Landroid/content/SyncStorageEngine;->removeAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Z)V

    #@62
    .line 1621
    const/4 v13, 0x1

    #@63
    goto :goto_4d

    #@64
    .line 1624
    .end local v9           #authorityInfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_64
    return v13
.end method

.method public static newTestInstance(Landroid/content/Context;)Landroid/content/SyncStorageEngine;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 375
    new-instance v0, Landroid/content/SyncStorageEngine;

    #@2
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, p0, v1}, Landroid/content/SyncStorageEngine;-><init>(Landroid/content/Context;Ljava/io/File;)V

    #@9
    return-object v0
.end method

.method private parseAuthority(Lorg/xmlpull/v1/XmlPullParser;I)Landroid/content/SyncStorageEngine$AuthorityInfo;
    .registers 16
    .parameter "parser"
    .parameter "version"

    #@0
    .prologue
    .line 1643
    const/4 v8, 0x0

    #@1
    .line 1644
    .local v8, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    const/4 v4, -0x1

    #@2
    .line 1646
    .local v4, id:I
    const/4 v0, 0x0

    #@3
    :try_start_3
    const-string v1, "id"

    #@5
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_c} :catch_7c
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_c} :catch_85

    #@c
    move-result v4

    #@d
    .line 1653
    :goto_d
    if-ltz v4, :cond_7b

    #@f
    .line 1654
    const/4 v0, 0x0

    #@10
    const-string v1, "authority"

    #@12
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    .line 1655
    .local v3, authorityName:Ljava/lang/String;
    const/4 v0, 0x0

    #@17
    const-string v1, "enabled"

    #@19
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v10

    #@1d
    .line 1656
    .local v10, enabled:Ljava/lang/String;
    const/4 v0, 0x0

    #@1e
    const-string/jumbo v1, "syncable"

    #@21
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v11

    #@25
    .line 1657
    .local v11, syncable:Ljava/lang/String;
    const/4 v0, 0x0

    #@26
    const-string v1, "account"

    #@28
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    .line 1658
    .local v6, accountName:Ljava/lang/String;
    const/4 v0, 0x0

    #@2d
    const-string/jumbo v1, "type"

    #@30
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v7

    #@34
    .line 1659
    .local v7, accountType:Ljava/lang/String;
    const/4 v0, 0x0

    #@35
    const-string/jumbo v1, "user"

    #@38
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3b
    move-result-object v12

    #@3c
    .line 1660
    .local v12, user:Ljava/lang/String;
    if-nez v12, :cond_90

    #@3e
    const/4 v2, 0x0

    #@3f
    .line 1661
    .local v2, userId:I
    :goto_3f
    if-nez v7, :cond_46

    #@41
    .line 1662
    const-string v7, "com.google"

    #@43
    .line 1663
    const-string/jumbo v11, "unknown"

    #@46
    .line 1665
    :cond_46
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@48
    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@4b
    move-result-object v8

    #@4c
    .end local v8           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    check-cast v8, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@4e
    .line 1671
    .restart local v8       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-nez v8, :cond_62

    #@50
    .line 1673
    new-instance v1, Landroid/accounts/Account;

    #@52
    invoke-direct {v1, v6, v7}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    const/4 v5, 0x0

    #@56
    move-object v0, p0

    #@57
    invoke-direct/range {v0 .. v5}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@5a
    move-result-object v8

    #@5b
    .line 1680
    if-lez p2, :cond_62

    #@5d
    .line 1681
    iget-object v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@5f
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@62
    .line 1684
    :cond_62
    if-eqz v8, :cond_a5

    #@64
    .line 1685
    if-eqz v10, :cond_6c

    #@66
    invoke-static {v10}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@69
    move-result v0

    #@6a
    if-eqz v0, :cond_95

    #@6c
    :cond_6c
    const/4 v0, 0x1

    #@6d
    :goto_6d
    iput-boolean v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@6f
    .line 1686
    const-string/jumbo v0, "unknown"

    #@72
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@75
    move-result v0

    #@76
    if-eqz v0, :cond_97

    #@78
    .line 1687
    const/4 v0, -0x1

    #@79
    iput v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@7b
    .line 1700
    .end local v2           #userId:I
    .end local v3           #authorityName:Ljava/lang/String;
    .end local v6           #accountName:Ljava/lang/String;
    .end local v7           #accountType:Ljava/lang/String;
    .end local v10           #enabled:Ljava/lang/String;
    .end local v11           #syncable:Ljava/lang/String;
    .end local v12           #user:Ljava/lang/String;
    :cond_7b
    :goto_7b
    return-object v8

    #@7c
    .line 1648
    :catch_7c
    move-exception v9

    #@7d
    .line 1649
    .local v9, e:Ljava/lang/NumberFormatException;
    const-string v0, "SyncManager"

    #@7f
    const-string v1, "error parsing the id of the authority"

    #@81
    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@84
    goto :goto_d

    #@85
    .line 1650
    .end local v9           #e:Ljava/lang/NumberFormatException;
    :catch_85
    move-exception v9

    #@86
    .line 1651
    .local v9, e:Ljava/lang/NullPointerException;
    const-string v0, "SyncManager"

    #@88
    const-string/jumbo v1, "the id of the authority is null"

    #@8b
    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8e
    goto/16 :goto_d

    #@90
    .line 1660
    .end local v9           #e:Ljava/lang/NullPointerException;
    .restart local v3       #authorityName:Ljava/lang/String;
    .restart local v6       #accountName:Ljava/lang/String;
    .restart local v7       #accountType:Ljava/lang/String;
    .restart local v10       #enabled:Ljava/lang/String;
    .restart local v11       #syncable:Ljava/lang/String;
    .restart local v12       #user:Ljava/lang/String;
    :cond_90
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@93
    move-result v2

    #@94
    goto :goto_3f

    #@95
    .line 1685
    .restart local v2       #userId:I
    :cond_95
    const/4 v0, 0x0

    #@96
    goto :goto_6d

    #@97
    .line 1689
    :cond_97
    if-eqz v11, :cond_9f

    #@99
    invoke-static {v11}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@9c
    move-result v0

    #@9d
    if-eqz v0, :cond_a3

    #@9f
    :cond_9f
    const/4 v0, 0x1

    #@a0
    :goto_a0
    iput v0, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@a2
    goto :goto_7b

    #@a3
    :cond_a3
    const/4 v0, 0x0

    #@a4
    goto :goto_a0

    #@a5
    .line 1693
    :cond_a5
    const-string v0, "SyncManager"

    #@a7
    new-instance v1, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v5, "Failure adding authority: account="

    #@ae
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v1

    #@b2
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v1

    #@b6
    const-string v5, " auth="

    #@b8
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v1

    #@bc
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v1

    #@c0
    const-string v5, " enabled="

    #@c2
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v1

    #@c6
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v1

    #@ca
    const-string v5, " syncable="

    #@cc
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v1

    #@d0
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v1

    #@d4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v1

    #@d8
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@db
    goto :goto_7b
.end method

.method private parseExtra(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/Pair;)V
    .registers 11
    .parameter "parser"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Landroid/util/Pair",
            "<",
            "Landroid/os/Bundle;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p2, periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    const/4 v7, 0x0

    #@1
    .line 1723
    iget-object v1, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@3
    check-cast v1, Landroid/os/Bundle;

    #@5
    .line 1724
    .local v1, extras:Landroid/os/Bundle;
    const-string/jumbo v6, "name"

    #@8
    invoke-interface {p1, v7, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    .line 1725
    .local v2, name:Ljava/lang/String;
    const-string/jumbo v6, "type"

    #@f
    invoke-interface {p1, v7, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    .line 1726
    .local v3, type:Ljava/lang/String;
    const-string/jumbo v6, "value1"

    #@16
    invoke-interface {p1, v7, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    .line 1727
    .local v4, value1:Ljava/lang/String;
    const-string/jumbo v6, "value2"

    #@1d
    invoke-interface {p1, v7, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    .line 1730
    .local v5, value2:Ljava/lang/String;
    :try_start_21
    const-string/jumbo v6, "long"

    #@24
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v6

    #@28
    if-eqz v6, :cond_32

    #@2a
    .line 1731
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@2d
    move-result-wide v6

    #@2e
    invoke-virtual {v1, v2, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@31
    .line 1750
    :cond_31
    :goto_31
    return-void

    #@32
    .line 1732
    :cond_32
    const-string v6, "integer"

    #@34
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v6

    #@38
    if-eqz v6, :cond_4b

    #@3a
    .line 1733
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3d
    move-result v6

    #@3e
    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_41
    .catch Ljava/lang/NumberFormatException; {:try_start_21 .. :try_end_41} :catch_42
    .catch Ljava/lang/NullPointerException; {:try_start_21 .. :try_end_41} :catch_5b

    #@41
    goto :goto_31

    #@42
    .line 1745
    :catch_42
    move-exception v0

    #@43
    .line 1746
    .local v0, e:Ljava/lang/NumberFormatException;
    const-string v6, "SyncManager"

    #@45
    const-string v7, "error parsing bundle value"

    #@47
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4a
    goto :goto_31

    #@4b
    .line 1734
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :cond_4b
    :try_start_4b
    const-string v6, "double"

    #@4d
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v6

    #@51
    if-eqz v6, :cond_64

    #@53
    .line 1735
    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@56
    move-result-wide v6

    #@57
    invoke-virtual {v1, v2, v6, v7}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V
    :try_end_5a
    .catch Ljava/lang/NumberFormatException; {:try_start_4b .. :try_end_5a} :catch_42
    .catch Ljava/lang/NullPointerException; {:try_start_4b .. :try_end_5a} :catch_5b

    #@5a
    goto :goto_31

    #@5b
    .line 1747
    :catch_5b
    move-exception v0

    #@5c
    .line 1748
    .local v0, e:Ljava/lang/NullPointerException;
    const-string v6, "SyncManager"

    #@5e
    const-string v7, "error parsing bundle value"

    #@60
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@63
    goto :goto_31

    #@64
    .line 1736
    .end local v0           #e:Ljava/lang/NullPointerException;
    :cond_64
    :try_start_64
    const-string v6, "float"

    #@66
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v6

    #@6a
    if-eqz v6, :cond_74

    #@6c
    .line 1737
    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@6f
    move-result v6

    #@70
    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@73
    goto :goto_31

    #@74
    .line 1738
    :cond_74
    const-string v6, "boolean"

    #@76
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@79
    move-result v6

    #@7a
    if-eqz v6, :cond_84

    #@7c
    .line 1739
    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@7f
    move-result v6

    #@80
    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@83
    goto :goto_31

    #@84
    .line 1740
    :cond_84
    const-string/jumbo v6, "string"

    #@87
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8a
    move-result v6

    #@8b
    if-eqz v6, :cond_91

    #@8d
    .line 1741
    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@90
    goto :goto_31

    #@91
    .line 1742
    :cond_91
    const-string v6, "account"

    #@93
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@96
    move-result v6

    #@97
    if-eqz v6, :cond_31

    #@99
    .line 1743
    new-instance v6, Landroid/accounts/Account;

    #@9b
    invoke-direct {v6, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@9e
    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_a1
    .catch Ljava/lang/NumberFormatException; {:try_start_64 .. :try_end_a1} :catch_42
    .catch Ljava/lang/NullPointerException; {:try_start_64 .. :try_end_a1} :catch_5b

    #@a1
    goto :goto_31
.end method

.method private parseListenForTickles(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 10
    .parameter "parser"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1628
    const-string/jumbo v5, "user"

    #@4
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 1629
    .local v3, user:Ljava/lang/String;
    const/4 v4, 0x0

    #@9
    .line 1631
    .local v4, userId:I
    :try_start_9
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_c} :catch_26
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_c} :catch_2f

    #@c
    move-result v4

    #@d
    .line 1637
    :goto_d
    const-string v5, "enabled"

    #@f
    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    .line 1638
    .local v1, enabled:Ljava/lang/String;
    if-eqz v1, :cond_1b

    #@15
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@18
    move-result v5

    #@19
    if-eqz v5, :cond_39

    #@1b
    :cond_1b
    const/4 v2, 0x1

    #@1c
    .line 1639
    .local v2, listen:Z
    :goto_1c
    iget-object v5, p0, Landroid/content/SyncStorageEngine;->mMasterSyncAutomatically:Landroid/util/SparseArray;

    #@1e
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@21
    move-result-object v6

    #@22
    invoke-virtual {v5, v4, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@25
    .line 1640
    return-void

    #@26
    .line 1632
    .end local v1           #enabled:Ljava/lang/String;
    .end local v2           #listen:Z
    :catch_26
    move-exception v0

    #@27
    .line 1633
    .local v0, e:Ljava/lang/NumberFormatException;
    const-string v5, "SyncManager"

    #@29
    const-string v6, "error parsing the user for listen-for-tickles"

    #@2b
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2e
    goto :goto_d

    #@2f
    .line 1634
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :catch_2f
    move-exception v0

    #@30
    .line 1635
    .local v0, e:Ljava/lang/NullPointerException;
    const-string v5, "SyncManager"

    #@32
    const-string/jumbo v6, "the user in listen-for-tickles is null"

    #@35
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@38
    goto :goto_d

    #@39
    .line 1638
    .end local v0           #e:Ljava/lang/NullPointerException;
    .restart local v1       #enabled:Ljava/lang/String;
    :cond_39
    const/4 v2, 0x0

    #@3a
    goto :goto_1c
.end method

.method private parsePeriodicSync(Lorg/xmlpull/v1/XmlPullParser;Landroid/content/SyncStorageEngine$AuthorityInfo;)Landroid/util/Pair;
    .registers 11
    .parameter "parser"
    .parameter "authority"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Landroid/content/SyncStorageEngine$AuthorityInfo;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Landroid/os/Bundle;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1704
    new-instance v1, Landroid/os/Bundle;

    #@3
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@6
    .line 1705
    .local v1, extras:Landroid/os/Bundle;
    const-string/jumbo v6, "period"

    #@9
    invoke-interface {p1, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v4

    #@d
    .line 1708
    .local v4, periodValue:Ljava/lang/String;
    :try_start_d
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_10
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_10} :catch_1f
    .catch Ljava/lang/NullPointerException; {:try_start_d .. :try_end_10} :catch_28

    #@10
    move-result-wide v2

    #@11
    .line 1716
    .local v2, period:J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@14
    move-result-object v6

    #@15
    invoke-static {v1, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@18
    move-result-object v5

    #@19
    .line 1717
    .local v5, periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    iget-object v6, p2, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e
    .line 1719
    .end local v2           #period:J
    .end local v5           #periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :goto_1e
    return-object v5

    #@1f
    .line 1709
    :catch_1f
    move-exception v0

    #@20
    .line 1710
    .local v0, e:Ljava/lang/NumberFormatException;
    const-string v6, "SyncManager"

    #@22
    const-string v7, "error parsing the period of a periodic sync"

    #@24
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_1e

    #@28
    .line 1712
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :catch_28
    move-exception v0

    #@29
    .line 1713
    .local v0, e:Ljava/lang/NullPointerException;
    const-string v6, "SyncManager"

    #@2b
    const-string/jumbo v7, "the period of a periodic sync is null"

    #@2e
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@31
    goto :goto_1e
.end method

.method private readAccountInfoLocked()V
    .registers 22

    #@0
    .prologue
    .line 1491
    const/4 v7, -0x1

    #@1
    .line 1492
    .local v7, highestAuthorityId:I
    const/4 v6, 0x0

    #@2
    .line 1494
    .local v6, fis:Ljava/io/FileInputStream;
    :try_start_2
    move-object/from16 v0, p0

    #@4
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mAccountInfoFile:Landroid/util/AtomicFile;

    #@6
    move-object/from16 v18, v0

    #@8
    invoke-virtual/range {v18 .. v18}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@b
    move-result-object v6

    #@c
    .line 1496
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@f
    move-result-object v12

    #@10
    .line 1497
    .local v12, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/16 v18, 0x0

    #@12
    move-object/from16 v0, v18

    #@14
    invoke-interface {v12, v6, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@17
    .line 1498
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@1a
    move-result v5

    #@1b
    .line 1499
    .local v5, eventType:I
    :goto_1b
    const/16 v18, 0x2

    #@1d
    move/from16 v0, v18

    #@1f
    if-eq v5, v0, :cond_26

    #@21
    .line 1500
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@24
    move-result v5

    #@25
    goto :goto_1b

    #@26
    .line 1502
    :cond_26
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@29
    move-result-object v15

    #@2a
    .line 1503
    .local v15, tagName:Ljava/lang/String;
    const-string v18, "accounts"

    #@2c
    move-object/from16 v0, v18

    #@2e
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v18

    #@32
    if-eqz v18, :cond_10f

    #@34
    .line 1504
    const/16 v18, 0x0

    #@36
    const-string/jumbo v19, "listen-for-tickles"

    #@39
    move-object/from16 v0, v18

    #@3b
    move-object/from16 v1, v19

    #@3d
    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v9

    #@41
    .line 1505
    .local v9, listen:Ljava/lang/String;
    const/16 v18, 0x0

    #@43
    const-string/jumbo v19, "version"

    #@46
    move-object/from16 v0, v18

    #@48
    move-object/from16 v1, v19

    #@4a
    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4d
    move-result-object v17

    #@4e
    .line 1508
    .local v17, versionString:Ljava/lang/String;
    if-nez v17, :cond_12a

    #@50
    const/16 v16, 0x0

    #@52
    .line 1512
    .local v16, version:I
    :goto_52
    const/16 v18, 0x0

    #@54
    const-string/jumbo v19, "nextAuthorityId"

    #@57
    move-object/from16 v0, v18

    #@59
    move-object/from16 v1, v19

    #@5b
    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_5e
    .catchall {:try_start_2 .. :try_end_5e} :catchall_1ea
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_5e} :catch_14c
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_5e} :catch_188

    #@5e
    move-result-object v10

    #@5f
    .line 1514
    .local v10, nextIdString:Ljava/lang/String;
    if-nez v10, :cond_135

    #@61
    const/4 v8, 0x0

    #@62
    .line 1515
    .local v8, id:I
    :goto_62
    :try_start_62
    move-object/from16 v0, p0

    #@64
    iget v0, v0, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@66
    move/from16 v18, v0

    #@68
    move/from16 v0, v18

    #@6a
    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    #@6d
    move-result v18

    #@6e
    move/from16 v0, v18

    #@70
    move-object/from16 v1, p0

    #@72
    iput v0, v1, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I
    :try_end_74
    .catchall {:try_start_62 .. :try_end_74} :catchall_1ea
    .catch Ljava/lang/NumberFormatException; {:try_start_62 .. :try_end_74} :catch_214
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_62 .. :try_end_74} :catch_14c
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_74} :catch_188

    #@74
    .line 1519
    .end local v8           #id:I
    :goto_74
    const/16 v18, 0x0

    #@76
    :try_start_76
    const-string/jumbo v19, "offsetInSeconds"

    #@79
    move-object/from16 v0, v18

    #@7b
    move-object/from16 v1, v19

    #@7d
    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_80
    .catchall {:try_start_76 .. :try_end_80} :catchall_1ea
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_76 .. :try_end_80} :catch_14c
    .catch Ljava/io/IOException; {:try_start_76 .. :try_end_80} :catch_188

    #@80
    move-result-object v11

    #@81
    .line 1521
    .local v11, offsetString:Ljava/lang/String;
    if-nez v11, :cond_13b

    #@83
    const/16 v18, 0x0

    #@85
    :goto_85
    :try_start_85
    move/from16 v0, v18

    #@87
    move-object/from16 v1, p0

    #@89
    iput v0, v1, Landroid/content/SyncStorageEngine;->mSyncRandomOffset:I
    :try_end_8b
    .catchall {:try_start_85 .. :try_end_8b} :catchall_1ea
    .catch Ljava/lang/NumberFormatException; {:try_start_85 .. :try_end_8b} :catch_141
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_85 .. :try_end_8b} :catch_14c
    .catch Ljava/io/IOException; {:try_start_85 .. :try_end_8b} :catch_188

    #@8b
    .line 1525
    :goto_8b
    :try_start_8b
    move-object/from16 v0, p0

    #@8d
    iget v0, v0, Landroid/content/SyncStorageEngine;->mSyncRandomOffset:I

    #@8f
    move/from16 v18, v0

    #@91
    if-nez v18, :cond_ad

    #@93
    .line 1526
    new-instance v14, Ljava/util/Random;

    #@95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@98
    move-result-wide v18

    #@99
    move-wide/from16 v0, v18

    #@9b
    invoke-direct {v14, v0, v1}, Ljava/util/Random;-><init>(J)V

    #@9e
    .line 1527
    .local v14, random:Ljava/util/Random;
    const v18, 0x15180

    #@a1
    move/from16 v0, v18

    #@a3
    invoke-virtual {v14, v0}, Ljava/util/Random;->nextInt(I)I

    #@a6
    move-result v18

    #@a7
    move/from16 v0, v18

    #@a9
    move-object/from16 v1, p0

    #@ab
    iput v0, v1, Landroid/content/SyncStorageEngine;->mSyncRandomOffset:I

    #@ad
    .line 1529
    .end local v14           #random:Ljava/util/Random;
    :cond_ad
    move-object/from16 v0, p0

    #@af
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mMasterSyncAutomatically:Landroid/util/SparseArray;

    #@b1
    move-object/from16 v19, v0

    #@b3
    const/16 v20, 0x0

    #@b5
    if-eqz v9, :cond_bd

    #@b7
    invoke-static {v9}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@ba
    move-result v18

    #@bb
    if-eqz v18, :cond_172

    #@bd
    :cond_bd
    const/16 v18, 0x1

    #@bf
    :goto_bf
    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@c2
    move-result-object v18

    #@c3
    move-object/from16 v0, v19

    #@c5
    move/from16 v1, v20

    #@c7
    move-object/from16 v2, v18

    #@c9
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@cc
    .line 1530
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@cf
    move-result v5

    #@d0
    .line 1531
    const/4 v3, 0x0

    #@d1
    .line 1532
    .local v3, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    const/4 v13, 0x0

    #@d2
    .line 1534
    .local v13, periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :cond_d2
    const/16 v18, 0x2

    #@d4
    move/from16 v0, v18

    #@d6
    if-ne v5, v0, :cond_105

    #@d8
    .line 1535
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@db
    move-result-object v15

    #@dc
    .line 1536
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@df
    move-result v18

    #@e0
    const/16 v19, 0x2

    #@e2
    move/from16 v0, v18

    #@e4
    move/from16 v1, v19

    #@e6
    if-ne v0, v1, :cond_1aa

    #@e8
    .line 1537
    const-string v18, "authority"

    #@ea
    move-object/from16 v0, v18

    #@ec
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ef
    move-result v18

    #@f0
    if-eqz v18, :cond_176

    #@f2
    .line 1538
    move-object/from16 v0, p0

    #@f4
    move/from16 v1, v16

    #@f6
    invoke-direct {v0, v12, v1}, Landroid/content/SyncStorageEngine;->parseAuthority(Lorg/xmlpull/v1/XmlPullParser;I)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@f9
    move-result-object v3

    #@fa
    .line 1539
    const/4 v13, 0x0

    #@fb
    .line 1540
    iget v0, v3, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@fd
    move/from16 v18, v0

    #@ff
    move/from16 v0, v18

    #@101
    if-le v0, v7, :cond_105

    #@103
    .line 1541
    iget v7, v3, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@105
    .line 1556
    :cond_105
    :goto_105
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_108
    .catchall {:try_start_8b .. :try_end_108} :catchall_1ea
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_8b .. :try_end_108} :catch_14c
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_108} :catch_188

    #@108
    move-result v5

    #@109
    .line 1557
    const/16 v18, 0x1

    #@10b
    move/from16 v0, v18

    #@10d
    if-ne v5, v0, :cond_d2

    #@10f
    .line 1567
    .end local v3           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v9           #listen:Ljava/lang/String;
    .end local v10           #nextIdString:Ljava/lang/String;
    .end local v11           #offsetString:Ljava/lang/String;
    .end local v13           #periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    .end local v16           #version:I
    .end local v17           #versionString:Ljava/lang/String;
    :cond_10f
    add-int/lit8 v18, v7, 0x1

    #@111
    move-object/from16 v0, p0

    #@113
    iget v0, v0, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@115
    move/from16 v19, v0

    #@117
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    #@11a
    move-result v18

    #@11b
    move/from16 v0, v18

    #@11d
    move-object/from16 v1, p0

    #@11f
    iput v0, v1, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@121
    .line 1568
    if-eqz v6, :cond_126

    #@123
    .line 1570
    :try_start_123
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_126
    .catch Ljava/io/IOException; {:try_start_123 .. :try_end_126} :catch_211

    #@126
    .line 1576
    :cond_126
    :goto_126
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->maybeMigrateSettingsForRenamedAuthorities()Z

    #@129
    .line 1577
    .end local v5           #eventType:I
    .end local v12           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v15           #tagName:Ljava/lang/String;
    :cond_129
    :goto_129
    return-void

    #@12a
    .line 1508
    .restart local v5       #eventType:I
    .restart local v9       #listen:Ljava/lang/String;
    .restart local v12       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v15       #tagName:Ljava/lang/String;
    .restart local v17       #versionString:Ljava/lang/String;
    :cond_12a
    :try_start_12a
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_12d
    .catchall {:try_start_12a .. :try_end_12d} :catchall_1ea
    .catch Ljava/lang/NumberFormatException; {:try_start_12a .. :try_end_12d} :catch_130
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_12a .. :try_end_12d} :catch_14c
    .catch Ljava/io/IOException; {:try_start_12a .. :try_end_12d} :catch_188

    #@12d
    move-result v16

    #@12e
    goto/16 :goto_52

    #@130
    .line 1509
    :catch_130
    move-exception v4

    #@131
    .line 1510
    .local v4, e:Ljava/lang/NumberFormatException;
    const/16 v16, 0x0

    #@133
    .restart local v16       #version:I
    goto/16 :goto_52

    #@135
    .line 1514
    .end local v4           #e:Ljava/lang/NumberFormatException;
    .restart local v10       #nextIdString:Ljava/lang/String;
    :cond_135
    :try_start_135
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_138
    .catchall {:try_start_135 .. :try_end_138} :catchall_1ea
    .catch Ljava/lang/NumberFormatException; {:try_start_135 .. :try_end_138} :catch_214
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_135 .. :try_end_138} :catch_14c
    .catch Ljava/io/IOException; {:try_start_135 .. :try_end_138} :catch_188

    #@138
    move-result v8

    #@139
    goto/16 :goto_62

    #@13b
    .line 1521
    .restart local v11       #offsetString:Ljava/lang/String;
    :cond_13b
    :try_start_13b
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_13e
    .catchall {:try_start_13b .. :try_end_13e} :catchall_1ea
    .catch Ljava/lang/NumberFormatException; {:try_start_13b .. :try_end_13e} :catch_141
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_13b .. :try_end_13e} :catch_14c
    .catch Ljava/io/IOException; {:try_start_13b .. :try_end_13e} :catch_188

    #@13e
    move-result v18

    #@13f
    goto/16 :goto_85

    #@141
    .line 1522
    :catch_141
    move-exception v4

    #@142
    .line 1523
    .restart local v4       #e:Ljava/lang/NumberFormatException;
    const/16 v18, 0x0

    #@144
    :try_start_144
    move/from16 v0, v18

    #@146
    move-object/from16 v1, p0

    #@148
    iput v0, v1, Landroid/content/SyncStorageEngine;->mSyncRandomOffset:I
    :try_end_14a
    .catchall {:try_start_144 .. :try_end_14a} :catchall_1ea
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_144 .. :try_end_14a} :catch_14c
    .catch Ljava/io/IOException; {:try_start_144 .. :try_end_14a} :catch_188

    #@14a
    goto/16 :goto_8b

    #@14c
    .line 1559
    .end local v4           #e:Ljava/lang/NumberFormatException;
    .end local v5           #eventType:I
    .end local v9           #listen:Ljava/lang/String;
    .end local v10           #nextIdString:Ljava/lang/String;
    .end local v11           #offsetString:Ljava/lang/String;
    .end local v12           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v15           #tagName:Ljava/lang/String;
    .end local v16           #version:I
    .end local v17           #versionString:Ljava/lang/String;
    :catch_14c
    move-exception v4

    #@14d
    .line 1560
    .local v4, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_14d
    const-string v18, "SyncManager"

    #@14f
    const-string v19, "Error reading accounts"

    #@151
    move-object/from16 v0, v18

    #@153
    move-object/from16 v1, v19

    #@155
    invoke-static {v0, v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_158
    .catchall {:try_start_14d .. :try_end_158} :catchall_1ea

    #@158
    .line 1567
    add-int/lit8 v18, v7, 0x1

    #@15a
    move-object/from16 v0, p0

    #@15c
    iget v0, v0, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@15e
    move/from16 v19, v0

    #@160
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    #@163
    move-result v18

    #@164
    move/from16 v0, v18

    #@166
    move-object/from16 v1, p0

    #@168
    iput v0, v1, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@16a
    .line 1568
    if-eqz v6, :cond_129

    #@16c
    .line 1570
    :try_start_16c
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_16f
    .catch Ljava/io/IOException; {:try_start_16c .. :try_end_16f} :catch_170

    #@16f
    goto :goto_129

    #@170
    .line 1571
    .end local v4           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_170
    move-exception v18

    #@171
    goto :goto_129

    #@172
    .line 1529
    .restart local v5       #eventType:I
    .restart local v9       #listen:Ljava/lang/String;
    .restart local v10       #nextIdString:Ljava/lang/String;
    .restart local v11       #offsetString:Ljava/lang/String;
    .restart local v12       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v15       #tagName:Ljava/lang/String;
    .restart local v16       #version:I
    .restart local v17       #versionString:Ljava/lang/String;
    :cond_172
    const/16 v18, 0x0

    #@174
    goto/16 :goto_bf

    #@176
    .line 1543
    .restart local v3       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v13       #periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :cond_176
    :try_start_176
    const-string/jumbo v18, "listenForTickles"

    #@179
    move-object/from16 v0, v18

    #@17b
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17e
    move-result v18

    #@17f
    if-eqz v18, :cond_105

    #@181
    .line 1544
    move-object/from16 v0, p0

    #@183
    invoke-direct {v0, v12}, Landroid/content/SyncStorageEngine;->parseListenForTickles(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_186
    .catchall {:try_start_176 .. :try_end_186} :catchall_1ea
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_176 .. :try_end_186} :catch_14c
    .catch Ljava/io/IOException; {:try_start_176 .. :try_end_186} :catch_188

    #@186
    goto/16 :goto_105

    #@188
    .line 1562
    .end local v3           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v5           #eventType:I
    .end local v9           #listen:Ljava/lang/String;
    .end local v10           #nextIdString:Ljava/lang/String;
    .end local v11           #offsetString:Ljava/lang/String;
    .end local v12           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v13           #periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    .end local v15           #tagName:Ljava/lang/String;
    .end local v16           #version:I
    .end local v17           #versionString:Ljava/lang/String;
    :catch_188
    move-exception v4

    #@189
    .line 1563
    .local v4, e:Ljava/io/IOException;
    if-nez v6, :cond_203

    #@18b
    :try_start_18b
    const-string v18, "SyncManager"

    #@18d
    const-string v19, "No initial accounts"

    #@18f
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_192
    .catchall {:try_start_18b .. :try_end_192} :catchall_1ea

    #@192
    .line 1567
    :goto_192
    add-int/lit8 v18, v7, 0x1

    #@194
    move-object/from16 v0, p0

    #@196
    iget v0, v0, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@198
    move/from16 v19, v0

    #@19a
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    #@19d
    move-result v18

    #@19e
    move/from16 v0, v18

    #@1a0
    move-object/from16 v1, p0

    #@1a2
    iput v0, v1, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@1a4
    .line 1568
    if-eqz v6, :cond_129

    #@1a6
    .line 1570
    :try_start_1a6
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_1a9
    .catch Ljava/io/IOException; {:try_start_1a6 .. :try_end_1a9} :catch_170

    #@1a9
    goto :goto_129

    #@1aa
    .line 1546
    .end local v4           #e:Ljava/io/IOException;
    .restart local v3       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v5       #eventType:I
    .restart local v9       #listen:Ljava/lang/String;
    .restart local v10       #nextIdString:Ljava/lang/String;
    .restart local v11       #offsetString:Ljava/lang/String;
    .restart local v12       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v13       #periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    .restart local v15       #tagName:Ljava/lang/String;
    .restart local v16       #version:I
    .restart local v17       #versionString:Ljava/lang/String;
    :cond_1aa
    :try_start_1aa
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@1ad
    move-result v18

    #@1ae
    const/16 v19, 0x3

    #@1b0
    move/from16 v0, v18

    #@1b2
    move/from16 v1, v19

    #@1b4
    if-ne v0, v1, :cond_1cb

    #@1b6
    .line 1547
    const-string/jumbo v18, "periodicSync"

    #@1b9
    move-object/from16 v0, v18

    #@1bb
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1be
    move-result v18

    #@1bf
    if-eqz v18, :cond_105

    #@1c1
    if-eqz v3, :cond_105

    #@1c3
    .line 1548
    move-object/from16 v0, p0

    #@1c5
    invoke-direct {v0, v12, v3}, Landroid/content/SyncStorageEngine;->parsePeriodicSync(Lorg/xmlpull/v1/XmlPullParser;Landroid/content/SyncStorageEngine$AuthorityInfo;)Landroid/util/Pair;

    #@1c8
    move-result-object v13

    #@1c9
    goto/16 :goto_105

    #@1cb
    .line 1550
    :cond_1cb
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@1ce
    move-result v18

    #@1cf
    const/16 v19, 0x4

    #@1d1
    move/from16 v0, v18

    #@1d3
    move/from16 v1, v19

    #@1d5
    if-ne v0, v1, :cond_105

    #@1d7
    if-eqz v13, :cond_105

    #@1d9
    .line 1551
    const-string v18, "extra"

    #@1db
    move-object/from16 v0, v18

    #@1dd
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e0
    move-result v18

    #@1e1
    if-eqz v18, :cond_105

    #@1e3
    .line 1552
    move-object/from16 v0, p0

    #@1e5
    invoke-direct {v0, v12, v13}, Landroid/content/SyncStorageEngine;->parseExtra(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/Pair;)V
    :try_end_1e8
    .catchall {:try_start_1aa .. :try_end_1e8} :catchall_1ea
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1aa .. :try_end_1e8} :catch_14c
    .catch Ljava/io/IOException; {:try_start_1aa .. :try_end_1e8} :catch_188

    #@1e8
    goto/16 :goto_105

    #@1ea
    .line 1567
    .end local v3           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v5           #eventType:I
    .end local v9           #listen:Ljava/lang/String;
    .end local v10           #nextIdString:Ljava/lang/String;
    .end local v11           #offsetString:Ljava/lang/String;
    .end local v12           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v13           #periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    .end local v15           #tagName:Ljava/lang/String;
    .end local v16           #version:I
    .end local v17           #versionString:Ljava/lang/String;
    :catchall_1ea
    move-exception v18

    #@1eb
    add-int/lit8 v19, v7, 0x1

    #@1ed
    move-object/from16 v0, p0

    #@1ef
    iget v0, v0, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@1f1
    move/from16 v20, v0

    #@1f3
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    #@1f6
    move-result v19

    #@1f7
    move/from16 v0, v19

    #@1f9
    move-object/from16 v1, p0

    #@1fb
    iput v0, v1, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@1fd
    .line 1568
    if-eqz v6, :cond_202

    #@1ff
    .line 1570
    :try_start_1ff
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_202
    .catch Ljava/io/IOException; {:try_start_1ff .. :try_end_202} :catch_20f

    #@202
    .line 1567
    :cond_202
    :goto_202
    throw v18

    #@203
    .line 1564
    .restart local v4       #e:Ljava/io/IOException;
    :cond_203
    :try_start_203
    const-string v18, "SyncManager"

    #@205
    const-string v19, "Error reading accounts"

    #@207
    move-object/from16 v0, v18

    #@209
    move-object/from16 v1, v19

    #@20b
    invoke-static {v0, v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_20e
    .catchall {:try_start_203 .. :try_end_20e} :catchall_1ea

    #@20e
    goto :goto_192

    #@20f
    .line 1571
    .end local v4           #e:Ljava/io/IOException;
    :catch_20f
    move-exception v19

    #@210
    goto :goto_202

    #@211
    .restart local v5       #eventType:I
    .restart local v12       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v15       #tagName:Ljava/lang/String;
    :catch_211
    move-exception v18

    #@212
    goto/16 :goto_126

    #@214
    .line 1516
    .restart local v9       #listen:Ljava/lang/String;
    .restart local v10       #nextIdString:Ljava/lang/String;
    .restart local v16       #version:I
    .restart local v17       #versionString:Ljava/lang/String;
    :catch_214
    move-exception v18

    #@215
    goto/16 :goto_74
.end method

.method private readAndDeleteLegacyAccountInfoLocked()V
    .registers 30

    #@0
    .prologue
    .line 1863
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Landroid/content/SyncStorageEngine;->mContext:Landroid/content/Context;

    #@4
    const-string/jumbo v4, "syncmanager.db"

    #@7
    invoke-virtual {v3, v4}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    #@a
    move-result-object v19

    #@b
    .line 1864
    .local v19, file:Ljava/io/File;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    #@e
    move-result v3

    #@f
    if-nez v3, :cond_12

    #@11
    .line 1980
    :cond_11
    :goto_11
    return-void

    #@12
    .line 1867
    :cond_12
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@15
    move-result-object v25

    #@16
    .line 1868
    .local v25, path:Ljava/lang/String;
    const/4 v2, 0x0

    #@17
    .line 1870
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x1

    #@19
    :try_start_19
    move-object/from16 v0, v25

    #@1b
    invoke-static {v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1e
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_19 .. :try_end_1e} :catch_302

    #@1e
    move-result-object v2

    #@1f
    .line 1875
    :goto_1f
    if-eqz v2, :cond_11

    #@21
    .line 1876
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    #@24
    move-result v3

    #@25
    const/16 v4, 0xb

    #@27
    if-lt v3, v4, :cond_234

    #@29
    const/16 v21, 0x1

    #@2b
    .line 1880
    .local v21, hasType:Z
    :goto_2b
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@2d
    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@30
    .line 1881
    .local v1, qb:Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string/jumbo v3, "stats, status"

    #@33
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@36
    .line 1882
    new-instance v23, Ljava/util/HashMap;

    #@38
    invoke-direct/range {v23 .. v23}, Ljava/util/HashMap;-><init>()V

    #@3b
    .line 1883
    .local v23, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "_id"

    #@3d
    const-string/jumbo v4, "status._id as _id"

    #@40
    move-object/from16 v0, v23

    #@42
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@45
    .line 1884
    const-string v3, "account"

    #@47
    const-string/jumbo v4, "stats.account as account"

    #@4a
    move-object/from16 v0, v23

    #@4c
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4f
    .line 1885
    if-eqz v21, :cond_5b

    #@51
    .line 1886
    const-string v3, "account_type"

    #@53
    const-string/jumbo v4, "stats.account_type as account_type"

    #@56
    move-object/from16 v0, v23

    #@58
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    .line 1888
    :cond_5b
    const-string v3, "authority"

    #@5d
    const-string/jumbo v4, "stats.authority as authority"

    #@60
    move-object/from16 v0, v23

    #@62
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@65
    .line 1889
    const-string/jumbo v3, "totalElapsedTime"

    #@68
    const-string/jumbo v4, "totalElapsedTime"

    #@6b
    move-object/from16 v0, v23

    #@6d
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@70
    .line 1890
    const-string/jumbo v3, "numSyncs"

    #@73
    const-string/jumbo v4, "numSyncs"

    #@76
    move-object/from16 v0, v23

    #@78
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7b
    .line 1891
    const-string/jumbo v3, "numSourceLocal"

    #@7e
    const-string/jumbo v4, "numSourceLocal"

    #@81
    move-object/from16 v0, v23

    #@83
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@86
    .line 1892
    const-string/jumbo v3, "numSourcePoll"

    #@89
    const-string/jumbo v4, "numSourcePoll"

    #@8c
    move-object/from16 v0, v23

    #@8e
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@91
    .line 1893
    const-string/jumbo v3, "numSourceServer"

    #@94
    const-string/jumbo v4, "numSourceServer"

    #@97
    move-object/from16 v0, v23

    #@99
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9c
    .line 1894
    const-string/jumbo v3, "numSourceUser"

    #@9f
    const-string/jumbo v4, "numSourceUser"

    #@a2
    move-object/from16 v0, v23

    #@a4
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a7
    .line 1895
    const-string/jumbo v3, "lastSuccessSource"

    #@aa
    const-string/jumbo v4, "lastSuccessSource"

    #@ad
    move-object/from16 v0, v23

    #@af
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b2
    .line 1896
    const-string/jumbo v3, "lastSuccessTime"

    #@b5
    const-string/jumbo v4, "lastSuccessTime"

    #@b8
    move-object/from16 v0, v23

    #@ba
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@bd
    .line 1897
    const-string/jumbo v3, "lastFailureSource"

    #@c0
    const-string/jumbo v4, "lastFailureSource"

    #@c3
    move-object/from16 v0, v23

    #@c5
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c8
    .line 1898
    const-string/jumbo v3, "lastFailureTime"

    #@cb
    const-string/jumbo v4, "lastFailureTime"

    #@ce
    move-object/from16 v0, v23

    #@d0
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d3
    .line 1899
    const-string/jumbo v3, "lastFailureMesg"

    #@d6
    const-string/jumbo v4, "lastFailureMesg"

    #@d9
    move-object/from16 v0, v23

    #@db
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@de
    .line 1900
    const-string/jumbo v3, "pending"

    #@e1
    const-string/jumbo v4, "pending"

    #@e4
    move-object/from16 v0, v23

    #@e6
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e9
    .line 1901
    move-object/from16 v0, v23

    #@eb
    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@ee
    .line 1902
    const-string/jumbo v3, "stats._id = status.stats_id"

    #@f1
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@f4
    .line 1903
    const/4 v3, 0x0

    #@f5
    const/4 v4, 0x0

    #@f6
    const/4 v5, 0x0

    #@f7
    const/4 v6, 0x0

    #@f8
    const/4 v7, 0x0

    #@f9
    const/4 v8, 0x0

    #@fa
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@fd
    move-result-object v18

    #@fe
    .line 1904
    .local v18, c:Landroid/database/Cursor;
    :cond_fe
    :goto_fe
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    #@101
    move-result v3

    #@102
    if-eqz v3, :cond_23e

    #@104
    .line 1905
    const-string v3, "account"

    #@106
    move-object/from16 v0, v18

    #@108
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@10b
    move-result v3

    #@10c
    move-object/from16 v0, v18

    #@10e
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@111
    move-result-object v15

    #@112
    .line 1906
    .local v15, accountName:Ljava/lang/String;
    if-eqz v21, :cond_238

    #@114
    const-string v3, "account_type"

    #@116
    move-object/from16 v0, v18

    #@118
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@11b
    move-result v3

    #@11c
    move-object/from16 v0, v18

    #@11e
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@121
    move-result-object v16

    #@122
    .line 1908
    .local v16, accountType:Ljava/lang/String;
    :goto_122
    if-nez v16, :cond_126

    #@124
    .line 1909
    const-string v16, "com.google"

    #@126
    .line 1911
    :cond_126
    const-string v3, "authority"

    #@128
    move-object/from16 v0, v18

    #@12a
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@12d
    move-result v3

    #@12e
    move-object/from16 v0, v18

    #@130
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@133
    move-result-object v6

    #@134
    .line 1912
    .local v6, authorityName:Ljava/lang/String;
    new-instance v4, Landroid/accounts/Account;

    #@136
    move-object/from16 v0, v16

    #@138
    invoke-direct {v4, v15, v0}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@13b
    const/4 v5, 0x0

    #@13c
    const/4 v7, -0x1

    #@13d
    const/4 v8, 0x0

    #@13e
    move-object/from16 v3, p0

    #@140
    invoke-direct/range {v3 .. v8}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@143
    move-result-object v17

    #@144
    .line 1915
    .local v17, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v17, :cond_fe

    #@146
    .line 1916
    move-object/from16 v0, p0

    #@148
    iget-object v3, v0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@14a
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@14d
    move-result v22

    #@14e
    .line 1917
    .local v22, i:I
    const/16 v20, 0x0

    #@150
    .line 1918
    .local v20, found:Z
    const/16 v27, 0x0

    #@152
    .line 1919
    .local v27, st:Landroid/content/SyncStatusInfo;
    :cond_152
    if-lez v22, :cond_16e

    #@154
    .line 1920
    add-int/lit8 v22, v22, -0x1

    #@156
    .line 1921
    move-object/from16 v0, p0

    #@158
    iget-object v3, v0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@15a
    move/from16 v0, v22

    #@15c
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@15f
    move-result-object v27

    #@160
    .end local v27           #st:Landroid/content/SyncStatusInfo;
    check-cast v27, Landroid/content/SyncStatusInfo;

    #@162
    .line 1922
    .restart local v27       #st:Landroid/content/SyncStatusInfo;
    move-object/from16 v0, v27

    #@164
    iget v3, v0, Landroid/content/SyncStatusInfo;->authorityId:I

    #@166
    move-object/from16 v0, v17

    #@168
    iget v4, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@16a
    if-ne v3, v4, :cond_152

    #@16c
    .line 1923
    const/16 v20, 0x1

    #@16e
    .line 1927
    :cond_16e
    if-nez v20, :cond_188

    #@170
    .line 1928
    new-instance v27, Landroid/content/SyncStatusInfo;

    #@172
    .end local v27           #st:Landroid/content/SyncStatusInfo;
    move-object/from16 v0, v17

    #@174
    iget v3, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@176
    move-object/from16 v0, v27

    #@178
    invoke-direct {v0, v3}, Landroid/content/SyncStatusInfo;-><init>(I)V

    #@17b
    .line 1929
    .restart local v27       #st:Landroid/content/SyncStatusInfo;
    move-object/from16 v0, p0

    #@17d
    iget-object v3, v0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@17f
    move-object/from16 v0, v17

    #@181
    iget v4, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@183
    move-object/from16 v0, v27

    #@185
    invoke-virtual {v3, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@188
    .line 1931
    :cond_188
    const-string/jumbo v3, "totalElapsedTime"

    #@18b
    move-object/from16 v0, v18

    #@18d
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getLongColumn(Landroid/database/Cursor;Ljava/lang/String;)J

    #@190
    move-result-wide v3

    #@191
    move-object/from16 v0, v27

    #@193
    iput-wide v3, v0, Landroid/content/SyncStatusInfo;->totalElapsedTime:J

    #@195
    .line 1932
    const-string/jumbo v3, "numSyncs"

    #@198
    move-object/from16 v0, v18

    #@19a
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getIntColumn(Landroid/database/Cursor;Ljava/lang/String;)I

    #@19d
    move-result v3

    #@19e
    move-object/from16 v0, v27

    #@1a0
    iput v3, v0, Landroid/content/SyncStatusInfo;->numSyncs:I

    #@1a2
    .line 1933
    const-string/jumbo v3, "numSourceLocal"

    #@1a5
    move-object/from16 v0, v18

    #@1a7
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getIntColumn(Landroid/database/Cursor;Ljava/lang/String;)I

    #@1aa
    move-result v3

    #@1ab
    move-object/from16 v0, v27

    #@1ad
    iput v3, v0, Landroid/content/SyncStatusInfo;->numSourceLocal:I

    #@1af
    .line 1934
    const-string/jumbo v3, "numSourcePoll"

    #@1b2
    move-object/from16 v0, v18

    #@1b4
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getIntColumn(Landroid/database/Cursor;Ljava/lang/String;)I

    #@1b7
    move-result v3

    #@1b8
    move-object/from16 v0, v27

    #@1ba
    iput v3, v0, Landroid/content/SyncStatusInfo;->numSourcePoll:I

    #@1bc
    .line 1935
    const-string/jumbo v3, "numSourceServer"

    #@1bf
    move-object/from16 v0, v18

    #@1c1
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getIntColumn(Landroid/database/Cursor;Ljava/lang/String;)I

    #@1c4
    move-result v3

    #@1c5
    move-object/from16 v0, v27

    #@1c7
    iput v3, v0, Landroid/content/SyncStatusInfo;->numSourceServer:I

    #@1c9
    .line 1936
    const-string/jumbo v3, "numSourceUser"

    #@1cc
    move-object/from16 v0, v18

    #@1ce
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getIntColumn(Landroid/database/Cursor;Ljava/lang/String;)I

    #@1d1
    move-result v3

    #@1d2
    move-object/from16 v0, v27

    #@1d4
    iput v3, v0, Landroid/content/SyncStatusInfo;->numSourceUser:I

    #@1d6
    .line 1937
    const/4 v3, 0x0

    #@1d7
    move-object/from16 v0, v27

    #@1d9
    iput v3, v0, Landroid/content/SyncStatusInfo;->numSourcePeriodic:I

    #@1db
    .line 1938
    const-string/jumbo v3, "lastSuccessSource"

    #@1de
    move-object/from16 v0, v18

    #@1e0
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getIntColumn(Landroid/database/Cursor;Ljava/lang/String;)I

    #@1e3
    move-result v3

    #@1e4
    move-object/from16 v0, v27

    #@1e6
    iput v3, v0, Landroid/content/SyncStatusInfo;->lastSuccessSource:I

    #@1e8
    .line 1939
    const-string/jumbo v3, "lastSuccessTime"

    #@1eb
    move-object/from16 v0, v18

    #@1ed
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getLongColumn(Landroid/database/Cursor;Ljava/lang/String;)J

    #@1f0
    move-result-wide v3

    #@1f1
    move-object/from16 v0, v27

    #@1f3
    iput-wide v3, v0, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    #@1f5
    .line 1940
    const-string/jumbo v3, "lastFailureSource"

    #@1f8
    move-object/from16 v0, v18

    #@1fa
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getIntColumn(Landroid/database/Cursor;Ljava/lang/String;)I

    #@1fd
    move-result v3

    #@1fe
    move-object/from16 v0, v27

    #@200
    iput v3, v0, Landroid/content/SyncStatusInfo;->lastFailureSource:I

    #@202
    .line 1941
    const-string/jumbo v3, "lastFailureTime"

    #@205
    move-object/from16 v0, v18

    #@207
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getLongColumn(Landroid/database/Cursor;Ljava/lang/String;)J

    #@20a
    move-result-wide v3

    #@20b
    move-object/from16 v0, v27

    #@20d
    iput-wide v3, v0, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    #@20f
    .line 1942
    const-string/jumbo v3, "lastFailureMesg"

    #@212
    move-object/from16 v0, v18

    #@214
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@217
    move-result v3

    #@218
    move-object/from16 v0, v18

    #@21a
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@21d
    move-result-object v3

    #@21e
    move-object/from16 v0, v27

    #@220
    iput-object v3, v0, Landroid/content/SyncStatusInfo;->lastFailureMesg:Ljava/lang/String;

    #@222
    .line 1943
    const-string/jumbo v3, "pending"

    #@225
    move-object/from16 v0, v18

    #@227
    invoke-static {v0, v3}, Landroid/content/SyncStorageEngine;->getIntColumn(Landroid/database/Cursor;Ljava/lang/String;)I

    #@22a
    move-result v3

    #@22b
    if-eqz v3, :cond_23c

    #@22d
    const/4 v3, 0x1

    #@22e
    :goto_22e
    move-object/from16 v0, v27

    #@230
    iput-boolean v3, v0, Landroid/content/SyncStatusInfo;->pending:Z

    #@232
    goto/16 :goto_fe

    #@234
    .line 1876
    .end local v1           #qb:Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v6           #authorityName:Ljava/lang/String;
    .end local v15           #accountName:Ljava/lang/String;
    .end local v16           #accountType:Ljava/lang/String;
    .end local v17           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v18           #c:Landroid/database/Cursor;
    .end local v20           #found:Z
    .end local v21           #hasType:Z
    .end local v22           #i:I
    .end local v23           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v27           #st:Landroid/content/SyncStatusInfo;
    :cond_234
    const/16 v21, 0x0

    #@236
    goto/16 :goto_2b

    #@238
    .line 1906
    .restart local v1       #qb:Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v15       #accountName:Ljava/lang/String;
    .restart local v18       #c:Landroid/database/Cursor;
    .restart local v21       #hasType:Z
    .restart local v23       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_238
    const/16 v16, 0x0

    #@23a
    goto/16 :goto_122

    #@23c
    .line 1943
    .restart local v6       #authorityName:Ljava/lang/String;
    .restart local v16       #accountType:Ljava/lang/String;
    .restart local v17       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v20       #found:Z
    .restart local v22       #i:I
    .restart local v27       #st:Landroid/content/SyncStatusInfo;
    :cond_23c
    const/4 v3, 0x0

    #@23d
    goto :goto_22e

    #@23e
    .line 1947
    .end local v6           #authorityName:Ljava/lang/String;
    .end local v15           #accountName:Ljava/lang/String;
    .end local v16           #accountType:Ljava/lang/String;
    .end local v17           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v20           #found:Z
    .end local v22           #i:I
    .end local v27           #st:Landroid/content/SyncStatusInfo;
    :cond_23e
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    #@241
    .line 1950
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@243
    .end local v1           #qb:Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@246
    .line 1951
    .restart local v1       #qb:Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string/jumbo v3, "settings"

    #@249
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@24c
    .line 1952
    const/4 v9, 0x0

    #@24d
    const/4 v10, 0x0

    #@24e
    const/4 v11, 0x0

    #@24f
    const/4 v12, 0x0

    #@250
    const/4 v13, 0x0

    #@251
    const/4 v14, 0x0

    #@252
    move-object v7, v1

    #@253
    move-object v8, v2

    #@254
    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@257
    move-result-object v18

    #@258
    .line 1953
    :cond_258
    :goto_258
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    #@25b
    move-result v3

    #@25c
    if-eqz v3, :cond_2f0

    #@25e
    .line 1954
    const-string/jumbo v3, "name"

    #@261
    move-object/from16 v0, v18

    #@263
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@266
    move-result v3

    #@267
    move-object/from16 v0, v18

    #@269
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@26c
    move-result-object v24

    #@26d
    .line 1955
    .local v24, name:Ljava/lang/String;
    const-string/jumbo v3, "value"

    #@270
    move-object/from16 v0, v18

    #@272
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@275
    move-result v3

    #@276
    move-object/from16 v0, v18

    #@278
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@27b
    move-result-object v28

    #@27c
    .line 1956
    .local v28, value:Ljava/lang/String;
    if-eqz v24, :cond_258

    #@27e
    .line 1957
    const-string/jumbo v3, "listen_for_tickles"

    #@281
    move-object/from16 v0, v24

    #@283
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@286
    move-result v3

    #@287
    if-eqz v3, :cond_29b

    #@289
    .line 1958
    if-eqz v28, :cond_291

    #@28b
    invoke-static/range {v28 .. v28}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@28e
    move-result v3

    #@28f
    if-eqz v3, :cond_299

    #@291
    :cond_291
    const/4 v3, 0x1

    #@292
    :goto_292
    const/4 v4, 0x0

    #@293
    move-object/from16 v0, p0

    #@295
    invoke-virtual {v0, v3, v4}, Landroid/content/SyncStorageEngine;->setMasterSyncAutomatically(ZI)V

    #@298
    goto :goto_258

    #@299
    :cond_299
    const/4 v3, 0x0

    #@29a
    goto :goto_292

    #@29b
    .line 1959
    :cond_29b
    const-string/jumbo v3, "sync_provider_"

    #@29e
    move-object/from16 v0, v24

    #@2a0
    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2a3
    move-result v3

    #@2a4
    if-eqz v3, :cond_258

    #@2a6
    .line 1960
    const-string/jumbo v3, "sync_provider_"

    #@2a9
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@2ac
    move-result v3

    #@2ad
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    #@2b0
    move-result v4

    #@2b1
    move-object/from16 v0, v24

    #@2b3
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2b6
    move-result-object v26

    #@2b7
    .line 1962
    .local v26, provider:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2b9
    iget-object v3, v0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2bb
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@2be
    move-result v22

    #@2bf
    .line 1963
    .restart local v22       #i:I
    :cond_2bf
    :goto_2bf
    if-lez v22, :cond_258

    #@2c1
    .line 1964
    add-int/lit8 v22, v22, -0x1

    #@2c3
    .line 1965
    move-object/from16 v0, p0

    #@2c5
    iget-object v3, v0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2c7
    move/from16 v0, v22

    #@2c9
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@2cc
    move-result-object v17

    #@2cd
    check-cast v17, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@2cf
    .line 1966
    .restart local v17       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    move-object/from16 v0, v17

    #@2d1
    iget-object v3, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@2d3
    move-object/from16 v0, v26

    #@2d5
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d8
    move-result v3

    #@2d9
    if-eqz v3, :cond_2bf

    #@2db
    .line 1967
    if-eqz v28, :cond_2e3

    #@2dd
    invoke-static/range {v28 .. v28}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@2e0
    move-result v3

    #@2e1
    if-eqz v3, :cond_2ee

    #@2e3
    :cond_2e3
    const/4 v3, 0x1

    #@2e4
    :goto_2e4
    move-object/from16 v0, v17

    #@2e6
    iput-boolean v3, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@2e8
    .line 1968
    const/4 v3, 0x1

    #@2e9
    move-object/from16 v0, v17

    #@2eb
    iput v3, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@2ed
    goto :goto_2bf

    #@2ee
    .line 1967
    :cond_2ee
    const/4 v3, 0x0

    #@2ef
    goto :goto_2e4

    #@2f0
    .line 1974
    .end local v17           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v22           #i:I
    .end local v24           #name:Ljava/lang/String;
    .end local v26           #provider:Ljava/lang/String;
    .end local v28           #value:Ljava/lang/String;
    :cond_2f0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    #@2f3
    .line 1976
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@2f6
    .line 1978
    new-instance v3, Ljava/io/File;

    #@2f8
    move-object/from16 v0, v25

    #@2fa
    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@2fd
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@300
    goto/16 :goto_11

    #@302
    .line 1872
    .end local v1           #qb:Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v18           #c:Landroid/database/Cursor;
    .end local v21           #hasType:Z
    .end local v23           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_302
    move-exception v3

    #@303
    goto/16 :goto_1f
.end method

.method private readPendingOperationsLocked()V
    .registers 16

    #@0
    .prologue
    .line 2057
    :try_start_0
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mPendingFile:Landroid/util/AtomicFile;

    #@2
    invoke-virtual {v1}, Landroid/util/AtomicFile;->readFully()[B

    #@5
    move-result-object v10

    #@6
    .line 2058
    .local v10, data:[B
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v13

    #@a
    .line 2059
    .local v13, in:Landroid/os/Parcel;
    const/4 v1, 0x0

    #@b
    array-length v2, v10

    #@c
    invoke-virtual {v13, v10, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    #@f
    .line 2060
    const/4 v1, 0x0

    #@10
    invoke-virtual {v13, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@13
    .line 2061
    invoke-virtual {v13}, Landroid/os/Parcel;->dataSize()I

    #@16
    move-result v7

    #@17
    .line 2062
    .local v7, SIZE:I
    :cond_17
    :goto_17
    invoke-virtual {v13}, Landroid/os/Parcel;->dataPosition()I

    #@1a
    move-result v1

    #@1b
    if-ge v1, v7, :cond_45

    #@1d
    .line 2063
    invoke-virtual {v13}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v14

    #@21
    .line 2064
    .local v14, version:I
    const/4 v1, 0x2

    #@22
    if-eq v14, v1, :cond_46

    #@24
    const/4 v1, 0x1

    #@25
    if-eq v14, v1, :cond_46

    #@27
    .line 2065
    const-string v1, "SyncManager"

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "Unknown pending operation version "

    #@30
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    const-string v4, "; dropping all ops"

    #@3a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 2104
    .end local v7           #SIZE:I
    .end local v10           #data:[B
    .end local v13           #in:Landroid/os/Parcel;
    .end local v14           #version:I
    :cond_45
    :goto_45
    return-void

    #@46
    .line 2069
    .restart local v7       #SIZE:I
    .restart local v10       #data:[B
    .restart local v13       #in:Landroid/os/Parcel;
    .restart local v14       #version:I
    :cond_46
    invoke-virtual {v13}, Landroid/os/Parcel;->readInt()I

    #@49
    move-result v9

    #@4a
    .line 2070
    .local v9, authorityId:I
    invoke-virtual {v13}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v3

    #@4e
    .line 2071
    .local v3, syncSource:I
    invoke-virtual {v13}, Landroid/os/Parcel;->createByteArray()[B

    #@51
    move-result-object v12

    #@52
    .line 2073
    .local v12, flatExtras:[B
    const/4 v1, 0x2

    #@53
    if-ne v14, v1, :cond_8c

    #@55
    .line 2074
    invoke-virtual {v13}, Landroid/os/Parcel;->readInt()I

    #@58
    move-result v1

    #@59
    if-eqz v1, :cond_8a

    #@5b
    const/4 v6, 0x1

    #@5c
    .line 2078
    .local v6, expedited:Z
    :goto_5c
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@5e
    invoke-virtual {v1, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@61
    move-result-object v8

    #@62
    check-cast v8, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@64
    .line 2079
    .local v8, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v8, :cond_17

    #@66
    .line 2081
    if-eqz v12, :cond_8e

    #@68
    .line 2082
    invoke-static {v12}, Landroid/content/SyncStorageEngine;->unflattenBundle([B)Landroid/os/Bundle;

    #@6b
    move-result-object v5

    #@6c
    .line 2088
    .local v5, extras:Landroid/os/Bundle;
    :goto_6c
    new-instance v0, Landroid/content/SyncStorageEngine$PendingOperation;

    #@6e
    iget-object v1, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@70
    iget v2, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@72
    iget-object v4, v8, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@74
    invoke-direct/range {v0 .. v6}, Landroid/content/SyncStorageEngine$PendingOperation;-><init>(Landroid/accounts/Account;IILjava/lang/String;Landroid/os/Bundle;Z)V

    #@77
    .line 2091
    .local v0, op:Landroid/content/SyncStorageEngine$PendingOperation;
    iput v9, v0, Landroid/content/SyncStorageEngine$PendingOperation;->authorityId:I

    #@79
    .line 2092
    iput-object v12, v0, Landroid/content/SyncStorageEngine$PendingOperation;->flatExtras:[B

    #@7b
    .line 2098
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@7d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_80
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_80} :catch_81

    #@80
    goto :goto_17

    #@81
    .line 2101
    .end local v0           #op:Landroid/content/SyncStorageEngine$PendingOperation;
    .end local v3           #syncSource:I
    .end local v5           #extras:Landroid/os/Bundle;
    .end local v6           #expedited:Z
    .end local v7           #SIZE:I
    .end local v8           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v9           #authorityId:I
    .end local v10           #data:[B
    .end local v12           #flatExtras:[B
    .end local v13           #in:Landroid/os/Parcel;
    .end local v14           #version:I
    :catch_81
    move-exception v11

    #@82
    .line 2102
    .local v11, e:Ljava/io/IOException;
    const-string v1, "SyncManager"

    #@84
    const-string v2, "No initial pending operations"

    #@86
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    goto :goto_45

    #@8a
    .line 2074
    .end local v11           #e:Ljava/io/IOException;
    .restart local v3       #syncSource:I
    .restart local v7       #SIZE:I
    .restart local v9       #authorityId:I
    .restart local v10       #data:[B
    .restart local v12       #flatExtras:[B
    .restart local v13       #in:Landroid/os/Parcel;
    .restart local v14       #version:I
    :cond_8a
    const/4 v6, 0x0

    #@8b
    goto :goto_5c

    #@8c
    .line 2076
    :cond_8c
    const/4 v6, 0x0

    #@8d
    .restart local v6       #expedited:Z
    goto :goto_5c

    #@8e
    .line 2086
    .restart local v8       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_8e
    :try_start_8e
    new-instance v5, Landroid/os/Bundle;

    #@90
    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V
    :try_end_93
    .catch Ljava/io/IOException; {:try_start_8e .. :try_end_93} :catch_81

    #@93
    .restart local v5       #extras:Landroid/os/Bundle;
    goto :goto_6c
.end method

.method private readStatisticsLocked()V
    .registers 11

    #@0
    .prologue
    const/16 v9, 0x64

    #@2
    .line 2231
    :try_start_2
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mStatisticsFile:Landroid/util/AtomicFile;

    #@4
    invoke-virtual {v7}, Landroid/util/AtomicFile;->readFully()[B

    #@7
    move-result-object v0

    #@8
    .line 2232
    .local v0, data:[B
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@b
    move-result-object v4

    #@c
    .line 2233
    .local v4, in:Landroid/os/Parcel;
    const/4 v7, 0x0

    #@d
    array-length v8, v0

    #@e
    invoke-virtual {v4, v0, v7, v8}, Landroid/os/Parcel;->unmarshall([BII)V

    #@11
    .line 2234
    const/4 v7, 0x0

    #@12
    invoke-virtual {v4, v7}, Landroid/os/Parcel;->setDataPosition(I)V

    #@15
    .line 2236
    const/4 v5, 0x0

    #@16
    .line 2237
    .local v5, index:I
    :cond_16
    :goto_16
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    #@19
    move-result v6

    #@1a
    .local v6, token:I
    if-eqz v6, :cond_6d

    #@1c
    .line 2238
    const/16 v7, 0x65

    #@1e
    if-eq v6, v7, :cond_22

    #@20
    if-ne v6, v9, :cond_55

    #@22
    .line 2240
    :cond_22
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v1

    #@26
    .line 2241
    .local v1, day:I
    if-ne v6, v9, :cond_2c

    #@28
    .line 2242
    add-int/lit16 v7, v1, -0x7d9

    #@2a
    add-int/lit16 v1, v7, 0x37a5

    #@2c
    .line 2244
    :cond_2c
    new-instance v2, Landroid/content/SyncStorageEngine$DayStats;

    #@2e
    invoke-direct {v2, v1}, Landroid/content/SyncStorageEngine$DayStats;-><init>(I)V

    #@31
    .line 2245
    .local v2, ds:Landroid/content/SyncStorageEngine$DayStats;
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v7

    #@35
    iput v7, v2, Landroid/content/SyncStorageEngine$DayStats;->successCount:I

    #@37
    .line 2246
    invoke-virtual {v4}, Landroid/os/Parcel;->readLong()J

    #@3a
    move-result-wide v7

    #@3b
    iput-wide v7, v2, Landroid/content/SyncStorageEngine$DayStats;->successTime:J

    #@3d
    .line 2247
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    #@40
    move-result v7

    #@41
    iput v7, v2, Landroid/content/SyncStorageEngine$DayStats;->failureCount:I

    #@43
    .line 2248
    invoke-virtual {v4}, Landroid/os/Parcel;->readLong()J

    #@46
    move-result-wide v7

    #@47
    iput-wide v7, v2, Landroid/content/SyncStorageEngine$DayStats;->failureTime:J

    #@49
    .line 2249
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@4b
    array-length v7, v7

    #@4c
    if-ge v5, v7, :cond_16

    #@4e
    .line 2250
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@50
    aput-object v2, v7, v5

    #@52
    .line 2251
    add-int/lit8 v5, v5, 0x1

    #@54
    goto :goto_16

    #@55
    .line 2255
    .end local v1           #day:I
    .end local v2           #ds:Landroid/content/SyncStorageEngine$DayStats;
    :cond_55
    const-string v7, "SyncManager"

    #@57
    new-instance v8, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v9, "Unknown stats token: "

    #@5e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v8

    #@62
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v8

    #@66
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v8

    #@6a
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6d
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_6d} :catch_6e

    #@6d
    .line 2262
    .end local v0           #data:[B
    .end local v4           #in:Landroid/os/Parcel;
    .end local v5           #index:I
    .end local v6           #token:I
    :cond_6d
    :goto_6d
    return-void

    #@6e
    .line 2259
    :catch_6e
    move-exception v3

    #@6f
    .line 2260
    .local v3, e:Ljava/io/IOException;
    const-string v7, "SyncManager"

    #@71
    const-string v8, "No initial statistics"

    #@73
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    goto :goto_6d
.end method

.method private readStatusLocked()V
    .registers 9

    #@0
    .prologue
    .line 1991
    :try_start_0
    iget-object v5, p0, Landroid/content/SyncStorageEngine;->mStatusFile:Landroid/util/AtomicFile;

    #@2
    invoke-virtual {v5}, Landroid/util/AtomicFile;->readFully()[B

    #@5
    move-result-object v0

    #@6
    .line 1992
    .local v0, data:[B
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v2

    #@a
    .line 1993
    .local v2, in:Landroid/os/Parcel;
    const/4 v5, 0x0

    #@b
    array-length v6, v0

    #@c
    invoke-virtual {v2, v0, v5, v6}, Landroid/os/Parcel;->unmarshall([BII)V

    #@f
    .line 1994
    const/4 v5, 0x0

    #@10
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    #@13
    .line 1996
    :cond_13
    :goto_13
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v4

    #@17
    .local v4, token:I
    if-eqz v4, :cond_3f

    #@19
    .line 1997
    const/16 v5, 0x64

    #@1b
    if-ne v4, v5, :cond_40

    #@1d
    .line 1998
    new-instance v3, Landroid/content/SyncStatusInfo;

    #@1f
    invoke-direct {v3, v2}, Landroid/content/SyncStatusInfo;-><init>(Landroid/os/Parcel;)V

    #@22
    .line 1999
    .local v3, status:Landroid/content/SyncStatusInfo;
    iget-object v5, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@24
    iget v6, v3, Landroid/content/SyncStatusInfo;->authorityId:I

    #@26
    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->indexOfKey(I)I

    #@29
    move-result v5

    #@2a
    if-ltz v5, :cond_13

    #@2c
    .line 2000
    const/4 v5, 0x0

    #@2d
    iput-boolean v5, v3, Landroid/content/SyncStatusInfo;->pending:Z

    #@2f
    .line 2003
    iget-object v5, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@31
    iget v6, v3, Landroid/content/SyncStatusInfo;->authorityId:I

    #@33
    invoke-virtual {v5, v6, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_36} :catch_37

    #@36
    goto :goto_13

    #@37
    .line 2011
    .end local v0           #data:[B
    .end local v2           #in:Landroid/os/Parcel;
    .end local v3           #status:Landroid/content/SyncStatusInfo;
    .end local v4           #token:I
    :catch_37
    move-exception v1

    #@38
    .line 2012
    .local v1, e:Ljava/io/IOException;
    const-string v5, "SyncManager"

    #@3a
    const-string v6, "No initial status"

    #@3c
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 2014
    .end local v1           #e:Ljava/io/IOException;
    :cond_3f
    :goto_3f
    return-void

    #@40
    .line 2007
    .restart local v0       #data:[B
    .restart local v2       #in:Landroid/os/Parcel;
    .restart local v4       #token:I
    :cond_40
    :try_start_40
    const-string v5, "SyncManager"

    #@42
    new-instance v6, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v7, "Unknown status token: "

    #@49
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v6

    #@51
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v6

    #@55
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_58
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_58} :catch_37

    #@58
    goto :goto_3f
.end method

.method private removeAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Z)V
    .registers 9
    .parameter "account"
    .parameter "userId"
    .parameter "authorityName"
    .parameter "doWrite"

    #@0
    .prologue
    .line 1422
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mAccounts:Ljava/util/HashMap;

    #@2
    new-instance v3, Landroid/accounts/AccountAndUser;

    #@4
    invoke-direct {v3, p1, p2}, Landroid/accounts/AccountAndUser;-><init>(Landroid/accounts/Account;I)V

    #@7
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/content/SyncStorageEngine$AccountInfo;

    #@d
    .line 1423
    .local v0, accountInfo:Landroid/content/SyncStorageEngine$AccountInfo;
    if-eqz v0, :cond_25

    #@f
    .line 1424
    iget-object v2, v0, Landroid/content/SyncStorageEngine$AccountInfo;->authorities:Ljava/util/HashMap;

    #@11
    invoke-virtual {v2, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@17
    .line 1425
    .local v1, authorityInfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v1, :cond_25

    #@19
    .line 1426
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@1b
    iget v3, v1, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@1d
    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->remove(I)V

    #@20
    .line 1427
    if-eqz p4, :cond_25

    #@22
    .line 1428
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@25
    .line 1432
    .end local v1           #authorityInfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_25
    return-void
.end method

.method private reportChange(I)V
    .registers 8
    .parameter "which"

    #@0
    .prologue
    .line 430
    const/4 v2, 0x0

    #@1
    .line 431
    .local v2, reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    iget-object v5, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@3
    monitor-enter v5

    #@4
    .line 432
    :try_start_4
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mChangeListeners:Landroid/os/RemoteCallbackList;

    #@6
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_4c

    #@9
    move-result v0

    #@a
    .local v0, i:I
    move-object v3, v2

    #@b
    .line 433
    .end local v2           #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    .local v3, reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    :cond_b
    :goto_b
    if-lez v0, :cond_30

    #@d
    .line 434
    add-int/lit8 v0, v0, -0x1

    #@f
    .line 435
    :try_start_f
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mChangeListeners:Landroid/os/RemoteCallbackList;

    #@11
    invoke-virtual {v4, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Ljava/lang/Integer;

    #@17
    .line 436
    .local v1, mask:Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@1a
    move-result v4

    #@1b
    and-int/2addr v4, p1

    #@1c
    if-eqz v4, :cond_b

    #@1e
    .line 439
    if-nez v3, :cond_53

    #@20
    .line 440
    new-instance v2, Ljava/util/ArrayList;

    #@22
    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_25
    .catchall {:try_start_f .. :try_end_25} :catchall_50

    #@25
    .line 442
    .end local v3           #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    .restart local v2       #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    :goto_25
    :try_start_25
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mChangeListeners:Landroid/os/RemoteCallbackList;

    #@27
    invoke-virtual {v4, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2e
    .catchall {:try_start_25 .. :try_end_2e} :catchall_4c

    #@2e
    move-object v3, v2

    #@2f
    .line 443
    .end local v2           #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    .restart local v3       #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    goto :goto_b

    #@30
    .line 444
    .end local v1           #mask:Ljava/lang/Integer;
    :cond_30
    :try_start_30
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mChangeListeners:Landroid/os/RemoteCallbackList;

    #@32
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@35
    .line 445
    monitor-exit v5
    :try_end_36
    .catchall {:try_start_30 .. :try_end_36} :catchall_50

    #@36
    .line 451
    if-eqz v3, :cond_4f

    #@38
    .line 452
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v0

    #@3c
    .line 453
    :goto_3c
    if-lez v0, :cond_4f

    #@3e
    .line 454
    add-int/lit8 v0, v0, -0x1

    #@40
    .line 456
    :try_start_40
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@43
    move-result-object v4

    #@44
    check-cast v4, Landroid/content/ISyncStatusObserver;

    #@46
    invoke-interface {v4, p1}, Landroid/content/ISyncStatusObserver;->onStatusChanged(I)V
    :try_end_49
    .catch Landroid/os/RemoteException; {:try_start_40 .. :try_end_49} :catch_4a

    #@49
    goto :goto_3c

    #@4a
    .line 457
    :catch_4a
    move-exception v4

    #@4b
    goto :goto_3c

    #@4c
    .line 445
    .end local v0           #i:I
    .end local v3           #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    .restart local v2       #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    :catchall_4c
    move-exception v4

    #@4d
    :goto_4d
    :try_start_4d
    monitor-exit v5
    :try_end_4e
    .catchall {:try_start_4d .. :try_end_4e} :catchall_4c

    #@4e
    throw v4

    #@4f
    .line 462
    .end local v2           #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    .restart local v0       #i:I
    .restart local v3       #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    :cond_4f
    return-void

    #@50
    .line 445
    :catchall_50
    move-exception v4

    #@51
    move-object v2, v3

    #@52
    .end local v3           #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    .restart local v2       #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    goto :goto_4d

    #@53
    .end local v2           #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    .restart local v1       #mask:Ljava/lang/Integer;
    .restart local v3       #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    :cond_53
    move-object v2, v3

    #@54
    .end local v3           #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    .restart local v2       #reports:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ISyncStatusObserver;>;"
    goto :goto_25
.end method

.method private requestSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 7
    .parameter "account"
    .parameter "userId"
    .parameter "authority"
    .parameter "extras"

    #@0
    .prologue
    .line 2214
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x3e8

    #@6
    if-ne v0, v1, :cond_12

    #@8
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mSyncRequestListener:Landroid/content/SyncStorageEngine$OnSyncRequestListener;

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 2216
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mSyncRequestListener:Landroid/content/SyncStorageEngine$OnSyncRequestListener;

    #@e
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/content/SyncStorageEngine$OnSyncRequestListener;->onSyncRequest(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V

    #@11
    .line 2220
    :goto_11
    return-void

    #@12
    .line 2218
    :cond_12
    invoke-static {p1, p3, p4}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    #@15
    goto :goto_11
.end method

.method private static unflattenBundle([B)Landroid/os/Bundle;
    .registers 6
    .parameter "flatData"

    #@0
    .prologue
    .line 2194
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v2

    #@4
    .line 2196
    .local v2, parcel:Landroid/os/Parcel;
    const/4 v3, 0x0

    #@5
    :try_start_5
    array-length v4, p0

    #@6
    invoke-virtual {v2, p0, v3, v4}, Landroid/os/Parcel;->unmarshall([BII)V

    #@9
    .line 2197
    const/4 v3, 0x0

    #@a
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    #@d
    .line 2198
    invoke-virtual {v2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;
    :try_end_10
    .catchall {:try_start_5 .. :try_end_10} :catchall_1c
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_10} :catch_15

    #@10
    move-result-object v0

    #@11
    .line 2204
    .local v0, bundle:Landroid/os/Bundle;
    :goto_11
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 2206
    return-object v0

    #@15
    .line 2199
    .end local v0           #bundle:Landroid/os/Bundle;
    :catch_15
    move-exception v1

    #@16
    .line 2202
    .local v1, e:Ljava/lang/RuntimeException;
    :try_start_16
    new-instance v0, Landroid/os/Bundle;

    #@18
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V
    :try_end_1b
    .catchall {:try_start_16 .. :try_end_1b} :catchall_1c

    #@1b
    .restart local v0       #bundle:Landroid/os/Bundle;
    goto :goto_11

    #@1c
    .line 2204
    .end local v0           #bundle:Landroid/os/Bundle;
    .end local v1           #e:Ljava/lang/RuntimeException;
    :catchall_1c
    move-exception v3

    #@1d
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@20
    throw v3
.end method

.method private updateOrRemovePeriodicSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;JZ)V
    .registers 25
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"
    .parameter "extras"
    .parameter "period"
    .parameter "add"

    #@0
    .prologue
    .line 683
    const-wide/16 v1, 0x0

    #@2
    cmp-long v1, p5, v1

    #@4
    if-gtz v1, :cond_8

    #@6
    .line 684
    const-wide/16 p5, 0x0

    #@8
    .line 686
    :cond_8
    if-nez p4, :cond_f

    #@a
    .line 687
    new-instance p4, Landroid/os/Bundle;

    #@c
    .end local p4
    invoke-direct/range {p4 .. p4}, Landroid/os/Bundle;-><init>()V

    #@f
    .line 694
    .restart local p4
    :cond_f
    move-object/from16 v0, p0

    #@11
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@13
    move-object/from16 v16, v0

    #@15
    monitor-enter v16

    #@16
    .line 696
    const/4 v5, -0x1

    #@17
    const/4 v6, 0x0

    #@18
    move-object/from16 v1, p0

    #@1a
    move-object/from16 v2, p1

    #@1c
    move/from16 v3, p2

    #@1e
    move-object/from16 v4, p3

    #@20
    :try_start_20
    invoke-direct/range {v1 .. v6}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@23
    move-result-object v9

    #@24
    .line 698
    .local v9, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz p7, :cond_9f

    #@26
    .line 701
    const/4 v8, 0x0

    #@27
    .line 702
    .local v8, alreadyPresent:Z
    const/4 v12, 0x0

    #@28
    .local v12, i:I
    iget-object v1, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@2d
    move-result v7

    #@2e
    .local v7, N:I
    :goto_2e
    if-ge v12, v7, :cond_68

    #@30
    .line 703
    iget-object v1, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@32
    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@35
    move-result-object v15

    #@36
    check-cast v15, Landroid/util/Pair;

    #@38
    .line 704
    .local v15, syncInfo:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    iget-object v11, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@3a
    check-cast v11, Landroid/os/Bundle;

    #@3c
    .line 705
    .local v11, existingExtras:Landroid/os/Bundle;
    move-object/from16 v0, p4

    #@3e
    invoke-static {v11, v0}, Landroid/content/SyncStorageEngine;->equals(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    #@41
    move-result v1

    #@42
    if-eqz v1, :cond_9c

    #@44
    .line 706
    iget-object v1, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@46
    check-cast v1, Ljava/lang/Long;

    #@48
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_4b
    .catchall {:try_start_20 .. :try_end_4b} :catchall_d5

    #@4b
    move-result-wide v1

    #@4c
    cmp-long v1, v1, p5

    #@4e
    if-nez v1, :cond_58

    #@50
    .line 746
    :try_start_50
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@53
    .line 747
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->writeStatusLocked()V

    #@56
    .line 707
    monitor-exit v16
    :try_end_57
    .catchall {:try_start_50 .. :try_end_57} :catchall_dd

    #@57
    .line 752
    .end local v7           #N:I
    .end local v8           #alreadyPresent:Z
    .end local v11           #existingExtras:Landroid/os/Bundle;
    .end local v15           #syncInfo:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :goto_57
    return-void

    #@58
    .line 709
    .restart local v7       #N:I
    .restart local v8       #alreadyPresent:Z
    .restart local v11       #existingExtras:Landroid/os/Bundle;
    .restart local v15       #syncInfo:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :cond_58
    :try_start_58
    iget-object v1, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@5a
    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5d
    move-result-object v2

    #@5e
    move-object/from16 v0, p4

    #@60
    invoke-static {v0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@63
    move-result-object v2

    #@64
    invoke-virtual {v1, v12, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@67
    .line 710
    const/4 v8, 0x1

    #@68
    .line 716
    .end local v11           #existingExtras:Landroid/os/Bundle;
    .end local v15           #syncInfo:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :cond_68
    if-nez v8, :cond_8e

    #@6a
    .line 717
    iget-object v1, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@6c
    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@6f
    move-result-object v2

    #@70
    move-object/from16 v0, p4

    #@72
    invoke-static {v0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@75
    move-result-object v2

    #@76
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@79
    .line 718
    iget v1, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@7b
    move-object/from16 v0, p0

    #@7d
    invoke-direct {v0, v1}, Landroid/content/SyncStorageEngine;->getOrCreateSyncStatusLocked(I)Landroid/content/SyncStatusInfo;

    #@80
    move-result-object v14

    #@81
    .line 719
    .local v14, status:Landroid/content/SyncStatusInfo;
    iget-object v1, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@83
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@86
    move-result v1

    #@87
    add-int/lit8 v1, v1, -0x1

    #@89
    const-wide/16 v2, 0x0

    #@8b
    invoke-virtual {v14, v1, v2, v3}, Landroid/content/SyncStatusInfo;->setPeriodicSyncTime(IJ)V
    :try_end_8e
    .catchall {:try_start_58 .. :try_end_8e} :catchall_d5

    #@8e
    .line 746
    .end local v7           #N:I
    .end local v8           #alreadyPresent:Z
    .end local v14           #status:Landroid/content/SyncStatusInfo;
    :cond_8e
    :try_start_8e
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@91
    .line 747
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->writeStatusLocked()V

    #@94
    .line 749
    monitor-exit v16
    :try_end_95
    .catchall {:try_start_8e .. :try_end_95} :catchall_dd

    #@95
    .line 751
    const/4 v1, 0x1

    #@96
    move-object/from16 v0, p0

    #@98
    invoke-direct {v0, v1}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@9b
    goto :goto_57

    #@9c
    .line 702
    .restart local v7       #N:I
    .restart local v8       #alreadyPresent:Z
    .restart local v11       #existingExtras:Landroid/os/Bundle;
    .restart local v15       #syncInfo:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :cond_9c
    add-int/lit8 v12, v12, 0x1

    #@9e
    goto :goto_2e

    #@9f
    .line 723
    .end local v7           #N:I
    .end local v8           #alreadyPresent:Z
    .end local v11           #existingExtras:Landroid/os/Bundle;
    .end local v12           #i:I
    .end local v15           #syncInfo:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :cond_9f
    :try_start_9f
    move-object/from16 v0, p0

    #@a1
    iget-object v1, v0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@a3
    iget v2, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@a5
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@a8
    move-result-object v14

    #@a9
    check-cast v14, Landroid/content/SyncStatusInfo;

    #@ab
    .line 724
    .restart local v14       #status:Landroid/content/SyncStatusInfo;
    const/4 v10, 0x0

    #@ac
    .line 725
    .local v10, changed:Z
    iget-object v1, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@ae
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@b1
    move-result-object v13

    #@b2
    .line 726
    .local v13, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;>;"
    const/4 v12, 0x0

    #@b3
    .line 727
    .restart local v12       #i:I
    :cond_b3
    :goto_b3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@b6
    move-result v1

    #@b7
    if-eqz v1, :cond_e3

    #@b9
    .line 728
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@bc
    move-result-object v15

    #@bd
    check-cast v15, Landroid/util/Pair;

    #@bf
    .line 729
    .restart local v15       #syncInfo:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    iget-object v1, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@c1
    check-cast v1, Landroid/os/Bundle;

    #@c3
    move-object/from16 v0, p4

    #@c5
    invoke-static {v1, v0}, Landroid/content/SyncStorageEngine;->equals(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    #@c8
    move-result v1

    #@c9
    if-eqz v1, :cond_e0

    #@cb
    .line 730
    invoke-interface {v13}, Ljava/util/Iterator;->remove()V

    #@ce
    .line 731
    const/4 v10, 0x1

    #@cf
    .line 734
    if-eqz v14, :cond_b3

    #@d1
    .line 735
    invoke-virtual {v14, v12}, Landroid/content/SyncStatusInfo;->removePeriodicSyncTime(I)V
    :try_end_d4
    .catchall {:try_start_9f .. :try_end_d4} :catchall_d5

    #@d4
    goto :goto_b3

    #@d5
    .line 746
    .end local v9           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v10           #changed:Z
    .end local v12           #i:I
    .end local v13           #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;>;"
    .end local v14           #status:Landroid/content/SyncStatusInfo;
    .end local v15           #syncInfo:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :catchall_d5
    move-exception v1

    #@d6
    :try_start_d6
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@d9
    .line 747
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->writeStatusLocked()V

    #@dc
    .line 746
    throw v1

    #@dd
    .line 749
    :catchall_dd
    move-exception v1

    #@de
    monitor-exit v16
    :try_end_df
    .catchall {:try_start_d6 .. :try_end_df} :catchall_dd

    #@df
    throw v1

    #@e0
    .line 738
    .restart local v9       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v10       #changed:Z
    .restart local v12       #i:I
    .restart local v13       #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;>;"
    .restart local v14       #status:Landroid/content/SyncStatusInfo;
    .restart local v15       #syncInfo:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :cond_e0
    add-int/lit8 v12, v12, 0x1

    #@e2
    goto :goto_b3

    #@e3
    .line 741
    .end local v15           #syncInfo:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :cond_e3
    if-nez v10, :cond_8e

    #@e5
    .line 746
    :try_start_e5
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@e8
    .line 747
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->writeStatusLocked()V

    #@eb
    .line 742
    monitor-exit v16
    :try_end_ec
    .catchall {:try_start_e5 .. :try_end_ec} :catchall_dd

    #@ec
    goto/16 :goto_57
.end method

.method private writeAccountInfoLocked()V
    .registers 25

    #@0
    .prologue
    .line 1757
    const/4 v8, 0x0

    #@1
    .line 1760
    .local v8, fos:Ljava/io/FileOutputStream;
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mAccountInfoFile:Landroid/util/AtomicFile;

    #@5
    move-object/from16 v19, v0

    #@7
    invoke-virtual/range {v19 .. v19}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@a
    move-result-object v8

    #@b
    .line 1761
    new-instance v15, Lcom/android/internal/util/FastXmlSerializer;

    #@d
    invoke-direct {v15}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@10
    .line 1762
    .local v15, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string/jumbo v19, "utf-8"

    #@13
    move-object/from16 v0, v19

    #@15
    invoke-interface {v15, v8, v0}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@18
    .line 1763
    const/16 v19, 0x0

    #@1a
    const/16 v20, 0x1

    #@1c
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@1f
    move-result-object v20

    #@20
    move-object/from16 v0, v19

    #@22
    move-object/from16 v1, v20

    #@24
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@27
    .line 1764
    const-string v19, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@29
    const/16 v20, 0x1

    #@2b
    move-object/from16 v0, v19

    #@2d
    move/from16 v1, v20

    #@2f
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@32
    .line 1766
    const/16 v19, 0x0

    #@34
    const-string v20, "accounts"

    #@36
    move-object/from16 v0, v19

    #@38
    move-object/from16 v1, v20

    #@3a
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3d
    .line 1767
    const/16 v19, 0x0

    #@3f
    const-string/jumbo v20, "version"

    #@42
    const/16 v21, 0x2

    #@44
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@47
    move-result-object v21

    #@48
    move-object/from16 v0, v19

    #@4a
    move-object/from16 v1, v20

    #@4c
    move-object/from16 v2, v21

    #@4e
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@51
    .line 1768
    const/16 v19, 0x0

    #@53
    const-string/jumbo v20, "nextAuthorityId"

    #@56
    move-object/from16 v0, p0

    #@58
    iget v0, v0, Landroid/content/SyncStorageEngine;->mNextAuthorityId:I

    #@5a
    move/from16 v21, v0

    #@5c
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5f
    move-result-object v21

    #@60
    move-object/from16 v0, v19

    #@62
    move-object/from16 v1, v20

    #@64
    move-object/from16 v2, v21

    #@66
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@69
    .line 1769
    const/16 v19, 0x0

    #@6b
    const-string/jumbo v20, "offsetInSeconds"

    #@6e
    move-object/from16 v0, p0

    #@70
    iget v0, v0, Landroid/content/SyncStorageEngine;->mSyncRandomOffset:I

    #@72
    move/from16 v21, v0

    #@74
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@77
    move-result-object v21

    #@78
    move-object/from16 v0, v19

    #@7a
    move-object/from16 v1, v20

    #@7c
    move-object/from16 v2, v21

    #@7e
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@81
    .line 1772
    move-object/from16 v0, p0

    #@83
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mMasterSyncAutomatically:Landroid/util/SparseArray;

    #@85
    move-object/from16 v19, v0

    #@87
    invoke-virtual/range {v19 .. v19}, Landroid/util/SparseArray;->size()I

    #@8a
    move-result v3

    #@8b
    .line 1773
    .local v3, M:I
    const/4 v14, 0x0

    #@8c
    .local v14, m:I
    :goto_8c
    if-ge v14, v3, :cond_ea

    #@8e
    .line 1774
    move-object/from16 v0, p0

    #@90
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mMasterSyncAutomatically:Landroid/util/SparseArray;

    #@92
    move-object/from16 v19, v0

    #@94
    move-object/from16 v0, v19

    #@96
    invoke-virtual {v0, v14}, Landroid/util/SparseArray;->keyAt(I)I

    #@99
    move-result v17

    #@9a
    .line 1775
    .local v17, userId:I
    move-object/from16 v0, p0

    #@9c
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mMasterSyncAutomatically:Landroid/util/SparseArray;

    #@9e
    move-object/from16 v19, v0

    #@a0
    move-object/from16 v0, v19

    #@a2
    invoke-virtual {v0, v14}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@a5
    move-result-object v13

    #@a6
    check-cast v13, Ljava/lang/Boolean;

    #@a8
    .line 1776
    .local v13, listen:Ljava/lang/Boolean;
    const/16 v19, 0x0

    #@aa
    const-string/jumbo v20, "listenForTickles"

    #@ad
    move-object/from16 v0, v19

    #@af
    move-object/from16 v1, v20

    #@b1
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@b4
    .line 1777
    const/16 v19, 0x0

    #@b6
    const-string/jumbo v20, "user"

    #@b9
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@bc
    move-result-object v21

    #@bd
    move-object/from16 v0, v19

    #@bf
    move-object/from16 v1, v20

    #@c1
    move-object/from16 v2, v21

    #@c3
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c6
    .line 1778
    const/16 v19, 0x0

    #@c8
    const-string v20, "enabled"

    #@ca
    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    #@cd
    move-result v21

    #@ce
    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@d1
    move-result-object v21

    #@d2
    move-object/from16 v0, v19

    #@d4
    move-object/from16 v1, v20

    #@d6
    move-object/from16 v2, v21

    #@d8
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@db
    .line 1779
    const/16 v19, 0x0

    #@dd
    const-string/jumbo v20, "listenForTickles"

    #@e0
    move-object/from16 v0, v19

    #@e2
    move-object/from16 v1, v20

    #@e4
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@e7
    .line 1773
    add-int/lit8 v14, v14, 0x1

    #@e9
    goto :goto_8c

    #@ea
    .line 1782
    .end local v13           #listen:Ljava/lang/Boolean;
    .end local v17           #userId:I
    :cond_ea
    move-object/from16 v0, p0

    #@ec
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@ee
    move-object/from16 v19, v0

    #@f0
    invoke-virtual/range {v19 .. v19}, Landroid/util/SparseArray;->size()I

    #@f3
    move-result v4

    #@f4
    .line 1783
    .local v4, N:I
    const/4 v9, 0x0

    #@f5
    .local v9, i:I
    :goto_f5
    if-ge v9, v4, :cond_3cd

    #@f7
    .line 1784
    move-object/from16 v0, p0

    #@f9
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@fb
    move-object/from16 v19, v0

    #@fd
    move-object/from16 v0, v19

    #@ff
    invoke-virtual {v0, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@102
    move-result-object v5

    #@103
    check-cast v5, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@105
    .line 1785
    .local v5, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    const/16 v19, 0x0

    #@107
    const-string v20, "authority"

    #@109
    move-object/from16 v0, v19

    #@10b
    move-object/from16 v1, v20

    #@10d
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@110
    .line 1786
    const/16 v19, 0x0

    #@112
    const-string v20, "id"

    #@114
    iget v0, v5, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@116
    move/from16 v21, v0

    #@118
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@11b
    move-result-object v21

    #@11c
    move-object/from16 v0, v19

    #@11e
    move-object/from16 v1, v20

    #@120
    move-object/from16 v2, v21

    #@122
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@125
    .line 1787
    const/16 v19, 0x0

    #@127
    const-string v20, "account"

    #@129
    iget-object v0, v5, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@12b
    move-object/from16 v21, v0

    #@12d
    move-object/from16 v0, v21

    #@12f
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@131
    move-object/from16 v21, v0

    #@133
    move-object/from16 v0, v19

    #@135
    move-object/from16 v1, v20

    #@137
    move-object/from16 v2, v21

    #@139
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@13c
    .line 1788
    const/16 v19, 0x0

    #@13e
    const-string/jumbo v20, "user"

    #@141
    iget v0, v5, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@143
    move/from16 v21, v0

    #@145
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@148
    move-result-object v21

    #@149
    move-object/from16 v0, v19

    #@14b
    move-object/from16 v1, v20

    #@14d
    move-object/from16 v2, v21

    #@14f
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@152
    .line 1789
    const/16 v19, 0x0

    #@154
    const-string/jumbo v20, "type"

    #@157
    iget-object v0, v5, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@159
    move-object/from16 v21, v0

    #@15b
    move-object/from16 v0, v21

    #@15d
    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@15f
    move-object/from16 v21, v0

    #@161
    move-object/from16 v0, v19

    #@163
    move-object/from16 v1, v20

    #@165
    move-object/from16 v2, v21

    #@167
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@16a
    .line 1790
    const/16 v19, 0x0

    #@16c
    const-string v20, "authority"

    #@16e
    iget-object v0, v5, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@170
    move-object/from16 v21, v0

    #@172
    move-object/from16 v0, v19

    #@174
    move-object/from16 v1, v20

    #@176
    move-object/from16 v2, v21

    #@178
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@17b
    .line 1791
    const/16 v19, 0x0

    #@17d
    const-string v20, "enabled"

    #@17f
    iget-boolean v0, v5, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@181
    move/from16 v21, v0

    #@183
    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@186
    move-result-object v21

    #@187
    move-object/from16 v0, v19

    #@189
    move-object/from16 v1, v20

    #@18b
    move-object/from16 v2, v21

    #@18d
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@190
    .line 1792
    iget v0, v5, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@192
    move/from16 v19, v0

    #@194
    if-gez v19, :cond_26b

    #@196
    .line 1793
    const/16 v19, 0x0

    #@198
    const-string/jumbo v20, "syncable"

    #@19b
    const-string/jumbo v21, "unknown"

    #@19e
    move-object/from16 v0, v19

    #@1a0
    move-object/from16 v1, v20

    #@1a2
    move-object/from16 v2, v21

    #@1a4
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1a7
    .line 1797
    :goto_1a7
    iget-object v0, v5, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@1a9
    move-object/from16 v19, v0

    #@1ab
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1ae
    move-result-object v10

    #@1af
    :goto_1af
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@1b2
    move-result v19

    #@1b3
    if-eqz v19, :cond_3be

    #@1b5
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b8
    move-result-object v16

    #@1b9
    check-cast v16, Landroid/util/Pair;

    #@1bb
    .line 1798
    .local v16, periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    const/16 v19, 0x0

    #@1bd
    const-string/jumbo v20, "periodicSync"

    #@1c0
    move-object/from16 v0, v19

    #@1c2
    move-object/from16 v1, v20

    #@1c4
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1c7
    .line 1799
    const/16 v20, 0x0

    #@1c9
    const-string/jumbo v21, "period"

    #@1cc
    move-object/from16 v0, v16

    #@1ce
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@1d0
    move-object/from16 v19, v0

    #@1d2
    check-cast v19, Ljava/lang/Long;

    #@1d4
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    #@1d7
    move-result-wide v22

    #@1d8
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@1db
    move-result-object v19

    #@1dc
    move-object/from16 v0, v20

    #@1de
    move-object/from16 v1, v21

    #@1e0
    move-object/from16 v2, v19

    #@1e2
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1e5
    .line 1800
    move-object/from16 v0, v16

    #@1e7
    iget-object v7, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@1e9
    check-cast v7, Landroid/os/Bundle;

    #@1eb
    .line 1801
    .local v7, extras:Landroid/os/Bundle;
    invoke-virtual {v7}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@1ee
    move-result-object v19

    #@1ef
    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1f2
    move-result-object v11

    #@1f3
    .local v11, i$:Ljava/util/Iterator;
    :goto_1f3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@1f6
    move-result v19

    #@1f7
    if-eqz v19, :cond_3b0

    #@1f9
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1fc
    move-result-object v12

    #@1fd
    check-cast v12, Ljava/lang/String;

    #@1ff
    .line 1802
    .local v12, key:Ljava/lang/String;
    const/16 v19, 0x0

    #@201
    const-string v20, "extra"

    #@203
    move-object/from16 v0, v19

    #@205
    move-object/from16 v1, v20

    #@207
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@20a
    .line 1803
    const/16 v19, 0x0

    #@20c
    const-string/jumbo v20, "name"

    #@20f
    move-object/from16 v0, v19

    #@211
    move-object/from16 v1, v20

    #@213
    invoke-interface {v15, v0, v1, v12}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@216
    .line 1804
    invoke-virtual {v7, v12}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@219
    move-result-object v18

    #@21a
    .line 1805
    .local v18, value:Ljava/lang/Object;
    move-object/from16 v0, v18

    #@21c
    instance-of v0, v0, Ljava/lang/Long;

    #@21e
    move/from16 v19, v0

    #@220
    if-eqz v19, :cond_28a

    #@222
    .line 1806
    const/16 v19, 0x0

    #@224
    const-string/jumbo v20, "type"

    #@227
    const-string/jumbo v21, "long"

    #@22a
    move-object/from16 v0, v19

    #@22c
    move-object/from16 v1, v20

    #@22e
    move-object/from16 v2, v21

    #@230
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@233
    .line 1807
    const/16 v19, 0x0

    #@235
    const-string/jumbo v20, "value1"

    #@238
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@23b
    move-result-object v21

    #@23c
    move-object/from16 v0, v19

    #@23e
    move-object/from16 v1, v20

    #@240
    move-object/from16 v2, v21

    #@242
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@245
    .line 1828
    .end local v18           #value:Ljava/lang/Object;
    :cond_245
    :goto_245
    const/16 v19, 0x0

    #@247
    const-string v20, "extra"

    #@249
    move-object/from16 v0, v19

    #@24b
    move-object/from16 v1, v20

    #@24d
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_250
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_250} :catch_251

    #@250
    goto :goto_1f3

    #@251
    .line 1840
    .end local v3           #M:I
    .end local v4           #N:I
    .end local v5           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v7           #extras:Landroid/os/Bundle;
    .end local v9           #i:I
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v12           #key:Ljava/lang/String;
    .end local v14           #m:I
    .end local v15           #out:Lorg/xmlpull/v1/XmlSerializer;
    .end local v16           #periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :catch_251
    move-exception v6

    #@252
    .line 1841
    .local v6, e1:Ljava/io/IOException;
    const-string v19, "SyncManager"

    #@254
    const-string v20, "Error writing accounts"

    #@256
    move-object/from16 v0, v19

    #@258
    move-object/from16 v1, v20

    #@25a
    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25d
    .line 1842
    if-eqz v8, :cond_26a

    #@25f
    .line 1843
    move-object/from16 v0, p0

    #@261
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mAccountInfoFile:Landroid/util/AtomicFile;

    #@263
    move-object/from16 v19, v0

    #@265
    move-object/from16 v0, v19

    #@267
    invoke-virtual {v0, v8}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@26a
    .line 1846
    .end local v6           #e1:Ljava/io/IOException;
    :cond_26a
    :goto_26a
    return-void

    #@26b
    .line 1795
    .restart local v3       #M:I
    .restart local v4       #N:I
    .restart local v5       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v9       #i:I
    .restart local v14       #m:I
    .restart local v15       #out:Lorg/xmlpull/v1/XmlSerializer;
    :cond_26b
    const/16 v20, 0x0

    #@26d
    :try_start_26d
    const-string/jumbo v21, "syncable"

    #@270
    iget v0, v5, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@272
    move/from16 v19, v0

    #@274
    if-eqz v19, :cond_287

    #@276
    const/16 v19, 0x1

    #@278
    :goto_278
    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@27b
    move-result-object v19

    #@27c
    move-object/from16 v0, v20

    #@27e
    move-object/from16 v1, v21

    #@280
    move-object/from16 v2, v19

    #@282
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@285
    goto/16 :goto_1a7

    #@287
    :cond_287
    const/16 v19, 0x0

    #@289
    goto :goto_278

    #@28a
    .line 1808
    .restart local v7       #extras:Landroid/os/Bundle;
    .restart local v11       #i$:Ljava/util/Iterator;
    .restart local v12       #key:Ljava/lang/String;
    .restart local v16       #periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    .restart local v18       #value:Ljava/lang/Object;
    :cond_28a
    move-object/from16 v0, v18

    #@28c
    instance-of v0, v0, Ljava/lang/Integer;

    #@28e
    move/from16 v19, v0

    #@290
    if-eqz v19, :cond_2b5

    #@292
    .line 1809
    const/16 v19, 0x0

    #@294
    const-string/jumbo v20, "type"

    #@297
    const-string v21, "integer"

    #@299
    move-object/from16 v0, v19

    #@29b
    move-object/from16 v1, v20

    #@29d
    move-object/from16 v2, v21

    #@29f
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2a2
    .line 1810
    const/16 v19, 0x0

    #@2a4
    const-string/jumbo v20, "value1"

    #@2a7
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2aa
    move-result-object v21

    #@2ab
    move-object/from16 v0, v19

    #@2ad
    move-object/from16 v1, v20

    #@2af
    move-object/from16 v2, v21

    #@2b1
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2b4
    goto :goto_245

    #@2b5
    .line 1811
    :cond_2b5
    move-object/from16 v0, v18

    #@2b7
    instance-of v0, v0, Ljava/lang/Boolean;

    #@2b9
    move/from16 v19, v0

    #@2bb
    if-eqz v19, :cond_2e1

    #@2bd
    .line 1812
    const/16 v19, 0x0

    #@2bf
    const-string/jumbo v20, "type"

    #@2c2
    const-string v21, "boolean"

    #@2c4
    move-object/from16 v0, v19

    #@2c6
    move-object/from16 v1, v20

    #@2c8
    move-object/from16 v2, v21

    #@2ca
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2cd
    .line 1813
    const/16 v19, 0x0

    #@2cf
    const-string/jumbo v20, "value1"

    #@2d2
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2d5
    move-result-object v21

    #@2d6
    move-object/from16 v0, v19

    #@2d8
    move-object/from16 v1, v20

    #@2da
    move-object/from16 v2, v21

    #@2dc
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2df
    goto/16 :goto_245

    #@2e1
    .line 1814
    :cond_2e1
    move-object/from16 v0, v18

    #@2e3
    instance-of v0, v0, Ljava/lang/Float;

    #@2e5
    move/from16 v19, v0

    #@2e7
    if-eqz v19, :cond_30d

    #@2e9
    .line 1815
    const/16 v19, 0x0

    #@2eb
    const-string/jumbo v20, "type"

    #@2ee
    const-string v21, "float"

    #@2f0
    move-object/from16 v0, v19

    #@2f2
    move-object/from16 v1, v20

    #@2f4
    move-object/from16 v2, v21

    #@2f6
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2f9
    .line 1816
    const/16 v19, 0x0

    #@2fb
    const-string/jumbo v20, "value1"

    #@2fe
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@301
    move-result-object v21

    #@302
    move-object/from16 v0, v19

    #@304
    move-object/from16 v1, v20

    #@306
    move-object/from16 v2, v21

    #@308
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@30b
    goto/16 :goto_245

    #@30d
    .line 1817
    :cond_30d
    move-object/from16 v0, v18

    #@30f
    instance-of v0, v0, Ljava/lang/Double;

    #@311
    move/from16 v19, v0

    #@313
    if-eqz v19, :cond_339

    #@315
    .line 1818
    const/16 v19, 0x0

    #@317
    const-string/jumbo v20, "type"

    #@31a
    const-string v21, "double"

    #@31c
    move-object/from16 v0, v19

    #@31e
    move-object/from16 v1, v20

    #@320
    move-object/from16 v2, v21

    #@322
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@325
    .line 1819
    const/16 v19, 0x0

    #@327
    const-string/jumbo v20, "value1"

    #@32a
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@32d
    move-result-object v21

    #@32e
    move-object/from16 v0, v19

    #@330
    move-object/from16 v1, v20

    #@332
    move-object/from16 v2, v21

    #@334
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@337
    goto/16 :goto_245

    #@339
    .line 1820
    :cond_339
    move-object/from16 v0, v18

    #@33b
    instance-of v0, v0, Ljava/lang/String;

    #@33d
    move/from16 v19, v0

    #@33f
    if-eqz v19, :cond_366

    #@341
    .line 1821
    const/16 v19, 0x0

    #@343
    const-string/jumbo v20, "type"

    #@346
    const-string/jumbo v21, "string"

    #@349
    move-object/from16 v0, v19

    #@34b
    move-object/from16 v1, v20

    #@34d
    move-object/from16 v2, v21

    #@34f
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@352
    .line 1822
    const/16 v19, 0x0

    #@354
    const-string/jumbo v20, "value1"

    #@357
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@35a
    move-result-object v21

    #@35b
    move-object/from16 v0, v19

    #@35d
    move-object/from16 v1, v20

    #@35f
    move-object/from16 v2, v21

    #@361
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@364
    goto/16 :goto_245

    #@366
    .line 1823
    :cond_366
    move-object/from16 v0, v18

    #@368
    instance-of v0, v0, Landroid/accounts/Account;

    #@36a
    move/from16 v19, v0

    #@36c
    if-eqz v19, :cond_245

    #@36e
    .line 1824
    const/16 v19, 0x0

    #@370
    const-string/jumbo v20, "type"

    #@373
    const-string v21, "account"

    #@375
    move-object/from16 v0, v19

    #@377
    move-object/from16 v1, v20

    #@379
    move-object/from16 v2, v21

    #@37b
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@37e
    .line 1825
    const/16 v20, 0x0

    #@380
    const-string/jumbo v21, "value1"

    #@383
    move-object/from16 v0, v18

    #@385
    check-cast v0, Landroid/accounts/Account;

    #@387
    move-object/from16 v19, v0

    #@389
    move-object/from16 v0, v19

    #@38b
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@38d
    move-object/from16 v19, v0

    #@38f
    move-object/from16 v0, v20

    #@391
    move-object/from16 v1, v21

    #@393
    move-object/from16 v2, v19

    #@395
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@398
    .line 1826
    const/16 v19, 0x0

    #@39a
    const-string/jumbo v20, "value2"

    #@39d
    check-cast v18, Landroid/accounts/Account;

    #@39f
    .end local v18           #value:Ljava/lang/Object;
    move-object/from16 v0, v18

    #@3a1
    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@3a3
    move-object/from16 v21, v0

    #@3a5
    move-object/from16 v0, v19

    #@3a7
    move-object/from16 v1, v20

    #@3a9
    move-object/from16 v2, v21

    #@3ab
    invoke-interface {v15, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3ae
    goto/16 :goto_245

    #@3b0
    .line 1830
    .end local v12           #key:Ljava/lang/String;
    :cond_3b0
    const/16 v19, 0x0

    #@3b2
    const-string/jumbo v20, "periodicSync"

    #@3b5
    move-object/from16 v0, v19

    #@3b7
    move-object/from16 v1, v20

    #@3b9
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3bc
    goto/16 :goto_1af

    #@3be
    .line 1832
    .end local v7           #extras:Landroid/os/Bundle;
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v16           #periodicSync:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :cond_3be
    const/16 v19, 0x0

    #@3c0
    const-string v20, "authority"

    #@3c2
    move-object/from16 v0, v19

    #@3c4
    move-object/from16 v1, v20

    #@3c6
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3c9
    .line 1783
    add-int/lit8 v9, v9, 0x1

    #@3cb
    goto/16 :goto_f5

    #@3cd
    .line 1835
    .end local v5           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_3cd
    const/16 v19, 0x0

    #@3cf
    const-string v20, "accounts"

    #@3d1
    move-object/from16 v0, v19

    #@3d3
    move-object/from16 v1, v20

    #@3d5
    invoke-interface {v15, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3d8
    .line 1837
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@3db
    .line 1839
    move-object/from16 v0, p0

    #@3dd
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mAccountInfoFile:Landroid/util/AtomicFile;

    #@3df
    move-object/from16 v19, v0

    #@3e1
    move-object/from16 v0, v19

    #@3e3
    invoke-virtual {v0, v8}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_3e6
    .catch Ljava/io/IOException; {:try_start_26d .. :try_end_3e6} :catch_251

    #@3e6
    goto/16 :goto_26a
.end method

.method private writePendingOperationLocked(Landroid/content/SyncStorageEngine$PendingOperation;Landroid/os/Parcel;)V
    .registers 4
    .parameter "op"
    .parameter "out"

    #@0
    .prologue
    .line 2107
    const/4 v0, 0x2

    #@1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4
    .line 2108
    iget v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->authorityId:I

    #@6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 2109
    iget v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->syncSource:I

    #@b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 2110
    iget-object v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->flatExtras:[B

    #@10
    if-nez v0, :cond_1e

    #@12
    iget-object v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->extras:Landroid/os/Bundle;

    #@14
    if-eqz v0, :cond_1e

    #@16
    .line 2111
    iget-object v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->extras:Landroid/os/Bundle;

    #@18
    invoke-static {v0}, Landroid/content/SyncStorageEngine;->flattenBundle(Landroid/os/Bundle;)[B

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->flatExtras:[B

    #@1e
    .line 2113
    :cond_1e
    iget-object v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->flatExtras:[B

    #@20
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@23
    .line 2114
    iget-boolean v0, p1, Landroid/content/SyncStorageEngine$PendingOperation;->expedited:Z

    #@25
    if-eqz v0, :cond_2c

    #@27
    const/4 v0, 0x1

    #@28
    :goto_28
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 2115
    return-void

    #@2c
    .line 2114
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_28
.end method

.method private writePendingOperationsLocked()V
    .registers 9

    #@0
    .prologue
    .line 2121
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 2122
    .local v0, N:I
    const/4 v2, 0x0

    #@7
    .line 2124
    .local v2, fos:Ljava/io/FileOutputStream;
    if-nez v0, :cond_f

    #@9
    .line 2126
    :try_start_9
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mPendingFile:Landroid/util/AtomicFile;

    #@b
    invoke-virtual {v6}, Landroid/util/AtomicFile;->truncate()V

    #@e
    .line 2148
    :cond_e
    :goto_e
    return-void

    #@f
    .line 2131
    :cond_f
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mPendingFile:Landroid/util/AtomicFile;

    #@11
    invoke-virtual {v6}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@14
    move-result-object v2

    #@15
    .line 2133
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@18
    move-result-object v5

    #@19
    .line 2134
    .local v5, out:Landroid/os/Parcel;
    const/4 v3, 0x0

    #@1a
    .local v3, i:I
    :goto_1a
    if-ge v3, v0, :cond_2a

    #@1c
    .line 2135
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v4

    #@22
    check-cast v4, Landroid/content/SyncStorageEngine$PendingOperation;

    #@24
    .line 2136
    .local v4, op:Landroid/content/SyncStorageEngine$PendingOperation;
    invoke-direct {p0, v4, v5}, Landroid/content/SyncStorageEngine;->writePendingOperationLocked(Landroid/content/SyncStorageEngine$PendingOperation;Landroid/os/Parcel;)V

    #@27
    .line 2134
    add-int/lit8 v3, v3, 0x1

    #@29
    goto :goto_1a

    #@2a
    .line 2138
    .end local v4           #op:Landroid/content/SyncStorageEngine$PendingOperation;
    :cond_2a
    invoke-virtual {v5}, Landroid/os/Parcel;->marshall()[B

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {v2, v6}, Ljava/io/FileOutputStream;->write([B)V

    #@31
    .line 2139
    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 2141
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mPendingFile:Landroid/util/AtomicFile;

    #@36
    invoke-virtual {v6, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_39} :catch_3a

    #@39
    goto :goto_e

    #@3a
    .line 2142
    .end local v3           #i:I
    .end local v5           #out:Landroid/os/Parcel;
    :catch_3a
    move-exception v1

    #@3b
    .line 2143
    .local v1, e1:Ljava/io/IOException;
    const-string v6, "SyncManager"

    #@3d
    const-string v7, "Error writing pending operations"

    #@3f
    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@42
    .line 2144
    if-eqz v2, :cond_e

    #@44
    .line 2145
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mPendingFile:Landroid/util/AtomicFile;

    #@46
    invoke-virtual {v6, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@49
    goto :goto_e
.end method

.method private writeStatisticsLocked()V
    .registers 9

    #@0
    .prologue
    .line 2272
    const/4 v6, 0x2

    #@1
    invoke-virtual {p0, v6}, Landroid/content/SyncStorageEngine;->removeMessages(I)V

    #@4
    .line 2274
    const/4 v3, 0x0

    #@5
    .line 2276
    .local v3, fos:Ljava/io/FileOutputStream;
    :try_start_5
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mStatisticsFile:Landroid/util/AtomicFile;

    #@7
    invoke-virtual {v6}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@a
    move-result-object v3

    #@b
    .line 2277
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v5

    #@f
    .line 2278
    .local v5, out:Landroid/os/Parcel;
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@11
    array-length v0, v6

    #@12
    .line 2279
    .local v0, N:I
    const/4 v4, 0x0

    #@13
    .local v4, i:I
    :goto_13
    if-ge v4, v0, :cond_1b

    #@15
    .line 2280
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@17
    aget-object v1, v6, v4

    #@19
    .line 2281
    .local v1, ds:Landroid/content/SyncStorageEngine$DayStats;
    if-nez v1, :cond_2f

    #@1b
    .line 2291
    .end local v1           #ds:Landroid/content/SyncStorageEngine$DayStats;
    :cond_1b
    const/4 v6, 0x0

    #@1c
    invoke-virtual {v5, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 2292
    invoke-virtual {v5}, Landroid/os/Parcel;->marshall()[B

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v3, v6}, Ljava/io/FileOutputStream;->write([B)V

    #@26
    .line 2293
    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 2295
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mStatisticsFile:Landroid/util/AtomicFile;

    #@2b
    invoke-virtual {v6, v3}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    #@2e
    .line 2302
    .end local v0           #N:I
    .end local v4           #i:I
    .end local v5           #out:Landroid/os/Parcel;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 2284
    .restart local v0       #N:I
    .restart local v1       #ds:Landroid/content/SyncStorageEngine$DayStats;
    .restart local v4       #i:I
    .restart local v5       #out:Landroid/os/Parcel;
    :cond_2f
    const/16 v6, 0x65

    #@31
    invoke-virtual {v5, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 2285
    iget v6, v1, Landroid/content/SyncStorageEngine$DayStats;->day:I

    #@36
    invoke-virtual {v5, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@39
    .line 2286
    iget v6, v1, Landroid/content/SyncStorageEngine$DayStats;->successCount:I

    #@3b
    invoke-virtual {v5, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 2287
    iget-wide v6, v1, Landroid/content/SyncStorageEngine$DayStats;->successTime:J

    #@40
    invoke-virtual {v5, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    #@43
    .line 2288
    iget v6, v1, Landroid/content/SyncStorageEngine$DayStats;->failureCount:I

    #@45
    invoke-virtual {v5, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@48
    .line 2289
    iget-wide v6, v1, Landroid/content/SyncStorageEngine$DayStats;->failureTime:J

    #@4a
    invoke-virtual {v5, v6, v7}, Landroid/os/Parcel;->writeLong(J)V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_4d} :catch_50

    #@4d
    .line 2279
    add-int/lit8 v4, v4, 0x1

    #@4f
    goto :goto_13

    #@50
    .line 2296
    .end local v0           #N:I
    .end local v1           #ds:Landroid/content/SyncStorageEngine$DayStats;
    .end local v4           #i:I
    .end local v5           #out:Landroid/os/Parcel;
    :catch_50
    move-exception v2

    #@51
    .line 2297
    .local v2, e1:Ljava/io/IOException;
    const-string v6, "SyncManager"

    #@53
    const-string v7, "Error writing stats"

    #@55
    invoke-static {v6, v7, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@58
    .line 2298
    if-eqz v3, :cond_2e

    #@5a
    .line 2299
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mStatisticsFile:Landroid/util/AtomicFile;

    #@5c
    invoke-virtual {v6, v3}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@5f
    goto :goto_2e
.end method

.method private writeStatusLocked()V
    .registers 9

    #@0
    .prologue
    .line 2024
    const/4 v6, 0x1

    #@1
    invoke-virtual {p0, v6}, Landroid/content/SyncStorageEngine;->removeMessages(I)V

    #@4
    .line 2026
    const/4 v2, 0x0

    #@5
    .line 2028
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_5
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mStatusFile:Landroid/util/AtomicFile;

    #@7
    invoke-virtual {v6}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    #@a
    move-result-object v2

    #@b
    .line 2029
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v4

    #@f
    .line 2030
    .local v4, out:Landroid/os/Parcel;
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    #@14
    move-result v0

    #@15
    .line 2031
    .local v0, N:I
    const/4 v3, 0x0

    #@16
    .local v3, i:I
    :goto_16
    if-ge v3, v0, :cond_2c

    #@18
    .line 2032
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@1a
    invoke-virtual {v6, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@1d
    move-result-object v5

    #@1e
    check-cast v5, Landroid/content/SyncStatusInfo;

    #@20
    .line 2033
    .local v5, status:Landroid/content/SyncStatusInfo;
    const/16 v6, 0x64

    #@22
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 2034
    const/4 v6, 0x0

    #@26
    invoke-virtual {v5, v4, v6}, Landroid/content/SyncStatusInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@29
    .line 2031
    add-int/lit8 v3, v3, 0x1

    #@2b
    goto :goto_16

    #@2c
    .line 2036
    .end local v5           #status:Landroid/content/SyncStatusInfo;
    :cond_2c
    const/4 v6, 0x0

    #@2d
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 2037
    invoke-virtual {v4}, Landroid/os/Parcel;->marshall()[B

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v2, v6}, Ljava/io/FileOutputStream;->write([B)V

    #@37
    .line 2038
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 2040
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mStatusFile:Landroid/util/AtomicFile;

    #@3c
    invoke-virtual {v6, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_3f
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_3f} :catch_40

    #@3f
    .line 2047
    .end local v0           #N:I
    .end local v3           #i:I
    .end local v4           #out:Landroid/os/Parcel;
    :cond_3f
    :goto_3f
    return-void

    #@40
    .line 2041
    :catch_40
    move-exception v1

    #@41
    .line 2042
    .local v1, e1:Ljava/io/IOException;
    const-string v6, "SyncManager"

    #@43
    const-string v7, "Error writing status"

    #@45
    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    .line 2043
    if-eqz v2, :cond_3f

    #@4a
    .line 2044
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mStatusFile:Landroid/util/AtomicFile;

    #@4c
    invoke-virtual {v6, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    #@4f
    goto :goto_3f
.end method


# virtual methods
.method public addActiveSync(Landroid/content/SyncManager$ActiveSyncContext;)Landroid/content/SyncInfo;
    .registers 10
    .parameter "activeSyncContext"

    #@0
    .prologue
    .line 1004
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v7

    #@3
    .line 1012
    :try_start_3
    iget-object v1, p1, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@5
    iget-object v1, v1, Landroid/content/SyncOperation;->account:Landroid/accounts/Account;

    #@7
    iget-object v2, p1, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@9
    iget v2, v2, Landroid/content/SyncOperation;->userId:I

    #@b
    iget-object v3, p1, Landroid/content/SyncManager$ActiveSyncContext;->mSyncOperation:Landroid/content/SyncOperation;

    #@d
    iget-object v3, v3, Landroid/content/SyncOperation;->authority:Ljava/lang/String;

    #@f
    const/4 v4, -0x1

    #@10
    const/4 v5, 0x1

    #@11
    move-object v0, p0

    #@12
    invoke-direct/range {v0 .. v5}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@15
    move-result-object v6

    #@16
    .line 1018
    .local v6, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    new-instance v0, Landroid/content/SyncInfo;

    #@18
    iget v1, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@1a
    iget-object v2, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@1c
    iget-object v3, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@1e
    iget-wide v4, p1, Landroid/content/SyncManager$ActiveSyncContext;->mStartTime:J

    #@20
    invoke-direct/range {v0 .. v5}, Landroid/content/SyncInfo;-><init>(ILandroid/accounts/Account;Ljava/lang/String;J)V

    #@23
    .line 1021
    .local v0, syncInfo:Landroid/content/SyncInfo;
    iget v1, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@25
    invoke-virtual {p0, v1}, Landroid/content/SyncStorageEngine;->getCurrentSyncs(I)Ljava/util/List;

    #@28
    move-result-object v1

    #@29
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2c
    .line 1022
    monitor-exit v7
    :try_end_2d
    .catchall {:try_start_3 .. :try_end_2d} :catchall_31

    #@2d
    .line 1024
    invoke-virtual {p0}, Landroid/content/SyncStorageEngine;->reportActiveChange()V

    #@30
    .line 1025
    return-object v0

    #@31
    .line 1022
    .end local v0           #syncInfo:Landroid/content/SyncInfo;
    .end local v6           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :catchall_31
    move-exception v1

    #@32
    :try_start_32
    monitor-exit v7
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_31

    #@33
    throw v1
.end method

.method public addPeriodicSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;J)V
    .registers 15
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"
    .parameter "extras"
    .parameter "pollFrequency"

    #@0
    .prologue
    .line 756
    const/4 v7, 0x1

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move-wide v5, p5

    #@7
    invoke-direct/range {v0 .. v7}, Landroid/content/SyncStorageEngine;->updateOrRemovePeriodicSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;JZ)V

    #@a
    .line 758
    return-void
.end method

.method public addStatusChangeListener(ILandroid/content/ISyncStatusObserver;)V
    .registers 6
    .parameter "mask"
    .parameter "callback"

    #@0
    .prologue
    .line 418
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 419
    :try_start_3
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mChangeListeners:Landroid/os/RemoteCallbackList;

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v0, p2, v2}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    #@c
    .line 420
    monitor-exit v1

    #@d
    .line 421
    return-void

    #@e
    .line 420
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public clearAllBackoffs(Landroid/content/SyncQueue;)V
    .registers 16
    .parameter "syncQueue"

    #@0
    .prologue
    const-wide/16 v12, -0x1

    #@2
    .line 620
    const/4 v8, 0x0

    #@3
    .line 621
    .local v8, changed:Z
    iget-object v11, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@5
    monitor-enter v11

    #@6
    .line 622
    :try_start_6
    monitor-enter p1
    :try_end_7
    .catchall {:try_start_6 .. :try_end_7} :catchall_65

    #@7
    .line 623
    :try_start_7
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mAccounts:Ljava/util/HashMap;

    #@9
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@c
    move-result-object v0

    #@d
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v9

    #@11
    :cond_11
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_59

    #@17
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v6

    #@1b
    check-cast v6, Landroid/content/SyncStorageEngine$AccountInfo;

    #@1d
    .line 624
    .local v6, accountInfo:Landroid/content/SyncStorageEngine$AccountInfo;
    iget-object v0, v6, Landroid/content/SyncStorageEngine$AccountInfo;->authorities:Ljava/util/HashMap;

    #@1f
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@22
    move-result-object v0

    #@23
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@26
    move-result-object v10

    #@27
    .local v10, i$:Ljava/util/Iterator;
    :cond_27
    :goto_27
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@2a
    move-result v0

    #@2b
    if-eqz v0, :cond_11

    #@2d
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@30
    move-result-object v7

    #@31
    check-cast v7, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@33
    .line 625
    .local v7, authorityInfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    iget-wide v0, v7, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@35
    cmp-long v0, v0, v12

    #@37
    if-nez v0, :cond_3f

    #@39
    iget-wide v0, v7, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@3b
    cmp-long v0, v0, v12

    #@3d
    if-eqz v0, :cond_27

    #@3f
    .line 635
    :cond_3f
    const-wide/16 v0, -0x1

    #@41
    iput-wide v0, v7, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@43
    .line 636
    const-wide/16 v0, -0x1

    #@45
    iput-wide v0, v7, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@47
    .line 637
    iget-object v0, v6, Landroid/content/SyncStorageEngine$AccountInfo;->accountAndUser:Landroid/accounts/AccountAndUser;

    #@49
    iget-object v1, v0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@4b
    iget-object v0, v6, Landroid/content/SyncStorageEngine$AccountInfo;->accountAndUser:Landroid/accounts/AccountAndUser;

    #@4d
    iget v2, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@4f
    iget-object v3, v7, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@51
    const-wide/16 v4, 0x0

    #@53
    move-object v0, p1

    #@54
    invoke-virtual/range {v0 .. v5}, Landroid/content/SyncQueue;->onBackoffChanged(Landroid/accounts/Account;ILjava/lang/String;J)V

    #@57
    .line 639
    const/4 v8, 0x1

    #@58
    goto :goto_27

    #@59
    .line 643
    .end local v6           #accountInfo:Landroid/content/SyncStorageEngine$AccountInfo;
    .end local v7           #authorityInfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v10           #i$:Ljava/util/Iterator;
    :cond_59
    monitor-exit p1
    :try_end_5a
    .catchall {:try_start_7 .. :try_end_5a} :catchall_62

    #@5a
    .line 644
    :try_start_5a
    monitor-exit v11
    :try_end_5b
    .catchall {:try_start_5a .. :try_end_5b} :catchall_65

    #@5b
    .line 646
    if-eqz v8, :cond_61

    #@5d
    .line 647
    const/4 v0, 0x1

    #@5e
    invoke-direct {p0, v0}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@61
    .line 649
    :cond_61
    return-void

    #@62
    .line 643
    :catchall_62
    move-exception v0

    #@63
    :try_start_63
    monitor-exit p1
    :try_end_64
    .catchall {:try_start_63 .. :try_end_64} :catchall_62

    #@64
    :try_start_64
    throw v0

    #@65
    .line 644
    :catchall_65
    move-exception v0

    #@66
    monitor-exit v11
    :try_end_67
    .catchall {:try_start_64 .. :try_end_67} :catchall_65

    #@67
    throw v0
.end method

.method public clearAndReadState()V
    .registers 3

    #@0
    .prologue
    .line 1468
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 1469
    :try_start_3
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    #@8
    .line 1470
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mAccounts:Ljava/util/HashMap;

    #@a
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@d
    .line 1471
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@12
    .line 1472
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@14
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    #@17
    .line 1473
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@1c
    .line 1475
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->readAccountInfoLocked()V

    #@1f
    .line 1476
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->readStatusLocked()V

    #@22
    .line 1477
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->readPendingOperationsLocked()V

    #@25
    .line 1478
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->readStatisticsLocked()V

    #@28
    .line 1479
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->readAndDeleteLegacyAccountInfoLocked()V

    #@2b
    .line 1480
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@2e
    .line 1481
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeStatusLocked()V

    #@31
    .line 1482
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writePendingOperationsLocked()V

    #@34
    .line 1483
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeStatisticsLocked()V

    #@37
    .line 1484
    monitor-exit v1

    #@38
    .line 1485
    return-void

    #@39
    .line 1484
    :catchall_39
    move-exception v0

    #@3a
    monitor-exit v1
    :try_end_3b
    .catchall {:try_start_3 .. :try_end_3b} :catchall_39

    #@3b
    throw v0
.end method

.method public deleteFromPending(Landroid/content/SyncStorageEngine$PendingOperation;)Z
    .registers 14
    .parameter "op"

    #@0
    .prologue
    .line 875
    const/4 v5, 0x0

    #@1
    .line 876
    .local v5, res:Z
    iget-object v8, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@3
    monitor-enter v8

    #@4
    .line 884
    :try_start_4
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@9
    move-result v7

    #@a
    if-eqz v7, :cond_66

    #@c
    .line 885
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v7

    #@12
    if-eqz v7, :cond_19

    #@14
    iget v7, p0, Landroid/content/SyncStorageEngine;->mNumPendingFinished:I

    #@16
    const/4 v9, 0x4

    #@17
    if-lt v7, v9, :cond_6c

    #@19
    .line 887
    :cond_19
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writePendingOperationsLocked()V

    #@1c
    .line 888
    const/4 v7, 0x0

    #@1d
    iput v7, p0, Landroid/content/SyncStorageEngine;->mNumPendingFinished:I

    #@1f
    .line 893
    :goto_1f
    iget-object v7, p1, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@21
    iget v9, p1, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@23
    iget-object v10, p1, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@25
    const-string v11, "deleteFromPending"

    #@27
    invoke-direct {p0, v7, v9, v10, v11}, Landroid/content/SyncStorageEngine;->getAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@2a
    move-result-object v1

    #@2b
    .line 895
    .local v1, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v1, :cond_65

    #@2d
    .line 897
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@2f
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@32
    move-result v0

    #@33
    .line 898
    .local v0, N:I
    const/4 v4, 0x0

    #@34
    .line 899
    .local v4, morePending:Z
    const/4 v3, 0x0

    #@35
    .local v3, i:I
    :goto_35
    if-ge v3, v0, :cond_5a

    #@37
    .line 900
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@39
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3c
    move-result-object v2

    #@3d
    check-cast v2, Landroid/content/SyncStorageEngine$PendingOperation;

    #@3f
    .line 901
    .local v2, cur:Landroid/content/SyncStorageEngine$PendingOperation;
    iget-object v7, v2, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@41
    iget-object v9, p1, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@43
    invoke-virtual {v7, v9}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v7

    #@47
    if-eqz v7, :cond_76

    #@49
    iget-object v7, v2, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@4b
    iget-object v9, p1, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@4d
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v7

    #@51
    if-eqz v7, :cond_76

    #@53
    iget v7, v2, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@55
    iget v9, p1, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@57
    if-ne v7, v9, :cond_76

    #@59
    .line 904
    const/4 v4, 0x1

    #@5a
    .line 909
    .end local v2           #cur:Landroid/content/SyncStorageEngine$PendingOperation;
    :cond_5a
    if-nez v4, :cond_65

    #@5c
    .line 911
    iget v7, v1, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@5e
    invoke-direct {p0, v7}, Landroid/content/SyncStorageEngine;->getOrCreateSyncStatusLocked(I)Landroid/content/SyncStatusInfo;

    #@61
    move-result-object v6

    #@62
    .line 912
    .local v6, status:Landroid/content/SyncStatusInfo;
    const/4 v7, 0x0

    #@63
    iput-boolean v7, v6, Landroid/content/SyncStatusInfo;->pending:Z

    #@65
    .line 916
    .end local v0           #N:I
    .end local v3           #i:I
    .end local v4           #morePending:Z
    .end local v6           #status:Landroid/content/SyncStatusInfo;
    :cond_65
    const/4 v5, 0x1

    #@66
    .line 918
    .end local v1           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_66
    monitor-exit v8
    :try_end_67
    .catchall {:try_start_4 .. :try_end_67} :catchall_73

    #@67
    .line 920
    const/4 v7, 0x2

    #@68
    invoke-direct {p0, v7}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@6b
    .line 921
    return v5

    #@6c
    .line 890
    :cond_6c
    :try_start_6c
    iget v7, p0, Landroid/content/SyncStorageEngine;->mNumPendingFinished:I

    #@6e
    add-int/lit8 v7, v7, 0x1

    #@70
    iput v7, p0, Landroid/content/SyncStorageEngine;->mNumPendingFinished:I

    #@72
    goto :goto_1f

    #@73
    .line 918
    :catchall_73
    move-exception v7

    #@74
    monitor-exit v8
    :try_end_75
    .catchall {:try_start_6c .. :try_end_75} :catchall_73

    #@75
    throw v7

    #@76
    .line 899
    .restart local v0       #N:I
    .restart local v1       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v2       #cur:Landroid/content/SyncStorageEngine$PendingOperation;
    .restart local v3       #i:I
    .restart local v4       #morePending:Z
    :cond_76
    add-int/lit8 v3, v3, 0x1

    #@78
    goto :goto_35
.end method

.method public doDatabaseCleanup([Landroid/accounts/Account;I)V
    .registers 14
    .parameter "accounts"
    .parameter "userId"

    #@0
    .prologue
    .line 949
    iget-object v9, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v9

    #@3
    .line 951
    :try_start_3
    new-instance v7, Landroid/util/SparseArray;

    #@5
    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    #@8
    .line 952
    .local v7, removing:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/content/SyncStorageEngine$AuthorityInfo;>;"
    iget-object v8, p0, Landroid/content/SyncStorageEngine;->mAccounts:Ljava/util/HashMap;

    #@a
    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@d
    move-result-object v8

    #@e
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v1

    #@12
    .line 953
    .local v1, accIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/SyncStorageEngine$AccountInfo;>;"
    :cond_12
    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v8

    #@16
    if-eqz v8, :cond_51

    #@18
    .line 954
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/content/SyncStorageEngine$AccountInfo;

    #@1e
    .line 955
    .local v0, acc:Landroid/content/SyncStorageEngine$AccountInfo;
    iget-object v8, v0, Landroid/content/SyncStorageEngine$AccountInfo;->accountAndUser:Landroid/accounts/AccountAndUser;

    #@20
    iget-object v8, v8, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@22
    invoke-static {p1, v8}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    #@25
    move-result v8

    #@26
    if-nez v8, :cond_12

    #@28
    iget-object v8, v0, Landroid/content/SyncStorageEngine$AccountInfo;->accountAndUser:Landroid/accounts/AccountAndUser;

    #@2a
    iget v8, v8, Landroid/accounts/AccountAndUser;->userId:I

    #@2c
    if-ne v8, p2, :cond_12

    #@2e
    .line 961
    iget-object v8, v0, Landroid/content/SyncStorageEngine$AccountInfo;->authorities:Ljava/util/HashMap;

    #@30
    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@33
    move-result-object v8

    #@34
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@37
    move-result-object v4

    #@38
    .local v4, i$:Ljava/util/Iterator;
    :goto_38
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@3b
    move-result v8

    #@3c
    if-eqz v8, :cond_4d

    #@3e
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@41
    move-result-object v2

    #@42
    check-cast v2, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@44
    .line 962
    .local v2, auth:Landroid/content/SyncStorageEngine$AuthorityInfo;
    iget v8, v2, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@46
    invoke-virtual {v7, v8, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@49
    goto :goto_38

    #@4a
    .line 995
    .end local v0           #acc:Landroid/content/SyncStorageEngine$AccountInfo;
    .end local v1           #accIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/SyncStorageEngine$AccountInfo;>;"
    .end local v2           #auth:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v7           #removing:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/content/SyncStorageEngine$AuthorityInfo;>;"
    :catchall_4a
    move-exception v8

    #@4b
    monitor-exit v9
    :try_end_4c
    .catchall {:try_start_3 .. :try_end_4c} :catchall_4a

    #@4c
    throw v8

    #@4d
    .line 964
    .restart local v0       #acc:Landroid/content/SyncStorageEngine$AccountInfo;
    .restart local v1       #accIt:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/content/SyncStorageEngine$AccountInfo;>;"
    .restart local v4       #i$:Ljava/util/Iterator;
    .restart local v7       #removing:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/content/SyncStorageEngine$AuthorityInfo;>;"
    :cond_4d
    :try_start_4d
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@50
    goto :goto_12

    #@51
    .line 969
    .end local v0           #acc:Landroid/content/SyncStorageEngine$AccountInfo;
    .end local v4           #i$:Ljava/util/Iterator;
    :cond_51
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    #@54
    move-result v3

    #@55
    .line 970
    .local v3, i:I
    if-lez v3, :cond_aa

    #@57
    .line 971
    :cond_57
    if-lez v3, :cond_9e

    #@59
    .line 972
    add-int/lit8 v3, v3, -0x1

    #@5b
    .line 973
    invoke-virtual {v7, v3}, Landroid/util/SparseArray;->keyAt(I)I

    #@5e
    move-result v5

    #@5f
    .line 974
    .local v5, ident:I
    iget-object v8, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@61
    invoke-virtual {v8, v5}, Landroid/util/SparseArray;->remove(I)V

    #@64
    .line 975
    iget-object v8, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@66
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    #@69
    move-result v6

    #@6a
    .line 976
    .local v6, j:I
    :cond_6a
    :goto_6a
    if-lez v6, :cond_82

    #@6c
    .line 977
    add-int/lit8 v6, v6, -0x1

    #@6e
    .line 978
    iget-object v8, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@70
    invoke-virtual {v8, v6}, Landroid/util/SparseArray;->keyAt(I)I

    #@73
    move-result v8

    #@74
    if-ne v8, v5, :cond_6a

    #@76
    .line 979
    iget-object v8, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@78
    iget-object v10, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@7a
    invoke-virtual {v10, v6}, Landroid/util/SparseArray;->keyAt(I)I

    #@7d
    move-result v10

    #@7e
    invoke-virtual {v8, v10}, Landroid/util/SparseArray;->remove(I)V

    #@81
    goto :goto_6a

    #@82
    .line 982
    :cond_82
    iget-object v8, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@84
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@87
    move-result v6

    #@88
    .line 983
    :cond_88
    :goto_88
    if-lez v6, :cond_57

    #@8a
    .line 984
    add-int/lit8 v6, v6, -0x1

    #@8c
    .line 985
    iget-object v8, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@8e
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@91
    move-result-object v8

    #@92
    check-cast v8, Landroid/content/SyncStorageEngine$SyncHistoryItem;

    #@94
    iget v8, v8, Landroid/content/SyncStorageEngine$SyncHistoryItem;->authorityId:I

    #@96
    if-ne v8, v5, :cond_88

    #@98
    .line 986
    iget-object v8, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@9a
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@9d
    goto :goto_88

    #@9e
    .line 990
    .end local v5           #ident:I
    .end local v6           #j:I
    :cond_9e
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@a1
    .line 991
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeStatusLocked()V

    #@a4
    .line 992
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writePendingOperationsLocked()V

    #@a7
    .line 993
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeStatisticsLocked()V

    #@aa
    .line 995
    :cond_aa
    monitor-exit v9
    :try_end_ab
    .catchall {:try_start_4d .. :try_end_ab} :catchall_4a

    #@ab
    .line 996
    return-void
.end method

.method public getAuthorities()Ljava/util/ArrayList;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/SyncStorageEngine$AuthorityInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1250
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v4

    #@3
    .line 1251
    :try_start_3
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@8
    move-result v0

    #@9
    .line 1252
    .local v0, N:I
    new-instance v2, Ljava/util/ArrayList;

    #@b
    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@e
    .line 1253
    .local v2, infos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStorageEngine$AuthorityInfo;>;"
    const/4 v1, 0x0

    #@f
    .local v1, i:I
    :goto_f
    if-ge v1, v0, :cond_24

    #@11
    .line 1255
    new-instance v5, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@13
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@15
    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@18
    move-result-object v3

    #@19
    check-cast v3, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@1b
    invoke-direct {v5, v3}, Landroid/content/SyncStorageEngine$AuthorityInfo;-><init>(Landroid/content/SyncStorageEngine$AuthorityInfo;)V

    #@1e
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@21
    .line 1253
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_f

    #@24
    .line 1257
    :cond_24
    monitor-exit v4

    #@25
    return-object v2

    #@26
    .line 1258
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #infos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStorageEngine$AuthorityInfo;>;"
    :catchall_26
    move-exception v3

    #@27
    monitor-exit v4
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_26

    #@28
    throw v3
.end method

.method public getAuthority(I)Landroid/content/SyncStorageEngine$AuthorityInfo;
    .registers 4
    .parameter "authorityId"

    #@0
    .prologue
    .line 819
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 820
    :try_start_3
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@b
    monitor-exit v1

    #@c
    return-object v0

    #@d
    .line 821
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public getBackoff(Landroid/accounts/Account;ILjava/lang/String;)Landroid/util/Pair;
    .registers 11
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 564
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v2

    #@3
    .line 565
    :try_start_3
    const-string v1, "getBackoff"

    #@5
    invoke-direct {p0, p1, p2, p3, v1}, Landroid/content/SyncStorageEngine;->getAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@8
    move-result-object v0

    #@9
    .line 567
    .local v0, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v0, :cond_13

    #@b
    iget-wide v3, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@d
    const-wide/16 v5, 0x0

    #@f
    cmp-long v1, v3, v5

    #@11
    if-gez v1, :cond_16

    #@13
    .line 568
    :cond_13
    const/4 v1, 0x0

    #@14
    monitor-exit v2

    #@15
    .line 570
    :goto_15
    return-object v1

    #@16
    :cond_16
    iget-wide v3, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@18
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1b
    move-result-object v1

    #@1c
    iget-wide v3, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@1e
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@21
    move-result-object v3

    #@22
    invoke-static {v1, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@25
    move-result-object v1

    #@26
    monitor-exit v2

    #@27
    goto :goto_15

    #@28
    .line 571
    .end local v0           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :catchall_28
    move-exception v1

    #@29
    monitor-exit v2
    :try_end_2a
    .catchall {:try_start_3 .. :try_end_2a} :catchall_28

    #@2a
    throw v1
.end method

.method public getCurrentSyncs(I)Ljava/util/List;
    .registers 5
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/content/SyncInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1218
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v2

    #@3
    .line 1219
    :try_start_3
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mCurrentSyncs:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/util/ArrayList;

    #@b
    .line 1220
    .local v0, syncs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncInfo;>;"
    if-nez v0, :cond_17

    #@d
    .line 1221
    new-instance v0, Ljava/util/ArrayList;

    #@f
    .end local v0           #syncs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncInfo;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@12
    .line 1222
    .restart local v0       #syncs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncInfo;>;"
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mCurrentSyncs:Landroid/util/SparseArray;

    #@14
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@17
    .line 1224
    :cond_17
    monitor-exit v2

    #@18
    return-object v0

    #@19
    .line 1225
    .end local v0           #syncs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncInfo;>;"
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method public getDayStatistics()[Landroid/content/SyncStorageEngine$DayStats;
    .registers 7

    #@0
    .prologue
    .line 1337
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v2

    #@3
    .line 1338
    :try_start_3
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@5
    array-length v1, v1

    #@6
    new-array v0, v1, [Landroid/content/SyncStorageEngine$DayStats;

    #@8
    .line 1339
    .local v0, ds:[Landroid/content/SyncStorageEngine$DayStats;
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@a
    const/4 v3, 0x0

    #@b
    const/4 v4, 0x0

    #@c
    array-length v5, v0

    #@d
    invoke-static {v1, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@10
    .line 1340
    monitor-exit v2

    #@11
    return-object v0

    #@12
    .line 1341
    .end local v0           #ds:[Landroid/content/SyncStorageEngine$DayStats;
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method public getDelayUntilTime(Landroid/accounts/Account;ILjava/lang/String;)J
    .registers 8
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"

    #@0
    .prologue
    .line 670
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v3

    #@3
    .line 671
    :try_start_3
    const-string v1, "getDelayUntil"

    #@5
    invoke-direct {p0, p1, p2, p3, v1}, Landroid/content/SyncStorageEngine;->getAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@8
    move-result-object v0

    #@9
    .line 673
    .local v0, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-nez v0, :cond_f

    #@b
    .line 674
    const-wide/16 v1, 0x0

    #@d
    monitor-exit v3

    #@e
    .line 676
    :goto_e
    return-wide v1

    #@f
    :cond_f
    iget-wide v1, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->delayUntil:J

    #@11
    monitor-exit v3

    #@12
    goto :goto_e

    #@13
    .line 677
    .end local v0           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :catchall_13
    move-exception v1

    #@14
    monitor-exit v3
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v1
.end method

.method public getIsSyncable(Landroid/accounts/Account;ILjava/lang/String;)I
    .registers 9
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 512
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@3
    monitor-enter v3

    #@4
    .line 513
    if-eqz p1, :cond_17

    #@6
    .line 514
    :try_start_6
    const-string v4, "getIsSyncable"

    #@8
    invoke-direct {p0, p1, p2, p3, v4}, Landroid/content/SyncStorageEngine;->getAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@b
    move-result-object v0

    #@c
    .line 516
    .local v0, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-nez v0, :cond_10

    #@e
    .line 517
    monitor-exit v3

    #@f
    .line 530
    .end local v0           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :goto_f
    return v2

    #@10
    .line 519
    .restart local v0       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_10
    iget v2, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@12
    monitor-exit v3

    #@13
    goto :goto_f

    #@14
    .line 531
    .end local v0           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :catchall_14
    move-exception v2

    #@15
    monitor-exit v3
    :try_end_16
    .catchall {:try_start_6 .. :try_end_16} :catchall_14

    #@16
    throw v2

    #@17
    .line 522
    :cond_17
    :try_start_17
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@19
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    #@1c
    move-result v1

    #@1d
    .line 523
    .local v1, i:I
    :cond_1d
    if-lez v1, :cond_35

    #@1f
    .line 524
    add-int/lit8 v1, v1, -0x1

    #@21
    .line 525
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@23
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    check-cast v0, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@29
    .line 526
    .restart local v0       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    iget-object v4, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@2b
    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v4

    #@2f
    if-eqz v4, :cond_1d

    #@31
    .line 527
    iget v2, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@33
    monitor-exit v3

    #@34
    goto :goto_f

    #@35
    .line 530
    .end local v0           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_35
    monitor-exit v3
    :try_end_36
    .catchall {:try_start_17 .. :try_end_36} :catchall_14

    #@36
    goto :goto_f
.end method

.method public getMasterSyncAutomatically(I)Z
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 798
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v2

    #@3
    .line 799
    :try_start_3
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mMasterSyncAutomatically:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/lang/Boolean;

    #@b
    .line 800
    .local v0, auto:Ljava/lang/Boolean;
    if-nez v0, :cond_11

    #@d
    iget-boolean v1, p0, Landroid/content/SyncStorageEngine;->mDefaultMasterSyncAutomatically:Z

    #@f
    :goto_f
    monitor-exit v2

    #@10
    return v1

    #@11
    :cond_11
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@14
    move-result v1

    #@15
    goto :goto_f

    #@16
    .line 801
    .end local v0           #auto:Ljava/lang/Boolean;
    :catchall_16
    move-exception v1

    #@17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v1
.end method

.method public getOrCreateAuthority(Landroid/accounts/Account;ILjava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;
    .registers 11
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    .line 805
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v6

    #@3
    .line 806
    const/4 v4, -0x1

    #@4
    const/4 v5, 0x1

    #@5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move v2, p2

    #@8
    move-object v3, p3

    #@9
    :try_start_9
    invoke-direct/range {v0 .. v5}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@c
    move-result-object v0

    #@d
    monitor-exit v6

    #@e
    return-object v0

    #@f
    .line 809
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v6
    :try_end_11
    .catchall {:try_start_9 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public getOrCreateSyncStatus(Landroid/content/SyncStorageEngine$AuthorityInfo;)Landroid/content/SyncStatusInfo;
    .registers 4
    .parameter "authority"

    #@0
    .prologue
    .line 1435
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 1436
    :try_start_3
    iget v0, p1, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@5
    invoke-direct {p0, v0}, Landroid/content/SyncStorageEngine;->getOrCreateSyncStatusLocked(I)Landroid/content/SyncStatusInfo;

    #@8
    move-result-object v0

    #@9
    monitor-exit v1

    #@a
    return-object v0

    #@b
    .line 1437
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public getPendingOperationCount()I
    .registers 3

    #@0
    .prologue
    .line 939
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 940
    :try_start_3
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    monitor-exit v1

    #@a
    return v0

    #@b
    .line 941
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public getPendingOperations()Ljava/util/ArrayList;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/SyncStorageEngine$PendingOperation;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 930
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 931
    :try_start_3
    new-instance v0, Ljava/util/ArrayList;

    #@5
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@7
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@a
    monitor-exit v1

    #@b
    return-object v0

    #@c
    .line 932
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public getPeriodicSyncs(Landroid/accounts/Account;ILjava/lang/String;)Ljava/util/List;
    .registers 15
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/PeriodicSync;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 767
    new-instance v9, Ljava/util/ArrayList;

    #@2
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 768
    .local v9, syncs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/PeriodicSync;>;"
    iget-object v10, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@7
    monitor-enter v10

    #@8
    .line 769
    :try_start_8
    const-string v0, "getPeriodicSyncs"

    #@a
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/content/SyncStorageEngine;->getAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@d
    move-result-object v6

    #@e
    .line 771
    .local v6, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v6, :cond_3c

    #@10
    .line 772
    iget-object v0, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->periodicSyncs:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v7

    #@16
    .local v7, i$:Ljava/util/Iterator;
    :goto_16
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_3c

    #@1c
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v8

    #@20
    check-cast v8, Landroid/util/Pair;

    #@22
    .line 773
    .local v8, item:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    new-instance v0, Landroid/content/PeriodicSync;

    #@24
    iget-object v3, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@26
    check-cast v3, Landroid/os/Bundle;

    #@28
    iget-object v1, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@2a
    check-cast v1, Ljava/lang/Long;

    #@2c
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    #@2f
    move-result-wide v4

    #@30
    move-object v1, p1

    #@31
    move-object v2, p3

    #@32
    invoke-direct/range {v0 .. v5}, Landroid/content/PeriodicSync;-><init>(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    #@35
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@38
    goto :goto_16

    #@39
    .line 777
    .end local v6           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v8           #item:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/os/Bundle;Ljava/lang/Long;>;"
    :catchall_39
    move-exception v0

    #@3a
    monitor-exit v10
    :try_end_3b
    .catchall {:try_start_8 .. :try_end_3b} :catchall_39

    #@3b
    throw v0

    #@3c
    .restart local v6       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_3c
    :try_start_3c
    monitor-exit v10
    :try_end_3d
    .catchall {:try_start_3c .. :try_end_3d} :catchall_39

    #@3d
    .line 778
    return-object v9
.end method

.method public getStatusByAccountAndAuthority(Landroid/accounts/Account;ILjava/lang/String;)Landroid/content/SyncStatusInfo;
    .registers 11
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    .line 1270
    if-eqz p1, :cond_4

    #@2
    if-nez p3, :cond_a

    #@4
    .line 1271
    :cond_4
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@6
    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@9
    throw v4

    #@a
    .line 1273
    :cond_a
    iget-object v5, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@c
    monitor-enter v5

    #@d
    .line 1274
    :try_start_d
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@f
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    #@12
    move-result v0

    #@13
    .line 1275
    .local v0, N:I
    const/4 v3, 0x0

    #@14
    .local v3, i:I
    :goto_14
    if-ge v3, v0, :cond_43

    #@16
    .line 1276
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@18
    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Landroid/content/SyncStatusInfo;

    #@1e
    .line 1277
    .local v2, cur:Landroid/content/SyncStatusInfo;
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@20
    iget v6, v2, Landroid/content/SyncStatusInfo;->authorityId:I

    #@22
    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@28
    .line 1279
    .local v1, ainfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v1, :cond_40

    #@2a
    iget-object v4, v1, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@2c
    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v4

    #@30
    if-eqz v4, :cond_40

    #@32
    iget v4, v1, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@34
    if-ne v4, p2, :cond_40

    #@36
    iget-object v4, v1, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@38
    invoke-virtual {p1, v4}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v4

    #@3c
    if-eqz v4, :cond_40

    #@3e
    .line 1282
    monitor-exit v5

    #@3f
    .line 1285
    .end local v1           #ainfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v2           #cur:Landroid/content/SyncStatusInfo;
    :goto_3f
    return-object v2

    #@40
    .line 1275
    .restart local v1       #ainfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v2       #cur:Landroid/content/SyncStatusInfo;
    :cond_40
    add-int/lit8 v3, v3, 0x1

    #@42
    goto :goto_14

    #@43
    .line 1285
    .end local v1           #ainfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v2           #cur:Landroid/content/SyncStatusInfo;
    :cond_43
    const/4 v2, 0x0

    #@44
    monitor-exit v5

    #@45
    goto :goto_3f

    #@46
    .line 1286
    .end local v0           #N:I
    .end local v3           #i:I
    :catchall_46
    move-exception v4

    #@47
    monitor-exit v5
    :try_end_48
    .catchall {:try_start_d .. :try_end_48} :catchall_46

    #@48
    throw v4
.end method

.method public getSyncAutomatically(Landroid/accounts/Account;ILjava/lang/String;)Z
    .registers 10
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 465
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@4
    monitor-enter v4

    #@5
    .line 466
    if-eqz p1, :cond_17

    #@7
    .line 467
    :try_start_7
    const-string v5, "getSyncAutomatically"

    #@9
    invoke-direct {p0, p1, p2, p3, v5}, Landroid/content/SyncStorageEngine;->getAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@c
    move-result-object v0

    #@d
    .line 469
    .local v0, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v0, :cond_15

    #@f
    iget-boolean v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@11
    if-eqz v5, :cond_15

    #@13
    :goto_13
    monitor-exit v4

    #@14
    .line 482
    .end local v0           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :goto_14
    return v2

    #@15
    .restart local v0       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_15
    move v2, v3

    #@16
    .line 469
    goto :goto_13

    #@17
    .line 472
    .end local v0           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_17
    iget-object v5, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@19
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@1c
    move-result v1

    #@1d
    .line 473
    .local v1, i:I
    :cond_1d
    if-lez v1, :cond_3e

    #@1f
    .line 474
    add-int/lit8 v1, v1, -0x1

    #@21
    .line 475
    iget-object v5, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@23
    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    check-cast v0, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@29
    .line 476
    .restart local v0       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    iget-object v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@2b
    invoke-virtual {v5, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v5

    #@2f
    if-eqz v5, :cond_1d

    #@31
    iget v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@33
    if-ne v5, p2, :cond_1d

    #@35
    iget-boolean v5, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@37
    if-eqz v5, :cond_1d

    #@39
    .line 479
    monitor-exit v4

    #@3a
    goto :goto_14

    #@3b
    .line 483
    .end local v0           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v1           #i:I
    :catchall_3b
    move-exception v2

    #@3c
    monitor-exit v4
    :try_end_3d
    .catchall {:try_start_7 .. :try_end_3d} :catchall_3b

    #@3d
    throw v2

    #@3e
    .line 482
    .restart local v1       #i:I
    :cond_3e
    :try_start_3e
    monitor-exit v4
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_3b

    #@3f
    move v2, v3

    #@40
    goto :goto_14
.end method

.method public getSyncHistory()Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/SyncStorageEngine$SyncHistoryItem;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1321
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v4

    #@3
    .line 1322
    :try_start_3
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    .line 1323
    .local v0, N:I
    new-instance v2, Ljava/util/ArrayList;

    #@b
    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@e
    .line 1324
    .local v2, items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStorageEngine$SyncHistoryItem;>;"
    const/4 v1, 0x0

    #@f
    .local v1, i:I
    :goto_f
    if-ge v1, v0, :cond_1d

    #@11
    .line 1325
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 1324
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_f

    #@1d
    .line 1327
    :cond_1d
    monitor-exit v4

    #@1e
    return-object v2

    #@1f
    .line 1328
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStorageEngine$SyncHistoryItem;>;"
    :catchall_1f
    move-exception v3

    #@20
    monitor-exit v4
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v3
.end method

.method public getSyncRandomOffset()I
    .registers 2

    #@0
    .prologue
    .line 414
    iget v0, p0, Landroid/content/SyncStorageEngine;->mSyncRandomOffset:I

    #@2
    return v0
.end method

.method public getSyncStatus()Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/SyncStatusInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1234
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v4

    #@3
    .line 1235
    :try_start_3
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@8
    move-result v0

    #@9
    .line 1236
    .local v0, N:I
    new-instance v2, Ljava/util/ArrayList;

    #@b
    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@e
    .line 1237
    .local v2, ops:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStatusInfo;>;"
    const/4 v1, 0x0

    #@f
    .local v1, i:I
    :goto_f
    if-ge v1, v0, :cond_1d

    #@11
    .line 1238
    iget-object v3, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 1237
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_f

    #@1d
    .line 1240
    :cond_1d
    monitor-exit v4

    #@1e
    return-object v2

    #@1f
    .line 1241
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #ops:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/SyncStatusInfo;>;"
    :catchall_1f
    move-exception v3

    #@20
    monitor-exit v4
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v3
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 402
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_10

    #@5
    .line 403
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@7
    monitor-enter v1

    #@8
    .line 404
    :try_start_8
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeStatusLocked()V

    #@b
    .line 405
    monitor-exit v1

    #@c
    .line 411
    :cond_c
    :goto_c
    return-void

    #@d
    .line 405
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_d

    #@f
    throw v0

    #@10
    .line 406
    :cond_10
    iget v0, p1, Landroid/os/Message;->what:I

    #@12
    const/4 v1, 0x2

    #@13
    if-ne v0, v1, :cond_c

    #@15
    .line 407
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@17
    monitor-enter v1

    #@18
    .line 408
    :try_start_18
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeStatisticsLocked()V

    #@1b
    .line 409
    monitor-exit v1

    #@1c
    goto :goto_c

    #@1d
    :catchall_1d
    move-exception v0

    #@1e
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_18 .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method public insertIntoPending(Landroid/content/SyncStorageEngine$PendingOperation;)Landroid/content/SyncStorageEngine$PendingOperation;
    .registers 12
    .parameter "op"

    #@0
    .prologue
    .line 844
    iget-object v9, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v9

    #@3
    .line 853
    :try_start_3
    iget-object v1, p1, Landroid/content/SyncStorageEngine$PendingOperation;->account:Landroid/accounts/Account;

    #@5
    iget v2, p1, Landroid/content/SyncStorageEngine$PendingOperation;->userId:I

    #@7
    iget-object v3, p1, Landroid/content/SyncStorageEngine$PendingOperation;->authority:Ljava/lang/String;

    #@9
    const/4 v4, -0x1

    #@a
    const/4 v5, 0x1

    #@b
    move-object v0, p0

    #@c
    invoke-direct/range {v0 .. v5}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@f
    move-result-object v6

    #@10
    .line 857
    .local v6, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-nez v6, :cond_15

    #@12
    .line 858
    const/4 v7, 0x0

    #@13
    monitor-exit v9

    #@14
    .line 871
    :goto_14
    return-object v7

    #@15
    .line 861
    :cond_15
    new-instance v7, Landroid/content/SyncStorageEngine$PendingOperation;

    #@17
    invoke-direct {v7, p1}, Landroid/content/SyncStorageEngine$PendingOperation;-><init>(Landroid/content/SyncStorageEngine$PendingOperation;)V
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_36

    #@1a
    .line 862
    .end local p1
    .local v7, op:Landroid/content/SyncStorageEngine$PendingOperation;
    :try_start_1a
    iget v0, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@1c
    iput v0, v7, Landroid/content/SyncStorageEngine$PendingOperation;->authorityId:I

    #@1e
    .line 863
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mPendingOperations:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    .line 864
    invoke-direct {p0, v7}, Landroid/content/SyncStorageEngine;->appendPendingOperationLocked(Landroid/content/SyncStorageEngine$PendingOperation;)V

    #@26
    .line 866
    iget v0, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@28
    invoke-direct {p0, v0}, Landroid/content/SyncStorageEngine;->getOrCreateSyncStatusLocked(I)Landroid/content/SyncStatusInfo;

    #@2b
    move-result-object v8

    #@2c
    .line 867
    .local v8, status:Landroid/content/SyncStatusInfo;
    const/4 v0, 0x1

    #@2d
    iput-boolean v0, v8, Landroid/content/SyncStatusInfo;->pending:Z

    #@2f
    .line 868
    monitor-exit v9
    :try_end_30
    .catchall {:try_start_1a .. :try_end_30} :catchall_39

    #@30
    .line 870
    const/4 v0, 0x2

    #@31
    invoke-direct {p0, v0}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@34
    move-object p1, v7

    #@35
    .line 871
    .end local v7           #op:Landroid/content/SyncStorageEngine$PendingOperation;
    .restart local p1
    goto :goto_14

    #@36
    .line 868
    .end local v6           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v8           #status:Landroid/content/SyncStatusInfo;
    :catchall_36
    move-exception v0

    #@37
    :goto_37
    :try_start_37
    monitor-exit v9
    :try_end_38
    .catchall {:try_start_37 .. :try_end_38} :catchall_36

    #@38
    throw v0

    #@39
    .end local p1
    .restart local v6       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v7       #op:Landroid/content/SyncStorageEngine$PendingOperation;
    :catchall_39
    move-exception v0

    #@3a
    move-object p1, v7

    #@3b
    .end local v7           #op:Landroid/content/SyncStorageEngine$PendingOperation;
    .restart local p1
    goto :goto_37
.end method

.method public insertStartSyncEvent(Landroid/accounts/Account;ILjava/lang/String;JIZ)J
    .registers 15
    .parameter "accountName"
    .parameter "userId"
    .parameter "authorityName"
    .parameter "now"
    .parameter "source"
    .parameter "initialization"

    #@0
    .prologue
    .line 1057
    iget-object v5, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v5

    #@3
    .line 1062
    :try_start_3
    const-string v4, "insertStartSyncEvent"

    #@5
    invoke-direct {p0, p1, p2, p3, v4}, Landroid/content/SyncStorageEngine;->getAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Ljava/lang/String;)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@8
    move-result-object v0

    #@9
    .line 1064
    .local v0, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-nez v0, :cond_f

    #@b
    .line 1065
    const-wide/16 v1, -0x1

    #@d
    monitor-exit v5

    #@e
    .line 1084
    :goto_e
    return-wide v1

    #@f
    .line 1067
    :cond_f
    new-instance v3, Landroid/content/SyncStorageEngine$SyncHistoryItem;

    #@11
    invoke-direct {v3}, Landroid/content/SyncStorageEngine$SyncHistoryItem;-><init>()V

    #@14
    .line 1068
    .local v3, item:Landroid/content/SyncStorageEngine$SyncHistoryItem;
    iput-boolean p7, v3, Landroid/content/SyncStorageEngine$SyncHistoryItem;->initialization:Z

    #@16
    .line 1069
    iget v4, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->ident:I

    #@18
    iput v4, v3, Landroid/content/SyncStorageEngine$SyncHistoryItem;->authorityId:I

    #@1a
    .line 1070
    iget v4, p0, Landroid/content/SyncStorageEngine;->mNextHistoryId:I

    #@1c
    add-int/lit8 v6, v4, 0x1

    #@1e
    iput v6, p0, Landroid/content/SyncStorageEngine;->mNextHistoryId:I

    #@20
    iput v4, v3, Landroid/content/SyncStorageEngine$SyncHistoryItem;->historyId:I

    #@22
    .line 1071
    iget v4, p0, Landroid/content/SyncStorageEngine;->mNextHistoryId:I

    #@24
    if-gez v4, :cond_29

    #@26
    const/4 v4, 0x0

    #@27
    iput v4, p0, Landroid/content/SyncStorageEngine;->mNextHistoryId:I

    #@29
    .line 1072
    :cond_29
    iput-wide p4, v3, Landroid/content/SyncStorageEngine$SyncHistoryItem;->eventTime:J

    #@2b
    .line 1073
    iput p6, v3, Landroid/content/SyncStorageEngine$SyncHistoryItem;->source:I

    #@2d
    .line 1074
    const/4 v4, 0x0

    #@2e
    iput v4, v3, Landroid/content/SyncStorageEngine$SyncHistoryItem;->event:I

    #@30
    .line 1075
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@32
    const/4 v6, 0x0

    #@33
    invoke-virtual {v4, v6, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@36
    .line 1076
    :goto_36
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v4

    #@3c
    const/16 v6, 0x64

    #@3e
    if-le v4, v6, :cond_51

    #@40
    .line 1077
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@42
    iget-object v6, p0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@44
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@47
    move-result v6

    #@48
    add-int/lit8 v6, v6, -0x1

    #@4a
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@4d
    goto :goto_36

    #@4e
    .line 1081
    .end local v0           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v3           #item:Landroid/content/SyncStorageEngine$SyncHistoryItem;
    :catchall_4e
    move-exception v4

    #@4f
    monitor-exit v5
    :try_end_50
    .catchall {:try_start_3 .. :try_end_50} :catchall_4e

    #@50
    throw v4

    #@51
    .line 1079
    .restart local v0       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .restart local v3       #item:Landroid/content/SyncStorageEngine$SyncHistoryItem;
    :cond_51
    :try_start_51
    iget v4, v3, Landroid/content/SyncStorageEngine$SyncHistoryItem;->historyId:I

    #@53
    int-to-long v1, v4

    #@54
    .line 1081
    .local v1, id:J
    monitor-exit v5
    :try_end_55
    .catchall {:try_start_51 .. :try_end_55} :catchall_4e

    #@55
    .line 1083
    const/16 v4, 0x8

    #@57
    invoke-direct {p0, v4}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@5a
    goto :goto_e
.end method

.method public isSyncActive(Landroid/accounts/Account;ILjava/lang/String;)Z
    .registers 9
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    .line 829
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v4

    #@3
    .line 830
    :try_start_3
    invoke-virtual {p0, p2}, Landroid/content/SyncStorageEngine;->getCurrentSyncs(I)Ljava/util/List;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_36

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/content/SyncInfo;

    #@17
    .line 831
    .local v2, syncInfo:Landroid/content/SyncInfo;
    iget v3, v2, Landroid/content/SyncInfo;->authorityId:I

    #@19
    invoke-virtual {p0, v3}, Landroid/content/SyncStorageEngine;->getAuthority(I)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@1c
    move-result-object v0

    #@1d
    .line 832
    .local v0, ainfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz v0, :cond_b

    #@1f
    iget-object v3, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@21
    invoke-virtual {v3, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_b

    #@27
    iget-object v3, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@29
    invoke-virtual {v3, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_b

    #@2f
    iget v3, v0, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@31
    if-ne v3, p2, :cond_b

    #@33
    .line 835
    const/4 v3, 0x1

    #@34
    monitor-exit v4

    #@35
    .line 840
    .end local v0           #ainfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v2           #syncInfo:Landroid/content/SyncInfo;
    :goto_35
    return v3

    #@36
    .line 838
    :cond_36
    monitor-exit v4

    #@37
    .line 840
    const/4 v3, 0x0

    #@38
    goto :goto_35

    #@39
    .line 838
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_39
    move-exception v3

    #@3a
    monitor-exit v4
    :try_end_3b
    .catchall {:try_start_3 .. :try_end_3b} :catchall_39

    #@3b
    throw v3
.end method

.method public isSyncPending(Landroid/accounts/Account;ILjava/lang/String;)Z
    .registers 11
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    .line 1293
    iget-object v5, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v5

    #@3
    .line 1294
    :try_start_3
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    #@8
    move-result v0

    #@9
    .line 1295
    .local v0, N:I
    const/4 v3, 0x0

    #@a
    .local v3, i:I
    :goto_a
    if-ge v3, v0, :cond_40

    #@c
    .line 1296
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mSyncStatus:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Landroid/content/SyncStatusInfo;

    #@14
    .line 1297
    .local v2, cur:Landroid/content/SyncStatusInfo;
    iget-object v4, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@16
    iget v6, v2, Landroid/content/SyncStatusInfo;->authorityId:I

    #@18
    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@1e
    .line 1298
    .local v1, ainfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-nez v1, :cond_23

    #@20
    .line 1295
    :cond_20
    add-int/lit8 v3, v3, 0x1

    #@22
    goto :goto_a

    #@23
    .line 1301
    :cond_23
    iget v4, v1, Landroid/content/SyncStorageEngine$AuthorityInfo;->userId:I

    #@25
    if-ne p2, v4, :cond_20

    #@27
    .line 1304
    if-eqz p1, :cond_31

    #@29
    iget-object v4, v1, Landroid/content/SyncStorageEngine$AuthorityInfo;->account:Landroid/accounts/Account;

    #@2b
    invoke-virtual {v4, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v4

    #@2f
    if-eqz v4, :cond_20

    #@31
    .line 1307
    :cond_31
    iget-object v4, v1, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@33
    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_20

    #@39
    iget-boolean v4, v2, Landroid/content/SyncStatusInfo;->pending:Z

    #@3b
    if-eqz v4, :cond_20

    #@3d
    .line 1308
    const/4 v4, 0x1

    #@3e
    monitor-exit v5

    #@3f
    .line 1311
    .end local v1           #ainfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v2           #cur:Landroid/content/SyncStatusInfo;
    :goto_3f
    return v4

    #@40
    :cond_40
    const/4 v4, 0x0

    #@41
    monitor-exit v5

    #@42
    goto :goto_3f

    #@43
    .line 1312
    .end local v0           #N:I
    .end local v3           #i:I
    :catchall_43
    move-exception v4

    #@44
    monitor-exit v5
    :try_end_45
    .catchall {:try_start_3 .. :try_end_45} :catchall_43

    #@45
    throw v4
.end method

.method public removeActiveSync(Landroid/content/SyncInfo;I)V
    .registers 5
    .parameter "syncInfo"
    .parameter "userId"

    #@0
    .prologue
    .line 1032
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 1038
    :try_start_3
    invoke-virtual {p0, p2}, Landroid/content/SyncStorageEngine;->getCurrentSyncs(I)Ljava/util/List;

    #@6
    move-result-object v0

    #@7
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@a
    .line 1039
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_f

    #@b
    .line 1041
    invoke-virtual {p0}, Landroid/content/SyncStorageEngine;->reportActiveChange()V

    #@e
    .line 1042
    return-void

    #@f
    .line 1039
    :catchall_f
    move-exception v0

    #@10
    :try_start_10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public removeAuthority(Landroid/accounts/Account;ILjava/lang/String;)V
    .registers 6
    .parameter "account"
    .parameter "userId"
    .parameter "authority"

    #@0
    .prologue
    .line 813
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 814
    const/4 v0, 0x1

    #@4
    :try_start_4
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/content/SyncStorageEngine;->removeAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;Z)V

    #@7
    .line 815
    monitor-exit v1

    #@8
    .line 816
    return-void

    #@9
    .line 815
    :catchall_9
    move-exception v0

    #@a
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public removePeriodicSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 13
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"
    .parameter "extras"

    #@0
    .prologue
    .line 762
    const-wide/16 v5, 0x0

    #@2
    const/4 v7, 0x0

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move v2, p2

    #@6
    move-object v3, p3

    #@7
    move-object v4, p4

    #@8
    invoke-direct/range {v0 .. v7}, Landroid/content/SyncStorageEngine;->updateOrRemovePeriodicSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;JZ)V

    #@b
    .line 764
    return-void
.end method

.method public removeStatusChangeListener(Landroid/content/ISyncStatusObserver;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 424
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 425
    :try_start_3
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mChangeListeners:Landroid/os/RemoteCallbackList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@8
    .line 426
    monitor-exit v1

    #@9
    .line 427
    return-void

    #@a
    .line 426
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public reportActiveChange()V
    .registers 2

    #@0
    .prologue
    .line 1048
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@4
    .line 1049
    return-void
.end method

.method public setBackoff(Landroid/accounts/Account;ILjava/lang/String;JJ)V
    .registers 23
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"
    .parameter "nextSyncTime"
    .parameter "nextDelay"

    #@0
    .prologue
    .line 581
    const/4 v11, 0x0

    #@1
    .line 582
    .local v11, changed:Z
    iget-object v14, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@3
    monitor-enter v14

    #@4
    .line 583
    if-eqz p1, :cond_8

    #@6
    if-nez p3, :cond_6c

    #@8
    .line 584
    :cond_8
    :try_start_8
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mAccounts:Ljava/util/HashMap;

    #@a
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@d
    move-result-object v2

    #@e
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v12

    #@12
    :cond_12
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_90

    #@18
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v8

    #@1c
    check-cast v8, Landroid/content/SyncStorageEngine$AccountInfo;

    #@1e
    .line 585
    .local v8, accountInfo:Landroid/content/SyncStorageEngine$AccountInfo;
    if-eqz p1, :cond_34

    #@20
    iget-object v2, v8, Landroid/content/SyncStorageEngine$AccountInfo;->accountAndUser:Landroid/accounts/AccountAndUser;

    #@22
    iget-object v2, v2, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@24
    move-object/from16 v0, p1

    #@26
    invoke-virtual {v0, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v2

    #@2a
    if-nez v2, :cond_34

    #@2c
    iget-object v2, v8, Landroid/content/SyncStorageEngine$AccountInfo;->accountAndUser:Landroid/accounts/AccountAndUser;

    #@2e
    iget v2, v2, Landroid/accounts/AccountAndUser;->userId:I

    #@30
    move/from16 v0, p2

    #@32
    if-ne v0, v2, :cond_12

    #@34
    .line 589
    :cond_34
    iget-object v2, v8, Landroid/content/SyncStorageEngine$AccountInfo;->authorities:Ljava/util/HashMap;

    #@36
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@39
    move-result-object v2

    #@3a
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3d
    move-result-object v13

    #@3e
    .local v13, i$:Ljava/util/Iterator;
    :cond_3e
    :goto_3e
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@41
    move-result v2

    #@42
    if-eqz v2, :cond_12

    #@44
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@47
    move-result-object v10

    #@48
    check-cast v10, Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@4a
    .line 590
    .local v10, authorityInfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    if-eqz p3, :cond_56

    #@4c
    iget-object v2, v10, Landroid/content/SyncStorageEngine$AuthorityInfo;->authority:Ljava/lang/String;

    #@4e
    move-object/from16 v0, p3

    #@50
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v2

    #@54
    if-eqz v2, :cond_3e

    #@56
    .line 593
    :cond_56
    iget-wide v2, v10, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@58
    cmp-long v2, v2, p4

    #@5a
    if-nez v2, :cond_62

    #@5c
    iget-wide v2, v10, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@5e
    cmp-long v2, v2, p6

    #@60
    if-eqz v2, :cond_3e

    #@62
    .line 595
    :cond_62
    move-wide/from16 v0, p4

    #@64
    iput-wide v0, v10, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@66
    .line 596
    move-wide/from16 v0, p6

    #@68
    iput-wide v0, v10, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@6a
    .line 597
    const/4 v11, 0x1

    #@6b
    goto :goto_3e

    #@6c
    .line 602
    .end local v8           #accountInfo:Landroid/content/SyncStorageEngine$AccountInfo;
    .end local v10           #authorityInfo:Landroid/content/SyncStorageEngine$AuthorityInfo;
    .end local v13           #i$:Ljava/util/Iterator;
    :cond_6c
    const/4 v6, -0x1

    #@6d
    const/4 v7, 0x1

    #@6e
    move-object v2, p0

    #@6f
    move-object/from16 v3, p1

    #@71
    move/from16 v4, p2

    #@73
    move-object/from16 v5, p3

    #@75
    invoke-direct/range {v2 .. v7}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@78
    move-result-object v9

    #@79
    .line 605
    .local v9, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    iget-wide v2, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@7b
    cmp-long v2, v2, p4

    #@7d
    if-nez v2, :cond_87

    #@7f
    iget-wide v2, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@81
    cmp-long v2, v2, p6

    #@83
    if-nez v2, :cond_87

    #@85
    .line 606
    monitor-exit v14

    #@86
    .line 617
    .end local v9           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_86
    :goto_86
    return-void

    #@87
    .line 608
    .restart local v9       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_87
    move-wide/from16 v0, p4

    #@89
    iput-wide v0, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffTime:J

    #@8b
    .line 609
    move-wide/from16 v0, p6

    #@8d
    iput-wide v0, v9, Landroid/content/SyncStorageEngine$AuthorityInfo;->backoffDelay:J

    #@8f
    .line 610
    const/4 v11, 0x1

    #@90
    .line 612
    .end local v9           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_90
    monitor-exit v14
    :try_end_91
    .catchall {:try_start_8 .. :try_end_91} :catchall_98

    #@91
    .line 614
    if-eqz v11, :cond_86

    #@93
    .line 615
    const/4 v2, 0x1

    #@94
    invoke-direct {p0, v2}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@97
    goto :goto_86

    #@98
    .line 612
    :catchall_98
    move-exception v2

    #@99
    :try_start_99
    monitor-exit v14
    :try_end_9a
    .catchall {:try_start_99 .. :try_end_9a} :catchall_98

    #@9a
    throw v2
.end method

.method public setDelayUntilTime(Landroid/accounts/Account;ILjava/lang/String;J)V
    .registers 15
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"
    .parameter "delayUntil"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 657
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@3
    monitor-enter v7

    #@4
    .line 658
    const/4 v4, -0x1

    #@5
    const/4 v5, 0x1

    #@6
    move-object v0, p0

    #@7
    move-object v1, p1

    #@8
    move v2, p2

    #@9
    move-object v3, p3

    #@a
    :try_start_a
    invoke-direct/range {v0 .. v5}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@d
    move-result-object v6

    #@e
    .line 660
    .local v6, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    iget-wide v0, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->delayUntil:J

    #@10
    cmp-long v0, v0, p4

    #@12
    if-nez v0, :cond_16

    #@14
    .line 661
    monitor-exit v7

    #@15
    .line 667
    :goto_15
    return-void

    #@16
    .line 663
    :cond_16
    iput-wide p4, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->delayUntil:J

    #@18
    .line 664
    monitor-exit v7
    :try_end_19
    .catchall {:try_start_a .. :try_end_19} :catchall_1d

    #@19
    .line 666
    invoke-direct {p0, v8}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@1c
    goto :goto_15

    #@1d
    .line 664
    .end local v6           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :catchall_1d
    move-exception v0

    #@1e
    :try_start_1e
    monitor-exit v7
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method public setIsSyncable(Landroid/accounts/Account;ILjava/lang/String;I)V
    .registers 14
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"
    .parameter "syncable"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v0, -0x1

    #@2
    .line 535
    if-le p4, v8, :cond_18

    #@4
    .line 536
    const/4 p4, 0x1

    #@5
    .line 544
    :cond_5
    :goto_5
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@7
    monitor-enter v7

    #@8
    .line 545
    const/4 v4, -0x1

    #@9
    const/4 v5, 0x0

    #@a
    move-object v0, p0

    #@b
    move-object v1, p1

    #@c
    move v2, p2

    #@d
    move-object v3, p3

    #@e
    :try_start_e
    invoke-direct/range {v0 .. v5}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@11
    move-result-object v6

    #@12
    .line 547
    .local v6, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    iget v0, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@14
    if-ne v0, p4, :cond_1c

    #@16
    .line 551
    monitor-exit v7

    #@17
    .line 561
    :goto_17
    return-void

    #@18
    .line 537
    .end local v6           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_18
    if-ge p4, v0, :cond_5

    #@1a
    .line 538
    const/4 p4, -0x1

    #@1b
    goto :goto_5

    #@1c
    .line 553
    .restart local v6       #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :cond_1c
    iput p4, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->syncable:I

    #@1e
    .line 554
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@21
    .line 555
    monitor-exit v7
    :try_end_22
    .catchall {:try_start_e .. :try_end_22} :catchall_30

    #@22
    .line 557
    if-lez p4, :cond_2c

    #@24
    .line 558
    new-instance v0, Landroid/os/Bundle;

    #@26
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@29
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/content/SyncStorageEngine;->requestSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V

    #@2c
    .line 560
    :cond_2c
    invoke-direct {p0, v8}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@2f
    goto :goto_17

    #@30
    .line 555
    .end local v6           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :catchall_30
    move-exception v0

    #@31
    :try_start_31
    monitor-exit v7
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_30

    #@32
    throw v0
.end method

.method public setMasterSyncAutomatically(ZI)V
    .registers 8
    .parameter "flag"
    .parameter "userId"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 782
    iget-object v2, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@3
    monitor-enter v2

    #@4
    .line 783
    :try_start_4
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mMasterSyncAutomatically:Landroid/util/SparseArray;

    #@6
    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/Boolean;

    #@c
    .line 784
    .local v0, auto:Ljava/lang/Boolean;
    if-eqz v0, :cond_16

    #@e
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@11
    move-result v1

    #@12
    if-ne v1, p1, :cond_16

    #@14
    .line 785
    monitor-exit v2

    #@15
    .line 795
    :goto_15
    return-void

    #@16
    .line 787
    :cond_16
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mMasterSyncAutomatically:Landroid/util/SparseArray;

    #@18
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v1, p2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1f
    .line 788
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@22
    .line 789
    monitor-exit v2
    :try_end_23
    .catchall {:try_start_4 .. :try_end_23} :catchall_39

    #@23
    .line 790
    if-eqz p1, :cond_2d

    #@25
    .line 791
    new-instance v1, Landroid/os/Bundle;

    #@27
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@2a
    invoke-direct {p0, v4, p2, v4, v1}, Landroid/content/SyncStorageEngine;->requestSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V

    #@2d
    .line 793
    :cond_2d
    const/4 v1, 0x1

    #@2e
    invoke-direct {p0, v1}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@31
    .line 794
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mContext:Landroid/content/Context;

    #@33
    sget-object v2, Landroid/content/SyncStorageEngine;->SYNC_CONNECTION_SETTING_CHANGED_INTENT:Landroid/content/Intent;

    #@35
    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@38
    goto :goto_15

    #@39
    .line 789
    .end local v0           #auto:Ljava/lang/Boolean;
    :catchall_39
    move-exception v1

    #@3a
    :try_start_3a
    monitor-exit v2
    :try_end_3b
    .catchall {:try_start_3a .. :try_end_3b} :catchall_39

    #@3b
    throw v1
.end method

.method protected setOnSyncRequestListener(Landroid/content/SyncStorageEngine$OnSyncRequestListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 396
    iget-object v0, p0, Landroid/content/SyncStorageEngine;->mSyncRequestListener:Landroid/content/SyncStorageEngine$OnSyncRequestListener;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 397
    iput-object p1, p0, Landroid/content/SyncStorageEngine;->mSyncRequestListener:Landroid/content/SyncStorageEngine$OnSyncRequestListener;

    #@6
    .line 399
    :cond_6
    return-void
.end method

.method public setSyncAutomatically(Landroid/accounts/Account;ILjava/lang/String;Z)V
    .registers 13
    .parameter "account"
    .parameter "userId"
    .parameter "providerName"
    .parameter "sync"

    #@0
    .prologue
    .line 492
    iget-object v7, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v7

    #@3
    .line 493
    const/4 v4, -0x1

    #@4
    const/4 v5, 0x0

    #@5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move v2, p2

    #@8
    move-object v3, p3

    #@9
    :try_start_9
    invoke-direct/range {v0 .. v5}, Landroid/content/SyncStorageEngine;->getOrCreateAuthorityLocked(Landroid/accounts/Account;ILjava/lang/String;IZ)Landroid/content/SyncStorageEngine$AuthorityInfo;

    #@c
    move-result-object v6

    #@d
    .line 495
    .local v6, authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    iget-boolean v0, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@f
    if-ne v0, p4, :cond_13

    #@11
    .line 499
    monitor-exit v7

    #@12
    .line 509
    :goto_12
    return-void

    #@13
    .line 501
    :cond_13
    iput-boolean p4, v6, Landroid/content/SyncStorageEngine$AuthorityInfo;->enabled:Z

    #@15
    .line 502
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeAccountInfoLocked()V

    #@18
    .line 503
    monitor-exit v7
    :try_end_19
    .catchall {:try_start_9 .. :try_end_19} :catchall_28

    #@19
    .line 505
    if-eqz p4, :cond_23

    #@1b
    .line 506
    new-instance v0, Landroid/os/Bundle;

    #@1d
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@20
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/content/SyncStorageEngine;->requestSync(Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V

    #@23
    .line 508
    :cond_23
    const/4 v0, 0x1

    #@24
    invoke-direct {p0, v0}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@27
    goto :goto_12

    #@28
    .line 503
    .end local v6           #authority:Landroid/content/SyncStorageEngine$AuthorityInfo;
    :catchall_28
    move-exception v0

    #@29
    :try_start_29
    monitor-exit v7
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_28

    #@2a
    throw v0
.end method

.method public stopSyncEvent(JJLjava/lang/String;JJ)V
    .registers 27
    .parameter "historyId"
    .parameter "elapsedTime"
    .parameter "resultMessage"
    .parameter "downstreamActivity"
    .parameter "upstreamActivity"

    #@0
    .prologue
    .line 1107
    move-object/from16 v0, p0

    #@2
    iget-object v12, v0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@4
    monitor-enter v12

    #@5
    .line 1111
    const/4 v5, 0x0

    #@6
    .line 1112
    .local v5, item:Landroid/content/SyncStorageEngine$SyncHistoryItem;
    :try_start_6
    move-object/from16 v0, p0

    #@8
    iget-object v11, v0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v4

    #@e
    .line 1113
    .local v4, i:I
    :goto_e
    if-lez v4, :cond_25

    #@10
    .line 1114
    add-int/lit8 v4, v4, -0x1

    #@12
    .line 1115
    move-object/from16 v0, p0

    #@14
    iget-object v11, v0, Landroid/content/SyncStorageEngine;->mSyncHistory:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v11

    #@1a
    move-object v0, v11

    #@1b
    check-cast v0, Landroid/content/SyncStorageEngine$SyncHistoryItem;

    #@1d
    move-object v5, v0

    #@1e
    .line 1116
    iget v11, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->historyId:I

    #@20
    int-to-long v13, v11

    #@21
    cmp-long v11, v13, p1

    #@23
    if-nez v11, :cond_44

    #@25
    .line 1122
    :cond_25
    if-nez v5, :cond_46

    #@27
    .line 1123
    const-string v11, "SyncManager"

    #@29
    new-instance v13, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string/jumbo v14, "stopSyncEvent: no history for id "

    #@31
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v13

    #@35
    move-wide/from16 v0, p1

    #@37
    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v13

    #@3b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v13

    #@3f
    invoke-static {v11, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 1124
    monitor-exit v12

    #@43
    .line 1211
    :goto_43
    return-void

    #@44
    .line 1119
    :cond_44
    const/4 v5, 0x0

    #@45
    goto :goto_e

    #@46
    .line 1127
    :cond_46
    move-wide/from16 v0, p3

    #@48
    iput-wide v0, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->elapsedTime:J

    #@4a
    .line 1128
    const/4 v11, 0x1

    #@4b
    iput v11, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->event:I

    #@4d
    .line 1129
    move-object/from16 v0, p5

    #@4f
    iput-object v0, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->mesg:Ljava/lang/String;

    #@51
    .line 1130
    move-wide/from16 v0, p6

    #@53
    iput-wide v0, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->downstreamActivity:J

    #@55
    .line 1131
    move-wide/from16 v0, p8

    #@57
    iput-wide v0, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->upstreamActivity:J

    #@59
    .line 1133
    iget v11, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->authorityId:I

    #@5b
    move-object/from16 v0, p0

    #@5d
    invoke-direct {v0, v11}, Landroid/content/SyncStorageEngine;->getOrCreateSyncStatusLocked(I)Landroid/content/SyncStatusInfo;

    #@60
    move-result-object v8

    #@61
    .line 1135
    .local v8, status:Landroid/content/SyncStatusInfo;
    iget v11, v8, Landroid/content/SyncStatusInfo;->numSyncs:I

    #@63
    add-int/lit8 v11, v11, 0x1

    #@65
    iput v11, v8, Landroid/content/SyncStatusInfo;->numSyncs:I

    #@67
    .line 1136
    iget-wide v13, v8, Landroid/content/SyncStatusInfo;->totalElapsedTime:J

    #@69
    add-long v13, v13, p3

    #@6b
    iput-wide v13, v8, Landroid/content/SyncStatusInfo;->totalElapsedTime:J

    #@6d
    .line 1137
    iget v11, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->source:I

    #@6f
    packed-switch v11, :pswitch_data_1bc

    #@72
    .line 1155
    :goto_72
    const/4 v9, 0x0

    #@73
    .line 1156
    .local v9, writeStatisticsNow:Z
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->getCurrentDayLocked()I

    #@76
    move-result v2

    #@77
    .line 1157
    .local v2, day:I
    move-object/from16 v0, p0

    #@79
    iget-object v11, v0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@7b
    const/4 v13, 0x0

    #@7c
    aget-object v11, v11, v13

    #@7e
    if-nez v11, :cond_112

    #@80
    .line 1158
    move-object/from16 v0, p0

    #@82
    iget-object v11, v0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@84
    const/4 v13, 0x0

    #@85
    new-instance v14, Landroid/content/SyncStorageEngine$DayStats;

    #@87
    invoke-direct {v14, v2}, Landroid/content/SyncStorageEngine$DayStats;-><init>(I)V

    #@8a
    aput-object v14, v11, v13

    #@8c
    .line 1165
    :cond_8c
    :goto_8c
    move-object/from16 v0, p0

    #@8e
    iget-object v11, v0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@90
    const/4 v13, 0x0

    #@91
    aget-object v3, v11, v13

    #@93
    .line 1167
    .local v3, ds:Landroid/content/SyncStorageEngine$DayStats;
    iget-wide v13, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->eventTime:J

    #@95
    add-long v6, v13, p3

    #@97
    .line 1168
    .local v6, lastSyncTime:J
    const/4 v10, 0x0

    #@98
    .line 1169
    .local v10, writeStatusNow:Z
    const-string/jumbo v11, "success"

    #@9b
    move-object/from16 v0, p5

    #@9d
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a0
    move-result v11

    #@a1
    if-eqz v11, :cond_153

    #@a3
    .line 1171
    iget-wide v13, v8, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    #@a5
    const-wide/16 v15, 0x0

    #@a7
    cmp-long v11, v13, v15

    #@a9
    if-eqz v11, :cond_b3

    #@ab
    iget-wide v13, v8, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    #@ad
    const-wide/16 v15, 0x0

    #@af
    cmp-long v11, v13, v15

    #@b1
    if-eqz v11, :cond_b4

    #@b3
    .line 1172
    :cond_b3
    const/4 v10, 0x1

    #@b4
    .line 1174
    :cond_b4
    iput-wide v6, v8, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    #@b6
    .line 1175
    iget v11, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->source:I

    #@b8
    iput v11, v8, Landroid/content/SyncStatusInfo;->lastSuccessSource:I

    #@ba
    .line 1176
    const-wide/16 v13, 0x0

    #@bc
    iput-wide v13, v8, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    #@be
    .line 1177
    const/4 v11, -0x1

    #@bf
    iput v11, v8, Landroid/content/SyncStatusInfo;->lastFailureSource:I

    #@c1
    .line 1178
    const/4 v11, 0x0

    #@c2
    iput-object v11, v8, Landroid/content/SyncStatusInfo;->lastFailureMesg:Ljava/lang/String;

    #@c4
    .line 1179
    const-wide/16 v13, 0x0

    #@c6
    iput-wide v13, v8, Landroid/content/SyncStatusInfo;->initialFailureTime:J

    #@c8
    .line 1180
    iget v11, v3, Landroid/content/SyncStorageEngine$DayStats;->successCount:I

    #@ca
    add-int/lit8 v11, v11, 0x1

    #@cc
    iput v11, v3, Landroid/content/SyncStorageEngine$DayStats;->successCount:I

    #@ce
    .line 1181
    iget-wide v13, v3, Landroid/content/SyncStorageEngine$DayStats;->successTime:J

    #@d0
    add-long v13, v13, p3

    #@d2
    iput-wide v13, v3, Landroid/content/SyncStorageEngine$DayStats;->successTime:J

    #@d4
    .line 1196
    :cond_d4
    :goto_d4
    if-eqz v10, :cond_188

    #@d6
    .line 1197
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->writeStatusLocked()V

    #@d9
    .line 1202
    :cond_d9
    :goto_d9
    if-eqz v9, :cond_1a2

    #@db
    .line 1203
    invoke-direct/range {p0 .. p0}, Landroid/content/SyncStorageEngine;->writeStatisticsLocked()V

    #@de
    .line 1208
    :cond_de
    :goto_de
    monitor-exit v12
    :try_end_df
    .catchall {:try_start_6 .. :try_end_df} :catchall_ef

    #@df
    .line 1210
    const/16 v11, 0x8

    #@e1
    move-object/from16 v0, p0

    #@e3
    invoke-direct {v0, v11}, Landroid/content/SyncStorageEngine;->reportChange(I)V

    #@e6
    goto/16 :goto_43

    #@e8
    .line 1139
    .end local v2           #day:I
    .end local v3           #ds:Landroid/content/SyncStorageEngine$DayStats;
    .end local v6           #lastSyncTime:J
    .end local v9           #writeStatisticsNow:Z
    .end local v10           #writeStatusNow:Z
    :pswitch_e8
    :try_start_e8
    iget v11, v8, Landroid/content/SyncStatusInfo;->numSourceLocal:I

    #@ea
    add-int/lit8 v11, v11, 0x1

    #@ec
    iput v11, v8, Landroid/content/SyncStatusInfo;->numSourceLocal:I

    #@ee
    goto :goto_72

    #@ef
    .line 1208
    .end local v4           #i:I
    .end local v8           #status:Landroid/content/SyncStatusInfo;
    :catchall_ef
    move-exception v11

    #@f0
    monitor-exit v12
    :try_end_f1
    .catchall {:try_start_e8 .. :try_end_f1} :catchall_ef

    #@f1
    throw v11

    #@f2
    .line 1142
    .restart local v4       #i:I
    .restart local v8       #status:Landroid/content/SyncStatusInfo;
    :pswitch_f2
    :try_start_f2
    iget v11, v8, Landroid/content/SyncStatusInfo;->numSourcePoll:I

    #@f4
    add-int/lit8 v11, v11, 0x1

    #@f6
    iput v11, v8, Landroid/content/SyncStatusInfo;->numSourcePoll:I

    #@f8
    goto/16 :goto_72

    #@fa
    .line 1145
    :pswitch_fa
    iget v11, v8, Landroid/content/SyncStatusInfo;->numSourceUser:I

    #@fc
    add-int/lit8 v11, v11, 0x1

    #@fe
    iput v11, v8, Landroid/content/SyncStatusInfo;->numSourceUser:I

    #@100
    goto/16 :goto_72

    #@102
    .line 1148
    :pswitch_102
    iget v11, v8, Landroid/content/SyncStatusInfo;->numSourceServer:I

    #@104
    add-int/lit8 v11, v11, 0x1

    #@106
    iput v11, v8, Landroid/content/SyncStatusInfo;->numSourceServer:I

    #@108
    goto/16 :goto_72

    #@10a
    .line 1151
    :pswitch_10a
    iget v11, v8, Landroid/content/SyncStatusInfo;->numSourcePeriodic:I

    #@10c
    add-int/lit8 v11, v11, 0x1

    #@10e
    iput v11, v8, Landroid/content/SyncStatusInfo;->numSourcePeriodic:I

    #@110
    goto/16 :goto_72

    #@112
    .line 1159
    .restart local v2       #day:I
    .restart local v9       #writeStatisticsNow:Z
    :cond_112
    move-object/from16 v0, p0

    #@114
    iget-object v11, v0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@116
    const/4 v13, 0x0

    #@117
    aget-object v11, v11, v13

    #@119
    iget v11, v11, Landroid/content/SyncStorageEngine$DayStats;->day:I

    #@11b
    if-eq v2, v11, :cond_148

    #@11d
    .line 1160
    move-object/from16 v0, p0

    #@11f
    iget-object v11, v0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@121
    const/4 v13, 0x0

    #@122
    move-object/from16 v0, p0

    #@124
    iget-object v14, v0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@126
    const/4 v15, 0x1

    #@127
    move-object/from16 v0, p0

    #@129
    iget-object v0, v0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@12b
    move-object/from16 v16, v0

    #@12d
    move-object/from16 v0, v16

    #@12f
    array-length v0, v0

    #@130
    move/from16 v16, v0

    #@132
    add-int/lit8 v16, v16, -0x1

    #@134
    move/from16 v0, v16

    #@136
    invoke-static {v11, v13, v14, v15, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@139
    .line 1161
    move-object/from16 v0, p0

    #@13b
    iget-object v11, v0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@13d
    const/4 v13, 0x0

    #@13e
    new-instance v14, Landroid/content/SyncStorageEngine$DayStats;

    #@140
    invoke-direct {v14, v2}, Landroid/content/SyncStorageEngine$DayStats;-><init>(I)V

    #@143
    aput-object v14, v11, v13

    #@145
    .line 1162
    const/4 v9, 0x1

    #@146
    goto/16 :goto_8c

    #@148
    .line 1163
    :cond_148
    move-object/from16 v0, p0

    #@14a
    iget-object v11, v0, Landroid/content/SyncStorageEngine;->mDayStats:[Landroid/content/SyncStorageEngine$DayStats;

    #@14c
    const/4 v13, 0x0

    #@14d
    aget-object v11, v11, v13

    #@14f
    if-nez v11, :cond_8c

    #@151
    goto/16 :goto_8c

    #@153
    .line 1182
    .restart local v3       #ds:Landroid/content/SyncStorageEngine$DayStats;
    .restart local v6       #lastSyncTime:J
    .restart local v10       #writeStatusNow:Z
    :cond_153
    const-string v11, "canceled"

    #@155
    move-object/from16 v0, p5

    #@157
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15a
    move-result v11

    #@15b
    if-nez v11, :cond_d4

    #@15d
    .line 1183
    iget-wide v13, v8, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    #@15f
    const-wide/16 v15, 0x0

    #@161
    cmp-long v11, v13, v15

    #@163
    if-nez v11, :cond_166

    #@165
    .line 1184
    const/4 v10, 0x1

    #@166
    .line 1186
    :cond_166
    iput-wide v6, v8, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    #@168
    .line 1187
    iget v11, v5, Landroid/content/SyncStorageEngine$SyncHistoryItem;->source:I

    #@16a
    iput v11, v8, Landroid/content/SyncStatusInfo;->lastFailureSource:I

    #@16c
    .line 1188
    move-object/from16 v0, p5

    #@16e
    iput-object v0, v8, Landroid/content/SyncStatusInfo;->lastFailureMesg:Ljava/lang/String;

    #@170
    .line 1189
    iget-wide v13, v8, Landroid/content/SyncStatusInfo;->initialFailureTime:J

    #@172
    const-wide/16 v15, 0x0

    #@174
    cmp-long v11, v13, v15

    #@176
    if-nez v11, :cond_17a

    #@178
    .line 1190
    iput-wide v6, v8, Landroid/content/SyncStatusInfo;->initialFailureTime:J

    #@17a
    .line 1192
    :cond_17a
    iget v11, v3, Landroid/content/SyncStorageEngine$DayStats;->failureCount:I

    #@17c
    add-int/lit8 v11, v11, 0x1

    #@17e
    iput v11, v3, Landroid/content/SyncStorageEngine$DayStats;->failureCount:I

    #@180
    .line 1193
    iget-wide v13, v3, Landroid/content/SyncStorageEngine$DayStats;->failureTime:J

    #@182
    add-long v13, v13, p3

    #@184
    iput-wide v13, v3, Landroid/content/SyncStorageEngine$DayStats;->failureTime:J

    #@186
    goto/16 :goto_d4

    #@188
    .line 1198
    :cond_188
    const/4 v11, 0x1

    #@189
    move-object/from16 v0, p0

    #@18b
    invoke-virtual {v0, v11}, Landroid/content/SyncStorageEngine;->hasMessages(I)Z

    #@18e
    move-result v11

    #@18f
    if-nez v11, :cond_d9

    #@191
    .line 1199
    const/4 v11, 0x1

    #@192
    move-object/from16 v0, p0

    #@194
    invoke-virtual {v0, v11}, Landroid/content/SyncStorageEngine;->obtainMessage(I)Landroid/os/Message;

    #@197
    move-result-object v11

    #@198
    const-wide/32 v13, 0x927c0

    #@19b
    move-object/from16 v0, p0

    #@19d
    invoke-virtual {v0, v11, v13, v14}, Landroid/content/SyncStorageEngine;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1a0
    goto/16 :goto_d9

    #@1a2
    .line 1204
    :cond_1a2
    const/4 v11, 0x2

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    invoke-virtual {v0, v11}, Landroid/content/SyncStorageEngine;->hasMessages(I)Z

    #@1a8
    move-result v11

    #@1a9
    if-nez v11, :cond_de

    #@1ab
    .line 1205
    const/4 v11, 0x2

    #@1ac
    move-object/from16 v0, p0

    #@1ae
    invoke-virtual {v0, v11}, Landroid/content/SyncStorageEngine;->obtainMessage(I)Landroid/os/Message;

    #@1b1
    move-result-object v11

    #@1b2
    const-wide/32 v13, 0x1b7740

    #@1b5
    move-object/from16 v0, p0

    #@1b7
    invoke-virtual {v0, v11, v13, v14}, Landroid/content/SyncStorageEngine;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1ba
    .catchall {:try_start_f2 .. :try_end_1ba} :catchall_ef

    #@1ba
    goto/16 :goto_de

    #@1bc
    .line 1137
    :pswitch_data_1bc
    .packed-switch 0x0
        :pswitch_102
        :pswitch_e8
        :pswitch_f2
        :pswitch_fa
        :pswitch_10a
    .end packed-switch
.end method

.method public writeAllState()V
    .registers 3

    #@0
    .prologue
    .line 1450
    iget-object v1, p0, Landroid/content/SyncStorageEngine;->mAuthorities:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 1453
    :try_start_3
    iget v0, p0, Landroid/content/SyncStorageEngine;->mNumPendingFinished:I

    #@5
    if-lez v0, :cond_a

    #@7
    .line 1455
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writePendingOperationsLocked()V

    #@a
    .line 1459
    :cond_a
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeStatusLocked()V

    #@d
    .line 1460
    invoke-direct {p0}, Landroid/content/SyncStorageEngine;->writeStatisticsLocked()V

    #@10
    .line 1461
    monitor-exit v1

    #@11
    .line 1462
    return-void

    #@12
    .line 1461
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method
