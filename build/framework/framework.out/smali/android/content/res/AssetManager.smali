.class public final Landroid/content/res/AssetManager;
.super Ljava/lang/Object;
.source "AssetManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/res/AssetManager$1;,
        Landroid/content/res/AssetManager$AssetInputStream;
    }
.end annotation


# static fields
.field public static final ACCESS_BUFFER:I = 0x3

.field public static final ACCESS_RANDOM:I = 0x1

.field public static final ACCESS_STREAMING:I = 0x2

.field public static final ACCESS_UNKNOWN:I = 0x0

.field private static final DEBUG_REFS:Z = false

.field static final STYLE_ASSET_COOKIE:I = 0x2

.field static final STYLE_CHANGING_CONFIGURATIONS:I = 0x4

.field static final STYLE_DATA:I = 0x1

.field static final STYLE_DENSITY:I = 0x5

.field static final STYLE_NUM_ENTRIES:I = 0x6

.field static final STYLE_RESOURCE_ID:I = 0x3

.field static final STYLE_TYPE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AssetManager"

.field private static final localLOGV:Z

.field private static final sSync:Ljava/lang/Object;

.field static sSystem:Landroid/content/res/AssetManager;


# instance fields
.field private mNObject:I

.field private mNumRefs:I

.field private mObject:I

.field private final mOffsets:[J

.field private mOpen:Z

.field private mRefStacks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/RuntimeException;",
            ">;"
        }
    .end annotation
.end field

.field private mStringBlocks:[Landroid/content/res/StringBlock;

.field private final mValue:Landroid/util/TypedValue;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 64
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/content/res/AssetManager;->sSync:Ljava/lang/Object;

    #@7
    .line 65
    const/4 v0, 0x0

    #@8
    sput-object v0, Landroid/content/res/AssetManager;->sSystem:Landroid/content/res/AssetManager;

    #@a
    return-void
.end method

.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 87
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 67
    new-instance v0, Landroid/util/TypedValue;

    #@6
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/res/AssetManager;->mValue:Landroid/util/TypedValue;

    #@b
    .line 68
    const/4 v0, 0x2

    #@c
    new-array v0, v0, [J

    #@e
    iput-object v0, p0, Landroid/content/res/AssetManager;->mOffsets:[J

    #@10
    .line 74
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@13
    .line 76
    iput v1, p0, Landroid/content/res/AssetManager;->mNumRefs:I

    #@15
    .line 77
    iput-boolean v1, p0, Landroid/content/res/AssetManager;->mOpen:Z

    #@17
    .line 88
    monitor-enter p0

    #@18
    .line 93
    :try_start_18
    invoke-direct {p0}, Landroid/content/res/AssetManager;->init()V

    #@1b
    .line 95
    invoke-static {}, Landroid/content/res/AssetManager;->ensureSystemAssets()V
	
	const-string v2, "/system/framework/lge-res.apk"
	
	invoke-virtual {p0, v2}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    #@1e
    .line 96
    monitor-exit p0

    #@1f
    .line 97
    return-void

    #@20
    .line 96
    :catchall_20
    move-exception v0

    #@21
    monitor-exit p0
    :try_end_22
    .catchall {:try_start_18 .. :try_end_22} :catchall_20

    #@22
    throw v0
.end method

.method private constructor <init>(Z)V
    .registers 5
    .parameter "isSystem"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 109
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 67
    new-instance v0, Landroid/util/TypedValue;

    #@6
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/res/AssetManager;->mValue:Landroid/util/TypedValue;

    #@b
    .line 68
    const/4 v0, 0x2

    #@c
    new-array v0, v0, [J

    #@e
    iput-object v0, p0, Landroid/content/res/AssetManager;->mOffsets:[J

    #@10
    .line 74
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@13
    .line 76
    iput v1, p0, Landroid/content/res/AssetManager;->mNumRefs:I

    #@15
    .line 77
    iput-boolean v1, p0, Landroid/content/res/AssetManager;->mOpen:Z

    #@17
    .line 116
    invoke-direct {p0}, Landroid/content/res/AssetManager;->init()V

	const-string v2, "/system/framework/lge-res.apk"
	
	invoke-virtual {p0, v2}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I
	
    #@1a
    .line 118
    return-void
.end method

.method static synthetic access$100(Landroid/content/res/AssetManager;I)J
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/content/res/AssetManager;->getAssetLength(I)J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method static synthetic access$200(Landroid/content/res/AssetManager;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/content/res/AssetManager;->readAssetChar(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Landroid/content/res/AssetManager;I)J
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/content/res/AssetManager;->getAssetRemainingLength(I)J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method static synthetic access$400(Landroid/content/res/AssetManager;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/content/res/AssetManager;->destroyAsset(I)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/content/res/AssetManager;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/content/res/AssetManager;->decRefsLocked(I)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/content/res/AssetManager;IJI)J
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/content/res/AssetManager;->seekAsset(IJI)J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method static synthetic access$700(Landroid/content/res/AssetManager;I[BII)I
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/content/res/AssetManager;->readAsset(I[BII)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static final native applyStyle(IIII[I[I[I)Z
.end method

.method static final native applyThemeStyle(IIZ)V
.end method

.method static final native copyTheme(II)V
.end method

.method private final decRefsLocked(I)V
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 788
    iget v0, p0, Landroid/content/res/AssetManager;->mNumRefs:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Landroid/content/res/AssetManager;->mNumRefs:I

    #@6
    .line 791
    iget v0, p0, Landroid/content/res/AssetManager;->mNumRefs:I

    #@8
    if-nez v0, :cond_d

    #@a
    .line 792
    invoke-direct {p0}, Landroid/content/res/AssetManager;->destroy()V

    #@d
    .line 794
    :cond_d
    return-void
.end method

.method private final native deleteTheme(I)V
.end method

.method private final native destroy()V
.end method

.method private final native destroyAsset(I)V
.end method

.method private final native addRedirectionsNative(I)V
.end method

.method private final native clearRedirectionsNative()V
.end method

.method public final native generateStyleRedirections(III)Z
.end method

.method public final native detachThemePath(Ljava/lang/String;I)Z
.end method

.method public final native attachThemePath(Ljava/lang/String;)I
.end method

.method static final native dumpTheme(IILjava/lang/String;Ljava/lang/String;)V
.end method

.method private static ensureSystemAssets()V
    .registers 4

    #@0
    .prologue
    .line 100
    sget-object v2, Landroid/content/res/AssetManager;->sSync:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 101
    :try_start_3
    sget-object v1, Landroid/content/res/AssetManager;->sSystem:Landroid/content/res/AssetManager;

    #@5
    if-nez v1, :cond_13

    #@7
    .line 102
    new-instance v0, Landroid/content/res/AssetManager;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-direct {v0, v1}, Landroid/content/res/AssetManager;-><init>(Z)V

    #@d
    .line 103
    .local v0, system:Landroid/content/res/AssetManager;
    const/4 v1, 0x0

    #@e
    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->makeStringBlocks(Z)V
	
    #@11
    .line 104
    sput-object v0, Landroid/content/res/AssetManager;->sSystem:Landroid/content/res/AssetManager;

    #@13
    .line 106
    :cond_13
    monitor-exit v2

    #@14
    .line 107
    return-void

    #@15
    .line 106
    :catchall_15
    move-exception v1

    #@16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v1
.end method

.method private final native getArrayStringInfo(I)[I
.end method

.method private final native getArrayStringResource(I)[Ljava/lang/String;
.end method

.method public static final native getAssetAllocations()Ljava/lang/String;
.end method

.method private final native getAssetLength(I)J
.end method

.method private final native getAssetRemainingLength(I)J
.end method

.method public static final native getGlobalAssetCount()I
.end method

.method public static final native getGlobalAssetManagerCount()I
.end method

.method private final native getNativeStringBlock(I)I
.end method

.method private final native getStringBlockCount()I
.end method

.method public static getSystem()Landroid/content/res/AssetManager;
    .registers 1

    #@0
    .prologue
    .line 126
    invoke-static {}, Landroid/content/res/AssetManager;->ensureSystemAssets()V

    #@3
    .line 127
    sget-object v0, Landroid/content/res/AssetManager;->sSystem:Landroid/content/res/AssetManager;

    #@5
    return-object v0
.end method

.method private final incRefsLocked(I)V
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 781
    iget v0, p0, Landroid/content/res/AssetManager;->mNumRefs:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/content/res/AssetManager;->mNumRefs:I

    #@6
    .line 782
    return-void
.end method

.method private final native init()V
.end method

.method private final native loadResourceBagValue(IILandroid/util/TypedValue;Z)I
.end method

.method private final native loadResourceValue(ISLandroid/util/TypedValue;Z)I
.end method

.method static final native loadThemeAttributeValue(IILandroid/util/TypedValue;Z)I
.end method

.method private final native newTheme()I
.end method

.method private final native openAsset(Ljava/lang/String;I)I
.end method

.method private final native openAssetFd(Ljava/lang/String;[J)Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private native openNonAssetFdNative(ILjava/lang/String;[J)Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private final native openNonAssetNative(ILjava/lang/String;I)I
.end method

.method private final native openXmlAssetNative(ILjava/lang/String;)I
.end method

.method private final native readAsset(I[BII)I
.end method

.method private final native readAssetChar(I)I
.end method

.method private final native seekAsset(IJI)J
.end method

# virtual methods
.method public final native addAssetPath(Ljava/lang/String;)I
.end method

.method public final addAssetPaths([Ljava/lang/String;)[I
    .registers 5
    .parameter "paths"

    #@0
    .prologue
    .line 623
    if-nez p1, :cond_4

    #@2
    .line 624
    const/4 v0, 0x0

    #@3
    .line 632
    :cond_3
    return-object v0

    #@4
    .line 627
    :cond_4
    array-length v2, p1

    #@5
    new-array v0, v2, [I

    #@7
    .line 628
    .local v0, cookies:[I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    array-length v2, p1

    #@9
    if-ge v1, v2, :cond_3

    #@b
    .line 629
    aget-object v2, p1, v1

    #@d
    invoke-virtual {p0, v2}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    #@10
    move-result v2

    #@11
    aput v2, v0, v1

    #@13
    .line 628
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_8
.end method

.method public final addOverlayPath(Ljava/lang/String;Ljava/lang/String;I)I
	.registers 4
	return p3
.end method

.method public close()V
    .registers 2

    #@0
    .prologue
    .line 134
    monitor-enter p0

    #@1
    .line 137
    :try_start_1
    iget-boolean v0, p0, Landroid/content/res/AssetManager;->mOpen:Z

    #@3
    if-eqz v0, :cond_f

    #@5
    .line 138
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/content/res/AssetManager;->mOpen:Z

    #@8
    .line 139
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@b
    move-result v0

    #@c
    invoke-direct {p0, v0}, Landroid/content/res/AssetManager;->decRefsLocked(I)V

    #@f
    .line 141
    :cond_f
    monitor-exit p0

    #@10
    .line 142
    return-void

    #@11
    .line 141
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method final createTheme()I
    .registers 4

    #@0
    .prologue
    .line 504
    monitor-enter p0

    #@1
    .line 505
    :try_start_1
    iget-boolean v1, p0, Landroid/content/res/AssetManager;->mOpen:Z

    #@3
    if-nez v1, :cond_10

    #@5
    .line 506
    new-instance v1, Ljava/lang/RuntimeException;

    #@7
    const-string v2, "Assetmanager has been closed"

    #@9
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1

    #@d
    .line 511
    :catchall_d
    move-exception v1

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v1

    #@10
    .line 508
    :cond_10
    :try_start_10
    invoke-direct {p0}, Landroid/content/res/AssetManager;->newTheme()I

    #@13
    move-result v0

    #@14
    .line 509
    .local v0, res:I
    invoke-direct {p0, v0}, Landroid/content/res/AssetManager;->incRefsLocked(I)V

    #@17
    .line 510
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_10 .. :try_end_18} :catchall_d

    #@18
    return v0
.end method

.method final ensureStringBlocks()V
    .registers 2

    #@0
    .prologue
    .line 246
    iget-object v0, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 247
    monitor-enter p0

    #@5
    .line 248
    :try_start_5
    iget-object v0, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@7
    if-nez v0, :cond_d

    #@9
    .line 249
    const/4 v0, 0x1

    #@a
    invoke-virtual {p0, v0}, Landroid/content/res/AssetManager;->makeStringBlocks(Z)V

    #@d
    .line 251
    :cond_d
    monitor-exit p0

    #@e
    .line 253
    :cond_e
    return-void

    #@f
    .line 251
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_5 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 532
    :try_start_0
    invoke-direct {p0}, Landroid/content/res/AssetManager;->destroy()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    #@3
    .line 534
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 536
    return-void

    #@7
    .line 534
    :catchall_7
    move-exception v0

    #@8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@b
    throw v0
.end method

.method final native getArrayIntResource(I)[I
.end method

.method final native getArraySize(I)I
.end method

.method public final native getBasePackageCount()I
.end method

.method public final native getBasePackageId(I)I
.end method

.method public final native getBasePackageName(I)Ljava/lang/String;
.end method

.method public final native getCookieName(I)Ljava/lang/String;
.end method

.method public final native getLocales()[Ljava/lang/String;
.end method

.method final getPooledString(II)Ljava/lang/CharSequence;
    .registers 5
    .parameter "block"
    .parameter "id"

    #@0
    .prologue
    .line 274
    iget-object v0, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@2
    add-int/lit8 v1, p1, -0x1

    #@4
    aget-object v0, v0, v1

    #@6
    invoke-virtual {v0, p2}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method final getResourceBagText(II)Ljava/lang/CharSequence;
    .registers 7
    .parameter "ident"
    .parameter "bagEntryId"

    #@0
    .prologue
    .line 167
    monitor-enter p0

    #@1
    .line 168
    :try_start_1
    iget-object v1, p0, Landroid/content/res/AssetManager;->mValue:Landroid/util/TypedValue;

    #@3
    .line 169
    .local v1, tmpValue:Landroid/util/TypedValue;
    const/4 v2, 0x1

    #@4
    invoke-direct {p0, p1, p2, v1, v2}, Landroid/content/res/AssetManager;->loadResourceBagValue(IILandroid/util/TypedValue;Z)I

    #@7
    move-result v0

    #@8
    .line 170
    .local v0, block:I
    if-ltz v0, :cond_24

    #@a
    .line 171
    iget v2, v1, Landroid/util/TypedValue;->type:I

    #@c
    const/4 v3, 0x3

    #@d
    if-ne v2, v3, :cond_1b

    #@f
    .line 172
    iget-object v2, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@11
    aget-object v2, v2, v0

    #@13
    iget v3, v1, Landroid/util/TypedValue;->data:I

    #@15
    invoke-virtual {v2, v3}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@18
    move-result-object v2

    #@19
    monitor-exit p0

    #@1a
    .line 177
    :goto_1a
    return-object v2

    #@1b
    .line 174
    :cond_1b
    invoke-virtual {v1}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@1e
    move-result-object v2

    #@1f
    monitor-exit p0

    #@20
    goto :goto_1a

    #@21
    .line 176
    .end local v0           #block:I
    .end local v1           #tmpValue:Landroid/util/TypedValue;
    :catchall_21
    move-exception v2

    #@22
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_21

    #@23
    throw v2

    #@24
    .restart local v0       #block:I
    .restart local v1       #tmpValue:Landroid/util/TypedValue;
    :cond_24
    :try_start_24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_21

    #@25
    .line 177
    const/4 v2, 0x0

    #@26
    goto :goto_1a
.end method

.method final native getResourceEntryName(I)Ljava/lang/String;
.end method

.method final native getResourceIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method final native getResourceName(I)Ljava/lang/String;
.end method

.method final native getResourcePackageName(I)Ljava/lang/String;
.end method

.method final getResourceStringArray(I)[Ljava/lang/String;
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 186
    invoke-direct {p0, p1}, Landroid/content/res/AssetManager;->getArrayStringResource(I)[Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 187
    .local v0, retArray:[Ljava/lang/String;
    return-object v0
.end method

.method final getResourceText(I)Ljava/lang/CharSequence;
    .registers 6
    .parameter "ident"

    #@0
    .prologue
    .line 149
    monitor-enter p0

    #@1
    .line 150
    :try_start_1
    iget-object v1, p0, Landroid/content/res/AssetManager;->mValue:Landroid/util/TypedValue;

    #@3
    .line 151
    .local v1, tmpValue:Landroid/util/TypedValue;
    const/4 v2, 0x0

    #@4
    const/4 v3, 0x1

    #@5
    invoke-direct {p0, p1, v2, v1, v3}, Landroid/content/res/AssetManager;->loadResourceValue(ISLandroid/util/TypedValue;Z)I

    #@8
    move-result v0

    #@9
    .line 152
    .local v0, block:I
    if-ltz v0, :cond_25

    #@b
    .line 153
    iget v2, v1, Landroid/util/TypedValue;->type:I

    #@d
    const/4 v3, 0x3

    #@e
    if-ne v2, v3, :cond_1c

    #@10
    .line 154
    iget-object v2, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@12
    aget-object v2, v2, v0

    #@14
    iget v3, v1, Landroid/util/TypedValue;->data:I

    #@16
    invoke-virtual {v2, v3}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@19
    move-result-object v2

    #@1a
    monitor-exit p0

    #@1b
    .line 159
    :goto_1b
    return-object v2

    #@1c
    .line 156
    :cond_1c
    invoke-virtual {v1}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@1f
    move-result-object v2

    #@20
    monitor-exit p0

    #@21
    goto :goto_1b

    #@22
    .line 158
    .end local v0           #block:I
    .end local v1           #tmpValue:Landroid/util/TypedValue;
    :catchall_22
    move-exception v2

    #@23
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_22

    #@24
    throw v2

    #@25
    .restart local v0       #block:I
    .restart local v1       #tmpValue:Landroid/util/TypedValue;
    :cond_25
    :try_start_25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_22

    #@26
    .line 159
    const/4 v2, 0x0

    #@27
    goto :goto_1b
.end method

.method final getResourceTextArray(I)[Ljava/lang/CharSequence;
    .registers 11
    .parameter "id"

    #@0
    .prologue
    .line 213
    invoke-direct {p0, p1}, Landroid/content/res/AssetManager;->getArrayStringInfo(I)[I

    #@3
    move-result-object v5

    #@4
    .line 214
    .local v5, rawInfoArray:[I
    array-length v6, v5

    #@5
    .line 215
    .local v6, rawInfoArrayLen:I
    div-int/lit8 v3, v6, 0x2

    #@7
    .line 218
    .local v3, infoArrayLen:I
    new-array v7, v3, [Ljava/lang/CharSequence;

    #@9
    .line 219
    .local v7, retArray:[Ljava/lang/CharSequence;
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    const/4 v4, 0x0

    #@b
    .local v4, j:I
    :goto_b
    if-ge v1, v6, :cond_26

    #@d
    .line 220
    aget v0, v5, v1

    #@f
    .line 221
    .local v0, block:I
    add-int/lit8 v8, v1, 0x1

    #@11
    aget v2, v5, v8

    #@13
    .line 222
    .local v2, index:I
    if-ltz v2, :cond_24

    #@15
    iget-object v8, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@17
    aget-object v8, v8, v0

    #@19
    invoke-virtual {v8, v2}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@1c
    move-result-object v8

    #@1d
    :goto_1d
    aput-object v8, v7, v4

    #@1f
    .line 219
    add-int/lit8 v1, v1, 0x2

    #@21
    add-int/lit8 v4, v4, 0x1

    #@23
    goto :goto_b

    #@24
    .line 222
    :cond_24
    const/4 v8, 0x0

    #@25
    goto :goto_1d

    #@26
    .line 224
    .end local v0           #block:I
    .end local v2           #index:I
    :cond_26
    return-object v7
.end method

.method final native getResourceTypeName(I)Ljava/lang/String;
.end method

.method private final native splitThemePackage(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method final getResourceValue(IILandroid/util/TypedValue;Z)Z
    .registers 9
    .parameter "ident"
    .parameter "density"
    .parameter "outValue"
    .parameter "resolveRefs"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 196
    int-to-short v2, p2

    #@2
    invoke-direct {p0, p1, v2, p3, p4}, Landroid/content/res/AssetManager;->loadResourceValue(ISLandroid/util/TypedValue;Z)I

    #@5
    move-result v0

    #@6
    .line 197
    .local v0, block:I
    if-ltz v0, :cond_1b

    #@8
    .line 198
    iget v2, p3, Landroid/util/TypedValue;->type:I

    #@a
    const/4 v3, 0x3

    #@b
    if-eq v2, v3, :cond_e

    #@d
    .line 204
    :goto_d
    return v1

    #@e
    .line 201
    :cond_e
    iget-object v2, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@10
    aget-object v2, v2, v0

    #@12
    iget v3, p3, Landroid/util/TypedValue;->data:I

    #@14
    invoke-virtual {v2, v3}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@17
    move-result-object v2

    #@18
    iput-object v2, p3, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@1a
    goto :goto_d

    #@1b
    .line 204
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_d
.end method

.method final getThemeValue(IILandroid/util/TypedValue;Z)Z
    .registers 10
    .parameter "theme"
    .parameter "ident"
    .parameter "outValue"
    .parameter "resolveRefs"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 229
    invoke-static {p1, p2, p3, p4}, Landroid/content/res/AssetManager;->loadThemeAttributeValue(IILandroid/util/TypedValue;Z)I

    #@4
    move-result v0

    #@5
    .line 230
    .local v0, block:I
    if-ltz v0, :cond_21

    #@7
    .line 231
    iget v3, p3, Landroid/util/TypedValue;->type:I

    #@9
    const/4 v4, 0x3

    #@a
    if-eq v3, v4, :cond_d

    #@c
    .line 242
    :goto_c
    return v2

    #@d
    .line 234
    :cond_d
    iget-object v1, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@f
    .line 235
    .local v1, blocks:[Landroid/content/res/StringBlock;
    if-nez v1, :cond_16

    #@11
    .line 236
    invoke-virtual {p0}, Landroid/content/res/AssetManager;->ensureStringBlocks()V

    #@14
    .line 237
    iget-object v1, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@16
    .line 239
    :cond_16
    aget-object v3, v1, v0

    #@18
    iget v4, p3, Landroid/util/TypedValue;->data:I

    #@1a
    invoke-virtual {v3, v4}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@1d
    move-result-object v3

    #@1e
    iput-object v3, p3, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@20
    goto :goto_c

    #@21
    .line 242
    .end local v1           #blocks:[Landroid/content/res/StringBlock;
    :cond_21
    const/4 v2, 0x0

    #@22
    goto :goto_c
.end method

.method public final native isUpToDate()Z
.end method

.method public final native list(Ljava/lang/String;)[Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method final makeStringBlocks(Z)V
    .registers 9
    .parameter "copyFromSystem"

    #@0
    .prologue
    .line 256
    if-eqz p1, :cond_21

    #@2
    sget-object v3, Landroid/content/res/AssetManager;->sSystem:Landroid/content/res/AssetManager;

    #@4
    iget-object v3, v3, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@6
    array-length v2, v3

    #@7
    .line 257
    .local v2, sysNum:I
    :goto_7
    invoke-direct {p0}, Landroid/content/res/AssetManager;->getStringBlockCount()I

    #@a
    move-result v1

    #@b
    .line 258
    .local v1, num:I
    new-array v3, v1, [Landroid/content/res/StringBlock;

    #@d
    iput-object v3, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@f
    .line 261
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    if-ge v0, v1, :cond_32

    #@12
    .line 262
    if-ge v0, v2, :cond_23

    #@14
    .line 263
    iget-object v3, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@16
    sget-object v4, Landroid/content/res/AssetManager;->sSystem:Landroid/content/res/AssetManager;

    #@18
    iget-object v4, v4, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@1a
    aget-object v4, v4, v0

    #@1c
    aput-object v4, v3, v0

    #@1e
    .line 261
    :goto_1e
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_10

    #@21
    .line 256
    .end local v0           #i:I
    .end local v1           #num:I
    .end local v2           #sysNum:I
    :cond_21
    const/4 v2, 0x0

    #@22
    goto :goto_7

    #@23
    .line 265
    .restart local v0       #i:I
    .restart local v1       #num:I
    .restart local v2       #sysNum:I
    :cond_23
    iget-object v3, p0, Landroid/content/res/AssetManager;->mStringBlocks:[Landroid/content/res/StringBlock;

    #@25
    new-instance v4, Landroid/content/res/StringBlock;

    #@27
    invoke-direct {p0, v0}, Landroid/content/res/AssetManager;->getNativeStringBlock(I)I

    #@2a
    move-result v5

    #@2b
    const/4 v6, 0x1

    #@2c
    invoke-direct {v4, v5, v6}, Landroid/content/res/StringBlock;-><init>(IZ)V

    #@2f
    aput-object v4, v3, v0

    #@31
    goto :goto_1e

    #@32
    .line 268
    :cond_32
    return-void
.end method

.method public final open(Ljava/lang/String;)Ljava/io/InputStream;
    .registers 3
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 289
    const/4 v0, 0x2

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public final open(Ljava/lang/String;I)Ljava/io/InputStream;
    .registers 8
    .parameter "fileName"
    .parameter "accessMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 311
    monitor-enter p0

    #@1
    .line 312
    :try_start_1
    iget-boolean v2, p0, Landroid/content/res/AssetManager;->mOpen:Z

    #@3
    if-nez v2, :cond_10

    #@5
    .line 313
    new-instance v2, Ljava/lang/RuntimeException;

    #@7
    const-string v3, "Assetmanager has been closed"

    #@9
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 321
    :catchall_d
    move-exception v2

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v2

    #@10
    .line 315
    :cond_10
    :try_start_10
    invoke-direct {p0, p1, p2}, Landroid/content/res/AssetManager;->openAsset(Ljava/lang/String;I)I

    #@13
    move-result v0

    #@14
    .line 316
    .local v0, asset:I
    if-eqz v0, :cond_25

    #@16
    .line 317
    new-instance v1, Landroid/content/res/AssetManager$AssetInputStream;

    #@18
    const/4 v2, 0x0

    #@19
    invoke-direct {v1, p0, v0, v2}, Landroid/content/res/AssetManager$AssetInputStream;-><init>(Landroid/content/res/AssetManager;ILandroid/content/res/AssetManager$1;)V

    #@1c
    .line 318
    .local v1, res:Landroid/content/res/AssetManager$AssetInputStream;
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    #@1f
    move-result v2

    #@20
    invoke-direct {p0, v2}, Landroid/content/res/AssetManager;->incRefsLocked(I)V

    #@23
    .line 319
    monitor-exit p0

    #@24
    return-object v1

    #@25
    .line 321
    .end local v1           #res:Landroid/content/res/AssetManager$AssetInputStream;
    :cond_25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_10 .. :try_end_26} :catchall_d

    #@26
    .line 322
    new-instance v2, Ljava/io/FileNotFoundException;

    #@28
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v4, "Asset file: "

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v2
.end method

.method public final openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .registers 8
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 327
    monitor-enter p0

    #@1
    .line 328
    :try_start_1
    iget-boolean v0, p0, Landroid/content/res/AssetManager;->mOpen:Z

    #@3
    if-nez v0, :cond_10

    #@5
    .line 329
    new-instance v0, Ljava/lang/RuntimeException;

    #@7
    const-string v2, "Assetmanager has been closed"

    #@9
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 335
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0

    #@10
    .line 331
    :cond_10
    :try_start_10
    iget-object v0, p0, Landroid/content/res/AssetManager;->mOffsets:[J

    #@12
    invoke-direct {p0, p1, v0}, Landroid/content/res/AssetManager;->openAssetFd(Ljava/lang/String;[J)Landroid/os/ParcelFileDescriptor;

    #@15
    move-result-object v1

    #@16
    .line 332
    .local v1, pfd:Landroid/os/ParcelFileDescriptor;
    if-eqz v1, :cond_29

    #@18
    .line 333
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    #@1a
    iget-object v2, p0, Landroid/content/res/AssetManager;->mOffsets:[J

    #@1c
    const/4 v3, 0x0

    #@1d
    aget-wide v2, v2, v3

    #@1f
    iget-object v4, p0, Landroid/content/res/AssetManager;->mOffsets:[J

    #@21
    const/4 v5, 0x1

    #@22
    aget-wide v4, v4, v5

    #@24
    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V

    #@27
    monitor-exit p0

    #@28
    return-object v0

    #@29
    .line 335
    :cond_29
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_10 .. :try_end_2a} :catchall_d

    #@2a
    .line 336
    new-instance v0, Ljava/io/FileNotFoundException;

    #@2c
    new-instance v2, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v3, "Asset file: "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-direct {v0, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@42
    throw v0
.end method

.method public final openNonAsset(ILjava/lang/String;)Ljava/io/InputStream;
    .registers 4
    .parameter "cookie"
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 390
    const/4 v0, 0x2

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/content/res/AssetManager;->openNonAsset(ILjava/lang/String;I)Ljava/io/InputStream;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public final openNonAsset(ILjava/lang/String;I)Ljava/io/InputStream;
    .registers 9
    .parameter "cookie"
    .parameter "fileName"
    .parameter "accessMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 403
    monitor-enter p0

    #@1
    .line 404
    :try_start_1
    iget-boolean v2, p0, Landroid/content/res/AssetManager;->mOpen:Z

    #@3
    if-nez v2, :cond_10

    #@5
    .line 405
    new-instance v2, Ljava/lang/RuntimeException;

    #@7
    const-string v3, "Assetmanager has been closed"

    #@9
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 413
    :catchall_d
    move-exception v2

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v2

    #@10
    .line 407
    :cond_10
    :try_start_10
    invoke-direct {p0, p1, p2, p3}, Landroid/content/res/AssetManager;->openNonAssetNative(ILjava/lang/String;I)I

    #@13
    move-result v0

    #@14
    .line 408
    .local v0, asset:I
    if-eqz v0, :cond_25

    #@16
    .line 409
    new-instance v1, Landroid/content/res/AssetManager$AssetInputStream;

    #@18
    const/4 v2, 0x0

    #@19
    invoke-direct {v1, p0, v0, v2}, Landroid/content/res/AssetManager$AssetInputStream;-><init>(Landroid/content/res/AssetManager;ILandroid/content/res/AssetManager$1;)V

    #@1c
    .line 410
    .local v1, res:Landroid/content/res/AssetManager$AssetInputStream;
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    #@1f
    move-result v2

    #@20
    invoke-direct {p0, v2}, Landroid/content/res/AssetManager;->incRefsLocked(I)V

    #@23
    .line 411
    monitor-exit p0

    #@24
    return-object v1

    #@25
    .line 413
    .end local v1           #res:Landroid/content/res/AssetManager$AssetInputStream;
    :cond_25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_10 .. :try_end_26} :catchall_d

    #@26
    .line 414
    new-instance v2, Ljava/io/FileNotFoundException;

    #@28
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v4, "Asset absolute file: "

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v2
.end method

.method public final openNonAsset(Ljava/lang/String;)Ljava/io/InputStream;
    .registers 4
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 364
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x2

    #@2
    invoke-virtual {p0, v0, p1, v1}, Landroid/content/res/AssetManager;->openNonAsset(ILjava/lang/String;I)Ljava/io/InputStream;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final openNonAsset(Ljava/lang/String;I)Ljava/io/InputStream;
    .registers 4
    .parameter "fileName"
    .parameter "accessMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 378
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1, p2}, Landroid/content/res/AssetManager;->openNonAsset(ILjava/lang/String;I)Ljava/io/InputStream;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public final openNonAssetFd(ILjava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .registers 9
    .parameter "cookie"
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 424
    monitor-enter p0

    #@1
    .line 425
    :try_start_1
    iget-boolean v0, p0, Landroid/content/res/AssetManager;->mOpen:Z

    #@3
    if-nez v0, :cond_10

    #@5
    .line 426
    new-instance v0, Ljava/lang/RuntimeException;

    #@7
    const-string v2, "Assetmanager has been closed"

    #@9
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 433
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0

    #@10
    .line 428
    :cond_10
    :try_start_10
    iget-object v0, p0, Landroid/content/res/AssetManager;->mOffsets:[J

    #@12
    invoke-direct {p0, p1, p2, v0}, Landroid/content/res/AssetManager;->openNonAssetFdNative(ILjava/lang/String;[J)Landroid/os/ParcelFileDescriptor;

    #@15
    move-result-object v1

    #@16
    .line 430
    .local v1, pfd:Landroid/os/ParcelFileDescriptor;
    if-eqz v1, :cond_29

    #@18
    .line 431
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    #@1a
    iget-object v2, p0, Landroid/content/res/AssetManager;->mOffsets:[J

    #@1c
    const/4 v3, 0x0

    #@1d
    aget-wide v2, v2, v3

    #@1f
    iget-object v4, p0, Landroid/content/res/AssetManager;->mOffsets:[J

    #@21
    const/4 v5, 0x1

    #@22
    aget-wide v4, v4, v5

    #@24
    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V

    #@27
    monitor-exit p0

    #@28
    return-object v0

    #@29
    .line 433
    :cond_29
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_10 .. :try_end_2a} :catchall_d

    #@2a
    .line 434
    new-instance v0, Ljava/io/FileNotFoundException;

    #@2c
    new-instance v2, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v3, "Asset absolute file: "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-direct {v0, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@42
    throw v0
.end method

.method public final openNonAssetFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .registers 3
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 419
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1}, Landroid/content/res/AssetManager;->openNonAssetFd(ILjava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method final openXmlBlockAsset(ILjava/lang/String;)Landroid/content/res/XmlBlock;
    .registers 8
    .parameter "cookie"
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 483
    monitor-enter p0

    #@1
    .line 484
    :try_start_1
    iget-boolean v2, p0, Landroid/content/res/AssetManager;->mOpen:Z

    #@3
    if-nez v2, :cond_10

    #@5
    .line 485
    new-instance v2, Ljava/lang/RuntimeException;

    #@7
    const-string v3, "Assetmanager has been closed"

    #@9
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 493
    :catchall_d
    move-exception v2

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v2

    #@10
    .line 487
    :cond_10
    :try_start_10
    invoke-direct {p0, p1, p2}, Landroid/content/res/AssetManager;->openXmlAssetNative(ILjava/lang/String;)I

    #@13
    move-result v1

    #@14
    .line 488
    .local v1, xmlBlock:I
    if-eqz v1, :cond_24

    #@16
    .line 489
    new-instance v0, Landroid/content/res/XmlBlock;

    #@18
    invoke-direct {v0, p0, v1}, Landroid/content/res/XmlBlock;-><init>(Landroid/content/res/AssetManager;I)V

    #@1b
    .line 490
    .local v0, res:Landroid/content/res/XmlBlock;
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    #@1e
    move-result v2

    #@1f
    invoke-direct {p0, v2}, Landroid/content/res/AssetManager;->incRefsLocked(I)V

    #@22
    .line 491
    monitor-exit p0

    #@23
    return-object v0

    #@24
    .line 493
    .end local v0           #res:Landroid/content/res/XmlBlock;
    :cond_24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_10 .. :try_end_25} :catchall_d

    #@25
    .line 494
    new-instance v2, Ljava/io/FileNotFoundException;

    #@27
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v4, "Asset XML file: "

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v2
.end method

.method final openXmlBlockAsset(Ljava/lang/String;)Landroid/content/res/XmlBlock;
    .registers 3
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 470
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1}, Landroid/content/res/AssetManager;->openXmlBlockAsset(ILjava/lang/String;)Landroid/content/res/XmlBlock;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public final openXmlResourceParser(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;
    .registers 5
    .parameter "cookie"
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 455
    invoke-virtual {p0, p1, p2}, Landroid/content/res/AssetManager;->openXmlBlockAsset(ILjava/lang/String;)Landroid/content/res/XmlBlock;

    #@3
    move-result-object v0

    #@4
    .line 456
    .local v0, block:Landroid/content/res/XmlBlock;
    invoke-virtual {v0}, Landroid/content/res/XmlBlock;->newParser()Landroid/content/res/XmlResourceParser;

    #@7
    move-result-object v1

    #@8
    .line 457
    .local v1, rp:Landroid/content/res/XmlResourceParser;
    invoke-virtual {v0}, Landroid/content/res/XmlBlock;->close()V

    #@b
    .line 458
    return-object v1
.end method

.method public final openXmlResourceParser(Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    .registers 3
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 444
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1}, Landroid/content/res/AssetManager;->openXmlResourceParser(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method final releaseTheme(I)V
    .registers 3
    .parameter "theme"

    #@0
    .prologue
    .line 515
    monitor-enter p0

    #@1
    .line 516
    :try_start_1
    invoke-direct {p0, p1}, Landroid/content/res/AssetManager;->deleteTheme(I)V

    #@4
    .line 517
    invoke-direct {p0, p1}, Landroid/content/res/AssetManager;->decRefsLocked(I)V

    #@7
    .line 518
    monitor-exit p0

    #@8
    .line 519
    return-void

    #@9
    .line 518
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method final native retrieveArray(I[I)I
.end method

.method final native retrieveAttributes(I[I[I[I)Z
.end method

.method public final native setConfiguration(IILjava/lang/String;IIIIIIIIIIIIII)V
.end method

.method public final native setLocale(Ljava/lang/String;)V
.end method

.method xmlBlockGone(I)V
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 498
    monitor-enter p0

    #@1
    .line 499
    :try_start_1
    invoke-direct {p0, p1}, Landroid/content/res/AssetManager;->decRefsLocked(I)V

    #@4
    .line 500
    monitor-exit p0

    #@5
    .line 501
    return-void

    #@6
    .line 500
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_6

    #@8
    throw v0
.end method
