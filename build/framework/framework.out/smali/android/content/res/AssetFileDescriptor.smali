.class public Landroid/content/res/AssetFileDescriptor;
.super Ljava/lang/Object;
.source "AssetFileDescriptor.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;,
        Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/res/AssetFileDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field public static final UNKNOWN_LENGTH:J = -0x1L


# instance fields
.field private final mFd:Landroid/os/ParcelFileDescriptor;

.field private final mLength:J

.field private final mStartOffset:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 327
    new-instance v0, Landroid/content/res/AssetFileDescriptor$1;

    #@2
    invoke-direct {v0}, Landroid/content/res/AssetFileDescriptor$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/res/AssetFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "src"

    #@0
    .prologue
    .line 321
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 322
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@5
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/os/ParcelFileDescriptor;

    #@b
    iput-object v0, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@d
    .line 323
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@10
    move-result-wide v0

    #@11
    iput-wide v0, p0, Landroid/content/res/AssetFileDescriptor;->mStartOffset:J

    #@13
    .line 324
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@16
    move-result-wide v0

    #@17
    iput-wide v0, p0, Landroid/content/res/AssetFileDescriptor;->mLength:J

    #@19
    .line 325
    return-void
.end method

.method public constructor <init>(Landroid/os/ParcelFileDescriptor;JJ)V
    .registers 9
    .parameter "fd"
    .parameter "startOffset"
    .parameter "length"

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 55
    if-nez p1, :cond_f

    #@7
    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v1, "fd must not be null"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 58
    :cond_f
    cmp-long v0, p4, v1

    #@11
    if-gez v0, :cond_20

    #@13
    cmp-long v0, p2, v1

    #@15
    if-eqz v0, :cond_20

    #@17
    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@19
    const-string/jumbo v1, "startOffset must be 0 when using UNKNOWN_LENGTH"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 62
    :cond_20
    iput-object p1, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@22
    .line 63
    iput-wide p2, p0, Landroid/content/res/AssetFileDescriptor;->mStartOffset:J

    #@24
    .line 64
    iput-wide p4, p0, Landroid/content/res/AssetFileDescriptor;->mLength:J

    #@26
    .line 65
    return-void
.end method


# virtual methods
.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@2
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V

    #@5
    .line 127
    return-void
.end method

.method public createInputStream()Ljava/io/FileInputStream;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 138
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor;->mLength:J

    #@2
    const-wide/16 v2, 0x0

    #@4
    cmp-long v0, v0, v2

    #@6
    if-gez v0, :cond_10

    #@8
    .line 139
    new-instance v0, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    #@a
    iget-object v1, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@c
    invoke-direct {v0, v1}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    #@f
    .line 141
    :goto_f
    return-object v0

    #@10
    :cond_10
    new-instance v0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;

    #@12
    invoke-direct {v0, p0}, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;-><init>(Landroid/content/res/AssetFileDescriptor;)V

    #@15
    goto :goto_f
.end method

.method public createOutputStream()Ljava/io/FileOutputStream;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 153
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor;->mLength:J

    #@2
    const-wide/16 v2, 0x0

    #@4
    cmp-long v0, v0, v2

    #@6
    if-gez v0, :cond_10

    #@8
    .line 154
    new-instance v0, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    #@a
    iget-object v1, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@c
    invoke-direct {v0, v1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    #@f
    .line 156
    :goto_f
    return-object v0

    #@10
    :cond_10
    new-instance v0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;

    #@12
    invoke-direct {v0, p0}, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/content/res/AssetFileDescriptor;)V

    #@15
    goto :goto_f
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 312
    iget-object v0, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@2
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->describeContents()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDeclaredLength()J
    .registers 3

    #@0
    .prologue
    .line 119
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor;->mLength:J

    #@2
    return-wide v0
.end method

.method public getFileDescriptor()Ljava/io/FileDescriptor;
    .registers 2

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@2
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLength()J
    .registers 7

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 103
    iget-wide v2, p0, Landroid/content/res/AssetFileDescriptor;->mLength:J

    #@4
    cmp-long v2, v2, v4

    #@6
    if-ltz v2, :cond_b

    #@8
    .line 104
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor;->mLength:J

    #@a
    .line 107
    :cond_a
    :goto_a
    return-wide v0

    #@b
    .line 106
    :cond_b
    iget-object v2, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@d
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    #@10
    move-result-wide v0

    #@11
    .line 107
    .local v0, len:J
    cmp-long v2, v0, v4

    #@13
    if-gez v2, :cond_a

    #@15
    const-wide/16 v0, -0x1

    #@17
    goto :goto_a
.end method

.method public getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .registers 2

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@2
    return-object v0
.end method

.method public getStartOffset()J
    .registers 3

    #@0
    .prologue
    .line 88
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor;->mStartOffset:J

    #@2
    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v1, "{AssetFileDescriptor: "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    iget-object v1, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, " start="

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    iget-wide v1, p0, Landroid/content/res/AssetFileDescriptor;->mStartOffset:J

    #@1a
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, " len="

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    iget-wide v1, p0, Landroid/content/res/AssetFileDescriptor;->mLength:J

    #@26
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    const-string/jumbo v1, "}"

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 316
    iget-object v0, p0, Landroid/content/res/AssetFileDescriptor;->mFd:Landroid/os/ParcelFileDescriptor;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@5
    .line 317
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor;->mStartOffset:J

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@a
    .line 318
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor;->mLength:J

    #@c
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@f
    .line 319
    return-void
.end method
