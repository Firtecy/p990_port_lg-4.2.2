.class public Landroid/content/res/TypedArray;
.super Ljava/lang/Object;
.source "TypedArray.java"


# instance fields
.field mData:[I

.field mIndices:[I

.field mLength:I

.field private final mResources:Landroid/content/res/Resources;

.field mRsrcs:[I

.field mValue:Landroid/util/TypedValue;

.field mXml:Landroid/content/res/XmlBlock$Parser;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;[I[II)V
    .registers 6
    .parameter "resources"
    .parameter "data"
    .parameter "indices"
    .parameter "len"

    #@0
    .prologue
    .line 730
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    new-instance v0, Landroid/util/TypedValue;

    #@5
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@8
    iput-object v0, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@a
    .line 731
    iput-object p1, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@c
    .line 732
    iput-object p2, p0, Landroid/content/res/TypedArray;->mData:[I

    #@e
    .line 733
    iput-object p3, p0, Landroid/content/res/TypedArray;->mIndices:[I

    #@10
    .line 734
    iput p4, p0, Landroid/content/res/TypedArray;->mLength:I

    #@12
    .line 735
    return-void
.end method

.method private getValueAt(ILandroid/util/TypedValue;)Z
    .registers 6
    .parameter "index"
    .parameter "outValue"

    #@0
    .prologue
    .line 700
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@2
    .line 701
    .local v0, data:[I
    add-int/lit8 v2, p1, 0x0

    #@4
    aget v1, v0, v2

    #@6
    .line 702
    .local v1, type:I
    if-nez v1, :cond_a

    #@8
    .line 703
    const/4 v2, 0x0

    #@9
    .line 712
    :goto_9
    return v2

    #@a
    .line 705
    :cond_a
    iput v1, p2, Landroid/util/TypedValue;->type:I

    #@c
    .line 706
    add-int/lit8 v2, p1, 0x1

    #@e
    aget v2, v0, v2

    #@10
    iput v2, p2, Landroid/util/TypedValue;->data:I

    #@12
    .line 707
    add-int/lit8 v2, p1, 0x2

    #@14
    aget v2, v0, v2

    #@16
    iput v2, p2, Landroid/util/TypedValue;->assetCookie:I

    #@18
    .line 708
    add-int/lit8 v2, p1, 0x3

    #@1a
    aget v2, v0, v2

    #@1c
    iput v2, p2, Landroid/util/TypedValue;->resourceId:I

    #@1e
    .line 709
    add-int/lit8 v2, p1, 0x4

    #@20
    aget v2, v0, v2

    #@22
    iput v2, p2, Landroid/util/TypedValue;->changingConfigurations:I

    #@24
    .line 710
    add-int/lit8 v2, p1, 0x5

    #@26
    aget v2, v0, v2

    #@28
    iput v2, p2, Landroid/util/TypedValue;->density:I

    #@2a
    .line 711
    const/4 v2, 0x3

    #@2b
    if-ne v1, v2, :cond_35

    #@2d
    invoke-direct {p0, p1}, Landroid/content/res/TypedArray;->loadStringValueAt(I)Ljava/lang/CharSequence;

    #@30
    move-result-object v2

    #@31
    :goto_31
    iput-object v2, p2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@33
    .line 712
    const/4 v2, 0x1

    #@34
    goto :goto_9

    #@35
    .line 711
    :cond_35
    const/4 v2, 0x0

    #@36
    goto :goto_31
.end method

.method private loadStringValueAt(I)Ljava/lang/CharSequence;
    .registers 6
    .parameter "index"

    #@0
    .prologue
    .line 716
    iget-object v1, p0, Landroid/content/res/TypedArray;->mData:[I

    #@2
    .line 717
    .local v1, data:[I
    add-int/lit8 v2, p1, 0x2

    #@4
    aget v0, v1, v2

    #@6
    .line 718
    .local v0, cookie:I
    if-gez v0, :cond_19

    #@8
    .line 719
    iget-object v2, p0, Landroid/content/res/TypedArray;->mXml:Landroid/content/res/XmlBlock$Parser;

    #@a
    if-eqz v2, :cond_17

    #@c
    .line 720
    iget-object v2, p0, Landroid/content/res/TypedArray;->mXml:Landroid/content/res/XmlBlock$Parser;

    #@e
    add-int/lit8 v3, p1, 0x1

    #@10
    aget v3, v1, v3

    #@12
    invoke-virtual {v2, v3}, Landroid/content/res/XmlBlock$Parser;->getPooledString(I)Ljava/lang/CharSequence;

    #@15
    move-result-object v2

    #@16
    .line 726
    :goto_16
    return-object v2

    #@17
    .line 723
    :cond_17
    const/4 v2, 0x0

    #@18
    goto :goto_16

    #@19
    .line 726
    :cond_19
    iget-object v2, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@1b
    iget-object v2, v2, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@1d
    add-int/lit8 v3, p1, 0x1

    #@1f
    aget v3, v1, v3

    #@21
    invoke-virtual {v2, v0, v3}, Landroid/content/res/AssetManager;->getPooledString(II)Ljava/lang/CharSequence;

    #@24
    move-result-object v2

    #@25
    goto :goto_16
.end method


# virtual methods
.method public getBoolean(IZ)Z
    .registers 9
    .parameter "index"
    .parameter "defValue"

    #@0
    .prologue
    .line 211
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 212
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 213
    .local v0, data:[I
    add-int/lit8 v3, p1, 0x0

    #@6
    aget v1, v0, v3

    #@8
    .line 214
    .local v1, type:I
    if-nez v1, :cond_b

    #@a
    .line 229
    .end local p2
    :goto_a
    return p2

    #@b
    .line 216
    .restart local p2
    :cond_b
    const/16 v3, 0x10

    #@d
    if-lt v1, v3, :cond_1e

    #@f
    const/16 v3, 0x1f

    #@11
    if-gt v1, v3, :cond_1e

    #@13
    .line 218
    add-int/lit8 v3, p1, 0x1

    #@15
    aget v3, v0, v3

    #@17
    if-eqz v3, :cond_1c

    #@19
    const/4 v3, 0x1

    #@1a
    :goto_1a
    move p2, v3

    #@1b
    goto :goto_a

    #@1c
    :cond_1c
    const/4 v3, 0x0

    #@1d
    goto :goto_1a

    #@1e
    .line 221
    :cond_1e
    iget-object v2, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@20
    .line 222
    .local v2, v:Landroid/util/TypedValue;
    invoke-direct {p0, p1, v2}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@23
    move-result v3

    #@24
    if-eqz v3, :cond_47

    #@26
    .line 223
    const-string v3, "Resources"

    #@28
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v5, "Converting to boolean: "

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 224
    invoke-virtual {v2}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@41
    move-result-object v3

    #@42
    invoke-static {v3, p2}, Lcom/android/internal/util/XmlUtils;->convertValueToBoolean(Ljava/lang/CharSequence;Z)Z

    #@45
    move-result p2

    #@46
    goto :goto_a

    #@47
    .line 227
    :cond_47
    const-string v3, "Resources"

    #@49
    new-instance v4, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v5, "getBoolean of bad type: 0x"

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    goto :goto_a
.end method

.method public getColor(II)I
    .registers 10
    .parameter "index"
    .parameter "defValue"

    #@0
    .prologue
    .line 308
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 309
    iget-object v1, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 310
    .local v1, data:[I
    add-int/lit8 v4, p1, 0x0

    #@6
    aget v2, v1, v4

    #@8
    .line 311
    .local v2, type:I
    if-nez v2, :cond_b

    #@a
    .line 323
    .end local p2
    :cond_a
    :goto_a
    return p2

    #@b
    .line 313
    .restart local p2
    :cond_b
    const/16 v4, 0x10

    #@d
    if-lt v2, v4, :cond_18

    #@f
    const/16 v4, 0x1f

    #@11
    if-gt v2, v4, :cond_18

    #@13
    .line 315
    add-int/lit8 v4, p1, 0x1

    #@15
    aget p2, v1, v4

    #@17
    goto :goto_a

    #@18
    .line 316
    :cond_18
    const/4 v4, 0x3

    #@19
    if-ne v2, v4, :cond_30

    #@1b
    .line 317
    iget-object v3, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@1d
    .line 318
    .local v3, value:Landroid/util/TypedValue;
    invoke-direct {p0, p1, v3}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@20
    move-result v4

    #@21
    if-eqz v4, :cond_a

    #@23
    .line 319
    iget-object v4, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@25
    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    #@27
    invoke-virtual {v4, v3, v5}, Landroid/content/res/Resources;->loadColorStateList(Landroid/util/TypedValue;I)Landroid/content/res/ColorStateList;

    #@2a
    move-result-object v0

    #@2b
    .line 321
    .local v0, csl:Landroid/content/res/ColorStateList;
    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    #@2e
    move-result p2

    #@2f
    goto :goto_a

    #@30
    .line 326
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    .end local v3           #value:Landroid/util/TypedValue;
    :cond_30
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    #@32
    new-instance v5, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v6, "Can\'t convert to color: type=0x"

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@40
    move-result-object v6

    #@41
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@4c
    throw v4
.end method

.method public getColorStateList(I)Landroid/content/res/ColorStateList;
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 340
    iget-object v0, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@2
    .line 341
    .local v0, value:Landroid/util/TypedValue;
    mul-int/lit8 v1, p1, 0x6

    #@4
    invoke-direct {p0, v1, v0}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_13

    #@a
    .line 342
    iget-object v1, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@c
    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    #@e
    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->loadColorStateList(Landroid/util/TypedValue;I)Landroid/content/res/ColorStateList;

    #@11
    move-result-object v1

    #@12
    .line 344
    :goto_12
    return-object v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_12
.end method

.method public getDimension(IF)F
    .registers 8
    .parameter "index"
    .parameter "defValue"

    #@0
    .prologue
    .line 388
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 389
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 390
    .local v0, data:[I
    add-int/lit8 v2, p1, 0x0

    #@6
    aget v1, v0, v2

    #@8
    .line 391
    .local v1, type:I
    if-nez v1, :cond_b

    #@a
    .line 394
    .end local p2
    :goto_a
    return p2

    #@b
    .line 393
    .restart local p2
    :cond_b
    const/4 v2, 0x5

    #@c
    if-ne v1, v2, :cond_1b

    #@e
    .line 394
    add-int/lit8 v2, p1, 0x1

    #@10
    aget v2, v0, v2

    #@12
    iget-object v3, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@14
    iget-object v3, v3, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@16
    invoke-static {v2, v3}, Landroid/util/TypedValue;->complexToDimension(ILandroid/util/DisplayMetrics;)F

    #@19
    move-result p2

    #@1a
    goto :goto_a

    #@1b
    .line 398
    :cond_1b
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v4, "Can\'t convert to dimension: type=0x"

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@37
    throw v2
.end method

.method public getDimensionPixelOffset(II)I
    .registers 8
    .parameter "index"
    .parameter "defValue"

    #@0
    .prologue
    .line 420
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 421
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 422
    .local v0, data:[I
    add-int/lit8 v2, p1, 0x0

    #@6
    aget v1, v0, v2

    #@8
    .line 423
    .local v1, type:I
    if-nez v1, :cond_b

    #@a
    .line 426
    .end local p2
    :goto_a
    return p2

    #@b
    .line 425
    .restart local p2
    :cond_b
    const/4 v2, 0x5

    #@c
    if-ne v1, v2, :cond_1b

    #@e
    .line 426
    add-int/lit8 v2, p1, 0x1

    #@10
    aget v2, v0, v2

    #@12
    iget-object v3, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@14
    iget-object v3, v3, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@16
    invoke-static {v2, v3}, Landroid/util/TypedValue;->complexToDimensionPixelOffset(ILandroid/util/DisplayMetrics;)I

    #@19
    move-result p2

    #@1a
    goto :goto_a

    #@1b
    .line 430
    :cond_1b
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v4, "Can\'t convert to dimension: type=0x"

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@37
    throw v2
.end method

.method public getDimensionPixelSize(II)I
    .registers 8
    .parameter "index"
    .parameter "defValue"

    #@0
    .prologue
    .line 453
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 454
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 455
    .local v0, data:[I
    add-int/lit8 v2, p1, 0x0

    #@6
    aget v1, v0, v2

    #@8
    .line 456
    .local v1, type:I
    if-nez v1, :cond_b

    #@a
    .line 459
    .end local p2
    :goto_a
    return p2

    #@b
    .line 458
    .restart local p2
    :cond_b
    const/4 v2, 0x5

    #@c
    if-ne v1, v2, :cond_1b

    #@e
    .line 459
    add-int/lit8 v2, p1, 0x1

    #@10
    aget v2, v0, v2

    #@12
    iget-object v3, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@14
    iget-object v3, v3, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@16
    invoke-static {v2, v3}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    #@19
    move-result p2

    #@1a
    goto :goto_a

    #@1b
    .line 463
    :cond_1b
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v4, "Can\'t convert to dimension: type=0x"

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@37
    throw v2
.end method

.method public getDrawable(I)Landroid/graphics/drawable/Drawable;
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 590
    iget-object v0, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@2
    .line 591
    .local v0, value:Landroid/util/TypedValue;
    mul-int/lit8 v1, p1, 0x6

    #@4
    invoke-direct {p0, v1, v0}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_13

    #@a
    .line 601
    iget-object v1, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@c
    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    #@e
    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->loadDrawable(Landroid/util/TypedValue;I)Landroid/graphics/drawable/Drawable;

    #@11
    move-result-object v1

    #@12
    .line 603
    :goto_12
    return-object v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_12
.end method

.method public getFloat(IF)F
    .registers 10
    .parameter "index"
    .parameter "defValue"

    #@0
    .prologue
    .line 270
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 271
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 272
    .local v0, data:[I
    add-int/lit8 v4, p1, 0x0

    #@6
    aget v2, v0, v4

    #@8
    .line 273
    .local v2, type:I
    if-nez v2, :cond_b

    #@a
    .line 292
    .end local p2
    :goto_a
    return p2

    #@b
    .line 275
    .restart local p2
    :cond_b
    const/4 v4, 0x4

    #@c
    if-ne v2, v4, :cond_17

    #@e
    .line 276
    add-int/lit8 v4, p1, 0x1

    #@10
    aget v4, v0, v4

    #@12
    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    #@15
    move-result p2

    #@16
    goto :goto_a

    #@17
    .line 277
    :cond_17
    const/16 v4, 0x10

    #@19
    if-lt v2, v4, :cond_25

    #@1b
    const/16 v4, 0x1f

    #@1d
    if-gt v2, v4, :cond_25

    #@1f
    .line 279
    add-int/lit8 v4, p1, 0x1

    #@21
    aget v4, v0, v4

    #@23
    int-to-float p2, v4

    #@24
    goto :goto_a

    #@25
    .line 282
    :cond_25
    iget-object v3, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@27
    .line 283
    .local v3, v:Landroid/util/TypedValue;
    invoke-direct {p0, p1, v3}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_54

    #@2d
    .line 284
    const-string v4, "Resources"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "Converting to float: "

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 285
    invoke-virtual {v3}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@48
    move-result-object v1

    #@49
    .line 286
    .local v1, str:Ljava/lang/CharSequence;
    if-eqz v1, :cond_54

    #@4b
    .line 287
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@52
    move-result p2

    #@53
    goto :goto_a

    #@54
    .line 290
    .end local v1           #str:Ljava/lang/CharSequence;
    :cond_54
    const-string v4, "Resources"

    #@56
    new-instance v5, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v6, "getFloat of bad type: 0x"

    #@5d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    goto :goto_a
.end method

.method public getFraction(IIIF)F
    .registers 10
    .parameter "index"
    .parameter "base"
    .parameter "pbase"
    .parameter "defValue"

    #@0
    .prologue
    .line 539
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 540
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 541
    .local v0, data:[I
    add-int/lit8 v2, p1, 0x0

    #@6
    aget v1, v0, v2

    #@8
    .line 542
    .local v1, type:I
    if-nez v1, :cond_b

    #@a
    .line 545
    .end local p4
    :goto_a
    return p4

    #@b
    .line 544
    .restart local p4
    :cond_b
    const/4 v2, 0x6

    #@c
    if-ne v1, v2, :cond_19

    #@e
    .line 545
    add-int/lit8 v2, p1, 0x1

    #@10
    aget v2, v0, v2

    #@12
    int-to-float v3, p2

    #@13
    int-to-float v4, p3

    #@14
    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->complexToFraction(IFF)F

    #@17
    move-result p4

    #@18
    goto :goto_a

    #@19
    .line 549
    :cond_19
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "Can\'t convert to fraction: type=0x"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@35
    throw v2
.end method

.method public getIndex(I)I
    .registers 4
    .parameter "at"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/content/res/TypedArray;->mIndices:[I

    #@2
    add-int/lit8 v1, p1, 0x1

    #@4
    aget v0, v0, v1

    #@6
    return v0
.end method

.method public getIndexCount()I
    .registers 3

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Landroid/content/res/TypedArray;->mIndices:[I

    #@2
    const/4 v1, 0x0

    #@3
    aget v0, v0, v1

    #@5
    return v0
.end method

.method public getInt(II)I
    .registers 9
    .parameter "index"
    .parameter "defValue"

    #@0
    .prologue
    .line 241
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 242
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 243
    .local v0, data:[I
    add-int/lit8 v3, p1, 0x0

    #@6
    aget v1, v0, v3

    #@8
    .line 244
    .local v1, type:I
    if-nez v1, :cond_b

    #@a
    .line 259
    .end local p2
    :goto_a
    return p2

    #@b
    .line 246
    .restart local p2
    :cond_b
    const/16 v3, 0x10

    #@d
    if-lt v1, v3, :cond_18

    #@f
    const/16 v3, 0x1f

    #@11
    if-gt v1, v3, :cond_18

    #@13
    .line 248
    add-int/lit8 v3, p1, 0x1

    #@15
    aget p2, v0, v3

    #@17
    goto :goto_a

    #@18
    .line 251
    :cond_18
    iget-object v2, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@1a
    .line 252
    .local v2, v:Landroid/util/TypedValue;
    invoke-direct {p0, p1, v2}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_41

    #@20
    .line 253
    const-string v3, "Resources"

    #@22
    new-instance v4, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v5, "Converting to int: "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 254
    invoke-virtual {v2}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v3, p2}, Lcom/android/internal/util/XmlUtils;->convertValueToInt(Ljava/lang/CharSequence;I)I

    #@3f
    move-result p2

    #@40
    goto :goto_a

    #@41
    .line 257
    :cond_41
    const-string v3, "Resources"

    #@43
    new-instance v4, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v5, "getInt of bad type: 0x"

    #@4a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_a
.end method

.method public getInteger(II)I
    .registers 8
    .parameter "index"
    .parameter "defValue"

    #@0
    .prologue
    .line 357
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 358
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 359
    .local v0, data:[I
    add-int/lit8 v2, p1, 0x0

    #@6
    aget v1, v0, v2

    #@8
    .line 360
    .local v1, type:I
    if-nez v1, :cond_b

    #@a
    .line 364
    .end local p2
    :goto_a
    return p2

    #@b
    .line 362
    .restart local p2
    :cond_b
    const/16 v2, 0x10

    #@d
    if-lt v1, v2, :cond_18

    #@f
    const/16 v2, 0x1f

    #@11
    if-gt v1, v2, :cond_18

    #@13
    .line 364
    add-int/lit8 v2, p1, 0x1

    #@15
    aget p2, v0, v2

    #@17
    goto :goto_a

    #@18
    .line 367
    :cond_18
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@1a
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "Can\'t convert to integer: type=0x"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@34
    throw v2
.end method

.method public getLayoutDimension(II)I
    .registers 7
    .parameter "index"
    .parameter "defValue"

    #@0
    .prologue
    .line 509
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 510
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 511
    .local v0, data:[I
    add-int/lit8 v2, p1, 0x0

    #@6
    aget v1, v0, v2

    #@8
    .line 512
    .local v1, type:I
    const/16 v2, 0x10

    #@a
    if-lt v1, v2, :cond_15

    #@c
    const/16 v2, 0x1f

    #@e
    if-gt v1, v2, :cond_15

    #@10
    .line 514
    add-int/lit8 v2, p1, 0x1

    #@12
    aget p2, v0, v2

    #@14
    .line 520
    .end local p2
    :cond_14
    :goto_14
    return p2

    #@15
    .line 515
    .restart local p2
    :cond_15
    const/4 v2, 0x5

    #@16
    if-ne v1, v2, :cond_14

    #@18
    .line 516
    add-int/lit8 v2, p1, 0x1

    #@1a
    aget v2, v0, v2

    #@1c
    iget-object v3, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@1e
    iget-object v3, v3, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@20
    invoke-static {v2, v3}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    #@23
    move-result p2

    #@24
    goto :goto_14
.end method

.method public getLayoutDimension(ILjava/lang/String;)I
    .registers 8
    .parameter "index"
    .parameter "name"

    #@0
    .prologue
    .line 480
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 481
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 482
    .local v0, data:[I
    add-int/lit8 v2, p1, 0x0

    #@6
    aget v1, v0, v2

    #@8
    .line 483
    .local v1, type:I
    const/16 v2, 0x10

    #@a
    if-lt v1, v2, :cond_15

    #@c
    const/16 v2, 0x1f

    #@e
    if-gt v1, v2, :cond_15

    #@10
    .line 485
    add-int/lit8 v2, p1, 0x1

    #@12
    aget v2, v0, v2

    #@14
    .line 487
    :goto_14
    return v2

    #@15
    .line 486
    :cond_15
    const/4 v2, 0x5

    #@16
    if-ne v1, v2, :cond_25

    #@18
    .line 487
    add-int/lit8 v2, p1, 0x1

    #@1a
    aget v2, v0, v2

    #@1c
    iget-object v3, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@1e
    iget-object v3, v3, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@20
    invoke-static {v2, v3}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    #@23
    move-result v2

    #@24
    goto :goto_14

    #@25
    .line 491
    :cond_25
    new-instance v2, Ljava/lang/RuntimeException;

    #@27
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    const-string v4, ": You must supply a "

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    const-string v4, " attribute."

    #@40
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v2
.end method

.method public getNonConfigurationString(II)Ljava/lang/String;
    .registers 11
    .parameter "index"
    .parameter "allowedChangingConfigs"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 179
    mul-int/lit8 p1, p1, 0x6

    #@3
    .line 180
    iget-object v1, p0, Landroid/content/res/TypedArray;->mData:[I

    #@5
    .line 181
    .local v1, data:[I
    add-int/lit8 v5, p1, 0x0

    #@7
    aget v2, v1, v5

    #@9
    .line 182
    .local v2, type:I
    add-int/lit8 v5, p1, 0x4

    #@b
    aget v5, v1, v5

    #@d
    xor-int/lit8 v6, p2, -0x1

    #@f
    and-int/2addr v5, v6

    #@10
    if-eqz v5, :cond_13

    #@12
    .line 199
    :cond_12
    :goto_12
    return-object v4

    #@13
    .line 185
    :cond_13
    if-eqz v2, :cond_12

    #@15
    .line 187
    const/4 v5, 0x3

    #@16
    if-ne v2, v5, :cond_21

    #@18
    .line 188
    invoke-direct {p0, p1}, Landroid/content/res/TypedArray;->loadStringValueAt(I)Ljava/lang/CharSequence;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    goto :goto_12

    #@21
    .line 191
    :cond_21
    iget-object v3, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@23
    .line 192
    .local v3, v:Landroid/util/TypedValue;
    invoke-direct {p0, p1, v3}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@26
    move-result v5

    #@27
    if-eqz v5, :cond_4c

    #@29
    .line 193
    const-string v5, "Resources"

    #@2b
    new-instance v6, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v7, "Converting to string: "

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 194
    invoke-virtual {v3}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@44
    move-result-object v0

    #@45
    .line 195
    .local v0, cs:Ljava/lang/CharSequence;
    if-eqz v0, :cond_12

    #@47
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    goto :goto_12

    #@4c
    .line 197
    .end local v0           #cs:Ljava/lang/CharSequence;
    :cond_4c
    const-string v5, "Resources"

    #@4e
    new-instance v6, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v7, "getString of bad type: 0x"

    #@55
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v6

    #@59
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@5c
    move-result-object v7

    #@5d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v6

    #@61
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v6

    #@65
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    goto :goto_12
.end method

.method public getNonResourceString(I)Ljava/lang/String;
    .registers 7
    .parameter "index"

    #@0
    .prologue
    .line 153
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 154
    iget-object v1, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 155
    .local v1, data:[I
    add-int/lit8 v3, p1, 0x0

    #@6
    aget v2, v1, v3

    #@8
    .line 156
    .local v2, type:I
    const/4 v3, 0x3

    #@9
    if-ne v2, v3, :cond_20

    #@b
    .line 157
    add-int/lit8 v3, p1, 0x2

    #@d
    aget v0, v1, v3

    #@f
    .line 158
    .local v0, cookie:I
    if-gez v0, :cond_20

    #@11
    .line 159
    iget-object v3, p0, Landroid/content/res/TypedArray;->mXml:Landroid/content/res/XmlBlock$Parser;

    #@13
    add-int/lit8 v4, p1, 0x1

    #@15
    aget v4, v1, v4

    #@17
    invoke-virtual {v3, v4}, Landroid/content/res/XmlBlock$Parser;->getPooledString(I)Ljava/lang/CharSequence;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    .line 163
    .end local v0           #cookie:I
    :goto_1f
    return-object v3

    #@20
    :cond_20
    const/4 v3, 0x0

    #@21
    goto :goto_1f
.end method

.method public getPositionDescription()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 683
    iget-object v0, p0, Landroid/content/res/TypedArray;->mXml:Landroid/content/res/XmlBlock$Parser;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/content/res/TypedArray;->mXml:Landroid/content/res/XmlBlock$Parser;

    #@6
    invoke-virtual {v0}, Landroid/content/res/XmlBlock$Parser;->getPositionDescription()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const-string v0, "<internal>"

    #@d
    goto :goto_a
.end method

.method public getResourceId(II)I
    .registers 6
    .parameter "index"
    .parameter "defValue"

    #@0
    .prologue
    .line 568
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 569
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 570
    .local v0, data:[I
    add-int/lit8 v2, p1, 0x0

    #@6
    aget v2, v0, v2

    #@8
    if-eqz v2, :cond_11

    #@a
    .line 571
    add-int/lit8 v2, p1, 0x3

    #@c
    aget v1, v0, v2

    #@e
    .line 572
    .local v1, resid:I
    if-eqz v1, :cond_11

    #@10
    .line 576
    .end local v1           #resid:I
    :goto_10
    return v1

    #@11
    :cond_11
    move v1, p2

    #@12
    goto :goto_10
.end method

.method public getResources()Landroid/content/res/Resources;
    .registers 2

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@2
    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .registers 10
    .parameter "index"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 118
    mul-int/lit8 p1, p1, 0x6

    #@3
    .line 119
    iget-object v1, p0, Landroid/content/res/TypedArray;->mData:[I

    #@5
    .line 120
    .local v1, data:[I
    add-int/lit8 v5, p1, 0x0

    #@7
    aget v2, v1, v5

    #@9
    .line 121
    .local v2, type:I
    if-nez v2, :cond_c

    #@b
    .line 135
    :cond_b
    :goto_b
    return-object v4

    #@c
    .line 123
    :cond_c
    const/4 v5, 0x3

    #@d
    if-ne v2, v5, :cond_18

    #@f
    .line 124
    invoke-direct {p0, p1}, Landroid/content/res/TypedArray;->loadStringValueAt(I)Ljava/lang/CharSequence;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    goto :goto_b

    #@18
    .line 127
    :cond_18
    iget-object v3, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@1a
    .line 128
    .local v3, v:Landroid/util/TypedValue;
    invoke-direct {p0, p1, v3}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@1d
    move-result v5

    #@1e
    if-eqz v5, :cond_43

    #@20
    .line 129
    const-string v5, "Resources"

    #@22
    new-instance v6, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v7, "Converting to string: "

    #@29
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 130
    invoke-virtual {v3}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@3b
    move-result-object v0

    #@3c
    .line 131
    .local v0, cs:Ljava/lang/CharSequence;
    if-eqz v0, :cond_b

    #@3e
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    goto :goto_b

    #@43
    .line 133
    .end local v0           #cs:Ljava/lang/CharSequence;
    :cond_43
    const-string v5, "Resources"

    #@45
    new-instance v6, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v7, "getString of bad type: 0x"

    #@4c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v6

    #@50
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v6

    #@5c
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_b
.end method

.method public getText(I)Ljava/lang/CharSequence;
    .registers 9
    .parameter "index"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 90
    mul-int/lit8 p1, p1, 0x6

    #@3
    .line 91
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@5
    .line 92
    .local v0, data:[I
    add-int/lit8 v4, p1, 0x0

    #@7
    aget v1, v0, v4

    #@9
    .line 93
    .local v1, type:I
    if-nez v1, :cond_c

    #@b
    .line 106
    :goto_b
    return-object v3

    #@c
    .line 95
    :cond_c
    const/4 v4, 0x3

    #@d
    if-ne v1, v4, :cond_14

    #@f
    .line 96
    invoke-direct {p0, p1}, Landroid/content/res/TypedArray;->loadStringValueAt(I)Ljava/lang/CharSequence;

    #@12
    move-result-object v3

    #@13
    goto :goto_b

    #@14
    .line 99
    :cond_14
    iget-object v2, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@16
    .line 100
    .local v2, v:Landroid/util/TypedValue;
    invoke-direct {p0, p1, v2}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_39

    #@1c
    .line 101
    const-string v3, "Resources"

    #@1e
    new-instance v4, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v5, "Converting to string: "

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 102
    invoke-virtual {v2}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@37
    move-result-object v3

    #@38
    goto :goto_b

    #@39
    .line 104
    :cond_39
    const-string v4, "Resources"

    #@3b
    new-instance v5, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v6, "getString of bad type: 0x"

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_b
.end method

.method public getTextArray(I)[Ljava/lang/CharSequence;
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 617
    iget-object v0, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@2
    .line 618
    .local v0, value:Landroid/util/TypedValue;
    mul-int/lit8 v1, p1, 0x6

    #@4
    invoke-direct {p0, v1, v0}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_13

    #@a
    .line 628
    iget-object v1, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@c
    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    #@e
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    #@11
    move-result-object v1

    #@12
    .line 630
    :goto_12
    return-object v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_12
.end method

.method public getValue(ILandroid/util/TypedValue;)Z
    .registers 4
    .parameter "index"
    .parameter "outValue"

    #@0
    .prologue
    .line 643
    mul-int/lit8 v0, p1, 0x6

    #@2
    invoke-direct {p0, v0, p2}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public hasValue(I)Z
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 654
    mul-int/lit8 p1, p1, 0x6

    #@2
    .line 655
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@4
    .line 656
    .local v0, data:[I
    add-int/lit8 v2, p1, 0x0

    #@6
    aget v1, v0, v2

    #@8
    .line 657
    .local v1, type:I
    if-eqz v1, :cond_c

    #@a
    const/4 v2, 0x1

    #@b
    :goto_b
    return v2

    #@c
    :cond_c
    const/4 v2, 0x0

    #@d
    goto :goto_b
.end method

.method public length()I
    .registers 2

    #@0
    .prologue
    .line 51
    iget v0, p0, Landroid/content/res/TypedArray;->mLength:I

    #@2
    return v0
.end method

.method public peekValue(I)Landroid/util/TypedValue;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 672
    iget-object v0, p0, Landroid/content/res/TypedArray;->mValue:Landroid/util/TypedValue;

    #@2
    .line 673
    .local v0, value:Landroid/util/TypedValue;
    mul-int/lit8 v1, p1, 0x6

    #@4
    invoke-direct {p0, v1, v0}, Landroid/content/res/TypedArray;->getValueAt(ILandroid/util/TypedValue;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_b

    #@a
    .line 676
    .end local v0           #value:Landroid/util/TypedValue;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #value:Landroid/util/TypedValue;
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public recycle()V
    .registers 5

    #@0
    .prologue
    .line 690
    iget-object v1, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@2
    iget-object v2, v1, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@4
    monitor-enter v2

    #@5
    .line 691
    :try_start_5
    iget-object v1, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@7
    iget-object v0, v1, Landroid/content/res/Resources;->mCachedStyledAttributes:Landroid/content/res/TypedArray;

    #@9
    .line 692
    .local v0, cached:Landroid/content/res/TypedArray;
    if-eqz v0, :cond_13

    #@b
    iget-object v1, v0, Landroid/content/res/TypedArray;->mData:[I

    #@d
    array-length v1, v1

    #@e
    iget-object v3, p0, Landroid/content/res/TypedArray;->mData:[I

    #@10
    array-length v3, v3

    #@11
    if-ge v1, v3, :cond_1a

    #@13
    .line 693
    :cond_13
    const/4 v1, 0x0

    #@14
    iput-object v1, p0, Landroid/content/res/TypedArray;->mXml:Landroid/content/res/XmlBlock$Parser;

    #@16
    .line 694
    iget-object v1, p0, Landroid/content/res/TypedArray;->mResources:Landroid/content/res/Resources;

    #@18
    iput-object p0, v1, Landroid/content/res/Resources;->mCachedStyledAttributes:Landroid/content/res/TypedArray;

    #@1a
    .line 696
    :cond_1a
    monitor-exit v2

    #@1b
    .line 697
    return-void

    #@1c
    .line 696
    .end local v0           #cached:Landroid/content/res/TypedArray;
    :catchall_1c
    move-exception v1

    #@1d
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_5 .. :try_end_1e} :catchall_1c

    #@1e
    throw v1
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 738
    iget-object v0, p0, Landroid/content/res/TypedArray;->mData:[I

    #@2
    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
