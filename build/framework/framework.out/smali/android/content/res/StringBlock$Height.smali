.class Landroid/content/res/StringBlock$Height;
.super Ljava/lang/Object;
.source "StringBlock.java"

# interfaces
.implements Landroid/text/style/LineHeightSpan$WithDensity;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/res/StringBlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Height"
.end annotation


# static fields
.field private static sProportion:F


# instance fields
.field private mSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 342
    const/4 v0, 0x0

    #@1
    sput v0, Landroid/content/res/StringBlock$Height;->sProportion:F

    #@3
    return-void
.end method

.method public constructor <init>(I)V
    .registers 2
    .parameter "size"

    #@0
    .prologue
    .line 344
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 345
    iput p1, p0, Landroid/content/res/StringBlock$Height;->mSize:I

    #@5
    .line 346
    return-void
.end method


# virtual methods
.method public chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;)V
    .registers 15
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "spanstartv"
    .parameter "v"
    .parameter "fm"

    #@0
    .prologue
    .line 352
    const/4 v7, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move-object v6, p6

    #@8
    invoke-virtual/range {v0 .. v7}, Landroid/content/res/StringBlock$Height;->chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V

    #@b
    .line 353
    return-void
.end method

.method public chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V
    .registers 15
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "spanstartv"
    .parameter "v"
    .parameter "fm"
    .parameter "paint"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 358
    iget v3, p0, Landroid/content/res/StringBlock$Height;->mSize:I

    #@3
    .line 359
    .local v3, size:I
    if-eqz p7, :cond_a

    #@5
    .line 360
    int-to-float v4, v3

    #@6
    iget v5, p7, Landroid/text/TextPaint;->density:F

    #@8
    mul-float/2addr v4, v5

    #@9
    float-to-int v3, v4

    #@a
    .line 363
    :cond_a
    iget v4, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@c
    iget v5, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@e
    sub-int/2addr v4, v5

    #@f
    if-ge v4, v3, :cond_1c

    #@11
    .line 364
    iget v4, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@13
    sub-int/2addr v4, v3

    #@14
    iput v4, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@16
    .line 365
    iget v4, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@18
    sub-int/2addr v4, v3

    #@19
    iput v4, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@1b
    .line 409
    :goto_1b
    return-void

    #@1c
    .line 367
    :cond_1c
    sget v4, Landroid/content/res/StringBlock$Height;->sProportion:F

    #@1e
    const/4 v5, 0x0

    #@1f
    cmpl-float v4, v4, v5

    #@21
    if-nez v4, :cond_42

    #@23
    .line 375
    new-instance v1, Landroid/graphics/Paint;

    #@25
    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    #@28
    .line 376
    .local v1, p:Landroid/graphics/Paint;
    const/high16 v4, 0x42c8

    #@2a
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    #@2d
    .line 377
    new-instance v2, Landroid/graphics/Rect;

    #@2f
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@32
    .line 378
    .local v2, r:Landroid/graphics/Rect;
    const-string v4, "ABCDEFG"

    #@34
    const/4 v5, 0x7

    #@35
    invoke-virtual {v1, v4, v6, v5, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    #@38
    .line 380
    iget v4, v2, Landroid/graphics/Rect;->top:I

    #@3a
    int-to-float v4, v4

    #@3b
    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    #@3e
    move-result v5

    #@3f
    div-float/2addr v4, v5

    #@40
    sput v4, Landroid/content/res/StringBlock$Height;->sProportion:F

    #@42
    .line 383
    .end local v1           #p:Landroid/graphics/Paint;
    .end local v2           #r:Landroid/graphics/Rect;
    :cond_42
    iget v4, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@44
    neg-int v4, v4

    #@45
    int-to-float v4, v4

    #@46
    sget v5, Landroid/content/res/StringBlock$Height;->sProportion:F

    #@48
    mul-float/2addr v4, v5

    #@49
    float-to-double v4, v4

    #@4a
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    #@4d
    move-result-wide v4

    #@4e
    double-to-int v0, v4

    #@4f
    .line 385
    .local v0, need:I
    iget v4, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@51
    sub-int v4, v3, v4

    #@53
    if-lt v4, v0, :cond_60

    #@55
    .line 390
    iget v4, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@57
    sub-int/2addr v4, v3

    #@58
    iput v4, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@5a
    .line 391
    iget v4, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@5c
    sub-int/2addr v4, v3

    #@5d
    iput v4, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@5f
    goto :goto_1b

    #@60
    .line 392
    :cond_60
    if-lt v3, v0, :cond_6f

    #@62
    .line 398
    neg-int v4, v0

    #@63
    iput v4, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@65
    iput v4, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@67
    .line 399
    iget v4, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@69
    add-int/2addr v4, v3

    #@6a
    iput v4, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@6c
    iput v4, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@6e
    goto :goto_1b

    #@6f
    .line 405
    :cond_6f
    neg-int v4, v3

    #@70
    iput v4, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@72
    iput v4, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@74
    .line 406
    iput v6, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@76
    iput v6, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@78
    goto :goto_1b
.end method
