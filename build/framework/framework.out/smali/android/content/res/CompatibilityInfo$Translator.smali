.class public Landroid/content/res/CompatibilityInfo$Translator;
.super Ljava/lang/Object;
.source "CompatibilityInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/res/CompatibilityInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Translator"
.end annotation


# instance fields
.field public final applicationInvertedScale:F

.field public final applicationScale:F

.field private mContentInsetsBuffer:Landroid/graphics/Rect;

.field private mTouchableAreaBuffer:Landroid/graphics/Region;

.field private mVisibleInsetsBuffer:Landroid/graphics/Rect;

.field final synthetic this$0:Landroid/content/res/CompatibilityInfo;


# direct methods
.method constructor <init>(Landroid/content/res/CompatibilityInfo;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 425
    iget v0, p1, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@2
    iget v1, p1, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@4
    invoke-direct {p0, p1, v0, v1}, Landroid/content/res/CompatibilityInfo$Translator;-><init>(Landroid/content/res/CompatibilityInfo;FF)V

    #@7
    .line 427
    return-void
.end method

.method constructor <init>(Landroid/content/res/CompatibilityInfo;FF)V
    .registers 5
    .parameter
    .parameter "applicationScale"
    .parameter "applicationInvertedScale"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 419
    iput-object p1, p0, Landroid/content/res/CompatibilityInfo$Translator;->this$0:Landroid/content/res/CompatibilityInfo;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 415
    iput-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mContentInsetsBuffer:Landroid/graphics/Rect;

    #@8
    .line 416
    iput-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mVisibleInsetsBuffer:Landroid/graphics/Rect;

    #@a
    .line 417
    iput-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mTouchableAreaBuffer:Landroid/graphics/Region;

    #@c
    .line 420
    iput p2, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F

    #@e
    .line 421
    iput p3, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationInvertedScale:F

    #@10
    .line 422
    return-void
.end method


# virtual methods
.method public getTranslatedContentInsets(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .registers 3
    .parameter "contentInsets"

    #@0
    .prologue
    .line 522
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mContentInsetsBuffer:Landroid/graphics/Rect;

    #@2
    if-nez v0, :cond_b

    #@4
    new-instance v0, Landroid/graphics/Rect;

    #@6
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mContentInsetsBuffer:Landroid/graphics/Rect;

    #@b
    .line 523
    :cond_b
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mContentInsetsBuffer:Landroid/graphics/Rect;

    #@d
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@10
    .line 524
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mContentInsetsBuffer:Landroid/graphics/Rect;

    #@12
    invoke-virtual {p0, v0}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInAppWindowToScreen(Landroid/graphics/Rect;)V

    #@15
    .line 525
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mContentInsetsBuffer:Landroid/graphics/Rect;

    #@17
    return-object v0
.end method

.method public getTranslatedTouchableArea(Landroid/graphics/Region;)Landroid/graphics/Region;
    .registers 4
    .parameter "touchableArea"

    #@0
    .prologue
    .line 544
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mTouchableAreaBuffer:Landroid/graphics/Region;

    #@2
    if-nez v0, :cond_b

    #@4
    new-instance v0, Landroid/graphics/Region;

    #@6
    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mTouchableAreaBuffer:Landroid/graphics/Region;

    #@b
    .line 545
    :cond_b
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mTouchableAreaBuffer:Landroid/graphics/Region;

    #@d
    invoke-virtual {v0, p1}, Landroid/graphics/Region;->set(Landroid/graphics/Region;)Z

    #@10
    .line 546
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mTouchableAreaBuffer:Landroid/graphics/Region;

    #@12
    iget v1, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F

    #@14
    invoke-virtual {v0, v1}, Landroid/graphics/Region;->scale(F)V

    #@17
    .line 547
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mTouchableAreaBuffer:Landroid/graphics/Region;

    #@19
    return-object v0
.end method

.method public getTranslatedVisibleInsets(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .registers 3
    .parameter "visibleInsets"

    #@0
    .prologue
    .line 533
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mVisibleInsetsBuffer:Landroid/graphics/Rect;

    #@2
    if-nez v0, :cond_b

    #@4
    new-instance v0, Landroid/graphics/Rect;

    #@6
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@9
    iput-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mVisibleInsetsBuffer:Landroid/graphics/Rect;

    #@b
    .line 534
    :cond_b
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mVisibleInsetsBuffer:Landroid/graphics/Rect;

    #@d
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@10
    .line 535
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mVisibleInsetsBuffer:Landroid/graphics/Rect;

    #@12
    invoke-virtual {p0, v0}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInAppWindowToScreen(Landroid/graphics/Rect;)V

    #@15
    .line 536
    iget-object v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->mVisibleInsetsBuffer:Landroid/graphics/Rect;

    #@17
    return-object v0
.end method

.method public translateCanvas(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter "canvas"

    #@0
    .prologue
    const v3, 0x3b2b5601

    #@3
    .line 447
    iget v1, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F

    #@5
    const/high16 v2, 0x3fc0

    #@7
    cmpl-float v1, v1, v2

    #@9
    if-nez v1, :cond_11

    #@b
    .line 463
    const v0, 0x3b2b5601

    #@e
    .line 464
    .local v0, tinyOffset:F
    invoke-virtual {p1, v3, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@11
    .line 466
    .end local v0           #tinyOffset:F
    :cond_11
    iget v1, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F

    #@13
    iget v2, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F

    #@15
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->scale(FF)V

    #@18
    .line 467
    return-void
.end method

.method public translateEventInScreenToAppWindow(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 473
    iget v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationInvertedScale:F

    #@2
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->scale(F)V

    #@5
    .line 474
    return-void
.end method

.method public translateLayoutParamsInAppWindowToScreen(Landroid/view/WindowManager$LayoutParams;)V
    .registers 3
    .parameter "params"

    #@0
    .prologue
    .line 514
    iget v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F

    #@2
    invoke-virtual {p1, v0}, Landroid/view/WindowManager$LayoutParams;->scale(F)V

    #@5
    .line 515
    return-void
.end method

.method public translatePointInScreenToAppWindow(Landroid/graphics/PointF;)V
    .registers 4
    .parameter "point"

    #@0
    .prologue
    .line 502
    iget v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationInvertedScale:F

    #@2
    .line 503
    .local v0, scale:F
    const/high16 v1, 0x3f80

    #@4
    cmpl-float v1, v0, v1

    #@6
    if-eqz v1, :cond_12

    #@8
    .line 504
    iget v1, p1, Landroid/graphics/PointF;->x:F

    #@a
    mul-float/2addr v1, v0

    #@b
    iput v1, p1, Landroid/graphics/PointF;->x:F

    #@d
    .line 505
    iget v1, p1, Landroid/graphics/PointF;->y:F

    #@f
    mul-float/2addr v1, v0

    #@10
    iput v1, p1, Landroid/graphics/PointF;->y:F

    #@12
    .line 507
    :cond_12
    return-void
.end method

.method public translateRectInAppWindowToScreen(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "rect"

    #@0
    .prologue
    .line 488
    iget v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F

    #@2
    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->scale(F)V

    #@5
    .line 489
    return-void
.end method

.method public translateRectInScreenToAppWinFrame(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "rect"

    #@0
    .prologue
    .line 433
    iget v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationInvertedScale:F

    #@2
    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->scale(F)V

    #@5
    .line 434
    return-void
.end method

.method public translateRectInScreenToAppWindow(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "rect"

    #@0
    .prologue
    .line 495
    iget v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationInvertedScale:F

    #@2
    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->scale(F)V

    #@5
    .line 496
    return-void
.end method

.method public translateRegionInWindowToScreen(Landroid/graphics/Region;)V
    .registers 3
    .parameter "transparentRegion"

    #@0
    .prologue
    .line 440
    iget v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F

    #@2
    invoke-virtual {p1, v0}, Landroid/graphics/Region;->scale(F)V

    #@5
    .line 441
    return-void
.end method

.method public translateWindowLayout(Landroid/view/WindowManager$LayoutParams;)V
    .registers 3
    .parameter "params"

    #@0
    .prologue
    .line 481
    iget v0, p0, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F

    #@2
    invoke-virtual {p1, v0}, Landroid/view/WindowManager$LayoutParams;->scale(F)V

    #@5
    .line 482
    return-void
.end method
