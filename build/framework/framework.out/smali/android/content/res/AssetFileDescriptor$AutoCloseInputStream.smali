.class public Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;
.super Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;
.source "AssetFileDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/res/AssetFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AutoCloseInputStream"
.end annotation


# instance fields
.field private mRemaining:J


# direct methods
.method public constructor <init>(Landroid/content/res/AssetFileDescriptor;)V
    .registers 4
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    #@7
    .line 176
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@a
    move-result-wide v0

    #@b
    invoke-super {p0, v0, v1}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->skip(J)J

    #@e
    .line 177
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    #@11
    move-result-wide v0

    #@12
    long-to-int v0, v0

    #@13
    int-to-long v0, v0

    #@14
    iput-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@16
    .line 178
    return-void
.end method


# virtual methods
.method public available()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 182
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@2
    const-wide/16 v2, 0x0

    #@4
    cmp-long v0, v0, v2

    #@6
    if-ltz v0, :cond_19

    #@8
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@a
    const-wide/32 v2, 0x7fffffff

    #@d
    cmp-long v0, v0, v2

    #@f
    if-gez v0, :cond_15

    #@11
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@13
    long-to-int v0, v0

    #@14
    :goto_14
    return v0

    #@15
    :cond_15
    const v0, 0x7fffffff

    #@18
    goto :goto_14

    #@19
    :cond_19
    invoke-super {p0}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->available()I

    #@1c
    move-result v0

    #@1d
    goto :goto_14
.end method

.method public mark(I)V
    .registers 6
    .parameter "readlimit"

    #@0
    .prologue
    .line 227
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@2
    const-wide/16 v2, 0x0

    #@4
    cmp-long v0, v0, v2

    #@6
    if-ltz v0, :cond_9

    #@8
    .line 232
    :goto_8
    return-void

    #@9
    .line 231
    :cond_9
    invoke-super {p0, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->mark(I)V

    #@c
    goto :goto_8
.end method

.method public markSupported()Z
    .registers 5

    #@0
    .prologue
    .line 236
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@2
    const-wide/16 v2, 0x0

    #@4
    cmp-long v0, v0, v2

    #@6
    if-ltz v0, :cond_a

    #@8
    .line 237
    const/4 v0, 0x0

    #@9
    .line 239
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-super {p0}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->markSupported()Z

    #@d
    move-result v0

    #@e
    goto :goto_9
.end method

.method public read()I
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, -0x1

    #@3
    .line 189
    new-array v0, v4, [B

    #@5
    .line 190
    .local v0, buffer:[B
    invoke-virtual {p0, v0, v3, v4}, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->read([BII)I

    #@8
    move-result v1

    #@9
    .line 191
    .local v1, result:I
    if-ne v1, v2, :cond_c

    #@b
    :goto_b
    return v2

    #@c
    :cond_c
    aget-byte v2, v0, v3

    #@e
    and-int/lit16 v2, v2, 0xff

    #@10
    goto :goto_b
.end method

.method public read([B)I
    .registers 4
    .parameter "buffer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 209
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->read([BII)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public read([BII)I
    .registers 9
    .parameter "buffer"
    .parameter "offset"
    .parameter "count"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 196
    iget-wide v1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@4
    cmp-long v1, v1, v3

    #@6
    if-ltz v1, :cond_27

    #@8
    .line 197
    iget-wide v1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@a
    cmp-long v1, v1, v3

    #@c
    if-nez v1, :cond_10

    #@e
    const/4 v0, -0x1

    #@f
    .line 204
    :cond_f
    :goto_f
    return v0

    #@10
    .line 198
    :cond_10
    int-to-long v1, p3

    #@11
    iget-wide v3, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@13
    cmp-long v1, v1, v3

    #@15
    if-lez v1, :cond_1a

    #@17
    iget-wide v1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@19
    long-to-int p3, v1

    #@1a
    .line 199
    :cond_1a
    invoke-super {p0, p1, p2, p3}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->read([BII)I

    #@1d
    move-result v0

    #@1e
    .line 200
    .local v0, res:I
    if-ltz v0, :cond_f

    #@20
    iget-wide v1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@22
    int-to-long v3, v0

    #@23
    sub-long/2addr v1, v3

    #@24
    iput-wide v1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@26
    goto :goto_f

    #@27
    .line 204
    .end local v0           #res:I
    :cond_27
    invoke-super {p0, p1, p2, p3}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->read([BII)I

    #@2a
    move-result v0

    #@2b
    goto :goto_f
.end method

.method public declared-synchronized reset()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 244
    monitor-enter p0

    #@1
    :try_start_1
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_f

    #@3
    const-wide/16 v2, 0x0

    #@5
    cmp-long v0, v0, v2

    #@7
    if-ltz v0, :cond_b

    #@9
    .line 249
    :goto_9
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 248
    :cond_b
    :try_start_b
    invoke-super {p0}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->reset()V
    :try_end_e
    .catchall {:try_start_b .. :try_end_e} :catchall_f

    #@e
    goto :goto_9

    #@f
    .line 244
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0
.end method

.method public skip(J)J
    .registers 9
    .parameter "count"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 214
    iget-wide v2, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@4
    cmp-long v2, v2, v4

    #@6
    if-ltz v2, :cond_27

    #@8
    .line 215
    iget-wide v2, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@a
    cmp-long v2, v2, v4

    #@c
    if-nez v2, :cond_11

    #@e
    const-wide/16 v0, -0x1

    #@10
    .line 222
    :cond_10
    :goto_10
    return-wide v0

    #@11
    .line 216
    :cond_11
    iget-wide v2, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@13
    cmp-long v2, p1, v2

    #@15
    if-lez v2, :cond_19

    #@17
    iget-wide p1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@19
    .line 217
    :cond_19
    invoke-super {p0, p1, p2}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->skip(J)J

    #@1c
    move-result-wide v0

    #@1d
    .line 218
    .local v0, res:J
    cmp-long v2, v0, v4

    #@1f
    if-ltz v2, :cond_10

    #@21
    iget-wide v2, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@23
    sub-long/2addr v2, v0

    #@24
    iput-wide v2, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseInputStream;->mRemaining:J

    #@26
    goto :goto_10

    #@27
    .line 222
    .end local v0           #res:J
    :cond_27
    invoke-super {p0, p1, p2}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->skip(J)J

    #@2a
    move-result-wide v0

    #@2b
    goto :goto_10
.end method
