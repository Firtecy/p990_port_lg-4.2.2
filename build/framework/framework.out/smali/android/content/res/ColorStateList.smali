.class public Landroid/content/res/ColorStateList;
.super Ljava/lang/Object;
.source "ColorStateList.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/res/ColorStateList;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMPTY:[[I

.field private static final sCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/res/ColorStateList;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private mColors:[I

.field private mDefaultColor:I

.field private mStateSpecs:[[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 68
    const/4 v0, 0x1

    #@2
    new-array v0, v0, [[I

    #@4
    new-array v1, v2, [I

    #@6
    aput-object v1, v0, v2

    #@8
    sput-object v0, Landroid/content/res/ColorStateList;->EMPTY:[[I

    #@a
    .line 69
    new-instance v0, Landroid/util/SparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@f
    sput-object v0, Landroid/content/res/ColorStateList;->sCache:Landroid/util/SparseArray;

    #@11
    .line 312
    new-instance v0, Landroid/content/res/ColorStateList$1;

    #@13
    invoke-direct {v0}, Landroid/content/res/ColorStateList$1;-><init>()V

    #@16
    sput-object v0, Landroid/content/res/ColorStateList;->CREATOR:Landroid/os/Parcelable$Creator;

    #@18
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 66
    const/high16 v0, -0x1

    #@5
    iput v0, p0, Landroid/content/res/ColorStateList;->mDefaultColor:I

    #@7
    .line 72
    return-void
.end method

.method public constructor <init>([[I[I)V
    .registers 5
    .parameter "states"
    .parameter "colors"

    #@0
    .prologue
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 66
    const/high16 v1, -0x1

    #@5
    iput v1, p0, Landroid/content/res/ColorStateList;->mDefaultColor:I

    #@7
    .line 79
    iput-object p1, p0, Landroid/content/res/ColorStateList;->mStateSpecs:[[I

    #@9
    .line 80
    iput-object p2, p0, Landroid/content/res/ColorStateList;->mColors:[I

    #@b
    .line 82
    array-length v1, p1

    #@c
    if-lez v1, :cond_23

    #@e
    .line 83
    const/4 v1, 0x0

    #@f
    aget v1, p2, v1

    #@11
    iput v1, p0, Landroid/content/res/ColorStateList;->mDefaultColor:I

    #@13
    .line 85
    const/4 v0, 0x0

    #@14
    .local v0, i:I
    :goto_14
    array-length v1, p1

    #@15
    if-ge v0, v1, :cond_23

    #@17
    .line 86
    aget-object v1, p1, v0

    #@19
    array-length v1, v1

    #@1a
    if-nez v1, :cond_20

    #@1c
    .line 87
    aget v1, p2, v0

    #@1e
    iput v1, p0, Landroid/content/res/ColorStateList;->mDefaultColor:I

    #@20
    .line 85
    :cond_20
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_14

    #@23
    .line 91
    .end local v0           #i:I
    :cond_23
    return-void
.end method

.method public static createFromXml(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;)Landroid/content/res/ColorStateList;
    .registers 6
    .parameter "r"
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 118
    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@4
    move-result-object v0

    #@5
    .line 122
    .local v0, attrs:Landroid/util/AttributeSet;
    :cond_5
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@8
    move-result v1

    #@9
    .local v1, type:I
    if-eq v1, v3, :cond_e

    #@b
    const/4 v2, 0x1

    #@c
    if-ne v1, v2, :cond_5

    #@e
    .line 125
    :cond_e
    if-eq v1, v3, :cond_18

    #@10
    .line 126
    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    #@12
    const-string v3, "No start tag found"

    #@14
    invoke-direct {v2, v3}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2

    #@18
    .line 129
    :cond_18
    invoke-static {p0, p1, v0}, Landroid/content/res/ColorStateList;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/content/res/ColorStateList;

    #@1b
    move-result-object v2

    #@1c
    return-object v2
.end method

.method private static createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/content/res/ColorStateList;
    .registers 8
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 141
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 143
    .local v1, name:Ljava/lang/String;
    const-string/jumbo v2, "selector"

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_16

    #@d
    .line 144
    new-instance v0, Landroid/content/res/ColorStateList;

    #@f
    invoke-direct {v0}, Landroid/content/res/ColorStateList;-><init>()V

    #@12
    .line 150
    .local v0, colorStateList:Landroid/content/res/ColorStateList;
    invoke-direct {v0, p0, p1, p2}, Landroid/content/res/ColorStateList;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    #@15
    .line 151
    return-object v0

    #@16
    .line 146
    .end local v0           #colorStateList:Landroid/content/res/ColorStateList;
    :cond_16
    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, ": invalid drawable tag "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-direct {v2, v3}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@36
    throw v2
.end method

.method private inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 29
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 178
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@3
    move-result v22

    #@4
    add-int/lit8 v10, v22, 0x1

    #@6
    .line 181
    .local v10, innerDepth:I
    const/16 v13, 0x14

    #@8
    .line 182
    .local v13, listAllocated:I
    const/4 v14, 0x0

    #@9
    .line 183
    .local v14, listSize:I
    new-array v5, v13, [I

    #@b
    .line 184
    .local v5, colorList:[I
    new-array v0, v13, [[I

    #@d
    move-object/from16 v20, v0

    #@f
    .line 187
    .local v20, stateSpecList:[[I
    :cond_f
    :goto_f
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@12
    move-result v21

    #@13
    .local v21, type:I
    const/16 v22, 0x1

    #@15
    move/from16 v0, v21

    #@17
    move/from16 v1, v22

    #@19
    if-eq v0, v1, :cond_106

    #@1b
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@1e
    move-result v7

    #@1f
    .local v7, depth:I
    if-ge v7, v10, :cond_29

    #@21
    const/16 v22, 0x3

    #@23
    move/from16 v0, v21

    #@25
    move/from16 v1, v22

    #@27
    if-eq v0, v1, :cond_106

    #@29
    .line 189
    :cond_29
    const/16 v22, 0x2

    #@2b
    move/from16 v0, v21

    #@2d
    move/from16 v1, v22

    #@2f
    if-ne v0, v1, :cond_f

    #@31
    .line 193
    if-gt v7, v10, :cond_f

    #@33
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@36
    move-result-object v22

    #@37
    const-string/jumbo v23, "item"

    #@3a
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v22

    #@3e
    if-eqz v22, :cond_f

    #@40
    .line 197
    const/4 v6, 0x0

    #@41
    .line 198
    .local v6, colorRes:I
    const/high16 v4, -0x1

    #@43
    .line 199
    .local v4, color:I
    const/4 v8, 0x0

    #@44
    .line 202
    .local v8, haveColor:Z
    const/4 v11, 0x0

    #@45
    .line 203
    .local v11, j:I
    invoke-interface/range {p3 .. p3}, Landroid/util/AttributeSet;->getAttributeCount()I

    #@48
    move-result v17

    #@49
    .line 204
    .local v17, numAttrs:I
    move/from16 v0, v17

    #@4b
    new-array v0, v0, [I

    #@4d
    move-object/from16 v19, v0

    #@4f
    .line 205
    .local v19, stateSpec:[I
    const/4 v9, 0x0

    #@50
    .local v9, i:I
    move v12, v11

    #@51
    .end local v11           #j:I
    .local v12, j:I
    :goto_51
    move/from16 v0, v17

    #@53
    if-ge v9, v0, :cond_5d

    #@55
    .line 206
    move-object/from16 v0, p3

    #@57
    invoke-interface {v0, v9}, Landroid/util/AttributeSet;->getAttributeNameResource(I)I

    #@5a
    move-result v18

    #@5b
    .line 207
    .local v18, stateResId:I
    if-nez v18, :cond_af

    #@5d
    .line 221
    .end local v18           #stateResId:I
    :cond_5d
    move-object/from16 v0, v19

    #@5f
    invoke-static {v0, v12}, Landroid/util/StateSet;->trimStateSet([II)[I

    #@62
    move-result-object v19

    #@63
    .line 223
    if-eqz v6, :cond_e7

    #@65
    .line 224
    move-object/from16 v0, p1

    #@67
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    #@6a
    move-result v4

    #@6b
    .line 231
    :cond_6b
    if-eqz v14, :cond_74

    #@6d
    move-object/from16 v0, v19

    #@6f
    array-length v0, v0

    #@70
    move/from16 v22, v0

    #@72
    if-nez v22, :cond_78

    #@74
    .line 232
    :cond_74
    move-object/from16 v0, p0

    #@76
    iput v4, v0, Landroid/content/res/ColorStateList;->mDefaultColor:I

    #@78
    .line 235
    :cond_78
    add-int/lit8 v22, v14, 0x1

    #@7a
    move/from16 v0, v22

    #@7c
    if-lt v0, v13, :cond_a7

    #@7e
    .line 236
    add-int/lit8 v22, v14, 0x1

    #@80
    invoke-static/range {v22 .. v22}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@83
    move-result v13

    #@84
    .line 238
    new-array v15, v13, [I

    #@86
    .line 239
    .local v15, ncolor:[I
    const/16 v22, 0x0

    #@88
    const/16 v23, 0x0

    #@8a
    move/from16 v0, v22

    #@8c
    move/from16 v1, v23

    #@8e
    invoke-static {v5, v0, v15, v1, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@91
    .line 241
    new-array v0, v13, [[I

    #@93
    move-object/from16 v16, v0

    #@95
    .line 242
    .local v16, nstate:[[I
    const/16 v22, 0x0

    #@97
    const/16 v23, 0x0

    #@99
    move-object/from16 v0, v20

    #@9b
    move/from16 v1, v22

    #@9d
    move-object/from16 v2, v16

    #@9f
    move/from16 v3, v23

    #@a1
    invoke-static {v0, v1, v2, v3, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@a4
    .line 244
    move-object v5, v15

    #@a5
    .line 245
    move-object/from16 v20, v16

    #@a7
    .line 248
    .end local v15           #ncolor:[I
    .end local v16           #nstate:[[I
    :cond_a7
    aput v4, v5, v14

    #@a9
    .line 249
    aput-object v19, v20, v14

    #@ab
    .line 250
    add-int/lit8 v14, v14, 0x1

    #@ad
    .line 251
    goto/16 :goto_f

    #@af
    .line 208
    .restart local v18       #stateResId:I
    :cond_af
    const v22, 0x10101a5

    #@b2
    move/from16 v0, v18

    #@b4
    move/from16 v1, v22

    #@b6
    if-ne v0, v1, :cond_d0

    #@b8
    .line 209
    const/16 v22, 0x0

    #@ba
    move-object/from16 v0, p3

    #@bc
    move/from16 v1, v22

    #@be
    invoke-interface {v0, v9, v1}, Landroid/util/AttributeSet;->getAttributeResourceValue(II)I

    #@c1
    move-result v6

    #@c2
    .line 211
    if-nez v6, :cond_143

    #@c4
    .line 212
    move-object/from16 v0, p3

    #@c6
    invoke-interface {v0, v9, v4}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    #@c9
    move-result v4

    #@ca
    .line 213
    const/4 v8, 0x1

    #@cb
    move v11, v12

    #@cc
    .line 205
    .end local v12           #j:I
    .end local v18           #stateResId:I
    .restart local v11       #j:I
    :goto_cc
    add-int/lit8 v9, v9, 0x1

    #@ce
    move v12, v11

    #@cf
    .end local v11           #j:I
    .restart local v12       #j:I
    goto :goto_51

    #@d0
    .line 216
    .restart local v18       #stateResId:I
    :cond_d0
    add-int/lit8 v11, v12, 0x1

    #@d2
    .end local v12           #j:I
    .restart local v11       #j:I
    const/16 v22, 0x0

    #@d4
    move-object/from16 v0, p3

    #@d6
    move/from16 v1, v22

    #@d8
    invoke-interface {v0, v9, v1}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    #@db
    move-result v22

    #@dc
    if-eqz v22, :cond_e1

    #@de
    .end local v18           #stateResId:I
    :goto_de
    aput v18, v19, v12

    #@e0
    goto :goto_cc

    #@e1
    .restart local v18       #stateResId:I
    :cond_e1
    move/from16 v0, v18

    #@e3
    neg-int v0, v0

    #@e4
    move/from16 v18, v0

    #@e6
    goto :goto_de

    #@e7
    .line 225
    .end local v11           #j:I
    .end local v18           #stateResId:I
    .restart local v12       #j:I
    :cond_e7
    if-nez v8, :cond_6b

    #@e9
    .line 226
    new-instance v22, Lorg/xmlpull/v1/XmlPullParserException;

    #@eb
    new-instance v23, Ljava/lang/StringBuilder;

    #@ed
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@f0
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@f3
    move-result-object v24

    #@f4
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v23

    #@f8
    const-string v24, ": <item> tag requires a \'android:color\' attribute."

    #@fa
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v23

    #@fe
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@101
    move-result-object v23

    #@102
    invoke-direct/range {v22 .. v23}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@105
    throw v22

    #@106
    .line 253
    .end local v4           #color:I
    .end local v6           #colorRes:I
    .end local v7           #depth:I
    .end local v8           #haveColor:Z
    .end local v9           #i:I
    .end local v12           #j:I
    .end local v17           #numAttrs:I
    .end local v19           #stateSpec:[I
    :cond_106
    new-array v0, v14, [I

    #@108
    move-object/from16 v22, v0

    #@10a
    move-object/from16 v0, v22

    #@10c
    move-object/from16 v1, p0

    #@10e
    iput-object v0, v1, Landroid/content/res/ColorStateList;->mColors:[I

    #@110
    .line 254
    new-array v0, v14, [[I

    #@112
    move-object/from16 v22, v0

    #@114
    move-object/from16 v0, v22

    #@116
    move-object/from16 v1, p0

    #@118
    iput-object v0, v1, Landroid/content/res/ColorStateList;->mStateSpecs:[[I

    #@11a
    .line 255
    const/16 v22, 0x0

    #@11c
    move-object/from16 v0, p0

    #@11e
    iget-object v0, v0, Landroid/content/res/ColorStateList;->mColors:[I

    #@120
    move-object/from16 v23, v0

    #@122
    const/16 v24, 0x0

    #@124
    move/from16 v0, v22

    #@126
    move-object/from16 v1, v23

    #@128
    move/from16 v2, v24

    #@12a
    invoke-static {v5, v0, v1, v2, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@12d
    .line 256
    const/16 v22, 0x0

    #@12f
    move-object/from16 v0, p0

    #@131
    iget-object v0, v0, Landroid/content/res/ColorStateList;->mStateSpecs:[[I

    #@133
    move-object/from16 v23, v0

    #@135
    const/16 v24, 0x0

    #@137
    move-object/from16 v0, v20

    #@139
    move/from16 v1, v22

    #@13b
    move-object/from16 v2, v23

    #@13d
    move/from16 v3, v24

    #@13f
    invoke-static {v0, v1, v2, v3, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@142
    .line 257
    return-void

    #@143
    .restart local v4       #color:I
    .restart local v6       #colorRes:I
    .restart local v7       #depth:I
    .restart local v8       #haveColor:Z
    .restart local v9       #i:I
    .restart local v12       #j:I
    .restart local v17       #numAttrs:I
    .restart local v18       #stateResId:I
    .restart local v19       #stateSpec:[I
    :cond_143
    move v11, v12

    #@144
    .end local v12           #j:I
    .restart local v11       #j:I
    goto :goto_cc
.end method

.method public static valueOf(I)Landroid/content/res/ColorStateList;
    .registers 8
    .parameter "color"

    #@0
    .prologue
    .line 98
    sget-object v4, Landroid/content/res/ColorStateList;->sCache:Landroid/util/SparseArray;

    #@2
    monitor-enter v4

    #@3
    .line 99
    :try_start_3
    sget-object v3, Landroid/content/res/ColorStateList;->sCache:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v3, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v2

    #@9
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@b
    .line 100
    .local v2, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/ColorStateList;>;"
    if-eqz v2, :cond_19

    #@d
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/content/res/ColorStateList;

    #@13
    move-object v0, v3

    #@14
    .line 102
    .local v0, csl:Landroid/content/res/ColorStateList;
    :goto_14
    if-eqz v0, :cond_1b

    #@16
    .line 103
    monitor-exit v4

    #@17
    move-object v1, v0

    #@18
    .line 108
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    .local v1, csl:Ljava/lang/Object;
    :goto_18
    return-object v1

    #@19
    .line 100
    .end local v1           #csl:Ljava/lang/Object;
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_14

    #@1b
    .line 106
    .restart local v0       #csl:Landroid/content/res/ColorStateList;
    :cond_1b
    new-instance v0, Landroid/content/res/ColorStateList;

    #@1d
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    sget-object v3, Landroid/content/res/ColorStateList;->EMPTY:[[I

    #@1f
    const/4 v5, 0x1

    #@20
    new-array v5, v5, [I

    #@22
    const/4 v6, 0x0

    #@23
    aput p0, v5, v6

    #@25
    invoke-direct {v0, v3, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    #@28
    .line 107
    .restart local v0       #csl:Landroid/content/res/ColorStateList;
    sget-object v3, Landroid/content/res/ColorStateList;->sCache:Landroid/util/SparseArray;

    #@2a
    new-instance v5, Ljava/lang/ref/WeakReference;

    #@2c
    invoke-direct {v5, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@2f
    invoke-virtual {v3, p0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@32
    .line 108
    monitor-exit v4

    #@33
    move-object v1, v0

    #@34
    .restart local v1       #csl:Ljava/lang/Object;
    goto :goto_18

    #@35
    .line 109
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    .end local v1           #csl:Ljava/lang/Object;
    .end local v2           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/ColorStateList;>;"
    :catchall_35
    move-exception v3

    #@36
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_3 .. :try_end_37} :catchall_35

    #@37
    throw v3
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 300
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getColorForState([II)I
    .registers 7
    .parameter "stateSet"
    .parameter "defaultColor"

    #@0
    .prologue
    .line 273
    iget-object v3, p0, Landroid/content/res/ColorStateList;->mStateSpecs:[[I

    #@2
    array-length v1, v3

    #@3
    .line 274
    .local v1, setLength:I
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    if-ge v0, v1, :cond_14

    #@6
    .line 275
    iget-object v3, p0, Landroid/content/res/ColorStateList;->mStateSpecs:[[I

    #@8
    aget-object v2, v3, v0

    #@a
    .line 276
    .local v2, stateSpec:[I
    invoke-static {v2, p1}, Landroid/util/StateSet;->stateSetMatches([I[I)Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_15

    #@10
    .line 277
    iget-object v3, p0, Landroid/content/res/ColorStateList;->mColors:[I

    #@12
    aget p2, v3, v0

    #@14
    .line 280
    .end local v2           #stateSpec:[I
    .end local p2
    :cond_14
    return p2

    #@15
    .line 274
    .restart local v2       #stateSpec:[I
    .restart local p2
    :cond_15
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_4
.end method

.method public getDefaultColor()I
    .registers 2

    #@0
    .prologue
    .line 289
    iget v0, p0, Landroid/content/res/ColorStateList;->mDefaultColor:I

    #@2
    return v0
.end method

.method public isStateful()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 260
    iget-object v1, p0, Landroid/content/res/ColorStateList;->mStateSpecs:[[I

    #@3
    array-length v1, v1

    #@4
    if-le v1, v0, :cond_7

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ColorStateList{mStateSpecs="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/content/res/ColorStateList;->mStateSpecs:[[I

    #@d
    invoke-static {v1}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string/jumbo v1, "mColors="

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    iget-object v1, p0, Landroid/content/res/ColorStateList;->mColors:[I

    #@1e
    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    const-string/jumbo v1, "mDefaultColor="

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    iget v1, p0, Landroid/content/res/ColorStateList;->mDefaultColor:I

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    const/16 v1, 0x7d

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    return-object v0
.end method

.method public withAlpha(I)Landroid/content/res/ColorStateList;
    .registers 7
    .parameter "alpha"

    #@0
    .prologue
    .line 160
    iget-object v3, p0, Landroid/content/res/ColorStateList;->mColors:[I

    #@2
    array-length v3, v3

    #@3
    new-array v0, v3, [I

    #@5
    .line 162
    .local v0, colors:[I
    array-length v2, v0

    #@6
    .line 163
    .local v2, len:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v2, :cond_19

    #@9
    .line 164
    iget-object v3, p0, Landroid/content/res/ColorStateList;->mColors:[I

    #@b
    aget v3, v3, v1

    #@d
    const v4, 0xffffff

    #@10
    and-int/2addr v3, v4

    #@11
    shl-int/lit8 v4, p1, 0x18

    #@13
    or-int/2addr v3, v4

    #@14
    aput v3, v0, v1

    #@16
    .line 163
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_7

    #@19
    .line 167
    :cond_19
    new-instance v3, Landroid/content/res/ColorStateList;

    #@1b
    iget-object v4, p0, Landroid/content/res/ColorStateList;->mStateSpecs:[[I

    #@1d
    invoke-direct {v3, v4, v0}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    #@20
    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 304
    iget-object v2, p0, Landroid/content/res/ColorStateList;->mStateSpecs:[[I

    #@2
    array-length v0, v2

    #@3
    .line 305
    .local v0, N:I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 306
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_13

    #@9
    .line 307
    iget-object v2, p0, Landroid/content/res/ColorStateList;->mStateSpecs:[[I

    #@b
    aget-object v2, v2, v1

    #@d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeIntArray([I)V

    #@10
    .line 306
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_7

    #@13
    .line 309
    :cond_13
    iget-object v2, p0, Landroid/content/res/ColorStateList;->mColors:[I

    #@15
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeIntArray([I)V

    #@18
    .line 310
    return-void
.end method
