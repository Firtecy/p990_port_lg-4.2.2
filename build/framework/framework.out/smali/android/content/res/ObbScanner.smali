.class public Landroid/content/res/ObbScanner;
.super Ljava/lang/Object;
.source "ObbScanner.java"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getObbInfo(Ljava/lang/String;)Landroid/content/res/ObbInfo;
    .registers 7
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 39
    if-nez p0, :cond_a

    #@2
    .line 40
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v4, "file path cannot be null"

    #@6
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v3

    #@a
    .line 43
    :cond_a
    new-instance v1, Ljava/io/File;

    #@c
    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@f
    .line 44
    .local v1, obbFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_2e

    #@15
    .line 45
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@17
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "OBB file does not exist: "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v3

    #@2e
    .line 52
    :cond_2e
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    .line 54
    .local v0, canonicalFilePath:Ljava/lang/String;
    new-instance v2, Landroid/content/res/ObbInfo;

    #@34
    invoke-direct {v2}, Landroid/content/res/ObbInfo;-><init>()V

    #@37
    .line 55
    .local v2, obbInfo:Landroid/content/res/ObbInfo;
    iput-object v0, v2, Landroid/content/res/ObbInfo;->filename:Ljava/lang/String;

    #@39
    .line 56
    invoke-static {v0, v2}, Landroid/content/res/ObbScanner;->getObbInfo_native(Ljava/lang/String;Landroid/content/res/ObbInfo;)V

    #@3c
    .line 58
    return-object v2
.end method

.method private static native getObbInfo_native(Ljava/lang/String;Landroid/content/res/ObbInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
