.class public final Landroid/content/res/Resources$Theme;
.super Ljava/lang/Object;
.source "Resources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/res/Resources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Theme"
.end annotation


# instance fields
.field private final mAssets:Landroid/content/res/AssetManager;

.field private final mTheme:I

.field final synthetic this$0:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1387
    iput-object p1, p0, Landroid/content/res/Resources$Theme;->this$0:Landroid/content/res/Resources;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1388
    iget-object v0, p1, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@7
    iput-object v0, p0, Landroid/content/res/Resources$Theme;->mAssets:Landroid/content/res/AssetManager;

    #@9
    .line 1389
    iget-object v0, p0, Landroid/content/res/Resources$Theme;->mAssets:Landroid/content/res/AssetManager;

    #@b
    invoke-virtual {v0}, Landroid/content/res/AssetManager;->createTheme()I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/content/res/Resources$Theme;->mTheme:I

    #@11
    .line 1390
    return-void
.end method


# virtual methods
.method public applyStyle(IZ)V
    .registers 4
    .parameter "resid"
    .parameter "force"

    #@0
    .prologue
    .line 1143
    iget v0, p0, Landroid/content/res/Resources$Theme;->mTheme:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/content/res/AssetManager;->applyThemeStyle(IIZ)V

    #@5
    .line 1144
    return-void
.end method

.method public dump(ILjava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "priority"
    .parameter "tag"
    .parameter "prefix"

    #@0
    .prologue
    .line 1379
    iget v0, p0, Landroid/content/res/Resources$Theme;->mTheme:I

    #@2
    invoke-static {v0, p1, p2, p3}, Landroid/content/res/AssetManager;->dumpTheme(IILjava/lang/String;Ljava/lang/String;)V

    #@5
    .line 1380
    return-void
.end method

.method protected finalize()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 1383
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@3
    .line 1384
    iget-object v0, p0, Landroid/content/res/Resources$Theme;->mAssets:Landroid/content/res/AssetManager;

    #@5
    iget v1, p0, Landroid/content/res/Resources$Theme;->mTheme:I

    #@7
    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->releaseTheme(I)V

    #@a
    .line 1385
    return-void
.end method

.method public obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
    .registers 12
    .parameter "resid"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1209
    array-length v8, p2

    #@2
    .line 1210
    .local v8, len:I
    iget-object v0, p0, Landroid/content/res/Resources$Theme;->this$0:Landroid/content/res/Resources;

    #@4
    #calls: Landroid/content/res/Resources;->getCachedStyledAttributes(I)Landroid/content/res/TypedArray;
    invoke-static {v0, v8}, Landroid/content/res/Resources;->access$000(Landroid/content/res/Resources;I)Landroid/content/res/TypedArray;

    #@7
    move-result-object v7

    #@8
    .line 1211
    .local v7, array:Landroid/content/res/TypedArray;
    iput-object p2, v7, Landroid/content/res/TypedArray;->mRsrcs:[I

    #@a
    .line 1213
    iget v0, p0, Landroid/content/res/Resources$Theme;->mTheme:I

    #@c
    iget-object v5, v7, Landroid/content/res/TypedArray;->mData:[I

    #@e
    iget-object v6, v7, Landroid/content/res/TypedArray;->mIndices:[I

    #@10
    move v2, p1

    #@11
    move v3, v1

    #@12
    move-object v4, p2

    #@13
    invoke-static/range {v0 .. v6}, Landroid/content/res/AssetManager;->applyStyle(IIII[I[I[I)Z

    #@16
    .line 1241
    return-object v7
.end method

.method public obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
    .registers 15
    .parameter "set"
    .parameter "attrs"
    .parameter "defStyleAttr"
    .parameter "defStyleRes"

    #@0
    .prologue
    .line 1294
    array-length v8, p2

    #@1
    .line 1295
    .local v8, len:I
    iget-object v0, p0, Landroid/content/res/Resources$Theme;->this$0:Landroid/content/res/Resources;

    #@3
    #calls: Landroid/content/res/Resources;->getCachedStyledAttributes(I)Landroid/content/res/TypedArray;
    invoke-static {v0, v8}, Landroid/content/res/Resources;->access$000(Landroid/content/res/Resources;I)Landroid/content/res/TypedArray;

    #@6
    move-result-object v7

    #@7
    .local v7, array:Landroid/content/res/TypedArray;
    move-object v9, p1

    #@8
    .line 1301
    check-cast v9, Landroid/content/res/XmlBlock$Parser;

    #@a
    .line 1302
    .local v9, parser:Landroid/content/res/XmlBlock$Parser;
    iget v0, p0, Landroid/content/res/Resources$Theme;->mTheme:I

    #@c
    if-eqz v9, :cond_1f

    #@e
    iget v3, v9, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@10
    :goto_10
    iget-object v5, v7, Landroid/content/res/TypedArray;->mData:[I

    #@12
    iget-object v6, v7, Landroid/content/res/TypedArray;->mIndices:[I

    #@14
    move v1, p3

    #@15
    move v2, p4

    #@16
    move-object v4, p2

    #@17
    invoke-static/range {v0 .. v6}, Landroid/content/res/AssetManager;->applyStyle(IIII[I[I[I)Z

    #@1a
    .line 1307
    iput-object p2, v7, Landroid/content/res/TypedArray;->mRsrcs:[I

    #@1c
    .line 1308
    iput-object v9, v7, Landroid/content/res/TypedArray;->mXml:Landroid/content/res/XmlBlock$Parser;

    #@1e
    .line 1339
    return-object v7

    #@1f
    .line 1302
    :cond_1f
    const/4 v3, 0x0

    #@20
    goto :goto_10
.end method

.method public obtainStyledAttributes([I)Landroid/content/res/TypedArray;
    .registers 11
    .parameter "attrs"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1179
    array-length v8, p1

    #@2
    .line 1180
    .local v8, len:I
    iget-object v0, p0, Landroid/content/res/Resources$Theme;->this$0:Landroid/content/res/Resources;

    #@4
    #calls: Landroid/content/res/Resources;->getCachedStyledAttributes(I)Landroid/content/res/TypedArray;
    invoke-static {v0, v8}, Landroid/content/res/Resources;->access$000(Landroid/content/res/Resources;I)Landroid/content/res/TypedArray;

    #@7
    move-result-object v7

    #@8
    .line 1181
    .local v7, array:Landroid/content/res/TypedArray;
    iput-object p1, v7, Landroid/content/res/TypedArray;->mRsrcs:[I

    #@a
    .line 1182
    iget v0, p0, Landroid/content/res/Resources$Theme;->mTheme:I

    #@c
    iget-object v5, v7, Landroid/content/res/TypedArray;->mData:[I

    #@e
    iget-object v6, v7, Landroid/content/res/TypedArray;->mIndices:[I

    #@10
    move v2, v1

    #@11
    move v3, v1

    #@12
    move-object v4, p1

    #@13
    invoke-static/range {v0 .. v6}, Landroid/content/res/AssetManager;->applyStyle(IIII[I[I[I)Z

    #@16
    .line 1184
    return-object v7
.end method

.method public resolveAttribute(ILandroid/util/TypedValue;Z)Z
    .registers 7
    .parameter "resid"
    .parameter "outValue"
    .parameter "resolveRefs"

    #@0
    .prologue
    .line 1361
    iget-object v1, p0, Landroid/content/res/Resources$Theme;->mAssets:Landroid/content/res/AssetManager;

    #@2
    iget v2, p0, Landroid/content/res/Resources$Theme;->mTheme:I

    #@4
    invoke-virtual {v1, v2, p1, p2, p3}, Landroid/content/res/AssetManager;->getThemeValue(IILandroid/util/TypedValue;Z)Z

    #@7
    move-result v0

    #@8
    .line 1368
    .local v0, got:Z
    return v0
.end method

.method public setTo(Landroid/content/res/Resources$Theme;)V
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 1156
    iget v0, p0, Landroid/content/res/Resources$Theme;->mTheme:I

    #@2
    iget v1, p1, Landroid/content/res/Resources$Theme;->mTheme:I

    #@4
    invoke-static {v0, v1}, Landroid/content/res/AssetManager;->copyTheme(II)V

    #@7
    .line 1157
    return-void
.end method
