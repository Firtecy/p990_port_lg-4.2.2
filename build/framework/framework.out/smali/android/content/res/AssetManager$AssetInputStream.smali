.class public final Landroid/content/res/AssetManager$AssetInputStream;
.super Ljava/io/InputStream;
.source "AssetManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/res/AssetManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "AssetInputStream"
.end annotation


# instance fields
.field private mAsset:I

.field private mLength:J

.field private mMarkPos:J

.field final synthetic this$0:Landroid/content/res/AssetManager;


# direct methods
.method private constructor <init>(Landroid/content/res/AssetManager;I)V
    .registers 5
    .parameter
    .parameter "asset"

    #@0
    .prologue
    .line 543
    iput-object p1, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@2
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    #@5
    .line 544
    iput p2, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@7
    .line 545
    #calls: Landroid/content/res/AssetManager;->getAssetLength(I)J
    invoke-static {p1, p2}, Landroid/content/res/AssetManager;->access$100(Landroid/content/res/AssetManager;I)J

    #@a
    move-result-wide v0

    #@b
    iput-wide v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->mLength:J

    #@d
    .line 546
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/res/AssetManager;ILandroid/content/res/AssetManager$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 538
    invoke-direct {p0, p1, p2}, Landroid/content/res/AssetManager$AssetInputStream;-><init>(Landroid/content/res/AssetManager;I)V

    #@3
    return-void
.end method


# virtual methods
.method public final available()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 554
    iget-object v2, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@2
    iget v3, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@4
    #calls: Landroid/content/res/AssetManager;->getAssetRemainingLength(I)J
    invoke-static {v2, v3}, Landroid/content/res/AssetManager;->access$300(Landroid/content/res/AssetManager;I)J

    #@7
    move-result-wide v0

    #@8
    .line 555
    .local v0, len:J
    const-wide/32 v2, 0x7fffffff

    #@b
    cmp-long v2, v0, v2

    #@d
    if-lez v2, :cond_13

    #@f
    const v2, 0x7fffffff

    #@12
    :goto_12
    return v2

    #@13
    :cond_13
    long-to-int v2, v0

    #@14
    goto :goto_12
.end method

.method public final close()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 558
    iget-object v1, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@2
    monitor-enter v1

    #@3
    .line 559
    :try_start_3
    iget v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@5
    if-eqz v0, :cond_1a

    #@7
    .line 560
    iget-object v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@9
    iget v2, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@b
    #calls: Landroid/content/res/AssetManager;->destroyAsset(I)V
    invoke-static {v0, v2}, Landroid/content/res/AssetManager;->access$400(Landroid/content/res/AssetManager;I)V

    #@e
    .line 561
    const/4 v0, 0x0

    #@f
    iput v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@11
    .line 562
    iget-object v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@13
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@16
    move-result v2

    #@17
    #calls: Landroid/content/res/AssetManager;->decRefsLocked(I)V
    invoke-static {v0, v2}, Landroid/content/res/AssetManager;->access$500(Landroid/content/res/AssetManager;I)V

    #@1a
    .line 564
    :cond_1a
    monitor-exit v1

    #@1b
    .line 565
    return-void

    #@1c
    .line 564
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 591
    invoke-virtual {p0}, Landroid/content/res/AssetManager$AssetInputStream;->close()V

    #@3
    .line 592
    return-void
.end method

.method public final getAssetInt()I
    .registers 2

    #@0
    .prologue
    .line 540
    iget v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@2
    return v0
.end method

.method public final mark(I)V
    .registers 7
    .parameter "readlimit"

    #@0
    .prologue
    .line 567
    iget-object v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@2
    iget v1, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@4
    const-wide/16 v2, 0x0

    #@6
    const/4 v4, 0x0

    #@7
    #calls: Landroid/content/res/AssetManager;->seekAsset(IJI)J
    invoke-static {v0, v1, v2, v3, v4}, Landroid/content/res/AssetManager;->access$600(Landroid/content/res/AssetManager;IJI)J

    #@a
    move-result-wide v0

    #@b
    iput-wide v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->mMarkPos:J

    #@d
    .line 568
    return-void
.end method

.method public final markSupported()Z
    .registers 2

    #@0
    .prologue
    .line 551
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public final read()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 548
    iget-object v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@2
    iget v1, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@4
    #calls: Landroid/content/res/AssetManager;->readAssetChar(I)I
    invoke-static {v0, v1}, Landroid/content/res/AssetManager;->access$200(Landroid/content/res/AssetManager;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final read([B)I
    .registers 6
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 573
    iget-object v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@2
    iget v1, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@4
    const/4 v2, 0x0

    #@5
    array-length v3, p1

    #@6
    #calls: Landroid/content/res/AssetManager;->readAsset(I[BII)I
    invoke-static {v0, v1, p1, v2, v3}, Landroid/content/res/AssetManager;->access$700(Landroid/content/res/AssetManager;I[BII)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final read([BII)I
    .registers 6
    .parameter "b"
    .parameter "off"
    .parameter "len"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 576
    iget-object v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@2
    iget v1, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@4
    #calls: Landroid/content/res/AssetManager;->readAsset(I[BII)I
    invoke-static {v0, v1, p1, p2, p3}, Landroid/content/res/AssetManager;->access$700(Landroid/content/res/AssetManager;I[BII)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final reset()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 570
    iget-object v0, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@2
    iget v1, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@4
    iget-wide v2, p0, Landroid/content/res/AssetManager$AssetInputStream;->mMarkPos:J

    #@6
    const/4 v4, -0x1

    #@7
    #calls: Landroid/content/res/AssetManager;->seekAsset(IJI)J
    invoke-static {v0, v1, v2, v3, v4}, Landroid/content/res/AssetManager;->access$600(Landroid/content/res/AssetManager;IJI)J

    #@a
    .line 571
    return-void
.end method

.method public final skip(J)J
    .registers 12
    .parameter "n"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v7, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    .line 579
    iget-object v2, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@5
    iget v3, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@7
    #calls: Landroid/content/res/AssetManager;->seekAsset(IJI)J
    invoke-static {v2, v3, v7, v8, v6}, Landroid/content/res/AssetManager;->access$600(Landroid/content/res/AssetManager;IJI)J

    #@a
    move-result-wide v0

    #@b
    .line 580
    .local v0, pos:J
    add-long v2, v0, p1

    #@d
    iget-wide v4, p0, Landroid/content/res/AssetManager$AssetInputStream;->mLength:J

    #@f
    cmp-long v2, v2, v4

    #@11
    if-lez v2, :cond_17

    #@13
    .line 581
    iget-wide v2, p0, Landroid/content/res/AssetManager$AssetInputStream;->mLength:J

    #@15
    sub-long p1, v2, v0

    #@17
    .line 583
    :cond_17
    cmp-long v2, p1, v7

    #@19
    if-lez v2, :cond_22

    #@1b
    .line 584
    iget-object v2, p0, Landroid/content/res/AssetManager$AssetInputStream;->this$0:Landroid/content/res/AssetManager;

    #@1d
    iget v3, p0, Landroid/content/res/AssetManager$AssetInputStream;->mAsset:I

    #@1f
    #calls: Landroid/content/res/AssetManager;->seekAsset(IJI)J
    invoke-static {v2, v3, p1, p2, v6}, Landroid/content/res/AssetManager;->access$600(Landroid/content/res/AssetManager;IJI)J

    #@22
    .line 586
    :cond_22
    return-wide p1
.end method
