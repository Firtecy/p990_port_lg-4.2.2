.class public final Landroid/content/res/Configuration;
.super Ljava/lang/Object;
.source "Configuration.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Landroid/content/res/Configuration;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/res/Configuration;",
            ">;"
        }
    .end annotation
.end field

.field public static final DENSITY_DPI_UNDEFINED:I = 0x0

.field public static final EMPTY:Landroid/content/res/Configuration; = null

.field public static final HARDKEYBOARDHIDDEN_NO:I = 0x1

.field public static final HARDKEYBOARDHIDDEN_UNDEFINED:I = 0x0

.field public static final HARDKEYBOARDHIDDEN_YES:I = 0x2

.field public static final KEYBOARDHIDDEN_NO:I = 0x1

.field public static final KEYBOARDHIDDEN_SOFT:I = 0x3

.field public static final KEYBOARDHIDDEN_UNDEFINED:I = 0x0

.field public static final KEYBOARDHIDDEN_YES:I = 0x2

.field public static final KEYBOARD_12KEY:I = 0x3

.field public static final KEYBOARD_NOKEYS:I = 0x1

.field public static final KEYBOARD_QWERTY:I = 0x2

.field public static final KEYBOARD_UNDEFINED:I = 0x0

.field public static final NAVIGATIONHIDDEN_NO:I = 0x1

.field public static final NAVIGATIONHIDDEN_UNDEFINED:I = 0x0

.field public static final NAVIGATIONHIDDEN_YES:I = 0x2

.field public static final NAVIGATION_DPAD:I = 0x2

.field public static final NAVIGATION_NONAV:I = 0x1

.field public static final NAVIGATION_TRACKBALL:I = 0x3

.field public static final NAVIGATION_UNDEFINED:I = 0x0

.field public static final NAVIGATION_WHEEL:I = 0x4

.field public static final ORIENTATION_LANDSCAPE:I = 0x2

.field public static final ORIENTATION_PORTRAIT:I = 0x1

.field public static final ORIENTATION_SQUARE:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ORIENTATION_UNDEFINED:I = 0x0

.field public static final SCREENLAYOUT_COMPAT_NEEDED:I = 0x10000000

.field public static final SCREENLAYOUT_LAYOUTDIR_LTR:I = 0x40

.field public static final SCREENLAYOUT_LAYOUTDIR_MASK:I = 0xc0

.field public static final SCREENLAYOUT_LAYOUTDIR_RTL:I = 0x80

.field public static final SCREENLAYOUT_LAYOUTDIR_SHIFT:I = 0x6

.field public static final SCREENLAYOUT_LAYOUTDIR_UNDEFINED:I = 0x0

.field public static final SCREENLAYOUT_LONG_MASK:I = 0x30

.field public static final SCREENLAYOUT_LONG_NO:I = 0x10

.field public static final SCREENLAYOUT_LONG_UNDEFINED:I = 0x0

.field public static final SCREENLAYOUT_LONG_YES:I = 0x20

.field public static final SCREENLAYOUT_SIZE_LARGE:I = 0x3

.field public static final SCREENLAYOUT_SIZE_MASK:I = 0xf

.field public static final SCREENLAYOUT_SIZE_NORMAL:I = 0x2

.field public static final SCREENLAYOUT_SIZE_SMALL:I = 0x1

.field public static final SCREENLAYOUT_SIZE_UNDEFINED:I = 0x0

.field public static final SCREENLAYOUT_SIZE_XLARGE:I = 0x4

.field public static final SCREENLAYOUT_UNDEFINED:I = 0x0

.field public static final SCREEN_HEIGHT_DP_UNDEFINED:I = 0x0

.field public static final SCREEN_WIDTH_DP_UNDEFINED:I = 0x0

.field public static final SMALLEST_SCREEN_WIDTH_DP_UNDEFINED:I = 0x0

.field public static final TOUCHSCREEN_FINGER:I = 0x3

.field public static final TOUCHSCREEN_NOTOUCH:I = 0x1

.field public static final TOUCHSCREEN_STYLUS:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOUCHSCREEN_UNDEFINED:I = 0x0

.field public static final UI_MODE_NIGHT_MASK:I = 0x30

.field public static final UI_MODE_NIGHT_NO:I = 0x10

.field public static final UI_MODE_NIGHT_UNDEFINED:I = 0x0

.field public static final UI_MODE_NIGHT_YES:I = 0x20

.field public static final UI_MODE_TYPE_APPLIANCE:I = 0x5

.field public static final UI_MODE_TYPE_CAR:I = 0x3

.field public static final UI_MODE_TYPE_DESK:I = 0x2

.field public static final UI_MODE_TYPE_MASK:I = 0xf

.field public static final UI_MODE_TYPE_NORMAL:I = 0x1

.field public static final UI_MODE_TYPE_TELEVISION:I = 0x4

.field public static final UI_MODE_TYPE_UNDEFINED:I


# instance fields
.field public compatScreenHeightDp:I

.field public compatScreenWidthDp:I

.field public compatSmallestScreenWidthDp:I

.field public densityDpi:I

.field public fontScale:F

.field public fontTypeIndex:I

.field public hardKeyboardHidden:I

.field public keyboard:I

.field public keyboardHidden:I

.field public locale:Ljava/util/Locale;

.field public mcc:I

.field public mnc:I

.field public navigation:I

.field public navigationHidden:I

.field public orientation:I

.field public screenHeightDp:I

.field public screenLayout:I

.field public screenWidthDp:I

.field public seq:I

.field public smallestScreenWidthDp:I

.field public themePackage:Ljava/lang/String;

.field public touchscreen:I

.field public uiMode:I

.field public userSetLocale:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 39
    new-instance v0, Landroid/content/res/Configuration;

    #@2
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@5
    sput-object v0, Landroid/content/res/Configuration;->EMPTY:Landroid/content/res/Configuration;

    #@7
    .line 1188
    new-instance v0, Landroid/content/res/Configuration$1;

    #@9
    invoke-direct {v0}, Landroid/content/res/Configuration$1;-><init>()V

    #@c
    sput-object v0, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 557
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 558
    invoke-virtual {p0}, Landroid/content/res/Configuration;->setToDefaults()V

    #@6
    .line 559
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter "o"

    #@0
    .prologue
    .line 564
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 565
    invoke-virtual {p0, p1}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    #@6
    .line 566
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 1202
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1203
    invoke-virtual {p0, p1}, Landroid/content/res/Configuration;->readFromParcel(Landroid/os/Parcel;)V

    #@6
    .line 1204
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/res/Configuration$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private static getScreenLayoutNoDirection(I)I
    .registers 2
    .parameter "screenLayout"

    #@0
    .prologue
    .line 1361
    and-int/lit16 v0, p0, -0xc1

    #@2
    return v0
.end method

.method public static needNewResources(II)Z
    .registers 3
    .parameter "configChanges"
    .parameter "interestingChanges"

    #@0
    .prologue
    .line 1071
    const/high16 v0, 0x4000

    #@2
    or-int/2addr v0, p1

    #@3
    and-int/2addr v0, p0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public static reduceScreenLayout(III)I
    .registers 9
    .parameter "curLayout"
    .parameter "longSizeDp"
    .parameter "shortSizeDp"

    #@0
    .prologue
    .line 199
    const/16 v4, 0x1d6

    #@2
    if-ge p1, v4, :cond_1b

    #@4
    .line 202
    const/4 v3, 0x1

    #@5
    .line 203
    .local v3, screenLayoutSize:I
    const/4 v2, 0x0

    #@6
    .line 204
    .local v2, screenLayoutLong:Z
    const/4 v1, 0x0

    #@7
    .line 239
    .local v1, screenLayoutCompatNeeded:Z
    :goto_7
    if-nez v2, :cond_d

    #@9
    .line 240
    and-int/lit8 v4, p0, -0x31

    #@b
    or-int/lit8 p0, v4, 0x10

    #@d
    .line 242
    :cond_d
    if-eqz v1, :cond_12

    #@f
    .line 243
    const/high16 v4, 0x1000

    #@11
    or-int/2addr p0, v4

    #@12
    .line 245
    :cond_12
    and-int/lit8 v0, p0, 0xf

    #@14
    .line 246
    .local v0, curSize:I
    if-ge v3, v0, :cond_1a

    #@16
    .line 247
    and-int/lit8 v4, p0, -0x10

    #@18
    or-int p0, v4, v3

    #@1a
    .line 249
    :cond_1a
    return p0

    #@1b
    .line 207
    .end local v0           #curSize:I
    .end local v1           #screenLayoutCompatNeeded:Z
    .end local v2           #screenLayoutLong:Z
    .end local v3           #screenLayoutSize:I
    :cond_1b
    const/16 v4, 0x3c0

    #@1d
    if-lt p1, v4, :cond_37

    #@1f
    const/16 v4, 0x2d0

    #@21
    if-lt p2, v4, :cond_37

    #@23
    .line 210
    const/4 v3, 0x4

    #@24
    .line 222
    .restart local v3       #screenLayoutSize:I
    :goto_24
    const/16 v4, 0x141

    #@26
    if-gt p2, v4, :cond_2c

    #@28
    const/16 v4, 0x23a

    #@2a
    if-le p1, v4, :cond_43

    #@2c
    .line 223
    :cond_2c
    const/4 v1, 0x1

    #@2d
    .line 229
    .restart local v1       #screenLayoutCompatNeeded:Z
    :goto_2d
    mul-int/lit8 v4, p1, 0x3

    #@2f
    div-int/lit8 v4, v4, 0x5

    #@31
    add-int/lit8 v5, p2, -0x1

    #@33
    if-lt v4, v5, :cond_45

    #@35
    .line 231
    const/4 v2, 0x1

    #@36
    .restart local v2       #screenLayoutLong:Z
    goto :goto_7

    #@37
    .line 211
    .end local v1           #screenLayoutCompatNeeded:Z
    .end local v2           #screenLayoutLong:Z
    .end local v3           #screenLayoutSize:I
    :cond_37
    const/16 v4, 0x280

    #@39
    if-lt p1, v4, :cond_41

    #@3b
    const/16 v4, 0x1e0

    #@3d
    if-lt p2, v4, :cond_41

    #@3f
    .line 214
    const/4 v3, 0x3

    #@40
    .restart local v3       #screenLayoutSize:I
    goto :goto_24

    #@41
    .line 216
    .end local v3           #screenLayoutSize:I
    :cond_41
    const/4 v3, 0x2

    #@42
    .restart local v3       #screenLayoutSize:I
    goto :goto_24

    #@43
    .line 225
    :cond_43
    const/4 v1, 0x0

    #@44
    .restart local v1       #screenLayoutCompatNeeded:Z
    goto :goto_2d

    #@45
    .line 233
    :cond_45
    const/4 v2, 0x0

    #@46
    .restart local v2       #screenLayoutLong:Z
    goto :goto_7
.end method

.method public static resetScreenLayout(I)I
    .registers 2
    .parameter "curLayout"

    #@0
    .prologue
    .line 184
    const v0, -0x10000040

    #@3
    and-int/2addr v0, p0

    #@4
    or-int/lit8 v0, v0, 0x24

    #@6
    return v0
.end method


# virtual methods
.method public compareTo(Landroid/content/res/Configuration;)I
    .registers 9
    .parameter "that"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, -0x1

    #@2
    .line 1208
    iget v0, p0, Landroid/content/res/Configuration;->fontScale:F

    #@4
    .line 1209
    .local v0, a:F
    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    #@6
    .line 1210
    .local v1, b:F
    cmpg-float v5, v0, v1

    #@8
    if-gez v5, :cond_c

    #@a
    move v2, v3

    #@b
    .line 1271
    :cond_b
    :goto_b
    return v2

    #@c
    .line 1211
    :cond_c
    cmpl-float v5, v0, v1

    #@e
    if-lez v5, :cond_12

    #@10
    move v2, v4

    #@11
    goto :goto_b

    #@12
    .line 1212
    :cond_12
    iget v5, p0, Landroid/content/res/Configuration;->mcc:I

    #@14
    iget v6, p1, Landroid/content/res/Configuration;->mcc:I

    #@16
    sub-int v2, v5, v6

    #@18
    .line 1213
    .local v2, n:I
    if-nez v2, :cond_b

    #@1a
    .line 1214
    iget v5, p0, Landroid/content/res/Configuration;->mnc:I

    #@1c
    iget v6, p1, Landroid/content/res/Configuration;->mnc:I

    #@1e
    sub-int v2, v5, v6

    #@20
    .line 1215
    if-nez v2, :cond_b

    #@22
    .line 1216
    iget-object v5, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@24
    if-nez v5, :cond_2c

    #@26
    .line 1217
    iget-object v5, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@28
    if-eqz v5, :cond_68

    #@2a
    move v2, v4

    #@2b
    goto :goto_b

    #@2c
    .line 1218
    :cond_2c
    iget-object v5, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@2e
    if-nez v5, :cond_32

    #@30
    move v2, v3

    #@31
    .line 1219
    goto :goto_b

    #@32
    .line 1221
    :cond_32
    iget-object v5, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@34
    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    iget-object v6, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@3a
    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@41
    move-result v2

    #@42
    .line 1222
    if-nez v2, :cond_b

    #@44
    .line 1223
    iget-object v5, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@46
    invoke-virtual {v5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    iget-object v6, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@4c
    invoke-virtual {v6}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@4f
    move-result-object v6

    #@50
    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@53
    move-result v2

    #@54
    .line 1224
    if-nez v2, :cond_b

    #@56
    .line 1225
    iget-object v5, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@58
    invoke-virtual {v5}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    #@5b
    move-result-object v5

    #@5c
    iget-object v6, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@5e
    invoke-virtual {v6}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@65
    move-result v2

    #@66
    .line 1226
    if-nez v2, :cond_b

    #@68
    .line 1228
    :cond_68
    iget v5, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@6a
    iget v6, p1, Landroid/content/res/Configuration;->touchscreen:I

    #@6c
    sub-int v2, v5, v6

    #@6e
    .line 1229
    if-nez v2, :cond_b

    #@70
    .line 1230
    iget v5, p0, Landroid/content/res/Configuration;->keyboard:I

    #@72
    iget v6, p1, Landroid/content/res/Configuration;->keyboard:I

    #@74
    sub-int v2, v5, v6

    #@76
    .line 1231
    if-nez v2, :cond_b

    #@78
    .line 1232
    iget v5, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@7a
    iget v6, p1, Landroid/content/res/Configuration;->keyboardHidden:I

    #@7c
    sub-int v2, v5, v6

    #@7e
    .line 1233
    if-nez v2, :cond_b

    #@80
    .line 1234
    iget v5, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@82
    iget v6, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@84
    sub-int v2, v5, v6

    #@86
    .line 1235
    if-nez v2, :cond_b

    #@88
    .line 1236
    iget v5, p0, Landroid/content/res/Configuration;->navigation:I

    #@8a
    iget v6, p1, Landroid/content/res/Configuration;->navigation:I

    #@8c
    sub-int v2, v5, v6

    #@8e
    .line 1237
    if-nez v2, :cond_b

    #@90
    .line 1238
    iget v5, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@92
    iget v6, p1, Landroid/content/res/Configuration;->navigationHidden:I

    #@94
    sub-int v2, v5, v6

    #@96
    .line 1239
    if-nez v2, :cond_b

    #@98
    .line 1240
    iget v5, p0, Landroid/content/res/Configuration;->orientation:I

    #@9a
    iget v6, p1, Landroid/content/res/Configuration;->orientation:I

    #@9c
    sub-int v2, v5, v6

    #@9e
    .line 1241
    if-nez v2, :cond_b

    #@a0
    .line 1242
    iget v5, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@a2
    iget v6, p1, Landroid/content/res/Configuration;->screenLayout:I

    #@a4
    sub-int v2, v5, v6

    #@a6
    .line 1243
    if-nez v2, :cond_b

    #@a8
    .line 1244
    iget v5, p0, Landroid/content/res/Configuration;->uiMode:I

    #@aa
    iget v6, p1, Landroid/content/res/Configuration;->uiMode:I

    #@ac
    sub-int v2, v5, v6

    #@ae
    .line 1245
    if-nez v2, :cond_b

    #@b0
    .line 1246
    iget v5, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@b2
    iget v6, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    #@b4
    sub-int v2, v5, v6

    #@b6
    .line 1247
    if-nez v2, :cond_b

    #@b8
    .line 1248
    iget v5, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@ba
    iget v6, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    #@bc
    sub-int v2, v5, v6

    #@be
    .line 1249
    if-nez v2, :cond_b

    #@c0
    .line 1250
    iget v5, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@c2
    iget v6, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@c4
    sub-int v2, v5, v6

    #@c6
    .line 1251
    if-nez v2, :cond_b

    #@c8
    .line 1252
    iget v5, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@ca
    iget v6, p1, Landroid/content/res/Configuration;->densityDpi:I

    #@cc
    sub-int v2, v5, v6

    #@ce
    .line 1255
    iget-object v5, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@d0
    if-nez v5, :cond_d9

    #@d2
    .line 1256
    iget-object v3, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@d4
    if-eqz v3, :cond_ea

    #@d6
    move v2, v4

    #@d7
    goto/16 :goto_b

    #@d9
    .line 1257
    :cond_d9
    iget-object v4, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@db
    if-nez v4, :cond_e0

    #@dd
    move v2, v3

    #@de
    .line 1258
    goto/16 :goto_b

    #@e0
    .line 1260
    :cond_e0
    iget-object v3, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@e2
    iget-object v4, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@e4
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@e7
    move-result v2

    #@e8
    .line 1261
    if-nez v2, :cond_b

    #@ea
    .line 1265
    :cond_ea
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@ec
    if-eqz v3, :cond_b

    #@ee
    .line 1266
    iget v3, p0, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@f0
    iget v4, p1, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@f2
    sub-int v2, v3, v4

    #@f4
    .line 1267
    if-eqz v2, :cond_b

    #@f6
    goto/16 :goto_b
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 37
    check-cast p1, Landroid/content/res/Configuration;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/content/res/Configuration;->compareTo(Landroid/content/res/Configuration;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 1107
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public diff(Landroid/content/res/Configuration;)I
    .registers 5
    .parameter "delta"

    #@0
    .prologue
    .line 973
    const/4 v0, 0x0

    #@1
    .line 974
    .local v0, changed:I
    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    #@3
    const/4 v2, 0x0

    #@4
    cmpl-float v1, v1, v2

    #@6
    if-lez v1, :cond_13

    #@8
    iget v1, p0, Landroid/content/res/Configuration;->fontScale:F

    #@a
    iget v2, p1, Landroid/content/res/Configuration;->fontScale:F

    #@c
    cmpl-float v1, v1, v2

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 975
    const/high16 v1, 0x4000

    #@12
    or-int/2addr v0, v1

    #@13
    .line 977
    :cond_13
    iget v1, p1, Landroid/content/res/Configuration;->mcc:I

    #@15
    if-eqz v1, :cond_1f

    #@17
    iget v1, p0, Landroid/content/res/Configuration;->mcc:I

    #@19
    iget v2, p1, Landroid/content/res/Configuration;->mcc:I

    #@1b
    if-eq v1, v2, :cond_1f

    #@1d
    .line 978
    or-int/lit8 v0, v0, 0x1

    #@1f
    .line 980
    :cond_1f
    iget v1, p1, Landroid/content/res/Configuration;->mnc:I

    #@21
    if-eqz v1, :cond_2b

    #@23
    iget v1, p0, Landroid/content/res/Configuration;->mnc:I

    #@25
    iget v2, p1, Landroid/content/res/Configuration;->mnc:I

    #@27
    if-eq v1, v2, :cond_2b

    #@29
    .line 981
    or-int/lit8 v0, v0, 0x2

    #@2b
    .line 983
    :cond_2b
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@2d
    if-eqz v1, :cond_41

    #@2f
    iget-object v1, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@31
    if-eqz v1, :cond_3d

    #@33
    iget-object v1, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@35
    iget-object v2, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@37
    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v1

    #@3b
    if-nez v1, :cond_41

    #@3d
    .line 985
    :cond_3d
    or-int/lit8 v0, v0, 0x4

    #@3f
    .line 986
    or-int/lit16 v0, v0, 0x2000

    #@41
    .line 988
    :cond_41
    iget v1, p1, Landroid/content/res/Configuration;->touchscreen:I

    #@43
    if-eqz v1, :cond_4d

    #@45
    iget v1, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@47
    iget v2, p1, Landroid/content/res/Configuration;->touchscreen:I

    #@49
    if-eq v1, v2, :cond_4d

    #@4b
    .line 990
    or-int/lit8 v0, v0, 0x8

    #@4d
    .line 992
    :cond_4d
    iget v1, p1, Landroid/content/res/Configuration;->keyboard:I

    #@4f
    if-eqz v1, :cond_59

    #@51
    iget v1, p0, Landroid/content/res/Configuration;->keyboard:I

    #@53
    iget v2, p1, Landroid/content/res/Configuration;->keyboard:I

    #@55
    if-eq v1, v2, :cond_59

    #@57
    .line 994
    or-int/lit8 v0, v0, 0x10

    #@59
    .line 996
    :cond_59
    iget v1, p1, Landroid/content/res/Configuration;->keyboardHidden:I

    #@5b
    if-eqz v1, :cond_65

    #@5d
    iget v1, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@5f
    iget v2, p1, Landroid/content/res/Configuration;->keyboardHidden:I

    #@61
    if-eq v1, v2, :cond_65

    #@63
    .line 998
    or-int/lit8 v0, v0, 0x20

    #@65
    .line 1000
    :cond_65
    iget v1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@67
    if-eqz v1, :cond_71

    #@69
    iget v1, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@6b
    iget v2, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@6d
    if-eq v1, v2, :cond_71

    #@6f
    .line 1002
    or-int/lit8 v0, v0, 0x20

    #@71
    .line 1004
    :cond_71
    iget v1, p1, Landroid/content/res/Configuration;->navigation:I

    #@73
    if-eqz v1, :cond_7d

    #@75
    iget v1, p0, Landroid/content/res/Configuration;->navigation:I

    #@77
    iget v2, p1, Landroid/content/res/Configuration;->navigation:I

    #@79
    if-eq v1, v2, :cond_7d

    #@7b
    .line 1006
    or-int/lit8 v0, v0, 0x40

    #@7d
    .line 1008
    :cond_7d
    iget v1, p1, Landroid/content/res/Configuration;->navigationHidden:I

    #@7f
    if-eqz v1, :cond_89

    #@81
    iget v1, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@83
    iget v2, p1, Landroid/content/res/Configuration;->navigationHidden:I

    #@85
    if-eq v1, v2, :cond_89

    #@87
    .line 1010
    or-int/lit8 v0, v0, 0x20

    #@89
    .line 1012
    :cond_89
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    #@8b
    if-eqz v1, :cond_95

    #@8d
    iget v1, p0, Landroid/content/res/Configuration;->orientation:I

    #@8f
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    #@91
    if-eq v1, v2, :cond_95

    #@93
    .line 1014
    or-int/lit16 v0, v0, 0x80

    #@95
    .line 1016
    :cond_95
    iget v1, p1, Landroid/content/res/Configuration;->screenLayout:I

    #@97
    invoke-static {v1}, Landroid/content/res/Configuration;->getScreenLayoutNoDirection(I)I

    #@9a
    move-result v1

    #@9b
    if-eqz v1, :cond_ad

    #@9d
    iget v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@9f
    invoke-static {v1}, Landroid/content/res/Configuration;->getScreenLayoutNoDirection(I)I

    #@a2
    move-result v1

    #@a3
    iget v2, p1, Landroid/content/res/Configuration;->screenLayout:I

    #@a5
    invoke-static {v2}, Landroid/content/res/Configuration;->getScreenLayoutNoDirection(I)I

    #@a8
    move-result v2

    #@a9
    if-eq v1, v2, :cond_ad

    #@ab
    .line 1020
    or-int/lit16 v0, v0, 0x100

    #@ad
    .line 1022
    :cond_ad
    iget v1, p1, Landroid/content/res/Configuration;->uiMode:I

    #@af
    if-eqz v1, :cond_b9

    #@b1
    iget v1, p0, Landroid/content/res/Configuration;->uiMode:I

    #@b3
    iget v2, p1, Landroid/content/res/Configuration;->uiMode:I

    #@b5
    if-eq v1, v2, :cond_b9

    #@b7
    .line 1024
    or-int/lit16 v0, v0, 0x200

    #@b9
    .line 1026
    :cond_b9
    iget v1, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    #@bb
    if-eqz v1, :cond_c5

    #@bd
    iget v1, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@bf
    iget v2, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    #@c1
    if-eq v1, v2, :cond_c5

    #@c3
    .line 1028
    or-int/lit16 v0, v0, 0x400

    #@c5
    .line 1030
    :cond_c5
    iget v1, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    #@c7
    if-eqz v1, :cond_d1

    #@c9
    iget v1, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@cb
    iget v2, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    #@cd
    if-eq v1, v2, :cond_d1

    #@cf
    .line 1032
    or-int/lit16 v0, v0, 0x400

    #@d1
    .line 1034
    :cond_d1
    iget v1, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@d3
    if-eqz v1, :cond_dd

    #@d5
    iget v1, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@d7
    iget v2, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@d9
    if-eq v1, v2, :cond_dd

    #@db
    .line 1036
    or-int/lit16 v0, v0, 0x800

    #@dd
    .line 1038
    :cond_dd
    iget v1, p1, Landroid/content/res/Configuration;->densityDpi:I

    #@df
    if-eqz v1, :cond_e9

    #@e1
    iget v1, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@e3
    iget v2, p1, Landroid/content/res/Configuration;->densityDpi:I

    #@e5
    if-eq v1, v2, :cond_e9

    #@e7
    .line 1040
    or-int/lit16 v0, v0, 0x1000

    #@e9
    .line 1043
    :cond_e9
    iget-object v1, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@eb
    if-eqz v1, :cond_fe

    #@ed
    iget-object v1, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@ef
    if-eqz v1, :cond_fb

    #@f1
    iget-object v1, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@f3
    iget-object v2, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@f5
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f8
    move-result v1

    #@f9
    if-nez v1, :cond_fe

    #@fb
    .line 1045
    :cond_fb
    const/high16 v1, 0x1000

    #@fd
    or-int/2addr v0, v1

    #@fe
    .line 1049
    :cond_fe
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@100
    if-eqz v1, :cond_10f

    #@102
    .line 1050
    iget v1, p1, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@104
    iget v2, p0, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@106
    if-eq v1, v2, :cond_10f

    #@108
    iget v1, p1, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@10a
    if-ltz v1, :cond_10f

    #@10c
    .line 1051
    const/high16 v1, 0x2000

    #@10e
    or-int/2addr v0, v1

    #@10f
    .line 1056
    :cond_10f
    return v0
.end method

.method public equals(Landroid/content/res/Configuration;)Z
    .registers 5
    .parameter "that"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1275
    if-nez p1, :cond_6

    #@4
    move v0, v1

    #@5
    .line 1277
    :cond_5
    :goto_5
    return v0

    #@6
    .line 1276
    :cond_6
    if-eq p1, p0, :cond_5

    #@8
    .line 1277
    invoke-virtual {p0, p1}, Landroid/content/res/Configuration;->compareTo(Landroid/content/res/Configuration;)I

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_5

    #@e
    move v0, v1

    #@f
    goto :goto_5
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "that"

    #@0
    .prologue
    .line 1282
    :try_start_0
    check-cast p1, Landroid/content/res/Configuration;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/content/res/Configuration;->equals(Landroid/content/res/Configuration;)Z
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v0

    #@6
    .line 1285
    :goto_6
    return v0

    #@7
    .line 1283
    :catch_7
    move-exception v0

    #@8
    .line 1285
    const/4 v0, 0x0

    #@9
    goto :goto_6
.end method

.method public getLayoutDirection()I
    .registers 2

    #@0
    .prologue
    .line 1340
    iget v0, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@2
    and-int/lit16 v0, v0, 0xc0

    #@4
    shr-int/lit8 v0, v0, 0x6

    #@6
    add-int/lit8 v0, v0, -0x1

    #@8
    return v0
.end method

.method public hashCode()I
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1289
    const/16 v0, 0x11

    #@3
    .line 1290
    .local v0, result:I
    iget v1, p0, Landroid/content/res/Configuration;->fontScale:F

    #@5
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    #@8
    move-result v1

    #@9
    add-int/lit16 v0, v1, 0x20f

    #@b
    .line 1291
    mul-int/lit8 v1, v0, 0x1f

    #@d
    iget v3, p0, Landroid/content/res/Configuration;->mcc:I

    #@f
    add-int v0, v1, v3

    #@11
    .line 1292
    mul-int/lit8 v1, v0, 0x1f

    #@13
    iget v3, p0, Landroid/content/res/Configuration;->mnc:I

    #@15
    add-int v0, v1, v3

    #@17
    .line 1293
    mul-int/lit8 v3, v0, 0x1f

    #@19
    iget-object v1, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@1b
    if-eqz v1, :cond_8c

    #@1d
    iget-object v1, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@1f
    invoke-virtual {v1}, Ljava/util/Locale;->hashCode()I

    #@22
    move-result v1

    #@23
    :goto_23
    add-int v0, v3, v1

    #@25
    .line 1294
    mul-int/lit8 v1, v0, 0x1f

    #@27
    iget v3, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@29
    add-int v0, v1, v3

    #@2b
    .line 1295
    mul-int/lit8 v1, v0, 0x1f

    #@2d
    iget v3, p0, Landroid/content/res/Configuration;->keyboard:I

    #@2f
    add-int v0, v1, v3

    #@31
    .line 1296
    mul-int/lit8 v1, v0, 0x1f

    #@33
    iget v3, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@35
    add-int v0, v1, v3

    #@37
    .line 1297
    mul-int/lit8 v1, v0, 0x1f

    #@39
    iget v3, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@3b
    add-int v0, v1, v3

    #@3d
    .line 1298
    mul-int/lit8 v1, v0, 0x1f

    #@3f
    iget v3, p0, Landroid/content/res/Configuration;->navigation:I

    #@41
    add-int v0, v1, v3

    #@43
    .line 1299
    mul-int/lit8 v1, v0, 0x1f

    #@45
    iget v3, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@47
    add-int v0, v1, v3

    #@49
    .line 1300
    mul-int/lit8 v1, v0, 0x1f

    #@4b
    iget v3, p0, Landroid/content/res/Configuration;->orientation:I

    #@4d
    add-int v0, v1, v3

    #@4f
    .line 1301
    mul-int/lit8 v1, v0, 0x1f

    #@51
    iget v3, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@53
    add-int v0, v1, v3

    #@55
    .line 1302
    mul-int/lit8 v1, v0, 0x1f

    #@57
    iget v3, p0, Landroid/content/res/Configuration;->uiMode:I

    #@59
    add-int v0, v1, v3

    #@5b
    .line 1303
    mul-int/lit8 v1, v0, 0x1f

    #@5d
    iget v3, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@5f
    add-int v0, v1, v3

    #@61
    .line 1304
    mul-int/lit8 v1, v0, 0x1f

    #@63
    iget v3, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@65
    add-int v0, v1, v3

    #@67
    .line 1305
    mul-int/lit8 v1, v0, 0x1f

    #@69
    iget v3, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@6b
    add-int v0, v1, v3

    #@6d
    .line 1306
    mul-int/lit8 v1, v0, 0x1f

    #@6f
    iget v3, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@71
    add-int v0, v1, v3

    #@73
    .line 1307
    mul-int/lit8 v1, v0, 0x1f

    #@75
    iget-object v3, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@77
    if-eqz v3, :cond_7f

    #@79
    iget-object v2, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@7b
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    #@7e
    move-result v2

    #@7f
    :cond_7f
    add-int v0, v1, v2

    #@81
    .line 1310
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@83
    if-eqz v1, :cond_8b

    #@85
    .line 1311
    mul-int/lit8 v1, v0, 0x1f

    #@87
    iget v2, p0, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@89
    add-int v0, v1, v2

    #@8b
    .line 1315
    :cond_8b
    return v0

    #@8c
    :cond_8c
    move v1, v2

    #@8d
    .line 1293
    goto :goto_23
.end method

.method public isLayoutSizeAtLeast(I)Z
    .registers 5
    .parameter "size"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 263
    iget v2, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@3
    and-int/lit8 v0, v2, 0xf

    #@5
    .line 264
    .local v0, cur:I
    if-nez v0, :cond_8

    #@7
    .line 265
    :cond_7
    :goto_7
    return v1

    #@8
    :cond_8
    if-lt v0, p1, :cond_7

    #@a
    const/4 v1, 0x1

    #@b
    goto :goto_7
.end method

.method public isOtherSeqNewer(Landroid/content/res/Configuration;)Z
    .registers 7
    .parameter "other"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1080
    if-nez p1, :cond_6

    #@4
    move v1, v2

    #@5
    .line 1100
    :cond_5
    :goto_5
    return v1

    #@6
    .line 1084
    :cond_6
    iget v3, p1, Landroid/content/res/Configuration;->seq:I

    #@8
    if-eqz v3, :cond_5

    #@a
    .line 1089
    iget v3, p0, Landroid/content/res/Configuration;->seq:I

    #@c
    if-eqz v3, :cond_5

    #@e
    .line 1094
    iget v3, p1, Landroid/content/res/Configuration;->seq:I

    #@10
    iget v4, p0, Landroid/content/res/Configuration;->seq:I

    #@12
    sub-int v0, v3, v4

    #@14
    .line 1095
    .local v0, diff:I
    const/high16 v3, 0x1

    #@16
    if-le v0, v3, :cond_1a

    #@18
    move v1, v2

    #@19
    .line 1098
    goto :goto_5

    #@1a
    .line 1100
    :cond_1a
    if-gtz v0, :cond_5

    #@1c
    move v1, v2

    #@1d
    goto :goto_5
.end method

.method public makeDefault()V
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 789
    invoke-virtual {p0}, Landroid/content/res/Configuration;->setToDefaults()V

    #@3
    .line 790
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 7
    .parameter "source"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1154
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@4
    move-result v1

    #@5
    iput v1, p0, Landroid/content/res/Configuration;->fontScale:F

    #@7
    .line 1155
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v1

    #@b
    iput v1, p0, Landroid/content/res/Configuration;->mcc:I

    #@d
    .line 1156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@10
    move-result v1

    #@11
    iput v1, p0, Landroid/content/res/Configuration;->mnc:I

    #@13
    .line 1157
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_2c

    #@19
    .line 1158
    new-instance v1, Ljava/util/Locale;

    #@1b
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-direct {v1, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    iput-object v1, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@2c
    .line 1161
    :cond_2c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v1

    #@30
    if-ne v1, v0, :cond_ab

    #@32
    :goto_32
    iput-boolean v0, p0, Landroid/content/res/Configuration;->userSetLocale:Z

    #@34
    .line 1162
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v0

    #@38
    iput v0, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@3a
    .line 1163
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v0

    #@3e
    iput v0, p0, Landroid/content/res/Configuration;->keyboard:I

    #@40
    .line 1164
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v0

    #@44
    iput v0, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@46
    .line 1165
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@49
    move-result v0

    #@4a
    iput v0, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@4c
    .line 1166
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v0

    #@50
    iput v0, p0, Landroid/content/res/Configuration;->navigation:I

    #@52
    .line 1167
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v0

    #@56
    iput v0, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@58
    .line 1168
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5b
    move-result v0

    #@5c
    iput v0, p0, Landroid/content/res/Configuration;->orientation:I

    #@5e
    .line 1169
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@61
    move-result v0

    #@62
    iput v0, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@64
    .line 1170
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@67
    move-result v0

    #@68
    iput v0, p0, Landroid/content/res/Configuration;->uiMode:I

    #@6a
    .line 1171
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6d
    move-result v0

    #@6e
    iput v0, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@70
    .line 1172
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v0

    #@74
    iput v0, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@76
    .line 1173
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@79
    move-result v0

    #@7a
    iput v0, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@7c
    .line 1174
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v0

    #@80
    iput v0, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@82
    .line 1175
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@85
    move-result v0

    #@86
    iput v0, p0, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    #@88
    .line 1176
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8b
    move-result v0

    #@8c
    iput v0, p0, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    #@8e
    .line 1177
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@91
    move-result v0

    #@92
    iput v0, p0, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    #@94
    .line 1178
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@97
    move-result v0

    #@98
    iput v0, p0, Landroid/content/res/Configuration;->seq:I

    #@9a
    .line 1179
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9d
    move-result-object v0

    #@9e
    iput-object v0, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@a0
    .line 1182
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@a2
    if-eqz v0, :cond_aa

    #@a4
    .line 1183
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a7
    move-result v0

    #@a8
    iput v0, p0, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@aa
    .line 1186
    :cond_aa
    return-void

    #@ab
    .line 1161
    :cond_ab
    const/4 v0, 0x0

    #@ac
    goto :goto_32
.end method

.method public setLayoutDirection(Ljava/util/Locale;)V
    .registers 5
    .parameter "locale"

    #@0
    .prologue
    .line 1355
    invoke-static {p1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    #@3
    move-result v1

    #@4
    add-int/lit8 v0, v1, 0x1

    #@6
    .line 1356
    .local v0, layoutDirection:I
    iget v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@8
    and-int/lit16 v1, v1, -0xc1

    #@a
    shl-int/lit8 v2, v0, 0x6

    #@c
    or-int/2addr v1, v2

    #@d
    iput v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@f
    .line 1358
    return-void
.end method

.method public setLocale(Ljava/util/Locale;)V
    .registers 3
    .parameter "loc"

    #@0
    .prologue
    .line 1326
    iput-object p1, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@2
    .line 1327
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/content/res/Configuration;->userSetLocale:Z

    #@5
    .line 1328
    iget-object v0, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@7
    invoke-virtual {p0, v0}, Landroid/content/res/Configuration;->setLayoutDirection(Ljava/util/Locale;)V

    #@a
    .line 1329
    return-void
.end method

.method public setTo(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "o"

    #@0
    .prologue
    .line 570
    if-nez p1, :cond_6

    #@2
    .line 571
    invoke-virtual {p0}, Landroid/content/res/Configuration;->setToDefaults()V

    #@5
    .line 609
    :cond_5
    :goto_5
    return-void

    #@6
    .line 575
    :cond_6
    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    #@8
    iput v0, p0, Landroid/content/res/Configuration;->fontScale:F

    #@a
    .line 576
    iget v0, p1, Landroid/content/res/Configuration;->mcc:I

    #@c
    iput v0, p0, Landroid/content/res/Configuration;->mcc:I

    #@e
    .line 577
    iget v0, p1, Landroid/content/res/Configuration;->mnc:I

    #@10
    iput v0, p0, Landroid/content/res/Configuration;->mnc:I

    #@12
    .line 578
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@14
    if-eqz v0, :cond_20

    #@16
    .line 579
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@18
    invoke-virtual {v0}, Ljava/util/Locale;->clone()Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Ljava/util/Locale;

    #@1e
    iput-object v0, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@20
    .line 581
    :cond_20
    iget-boolean v0, p1, Landroid/content/res/Configuration;->userSetLocale:Z

    #@22
    iput-boolean v0, p0, Landroid/content/res/Configuration;->userSetLocale:Z

    #@24
    .line 582
    iget v0, p1, Landroid/content/res/Configuration;->touchscreen:I

    #@26
    iput v0, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@28
    .line 583
    iget v0, p1, Landroid/content/res/Configuration;->keyboard:I

    #@2a
    iput v0, p0, Landroid/content/res/Configuration;->keyboard:I

    #@2c
    .line 584
    iget v0, p1, Landroid/content/res/Configuration;->keyboardHidden:I

    #@2e
    iput v0, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@30
    .line 585
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@32
    iput v0, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@34
    .line 586
    iget v0, p1, Landroid/content/res/Configuration;->navigation:I

    #@36
    iput v0, p0, Landroid/content/res/Configuration;->navigation:I

    #@38
    .line 587
    iget v0, p1, Landroid/content/res/Configuration;->navigationHidden:I

    #@3a
    iput v0, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@3c
    .line 588
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    #@3e
    iput v0, p0, Landroid/content/res/Configuration;->orientation:I

    #@40
    .line 589
    iget v0, p1, Landroid/content/res/Configuration;->screenLayout:I

    #@42
    iput v0, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@44
    .line 590
    iget v0, p1, Landroid/content/res/Configuration;->uiMode:I

    #@46
    iput v0, p0, Landroid/content/res/Configuration;->uiMode:I

    #@48
    .line 591
    iget v0, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    #@4a
    iput v0, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@4c
    .line 592
    iget v0, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    #@4e
    iput v0, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@50
    .line 593
    iget v0, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@52
    iput v0, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@54
    .line 594
    iget v0, p1, Landroid/content/res/Configuration;->densityDpi:I

    #@56
    iput v0, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@58
    .line 595
    iget v0, p1, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    #@5a
    iput v0, p0, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    #@5c
    .line 596
    iget v0, p1, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    #@5e
    iput v0, p0, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    #@60
    .line 597
    iget v0, p1, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    #@62
    iput v0, p0, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    #@64
    .line 598
    iget v0, p1, Landroid/content/res/Configuration;->seq:I

    #@66
    iput v0, p0, Landroid/content/res/Configuration;->seq:I

    #@68
    .line 600
    iget-object v0, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@6a
    if-eqz v0, :cond_75

    #@6c
    .line 601
    new-instance v0, Ljava/lang/String;

    #@6e
    iget-object v1, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@70
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@73
    iput-object v0, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@75
    .line 605
    :cond_75
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@77
    if-eqz v0, :cond_5

    #@79
    .line 606
    iget v0, p1, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@7b
    iput v0, p0, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@7d
    goto :goto_5
.end method

.method public setToDefaults()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 760
    const/high16 v0, 0x3f80

    #@4
    iput v0, p0, Landroid/content/res/Configuration;->fontScale:F

    #@6
    .line 761
    iput v1, p0, Landroid/content/res/Configuration;->mnc:I

    #@8
    iput v1, p0, Landroid/content/res/Configuration;->mcc:I

    #@a
    .line 762
    iput-object v2, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@c
    .line 763
    iput-boolean v1, p0, Landroid/content/res/Configuration;->userSetLocale:Z

    #@e
    .line 764
    iput v1, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@10
    .line 765
    iput v1, p0, Landroid/content/res/Configuration;->keyboard:I

    #@12
    .line 766
    iput v1, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@14
    .line 767
    iput v1, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@16
    .line 768
    iput v1, p0, Landroid/content/res/Configuration;->navigation:I

    #@18
    .line 769
    iput v1, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@1a
    .line 770
    iput v1, p0, Landroid/content/res/Configuration;->orientation:I

    #@1c
    .line 771
    iput v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@1e
    .line 772
    iput v1, p0, Landroid/content/res/Configuration;->uiMode:I

    #@20
    .line 773
    iput v1, p0, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    #@22
    iput v1, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@24
    .line 774
    iput v1, p0, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    #@26
    iput v1, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@28
    .line 775
    iput v1, p0, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    #@2a
    iput v1, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@2c
    .line 776
    iput v1, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@2e
    .line 777
    iput v1, p0, Landroid/content/res/Configuration;->seq:I

    #@30
    .line 778
    iput-object v2, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@32
    .line 781
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@34
    if-eqz v0, :cond_39

    #@36
    .line 782
    const/4 v0, -0x1

    #@37
    iput v0, p0, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@39
    .line 785
    :cond_39
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 612
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    const/16 v2, 0x80

    #@4
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 613
    .local v1, sb:Ljava/lang/StringBuilder;
    const-string/jumbo v2, "{"

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 614
    iget v2, p0, Landroid/content/res/Configuration;->fontScale:F

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@12
    .line 615
    const-string v2, " "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 616
    iget v2, p0, Landroid/content/res/Configuration;->mcc:I

    #@19
    if-eqz v2, :cond_17b

    #@1b
    .line 617
    iget v2, p0, Landroid/content/res/Configuration;->mcc:I

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    .line 618
    const-string/jumbo v2, "mcc"

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 622
    :goto_26
    iget v2, p0, Landroid/content/res/Configuration;->mnc:I

    #@28
    if-eqz v2, :cond_182

    #@2a
    .line 623
    iget v2, p0, Landroid/content/res/Configuration;->mnc:I

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    .line 624
    const-string/jumbo v2, "mnc"

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    .line 628
    :goto_35
    iget-object v2, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@37
    if-eqz v2, :cond_189

    #@39
    .line 629
    const-string v2, " "

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    .line 630
    iget-object v2, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    .line 634
    :goto_43
    iget v2, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@45
    and-int/lit16 v0, v2, 0xc0

    #@47
    .line 635
    .local v0, layoutDir:I
    sparse-switch v0, :sswitch_data_2da

    #@4a
    .line 639
    const-string v2, " layoutDir="

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    .line 640
    shr-int/lit8 v2, v0, 0x6

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    .line 642
    :goto_54
    iget v2, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@56
    if-eqz v2, :cond_1a5

    #@58
    .line 643
    const-string v2, " sw"

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    iget v2, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    const-string v2, "dp"

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    .line 647
    :goto_67
    iget v2, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@69
    if-eqz v2, :cond_1ac

    #@6b
    .line 648
    const-string v2, " w"

    #@6d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    iget v2, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    const-string v2, "dp"

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    .line 652
    :goto_7a
    iget v2, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@7c
    if-eqz v2, :cond_1b3

    #@7e
    .line 653
    const-string v2, " h"

    #@80
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    iget v2, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@85
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    const-string v2, "dp"

    #@8a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    .line 657
    :goto_8d
    iget v2, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@8f
    if-eqz v2, :cond_1ba

    #@91
    .line 658
    const-string v2, " "

    #@93
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    iget v2, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@98
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9b
    const-string v2, "dpi"

    #@9d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    .line 662
    :goto_a0
    iget v2, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@a2
    and-int/lit8 v2, v2, 0xf

    #@a4
    packed-switch v2, :pswitch_data_2e8

    #@a7
    .line 668
    const-string v2, " layoutSize="

    #@a9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    .line 669
    iget v2, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@ae
    and-int/lit8 v2, v2, 0xf

    #@b0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b3
    .line 671
    :goto_b3
    iget v2, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@b5
    and-int/lit8 v2, v2, 0x30

    #@b7
    sparse-switch v2, :sswitch_data_2f6

    #@ba
    .line 675
    const-string v2, " layoutLong="

    #@bc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    .line 676
    iget v2, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@c1
    and-int/lit8 v2, v2, 0x30

    #@c3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    .line 678
    :goto_c6
    :sswitch_c6
    iget v2, p0, Landroid/content/res/Configuration;->orientation:I

    #@c8
    packed-switch v2, :pswitch_data_304

    #@cb
    .line 682
    const-string v2, " orien="

    #@cd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    iget v2, p0, Landroid/content/res/Configuration;->orientation:I

    #@d2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d5
    .line 684
    :goto_d5
    iget v2, p0, Landroid/content/res/Configuration;->uiMode:I

    #@d7
    and-int/lit8 v2, v2, 0xf

    #@d9
    packed-switch v2, :pswitch_data_30e

    #@dc
    .line 691
    const-string v2, " uimode="

    #@de
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    iget v2, p0, Landroid/content/res/Configuration;->uiMode:I

    #@e3
    and-int/lit8 v2, v2, 0xf

    #@e5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e8
    .line 693
    :goto_e8
    :pswitch_e8
    iget v2, p0, Landroid/content/res/Configuration;->uiMode:I

    #@ea
    and-int/lit8 v2, v2, 0x30

    #@ec
    sparse-switch v2, :sswitch_data_31e

    #@ef
    .line 697
    const-string v2, " night="

    #@f1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    iget v2, p0, Landroid/content/res/Configuration;->uiMode:I

    #@f6
    and-int/lit8 v2, v2, 0x30

    #@f8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fb
    .line 699
    :goto_fb
    :sswitch_fb
    iget v2, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@fd
    packed-switch v2, :pswitch_data_32c

    #@100
    .line 704
    const-string v2, " touch="

    #@102
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    iget v2, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@107
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10a
    .line 706
    :goto_10a
    iget v2, p0, Landroid/content/res/Configuration;->keyboard:I

    #@10c
    packed-switch v2, :pswitch_data_338

    #@10f
    .line 711
    const-string v2, " keys="

    #@111
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    iget v2, p0, Landroid/content/res/Configuration;->keyboard:I

    #@116
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@119
    .line 713
    :goto_119
    iget v2, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@11b
    packed-switch v2, :pswitch_data_344

    #@11e
    .line 718
    const-string v2, "/"

    #@120
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    iget v2, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@125
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@128
    .line 720
    :goto_128
    iget v2, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@12a
    packed-switch v2, :pswitch_data_350

    #@12d
    .line 724
    const-string v2, "/"

    #@12f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    iget v2, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@134
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@137
    .line 726
    :goto_137
    iget v2, p0, Landroid/content/res/Configuration;->navigation:I

    #@139
    packed-switch v2, :pswitch_data_35a

    #@13c
    .line 732
    const-string v2, " nav="

    #@13e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    iget v2, p0, Landroid/content/res/Configuration;->navigation:I

    #@143
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@146
    .line 734
    :goto_146
    iget v2, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@148
    packed-switch v2, :pswitch_data_368

    #@14b
    .line 738
    const-string v2, "/"

    #@14d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    iget v2, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@152
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@155
    .line 740
    :goto_155
    iget v2, p0, Landroid/content/res/Configuration;->seq:I

    #@157
    if-eqz v2, :cond_163

    #@159
    .line 741
    const-string v2, " s."

    #@15b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    .line 742
    iget v2, p0, Landroid/content/res/Configuration;->seq:I

    #@160
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@163
    .line 746
    :cond_163
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@165
    if-eqz v2, :cond_171

    #@167
    .line 747
    iget v2, p0, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@169
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16c
    .line 748
    const-string v2, "fontTypeIndex"

    #@16e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@171
    .line 752
    :cond_171
    const/16 v2, 0x7d

    #@173
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@176
    .line 753
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@179
    move-result-object v2

    #@17a
    return-object v2

    #@17b
    .line 620
    .end local v0           #layoutDir:I
    :cond_17b
    const-string v2, "?mcc"

    #@17d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    goto/16 :goto_26

    #@182
    .line 626
    :cond_182
    const-string v2, "?mnc"

    #@184
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    goto/16 :goto_35

    #@189
    .line 632
    :cond_189
    const-string v2, " ?locale"

    #@18b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    goto/16 :goto_43

    #@190
    .line 636
    .restart local v0       #layoutDir:I
    :sswitch_190
    const-string v2, " ?layoutDir"

    #@192
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@195
    goto/16 :goto_54

    #@197
    .line 637
    :sswitch_197
    const-string v2, " ldltr"

    #@199
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    goto/16 :goto_54

    #@19e
    .line 638
    :sswitch_19e
    const-string v2, " ldrtl"

    #@1a0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    goto/16 :goto_54

    #@1a5
    .line 645
    :cond_1a5
    const-string v2, " ?swdp"

    #@1a7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    goto/16 :goto_67

    #@1ac
    .line 650
    :cond_1ac
    const-string v2, " ?wdp"

    #@1ae
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b1
    goto/16 :goto_7a

    #@1b3
    .line 655
    :cond_1b3
    const-string v2, " ?hdp"

    #@1b5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    goto/16 :goto_8d

    #@1ba
    .line 660
    :cond_1ba
    const-string v2, " ?density"

    #@1bc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    goto/16 :goto_a0

    #@1c1
    .line 663
    :pswitch_1c1
    const-string v2, " ?lsize"

    #@1c3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c6
    goto/16 :goto_b3

    #@1c8
    .line 664
    :pswitch_1c8
    const-string v2, " smll"

    #@1ca
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cd
    goto/16 :goto_b3

    #@1cf
    .line 665
    :pswitch_1cf
    const-string v2, " nrml"

    #@1d1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    goto/16 :goto_b3

    #@1d6
    .line 666
    :pswitch_1d6
    const-string v2, " lrg"

    #@1d8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    goto/16 :goto_b3

    #@1dd
    .line 667
    :pswitch_1dd
    const-string v2, " xlrg"

    #@1df
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e2
    goto/16 :goto_b3

    #@1e4
    .line 672
    :sswitch_1e4
    const-string v2, " ?long"

    #@1e6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    goto/16 :goto_c6

    #@1eb
    .line 674
    :sswitch_1eb
    const-string v2, " long"

    #@1ed
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f0
    goto/16 :goto_c6

    #@1f2
    .line 679
    :pswitch_1f2
    const-string v2, " ?orien"

    #@1f4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f7
    goto/16 :goto_d5

    #@1f9
    .line 680
    :pswitch_1f9
    const-string v2, " land"

    #@1fb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fe
    goto/16 :goto_d5

    #@200
    .line 681
    :pswitch_200
    const-string v2, " port"

    #@202
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@205
    goto/16 :goto_d5

    #@207
    .line 685
    :pswitch_207
    const-string v2, " ?uimode"

    #@209
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20c
    goto/16 :goto_e8

    #@20e
    .line 687
    :pswitch_20e
    const-string v2, " desk"

    #@210
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@213
    goto/16 :goto_e8

    #@215
    .line 688
    :pswitch_215
    const-string v2, " car"

    #@217
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    goto/16 :goto_e8

    #@21c
    .line 689
    :pswitch_21c
    const-string v2, " television"

    #@21e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@221
    goto/16 :goto_e8

    #@223
    .line 690
    :pswitch_223
    const-string v2, " appliance"

    #@225
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@228
    goto/16 :goto_e8

    #@22a
    .line 694
    :sswitch_22a
    const-string v2, " ?night"

    #@22c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22f
    goto/16 :goto_fb

    #@231
    .line 696
    :sswitch_231
    const-string v2, " night"

    #@233
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@236
    goto/16 :goto_fb

    #@238
    .line 700
    :pswitch_238
    const-string v2, " ?touch"

    #@23a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23d
    goto/16 :goto_10a

    #@23f
    .line 701
    :pswitch_23f
    const-string v2, " -touch"

    #@241
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@244
    goto/16 :goto_10a

    #@246
    .line 702
    :pswitch_246
    const-string v2, " stylus"

    #@248
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24b
    goto/16 :goto_10a

    #@24d
    .line 703
    :pswitch_24d
    const-string v2, " finger"

    #@24f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@252
    goto/16 :goto_10a

    #@254
    .line 707
    :pswitch_254
    const-string v2, " ?keyb"

    #@256
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@259
    goto/16 :goto_119

    #@25b
    .line 708
    :pswitch_25b
    const-string v2, " -keyb"

    #@25d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@260
    goto/16 :goto_119

    #@262
    .line 709
    :pswitch_262
    const-string v2, " qwerty"

    #@264
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@267
    goto/16 :goto_119

    #@269
    .line 710
    :pswitch_269
    const-string v2, " 12key"

    #@26b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26e
    goto/16 :goto_119

    #@270
    .line 714
    :pswitch_270
    const-string v2, "/?"

    #@272
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@275
    goto/16 :goto_128

    #@277
    .line 715
    :pswitch_277
    const-string v2, "/v"

    #@279
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27c
    goto/16 :goto_128

    #@27e
    .line 716
    :pswitch_27e
    const-string v2, "/h"

    #@280
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@283
    goto/16 :goto_128

    #@285
    .line 717
    :pswitch_285
    const-string v2, "/s"

    #@287
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28a
    goto/16 :goto_128

    #@28c
    .line 721
    :pswitch_28c
    const-string v2, "/?"

    #@28e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@291
    goto/16 :goto_137

    #@293
    .line 722
    :pswitch_293
    const-string v2, "/v"

    #@295
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@298
    goto/16 :goto_137

    #@29a
    .line 723
    :pswitch_29a
    const-string v2, "/h"

    #@29c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29f
    goto/16 :goto_137

    #@2a1
    .line 727
    :pswitch_2a1
    const-string v2, " ?nav"

    #@2a3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a6
    goto/16 :goto_146

    #@2a8
    .line 728
    :pswitch_2a8
    const-string v2, " -nav"

    #@2aa
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ad
    goto/16 :goto_146

    #@2af
    .line 729
    :pswitch_2af
    const-string v2, " dpad"

    #@2b1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b4
    goto/16 :goto_146

    #@2b6
    .line 730
    :pswitch_2b6
    const-string v2, " tball"

    #@2b8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bb
    goto/16 :goto_146

    #@2bd
    .line 731
    :pswitch_2bd
    const-string v2, " wheel"

    #@2bf
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c2
    goto/16 :goto_146

    #@2c4
    .line 735
    :pswitch_2c4
    const-string v2, "/?"

    #@2c6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c9
    goto/16 :goto_155

    #@2cb
    .line 736
    :pswitch_2cb
    const-string v2, "/v"

    #@2cd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d0
    goto/16 :goto_155

    #@2d2
    .line 737
    :pswitch_2d2
    const-string v2, "/h"

    #@2d4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d7
    goto/16 :goto_155

    #@2d9
    .line 635
    nop

    #@2da
    :sswitch_data_2da
    .sparse-switch
        0x0 -> :sswitch_190
        0x40 -> :sswitch_197
        0x80 -> :sswitch_19e
    .end sparse-switch

    #@2e8
    .line 662
    :pswitch_data_2e8
    .packed-switch 0x0
        :pswitch_1c1
        :pswitch_1c8
        :pswitch_1cf
        :pswitch_1d6
        :pswitch_1dd
    .end packed-switch

    #@2f6
    .line 671
    :sswitch_data_2f6
    .sparse-switch
        0x0 -> :sswitch_1e4
        0x10 -> :sswitch_c6
        0x20 -> :sswitch_1eb
    .end sparse-switch

    #@304
    .line 678
    :pswitch_data_304
    .packed-switch 0x0
        :pswitch_1f2
        :pswitch_200
        :pswitch_1f9
    .end packed-switch

    #@30e
    .line 684
    :pswitch_data_30e
    .packed-switch 0x0
        :pswitch_207
        :pswitch_e8
        :pswitch_20e
        :pswitch_215
        :pswitch_21c
        :pswitch_223
    .end packed-switch

    #@31e
    .line 693
    :sswitch_data_31e
    .sparse-switch
        0x0 -> :sswitch_22a
        0x10 -> :sswitch_fb
        0x20 -> :sswitch_231
    .end sparse-switch

    #@32c
    .line 699
    :pswitch_data_32c
    .packed-switch 0x0
        :pswitch_238
        :pswitch_23f
        :pswitch_246
        :pswitch_24d
    .end packed-switch

    #@338
    .line 706
    :pswitch_data_338
    .packed-switch 0x0
        :pswitch_254
        :pswitch_25b
        :pswitch_262
        :pswitch_269
    .end packed-switch

    #@344
    .line 713
    :pswitch_data_344
    .packed-switch 0x0
        :pswitch_270
        :pswitch_277
        :pswitch_27e
        :pswitch_285
    .end packed-switch

    #@350
    .line 720
    :pswitch_data_350
    .packed-switch 0x0
        :pswitch_28c
        :pswitch_293
        :pswitch_29a
    .end packed-switch

    #@35a
    .line 726
    :pswitch_data_35a
    .packed-switch 0x0
        :pswitch_2a1
        :pswitch_2a8
        :pswitch_2af
        :pswitch_2b6
        :pswitch_2bd
    .end packed-switch

    #@368
    .line 734
    :pswitch_data_368
    .packed-switch 0x0
        :pswitch_2c4
        :pswitch_2cb
        :pswitch_2d2
    .end packed-switch
.end method

.method public updateFrom(Landroid/content/res/Configuration;)I
    .registers 5
    .parameter "delta"

    #@0
    .prologue
    .line 801
    const/4 v0, 0x0

    #@1
    .line 802
    .local v0, changed:I
    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    #@3
    const/4 v2, 0x0

    #@4
    cmpl-float v1, v1, v2

    #@6
    if-lez v1, :cond_17

    #@8
    iget v1, p0, Landroid/content/res/Configuration;->fontScale:F

    #@a
    iget v2, p1, Landroid/content/res/Configuration;->fontScale:F

    #@c
    cmpl-float v1, v1, v2

    #@e
    if-eqz v1, :cond_17

    #@10
    .line 803
    const/high16 v1, 0x4000

    #@12
    or-int/2addr v0, v1

    #@13
    .line 804
    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    #@15
    iput v1, p0, Landroid/content/res/Configuration;->fontScale:F

    #@17
    .line 806
    :cond_17
    iget v1, p1, Landroid/content/res/Configuration;->mcc:I

    #@19
    if-eqz v1, :cond_27

    #@1b
    iget v1, p0, Landroid/content/res/Configuration;->mcc:I

    #@1d
    iget v2, p1, Landroid/content/res/Configuration;->mcc:I

    #@1f
    if-eq v1, v2, :cond_27

    #@21
    .line 807
    or-int/lit8 v0, v0, 0x1

    #@23
    .line 808
    iget v1, p1, Landroid/content/res/Configuration;->mcc:I

    #@25
    iput v1, p0, Landroid/content/res/Configuration;->mcc:I

    #@27
    .line 810
    :cond_27
    iget v1, p1, Landroid/content/res/Configuration;->mnc:I

    #@29
    if-eqz v1, :cond_37

    #@2b
    iget v1, p0, Landroid/content/res/Configuration;->mnc:I

    #@2d
    iget v2, p1, Landroid/content/res/Configuration;->mnc:I

    #@2f
    if-eq v1, v2, :cond_37

    #@31
    .line 811
    or-int/lit8 v0, v0, 0x2

    #@33
    .line 812
    iget v1, p1, Landroid/content/res/Configuration;->mnc:I

    #@35
    iput v1, p0, Landroid/content/res/Configuration;->mnc:I

    #@37
    .line 814
    :cond_37
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@39
    if-eqz v1, :cond_60

    #@3b
    iget-object v1, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@3d
    if-eqz v1, :cond_49

    #@3f
    iget-object v1, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@41
    iget-object v2, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@43
    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v1

    #@47
    if-nez v1, :cond_60

    #@49
    .line 816
    :cond_49
    or-int/lit8 v0, v0, 0x4

    #@4b
    .line 817
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@4d
    if-eqz v1, :cond_1ca

    #@4f
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@51
    invoke-virtual {v1}, Ljava/util/Locale;->clone()Ljava/lang/Object;

    #@54
    move-result-object v1

    #@55
    check-cast v1, Ljava/util/Locale;

    #@57
    :goto_57
    iput-object v1, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@59
    .line 820
    or-int/lit16 v0, v0, 0x2000

    #@5b
    .line 823
    iget-object v1, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@5d
    invoke-virtual {p0, v1}, Landroid/content/res/Configuration;->setLayoutDirection(Ljava/util/Locale;)V

    #@60
    .line 825
    :cond_60
    iget-boolean v1, p1, Landroid/content/res/Configuration;->userSetLocale:Z

    #@62
    if-eqz v1, :cond_71

    #@64
    iget-boolean v1, p0, Landroid/content/res/Configuration;->userSetLocale:Z

    #@66
    if-eqz v1, :cond_6c

    #@68
    and-int/lit8 v1, v0, 0x4

    #@6a
    if-eqz v1, :cond_71

    #@6c
    .line 827
    :cond_6c
    const/4 v1, 0x1

    #@6d
    iput-boolean v1, p0, Landroid/content/res/Configuration;->userSetLocale:Z

    #@6f
    .line 828
    or-int/lit8 v0, v0, 0x4

    #@71
    .line 830
    :cond_71
    iget v1, p1, Landroid/content/res/Configuration;->touchscreen:I

    #@73
    if-eqz v1, :cond_81

    #@75
    iget v1, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@77
    iget v2, p1, Landroid/content/res/Configuration;->touchscreen:I

    #@79
    if-eq v1, v2, :cond_81

    #@7b
    .line 832
    or-int/lit8 v0, v0, 0x8

    #@7d
    .line 833
    iget v1, p1, Landroid/content/res/Configuration;->touchscreen:I

    #@7f
    iput v1, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@81
    .line 835
    :cond_81
    iget v1, p1, Landroid/content/res/Configuration;->keyboard:I

    #@83
    if-eqz v1, :cond_91

    #@85
    iget v1, p0, Landroid/content/res/Configuration;->keyboard:I

    #@87
    iget v2, p1, Landroid/content/res/Configuration;->keyboard:I

    #@89
    if-eq v1, v2, :cond_91

    #@8b
    .line 837
    or-int/lit8 v0, v0, 0x10

    #@8d
    .line 838
    iget v1, p1, Landroid/content/res/Configuration;->keyboard:I

    #@8f
    iput v1, p0, Landroid/content/res/Configuration;->keyboard:I

    #@91
    .line 840
    :cond_91
    iget v1, p1, Landroid/content/res/Configuration;->keyboardHidden:I

    #@93
    if-eqz v1, :cond_a1

    #@95
    iget v1, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@97
    iget v2, p1, Landroid/content/res/Configuration;->keyboardHidden:I

    #@99
    if-eq v1, v2, :cond_a1

    #@9b
    .line 842
    or-int/lit8 v0, v0, 0x20

    #@9d
    .line 843
    iget v1, p1, Landroid/content/res/Configuration;->keyboardHidden:I

    #@9f
    iput v1, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@a1
    .line 845
    :cond_a1
    iget v1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@a3
    if-eqz v1, :cond_b1

    #@a5
    iget v1, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@a7
    iget v2, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@a9
    if-eq v1, v2, :cond_b1

    #@ab
    .line 847
    or-int/lit8 v0, v0, 0x20

    #@ad
    .line 848
    iget v1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@af
    iput v1, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@b1
    .line 850
    :cond_b1
    iget v1, p1, Landroid/content/res/Configuration;->navigation:I

    #@b3
    if-eqz v1, :cond_c1

    #@b5
    iget v1, p0, Landroid/content/res/Configuration;->navigation:I

    #@b7
    iget v2, p1, Landroid/content/res/Configuration;->navigation:I

    #@b9
    if-eq v1, v2, :cond_c1

    #@bb
    .line 852
    or-int/lit8 v0, v0, 0x40

    #@bd
    .line 853
    iget v1, p1, Landroid/content/res/Configuration;->navigation:I

    #@bf
    iput v1, p0, Landroid/content/res/Configuration;->navigation:I

    #@c1
    .line 855
    :cond_c1
    iget v1, p1, Landroid/content/res/Configuration;->navigationHidden:I

    #@c3
    if-eqz v1, :cond_d1

    #@c5
    iget v1, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@c7
    iget v2, p1, Landroid/content/res/Configuration;->navigationHidden:I

    #@c9
    if-eq v1, v2, :cond_d1

    #@cb
    .line 857
    or-int/lit8 v0, v0, 0x20

    #@cd
    .line 858
    iget v1, p1, Landroid/content/res/Configuration;->navigationHidden:I

    #@cf
    iput v1, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@d1
    .line 860
    :cond_d1
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    #@d3
    if-eqz v1, :cond_e1

    #@d5
    iget v1, p0, Landroid/content/res/Configuration;->orientation:I

    #@d7
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    #@d9
    if-eq v1, v2, :cond_e1

    #@db
    .line 862
    or-int/lit16 v0, v0, 0x80

    #@dd
    .line 863
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    #@df
    iput v1, p0, Landroid/content/res/Configuration;->orientation:I

    #@e1
    .line 865
    :cond_e1
    iget v1, p1, Landroid/content/res/Configuration;->screenLayout:I

    #@e3
    invoke-static {v1}, Landroid/content/res/Configuration;->getScreenLayoutNoDirection(I)I

    #@e6
    move-result v1

    #@e7
    if-eqz v1, :cond_108

    #@e9
    iget v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@eb
    invoke-static {v1}, Landroid/content/res/Configuration;->getScreenLayoutNoDirection(I)I

    #@ee
    move-result v1

    #@ef
    iget v2, p1, Landroid/content/res/Configuration;->screenLayout:I

    #@f1
    invoke-static {v2}, Landroid/content/res/Configuration;->getScreenLayoutNoDirection(I)I

    #@f4
    move-result v2

    #@f5
    if-eq v1, v2, :cond_108

    #@f7
    .line 869
    or-int/lit16 v0, v0, 0x100

    #@f9
    .line 871
    iget v1, p1, Landroid/content/res/Configuration;->screenLayout:I

    #@fb
    and-int/lit16 v1, v1, 0xc0

    #@fd
    if-nez v1, :cond_1cd

    #@ff
    .line 872
    iget v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@101
    and-int/lit16 v1, v1, 0xc0

    #@103
    iget v2, p1, Landroid/content/res/Configuration;->screenLayout:I

    #@105
    or-int/2addr v1, v2

    #@106
    iput v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@108
    .line 877
    :cond_108
    :goto_108
    iget v1, p1, Landroid/content/res/Configuration;->uiMode:I

    #@10a
    if-eqz v1, :cond_136

    #@10c
    iget v1, p0, Landroid/content/res/Configuration;->uiMode:I

    #@10e
    iget v2, p1, Landroid/content/res/Configuration;->uiMode:I

    #@110
    if-eq v1, v2, :cond_136

    #@112
    .line 879
    or-int/lit16 v0, v0, 0x200

    #@114
    .line 880
    iget v1, p1, Landroid/content/res/Configuration;->uiMode:I

    #@116
    and-int/lit8 v1, v1, 0xf

    #@118
    if-eqz v1, :cond_125

    #@11a
    .line 881
    iget v1, p0, Landroid/content/res/Configuration;->uiMode:I

    #@11c
    and-int/lit8 v1, v1, -0x10

    #@11e
    iget v2, p1, Landroid/content/res/Configuration;->uiMode:I

    #@120
    and-int/lit8 v2, v2, 0xf

    #@122
    or-int/2addr v1, v2

    #@123
    iput v1, p0, Landroid/content/res/Configuration;->uiMode:I

    #@125
    .line 884
    :cond_125
    iget v1, p1, Landroid/content/res/Configuration;->uiMode:I

    #@127
    and-int/lit8 v1, v1, 0x30

    #@129
    if-eqz v1, :cond_136

    #@12b
    .line 885
    iget v1, p0, Landroid/content/res/Configuration;->uiMode:I

    #@12d
    and-int/lit8 v1, v1, -0x31

    #@12f
    iget v2, p1, Landroid/content/res/Configuration;->uiMode:I

    #@131
    and-int/lit8 v2, v2, 0x30

    #@133
    or-int/2addr v1, v2

    #@134
    iput v1, p0, Landroid/content/res/Configuration;->uiMode:I

    #@136
    .line 889
    :cond_136
    iget v1, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    #@138
    if-eqz v1, :cond_146

    #@13a
    iget v1, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@13c
    iget v2, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    #@13e
    if-eq v1, v2, :cond_146

    #@140
    .line 891
    or-int/lit16 v0, v0, 0x400

    #@142
    .line 892
    iget v1, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    #@144
    iput v1, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@146
    .line 894
    :cond_146
    iget v1, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    #@148
    if-eqz v1, :cond_156

    #@14a
    iget v1, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@14c
    iget v2, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    #@14e
    if-eq v1, v2, :cond_156

    #@150
    .line 896
    or-int/lit16 v0, v0, 0x400

    #@152
    .line 897
    iget v1, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    #@154
    iput v1, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@156
    .line 899
    :cond_156
    iget v1, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@158
    if-eqz v1, :cond_166

    #@15a
    iget v1, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@15c
    iget v2, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@15e
    if-eq v1, v2, :cond_166

    #@160
    .line 901
    or-int/lit16 v0, v0, 0x800

    #@162
    .line 902
    iget v1, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@164
    iput v1, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@166
    .line 904
    :cond_166
    iget v1, p1, Landroid/content/res/Configuration;->densityDpi:I

    #@168
    if-eqz v1, :cond_176

    #@16a
    iget v1, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@16c
    iget v2, p1, Landroid/content/res/Configuration;->densityDpi:I

    #@16e
    if-eq v1, v2, :cond_176

    #@170
    .line 906
    or-int/lit16 v0, v0, 0x1000

    #@172
    .line 907
    iget v1, p1, Landroid/content/res/Configuration;->densityDpi:I

    #@174
    iput v1, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@176
    .line 909
    :cond_176
    iget v1, p1, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    #@178
    if-eqz v1, :cond_17e

    #@17a
    .line 910
    iget v1, p1, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    #@17c
    iput v1, p0, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    #@17e
    .line 912
    :cond_17e
    iget v1, p1, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    #@180
    if-eqz v1, :cond_186

    #@182
    .line 913
    iget v1, p1, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    #@184
    iput v1, p0, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    #@186
    .line 915
    :cond_186
    iget v1, p1, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    #@188
    if-eqz v1, :cond_18e

    #@18a
    .line 916
    iget v1, p1, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    #@18c
    iput v1, p0, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    #@18e
    .line 918
    :cond_18e
    iget v1, p1, Landroid/content/res/Configuration;->seq:I

    #@190
    if-eqz v1, :cond_196

    #@192
    .line 919
    iget v1, p1, Landroid/content/res/Configuration;->seq:I

    #@194
    iput v1, p0, Landroid/content/res/Configuration;->seq:I

    #@196
    .line 923
    :cond_196
    iget-object v1, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@198
    if-eqz v1, :cond_1b4

    #@19a
    iget-object v1, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@19c
    if-eqz v1, :cond_1a8

    #@19e
    iget-object v1, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@1a0
    iget-object v2, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@1a2
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a5
    move-result v1

    #@1a6
    if-nez v1, :cond_1b4

    #@1a8
    .line 925
    :cond_1a8
    const/high16 v1, 0x1000

    #@1aa
    or-int/2addr v0, v1

    #@1ab
    .line 926
    new-instance v1, Ljava/lang/String;

    #@1ad
    iget-object v2, p1, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@1af
    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@1b2
    iput-object v1, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@1b4
    .line 930
    :cond_1b4
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@1b6
    if-eqz v1, :cond_1c9

    #@1b8
    .line 931
    iget v1, p1, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@1ba
    iget v2, p0, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@1bc
    if-eq v1, v2, :cond_1c9

    #@1be
    iget v1, p1, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@1c0
    if-ltz v1, :cond_1c9

    #@1c2
    .line 932
    const/high16 v1, 0x2000

    #@1c4
    or-int/2addr v0, v1

    #@1c5
    .line 933
    iget v1, p1, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@1c7
    iput v1, p0, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@1c9
    .line 938
    :cond_1c9
    return v0

    #@1ca
    .line 817
    :cond_1ca
    const/4 v1, 0x0

    #@1cb
    goto/16 :goto_57

    #@1cd
    .line 874
    :cond_1cd
    iget v1, p1, Landroid/content/res/Configuration;->screenLayout:I

    #@1cf
    iput v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@1d1
    goto/16 :goto_108
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1111
    iget v0, p0, Landroid/content/res/Configuration;->fontScale:F

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@7
    .line 1112
    iget v0, p0, Landroid/content/res/Configuration;->mcc:I

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 1113
    iget v0, p0, Landroid/content/res/Configuration;->mnc:I

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 1114
    iget-object v0, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@13
    if-nez v0, :cond_83

    #@15
    .line 1115
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 1122
    :goto_18
    iget-boolean v0, p0, Landroid/content/res/Configuration;->userSetLocale:Z

    #@1a
    if-eqz v0, :cond_a3

    #@1c
    .line 1123
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 1127
    :goto_1f
    iget v0, p0, Landroid/content/res/Configuration;->touchscreen:I

    #@21
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 1128
    iget v0, p0, Landroid/content/res/Configuration;->keyboard:I

    #@26
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 1129
    iget v0, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    #@2b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 1130
    iget v0, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@30
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 1131
    iget v0, p0, Landroid/content/res/Configuration;->navigation:I

    #@35
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 1132
    iget v0, p0, Landroid/content/res/Configuration;->navigationHidden:I

    #@3a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    .line 1133
    iget v0, p0, Landroid/content/res/Configuration;->orientation:I

    #@3f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@42
    .line 1134
    iget v0, p0, Landroid/content/res/Configuration;->screenLayout:I

    #@44
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@47
    .line 1135
    iget v0, p0, Landroid/content/res/Configuration;->uiMode:I

    #@49
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4c
    .line 1136
    iget v0, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    #@4e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@51
    .line 1137
    iget v0, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@53
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@56
    .line 1138
    iget v0, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@58
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5b
    .line 1139
    iget v0, p0, Landroid/content/res/Configuration;->densityDpi:I

    #@5d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@60
    .line 1140
    iget v0, p0, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    #@62
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@65
    .line 1141
    iget v0, p0, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    #@67
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6a
    .line 1142
    iget v0, p0, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    #@6c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6f
    .line 1143
    iget v0, p0, Landroid/content/res/Configuration;->seq:I

    #@71
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@74
    .line 1144
    iget-object v0, p0, Landroid/content/res/Configuration;->themePackage:Ljava/lang/String;

    #@76
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@79
    .line 1147
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@7b
    if-eqz v0, :cond_82

    #@7d
    .line 1148
    iget v0, p0, Landroid/content/res/Configuration;->fontTypeIndex:I

    #@7f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@82
    .line 1151
    :cond_82
    return-void

    #@83
    .line 1117
    :cond_83
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@86
    .line 1118
    iget-object v0, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@88
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@8b
    move-result-object v0

    #@8c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@8f
    .line 1119
    iget-object v0, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@91
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@98
    .line 1120
    iget-object v0, p0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@9a
    invoke-virtual {v0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    #@9d
    move-result-object v0

    #@9e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a1
    goto/16 :goto_18

    #@a3
    .line 1125
    :cond_a3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@a6
    goto/16 :goto_1f
.end method
