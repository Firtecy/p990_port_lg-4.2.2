.class final Landroid/content/res/XmlBlock$Parser;
.super Ljava/lang/Object;
.source "XmlBlock.java"

# interfaces
.implements Landroid/content/res/XmlResourceParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/res/XmlBlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "Parser"
.end annotation


# instance fields
.field private final mBlock:Landroid/content/res/XmlBlock;

.field private mDecNextDepth:Z

.field private mDepth:I

.field private mEventType:I

.field mParseState:I

.field private mStarted:Z

.field final synthetic this$0:Landroid/content/res/XmlBlock;


# direct methods
.method constructor <init>(Landroid/content/res/XmlBlock;ILandroid/content/res/XmlBlock;)V
    .registers 5
    .parameter
    .parameter "parseState"
    .parameter "block"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 78
    iput-object p1, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 463
    iput-boolean v0, p0, Landroid/content/res/XmlBlock$Parser;->mStarted:Z

    #@8
    .line 464
    iput-boolean v0, p0, Landroid/content/res/XmlBlock$Parser;->mDecNextDepth:Z

    #@a
    .line 465
    iput v0, p0, Landroid/content/res/XmlBlock$Parser;->mDepth:I

    #@c
    .line 466
    iput v0, p0, Landroid/content/res/XmlBlock$Parser;->mEventType:I

    #@e
    .line 79
    iput p2, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@10
    .line 80
    iput-object p3, p0, Landroid/content/res/XmlBlock$Parser;->mBlock:Landroid/content/res/XmlBlock;

    #@12
    .line 81
    invoke-static {p3}, Landroid/content/res/XmlBlock;->access$008(Landroid/content/res/XmlBlock;)I

    #@15
    .line 82
    return-void
.end method


# virtual methods
.method public close()V
    .registers 3

    #@0
    .prologue
    .line 444
    iget-object v1, p0, Landroid/content/res/XmlBlock$Parser;->mBlock:Landroid/content/res/XmlBlock;

    #@2
    monitor-enter v1

    #@3
    .line 445
    :try_start_3
    iget v0, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@5
    if-eqz v0, :cond_14

    #@7
    .line 446
    iget v0, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@9
    #calls: Landroid/content/res/XmlBlock;->nativeDestroyParseState(I)V
    invoke-static {v0}, Landroid/content/res/XmlBlock;->access$1500(I)V

    #@c
    .line 447
    const/4 v0, 0x0

    #@d
    iput v0, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@f
    .line 448
    iget-object v0, p0, Landroid/content/res/XmlBlock$Parser;->mBlock:Landroid/content/res/XmlBlock;

    #@11
    #calls: Landroid/content/res/XmlBlock;->decOpenCountLocked()V
    invoke-static {v0}, Landroid/content/res/XmlBlock;->access$1600(Landroid/content/res/XmlBlock;)V

    #@14
    .line 450
    :cond_14
    monitor-exit v1

    #@15
    .line 451
    return-void

    #@16
    .line 450
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public defineEntityReplacementText(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "entityName"
    .parameter "replacementText"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 115
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    #@2
    const-string v1, "defineEntityReplacementText() not supported"

    #@4
    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 454
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->close()V

    #@3
    .line 455
    return-void
.end method

.method public getAttributeBooleanValue(IZ)Z
    .registers 5
    .parameter "idx"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 376
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeDataType(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$800(II)I

    #@5
    move-result v0

    #@6
    .line 379
    .local v0, t:I
    const/16 v1, 0x10

    #@8
    if-lt v0, v1, :cond_1a

    #@a
    const/16 v1, 0x1f

    #@c
    if-gt v0, v1, :cond_1a

    #@e
    .line 381
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@10
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeData(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$900(II)I

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_18

    #@16
    const/4 v1, 0x1

    #@17
    .line 383
    :goto_17
    return v1

    #@18
    .line 381
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_17

    #@1a
    :cond_1a
    move v1, p2

    #@1b
    .line 383
    goto :goto_17
.end method

.method public getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 6
    .parameter "namespace"
    .parameter "attribute"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 324
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeIndex(ILjava/lang/String;Ljava/lang/String;)I
    invoke-static {v1, p1, p2}, Landroid/content/res/XmlBlock;->access$1000(ILjava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 325
    .local v0, idx:I
    if-ltz v0, :cond_c

    #@8
    .line 326
    invoke-virtual {p0, v0, p3}, Landroid/content/res/XmlBlock$Parser;->getAttributeBooleanValue(IZ)Z

    #@b
    move-result p3

    #@c
    .line 328
    .end local p3
    :cond_c
    return p3
.end method

.method public getAttributeCount()I
    .registers 3

    #@0
    .prologue
    .line 198
    iget v0, p0, Landroid/content/res/XmlBlock$Parser;->mEventType:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_c

    #@5
    iget v0, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@7
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeCount(I)I
    invoke-static {v0}, Landroid/content/res/XmlBlock;->access$600(I)I

    #@a
    move-result v0

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, -0x1

    #@d
    goto :goto_b
.end method

.method public getAttributeFloatValue(IF)F
    .registers 6
    .parameter "idx"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 415
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeDataType(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$800(II)I

    #@5
    move-result v0

    #@6
    .line 418
    .local v0, t:I
    const/4 v1, 0x4

    #@7
    if-ne v0, v1, :cond_14

    #@9
    .line 419
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@b
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeData(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$900(II)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    #@12
    move-result v1

    #@13
    return v1

    #@14
    .line 422
    :cond_14
    new-instance v1, Ljava/lang/RuntimeException;

    #@16
    const-string/jumbo v2, "not a float!"

    #@19
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v1
.end method

.method public getAttributeFloatValue(Ljava/lang/String;Ljava/lang/String;F)F
    .registers 6
    .parameter "namespace"
    .parameter "attribute"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 357
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeIndex(ILjava/lang/String;Ljava/lang/String;)I
    invoke-static {v1, p1, p2}, Landroid/content/res/XmlBlock;->access$1000(ILjava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 358
    .local v0, idx:I
    if-ltz v0, :cond_c

    #@8
    .line 359
    invoke-virtual {p0, v0, p3}, Landroid/content/res/XmlBlock$Parser;->getAttributeFloatValue(IF)F

    #@b
    move-result p3

    #@c
    .line 361
    .end local p3
    :cond_c
    return p3
.end method

.method public getAttributeIntValue(II)I
    .registers 5
    .parameter "idx"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 395
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeDataType(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$800(II)I

    #@5
    move-result v0

    #@6
    .line 398
    .local v0, t:I
    const/16 v1, 0x10

    #@8
    if-lt v0, v1, :cond_14

    #@a
    const/16 v1, 0x1f

    #@c
    if-gt v0, v1, :cond_14

    #@e
    .line 400
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@10
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeData(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$900(II)I

    #@13
    move-result p2

    #@14
    .line 402
    .end local p2
    :cond_14
    return p2
.end method

.method public getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 6
    .parameter "namespace"
    .parameter "attribute"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 340
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeIndex(ILjava/lang/String;Ljava/lang/String;)I
    invoke-static {v1, p1, p2}, Landroid/content/res/XmlBlock;->access$1000(ILjava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 341
    .local v0, idx:I
    if-ltz v0, :cond_c

    #@8
    .line 342
    invoke-virtual {p0, v0, p3}, Landroid/content/res/XmlBlock$Parser;->getAttributeIntValue(II)I

    #@b
    move-result p3

    #@c
    .line 344
    .end local p3
    :cond_c
    return p3
.end method

.method public getAttributeListValue(I[Ljava/lang/String;I)I
    .registers 7
    .parameter "idx"
    .parameter "options"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 366
    iget v2, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeDataType(II)I
    invoke-static {v2, p1}, Landroid/content/res/XmlBlock;->access$800(II)I

    #@5
    move-result v0

    #@6
    .line 367
    .local v0, t:I
    iget v2, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@8
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeData(II)I
    invoke-static {v2, p1}, Landroid/content/res/XmlBlock;->access$900(II)I

    #@b
    move-result v1

    #@c
    .line 368
    .local v1, v:I
    const/4 v2, 0x3

    #@d
    if-ne v0, v2, :cond_1b

    #@f
    .line 369
    iget-object v2, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@11
    iget-object v2, v2, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@13
    invoke-virtual {v2, v1}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@16
    move-result-object v2

    #@17
    invoke-static {v2, p2, p3}, Lcom/android/internal/util/XmlUtils;->convertValueToList(Ljava/lang/CharSequence;[Ljava/lang/String;I)I

    #@1a
    move-result v1

    #@1b
    .line 372
    .end local v1           #v:I
    :cond_1b
    return v1
.end method

.method public getAttributeListValue(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)I
    .registers 7
    .parameter "namespace"
    .parameter "attribute"
    .parameter "options"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 316
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeIndex(ILjava/lang/String;Ljava/lang/String;)I
    invoke-static {v1, p1, p2}, Landroid/content/res/XmlBlock;->access$1000(ILjava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 317
    .local v0, idx:I
    if-ltz v0, :cond_c

    #@8
    .line 318
    invoke-virtual {p0, v0, p3, p4}, Landroid/content/res/XmlBlock$Parser;->getAttributeListValue(I[Ljava/lang/String;I)I

    #@b
    move-result p4

    #@c
    .line 320
    .end local p4
    :cond_c
    return p4
.end method

.method public getAttributeName(I)Ljava/lang/String;
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 185
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeName(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$500(II)I

    #@5
    move-result v0

    #@6
    .line 187
    .local v0, id:I
    if-ltz v0, :cond_15

    #@8
    iget-object v1, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@a
    iget-object v1, v1, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@c
    invoke-virtual {v1, v0}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    return-object v1

    #@15
    .line 188
    :cond_15
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@17
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1
.end method

.method public getAttributeNameResource(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 311
    iget v0, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeResource(II)I
    invoke-static {v0, p1}, Landroid/content/res/XmlBlock;->access$1100(II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getAttributeNamespace(I)Ljava/lang/String;
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 178
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeNamespace(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$400(II)I

    #@5
    move-result v0

    #@6
    .line 180
    .local v0, id:I
    if-ltz v0, :cond_15

    #@8
    iget-object v1, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@a
    iget-object v1, v1, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@c
    invoke-virtual {v1, v0}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 181
    :goto_14
    return-object v1

    #@15
    :cond_15
    const/4 v1, -0x1

    #@16
    if-ne v0, v1, :cond_1b

    #@18
    const-string v1, ""

    #@1a
    goto :goto_14

    #@1b
    .line 182
    :cond_1b
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@1d
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1
.end method

.method public getAttributePrefix(I)Ljava/lang/String;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 191
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "getAttributePrefix not supported"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getAttributeResourceValue(II)I
    .registers 5
    .parameter "idx"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 386
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeDataType(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$800(II)I

    #@5
    move-result v0

    #@6
    .line 389
    .local v0, t:I
    const/4 v1, 0x1

    #@7
    if-ne v0, v1, :cond_f

    #@9
    .line 390
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@b
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeData(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$900(II)I

    #@e
    move-result p2

    #@f
    .line 392
    .end local p2
    :cond_f
    return p2
.end method

.method public getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 6
    .parameter "namespace"
    .parameter "attribute"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 332
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeIndex(ILjava/lang/String;Ljava/lang/String;)I
    invoke-static {v1, p1, p2}, Landroid/content/res/XmlBlock;->access$1000(ILjava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 333
    .local v0, idx:I
    if-ltz v0, :cond_c

    #@8
    .line 334
    invoke-virtual {p0, v0, p3}, Landroid/content/res/XmlBlock$Parser;->getAttributeResourceValue(II)I

    #@b
    move-result p3

    #@c
    .line 336
    .end local p3
    :cond_c
    return p3
.end method

.method public getAttributeType(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 215
    const-string v0, "CDATA"

    #@2
    return-object v0
.end method

.method public getAttributeUnsignedIntValue(II)I
    .registers 5
    .parameter "idx"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 405
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeDataType(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$800(II)I

    #@5
    move-result v0

    #@6
    .line 408
    .local v0, t:I
    const/16 v1, 0x10

    #@8
    if-lt v0, v1, :cond_14

    #@a
    const/16 v1, 0x1f

    #@c
    if-gt v0, v1, :cond_14

    #@e
    .line 410
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@10
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeData(II)I
    invoke-static {v1, p1}, Landroid/content/res/XmlBlock;->access$900(II)I

    #@13
    move-result p2

    #@14
    .line 412
    .end local p2
    :cond_14
    return p2
.end method

.method public getAttributeUnsignedIntValue(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 6
    .parameter "namespace"
    .parameter "attribute"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 349
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeIndex(ILjava/lang/String;Ljava/lang/String;)I
    invoke-static {v1, p1, p2}, Landroid/content/res/XmlBlock;->access$1000(ILjava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 350
    .local v0, idx:I
    if-ltz v0, :cond_c

    #@8
    .line 351
    invoke-virtual {p0, v0, p3}, Landroid/content/res/XmlBlock$Parser;->getAttributeUnsignedIntValue(II)I

    #@b
    move-result p3

    #@c
    .line 353
    .end local p3
    :cond_c
    return p3
.end method

.method public getAttributeValue(I)Ljava/lang/String;
    .registers 7
    .parameter "index"

    #@0
    .prologue
    .line 201
    iget v3, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeStringValue(II)I
    invoke-static {v3, p1}, Landroid/content/res/XmlBlock;->access$700(II)I

    #@5
    move-result v0

    #@6
    .line 203
    .local v0, id:I
    if-ltz v0, :cond_15

    #@8
    iget-object v3, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@a
    iget-object v3, v3, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@c
    invoke-virtual {v3, v0}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    .line 212
    :goto_14
    return-object v3

    #@15
    .line 206
    :cond_15
    iget v3, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@17
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeDataType(II)I
    invoke-static {v3, p1}, Landroid/content/res/XmlBlock;->access$800(II)I

    #@1a
    move-result v1

    #@1b
    .line 207
    .local v1, t:I
    if-nez v1, :cond_27

    #@1d
    .line 208
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    #@1f
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-direct {v3, v4}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@26
    throw v3

    #@27
    .line 211
    :cond_27
    iget v3, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@29
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeData(II)I
    invoke-static {v3, p1}, Landroid/content/res/XmlBlock;->access$900(II)I

    #@2c
    move-result v2

    #@2d
    .line 212
    .local v2, v:I
    invoke-static {v1, v2}, Landroid/util/TypedValue;->coerceToString(II)Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    goto :goto_14
.end method

.method public getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "namespace"
    .parameter "name"

    #@0
    .prologue
    .line 224
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetAttributeIndex(ILjava/lang/String;Ljava/lang/String;)I
    invoke-static {v1, p1, p2}, Landroid/content/res/XmlBlock;->access$1000(ILjava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 225
    .local v0, idx:I
    if-ltz v0, :cond_d

    #@8
    .line 232
    invoke-virtual {p0, v0}, Landroid/content/res/XmlBlock$Parser;->getAttributeValue(I)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 234
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method public getClassAttribute()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 430
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetClassAttribute(I)I
    invoke-static {v1}, Landroid/content/res/XmlBlock;->access$1300(I)I

    #@5
    move-result v0

    #@6
    .line 431
    .local v0, id:I
    if-ltz v0, :cond_15

    #@8
    iget-object v1, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@a
    iget-object v1, v1, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@c
    invoke-virtual {v1, v0}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    :goto_14
    return-object v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14
.end method

.method public getColumnNumber()I
    .registers 2

    #@0
    .prologue
    .line 136
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public getDepth()I
    .registers 2

    #@0
    .prologue
    .line 139
    iget v0, p0, Landroid/content/res/XmlBlock$Parser;->mDepth:I

    #@2
    return v0
.end method

.method public getEventType()I
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 149
    iget v0, p0, Landroid/content/res/XmlBlock$Parser;->mEventType:I

    #@2
    return v0
.end method

.method public getFeature(Ljava/lang/String;)Z
    .registers 4
    .parameter "name"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 94
    const-string v1, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    #@3
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_a

    #@9
    .line 100
    :cond_9
    :goto_9
    return v0

    #@a
    .line 97
    :cond_a
    const-string v1, "http://xmlpull.org/v1/doc/features.html#report-namespace-prefixes"

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_9

    #@12
    .line 100
    const/4 v0, 0x0

    #@13
    goto :goto_9
.end method

.method public getIdAttribute()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 426
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetIdAttribute(I)I
    invoke-static {v1}, Landroid/content/res/XmlBlock;->access$1200(I)I

    #@5
    move-result v0

    #@6
    .line 427
    .local v0, id:I
    if-ltz v0, :cond_15

    #@8
    iget-object v1, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@a
    iget-object v1, v1, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@c
    invoke-virtual {v1, v0}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    :goto_14
    return-object v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14
.end method

.method public getIdAttributeResourceValue(I)I
    .registers 4
    .parameter "defaultValue"

    #@0
    .prologue
    .line 436
    const/4 v0, 0x0

    #@1
    const-string v1, "id"

    #@3
    invoke-virtual {p0, v0, v1, p1}, Landroid/content/res/XmlBlock$Parser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getInputEncoding()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 121
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getLineNumber()I
    .registers 2

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetLineNumber(I)I
    invoke-static {v0}, Landroid/content/res/XmlBlock;->access$200(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 174
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    invoke-static {v1}, Landroid/content/res/XmlBlock;->nativeGetName(I)I

    #@5
    move-result v0

    #@6
    .line 175
    .local v0, id:I
    if-ltz v0, :cond_15

    #@8
    iget-object v1, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@a
    iget-object v1, v1, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@c
    invoke-virtual {v1, v0}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    :goto_14
    return-object v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14
.end method

.method public getNamespace()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 170
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetNamespace(I)I
    invoke-static {v1}, Landroid/content/res/XmlBlock;->access$300(I)I

    #@5
    move-result v0

    #@6
    .line 171
    .local v0, id:I
    if-ltz v0, :cond_15

    #@8
    iget-object v1, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@a
    iget-object v1, v1, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@c
    invoke-virtual {v1, v0}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    :goto_14
    return-object v1

    #@15
    :cond_15
    const-string v1, ""

    #@17
    goto :goto_14
.end method

.method public getNamespace(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "prefix"

    #@0
    .prologue
    .line 124
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "getNamespace() not supported"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getNamespaceCount(I)I
    .registers 4
    .parameter "depth"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 127
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    #@2
    const-string v1, "getNamespaceCount() not supported"

    #@4
    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getNamespacePrefix(I)Ljava/lang/String;
    .registers 4
    .parameter "pos"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 118
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    #@2
    const-string v1, "getNamespacePrefix() not supported"

    #@4
    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getNamespaceUri(I)Ljava/lang/String;
    .registers 4
    .parameter "pos"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 133
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    #@2
    const-string v1, "getNamespaceUri() not supported"

    #@4
    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method final getPooledString(I)Ljava/lang/CharSequence;
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 458
    iget-object v0, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@2
    iget-object v0, v0, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@4
    invoke-virtual {v0, p1}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getPositionDescription()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Binary XML file line #"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getLineNumber()I

    #@e
    move-result v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 156
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "getPrefix not supported"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 106
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getStyleAttribute()I
    .registers 2

    #@0
    .prologue
    .line 440
    iget v0, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetStyleAttribute(I)I
    invoke-static {v0}, Landroid/content/res/XmlBlock;->access$1400(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getText()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 142
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@2
    #calls: Landroid/content/res/XmlBlock;->nativeGetText(I)I
    invoke-static {v1}, Landroid/content/res/XmlBlock;->access$100(I)I

    #@5
    move-result v0

    #@6
    .line 143
    .local v0, id:I
    if-ltz v0, :cond_15

    #@8
    iget-object v1, p0, Landroid/content/res/XmlBlock$Parser;->this$0:Landroid/content/res/XmlBlock;

    #@a
    iget-object v1, v1, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@c
    invoke-virtual {v1, v0}, Landroid/content/res/StringBlock;->get(I)Ljava/lang/CharSequence;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    :goto_14
    return-object v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14
.end method

.method public getTextCharacters([I)[C
    .registers 7
    .parameter "holderForStartAndLength"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 159
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getText()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 160
    .local v1, txt:Ljava/lang/String;
    const/4 v0, 0x0

    #@6
    .line 161
    .local v0, chars:[C
    if-eqz v1, :cond_1e

    #@8
    .line 162
    aput v4, p1, v4

    #@a
    .line 163
    const/4 v2, 0x1

    #@b
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@e
    move-result v3

    #@f
    aput v3, p1, v2

    #@11
    .line 164
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@14
    move-result v2

    #@15
    new-array v0, v2, [C

    #@17
    .line 165
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@1a
    move-result v2

    #@1b
    invoke-virtual {v1, v4, v2, v0, v4}, Ljava/lang/String;->getChars(II[CI)V

    #@1e
    .line 167
    :cond_1e
    return-object v0
.end method

.method public isAttributeDefault(I)Z
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 218
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isEmptyElementTag()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 195
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isWhitespace()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 153
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public next()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 237
    iget-boolean v3, p0, Landroid/content/res/XmlBlock$Parser;->mStarted:Z

    #@4
    if-nez v3, :cond_a

    #@6
    .line 238
    iput-boolean v2, p0, Landroid/content/res/XmlBlock$Parser;->mStarted:Z

    #@8
    move v0, v1

    #@9
    .line 265
    :cond_9
    :goto_9
    return v0

    #@a
    .line 241
    :cond_a
    iget v3, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@c
    if-nez v3, :cond_10

    #@e
    move v0, v2

    #@f
    .line 242
    goto :goto_9

    #@10
    .line 244
    :cond_10
    iget v3, p0, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@12
    invoke-static {v3}, Landroid/content/res/XmlBlock;->nativeNext(I)I

    #@15
    move-result v0

    #@16
    .line 245
    .local v0, ev:I
    iget-boolean v3, p0, Landroid/content/res/XmlBlock$Parser;->mDecNextDepth:Z

    #@18
    if-eqz v3, :cond_22

    #@1a
    .line 246
    iget v3, p0, Landroid/content/res/XmlBlock$Parser;->mDepth:I

    #@1c
    add-int/lit8 v3, v3, -0x1

    #@1e
    iput v3, p0, Landroid/content/res/XmlBlock$Parser;->mDepth:I

    #@20
    .line 247
    iput-boolean v1, p0, Landroid/content/res/XmlBlock$Parser;->mDecNextDepth:Z

    #@22
    .line 249
    :cond_22
    packed-switch v0, :pswitch_data_38

    #@25
    .line 257
    :goto_25
    iput v0, p0, Landroid/content/res/XmlBlock$Parser;->mEventType:I

    #@27
    .line 258
    if-ne v0, v2, :cond_9

    #@29
    .line 263
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->close()V

    #@2c
    goto :goto_9

    #@2d
    .line 251
    :pswitch_2d
    iget v1, p0, Landroid/content/res/XmlBlock$Parser;->mDepth:I

    #@2f
    add-int/lit8 v1, v1, 0x1

    #@31
    iput v1, p0, Landroid/content/res/XmlBlock$Parser;->mDepth:I

    #@33
    goto :goto_25

    #@34
    .line 254
    :pswitch_34
    iput-boolean v2, p0, Landroid/content/res/XmlBlock$Parser;->mDecNextDepth:Z

    #@36
    goto :goto_25

    #@37
    .line 249
    nop

    #@38
    :pswitch_data_38
    .packed-switch 0x2
        :pswitch_2d
        :pswitch_34
    .end packed-switch
.end method

.method public nextTag()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 298
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->next()I

    #@3
    move-result v0

    #@4
    .line 299
    .local v0, eventType:I
    const/4 v1, 0x4

    #@5
    if-ne v0, v1, :cond_11

    #@7
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->isWhitespace()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_11

    #@d
    .line 300
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->next()I

    #@10
    move-result v0

    #@11
    .line 302
    :cond_11
    const/4 v1, 0x2

    #@12
    if-eq v0, v1, :cond_35

    #@14
    const/4 v1, 0x3

    #@15
    if-eq v0, v1, :cond_35

    #@17
    .line 303
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    #@19
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getPositionDescription()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    const-string v3, ": expected start or end tag"

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    const/4 v3, 0x0

    #@31
    invoke-direct {v1, v2, p0, v3}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Throwable;)V

    #@34
    throw v1

    #@35
    .line 307
    :cond_35
    return v0
.end method

.method public nextText()Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v5, 0x0

    #@2
    .line 274
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getEventType()I

    #@5
    move-result v2

    #@6
    const/4 v3, 0x2

    #@7
    if-eq v2, v3, :cond_26

    #@9
    .line 275
    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getPositionDescription()Ljava/lang/String;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    const-string v4, ": parser must be on START_TAG to read next text"

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-direct {v2, v3, p0, v5}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Throwable;)V

    #@25
    throw v2

    #@26
    .line 279
    :cond_26
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->next()I

    #@29
    move-result v0

    #@2a
    .line 280
    .local v0, eventType:I
    const/4 v2, 0x4

    #@2b
    if-ne v0, v2, :cond_54

    #@2d
    .line 281
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getText()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    .line 282
    .local v1, result:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->next()I

    #@34
    move-result v0

    #@35
    .line 283
    if-eq v0, v4, :cond_58

    #@37
    .line 284
    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    #@39
    new-instance v3, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getPositionDescription()Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    const-string v4, ": event TEXT it must be immediately followed by END_TAG"

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-direct {v2, v3, p0, v5}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Throwable;)V

    #@53
    throw v2

    #@54
    .line 289
    .end local v1           #result:Ljava/lang/String;
    :cond_54
    if-ne v0, v4, :cond_59

    #@56
    .line 290
    const-string v1, ""

    #@58
    :cond_58
    return-object v1

    #@59
    .line 292
    :cond_59
    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    #@5b
    new-instance v3, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getPositionDescription()Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    const-string v4, ": parser must be on START_TAG or TEXT to read text"

    #@6a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    invoke-direct {v2, v3, p0, v5}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Throwable;)V

    #@75
    throw v2
.end method

.method public nextToken()I
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 221
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->next()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public require(ILjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "type"
    .parameter "namespace"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 268
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getEventType()I

    #@3
    move-result v0

    #@4
    if-ne p1, v0, :cond_1e

    #@6
    if-eqz p2, :cond_12

    #@8
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getNamespace()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_1e

    #@12
    :cond_12
    if-eqz p3, :cond_43

    #@14
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getName()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_43

    #@1e
    .line 271
    :cond_1e
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    #@20
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v2, "expected "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    sget-object v2, Landroid/content/res/XmlBlock$Parser;->TYPES:[Ljava/lang/String;

    #@2d
    aget-object v2, v2, p1

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {p0}, Landroid/content/res/XmlBlock$Parser;->getPositionDescription()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@42
    throw v0

    #@43
    .line 272
    :cond_43
    return-void
.end method

.method public setFeature(Ljava/lang/String;Z)V
    .registers 6
    .parameter "name"
    .parameter "state"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 85
    const-string v0, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    #@2
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    if-eqz p2, :cond_b

    #@a
    .line 89
    :cond_a
    return-void

    #@b
    .line 88
    :cond_b
    const-string v0, "http://xmlpull.org/v1/doc/features.html#report-namespace-prefixes"

    #@d
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_15

    #@13
    if-nez p2, :cond_a

    #@15
    .line 91
    :cond_15
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "Unsupported feature: "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v0
.end method

.method public setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    .registers 5
    .parameter "inputStream"
    .parameter "inputEncoding"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 112
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    #@2
    const-string/jumbo v1, "setInput() not supported"

    #@5
    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@8
    throw v0
.end method

.method public setInput(Ljava/io/Reader;)V
    .registers 4
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 109
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    #@2
    const-string/jumbo v1, "setInput() not supported"

    #@5
    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@8
    throw v0
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 5
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 103
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    #@2
    const-string/jumbo v1, "setProperty() not supported"

    #@5
    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@8
    throw v0
.end method
