.class final Landroid/content/res/StringBlock;
.super Ljava/lang/Object;
.source "StringBlock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/res/StringBlock$Height;,
        Landroid/content/res/StringBlock$StyleIDs;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AssetManager"

.field private static final localLOGV:Z


# instance fields
.field private final mNative:I

.field private final mOwnsNative:Z

.field private mSparseStrings:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private mStrings:[Ljava/lang/CharSequence;

.field mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

.field private final mUseSparse:Z


# direct methods
.method constructor <init>(IZ)V
    .registers 4
    .parameter "obj"
    .parameter "useSparse"

    #@0
    .prologue
    .line 418
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@6
    .line 419
    iput p1, p0, Landroid/content/res/StringBlock;->mNative:I

    #@8
    .line 420
    iput-boolean p2, p0, Landroid/content/res/StringBlock;->mUseSparse:Z

    #@a
    .line 421
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/content/res/StringBlock;->mOwnsNative:Z

    #@d
    .line 424
    return-void
.end method

.method public constructor <init>([BIIZ)V
    .registers 6
    .parameter "data"
    .parameter "offset"
    .parameter "size"
    .parameter "useSparse"

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@6
    .line 54
    invoke-static {p1, p2, p3}, Landroid/content/res/StringBlock;->nativeCreate([BII)I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/content/res/StringBlock;->mNative:I

    #@c
    .line 55
    iput-boolean p4, p0, Landroid/content/res/StringBlock;->mUseSparse:Z

    #@e
    .line 56
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Landroid/content/res/StringBlock;->mOwnsNative:Z

    #@11
    .line 59
    return-void
.end method

.method public constructor <init>([BZ)V
    .registers 5
    .parameter "data"
    .parameter "useSparse"

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@6
    .line 46
    const/4 v0, 0x0

    #@7
    array-length v1, p1

    #@8
    invoke-static {p1, v0, v1}, Landroid/content/res/StringBlock;->nativeCreate([BII)I

    #@b
    move-result v0

    #@c
    iput v0, p0, Landroid/content/res/StringBlock;->mNative:I

    #@e
    .line 47
    iput-boolean p2, p0, Landroid/content/res/StringBlock;->mUseSparse:Z

    #@10
    .line 48
    const/4 v0, 0x1

    #@11
    iput-boolean v0, p0, Landroid/content/res/StringBlock;->mOwnsNative:Z

    #@13
    .line 51
    return-void
.end method

.method private static addParagraphSpan(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .registers 7
    .parameter "buffer"
    .parameter "what"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/16 v2, 0xa

    #@2
    .line 298
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@5
    move-result v0

    #@6
    .line 300
    .local v0, len:I
    if-eqz p2, :cond_1e

    #@8
    if-eq p2, v0, :cond_1e

    #@a
    add-int/lit8 v1, p2, -0x1

    #@c
    invoke-interface {p0, v1}, Landroid/text/Spannable;->charAt(I)C

    #@f
    move-result v1

    #@10
    if-eq v1, v2, :cond_1e

    #@12
    .line 301
    add-int/lit8 p2, p2, -0x1

    #@14
    :goto_14
    if-lez p2, :cond_1e

    #@16
    .line 302
    add-int/lit8 v1, p2, -0x1

    #@18
    invoke-interface {p0, v1}, Landroid/text/Spannable;->charAt(I)C

    #@1b
    move-result v1

    #@1c
    if-ne v1, v2, :cond_3c

    #@1e
    .line 308
    :cond_1e
    if-eqz p3, :cond_36

    #@20
    if-eq p3, v0, :cond_36

    #@22
    add-int/lit8 v1, p3, -0x1

    #@24
    invoke-interface {p0, v1}, Landroid/text/Spannable;->charAt(I)C

    #@27
    move-result v1

    #@28
    if-eq v1, v2, :cond_36

    #@2a
    .line 309
    add-int/lit8 p3, p3, 0x1

    #@2c
    :goto_2c
    if-ge p3, v0, :cond_36

    #@2e
    .line 310
    add-int/lit8 v1, p3, -0x1

    #@30
    invoke-interface {p0, v1}, Landroid/text/Spannable;->charAt(I)C

    #@33
    move-result v1

    #@34
    if-ne v1, v2, :cond_3f

    #@36
    .line 316
    :cond_36
    const/16 v1, 0x33

    #@38
    invoke-interface {p0, p1, p2, p3, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@3b
    .line 317
    return-void

    #@3c
    .line 301
    :cond_3c
    add-int/lit8 p2, p2, -0x1

    #@3e
    goto :goto_14

    #@3f
    .line 309
    :cond_3f
    add-int/lit8 p3, p3, 0x1

    #@41
    goto :goto_2c
.end method

.method private applyStyles(Ljava/lang/String;[ILandroid/content/res/StringBlock$StyleIDs;)Ljava/lang/CharSequence;
    .registers 25
    .parameter "str"
    .parameter "style"
    .parameter "ids"

    #@0
    .prologue
    .line 162
    move-object/from16 v0, p2

    #@2
    array-length v0, v0

    #@3
    move/from16 v17, v0

    #@5
    if-nez v17, :cond_8

    #@7
    .line 288
    .end local p1
    :goto_7
    return-object p1

    #@8
    .line 165
    .restart local p1
    :cond_8
    new-instance v4, Landroid/text/SpannableString;

    #@a
    move-object/from16 v0, p1

    #@c
    invoke-direct {v4, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    #@f
    .line 166
    .local v4, buffer:Landroid/text/SpannableString;
    const/4 v7, 0x0

    #@10
    .line 167
    .local v7, i:I
    :goto_10
    move-object/from16 v0, p2

    #@12
    array-length v0, v0

    #@13
    move/from16 v17, v0

    #@15
    move/from16 v0, v17

    #@17
    if-ge v7, v0, :cond_325

    #@19
    .line 168
    aget v15, p2, v7

    #@1b
    .line 173
    .local v15, type:I
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->boldId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$000(Landroid/content/res/StringBlock$StyleIDs;)I

    #@1e
    move-result v17

    #@1f
    move/from16 v0, v17

    #@21
    if-ne v15, v0, :cond_44

    #@23
    .line 174
    new-instance v17, Landroid/text/style/StyleSpan;

    #@25
    const/16 v18, 0x1

    #@27
    invoke-direct/range {v17 .. v18}, Landroid/text/style/StyleSpan;-><init>(I)V

    #@2a
    add-int/lit8 v18, v7, 0x1

    #@2c
    aget v18, p2, v18

    #@2e
    add-int/lit8 v19, v7, 0x2

    #@30
    aget v19, p2, v19

    #@32
    add-int/lit8 v19, v19, 0x1

    #@34
    const/16 v20, 0x21

    #@36
    move-object/from16 v0, v17

    #@38
    move/from16 v1, v18

    #@3a
    move/from16 v2, v19

    #@3c
    move/from16 v3, v20

    #@3e
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@41
    .line 286
    :cond_41
    :goto_41
    add-int/lit8 v7, v7, 0x3

    #@43
    .line 287
    goto :goto_10

    #@44
    .line 177
    :cond_44
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->italicId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$100(Landroid/content/res/StringBlock$StyleIDs;)I

    #@47
    move-result v17

    #@48
    move/from16 v0, v17

    #@4a
    if-ne v15, v0, :cond_6b

    #@4c
    .line 178
    new-instance v17, Landroid/text/style/StyleSpan;

    #@4e
    const/16 v18, 0x2

    #@50
    invoke-direct/range {v17 .. v18}, Landroid/text/style/StyleSpan;-><init>(I)V

    #@53
    add-int/lit8 v18, v7, 0x1

    #@55
    aget v18, p2, v18

    #@57
    add-int/lit8 v19, v7, 0x2

    #@59
    aget v19, p2, v19

    #@5b
    add-int/lit8 v19, v19, 0x1

    #@5d
    const/16 v20, 0x21

    #@5f
    move-object/from16 v0, v17

    #@61
    move/from16 v1, v18

    #@63
    move/from16 v2, v19

    #@65
    move/from16 v3, v20

    #@67
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@6a
    goto :goto_41

    #@6b
    .line 181
    :cond_6b
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->underlineId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$200(Landroid/content/res/StringBlock$StyleIDs;)I

    #@6e
    move-result v17

    #@6f
    move/from16 v0, v17

    #@71
    if-ne v15, v0, :cond_90

    #@73
    .line 182
    new-instance v17, Landroid/text/style/UnderlineSpan;

    #@75
    invoke-direct/range {v17 .. v17}, Landroid/text/style/UnderlineSpan;-><init>()V

    #@78
    add-int/lit8 v18, v7, 0x1

    #@7a
    aget v18, p2, v18

    #@7c
    add-int/lit8 v19, v7, 0x2

    #@7e
    aget v19, p2, v19

    #@80
    add-int/lit8 v19, v19, 0x1

    #@82
    const/16 v20, 0x21

    #@84
    move-object/from16 v0, v17

    #@86
    move/from16 v1, v18

    #@88
    move/from16 v2, v19

    #@8a
    move/from16 v3, v20

    #@8c
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@8f
    goto :goto_41

    #@90
    .line 185
    :cond_90
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->ttId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$300(Landroid/content/res/StringBlock$StyleIDs;)I

    #@93
    move-result v17

    #@94
    move/from16 v0, v17

    #@96
    if-ne v15, v0, :cond_b8

    #@98
    .line 186
    new-instance v17, Landroid/text/style/TypefaceSpan;

    #@9a
    const-string/jumbo v18, "monospace"

    #@9d
    invoke-direct/range {v17 .. v18}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    #@a0
    add-int/lit8 v18, v7, 0x1

    #@a2
    aget v18, p2, v18

    #@a4
    add-int/lit8 v19, v7, 0x2

    #@a6
    aget v19, p2, v19

    #@a8
    add-int/lit8 v19, v19, 0x1

    #@aa
    const/16 v20, 0x21

    #@ac
    move-object/from16 v0, v17

    #@ae
    move/from16 v1, v18

    #@b0
    move/from16 v2, v19

    #@b2
    move/from16 v3, v20

    #@b4
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@b7
    goto :goto_41

    #@b8
    .line 189
    :cond_b8
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->bigId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$400(Landroid/content/res/StringBlock$StyleIDs;)I

    #@bb
    move-result v17

    #@bc
    move/from16 v0, v17

    #@be
    if-ne v15, v0, :cond_e0

    #@c0
    .line 190
    new-instance v17, Landroid/text/style/RelativeSizeSpan;

    #@c2
    const/high16 v18, 0x3fa0

    #@c4
    invoke-direct/range {v17 .. v18}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    #@c7
    add-int/lit8 v18, v7, 0x1

    #@c9
    aget v18, p2, v18

    #@cb
    add-int/lit8 v19, v7, 0x2

    #@cd
    aget v19, p2, v19

    #@cf
    add-int/lit8 v19, v19, 0x1

    #@d1
    const/16 v20, 0x21

    #@d3
    move-object/from16 v0, v17

    #@d5
    move/from16 v1, v18

    #@d7
    move/from16 v2, v19

    #@d9
    move/from16 v3, v20

    #@db
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@de
    goto/16 :goto_41

    #@e0
    .line 193
    :cond_e0
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->smallId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$500(Landroid/content/res/StringBlock$StyleIDs;)I

    #@e3
    move-result v17

    #@e4
    move/from16 v0, v17

    #@e6
    if-ne v15, v0, :cond_109

    #@e8
    .line 194
    new-instance v17, Landroid/text/style/RelativeSizeSpan;

    #@ea
    const v18, 0x3f4ccccd

    #@ed
    invoke-direct/range {v17 .. v18}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    #@f0
    add-int/lit8 v18, v7, 0x1

    #@f2
    aget v18, p2, v18

    #@f4
    add-int/lit8 v19, v7, 0x2

    #@f6
    aget v19, p2, v19

    #@f8
    add-int/lit8 v19, v19, 0x1

    #@fa
    const/16 v20, 0x21

    #@fc
    move-object/from16 v0, v17

    #@fe
    move/from16 v1, v18

    #@100
    move/from16 v2, v19

    #@102
    move/from16 v3, v20

    #@104
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@107
    goto/16 :goto_41

    #@109
    .line 197
    :cond_109
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->subId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$600(Landroid/content/res/StringBlock$StyleIDs;)I

    #@10c
    move-result v17

    #@10d
    move/from16 v0, v17

    #@10f
    if-ne v15, v0, :cond_12f

    #@111
    .line 198
    new-instance v17, Landroid/text/style/SubscriptSpan;

    #@113
    invoke-direct/range {v17 .. v17}, Landroid/text/style/SubscriptSpan;-><init>()V

    #@116
    add-int/lit8 v18, v7, 0x1

    #@118
    aget v18, p2, v18

    #@11a
    add-int/lit8 v19, v7, 0x2

    #@11c
    aget v19, p2, v19

    #@11e
    add-int/lit8 v19, v19, 0x1

    #@120
    const/16 v20, 0x21

    #@122
    move-object/from16 v0, v17

    #@124
    move/from16 v1, v18

    #@126
    move/from16 v2, v19

    #@128
    move/from16 v3, v20

    #@12a
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@12d
    goto/16 :goto_41

    #@12f
    .line 201
    :cond_12f
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->supId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$700(Landroid/content/res/StringBlock$StyleIDs;)I

    #@132
    move-result v17

    #@133
    move/from16 v0, v17

    #@135
    if-ne v15, v0, :cond_155

    #@137
    .line 202
    new-instance v17, Landroid/text/style/SuperscriptSpan;

    #@139
    invoke-direct/range {v17 .. v17}, Landroid/text/style/SuperscriptSpan;-><init>()V

    #@13c
    add-int/lit8 v18, v7, 0x1

    #@13e
    aget v18, p2, v18

    #@140
    add-int/lit8 v19, v7, 0x2

    #@142
    aget v19, p2, v19

    #@144
    add-int/lit8 v19, v19, 0x1

    #@146
    const/16 v20, 0x21

    #@148
    move-object/from16 v0, v17

    #@14a
    move/from16 v1, v18

    #@14c
    move/from16 v2, v19

    #@14e
    move/from16 v3, v20

    #@150
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@153
    goto/16 :goto_41

    #@155
    .line 205
    :cond_155
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->strikeId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$800(Landroid/content/res/StringBlock$StyleIDs;)I

    #@158
    move-result v17

    #@159
    move/from16 v0, v17

    #@15b
    if-ne v15, v0, :cond_17b

    #@15d
    .line 206
    new-instance v17, Landroid/text/style/StrikethroughSpan;

    #@15f
    invoke-direct/range {v17 .. v17}, Landroid/text/style/StrikethroughSpan;-><init>()V

    #@162
    add-int/lit8 v18, v7, 0x1

    #@164
    aget v18, p2, v18

    #@166
    add-int/lit8 v19, v7, 0x2

    #@168
    aget v19, p2, v19

    #@16a
    add-int/lit8 v19, v19, 0x1

    #@16c
    const/16 v20, 0x21

    #@16e
    move-object/from16 v0, v17

    #@170
    move/from16 v1, v18

    #@172
    move/from16 v2, v19

    #@174
    move/from16 v3, v20

    #@176
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@179
    goto/16 :goto_41

    #@17b
    .line 209
    :cond_17b
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->listItemId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$900(Landroid/content/res/StringBlock$StyleIDs;)I

    #@17e
    move-result v17

    #@17f
    move/from16 v0, v17

    #@181
    if-ne v15, v0, :cond_19f

    #@183
    .line 210
    new-instance v17, Landroid/text/style/BulletSpan;

    #@185
    const/16 v18, 0xa

    #@187
    invoke-direct/range {v17 .. v18}, Landroid/text/style/BulletSpan;-><init>(I)V

    #@18a
    add-int/lit8 v18, v7, 0x1

    #@18c
    aget v18, p2, v18

    #@18e
    add-int/lit8 v19, v7, 0x2

    #@190
    aget v19, p2, v19

    #@192
    add-int/lit8 v19, v19, 0x1

    #@194
    move-object/from16 v0, v17

    #@196
    move/from16 v1, v18

    #@198
    move/from16 v2, v19

    #@19a
    invoke-static {v4, v0, v1, v2}, Landroid/content/res/StringBlock;->addParagraphSpan(Landroid/text/Spannable;Ljava/lang/Object;II)V

    #@19d
    goto/16 :goto_41

    #@19f
    .line 212
    :cond_19f
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->marqueeId:I
    invoke-static/range {p3 .. p3}, Landroid/content/res/StringBlock$StyleIDs;->access$1000(Landroid/content/res/StringBlock$StyleIDs;)I

    #@1a2
    move-result v17

    #@1a3
    move/from16 v0, v17

    #@1a5
    if-ne v15, v0, :cond_1c2

    #@1a7
    .line 213
    sget-object v17, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@1a9
    add-int/lit8 v18, v7, 0x1

    #@1ab
    aget v18, p2, v18

    #@1ad
    add-int/lit8 v19, v7, 0x2

    #@1af
    aget v19, p2, v19

    #@1b1
    add-int/lit8 v19, v19, 0x1

    #@1b3
    const/16 v20, 0x12

    #@1b5
    move-object/from16 v0, v17

    #@1b7
    move/from16 v1, v18

    #@1b9
    move/from16 v2, v19

    #@1bb
    move/from16 v3, v20

    #@1bd
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@1c0
    goto/16 :goto_41

    #@1c2
    .line 217
    :cond_1c2
    move-object/from16 v0, p0

    #@1c4
    iget v0, v0, Landroid/content/res/StringBlock;->mNative:I

    #@1c6
    move/from16 v17, v0

    #@1c8
    move/from16 v0, v17

    #@1ca
    invoke-static {v0, v15}, Landroid/content/res/StringBlock;->nativeGetString(II)Ljava/lang/String;

    #@1cd
    move-result-object v14

    #@1ce
    .line 219
    .local v14, tag:Ljava/lang/String;
    const-string v17, "font;"

    #@1d0
    move-object/from16 v0, v17

    #@1d2
    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1d5
    move-result v17

    #@1d6
    if-eqz v17, :cond_292

    #@1d8
    .line 222
    const-string v17, ";height="

    #@1da
    move-object/from16 v0, v17

    #@1dc
    invoke-static {v14, v0}, Landroid/content/res/StringBlock;->subtag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1df
    move-result-object v12

    #@1e0
    .line 223
    .local v12, sub:Ljava/lang/String;
    if-eqz v12, :cond_200

    #@1e2
    .line 224
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1e5
    move-result v11

    #@1e6
    .line 225
    .local v11, size:I
    new-instance v17, Landroid/content/res/StringBlock$Height;

    #@1e8
    move-object/from16 v0, v17

    #@1ea
    invoke-direct {v0, v11}, Landroid/content/res/StringBlock$Height;-><init>(I)V

    #@1ed
    add-int/lit8 v18, v7, 0x1

    #@1ef
    aget v18, p2, v18

    #@1f1
    add-int/lit8 v19, v7, 0x2

    #@1f3
    aget v19, p2, v19

    #@1f5
    add-int/lit8 v19, v19, 0x1

    #@1f7
    move-object/from16 v0, v17

    #@1f9
    move/from16 v1, v18

    #@1fb
    move/from16 v2, v19

    #@1fd
    invoke-static {v4, v0, v1, v2}, Landroid/content/res/StringBlock;->addParagraphSpan(Landroid/text/Spannable;Ljava/lang/Object;II)V

    #@200
    .line 229
    .end local v11           #size:I
    :cond_200
    const-string v17, ";size="

    #@202
    move-object/from16 v0, v17

    #@204
    invoke-static {v14, v0}, Landroid/content/res/StringBlock;->subtag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@207
    move-result-object v12

    #@208
    .line 230
    if-eqz v12, :cond_230

    #@20a
    .line 231
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@20d
    move-result v11

    #@20e
    .line 232
    .restart local v11       #size:I
    new-instance v17, Landroid/text/style/AbsoluteSizeSpan;

    #@210
    const/16 v18, 0x1

    #@212
    move-object/from16 v0, v17

    #@214
    move/from16 v1, v18

    #@216
    invoke-direct {v0, v11, v1}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    #@219
    add-int/lit8 v18, v7, 0x1

    #@21b
    aget v18, p2, v18

    #@21d
    add-int/lit8 v19, v7, 0x2

    #@21f
    aget v19, p2, v19

    #@221
    add-int/lit8 v19, v19, 0x1

    #@223
    const/16 v20, 0x21

    #@225
    move-object/from16 v0, v17

    #@227
    move/from16 v1, v18

    #@229
    move/from16 v2, v19

    #@22b
    move/from16 v3, v20

    #@22d
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@230
    .line 237
    .end local v11           #size:I
    :cond_230
    const-string v17, ";fgcolor="

    #@232
    move-object/from16 v0, v17

    #@234
    invoke-static {v14, v0}, Landroid/content/res/StringBlock;->subtag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@237
    move-result-object v12

    #@238
    .line 238
    if-eqz v12, :cond_260

    #@23a
    .line 239
    const/16 v17, -0x1

    #@23c
    move/from16 v0, v17

    #@23e
    invoke-static {v12, v0}, Lcom/android/internal/util/XmlUtils;->convertValueToUnsignedInt(Ljava/lang/String;I)I

    #@241
    move-result v5

    #@242
    .line 240
    .local v5, color:I
    new-instance v17, Landroid/text/style/ForegroundColorSpan;

    #@244
    move-object/from16 v0, v17

    #@246
    invoke-direct {v0, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    #@249
    add-int/lit8 v18, v7, 0x1

    #@24b
    aget v18, p2, v18

    #@24d
    add-int/lit8 v19, v7, 0x2

    #@24f
    aget v19, p2, v19

    #@251
    add-int/lit8 v19, v19, 0x1

    #@253
    const/16 v20, 0x21

    #@255
    move-object/from16 v0, v17

    #@257
    move/from16 v1, v18

    #@259
    move/from16 v2, v19

    #@25b
    move/from16 v3, v20

    #@25d
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@260
    .line 245
    .end local v5           #color:I
    :cond_260
    const-string v17, ";bgcolor="

    #@262
    move-object/from16 v0, v17

    #@264
    invoke-static {v14, v0}, Landroid/content/res/StringBlock;->subtag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@267
    move-result-object v12

    #@268
    .line 246
    if-eqz v12, :cond_41

    #@26a
    .line 247
    const/16 v17, -0x1

    #@26c
    move/from16 v0, v17

    #@26e
    invoke-static {v12, v0}, Lcom/android/internal/util/XmlUtils;->convertValueToUnsignedInt(Ljava/lang/String;I)I

    #@271
    move-result v5

    #@272
    .line 248
    .restart local v5       #color:I
    new-instance v17, Landroid/text/style/BackgroundColorSpan;

    #@274
    move-object/from16 v0, v17

    #@276
    invoke-direct {v0, v5}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    #@279
    add-int/lit8 v18, v7, 0x1

    #@27b
    aget v18, p2, v18

    #@27d
    add-int/lit8 v19, v7, 0x2

    #@27f
    aget v19, p2, v19

    #@281
    add-int/lit8 v19, v19, 0x1

    #@283
    const/16 v20, 0x21

    #@285
    move-object/from16 v0, v17

    #@287
    move/from16 v1, v18

    #@289
    move/from16 v2, v19

    #@28b
    move/from16 v3, v20

    #@28d
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@290
    goto/16 :goto_41

    #@292
    .line 252
    .end local v5           #color:I
    .end local v12           #sub:Ljava/lang/String;
    :cond_292
    const-string v17, "a;"

    #@294
    move-object/from16 v0, v17

    #@296
    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@299
    move-result v17

    #@29a
    if-eqz v17, :cond_2c6

    #@29c
    .line 255
    const-string v17, ";href="

    #@29e
    move-object/from16 v0, v17

    #@2a0
    invoke-static {v14, v0}, Landroid/content/res/StringBlock;->subtag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2a3
    move-result-object v12

    #@2a4
    .line 256
    .restart local v12       #sub:Ljava/lang/String;
    if-eqz v12, :cond_41

    #@2a6
    .line 257
    new-instance v17, Landroid/text/style/URLSpan;

    #@2a8
    move-object/from16 v0, v17

    #@2aa
    invoke-direct {v0, v12}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    #@2ad
    add-int/lit8 v18, v7, 0x1

    #@2af
    aget v18, p2, v18

    #@2b1
    add-int/lit8 v19, v7, 0x2

    #@2b3
    aget v19, p2, v19

    #@2b5
    add-int/lit8 v19, v19, 0x1

    #@2b7
    const/16 v20, 0x21

    #@2b9
    move-object/from16 v0, v17

    #@2bb
    move/from16 v1, v18

    #@2bd
    move/from16 v2, v19

    #@2bf
    move/from16 v3, v20

    #@2c1
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@2c4
    goto/16 :goto_41

    #@2c6
    .line 261
    .end local v12           #sub:Ljava/lang/String;
    :cond_2c6
    const-string v17, "annotation;"

    #@2c8
    move-object/from16 v0, v17

    #@2ca
    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2cd
    move-result v17

    #@2ce
    if-eqz v17, :cond_41

    #@2d0
    .line 262
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    #@2d3
    move-result v9

    #@2d4
    .line 265
    .local v9, len:I
    const/16 v17, 0x3b

    #@2d6
    move/from16 v0, v17

    #@2d8
    invoke-virtual {v14, v0}, Ljava/lang/String;->indexOf(I)I

    #@2db
    move-result v13

    #@2dc
    .local v13, t:I
    :goto_2dc
    if-ge v13, v9, :cond_41

    #@2de
    .line 266
    const/16 v17, 0x3d

    #@2e0
    move/from16 v0, v17

    #@2e2
    invoke-virtual {v14, v0, v13}, Ljava/lang/String;->indexOf(II)I

    #@2e5
    move-result v6

    #@2e6
    .line 267
    .local v6, eq:I
    if-ltz v6, :cond_41

    #@2e8
    .line 271
    const/16 v17, 0x3b

    #@2ea
    move/from16 v0, v17

    #@2ec
    invoke-virtual {v14, v0, v6}, Ljava/lang/String;->indexOf(II)I

    #@2ef
    move-result v10

    #@2f0
    .line 272
    .local v10, next:I
    if-gez v10, :cond_2f3

    #@2f2
    .line 273
    move v10, v9

    #@2f3
    .line 276
    :cond_2f3
    add-int/lit8 v17, v13, 0x1

    #@2f5
    move/from16 v0, v17

    #@2f7
    invoke-virtual {v14, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2fa
    move-result-object v8

    #@2fb
    .line 277
    .local v8, key:Ljava/lang/String;
    add-int/lit8 v17, v6, 0x1

    #@2fd
    move/from16 v0, v17

    #@2ff
    invoke-virtual {v14, v0, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@302
    move-result-object v16

    #@303
    .line 279
    .local v16, value:Ljava/lang/String;
    new-instance v17, Landroid/text/Annotation;

    #@305
    move-object/from16 v0, v17

    #@307
    move-object/from16 v1, v16

    #@309
    invoke-direct {v0, v8, v1}, Landroid/text/Annotation;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@30c
    add-int/lit8 v18, v7, 0x1

    #@30e
    aget v18, p2, v18

    #@310
    add-int/lit8 v19, v7, 0x2

    #@312
    aget v19, p2, v19

    #@314
    add-int/lit8 v19, v19, 0x1

    #@316
    const/16 v20, 0x21

    #@318
    move-object/from16 v0, v17

    #@31a
    move/from16 v1, v18

    #@31c
    move/from16 v2, v19

    #@31e
    move/from16 v3, v20

    #@320
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    #@323
    .line 265
    move v13, v10

    #@324
    goto :goto_2dc

    #@325
    .line 288
    .end local v6           #eq:I
    .end local v8           #key:Ljava/lang/String;
    .end local v9           #len:I
    .end local v10           #next:I
    .end local v13           #t:I
    .end local v14           #tag:Ljava/lang/String;
    .end local v15           #type:I
    .end local v16           #value:Ljava/lang/String;
    :cond_325
    new-instance p1, Landroid/text/SpannedString;

    #@327
    .end local p1
    move-object/from16 v0, p1

    #@329
    invoke-direct {v0, v4}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    #@32c
    goto/16 :goto_7
.end method

.method private static final native nativeCreate([BII)I
.end method

.method private static final native nativeDestroy(I)V
.end method

.method private static final native nativeGetSize(I)I
.end method

.method private static final native nativeGetString(II)Ljava/lang/String;
.end method

.method private static final native nativeGetStyle(II)[I
.end method

.method private static subtag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "full"
    .parameter "attribute"

    #@0
    .prologue
    .line 320
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@3
    move-result v1

    #@4
    .line 321
    .local v1, start:I
    if-gez v1, :cond_8

    #@6
    .line 322
    const/4 v2, 0x0

    #@7
    .line 331
    :goto_7
    return-object v2

    #@8
    .line 325
    :cond_8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@b
    move-result v2

    #@c
    add-int/2addr v1, v2

    #@d
    .line 326
    const/16 v2, 0x3b

    #@f
    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->indexOf(II)I

    #@12
    move-result v0

    #@13
    .line 328
    .local v0, end:I
    if-gez v0, :cond_1a

    #@15
    .line 329
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    goto :goto_7

    #@1a
    .line 331
    :cond_1a
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    goto :goto_7
.end method


# virtual methods
.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 142
    iget-boolean v0, p0, Landroid/content/res/StringBlock;->mOwnsNative:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 143
    iget v0, p0, Landroid/content/res/StringBlock;->mNative:I

    #@6
    invoke-static {v0}, Landroid/content/res/StringBlock;->nativeDestroy(I)V

    #@9
    .line 145
    :cond_9
    return-void
.end method

.method public get(I)Ljava/lang/CharSequence;
    .registers 10
    .parameter "idx"

    #@0
    .prologue
    .line 62
    monitor-enter p0

    #@1
    .line 63
    :try_start_1
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStrings:[Ljava/lang/CharSequence;

    #@3
    if-eqz v7, :cond_d

    #@5
    .line 64
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStrings:[Ljava/lang/CharSequence;

    #@7
    aget-object v1, v7, p1

    #@9
    .line 65
    .local v1, res:Ljava/lang/CharSequence;
    if-eqz v1, :cond_35

    #@b
    .line 66
    monitor-exit p0

    #@c
    .line 137
    :goto_c
    return-object v1

    #@d
    .line 68
    .end local v1           #res:Ljava/lang/CharSequence;
    :cond_d
    iget-object v7, p0, Landroid/content/res/StringBlock;->mSparseStrings:Landroid/util/SparseArray;

    #@f
    if-eqz v7, :cond_20

    #@11
    .line 69
    iget-object v7, p0, Landroid/content/res/StringBlock;->mSparseStrings:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v7, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Ljava/lang/CharSequence;

    #@19
    .line 70
    .restart local v1       #res:Ljava/lang/CharSequence;
    if-eqz v1, :cond_35

    #@1b
    .line 71
    monitor-exit p0

    #@1c
    goto :goto_c

    #@1d
    .line 138
    .end local v1           #res:Ljava/lang/CharSequence;
    :catchall_1d
    move-exception v7

    #@1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1 .. :try_end_1f} :catchall_1d

    #@1f
    throw v7

    #@20
    .line 74
    :cond_20
    :try_start_20
    iget v7, p0, Landroid/content/res/StringBlock;->mNative:I

    #@22
    invoke-static {v7}, Landroid/content/res/StringBlock;->nativeGetSize(I)I

    #@25
    move-result v0

    #@26
    .line 75
    .local v0, num:I
    iget-boolean v7, p0, Landroid/content/res/StringBlock;->mUseSparse:Z

    #@28
    if-eqz v7, :cond_b0

    #@2a
    const/16 v7, 0xfa

    #@2c
    if-le v0, v7, :cond_b0

    #@2e
    .line 76
    new-instance v7, Landroid/util/SparseArray;

    #@30
    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    #@33
    iput-object v7, p0, Landroid/content/res/StringBlock;->mSparseStrings:Landroid/util/SparseArray;

    #@35
    .line 81
    .end local v0           #num:I
    :cond_35
    :goto_35
    iget v7, p0, Landroid/content/res/StringBlock;->mNative:I

    #@37
    invoke-static {v7, p1}, Landroid/content/res/StringBlock;->nativeGetString(II)Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    .line 82
    .local v2, str:Ljava/lang/String;
    move-object v1, v2

    #@3c
    .line 83
    .restart local v1       #res:Ljava/lang/CharSequence;
    iget v7, p0, Landroid/content/res/StringBlock;->mNative:I

    #@3e
    invoke-static {v7, p1}, Landroid/content/res/StringBlock;->nativeGetStyle(II)[I

    #@41
    move-result-object v3

    #@42
    .line 86
    .local v3, style:[I
    if-eqz v3, :cond_167

    #@44
    .line 87
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@46
    if-nez v7, :cond_4f

    #@48
    .line 88
    new-instance v7, Landroid/content/res/StringBlock$StyleIDs;

    #@4a
    invoke-direct {v7}, Landroid/content/res/StringBlock$StyleIDs;-><init>()V

    #@4d
    iput-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@4f
    .line 93
    :cond_4f
    const/4 v5, 0x0

    #@50
    .local v5, styleIndex:I
    :goto_50
    array-length v7, v3

    #@51
    if-ge v5, v7, :cond_161

    #@53
    .line 94
    aget v4, v3, v5

    #@55
    .line 96
    .local v4, styleId:I
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@57
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->boldId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$000(Landroid/content/res/StringBlock$StyleIDs;)I

    #@5a
    move-result v7

    #@5b
    if-eq v4, v7, :cond_ad

    #@5d
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@5f
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->italicId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$100(Landroid/content/res/StringBlock$StyleIDs;)I

    #@62
    move-result v7

    #@63
    if-eq v4, v7, :cond_ad

    #@65
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@67
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->underlineId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$200(Landroid/content/res/StringBlock$StyleIDs;)I

    #@6a
    move-result v7

    #@6b
    if-eq v4, v7, :cond_ad

    #@6d
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@6f
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->ttId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$300(Landroid/content/res/StringBlock$StyleIDs;)I

    #@72
    move-result v7

    #@73
    if-eq v4, v7, :cond_ad

    #@75
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@77
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->bigId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$400(Landroid/content/res/StringBlock$StyleIDs;)I

    #@7a
    move-result v7

    #@7b
    if-eq v4, v7, :cond_ad

    #@7d
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@7f
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->smallId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$500(Landroid/content/res/StringBlock$StyleIDs;)I

    #@82
    move-result v7

    #@83
    if-eq v4, v7, :cond_ad

    #@85
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@87
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->subId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$600(Landroid/content/res/StringBlock$StyleIDs;)I

    #@8a
    move-result v7

    #@8b
    if-eq v4, v7, :cond_ad

    #@8d
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@8f
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->supId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$700(Landroid/content/res/StringBlock$StyleIDs;)I

    #@92
    move-result v7

    #@93
    if-eq v4, v7, :cond_ad

    #@95
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@97
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->strikeId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$800(Landroid/content/res/StringBlock$StyleIDs;)I

    #@9a
    move-result v7

    #@9b
    if-eq v4, v7, :cond_ad

    #@9d
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@9f
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->listItemId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$900(Landroid/content/res/StringBlock$StyleIDs;)I

    #@a2
    move-result v7

    #@a3
    if-eq v4, v7, :cond_ad

    #@a5
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@a7
    #getter for: Landroid/content/res/StringBlock$StyleIDs;->marqueeId:I
    invoke-static {v7}, Landroid/content/res/StringBlock$StyleIDs;->access$1000(Landroid/content/res/StringBlock$StyleIDs;)I

    #@aa
    move-result v7

    #@ab
    if-ne v4, v7, :cond_b5

    #@ad
    .line 93
    :cond_ad
    :goto_ad
    add-int/lit8 v5, v5, 0x3

    #@af
    goto :goto_50

    #@b0
    .line 78
    .end local v1           #res:Ljava/lang/CharSequence;
    .end local v2           #str:Ljava/lang/String;
    .end local v3           #style:[I
    .end local v4           #styleId:I
    .end local v5           #styleIndex:I
    .restart local v0       #num:I
    :cond_b0
    new-array v7, v0, [Ljava/lang/CharSequence;

    #@b2
    iput-object v7, p0, Landroid/content/res/StringBlock;->mStrings:[Ljava/lang/CharSequence;

    #@b4
    goto :goto_35

    #@b5
    .line 106
    .end local v0           #num:I
    .restart local v1       #res:Ljava/lang/CharSequence;
    .restart local v2       #str:Ljava/lang/String;
    .restart local v3       #style:[I
    .restart local v4       #styleId:I
    .restart local v5       #styleIndex:I
    :cond_b5
    iget v7, p0, Landroid/content/res/StringBlock;->mNative:I

    #@b7
    invoke-static {v7, v4}, Landroid/content/res/StringBlock;->nativeGetString(II)Ljava/lang/String;

    #@ba
    move-result-object v6

    #@bb
    .line 108
    .local v6, styleTag:Ljava/lang/String;
    const-string v7, "b"

    #@bd
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c0
    move-result v7

    #@c1
    if-eqz v7, :cond_c9

    #@c3
    .line 109
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@c5
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->boldId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$002(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@c8
    goto :goto_ad

    #@c9
    .line 110
    :cond_c9
    const-string v7, "i"

    #@cb
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ce
    move-result v7

    #@cf
    if-eqz v7, :cond_d7

    #@d1
    .line 111
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@d3
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->italicId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$102(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@d6
    goto :goto_ad

    #@d7
    .line 112
    :cond_d7
    const-string/jumbo v7, "u"

    #@da
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dd
    move-result v7

    #@de
    if-eqz v7, :cond_e6

    #@e0
    .line 113
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@e2
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->underlineId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$202(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@e5
    goto :goto_ad

    #@e6
    .line 114
    :cond_e6
    const-string/jumbo v7, "tt"

    #@e9
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ec
    move-result v7

    #@ed
    if-eqz v7, :cond_f5

    #@ef
    .line 115
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@f1
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->ttId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$302(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@f4
    goto :goto_ad

    #@f5
    .line 116
    :cond_f5
    const-string v7, "big"

    #@f7
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fa
    move-result v7

    #@fb
    if-eqz v7, :cond_103

    #@fd
    .line 117
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@ff
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->bigId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$402(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@102
    goto :goto_ad

    #@103
    .line 118
    :cond_103
    const-string/jumbo v7, "small"

    #@106
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@109
    move-result v7

    #@10a
    if-eqz v7, :cond_112

    #@10c
    .line 119
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@10e
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->smallId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$502(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@111
    goto :goto_ad

    #@112
    .line 120
    :cond_112
    const-string/jumbo v7, "sup"

    #@115
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@118
    move-result v7

    #@119
    if-eqz v7, :cond_121

    #@11b
    .line 121
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@11d
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->supId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$702(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@120
    goto :goto_ad

    #@121
    .line 122
    :cond_121
    const-string/jumbo v7, "sub"

    #@124
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@127
    move-result v7

    #@128
    if-eqz v7, :cond_131

    #@12a
    .line 123
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@12c
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->subId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$602(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@12f
    goto/16 :goto_ad

    #@131
    .line 124
    :cond_131
    const-string/jumbo v7, "strike"

    #@134
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@137
    move-result v7

    #@138
    if-eqz v7, :cond_141

    #@13a
    .line 125
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@13c
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->strikeId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$802(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@13f
    goto/16 :goto_ad

    #@141
    .line 126
    :cond_141
    const-string/jumbo v7, "li"

    #@144
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@147
    move-result v7

    #@148
    if-eqz v7, :cond_151

    #@14a
    .line 127
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@14c
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->listItemId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$902(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@14f
    goto/16 :goto_ad

    #@151
    .line 128
    :cond_151
    const-string/jumbo v7, "marquee"

    #@154
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@157
    move-result v7

    #@158
    if-eqz v7, :cond_ad

    #@15a
    .line 129
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@15c
    #setter for: Landroid/content/res/StringBlock$StyleIDs;->marqueeId:I
    invoke-static {v7, v4}, Landroid/content/res/StringBlock$StyleIDs;->access$1002(Landroid/content/res/StringBlock$StyleIDs;I)I

    #@15f
    goto/16 :goto_ad

    #@161
    .line 133
    .end local v4           #styleId:I
    .end local v6           #styleTag:Ljava/lang/String;
    :cond_161
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStyleIDs:Landroid/content/res/StringBlock$StyleIDs;

    #@163
    invoke-direct {p0, v2, v3, v7}, Landroid/content/res/StringBlock;->applyStyles(Ljava/lang/String;[ILandroid/content/res/StringBlock$StyleIDs;)Ljava/lang/CharSequence;

    #@166
    move-result-object v1

    #@167
    .line 135
    .end local v5           #styleIndex:I
    :cond_167
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStrings:[Ljava/lang/CharSequence;

    #@169
    if-eqz v7, :cond_172

    #@16b
    iget-object v7, p0, Landroid/content/res/StringBlock;->mStrings:[Ljava/lang/CharSequence;

    #@16d
    aput-object v1, v7, p1

    #@16f
    .line 137
    :goto_16f
    monitor-exit p0

    #@170
    goto/16 :goto_c

    #@172
    .line 136
    :cond_172
    iget-object v7, p0, Landroid/content/res/StringBlock;->mSparseStrings:Landroid/util/SparseArray;

    #@174
    invoke-virtual {v7, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_177
    .catchall {:try_start_20 .. :try_end_177} :catchall_1d

    #@177
    goto :goto_16f
.end method
