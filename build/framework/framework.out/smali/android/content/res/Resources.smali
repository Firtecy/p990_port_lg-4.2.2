.class public Landroid/content/res/Resources;
.super Ljava/lang/Object;
.source "Resources.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/res/Resources$Theme;,
        Landroid/content/res/Resources$NotFoundException;
    }
.end annotation


# static fields
.field private static final DEBUG_ATTRIBUTES_CACHE:Z = false

.field private static final DEBUG_CONFIG:Z = false

.field private static final DEBUG_LOAD:Z = false

.field private static final ID_OTHER:I = 0x1000004

.field static final TAG:Ljava/lang/String; = "Resources"

.field private static final TRACE_FOR_MISS_PRELOAD:Z

.field private static final TRACE_FOR_PRELOAD:Z

.field private static final mSync:Ljava/lang/Object;

.field static mSystem:Landroid/content/res/Resources;

.field private static sPreloaded:Z

.field private static final sPreloadedColorDrawables:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPreloadedColorStateLists:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Landroid/content/res/ColorStateList;",
            ">;"
        }
    .end annotation
.end field

.field private static sPreloadedDensity:I

.field private static final sPreloadedDrawables:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mAssets:Landroid/content/res/AssetManager;

.field mCachedStyledAttributes:Landroid/content/res/TypedArray;

.field private final mCachedXmlBlockIds:[I

.field private final mCachedXmlBlocks:[Landroid/content/res/XmlBlock;

.field private final mColorDrawableCache:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mColorStateListCache:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/res/ColorStateList;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

.field private final mConfiguration:Landroid/content/res/Configuration;

.field private final mDrawableCache:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;>;"
        }
    .end annotation
.end field

.field private mLastCachedXmlBlockIndex:I

.field mLastRetrievedAttrs:Ljava/lang/RuntimeException;

.field final mMetrics:Landroid/util/DisplayMetrics;

.field private mPluralRule:Llibcore/icu/NativePluralRules;

.field private mPreloading:Z

.field private mThemeIconManager:Landroid/content/thm/ThemeIconManager;

.field final mTmpConfig:Landroid/content/res/Configuration;

.field final mTmpValue:Landroid/util/TypedValue;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 81
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/content/res/Resources;->mSync:Ljava/lang/Object;

    #@7
    .line 82
    const/4 v0, 0x0

    #@8
    sput-object v0, Landroid/content/res/Resources;->mSystem:Landroid/content/res/Resources;

    #@a
    .line 87
    new-instance v0, Landroid/util/LongSparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@f
    sput-object v0, Landroid/content/res/Resources;->sPreloadedDrawables:Landroid/util/LongSparseArray;

    #@11
    .line 89
    new-instance v0, Landroid/util/LongSparseArray;

    #@13
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@16
    sput-object v0, Landroid/content/res/Resources;->sPreloadedColorStateLists:Landroid/util/LongSparseArray;

    #@18
    .line 91
    new-instance v0, Landroid/util/LongSparseArray;

    #@1a
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@1d
    sput-object v0, Landroid/content/res/Resources;->sPreloadedColorDrawables:Landroid/util/LongSparseArray;

    #@1f
    return-void
.end method

.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x4

    #@1
    const/4 v1, 0x0

    #@2
    .line 2273
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 96
    new-instance v0, Landroid/util/TypedValue;

    #@7
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@a
    iput-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@c
    .line 97
    new-instance v0, Landroid/content/res/Configuration;

    #@e
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@11
    iput-object v0, p0, Landroid/content/res/Resources;->mTmpConfig:Landroid/content/res/Configuration;

    #@13
    .line 100
    new-instance v0, Landroid/util/LongSparseArray;

    #@15
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@18
    iput-object v0, p0, Landroid/content/res/Resources;->mDrawableCache:Landroid/util/LongSparseArray;

    #@1a
    .line 102
    new-instance v0, Landroid/util/LongSparseArray;

    #@1c
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@1f
    iput-object v0, p0, Landroid/content/res/Resources;->mColorStateListCache:Landroid/util/LongSparseArray;

    #@21
    .line 104
    new-instance v0, Landroid/util/LongSparseArray;

    #@23
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@26
    iput-object v0, p0, Landroid/content/res/Resources;->mColorDrawableCache:Landroid/util/LongSparseArray;

    #@28
    .line 108
    iput-object v1, p0, Landroid/content/res/Resources;->mCachedStyledAttributes:Landroid/content/res/TypedArray;

    #@2a
    .line 109
    iput-object v1, p0, Landroid/content/res/Resources;->mLastRetrievedAttrs:Ljava/lang/RuntimeException;

    #@2c
    .line 111
    const/4 v0, -0x1

    #@2d
    iput v0, p0, Landroid/content/res/Resources;->mLastCachedXmlBlockIndex:I

    #@2f
    .line 112
    new-array v0, v2, [I

    #@31
    fill-array-data v0, :array_66

    #@34
    iput-object v0, p0, Landroid/content/res/Resources;->mCachedXmlBlockIds:[I

    #@36
    .line 113
    new-array v0, v2, [Landroid/content/res/XmlBlock;

    #@38
    iput-object v0, p0, Landroid/content/res/Resources;->mCachedXmlBlocks:[Landroid/content/res/XmlBlock;

    #@3a
    .line 116
    new-instance v0, Landroid/content/res/Configuration;

    #@3c
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@3f
    iput-object v0, p0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@41
    .line 117
    new-instance v0, Landroid/util/DisplayMetrics;

    #@43
    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    #@46
    iput-object v0, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@48
    .line 2274
    invoke-static {}, Landroid/content/res/AssetManager;->getSystem()Landroid/content/res/AssetManager;

    #@4b
    move-result-object v0

    #@4c
    iput-object v0, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@4e
    .line 2278
    iget-object v0, p0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@50
    invoke-virtual {v0}, Landroid/content/res/Configuration;->setToDefaults()V

    #@53
    .line 2279
    iget-object v0, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@55
    invoke-virtual {v0}, Landroid/util/DisplayMetrics;->setToDefaults()V

    #@58
    .line 2280
    invoke-virtual {p0, v1, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    #@5b
    .line 2281
    iget-object v0, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@5d
    invoke-virtual {v0}, Landroid/content/res/AssetManager;->ensureStringBlocks()V

    #@60
    .line 2282
    sget-object v0, Landroid/content/res/CompatibilityInfo;->DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo;

    #@62
    iput-object v0, p0, Landroid/content/res/Resources;->mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@64
    .line 2283
    return-void

    #@65
    .line 112
    nop

    #@66
    :array_66
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V
    .registers 5
    .parameter "assets"
    .parameter "metrics"
    .parameter "config"

    #@0
    .prologue
    .line 169
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V

    #@4
    .line 170
    return-void
.end method

.method public constructor <init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V
    .registers 8
    .parameter "assets"
    .parameter "metrics"
    .parameter "config"
    .parameter "compInfo"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x4

    #@2
    .line 185
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 96
    new-instance v0, Landroid/util/TypedValue;

    #@7
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@a
    iput-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@c
    .line 97
    new-instance v0, Landroid/content/res/Configuration;

    #@e
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@11
    iput-object v0, p0, Landroid/content/res/Resources;->mTmpConfig:Landroid/content/res/Configuration;

    #@13
    .line 100
    new-instance v0, Landroid/util/LongSparseArray;

    #@15
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@18
    iput-object v0, p0, Landroid/content/res/Resources;->mDrawableCache:Landroid/util/LongSparseArray;

    #@1a
    .line 102
    new-instance v0, Landroid/util/LongSparseArray;

    #@1c
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@1f
    iput-object v0, p0, Landroid/content/res/Resources;->mColorStateListCache:Landroid/util/LongSparseArray;

    #@21
    .line 104
    new-instance v0, Landroid/util/LongSparseArray;

    #@23
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@26
    iput-object v0, p0, Landroid/content/res/Resources;->mColorDrawableCache:Landroid/util/LongSparseArray;

    #@28
    .line 108
    iput-object v2, p0, Landroid/content/res/Resources;->mCachedStyledAttributes:Landroid/content/res/TypedArray;

    #@2a
    .line 109
    iput-object v2, p0, Landroid/content/res/Resources;->mLastRetrievedAttrs:Ljava/lang/RuntimeException;

    #@2c
    .line 111
    const/4 v0, -0x1

    #@2d
    iput v0, p0, Landroid/content/res/Resources;->mLastCachedXmlBlockIndex:I

    #@2f
    .line 112
    new-array v0, v1, [I

    #@31
    fill-array-data v0, :array_58

    #@34
    iput-object v0, p0, Landroid/content/res/Resources;->mCachedXmlBlockIds:[I

    #@36
    .line 113
    new-array v0, v1, [Landroid/content/res/XmlBlock;

    #@38
    iput-object v0, p0, Landroid/content/res/Resources;->mCachedXmlBlocks:[Landroid/content/res/XmlBlock;

    #@3a
    .line 116
    new-instance v0, Landroid/content/res/Configuration;

    #@3c
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@3f
    iput-object v0, p0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@41
    .line 117
    new-instance v0, Landroid/util/DisplayMetrics;

    #@43
    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    #@46
    iput-object v0, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@48
    .line 186
    iput-object p1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@4a
    .line 187
    iget-object v0, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@4c
    invoke-virtual {v0}, Landroid/util/DisplayMetrics;->setToDefaults()V

    #@4f
    .line 188
    iput-object p4, p0, Landroid/content/res/Resources;->mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@51
    .line 189
    invoke-virtual {p0, p3, p2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    #@54
    .line 190
    invoke-virtual {p1}, Landroid/content/res/AssetManager;->ensureStringBlocks()V

    #@57
    .line 191
    return-void

    #@58
    .line 112
    :array_58
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method static synthetic access$000(Landroid/content/res/Resources;I)Landroid/content/res/TypedArray;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/content/res/Resources;->getCachedStyledAttributes(I)Landroid/content/res/TypedArray;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static attrForQuantityCode(I)I
    .registers 2
    .parameter "quantityCode"

    #@0
    .prologue
    .line 279
    packed-switch p0, :pswitch_data_1c

    #@3
    .line 285
    const v0, 0x1000004

    #@6
    :goto_6
    return v0

    #@7
    .line 280
    :pswitch_7
    const v0, 0x1000005

    #@a
    goto :goto_6

    #@b
    .line 281
    :pswitch_b
    const v0, 0x1000006

    #@e
    goto :goto_6

    #@f
    .line 282
    :pswitch_f
    const v0, 0x1000007

    #@12
    goto :goto_6

    #@13
    .line 283
    :pswitch_13
    const v0, 0x1000008

    #@16
    goto :goto_6

    #@17
    .line 284
    :pswitch_17
    const v0, 0x1000009

    #@1a
    goto :goto_6

    #@1b
    .line 279
    nop

    #@1c
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_7
        :pswitch_b
        :pswitch_f
        :pswitch_13
        :pswitch_17
    .end packed-switch
.end method

.method private clearDrawableCache(Landroid/util/LongSparseArray;I)V
    .registers 8
    .parameter
    .parameter "configChanges"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;>;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1556
    .local p1, cache:Landroid/util/LongSparseArray;,"Landroid/util/LongSparseArray<Ljava/lang/ref/WeakReference<Landroid/graphics/drawable/Drawable$ConstantState;>;>;"
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    #@3
    move-result v0

    #@4
    .line 1561
    .local v0, N:I
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v0, :cond_28

    #@7
    .line 1562
    invoke-virtual {p1, v2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    #@a
    move-result-object v3

    #@b
    check-cast v3, Ljava/lang/ref/WeakReference;

    #@d
    .line 1563
    .local v3, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/graphics/drawable/Drawable$ConstantState;>;"
    if-eqz v3, :cond_25

    #@f
    .line 1564
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/graphics/drawable/Drawable$ConstantState;

    #@15
    .line 1565
    .local v1, cs:Landroid/graphics/drawable/Drawable$ConstantState;
    if-eqz v1, :cond_25

    #@17
    .line 1566
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->getChangingConfigurations()I

    #@1a
    move-result v4

    #@1b
    invoke-static {p2, v4}, Landroid/content/res/Configuration;->needNewResources(II)Z

    #@1e
    move-result v4

    #@1f
    if-eqz v4, :cond_25

    #@21
    .line 1574
    const/4 v4, 0x0

    #@22
    invoke-virtual {p1, v2, v4}, Landroid/util/LongSparseArray;->setValueAt(ILjava/lang/Object;)V

    #@25
    .line 1561
    .end local v1           #cs:Landroid/graphics/drawable/Drawable$ConstantState;
    :cond_25
    add-int/lit8 v2, v2, 0x1

    #@27
    goto :goto_5

    #@28
    .line 1585
    .end local v3           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/graphics/drawable/Drawable$ConstantState;>;"
    :cond_28
    return-void
.end method

.method private getCachedColorStateList(J)Landroid/content/res/ColorStateList;
    .registers 7
    .parameter "key"

    #@0
    .prologue
    .line 2158
    iget-object v3, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v3

    #@3
    .line 2159
    :try_start_3
    iget-object v2, p0, Landroid/content/res/Resources;->mColorStateListCache:Landroid/util/LongSparseArray;

    #@5
    invoke-virtual {v2, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Ljava/lang/ref/WeakReference;

    #@b
    .line 2160
    .local v1, wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/ColorStateList;>;"
    if-eqz v1, :cond_1c

    #@d
    .line 2161
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/content/res/ColorStateList;

    #@13
    .line 2162
    .local v0, entry:Landroid/content/res/ColorStateList;
    if-eqz v0, :cond_17

    #@15
    .line 2166
    monitor-exit v3

    #@16
    .line 2172
    .end local v0           #entry:Landroid/content/res/ColorStateList;
    :goto_16
    return-object v0

    #@17
    .line 2168
    .restart local v0       #entry:Landroid/content/res/ColorStateList;
    :cond_17
    iget-object v2, p0, Landroid/content/res/Resources;->mColorStateListCache:Landroid/util/LongSparseArray;

    #@19
    invoke-virtual {v2, p1, p2}, Landroid/util/LongSparseArray;->delete(J)V

    #@1c
    .line 2171
    .end local v0           #entry:Landroid/content/res/ColorStateList;
    :cond_1c
    monitor-exit v3

    #@1d
    .line 2172
    const/4 v0, 0x0

    #@1e
    goto :goto_16

    #@1f
    .line 2171
    .end local v1           #wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/ColorStateList;>;"
    :catchall_1f
    move-exception v2

    #@20
    monitor-exit v3
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v2
.end method

.method private getCachedDrawable(Landroid/util/LongSparseArray;J)Landroid/graphics/drawable/Drawable;
    .registers 8
    .parameter
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;>;J)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    #@0
    .prologue
    .line 2053
    .local p1, drawableCache:Landroid/util/LongSparseArray;,"Landroid/util/LongSparseArray<Ljava/lang/ref/WeakReference<Landroid/graphics/drawable/Drawable$ConstantState;>;>;"
    iget-object v3, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v3

    #@3
    .line 2054
    :try_start_3
    invoke-virtual {p1, p2, p3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Ljava/lang/ref/WeakReference;

    #@9
    .line 2055
    .local v1, wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/graphics/drawable/Drawable$ConstantState;>;"
    if-eqz v1, :cond_1c

    #@b
    .line 2056
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/graphics/drawable/Drawable$ConstantState;

    #@11
    .line 2057
    .local v0, entry:Landroid/graphics/drawable/Drawable$ConstantState;
    if-eqz v0, :cond_19

    #@13
    .line 2061
    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    #@16
    move-result-object v2

    #@17
    monitor-exit v3

    #@18
    .line 2068
    .end local v0           #entry:Landroid/graphics/drawable/Drawable$ConstantState;
    :goto_18
    return-object v2

    #@19
    .line 2064
    .restart local v0       #entry:Landroid/graphics/drawable/Drawable$ConstantState;
    :cond_19
    invoke-virtual {p1, p2, p3}, Landroid/util/LongSparseArray;->delete(J)V

    #@1c
    .line 2067
    .end local v0           #entry:Landroid/graphics/drawable/Drawable$ConstantState;
    :cond_1c
    monitor-exit v3

    #@1d
    .line 2068
    const/4 v2, 0x0

    #@1e
    goto :goto_18

    #@1f
    .line 2067
    .end local v1           #wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/graphics/drawable/Drawable$ConstantState;>;"
    :catchall_1f
    move-exception v2

    #@20
    monitor-exit v3
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v2
.end method

.method private getCachedStyledAttributes(I)Landroid/content/res/TypedArray;
    .registers 7
    .parameter "len"

    #@0
    .prologue
    .line 2240
    iget-object v3, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v3

    #@3
    .line 2241
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mCachedStyledAttributes:Landroid/content/res/TypedArray;

    #@5
    .line 2242
    .local v0, attrs:Landroid/content/res/TypedArray;
    if-eqz v0, :cond_24

    #@7
    .line 2243
    const/4 v2, 0x0

    #@8
    iput-object v2, p0, Landroid/content/res/Resources;->mCachedStyledAttributes:Landroid/content/res/TypedArray;

    #@a
    .line 2249
    iput p1, v0, Landroid/content/res/TypedArray;->mLength:I

    #@c
    .line 2250
    mul-int/lit8 v1, p1, 0x6

    #@e
    .line 2251
    .local v1, fullLen:I
    iget-object v2, v0, Landroid/content/res/TypedArray;->mData:[I

    #@10
    array-length v2, v2

    #@11
    if-lt v2, v1, :cond_15

    #@13
    .line 2252
    monitor-exit v3

    #@14
    .line 2267
    .end local v0           #attrs:Landroid/content/res/TypedArray;
    .end local v1           #fullLen:I
    :goto_14
    return-object v0

    #@15
    .line 2254
    .restart local v0       #attrs:Landroid/content/res/TypedArray;
    .restart local v1       #fullLen:I
    :cond_15
    new-array v2, v1, [I

    #@17
    iput-object v2, v0, Landroid/content/res/TypedArray;->mData:[I

    #@19
    .line 2255
    add-int/lit8 v2, p1, 0x1

    #@1b
    new-array v2, v2, [I

    #@1d
    iput-object v2, v0, Landroid/content/res/TypedArray;->mIndices:[I

    #@1f
    .line 2256
    monitor-exit v3

    #@20
    goto :goto_14

    #@21
    .line 2270
    .end local v0           #attrs:Landroid/content/res/TypedArray;
    .end local v1           #fullLen:I
    :catchall_21
    move-exception v2

    #@22
    monitor-exit v3
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v2

    #@24
    .line 2267
    .restart local v0       #attrs:Landroid/content/res/TypedArray;
    :cond_24
    :try_start_24
    new-instance v0, Landroid/content/res/TypedArray;

    #@26
    .end local v0           #attrs:Landroid/content/res/TypedArray;
    mul-int/lit8 v2, p1, 0x6

    #@28
    new-array v2, v2, [I

    #@2a
    add-int/lit8 v4, p1, 0x1

    #@2c
    new-array v4, v4, [I

    #@2e
    invoke-direct {v0, p0, v2, v4, p1}, Landroid/content/res/TypedArray;-><init>(Landroid/content/res/Resources;[I[II)V

    #@31
    monitor-exit v3
    :try_end_32
    .catchall {:try_start_24 .. :try_end_32} :catchall_21

    #@32
    goto :goto_14
.end method

.method private getPluralRule()Llibcore/icu/NativePluralRules;
    .registers 3

    #@0
    .prologue
    .line 270
    sget-object v1, Landroid/content/res/Resources;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 271
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mPluralRule:Llibcore/icu/NativePluralRules;

    #@5
    if-nez v0, :cond_11

    #@7
    .line 272
    iget-object v0, p0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@9
    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@b
    invoke-static {v0}, Llibcore/icu/NativePluralRules;->forLocale(Ljava/util/Locale;)Llibcore/icu/NativePluralRules;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/content/res/Resources;->mPluralRule:Llibcore/icu/NativePluralRules;

    #@11
    .line 274
    :cond_11
    iget-object v0, p0, Landroid/content/res/Resources;->mPluralRule:Llibcore/icu/NativePluralRules;

    #@13
    monitor-exit v1

    #@14
    return-object v0

    #@15
    .line 275
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method public static getSystem()Landroid/content/res/Resources;
    .registers 3

    #@0
    .prologue
    .line 200
    sget-object v2, Landroid/content/res/Resources;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 201
    :try_start_3
    sget-object v0, Landroid/content/res/Resources;->mSystem:Landroid/content/res/Resources;

    #@5
    .line 202
    .local v0, ret:Landroid/content/res/Resources;
    if-nez v0, :cond_e

    #@7
    .line 203
    new-instance v0, Landroid/content/res/Resources;

    #@9
    .end local v0           #ret:Landroid/content/res/Resources;
    invoke-direct {v0}, Landroid/content/res/Resources;-><init>()V

    #@c
    .line 204
    .restart local v0       #ret:Landroid/content/res/Resources;
    sput-object v0, Landroid/content/res/Resources;->mSystem:Landroid/content/res/Resources;

    #@e
    .line 207
    :cond_e
    monitor-exit v2

    #@f
    return-object v0

    #@10
    .line 208
    :catchall_10
    move-exception v1

    #@11
    monitor-exit v2
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v1
.end method

.method public static selectDefaultTheme(II)I
    .registers 5
    .parameter "curTheme"
    .parameter "targetSdkVersion"

    #@0
    .prologue
    .line 124
    const v0, 0x1030005

    #@3
    const v1, 0x103006b

    #@6
    const v2, 0x1030128

    #@9
    invoke-static {p0, p1, v0, v1, v2}, Landroid/content/res/Resources;->selectSystemTheme(IIIII)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public static selectSystemTheme(IIIII)I
    .registers 6
    .parameter "curTheme"
    .parameter "targetSdkVersion"
    .parameter "orig"
    .parameter "holo"
    .parameter "deviceDefault"

    #@0
    .prologue
    .line 133
    if-eqz p0, :cond_3

    #@2
    .line 142
    .end local p0
    :goto_2
    return p0

    #@3
    .line 136
    .restart local p0
    :cond_3
    const/16 v0, 0xb

    #@5
    if-ge p1, v0, :cond_9

    #@7
    move p0, p2

    #@8
    .line 137
    goto :goto_2

    #@9
    .line 139
    :cond_9
    const/16 v0, 0xe

    #@b
    if-ge p1, v0, :cond_f

    #@d
    move p0, p3

    #@e
    .line 140
    goto :goto_2

    #@f
    :cond_f
    move p0, p4

    #@10
    .line 142
    goto :goto_2
.end method

.method private static stringForQuantityCode(I)Ljava/lang/String;
    .registers 2
    .parameter "quantityCode"

    #@0
    .prologue
    .line 290
    packed-switch p0, :pswitch_data_1a

    #@3
    .line 296
    const-string/jumbo v0, "other"

    #@6
    :goto_6
    return-object v0

    #@7
    .line 291
    :pswitch_7
    const-string/jumbo v0, "zero"

    #@a
    goto :goto_6

    #@b
    .line 292
    :pswitch_b
    const-string/jumbo v0, "one"

    #@e
    goto :goto_6

    #@f
    .line 293
    :pswitch_f
    const-string/jumbo v0, "two"

    #@12
    goto :goto_6

    #@13
    .line 294
    :pswitch_13
    const-string v0, "few"

    #@15
    goto :goto_6

    #@16
    .line 295
    :pswitch_16
    const-string/jumbo v0, "many"

    #@19
    goto :goto_6

    #@1a
    .line 290
    :pswitch_data_1a
    .packed-switch 0x0
        :pswitch_7
        :pswitch_b
        :pswitch_f
        :pswitch_13
        :pswitch_16
    .end packed-switch
.end method

.method public static updateSystemConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V
    .registers 3
    .parameter "config"
    .parameter "metrics"

    #@0
    .prologue
    .line 1606
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/content/res/Resources;->updateSystemConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@4
    .line 1607
    return-void
.end method

.method public static updateSystemConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V
    .registers 4
    .parameter "config"
    .parameter "metrics"
    .parameter "compat"

    #@0
    .prologue
    .line 1595
    sget-object v0, Landroid/content/res/Resources;->mSystem:Landroid/content/res/Resources;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1596
    sget-object v0, Landroid/content/res/Resources;->mSystem:Landroid/content/res/Resources;

    #@6
    invoke-virtual {v0, p0, p1, p2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@9
    .line 1600
    :cond_9
    return-void
.end method

.method private verifyPreloadConfig(Landroid/util/TypedValue;Ljava/lang/String;)Z
    .registers 8
    .parameter "value"
    .parameter "name"

    #@0
    .prologue
    .line 1912
    iget v2, p1, Landroid/util/TypedValue;->changingConfigurations:I

    #@2
    const v3, -0x40001101

    #@5
    and-int/2addr v2, v3

    #@6
    if-eqz v2, :cond_58

    #@8
    .line 1916
    :try_start_8
    iget v2, p1, Landroid/util/TypedValue;->resourceId:I

    #@a
    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;
    :try_end_d
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_8 .. :try_end_d} :catch_54

    #@d
    move-result-object v1

    #@e
    .line 1920
    .local v1, resName:Ljava/lang/String;
    :goto_e
    const-string v2, "Resources"

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "Preloaded "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, " resource #0x"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    iget v4, p1, Landroid/util/TypedValue;->resourceId:I

    #@27
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v4, " ("

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    const-string v4, "/"

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    iget v4, p1, Landroid/util/TypedValue;->changingConfigurations:I

    #@41
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    const-string v4, ") that varies with configuration!!"

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1923
    const/4 v2, 0x0

    #@53
    .line 1925
    .end local v1           #resName:Ljava/lang/String;
    :goto_53
    return v2

    #@54
    .line 1917
    :catch_54
    move-exception v0

    #@55
    .line 1918
    .local v0, e:Landroid/content/res/Resources$NotFoundException;
    const-string v1, "?"

    #@57
    .restart local v1       #resName:Ljava/lang/String;
    goto :goto_e

    #@58
    .line 1925
    .end local v0           #e:Landroid/content/res/Resources$NotFoundException;
    .end local v1           #resName:Ljava/lang/String;
    :cond_58
    const/4 v2, 0x1

    #@59
    goto :goto_53
.end method


# virtual methods
.method public final finishPreloading()V
    .registers 2

    #@0
    .prologue
    .line 1905
    iget-boolean v0, p0, Landroid/content/res/Resources;->mPreloading:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 1906
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/content/res/Resources;->mPreloading:Z

    #@7
    .line 1907
    invoke-virtual {p0}, Landroid/content/res/Resources;->flushLayoutCache()V

    #@a
    .line 1909
    :cond_a
    return-void
.end method

.method public final flushLayoutCache()V
    .registers 7

    #@0
    .prologue
    .line 1868
    iget-object v4, p0, Landroid/content/res/Resources;->mCachedXmlBlockIds:[I

    #@2
    monitor-enter v4

    #@3
    .line 1870
    :try_start_3
    iget-object v3, p0, Landroid/content/res/Resources;->mCachedXmlBlockIds:[I

    #@5
    array-length v1, v3

    #@6
    .line 1871
    .local v1, num:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_1f

    #@9
    .line 1872
    iget-object v3, p0, Landroid/content/res/Resources;->mCachedXmlBlockIds:[I

    #@b
    const/4 v5, 0x0

    #@c
    aput v5, v3, v0

    #@e
    .line 1873
    iget-object v3, p0, Landroid/content/res/Resources;->mCachedXmlBlocks:[Landroid/content/res/XmlBlock;

    #@10
    aget-object v2, v3, v0

    #@12
    .line 1874
    .local v2, oldBlock:Landroid/content/res/XmlBlock;
    if-eqz v2, :cond_17

    #@14
    .line 1875
    invoke-virtual {v2}, Landroid/content/res/XmlBlock;->close()V

    #@17
    .line 1877
    :cond_17
    iget-object v3, p0, Landroid/content/res/Resources;->mCachedXmlBlocks:[Landroid/content/res/XmlBlock;

    #@19
    const/4 v5, 0x0

    #@1a
    aput-object v5, v3, v0

    #@1c
    .line 1871
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_7

    #@1f
    .line 1879
    .end local v2           #oldBlock:Landroid/content/res/XmlBlock;
    :cond_1f
    monitor-exit v4

    #@20
    .line 1880
    return-void

    #@21
    .line 1879
    .end local v0           #i:I
    .end local v1           #num:I
    :catchall_21
    move-exception v3

    #@22
    monitor-exit v4
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v3
.end method

.method public getAnimation(I)Landroid/content/res/XmlResourceParser;
    .registers 3
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 916
    const-string v0, "anim"

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/content/res/Resources;->loadXmlResourceParser(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final getAssets()Landroid/content/res/AssetManager;
    .registers 2

    #@0
    .prologue
    .line 1859
    iget-object v0, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    return-object v0
.end method

.method public getBoolean(I)Z
    .registers 7
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 833
    iget-object v2, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@3
    monitor-enter v2

    #@4
    .line 834
    :try_start_4
    iget-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@6
    .line 835
    .local v0, value:Landroid/util/TypedValue;
    const/4 v3, 0x1

    #@7
    invoke-virtual {p0, p1, v0, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@a
    .line 836
    iget v3, v0, Landroid/util/TypedValue;->type:I

    #@c
    const/16 v4, 0x10

    #@e
    if-lt v3, v4, :cond_1e

    #@10
    iget v3, v0, Landroid/util/TypedValue;->type:I

    #@12
    const/16 v4, 0x1f

    #@14
    if-gt v3, v4, :cond_1e

    #@16
    .line 838
    iget v3, v0, Landroid/util/TypedValue;->data:I

    #@18
    if-eqz v3, :cond_1c

    #@1a
    :goto_1a
    monitor-exit v2

    #@1b
    return v1

    #@1c
    :cond_1c
    const/4 v1, 0x0

    #@1d
    goto :goto_1a

    #@1e
    .line 840
    :cond_1e
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "Resource ID #0x"

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, " type #0x"

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    iget v4, v0, Landroid/util/TypedValue;->type:I

    #@3b
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    const-string v4, " is not valid"

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-direct {v1, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@50
    throw v1

    #@51
    .line 843
    .end local v0           #value:Landroid/util/TypedValue;
    :catchall_51
    move-exception v1

    #@52
    monitor-exit v2
    :try_end_53
    .catchall {:try_start_4 .. :try_end_53} :catchall_51

    #@53
    throw v1
.end method

.method public getColor(I)I
    .registers 8
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 781
    iget-object v3, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v3

    #@3
    .line 782
    :try_start_3
    iget-object v1, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    .line 783
    .local v1, value:Landroid/util/TypedValue;
    const/4 v2, 0x1

    #@6
    invoke-virtual {p0, p1, v1, v2}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@9
    .line 784
    iget v2, v1, Landroid/util/TypedValue;->type:I

    #@b
    const/16 v4, 0x10

    #@d
    if-lt v2, v4, :cond_19

    #@f
    iget v2, v1, Landroid/util/TypedValue;->type:I

    #@11
    const/16 v4, 0x1f

    #@13
    if-gt v2, v4, :cond_19

    #@15
    .line 786
    iget v2, v1, Landroid/util/TypedValue;->data:I

    #@17
    monitor-exit v3

    #@18
    .line 789
    :goto_18
    return v2

    #@19
    .line 787
    :cond_19
    iget v2, v1, Landroid/util/TypedValue;->type:I

    #@1b
    const/4 v4, 0x3

    #@1c
    if-ne v2, v4, :cond_2d

    #@1e
    .line 788
    iget-object v2, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@20
    invoke-virtual {p0, v2, p1}, Landroid/content/res/Resources;->loadColorStateList(Landroid/util/TypedValue;I)Landroid/content/res/ColorStateList;

    #@23
    move-result-object v0

    #@24
    .line 789
    .local v0, csl:Landroid/content/res/ColorStateList;
    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    #@27
    move-result v2

    #@28
    monitor-exit v3

    #@29
    goto :goto_18

    #@2a
    .line 794
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    .end local v1           #value:Landroid/util/TypedValue;
    :catchall_2a
    move-exception v2

    #@2b
    monitor-exit v3
    :try_end_2c
    .catchall {:try_start_3 .. :try_end_2c} :catchall_2a

    #@2c
    throw v2

    #@2d
    .line 791
    .restart local v1       #value:Landroid/util/TypedValue;
    :cond_2d
    :try_start_2d
    new-instance v2, Landroid/content/res/Resources$NotFoundException;

    #@2f
    new-instance v4, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v5, "Resource ID #0x"

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    const-string v5, " type #0x"

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    iget v5, v1, Landroid/util/TypedValue;->type:I

    #@4a
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    const-string v5, " is not valid"

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v4

    #@5c
    invoke-direct {v2, v4}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v2
    :try_end_60
    .catchall {:try_start_2d .. :try_end_60} :catchall_2a
.end method

.method public getColorStateList(I)Landroid/content/res/ColorStateList;
    .registers 5
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 812
    iget-object v2, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v2

    #@3
    .line 813
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    .line 814
    .local v0, value:Landroid/util/TypedValue;
    const/4 v1, 0x1

    #@6
    invoke-virtual {p0, p1, v0, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@9
    .line 815
    invoke-virtual {p0, v0, p1}, Landroid/content/res/Resources;->loadColorStateList(Landroid/util/TypedValue;I)Landroid/content/res/ColorStateList;

    #@c
    move-result-object v1

    #@d
    monitor-exit v2

    #@e
    return-object v1

    #@f
    .line 816
    .end local v0           #value:Landroid/util/TypedValue;
    :catchall_f
    move-exception v1

    #@10
    monitor-exit v2
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v1
.end method

.method public getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;
    .registers 2

    #@0
    .prologue
    .line 1639
    iget-object v0, p0, Landroid/content/res/Resources;->mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@2
    if-eqz v0, :cond_7

    #@4
    iget-object v0, p0, Landroid/content/res/Resources;->mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    sget-object v0, Landroid/content/res/CompatibilityInfo;->DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo;

    #@9
    goto :goto_6
.end method

.method public getConfiguration()Landroid/content/res/Configuration;
    .registers 2

    #@0
    .prologue
    .line 1628
    iget-object v0, p0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@2
    return-object v0
.end method

.method public getDimension(I)F
    .registers 7
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 521
    iget-object v2, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v2

    #@3
    .line 522
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    .line 523
    .local v0, value:Landroid/util/TypedValue;
    const/4 v1, 0x1

    #@6
    invoke-virtual {p0, p1, v0, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@9
    .line 524
    iget v1, v0, Landroid/util/TypedValue;->type:I

    #@b
    const/4 v3, 0x5

    #@c
    if-ne v1, v3, :cond_18

    #@e
    .line 525
    iget v1, v0, Landroid/util/TypedValue;->data:I

    #@10
    iget-object v3, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@12
    invoke-static {v1, v3}, Landroid/util/TypedValue;->complexToDimension(ILandroid/util/DisplayMetrics;)F

    #@15
    move-result v1

    #@16
    monitor-exit v2

    #@17
    return v1

    #@18
    .line 527
    :cond_18
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@1a
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "Resource ID #0x"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, " type #0x"

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    iget v4, v0, Landroid/util/TypedValue;->type:I

    #@35
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, " is not valid"

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-direct {v1, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v1

    #@4b
    .line 530
    .end local v0           #value:Landroid/util/TypedValue;
    :catchall_4b
    move-exception v1

    #@4c
    monitor-exit v2
    :try_end_4d
    .catchall {:try_start_3 .. :try_end_4d} :catchall_4b

    #@4d
    throw v1
.end method

.method public getDimensionPixelOffset(I)I
    .registers 7
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 553
    iget-object v2, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v2

    #@3
    .line 554
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    .line 555
    .local v0, value:Landroid/util/TypedValue;
    const/4 v1, 0x1

    #@6
    invoke-virtual {p0, p1, v0, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@9
    .line 556
    iget v1, v0, Landroid/util/TypedValue;->type:I

    #@b
    const/4 v3, 0x5

    #@c
    if-ne v1, v3, :cond_18

    #@e
    .line 557
    iget v1, v0, Landroid/util/TypedValue;->data:I

    #@10
    iget-object v3, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@12
    invoke-static {v1, v3}, Landroid/util/TypedValue;->complexToDimensionPixelOffset(ILandroid/util/DisplayMetrics;)I

    #@15
    move-result v1

    #@16
    monitor-exit v2

    #@17
    return v1

    #@18
    .line 560
    :cond_18
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@1a
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "Resource ID #0x"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, " type #0x"

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    iget v4, v0, Landroid/util/TypedValue;->type:I

    #@35
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, " is not valid"

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-direct {v1, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v1

    #@4b
    .line 563
    .end local v0           #value:Landroid/util/TypedValue;
    :catchall_4b
    move-exception v1

    #@4c
    monitor-exit v2
    :try_end_4d
    .catchall {:try_start_3 .. :try_end_4d} :catchall_4b

    #@4d
    throw v1
.end method

.method public getDimensionPixelSize(I)I
    .registers 7
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 587
    iget-object v2, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v2

    #@3
    .line 588
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    .line 589
    .local v0, value:Landroid/util/TypedValue;
    const/4 v1, 0x1

    #@6
    invoke-virtual {p0, p1, v0, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@9
    .line 590
    iget v1, v0, Landroid/util/TypedValue;->type:I

    #@b
    const/4 v3, 0x5

    #@c
    if-ne v1, v3, :cond_18

    #@e
    .line 591
    iget v1, v0, Landroid/util/TypedValue;->data:I

    #@10
    iget-object v3, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@12
    invoke-static {v1, v3}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    #@15
    move-result v1

    #@16
    monitor-exit v2

    #@17
    return v1

    #@18
    .line 594
    :cond_18
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@1a
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "Resource ID #0x"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, " type #0x"

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    iget v4, v0, Landroid/util/TypedValue;->type:I

    #@35
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, " is not valid"

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-direct {v1, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v1

    #@4b
    .line 597
    .end local v0           #value:Landroid/util/TypedValue;
    :catchall_4b
    move-exception v1

    #@4c
    monitor-exit v2
    :try_end_4d
    .catchall {:try_start_3 .. :try_end_4d} :catchall_4b

    #@4d
    throw v1
.end method

.method public getDisplayMetrics()Landroid/util/DisplayMetrics;
    .registers 2

    #@0
    .prologue
    .line 1618
    iget-object v0, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@2
    return-object v0
.end method

.method public getDrawable(I)Landroid/graphics/drawable/Drawable;
    .registers 9
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 659
    const/4 v2, 0x0

    #@2
    .line 660
    .local v2, tim:Landroid/content/thm/ThemeIconManager;
    const/4 v0, 0x0

    #@3
    .line 661
    .local v0, myDrawable:Landroid/graphics/drawable/Drawable;
    iget-object v5, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    monitor-enter v5

    #@6
    .line 662
    :try_start_6
    iget-object v2, p0, Landroid/content/res/Resources;->mThemeIconManager:Landroid/content/thm/ThemeIconManager;

    #@8
    .line 663
    monitor-exit v5
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_12

    #@9
    .line 664
    if-eqz v2, :cond_15

    #@b
    .line 665
    invoke-virtual {v2, p1, v6}, Landroid/content/thm/ThemeIconManager;->getThemeIcon(II)Landroid/graphics/drawable/Drawable;

    #@e
    move-result-object v1

    #@f
    .line 666
    .local v1, themeIcon:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_15

    #@11
    .line 680
    .end local v1           #themeIcon:Landroid/graphics/drawable/Drawable;
    :goto_11
    return-object v1

    #@12
    .line 663
    :catchall_12
    move-exception v4

    #@13
    :try_start_13
    monitor-exit v5
    :try_end_14
    .catchall {:try_start_13 .. :try_end_14} :catchall_12

    #@14
    throw v4

    #@15
    .line 671
    :cond_15
    iget-object v5, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@17
    monitor-enter v5

    #@18
    .line 672
    :try_start_18
    iget-object v3, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@1a
    .line 673
    .local v3, value:Landroid/util/TypedValue;
    const/4 v4, 0x1

    #@1b
    invoke-virtual {p0, p1, v3, v4}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@1e
    .line 674
    invoke-virtual {p0, v3, p1}, Landroid/content/res/Resources;->loadDrawable(Landroid/util/TypedValue;I)Landroid/graphics/drawable/Drawable;

    #@21
    move-result-object v0

    #@22
    .line 675
    monitor-exit v5
    :try_end_23
    .catchall {:try_start_18 .. :try_end_23} :catchall_2a

    #@23
    .line 677
    if-eqz v2, :cond_2d

    #@25
    .line 678
    invoke-virtual {v2, v0, p1, v6}, Landroid/content/thm/ThemeIconManager;->doPostProcessing(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;

    #@28
    move-result-object v1

    #@29
    goto :goto_11

    #@2a
    .line 675
    .end local v3           #value:Landroid/util/TypedValue;
    :catchall_2a
    move-exception v4

    #@2b
    :try_start_2b
    monitor-exit v5
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_2a

    #@2c
    throw v4

    #@2d
    .restart local v3       #value:Landroid/util/TypedValue;
    :cond_2d
    move-object v1, v0

    #@2e
    .line 680
    goto :goto_11
.end method

.method public getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    .registers 10
    .parameter "id"
    .parameter "density"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 705
    const/4 v2, 0x0

    #@1
    .line 706
    .local v2, tim:Landroid/content/thm/ThemeIconManager;
    const/4 v0, 0x0

    #@2
    .line 707
    .local v0, myDrawable:Landroid/graphics/drawable/Drawable;
    iget-object v5, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@4
    monitor-enter v5

    #@5
    .line 708
    :try_start_5
    iget-object v2, p0, Landroid/content/res/Resources;->mThemeIconManager:Landroid/content/thm/ThemeIconManager;

    #@7
    .line 709
    monitor-exit v5
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_11

    #@8
    .line 710
    if-eqz v2, :cond_14

    #@a
    .line 711
    invoke-virtual {v2, p1, p2}, Landroid/content/thm/ThemeIconManager;->getThemeIcon(II)Landroid/graphics/drawable/Drawable;

    #@d
    move-result-object v1

    #@e
    .line 712
    .local v1, themeIcon:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_14

    #@10
    .line 742
    .end local v1           #themeIcon:Landroid/graphics/drawable/Drawable;
    :goto_10
    return-object v1

    #@11
    .line 709
    :catchall_11
    move-exception v4

    #@12
    :try_start_12
    monitor-exit v5
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_11

    #@13
    throw v4

    #@14
    .line 717
    :cond_14
    iget-object v5, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@16
    monitor-enter v5

    #@17
    .line 718
    :try_start_17
    iget-object v3, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@19
    .line 719
    .local v3, value:Landroid/util/TypedValue;
    const/4 v4, 0x1

    #@1a
    invoke-virtual {p0, p1, p2, v3, v4}, Landroid/content/res/Resources;->getValueForDensity(IILandroid/util/TypedValue;Z)V

    #@1d
    .line 728
    iget v4, v3, Landroid/util/TypedValue;->density:I

    #@1f
    if-lez v4, :cond_32

    #@21
    iget v4, v3, Landroid/util/TypedValue;->density:I

    #@23
    const v6, 0xffff

    #@26
    if-eq v4, v6, :cond_32

    #@28
    .line 729
    iget v4, v3, Landroid/util/TypedValue;->density:I

    #@2a
    if-ne v4, p2, :cond_3e

    #@2c
    .line 730
    iget-object v4, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@2e
    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    #@30
    iput v4, v3, Landroid/util/TypedValue;->density:I

    #@32
    .line 736
    :cond_32
    :goto_32
    invoke-virtual {p0, v3, p1}, Landroid/content/res/Resources;->loadDrawable(Landroid/util/TypedValue;I)Landroid/graphics/drawable/Drawable;

    #@35
    move-result-object v0

    #@36
    .line 737
    monitor-exit v5
    :try_end_37
    .catchall {:try_start_17 .. :try_end_37} :catchall_49

    #@37
    .line 739
    if-eqz v2, :cond_4c

    #@39
    .line 740
    invoke-virtual {v2, v0, p1, p2}, Landroid/content/thm/ThemeIconManager;->doPostProcessing(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;

    #@3c
    move-result-object v1

    #@3d
    goto :goto_10

    #@3e
    .line 732
    :cond_3e
    :try_start_3e
    iget v4, v3, Landroid/util/TypedValue;->density:I

    #@40
    iget-object v6, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@42
    iget v6, v6, Landroid/util/DisplayMetrics;->densityDpi:I

    #@44
    mul-int/2addr v4, v6

    #@45
    div-int/2addr v4, p2

    #@46
    iput v4, v3, Landroid/util/TypedValue;->density:I

    #@48
    goto :goto_32

    #@49
    .line 737
    .end local v3           #value:Landroid/util/TypedValue;
    :catchall_49
    move-exception v4

    #@4a
    monitor-exit v5
    :try_end_4b
    .catchall {:try_start_3e .. :try_end_4b} :catchall_49

    #@4b
    throw v4

    #@4c
    .restart local v3       #value:Landroid/util/TypedValue;
    :cond_4c
    move-object v1, v0

    #@4d
    .line 742
    goto :goto_10
.end method

.method public getFraction(III)F
    .registers 9
    .parameter "id"
    .parameter "base"
    .parameter "pbase"

    #@0
    .prologue
    .line 618
    iget-object v2, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v2

    #@3
    .line 619
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    .line 620
    .local v0, value:Landroid/util/TypedValue;
    const/4 v1, 0x1

    #@6
    invoke-virtual {p0, p1, v0, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@9
    .line 621
    iget v1, v0, Landroid/util/TypedValue;->type:I

    #@b
    const/4 v3, 0x6

    #@c
    if-ne v1, v3, :cond_18

    #@e
    .line 622
    iget v1, v0, Landroid/util/TypedValue;->data:I

    #@10
    int-to-float v3, p2

    #@11
    int-to-float v4, p3

    #@12
    invoke-static {v1, v3, v4}, Landroid/util/TypedValue;->complexToFraction(IFF)F

    #@15
    move-result v1

    #@16
    monitor-exit v2

    #@17
    return v1

    #@18
    .line 624
    :cond_18
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@1a
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "Resource ID #0x"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, " type #0x"

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    iget v4, v0, Landroid/util/TypedValue;->type:I

    #@35
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, " is not valid"

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-direct {v1, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v1

    #@4b
    .line 627
    .end local v0           #value:Landroid/util/TypedValue;
    :catchall_4b
    move-exception v1

    #@4c
    monitor-exit v2
    :try_end_4d
    .catchall {:try_start_3 .. :try_end_4d} :catchall_4b

    #@4d
    throw v1
.end method

.method public getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .registers 5
    .parameter "name"
    .parameter "defType"
    .parameter "defPackage"

    #@0
    .prologue
    .line 1674
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result v0

    #@4
    .line 1678
    :goto_4
    return v0

    #@5
    .line 1675
    :catch_5
    move-exception v0

    #@6
    .line 1678
    iget-object v0, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@8
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/res/AssetManager;->getResourceIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    goto :goto_4
.end method

.method public getIntArray(I)[I
    .registers 6
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 468
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->getArrayIntResource(I)[I

    #@5
    move-result-object v0

    #@6
    .line 469
    .local v0, res:[I
    if-eqz v0, :cond_9

    #@8
    .line 470
    return-object v0

    #@9
    .line 472
    :cond_9
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Int array resource ID #0x"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1
.end method

.method public getInteger(I)I
    .registers 7
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 858
    iget-object v2, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v2

    #@3
    .line 859
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    .line 860
    .local v0, value:Landroid/util/TypedValue;
    const/4 v1, 0x1

    #@6
    invoke-virtual {p0, p1, v0, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@9
    .line 861
    iget v1, v0, Landroid/util/TypedValue;->type:I

    #@b
    const/16 v3, 0x10

    #@d
    if-lt v1, v3, :cond_19

    #@f
    iget v1, v0, Landroid/util/TypedValue;->type:I

    #@11
    const/16 v3, 0x1f

    #@13
    if-gt v1, v3, :cond_19

    #@15
    .line 863
    iget v1, v0, Landroid/util/TypedValue;->data:I

    #@17
    monitor-exit v2

    #@18
    return v1

    #@19
    .line 865
    :cond_19
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "Resource ID #0x"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    const-string v4, " type #0x"

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    iget v4, v0, Landroid/util/TypedValue;->type:I

    #@36
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    const-string v4, " is not valid"

    #@40
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-direct {v1, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v1

    #@4c
    .line 868
    .end local v0           #value:Landroid/util/TypedValue;
    :catchall_4c
    move-exception v1

    #@4d
    monitor-exit v2
    :try_end_4e
    .catchall {:try_start_3 .. :try_end_4e} :catchall_4c

    #@4e
    throw v1
.end method

.method public getLayout(I)Landroid/content/res/XmlResourceParser;
    .registers 3
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 892
    const-string/jumbo v0, "layout"

    #@3
    invoke-virtual {p0, p1, v0}, Landroid/content/res/Resources;->loadXmlResourceParser(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getMovie(I)Landroid/graphics/Movie;
    .registers 5
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 755
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    #@3
    move-result-object v0

    #@4
    .line 756
    .local v0, is:Ljava/io/InputStream;
    invoke-static {v0}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    #@7
    move-result-object v1

    #@8
    .line 758
    .local v1, movie:Landroid/graphics/Movie;
    :try_start_8
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_b} :catch_c

    #@b
    .line 763
    :goto_b
    return-object v1

    #@c
    .line 760
    :catch_c
    move-exception v2

    #@d
    goto :goto_b
.end method

.method public getQuantityString(II)Ljava/lang/String;
    .registers 4
    .parameter "id"
    .parameter "quantity"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 394
    invoke-virtual {p0, p1, p2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public varargs getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;
    .registers 6
    .parameter "id"
    .parameter "quantity"
    .parameter "formatArgs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 371
    invoke-virtual {p0, p1, p2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 372
    .local v0, raw:Ljava/lang/String;
    iget-object v1, p0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@a
    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@c
    invoke-static {v1, v0, p3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    return-object v1
.end method

.method public getQuantityText(II)Ljava/lang/CharSequence;
    .registers 9
    .parameter "id"
    .parameter "quantity"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 254
    invoke-direct {p0}, Landroid/content/res/Resources;->getPluralRule()Llibcore/icu/NativePluralRules;

    #@3
    move-result-object v2

    #@4
    .line 255
    .local v2, rule:Llibcore/icu/NativePluralRules;
    iget-object v3, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@6
    invoke-virtual {v2, p2}, Llibcore/icu/NativePluralRules;->quantityForInt(I)I

    #@9
    move-result v4

    #@a
    invoke-static {v4}, Landroid/content/res/Resources;->attrForQuantityCode(I)I

    #@d
    move-result v4

    #@e
    invoke-virtual {v3, p1, v4}, Landroid/content/res/AssetManager;->getResourceBagText(II)Ljava/lang/CharSequence;

    #@11
    move-result-object v0

    #@12
    .line 257
    .local v0, res:Ljava/lang/CharSequence;
    if-eqz v0, :cond_16

    #@14
    move-object v1, v0

    #@15
    .line 262
    .end local v0           #res:Ljava/lang/CharSequence;
    .local v1, res:Ljava/lang/CharSequence;
    :goto_15
    return-object v1

    #@16
    .line 260
    .end local v1           #res:Ljava/lang/CharSequence;
    .restart local v0       #res:Ljava/lang/CharSequence;
    :cond_16
    iget-object v3, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@18
    const v4, 0x1000004

    #@1b
    invoke-virtual {v3, p1, v4}, Landroid/content/res/AssetManager;->getResourceBagText(II)Ljava/lang/CharSequence;

    #@1e
    move-result-object v0

    #@1f
    .line 261
    if-eqz v0, :cond_23

    #@21
    move-object v1, v0

    #@22
    .line 262
    .end local v0           #res:Ljava/lang/CharSequence;
    .restart local v1       #res:Ljava/lang/CharSequence;
    goto :goto_15

    #@23
    .line 264
    .end local v1           #res:Ljava/lang/CharSequence;
    .restart local v0       #res:Ljava/lang/CharSequence;
    :cond_23
    new-instance v3, Landroid/content/res/Resources$NotFoundException;

    #@25
    new-instance v4, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "Plural resource ID #0x"

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    const-string v5, " quantity="

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    const-string v5, " item="

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v2, p2}, Llibcore/icu/NativePluralRules;->quantityForInt(I)I

    #@4b
    move-result v5

    #@4c
    invoke-static {v5}, Landroid/content/res/Resources;->stringForQuantityCode(I)Ljava/lang/String;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v4

    #@58
    invoke-direct {v3, v4}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v3
.end method

.method public getResourceEntryName(I)Ljava/lang/String;
    .registers 6
    .parameter "resid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1753
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->getResourceEntryName(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 1754
    .local v0, str:Ljava/lang/String;
    if-eqz v0, :cond_9

    #@8
    return-object v0

    #@9
    .line 1755
    :cond_9
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Unable to find resource ID #0x"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1
.end method

.method public getResourceName(I)Ljava/lang/String;
    .registers 6
    .parameter "resid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1696
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->getResourceName(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 1697
    .local v0, str:Ljava/lang/String;
    if-eqz v0, :cond_9

    #@8
    return-object v0

    #@9
    .line 1698
    :cond_9
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Unable to find resource ID #0x"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1
.end method

.method public getResourcePackageName(I)Ljava/lang/String;
    .registers 6
    .parameter "resid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1715
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->getResourcePackageName(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 1716
    .local v0, str:Ljava/lang/String;
    if-eqz v0, :cond_9

    #@8
    return-object v0

    #@9
    .line 1717
    :cond_9
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Unable to find resource ID #0x"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1
.end method

.method public getResourceTypeName(I)Ljava/lang/String;
    .registers 6
    .parameter "resid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1734
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->getResourceTypeName(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 1735
    .local v0, str:Ljava/lang/String;
    if-eqz v0, :cond_9

    #@8
    return-object v0

    #@9
    .line 1736
    :cond_9
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Unable to find resource ID #0x"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1
.end method

.method public getString(I)Ljava/lang/String;
    .registers 6
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 315
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    .line 316
    .local v0, res:Ljava/lang/CharSequence;
    if-eqz v0, :cond_b

    #@6
    .line 317
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    return-object v1

    #@b
    .line 319
    :cond_b
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "String resource ID #0x"

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1
.end method

.method public varargs getString(I[Ljava/lang/Object;)Ljava/lang/String;
    .registers 5
    .parameter "id"
    .parameter "formatArgs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 343
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 344
    .local v0, raw:Ljava/lang/String;
    iget-object v1, p0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@6
    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@8
    invoke-static {v1, v0, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    return-object v1
.end method

.method public getStringArray(I)[Ljava/lang/String;
    .registers 6
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 448
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->getResourceStringArray(I)[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 449
    .local v0, res:[Ljava/lang/String;
    if-eqz v0, :cond_9

    #@8
    .line 450
    return-object v0

    #@9
    .line 452
    :cond_9
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "String array resource ID #0x"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1
.end method

.method public getText(I)Ljava/lang/CharSequence;
    .registers 6
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 227
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->getResourceText(I)Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    .line 228
    .local v0, res:Ljava/lang/CharSequence;
    if-eqz v0, :cond_9

    #@8
    .line 229
    return-object v0

    #@9
    .line 231
    :cond_9
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "String resource ID #0x"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1
.end method

.method public getText(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "id"
    .parameter "def"

    #@0
    .prologue
    .line 412
    if-eqz p1, :cond_b

    #@2
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@4
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->getResourceText(I)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    .line 413
    .local v0, res:Ljava/lang/CharSequence;
    :goto_8
    if-eqz v0, :cond_d

    #@a
    .end local v0           #res:Ljava/lang/CharSequence;
    :goto_a
    return-object v0

    #@b
    .line 412
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_8

    #@d
    .restart local v0       #res:Ljava/lang/CharSequence;
    :cond_d
    move-object v0, p2

    #@e
    .line 413
    goto :goto_a
.end method

.method public getTextArray(I)[Ljava/lang/CharSequence;
    .registers 6
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 428
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->getResourceTextArray(I)[Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    .line 429
    .local v0, res:[Ljava/lang/CharSequence;
    if-eqz v0, :cond_9

    #@8
    .line 430
    return-object v0

    #@9
    .line 432
    :cond_9
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Text array resource ID #0x"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1
.end method

.method public getThemeIconManager()Landroid/content/thm/ThemeIconManager;
    .registers 3

    #@0
    .prologue
    .line 2301
    iget-object v1, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v1

    #@3
    .line 2302
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mThemeIconManager:Landroid/content/thm/ThemeIconManager;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 2303
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getValue(ILandroid/util/TypedValue;Z)V
    .registers 8
    .parameter "id"
    .parameter "outValue"
    .parameter "resolveRefs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1049
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {v1, p1, v2, p2, p3}, Landroid/content/res/AssetManager;->getResourceValue(IILandroid/util/TypedValue;Z)Z

    #@6
    move-result v0

    #@7
    .line 1050
    .local v0, found:Z
    if-eqz v0, :cond_a

    #@9
    .line 1051
    return-void

    #@a
    .line 1053
    :cond_a
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Resource ID #0x"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1
.end method

.method public getValue(Ljava/lang/String;Landroid/util/TypedValue;Z)V
    .registers 8
    .parameter "name"
    .parameter "outValue"
    .parameter "resolveRefs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1101
    const-string/jumbo v1, "string"

    #@3
    const/4 v2, 0x0

    #@4
    invoke-virtual {p0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    .line 1102
    .local v0, id:I
    if-eqz v0, :cond_e

    #@a
    .line 1103
    invoke-virtual {p0, v0, p2, p3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@d
    .line 1104
    return-void

    #@e
    .line 1106
    :cond_e
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "String resource name "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1
.end method

.method public getValueForDensity(IILandroid/util/TypedValue;Z)V
    .registers 9
    .parameter "id"
    .parameter "density"
    .parameter "outValue"
    .parameter "resolveRefs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1072
    iget-object v1, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2
    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/content/res/AssetManager;->getResourceValue(IILandroid/util/TypedValue;Z)Z

    #@5
    move-result v0

    #@6
    .line 1073
    .local v0, found:Z
    if-eqz v0, :cond_9

    #@8
    .line 1074
    return-void

    #@9
    .line 1076
    :cond_9
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Resource ID #0x"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1
.end method

.method public getXml(I)Landroid/content/res/XmlResourceParser;
    .registers 3
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 941
    const-string/jumbo v0, "xml"

    #@3
    invoke-virtual {p0, p1, v0}, Landroid/content/res/Resources;->loadXmlResourceParser(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method loadColorStateList(Landroid/util/TypedValue;I)Landroid/content/res/ColorStateList;
    .registers 15
    .parameter "value"
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 2081
    iget v8, p1, Landroid/util/TypedValue;->assetCookie:I

    #@2
    int-to-long v8, v8

    #@3
    const/16 v10, 0x20

    #@5
    shl-long/2addr v8, v10

    #@6
    iget v10, p1, Landroid/util/TypedValue;->data:I

    #@8
    int-to-long v10, v10

    #@9
    or-long v4, v8, v10

    #@b
    .line 2085
    .local v4, key:J
    iget v8, p1, Landroid/util/TypedValue;->type:I

    #@d
    const/16 v9, 0x1c

    #@f
    if-lt v8, v9, :cond_3c

    #@11
    iget v8, p1, Landroid/util/TypedValue;->type:I

    #@13
    const/16 v9, 0x1f

    #@15
    if-gt v8, v9, :cond_3c

    #@17
    .line 2088
    sget-object v8, Landroid/content/res/Resources;->sPreloadedColorStateLists:Landroid/util/LongSparseArray;

    #@19
    invoke-virtual {v8, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Landroid/content/res/ColorStateList;

    #@1f
    .line 2089
    .local v0, csl:Landroid/content/res/ColorStateList;
    if-eqz v0, :cond_23

    #@21
    move-object v1, v0

    #@22
    .line 2154
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    .local v1, csl:Landroid/content/res/ColorStateList;
    :goto_22
    return-object v1

    #@23
    .line 2093
    .end local v1           #csl:Landroid/content/res/ColorStateList;
    .restart local v0       #csl:Landroid/content/res/ColorStateList;
    :cond_23
    iget v8, p1, Landroid/util/TypedValue;->data:I

    #@25
    invoke-static {v8}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    #@28
    move-result-object v0

    #@29
    .line 2094
    iget-boolean v8, p0, Landroid/content/res/Resources;->mPreloading:Z

    #@2b
    if-eqz v8, :cond_3a

    #@2d
    .line 2095
    const-string v8, "color"

    #@2f
    invoke-direct {p0, p1, v8}, Landroid/content/res/Resources;->verifyPreloadConfig(Landroid/util/TypedValue;Ljava/lang/String;)Z

    #@32
    move-result v8

    #@33
    if-eqz v8, :cond_3a

    #@35
    .line 2096
    sget-object v8, Landroid/content/res/Resources;->sPreloadedColorStateLists:Landroid/util/LongSparseArray;

    #@37
    invoke-virtual {v8, v4, v5, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@3a
    :cond_3a
    move-object v1, v0

    #@3b
    .line 2100
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    .restart local v1       #csl:Landroid/content/res/ColorStateList;
    goto :goto_22

    #@3c
    .line 2103
    .end local v1           #csl:Landroid/content/res/ColorStateList;
    :cond_3c
    invoke-direct {p0, v4, v5}, Landroid/content/res/Resources;->getCachedColorStateList(J)Landroid/content/res/ColorStateList;

    #@3f
    move-result-object v0

    #@40
    .line 2104
    .restart local v0       #csl:Landroid/content/res/ColorStateList;
    if-eqz v0, :cond_44

    #@42
    move-object v1, v0

    #@43
    .line 2105
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    .restart local v1       #csl:Landroid/content/res/ColorStateList;
    goto :goto_22

    #@44
    .line 2108
    .end local v1           #csl:Landroid/content/res/ColorStateList;
    .restart local v0       #csl:Landroid/content/res/ColorStateList;
    :cond_44
    sget-object v8, Landroid/content/res/Resources;->sPreloadedColorStateLists:Landroid/util/LongSparseArray;

    #@46
    invoke-virtual {v8, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@49
    move-result-object v0

    #@4a
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    check-cast v0, Landroid/content/res/ColorStateList;

    #@4c
    .line 2109
    .restart local v0       #csl:Landroid/content/res/ColorStateList;
    if-eqz v0, :cond_50

    #@4e
    move-object v1, v0

    #@4f
    .line 2110
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    .restart local v1       #csl:Landroid/content/res/ColorStateList;
    goto :goto_22

    #@50
    .line 2113
    .end local v1           #csl:Landroid/content/res/ColorStateList;
    .restart local v0       #csl:Landroid/content/res/ColorStateList;
    :cond_50
    iget-object v8, p1, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@52
    if-nez v8, :cond_6d

    #@54
    .line 2114
    new-instance v8, Landroid/content/res/Resources$NotFoundException;

    #@56
    new-instance v9, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v10, "Resource is not a ColorStateList (color or path): "

    #@5d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v9

    #@61
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v9

    #@65
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v9

    #@69
    invoke-direct {v8, v9}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@6c
    throw v8

    #@6d
    .line 2118
    :cond_6d
    iget-object v8, p1, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@6f
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    .line 2120
    .local v3, file:Ljava/lang/String;
    const-string v8, ".xml"

    #@75
    invoke-virtual {v3, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@78
    move-result v8

    #@79
    if-eqz v8, :cond_ca

    #@7b
    .line 2122
    :try_start_7b
    iget v8, p1, Landroid/util/TypedValue;->assetCookie:I

    #@7d
    const-string v9, "colorstatelist"

    #@7f
    invoke-virtual {p0, v3, p2, v8, v9}, Landroid/content/res/Resources;->loadXmlResourceParser(Ljava/lang/String;IILjava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@82
    move-result-object v7

    #@83
    .line 2124
    .local v7, rp:Landroid/content/res/XmlResourceParser;
    invoke-static {p0, v7}, Landroid/content/res/ColorStateList;->createFromXml(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;)Landroid/content/res/ColorStateList;

    #@86
    move-result-object v0

    #@87
    .line 2125
    invoke-interface {v7}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_8a
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_8a} :catch_9f

    #@8a
    .line 2139
    if-eqz v0, :cond_9d

    #@8c
    .line 2140
    iget-boolean v8, p0, Landroid/content/res/Resources;->mPreloading:Z

    #@8e
    if-eqz v8, :cond_f7

    #@90
    .line 2141
    const-string v8, "color"

    #@92
    invoke-direct {p0, p1, v8}, Landroid/content/res/Resources;->verifyPreloadConfig(Landroid/util/TypedValue;Ljava/lang/String;)Z

    #@95
    move-result v8

    #@96
    if-eqz v8, :cond_9d

    #@98
    .line 2142
    sget-object v8, Landroid/content/res/Resources;->sPreloadedColorStateLists:Landroid/util/LongSparseArray;

    #@9a
    invoke-virtual {v8, v4, v5, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@9d
    :cond_9d
    :goto_9d
    move-object v1, v0

    #@9e
    .line 2154
    .end local v0           #csl:Landroid/content/res/ColorStateList;
    .restart local v1       #csl:Landroid/content/res/ColorStateList;
    goto :goto_22

    #@9f
    .line 2126
    .end local v1           #csl:Landroid/content/res/ColorStateList;
    .end local v7           #rp:Landroid/content/res/XmlResourceParser;
    .restart local v0       #csl:Landroid/content/res/ColorStateList;
    :catch_9f
    move-exception v2

    #@a0
    .line 2127
    .local v2, e:Ljava/lang/Exception;
    new-instance v6, Landroid/content/res/Resources$NotFoundException;

    #@a2
    new-instance v8, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    const-string v9, "File "

    #@a9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v8

    #@ad
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v8

    #@b1
    const-string v9, " from color state list resource ID #0x"

    #@b3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v8

    #@b7
    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@ba
    move-result-object v9

    #@bb
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v8

    #@bf
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v8

    #@c3
    invoke-direct {v6, v8}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@c6
    .line 2130
    .local v6, rnf:Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v6, v2}, Landroid/content/res/Resources$NotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@c9
    .line 2131
    throw v6

    #@ca
    .line 2134
    .end local v2           #e:Ljava/lang/Exception;
    .end local v6           #rnf:Landroid/content/res/Resources$NotFoundException;
    :cond_ca
    new-instance v8, Landroid/content/res/Resources$NotFoundException;

    #@cc
    new-instance v9, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v10, "File "

    #@d3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v9

    #@d7
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v9

    #@db
    const-string v10, " from drawable resource ID #0x"

    #@dd
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v9

    #@e1
    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@e4
    move-result-object v10

    #@e5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v9

    #@e9
    const-string v10, ": .xml extension required"

    #@eb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v9

    #@ef
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v9

    #@f3
    invoke-direct {v8, v9}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@f6
    throw v8

    #@f7
    .line 2145
    .restart local v7       #rp:Landroid/content/res/XmlResourceParser;
    :cond_f7
    iget-object v9, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@f9
    monitor-enter v9

    #@fa
    .line 2149
    :try_start_fa
    iget-object v8, p0, Landroid/content/res/Resources;->mColorStateListCache:Landroid/util/LongSparseArray;

    #@fc
    new-instance v10, Ljava/lang/ref/WeakReference;

    #@fe
    invoke-direct {v10, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@101
    invoke-virtual {v8, v4, v5, v10}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@104
    .line 2150
    monitor-exit v9

    #@105
    goto :goto_9d

    #@106
    :catchall_106
    move-exception v8

    #@107
    monitor-exit v9
    :try_end_108
    .catchall {:try_start_fa .. :try_end_108} :catchall_106

    #@108
    throw v8
.end method

.method loadDrawable(Landroid/util/TypedValue;I)Landroid/graphics/drawable/Drawable;
    .registers 20
    .parameter "value"
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1939
    const/4 v8, 0x0

    #@1
    .line 1940
    .local v8, isColorDrawable:Z
    move-object/from16 v0, p1

    #@3
    iget v13, v0, Landroid/util/TypedValue;->type:I

    #@5
    const/16 v14, 0x1c

    #@7
    if-lt v13, v14, :cond_12

    #@9
    move-object/from16 v0, p1

    #@b
    iget v13, v0, Landroid/util/TypedValue;->type:I

    #@d
    const/16 v14, 0x1f

    #@f
    if-gt v13, v14, :cond_12

    #@11
    .line 1942
    const/4 v8, 0x1

    #@12
    .line 1944
    :cond_12
    if-eqz v8, :cond_29

    #@14
    move-object/from16 v0, p1

    #@16
    iget v13, v0, Landroid/util/TypedValue;->data:I

    #@18
    int-to-long v9, v13

    #@19
    .line 1947
    .local v9, key:J
    :goto_19
    if-eqz v8, :cond_39

    #@1b
    move-object/from16 v0, p0

    #@1d
    iget-object v13, v0, Landroid/content/res/Resources;->mColorDrawableCache:Landroid/util/LongSparseArray;

    #@1f
    :goto_1f
    move-object/from16 v0, p0

    #@21
    invoke-direct {v0, v13, v9, v10}, Landroid/content/res/Resources;->getCachedDrawable(Landroid/util/LongSparseArray;J)Landroid/graphics/drawable/Drawable;

    #@24
    move-result-object v3

    #@25
    .line 1949
    .local v3, dr:Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_3e

    #@27
    move-object v4, v3

    #@28
    .line 2047
    .end local v3           #dr:Landroid/graphics/drawable/Drawable;
    .local v4, dr:Landroid/graphics/drawable/Drawable;
    :goto_28
    return-object v4

    #@29
    .line 1944
    .end local v4           #dr:Landroid/graphics/drawable/Drawable;
    .end local v9           #key:J
    :cond_29
    move-object/from16 v0, p1

    #@2b
    iget v13, v0, Landroid/util/TypedValue;->assetCookie:I

    #@2d
    int-to-long v13, v13

    #@2e
    const/16 v15, 0x20

    #@30
    shl-long/2addr v13, v15

    #@31
    move-object/from16 v0, p1

    #@33
    iget v15, v0, Landroid/util/TypedValue;->data:I

    #@35
    int-to-long v15, v15

    #@36
    or-long v9, v13, v15

    #@38
    goto :goto_19

    #@39
    .line 1947
    .restart local v9       #key:J
    :cond_39
    move-object/from16 v0, p0

    #@3b
    iget-object v13, v0, Landroid/content/res/Resources;->mDrawableCache:Landroid/util/LongSparseArray;

    #@3d
    goto :goto_1f

    #@3e
    .line 1953
    .restart local v3       #dr:Landroid/graphics/drawable/Drawable;
    :cond_3e
    if-eqz v8, :cond_7b

    #@40
    sget-object v13, Landroid/content/res/Resources;->sPreloadedColorDrawables:Landroid/util/LongSparseArray;

    #@42
    invoke-virtual {v13, v9, v10}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@45
    move-result-object v13

    #@46
    check-cast v13, Landroid/graphics/drawable/Drawable$ConstantState;

    #@48
    move-object v2, v13

    #@49
    .line 1957
    .local v2, cs:Landroid/graphics/drawable/Drawable$ConstantState;
    :goto_49
    if-eqz v2, :cond_92

    #@4b
    .line 1958
    move-object/from16 v0, p0

    #@4d
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    #@50
    move-result-object v3

    #@51
    .line 2019
    :cond_51
    :goto_51
    if-eqz v3, :cond_79

    #@53
    .line 2020
    move-object/from16 v0, p1

    #@55
    iget v13, v0, Landroid/util/TypedValue;->changingConfigurations:I

    #@57
    invoke-virtual {v3, v13}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    #@5a
    .line 2021
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@5d
    move-result-object v2

    #@5e
    .line 2022
    if-eqz v2, :cond_79

    #@60
    .line 2023
    move-object/from16 v0, p0

    #@62
    iget-boolean v13, v0, Landroid/content/res/Resources;->mPreloading:Z

    #@64
    if-eqz v13, :cond_193

    #@66
    .line 2024
    const-string v13, "drawable"

    #@68
    move-object/from16 v0, p0

    #@6a
    move-object/from16 v1, p1

    #@6c
    invoke-direct {v0, v1, v13}, Landroid/content/res/Resources;->verifyPreloadConfig(Landroid/util/TypedValue;Ljava/lang/String;)Z

    #@6f
    move-result v13

    #@70
    if-eqz v13, :cond_79

    #@72
    .line 2025
    if-eqz v8, :cond_15a

    #@74
    .line 2026
    sget-object v13, Landroid/content/res/Resources;->sPreloadedColorDrawables:Landroid/util/LongSparseArray;

    #@76
    invoke-virtual {v13, v9, v10, v2}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@79
    :cond_79
    :goto_79
    move-object v4, v3

    #@7a
    .line 2047
    .end local v3           #dr:Landroid/graphics/drawable/Drawable;
    .restart local v4       #dr:Landroid/graphics/drawable/Drawable;
    goto :goto_28

    #@7b
    .line 1953
    .end local v2           #cs:Landroid/graphics/drawable/Drawable$ConstantState;
    .end local v4           #dr:Landroid/graphics/drawable/Drawable;
    .restart local v3       #dr:Landroid/graphics/drawable/Drawable;
    :cond_7b
    sget v13, Landroid/content/res/Resources;->sPreloadedDensity:I

    #@7d
    move-object/from16 v0, p0

    #@7f
    iget-object v14, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@81
    iget v14, v14, Landroid/content/res/Configuration;->densityDpi:I

    #@83
    if-ne v13, v14, :cond_8f

    #@85
    sget-object v13, Landroid/content/res/Resources;->sPreloadedDrawables:Landroid/util/LongSparseArray;

    #@87
    invoke-virtual {v13, v9, v10}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@8a
    move-result-object v13

    #@8b
    check-cast v13, Landroid/graphics/drawable/Drawable$ConstantState;

    #@8d
    move-object v2, v13

    #@8e
    goto :goto_49

    #@8f
    :cond_8f
    const/4 v13, 0x0

    #@90
    move-object v2, v13

    #@91
    goto :goto_49

    #@92
    .line 1960
    .restart local v2       #cs:Landroid/graphics/drawable/Drawable$ConstantState;
    :cond_92
    if-eqz v8, :cond_9d

    #@94
    .line 1961
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    #@96
    .end local v3           #dr:Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p1

    #@98
    iget v13, v0, Landroid/util/TypedValue;->data:I

    #@9a
    invoke-direct {v3, v13}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    #@9d
    .line 1964
    .restart local v3       #dr:Landroid/graphics/drawable/Drawable;
    :cond_9d
    if-nez v3, :cond_51

    #@9f
    .line 1965
    move-object/from16 v0, p1

    #@a1
    iget-object v13, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@a3
    if-nez v13, :cond_c0

    #@a5
    .line 1966
    new-instance v13, Landroid/content/res/Resources$NotFoundException;

    #@a7
    new-instance v14, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v15, "Resource is not a Drawable (color or path): "

    #@ae
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v14

    #@b2
    move-object/from16 v0, p1

    #@b4
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v14

    #@b8
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v14

    #@bc
    invoke-direct {v13, v14}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@bf
    throw v13

    #@c0
    .line 1970
    :cond_c0
    move-object/from16 v0, p1

    #@c2
    iget-object v13, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@c4
    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c7
    move-result-object v6

    #@c8
    .line 1985
    .local v6, file:Ljava/lang/String;
    const-string v13, ".xml"

    #@ca
    invoke-virtual {v6, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@cd
    move-result v13

    #@ce
    if-eqz v13, :cond_114

    #@d0
    .line 1987
    :try_start_d0
    move-object/from16 v0, p1

    #@d2
    iget v13, v0, Landroid/util/TypedValue;->assetCookie:I

    #@d4
    const-string v14, "drawable"

    #@d6
    move-object/from16 v0, p0

    #@d8
    move/from16 v1, p2

    #@da
    invoke-virtual {v0, v6, v1, v13, v14}, Landroid/content/res/Resources;->loadXmlResourceParser(Ljava/lang/String;IILjava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@dd
    move-result-object v12

    #@de
    .line 1989
    .local v12, rp:Landroid/content/res/XmlResourceParser;
    move-object/from16 v0, p0

    #@e0
    invoke-static {v0, v12}, Landroid/graphics/drawable/Drawable;->createFromXml(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;)Landroid/graphics/drawable/Drawable;

    #@e3
    move-result-object v3

    #@e4
    .line 1990
    invoke-interface {v12}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_e7
    .catch Ljava/lang/Exception; {:try_start_d0 .. :try_end_e7} :catch_e9

    #@e7
    goto/16 :goto_51

    #@e9
    .line 1991
    .end local v12           #rp:Landroid/content/res/XmlResourceParser;
    :catch_e9
    move-exception v5

    #@ea
    .line 1992
    .local v5, e:Ljava/lang/Exception;
    new-instance v11, Landroid/content/res/Resources$NotFoundException;

    #@ec
    new-instance v13, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    const-string v14, "File "

    #@f3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v13

    #@f7
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v13

    #@fb
    const-string v14, " from drawable resource ID #0x"

    #@fd
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v13

    #@101
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@104
    move-result-object v14

    #@105
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v13

    #@109
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v13

    #@10d
    invoke-direct {v11, v13}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@110
    .line 1995
    .local v11, rnf:Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v11, v5}, Landroid/content/res/Resources$NotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@113
    .line 1996
    throw v11

    #@114
    .line 2001
    .end local v5           #e:Ljava/lang/Exception;
    .end local v11           #rnf:Landroid/content/res/Resources$NotFoundException;
    :cond_114
    :try_start_114
    move-object/from16 v0, p0

    #@116
    iget-object v13, v0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@118
    move-object/from16 v0, p1

    #@11a
    iget v14, v0, Landroid/util/TypedValue;->assetCookie:I

    #@11c
    const/4 v15, 0x2

    #@11d
    invoke-virtual {v13, v14, v6, v15}, Landroid/content/res/AssetManager;->openNonAsset(ILjava/lang/String;I)Ljava/io/InputStream;

    #@120
    move-result-object v7

    #@121
    .line 2004
    .local v7, is:Ljava/io/InputStream;
    const/4 v13, 0x0

    #@122
    move-object/from16 v0, p0

    #@124
    move-object/from16 v1, p1

    #@126
    invoke-static {v0, v1, v7, v6, v13}, Landroid/graphics/drawable/Drawable;->createFromResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/drawable/Drawable;

    #@129
    move-result-object v3

    #@12a
    .line 2006
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_12d
    .catch Ljava/lang/Exception; {:try_start_114 .. :try_end_12d} :catch_12f

    #@12d
    goto/16 :goto_51

    #@12f
    .line 2008
    .end local v7           #is:Ljava/io/InputStream;
    :catch_12f
    move-exception v5

    #@130
    .line 2009
    .restart local v5       #e:Ljava/lang/Exception;
    new-instance v11, Landroid/content/res/Resources$NotFoundException;

    #@132
    new-instance v13, Ljava/lang/StringBuilder;

    #@134
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@137
    const-string v14, "File "

    #@139
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v13

    #@13d
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v13

    #@141
    const-string v14, " from drawable resource ID #0x"

    #@143
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v13

    #@147
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@14a
    move-result-object v14

    #@14b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v13

    #@14f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@152
    move-result-object v13

    #@153
    invoke-direct {v11, v13}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@156
    .line 2012
    .restart local v11       #rnf:Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v11, v5}, Landroid/content/res/Resources$NotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@159
    .line 2013
    throw v11

    #@15a
    .line 2028
    .end local v5           #e:Ljava/lang/Exception;
    .end local v6           #file:Ljava/lang/String;
    .end local v11           #rnf:Landroid/content/res/Resources$NotFoundException;
    :cond_15a
    sget-object v13, Landroid/content/res/Resources;->sPreloadedDrawables:Landroid/util/LongSparseArray;

    #@15c
    invoke-virtual {v13, v9, v10, v2}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@15f
    .line 2029
    const-string v13, "Resources"

    #@161
    new-instance v14, Ljava/lang/StringBuilder;

    #@163
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@166
    const-string/jumbo v15, "sPreloadedDrawables.put(key : "

    #@169
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v14

    #@16d
    invoke-virtual {v14, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@170
    move-result-object v14

    #@171
    const-string/jumbo v15, "value : "

    #@174
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v14

    #@178
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v14

    #@17c
    const-string v15, "/"

    #@17e
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v14

    #@182
    move-object/from16 v0, p1

    #@184
    iget v15, v0, Landroid/util/TypedValue;->changingConfigurations:I

    #@186
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@189
    move-result-object v14

    #@18a
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18d
    move-result-object v14

    #@18e
    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@191
    goto/16 :goto_79

    #@193
    .line 2033
    :cond_193
    move-object/from16 v0, p0

    #@195
    iget-object v14, v0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@197
    monitor-enter v14

    #@198
    .line 2037
    if-eqz v8, :cond_1ac

    #@19a
    .line 2038
    :try_start_19a
    move-object/from16 v0, p0

    #@19c
    iget-object v13, v0, Landroid/content/res/Resources;->mColorDrawableCache:Landroid/util/LongSparseArray;

    #@19e
    new-instance v15, Ljava/lang/ref/WeakReference;

    #@1a0
    invoke-direct {v15, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@1a3
    invoke-virtual {v13, v9, v10, v15}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@1a6
    .line 2042
    :goto_1a6
    monitor-exit v14

    #@1a7
    goto/16 :goto_79

    #@1a9
    :catchall_1a9
    move-exception v13

    #@1aa
    monitor-exit v14
    :try_end_1ab
    .catchall {:try_start_19a .. :try_end_1ab} :catchall_1a9

    #@1ab
    throw v13

    #@1ac
    .line 2040
    :cond_1ac
    :try_start_1ac
    move-object/from16 v0, p0

    #@1ae
    iget-object v13, v0, Landroid/content/res/Resources;->mDrawableCache:Landroid/util/LongSparseArray;

    #@1b0
    new-instance v15, Ljava/lang/ref/WeakReference;

    #@1b2
    invoke-direct {v15, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@1b5
    invoke-virtual {v13, v9, v10, v15}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
    :try_end_1b8
    .catchall {:try_start_1ac .. :try_end_1b8} :catchall_1a9

    #@1b8
    goto :goto_1a6
.end method

.method loadXmlResourceParser(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;
    .registers 8
    .parameter "id"
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 2177
    iget-object v2, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v2

    #@3
    .line 2178
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    .line 2179
    .local v0, value:Landroid/util/TypedValue;
    const/4 v1, 0x1

    #@6
    invoke-virtual {p0, p1, v0, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@9
    .line 2180
    iget v1, v0, Landroid/util/TypedValue;->type:I

    #@b
    const/4 v3, 0x3

    #@c
    if-ne v1, v3, :cond_1c

    #@e
    .line 2181
    iget-object v1, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@10
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    iget v3, v0, Landroid/util/TypedValue;->assetCookie:I

    #@16
    invoke-virtual {p0, v1, p1, v3, p2}, Landroid/content/res/Resources;->loadXmlResourceParser(Ljava/lang/String;IILjava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@19
    move-result-object v1

    #@1a
    monitor-exit v2

    #@1b
    return-object v1

    #@1c
    .line 2184
    :cond_1c
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "Resource ID #0x"

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, " type #0x"

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    iget v4, v0, Landroid/util/TypedValue;->type:I

    #@39
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    const-string v4, " is not valid"

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-direct {v1, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@4e
    throw v1

    #@4f
    .line 2187
    .end local v0           #value:Landroid/util/TypedValue;
    :catchall_4f
    move-exception v1

    #@50
    monitor-exit v2
    :try_end_51
    .catchall {:try_start_3 .. :try_end_51} :catchall_4f

    #@51
    throw v1
.end method

.method loadXmlResourceParser(Ljava/lang/String;IILjava/lang/String;)Landroid/content/res/XmlResourceParser;
    .registers 15
    .parameter "file"
    .parameter "id"
    .parameter "assetCookie"
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 2192
    if-eqz p2, :cond_7f

    #@2
    .line 2195
    :try_start_2
    iget-object v8, p0, Landroid/content/res/Resources;->mCachedXmlBlockIds:[I

    #@4
    monitor-enter v8
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_5} :catch_49

    #@5
    .line 2197
    :try_start_5
    iget-object v7, p0, Landroid/content/res/Resources;->mCachedXmlBlockIds:[I

    #@7
    array-length v3, v7

    #@8
    .line 2198
    .local v3, num:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v3, :cond_1e

    #@b
    .line 2199
    iget-object v7, p0, Landroid/content/res/Resources;->mCachedXmlBlockIds:[I

    #@d
    aget v7, v7, v2

    #@f
    if-ne v7, p2, :cond_1b

    #@11
    .line 2202
    iget-object v7, p0, Landroid/content/res/Resources;->mCachedXmlBlocks:[Landroid/content/res/XmlBlock;

    #@13
    aget-object v7, v7, v2

    #@15
    invoke-virtual {v7}, Landroid/content/res/XmlBlock;->newParser()Landroid/content/res/XmlResourceParser;

    #@18
    move-result-object v7

    #@19
    monitor-exit v8

    #@1a
    .line 2222
    :goto_1a
    return-object v7

    #@1b
    .line 2198
    :cond_1b
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_9

    #@1e
    .line 2208
    :cond_1e
    iget-object v7, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@20
    invoke-virtual {v7, p3, p1}, Landroid/content/res/AssetManager;->openXmlBlockAsset(ILjava/lang/String;)Landroid/content/res/XmlBlock;

    #@23
    move-result-object v0

    #@24
    .line 2210
    .local v0, block:Landroid/content/res/XmlBlock;
    if-eqz v0, :cond_7e

    #@26
    .line 2211
    iget v7, p0, Landroid/content/res/Resources;->mLastCachedXmlBlockIndex:I

    #@28
    add-int/lit8 v5, v7, 0x1

    #@2a
    .line 2212
    .local v5, pos:I
    if-lt v5, v3, :cond_2d

    #@2c
    const/4 v5, 0x0

    #@2d
    .line 2213
    :cond_2d
    iput v5, p0, Landroid/content/res/Resources;->mLastCachedXmlBlockIndex:I

    #@2f
    .line 2214
    iget-object v7, p0, Landroid/content/res/Resources;->mCachedXmlBlocks:[Landroid/content/res/XmlBlock;

    #@31
    aget-object v4, v7, v5

    #@33
    .line 2215
    .local v4, oldBlock:Landroid/content/res/XmlBlock;
    if-eqz v4, :cond_38

    #@35
    .line 2216
    invoke-virtual {v4}, Landroid/content/res/XmlBlock;->close()V

    #@38
    .line 2218
    :cond_38
    iget-object v7, p0, Landroid/content/res/Resources;->mCachedXmlBlockIds:[I

    #@3a
    aput p2, v7, v5

    #@3c
    .line 2219
    iget-object v7, p0, Landroid/content/res/Resources;->mCachedXmlBlocks:[Landroid/content/res/XmlBlock;

    #@3e
    aput-object v0, v7, v5

    #@40
    .line 2222
    invoke-virtual {v0}, Landroid/content/res/XmlBlock;->newParser()Landroid/content/res/XmlResourceParser;

    #@43
    move-result-object v7

    #@44
    monitor-exit v8

    #@45
    goto :goto_1a

    #@46
    .line 2224
    .end local v0           #block:Landroid/content/res/XmlBlock;
    .end local v2           #i:I
    .end local v3           #num:I
    .end local v4           #oldBlock:Landroid/content/res/XmlBlock;
    .end local v5           #pos:I
    :catchall_46
    move-exception v7

    #@47
    monitor-exit v8
    :try_end_48
    .catchall {:try_start_5 .. :try_end_48} :catchall_46

    #@48
    :try_start_48
    throw v7
    :try_end_49
    .catch Ljava/lang/Exception; {:try_start_48 .. :try_end_49} :catch_49

    #@49
    .line 2225
    :catch_49
    move-exception v1

    #@4a
    .line 2226
    .local v1, e:Ljava/lang/Exception;
    new-instance v6, Landroid/content/res/Resources$NotFoundException;

    #@4c
    new-instance v7, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v8, "File "

    #@53
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    const-string v8, " from xml type "

    #@5d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v7

    #@61
    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v7

    #@65
    const-string v8, " resource ID #0x"

    #@67
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v7

    #@6b
    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@6e
    move-result-object v8

    #@6f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    invoke-direct {v6, v7}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@7a
    .line 2229
    .local v6, rnf:Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v6, v1}, Landroid/content/res/Resources$NotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@7d
    .line 2230
    throw v6

    #@7e
    .line 2224
    .end local v1           #e:Ljava/lang/Exception;
    .end local v6           #rnf:Landroid/content/res/Resources$NotFoundException;
    .restart local v0       #block:Landroid/content/res/XmlBlock;
    .restart local v2       #i:I
    .restart local v3       #num:I
    :cond_7e
    :try_start_7e
    monitor-exit v8
    :try_end_7f
    .catchall {:try_start_7e .. :try_end_7f} :catchall_46

    #@7f
    .line 2234
    .end local v0           #block:Landroid/content/res/XmlBlock;
    .end local v2           #i:I
    .end local v3           #num:I
    :cond_7f
    new-instance v7, Landroid/content/res/Resources$NotFoundException;

    #@81
    new-instance v8, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v9, "File "

    #@88
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v8

    #@8c
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v8

    #@90
    const-string v9, " from xml type "

    #@92
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v8

    #@96
    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v8

    #@9a
    const-string v9, " resource ID #0x"

    #@9c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v8

    #@a0
    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@a3
    move-result-object v9

    #@a4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v8

    #@a8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v8

    #@ac
    invoke-direct {v7, v8}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@af
    throw v7
.end method

.method public final newTheme()Landroid/content/res/Resources$Theme;
    .registers 2

    #@0
    .prologue
    .line 1403
    new-instance v0, Landroid/content/res/Resources$Theme;

    #@2
    invoke-direct {v0, p0}, Landroid/content/res/Resources$Theme;-><init>(Landroid/content/res/Resources;)V

    #@5
    return-object v0
.end method

.method public obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    .registers 10
    .parameter "set"
    .parameter "attrs"

    #@0
    .prologue
    .line 1419
    array-length v1, p2

    #@1
    .line 1420
    .local v1, len:I
    invoke-direct {p0, v1}, Landroid/content/res/Resources;->getCachedStyledAttributes(I)Landroid/content/res/TypedArray;

    #@4
    move-result-object v0

    #@5
    .local v0, array:Landroid/content/res/TypedArray;
    move-object v2, p1

    #@6
    .line 1426
    check-cast v2, Landroid/content/res/XmlBlock$Parser;

    #@8
    .line 1427
    .local v2, parser:Landroid/content/res/XmlBlock$Parser;
    iget-object v3, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@a
    iget v4, v2, Landroid/content/res/XmlBlock$Parser;->mParseState:I

    #@c
    iget-object v5, v0, Landroid/content/res/TypedArray;->mData:[I

    #@e
    iget-object v6, v0, Landroid/content/res/TypedArray;->mIndices:[I

    #@10
    invoke-virtual {v3, v4, p2, v5, v6}, Landroid/content/res/AssetManager;->retrieveAttributes(I[I[I[I)Z

    #@13
    .line 1430
    iput-object p2, v0, Landroid/content/res/TypedArray;->mRsrcs:[I

    #@15
    .line 1431
    iput-object v2, v0, Landroid/content/res/TypedArray;->mXml:Landroid/content/res/XmlBlock$Parser;

    #@17
    .line 1433
    return-object v0
.end method

.method public obtainTypedArray(I)Landroid/content/res/TypedArray;
    .registers 7
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 490
    iget-object v2, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@3
    invoke-virtual {v2, p1}, Landroid/content/res/AssetManager;->getArraySize(I)I

    #@6
    move-result v1

    #@7
    .line 491
    .local v1, len:I
    if-gez v1, :cond_26

    #@9
    .line 492
    new-instance v2, Landroid/content/res/Resources$NotFoundException;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Array resource ID #0x"

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-direct {v2, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@25
    throw v2

    #@26
    .line 496
    :cond_26
    invoke-direct {p0, v1}, Landroid/content/res/Resources;->getCachedStyledAttributes(I)Landroid/content/res/TypedArray;

    #@29
    move-result-object v0

    #@2a
    .line 497
    .local v0, array:Landroid/content/res/TypedArray;
    iget-object v2, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@2c
    iget-object v3, v0, Landroid/content/res/TypedArray;->mData:[I

    #@2e
    invoke-virtual {v2, p1, v3}, Landroid/content/res/AssetManager;->retrieveArray(I[I)I

    #@31
    move-result v2

    #@32
    iput v2, v0, Landroid/content/res/TypedArray;->mLength:I

    #@34
    .line 498
    iget-object v2, v0, Landroid/content/res/TypedArray;->mIndices:[I

    #@36
    aput v4, v2, v4

    #@38
    .line 500
    return-object v0
.end method

.method public openRawResource(I)Ljava/io/InputStream;
    .registers 4
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 959
    iget-object v1, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v1

    #@3
    .line 960
    :try_start_3
    iget-object v0, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    invoke-virtual {p0, p1, v0}, Landroid/content/res/Resources;->openRawResource(ILandroid/util/TypedValue;)Ljava/io/InputStream;

    #@8
    move-result-object v0

    #@9
    monitor-exit v1

    #@a
    return-object v0

    #@b
    .line 961
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public openRawResource(ILandroid/util/TypedValue;)Ljava/io/InputStream;
    .registers 9
    .parameter "id"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 978
    const/4 v2, 0x1

    #@1
    invoke-virtual {p0, p1, p2, v2}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@4
    .line 981
    :try_start_4
    iget-object v2, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@6
    iget v3, p2, Landroid/util/TypedValue;->assetCookie:I

    #@8
    iget-object v4, p2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@a
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    const/4 v5, 0x2

    #@f
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/AssetManager;->openNonAsset(ILjava/lang/String;I)Ljava/io/InputStream;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_12} :catch_14

    #@12
    move-result-object v2

    #@13
    return-object v2

    #@14
    .line 983
    :catch_14
    move-exception v0

    #@15
    .line 984
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "File "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    iget-object v3, p2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@24
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    const-string v3, " from drawable resource ID #0x"

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-direct {v1, v2}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@41
    .line 986
    .local v1, rnf:Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v1, v0}, Landroid/content/res/Resources$NotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@44
    .line 987
    throw v1
.end method

.method public openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;
    .registers 9
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1013
    iget-object v4, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v4

    #@3
    .line 1014
    :try_start_3
    iget-object v2, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@5
    .line 1015
    .local v2, value:Landroid/util/TypedValue;
    const/4 v3, 0x1

    #@6
    invoke-virtual {p0, p1, v2, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_4a

    #@9
    .line 1018
    :try_start_9
    iget-object v3, p0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@b
    iget v5, v2, Landroid/util/TypedValue;->assetCookie:I

    #@d
    iget-object v6, v2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@f
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@12
    move-result-object v6

    #@13
    invoke-virtual {v3, v5, v6}, Landroid/content/res/AssetManager;->openNonAssetFd(ILjava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_16
    .catchall {:try_start_9 .. :try_end_16} :catchall_4a
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_16} :catch_19

    #@16
    move-result-object v3

    #@17
    :try_start_17
    monitor-exit v4

    #@18
    return-object v3

    #@19
    .line 1020
    :catch_19
    move-exception v0

    #@1a
    .line 1021
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Landroid/content/res/Resources$NotFoundException;

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "File "

    #@23
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    iget-object v5, v2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@29
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v5, " from drawable resource ID #0x"

    #@33
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-direct {v1, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@46
    .line 1025
    .local v1, rnf:Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v1, v0}, Landroid/content/res/Resources$NotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@49
    .line 1026
    throw v1

    #@4a
    .line 1029
    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #rnf:Landroid/content/res/Resources$NotFoundException;
    .end local v2           #value:Landroid/util/TypedValue;
    :catchall_4a
    move-exception v3

    #@4b
    monitor-exit v4
    :try_end_4c
    .catchall {:try_start_17 .. :try_end_4c} :catchall_4a

    #@4c
    throw v3
.end method

.method public parseBundleExtra(Ljava/lang/String;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .registers 12
    .parameter "tagName"
    .parameter "attrs"
    .parameter "outBundle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1814
    sget-object v6, Lcom/android/internal/R$styleable;->Extra:[I

    #@4
    invoke-virtual {p0, p2, v6}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@7
    move-result-object v2

    #@8
    .line 1817
    .local v2, sa:Landroid/content/res/TypedArray;
    invoke-virtual {v2, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 1819
    .local v1, name:Ljava/lang/String;
    if-nez v1, :cond_38

    #@e
    .line 1820
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    #@11
    .line 1821
    new-instance v4, Lorg/xmlpull/v1/XmlPullParserException;

    #@13
    new-instance v5, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v6, "<"

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    const-string v6, "> requires an android:name attribute at "

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-interface {p2}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    invoke-direct {v4, v5}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@37
    throw v4

    #@38
    .line 1826
    :cond_38
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@3b
    move-result-object v3

    #@3c
    .line 1828
    .local v3, v:Landroid/util/TypedValue;
    if-eqz v3, :cond_a7

    #@3e
    .line 1829
    iget v6, v3, Landroid/util/TypedValue;->type:I

    #@40
    const/4 v7, 0x3

    #@41
    if-ne v6, v7, :cond_4e

    #@43
    .line 1830
    invoke-virtual {v3}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@46
    move-result-object v0

    #@47
    .line 1831
    .local v0, cs:Ljava/lang/CharSequence;
    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    #@4a
    .line 1852
    .end local v0           #cs:Ljava/lang/CharSequence;
    :goto_4a
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    #@4d
    .line 1853
    return-void

    #@4e
    .line 1832
    :cond_4e
    iget v6, v3, Landroid/util/TypedValue;->type:I

    #@50
    const/16 v7, 0x12

    #@52
    if-ne v6, v7, :cond_5e

    #@54
    .line 1833
    iget v6, v3, Landroid/util/TypedValue;->data:I

    #@56
    if-eqz v6, :cond_5c

    #@58
    :goto_58
    invoke-virtual {p3, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@5b
    goto :goto_4a

    #@5c
    :cond_5c
    move v4, v5

    #@5d
    goto :goto_58

    #@5e
    .line 1834
    :cond_5e
    iget v4, v3, Landroid/util/TypedValue;->type:I

    #@60
    const/16 v5, 0x10

    #@62
    if-lt v4, v5, :cond_70

    #@64
    iget v4, v3, Landroid/util/TypedValue;->type:I

    #@66
    const/16 v5, 0x1f

    #@68
    if-gt v4, v5, :cond_70

    #@6a
    .line 1836
    iget v4, v3, Landroid/util/TypedValue;->data:I

    #@6c
    invoke-virtual {p3, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@6f
    goto :goto_4a

    #@70
    .line 1837
    :cond_70
    iget v4, v3, Landroid/util/TypedValue;->type:I

    #@72
    const/4 v5, 0x4

    #@73
    if-ne v4, v5, :cond_7d

    #@75
    .line 1838
    invoke-virtual {v3}, Landroid/util/TypedValue;->getFloat()F

    #@78
    move-result v4

    #@79
    invoke-virtual {p3, v1, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@7c
    goto :goto_4a

    #@7d
    .line 1840
    :cond_7d
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    #@80
    .line 1841
    new-instance v4, Lorg/xmlpull/v1/XmlPullParserException;

    #@82
    new-instance v5, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v6, "<"

    #@89
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v5

    #@8d
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    const-string v6, "> only supports string, integer, float, color, and boolean at "

    #@93
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v5

    #@97
    invoke-interface {p2}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v5

    #@9f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v5

    #@a3
    invoke-direct {v4, v5}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@a6
    throw v4

    #@a7
    .line 1846
    :cond_a7
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    #@aa
    .line 1847
    new-instance v4, Lorg/xmlpull/v1/XmlPullParserException;

    #@ac
    new-instance v5, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v6, "<"

    #@b3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v5

    #@b7
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v5

    #@bb
    const-string v6, "> requires an android:value or android:resource attribute at "

    #@bd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v5

    #@c1
    invoke-interface {p2}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@c4
    move-result-object v6

    #@c5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v5

    #@c9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v5

    #@cd
    invoke-direct {v4, v5}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@d0
    throw v4
.end method

.method public parseBundleExtras(Landroid/content/res/XmlResourceParser;Landroid/os/Bundle;)V
    .registers 8
    .parameter "parser"
    .parameter "outBundle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    .line 1772
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@4
    move-result v1

    #@5
    .line 1775
    .local v1, outerDepth:I
    :cond_5
    :goto_5
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->next()I

    #@8
    move-result v2

    #@9
    .local v2, type:I
    const/4 v3, 0x1

    #@a
    if-eq v2, v3, :cond_32

    #@c
    if-ne v2, v4, :cond_14

    #@e
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@11
    move-result v3

    #@12
    if-le v3, v1, :cond_32

    #@14
    .line 1776
    :cond_14
    if-eq v2, v4, :cond_5

    #@16
    const/4 v3, 0x4

    #@17
    if-eq v2, v3, :cond_5

    #@19
    .line 1780
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 1781
    .local v0, nodeName:Ljava/lang/String;
    const-string v3, "extra"

    #@1f
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_2e

    #@25
    .line 1782
    const-string v3, "extra"

    #@27
    invoke-virtual {p0, v3, p1, p2}, Landroid/content/res/Resources;->parseBundleExtra(Ljava/lang/String;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    #@2a
    .line 1783
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@2d
    goto :goto_5

    #@2e
    .line 1786
    :cond_2e
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@31
    goto :goto_5

    #@32
    .line 1789
    .end local v0           #nodeName:Ljava/lang/String;
    :cond_32
    return-void
.end method

.method public setCompatibilityInfo(Landroid/content/res/CompatibilityInfo;)V
    .registers 4
    .parameter "ci"

    #@0
    .prologue
    .line 1648
    iput-object p1, p0, Landroid/content/res/Resources;->mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@2
    .line 1649
    iget-object v0, p0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@4
    iget-object v1, p0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    #@9
    .line 1650
    return-void
.end method

.method public setThemeIconManager(Landroid/content/thm/ThemeIconManager;)V
    .registers 4
    .parameter "tm"

    #@0
    .prologue
    .line 2292
    iget-object v1, p0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@2
    monitor-enter v1

    #@3
    .line 2293
    :try_start_3
    iput-object p1, p0, Landroid/content/res/Resources;->mThemeIconManager:Landroid/content/thm/ThemeIconManager;

    #@5
    .line 2294
    monitor-exit v1

    #@6
    .line 2295
    return-void

    #@7
    .line 2294
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public final startPreloading()V
    .registers 4

    #@0
    .prologue
    .line 1888
    sget-object v1, Landroid/content/res/Resources;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1889
    :try_start_3
    sget-boolean v0, Landroid/content/res/Resources;->sPreloaded:Z

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 1890
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v2, "Resources already preloaded"

    #@b
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 1897
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0

    #@12
    .line 1892
    :cond_12
    const/4 v0, 0x1

    #@13
    :try_start_13
    sput-boolean v0, Landroid/content/res/Resources;->sPreloaded:Z

    #@15
    .line 1893
    const/4 v0, 0x1

    #@16
    iput-boolean v0, p0, Landroid/content/res/Resources;->mPreloading:Z

    #@18
    .line 1894
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@1a
    sput v0, Landroid/content/res/Resources;->sPreloadedDensity:I

    #@1c
    .line 1895
    iget-object v0, p0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@1e
    sget v2, Landroid/content/res/Resources;->sPreloadedDensity:I

    #@20
    iput v2, v0, Landroid/content/res/Configuration;->densityDpi:I

    #@22
    .line 1896
    const/4 v0, 0x0

    #@23
    const/4 v2, 0x0

    #@24
    invoke-virtual {p0, v0, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    #@27
    .line 1897
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_13 .. :try_end_28} :catchall_f

    #@28
    .line 1898
    return-void
.end method

.method public updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V
    .registers 4
    .parameter "config"
    .parameter "metrics"

    #@0
    .prologue
    .line 1441
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@4
    .line 1442
    return-void
.end method

.method public updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V
    .registers 27
    .parameter "config"
    .parameter "metrics"
    .parameter "compat"

    #@0
    .prologue
    .line 1449
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/content/res/Resources;->mTmpValue:Landroid/util/TypedValue;

    #@4
    move-object/from16 v22, v0

    #@6
    monitor-enter v22

    #@7
    .line 1456
    if-eqz p3, :cond_f

    #@9
    .line 1457
    :try_start_9
    move-object/from16 v0, p3

    #@b
    move-object/from16 v1, p0

    #@d
    iput-object v0, v1, Landroid/content/res/Resources;->mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@f
    .line 1459
    :cond_f
    if-eqz p2, :cond_1a

    #@11
    .line 1460
    move-object/from16 v0, p0

    #@13
    iget-object v2, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@15
    move-object/from16 v0, p2

    #@17
    invoke-virtual {v2, v0}, Landroid/util/DisplayMetrics;->setTo(Landroid/util/DisplayMetrics;)V

    #@1a
    .line 1471
    :cond_1a
    move-object/from16 v0, p0

    #@1c
    iget-object v2, v0, Landroid/content/res/Resources;->mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@1e
    if-eqz v2, :cond_2b

    #@20
    .line 1472
    move-object/from16 v0, p0

    #@22
    iget-object v2, v0, Landroid/content/res/Resources;->mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@24
    move-object/from16 v0, p0

    #@26
    iget-object v3, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@28
    invoke-virtual {v2, v3}, Landroid/content/res/CompatibilityInfo;->applyToDisplayMetrics(Landroid/util/DisplayMetrics;)V

    #@2b
    .line 1474
    :cond_2b
    const v20, 0xfffffff

    #@2e
    .line 1475
    .local v20, configChanges:I
    if-eqz p1, :cond_8b

    #@30
    .line 1476
    move-object/from16 v0, p0

    #@32
    iget-object v2, v0, Landroid/content/res/Resources;->mTmpConfig:Landroid/content/res/Configuration;

    #@34
    move-object/from16 v0, p1

    #@36
    invoke-virtual {v2, v0}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    #@39
    .line 1477
    move-object/from16 v0, p1

    #@3b
    iget v0, v0, Landroid/content/res/Configuration;->densityDpi:I

    #@3d
    move/from16 v21, v0

    #@3f
    .line 1478
    .local v21, density:I
    if-nez v21, :cond_49

    #@41
    .line 1479
    move-object/from16 v0, p0

    #@43
    iget-object v2, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@45
    iget v0, v2, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@47
    move/from16 v21, v0

    #@49
    .line 1481
    :cond_49
    move-object/from16 v0, p0

    #@4b
    iget-object v2, v0, Landroid/content/res/Resources;->mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@4d
    if-eqz v2, :cond_5c

    #@4f
    .line 1482
    move-object/from16 v0, p0

    #@51
    iget-object v2, v0, Landroid/content/res/Resources;->mCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@53
    move-object/from16 v0, p0

    #@55
    iget-object v3, v0, Landroid/content/res/Resources;->mTmpConfig:Landroid/content/res/Configuration;

    #@57
    move/from16 v0, v21

    #@59
    invoke-virtual {v2, v0, v3}, Landroid/content/res/CompatibilityInfo;->applyToConfiguration(ILandroid/content/res/Configuration;)V

    #@5c
    .line 1484
    :cond_5c
    move-object/from16 v0, p0

    #@5e
    iget-object v2, v0, Landroid/content/res/Resources;->mTmpConfig:Landroid/content/res/Configuration;

    #@60
    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@62
    if-nez v2, :cond_7b

    #@64
    .line 1485
    move-object/from16 v0, p0

    #@66
    iget-object v2, v0, Landroid/content/res/Resources;->mTmpConfig:Landroid/content/res/Configuration;

    #@68
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@6b
    move-result-object v3

    #@6c
    iput-object v3, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@6e
    .line 1486
    move-object/from16 v0, p0

    #@70
    iget-object v2, v0, Landroid/content/res/Resources;->mTmpConfig:Landroid/content/res/Configuration;

    #@72
    move-object/from16 v0, p0

    #@74
    iget-object v3, v0, Landroid/content/res/Resources;->mTmpConfig:Landroid/content/res/Configuration;

    #@76
    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@78
    invoke-virtual {v2, v3}, Landroid/content/res/Configuration;->setLayoutDirection(Ljava/util/Locale;)V

    #@7b
    .line 1488
    :cond_7b
    move-object/from16 v0, p0

    #@7d
    iget-object v2, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@7f
    move-object/from16 v0, p0

    #@81
    iget-object v3, v0, Landroid/content/res/Resources;->mTmpConfig:Landroid/content/res/Configuration;

    #@83
    invoke-virtual {v2, v3}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    #@86
    move-result v20

    #@87
    .line 1489
    invoke-static/range {v20 .. v20}, Landroid/content/pm/ActivityInfo;->activityInfoConfigToNative(I)I

    #@8a
    move-result v20

    #@8b
    .line 1491
    .end local v21           #density:I
    :cond_8b
    move-object/from16 v0, p0

    #@8d
    iget-object v2, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@8f
    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@91
    if-nez v2, :cond_aa

    #@93
    .line 1492
    move-object/from16 v0, p0

    #@95
    iget-object v2, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@97
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@9a
    move-result-object v3

    #@9b
    iput-object v3, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@9d
    .line 1493
    move-object/from16 v0, p0

    #@9f
    iget-object v2, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@a1
    move-object/from16 v0, p0

    #@a3
    iget-object v3, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@a5
    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@a7
    invoke-virtual {v2, v3}, Landroid/content/res/Configuration;->setLayoutDirection(Ljava/util/Locale;)V

    #@aa
    .line 1495
    :cond_aa
    move-object/from16 v0, p0

    #@ac
    iget-object v2, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@ae
    iget v2, v2, Landroid/content/res/Configuration;->densityDpi:I

    #@b0
    if-eqz v2, :cond_cf

    #@b2
    .line 1496
    move-object/from16 v0, p0

    #@b4
    iget-object v2, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@b6
    move-object/from16 v0, p0

    #@b8
    iget-object v3, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@ba
    iget v3, v3, Landroid/content/res/Configuration;->densityDpi:I

    #@bc
    iput v3, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    #@be
    .line 1497
    move-object/from16 v0, p0

    #@c0
    iget-object v2, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@c2
    move-object/from16 v0, p0

    #@c4
    iget-object v3, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@c6
    iget v3, v3, Landroid/content/res/Configuration;->densityDpi:I

    #@c8
    int-to-float v3, v3

    #@c9
    const v4, 0x3bcccccd

    #@cc
    mul-float/2addr v3, v4

    #@cd
    iput v3, v2, Landroid/util/DisplayMetrics;->density:F

    #@cf
    .line 1499
    :cond_cf
    move-object/from16 v0, p0

    #@d1
    iget-object v2, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@d3
    move-object/from16 v0, p0

    #@d5
    iget-object v3, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@d7
    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    #@d9
    move-object/from16 v0, p0

    #@db
    iget-object v4, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@dd
    iget v4, v4, Landroid/content/res/Configuration;->fontScale:F

    #@df
    mul-float/2addr v3, v4

    #@e0
    iput v3, v2, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@e2
    .line 1501
    const/4 v5, 0x0

    #@e3
    .line 1502
    .local v5, locale:Ljava/lang/String;
    move-object/from16 v0, p0

    #@e5
    iget-object v2, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@e7
    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@e9
    if-eqz v2, :cond_122

    #@eb
    .line 1503
    move-object/from16 v0, p0

    #@ed
    iget-object v2, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@ef
    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@f1
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@f4
    move-result-object v5

    #@f5
    .line 1504
    move-object/from16 v0, p0

    #@f7
    iget-object v2, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@f9
    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@fb
    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@fe
    move-result-object v2

    #@ff
    if-eqz v2, :cond_122

    #@101
    .line 1505
    new-instance v2, Ljava/lang/StringBuilder;

    #@103
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@106
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v2

    #@10a
    const-string v3, "-"

    #@10c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v2

    #@110
    move-object/from16 v0, p0

    #@112
    iget-object v3, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@114
    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@116
    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@119
    move-result-object v3

    #@11a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v2

    #@11e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v5

    #@122
    .line 1509
    :cond_122
    move-object/from16 v0, p0

    #@124
    iget-object v2, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@126
    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    #@128
    move-object/from16 v0, p0

    #@12a
    iget-object v3, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@12c
    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    #@12e
    if-lt v2, v3, :cond_1ea

    #@130
    .line 1510
    move-object/from16 v0, p0

    #@132
    iget-object v2, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@134
    iget v12, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    #@136
    .line 1511
    .local v12, width:I
    move-object/from16 v0, p0

    #@138
    iget-object v2, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@13a
    iget v13, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    #@13c
    .line 1518
    .local v13, height:I
    :goto_13c
    move-object/from16 v0, p0

    #@13e
    iget-object v2, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@140
    iget v10, v2, Landroid/content/res/Configuration;->keyboardHidden:I

    #@142
    .line 1519
    .local v10, keyboardHidden:I
    const/4 v2, 0x1

    #@143
    if-ne v10, v2, :cond_14f

    #@145
    move-object/from16 v0, p0

    #@147
    iget-object v2, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@149
    iget v2, v2, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@14b
    const/4 v3, 0x2

    #@14c
    if-ne v2, v3, :cond_14f

    #@14e
    .line 1522
    const/4 v10, 0x3

    #@14f
    .line 1524
    :cond_14f
    move-object/from16 v0, p0

    #@151
    iget-object v2, v0, Landroid/content/res/Resources;->mAssets:Landroid/content/res/AssetManager;

    #@153
    move-object/from16 v0, p0

    #@155
    iget-object v3, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@157
    iget v3, v3, Landroid/content/res/Configuration;->mcc:I

    #@159
    move-object/from16 v0, p0

    #@15b
    iget-object v4, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@15d
    iget v4, v4, Landroid/content/res/Configuration;->mnc:I

    #@15f
    move-object/from16 v0, p0

    #@161
    iget-object v6, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@163
    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    #@165
    move-object/from16 v0, p0

    #@167
    iget-object v7, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@169
    iget v7, v7, Landroid/content/res/Configuration;->touchscreen:I

    #@16b
    move-object/from16 v0, p0

    #@16d
    iget-object v8, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@16f
    iget v8, v8, Landroid/content/res/Configuration;->densityDpi:I

    #@171
    move-object/from16 v0, p0

    #@173
    iget-object v9, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@175
    iget v9, v9, Landroid/content/res/Configuration;->keyboard:I

    #@177
    move-object/from16 v0, p0

    #@179
    iget-object v11, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@17b
    iget v11, v11, Landroid/content/res/Configuration;->navigation:I

    #@17d
    move-object/from16 v0, p0

    #@17f
    iget-object v14, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@181
    iget v14, v14, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@183
    move-object/from16 v0, p0

    #@185
    iget-object v15, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@187
    iget v15, v15, Landroid/content/res/Configuration;->screenWidthDp:I

    #@189
    move-object/from16 v0, p0

    #@18b
    iget-object v0, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@18d
    move-object/from16 v16, v0

    #@18f
    move-object/from16 v0, v16

    #@191
    iget v0, v0, Landroid/content/res/Configuration;->screenHeightDp:I

    #@193
    move/from16 v16, v0

    #@195
    move-object/from16 v0, p0

    #@197
    iget-object v0, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@199
    move-object/from16 v17, v0

    #@19b
    move-object/from16 v0, v17

    #@19d
    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    #@19f
    move/from16 v17, v0

    #@1a1
    move-object/from16 v0, p0

    #@1a3
    iget-object v0, v0, Landroid/content/res/Resources;->mConfiguration:Landroid/content/res/Configuration;

    #@1a5
    move-object/from16 v18, v0

    #@1a7
    move-object/from16 v0, v18

    #@1a9
    iget v0, v0, Landroid/content/res/Configuration;->uiMode:I

    #@1ab
    move/from16 v18, v0

    #@1ad
    sget v19, Landroid/os/Build$VERSION;->RESOURCES_SDK_INT:I

    #@1af
    invoke-virtual/range {v2 .. v19}, Landroid/content/res/AssetManager;->setConfiguration(IILjava/lang/String;IIIIIIIIIIIIII)V

    #@1b2
    .line 1539
    move-object/from16 v0, p0

    #@1b4
    iget-object v2, v0, Landroid/content/res/Resources;->mDrawableCache:Landroid/util/LongSparseArray;

    #@1b6
    move-object/from16 v0, p0

    #@1b8
    move/from16 v1, v20

    #@1ba
    invoke-direct {v0, v2, v1}, Landroid/content/res/Resources;->clearDrawableCache(Landroid/util/LongSparseArray;I)V

    #@1bd
    .line 1540
    move-object/from16 v0, p0

    #@1bf
    iget-object v2, v0, Landroid/content/res/Resources;->mColorDrawableCache:Landroid/util/LongSparseArray;

    #@1c1
    move-object/from16 v0, p0

    #@1c3
    move/from16 v1, v20

    #@1c5
    invoke-direct {v0, v2, v1}, Landroid/content/res/Resources;->clearDrawableCache(Landroid/util/LongSparseArray;I)V

    #@1c8
    .line 1542
    move-object/from16 v0, p0

    #@1ca
    iget-object v2, v0, Landroid/content/res/Resources;->mColorStateListCache:Landroid/util/LongSparseArray;

    #@1cc
    invoke-virtual {v2}, Landroid/util/LongSparseArray;->clear()V

    #@1cf
    .line 1544
    invoke-virtual/range {p0 .. p0}, Landroid/content/res/Resources;->flushLayoutCache()V

    #@1d2
    .line 1545
    monitor-exit v22
    :try_end_1d3
    .catchall {:try_start_9 .. :try_end_1d3} :catchall_1f8

    #@1d3
    .line 1546
    sget-object v3, Landroid/content/res/Resources;->mSync:Ljava/lang/Object;

    #@1d5
    monitor-enter v3

    #@1d6
    .line 1547
    :try_start_1d6
    move-object/from16 v0, p0

    #@1d8
    iget-object v2, v0, Landroid/content/res/Resources;->mPluralRule:Llibcore/icu/NativePluralRules;

    #@1da
    if-eqz v2, :cond_1e8

    #@1dc
    .line 1548
    move-object/from16 v0, p1

    #@1de
    iget-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@1e0
    invoke-static {v2}, Llibcore/icu/NativePluralRules;->forLocale(Ljava/util/Locale;)Llibcore/icu/NativePluralRules;

    #@1e3
    move-result-object v2

    #@1e4
    move-object/from16 v0, p0

    #@1e6
    iput-object v2, v0, Landroid/content/res/Resources;->mPluralRule:Llibcore/icu/NativePluralRules;

    #@1e8
    .line 1550
    :cond_1e8
    monitor-exit v3
    :try_end_1e9
    .catchall {:try_start_1d6 .. :try_end_1e9} :catchall_1fb

    #@1e9
    .line 1551
    return-void

    #@1ea
    .line 1514
    .end local v10           #keyboardHidden:I
    .end local v12           #width:I
    .end local v13           #height:I
    :cond_1ea
    :try_start_1ea
    move-object/from16 v0, p0

    #@1ec
    iget-object v2, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@1ee
    iget v12, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    #@1f0
    .line 1516
    .restart local v12       #width:I
    move-object/from16 v0, p0

    #@1f2
    iget-object v2, v0, Landroid/content/res/Resources;->mMetrics:Landroid/util/DisplayMetrics;

    #@1f4
    iget v13, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    #@1f6
    .restart local v13       #height:I
    goto/16 :goto_13c

    #@1f8
    .line 1545
    .end local v5           #locale:Ljava/lang/String;
    .end local v12           #width:I
    .end local v13           #height:I
    .end local v20           #configChanges:I
    :catchall_1f8
    move-exception v2

    #@1f9
    monitor-exit v22
    :try_end_1fa
    .catchall {:try_start_1ea .. :try_end_1fa} :catchall_1f8

    #@1fa
    throw v2

    #@1fb
    .line 1550
    .restart local v5       #locale:Ljava/lang/String;
    .restart local v10       #keyboardHidden:I
    .restart local v12       #width:I
    .restart local v13       #height:I
    .restart local v20       #configChanges:I
    :catchall_1fb
    move-exception v2

    #@1fc
    :try_start_1fc
    monitor-exit v3
    :try_end_1fd
    .catchall {:try_start_1fc .. :try_end_1fd} :catchall_1fb

    #@1fd
    throw v2
.end method
