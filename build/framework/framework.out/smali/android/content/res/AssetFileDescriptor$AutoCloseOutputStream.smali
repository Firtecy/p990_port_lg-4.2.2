.class public Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;
.super Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;
.source "AssetFileDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/res/AssetFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AutoCloseOutputStream"
.end annotation


# instance fields
.field private mRemaining:J


# direct methods
.method public constructor <init>(Landroid/content/res/AssetFileDescriptor;)V
    .registers 6
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 262
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    #@7
    .line 263
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@e
    move-result-wide v1

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/os/ParcelFileDescriptor;->seekTo(J)J

    #@12
    move-result-wide v0

    #@13
    const-wide/16 v2, 0x0

    #@15
    cmp-long v0, v0, v2

    #@17
    if-gez v0, :cond_21

    #@19
    .line 264
    new-instance v0, Ljava/io/IOException;

    #@1b
    const-string v1, "Unable to seek"

    #@1d
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 266
    :cond_21
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    #@24
    move-result-wide v0

    #@25
    long-to-int v0, v0

    #@26
    int-to-long v0, v0

    #@27
    iput-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@29
    .line 267
    return-void
.end method


# virtual methods
.method public write(I)V
    .registers 6
    .parameter "oneByte"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 298
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@4
    cmp-long v0, v0, v2

    #@6
    if-ltz v0, :cond_1a

    #@8
    .line 299
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@a
    cmp-long v0, v0, v2

    #@c
    if-nez v0, :cond_f

    #@e
    .line 306
    :goto_e
    return-void

    #@f
    .line 300
    :cond_f
    invoke-super {p0, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->write(I)V

    #@12
    .line 301
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@14
    const-wide/16 v2, 0x1

    #@16
    sub-long/2addr v0, v2

    #@17
    iput-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@19
    goto :goto_e

    #@1a
    .line 305
    :cond_1a
    invoke-super {p0, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->write(I)V

    #@1d
    goto :goto_e
.end method

.method public write([B)V
    .registers 7
    .parameter "buffer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 284
    iget-wide v1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@4
    cmp-long v1, v1, v3

    #@6
    if-ltz v1, :cond_24

    #@8
    .line 285
    iget-wide v1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@a
    cmp-long v1, v1, v3

    #@c
    if-nez v1, :cond_f

    #@e
    .line 294
    :goto_e
    return-void

    #@f
    .line 286
    :cond_f
    array-length v0, p1

    #@10
    .line 287
    .local v0, count:I
    int-to-long v1, v0

    #@11
    iget-wide v3, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@13
    cmp-long v1, v1, v3

    #@15
    if-lez v1, :cond_1a

    #@17
    iget-wide v1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@19
    long-to-int v0, v1

    #@1a
    .line 288
    :cond_1a
    invoke-super {p0, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->write([B)V

    #@1d
    .line 289
    iget-wide v1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@1f
    int-to-long v3, v0

    #@20
    sub-long/2addr v1, v3

    #@21
    iput-wide v1, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@23
    goto :goto_e

    #@24
    .line 293
    .end local v0           #count:I
    :cond_24
    invoke-super {p0, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->write([B)V

    #@27
    goto :goto_e
.end method

.method public write([BII)V
    .registers 8
    .parameter "buffer"
    .parameter "offset"
    .parameter "count"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 271
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@4
    cmp-long v0, v0, v2

    #@6
    if-ltz v0, :cond_23

    #@8
    .line 272
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@a
    cmp-long v0, v0, v2

    #@c
    if-nez v0, :cond_f

    #@e
    .line 280
    :goto_e
    return-void

    #@f
    .line 273
    :cond_f
    int-to-long v0, p3

    #@10
    iget-wide v2, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@12
    cmp-long v0, v0, v2

    #@14
    if-lez v0, :cond_19

    #@16
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@18
    long-to-int p3, v0

    #@19
    .line 274
    :cond_19
    invoke-super {p0, p1, p2, p3}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->write([BII)V

    #@1c
    .line 275
    iget-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@1e
    int-to-long v2, p3

    #@1f
    sub-long/2addr v0, v2

    #@20
    iput-wide v0, p0, Landroid/content/res/AssetFileDescriptor$AutoCloseOutputStream;->mRemaining:J

    #@22
    goto :goto_e

    #@23
    .line 279
    :cond_23
    invoke-super {p0, p1, p2, p3}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->write([BII)V

    #@26
    goto :goto_e
.end method
