.class final Landroid/content/res/XmlBlock;
.super Ljava/lang/Object;
.source "XmlBlock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/res/XmlBlock$Parser;
    }
.end annotation


# static fields
.field private static final DEBUG:Z


# instance fields
.field private final mAssets:Landroid/content/res/AssetManager;

.field private final mNative:I

.field private mOpen:Z

.field private mOpenCount:I

.field final mStrings:Landroid/content/res/StringBlock;


# direct methods
.method constructor <init>(Landroid/content/res/AssetManager;I)V
    .registers 6
    .parameter "assets"
    .parameter "xmlBlock"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 479
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 488
    iput-boolean v0, p0, Landroid/content/res/XmlBlock;->mOpen:Z

    #@6
    .line 489
    iput v0, p0, Landroid/content/res/XmlBlock;->mOpenCount:I

    #@8
    .line 480
    iput-object p1, p0, Landroid/content/res/XmlBlock;->mAssets:Landroid/content/res/AssetManager;

    #@a
    .line 481
    iput p2, p0, Landroid/content/res/XmlBlock;->mNative:I

    #@c
    .line 482
    new-instance v0, Landroid/content/res/StringBlock;

    #@e
    invoke-static {p2}, Landroid/content/res/XmlBlock;->nativeGetStringBlock(I)I

    #@11
    move-result v1

    #@12
    const/4 v2, 0x0

    #@13
    invoke-direct {v0, v1, v2}, Landroid/content/res/StringBlock;-><init>(IZ)V

    #@16
    iput-object v0, p0, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@18
    .line 483
    return-void
.end method

.method public constructor <init>([B)V
    .registers 5
    .parameter "data"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 37
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 488
    iput-boolean v0, p0, Landroid/content/res/XmlBlock;->mOpen:Z

    #@7
    .line 489
    iput v0, p0, Landroid/content/res/XmlBlock;->mOpenCount:I

    #@9
    .line 38
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/content/res/XmlBlock;->mAssets:Landroid/content/res/AssetManager;

    #@c
    .line 39
    array-length v0, p1

    #@d
    invoke-static {p1, v2, v0}, Landroid/content/res/XmlBlock;->nativeCreate([BII)I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/content/res/XmlBlock;->mNative:I

    #@13
    .line 40
    new-instance v0, Landroid/content/res/StringBlock;

    #@15
    iget v1, p0, Landroid/content/res/XmlBlock;->mNative:I

    #@17
    invoke-static {v1}, Landroid/content/res/XmlBlock;->nativeGetStringBlock(I)I

    #@1a
    move-result v1

    #@1b
    invoke-direct {v0, v1, v2}, Landroid/content/res/StringBlock;-><init>(IZ)V

    #@1e
    iput-object v0, p0, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@20
    .line 41
    return-void
.end method

.method public constructor <init>([BII)V
    .registers 7
    .parameter "data"
    .parameter "offset"
    .parameter "size"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 488
    iput-boolean v0, p0, Landroid/content/res/XmlBlock;->mOpen:Z

    #@6
    .line 489
    iput v0, p0, Landroid/content/res/XmlBlock;->mOpenCount:I

    #@8
    .line 44
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Landroid/content/res/XmlBlock;->mAssets:Landroid/content/res/AssetManager;

    #@b
    .line 45
    invoke-static {p1, p2, p3}, Landroid/content/res/XmlBlock;->nativeCreate([BII)I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/content/res/XmlBlock;->mNative:I

    #@11
    .line 46
    new-instance v0, Landroid/content/res/StringBlock;

    #@13
    iget v1, p0, Landroid/content/res/XmlBlock;->mNative:I

    #@15
    invoke-static {v1}, Landroid/content/res/XmlBlock;->nativeGetStringBlock(I)I

    #@18
    move-result v1

    #@19
    const/4 v2, 0x0

    #@1a
    invoke-direct {v0, v1, v2}, Landroid/content/res/StringBlock;-><init>(IZ)V

    #@1d
    iput-object v0, p0, Landroid/content/res/XmlBlock;->mStrings:Landroid/content/res/StringBlock;

    #@1f
    .line 47
    return-void
.end method

.method static synthetic access$008(Landroid/content/res/XmlBlock;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget v0, p0, Landroid/content/res/XmlBlock;->mOpenCount:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Landroid/content/res/XmlBlock;->mOpenCount:I

    #@6
    return v0
.end method

.method static synthetic access$100(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-static {p0}, Landroid/content/res/XmlBlock;->nativeGetText(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1000(ILjava/lang/String;Ljava/lang/String;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 34
    invoke-static {p0, p1, p2}, Landroid/content/res/XmlBlock;->nativeGetAttributeIndex(ILjava/lang/String;Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(II)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    invoke-static {p0, p1}, Landroid/content/res/XmlBlock;->nativeGetAttributeResource(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1200(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-static {p0}, Landroid/content/res/XmlBlock;->nativeGetIdAttribute(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1300(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-static {p0}, Landroid/content/res/XmlBlock;->nativeGetClassAttribute(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1400(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-static {p0}, Landroid/content/res/XmlBlock;->nativeGetStyleAttribute(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1500(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-static {p0}, Landroid/content/res/XmlBlock;->nativeDestroyParseState(I)V

    #@3
    return-void
.end method

.method static synthetic access$1600(Landroid/content/res/XmlBlock;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/res/XmlBlock;->decOpenCountLocked()V

    #@3
    return-void
.end method

.method static synthetic access$200(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-static {p0}, Landroid/content/res/XmlBlock;->nativeGetLineNumber(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-static {p0}, Landroid/content/res/XmlBlock;->nativeGetNamespace(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(II)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    invoke-static {p0, p1}, Landroid/content/res/XmlBlock;->nativeGetAttributeNamespace(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(II)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    invoke-static {p0, p1}, Landroid/content/res/XmlBlock;->nativeGetAttributeName(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-static {p0}, Landroid/content/res/XmlBlock;->nativeGetAttributeCount(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(II)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    invoke-static {p0, p1}, Landroid/content/res/XmlBlock;->nativeGetAttributeStringValue(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$800(II)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    invoke-static {p0, p1}, Landroid/content/res/XmlBlock;->nativeGetAttributeDataType(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$900(II)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    invoke-static {p0, p1}, Landroid/content/res/XmlBlock;->nativeGetAttributeData(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private decOpenCountLocked()V
    .registers 3

    #@0
    .prologue
    .line 59
    iget v0, p0, Landroid/content/res/XmlBlock;->mOpenCount:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Landroid/content/res/XmlBlock;->mOpenCount:I

    #@6
    .line 60
    iget v0, p0, Landroid/content/res/XmlBlock;->mOpenCount:I

    #@8
    if-nez v0, :cond_1c

    #@a
    .line 61
    iget v0, p0, Landroid/content/res/XmlBlock;->mNative:I

    #@c
    invoke-static {v0}, Landroid/content/res/XmlBlock;->nativeDestroy(I)V

    #@f
    .line 62
    iget-object v0, p0, Landroid/content/res/XmlBlock;->mAssets:Landroid/content/res/AssetManager;

    #@11
    if-eqz v0, :cond_1c

    #@13
    .line 63
    iget-object v0, p0, Landroid/content/res/XmlBlock;->mAssets:Landroid/content/res/AssetManager;

    #@15
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@18
    move-result v1

    #@19
    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->xmlBlockGone(I)V

    #@1c
    .line 66
    :cond_1c
    return-void
.end method

.method private static final native nativeCreate([BII)I
.end method

.method private static final native nativeCreateParseState(I)I
.end method

.method private static final native nativeDestroy(I)V
.end method

.method private static final native nativeDestroyParseState(I)V
.end method

.method private static final native nativeGetAttributeCount(I)I
.end method

.method private static final native nativeGetAttributeData(II)I
.end method

.method private static final native nativeGetAttributeDataType(II)I
.end method

.method private static final native nativeGetAttributeIndex(ILjava/lang/String;Ljava/lang/String;)I
.end method

.method private static final native nativeGetAttributeName(II)I
.end method

.method private static final native nativeGetAttributeNamespace(II)I
.end method

.method private static final native nativeGetAttributeResource(II)I
.end method

.method private static final native nativeGetAttributeStringValue(II)I
.end method

.method private static final native nativeGetClassAttribute(I)I
.end method

.method private static final native nativeGetIdAttribute(I)I
.end method

.method private static final native nativeGetLineNumber(I)I
.end method

.method static final native nativeGetName(I)I
.end method

.method private static final native nativeGetNamespace(I)I
.end method

.method private static final native nativeGetStringBlock(I)I
.end method

.method private static final native nativeGetStyleAttribute(I)I
.end method

.method private static final native nativeGetText(I)I
.end method

.method static final native nativeNext(I)I
.end method


# virtual methods
.method public close()V
    .registers 2

    #@0
    .prologue
    .line 50
    monitor-enter p0

    #@1
    .line 51
    :try_start_1
    iget-boolean v0, p0, Landroid/content/res/XmlBlock;->mOpen:Z

    #@3
    if-eqz v0, :cond_b

    #@5
    .line 52
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/content/res/XmlBlock;->mOpen:Z

    #@8
    .line 53
    invoke-direct {p0}, Landroid/content/res/XmlBlock;->decOpenCountLocked()V

    #@b
    .line 55
    :cond_b
    monitor-exit p0

    #@c
    .line 56
    return-void

    #@d
    .line 55
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 470
    invoke-virtual {p0}, Landroid/content/res/XmlBlock;->close()V

    #@3
    .line 471
    return-void
.end method

.method public newParser()Landroid/content/res/XmlResourceParser;
    .registers 3

    #@0
    .prologue
    .line 69
    monitor-enter p0

    #@1
    .line 70
    :try_start_1
    iget v0, p0, Landroid/content/res/XmlBlock;->mNative:I

    #@3
    if-eqz v0, :cond_12

    #@5
    .line 71
    new-instance v0, Landroid/content/res/XmlBlock$Parser;

    #@7
    iget v1, p0, Landroid/content/res/XmlBlock;->mNative:I

    #@9
    invoke-static {v1}, Landroid/content/res/XmlBlock;->nativeCreateParseState(I)I

    #@c
    move-result v1

    #@d
    invoke-direct {v0, p0, v1, p0}, Landroid/content/res/XmlBlock$Parser;-><init>(Landroid/content/res/XmlBlock;ILandroid/content/res/XmlBlock;)V

    #@10
    monitor-exit p0

    #@11
    .line 73
    :goto_11
    return-object v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    monitor-exit p0

    #@14
    goto :goto_11

    #@15
    .line 74
    :catchall_15
    move-exception v0

    #@16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method
