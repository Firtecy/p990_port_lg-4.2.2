.class public Landroid/content/res/CompatibilityInfo;
.super Ljava/lang/Object;
.source "CompatibilityInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/res/CompatibilityInfo$Translator;
    }
.end annotation


# static fields
.field private static final ALWAYS_NEEDS_COMPAT:I = 0x2

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/res/CompatibilityInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo; = null

.field public static final DEFAULT_NORMAL_SHORT_DIMENSION:I = 0x140

.field public static final MAXIMUM_ASPECT_RATIO:F = 1.7791667f

.field private static final NEEDS_ASPECT_WVGA:I = 0x10

.field private static final NEEDS_SCREEN_COMPAT:I = 0x8

.field private static final NEEDS_SECONDARY_DPI:I = 0x20

.field private static final NEVER_NEEDS_COMPAT:I = 0x4

.field private static final SCALING_REQUIRED:I = 0x1

.field private static ctsChecked:Z

.field private static ctsDetected:Z


# instance fields
.field public final applicationDensity:I

.field public final applicationInvertedScale:F

.field public final applicationScale:F

.field private final mCompatibilityFlags:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 44
    new-instance v0, Landroid/content/res/CompatibilityInfo$1;

    #@3
    invoke-direct {v0}, Landroid/content/res/CompatibilityInfo$1;-><init>()V

    #@6
    sput-object v0, Landroid/content/res/CompatibilityInfo;->DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo;

    #@8
    .line 347
    sput-boolean v1, Landroid/content/res/CompatibilityInfo;->ctsDetected:Z

    #@a
    .line 349
    sput-boolean v1, Landroid/content/res/CompatibilityInfo;->ctsChecked:Z

    #@c
    .line 728
    new-instance v0, Landroid/content/res/CompatibilityInfo$2;

    #@e
    invoke-direct {v0}, Landroid/content/res/CompatibilityInfo$2;-><init>()V

    #@11
    sput-object v0, Landroid/content/res/CompatibilityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@13
    return-void
.end method

.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/high16 v2, 0x3f80

    #@2
    .line 341
    const/4 v0, 0x4

    #@3
    sget v1, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@5
    invoke-direct {p0, v0, v1, v2, v2}, Landroid/content/res/CompatibilityInfo;-><init>(IIFF)V

    #@8
    .line 344
    return-void
.end method

.method private constructor <init>(IIFF)V
    .registers 5
    .parameter "compFlags"
    .parameter "dens"
    .parameter "scale"
    .parameter "invertedScale"

    #@0
    .prologue
    .line 333
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 334
    iput p1, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@5
    .line 335
    iput p2, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@7
    .line 336
    iput p3, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@9
    .line 337
    iput p4, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@b
    .line 338
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/ApplicationInfo;IIZ)V
    .registers 11
    .parameter "appInfo"
    .parameter "screenLayout"
    .parameter "sw"
    .parameter "forceCompat"

    #@0
    .prologue
    .line 113
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/content/res/CompatibilityInfo;-><init>(Landroid/content/pm/ApplicationInfo;IIZZ)V

    #@9
    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/ApplicationInfo;IIZZ)V
    .registers 18
    .parameter "appInfo"
    .parameter "screenLayout"
    .parameter "sw"
    .parameter "forceCompat"
    .parameter "forceWVGA"

    #@0
    .prologue
    .line 117
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 118
    const/4 v5, 0x0

    #@4
    .line 121
    .local v5, compatFlags:I
    const/4 v7, 0x0

    #@5
    .line 122
    .local v7, needsSecondaryDpi:Z
    sget v10, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@7
    sget v11, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@9
    if-eq v10, v11, :cond_2d

    #@b
    .line 124
    iget-object v10, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@d
    if-eqz v10, :cond_24

    #@f
    .line 125
    iget-object v10, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@11
    const-string v11, "com.lge.launcher2"

    #@13
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v10

    #@17
    if-nez v10, :cond_23

    #@19
    iget-object v10, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@1b
    const-string v11, "com.lge.myapplications"

    #@1d
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v10

    #@21
    if-eqz v10, :cond_24

    #@23
    .line 127
    :cond_23
    const/4 v7, 0x1

    #@24
    .line 132
    :cond_24
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@26
    invoke-static {v10}, Landroid/content/res/CompatibilityInfo;->checkIfCtsInstalled(I)Z

    #@29
    move-result v10

    #@2a
    if-eqz v10, :cond_2d

    #@2c
    .line 133
    const/4 v7, 0x0

    #@2d
    .line 138
    :cond_2d
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@2f
    if-nez v10, :cond_39

    #@31
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@33
    if-nez v10, :cond_39

    #@35
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@37
    if-eqz v10, :cond_a2

    #@39
    .line 141
    :cond_39
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@3b
    if-eqz v10, :cond_74

    #@3d
    iget v8, p1, Landroid/content/pm/ApplicationInfo;->requiresSmallestWidthDp:I

    #@3f
    .line 144
    .local v8, required:I
    :goto_3f
    if-nez v8, :cond_43

    #@41
    .line 145
    iget v8, p1, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@43
    .line 147
    :cond_43
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@45
    if-eqz v10, :cond_77

    #@47
    iget v4, p1, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@49
    .line 149
    .local v4, compat:I
    :goto_49
    if-ge v4, v8, :cond_4c

    #@4b
    .line 150
    move v4, v8

    #@4c
    .line 152
    :cond_4c
    iget v6, p1, Landroid/content/pm/ApplicationInfo;->largestWidthLimitDp:I

    #@4e
    .line 154
    .local v6, largest:I
    const/16 v10, 0x140

    #@50
    if-le v8, v10, :cond_79

    #@52
    .line 161
    or-int/lit8 v5, v5, 0x4

    #@54
    .line 180
    :cond_54
    :goto_54
    if-eqz p4, :cond_8a

    #@56
    if-eqz p5, :cond_8a

    #@58
    .line 181
    const/16 v10, 0xf0

    #@5a
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@5c
    .line 182
    sget v10, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@5e
    int-to-float v10, v10

    #@5f
    const/high16 v11, 0x4370

    #@61
    div-float/2addr v10, v11

    #@62
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@64
    .line 184
    const/high16 v10, 0x3f80

    #@66
    iget v11, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@68
    div-float/2addr v10, v11

    #@69
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@6b
    .line 185
    or-int/lit8 v10, v5, 0x1

    #@6d
    or-int/lit8 v10, v10, 0x10

    #@6f
    or-int/lit8 v10, v10, 0x8

    #@71
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@73
    .line 330
    .end local v4           #compat:I
    .end local v6           #largest:I
    .end local v8           #required:I
    :goto_73
    return-void

    #@74
    .line 141
    :cond_74
    iget v8, p1, Landroid/content/pm/ApplicationInfo;->compatibleWidthLimitDp:I

    #@76
    goto :goto_3f

    #@77
    .restart local v8       #required:I
    :cond_77
    move v4, v8

    #@78
    .line 147
    goto :goto_49

    #@79
    .line 162
    .restart local v4       #compat:I
    .restart local v6       #largest:I
    :cond_79
    if-eqz v6, :cond_80

    #@7b
    if-le p3, v6, :cond_80

    #@7d
    .line 166
    or-int/lit8 v5, v5, 0xa

    #@7f
    goto :goto_54

    #@80
    .line 167
    :cond_80
    if-lt v4, p3, :cond_85

    #@82
    .line 170
    or-int/lit8 v5, v5, 0x4

    #@84
    goto :goto_54

    #@85
    .line 171
    :cond_85
    if-eqz p4, :cond_54

    #@87
    .line 174
    or-int/lit8 v5, v5, 0x8

    #@89
    goto :goto_54

    #@8a
    .line 192
    :cond_8a
    if-eqz v7, :cond_9d

    #@8c
    .line 193
    or-int/lit8 v5, v5, 0x20

    #@8e
    .line 194
    sget v10, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@90
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@92
    .line 200
    :goto_92
    const/high16 v10, 0x3f80

    #@94
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@96
    .line 201
    const/high16 v10, 0x3f80

    #@98
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@9a
    .line 329
    .end local v4           #compat:I
    .end local v6           #largest:I
    .end local v8           #required:I
    :goto_9a
    iput v5, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@9c
    goto :goto_73

    #@9d
    .line 196
    .restart local v4       #compat:I
    .restart local v6       #largest:I
    .restart local v8       #required:I
    :cond_9d
    sget v10, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@9f
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@a1
    goto :goto_92

    #@a2
    .line 208
    .end local v4           #compat:I
    .end local v6           #largest:I
    .end local v8           #required:I
    :cond_a2
    const/4 v0, 0x2

    #@a3
    .line 214
    .local v0, EXPANDABLE:I
    const/16 v1, 0x8

    #@a5
    .line 220
    .local v1, LARGE_SCREENS:I
    const/16 v2, 0x20

    #@a7
    .line 222
    .local v2, XLARGE_SCREENS:I
    const/4 v9, 0x0

    #@a8
    .line 226
    .local v9, sizeInfo:I
    const/4 v3, 0x0

    #@a9
    .line 228
    .local v3, anyResizeable:Z
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@ab
    and-int/lit16 v10, v10, 0x800

    #@ad
    if-eqz v10, :cond_b6

    #@af
    .line 229
    or-int/lit8 v9, v9, 0x8

    #@b1
    .line 230
    const/4 v3, 0x1

    #@b2
    .line 231
    if-nez p4, :cond_b6

    #@b4
    .line 235
    or-int/lit8 v9, v9, 0x22

    #@b6
    .line 238
    :cond_b6
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@b8
    const/high16 v11, 0x8

    #@ba
    and-int/2addr v10, v11

    #@bb
    if-eqz v10, :cond_c2

    #@bd
    .line 239
    const/4 v3, 0x1

    #@be
    .line 240
    if-nez p4, :cond_c2

    #@c0
    .line 241
    or-int/lit8 v9, v9, 0x22

    #@c2
    .line 244
    :cond_c2
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@c4
    and-int/lit16 v10, v10, 0x1000

    #@c6
    if-eqz v10, :cond_cb

    #@c8
    .line 245
    const/4 v3, 0x1

    #@c9
    .line 246
    or-int/lit8 v9, v9, 0x2

    #@cb
    .line 249
    :cond_cb
    if-eqz p4, :cond_cf

    #@cd
    .line 254
    and-int/lit8 v9, v9, -0x3

    #@cf
    .line 257
    :cond_cf
    or-int/lit8 v5, v5, 0x8

    #@d1
    .line 258
    and-int/lit8 v10, p2, 0xf

    #@d3
    packed-switch v10, :pswitch_data_166

    #@d6
    .line 277
    :cond_d6
    :goto_d6
    const/high16 v10, 0x1000

    #@d8
    and-int/2addr v10, p2

    #@d9
    if-eqz v10, :cond_12c

    #@db
    .line 278
    and-int/lit8 v10, v9, 0x2

    #@dd
    if-eqz v10, :cond_127

    #@df
    .line 279
    and-int/lit8 v5, v5, -0x9

    #@e1
    .line 288
    :cond_e1
    :goto_e1
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@e3
    and-int/lit16 v10, v10, 0x2000

    #@e5
    if-eqz v10, :cond_148

    #@e7
    .line 292
    if-eqz p4, :cond_131

    #@e9
    if-eqz p5, :cond_131

    #@eb
    .line 293
    const/16 v10, 0xf0

    #@ed
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@ef
    .line 294
    sget v10, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@f1
    int-to-float v10, v10

    #@f2
    const/high16 v11, 0x4370

    #@f4
    div-float/2addr v10, v11

    #@f5
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@f7
    .line 296
    const/high16 v10, 0x3f80

    #@f9
    iget v11, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@fb
    div-float/2addr v10, v11

    #@fc
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@fe
    .line 297
    or-int/lit8 v10, v5, 0x1

    #@100
    or-int/lit8 v10, v10, 0x10

    #@102
    or-int/lit8 v10, v10, 0x8

    #@104
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@106
    goto/16 :goto_73

    #@108
    .line 260
    :pswitch_108
    and-int/lit8 v10, v9, 0x20

    #@10a
    if-eqz v10, :cond_10e

    #@10c
    .line 261
    and-int/lit8 v5, v5, -0x9

    #@10e
    .line 263
    :cond_10e
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@110
    const/high16 v11, 0x8

    #@112
    and-int/2addr v10, v11

    #@113
    if-eqz v10, :cond_d6

    #@115
    .line 264
    or-int/lit8 v5, v5, 0x4

    #@117
    goto :goto_d6

    #@118
    .line 268
    :pswitch_118
    and-int/lit8 v10, v9, 0x8

    #@11a
    if-eqz v10, :cond_11e

    #@11c
    .line 269
    and-int/lit8 v5, v5, -0x9

    #@11e
    .line 271
    :cond_11e
    iget v10, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@120
    and-int/lit16 v10, v10, 0x800

    #@122
    if-eqz v10, :cond_d6

    #@124
    .line 272
    or-int/lit8 v5, v5, 0x4

    #@126
    goto :goto_d6

    #@127
    .line 280
    :cond_127
    if-nez v3, :cond_e1

    #@129
    .line 281
    or-int/lit8 v5, v5, 0x2

    #@12b
    goto :goto_e1

    #@12c
    .line 284
    :cond_12c
    and-int/lit8 v5, v5, -0x9

    #@12e
    .line 285
    or-int/lit8 v5, v5, 0x4

    #@130
    goto :goto_e1

    #@131
    .line 303
    :cond_131
    if-eqz v7, :cond_143

    #@133
    .line 304
    or-int/lit8 v5, v5, 0x20

    #@135
    .line 305
    sget v10, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@137
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@139
    .line 310
    :goto_139
    const/high16 v10, 0x3f80

    #@13b
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@13d
    .line 311
    const/high16 v10, 0x3f80

    #@13f
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@141
    goto/16 :goto_9a

    #@143
    .line 307
    :cond_143
    sget v10, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@145
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@147
    goto :goto_139

    #@148
    .line 316
    :cond_148
    if-eqz p4, :cond_14e

    #@14a
    if-eqz p5, :cond_14e

    #@14c
    .line 317
    or-int/lit8 v5, v5, 0x18

    #@14e
    .line 321
    :cond_14e
    const/16 v10, 0xa0

    #@150
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@152
    .line 322
    sget v10, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@154
    int-to-float v10, v10

    #@155
    const/high16 v11, 0x4320

    #@157
    div-float/2addr v10, v11

    #@158
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@15a
    .line 324
    const/high16 v10, 0x3f80

    #@15c
    iget v11, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@15e
    div-float/2addr v10, v11

    #@15f
    iput v10, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@161
    .line 325
    or-int/lit8 v5, v5, 0x1

    #@163
    goto/16 :goto_9a

    #@165
    .line 258
    nop

    #@166
    :pswitch_data_166
    .packed-switch 0x3
        :pswitch_118
        :pswitch_108
    .end packed-switch
.end method

.method synthetic constructor <init>(Landroid/content/res/CompatibilityInfo$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/res/CompatibilityInfo;-><init>()V

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 739
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 740
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@9
    .line 741
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@f
    .line 742
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@15
    .line 743
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@1b
    .line 744
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/content/res/CompatibilityInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/content/res/CompatibilityInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private static checkIfCtsInstalled(I)Z
    .registers 6
    .parameter "userId"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 352
    sget-boolean v3, Landroid/content/res/CompatibilityInfo;->ctsChecked:Z

    #@3
    if-eqz v3, :cond_8

    #@5
    .line 353
    sget-boolean v2, Landroid/content/res/CompatibilityInfo;->ctsDetected:Z

    #@7
    .line 366
    :goto_7
    return v2

    #@8
    .line 356
    :cond_8
    :try_start_8
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@b
    move-result-object v1

    #@c
    .line 357
    .local v1, pm:Landroid/content/pm/IPackageManager;
    if-eqz v1, :cond_22

    #@e
    const-string v3, "android.tests.devicesetup"

    #@10
    const/4 v4, 0x1

    #@11
    invoke-interface {v1, v3, v4, p0}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    #@14
    move-result-object v3

    #@15
    if-eqz v3, :cond_22

    #@17
    .line 358
    const/4 v3, 0x1

    #@18
    sput-boolean v3, Landroid/content/res/CompatibilityInfo;->ctsDetected:Z

    #@1a
    .line 359
    const/4 v3, 0x1

    #@1b
    sput-boolean v3, Landroid/content/res/CompatibilityInfo;->ctsChecked:Z
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_1d} :catch_1e

    #@1d
    goto :goto_7

    #@1e
    .line 363
    .end local v1           #pm:Landroid/content/pm/IPackageManager;
    :catch_1e
    move-exception v0

    #@1f
    .line 364
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@22
    .line 366
    .end local v0           #e:Ljava/lang/Exception;
    :cond_22
    const/4 v2, 0x0

    #@23
    goto :goto_7
.end method

.method public static computeCompatibleScaling(Landroid/util/DisplayMetrics;Landroid/util/DisplayMetrics;)F
    .registers 3
    .parameter "dm"
    .parameter "outDm"

    #@0
    .prologue
    .line 610
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/content/res/CompatibilityInfo;->computeCompatibleScaling(Landroid/util/DisplayMetrics;Landroid/util/DisplayMetrics;Z)F

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static computeCompatibleScaling(Landroid/util/DisplayMetrics;Landroid/util/DisplayMetrics;Z)F
    .registers 17
    .parameter "dm"
    .parameter "outDm"
    .parameter "isWvga"

    #@0
    .prologue
    .line 622
    iget v11, p0, Landroid/util/DisplayMetrics;->noncompatWidthPixels:I

    #@2
    .line 623
    .local v11, width:I
    iget v1, p0, Landroid/util/DisplayMetrics;->noncompatHeightPixels:I

    #@4
    .line 625
    .local v1, height:I
    if-ge v11, v1, :cond_4c

    #@6
    .line 626
    move v9, v11

    #@7
    .line 627
    .local v9, shortSize:I
    move v2, v1

    #@8
    .line 632
    .local v2, longSize:I
    :goto_8
    const/high16 v12, 0x43a0

    #@a
    iget v13, p0, Landroid/util/DisplayMetrics;->density:F

    #@c
    mul-float/2addr v12, v13

    #@d
    const/high16 v13, 0x3f00

    #@f
    add-float/2addr v12, v13

    #@10
    float-to-int v5, v12

    #@11
    .line 633
    .local v5, newShortSize:I
    int-to-float v12, v2

    #@12
    int-to-float v13, v9

    #@13
    div-float v0, v12, v13

    #@15
    .line 634
    .local v0, aspect:F
    const v12, 0x3fe3bbbc

    #@18
    cmpl-float v12, v0, v12

    #@1a
    if-lez v12, :cond_1f

    #@1c
    .line 635
    const v0, 0x3fe3bbbc

    #@1f
    .line 638
    :cond_1f
    if-eqz p2, :cond_24

    #@21
    .line 639
    const v0, 0x3fd55555

    #@24
    .line 642
    :cond_24
    int-to-float v12, v5

    #@25
    mul-float/2addr v12, v0

    #@26
    const/high16 v13, 0x3f00

    #@28
    add-float/2addr v12, v13

    #@29
    float-to-int v4, v12

    #@2a
    .line 644
    .local v4, newLongSize:I
    if-ge v11, v1, :cond_4f

    #@2c
    .line 645
    move v6, v5

    #@2d
    .line 646
    .local v6, newWidth:I
    move v3, v4

    #@2e
    .line 652
    .local v3, newHeight:I
    :goto_2e
    int-to-float v12, v11

    #@2f
    int-to-float v13, v6

    #@30
    div-float v10, v12, v13

    #@32
    .line 653
    .local v10, sw:F
    int-to-float v12, v1

    #@33
    int-to-float v13, v3

    #@34
    div-float v8, v12, v13

    #@36
    .line 654
    .local v8, sh:F
    cmpg-float v12, v10, v8

    #@38
    if-gez v12, :cond_52

    #@3a
    move v7, v10

    #@3b
    .line 655
    .local v7, scale:F
    :goto_3b
    const/high16 v12, 0x3f80

    #@3d
    cmpg-float v12, v7, v12

    #@3f
    if-gez v12, :cond_45

    #@41
    if-nez p2, :cond_45

    #@43
    .line 656
    const/high16 v7, 0x3f80

    #@45
    .line 659
    :cond_45
    if-eqz p1, :cond_4b

    #@47
    .line 660
    iput v6, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@49
    .line 661
    iput v3, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@4b
    .line 664
    :cond_4b
    return v7

    #@4c
    .line 629
    .end local v0           #aspect:F
    .end local v2           #longSize:I
    .end local v3           #newHeight:I
    .end local v4           #newLongSize:I
    .end local v5           #newShortSize:I
    .end local v6           #newWidth:I
    .end local v7           #scale:F
    .end local v8           #sh:F
    .end local v9           #shortSize:I
    .end local v10           #sw:F
    :cond_4c
    move v9, v1

    #@4d
    .line 630
    .restart local v9       #shortSize:I
    move v2, v11

    #@4e
    .restart local v2       #longSize:I
    goto :goto_8

    #@4f
    .line 648
    .restart local v0       #aspect:F
    .restart local v4       #newLongSize:I
    .restart local v5       #newShortSize:I
    :cond_4f
    move v6, v4

    #@50
    .line 649
    .restart local v6       #newWidth:I
    move v3, v5

    #@51
    .restart local v3       #newHeight:I
    goto :goto_2e

    #@52
    .restart local v8       #sh:F
    .restart local v10       #sw:F
    :cond_52
    move v7, v8

    #@53
    .line 654
    goto :goto_3b
.end method


# virtual methods
.method public alwaysSupportsScreen()Z
    .registers 2

    #@0
    .prologue
    .line 386
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public applyToConfiguration(ILandroid/content/res/Configuration;)V
    .registers 6
    .parameter "displayDensity"
    .parameter "inoutConfig"

    #@0
    .prologue
    .line 584
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->supportsScreen()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_1a

    #@6
    .line 588
    iget v1, p2, Landroid/content/res/Configuration;->screenLayout:I

    #@8
    and-int/lit8 v1, v1, -0x10

    #@a
    or-int/lit8 v1, v1, 0x2

    #@c
    iput v1, p2, Landroid/content/res/Configuration;->screenLayout:I

    #@e
    .line 591
    iget v1, p2, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    #@10
    iput v1, p2, Landroid/content/res/Configuration;->screenWidthDp:I

    #@12
    .line 592
    iget v1, p2, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    #@14
    iput v1, p2, Landroid/content/res/Configuration;->screenHeightDp:I

    #@16
    .line 593
    iget v1, p2, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    #@18
    iput v1, p2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@1a
    .line 595
    :cond_1a
    iput p1, p2, Landroid/content/res/Configuration;->densityDpi:I

    #@1c
    .line 596
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->isScalingRequired()Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_2e

    #@22
    .line 597
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@24
    .line 598
    .local v0, invertedRatio:F
    iget v1, p2, Landroid/content/res/Configuration;->densityDpi:I

    #@26
    int-to-float v1, v1

    #@27
    mul-float/2addr v1, v0

    #@28
    const/high16 v2, 0x3f00

    #@2a
    add-float/2addr v1, v2

    #@2b
    float-to-int v1, v1

    #@2c
    iput v1, p2, Landroid/content/res/Configuration;->densityDpi:I

    #@2e
    .line 602
    .end local v0           #invertedRatio:F
    :cond_2e
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->needsSecondaryDpi()Z

    #@31
    move-result v1

    #@32
    if-eqz v1, :cond_38

    #@34
    .line 603
    sget v1, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@36
    iput v1, p2, Landroid/content/res/Configuration;->densityDpi:I

    #@38
    .line 606
    :cond_38
    return-void
.end method

.method public applyToDisplayMetrics(Landroid/util/DisplayMetrics;)V
    .registers 5
    .parameter "inoutDm"

    #@0
    .prologue
    const/high16 v2, 0x3f00

    #@2
    .line 552
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->supportsScreen()Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_5a

    #@8
    .line 555
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->requiresWvgaAspect()Z

    #@b
    move-result v1

    #@c
    invoke-static {p1, p1, v1}, Landroid/content/res/CompatibilityInfo;->computeCompatibleScaling(Landroid/util/DisplayMetrics;Landroid/util/DisplayMetrics;Z)F

    #@f
    .line 562
    :goto_f
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->isScalingRequired()Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_43

    #@15
    .line 563
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@17
    .line 564
    .local v0, invertedRatio:F
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatDensity:F

    #@19
    mul-float/2addr v1, v0

    #@1a
    iput v1, p1, Landroid/util/DisplayMetrics;->density:F

    #@1c
    .line 565
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@1e
    int-to-float v1, v1

    #@1f
    mul-float/2addr v1, v0

    #@20
    add-float/2addr v1, v2

    #@21
    float-to-int v1, v1

    #@22
    iput v1, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@24
    .line 566
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatScaledDensity:F

    #@26
    mul-float/2addr v1, v0

    #@27
    iput v1, p1, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@29
    .line 567
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatXdpi:F

    #@2b
    mul-float/2addr v1, v0

    #@2c
    iput v1, p1, Landroid/util/DisplayMetrics;->xdpi:F

    #@2e
    .line 568
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatYdpi:F

    #@30
    mul-float/2addr v1, v0

    #@31
    iput v1, p1, Landroid/util/DisplayMetrics;->ydpi:F

    #@33
    .line 569
    iget v1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@35
    int-to-float v1, v1

    #@36
    mul-float/2addr v1, v0

    #@37
    add-float/2addr v1, v2

    #@38
    float-to-int v1, v1

    #@39
    iput v1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@3b
    .line 570
    iget v1, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@3d
    int-to-float v1, v1

    #@3e
    mul-float/2addr v1, v0

    #@3f
    add-float/2addr v1, v2

    #@40
    float-to-int v1, v1

    #@41
    iput v1, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@43
    .line 574
    .end local v0           #invertedRatio:F
    :cond_43
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->needsSecondaryDpi()Z

    #@46
    move-result v1

    #@47
    if-eqz v1, :cond_59

    #@49
    .line 575
    sget v1, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@4b
    int-to-float v1, v1

    #@4c
    const/high16 v2, 0x4320

    #@4e
    div-float/2addr v1, v2

    #@4f
    iput v1, p1, Landroid/util/DisplayMetrics;->density:F

    #@51
    .line 576
    sget v1, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@53
    iput v1, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@55
    .line 577
    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    #@57
    iput v1, p1, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@59
    .line 581
    :cond_59
    return-void

    #@5a
    .line 558
    :cond_5a
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatWidthPixels:I

    #@5c
    iput v1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@5e
    .line 559
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatHeightPixels:I

    #@60
    iput v1, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@62
    goto :goto_f
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 717
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "o"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 670
    :try_start_1
    move-object v0, p1

    #@2
    check-cast v0, Landroid/content/res/CompatibilityInfo;

    #@4
    move-object v2, v0

    #@5
    .line 671
    .local v2, oc:Landroid/content/res/CompatibilityInfo;
    iget v4, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@7
    iget v5, v2, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@9
    if-eq v4, v5, :cond_c

    #@b
    .line 677
    .end local v2           #oc:Landroid/content/res/CompatibilityInfo;
    :cond_b
    :goto_b
    return v3

    #@c
    .line 672
    .restart local v2       #oc:Landroid/content/res/CompatibilityInfo;
    :cond_c
    iget v4, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@e
    iget v5, v2, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@10
    if-ne v4, v5, :cond_b

    #@12
    .line 673
    iget v4, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@14
    iget v5, v2, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@16
    cmpl-float v4, v4, v5

    #@18
    if-nez v4, :cond_b

    #@1a
    .line 674
    iget v4, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@1c
    iget v5, v2, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F
    :try_end_1e
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1e} :catch_24

    #@1e
    cmpl-float v4, v4, v5

    #@20
    if-nez v4, :cond_b

    #@22
    .line 675
    const/4 v3, 0x1

    #@23
    goto :goto_b

    #@24
    .line 676
    .end local v2           #oc:Landroid/content/res/CompatibilityInfo;
    :catch_24
    move-exception v1

    #@25
    .line 677
    .local v1, e:Ljava/lang/ClassCastException;
    goto :goto_b
.end method

.method public getTranslator()Landroid/content/res/CompatibilityInfo$Translator;
    .registers 2

    #@0
    .prologue
    .line 404
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->isScalingRequired()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    new-instance v0, Landroid/content/res/CompatibilityInfo$Translator;

    #@8
    invoke-direct {v0, p0}, Landroid/content/res/CompatibilityInfo$Translator;-><init>(Landroid/content/res/CompatibilityInfo;)V

    #@b
    :goto_b
    return-object v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 707
    const/16 v0, 0x11

    #@2
    .line 708
    .local v0, result:I
    iget v1, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@4
    add-int/lit16 v0, v1, 0x20f

    #@6
    .line 709
    mul-int/lit8 v1, v0, 0x1f

    #@8
    iget v2, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@a
    add-int v0, v1, v2

    #@c
    .line 710
    mul-int/lit8 v1, v0, 0x1f

    #@e
    iget v2, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@10
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    #@13
    move-result v2

    #@14
    add-int v0, v1, v2

    #@16
    .line 711
    mul-int/lit8 v1, v0, 0x1f

    #@18
    iget v2, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@1a
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    #@1d
    move-result v2

    #@1e
    add-int v0, v1, v2

    #@20
    .line 712
    return v0
.end method

.method public isScalingRequired()Z
    .registers 2

    #@0
    .prologue
    .line 374
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public needsSecondaryDpi()Z
    .registers 2

    #@0
    .prologue
    .line 396
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@2
    and-int/lit8 v0, v0, 0x20

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public neverSupportsScreen()Z
    .registers 2

    #@0
    .prologue
    .line 382
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public requiresWvgaAspect()Z
    .registers 2

    #@0
    .prologue
    .line 391
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@2
    and-int/lit8 v0, v0, 0x10

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public supportsScreen()Z
    .registers 2

    #@0
    .prologue
    .line 378
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@2
    and-int/lit8 v0, v0, 0x8

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 683
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 684
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{"

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 685
    iget v1, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    .line 686
    const-string v1, "dpi"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 687
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->isScalingRequired()Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_2d

    #@1d
    .line 688
    const-string v1, " "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 689
    iget v1, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@27
    .line 690
    const-string/jumbo v1, "x"

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    .line 692
    :cond_2d
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->supportsScreen()Z

    #@30
    move-result v1

    #@31
    if-nez v1, :cond_38

    #@33
    .line 693
    const-string v1, " resizing"

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 695
    :cond_38
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->neverSupportsScreen()Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_43

    #@3e
    .line 696
    const-string v1, " never-compat"

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    .line 698
    :cond_43
    invoke-virtual {p0}, Landroid/content/res/CompatibilityInfo;->alwaysSupportsScreen()Z

    #@46
    move-result v1

    #@47
    if-eqz v1, :cond_4e

    #@49
    .line 699
    const-string v1, " always-compat"

    #@4b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    .line 701
    :cond_4e
    const-string/jumbo v1, "}"

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    .line 702
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v1

    #@58
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 722
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->mCompatibilityFlags:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 723
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->applicationDensity:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 724
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@f
    .line 725
    iget v0, p0, Landroid/content/res/CompatibilityInfo;->applicationInvertedScale:F

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@14
    .line 726
    return-void
.end method
