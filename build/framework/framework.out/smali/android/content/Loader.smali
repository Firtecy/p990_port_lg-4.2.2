.class public Landroid/content/Loader;
.super Ljava/lang/Object;
.source "Loader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/Loader$OnLoadCanceledListener;,
        Landroid/content/Loader$OnLoadCompleteListener;,
        Landroid/content/Loader$ForceLoadContentObserver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field mAbandoned:Z

.field mContentChanged:Z

.field mContext:Landroid/content/Context;

.field mId:I

.field mListener:Landroid/content/Loader$OnLoadCompleteListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/content/Loader$OnLoadCompleteListener",
            "<TD;>;"
        }
    .end annotation
.end field

.field mOnLoadCanceledListener:Landroid/content/Loader$OnLoadCanceledListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/content/Loader$OnLoadCanceledListener",
            "<TD;>;"
        }
    .end annotation
.end field

.field mReset:Z

.field mStarted:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    const/4 v1, 0x0

    #@1
    .line 130
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 57
    iput-boolean v1, p0, Landroid/content/Loader;->mStarted:Z

    #@6
    .line 58
    iput-boolean v1, p0, Landroid/content/Loader;->mAbandoned:Z

    #@8
    .line 59
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Landroid/content/Loader;->mReset:Z

    #@b
    .line 60
    iput-boolean v1, p0, Landroid/content/Loader;->mContentChanged:Z

    #@d
    .line 131
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/content/Loader;->mContext:Landroid/content/Context;

    #@13
    .line 132
    return-void
.end method


# virtual methods
.method public abandon()V
    .registers 2

    #@0
    .prologue
    .line 402
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/content/Loader;->mAbandoned:Z

    #@3
    .line 403
    invoke-virtual {p0}, Landroid/content/Loader;->onAbandon()V

    #@6
    .line 404
    return-void
.end method

.method public cancelLoad()Z
    .registers 2

    #@0
    .prologue
    .line 319
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    invoke-virtual {p0}, Landroid/content/Loader;->onCancelLoad()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public dataToString(Ljava/lang/Object;)Ljava/lang/String;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 488
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    .local p1, data:Ljava/lang/Object;,"TD;"
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x40

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 489
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-static {p1, v0}, Landroid/util/DebugUtils;->buildShortClassTag(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    #@a
    .line 490
    const-string/jumbo v1, "}"

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 491
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    return-object v1
.end method

.method public deliverCancellation()V
    .registers 2

    #@0
    .prologue
    .line 154
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    iget-object v0, p0, Landroid/content/Loader;->mOnLoadCanceledListener:Landroid/content/Loader$OnLoadCanceledListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 155
    iget-object v0, p0, Landroid/content/Loader;->mOnLoadCanceledListener:Landroid/content/Loader$OnLoadCanceledListener;

    #@6
    invoke-interface {v0, p0}, Landroid/content/Loader$OnLoadCanceledListener;->onLoadCanceled(Landroid/content/Loader;)V

    #@9
    .line 157
    :cond_9
    return-void
.end method

.method public deliverResult(Ljava/lang/Object;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 142
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    .local p1, data:Ljava/lang/Object;,"TD;"
    iget-object v0, p0, Landroid/content/Loader;->mListener:Landroid/content/Loader$OnLoadCompleteListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 143
    iget-object v0, p0, Landroid/content/Loader;->mListener:Landroid/content/Loader$OnLoadCompleteListener;

    #@6
    invoke-interface {v0, p0, p1}, Landroid/content/Loader$OnLoadCompleteListener;->onLoadComplete(Landroid/content/Loader;Ljava/lang/Object;)V

    #@9
    .line 145
    :cond_9
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "prefix"
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 513
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string/jumbo v0, "mId="

    #@6
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9
    iget v0, p0, Landroid/content/Loader;->mId:I

    #@b
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@e
    .line 514
    const-string v0, " mListener="

    #@10
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13
    iget-object v0, p0, Landroid/content/Loader;->mListener:Landroid/content/Loader$OnLoadCompleteListener;

    #@15
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@18
    .line 515
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b
    const-string/jumbo v0, "mStarted="

    #@1e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21
    iget-boolean v0, p0, Landroid/content/Loader;->mStarted:Z

    #@23
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@26
    .line 516
    const-string v0, " mContentChanged="

    #@28
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b
    iget-boolean v0, p0, Landroid/content/Loader;->mContentChanged:Z

    #@2d
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@30
    .line 517
    const-string v0, " mAbandoned="

    #@32
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@35
    iget-boolean v0, p0, Landroid/content/Loader;->mAbandoned:Z

    #@37
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@3a
    .line 518
    const-string v0, " mReset="

    #@3c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    iget-boolean v0, p0, Landroid/content/Loader;->mReset:Z

    #@41
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@44
    .line 519
    return-void
.end method

.method public forceLoad()V
    .registers 1

    #@0
    .prologue
    .line 346
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    invoke-virtual {p0}, Landroid/content/Loader;->onForceLoad()V

    #@3
    .line 347
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 163
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    iget-object v0, p0, Landroid/content/Loader;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getId()I
    .registers 2

    #@0
    .prologue
    .line 170
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    iget v0, p0, Landroid/content/Loader;->mId:I

    #@2
    return v0
.end method

.method public isAbandoned()Z
    .registers 2

    #@0
    .prologue
    .line 252
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    iget-boolean v0, p0, Landroid/content/Loader;->mAbandoned:Z

    #@2
    return v0
.end method

.method public isReset()Z
    .registers 2

    #@0
    .prologue
    .line 261
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    iget-boolean v0, p0, Landroid/content/Loader;->mReset:Z

    #@2
    return v0
.end method

.method public isStarted()Z
    .registers 2

    #@0
    .prologue
    .line 243
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    iget-boolean v0, p0, Landroid/content/Loader;->mStarted:Z

    #@2
    return v0
.end method

.method protected onAbandon()V
    .registers 1

    #@0
    .prologue
    .line 416
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    return-void
.end method

.method protected onCancelLoad()Z
    .registers 2

    #@0
    .prologue
    .line 334
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onContentChanged()V
    .registers 2

    #@0
    .prologue
    .line 473
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    iget-boolean v0, p0, Landroid/content/Loader;->mStarted:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    .line 474
    invoke-virtual {p0}, Landroid/content/Loader;->forceLoad()V

    #@7
    .line 481
    :goto_7
    return-void

    #@8
    .line 479
    :cond_8
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Landroid/content/Loader;->mContentChanged:Z

    #@b
    goto :goto_7
.end method

.method protected onForceLoad()V
    .registers 1

    #@0
    .prologue
    .line 354
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    return-void
.end method

.method protected onReset()V
    .registers 1

    #@0
    .prologue
    .line 451
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    return-void
.end method

.method protected onStartLoading()V
    .registers 1

    #@0
    .prologue
    .line 298
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    return-void
.end method

.method protected onStopLoading()V
    .registers 1

    #@0
    .prologue
    .line 388
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    return-void
.end method

.method public registerListener(ILandroid/content/Loader$OnLoadCompleteListener;)V
    .registers 5
    .parameter "id"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Loader$OnLoadCompleteListener",
            "<TD;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 181
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    .local p2, listener:Landroid/content/Loader$OnLoadCompleteListener;,"Landroid/content/Loader$OnLoadCompleteListener<TD;>;"
    iget-object v0, p0, Landroid/content/Loader;->mListener:Landroid/content/Loader$OnLoadCompleteListener;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 182
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "There is already a listener registered"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 184
    :cond_c
    iput-object p2, p0, Landroid/content/Loader;->mListener:Landroid/content/Loader$OnLoadCompleteListener;

    #@e
    .line 185
    iput p1, p0, Landroid/content/Loader;->mId:I

    #@10
    .line 186
    return-void
.end method

.method public registerOnLoadCanceledListener(Landroid/content/Loader$OnLoadCanceledListener;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader$OnLoadCanceledListener",
            "<TD;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 213
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    .local p1, listener:Landroid/content/Loader$OnLoadCanceledListener;,"Landroid/content/Loader$OnLoadCanceledListener<TD;>;"
    iget-object v0, p0, Landroid/content/Loader;->mOnLoadCanceledListener:Landroid/content/Loader$OnLoadCanceledListener;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 214
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "There is already a listener registered"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 216
    :cond_c
    iput-object p1, p0, Landroid/content/Loader;->mOnLoadCanceledListener:Landroid/content/Loader$OnLoadCanceledListener;

    #@e
    .line 217
    return-void
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    const/4 v1, 0x0

    #@1
    .line 437
    invoke-virtual {p0}, Landroid/content/Loader;->onReset()V

    #@4
    .line 438
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/content/Loader;->mReset:Z

    #@7
    .line 439
    iput-boolean v1, p0, Landroid/content/Loader;->mStarted:Z

    #@9
    .line 440
    iput-boolean v1, p0, Landroid/content/Loader;->mAbandoned:Z

    #@b
    .line 441
    iput-boolean v1, p0, Landroid/content/Loader;->mContentChanged:Z

    #@d
    .line 442
    return-void
.end method

.method public final startLoading()V
    .registers 3

    #@0
    .prologue
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    const/4 v1, 0x0

    #@1
    .line 286
    const/4 v0, 0x1

    #@2
    iput-boolean v0, p0, Landroid/content/Loader;->mStarted:Z

    #@4
    .line 287
    iput-boolean v1, p0, Landroid/content/Loader;->mReset:Z

    #@6
    .line 288
    iput-boolean v1, p0, Landroid/content/Loader;->mAbandoned:Z

    #@8
    .line 289
    invoke-virtual {p0}, Landroid/content/Loader;->onStartLoading()V

    #@b
    .line 290
    return-void
.end method

.method public stopLoading()V
    .registers 2

    #@0
    .prologue
    .line 377
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/content/Loader;->mStarted:Z

    #@3
    .line 378
    invoke-virtual {p0}, Landroid/content/Loader;->onStopLoading()V

    #@6
    .line 379
    return-void
.end method

.method public takeContentChanged()Z
    .registers 3

    #@0
    .prologue
    .line 459
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    iget-boolean v0, p0, Landroid/content/Loader;->mContentChanged:Z

    #@2
    .line 460
    .local v0, res:Z
    const/4 v1, 0x0

    #@3
    iput-boolean v1, p0, Landroid/content/Loader;->mContentChanged:Z

    #@5
    .line 461
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 496
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x40

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 497
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-static {p0, v0}, Landroid/util/DebugUtils;->buildShortClassTag(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    #@a
    .line 498
    const-string v1, " id="

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 499
    iget v1, p0, Landroid/content/Loader;->mId:I

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    .line 500
    const-string/jumbo v1, "}"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 501
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    return-object v1
.end method

.method public unregisterListener(Landroid/content/Loader$OnLoadCompleteListener;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader$OnLoadCompleteListener",
            "<TD;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 194
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    .local p1, listener:Landroid/content/Loader$OnLoadCompleteListener;,"Landroid/content/Loader$OnLoadCompleteListener<TD;>;"
    iget-object v0, p0, Landroid/content/Loader;->mListener:Landroid/content/Loader$OnLoadCompleteListener;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 195
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "No listener register"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 197
    :cond_c
    iget-object v0, p0, Landroid/content/Loader;->mListener:Landroid/content/Loader$OnLoadCompleteListener;

    #@e
    if-eq v0, p1, :cond_18

    #@10
    .line 198
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "Attempting to unregister the wrong listener"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 200
    :cond_18
    const/4 v0, 0x0

    #@19
    iput-object v0, p0, Landroid/content/Loader;->mListener:Landroid/content/Loader$OnLoadCompleteListener;

    #@1b
    .line 201
    return-void
.end method

.method public unregisterOnLoadCanceledListener(Landroid/content/Loader$OnLoadCanceledListener;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader$OnLoadCanceledListener",
            "<TD;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 228
    .local p0, this:Landroid/content/Loader;,"Landroid/content/Loader<TD;>;"
    .local p1, listener:Landroid/content/Loader$OnLoadCanceledListener;,"Landroid/content/Loader$OnLoadCanceledListener<TD;>;"
    iget-object v0, p0, Landroid/content/Loader;->mOnLoadCanceledListener:Landroid/content/Loader$OnLoadCanceledListener;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 229
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "No listener register"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 231
    :cond_c
    iget-object v0, p0, Landroid/content/Loader;->mOnLoadCanceledListener:Landroid/content/Loader$OnLoadCanceledListener;

    #@e
    if-eq v0, p1, :cond_18

    #@10
    .line 232
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "Attempting to unregister the wrong listener"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 234
    :cond_18
    const/4 v0, 0x0

    #@19
    iput-object v0, p0, Landroid/content/Loader;->mOnLoadCanceledListener:Landroid/content/Loader$OnLoadCanceledListener;

    #@1b
    .line 235
    return-void
.end method
