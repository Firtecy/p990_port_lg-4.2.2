.class Landroid/content/SyncManager$5;
.super Landroid/content/BroadcastReceiver;
.source "SyncManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/SyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/content/SyncManager;


# direct methods
.method constructor <init>(Landroid/content/SyncManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 278
    iput-object p1, p0, Landroid/content/SyncManager$5;->this$0:Landroid/content/SyncManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 280
    iget-object v1, p0, Landroid/content/SyncManager$5;->this$0:Landroid/content/SyncManager;

    #@2
    invoke-static {v1}, Landroid/content/SyncManager;->access$400(Landroid/content/SyncManager;)Z

    #@5
    move-result v0

    #@6
    .line 284
    .local v0, wasConnected:Z
    iget-object v1, p0, Landroid/content/SyncManager$5;->this$0:Landroid/content/SyncManager;

    #@8
    iget-object v2, p0, Landroid/content/SyncManager$5;->this$0:Landroid/content/SyncManager;

    #@a
    invoke-static {v2}, Landroid/content/SyncManager;->access$500(Landroid/content/SyncManager;)Z

    #@d
    move-result v2

    #@e
    invoke-static {v1, v2}, Landroid/content/SyncManager;->access$402(Landroid/content/SyncManager;Z)Z

    #@11
    .line 285
    iget-object v1, p0, Landroid/content/SyncManager$5;->this$0:Landroid/content/SyncManager;

    #@13
    invoke-static {v1}, Landroid/content/SyncManager;->access$400(Landroid/content/SyncManager;)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_3f

    #@19
    .line 286
    if-nez v0, :cond_3a

    #@1b
    .line 287
    const-string v1, "SyncManager"

    #@1d
    const/4 v2, 0x2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_2b

    #@24
    .line 288
    const-string v1, "SyncManager"

    #@26
    const-string v2, "Reconnection detected: clearing all backoffs"

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 290
    :cond_2b
    iget-object v1, p0, Landroid/content/SyncManager$5;->this$0:Landroid/content/SyncManager;

    #@2d
    invoke-static {v1}, Landroid/content/SyncManager;->access$700(Landroid/content/SyncManager;)Landroid/content/SyncStorageEngine;

    #@30
    move-result-object v1

    #@31
    iget-object v2, p0, Landroid/content/SyncManager$5;->this$0:Landroid/content/SyncManager;

    #@33
    invoke-static {v2}, Landroid/content/SyncManager;->access$600(Landroid/content/SyncManager;)Landroid/content/SyncQueue;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v1, v2}, Landroid/content/SyncStorageEngine;->clearAllBackoffs(Landroid/content/SyncQueue;)V

    #@3a
    .line 292
    :cond_3a
    iget-object v1, p0, Landroid/content/SyncManager$5;->this$0:Landroid/content/SyncManager;

    #@3c
    invoke-static {v1}, Landroid/content/SyncManager;->access$100(Landroid/content/SyncManager;)V

    #@3f
    .line 294
    :cond_3f
    return-void
.end method
