.class public Landroid/content/ContextWrapper;
.super Landroid/content/Context;
.source "ContextWrapper.java"


# instance fields
.field mBase:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "base"

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Landroid/content/Context;-><init>()V

    #@3
    .line 57
    iput-object p1, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@5
    .line 58
    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .registers 4
    .parameter "base"

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 69
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Base context already set"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 71
    :cond_c
    iput-object p1, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@e
    .line 72
    return-void
.end method

.method public bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    .registers 5
    .parameter "service"
    .parameter "conn"
    .parameter "flags"

    #@0
    .prologue
    .line 473
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    .registers 6
    .parameter "service"
    .parameter "conn"
    .parameter "flags"
    .parameter "userHandle"

    #@0
    .prologue
    .line 479
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public checkCallingOrSelfPermission(Ljava/lang/String;)I
    .registers 3
    .parameter "permission"

    #@0
    .prologue
    .line 510
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public checkCallingOrSelfUriPermission(Landroid/net/Uri;I)I
    .registers 4
    .parameter "uri"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 552
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->checkCallingOrSelfUriPermission(Landroid/net/Uri;I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public checkCallingPermission(Ljava/lang/String;)I
    .registers 3
    .parameter "permission"

    #@0
    .prologue
    .line 505
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public checkCallingUriPermission(Landroid/net/Uri;I)I
    .registers 4
    .parameter "uri"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 547
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->checkCallingUriPermission(Landroid/net/Uri;I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public checkPermission(Ljava/lang/String;II)I
    .registers 5
    .parameter "permission"
    .parameter "pid"
    .parameter "uid"

    #@0
    .prologue
    .line 500
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public checkUriPermission(Landroid/net/Uri;III)I
    .registers 6
    .parameter "uri"
    .parameter "pid"
    .parameter "uid"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 542
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/content/Context;->checkUriPermission(Landroid/net/Uri;III)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public checkUriPermission(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;III)I
    .registers 14
    .parameter "uri"
    .parameter "readPermission"
    .parameter "writePermission"
    .parameter "pid"
    .parameter "uid"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 558
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move v6, p6

    #@8
    invoke-virtual/range {v0 .. v6}, Landroid/content/Context;->checkUriPermission(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;III)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public clearWallpaper()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 279
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->clearWallpaper()V

    #@5
    .line 280
    return-void
.end method

.method public createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;
    .registers 3
    .parameter "overrideConfiguration"

    #@0
    .prologue
    .line 604
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public createDisplayContext(Landroid/view/Display;)Landroid/content/Context;
    .registers 3
    .parameter "display"

    #@0
    .prologue
    .line 609
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->createDisplayContext(Landroid/view/Display;)Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    .registers 4
    .parameter "packageName"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 592
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
    .registers 5
    .parameter "packageName"
    .parameter "flags"
    .parameter "user"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 599
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public databaseList()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 244
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->databaseList()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public deleteDatabase(Ljava/lang/String;)Z
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 234
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public deleteFile(Ljava/lang/String;)Z
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "permission"
    .parameter "message"

    #@0
    .prologue
    .line 527
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 528
    return-void
.end method

.method public enforceCallingOrSelfUriPermission(Landroid/net/Uri;ILjava/lang/String;)V
    .registers 5
    .parameter "uri"
    .parameter "modeFlags"
    .parameter "message"

    #@0
    .prologue
    .line 577
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->enforceCallingOrSelfUriPermission(Landroid/net/Uri;ILjava/lang/String;)V

    #@5
    .line 578
    return-void
.end method

.method public enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "permission"
    .parameter "message"

    #@0
    .prologue
    .line 521
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 522
    return-void
.end method

.method public enforceCallingUriPermission(Landroid/net/Uri;ILjava/lang/String;)V
    .registers 5
    .parameter "uri"
    .parameter "modeFlags"
    .parameter "message"

    #@0
    .prologue
    .line 571
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->enforceCallingUriPermission(Landroid/net/Uri;ILjava/lang/String;)V

    #@5
    .line 572
    return-void
.end method

.method public enforcePermission(Ljava/lang/String;IILjava/lang/String;)V
    .registers 6
    .parameter "permission"
    .parameter "pid"
    .parameter "uid"
    .parameter "message"

    #@0
    .prologue
    .line 516
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V

    #@5
    .line 517
    return-void
.end method

.method public enforceUriPermission(Landroid/net/Uri;IIILjava/lang/String;)V
    .registers 12
    .parameter "uri"
    .parameter "pid"
    .parameter "uid"
    .parameter "modeFlags"
    .parameter "message"

    #@0
    .prologue
    .line 565
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move-object v5, p5

    #@7
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->enforceUriPermission(Landroid/net/Uri;IIILjava/lang/String;)V

    #@a
    .line 566
    return-void
.end method

.method public enforceUriPermission(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V
    .registers 16
    .parameter "uri"
    .parameter "readPermission"
    .parameter "writePermission"
    .parameter "pid"
    .parameter "uid"
    .parameter "modeFlags"
    .parameter "message"

    #@0
    .prologue
    .line 584
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move v6, p6

    #@8
    move-object v7, p7

    #@9
    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->enforceUriPermission(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    #@c
    .line 587
    return-void
.end method

.method public fileList()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 188
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->fileList()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .registers 2

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAssets()Landroid/content/res/AssetManager;
    .registers 2

    #@0
    .prologue
    .line 83
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getBaseContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getCacheDir()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 208
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getClassLoader()Ljava/lang/ClassLoader;
    .registers 2

    #@0
    .prologue
    .line 130
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCompatibilityInfo(I)Landroid/view/CompatibilityInfoHolder;
    .registers 3
    .parameter "displayId"

    #@0
    .prologue
    .line 620
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getCompatibilityInfo(I)Landroid/view/CompatibilityInfoHolder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .registers 2

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDatabasePath(Ljava/lang/String;)Ljava/io/File;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 239
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDir(Ljava/lang/String;I)Ljava/io/File;
    .registers 4
    .parameter "name"
    .parameter "mode"

    #@0
    .prologue
    .line 218
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getExternalCacheDir()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 213
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 198
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getFileStreamPath(Ljava/lang/String;)Ljava/io/File;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 183
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getFilesDir()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMainLooper()Landroid/os/Looper;
    .registers 2

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getObbDir()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getObbDir()Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPackageCodePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getPackageCodePath()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPackageManager()Landroid/content/pm/PackageManager;
    .registers 2

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPackageResourcePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 145
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getPackageResourcePath()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .registers 2

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    .registers 4
    .parameter "name"
    .parameter "mode"

    #@0
    .prologue
    .line 161
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSharedPrefsFile(Ljava/lang/String;)Ljava/io/File;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getSharedPrefsFile(Ljava/lang/String;)Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 495
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getTheme()Landroid/content/res/Resources$Theme;
    .registers 2

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getThemeResId()I
    .registers 2

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getThemeResId()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getWallpaper()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 249
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getWallpaper()Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getWallpaperDesiredMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 264
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getWallpaperDesiredMinimumHeight()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getWallpaperDesiredMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 259
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getWallpaperDesiredMinimumWidth()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V
    .registers 5
    .parameter "toPackage"
    .parameter "uri"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 532
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    #@5
    .line 533
    return-void
.end method

.method public isRestricted()Z
    .registers 2

    #@0
    .prologue
    .line 614
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->isRestricted()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    .registers 3
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    .registers 4
    .parameter "name"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;
    .registers 5
    .parameter "name"
    .parameter "mode"
    .parameter "factory"

    #@0
    .prologue
    .line 223
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;Landroid/database/DatabaseErrorHandler;)Landroid/database/sqlite/SQLiteDatabase;
    .registers 6
    .parameter "name"
    .parameter "mode"
    .parameter "factory"
    .parameter "errorHandler"

    #@0
    .prologue
    .line 229
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;Landroid/database/DatabaseErrorHandler;)Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public peekWallpaper()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 254
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->peekWallpaper()Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    .registers 4
    .parameter "receiver"
    .parameter "filter"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
    .registers 6
    .parameter "receiver"
    .parameter "filter"
    .parameter "broadcastPermission"
    .parameter "scheduler"

    #@0
    .prologue
    .line 430
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
    .registers 12
    .parameter "receiver"
    .parameter "user"
    .parameter "filter"
    .parameter "broadcastPermission"
    .parameter "scheduler"

    #@0
    .prologue
    .line 439
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object v5, p5

    #@7
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public removeStickyBroadcast(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 398
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->removeStickyBroadcast(Landroid/content/Intent;)V

    #@5
    .line 399
    return-void
.end method

.method public removeStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    .registers 4
    .parameter "intent"
    .parameter "user"

    #@0
    .prologue
    .line 417
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->removeStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@5
    .line 418
    return-void
.end method

.method public revokeUriPermission(Landroid/net/Uri;I)V
    .registers 4
    .parameter "uri"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 537
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    #@5
    .line 538
    return-void
.end method

.method public sendBroadcast(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 338
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@5
    .line 339
    return-void
.end method

.method public sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    .registers 4
    .parameter "intent"
    .parameter "receiverPermission"

    #@0
    .prologue
    .line 343
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@5
    .line 344
    return-void
.end method

.method public sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    .registers 4
    .parameter "intent"
    .parameter "user"

    #@0
    .prologue
    .line 364
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@5
    .line 365
    return-void
.end method

.method public sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
    .registers 5
    .parameter "intent"
    .parameter "user"
    .parameter "receiverPermission"

    #@0
    .prologue
    .line 370
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    #@5
    .line 371
    return-void
.end method

.method public sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    .registers 4
    .parameter "intent"
    .parameter "receiverPermission"

    #@0
    .prologue
    .line 349
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@5
    .line 350
    return-void
.end method

.method public sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 16
    .parameter "intent"
    .parameter "receiverPermission"
    .parameter "resultReceiver"
    .parameter "scheduler"
    .parameter "initialCode"
    .parameter "initialData"
    .parameter "initialExtras"

    #@0
    .prologue
    .line 357
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move v5, p5

    #@7
    move-object v6, p6

    #@8
    move-object v7, p7

    #@9
    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@c
    .line 360
    return-void
.end method

.method public sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 18
    .parameter "intent"
    .parameter "user"
    .parameter "receiverPermission"
    .parameter "resultReceiver"
    .parameter "scheduler"
    .parameter "initialCode"
    .parameter "initialData"
    .parameter "initialExtras"

    #@0
    .prologue
    .line 377
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object v5, p5

    #@7
    move v6, p6

    #@8
    move-object/from16 v7, p7

    #@a
    move-object/from16 v8, p8

    #@c
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@f
    .line 379
    return-void
.end method

.method public sendStickyBroadcast(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 383
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@5
    .line 384
    return-void
.end method

.method public sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    .registers 4
    .parameter "intent"
    .parameter "user"

    #@0
    .prologue
    .line 403
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@5
    .line 404
    return-void
.end method

.method public sendStickyOrderedBroadcast(Landroid/content/Intent;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 14
    .parameter "intent"
    .parameter "resultReceiver"
    .parameter "scheduler"
    .parameter "initialCode"
    .parameter "initialData"
    .parameter "initialExtras"

    #@0
    .prologue
    .line 391
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move-object v5, p5

    #@7
    move-object v6, p6

    #@8
    invoke-virtual/range {v0 .. v6}, Landroid/content/Context;->sendStickyOrderedBroadcast(Landroid/content/Intent;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@b
    .line 394
    return-void
.end method

.method public sendStickyOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 16
    .parameter "intent"
    .parameter "user"
    .parameter "resultReceiver"
    .parameter "scheduler"
    .parameter "initialCode"
    .parameter "initialData"
    .parameter "initialExtras"

    #@0
    .prologue
    .line 411
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move v5, p5

    #@7
    move-object v6, p6

    #@8
    move-object v7, p7

    #@9
    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendStickyOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@c
    .line 413
    return-void
.end method

.method public setTheme(I)V
    .registers 3
    .parameter "resid"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->setTheme(I)V

    #@5
    .line 115
    return-void
.end method

.method public setWallpaper(Landroid/graphics/Bitmap;)V
    .registers 3
    .parameter "bitmap"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 269
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->setWallpaper(Landroid/graphics/Bitmap;)V

    #@5
    .line 270
    return-void
.end method

.method public setWallpaper(Ljava/io/InputStream;)V
    .registers 3
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 274
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->setWallpaper(Ljava/io/InputStream;)V

    #@5
    .line 275
    return-void
.end method

.method public startActivities([Landroid/content/Intent;)V
    .registers 3
    .parameter "intents"

    #@0
    .prologue
    .line 306
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivities([Landroid/content/Intent;)V

    #@5
    .line 307
    return-void
.end method

.method public startActivities([Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 4
    .parameter "intents"
    .parameter "options"

    #@0
    .prologue
    .line 311
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->startActivities([Landroid/content/Intent;Landroid/os/Bundle;)V

    #@5
    .line 312
    return-void
.end method

.method public startActivitiesAsUser([Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
    .registers 5
    .parameter "intents"
    .parameter "options"
    .parameter "userHandle"

    #@0
    .prologue
    .line 317
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->startActivitiesAsUser([Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    #@5
    .line 318
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 284
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@5
    .line 285
    return-void
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 4
    .parameter "intent"
    .parameter "options"

    #@0
    .prologue
    .line 295
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    #@5
    .line 296
    return-void
.end method

.method public startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
    .registers 5
    .parameter "intent"
    .parameter "options"
    .parameter "user"

    #@0
    .prologue
    .line 301
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    #@5
    .line 302
    return-void
.end method

.method public startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    .registers 4
    .parameter "intent"
    .parameter "user"

    #@0
    .prologue
    .line 290
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@5
    .line 291
    return-void
.end method

.method public startInstrumentation(Landroid/content/ComponentName;Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 5
    .parameter "className"
    .parameter "profileFile"
    .parameter "arguments"

    #@0
    .prologue
    .line 490
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->startInstrumentation(Landroid/content/ComponentName;Ljava/lang/String;Landroid/os/Bundle;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    .registers 12
    .parameter "intent"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "extraFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 324
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V

    #@a
    .line 326
    return-void
.end method

.method public startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;IIILandroid/os/Bundle;)V
    .registers 14
    .parameter "intent"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "extraFlags"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 332
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move-object v6, p6

    #@8
    invoke-virtual/range {v0 .. v6}, Landroid/content/Context;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;IIILandroid/os/Bundle;)V

    #@b
    .line 334
    return-void
.end method

.method public startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 450
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    .registers 4
    .parameter "service"
    .parameter "user"

    #@0
    .prologue
    .line 461
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public stopService(Landroid/content/Intent;)Z
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 455
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public stopServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Z
    .registers 4
    .parameter "name"
    .parameter "user"

    #@0
    .prologue
    .line 467
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->stopServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public unbindService(Landroid/content/ServiceConnection;)V
    .registers 3
    .parameter "conn"

    #@0
    .prologue
    .line 484
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@5
    .line 485
    return-void
.end method

.method public unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    .registers 3
    .parameter "receiver"

    #@0
    .prologue
    .line 445
    iget-object v0, p0, Landroid/content/ContextWrapper;->mBase:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@5
    .line 446
    return-void
.end method
