.class public final Landroid/content/ComponentName;
.super Ljava/lang/Object;
.source "ComponentName.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Cloneable;",
        "Ljava/lang/Comparable",
        "<",
        "Landroid/content/ComponentName;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mClass:Ljava/lang/String;

.field private final mPackage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 256
    new-instance v0, Landroid/content/ComponentName$1;

    #@2
    invoke-direct {v0}, Landroid/content/ComponentName$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .registers 4
    .parameter "pkg"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 74
    .local p2, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@9
    .line 76
    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@f
    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter "pkg"
    .parameter "cls"

    #@0
    .prologue
    .line 60
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 61
    if-nez p2, :cond_d

    #@5
    new-instance v0, Ljava/lang/NullPointerException;

    #@7
    const-string v1, "class name is null"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 62
    :cond_d
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@13
    .line 63
    iput-object p2, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@15
    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 277
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 278
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@9
    .line 279
    iget-object v0, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@b
    if-nez v0, :cond_16

    #@d
    new-instance v0, Ljava/lang/NullPointerException;

    #@f
    const-string/jumbo v1, "package name is null"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 281
    :cond_16
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@1c
    .line 282
    iget-object v0, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@1e
    if-nez v0, :cond_28

    #@20
    new-instance v0, Ljava/lang/NullPointerException;

    #@22
    const-string v1, "class name is null"

    #@24
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@27
    throw v0

    #@28
    .line 284
    :cond_28
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/os/Parcel;)V
    .registers 4
    .parameter "pkg"
    .parameter "in"

    #@0
    .prologue
    .line 286
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 287
    iput-object p1, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@5
    .line 288
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@b
    .line 289
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "pkg"
    .parameter "cls"

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    if-nez p1, :cond_e

    #@5
    new-instance v0, Ljava/lang/NullPointerException;

    #@7
    const-string/jumbo v1, "package name is null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 47
    :cond_e
    if-nez p2, :cond_18

    #@10
    new-instance v0, Ljava/lang/NullPointerException;

    #@12
    const-string v1, "class name is null"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 48
    :cond_18
    iput-object p1, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@1a
    .line 49
    iput-object p2, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@1c
    .line 50
    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 252
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 253
    .local v0, pkg:Ljava/lang/String;
    if-eqz v0, :cond_c

    #@6
    new-instance v1, Landroid/content/ComponentName;

    #@8
    invoke-direct {v1, v0, p0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method public static unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;
    .registers 7
    .parameter "str"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 160
    const/16 v3, 0x2f

    #@3
    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(I)I

    #@6
    move-result v2

    #@7
    .line 161
    .local v2, sep:I
    if-ltz v2, :cond_11

    #@9
    add-int/lit8 v3, v2, 0x1

    #@b
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@e
    move-result v4

    #@f
    if-lt v3, v4, :cond_13

    #@11
    .line 162
    :cond_11
    const/4 v3, 0x0

    #@12
    .line 169
    :goto_12
    return-object v3

    #@13
    .line 164
    :cond_13
    invoke-virtual {p0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    .line 165
    .local v1, pkg:Ljava/lang/String;
    add-int/lit8 v3, v2, 0x1

    #@19
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 166
    .local v0, cls:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@20
    move-result v3

    #@21
    if-lez v3, :cond_3c

    #@23
    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    #@26
    move-result v3

    #@27
    const/16 v4, 0x2e

    #@29
    if-ne v3, v4, :cond_3c

    #@2b
    .line 167
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    .line 169
    :cond_3c
    new-instance v3, Landroid/content/ComponentName;

    #@3e
    invoke-direct {v3, v1, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    goto :goto_12
.end method

.method public static writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V
    .registers 3
    .parameter "c"
    .parameter "out"

    #@0
    .prologue
    .line 233
    if-eqz p0, :cond_7

    #@2
    .line 234
    const/4 v0, 0x0

    #@3
    invoke-virtual {p0, p1, v0}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@6
    .line 238
    :goto_6
    return-void

    #@7
    .line 236
    :cond_7
    const/4 v0, 0x0

    #@8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b
    goto :goto_6
.end method


# virtual methods
.method public clone()Landroid/content/ComponentName;
    .registers 4

    #@0
    .prologue
    .line 80
    new-instance v0, Landroid/content/ComponentName;

    #@2
    iget-object v1, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@4
    iget-object v2, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@6
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 33
    invoke-virtual {p0}, Landroid/content/ComponentName;->clone()Landroid/content/ComponentName;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public compareTo(Landroid/content/ComponentName;)I
    .registers 5
    .parameter "that"

    #@0
    .prologue
    .line 207
    iget-object v1, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@2
    iget-object v2, p1, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@4
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    .line 208
    .local v0, v:I
    if-eqz v0, :cond_b

    #@a
    .line 211
    .end local v0           #v:I
    :goto_a
    return v0

    #@b
    .restart local v0       #v:I
    :cond_b
    iget-object v1, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@d
    iget-object v2, p1, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@12
    move-result v0

    #@13
    goto :goto_a
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 33
    check-cast p1, Landroid/content/ComponentName;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/content/ComponentName;->compareTo(Landroid/content/ComponentName;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 215
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "obj"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 188
    if-eqz p1, :cond_1c

    #@3
    .line 189
    :try_start_3
    move-object v0, p1

    #@4
    check-cast v0, Landroid/content/ComponentName;

    #@6
    move-object v1, v0

    #@7
    .line 192
    .local v1, other:Landroid/content/ComponentName;
    iget-object v3, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@9
    iget-object v4, v1, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1c

    #@11
    iget-object v3, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@13
    iget-object v4, v1, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_18
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_18} :catch_1d

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_1c

    #@1b
    const/4 v2, 0x1

    #@1c
    .line 197
    .end local v1           #other:Landroid/content/ComponentName;
    :cond_1c
    :goto_1c
    return v2

    #@1d
    .line 195
    :catch_1d
    move-exception v3

    #@1e
    goto :goto_1c
.end method

.method public flattenToShortString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    iget-object v1, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    const-string v1, "/"

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {p0}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method

.method public flattenToString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    iget-object v1, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    const-string v1, "/"

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    iget-object v1, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method

.method public getClassName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getShortClassName()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 102
    iget-object v2, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@2
    iget-object v3, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@4
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_29

    #@a
    .line 103
    iget-object v2, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@f
    move-result v1

    #@10
    .line 104
    .local v1, PN:I
    iget-object v2, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@12
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@15
    move-result v0

    #@16
    .line 105
    .local v0, CN:I
    if-le v0, v1, :cond_29

    #@18
    iget-object v2, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@1a
    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    #@1d
    move-result v2

    #@1e
    const/16 v3, 0x2e

    #@20
    if-ne v2, v3, :cond_29

    #@22
    .line 106
    iget-object v2, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@24
    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    .line 109
    .end local v0           #CN:I
    .end local v1           #PN:I
    :goto_28
    return-object v2

    #@29
    :cond_29
    iget-object v2, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@2b
    goto :goto_28
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@5
    move-result v0

    #@6
    iget-object v1, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@8
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@b
    move-result v1

    #@c
    add-int/2addr v0, v1

    #@d
    return v0
.end method

.method public toShortString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v1, "{"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    iget-object v1, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, "/"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    iget-object v1, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string/jumbo v1, "}"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ComponentInfo{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "/"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string/jumbo v1, "}"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 219
    iget-object v0, p0, Landroid/content/ComponentName;->mPackage:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 220
    iget-object v0, p0, Landroid/content/ComponentName;->mClass:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 221
    return-void
.end method
