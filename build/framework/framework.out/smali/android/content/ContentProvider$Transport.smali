.class Landroid/content/ContentProvider$Transport;
.super Landroid/content/ContentProviderNative;
.source "ContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/ContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Transport"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/content/ContentProvider;


# direct methods
.method constructor <init>(Landroid/content/ContentProvider;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 174
    iput-object p1, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@2
    invoke-direct {p0}, Landroid/content/ContentProviderNative;-><init>()V

    #@5
    return-void
.end method

.method private enforceReadPermission(Landroid/net/Uri;)V
    .registers 21
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 277
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@4
    move-object/from16 v16, v0

    #@6
    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    #@9
    move-result-object v5

    #@a
    .line 278
    .local v5, context:Landroid/content/Context;
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@d
    move-result v12

    #@e
    .line 279
    .local v12, pid:I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@11
    move-result v15

    #@12
    .line 280
    .local v15, uid:I
    const/4 v9, 0x0

    #@13
    .line 282
    .local v9, missingPerm:Ljava/lang/String;
    move-object/from16 v0, p0

    #@15
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@17
    move-object/from16 v16, v0

    #@19
    invoke-static/range {v16 .. v16}, Landroid/content/ContentProvider;->access$000(Landroid/content/ContentProvider;)I

    #@1c
    move-result v16

    #@1d
    invoke-static/range {v15 .. v16}, Landroid/os/UserHandle;->isSameApp(II)Z

    #@20
    move-result v16

    #@21
    if-eqz v16, :cond_24

    #@23
    .line 326
    :cond_23
    return-void

    #@24
    .line 286
    :cond_24
    move-object/from16 v0, p0

    #@26
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@28
    move-object/from16 v16, v0

    #@2a
    invoke-static/range {v16 .. v16}, Landroid/content/ContentProvider;->access$100(Landroid/content/ContentProvider;)Z

    #@2d
    move-result v16

    #@2e
    if-eqz v16, :cond_78

    #@30
    .line 287
    move-object/from16 v0, p0

    #@32
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@34
    move-object/from16 v16, v0

    #@36
    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentProvider;->getReadPermission()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    .line 288
    .local v4, componentPerm:Ljava/lang/String;
    if-eqz v4, :cond_43

    #@3c
    .line 289
    invoke-virtual {v5, v4, v12, v15}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@3f
    move-result v16

    #@40
    if-eqz v16, :cond_23

    #@42
    .line 292
    move-object v9, v4

    #@43
    .line 298
    :cond_43
    if-nez v4, :cond_74

    #@45
    const/4 v2, 0x1

    #@46
    .line 300
    .local v2, allowDefaultRead:Z
    :goto_46
    move-object/from16 v0, p0

    #@48
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@4a
    move-object/from16 v16, v0

    #@4c
    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentProvider;->getPathPermissions()[Landroid/content/pm/PathPermission;

    #@4f
    move-result-object v14

    #@50
    .line 301
    .local v14, pps:[Landroid/content/pm/PathPermission;
    if-eqz v14, :cond_76

    #@52
    .line 302
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@55
    move-result-object v10

    #@56
    .line 303
    .local v10, path:Ljava/lang/String;
    move-object v3, v14

    #@57
    .local v3, arr$:[Landroid/content/pm/PathPermission;
    array-length v8, v3

    #@58
    .local v8, len$:I
    const/4 v7, 0x0

    #@59
    .local v7, i$:I
    :goto_59
    if-ge v7, v8, :cond_76

    #@5b
    aget-object v13, v3, v7

    #@5d
    .line 304
    .local v13, pp:Landroid/content/pm/PathPermission;
    invoke-virtual {v13}, Landroid/content/pm/PathPermission;->getReadPermission()Ljava/lang/String;

    #@60
    move-result-object v11

    #@61
    .line 305
    .local v11, pathPerm:Ljava/lang/String;
    if-eqz v11, :cond_71

    #@63
    invoke-virtual {v13, v10}, Landroid/content/pm/PathPermission;->match(Ljava/lang/String;)Z

    #@66
    move-result v16

    #@67
    if-eqz v16, :cond_71

    #@69
    .line 306
    invoke-virtual {v5, v11, v12, v15}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@6c
    move-result v16

    #@6d
    if-eqz v16, :cond_23

    #@6f
    .line 311
    const/4 v2, 0x0

    #@70
    .line 312
    move-object v9, v11

    #@71
    .line 303
    :cond_71
    add-int/lit8 v7, v7, 0x1

    #@73
    goto :goto_59

    #@74
    .line 298
    .end local v2           #allowDefaultRead:Z
    .end local v3           #arr$:[Landroid/content/pm/PathPermission;
    .end local v7           #i$:I
    .end local v8           #len$:I
    .end local v10           #path:Ljava/lang/String;
    .end local v11           #pathPerm:Ljava/lang/String;
    .end local v13           #pp:Landroid/content/pm/PathPermission;
    .end local v14           #pps:[Landroid/content/pm/PathPermission;
    :cond_74
    const/4 v2, 0x0

    #@75
    goto :goto_46

    #@76
    .line 320
    .restart local v2       #allowDefaultRead:Z
    .restart local v14       #pps:[Landroid/content/pm/PathPermission;
    :cond_76
    if-nez v2, :cond_23

    #@78
    .line 324
    .end local v2           #allowDefaultRead:Z
    .end local v4           #componentPerm:Ljava/lang/String;
    .end local v14           #pps:[Landroid/content/pm/PathPermission;
    :cond_78
    const/16 v16, 0x1

    #@7a
    move-object/from16 v0, p1

    #@7c
    move/from16 v1, v16

    #@7e
    invoke-virtual {v5, v0, v12, v15, v1}, Landroid/content/Context;->checkUriPermission(Landroid/net/Uri;III)I

    #@81
    move-result v16

    #@82
    if-eqz v16, :cond_23

    #@84
    .line 329
    move-object/from16 v0, p0

    #@86
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@88
    move-object/from16 v16, v0

    #@8a
    invoke-static/range {v16 .. v16}, Landroid/content/ContentProvider;->access$100(Landroid/content/ContentProvider;)Z

    #@8d
    move-result v16

    #@8e
    if-eqz v16, :cond_fe

    #@90
    new-instance v16, Ljava/lang/StringBuilder;

    #@92
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v17, " requires "

    #@97
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v16

    #@9b
    move-object/from16 v0, v16

    #@9d
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v16

    #@a1
    const-string v17, ", or grantUriPermission()"

    #@a3
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v16

    #@a7
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v6

    #@ab
    .line 332
    .local v6, failReason:Ljava/lang/String;
    :goto_ab
    new-instance v16, Ljava/lang/SecurityException;

    #@ad
    new-instance v17, Ljava/lang/StringBuilder;

    #@af
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v18, "Permission Denial: reading "

    #@b4
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v17

    #@b8
    move-object/from16 v0, p0

    #@ba
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@bc
    move-object/from16 v18, v0

    #@be
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@c1
    move-result-object v18

    #@c2
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@c5
    move-result-object v18

    #@c6
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v17

    #@ca
    const-string v18, " uri "

    #@cc
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v17

    #@d0
    move-object/from16 v0, v17

    #@d2
    move-object/from16 v1, p1

    #@d4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v17

    #@d8
    const-string v18, " from pid="

    #@da
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v17

    #@de
    move-object/from16 v0, v17

    #@e0
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v17

    #@e4
    const-string v18, ", uid="

    #@e6
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v17

    #@ea
    move-object/from16 v0, v17

    #@ec
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v17

    #@f0
    move-object/from16 v0, v17

    #@f2
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v17

    #@f6
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v17

    #@fa
    invoke-direct/range {v16 .. v17}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@fd
    throw v16

    #@fe
    .line 329
    .end local v6           #failReason:Ljava/lang/String;
    :cond_fe
    const-string v6, " requires the provider be exported, or grantUriPermission()"

    #@100
    goto :goto_ab
.end method

.method private enforceWritePermission(Landroid/net/Uri;)V
    .registers 21
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 338
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@4
    move-object/from16 v16, v0

    #@6
    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    #@9
    move-result-object v5

    #@a
    .line 339
    .local v5, context:Landroid/content/Context;
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@d
    move-result v12

    #@e
    .line 340
    .local v12, pid:I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@11
    move-result v15

    #@12
    .line 341
    .local v15, uid:I
    const/4 v9, 0x0

    #@13
    .line 343
    .local v9, missingPerm:Ljava/lang/String;
    move-object/from16 v0, p0

    #@15
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@17
    move-object/from16 v16, v0

    #@19
    invoke-static/range {v16 .. v16}, Landroid/content/ContentProvider;->access$000(Landroid/content/ContentProvider;)I

    #@1c
    move-result v16

    #@1d
    invoke-static/range {v15 .. v16}, Landroid/os/UserHandle;->isSameApp(II)Z

    #@20
    move-result v16

    #@21
    if-eqz v16, :cond_24

    #@23
    .line 387
    :cond_23
    return-void

    #@24
    .line 347
    :cond_24
    move-object/from16 v0, p0

    #@26
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@28
    move-object/from16 v16, v0

    #@2a
    invoke-static/range {v16 .. v16}, Landroid/content/ContentProvider;->access$100(Landroid/content/ContentProvider;)Z

    #@2d
    move-result v16

    #@2e
    if-eqz v16, :cond_78

    #@30
    .line 348
    move-object/from16 v0, p0

    #@32
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@34
    move-object/from16 v16, v0

    #@36
    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentProvider;->getWritePermission()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    .line 349
    .local v4, componentPerm:Ljava/lang/String;
    if-eqz v4, :cond_43

    #@3c
    .line 350
    invoke-virtual {v5, v4, v12, v15}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@3f
    move-result v16

    #@40
    if-eqz v16, :cond_23

    #@42
    .line 353
    move-object v9, v4

    #@43
    .line 359
    :cond_43
    if-nez v4, :cond_74

    #@45
    const/4 v2, 0x1

    #@46
    .line 361
    .local v2, allowDefaultWrite:Z
    :goto_46
    move-object/from16 v0, p0

    #@48
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@4a
    move-object/from16 v16, v0

    #@4c
    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentProvider;->getPathPermissions()[Landroid/content/pm/PathPermission;

    #@4f
    move-result-object v14

    #@50
    .line 362
    .local v14, pps:[Landroid/content/pm/PathPermission;
    if-eqz v14, :cond_76

    #@52
    .line 363
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@55
    move-result-object v10

    #@56
    .line 364
    .local v10, path:Ljava/lang/String;
    move-object v3, v14

    #@57
    .local v3, arr$:[Landroid/content/pm/PathPermission;
    array-length v8, v3

    #@58
    .local v8, len$:I
    const/4 v7, 0x0

    #@59
    .local v7, i$:I
    :goto_59
    if-ge v7, v8, :cond_76

    #@5b
    aget-object v13, v3, v7

    #@5d
    .line 365
    .local v13, pp:Landroid/content/pm/PathPermission;
    invoke-virtual {v13}, Landroid/content/pm/PathPermission;->getWritePermission()Ljava/lang/String;

    #@60
    move-result-object v11

    #@61
    .line 366
    .local v11, pathPerm:Ljava/lang/String;
    if-eqz v11, :cond_71

    #@63
    invoke-virtual {v13, v10}, Landroid/content/pm/PathPermission;->match(Ljava/lang/String;)Z

    #@66
    move-result v16

    #@67
    if-eqz v16, :cond_71

    #@69
    .line 367
    invoke-virtual {v5, v11, v12, v15}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@6c
    move-result v16

    #@6d
    if-eqz v16, :cond_23

    #@6f
    .line 372
    const/4 v2, 0x0

    #@70
    .line 373
    move-object v9, v11

    #@71
    .line 364
    :cond_71
    add-int/lit8 v7, v7, 0x1

    #@73
    goto :goto_59

    #@74
    .line 359
    .end local v2           #allowDefaultWrite:Z
    .end local v3           #arr$:[Landroid/content/pm/PathPermission;
    .end local v7           #i$:I
    .end local v8           #len$:I
    .end local v10           #path:Ljava/lang/String;
    .end local v11           #pathPerm:Ljava/lang/String;
    .end local v13           #pp:Landroid/content/pm/PathPermission;
    .end local v14           #pps:[Landroid/content/pm/PathPermission;
    :cond_74
    const/4 v2, 0x0

    #@75
    goto :goto_46

    #@76
    .line 381
    .restart local v2       #allowDefaultWrite:Z
    .restart local v14       #pps:[Landroid/content/pm/PathPermission;
    :cond_76
    if-nez v2, :cond_23

    #@78
    .line 385
    .end local v2           #allowDefaultWrite:Z
    .end local v4           #componentPerm:Ljava/lang/String;
    .end local v14           #pps:[Landroid/content/pm/PathPermission;
    :cond_78
    const/16 v16, 0x2

    #@7a
    move-object/from16 v0, p1

    #@7c
    move/from16 v1, v16

    #@7e
    invoke-virtual {v5, v0, v12, v15, v1}, Landroid/content/Context;->checkUriPermission(Landroid/net/Uri;III)I

    #@81
    move-result v16

    #@82
    if-eqz v16, :cond_23

    #@84
    .line 390
    move-object/from16 v0, p0

    #@86
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@88
    move-object/from16 v16, v0

    #@8a
    invoke-static/range {v16 .. v16}, Landroid/content/ContentProvider;->access$100(Landroid/content/ContentProvider;)Z

    #@8d
    move-result v16

    #@8e
    if-eqz v16, :cond_fe

    #@90
    new-instance v16, Ljava/lang/StringBuilder;

    #@92
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v17, " requires "

    #@97
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v16

    #@9b
    move-object/from16 v0, v16

    #@9d
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v16

    #@a1
    const-string v17, ", or grantUriPermission()"

    #@a3
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v16

    #@a7
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v6

    #@ab
    .line 393
    .local v6, failReason:Ljava/lang/String;
    :goto_ab
    new-instance v16, Ljava/lang/SecurityException;

    #@ad
    new-instance v17, Ljava/lang/StringBuilder;

    #@af
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v18, "Permission Denial: writing "

    #@b4
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v17

    #@b8
    move-object/from16 v0, p0

    #@ba
    iget-object v0, v0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@bc
    move-object/from16 v18, v0

    #@be
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@c1
    move-result-object v18

    #@c2
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@c5
    move-result-object v18

    #@c6
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v17

    #@ca
    const-string v18, " uri "

    #@cc
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v17

    #@d0
    move-object/from16 v0, v17

    #@d2
    move-object/from16 v1, p1

    #@d4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v17

    #@d8
    const-string v18, " from pid="

    #@da
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v17

    #@de
    move-object/from16 v0, v17

    #@e0
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v17

    #@e4
    const-string v18, ", uid="

    #@e6
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v17

    #@ea
    move-object/from16 v0, v17

    #@ec
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v17

    #@f0
    move-object/from16 v0, v17

    #@f2
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v17

    #@f6
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v17

    #@fa
    invoke-direct/range {v16 .. v17}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@fd
    throw v16

    #@fe
    .line 390
    .end local v6           #failReason:Ljava/lang/String;
    :cond_fe
    const-string v6, " requires the provider be exported, or grantUriPermission()"

    #@100
    goto :goto_ab
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    #@0
    .prologue
    .line 213
    .local p1, operations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3
    move-result-object v0

    #@4
    .local v0, i$:Ljava/util/Iterator;
    :cond_4
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_2b

    #@a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Landroid/content/ContentProviderOperation;

    #@10
    .line 214
    .local v1, operation:Landroid/content/ContentProviderOperation;
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation;->isReadOperation()Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_1d

    #@16
    .line 215
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation;->getUri()Landroid/net/Uri;

    #@19
    move-result-object v2

    #@1a
    invoke-direct {p0, v2}, Landroid/content/ContentProvider$Transport;->enforceReadPermission(Landroid/net/Uri;)V

    #@1d
    .line 218
    :cond_1d
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation;->isWriteOperation()Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_4

    #@23
    .line 219
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation;->getUri()Landroid/net/Uri;

    #@26
    move-result-object v2

    #@27
    invoke-direct {p0, v2}, Landroid/content/ContentProvider$Transport;->enforceWritePermission(Landroid/net/Uri;)V

    #@2a
    goto :goto_4

    #@2b
    .line 222
    .end local v1           #operation:Landroid/content/ContentProviderOperation;
    :cond_2b
    iget-object v2, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@2d
    invoke-virtual {v2, p1}, Landroid/content/ContentProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    #@30
    move-result-object v2

    #@31
    return-object v2
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .registers 4
    .parameter "uri"
    .parameter "initialValues"

    #@0
    .prologue
    .line 206
    invoke-direct {p0, p1}, Landroid/content/ContentProvider$Transport;->enforceWritePermission(Landroid/net/Uri;)V

    #@3
    .line 207
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@5
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 5
    .parameter "method"
    .parameter "arg"
    .parameter "extras"

    #@0
    .prologue
    .line 256
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/ContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public createCancellationSignal()Landroid/os/ICancellationSignal;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 273
    invoke-static {}, Landroid/os/CancellationSignal;->createTransport()Landroid/os/ICancellationSignal;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 5
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 227
    invoke-direct {p0, p1}, Landroid/content/ContentProvider$Transport;->enforceWritePermission(Landroid/net/Uri;)V

    #@3
    .line 228
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/ContentProvider;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method getContentProvider()Landroid/content/ContentProvider;
    .registers 2

    #@0
    .prologue
    .line 176
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@2
    return-object v0
.end method

.method public getProviderName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 181
    invoke-virtual {p0}, Landroid/content/ContentProvider$Transport;->getContentProvider()Landroid/content/ContentProvider;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;
    .registers 4
    .parameter "uri"
    .parameter "mimeTypeFilter"

    #@0
    .prologue
    .line 261
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentProvider;->getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 3
    .parameter "uri"

    #@0
    .prologue
    .line 195
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/ContentProvider;->getType(Landroid/net/Uri;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 4
    .parameter "uri"
    .parameter "initialValues"

    #@0
    .prologue
    .line 200
    invoke-direct {p0, p1}, Landroid/content/ContentProvider$Transport;->enforceWritePermission(Landroid/net/Uri;)V

    #@3
    .line 201
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@5
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .registers 5
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 249
    if-eqz p2, :cond_15

    #@2
    const/16 v0, 0x77

    #@4
    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    #@7
    move-result v0

    #@8
    const/4 v1, -0x1

    #@9
    if-eq v0, v1, :cond_15

    #@b
    invoke-direct {p0, p1}, Landroid/content/ContentProvider$Transport;->enforceWritePermission(Landroid/net/Uri;)V

    #@e
    .line 251
    :goto_e
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@10
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@13
    move-result-object v0

    #@14
    return-object v0

    #@15
    .line 250
    :cond_15
    invoke-direct {p0, p1}, Landroid/content/ContentProvider$Transport;->enforceReadPermission(Landroid/net/Uri;)V

    #@18
    goto :goto_e
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 5
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 241
    if-eqz p2, :cond_15

    #@2
    const/16 v0, 0x77

    #@4
    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    #@7
    move-result v0

    #@8
    const/4 v1, -0x1

    #@9
    if-eq v0, v1, :cond_15

    #@b
    invoke-direct {p0, p1}, Landroid/content/ContentProvider$Transport;->enforceWritePermission(Landroid/net/Uri;)V

    #@e
    .line 243
    :goto_e
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@10
    invoke-virtual {v0, p1, p2}, Landroid/content/ContentProvider;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@13
    move-result-object v0

    #@14
    return-object v0

    #@15
    .line 242
    :cond_15
    invoke-direct {p0, p1}, Landroid/content/ContentProvider$Transport;->enforceReadPermission(Landroid/net/Uri;)V

    #@18
    goto :goto_e
.end method

.method public openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
    .registers 5
    .parameter "uri"
    .parameter "mimeType"
    .parameter "opts"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 267
    invoke-direct {p0, p1}, Landroid/content/ContentProvider$Transport;->enforceReadPermission(Landroid/net/Uri;)V

    #@3
    .line 268
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/content/ContentProvider;->openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;
    .registers 14
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"
    .parameter "cancellationSignal"

    #@0
    .prologue
    .line 188
    invoke-direct {p0, p1}, Landroid/content/ContentProvider$Transport;->enforceReadPermission(Landroid/net/Uri;)V

    #@3
    .line 189
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@5
    invoke-static {p6}, Landroid/os/CancellationSignal;->fromTransport(Landroid/os/ICancellationSignal;)Landroid/os/CancellationSignal;

    #@8
    move-result-object v6

    #@9
    move-object v1, p1

    #@a
    move-object v2, p2

    #@b
    move-object v3, p3

    #@c
    move-object v4, p4

    #@d
    move-object v5, p5

    #@e
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    #@11
    move-result-object v0

    #@12
    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 6
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    .line 234
    invoke-direct {p0, p1}, Landroid/content/ContentProvider$Transport;->enforceWritePermission(Landroid/net/Uri;)V

    #@3
    .line 235
    iget-object v0, p0, Landroid/content/ContentProvider$Transport;->this$0:Landroid/content/ContentProvider;

    #@5
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/content/ContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@8
    move-result v0

    #@9
    return v0
.end method
