.class Landroid/content/SyncAdaptersCache;
.super Landroid/content/pm/RegisteredServicesCache;
.source "SyncAdaptersCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/SyncAdaptersCache$MySerializer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/pm/RegisteredServicesCache",
        "<",
        "Landroid/content/SyncAdapterType;",
        ">;"
    }
.end annotation


# static fields
.field private static final ATTRIBUTES_NAME:Ljava/lang/String; = "sync-adapter"

.field private static final SERVICE_INTERFACE:Ljava/lang/String; = "android.content.SyncAdapter"

.field private static final SERVICE_META_DATA:Ljava/lang/String; = "android.content.SyncAdapter"

.field private static final TAG:Ljava/lang/String; = "Account"

.field private static final sSerializer:Landroid/content/SyncAdaptersCache$MySerializer;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 40
    new-instance v0, Landroid/content/SyncAdaptersCache$MySerializer;

    #@2
    invoke-direct {v0}, Landroid/content/SyncAdaptersCache$MySerializer;-><init>()V

    #@5
    sput-object v0, Landroid/content/SyncAdaptersCache;->sSerializer:Landroid/content/SyncAdaptersCache$MySerializer;

    #@7
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 43
    const-string v2, "android.content.SyncAdapter"

    #@2
    const-string v3, "android.content.SyncAdapter"

    #@4
    const-string/jumbo v4, "sync-adapter"

    #@7
    sget-object v5, Landroid/content/SyncAdaptersCache;->sSerializer:Landroid/content/SyncAdaptersCache$MySerializer;

    #@9
    move-object v0, p0

    #@a
    move-object v1, p1

    #@b
    invoke-direct/range {v0 .. v5}, Landroid/content/pm/RegisteredServicesCache;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/XmlSerializerAndParser;)V

    #@e
    .line 44
    return-void
.end method


# virtual methods
.method public parseServiceAttributes(Landroid/content/res/Resources;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/content/SyncAdapterType;
    .registers 14
    .parameter "res"
    .parameter "packageName"
    .parameter "attrs"

    #@0
    .prologue
    .line 48
    sget-object v0, Lcom/android/internal/R$styleable;->SyncAdapter:[I

    #@2
    invoke-virtual {p1, p3, v0}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@5
    move-result-object v8

    #@6
    .line 51
    .local v8, sa:Landroid/content/res/TypedArray;
    const/4 v0, 0x2

    #@7
    :try_start_7
    invoke-virtual {v8, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 53
    .local v1, authority:Ljava/lang/String;
    const/4 v0, 0x1

    #@c
    invoke-virtual {v8, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;
    :try_end_f
    .catchall {:try_start_7 .. :try_end_f} :catchall_3f

    #@f
    move-result-object v2

    #@10
    .line 55
    .local v2, accountType:Ljava/lang/String;
    if-eqz v1, :cond_14

    #@12
    if-nez v2, :cond_19

    #@14
    .line 56
    :cond_14
    const/4 v0, 0x0

    #@15
    .line 75
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@18
    :goto_18
    return-object v0

    #@19
    .line 58
    :cond_19
    const/4 v0, 0x3

    #@1a
    const/4 v9, 0x1

    #@1b
    :try_start_1b
    invoke-virtual {v8, v0, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1e
    move-result v3

    #@1f
    .line 60
    .local v3, userVisible:Z
    const/4 v0, 0x4

    #@20
    const/4 v9, 0x1

    #@21
    invoke-virtual {v8, v0, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@24
    move-result v4

    #@25
    .line 63
    .local v4, supportsUploading:Z
    const/4 v0, 0x6

    #@26
    const/4 v9, 0x0

    #@27
    invoke-virtual {v8, v0, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2a
    move-result v5

    #@2b
    .line 66
    .local v5, isAlwaysSyncable:Z
    const/4 v0, 0x5

    #@2c
    const/4 v9, 0x0

    #@2d
    invoke-virtual {v8, v0, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@30
    move-result v6

    #@31
    .line 69
    .local v6, allowParallelSyncs:Z
    const/4 v0, 0x0

    #@32
    invoke-virtual {v8, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@35
    move-result-object v7

    #@36
    .line 72
    .local v7, settingsActivity:Ljava/lang/String;
    new-instance v0, Landroid/content/SyncAdapterType;

    #@38
    invoke-direct/range {v0 .. v7}, Landroid/content/SyncAdapterType;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;)V
    :try_end_3b
    .catchall {:try_start_1b .. :try_end_3b} :catchall_3f

    #@3b
    .line 75
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@3e
    goto :goto_18

    #@3f
    .end local v1           #authority:Ljava/lang/String;
    .end local v2           #accountType:Ljava/lang/String;
    .end local v3           #userVisible:Z
    .end local v4           #supportsUploading:Z
    .end local v5           #isAlwaysSyncable:Z
    .end local v6           #allowParallelSyncs:Z
    .end local v7           #settingsActivity:Ljava/lang/String;
    :catchall_3f
    move-exception v0

    #@40
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@43
    throw v0
.end method

.method public bridge synthetic parseServiceAttributes(Landroid/content/res/Resources;Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 34
    invoke-virtual {p0, p1, p2, p3}, Landroid/content/SyncAdaptersCache;->parseServiceAttributes(Landroid/content/res/Resources;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/content/SyncAdapterType;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
