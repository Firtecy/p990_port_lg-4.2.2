.class public Landroid/content/IntentSender;
.super Ljava/lang/Object;
.source "IntentSender.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/IntentSender$FinishedDispatcher;,
        Landroid/content/IntentSender$OnFinished;,
        Landroid/content/IntentSender$SendIntentException;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/IntentSender;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mTarget:Landroid/content/IIntentSender;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 319
    new-instance v0, Landroid/content/IntentSender$1;

    #@2
    invoke-direct {v0}, Landroid/content/IntentSender$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/IntentSender;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/IIntentSender;)V
    .registers 2
    .parameter "target"

    #@0
    .prologue
    .line 366
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 367
    iput-object p1, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@5
    .line 368
    return-void
.end method

.method public constructor <init>(Landroid/os/IBinder;)V
    .registers 3
    .parameter "target"

    #@0
    .prologue
    .line 371
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 372
    invoke-static {p1}, Landroid/content/IIntentSender$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentSender;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@9
    .line 373
    return-void
.end method

.method public static readIntentSenderOrNullFromParcel(Landroid/os/Parcel;)Landroid/content/IntentSender;
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 356
    invoke-virtual {p0}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3
    move-result-object v0

    #@4
    .line 357
    .local v0, b:Landroid/os/IBinder;
    if-eqz v0, :cond_c

    #@6
    new-instance v1, Landroid/content/IntentSender;

    #@8
    invoke-direct {v1, v0}, Landroid/content/IntentSender;-><init>(Landroid/os/IBinder;)V

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method public static writeIntentSenderOrNullToParcel(Landroid/content/IntentSender;Landroid/os/Parcel;)V
    .registers 3
    .parameter "sender"
    .parameter "out"

    #@0
    .prologue
    .line 341
    if-eqz p0, :cond_c

    #@2
    iget-object v0, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@4
    invoke-interface {v0}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v0

    #@8
    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@b
    .line 343
    return-void

    #@c
    .line 341
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_8
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 312
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "otherObj"

    #@0
    .prologue
    .line 288
    instance-of v0, p1, Landroid/content/IntentSender;

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 289
    iget-object v0, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v0}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    check-cast p1, Landroid/content/IntentSender;

    #@c
    .end local p1
    iget-object v1, p1, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@e
    invoke-interface {v1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    .line 292
    :goto_16
    return v0

    #@17
    .restart local p1
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method public getCreatorPackage()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 232
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getPackageForIntentSender(Landroid/content/IIntentSender;)Ljava/lang/String;
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result-object v1

    #@a
    .line 236
    :goto_a
    return-object v1

    #@b
    .line 234
    :catch_b
    move-exception v0

    #@c
    .line 236
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public getCreatorUid()I
    .registers 4

    #@0
    .prologue
    .line 251
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getUidForIntentSender(Landroid/content/IIntentSender;)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 255
    :goto_a
    return v1

    #@b
    .line 253
    :catch_b
    move-exception v0

    #@c
    .line 255
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@d
    goto :goto_a
.end method

.method public getCreatorUserHandle()Landroid/os/UserHandle;
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 272
    :try_start_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@4
    move-result-object v2

    #@5
    iget-object v4, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@7
    invoke-interface {v2, v4}, Landroid/app/IActivityManager;->getUidForIntentSender(Landroid/content/IIntentSender;)I

    #@a
    move-result v1

    #@b
    .line 274
    .local v1, uid:I
    if-lez v1, :cond_17

    #@d
    new-instance v2, Landroid/os/UserHandle;

    #@f
    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    #@12
    move-result v4

    #@13
    invoke-direct {v2, v4}, Landroid/os/UserHandle;-><init>(I)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_16} :catch_19

    #@16
    .line 277
    .end local v1           #uid:I
    :goto_16
    return-object v2

    #@17
    .restart local v1       #uid:I
    :cond_17
    move-object v2, v3

    #@18
    .line 274
    goto :goto_16

    #@19
    .line 275
    .end local v1           #uid:I
    :catch_19
    move-exception v0

    #@1a
    .local v0, e:Landroid/os/RemoteException;
    move-object v2, v3

    #@1b
    .line 277
    goto :goto_16
.end method

.method public getTarget()Landroid/content/IIntentSender;
    .registers 2

    #@0
    .prologue
    .line 362
    iget-object v0, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@2
    return-object v0
.end method

.method public getTargetPackage()Ljava/lang/String;
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 213
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getPackageForIntentSender(Landroid/content/IIntentSender;)Ljava/lang/String;
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result-object v1

    #@a
    .line 217
    :goto_a
    return-object v1

    #@b
    .line 215
    :catch_b
    move-exception v0

    #@c
    .line 217
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 297
    iget-object v0, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@2
    invoke-interface {v0}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public sendIntent(Landroid/content/Context;ILandroid/content/Intent;Landroid/content/IntentSender$OnFinished;Landroid/os/Handler;)V
    .registers 13
    .parameter "context"
    .parameter "code"
    .parameter "intent"
    .parameter "onFinished"
    .parameter "handler"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 158
    const/4 v6, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object v5, p5

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/content/IntentSender;->sendIntent(Landroid/content/Context;ILandroid/content/Intent;Landroid/content/IntentSender$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V

    #@a
    .line 159
    return-void
.end method

.method public sendIntent(Landroid/content/Context;ILandroid/content/Intent;Landroid/content/IntentSender$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V
    .registers 15
    .parameter "context"
    .parameter "code"
    .parameter "intent"
    .parameter "onFinished"
    .parameter "handler"
    .parameter "requiredPermission"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 191
    if-eqz p3, :cond_2a

    #@3
    :try_start_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p3, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 194
    .local v3, resolvedType:Ljava/lang/String;
    :goto_b
    iget-object v0, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@d
    if-eqz p4, :cond_14

    #@f
    new-instance v4, Landroid/content/IntentSender$FinishedDispatcher;

    #@11
    invoke-direct {v4, p0, p4, p5}, Landroid/content/IntentSender$FinishedDispatcher;-><init>(Landroid/content/IntentSender;Landroid/content/IntentSender$OnFinished;Landroid/os/Handler;)V

    #@14
    :cond_14
    move v1, p2

    #@15
    move-object v2, p3

    #@16
    move-object v5, p6

    #@17
    invoke-interface/range {v0 .. v5}, Landroid/content/IIntentSender;->send(ILandroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;Ljava/lang/String;)I

    #@1a
    move-result v7

    #@1b
    .line 199
    .local v7, res:I
    if-gez v7, :cond_2c

    #@1d
    .line 200
    new-instance v0, Landroid/content/IntentSender$SendIntentException;

    #@1f
    invoke-direct {v0}, Landroid/content/IntentSender$SendIntentException;-><init>()V

    #@22
    throw v0
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_23} :catch_23

    #@23
    .line 202
    .end local v3           #resolvedType:Ljava/lang/String;
    .end local v7           #res:I
    :catch_23
    move-exception v6

    #@24
    .line 203
    .local v6, e:Landroid/os/RemoteException;
    new-instance v0, Landroid/content/IntentSender$SendIntentException;

    #@26
    invoke-direct {v0}, Landroid/content/IntentSender$SendIntentException;-><init>()V

    #@29
    throw v0

    #@2a
    .end local v6           #e:Landroid/os/RemoteException;
    :cond_2a
    move-object v3, v4

    #@2b
    .line 191
    goto :goto_b

    #@2c
    .line 205
    .restart local v3       #resolvedType:Ljava/lang/String;
    .restart local v7       #res:I
    :cond_2c
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 302
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 303
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "IntentSender{"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 304
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@f
    move-result v1

    #@10
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 305
    const-string v1, ": "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 306
    iget-object v1, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@1e
    if-eqz v1, :cond_33

    #@20
    iget-object v1, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@22
    invoke-interface {v1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@25
    move-result-object v1

    #@26
    :goto_26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    .line 307
    const/16 v1, 0x7d

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2e
    .line 308
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    return-object v1

    #@33
    .line 306
    :cond_33
    const/4 v1, 0x0

    #@34
    goto :goto_26
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 316
    iget-object v0, p0, Landroid/content/IntentSender;->mTarget:Landroid/content/IIntentSender;

    #@2
    invoke-interface {v0}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@9
    .line 317
    return-void
.end method
