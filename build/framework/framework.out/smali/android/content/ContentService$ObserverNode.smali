.class public final Landroid/content/ContentService$ObserverNode;
.super Ljava/lang/Object;
.source "ContentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/ContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ObserverNode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/ContentService$ObserverNode$ObserverEntry;
    }
.end annotation


# static fields
.field public static final DELETE_TYPE:I = 0x2

.field public static final INSERT_TYPE:I = 0x0

.field public static final UPDATE_TYPE:I = 0x1


# instance fields
.field private mChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentService$ObserverNode;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private mObservers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentService$ObserverNode$ObserverEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 665
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 662
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@a
    .line 663
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@11
    .line 666
    iput-object p1, p0, Landroid/content/ContentService$ObserverNode;->mName:Ljava/lang/String;

    #@13
    .line 667
    return-void
.end method

.method static synthetic access$000(Landroid/content/ContentService$ObserverNode;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 615
    iget-object v0, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method private addObserverLocked(Landroid/net/Uri;ILandroid/database/IContentObserver;ZLjava/lang/Object;III)V
    .registers 21
    .parameter "uri"
    .parameter "index"
    .parameter "observer"
    .parameter "notifyForDescendants"
    .parameter "observersLock"
    .parameter "uid"
    .parameter "pid"
    .parameter "userHandle"

    #@0
    .prologue
    .line 731
    invoke-direct {p0, p1}, Landroid/content/ContentService$ObserverNode;->countUriSegments(Landroid/net/Uri;)I

    #@3
    move-result v1

    #@4
    if-ne p2, v1, :cond_1d

    #@6
    .line 732
    iget-object v8, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@8
    new-instance v0, Landroid/content/ContentService$ObserverNode$ObserverEntry;

    #@a
    move-object v1, p0

    #@b
    move-object v2, p3

    #@c
    move/from16 v3, p4

    #@e
    move-object/from16 v4, p5

    #@10
    move/from16 v5, p6

    #@12
    move/from16 v6, p7

    #@14
    move/from16 v7, p8

    #@16
    invoke-direct/range {v0 .. v7}, Landroid/content/ContentService$ObserverNode$ObserverEntry;-><init>(Landroid/content/ContentService$ObserverNode;Landroid/database/IContentObserver;ZLjava/lang/Object;III)V

    #@19
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 757
    :goto_1c
    return-void

    #@1d
    .line 738
    :cond_1d
    invoke-direct {p0, p1, p2}, Landroid/content/ContentService$ObserverNode;->getUriSegment(Landroid/net/Uri;I)Ljava/lang/String;

    #@20
    move-result-object v11

    #@21
    .line 739
    .local v11, segment:Ljava/lang/String;
    if-nez v11, :cond_42

    #@23
    .line 740
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@25
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v3, "Invalid Uri ("

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    const-string v3, ") used for observer"

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@41
    throw v1

    #@42
    .line 742
    :cond_42
    iget-object v1, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@44
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@47
    move-result v9

    #@48
    .line 743
    .local v9, N:I
    const/4 v10, 0x0

    #@49
    .local v10, i:I
    :goto_49
    if-ge v10, v9, :cond_70

    #@4b
    .line 744
    iget-object v1, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@4d
    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@50
    move-result-object v0

    #@51
    check-cast v0, Landroid/content/ContentService$ObserverNode;

    #@53
    .line 745
    .local v0, node:Landroid/content/ContentService$ObserverNode;
    iget-object v1, v0, Landroid/content/ContentService$ObserverNode;->mName:Ljava/lang/String;

    #@55
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@58
    move-result v1

    #@59
    if-eqz v1, :cond_6d

    #@5b
    .line 746
    add-int/lit8 v2, p2, 0x1

    #@5d
    move-object v1, p1

    #@5e
    move-object v3, p3

    #@5f
    move/from16 v4, p4

    #@61
    move-object/from16 v5, p5

    #@63
    move/from16 v6, p6

    #@65
    move/from16 v7, p7

    #@67
    move/from16 v8, p8

    #@69
    invoke-direct/range {v0 .. v8}, Landroid/content/ContentService$ObserverNode;->addObserverLocked(Landroid/net/Uri;ILandroid/database/IContentObserver;ZLjava/lang/Object;III)V

    #@6c
    goto :goto_1c

    #@6d
    .line 743
    :cond_6d
    add-int/lit8 v10, v10, 0x1

    #@6f
    goto :goto_49

    #@70
    .line 753
    .end local v0           #node:Landroid/content/ContentService$ObserverNode;
    :cond_70
    new-instance v0, Landroid/content/ContentService$ObserverNode;

    #@72
    invoke-direct {v0, v11}, Landroid/content/ContentService$ObserverNode;-><init>(Ljava/lang/String;)V

    #@75
    .line 754
    .restart local v0       #node:Landroid/content/ContentService$ObserverNode;
    iget-object v1, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@77
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7a
    .line 755
    add-int/lit8 v2, p2, 0x1

    #@7c
    move-object v1, p1

    #@7d
    move-object v3, p3

    #@7e
    move/from16 v4, p4

    #@80
    move-object/from16 v5, p5

    #@82
    move/from16 v6, p6

    #@84
    move/from16 v7, p7

    #@86
    move/from16 v8, p8

    #@88
    invoke-direct/range {v0 .. v8}, Landroid/content/ContentService$ObserverNode;->addObserverLocked(Landroid/net/Uri;ILandroid/database/IContentObserver;ZLjava/lang/Object;III)V

    #@8b
    goto :goto_1c
.end method

.method private collectMyObserversLocked(ZLandroid/database/IContentObserver;ZILjava/util/ArrayList;)V
    .registers 14
    .parameter "leaf"
    .parameter "observer"
    .parameter "observerWantsSelfNotifications"
    .parameter "targetUserHandle"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Landroid/database/IContentObserver;",
            "ZI",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentService$ObserverCall;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p5, calls:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentService$ObserverCall;>;"
    const/4 v7, -0x1

    #@1
    .line 791
    iget-object v5, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 792
    .local v0, N:I
    if-nez p2, :cond_25

    #@9
    const/4 v3, 0x0

    #@a
    .line 793
    .local v3, observerBinder:Landroid/os/IBinder;
    :goto_a
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    if-ge v2, v0, :cond_4d

    #@d
    .line 794
    iget-object v5, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/content/ContentService$ObserverNode$ObserverEntry;

    #@15
    .line 798
    .local v1, entry:Landroid/content/ContentService$ObserverNode$ObserverEntry;
    iget-object v5, v1, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observer:Landroid/database/IContentObserver;

    #@17
    invoke-interface {v5}, Landroid/database/IContentObserver;->asBinder()Landroid/os/IBinder;

    #@1a
    move-result-object v5

    #@1b
    if-ne v5, v3, :cond_2a

    #@1d
    const/4 v4, 0x1

    #@1e
    .line 799
    .local v4, selfChange:Z
    :goto_1e
    if-eqz v4, :cond_2c

    #@20
    if-nez p3, :cond_2c

    #@22
    .line 793
    :cond_22
    :goto_22
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_b

    #@25
    .line 792
    .end local v1           #entry:Landroid/content/ContentService$ObserverNode$ObserverEntry;
    .end local v2           #i:I
    .end local v3           #observerBinder:Landroid/os/IBinder;
    .end local v4           #selfChange:Z
    :cond_25
    invoke-interface {p2}, Landroid/database/IContentObserver;->asBinder()Landroid/os/IBinder;

    #@28
    move-result-object v3

    #@29
    goto :goto_a

    #@2a
    .line 798
    .restart local v1       #entry:Landroid/content/ContentService$ObserverNode$ObserverEntry;
    .restart local v2       #i:I
    .restart local v3       #observerBinder:Landroid/os/IBinder;
    :cond_2a
    const/4 v4, 0x0

    #@2b
    goto :goto_1e

    #@2c
    .line 804
    .restart local v4       #selfChange:Z
    :cond_2c
    if-eq p4, v7, :cond_3a

    #@2e
    invoke-static {v1}, Landroid/content/ContentService$ObserverNode$ObserverEntry;->access$100(Landroid/content/ContentService$ObserverNode$ObserverEntry;)I

    #@31
    move-result v5

    #@32
    if-eq v5, v7, :cond_3a

    #@34
    invoke-static {v1}, Landroid/content/ContentService$ObserverNode$ObserverEntry;->access$100(Landroid/content/ContentService$ObserverNode$ObserverEntry;)I

    #@37
    move-result v5

    #@38
    if-ne p4, v5, :cond_22

    #@3a
    .line 808
    :cond_3a
    if-nez p1, :cond_42

    #@3c
    if-nez p1, :cond_22

    #@3e
    iget-boolean v5, v1, Landroid/content/ContentService$ObserverNode$ObserverEntry;->notifyForDescendants:Z

    #@40
    if-eqz v5, :cond_22

    #@42
    .line 809
    :cond_42
    new-instance v5, Landroid/content/ContentService$ObserverCall;

    #@44
    iget-object v6, v1, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observer:Landroid/database/IContentObserver;

    #@46
    invoke-direct {v5, p0, v6, v4}, Landroid/content/ContentService$ObserverCall;-><init>(Landroid/content/ContentService$ObserverNode;Landroid/database/IContentObserver;Z)V

    #@49
    invoke-virtual {p5, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4c
    goto :goto_22

    #@4d
    .line 813
    .end local v1           #entry:Landroid/content/ContentService$ObserverNode$ObserverEntry;
    .end local v4           #selfChange:Z
    :cond_4d
    return-void
.end method

.method private countUriSegments(Landroid/net/Uri;)I
    .registers 3
    .parameter "uri"

    #@0
    .prologue
    .line 713
    if-nez p1, :cond_4

    #@2
    .line 714
    const/4 v0, 0x0

    #@3
    .line 716
    :goto_3
    return v0

    #@4
    :cond_4
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@7
    move-result-object v0

    #@8
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@b
    move-result v0

    #@c
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_3
.end method

.method private getUriSegment(Landroid/net/Uri;I)Ljava/lang/String;
    .registers 5
    .parameter "uri"
    .parameter "index"

    #@0
    .prologue
    .line 701
    if-eqz p1, :cond_16

    #@2
    .line 702
    if-nez p2, :cond_9

    #@4
    .line 703
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 708
    :goto_8
    return-object v0

    #@9
    .line 705
    :cond_9
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@c
    move-result-object v0

    #@d
    add-int/lit8 v1, p2, -0x1

    #@f
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Ljava/lang/String;

    #@15
    goto :goto_8

    #@16
    .line 708
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_8
.end method


# virtual methods
.method public addObserverLocked(Landroid/net/Uri;Landroid/database/IContentObserver;ZLjava/lang/Object;III)V
    .registers 17
    .parameter "uri"
    .parameter "observer"
    .parameter "notifyForDescendants"
    .parameter "observersLock"
    .parameter "uid"
    .parameter "pid"
    .parameter "userHandle"

    #@0
    .prologue
    .line 723
    const/4 v2, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v3, p2

    #@4
    move v4, p3

    #@5
    move-object v5, p4

    #@6
    move v6, p5

    #@7
    move v7, p6

    #@8
    move/from16 v8, p7

    #@a
    invoke-direct/range {v0 .. v8}, Landroid/content/ContentService$ObserverNode;->addObserverLocked(Landroid/net/Uri;ILandroid/database/IContentObserver;ZLjava/lang/Object;III)V

    #@d
    .line 725
    return-void
.end method

.method public collectObserversLocked(Landroid/net/Uri;ILandroid/database/IContentObserver;ZILjava/util/ArrayList;)V
    .registers 18
    .parameter "uri"
    .parameter "index"
    .parameter "observer"
    .parameter "observerWantsSelfNotifications"
    .parameter "targetUserHandle"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "I",
            "Landroid/database/IContentObserver;",
            "ZI",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentService$ObserverCall;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 821
    .local p6, calls:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentService$ObserverCall;>;"
    const/4 v9, 0x0

    #@1
    .line 822
    .local v9, segment:Ljava/lang/String;
    invoke-direct {p0, p1}, Landroid/content/ContentService$ObserverNode;->countUriSegments(Landroid/net/Uri;)I

    #@4
    move-result v10

    #@5
    .line 823
    .local v10, segmentCount:I
    if-lt p2, v10, :cond_3c

    #@7
    .line 825
    const/4 v1, 0x1

    #@8
    move-object v0, p0

    #@9
    move-object v2, p3

    #@a
    move v3, p4

    #@b
    move/from16 v4, p5

    #@d
    move-object/from16 v5, p6

    #@f
    invoke-direct/range {v0 .. v5}, Landroid/content/ContentService$ObserverNode;->collectMyObserversLocked(ZLandroid/database/IContentObserver;ZILjava/util/ArrayList;)V

    #@12
    .line 834
    :cond_12
    :goto_12
    iget-object v1, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v7

    #@18
    .line 835
    .local v7, N:I
    const/4 v8, 0x0

    #@19
    .local v8, i:I
    :goto_19
    if-ge v8, v7, :cond_3b

    #@1b
    .line 836
    iget-object v1, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/content/ContentService$ObserverNode;

    #@23
    .line 837
    .local v0, node:Landroid/content/ContentService$ObserverNode;
    if-eqz v9, :cond_2d

    #@25
    iget-object v1, v0, Landroid/content/ContentService$ObserverNode;->mName:Ljava/lang/String;

    #@27
    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_4e

    #@2d
    .line 839
    :cond_2d
    add-int/lit8 v2, p2, 0x1

    #@2f
    move-object v1, p1

    #@30
    move-object v3, p3

    #@31
    move v4, p4

    #@32
    move/from16 v5, p5

    #@34
    move-object/from16 v6, p6

    #@36
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentService$ObserverNode;->collectObserversLocked(Landroid/net/Uri;ILandroid/database/IContentObserver;ZILjava/util/ArrayList;)V

    #@39
    .line 841
    if-eqz v9, :cond_4e

    #@3b
    .line 846
    .end local v0           #node:Landroid/content/ContentService$ObserverNode;
    :cond_3b
    return-void

    #@3c
    .line 827
    .end local v7           #N:I
    .end local v8           #i:I
    :cond_3c
    if-ge p2, v10, :cond_12

    #@3e
    .line 828
    invoke-direct {p0, p1, p2}, Landroid/content/ContentService$ObserverNode;->getUriSegment(Landroid/net/Uri;I)Ljava/lang/String;

    #@41
    move-result-object v9

    #@42
    .line 830
    const/4 v1, 0x0

    #@43
    move-object v0, p0

    #@44
    move-object v2, p3

    #@45
    move v3, p4

    #@46
    move/from16 v4, p5

    #@48
    move-object/from16 v5, p6

    #@4a
    invoke-direct/range {v0 .. v5}, Landroid/content/ContentService$ObserverNode;->collectMyObserversLocked(ZLandroid/database/IContentObserver;ZILjava/util/ArrayList;)V

    #@4d
    goto :goto_12

    #@4e
    .line 835
    .restart local v0       #node:Landroid/content/ContentService$ObserverNode;
    .restart local v7       #N:I
    .restart local v8       #i:I
    :cond_4e
    add-int/lit8 v8, v8, 0x1

    #@50
    goto :goto_19
.end method

.method public dumpLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[ILandroid/util/SparseIntArray;)V
    .registers 17
    .parameter "fd"
    .parameter "pw"
    .parameter "args"
    .parameter "name"
    .parameter "prefix"
    .parameter "counts"
    .parameter "pidCounts"

    #@0
    .prologue
    .line 671
    const/4 v4, 0x0

    #@1
    .line 672
    .local v4, innerName:Ljava/lang/String;
    iget-object v0, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    if-lez v0, :cond_51

    #@9
    .line 673
    const-string v0, ""

    #@b
    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_37

    #@11
    .line 674
    iget-object v4, p0, Landroid/content/ContentService$ObserverNode;->mName:Ljava/lang/String;

    #@13
    .line 678
    :goto_13
    const/4 v8, 0x0

    #@14
    .local v8, i:I
    :goto_14
    iget-object v0, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v0

    #@1a
    if-ge v8, v0, :cond_51

    #@1c
    .line 679
    const/4 v0, 0x1

    #@1d
    aget v1, p6, v0

    #@1f
    add-int/lit8 v1, v1, 0x1

    #@21
    aput v1, p6, v0

    #@23
    .line 680
    iget-object v0, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v0

    #@29
    check-cast v0, Landroid/content/ContentService$ObserverNode$ObserverEntry;

    #@2b
    move-object v1, p1

    #@2c
    move-object v2, p2

    #@2d
    move-object v3, p3

    #@2e
    move-object v5, p5

    #@2f
    move-object/from16 v6, p7

    #@31
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentService$ObserverNode$ObserverEntry;->dumpLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/SparseIntArray;)V

    #@34
    .line 678
    add-int/lit8 v8, v8, 0x1

    #@36
    goto :goto_14

    #@37
    .line 676
    .end local v8           #i:I
    :cond_37
    new-instance v0, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    const-string v1, "/"

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    iget-object v1, p0, Landroid/content/ContentService$ObserverNode;->mName:Ljava/lang/String;

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v0

    #@4c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    goto :goto_13

    #@51
    .line 684
    :cond_51
    iget-object v0, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@53
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@56
    move-result v0

    #@57
    if-lez v0, :cond_a4

    #@59
    .line 685
    if-nez v4, :cond_65

    #@5b
    .line 686
    const-string v0, ""

    #@5d
    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@60
    move-result v0

    #@61
    if-eqz v0, :cond_8a

    #@63
    .line 687
    iget-object v4, p0, Landroid/content/ContentService$ObserverNode;->mName:Ljava/lang/String;

    #@65
    .line 692
    :cond_65
    :goto_65
    const/4 v8, 0x0

    #@66
    .restart local v8       #i:I
    :goto_66
    iget-object v0, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@68
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@6b
    move-result v0

    #@6c
    if-ge v8, v0, :cond_a4

    #@6e
    .line 693
    const/4 v0, 0x0

    #@6f
    aget v1, p6, v0

    #@71
    add-int/lit8 v1, v1, 0x1

    #@73
    aput v1, p6, v0

    #@75
    .line 694
    iget-object v0, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@77
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7a
    move-result-object v0

    #@7b
    check-cast v0, Landroid/content/ContentService$ObserverNode;

    #@7d
    move-object v1, p1

    #@7e
    move-object v2, p2

    #@7f
    move-object v3, p3

    #@80
    move-object v5, p5

    #@81
    move-object v6, p6

    #@82
    move-object/from16 v7, p7

    #@84
    invoke-virtual/range {v0 .. v7}, Landroid/content/ContentService$ObserverNode;->dumpLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[ILandroid/util/SparseIntArray;)V

    #@87
    .line 692
    add-int/lit8 v8, v8, 0x1

    #@89
    goto :goto_66

    #@8a
    .line 689
    .end local v8           #i:I
    :cond_8a
    new-instance v0, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v0

    #@93
    const-string v1, "/"

    #@95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v0

    #@99
    iget-object v1, p0, Landroid/content/ContentService$ObserverNode;->mName:Ljava/lang/String;

    #@9b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v0

    #@9f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v4

    #@a3
    goto :goto_65

    #@a4
    .line 698
    :cond_a4
    return-void
.end method

.method public removeObserverLocked(Landroid/database/IContentObserver;)Z
    .registers 9
    .parameter "observer"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 760
    iget-object v5, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v4

    #@7
    .line 761
    .local v4, size:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v4, :cond_24

    #@a
    .line 762
    iget-object v5, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v5

    #@10
    check-cast v5, Landroid/content/ContentService$ObserverNode;

    #@12
    invoke-virtual {v5, p1}, Landroid/content/ContentService$ObserverNode;->removeObserverLocked(Landroid/database/IContentObserver;)Z

    #@15
    move-result v0

    #@16
    .line 763
    .local v0, empty:Z
    if-eqz v0, :cond_21

    #@18
    .line 764
    iget-object v5, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1d
    .line 765
    add-int/lit8 v2, v2, -0x1

    #@1f
    .line 766
    add-int/lit8 v4, v4, -0x1

    #@21
    .line 761
    :cond_21
    add-int/lit8 v2, v2, 0x1

    #@23
    goto :goto_8

    #@24
    .line 770
    .end local v0           #empty:Z
    :cond_24
    invoke-interface {p1}, Landroid/database/IContentObserver;->asBinder()Landroid/os/IBinder;

    #@27
    move-result-object v3

    #@28
    .line 771
    .local v3, observerBinder:Landroid/os/IBinder;
    iget-object v5, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@2d
    move-result v4

    #@2e
    .line 772
    const/4 v2, 0x0

    #@2f
    :goto_2f
    if-ge v2, v4, :cond_49

    #@31
    .line 773
    iget-object v5, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@36
    move-result-object v1

    #@37
    check-cast v1, Landroid/content/ContentService$ObserverNode$ObserverEntry;

    #@39
    .line 774
    .local v1, entry:Landroid/content/ContentService$ObserverNode$ObserverEntry;
    iget-object v5, v1, Landroid/content/ContentService$ObserverNode$ObserverEntry;->observer:Landroid/database/IContentObserver;

    #@3b
    invoke-interface {v5}, Landroid/database/IContentObserver;->asBinder()Landroid/os/IBinder;

    #@3e
    move-result-object v5

    #@3f
    if-ne v5, v3, :cond_5b

    #@41
    .line 775
    iget-object v5, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@43
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@46
    .line 777
    invoke-interface {v3, v1, v6}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@49
    .line 782
    .end local v1           #entry:Landroid/content/ContentService$ObserverNode$ObserverEntry;
    :cond_49
    iget-object v5, p0, Landroid/content/ContentService$ObserverNode;->mChildren:Ljava/util/ArrayList;

    #@4b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@4e
    move-result v5

    #@4f
    if-nez v5, :cond_5e

    #@51
    iget-object v5, p0, Landroid/content/ContentService$ObserverNode;->mObservers:Ljava/util/ArrayList;

    #@53
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@56
    move-result v5

    #@57
    if-nez v5, :cond_5e

    #@59
    .line 783
    const/4 v5, 0x1

    #@5a
    .line 785
    :goto_5a
    return v5

    #@5b
    .line 772
    .restart local v1       #entry:Landroid/content/ContentService$ObserverNode$ObserverEntry;
    :cond_5b
    add-int/lit8 v2, v2, 0x1

    #@5d
    goto :goto_2f

    #@5e
    .end local v1           #entry:Landroid/content/ContentService$ObserverNode$ObserverEntry;
    :cond_5e
    move v5, v6

    #@5f
    .line 785
    goto :goto_5a
.end method
