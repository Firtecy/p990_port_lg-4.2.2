.class public abstract Landroid/content/ContentProviderNative;
.super Landroid/os/Binder;
.source "ContentProviderNative.java"

# interfaces
.implements Landroid/content/IContentProvider;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 47
    const-string v0, "android.content.IContentProvider"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/content/ContentProviderNative;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 48
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/content/IContentProvider;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 56
    if-nez p0, :cond_4

    #@2
    .line 57
    const/4 v0, 0x0

    #@3
    .line 65
    :cond_3
    :goto_3
    return-object v0

    #@4
    .line 59
    :cond_4
    const-string v1, "android.content.IContentProvider"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/content/IContentProvider;

    #@c
    .line 61
    .local v0, in:Landroid/content/IContentProvider;
    if-nez v0, :cond_3

    #@e
    .line 65
    new-instance v0, Landroid/content/ContentProviderProxy;

    #@10
    .end local v0           #in:Landroid/content/IContentProvider;
    invoke-direct {v0, p0}, Landroid/content/ContentProviderProxy;-><init>(Landroid/os/IBinder;)V

    #@13
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 315
    return-object p0
.end method

.method public abstract getProviderName()Ljava/lang/String;
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 39
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 79
    packed-switch p1, :pswitch_data_2ee

    #@3
    .line 310
    :pswitch_3
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v3

    #@7
    :goto_7
    return v3

    #@8
    .line 82
    :pswitch_8
    :try_start_8
    const-string v3, "android.content.IContentProvider"

    #@a
    move-object/from16 v0, p2

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f
    .line 84
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@11
    move-object/from16 v0, p2

    #@13
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@16
    move-result-object v4

    #@17
    check-cast v4, Landroid/net/Uri;

    #@19
    .line 87
    .local v4, url:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v22

    #@1d
    .line 88
    .local v22, num:I
    const/4 v5, 0x0

    #@1e
    .line 89
    .local v5, projection:[Ljava/lang/String;
    if-lez v22, :cond_35

    #@20
    .line 90
    move/from16 v0, v22

    #@22
    new-array v5, v0, [Ljava/lang/String;

    #@24
    .line 91
    const/16 v17, 0x0

    #@26
    .local v17, i:I
    :goto_26
    move/from16 v0, v17

    #@28
    move/from16 v1, v22

    #@2a
    if-ge v0, v1, :cond_35

    #@2c
    .line 92
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    aput-object v3, v5, v17

    #@32
    .line 91
    add-int/lit8 v17, v17, 0x1

    #@34
    goto :goto_26

    #@35
    .line 97
    .end local v17           #i:I
    :cond_35
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    .line 98
    .local v6, selection:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v22

    #@3d
    .line 99
    const/4 v7, 0x0

    #@3e
    .line 100
    .local v7, selectionArgs:[Ljava/lang/String;
    if-lez v22, :cond_55

    #@40
    .line 101
    move/from16 v0, v22

    #@42
    new-array v7, v0, [Ljava/lang/String;

    #@44
    .line 102
    const/16 v17, 0x0

    #@46
    .restart local v17       #i:I
    :goto_46
    move/from16 v0, v17

    #@48
    move/from16 v1, v22

    #@4a
    if-ge v0, v1, :cond_55

    #@4c
    .line 103
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    aput-object v3, v7, v17

    #@52
    .line 102
    add-int/lit8 v17, v17, 0x1

    #@54
    goto :goto_46

    #@55
    .line 107
    .end local v17           #i:I
    :cond_55
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@58
    move-result-object v8

    #@59
    .line 108
    .local v8, sortOrder:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5c
    move-result-object v3

    #@5d
    invoke-static {v3}, Landroid/database/IContentObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/database/IContentObserver;

    #@60
    move-result-object v24

    #@61
    .line 110
    .local v24, observer:Landroid/database/IContentObserver;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@64
    move-result-object v3

    #@65
    invoke-static {v3}, Landroid/os/ICancellationSignal$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/ICancellationSignal;

    #@68
    move-result-object v9

    #@69
    .local v9, cancellationSignal:Landroid/os/ICancellationSignal;
    move-object/from16 v3, p0

    #@6b
    .line 113
    invoke-virtual/range {v3 .. v9}, Landroid/content/ContentProviderNative;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@6e
    move-result-object v13

    #@6f
    .line 115
    .local v13, cursor:Landroid/database/Cursor;
    if-eqz v13, :cond_92

    #@71
    .line 116
    new-instance v10, Landroid/database/CursorToBulkCursorAdaptor;

    #@73
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContentProviderNative;->getProviderName()Ljava/lang/String;

    #@76
    move-result-object v3

    #@77
    move-object/from16 v0, v24

    #@79
    invoke-direct {v10, v13, v0, v3}, Landroid/database/CursorToBulkCursorAdaptor;-><init>(Landroid/database/Cursor;Landroid/database/IContentObserver;Ljava/lang/String;)V

    #@7c
    .line 118
    .local v10, adaptor:Landroid/database/CursorToBulkCursorAdaptor;
    invoke-virtual {v10}, Landroid/database/CursorToBulkCursorAdaptor;->getBulkCursorDescriptor()Landroid/database/BulkCursorDescriptor;

    #@7f
    move-result-object v14

    #@80
    .line 120
    .local v14, d:Landroid/database/BulkCursorDescriptor;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@83
    .line 121
    const/4 v3, 0x1

    #@84
    move-object/from16 v0, p3

    #@86
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@89
    .line 122
    const/4 v3, 0x1

    #@8a
    move-object/from16 v0, p3

    #@8c
    invoke-virtual {v14, v0, v3}, Landroid/database/BulkCursorDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@8f
    .line 128
    .end local v10           #adaptor:Landroid/database/CursorToBulkCursorAdaptor;
    .end local v14           #d:Landroid/database/BulkCursorDescriptor;
    :goto_8f
    const/4 v3, 0x1

    #@90
    goto/16 :goto_7

    #@92
    .line 124
    :cond_92
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@95
    .line 125
    const/4 v3, 0x0

    #@96
    move-object/from16 v0, p3

    #@98
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_9b
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_9b} :catch_9c

    #@9b
    goto :goto_8f

    #@9c
    .line 305
    .end local v4           #url:Landroid/net/Uri;
    .end local v5           #projection:[Ljava/lang/String;
    .end local v6           #selection:Ljava/lang/String;
    .end local v7           #selectionArgs:[Ljava/lang/String;
    .end local v8           #sortOrder:Ljava/lang/String;
    .end local v9           #cancellationSignal:Landroid/os/ICancellationSignal;
    .end local v13           #cursor:Landroid/database/Cursor;
    .end local v22           #num:I
    .end local v24           #observer:Landroid/database/IContentObserver;
    :catch_9c
    move-exception v15

    #@9d
    .line 306
    .local v15, e:Ljava/lang/Exception;
    move-object/from16 v0, p3

    #@9f
    invoke-static {v0, v15}, Landroid/database/DatabaseUtils;->writeExceptionToParcel(Landroid/os/Parcel;Ljava/lang/Exception;)V

    #@a2
    .line 307
    const/4 v3, 0x1

    #@a3
    goto/16 :goto_7

    #@a5
    .line 133
    .end local v15           #e:Ljava/lang/Exception;
    :pswitch_a5
    :try_start_a5
    const-string v3, "android.content.IContentProvider"

    #@a7
    move-object/from16 v0, p2

    #@a9
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ac
    .line 134
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ae
    move-object/from16 v0, p2

    #@b0
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b3
    move-result-object v4

    #@b4
    check-cast v4, Landroid/net/Uri;

    #@b6
    .line 135
    .restart local v4       #url:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@b8
    invoke-virtual {v0, v4}, Landroid/content/ContentProviderNative;->getType(Landroid/net/Uri;)Ljava/lang/String;

    #@bb
    move-result-object v31

    #@bc
    .line 136
    .local v31, type:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@bf
    .line 137
    move-object/from16 v0, p3

    #@c1
    move-object/from16 v1, v31

    #@c3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c6
    .line 139
    const/4 v3, 0x1

    #@c7
    goto/16 :goto_7

    #@c9
    .line 144
    .end local v4           #url:Landroid/net/Uri;
    .end local v31           #type:Ljava/lang/String;
    :pswitch_c9
    const-string v3, "android.content.IContentProvider"

    #@cb
    move-object/from16 v0, p2

    #@cd
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d0
    .line 145
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@d2
    move-object/from16 v0, p2

    #@d4
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@d7
    move-result-object v4

    #@d8
    check-cast v4, Landroid/net/Uri;

    #@da
    .line 146
    .restart local v4       #url:Landroid/net/Uri;
    sget-object v3, Landroid/content/ContentValues;->CREATOR:Landroid/os/Parcelable$Creator;

    #@dc
    move-object/from16 v0, p2

    #@de
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e1
    move-result-object v33

    #@e2
    check-cast v33, Landroid/content/ContentValues;

    #@e4
    .line 148
    .local v33, values:Landroid/content/ContentValues;
    move-object/from16 v0, p0

    #@e6
    move-object/from16 v1, v33

    #@e8
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentProviderNative;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@eb
    move-result-object v27

    #@ec
    .line 149
    .local v27, out:Landroid/net/Uri;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ef
    .line 150
    move-object/from16 v0, p3

    #@f1
    move-object/from16 v1, v27

    #@f3
    invoke-static {v0, v1}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;Landroid/net/Uri;)V

    #@f6
    .line 151
    const/4 v3, 0x1

    #@f7
    goto/16 :goto_7

    #@f9
    .line 156
    .end local v4           #url:Landroid/net/Uri;
    .end local v27           #out:Landroid/net/Uri;
    .end local v33           #values:Landroid/content/ContentValues;
    :pswitch_f9
    const-string v3, "android.content.IContentProvider"

    #@fb
    move-object/from16 v0, p2

    #@fd
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@100
    .line 157
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@102
    move-object/from16 v0, p2

    #@104
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@107
    move-result-object v4

    #@108
    check-cast v4, Landroid/net/Uri;

    #@10a
    .line 158
    .restart local v4       #url:Landroid/net/Uri;
    sget-object v3, Landroid/content/ContentValues;->CREATOR:Landroid/os/Parcelable$Creator;

    #@10c
    move-object/from16 v0, p2

    #@10e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@111
    move-result-object v33

    #@112
    check-cast v33, [Landroid/content/ContentValues;

    #@114
    .line 160
    .local v33, values:[Landroid/content/ContentValues;
    move-object/from16 v0, p0

    #@116
    move-object/from16 v1, v33

    #@118
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentProviderNative;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    #@11b
    move-result v12

    #@11c
    .line 161
    .local v12, count:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@11f
    .line 162
    move-object/from16 v0, p3

    #@121
    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@124
    .line 163
    const/4 v3, 0x1

    #@125
    goto/16 :goto_7

    #@127
    .line 168
    .end local v4           #url:Landroid/net/Uri;
    .end local v12           #count:I
    .end local v33           #values:[Landroid/content/ContentValues;
    :pswitch_127
    const-string v3, "android.content.IContentProvider"

    #@129
    move-object/from16 v0, p2

    #@12b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12e
    .line 169
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@131
    move-result v23

    #@132
    .line 170
    .local v23, numOperations:I
    new-instance v25, Ljava/util/ArrayList;

    #@134
    move-object/from16 v0, v25

    #@136
    move/from16 v1, v23

    #@138
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@13b
    .line 172
    .local v25, operations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/16 v17, 0x0

    #@13d
    .restart local v17       #i:I
    :goto_13d
    move/from16 v0, v17

    #@13f
    move/from16 v1, v23

    #@141
    if-ge v0, v1, :cond_155

    #@143
    .line 173
    sget-object v3, Landroid/content/ContentProviderOperation;->CREATOR:Landroid/os/Parcelable$Creator;

    #@145
    move-object/from16 v0, p2

    #@147
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@14a
    move-result-object v3

    #@14b
    move-object/from16 v0, v25

    #@14d
    move/from16 v1, v17

    #@14f
    invoke-virtual {v0, v1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@152
    .line 172
    add-int/lit8 v17, v17, 0x1

    #@154
    goto :goto_13d

    #@155
    .line 175
    :cond_155
    move-object/from16 v0, p0

    #@157
    move-object/from16 v1, v25

    #@159
    invoke-virtual {v0, v1}, Landroid/content/ContentProviderNative;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    #@15c
    move-result-object v29

    #@15d
    .line 176
    .local v29, results:[Landroid/content/ContentProviderResult;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@160
    .line 177
    const/4 v3, 0x0

    #@161
    move-object/from16 v0, p3

    #@163
    move-object/from16 v1, v29

    #@165
    invoke-virtual {v0, v1, v3}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@168
    .line 178
    const/4 v3, 0x1

    #@169
    goto/16 :goto_7

    #@16b
    .line 183
    .end local v17           #i:I
    .end local v23           #numOperations:I
    .end local v25           #operations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v29           #results:[Landroid/content/ContentProviderResult;
    :pswitch_16b
    const-string v3, "android.content.IContentProvider"

    #@16d
    move-object/from16 v0, p2

    #@16f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@172
    .line 184
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@174
    move-object/from16 v0, p2

    #@176
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@179
    move-result-object v4

    #@17a
    check-cast v4, Landroid/net/Uri;

    #@17c
    .line 185
    .restart local v4       #url:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17f
    move-result-object v6

    #@180
    .line 186
    .restart local v6       #selection:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@183
    move-result-object v7

    #@184
    .line 188
    .restart local v7       #selectionArgs:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@186
    invoke-virtual {v0, v4, v6, v7}, Landroid/content/ContentProviderNative;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@189
    move-result v12

    #@18a
    .line 190
    .restart local v12       #count:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@18d
    .line 191
    move-object/from16 v0, p3

    #@18f
    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@192
    .line 192
    const/4 v3, 0x1

    #@193
    goto/16 :goto_7

    #@195
    .line 197
    .end local v4           #url:Landroid/net/Uri;
    .end local v6           #selection:Ljava/lang/String;
    .end local v7           #selectionArgs:[Ljava/lang/String;
    .end local v12           #count:I
    :pswitch_195
    const-string v3, "android.content.IContentProvider"

    #@197
    move-object/from16 v0, p2

    #@199
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19c
    .line 198
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@19e
    move-object/from16 v0, p2

    #@1a0
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1a3
    move-result-object v4

    #@1a4
    check-cast v4, Landroid/net/Uri;

    #@1a6
    .line 199
    .restart local v4       #url:Landroid/net/Uri;
    sget-object v3, Landroid/content/ContentValues;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a8
    move-object/from16 v0, p2

    #@1aa
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1ad
    move-result-object v33

    #@1ae
    check-cast v33, Landroid/content/ContentValues;

    #@1b0
    .line 200
    .local v33, values:Landroid/content/ContentValues;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b3
    move-result-object v6

    #@1b4
    .line 201
    .restart local v6       #selection:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@1b7
    move-result-object v7

    #@1b8
    .line 203
    .restart local v7       #selectionArgs:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@1ba
    move-object/from16 v1, v33

    #@1bc
    invoke-virtual {v0, v4, v1, v6, v7}, Landroid/content/ContentProviderNative;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@1bf
    move-result v12

    #@1c0
    .line 205
    .restart local v12       #count:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c3
    .line 206
    move-object/from16 v0, p3

    #@1c5
    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@1c8
    .line 207
    const/4 v3, 0x1

    #@1c9
    goto/16 :goto_7

    #@1cb
    .line 212
    .end local v4           #url:Landroid/net/Uri;
    .end local v6           #selection:Ljava/lang/String;
    .end local v7           #selectionArgs:[Ljava/lang/String;
    .end local v12           #count:I
    .end local v33           #values:Landroid/content/ContentValues;
    :pswitch_1cb
    const-string v3, "android.content.IContentProvider"

    #@1cd
    move-object/from16 v0, p2

    #@1cf
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d2
    .line 213
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d4
    move-object/from16 v0, p2

    #@1d6
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1d9
    move-result-object v4

    #@1da
    check-cast v4, Landroid/net/Uri;

    #@1dc
    .line 214
    .restart local v4       #url:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1df
    move-result-object v21

    #@1e0
    .line 217
    .local v21, mode:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1e2
    move-object/from16 v1, v21

    #@1e4
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentProviderNative;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@1e7
    move-result-object v16

    #@1e8
    .line 218
    .local v16, fd:Landroid/os/ParcelFileDescriptor;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1eb
    .line 219
    if-eqz v16, :cond_1fe

    #@1ed
    .line 220
    const/4 v3, 0x1

    #@1ee
    move-object/from16 v0, p3

    #@1f0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f3
    .line 221
    const/4 v3, 0x1

    #@1f4
    move-object/from16 v0, v16

    #@1f6
    move-object/from16 v1, p3

    #@1f8
    invoke-virtual {v0, v1, v3}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@1fb
    .line 226
    :goto_1fb
    const/4 v3, 0x1

    #@1fc
    goto/16 :goto_7

    #@1fe
    .line 224
    :cond_1fe
    const/4 v3, 0x0

    #@1ff
    move-object/from16 v0, p3

    #@201
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@204
    goto :goto_1fb

    #@205
    .line 231
    .end local v4           #url:Landroid/net/Uri;
    .end local v16           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v21           #mode:Ljava/lang/String;
    :pswitch_205
    const-string v3, "android.content.IContentProvider"

    #@207
    move-object/from16 v0, p2

    #@209
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20c
    .line 232
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20e
    move-object/from16 v0, p2

    #@210
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@213
    move-result-object v4

    #@214
    check-cast v4, Landroid/net/Uri;

    #@216
    .line 233
    .restart local v4       #url:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@219
    move-result-object v21

    #@21a
    .line 236
    .restart local v21       #mode:Ljava/lang/String;
    move-object/from16 v0, p0

    #@21c
    move-object/from16 v1, v21

    #@21e
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentProviderNative;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@221
    move-result-object v16

    #@222
    .line 237
    .local v16, fd:Landroid/content/res/AssetFileDescriptor;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@225
    .line 238
    if-eqz v16, :cond_238

    #@227
    .line 239
    const/4 v3, 0x1

    #@228
    move-object/from16 v0, p3

    #@22a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@22d
    .line 240
    const/4 v3, 0x1

    #@22e
    move-object/from16 v0, v16

    #@230
    move-object/from16 v1, p3

    #@232
    invoke-virtual {v0, v1, v3}, Landroid/content/res/AssetFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@235
    .line 245
    :goto_235
    const/4 v3, 0x1

    #@236
    goto/16 :goto_7

    #@238
    .line 243
    :cond_238
    const/4 v3, 0x0

    #@239
    move-object/from16 v0, p3

    #@23b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@23e
    goto :goto_235

    #@23f
    .line 250
    .end local v4           #url:Landroid/net/Uri;
    .end local v16           #fd:Landroid/content/res/AssetFileDescriptor;
    .end local v21           #mode:Ljava/lang/String;
    :pswitch_23f
    const-string v3, "android.content.IContentProvider"

    #@241
    move-object/from16 v0, p2

    #@243
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@246
    .line 252
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@249
    move-result-object v18

    #@24a
    .line 253
    .local v18, method:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24d
    move-result-object v30

    #@24e
    .line 254
    .local v30, stringArg:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@251
    move-result-object v11

    #@252
    .line 256
    .local v11, args:Landroid/os/Bundle;
    move-object/from16 v0, p0

    #@254
    move-object/from16 v1, v18

    #@256
    move-object/from16 v2, v30

    #@258
    invoke-virtual {v0, v1, v2, v11}, Landroid/content/ContentProviderNative;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    #@25b
    move-result-object v28

    #@25c
    .line 258
    .local v28, responseBundle:Landroid/os/Bundle;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@25f
    .line 259
    move-object/from16 v0, p3

    #@261
    move-object/from16 v1, v28

    #@263
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@266
    .line 260
    const/4 v3, 0x1

    #@267
    goto/16 :goto_7

    #@269
    .line 265
    .end local v11           #args:Landroid/os/Bundle;
    .end local v18           #method:Ljava/lang/String;
    .end local v28           #responseBundle:Landroid/os/Bundle;
    .end local v30           #stringArg:Ljava/lang/String;
    :pswitch_269
    const-string v3, "android.content.IContentProvider"

    #@26b
    move-object/from16 v0, p2

    #@26d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@270
    .line 266
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@272
    move-object/from16 v0, p2

    #@274
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@277
    move-result-object v4

    #@278
    check-cast v4, Landroid/net/Uri;

    #@27a
    .line 267
    .restart local v4       #url:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@27d
    move-result-object v20

    #@27e
    .line 268
    .local v20, mimeTypeFilter:Ljava/lang/String;
    move-object/from16 v0, p0

    #@280
    move-object/from16 v1, v20

    #@282
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentProviderNative;->getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;

    #@285
    move-result-object v32

    #@286
    .line 269
    .local v32, types:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@289
    .line 270
    move-object/from16 v0, p3

    #@28b
    move-object/from16 v1, v32

    #@28d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@290
    .line 272
    const/4 v3, 0x1

    #@291
    goto/16 :goto_7

    #@293
    .line 277
    .end local v4           #url:Landroid/net/Uri;
    .end local v20           #mimeTypeFilter:Ljava/lang/String;
    .end local v32           #types:[Ljava/lang/String;
    :pswitch_293
    const-string v3, "android.content.IContentProvider"

    #@295
    move-object/from16 v0, p2

    #@297
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29a
    .line 278
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@29c
    move-object/from16 v0, p2

    #@29e
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2a1
    move-result-object v4

    #@2a2
    check-cast v4, Landroid/net/Uri;

    #@2a4
    .line 279
    .restart local v4       #url:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2a7
    move-result-object v19

    #@2a8
    .line 280
    .local v19, mimeType:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@2ab
    move-result-object v26

    #@2ac
    .line 283
    .local v26, opts:Landroid/os/Bundle;
    move-object/from16 v0, p0

    #@2ae
    move-object/from16 v1, v19

    #@2b0
    move-object/from16 v2, v26

    #@2b2
    invoke-virtual {v0, v4, v1, v2}, Landroid/content/ContentProviderNative;->openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;

    #@2b5
    move-result-object v16

    #@2b6
    .line 284
    .restart local v16       #fd:Landroid/content/res/AssetFileDescriptor;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b9
    .line 285
    if-eqz v16, :cond_2cc

    #@2bb
    .line 286
    const/4 v3, 0x1

    #@2bc
    move-object/from16 v0, p3

    #@2be
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2c1
    .line 287
    const/4 v3, 0x1

    #@2c2
    move-object/from16 v0, v16

    #@2c4
    move-object/from16 v1, p3

    #@2c6
    invoke-virtual {v0, v1, v3}, Landroid/content/res/AssetFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@2c9
    .line 292
    :goto_2c9
    const/4 v3, 0x1

    #@2ca
    goto/16 :goto_7

    #@2cc
    .line 290
    :cond_2cc
    const/4 v3, 0x0

    #@2cd
    move-object/from16 v0, p3

    #@2cf
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2d2
    goto :goto_2c9

    #@2d3
    .line 297
    .end local v4           #url:Landroid/net/Uri;
    .end local v16           #fd:Landroid/content/res/AssetFileDescriptor;
    .end local v19           #mimeType:Ljava/lang/String;
    .end local v26           #opts:Landroid/os/Bundle;
    :pswitch_2d3
    const-string v3, "android.content.IContentProvider"

    #@2d5
    move-object/from16 v0, p2

    #@2d7
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2da
    .line 299
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContentProviderNative;->createCancellationSignal()Landroid/os/ICancellationSignal;

    #@2dd
    move-result-object v9

    #@2de
    .line 300
    .restart local v9       #cancellationSignal:Landroid/os/ICancellationSignal;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e1
    .line 301
    invoke-interface {v9}, Landroid/os/ICancellationSignal;->asBinder()Landroid/os/IBinder;

    #@2e4
    move-result-object v3

    #@2e5
    move-object/from16 v0, p3

    #@2e7
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V
    :try_end_2ea
    .catch Ljava/lang/Exception; {:try_start_a5 .. :try_end_2ea} :catch_9c

    #@2ea
    .line 302
    const/4 v3, 0x1

    #@2eb
    goto/16 :goto_7

    #@2ed
    .line 79
    nop

    #@2ee
    :pswitch_data_2ee
    .packed-switch 0x1
        :pswitch_8
        :pswitch_a5
        :pswitch_c9
        :pswitch_16b
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_195
        :pswitch_3
        :pswitch_3
        :pswitch_f9
        :pswitch_1cb
        :pswitch_205
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_127
        :pswitch_23f
        :pswitch_269
        :pswitch_293
        :pswitch_2d3
    .end packed-switch
.end method
