.class public Landroid/content/SyncContext;
.super Ljava/lang/Object;
.source "SyncContext.java"


# static fields
.field private static final HEARTBEAT_SEND_INTERVAL_IN_MS:J = 0x3e8L


# instance fields
.field private mLastHeartbeatSendTime:J

.field private mSyncContext:Landroid/content/ISyncContext;


# direct methods
.method public constructor <init>(Landroid/content/ISyncContext;)V
    .registers 4
    .parameter "syncContextInterface"

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    iput-object p1, p0, Landroid/content/SyncContext;->mSyncContext:Landroid/content/ISyncContext;

    #@5
    .line 34
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Landroid/content/SyncContext;->mLastHeartbeatSendTime:J

    #@9
    .line 35
    return-void
.end method

.method private updateHeartbeat()V
    .registers 7

    #@0
    .prologue
    .line 55
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    .line 56
    .local v0, now:J
    iget-wide v2, p0, Landroid/content/SyncContext;->mLastHeartbeatSendTime:J

    #@6
    const-wide/16 v4, 0x3e8

    #@8
    add-long/2addr v2, v4

    #@9
    cmp-long v2, v0, v2

    #@b
    if-gez v2, :cond_e

    #@d
    .line 65
    :cond_d
    :goto_d
    return-void

    #@e
    .line 58
    :cond_e
    :try_start_e
    iput-wide v0, p0, Landroid/content/SyncContext;->mLastHeartbeatSendTime:J

    #@10
    .line 59
    iget-object v2, p0, Landroid/content/SyncContext;->mSyncContext:Landroid/content/ISyncContext;

    #@12
    if-eqz v2, :cond_d

    #@14
    .line 60
    iget-object v2, p0, Landroid/content/SyncContext;->mSyncContext:Landroid/content/ISyncContext;

    #@16
    invoke-interface {v2}, Landroid/content/ISyncContext;->sendHeartbeat()V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_19} :catch_1a

    #@19
    goto :goto_d

    #@1a
    .line 62
    :catch_1a
    move-exception v2

    #@1b
    goto :goto_d
.end method


# virtual methods
.method public getSyncContextBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/content/SyncContext;->mSyncContext:Landroid/content/ISyncContext;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/content/SyncContext;->mSyncContext:Landroid/content/ISyncContext;

    #@8
    invoke-interface {v0}, Landroid/content/ISyncContext;->asBinder()Landroid/os/IBinder;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public onFinished(Landroid/content/SyncResult;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 69
    :try_start_0
    iget-object v0, p0, Landroid/content/SyncContext;->mSyncContext:Landroid/content/ISyncContext;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 70
    iget-object v0, p0, Landroid/content/SyncContext;->mSyncContext:Landroid/content/ISyncContext;

    #@6
    invoke-interface {v0, p1}, Landroid/content/ISyncContext;->onFinished(Landroid/content/SyncResult;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 75
    :cond_9
    :goto_9
    return-void

    #@a
    .line 72
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public setStatusText(Ljava/lang/String;)V
    .registers 2
    .parameter "message"

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Landroid/content/SyncContext;->updateHeartbeat()V

    #@3
    .line 47
    return-void
.end method
