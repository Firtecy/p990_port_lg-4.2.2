.class public abstract Landroid/content/CursorEntityIterator;
.super Ljava/lang/Object;
.source "CursorEntityIterator.java"

# interfaces
.implements Landroid/content/EntityIterator;


# instance fields
.field private final mCursor:Landroid/database/Cursor;

.field private mIsClosed:Z


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/content/CursorEntityIterator;->mIsClosed:Z

    #@6
    .line 38
    iput-object p1, p0, Landroid/content/CursorEntityIterator;->mCursor:Landroid/database/Cursor;

    #@8
    .line 39
    iget-object v0, p0, Landroid/content/CursorEntityIterator;->mCursor:Landroid/database/Cursor;

    #@a
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    #@d
    .line 40
    return-void
.end method


# virtual methods
.method public final close()V
    .registers 3

    #@0
    .prologue
    .line 106
    iget-boolean v0, p0, Landroid/content/CursorEntityIterator;->mIsClosed:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 107
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "closing when already closed"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 109
    :cond_c
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Landroid/content/CursorEntityIterator;->mIsClosed:Z

    #@f
    .line 110
    iget-object v0, p0, Landroid/content/CursorEntityIterator;->mCursor:Landroid/database/Cursor;

    #@11
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@14
    .line 111
    return-void
.end method

.method public abstract getEntityAndIncrementCursor(Landroid/database/Cursor;)Landroid/content/Entity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public final hasNext()Z
    .registers 3

    #@0
    .prologue
    .line 59
    iget-boolean v0, p0, Landroid/content/CursorEntityIterator;->mIsClosed:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 60
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "calling hasNext() when the iterator is closed"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 63
    :cond_c
    iget-object v0, p0, Landroid/content/CursorEntityIterator;->mCursor:Landroid/database/Cursor;

    #@e
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_16

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public next()Landroid/content/Entity;
    .registers 4

    #@0
    .prologue
    .line 76
    iget-boolean v1, p0, Landroid/content/CursorEntityIterator;->mIsClosed:Z

    #@2
    if-eqz v1, :cond_c

    #@4
    .line 77
    new-instance v1, Ljava/lang/IllegalStateException;

    #@6
    const-string v2, "calling next() when the iterator is closed"

    #@8
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1

    #@c
    .line 79
    :cond_c
    invoke-virtual {p0}, Landroid/content/CursorEntityIterator;->hasNext()Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_1b

    #@12
    .line 80
    new-instance v1, Ljava/lang/IllegalStateException;

    #@14
    const-string/jumbo v2, "you may only call next() if hasNext() is true"

    #@17
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 84
    :cond_1b
    :try_start_1b
    iget-object v1, p0, Landroid/content/CursorEntityIterator;->mCursor:Landroid/database/Cursor;

    #@1d
    invoke-virtual {p0, v1}, Landroid/content/CursorEntityIterator;->getEntityAndIncrementCursor(Landroid/database/Cursor;)Landroid/content/Entity;
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_20} :catch_22

    #@20
    move-result-object v1

    #@21
    return-object v1

    #@22
    .line 85
    :catch_22
    move-exception v0

    #@23
    .line 86
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@25
    const-string v2, "caught a remote exception, this process will die soon"

    #@27
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@2a
    throw v1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 27
    invoke-virtual {p0}, Landroid/content/CursorEntityIterator;->next()Landroid/content/Entity;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public remove()V
    .registers 3

    #@0
    .prologue
    .line 91
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string/jumbo v1, "remove not supported by EntityIterators"

    #@5
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@8
    throw v0
.end method

.method public final reset()V
    .registers 3

    #@0
    .prologue
    .line 95
    iget-boolean v0, p0, Landroid/content/CursorEntityIterator;->mIsClosed:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 96
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "calling reset() when the iterator is closed"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 98
    :cond_c
    iget-object v0, p0, Landroid/content/CursorEntityIterator;->mCursor:Landroid/database/Cursor;

    #@e
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    #@11
    .line 99
    return-void
.end method
