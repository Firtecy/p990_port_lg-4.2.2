.class Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;
.super Landroid/content/ISyncAdapter$Stub;
.source "AbstractThreadedSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/AbstractThreadedSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ISyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/content/AbstractThreadedSyncAdapter;


# direct methods
.method private constructor <init>(Landroid/content/AbstractThreadedSyncAdapter;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 149
    iput-object p1, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@2
    invoke-direct {p0}, Landroid/content/ISyncAdapter$Stub;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/AbstractThreadedSyncAdapter;Landroid/content/AbstractThreadedSyncAdapter$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 149
    invoke-direct {p0, p1}, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;-><init>(Landroid/content/AbstractThreadedSyncAdapter;)V

    #@3
    return-void
.end method


# virtual methods
.method public cancelSync(Landroid/content/ISyncContext;)V
    .registers 8
    .parameter "syncContext"

    #@0
    .prologue
    .line 190
    const/4 v2, 0x0

    #@1
    .line 191
    .local v2, info:Landroid/content/AbstractThreadedSyncAdapter$SyncThread;
    iget-object v3, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@3
    invoke-static {v3}, Landroid/content/AbstractThreadedSyncAdapter;->access$200(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/lang/Object;

    #@6
    move-result-object v4

    #@7
    monitor-enter v4

    #@8
    .line 192
    :try_start_8
    iget-object v3, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@a
    invoke-static {v3}, Landroid/content/AbstractThreadedSyncAdapter;->access$300(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/util/HashMap;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@11
    move-result-object v3

    #@12
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v1

    #@16
    .local v1, i$:Ljava/util/Iterator;
    :cond_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_31

    #@1c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;

    #@22
    .line 193
    .local v0, current:Landroid/content/AbstractThreadedSyncAdapter$SyncThread;
    invoke-static {v0}, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->access$700(Landroid/content/AbstractThreadedSyncAdapter$SyncThread;)Landroid/content/SyncContext;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Landroid/content/SyncContext;->getSyncContextBinder()Landroid/os/IBinder;

    #@29
    move-result-object v3

    #@2a
    invoke-interface {p1}, Landroid/content/ISyncContext;->asBinder()Landroid/os/IBinder;

    #@2d
    move-result-object v5

    #@2e
    if-ne v3, v5, :cond_16

    #@30
    .line 194
    move-object v2, v0

    #@31
    .line 198
    .end local v0           #current:Landroid/content/AbstractThreadedSyncAdapter$SyncThread;
    :cond_31
    monitor-exit v4
    :try_end_32
    .catchall {:try_start_8 .. :try_end_32} :catchall_42

    #@32
    .line 199
    if-eqz v2, :cond_41

    #@34
    .line 200
    iget-object v3, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@36
    invoke-static {v3}, Landroid/content/AbstractThreadedSyncAdapter;->access$800(Landroid/content/AbstractThreadedSyncAdapter;)Z

    #@39
    move-result v3

    #@3a
    if-eqz v3, :cond_45

    #@3c
    .line 201
    iget-object v3, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@3e
    invoke-virtual {v3, v2}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled(Ljava/lang/Thread;)V

    #@41
    .line 206
    :cond_41
    :goto_41
    return-void

    #@42
    .line 198
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_42
    move-exception v3

    #@43
    :try_start_43
    monitor-exit v4
    :try_end_44
    .catchall {:try_start_43 .. :try_end_44} :catchall_42

    #@44
    throw v3

    #@45
    .line 203
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_45
    iget-object v3, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@47
    invoke-virtual {v3}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled()V

    #@4a
    goto :goto_41
.end method

.method public initialize(Landroid/accounts/Account;Ljava/lang/String;)V
    .registers 6
    .parameter "account"
    .parameter "authority"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 209
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 210
    .local v0, extras:Landroid/os/Bundle;
    const-string v1, "initialize"

    #@7
    const/4 v2, 0x1

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@b
    .line 211
    const/4 v1, 0x0

    #@c
    invoke-virtual {p0, v1, p2, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->startSync(Landroid/content/ISyncContext;Ljava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;)V

    #@f
    .line 212
    return-void
.end method

.method public startSync(Landroid/content/ISyncContext;Ljava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;)V
    .registers 16
    .parameter "syncContext"
    .parameter "authority"
    .parameter "account"
    .parameter "extras"

    #@0
    .prologue
    .line 152
    new-instance v3, Landroid/content/SyncContext;

    #@2
    invoke-direct {v3, p1}, Landroid/content/SyncContext;-><init>(Landroid/content/ISyncContext;)V

    #@5
    .line 157
    .local v3, syncContextClient:Landroid/content/SyncContext;
    iget-object v1, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@7
    invoke-static {v1, p3}, Landroid/content/AbstractThreadedSyncAdapter;->access$100(Landroid/content/AbstractThreadedSyncAdapter;Landroid/accounts/Account;)Landroid/accounts/Account;

    #@a
    move-result-object v9

    #@b
    .line 158
    .local v9, threadsKey:Landroid/accounts/Account;
    iget-object v1, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@d
    invoke-static {v1}, Landroid/content/AbstractThreadedSyncAdapter;->access$200(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/lang/Object;

    #@10
    move-result-object v10

    #@11
    monitor-enter v10

    #@12
    .line 159
    :try_start_12
    iget-object v1, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@14
    invoke-static {v1}, Landroid/content/AbstractThreadedSyncAdapter;->access$300(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/util/HashMap;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_83

    #@1e
    .line 160
    iget-object v1, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@20
    invoke-static {v1}, Landroid/content/AbstractThreadedSyncAdapter;->access$400(Landroid/content/AbstractThreadedSyncAdapter;)Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_45

    #@26
    if-eqz p4, :cond_45

    #@28
    const-string v1, "initialize"

    #@2a
    const/4 v2, 0x0

    #@2b
    invoke-virtual {p4, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_45

    #@31
    .line 163
    invoke-static {p3, p2}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    #@34
    move-result v1

    #@35
    if-gez v1, :cond_3b

    #@37
    .line 164
    const/4 v1, 0x1

    #@38
    invoke-static {p3, p2, v1}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    #@3b
    .line 166
    :cond_3b
    new-instance v1, Landroid/content/SyncResult;

    #@3d
    invoke-direct {v1}, Landroid/content/SyncResult;-><init>()V

    #@40
    invoke-virtual {v3, v1}, Landroid/content/SyncContext;->onFinished(Landroid/content/SyncResult;)V

    #@43
    .line 167
    monitor-exit v10

    #@44
    .line 185
    :cond_44
    :goto_44
    return-void

    #@45
    .line 169
    :cond_45
    new-instance v0, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;

    #@47
    iget-object v1, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@49
    new-instance v2, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v4, "SyncAdapterThread-"

    #@50
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    iget-object v4, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@56
    invoke-static {v4}, Landroid/content/AbstractThreadedSyncAdapter;->access$500(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    #@5d
    move-result v4

    #@5e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v2

    #@66
    const/4 v7, 0x0

    #@67
    move-object v4, p2

    #@68
    move-object v5, p3

    #@69
    move-object v6, p4

    #@6a
    invoke-direct/range {v0 .. v7}, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;-><init>(Landroid/content/AbstractThreadedSyncAdapter;Ljava/lang/String;Landroid/content/SyncContext;Ljava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/AbstractThreadedSyncAdapter$1;)V

    #@6d
    .line 172
    .local v0, syncThread:Landroid/content/AbstractThreadedSyncAdapter$SyncThread;
    iget-object v1, p0, Landroid/content/AbstractThreadedSyncAdapter$ISyncAdapterImpl;->this$0:Landroid/content/AbstractThreadedSyncAdapter;

    #@6f
    invoke-static {v1}, Landroid/content/AbstractThreadedSyncAdapter;->access$300(Landroid/content/AbstractThreadedSyncAdapter;)Ljava/util/HashMap;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@76
    .line 173
    invoke-virtual {v0}, Landroid/content/AbstractThreadedSyncAdapter$SyncThread;->start()V

    #@79
    .line 174
    const/4 v8, 0x0

    #@7a
    .line 178
    .end local v0           #syncThread:Landroid/content/AbstractThreadedSyncAdapter$SyncThread;
    .local v8, alreadyInProgress:Z
    :goto_7a
    monitor-exit v10
    :try_end_7b
    .catchall {:try_start_12 .. :try_end_7b} :catchall_85

    #@7b
    .line 182
    if-eqz v8, :cond_44

    #@7d
    .line 183
    sget-object v1, Landroid/content/SyncResult;->ALREADY_IN_PROGRESS:Landroid/content/SyncResult;

    #@7f
    invoke-virtual {v3, v1}, Landroid/content/SyncContext;->onFinished(Landroid/content/SyncResult;)V

    #@82
    goto :goto_44

    #@83
    .line 176
    .end local v8           #alreadyInProgress:Z
    :cond_83
    const/4 v8, 0x1

    #@84
    .restart local v8       #alreadyInProgress:Z
    goto :goto_7a

    #@85
    .line 178
    .end local v8           #alreadyInProgress:Z
    :catchall_85
    move-exception v1

    #@86
    :try_start_86
    monitor-exit v10
    :try_end_87
    .catchall {:try_start_86 .. :try_end_87} :catchall_85

    #@87
    throw v1
.end method
