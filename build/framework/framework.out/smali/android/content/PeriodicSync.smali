.class public Landroid/content/PeriodicSync;
.super Ljava/lang/Object;
.source "PeriodicSync.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/PeriodicSync;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final account:Landroid/accounts/Account;

.field public final authority:Ljava/lang/String;

.field public final extras:Landroid/os/Bundle;

.field public final period:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 57
    new-instance v0, Landroid/content/PeriodicSync$1;

    #@2
    invoke-direct {v0}, Landroid/content/PeriodicSync$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/PeriodicSync;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V
    .registers 7
    .parameter "account"
    .parameter "authority"
    .parameter "extras"
    .parameter "period"

    #@0
    .prologue
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 40
    iput-object p1, p0, Landroid/content/PeriodicSync;->account:Landroid/accounts/Account;

    #@5
    .line 41
    iput-object p2, p0, Landroid/content/PeriodicSync;->authority:Ljava/lang/String;

    #@7
    .line 42
    new-instance v0, Landroid/os/Bundle;

    #@9
    invoke-direct {v0, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@c
    iput-object v0, p0, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    #@e
    .line 43
    iput-wide p4, p0, Landroid/content/PeriodicSync;->period:J

    #@10
    .line 44
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 47
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 9
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 69
    if-ne p1, p0, :cond_5

    #@4
    .line 79
    :cond_4
    :goto_4
    return v1

    #@5
    .line 73
    :cond_5
    instance-of v3, p1, Landroid/content/PeriodicSync;

    #@7
    if-nez v3, :cond_b

    #@9
    move v1, v2

    #@a
    .line 74
    goto :goto_4

    #@b
    :cond_b
    move-object v0, p1

    #@c
    .line 77
    check-cast v0, Landroid/content/PeriodicSync;

    #@e
    .line 79
    .local v0, other:Landroid/content/PeriodicSync;
    iget-object v3, p0, Landroid/content/PeriodicSync;->account:Landroid/accounts/Account;

    #@10
    iget-object v4, v0, Landroid/content/PeriodicSync;->account:Landroid/accounts/Account;

    #@12
    invoke-virtual {v3, v4}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_34

    #@18
    iget-object v3, p0, Landroid/content/PeriodicSync;->authority:Ljava/lang/String;

    #@1a
    iget-object v4, v0, Landroid/content/PeriodicSync;->authority:Ljava/lang/String;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_34

    #@22
    iget-wide v3, p0, Landroid/content/PeriodicSync;->period:J

    #@24
    iget-wide v5, v0, Landroid/content/PeriodicSync;->period:J

    #@26
    cmp-long v3, v3, v5

    #@28
    if-nez v3, :cond_34

    #@2a
    iget-object v3, p0, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    #@2c
    iget-object v4, v0, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    #@2e
    invoke-static {v3, v4}, Landroid/content/SyncStorageEngine;->equals(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    #@31
    move-result v3

    #@32
    if-nez v3, :cond_4

    #@34
    :cond_34
    move v1, v2

    #@35
    goto :goto_4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Landroid/content/PeriodicSync;->account:Landroid/accounts/Account;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@5
    .line 52
    iget-object v0, p0, Landroid/content/PeriodicSync;->authority:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 53
    iget-object v0, p0, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@f
    .line 54
    iget-wide v0, p0, Landroid/content/PeriodicSync;->period:J

    #@11
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@14
    .line 55
    return-void
.end method
