.class final Landroid/content/ContentProviderProxy;
.super Ljava/lang/Object;
.source "ContentProviderNative.java"

# interfaces
.implements Landroid/content/IContentProvider;


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 323
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 324
    iput-object p1, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 325
    return-void
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    #@0
    .prologue
    .line 451
    .local p1, operations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 452
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v3

    #@8
    .line 454
    .local v3, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v5, "android.content.IContentProvider"

    #@a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 455
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v5

    #@11
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 456
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v1

    #@18
    .local v1, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v5

    #@1c
    if-eqz v5, :cond_31

    #@1e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v2

    #@22
    check-cast v2, Landroid/content/ContentProviderOperation;

    #@24
    .line 457
    .local v2, operation:Landroid/content/ContentProviderOperation;
    const/4 v5, 0x0

    #@25
    invoke-virtual {v2, v0, v5}, Landroid/content/ContentProviderOperation;->writeToParcel(Landroid/os/Parcel;I)V
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_29

    #@28
    goto :goto_18

    #@29
    .line 466
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #operation:Landroid/content/ContentProviderOperation;
    :catchall_29
    move-exception v5

    #@2a
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 467
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v5

    #@31
    .line 459
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_31
    :try_start_31
    iget-object v5, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@33
    const/16 v6, 0x14

    #@35
    const/4 v7, 0x0

    #@36
    invoke-interface {v5, v6, v0, v3, v7}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@39
    .line 461
    invoke-static {v3}, Landroid/database/DatabaseUtils;->readExceptionWithOperationApplicationExceptionFromParcel(Landroid/os/Parcel;)V

    #@3c
    .line 462
    sget-object v5, Landroid/content/ContentProviderResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3e
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@41
    move-result-object v4

    #@42
    check-cast v4, [Landroid/content/ContentProviderResult;
    :try_end_44
    .catchall {:try_start_31 .. :try_end_44} :catchall_29

    #@44
    .line 466
    .local v4, results:[Landroid/content/ContentProviderResult;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 467
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@4a
    return-object v4
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 329
    iget-object v0, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .registers 9
    .parameter "url"
    .parameter "values"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 430
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 431
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 433
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.content.IContentProvider"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 435
    const/4 v3, 0x0

    #@e
    invoke-virtual {p1, v1, v3}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 436
    const/4 v3, 0x0

    #@12
    invoke-virtual {v1, p2, v3}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@15
    .line 438
    iget-object v3, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/16 v4, 0xd

    #@19
    const/4 v5, 0x0

    #@1a
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 440
    invoke-static {v2}, Landroid/database/DatabaseUtils;->readExceptionFromParcel(Landroid/os/Parcel;)V

    #@20
    .line 441
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_23
    .catchall {:try_start_8 .. :try_end_23} :catchall_2b

    #@23
    move-result v0

    #@24
    .line 444
    .local v0, count:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 445
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2a
    return v0

    #@2b
    .line 444
    .end local v0           #count:I
    :catchall_2b
    move-exception v3

    #@2c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 445
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@32
    throw v3
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 10
    .parameter "method"
    .parameter "request"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 563
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 564
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 566
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.content.IContentProvider"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 568
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 569
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 570
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@16
    .line 572
    iget-object v3, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v4, 0x15

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 574
    invoke-static {v2}, Landroid/database/DatabaseUtils;->readExceptionFromParcel(Landroid/os/Parcel;)V

    #@21
    .line 575
    invoke-virtual {v2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_2c

    #@24
    move-result-object v0

    #@25
    .line 578
    .local v0, bundle:Landroid/os/Bundle;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 579
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2b
    return-object v0

    #@2c
    .line 578
    .end local v0           #bundle:Landroid/os/Bundle;
    :catchall_2c
    move-exception v3

    #@2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 579
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@33
    throw v3
.end method

.method public createCancellationSignal()Landroid/os/ICancellationSignal;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 629
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 630
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 632
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.content.IContentProvider"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 634
    iget-object v3, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x18

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 637
    invoke-static {v2}, Landroid/database/DatabaseUtils;->readExceptionFromParcel(Landroid/os/Parcel;)V

    #@18
    .line 638
    invoke-virtual {v2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v3}, Landroid/os/ICancellationSignal$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/ICancellationSignal;
    :try_end_1f
    .catchall {:try_start_8 .. :try_end_1f} :catchall_27

    #@1f
    move-result-object v0

    #@20
    .line 642
    .local v0, cancellationSignal:Landroid/os/ICancellationSignal;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 643
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@26
    return-object v0

    #@27
    .line 642
    .end local v0           #cancellationSignal:Landroid/os/ICancellationSignal;
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 643
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 10
    .parameter "url"
    .parameter "selection"
    .parameter "selectionArgs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 473
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 474
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 476
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.content.IContentProvider"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 478
    const/4 v3, 0x0

    #@e
    invoke-virtual {p1, v1, v3}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 479
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 480
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@17
    .line 482
    iget-object v3, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x4

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 484
    invoke-static {v2}, Landroid/database/DatabaseUtils;->readExceptionFromParcel(Landroid/os/Parcel;)V

    #@21
    .line 485
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_2c

    #@24
    move-result v0

    #@25
    .line 488
    .local v0, count:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 489
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2b
    return v0

    #@2c
    .line 488
    .end local v0           #count:I
    :catchall_2c
    move-exception v3

    #@2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 489
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@33
    throw v3
.end method

.method public getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;
    .registers 9
    .parameter "url"
    .parameter "mimeTypeFilter"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 585
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 586
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 588
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.content.IContentProvider"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 590
    const/4 v3, 0x0

    #@e
    invoke-virtual {p1, v0, v3}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 591
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 593
    iget-object v3, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x16

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 595
    invoke-static {v2}, Landroid/database/DatabaseUtils;->readExceptionFromParcel(Landroid/os/Parcel;)V

    #@1f
    .line 596
    invoke-virtual {v2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2a

    #@22
    move-result-object v1

    #@23
    .line 599
    .local v1, out:[Ljava/lang/String;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 600
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@29
    return-object v1

    #@2a
    .line 599
    .end local v1           #out:[Ljava/lang/String;
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 600
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v3
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 8
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 390
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 391
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 393
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.content.IContentProvider"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 395
    const/4 v3, 0x0

    #@e
    invoke-virtual {p1, v0, v3}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 397
    iget-object v3, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x2

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 399
    invoke-static {v2}, Landroid/database/DatabaseUtils;->readExceptionFromParcel(Landroid/os/Parcel;)V

    #@1b
    .line 400
    invoke-virtual {v2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result-object v1

    #@1f
    .line 403
    .local v1, out:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 404
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@25
    return-object v1

    #@26
    .line 403
    .end local v1           #out:Ljava/lang/String;
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 404
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 9
    .parameter "url"
    .parameter "values"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 410
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 411
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 413
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.content.IContentProvider"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 415
    const/4 v3, 0x0

    #@e
    invoke-virtual {p1, v0, v3}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 416
    const/4 v3, 0x0

    #@12
    invoke-virtual {p2, v0, v3}, Landroid/content/ContentValues;->writeToParcel(Landroid/os/Parcel;I)V

    #@15
    .line 418
    iget-object v3, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/4 v4, 0x3

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 420
    invoke-static {v2}, Landroid/database/DatabaseUtils;->readExceptionFromParcel(Landroid/os/Parcel;)V

    #@1f
    .line 421
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@21
    invoke-interface {v3, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Landroid/net/Uri;
    :try_end_27
    .catchall {:try_start_8 .. :try_end_27} :catchall_2e

    #@27
    .line 424
    .local v1, out:Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 425
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2d
    return-object v1

    #@2e
    .line 424
    .end local v1           #out:Landroid/net/Uri;
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 425
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v3
.end method

.method public openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .registers 10
    .parameter "url"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 540
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 541
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v3

    #@8
    .line 543
    .local v3, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.content.IContentProvider"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 545
    const/4 v4, 0x0

    #@e
    invoke-virtual {p1, v0, v4}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 546
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 548
    iget-object v4, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v5, 0xf

    #@18
    const/4 v6, 0x0

    #@19
    invoke-interface {v4, v5, v0, v3, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 550
    invoke-static {v3}, Landroid/database/DatabaseUtils;->readExceptionWithFileNotFoundExceptionFromParcel(Landroid/os/Parcel;)V

    #@1f
    .line 551
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v2

    #@23
    .line 552
    .local v2, has:I
    if-eqz v2, :cond_35

    #@25
    sget-object v4, Landroid/content/res/AssetFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@27
    invoke-interface {v4, v3}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2a
    move-result-object v4

    #@2b
    check-cast v4, Landroid/content/res/AssetFileDescriptor;
    :try_end_2d
    .catchall {:try_start_8 .. :try_end_2d} :catchall_37

    #@2d
    move-object v1, v4

    #@2e
    .line 556
    .local v1, fd:Landroid/content/res/AssetFileDescriptor;
    :goto_2e
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 557
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@34
    return-object v1

    #@35
    .line 552
    .end local v1           #fd:Landroid/content/res/AssetFileDescriptor;
    :cond_35
    const/4 v1, 0x0

    #@36
    goto :goto_2e

    #@37
    .line 556
    .end local v2           #has:I
    :catchall_37
    move-exception v4

    #@38
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 557
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v4
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 10
    .parameter "url"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 518
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 519
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v3

    #@8
    .line 521
    .local v3, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.content.IContentProvider"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 523
    const/4 v4, 0x0

    #@e
    invoke-virtual {p1, v0, v4}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 524
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 526
    iget-object v4, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v5, 0xe

    #@18
    const/4 v6, 0x0

    #@19
    invoke-interface {v4, v5, v0, v3, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 528
    invoke-static {v3}, Landroid/database/DatabaseUtils;->readExceptionWithFileNotFoundExceptionFromParcel(Landroid/os/Parcel;)V

    #@1f
    .line 529
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v2

    #@23
    .line 530
    .local v2, has:I
    if-eqz v2, :cond_30

    #@25
    invoke-virtual {v3}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_32

    #@28
    move-result-object v1

    #@29
    .line 533
    .local v1, fd:Landroid/os/ParcelFileDescriptor;
    :goto_29
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 534
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@2f
    return-object v1

    #@30
    .line 530
    .end local v1           #fd:Landroid/os/ParcelFileDescriptor;
    :cond_30
    const/4 v1, 0x0

    #@31
    goto :goto_29

    #@32
    .line 533
    .end local v2           #has:I
    :catchall_32
    move-exception v4

    #@33
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 534
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@39
    throw v4
.end method

.method public openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
    .registers 11
    .parameter "url"
    .parameter "mimeType"
    .parameter "opts"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 606
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 607
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v3

    #@8
    .line 609
    .local v3, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.content.IContentProvider"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 611
    const/4 v4, 0x0

    #@e
    invoke-virtual {p1, v0, v4}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 612
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 613
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@17
    .line 615
    iget-object v4, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v5, 0x17

    #@1b
    const/4 v6, 0x0

    #@1c
    invoke-interface {v4, v5, v0, v3, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 617
    invoke-static {v3}, Landroid/database/DatabaseUtils;->readExceptionWithFileNotFoundExceptionFromParcel(Landroid/os/Parcel;)V

    #@22
    .line 618
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v2

    #@26
    .line 619
    .local v2, has:I
    if-eqz v2, :cond_38

    #@28
    sget-object v4, Landroid/content/res/AssetFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2a
    invoke-interface {v4, v3}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2d
    move-result-object v4

    #@2e
    check-cast v4, Landroid/content/res/AssetFileDescriptor;
    :try_end_30
    .catchall {:try_start_8 .. :try_end_30} :catchall_3a

    #@30
    move-object v1, v4

    #@31
    .line 623
    .local v1, fd:Landroid/content/res/AssetFileDescriptor;
    :goto_31
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 624
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@37
    return-object v1

    #@38
    .line 619
    .end local v1           #fd:Landroid/content/res/AssetFileDescriptor;
    :cond_38
    const/4 v1, 0x0

    #@39
    goto :goto_31

    #@3a
    .line 623
    .end local v2           #has:I
    :catchall_3a
    move-exception v4

    #@3b
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    .line 624
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@41
    throw v4
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;
    .registers 17
    .parameter "url"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"
    .parameter "cancellationSignal"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 335
    new-instance v0, Landroid/database/BulkCursorToCursorAdaptor;

    #@2
    invoke-direct {v0}, Landroid/database/BulkCursorToCursorAdaptor;-><init>()V

    #@5
    .line 336
    .local v0, adaptor:Landroid/database/BulkCursorToCursorAdaptor;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v2

    #@9
    .line 337
    .local v2, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@c
    move-result-object v6

    #@d
    .line 339
    .local v6, reply:Landroid/os/Parcel;
    :try_start_d
    const-string v7, "android.content.IContentProvider"

    #@f
    invoke-virtual {v2, v7}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@12
    .line 341
    const/4 v7, 0x0

    #@13
    invoke-virtual {p1, v2, v7}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@16
    .line 342
    const/4 v5, 0x0

    #@17
    .line 343
    .local v5, length:I
    if-eqz p2, :cond_1a

    #@19
    .line 344
    array-length v5, p2

    #@1a
    .line 346
    :cond_1a
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 347
    const/4 v4, 0x0

    #@1e
    .local v4, i:I
    :goto_1e
    if-ge v4, v5, :cond_28

    #@20
    .line 348
    aget-object v7, p2, v4

    #@22
    invoke-virtual {v2, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@25
    .line 347
    add-int/lit8 v4, v4, 0x1

    #@27
    goto :goto_1e

    #@28
    .line 350
    :cond_28
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2b
    .line 351
    if-eqz p4, :cond_3c

    #@2d
    .line 352
    array-length v5, p4

    #@2e
    .line 356
    :goto_2e
    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 357
    const/4 v4, 0x0

    #@32
    :goto_32
    if-ge v4, v5, :cond_3e

    #@34
    .line 358
    aget-object v7, p4, v4

    #@36
    invoke-virtual {v2, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@39
    .line 357
    add-int/lit8 v4, v4, 0x1

    #@3b
    goto :goto_32

    #@3c
    .line 354
    :cond_3c
    const/4 v5, 0x0

    #@3d
    goto :goto_2e

    #@3e
    .line 360
    :cond_3e
    invoke-virtual {v2, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@41
    .line 361
    invoke-virtual {v0}, Landroid/database/BulkCursorToCursorAdaptor;->getObserver()Landroid/database/IContentObserver;

    #@44
    move-result-object v7

    #@45
    invoke-interface {v7}, Landroid/database/IContentObserver;->asBinder()Landroid/os/IBinder;

    #@48
    move-result-object v7

    #@49
    invoke-virtual {v2, v7}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@4c
    .line 362
    if-eqz p6, :cond_77

    #@4e
    invoke-interface/range {p6 .. p6}, Landroid/os/ICancellationSignal;->asBinder()Landroid/os/IBinder;

    #@51
    move-result-object v7

    #@52
    :goto_52
    invoke-virtual {v2, v7}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@55
    .line 364
    iget-object v7, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@57
    const/4 v8, 0x1

    #@58
    const/4 v9, 0x0

    #@59
    invoke-interface {v7, v8, v2, v6, v9}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@5c
    .line 366
    invoke-static {v6}, Landroid/database/DatabaseUtils;->readExceptionFromParcel(Landroid/os/Parcel;)V

    #@5f
    .line 368
    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    #@62
    move-result v7

    #@63
    if-eqz v7, :cond_79

    #@65
    .line 369
    sget-object v7, Landroid/database/BulkCursorDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@67
    invoke-interface {v7, v6}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6a
    move-result-object v1

    #@6b
    check-cast v1, Landroid/database/BulkCursorDescriptor;

    #@6d
    .line 370
    .local v1, d:Landroid/database/BulkCursorDescriptor;
    invoke-virtual {v0, v1}, Landroid/database/BulkCursorToCursorAdaptor;->initialize(Landroid/database/BulkCursorDescriptor;)V
    :try_end_70
    .catchall {:try_start_d .. :try_end_70} :catchall_83
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_70} :catch_7e
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_70} :catch_8b

    #@70
    .line 383
    .end local v1           #d:Landroid/database/BulkCursorDescriptor;
    :goto_70
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@73
    .line 384
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    #@76
    return-object v0

    #@77
    .line 362
    :cond_77
    const/4 v7, 0x0

    #@78
    goto :goto_52

    #@79
    .line 372
    :cond_79
    :try_start_79
    invoke-virtual {v0}, Landroid/database/BulkCursorToCursorAdaptor;->close()V
    :try_end_7c
    .catchall {:try_start_79 .. :try_end_7c} :catchall_83
    .catch Landroid/os/RemoteException; {:try_start_79 .. :try_end_7c} :catch_7e
    .catch Ljava/lang/RuntimeException; {:try_start_79 .. :try_end_7c} :catch_8b

    #@7c
    .line 373
    const/4 v0, 0x0

    #@7d
    goto :goto_70

    #@7e
    .line 376
    .end local v4           #i:I
    .end local v5           #length:I
    :catch_7e
    move-exception v3

    #@7f
    .line 377
    .local v3, ex:Landroid/os/RemoteException;
    :try_start_7f
    invoke-virtual {v0}, Landroid/database/BulkCursorToCursorAdaptor;->close()V

    #@82
    .line 378
    throw v3
    :try_end_83
    .catchall {:try_start_7f .. :try_end_83} :catchall_83

    #@83
    .line 383
    .end local v3           #ex:Landroid/os/RemoteException;
    :catchall_83
    move-exception v7

    #@84
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@87
    .line 384
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    #@8a
    throw v7

    #@8b
    .line 379
    :catch_8b
    move-exception v3

    #@8c
    .line 380
    .local v3, ex:Ljava/lang/RuntimeException;
    :try_start_8c
    invoke-virtual {v0}, Landroid/database/BulkCursorToCursorAdaptor;->close()V

    #@8f
    .line 381
    throw v3
    :try_end_90
    .catchall {:try_start_8c .. :try_end_90} :catchall_83
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 11
    .parameter "url"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 495
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 496
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 498
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.content.IContentProvider"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 500
    const/4 v3, 0x0

    #@e
    invoke-virtual {p1, v1, v3}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 501
    const/4 v3, 0x0

    #@12
    invoke-virtual {p2, v1, v3}, Landroid/content/ContentValues;->writeToParcel(Landroid/os/Parcel;I)V

    #@15
    .line 502
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@18
    .line 503
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@1b
    .line 505
    iget-object v3, p0, Landroid/content/ContentProviderProxy;->mRemote:Landroid/os/IBinder;

    #@1d
    const/16 v4, 0xa

    #@1f
    const/4 v5, 0x0

    #@20
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@23
    .line 507
    invoke-static {v2}, Landroid/database/DatabaseUtils;->readExceptionFromParcel(Landroid/os/Parcel;)V

    #@26
    .line 508
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_29
    .catchall {:try_start_8 .. :try_end_29} :catchall_31

    #@29
    move-result v0

    #@2a
    .line 511
    .local v0, count:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 512
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@30
    return v0

    #@31
    .line 511
    .end local v0           #count:I
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 512
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method
