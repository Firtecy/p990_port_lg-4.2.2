.class public Landroid/content/ClipDescription;
.super Ljava/lang/Object;
.source "ClipDescription.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/ClipDescription;",
            ">;"
        }
    .end annotation
.end field

.field public static final MIMETYPE_TEXT_HTML:Ljava/lang/String; = "text/html"

.field public static final MIMETYPE_TEXT_INTENT:Ljava/lang/String; = "text/vnd.android.intent"

.field public static final MIMETYPE_TEXT_PLAIN:Ljava/lang/String; = "text/plain"

.field public static final MIMETYPE_TEXT_URILIST:Ljava/lang/String; = "text/uri-list"


# instance fields
.field final mLabel:Ljava/lang/CharSequence;

.field final mMimeTypes:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 240
    new-instance v0, Landroid/content/ClipDescription$1;

    #@2
    invoke-direct {v0}, Landroid/content/ClipDescription$1;-><init>()V

    #@5
    sput-object v0, Landroid/content/ClipDescription;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/ClipDescription;)V
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 83
    iget-object v0, p1, Landroid/content/ClipDescription;->mLabel:Ljava/lang/CharSequence;

    #@5
    iput-object v0, p0, Landroid/content/ClipDescription;->mLabel:Ljava/lang/CharSequence;

    #@7
    .line 84
    iget-object v0, p1, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@9
    iput-object v0, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@b
    .line 85
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 235
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 236
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@5
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/lang/CharSequence;

    #@b
    iput-object v0, p0, Landroid/content/ClipDescription;->mLabel:Ljava/lang/CharSequence;

    #@d
    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@13
    .line 238
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;[Ljava/lang/String;)V
    .registers 5
    .parameter "label"
    .parameter "mimeTypes"

    #@0
    .prologue
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 72
    if-nez p2, :cond_e

    #@5
    .line 73
    new-instance v0, Ljava/lang/NullPointerException;

    #@7
    const-string/jumbo v1, "mimeTypes is null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 75
    :cond_e
    iput-object p1, p0, Landroid/content/ClipDescription;->mLabel:Ljava/lang/CharSequence;

    #@10
    .line 76
    iput-object p2, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@12
    .line 77
    return-void
.end method

.method public static compareMimeTypes(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 8
    .parameter "concreteType"
    .parameter "desiredType"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 94
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v1

    #@6
    .line 95
    .local v1, typeLength:I
    const/4 v4, 0x3

    #@7
    if-ne v1, v4, :cond_12

    #@9
    const-string v4, "*/*"

    #@b
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_12

    #@11
    .line 110
    :cond_11
    :goto_11
    return v2

    #@12
    .line 99
    :cond_12
    const/16 v4, 0x2f

    #@14
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    #@17
    move-result v0

    #@18
    .line 100
    .local v0, slashpos:I
    if-lez v0, :cond_30

    #@1a
    .line 101
    add-int/lit8 v4, v0, 0x2

    #@1c
    if-ne v1, v4, :cond_32

    #@1e
    add-int/lit8 v4, v0, 0x1

    #@20
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@23
    move-result v4

    #@24
    const/16 v5, 0x2a

    #@26
    if-ne v4, v5, :cond_32

    #@28
    .line 102
    add-int/lit8 v4, v0, 0x1

    #@2a
    invoke-virtual {p1, v3, p0, v3, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@2d
    move-result v4

    #@2e
    if-nez v4, :cond_11

    #@30
    :cond_30
    move v2, v3

    #@31
    .line 110
    goto :goto_11

    #@32
    .line 105
    :cond_32
    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_30

    #@38
    goto :goto_11
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 226
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public filterMimeTypes(Ljava/lang/String;)[Ljava/lang/String;
    .registers 6
    .parameter "mimeType"

    #@0
    .prologue
    .line 145
    const/4 v0, 0x0

    #@1
    .line 146
    .local v0, array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    iget-object v3, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@4
    array-length v3, v3

    #@5
    if-ge v1, v3, :cond_22

    #@7
    .line 147
    iget-object v3, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@9
    aget-object v3, v3, v1

    #@b
    invoke-static {v3, p1}, Landroid/content/ClipDescription;->compareMimeTypes(Ljava/lang/String;Ljava/lang/String;)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1f

    #@11
    .line 148
    if-nez v0, :cond_18

    #@13
    .line 149
    new-instance v0, Ljava/util/ArrayList;

    #@15
    .end local v0           #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@18
    .line 151
    .restart local v0       #array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_18
    iget-object v3, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@1a
    aget-object v3, v3, v1

    #@1c
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1f
    .line 146
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_2

    #@22
    .line 154
    :cond_22
    if-nez v0, :cond_26

    #@24
    .line 155
    const/4 v2, 0x0

    #@25
    .line 159
    :goto_25
    return-object v2

    #@26
    .line 157
    :cond_26
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@29
    move-result v3

    #@2a
    new-array v2, v3, [Ljava/lang/String;

    #@2c
    .line 158
    .local v2, rawArray:[Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@2f
    goto :goto_25
.end method

.method public getLabel()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 117
    iget-object v0, p0, Landroid/content/ClipDescription;->mLabel:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getMimeType(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@2
    aget-object v0, v0, p1

    #@4
    return-object v0
.end method

.method public getMimeTypeCount()I
    .registers 2

    #@0
    .prologue
    .line 166
    iget-object v0, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@2
    array-length v0, v0

    #@3
    return v0
.end method

.method public hasMimeType(Ljava/lang/String;)Z
    .registers 4
    .parameter "mimeType"

    #@0
    .prologue
    .line 128
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_15

    #@6
    .line 129
    iget-object v1, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@8
    aget-object v1, v1, v0

    #@a
    invoke-static {v1, p1}, Landroid/content/ClipDescription;->compareMimeTypes(Ljava/lang/String;Ljava/lang/String;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    .line 130
    const/4 v1, 0x1

    #@11
    .line 133
    :goto_11
    return v1

    #@12
    .line 128
    :cond_12
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_1

    #@15
    .line 133
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_11
.end method

.method public toShortString(Ljava/lang/StringBuilder;)Z
    .registers 7
    .parameter "b"

    #@0
    .prologue
    const/16 v4, 0x22

    #@2
    const/16 v3, 0x20

    #@4
    .line 204
    const/4 v0, 0x1

    #@5
    .line 205
    .local v0, first:Z
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    iget-object v2, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@8
    array-length v2, v2

    #@9
    if-ge v1, v2, :cond_1b

    #@b
    .line 206
    if-nez v0, :cond_10

    #@d
    .line 207
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@10
    .line 209
    :cond_10
    const/4 v0, 0x0

    #@11
    .line 210
    iget-object v2, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@13
    aget-object v2, v2, v1

    #@15
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 205
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_6

    #@1b
    .line 212
    :cond_1b
    iget-object v2, p0, Landroid/content/ClipDescription;->mLabel:Ljava/lang/CharSequence;

    #@1d
    if-eqz v2, :cond_30

    #@1f
    .line 213
    if-nez v0, :cond_24

    #@21
    .line 214
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@24
    .line 216
    :cond_24
    const/4 v0, 0x0

    #@25
    .line 217
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@28
    .line 218
    iget-object v2, p0, Landroid/content/ClipDescription;->mLabel:Ljava/lang/CharSequence;

    #@2a
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@2d
    .line 219
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@30
    .line 221
    :cond_30
    if-nez v0, :cond_34

    #@32
    const/4 v2, 0x1

    #@33
    :goto_33
    return v2

    #@34
    :cond_34
    const/4 v2, 0x0

    #@35
    goto :goto_33
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 195
    .local v0, b:Ljava/lang/StringBuilder;
    const-string v1, "ClipDescription { "

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 196
    invoke-virtual {p0, v0}, Landroid/content/ClipDescription;->toShortString(Ljava/lang/StringBuilder;)Z

    #@f
    .line 197
    const-string v1, " }"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 199
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    return-object v1
.end method

.method public validate()V
    .registers 5

    #@0
    .prologue
    .line 178
    iget-object v1, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@2
    if-nez v1, :cond_d

    #@4
    .line 179
    new-instance v1, Ljava/lang/NullPointerException;

    #@6
    const-string/jumbo v2, "null mime types"

    #@9
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1

    #@d
    .line 181
    :cond_d
    iget-object v1, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@f
    array-length v1, v1

    #@10
    if-gtz v1, :cond_1b

    #@12
    .line 182
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@14
    const-string/jumbo v2, "must have at least 1 mime type"

    #@17
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 184
    :cond_1b
    const/4 v0, 0x0

    #@1c
    .local v0, i:I
    :goto_1c
    iget-object v1, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@1e
    array-length v1, v1

    #@1f
    if-ge v0, v1, :cond_4a

    #@21
    .line 185
    iget-object v1, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@23
    aget-object v1, v1, v0

    #@25
    if-nez v1, :cond_47

    #@27
    .line 186
    new-instance v1, Ljava/lang/NullPointerException;

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string/jumbo v3, "mime type at "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    const-string v3, " is null"

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@46
    throw v1

    #@47
    .line 184
    :cond_47
    add-int/lit8 v0, v0, 0x1

    #@49
    goto :goto_1c

    #@4a
    .line 189
    :cond_4a
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Landroid/content/ClipDescription;->mLabel:Ljava/lang/CharSequence;

    #@2
    invoke-static {v0, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@5
    .line 232
    iget-object v0, p0, Landroid/content/ClipDescription;->mMimeTypes:[Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@a
    .line 233
    return-void
.end method
