.class public final Landroid/content/Loader$ForceLoadContentObserver;
.super Landroid/database/ContentObserver;
.source "Loader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/Loader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ForceLoadContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/content/Loader;


# direct methods
.method public constructor <init>(Landroid/content/Loader;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 70
    .local p0, this:Landroid/content/Loader$ForceLoadContentObserver;,"Landroid/content/Loader<TD;>.ForceLoadContentObserver;"
    iput-object p1, p0, Landroid/content/Loader$ForceLoadContentObserver;->this$0:Landroid/content/Loader;

    #@2
    .line 71
    new-instance v0, Landroid/os/Handler;

    #@4
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@7
    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@a
    .line 72
    return-void
.end method


# virtual methods
.method public deliverSelfNotifications()Z
    .registers 2

    #@0
    .prologue
    .line 76
    .local p0, this:Landroid/content/Loader$ForceLoadContentObserver;,"Landroid/content/Loader<TD;>.ForceLoadContentObserver;"
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public onChange(Z)V
    .registers 3
    .parameter "selfChange"

    #@0
    .prologue
    .line 81
    .local p0, this:Landroid/content/Loader$ForceLoadContentObserver;,"Landroid/content/Loader<TD;>.ForceLoadContentObserver;"
    iget-object v0, p0, Landroid/content/Loader$ForceLoadContentObserver;->this$0:Landroid/content/Loader;

    #@2
    invoke-virtual {v0}, Landroid/content/Loader;->onContentChanged()V

    #@5
    .line 82
    return-void
.end method
