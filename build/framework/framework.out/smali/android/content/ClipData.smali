.class public Landroid/content/ClipData;
.super Ljava/lang/Object;
.source "ClipData.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/ClipData$Item;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/content/ClipData;",
            ">;"
        }
    .end annotation
.end field

.field static final MIMETYPES_TEXT_HTML:[Ljava/lang/String;

.field static final MIMETYPES_TEXT_INTENT:[Ljava/lang/String;

.field static final MIMETYPES_TEXT_PLAIN:[Ljava/lang/String;

.field static final MIMETYPES_TEXT_URILIST:[Ljava/lang/String;


# instance fields
.field final mClipDescription:Landroid/content/ClipDescription;

.field final mIcon:Landroid/graphics/Bitmap;

.field final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ClipData$Item;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 150
    new-array v0, v3, [Ljava/lang/String;

    #@4
    const-string/jumbo v1, "text/plain"

    #@7
    aput-object v1, v0, v2

    #@9
    sput-object v0, Landroid/content/ClipData;->MIMETYPES_TEXT_PLAIN:[Ljava/lang/String;

    #@b
    .line 152
    new-array v0, v3, [Ljava/lang/String;

    #@d
    const-string/jumbo v1, "text/html"

    #@10
    aput-object v1, v0, v2

    #@12
    sput-object v0, Landroid/content/ClipData;->MIMETYPES_TEXT_HTML:[Ljava/lang/String;

    #@14
    .line 154
    new-array v0, v3, [Ljava/lang/String;

    #@16
    const-string/jumbo v1, "text/uri-list"

    #@19
    aput-object v1, v0, v2

    #@1b
    sput-object v0, Landroid/content/ClipData;->MIMETYPES_TEXT_URILIST:[Ljava/lang/String;

    #@1d
    .line 156
    new-array v0, v3, [Ljava/lang/String;

    #@1f
    const-string/jumbo v1, "text/vnd.android.intent"

    #@22
    aput-object v1, v0, v2

    #@24
    sput-object v0, Landroid/content/ClipData;->MIMETYPES_TEXT_INTENT:[Ljava/lang/String;

    #@26
    .line 886
    new-instance v0, Landroid/content/ClipData$1;

    #@28
    invoke-direct {v0}, Landroid/content/ClipData$1;-><init>()V

    #@2b
    sput-object v0, Landroid/content/ClipData;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2d
    return-void
.end method

.method public constructor <init>(Landroid/content/ClipData;)V
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 651
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 652
    iget-object v0, p1, Landroid/content/ClipData;->mClipDescription:Landroid/content/ClipDescription;

    #@5
    iput-object v0, p0, Landroid/content/ClipData;->mClipDescription:Landroid/content/ClipDescription;

    #@7
    .line 653
    iget-object v0, p1, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@9
    iput-object v0, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@b
    .line 654
    new-instance v0, Ljava/util/ArrayList;

    #@d
    iget-object v1, p1, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@f
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@12
    iput-object v0, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@14
    .line 655
    return-void
.end method

.method public constructor <init>(Landroid/content/ClipDescription;Landroid/content/ClipData$Item;)V
    .registers 5
    .parameter "description"
    .parameter "item"

    #@0
    .prologue
    .line 635
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 636
    iput-object p1, p0, Landroid/content/ClipData;->mClipDescription:Landroid/content/ClipDescription;

    #@5
    .line 637
    if-nez p2, :cond_10

    #@7
    .line 638
    new-instance v0, Ljava/lang/NullPointerException;

    #@9
    const-string/jumbo v1, "item is null"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 640
    :cond_10
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@13
    .line 641
    new-instance v0, Ljava/util/ArrayList;

    #@15
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@18
    iput-object v0, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@1a
    .line 642
    iget-object v0, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1f
    .line 643
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 11
    .parameter "in"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 868
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 869
    new-instance v6, Landroid/content/ClipDescription;

    #@6
    invoke-direct {v6, p1}, Landroid/content/ClipDescription;-><init>(Landroid/os/Parcel;)V

    #@9
    iput-object v6, p0, Landroid/content/ClipData;->mClipDescription:Landroid/content/ClipDescription;

    #@b
    .line 870
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@e
    move-result v6

    #@f
    if-eqz v6, :cond_60

    #@11
    .line 871
    sget-object v6, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@13
    invoke-interface {v6, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@16
    move-result-object v6

    #@17
    check-cast v6, Landroid/graphics/Bitmap;

    #@19
    iput-object v6, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@1b
    .line 875
    :goto_1b
    new-instance v6, Ljava/util/ArrayList;

    #@1d
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@20
    iput-object v6, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@22
    .line 876
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v0

    #@26
    .line 877
    .local v0, N:I
    const/4 v2, 0x0

    #@27
    .local v2, i:I
    :goto_27
    if-ge v2, v0, :cond_67

    #@29
    .line 878
    sget-object v6, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@2b
    invoke-interface {v6, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2e
    move-result-object v4

    #@2f
    check-cast v4, Ljava/lang/CharSequence;

    #@31
    .line 879
    .local v4, text:Ljava/lang/CharSequence;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    .line 880
    .local v1, htmlText:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@38
    move-result v6

    #@39
    if-eqz v6, :cond_63

    #@3b
    sget-object v6, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3d
    invoke-interface {v6, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@40
    move-result-object v6

    #@41
    check-cast v6, Landroid/content/Intent;

    #@43
    move-object v3, v6

    #@44
    .line 881
    .local v3, intent:Landroid/content/Intent;
    :goto_44
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v6

    #@48
    if-eqz v6, :cond_65

    #@4a
    sget-object v6, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4c
    invoke-interface {v6, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4f
    move-result-object v6

    #@50
    check-cast v6, Landroid/net/Uri;

    #@52
    move-object v5, v6

    #@53
    .line 882
    .local v5, uri:Landroid/net/Uri;
    :goto_53
    iget-object v6, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@55
    new-instance v8, Landroid/content/ClipData$Item;

    #@57
    invoke-direct {v8, v4, v1, v3, v5}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/net/Uri;)V

    #@5a
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5d
    .line 877
    add-int/lit8 v2, v2, 0x1

    #@5f
    goto :goto_27

    #@60
    .line 873
    .end local v0           #N:I
    .end local v1           #htmlText:Ljava/lang/String;
    .end local v2           #i:I
    .end local v3           #intent:Landroid/content/Intent;
    .end local v4           #text:Ljava/lang/CharSequence;
    .end local v5           #uri:Landroid/net/Uri;
    :cond_60
    iput-object v7, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@62
    goto :goto_1b

    #@63
    .restart local v0       #N:I
    .restart local v1       #htmlText:Ljava/lang/String;
    .restart local v2       #i:I
    .restart local v4       #text:Ljava/lang/CharSequence;
    :cond_63
    move-object v3, v7

    #@64
    .line 880
    goto :goto_44

    #@65
    .restart local v3       #intent:Landroid/content/Intent;
    :cond_65
    move-object v5, v7

    #@66
    .line 881
    goto :goto_53

    #@67
    .line 884
    .end local v1           #htmlText:Ljava/lang/String;
    .end local v3           #intent:Landroid/content/Intent;
    .end local v4           #text:Ljava/lang/CharSequence;
    :cond_67
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V
    .registers 6
    .parameter "label"
    .parameter "mimeTypes"
    .parameter "item"

    #@0
    .prologue
    .line 619
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 620
    new-instance v0, Landroid/content/ClipDescription;

    #@5
    invoke-direct {v0, p1, p2}, Landroid/content/ClipDescription;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;)V

    #@8
    iput-object v0, p0, Landroid/content/ClipData;->mClipDescription:Landroid/content/ClipDescription;

    #@a
    .line 621
    if-nez p3, :cond_15

    #@c
    .line 622
    new-instance v0, Ljava/lang/NullPointerException;

    #@e
    const-string/jumbo v1, "item is null"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 624
    :cond_15
    const/4 v0, 0x0

    #@16
    iput-object v0, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@18
    .line 625
    new-instance v0, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1d
    iput-object v0, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@1f
    .line 626
    iget-object v0, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@24
    .line 627
    return-void
.end method

.method public static newHtmlText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/content/ClipData;
    .registers 6
    .parameter "label"
    .parameter "text"
    .parameter "htmlText"

    #@0
    .prologue
    .line 682
    new-instance v0, Landroid/content/ClipData$Item;

    #@2
    invoke-direct {v0, p1, p2}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    #@5
    .line 683
    .local v0, item:Landroid/content/ClipData$Item;
    new-instance v1, Landroid/content/ClipData;

    #@7
    sget-object v2, Landroid/content/ClipData;->MIMETYPES_TEXT_HTML:[Ljava/lang/String;

    #@9
    invoke-direct {v1, p0, v2, v0}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@c
    return-object v1
.end method

.method public static newIntent(Ljava/lang/CharSequence;Landroid/content/Intent;)Landroid/content/ClipData;
    .registers 5
    .parameter "label"
    .parameter "intent"

    #@0
    .prologue
    .line 695
    new-instance v0, Landroid/content/ClipData$Item;

    #@2
    invoke-direct {v0, p1}, Landroid/content/ClipData$Item;-><init>(Landroid/content/Intent;)V

    #@5
    .line 696
    .local v0, item:Landroid/content/ClipData$Item;
    new-instance v1, Landroid/content/ClipData;

    #@7
    sget-object v2, Landroid/content/ClipData;->MIMETYPES_TEXT_INTENT:[Ljava/lang/String;

    #@9
    invoke-direct {v1, p0, v2, v0}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@c
    return-object v1
.end method

.method public static newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;
    .registers 5
    .parameter "label"
    .parameter "text"

    #@0
    .prologue
    .line 666
    new-instance v0, Landroid/content/ClipData$Item;

    #@2
    invoke-direct {v0, p1}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    #@5
    .line 667
    .local v0, item:Landroid/content/ClipData$Item;
    new-instance v1, Landroid/content/ClipData;

    #@7
    sget-object v2, Landroid/content/ClipData;->MIMETYPES_TEXT_PLAIN:[Ljava/lang/String;

    #@9
    invoke-direct {v1, p0, v2, v0}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@c
    return-object v1
.end method

.method public static newRawUri(Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;
    .registers 5
    .parameter "label"
    .parameter "uri"

    #@0
    .prologue
    .line 751
    new-instance v0, Landroid/content/ClipData$Item;

    #@2
    invoke-direct {v0, p1}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    #@5
    .line 752
    .local v0, item:Landroid/content/ClipData$Item;
    new-instance v1, Landroid/content/ClipData;

    #@7
    sget-object v2, Landroid/content/ClipData;->MIMETYPES_TEXT_URILIST:[Ljava/lang/String;

    #@9
    invoke-direct {v1, p0, v2, v0}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@c
    return-object v1
.end method

.method public static newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;
    .registers 13
    .parameter "resolver"
    .parameter "label"
    .parameter "uri"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 712
    new-instance v1, Landroid/content/ClipData$Item;

    #@5
    invoke-direct {v1, p2}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    #@8
    .line 713
    .local v1, item:Landroid/content/ClipData$Item;
    const/4 v2, 0x0

    #@9
    .line 714
    .local v2, mimeTypes:[Ljava/lang/String;
    const-string v7, "content"

    #@b
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@e
    move-result-object v8

    #@f
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v7

    #@13
    if-eqz v7, :cond_2c

    #@15
    .line 715
    invoke-virtual {p0, p2}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    .line 716
    .local v3, realType:Ljava/lang/String;
    const-string v7, "*/*"

    #@1b
    invoke-virtual {p0, p2, v7}, Landroid/content/ContentResolver;->getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    .line 717
    if-nez v2, :cond_36

    #@21
    .line 718
    if-eqz v3, :cond_2c

    #@23
    .line 719
    new-array v2, v5, [Ljava/lang/String;

    #@25
    .end local v2           #mimeTypes:[Ljava/lang/String;
    aput-object v3, v2, v9

    #@27
    const-string/jumbo v5, "text/uri-list"

    #@2a
    aput-object v5, v2, v6

    #@2c
    .line 733
    .end local v3           #realType:Ljava/lang/String;
    .restart local v2       #mimeTypes:[Ljava/lang/String;
    :cond_2c
    :goto_2c
    if-nez v2, :cond_30

    #@2e
    .line 734
    sget-object v2, Landroid/content/ClipData;->MIMETYPES_TEXT_URILIST:[Ljava/lang/String;

    #@30
    .line 736
    :cond_30
    new-instance v5, Landroid/content/ClipData;

    #@32
    invoke-direct {v5, p1, v2, v1}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    #@35
    return-object v5

    #@36
    .line 722
    .restart local v3       #realType:Ljava/lang/String;
    :cond_36
    array-length v7, v2

    #@37
    if-eqz v3, :cond_50

    #@39
    :goto_39
    add-int/2addr v5, v7

    #@3a
    new-array v4, v5, [Ljava/lang/String;

    #@3c
    .line 723
    .local v4, tmp:[Ljava/lang/String;
    const/4 v0, 0x0

    #@3d
    .line 724
    .local v0, i:I
    if-eqz v3, :cond_43

    #@3f
    .line 725
    aput-object v3, v4, v9

    #@41
    .line 726
    add-int/lit8 v0, v0, 0x1

    #@43
    .line 728
    :cond_43
    array-length v5, v2

    #@44
    invoke-static {v2, v9, v4, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@47
    .line 729
    array-length v5, v2

    #@48
    add-int/2addr v5, v0

    #@49
    const-string/jumbo v6, "text/uri-list"

    #@4c
    aput-object v6, v4, v5

    #@4e
    .line 730
    move-object v2, v4

    #@4f
    goto :goto_2c

    #@50
    .end local v0           #i:I
    .end local v4           #tmp:[Ljava/lang/String;
    :cond_50
    move v5, v6

    #@51
    .line 722
    goto :goto_39
.end method


# virtual methods
.method public addItem(Landroid/content/ClipData$Item;)V
    .registers 4
    .parameter "item"

    #@0
    .prologue
    .line 767
    if-nez p1, :cond_b

    #@2
    .line 768
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "item is null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 770
    :cond_b
    iget-object v0, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 771
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 835
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getDescription()Landroid/content/ClipDescription;
    .registers 2

    #@0
    .prologue
    .line 760
    iget-object v0, p0, Landroid/content/ClipData;->mClipDescription:Landroid/content/ClipDescription;

    #@2
    return-object v0
.end method

.method public getIcon()Landroid/graphics/Bitmap;
    .registers 2

    #@0
    .prologue
    .line 775
    iget-object v0, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@2
    return-object v0
.end method

.method public getItemAt(I)Landroid/content/ClipData$Item;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 790
    iget-object v0, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/content/ClipData$Item;

    #@8
    return-object v0
.end method

.method public getItemCount()I
    .registers 2

    #@0
    .prologue
    .line 782
    iget-object v0, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public toShortString(Ljava/lang/StringBuilder;)V
    .registers 6
    .parameter "b"

    #@0
    .prologue
    const/16 v3, 0x20

    #@2
    .line 807
    iget-object v2, p0, Landroid/content/ClipData;->mClipDescription:Landroid/content/ClipDescription;

    #@4
    if-eqz v2, :cond_5e

    #@6
    .line 808
    iget-object v2, p0, Landroid/content/ClipData;->mClipDescription:Landroid/content/ClipDescription;

    #@8
    invoke-virtual {v2, p1}, Landroid/content/ClipDescription;->toShortString(Ljava/lang/StringBuilder;)Z

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_5c

    #@e
    const/4 v0, 0x1

    #@f
    .line 812
    .local v0, first:Z
    :goto_f
    iget-object v2, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@11
    if-eqz v2, :cond_35

    #@13
    .line 813
    if-nez v0, :cond_18

    #@15
    .line 814
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    .line 816
    :cond_18
    const/4 v0, 0x0

    #@19
    .line 817
    const-string v2, "I:"

    #@1b
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 818
    iget-object v2, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@20
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    #@23
    move-result v2

    #@24
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    .line 819
    const/16 v2, 0x78

    #@29
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2c
    .line 820
    iget-object v2, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@2e
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    #@31
    move-result v2

    #@32
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    .line 822
    :cond_35
    const/4 v1, 0x0

    #@36
    .local v1, i:I
    :goto_36
    iget-object v2, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v2

    #@3c
    if-ge v1, v2, :cond_60

    #@3e
    .line 823
    if-nez v0, :cond_43

    #@40
    .line 824
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@43
    .line 826
    :cond_43
    const/4 v0, 0x0

    #@44
    .line 827
    const/16 v2, 0x7b

    #@46
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@49
    .line 828
    iget-object v2, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@4b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4e
    move-result-object v2

    #@4f
    check-cast v2, Landroid/content/ClipData$Item;

    #@51
    invoke-virtual {v2, p1}, Landroid/content/ClipData$Item;->toShortString(Ljava/lang/StringBuilder;)V

    #@54
    .line 829
    const/16 v2, 0x7d

    #@56
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@59
    .line 822
    add-int/lit8 v1, v1, 0x1

    #@5b
    goto :goto_36

    #@5c
    .line 808
    .end local v0           #first:Z
    .end local v1           #i:I
    :cond_5c
    const/4 v0, 0x0

    #@5d
    goto :goto_f

    #@5e
    .line 810
    :cond_5e
    const/4 v0, 0x1

    #@5f
    .restart local v0       #first:Z
    goto :goto_f

    #@60
    .line 831
    .restart local v1       #i:I
    :cond_60
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 795
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 797
    .local v0, b:Ljava/lang/StringBuilder;
    const-string v1, "ClipData { "

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 798
    invoke-virtual {p0, v0}, Landroid/content/ClipData;->toShortString(Ljava/lang/StringBuilder;)V

    #@f
    .line 799
    const-string v1, " }"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 801
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 9
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 840
    iget-object v3, p0, Landroid/content/ClipData;->mClipDescription:Landroid/content/ClipDescription;

    #@4
    invoke-virtual {v3, p1, p2}, Landroid/content/ClipDescription;->writeToParcel(Landroid/os/Parcel;I)V

    #@7
    .line 841
    iget-object v3, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@9
    if-eqz v3, :cond_4c

    #@b
    .line 842
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 843
    iget-object v3, p0, Landroid/content/ClipData;->mIcon:Landroid/graphics/Bitmap;

    #@10
    invoke-virtual {v3, p1, p2}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 847
    :goto_13
    iget-object v3, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v0

    #@19
    .line 848
    .local v0, N:I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 849
    const/4 v1, 0x0

    #@1d
    .local v1, i:I
    :goto_1d
    if-ge v1, v0, :cond_58

    #@1f
    .line 850
    iget-object v3, p0, Landroid/content/ClipData;->mItems:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v2

    #@25
    check-cast v2, Landroid/content/ClipData$Item;

    #@27
    .line 851
    .local v2, item:Landroid/content/ClipData$Item;
    iget-object v3, v2, Landroid/content/ClipData$Item;->mText:Ljava/lang/CharSequence;

    #@29
    invoke-static {v3, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@2c
    .line 852
    iget-object v3, v2, Landroid/content/ClipData$Item;->mHtmlText:Ljava/lang/String;

    #@2e
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@31
    .line 853
    iget-object v3, v2, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@33
    if-eqz v3, :cond_50

    #@35
    .line 854
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 855
    iget-object v3, v2, Landroid/content/ClipData$Item;->mIntent:Landroid/content/Intent;

    #@3a
    invoke-virtual {v3, p1, p2}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@3d
    .line 859
    :goto_3d
    iget-object v3, v2, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@3f
    if-eqz v3, :cond_54

    #@41
    .line 860
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@44
    .line 861
    iget-object v3, v2, Landroid/content/ClipData$Item;->mUri:Landroid/net/Uri;

    #@46
    invoke-virtual {v3, p1, p2}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@49
    .line 849
    :goto_49
    add-int/lit8 v1, v1, 0x1

    #@4b
    goto :goto_1d

    #@4c
    .line 845
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #item:Landroid/content/ClipData$Item;
    :cond_4c
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@4f
    goto :goto_13

    #@50
    .line 857
    .restart local v0       #N:I
    .restart local v1       #i:I
    .restart local v2       #item:Landroid/content/ClipData$Item;
    :cond_50
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@53
    goto :goto_3d

    #@54
    .line 863
    :cond_54
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@57
    goto :goto_49

    #@58
    .line 866
    .end local v2           #item:Landroid/content/ClipData$Item;
    :cond_58
    return-void
.end method
