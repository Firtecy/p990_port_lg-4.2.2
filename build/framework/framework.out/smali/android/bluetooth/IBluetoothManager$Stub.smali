.class public abstract Landroid/bluetooth/IBluetoothManager$Stub;
.super Landroid/os/Binder;
.source "IBluetoothManager.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/IBluetoothManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.bluetooth.IBluetoothManager"

.field static final TRANSACTION_disable:I = 0x8

.field static final TRANSACTION_disableRadio:I = 0xb

.field static final TRANSACTION_enable:I = 0x6

.field static final TRANSACTION_enableNoAutoConnect:I = 0x7

.field static final TRANSACTION_enableRadio:I = 0xa

.field static final TRANSACTION_getAddress:I = 0xc

.field static final TRANSACTION_getName:I = 0xd

.field static final TRANSACTION_isEnabled:I = 0x5

.field static final TRANSACTION_isRadioEnabled:I = 0x9

.field static final TRANSACTION_registerAdapter:I = 0x1

.field static final TRANSACTION_registerStateChangeCallback:I = 0x3

.field static final TRANSACTION_unregisterAdapter:I = 0x2

.field static final TRANSACTION_unregisterStateChangeCallback:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.bluetooth.IBluetoothManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/bluetooth/IBluetoothManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.bluetooth.IBluetoothManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/bluetooth/IBluetoothManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/bluetooth/IBluetoothManager;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/bluetooth/IBluetoothManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/bluetooth/IBluetoothManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_122

    #@5
    .line 162
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v3

    #@9
    :goto_9
    return v3

    #@a
    .line 47
    :sswitch_a
    const-string v2, "android.bluetooth.IBluetoothManager"

    #@c
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v2, "android.bluetooth.IBluetoothManager"

    #@12
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@18
    move-result-object v2

    #@19
    invoke-static {v2}, Landroid/bluetooth/IBluetoothManagerCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManagerCallback;

    #@1c
    move-result-object v0

    #@1d
    .line 55
    .local v0, _arg0:Landroid/bluetooth/IBluetoothManagerCallback;
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothManager$Stub;->registerAdapter(Landroid/bluetooth/IBluetoothManagerCallback;)Landroid/bluetooth/IBluetooth;

    #@20
    move-result-object v1

    #@21
    .line 56
    .local v1, _result:Landroid/bluetooth/IBluetooth;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24
    .line 57
    if-eqz v1, :cond_2e

    #@26
    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->asBinder()Landroid/os/IBinder;

    #@29
    move-result-object v2

    #@2a
    :goto_2a
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@2d
    goto :goto_9

    #@2e
    :cond_2e
    const/4 v2, 0x0

    #@2f
    goto :goto_2a

    #@30
    .line 62
    .end local v0           #_arg0:Landroid/bluetooth/IBluetoothManagerCallback;
    .end local v1           #_result:Landroid/bluetooth/IBluetooth;
    :sswitch_30
    const-string v2, "android.bluetooth.IBluetoothManager"

    #@32
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 64
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@38
    move-result-object v2

    #@39
    invoke-static {v2}, Landroid/bluetooth/IBluetoothManagerCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManagerCallback;

    #@3c
    move-result-object v0

    #@3d
    .line 65
    .restart local v0       #_arg0:Landroid/bluetooth/IBluetoothManagerCallback;
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothManager$Stub;->unregisterAdapter(Landroid/bluetooth/IBluetoothManagerCallback;)V

    #@40
    .line 66
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@43
    goto :goto_9

    #@44
    .line 71
    .end local v0           #_arg0:Landroid/bluetooth/IBluetoothManagerCallback;
    :sswitch_44
    const-string v2, "android.bluetooth.IBluetoothManager"

    #@46
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@49
    .line 73
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4c
    move-result-object v2

    #@4d
    invoke-static {v2}, Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@50
    move-result-object v0

    #@51
    .line 74
    .local v0, _arg0:Landroid/bluetooth/IBluetoothStateChangeCallback;
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothManager$Stub;->registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V

    #@54
    .line 75
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@57
    goto :goto_9

    #@58
    .line 80
    .end local v0           #_arg0:Landroid/bluetooth/IBluetoothStateChangeCallback;
    :sswitch_58
    const-string v2, "android.bluetooth.IBluetoothManager"

    #@5a
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5d
    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@60
    move-result-object v2

    #@61
    invoke-static {v2}, Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@64
    move-result-object v0

    #@65
    .line 83
    .restart local v0       #_arg0:Landroid/bluetooth/IBluetoothStateChangeCallback;
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothManager$Stub;->unregisterStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V

    #@68
    .line 84
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6b
    goto :goto_9

    #@6c
    .line 89
    .end local v0           #_arg0:Landroid/bluetooth/IBluetoothStateChangeCallback;
    :sswitch_6c
    const-string v4, "android.bluetooth.IBluetoothManager"

    #@6e
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@71
    .line 90
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothManager$Stub;->isEnabled()Z

    #@74
    move-result v1

    #@75
    .line 91
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@78
    .line 92
    if-eqz v1, :cond_7b

    #@7a
    move v2, v3

    #@7b
    :cond_7b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@7e
    goto :goto_9

    #@7f
    .line 97
    .end local v1           #_result:Z
    :sswitch_7f
    const-string v4, "android.bluetooth.IBluetoothManager"

    #@81
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@84
    .line 98
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothManager$Stub;->enable()Z

    #@87
    move-result v1

    #@88
    .line 99
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8b
    .line 100
    if-eqz v1, :cond_8e

    #@8d
    move v2, v3

    #@8e
    :cond_8e
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@91
    goto/16 :goto_9

    #@93
    .line 105
    .end local v1           #_result:Z
    :sswitch_93
    const-string v4, "android.bluetooth.IBluetoothManager"

    #@95
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@98
    .line 106
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothManager$Stub;->enableNoAutoConnect()Z

    #@9b
    move-result v1

    #@9c
    .line 107
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9f
    .line 108
    if-eqz v1, :cond_a2

    #@a1
    move v2, v3

    #@a2
    :cond_a2
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@a5
    goto/16 :goto_9

    #@a7
    .line 113
    .end local v1           #_result:Z
    :sswitch_a7
    const-string v4, "android.bluetooth.IBluetoothManager"

    #@a9
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ac
    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@af
    move-result v4

    #@b0
    if-eqz v4, :cond_c2

    #@b2
    move v0, v3

    #@b3
    .line 116
    .local v0, _arg0:Z
    :goto_b3
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothManager$Stub;->disable(Z)Z

    #@b6
    move-result v1

    #@b7
    .line 117
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ba
    .line 118
    if-eqz v1, :cond_bd

    #@bc
    move v2, v3

    #@bd
    :cond_bd
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@c0
    goto/16 :goto_9

    #@c2
    .end local v0           #_arg0:Z
    .end local v1           #_result:Z
    :cond_c2
    move v0, v2

    #@c3
    .line 115
    goto :goto_b3

    #@c4
    .line 123
    :sswitch_c4
    const-string v4, "android.bluetooth.IBluetoothManager"

    #@c6
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c9
    .line 124
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothManager$Stub;->isRadioEnabled()Z

    #@cc
    move-result v1

    #@cd
    .line 125
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d0
    .line 126
    if-eqz v1, :cond_d3

    #@d2
    move v2, v3

    #@d3
    :cond_d3
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@d6
    goto/16 :goto_9

    #@d8
    .line 131
    .end local v1           #_result:Z
    :sswitch_d8
    const-string v4, "android.bluetooth.IBluetoothManager"

    #@da
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dd
    .line 132
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothManager$Stub;->enableRadio()Z

    #@e0
    move-result v1

    #@e1
    .line 133
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e4
    .line 134
    if-eqz v1, :cond_e7

    #@e6
    move v2, v3

    #@e7
    :cond_e7
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@ea
    goto/16 :goto_9

    #@ec
    .line 139
    .end local v1           #_result:Z
    :sswitch_ec
    const-string v4, "android.bluetooth.IBluetoothManager"

    #@ee
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f1
    .line 140
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothManager$Stub;->disableRadio()Z

    #@f4
    move-result v1

    #@f5
    .line 141
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f8
    .line 142
    if-eqz v1, :cond_fb

    #@fa
    move v2, v3

    #@fb
    :cond_fb
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@fe
    goto/16 :goto_9

    #@100
    .line 147
    .end local v1           #_result:Z
    :sswitch_100
    const-string v2, "android.bluetooth.IBluetoothManager"

    #@102
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@105
    .line 148
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothManager$Stub;->getAddress()Ljava/lang/String;

    #@108
    move-result-object v1

    #@109
    .line 149
    .local v1, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@10c
    .line 150
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10f
    goto/16 :goto_9

    #@111
    .line 155
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_111
    const-string v2, "android.bluetooth.IBluetoothManager"

    #@113
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@116
    .line 156
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothManager$Stub;->getName()Ljava/lang/String;

    #@119
    move-result-object v1

    #@11a
    .line 157
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11d
    .line 158
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@120
    goto/16 :goto_9

    #@122
    .line 43
    :sswitch_data_122
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_30
        0x3 -> :sswitch_44
        0x4 -> :sswitch_58
        0x5 -> :sswitch_6c
        0x6 -> :sswitch_7f
        0x7 -> :sswitch_93
        0x8 -> :sswitch_a7
        0x9 -> :sswitch_c4
        0xa -> :sswitch_d8
        0xb -> :sswitch_ec
        0xc -> :sswitch_100
        0xd -> :sswitch_111
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
