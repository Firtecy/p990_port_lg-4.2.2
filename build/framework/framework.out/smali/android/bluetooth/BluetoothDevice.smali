.class public final Landroid/bluetooth/BluetoothDevice;
.super Ljava/lang/Object;
.source "BluetoothDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ACTION_ACL_CONNECTED:Ljava/lang/String; = "android.bluetooth.device.action.ACL_CONNECTED"

.field public static final ACTION_ACL_DISCONNECTED:Ljava/lang/String; = "android.bluetooth.device.action.ACL_DISCONNECTED"

.field public static final ACTION_ACL_DISCONNECT_REQUESTED:Ljava/lang/String; = "android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED"

.field public static final ACTION_ALIAS_CHANGED:Ljava/lang/String; = "android.bluetooth.device.action.ALIAS_CHANGED"

.field public static final ACTION_BOND_STATE_CHANGED:Ljava/lang/String; = "android.bluetooth.device.action.BOND_STATE_CHANGED"

.field public static final ACTION_CLASS_CHANGED:Ljava/lang/String; = "android.bluetooth.device.action.CLASS_CHANGED"

.field public static final ACTION_CONNECTION_ACCESS_CANCEL:Ljava/lang/String; = "android.bluetooth.device.action.CONNECTION_ACCESS_CANCEL"

.field public static final ACTION_CONNECTION_ACCESS_REPLY:Ljava/lang/String; = "android.bluetooth.device.action.CONNECTION_ACCESS_REPLY"

.field public static final ACTION_CONNECTION_ACCESS_REQUEST:Ljava/lang/String; = "android.bluetooth.device.action.CONNECTION_ACCESS_REQUEST"

.field public static final ACTION_DISAPPEARED:Ljava/lang/String; = "android.bluetooth.device.action.DISAPPEARED"

.field public static final ACTION_FOUND:Ljava/lang/String; = "android.bluetooth.device.action.FOUND"

.field public static final ACTION_NAME_CHANGED:Ljava/lang/String; = "android.bluetooth.device.action.NAME_CHANGED"

.field public static final ACTION_NAME_FAILED:Ljava/lang/String; = "android.bluetooth.device.action.NAME_FAILED"

.field public static final ACTION_PAIRING_CANCEL:Ljava/lang/String; = "android.bluetooth.device.action.PAIRING_CANCEL"

.field public static final ACTION_PAIRING_REQUEST:Ljava/lang/String; = "android.bluetooth.device.action.PAIRING_REQUEST"

.field public static final ACTION_UUID:Ljava/lang/String; = "android.bluetooth.device.action.UUID"

.field public static final BOND_BONDED:I = 0xc

.field public static final BOND_BONDING:I = 0xb

.field public static final BOND_NONE:I = 0xa

.field public static final BOND_SUCCESS:I = 0x0

.field public static final CONNECTION_ACCESS_NO:I = 0x2

.field public static final CONNECTION_ACCESS_YES:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = true

.field public static final ERROR:I = -0x80000000

.field public static final EXTRA_ACCESS_REQUEST_TYPE:Ljava/lang/String; = "android.bluetooth.device.extra.ACCESS_REQUEST_TYPE"

.field public static final EXTRA_ALWAYS_ALLOWED:Ljava/lang/String; = "android.bluetooth.device.extra.ALWAYS_ALLOWED"

.field public static final EXTRA_BOND_STATE:Ljava/lang/String; = "android.bluetooth.device.extra.BOND_STATE"

.field public static final EXTRA_CLASS:Ljava/lang/String; = "android.bluetooth.device.extra.CLASS"

.field public static final EXTRA_CLASS_NAME:Ljava/lang/String; = "android.bluetooth.device.extra.CLASS_NAME"

.field public static final EXTRA_CONNECTION_ACCESS_RESULT:Ljava/lang/String; = "android.bluetooth.device.extra.CONNECTION_ACCESS_RESULT"

.field public static final EXTRA_DEVICE:Ljava/lang/String; = "android.bluetooth.device.extra.DEVICE"

.field public static final EXTRA_NAME:Ljava/lang/String; = "android.bluetooth.device.extra.NAME"

.field public static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "android.bluetooth.device.extra.PACKAGE_NAME"

.field public static final EXTRA_PAIRING_KEY:Ljava/lang/String; = "android.bluetooth.device.extra.PAIRING_KEY"

.field public static final EXTRA_PAIRING_VARIANT:Ljava/lang/String; = "android.bluetooth.device.extra.PAIRING_VARIANT"

.field public static final EXTRA_PREVIOUS_BOND_STATE:Ljava/lang/String; = "android.bluetooth.device.extra.PREVIOUS_BOND_STATE"

.field public static final EXTRA_REASON:Ljava/lang/String; = "android.bluetooth.device.extra.REASON"

.field public static final EXTRA_RSSI:Ljava/lang/String; = "android.bluetooth.device.extra.RSSI"

.field public static final EXTRA_UUID:Ljava/lang/String; = "android.bluetooth.device.extra.UUID"

.field public static final PAIRING_VARIANT_CONSENT:I = 0x3

.field public static final PAIRING_VARIANT_DISPLAY_PASSKEY:I = 0x4

.field public static final PAIRING_VARIANT_DISPLAY_PIN:I = 0x5

.field public static final PAIRING_VARIANT_OOB_CONSENT:I = 0x6

.field public static final PAIRING_VARIANT_PASSKEY:I = 0x1

.field public static final PAIRING_VARIANT_PASSKEY_CONFIRMATION:I = 0x2

.field public static final PAIRING_VARIANT_PIN:I = 0x0

.field public static final REQUEST_TYPE_PHONEBOOK_ACCESS:I = 0x2

.field public static final REQUEST_TYPE_PROFILE_CONNECTION:I = 0x1

.field private static final TAG:Ljava/lang/String; = "BluetoothDevice"

.field public static final UNBOND_REASON_AUTH_CANCELED:I = 0x3

.field public static final UNBOND_REASON_AUTH_FAILED:I = 0x1

.field public static final UNBOND_REASON_AUTH_REJECTED:I = 0x2

.field public static final UNBOND_REASON_AUTH_TIMEOUT:I = 0x6

.field public static final UNBOND_REASON_DISCOVERY_IN_PROGRESS:I = 0x5

.field public static final UNBOND_REASON_REMOTE_AUTH_CANCELED:I = 0x8

.field public static final UNBOND_REASON_REMOTE_DEVICE_DOWN:I = 0x4

.field public static final UNBOND_REASON_REMOVED:I = 0x9

.field public static final UNBOND_REASON_REPEATED_ATTEMPTS:I = 0x7

.field static mStateChangeCallback:Landroid/bluetooth/IBluetoothManagerCallback;

.field private static sService:Landroid/bluetooth/IBluetooth;


# instance fields
.field private final mAddress:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 495
    new-instance v0, Landroid/bluetooth/BluetoothDevice$1;

    #@2
    invoke-direct {v0}, Landroid/bluetooth/BluetoothDevice$1;-><init>()V

    #@5
    sput-object v0, Landroid/bluetooth/BluetoothDevice;->mStateChangeCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@7
    .line 559
    new-instance v0, Landroid/bluetooth/BluetoothDevice$2;

    #@9
    invoke-direct {v0}, Landroid/bluetooth/BluetoothDevice$2;-><init>()V

    #@c
    sput-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .registers 5
    .parameter "address"

    #@0
    .prologue
    .line 520
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 521
    invoke-static {}, Landroid/bluetooth/BluetoothDevice;->getService()Landroid/bluetooth/IBluetooth;

    #@6
    .line 522
    invoke-static {p1}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_25

    #@c
    .line 523
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " is not a valid Bluetooth address"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 526
    :cond_25
    iput-object p1, p0, Landroid/bluetooth/BluetoothDevice;->mAddress:Ljava/lang/String;

    #@27
    .line 527
    return-void
.end method

.method static synthetic access$002(Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 67
    sput-object p0, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@2
    return-object p0
.end method

.method public static convertPinToBytes(Ljava/lang/String;)[B
    .registers 6
    .parameter "pin"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1147
    if-nez p0, :cond_5

    #@3
    move-object v0, v2

    #@4
    .line 1160
    :cond_4
    :goto_4
    return-object v0

    #@5
    .line 1152
    :cond_5
    :try_start_5
    const-string v3, "UTF-8"

    #@7
    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_a} :catch_15

    #@a
    move-result-object v0

    #@b
    .line 1157
    .local v0, pinBytes:[B
    array-length v3, v0

    #@c
    if-lez v3, :cond_13

    #@e
    array-length v3, v0

    #@f
    const/16 v4, 0x10

    #@11
    if-le v3, v4, :cond_4

    #@13
    :cond_13
    move-object v0, v2

    #@14
    .line 1158
    goto :goto_4

    #@15
    .line 1153
    .end local v0           #pinBytes:[B
    :catch_15
    move-exception v1

    #@16
    .line 1154
    .local v1, uee:Ljava/io/UnsupportedEncodingException;
    const-string v3, "BluetoothDevice"

    #@18
    const-string v4, "UTF-8 not supported?!?"

    #@1a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    move-object v0, v2

    #@1e
    .line 1155
    goto :goto_4
.end method

.method static getService()Landroid/bluetooth/IBluetooth;
    .registers 3

    #@0
    .prologue
    .line 486
    const-class v2, Landroid/bluetooth/BluetoothDevice;

    #@2
    monitor-enter v2

    #@3
    .line 487
    :try_start_3
    sget-object v1, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@5
    if-nez v1, :cond_13

    #@7
    .line 488
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@a
    move-result-object v0

    #@b
    .line 489
    .local v0, adapter:Landroid/bluetooth/BluetoothAdapter;
    sget-object v1, Landroid/bluetooth/BluetoothDevice;->mStateChangeCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@d
    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothService(Landroid/bluetooth/IBluetoothManagerCallback;)Landroid/bluetooth/IBluetooth;

    #@10
    move-result-object v1

    #@11
    sput-object v1, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@13
    .line 491
    :cond_13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_17

    #@14
    .line 492
    sget-object v1, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@16
    return-object v1

    #@17
    .line 491
    :catchall_17
    move-exception v1

    #@18
    :try_start_18
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_18 .. :try_end_19} :catchall_17

    #@19
    throw v1
.end method


# virtual methods
.method public cancelBondProcess()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 741
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v2, :cond_d

    #@5
    .line 742
    const-string v2, "BluetoothDevice"

    #@7
    const-string v3, "BT not enabled. Cannot cancel Remote Device bond"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 748
    :goto_c
    return v1

    #@d
    .line 746
    :cond_d
    :try_start_d
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-interface {v2, p0}, Landroid/bluetooth/IBluetooth;->cancelBondProcess(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result v1

    #@13
    goto :goto_c

    #@14
    .line 747
    :catch_14
    move-exception v0

    #@15
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@17
    const-string v3, ""

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_c
.end method

.method public cancelPairingUserInput()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 985
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v2, :cond_d

    #@5
    .line 986
    const-string v2, "BluetoothDevice"

    #@7
    const-string v3, "BT not enabled. Cannot create pairing user input"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 992
    :goto_c
    return v1

    #@d
    .line 990
    :cond_d
    :try_start_d
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-interface {v2, p0}, Landroid/bluetooth/IBluetooth;->cancelBondProcess(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result v1

    #@13
    goto :goto_c

    #@14
    .line 991
    :catch_14
    move-exception v0

    #@15
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@17
    const-string v3, ""

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_c
.end method

.method public createBond()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 673
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v2, :cond_d

    #@5
    .line 674
    const-string v2, "BluetoothDevice"

    #@7
    const-string v3, "BT not enabled. Cannot create bond to Remote Device"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 680
    :goto_c
    return v1

    #@d
    .line 678
    :cond_d
    :try_start_d
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-interface {v2, p0}, Landroid/bluetooth/IBluetooth;->createBond(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result v1

    #@13
    goto :goto_c

    #@14
    .line 679
    :catch_14
    move-exception v0

    #@15
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@17
    const-string v3, ""

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_c
.end method

.method public createBondOutOfBand([B[B)Z
    .registers 4
    .parameter "hash"
    .parameter "randomizer"

    #@0
    .prologue
    .line 708
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public createInsecureRfcommSocket(I)Landroid/bluetooth/BluetoothSocket;
    .registers 10
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1119
    new-instance v0, Landroid/bluetooth/BluetoothSocket;

    #@3
    const/4 v1, 0x1

    #@4
    const/4 v2, -0x1

    #@5
    const/4 v7, 0x0

    #@6
    move v4, v3

    #@7
    move-object v5, p0

    #@8
    move v6, p1

    #@9
    invoke-direct/range {v0 .. v7}, Landroid/bluetooth/BluetoothSocket;-><init>(IIZZLandroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;)V

    #@c
    return-object v0
.end method

.method public createInsecureRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;
    .registers 10
    .parameter "uuid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 1100
    new-instance v0, Landroid/bluetooth/BluetoothSocket;

    #@4
    const/4 v1, 0x1

    #@5
    new-instance v7, Landroid/os/ParcelUuid;

    #@7
    invoke-direct {v7, p1}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@a
    move v4, v3

    #@b
    move-object v5, p0

    #@c
    move v6, v2

    #@d
    invoke-direct/range {v0 .. v7}, Landroid/bluetooth/BluetoothSocket;-><init>(IIZZLandroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;)V

    #@10
    return-object v0
.end method

.method public createRfcommSocket(I)Landroid/bluetooth/BluetoothSocket;
    .registers 10
    .parameter "channel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1031
    new-instance v0, Landroid/bluetooth/BluetoothSocket;

    #@3
    const/4 v2, -0x1

    #@4
    const/4 v7, 0x0

    #@5
    move v3, v1

    #@6
    move v4, v1

    #@7
    move-object v5, p0

    #@8
    move v6, p1

    #@9
    invoke-direct/range {v0 .. v7}, Landroid/bluetooth/BluetoothSocket;-><init>(IIZZLandroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;)V

    #@c
    return-object v0
.end method

.method public createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;
    .registers 10
    .parameter "uuid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v1, 0x1

    #@2
    .line 1067
    new-instance v0, Landroid/bluetooth/BluetoothSocket;

    #@4
    new-instance v7, Landroid/os/ParcelUuid;

    #@6
    invoke-direct {v7, p1}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@9
    move v3, v1

    #@a
    move v4, v1

    #@b
    move-object v5, p0

    #@c
    move v6, v2

    #@d
    invoke-direct/range {v0 .. v7}, Landroid/bluetooth/BluetoothSocket;-><init>(IIZZLandroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;)V

    #@10
    return-object v0
.end method

.method public createScoSocket()Landroid/bluetooth/BluetoothSocket;
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, -0x1

    #@2
    .line 1134
    new-instance v0, Landroid/bluetooth/BluetoothSocket;

    #@4
    const/4 v1, 0x2

    #@5
    const/4 v7, 0x0

    #@6
    move v4, v3

    #@7
    move-object v5, p0

    #@8
    move v6, v2

    #@9
    invoke-direct/range {v0 .. v7}, Landroid/bluetooth/BluetoothSocket;-><init>(IIZZLandroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;)V

    #@c
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 556
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "o"

    #@0
    .prologue
    .line 531
    instance-of v0, p1, Landroid/bluetooth/BluetoothDevice;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 532
    iget-object v0, p0, Landroid/bluetooth/BluetoothDevice;->mAddress:Ljava/lang/String;

    #@6
    check-cast p1, Landroid/bluetooth/BluetoothDevice;

    #@8
    .end local p1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    .line 534
    :goto_10
    return v0

    #@11
    .restart local p1
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public fetchUuidsWithSdp()Z
    .registers 4

    #@0
    .prologue
    .line 924
    :try_start_0
    sget-object v1, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@2
    invoke-interface {v1, p0}, Landroid/bluetooth/IBluetooth;->fetchRemoteUuids(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 926
    :goto_6
    return v1

    #@7
    .line 925
    :catch_7
    move-exception v0

    #@8
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothDevice"

    #@a
    const-string v2, ""

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 926
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public getAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 580
    iget-object v0, p0, Landroid/bluetooth/BluetoothDevice;->mAddress:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getAlias()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 612
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v2, :cond_d

    #@5
    .line 613
    const-string v2, "BluetoothDevice"

    #@7
    const-string v3, "BT not enabled. Cannot get Remote Device Alias"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 619
    :goto_c
    return-object v1

    #@d
    .line 617
    :cond_d
    :try_start_d
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-interface {v2, p0}, Landroid/bluetooth/IBluetooth;->getRemoteAlias(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result-object v1

    #@13
    goto :goto_c

    #@14
    .line 618
    :catch_14
    move-exception v0

    #@15
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@17
    const-string v3, ""

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_c
.end method

.method public getAliasName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 653
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 654
    .local v0, name:Ljava/lang/String;
    if-nez v0, :cond_a

    #@6
    .line 655
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 657
    :cond_a
    return-object v0
.end method

.method public getBluetoothClass()Landroid/bluetooth/BluetoothClass;
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 806
    sget-object v3, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v3, :cond_d

    #@5
    .line 807
    const-string v3, "BluetoothDevice"

    #@7
    const-string v4, "BT not enabled. Cannot get Bluetooth Class"

    #@9
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 815
    :cond_c
    :goto_c
    return-object v2

    #@d
    .line 811
    :cond_d
    :try_start_d
    sget-object v3, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-interface {v3, p0}, Landroid/bluetooth/IBluetooth;->getRemoteClass(Landroid/bluetooth/BluetoothDevice;)I

    #@12
    move-result v0

    #@13
    .line 812
    .local v0, classInt:I
    const/high16 v3, -0x100

    #@15
    if-eq v0, v3, :cond_c

    #@17
    .line 813
    new-instance v3, Landroid/bluetooth/BluetoothClass;

    #@19
    invoke-direct {v3, v0}, Landroid/bluetooth/BluetoothClass;-><init>(I)V
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_1c} :catch_1e

    #@1c
    move-object v2, v3

    #@1d
    goto :goto_c

    #@1e
    .line 814
    .end local v0           #classInt:I
    :catch_1e
    move-exception v1

    #@1f
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "BluetoothDevice"

    #@21
    const-string v4, ""

    #@23
    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    goto :goto_c
.end method

.method public getBondState()I
    .registers 7

    #@0
    .prologue
    const/16 v2, 0xa

    #@2
    .line 783
    sget-object v3, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@4
    if-nez v3, :cond_e

    #@6
    .line 784
    const-string v3, "BluetoothDevice"

    #@8
    const-string v4, "BT not enabled. Cannot get bond state"

    #@a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 796
    :goto_d
    return v2

    #@e
    .line 788
    :cond_e
    :try_start_e
    sget-object v3, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@10
    invoke-interface {v3, p0}, Landroid/bluetooth/IBluetooth;->getBondState(Landroid/bluetooth/BluetoothDevice;)I
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_13} :catch_15
    .catch Ljava/lang/NullPointerException; {:try_start_e .. :try_end_13} :catch_1e

    #@13
    move-result v2

    #@14
    goto :goto_d

    #@15
    .line 789
    :catch_15
    move-exception v0

    #@16
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "BluetoothDevice"

    #@18
    const-string v4, ""

    #@1a
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d
    goto :goto_d

    #@1e
    .line 790
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_1e
    move-exception v1

    #@1f
    .line 793
    .local v1, npe:Ljava/lang/NullPointerException;
    const-string v3, "BluetoothDevice"

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "NullPointerException for getBondState() of device ("

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    const-string v5, ")"

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@41
    goto :goto_d
.end method

.method public getJustWorks()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 871
    :try_start_1
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-eqz v2, :cond_c

    #@5
    .line 872
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@7
    invoke-interface {v2, p0}, Landroid/bluetooth/IBluetooth;->getJustWorks(Landroid/bluetooth/BluetoothDevice;)Z

    #@a
    move-result v1

    #@b
    .line 880
    :goto_b
    return v1

    #@c
    .line 874
    :cond_c
    const-string v2, "BluetoothDevice"

    #@e
    const-string/jumbo v3, "sService is null"

    #@11
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_14} :catch_15

    #@14
    goto :goto_b

    #@15
    .line 877
    :catch_15
    move-exception v0

    #@16
    .line 878
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@18
    const-string v3, ""

    #@1a
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d
    goto :goto_b
.end method

.method public getName()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 594
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v2, :cond_d

    #@5
    .line 595
    const-string v2, "BluetoothDevice"

    #@7
    const-string v3, "BT not enabled. Cannot get Remote Device name"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 601
    :goto_c
    return-object v1

    #@d
    .line 599
    :cond_d
    :try_start_d
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-interface {v2, p0}, Landroid/bluetooth/IBluetooth;->getRemoteName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result-object v1

    #@13
    goto :goto_c

    #@14
    .line 600
    :catch_14
    move-exception v0

    #@15
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@17
    const-string v3, ""

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_c
.end method

.method public getServiceChannel(Landroid/os/ParcelUuid;)I
    .registers 3
    .parameter "uuid"

    #@0
    .prologue
    .line 936
    const/high16 v0, -0x8000

    #@2
    return v0
.end method

.method public getTrustState()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 828
    :try_start_1
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-eqz v2, :cond_c

    #@5
    .line 829
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@7
    invoke-interface {v2, p0}, Landroid/bluetooth/IBluetooth;->getTrustState(Landroid/bluetooth/BluetoothDevice;)Z

    #@a
    move-result v1

    #@b
    .line 842
    :goto_b
    return v1

    #@c
    .line 831
    :cond_c
    const-string v2, "BluetoothDevice"

    #@e
    const-string/jumbo v3, "sService is null"

    #@11
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_14} :catch_15

    #@14
    goto :goto_b

    #@15
    .line 838
    :catch_15
    move-exception v0

    #@16
    .line 839
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@18
    const-string v3, ""

    #@1a
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d
    goto :goto_b
.end method

.method public getUuids()[Landroid/os/ParcelUuid;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 897
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v2, :cond_d

    #@5
    .line 898
    const-string v2, "BluetoothDevice"

    #@7
    const-string v3, "BT not enabled. Cannot get remote device Uuids"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 904
    :goto_c
    return-object v1

    #@d
    .line 902
    :cond_d
    :try_start_d
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-interface {v2, p0}, Landroid/bluetooth/IBluetooth;->getRemoteUuids(Landroid/bluetooth/BluetoothDevice;)[Landroid/os/ParcelUuid;
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result-object v1

    #@13
    goto :goto_c

    #@14
    .line 903
    :catch_14
    move-exception v0

    #@15
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@17
    const-string v3, ""

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_c
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 539
    iget-object v0, p0, Landroid/bluetooth/BluetoothDevice;->mAddress:Ljava/lang/String;

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isBluetoothDock()Z
    .registers 2

    #@0
    .prologue
    .line 1002
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public removeBond()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 762
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v2, :cond_d

    #@5
    .line 763
    const-string v2, "BluetoothDevice"

    #@7
    const-string v3, "BT not enabled. Cannot remove Remote Device bond"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 769
    :goto_c
    return v1

    #@d
    .line 767
    :cond_d
    :try_start_d
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-interface {v2, p0}, Landroid/bluetooth/IBluetooth;->removeBond(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result v1

    #@13
    goto :goto_c

    #@14
    .line 768
    :catch_14
    move-exception v0

    #@15
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@17
    const-string v3, ""

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_c
.end method

.method public setAlias(Ljava/lang/String;)Z
    .registers 6
    .parameter "alias"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 633
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v2, :cond_d

    #@5
    .line 634
    const-string v2, "BluetoothDevice"

    #@7
    const-string v3, "BT not enabled. Cannot set Remote Device name"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 640
    :goto_c
    return v1

    #@d
    .line 638
    :cond_d
    :try_start_d
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-interface {v2, p0, p1}, Landroid/bluetooth/IBluetooth;->setRemoteAlias(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result v1

    #@13
    goto :goto_c

    #@14
    .line 639
    :catch_14
    move-exception v0

    #@15
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@17
    const-string v3, ""

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_c
.end method

.method public setDeviceOutOfBandData([B[B)Z
    .registers 4
    .parameter "hash"
    .parameter "randomizer"

    #@0
    .prologue
    .line 730
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setPairingConfirmation(Z)Z
    .registers 6
    .parameter "confirm"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 963
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v2, :cond_d

    #@5
    .line 964
    const-string v2, "BluetoothDevice"

    #@7
    const-string v3, "BT not enabled. Cannot set pairing confirmation"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 970
    :goto_c
    return v1

    #@d
    .line 968
    :cond_d
    :try_start_d
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    invoke-interface {v2, p0, p1}, Landroid/bluetooth/IBluetooth;->setPairingConfirmation(Landroid/bluetooth/BluetoothDevice;Z)Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result v1

    #@13
    goto :goto_c

    #@14
    .line 969
    :catch_14
    move-exception v0

    #@15
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@17
    const-string v3, ""

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_c
.end method

.method public setPasskey(I)Z
    .registers 3
    .parameter "passkey"

    #@0
    .prologue
    .line 958
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setPin([B)Z
    .registers 7
    .parameter "pin"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 941
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@3
    if-nez v2, :cond_d

    #@5
    .line 942
    const-string v2, "BluetoothDevice"

    #@7
    const-string v3, "BT not enabled. Cannot set Remote Device pin"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 948
    :goto_c
    return v1

    #@d
    .line 946
    :cond_d
    :try_start_d
    sget-object v2, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@f
    const/4 v3, 0x1

    #@10
    array-length v4, p1

    #@11
    invoke-interface {v2, p0, v3, v4, p1}, Landroid/bluetooth/IBluetooth;->setPin(Landroid/bluetooth/BluetoothDevice;ZI[B)Z
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_14} :catch_16

    #@14
    move-result v1

    #@15
    goto :goto_c

    #@16
    .line 947
    :catch_16
    move-exception v0

    #@17
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothDevice"

    #@19
    const-string v3, ""

    #@1b
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    goto :goto_c
.end method

.method public setRemoteOutOfBandData()Z
    .registers 2

    #@0
    .prologue
    .line 980
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setTrust(Z)Z
    .registers 5
    .parameter "value"

    #@0
    .prologue
    .line 855
    :try_start_0
    sget-object v1, Landroid/bluetooth/BluetoothDevice;->sService:Landroid/bluetooth/IBluetooth;

    #@2
    invoke-interface {v1, p0, p1}, Landroid/bluetooth/IBluetooth;->setTrustState(Landroid/bluetooth/BluetoothDevice;Z)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 860
    :goto_6
    return v1

    #@7
    .line 856
    :catch_7
    move-exception v0

    #@8
    .line 857
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothDevice"

    #@a
    const-string v2, ""

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 860
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 552
    iget-object v0, p0, Landroid/bluetooth/BluetoothDevice;->mAddress:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 570
    iget-object v0, p0, Landroid/bluetooth/BluetoothDevice;->mAddress:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 571
    return-void
.end method
