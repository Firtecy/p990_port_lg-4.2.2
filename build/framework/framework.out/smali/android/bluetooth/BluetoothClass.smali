.class public final Landroid/bluetooth/BluetoothClass;
.super Ljava/lang/Object;
.source "BluetoothClass.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/BluetoothClass$Device;,
        Landroid/bluetooth/BluetoothClass$Service;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/bluetooth/BluetoothClass;",
            ">;"
        }
    .end annotation
.end field

.field public static final ERROR:I = -0x1000000

.field public static final PROFILE_A2DP:I = 0x1

.field public static final PROFILE_HEADSET:I = 0x0

.field public static final PROFILE_HID:I = 0x3

.field public static final PROFILE_NAP:I = 0x5

.field public static final PROFILE_OPP:I = 0x2

.field public static final PROFILE_PANU:I = 0x4


# instance fields
.field private final mClass:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 87
    new-instance v0, Landroid/bluetooth/BluetoothClass$1;

    #@2
    invoke-direct {v0}, Landroid/bluetooth/BluetoothClass$1;-><init>()V

    #@5
    sput-object v0, Landroid/bluetooth/BluetoothClass;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(I)V
    .registers 2
    .parameter "classInt"

    #@0
    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 62
    iput p1, p0, Landroid/bluetooth/BluetoothClass;->mClass:I

    #@5
    .line 63
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 84
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public doesClassMatch(I)Z
    .registers 6
    .parameter "profile"

    #@0
    .prologue
    const/high16 v2, 0x4

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v0, 0x1

    #@4
    .line 297
    if-ne p1, v0, :cond_16

    #@6
    .line 298
    invoke-virtual {p0, v2}, Landroid/bluetooth/BluetoothClass;->hasService(I)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_d

    #@c
    .line 360
    :cond_c
    :goto_c
    :sswitch_c
    return v0

    #@d
    .line 304
    :cond_d
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    #@10
    move-result v2

    #@11
    sparse-switch v2, :sswitch_data_66

    #@14
    move v0, v1

    #@15
    .line 311
    goto :goto_c

    #@16
    .line 313
    :cond_16
    if-nez p1, :cond_27

    #@18
    .line 316
    invoke-virtual {p0, v2}, Landroid/bluetooth/BluetoothClass;->hasService(I)Z

    #@1b
    move-result v2

    #@1c
    if-nez v2, :cond_c

    #@1e
    .line 320
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    #@21
    move-result v2

    #@22
    sparse-switch v2, :sswitch_data_78

    #@25
    move v0, v1

    #@26
    .line 326
    goto :goto_c

    #@27
    .line 328
    :cond_27
    const/4 v2, 0x2

    #@28
    if-ne p1, v2, :cond_3b

    #@2a
    .line 329
    const/high16 v2, 0x10

    #@2c
    invoke-virtual {p0, v2}, Landroid/bluetooth/BluetoothClass;->hasService(I)Z

    #@2f
    move-result v2

    #@30
    if-nez v2, :cond_c

    #@32
    .line 333
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    #@35
    move-result v2

    #@36
    sparse-switch v2, :sswitch_data_86

    #@39
    move v0, v1

    #@3a
    .line 349
    goto :goto_c

    #@3b
    .line 351
    :cond_3b
    const/4 v2, 0x3

    #@3c
    if-ne p1, v2, :cond_4a

    #@3e
    .line 352
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    #@41
    move-result v2

    #@42
    and-int/lit16 v2, v2, 0x500

    #@44
    const/16 v3, 0x500

    #@46
    if-eq v2, v3, :cond_c

    #@48
    move v0, v1

    #@49
    goto :goto_c

    #@4a
    .line 353
    :cond_4a
    const/4 v2, 0x4

    #@4b
    if-eq p1, v2, :cond_50

    #@4d
    const/4 v2, 0x5

    #@4e
    if-ne p1, v2, :cond_64

    #@50
    .line 355
    :cond_50
    const/high16 v2, 0x2

    #@52
    invoke-virtual {p0, v2}, Landroid/bluetooth/BluetoothClass;->hasService(I)Z

    #@55
    move-result v2

    #@56
    if-nez v2, :cond_c

    #@58
    .line 358
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    #@5b
    move-result v2

    #@5c
    and-int/lit16 v2, v2, 0x300

    #@5e
    const/16 v3, 0x300

    #@60
    if-eq v2, v3, :cond_c

    #@62
    move v0, v1

    #@63
    goto :goto_c

    #@64
    :cond_64
    move v0, v1

    #@65
    .line 360
    goto :goto_c

    #@66
    .line 304
    :sswitch_data_66
    .sparse-switch
        0x414 -> :sswitch_c
        0x418 -> :sswitch_c
        0x420 -> :sswitch_c
        0x428 -> :sswitch_c
    .end sparse-switch

    #@78
    .line 320
    :sswitch_data_78
    .sparse-switch
        0x404 -> :sswitch_c
        0x408 -> :sswitch_c
        0x420 -> :sswitch_c
    .end sparse-switch

    #@86
    .line 333
    :sswitch_data_86
    .sparse-switch
        0x100 -> :sswitch_c
        0x104 -> :sswitch_c
        0x108 -> :sswitch_c
        0x10c -> :sswitch_c
        0x110 -> :sswitch_c
        0x114 -> :sswitch_c
        0x118 -> :sswitch_c
        0x200 -> :sswitch_c
        0x204 -> :sswitch_c
        0x208 -> :sswitch_c
        0x20c -> :sswitch_c
        0x210 -> :sswitch_c
        0x214 -> :sswitch_c
    .end sparse-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter "o"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 67
    instance-of v1, p1, Landroid/bluetooth/BluetoothClass;

    #@3
    if-eqz v1, :cond_e

    #@5
    .line 68
    iget v1, p0, Landroid/bluetooth/BluetoothClass;->mClass:I

    #@7
    check-cast p1, Landroid/bluetooth/BluetoothClass;

    #@9
    .end local p1
    iget v2, p1, Landroid/bluetooth/BluetoothClass;->mClass:I

    #@b
    if-ne v1, v2, :cond_e

    #@d
    const/4 v0, 0x1

    #@e
    .line 70
    :cond_e
    return v0
.end method

.method public getDeviceClass()I
    .registers 2

    #@0
    .prologue
    .line 271
    iget v0, p0, Landroid/bluetooth/BluetoothClass;->mClass:I

    #@2
    and-int/lit16 v0, v0, 0x1ffc

    #@4
    return v0
.end method

.method public getMajorDeviceClass()I
    .registers 2

    #@0
    .prologue
    .line 258
    iget v0, p0, Landroid/bluetooth/BluetoothClass;->mClass:I

    #@2
    and-int/lit16 v0, v0, 0x1f00

    #@4
    return v0
.end method

.method public hasService(I)Z
    .registers 4
    .parameter "service"

    #@0
    .prologue
    .line 130
    iget v0, p0, Landroid/bluetooth/BluetoothClass;->mClass:I

    #@2
    const v1, 0xffe000

    #@5
    and-int/2addr v0, v1

    #@6
    and-int/2addr v0, p1

    #@7
    if-eqz v0, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 75
    iget v0, p0, Landroid/bluetooth/BluetoothClass;->mClass:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 80
    iget v0, p0, Landroid/bluetooth/BluetoothClass;->mClass:I

    #@2
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 98
    iget v0, p0, Landroid/bluetooth/BluetoothClass;->mClass:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 99
    return-void
.end method
