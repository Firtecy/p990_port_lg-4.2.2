.class Landroid/bluetooth/BluetoothHealth$1;
.super Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;
.source "BluetoothHealth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/BluetoothHealth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/bluetooth/BluetoothHealth;


# direct methods
.method constructor <init>(Landroid/bluetooth/BluetoothHealth;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 103
    iput-object p1, p0, Landroid/bluetooth/BluetoothHealth$1;->this$0:Landroid/bluetooth/BluetoothHealth;

    #@2
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onBluetoothStateChange(Z)V
    .registers 8
    .parameter "up"

    #@0
    .prologue
    .line 105
    const-string v1, "BluetoothHealth"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "onBluetoothStateChange: up="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 106
    if-nez p1, :cond_4c

    #@1b
    .line 107
    const-string v1, "BluetoothHealth"

    #@1d
    const-string v2, "Unbinding service..."

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 108
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth$1;->this$0:Landroid/bluetooth/BluetoothHealth;

    #@24
    invoke-static {v1}, Landroid/bluetooth/BluetoothHealth;->access$000(Landroid/bluetooth/BluetoothHealth;)Landroid/content/ServiceConnection;

    #@27
    move-result-object v2

    #@28
    monitor-enter v2

    #@29
    .line 110
    :try_start_29
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth$1;->this$0:Landroid/bluetooth/BluetoothHealth;

    #@2b
    const/4 v3, 0x0

    #@2c
    invoke-static {v1, v3}, Landroid/bluetooth/BluetoothHealth;->access$102(Landroid/bluetooth/BluetoothHealth;Landroid/bluetooth/IBluetoothHealth;)Landroid/bluetooth/IBluetoothHealth;

    #@2f
    .line 111
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth$1;->this$0:Landroid/bluetooth/BluetoothHealth;

    #@31
    invoke-static {v1}, Landroid/bluetooth/BluetoothHealth;->access$200(Landroid/bluetooth/BluetoothHealth;)Landroid/content/Context;

    #@34
    move-result-object v1

    #@35
    iget-object v3, p0, Landroid/bluetooth/BluetoothHealth$1;->this$0:Landroid/bluetooth/BluetoothHealth;

    #@37
    invoke-static {v3}, Landroid/bluetooth/BluetoothHealth;->access$000(Landroid/bluetooth/BluetoothHealth;)Landroid/content/ServiceConnection;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v1, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_3e
    .catchall {:try_start_29 .. :try_end_3e} :catchall_49
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_3e} :catch_40

    #@3e
    .line 115
    :goto_3e
    :try_start_3e
    monitor-exit v2

    #@3f
    .line 130
    :goto_3f
    return-void

    #@40
    .line 112
    :catch_40
    move-exception v0

    #@41
    .line 113
    .local v0, re:Ljava/lang/Exception;
    const-string v1, "BluetoothHealth"

    #@43
    const-string v3, ""

    #@45
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    goto :goto_3e

    #@49
    .line 115
    .end local v0           #re:Ljava/lang/Exception;
    :catchall_49
    move-exception v1

    #@4a
    monitor-exit v2
    :try_end_4b
    .catchall {:try_start_3e .. :try_end_4b} :catchall_49

    #@4b
    throw v1

    #@4c
    .line 117
    :cond_4c
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth$1;->this$0:Landroid/bluetooth/BluetoothHealth;

    #@4e
    invoke-static {v1}, Landroid/bluetooth/BluetoothHealth;->access$000(Landroid/bluetooth/BluetoothHealth;)Landroid/content/ServiceConnection;

    #@51
    move-result-object v2

    #@52
    monitor-enter v2

    #@53
    .line 119
    :try_start_53
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth$1;->this$0:Landroid/bluetooth/BluetoothHealth;

    #@55
    invoke-static {v1}, Landroid/bluetooth/BluetoothHealth;->access$100(Landroid/bluetooth/BluetoothHealth;)Landroid/bluetooth/IBluetoothHealth;

    #@58
    move-result-object v1

    #@59
    if-nez v1, :cond_87

    #@5b
    .line 120
    const-string v1, "BluetoothHealth"

    #@5d
    const-string v3, "Binding service..."

    #@5f
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 121
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth$1;->this$0:Landroid/bluetooth/BluetoothHealth;

    #@64
    invoke-static {v1}, Landroid/bluetooth/BluetoothHealth;->access$200(Landroid/bluetooth/BluetoothHealth;)Landroid/content/Context;

    #@67
    move-result-object v1

    #@68
    new-instance v3, Landroid/content/Intent;

    #@6a
    const-class v4, Landroid/bluetooth/IBluetoothHealth;

    #@6c
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@6f
    move-result-object v4

    #@70
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@73
    iget-object v4, p0, Landroid/bluetooth/BluetoothHealth$1;->this$0:Landroid/bluetooth/BluetoothHealth;

    #@75
    invoke-static {v4}, Landroid/bluetooth/BluetoothHealth;->access$000(Landroid/bluetooth/BluetoothHealth;)Landroid/content/ServiceConnection;

    #@78
    move-result-object v4

    #@79
    const/4 v5, 0x0

    #@7a
    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@7d
    move-result v1

    #@7e
    if-nez v1, :cond_87

    #@80
    .line 122
    const-string v1, "BluetoothHealth"

    #@82
    const-string v3, "Could not bind to Bluetooth Health Service"

    #@84
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_87
    .catchall {:try_start_53 .. :try_end_87} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_87} :catch_8c

    #@87
    .line 128
    :cond_87
    :goto_87
    :try_start_87
    monitor-exit v2

    #@88
    goto :goto_3f

    #@89
    :catchall_89
    move-exception v1

    #@8a
    monitor-exit v2
    :try_end_8b
    .catchall {:try_start_87 .. :try_end_8b} :catchall_89

    #@8b
    throw v1

    #@8c
    .line 125
    :catch_8c
    move-exception v0

    #@8d
    .line 126
    .restart local v0       #re:Ljava/lang/Exception;
    :try_start_8d
    const-string v1, "BluetoothHealth"

    #@8f
    const-string v3, ""

    #@91
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_94
    .catchall {:try_start_8d .. :try_end_94} :catchall_89

    #@94
    goto :goto_87
.end method
