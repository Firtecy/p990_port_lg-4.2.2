.class Landroid/bluetooth/BluetoothAdapter$1;
.super Landroid/bluetooth/IBluetoothManagerCallback$Stub;
.source "BluetoothAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/BluetoothAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/bluetooth/BluetoothAdapter;


# direct methods
.method constructor <init>(Landroid/bluetooth/BluetoothAdapter;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1295
    iput-object p1, p0, Landroid/bluetooth/BluetoothAdapter$1;->this$0:Landroid/bluetooth/BluetoothAdapter;

    #@2
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothManagerCallback$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onBluetoothServiceDown()V
    .registers 7

    #@0
    .prologue
    .line 1313
    const-string v3, "BluetoothAdapter"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v5, "onBluetoothServiceDown: "

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    iget-object v5, p0, Landroid/bluetooth/BluetoothAdapter$1;->this$0:Landroid/bluetooth/BluetoothAdapter;

    #@10
    invoke-static {v5}, Landroid/bluetooth/BluetoothAdapter;->access$100(Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/IBluetooth;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1314
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter$1;->this$0:Landroid/bluetooth/BluetoothAdapter;

    #@21
    invoke-static {v3}, Landroid/bluetooth/BluetoothAdapter;->access$000(Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/IBluetoothManagerCallback;

    #@24
    move-result-object v4

    #@25
    monitor-enter v4

    #@26
    .line 1315
    :try_start_26
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter$1;->this$0:Landroid/bluetooth/BluetoothAdapter;

    #@28
    const/4 v5, 0x0

    #@29
    invoke-static {v3, v5}, Landroid/bluetooth/BluetoothAdapter;->access$102(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;

    #@2c
    .line 1316
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter$1;->this$0:Landroid/bluetooth/BluetoothAdapter;

    #@2e
    invoke-static {v3}, Landroid/bluetooth/BluetoothAdapter;->access$200(Landroid/bluetooth/BluetoothAdapter;)Ljava/util/ArrayList;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@35
    move-result-object v2

    #@36
    .local v2, i$:Ljava/util/Iterator;
    :goto_36
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@39
    move-result v3

    #@3a
    if-eqz v3, :cond_5d

    #@3c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3f
    move-result-object v0

    #@40
    check-cast v0, Landroid/bluetooth/IBluetoothManagerCallback;
    :try_end_42
    .catchall {:try_start_26 .. :try_end_42} :catchall_51

    #@42
    .line 1318
    .local v0, cb:Landroid/bluetooth/IBluetoothManagerCallback;
    if-eqz v0, :cond_54

    #@44
    .line 1319
    :try_start_44
    invoke-interface {v0}, Landroid/bluetooth/IBluetoothManagerCallback;->onBluetoothServiceDown()V
    :try_end_47
    .catchall {:try_start_44 .. :try_end_47} :catchall_51
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_47} :catch_48

    #@47
    goto :goto_36

    #@48
    .line 1323
    :catch_48
    move-exception v1

    #@49
    .local v1, e:Ljava/lang/Exception;
    :try_start_49
    const-string v3, "BluetoothAdapter"

    #@4b
    const-string v5, ""

    #@4d
    invoke-static {v3, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@50
    goto :goto_36

    #@51
    .line 1325
    .end local v0           #cb:Landroid/bluetooth/IBluetoothManagerCallback;
    .end local v1           #e:Ljava/lang/Exception;
    .end local v2           #i$:Ljava/util/Iterator;
    :catchall_51
    move-exception v3

    #@52
    monitor-exit v4
    :try_end_53
    .catchall {:try_start_49 .. :try_end_53} :catchall_51

    #@53
    throw v3

    #@54
    .line 1321
    .restart local v0       #cb:Landroid/bluetooth/IBluetoothManagerCallback;
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_54
    :try_start_54
    const-string v3, "BluetoothAdapter"

    #@56
    const-string/jumbo v5, "onBluetoothServiceDown: cb is null!!!"

    #@59
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5c
    .catchall {:try_start_54 .. :try_end_5c} :catchall_51
    .catch Ljava/lang/Exception; {:try_start_54 .. :try_end_5c} :catch_48

    #@5c
    goto :goto_36

    #@5d
    .line 1325
    .end local v0           #cb:Landroid/bluetooth/IBluetoothManagerCallback;
    :cond_5d
    :try_start_5d
    monitor-exit v4
    :try_end_5e
    .catchall {:try_start_5d .. :try_end_5e} :catchall_51

    #@5e
    .line 1326
    return-void
.end method

.method public onBluetoothServiceUp(Landroid/bluetooth/IBluetooth;)V
    .registers 8
    .parameter "bluetoothService"

    #@0
    .prologue
    .line 1297
    const-string v3, "BluetoothAdapter"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v5, "onBluetoothServiceUp: "

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v4

    #@16
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1298
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter$1;->this$0:Landroid/bluetooth/BluetoothAdapter;

    #@1b
    invoke-static {v3}, Landroid/bluetooth/BluetoothAdapter;->access$000(Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/IBluetoothManagerCallback;

    #@1e
    move-result-object v4

    #@1f
    monitor-enter v4

    #@20
    .line 1299
    :try_start_20
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter$1;->this$0:Landroid/bluetooth/BluetoothAdapter;

    #@22
    invoke-static {v3, p1}, Landroid/bluetooth/BluetoothAdapter;->access$102(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;

    #@25
    .line 1300
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter$1;->this$0:Landroid/bluetooth/BluetoothAdapter;

    #@27
    invoke-static {v3}, Landroid/bluetooth/BluetoothAdapter;->access$200(Landroid/bluetooth/BluetoothAdapter;)Ljava/util/ArrayList;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@2e
    move-result-object v2

    #@2f
    .local v2, i$:Ljava/util/Iterator;
    :goto_2f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_56

    #@35
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@38
    move-result-object v0

    #@39
    check-cast v0, Landroid/bluetooth/IBluetoothManagerCallback;
    :try_end_3b
    .catchall {:try_start_20 .. :try_end_3b} :catchall_4a

    #@3b
    .line 1302
    .local v0, cb:Landroid/bluetooth/IBluetoothManagerCallback;
    if-eqz v0, :cond_4d

    #@3d
    .line 1303
    :try_start_3d
    invoke-interface {v0, p1}, Landroid/bluetooth/IBluetoothManagerCallback;->onBluetoothServiceUp(Landroid/bluetooth/IBluetooth;)V
    :try_end_40
    .catchall {:try_start_3d .. :try_end_40} :catchall_4a
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_40} :catch_41

    #@40
    goto :goto_2f

    #@41
    .line 1307
    :catch_41
    move-exception v1

    #@42
    .local v1, e:Ljava/lang/Exception;
    :try_start_42
    const-string v3, "BluetoothAdapter"

    #@44
    const-string v5, ""

    #@46
    invoke-static {v3, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@49
    goto :goto_2f

    #@4a
    .line 1309
    .end local v0           #cb:Landroid/bluetooth/IBluetoothManagerCallback;
    .end local v1           #e:Ljava/lang/Exception;
    .end local v2           #i$:Ljava/util/Iterator;
    :catchall_4a
    move-exception v3

    #@4b
    monitor-exit v4
    :try_end_4c
    .catchall {:try_start_42 .. :try_end_4c} :catchall_4a

    #@4c
    throw v3

    #@4d
    .line 1305
    .restart local v0       #cb:Landroid/bluetooth/IBluetoothManagerCallback;
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_4d
    :try_start_4d
    const-string v3, "BluetoothAdapter"

    #@4f
    const-string/jumbo v5, "onBluetoothServiceUp: cb is null!!!"

    #@52
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_55
    .catchall {:try_start_4d .. :try_end_55} :catchall_4a
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_55} :catch_41

    #@55
    goto :goto_2f

    #@56
    .line 1309
    .end local v0           #cb:Landroid/bluetooth/IBluetoothManagerCallback;
    :cond_56
    :try_start_56
    monitor-exit v4
    :try_end_57
    .catchall {:try_start_56 .. :try_end_57} :catchall_4a

    #@57
    .line 1310
    return-void
.end method
