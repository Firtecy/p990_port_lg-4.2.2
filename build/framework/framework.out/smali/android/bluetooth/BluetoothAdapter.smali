.class public final Landroid/bluetooth/BluetoothAdapter;
.super Ljava/lang/Object;
.source "BluetoothAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/BluetoothAdapter$StateChangeCallbackWrapper;,
        Landroid/bluetooth/BluetoothAdapter$BluetoothStateChangeCallback;
    }
.end annotation


# static fields
.field public static final ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String; = "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

.field public static final ACTION_DISCOVERY_FINISHED:Ljava/lang/String; = "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

.field public static final ACTION_DISCOVERY_STARTED:Ljava/lang/String; = "android.bluetooth.adapter.action.DISCOVERY_STARTED"

.field public static final ACTION_LOCAL_NAME_CHANGED:Ljava/lang/String; = "android.bluetooth.adapter.action.LOCAL_NAME_CHANGED"

.field public static final ACTION_RADIO_STATE_CHANGED:Ljava/lang/String; = "android.bluetooth.adapter.action.RADIO_STATE_CHANGED"

.field public static final ACTION_REQUEST_DISCOVERABLE:Ljava/lang/String; = "android.bluetooth.adapter.action.REQUEST_DISCOVERABLE"

.field public static final ACTION_REQUEST_ENABLE:Ljava/lang/String; = "android.bluetooth.adapter.action.REQUEST_ENABLE"

.field public static final ACTION_SCAN_MODE_CHANGED:Ljava/lang/String; = "android.bluetooth.adapter.action.SCAN_MODE_CHANGED"

.field public static final ACTION_STATE_CHANGED:Ljava/lang/String; = "android.bluetooth.adapter.action.STATE_CHANGED"

.field private static final ADDRESS_LENGTH:I = 0x11

.field public static final BLUETOOTH_MANAGER_SERVICE:Ljava/lang/String; = "bluetooth_manager"

.field private static final DBG:Z = true

.field public static final ERROR:I = -0x80000000

.field public static final EXTRA_CONNECTION_STATE:Ljava/lang/String; = "android.bluetooth.adapter.extra.CONNECTION_STATE"

.field public static final EXTRA_DISCOVERABLE_DURATION:Ljava/lang/String; = "android.bluetooth.adapter.extra.DISCOVERABLE_DURATION"

.field public static final EXTRA_LOCAL_NAME:Ljava/lang/String; = "android.bluetooth.adapter.extra.LOCAL_NAME"

.field public static final EXTRA_PREVIOUS_CONNECTION_STATE:Ljava/lang/String; = "android.bluetooth.adapter.extra.PREVIOUS_CONNECTION_STATE"

.field public static final EXTRA_PREVIOUS_SCAN_MODE:Ljava/lang/String; = "android.bluetooth.adapter.extra.PREVIOUS_SCAN_MODE"

.field public static final EXTRA_PREVIOUS_STATE:Ljava/lang/String; = "android.bluetooth.adapter.extra.PREVIOUS_STATE"

.field public static final EXTRA_SCAN_MODE:Ljava/lang/String; = "android.bluetooth.adapter.extra.SCAN_MODE"

.field public static final EXTRA_STATE:Ljava/lang/String; = "android.bluetooth.adapter.extra.STATE"

.field public static final SCAN_MODE_CONNECTABLE:I = 0x15

.field public static final SCAN_MODE_CONNECTABLE_DISCOVERABLE:I = 0x17

.field public static final SCAN_MODE_NONE:I = 0x14

.field public static final STATE_CONNECTED:I = 0x2

.field public static final STATE_CONNECTING:I = 0x1

.field public static final STATE_DISCONNECTED:I = 0x0

.field public static final STATE_DISCONNECTING:I = 0x3

.field public static final STATE_OFF:I = 0xa

.field public static final STATE_ON:I = 0xc

.field public static final STATE_RADIO_OFF:I = 0xf

.field public static final STATE_RADIO_ON:I = 0xe

.field public static final STATE_TURNING_OFF:I = 0xd

.field public static final STATE_TURNING_ON:I = 0xb

.field private static final TAG:Ljava/lang/String; = "BluetoothAdapter"

.field private static final VDBG:Z = true

.field private static sAdapter:Landroid/bluetooth/BluetoothAdapter;


# instance fields
.field private final mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

.field private final mManagerService:Landroid/bluetooth/IBluetoothManager;

.field private mProxyServiceStateCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/IBluetoothManagerCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mService:Landroid/bluetooth/IBluetooth;

.field private mServiceRecordHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/bluetooth/IBluetoothManager;)V
    .registers 5
    .parameter "managerService"

    #@0
    .prologue
    .line 423
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1294
    new-instance v1, Landroid/bluetooth/BluetoothAdapter$1;

    #@5
    invoke-direct {v1, p0}, Landroid/bluetooth/BluetoothAdapter$1;-><init>(Landroid/bluetooth/BluetoothAdapter;)V

    #@8
    iput-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@a
    .line 1464
    new-instance v1, Ljava/util/ArrayList;

    #@c
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mProxyServiceStateCallbacks:Ljava/util/ArrayList;

    #@11
    .line 425
    if-nez p1, :cond_1b

    #@13
    .line 426
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@15
    const-string v2, "bluetooth manager service is null"

    #@17
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 429
    :cond_1b
    :try_start_1b
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@1d
    invoke-interface {p1, v1}, Landroid/bluetooth/IBluetoothManager;->registerAdapter(Landroid/bluetooth/IBluetoothManagerCallback;)Landroid/bluetooth/IBluetooth;

    #@20
    move-result-object v1

    #@21
    iput-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_23} :catch_29

    #@23
    .line 431
    :goto_23
    iput-object p1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@25
    .line 432
    const/4 v1, 0x0

    #@26
    iput-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mServiceRecordHandler:Landroid/os/Handler;

    #@28
    .line 433
    return-void

    #@29
    .line 430
    :catch_29
    move-exception v0

    #@2a
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@2c
    const-string v2, ""

    #@2e
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@31
    goto :goto_23
.end method

.method static synthetic access$000(Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/IBluetoothManagerCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/IBluetooth;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    iput-object p1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/bluetooth/BluetoothAdapter;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/bluetooth/BluetoothAdapter;->mProxyServiceStateCallbacks:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public static checkBluetoothAddress(Ljava/lang/String;)Z
    .registers 6
    .parameter "address"

    #@0
    .prologue
    const/16 v4, 0x11

    #@2
    const/4 v2, 0x0

    #@3
    .line 1437
    if-eqz p0, :cond_b

    #@5
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v3

    #@9
    if-eq v3, v4, :cond_c

    #@b
    .line 1457
    :cond_b
    :goto_b
    return v2

    #@c
    .line 1440
    :cond_c
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v4, :cond_31

    #@f
    .line 1441
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@12
    move-result v0

    #@13
    .line 1442
    .local v0, c:C
    rem-int/lit8 v3, v1, 0x3

    #@15
    packed-switch v3, :pswitch_data_34

    #@18
    .line 1440
    :cond_18
    :goto_18
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_d

    #@1b
    .line 1445
    :pswitch_1b
    const/16 v3, 0x30

    #@1d
    if-lt v0, v3, :cond_23

    #@1f
    const/16 v3, 0x39

    #@21
    if-le v0, v3, :cond_18

    #@23
    :cond_23
    const/16 v3, 0x41

    #@25
    if-lt v0, v3, :cond_b

    #@27
    const/16 v3, 0x46

    #@29
    if-gt v0, v3, :cond_b

    #@2b
    goto :goto_18

    #@2c
    .line 1451
    :pswitch_2c
    const/16 v3, 0x3a

    #@2e
    if-ne v0, v3, :cond_b

    #@30
    goto :goto_18

    #@31
    .line 1457
    .end local v0           #c:C
    :cond_31
    const/4 v2, 0x1

    #@32
    goto :goto_b

    #@33
    .line 1442
    nop

    #@34
    :pswitch_data_34
    .packed-switch 0x0
        :pswitch_1b
        :pswitch_1b
        :pswitch_2c
    .end packed-switch
.end method

.method private createNewRfcommSocketAndRecord(Ljava/lang/String;Ljava/util/UUID;ZZ)Landroid/bluetooth/BluetoothServerSocket;
    .registers 10
    .parameter "name"
    .parameter "uuid"
    .parameter "auth"
    .parameter "encrypt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1109
    new-instance v1, Landroid/bluetooth/BluetoothServerSocket;

    #@2
    const/4 v2, 0x1

    #@3
    new-instance v3, Landroid/os/ParcelUuid;

    #@5
    invoke-direct {v3, p2}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@8
    invoke-direct {v1, v2, p3, p4, v3}, Landroid/bluetooth/BluetoothServerSocket;-><init>(IZZLandroid/os/ParcelUuid;)V

    #@b
    .line 1111
    .local v1, socket:Landroid/bluetooth/BluetoothServerSocket;
    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothServerSocket;->setServiceName(Ljava/lang/String;)V

    #@e
    .line 1112
    iget-object v2, v1, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@10
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->bindListen()I

    #@13
    move-result v0

    #@14
    .line 1113
    .local v0, errno:I
    if-eqz v0, :cond_2f

    #@16
    .line 1117
    new-instance v2, Ljava/io/IOException;

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "Error: "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v2

    #@2f
    .line 1119
    :cond_2f
    return-object v1
.end method

.method public static declared-synchronized getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;
    .registers 5

    #@0
    .prologue
    .line 408
    const-class v3, Landroid/bluetooth/BluetoothAdapter;

    #@2
    monitor-enter v3

    #@3
    :try_start_3
    sget-object v2, Landroid/bluetooth/BluetoothAdapter;->sAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@5
    if-nez v2, :cond_1a

    #@7
    .line 409
    const-string v2, "bluetooth_manager"

    #@9
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 410
    .local v0, b:Landroid/os/IBinder;
    if-eqz v0, :cond_1e

    #@f
    .line 411
    invoke-static {v0}, Landroid/bluetooth/IBluetoothManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManager;

    #@12
    move-result-object v1

    #@13
    .line 412
    .local v1, managerService:Landroid/bluetooth/IBluetoothManager;
    new-instance v2, Landroid/bluetooth/BluetoothAdapter;

    #@15
    invoke-direct {v2, v1}, Landroid/bluetooth/BluetoothAdapter;-><init>(Landroid/bluetooth/IBluetoothManager;)V

    #@18
    sput-object v2, Landroid/bluetooth/BluetoothAdapter;->sAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@1a
    .line 417
    .end local v1           #managerService:Landroid/bluetooth/IBluetoothManager;
    :cond_1a
    :goto_1a
    sget-object v2, Landroid/bluetooth/BluetoothAdapter;->sAdapter:Landroid/bluetooth/BluetoothAdapter;
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_26

    #@1c
    monitor-exit v3

    #@1d
    return-object v2

    #@1e
    .line 414
    :cond_1e
    :try_start_1e
    const-string v2, "BluetoothAdapter"

    #@20
    const-string v4, "Bluetooth binder is null"

    #@22
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_25
    .catchall {:try_start_1e .. :try_end_25} :catchall_26

    #@25
    goto :goto_1a

    #@26
    .line 408
    :catchall_26
    move-exception v2

    #@27
    monitor-exit v3

    #@28
    throw v2
.end method

.method public static listenUsingScoOn()Landroid/bluetooth/BluetoothServerSocket;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1174
    new-instance v1, Landroid/bluetooth/BluetoothServerSocket;

    #@3
    const/4 v2, 0x2

    #@4
    const/4 v3, -0x1

    #@5
    invoke-direct {v1, v2, v4, v4, v3}, Landroid/bluetooth/BluetoothServerSocket;-><init>(IZZI)V

    #@8
    .line 1176
    .local v1, socket:Landroid/bluetooth/BluetoothServerSocket;
    iget-object v2, v1, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@a
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->bindListen()I

    #@d
    move-result v0

    #@e
    .line 1177
    .local v0, errno:I
    if-gez v0, :cond_10

    #@10
    .line 1182
    :cond_10
    return-object v1
.end method

.method private toDeviceSet([Landroid/bluetooth/BluetoothDevice;)Ljava/util/Set;
    .registers 4
    .parameter "devices"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/bluetooth/BluetoothDevice;",
            ")",
            "Ljava/util/Set",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1414
    new-instance v0, Ljava/util/HashSet;

    #@2
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@9
    .line 1415
    .local v0, deviceSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    #@c
    move-result-object v1

    #@d
    return-object v1
.end method


# virtual methods
.method public cancelDiscovery()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 870
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0xc

    #@7
    if-eq v2, v3, :cond_a

    #@9
    .line 876
    :goto_9
    return v1

    #@a
    .line 872
    :cond_a
    :try_start_a
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@c
    monitor-enter v3
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_d} :catch_1f

    #@d
    .line 873
    :try_start_d
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@f
    if-eqz v2, :cond_1a

    #@11
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@13
    invoke-interface {v2}, Landroid/bluetooth/IBluetooth;->cancelDiscovery()Z

    #@16
    move-result v2

    #@17
    monitor-exit v3

    #@18
    move v1, v2

    #@19
    goto :goto_9

    #@1a
    .line 874
    :cond_1a
    monitor-exit v3

    #@1b
    goto :goto_9

    #@1c
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_1c

    #@1e
    :try_start_1e
    throw v2
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1f} :catch_1f

    #@1f
    .line 875
    :catch_1f
    move-exception v0

    #@20
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@22
    const-string v3, ""

    #@24
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_9
.end method

.method public changeApplicationBluetoothState(ZLandroid/bluetooth/BluetoothAdapter$BluetoothStateChangeCallback;)Z
    .registers 4
    .parameter "on"
    .parameter "callback"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1376
    if-nez p2, :cond_3

    #@3
    .line 1386
    :cond_3
    return v0
.end method

.method public closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V
    .registers 8
    .parameter "profile"
    .parameter "proxy"

    #@0
    .prologue
    .line 1268
    if-nez p2, :cond_3

    #@2
    .line 1292
    :goto_2
    return-void

    #@3
    .line 1270
    :cond_3
    packed-switch p1, :pswitch_data_2a

    #@6
    goto :goto_2

    #@7
    :pswitch_7
    move-object v1, p2

    #@8
    .line 1272
    check-cast v1, Landroid/bluetooth/BluetoothHeadset;

    #@a
    .line 1273
    .local v1, headset:Landroid/bluetooth/BluetoothHeadset;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothHeadset;->close()V

    #@d
    goto :goto_2

    #@e
    .end local v1           #headset:Landroid/bluetooth/BluetoothHeadset;
    :pswitch_e
    move-object v0, p2

    #@f
    .line 1276
    check-cast v0, Landroid/bluetooth/BluetoothA2dp;

    #@11
    .line 1277
    .local v0, a2dp:Landroid/bluetooth/BluetoothA2dp;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothA2dp;->close()V

    #@14
    goto :goto_2

    #@15
    .end local v0           #a2dp:Landroid/bluetooth/BluetoothA2dp;
    :pswitch_15
    move-object v3, p2

    #@16
    .line 1280
    check-cast v3, Landroid/bluetooth/BluetoothInputDevice;

    #@18
    .line 1281
    .local v3, iDev:Landroid/bluetooth/BluetoothInputDevice;
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothInputDevice;->close()V

    #@1b
    goto :goto_2

    #@1c
    .end local v3           #iDev:Landroid/bluetooth/BluetoothInputDevice;
    :pswitch_1c
    move-object v4, p2

    #@1d
    .line 1284
    check-cast v4, Landroid/bluetooth/BluetoothPan;

    #@1f
    .line 1285
    .local v4, pan:Landroid/bluetooth/BluetoothPan;
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothPan;->close()V

    #@22
    goto :goto_2

    #@23
    .end local v4           #pan:Landroid/bluetooth/BluetoothPan;
    :pswitch_23
    move-object v2, p2

    #@24
    .line 1288
    check-cast v2, Landroid/bluetooth/BluetoothHealth;

    #@26
    .line 1289
    .local v2, health:Landroid/bluetooth/BluetoothHealth;
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothHealth;->close()V

    #@29
    goto :goto_2

    #@2a
    .line 1270
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_7
        :pswitch_e
        :pswitch_23
        :pswitch_15
        :pswitch_1c
    .end packed-switch
.end method

.method public disable()Z
    .registers 4

    #@0
    .prologue
    .line 612
    :try_start_0
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@2
    const/4 v2, 0x1

    #@3
    invoke-interface {v1, v2}, Landroid/bluetooth/IBluetoothManager;->disable(Z)Z
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_6} :catch_8

    #@6
    move-result v1

    #@7
    .line 614
    :goto_7
    return v1

    #@8
    .line 613
    :catch_8
    move-exception v0

    #@9
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@b
    const-string v2, ""

    #@d
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    .line 614
    const/4 v1, 0x0

    #@11
    goto :goto_7
.end method

.method public disable(Z)Z
    .registers 5
    .parameter "persist"

    #@0
    .prologue
    .line 644
    :try_start_0
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@2
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothManager;->disable(Z)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 646
    :goto_6
    return v1

    #@7
    .line 645
    :catch_7
    move-exception v0

    #@8
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@a
    const-string v2, ""

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 646
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public disableRadio()Z
    .registers 4

    #@0
    .prologue
    .line 625
    :try_start_0
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@2
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothManager;->disableRadio()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 627
    :goto_6
    return v1

    #@7
    .line 626
    :catch_7
    move-exception v0

    #@8
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@a
    const-string v2, ""

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 627
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public enable()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 561
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@4
    move-result v2

    #@5
    if-ne v2, v1, :cond_f

    #@7
    .line 562
    const-string v2, "BluetoothAdapter"

    #@9
    const-string v3, "enable(): BT is already enabled..!"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 568
    :goto_e
    return v1

    #@f
    .line 566
    :cond_f
    :try_start_f
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@11
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothManager;->enable()Z
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_16

    #@14
    move-result v1

    #@15
    goto :goto_e

    #@16
    .line 567
    :catch_16
    move-exception v0

    #@17
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@19
    const-string v2, ""

    #@1b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    .line 568
    const/4 v1, 0x0

    #@1f
    goto :goto_e
.end method

.method public enableNoAutoConnect()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1335
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@4
    move-result v2

    #@5
    if-ne v2, v1, :cond_f

    #@7
    .line 1336
    const-string v2, "BluetoothAdapter"

    #@9
    const-string v3, "enableNoAutoConnect(): BT is already enabled..!"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1342
    :goto_e
    return v1

    #@f
    .line 1340
    :cond_f
    :try_start_f
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@11
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothManager;->enableNoAutoConnect()Z
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_16

    #@14
    move-result v1

    #@15
    goto :goto_e

    #@16
    .line 1341
    :catch_16
    move-exception v0

    #@17
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@19
    const-string v2, ""

    #@1b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    .line 1342
    const/4 v1, 0x0

    #@1f
    goto :goto_e
.end method

.method public enableRadio()Z
    .registers 5

    #@0
    .prologue
    .line 578
    const/4 v1, 0x0

    #@1
    .line 580
    .local v1, enabled:Z
    :try_start_1
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@3
    invoke-interface {v2}, Landroid/bluetooth/IBluetoothManager;->enableRadio()Z
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_6} :catch_8

    #@6
    move-result v2

    #@7
    .line 582
    :goto_7
    return v2

    #@8
    .line 581
    :catch_8
    move-exception v0

    #@9
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@b
    const-string v3, ""

    #@d
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    .line 582
    const/4 v2, 0x0

    #@11
    goto :goto_7
.end method

.method protected finalize()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 1420
    :try_start_0
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@2
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@4
    invoke-interface {v1, v2}, Landroid/bluetooth/IBluetoothManager;->unregisterAdapter(Landroid/bluetooth/IBluetoothManagerCallback;)V
    :try_end_7
    .catchall {:try_start_0 .. :try_end_7} :catchall_14
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_b

    #@7
    .line 1424
    :goto_7
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@a
    .line 1426
    return-void

    #@b
    .line 1421
    :catch_b
    move-exception v0

    #@c
    .line 1422
    .local v0, e:Landroid/os/RemoteException;
    :try_start_c
    const-string v1, "BluetoothAdapter"

    #@e
    const-string v2, ""

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_13
    .catchall {:try_start_c .. :try_end_13} :catchall_14

    #@13
    goto :goto_7

    #@14
    .line 1424
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_14
    move-exception v1

    #@15
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@18
    throw v1
.end method

.method public getAddress()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 658
    :try_start_0
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@2
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothManager;->getAddress()Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 660
    :goto_6
    return-object v1

    #@7
    .line 659
    :catch_7
    move-exception v0

    #@8
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@a
    const-string v2, ""

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 660
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method getBluetoothManager()Landroid/bluetooth/IBluetoothManager;
    .registers 2

    #@0
    .prologue
    .line 1461
    iget-object v0, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@2
    return-object v0
.end method

.method getBluetoothService(Landroid/bluetooth/IBluetoothManagerCallback;)Landroid/bluetooth/IBluetooth;
    .registers 5
    .parameter "cb"

    #@0
    .prologue
    .line 1467
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@2
    monitor-enter v1

    #@3
    .line 1468
    if-nez p1, :cond_10

    #@5
    .line 1469
    :try_start_5
    const-string v0, "BluetoothAdapter"

    #@7
    const-string v2, "getBluetoothService() called with no BluetoothManagerCallback"

    #@9
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 1473
    :cond_c
    :goto_c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_1e

    #@d
    .line 1474
    iget-object v0, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@f
    return-object v0

    #@10
    .line 1470
    :cond_10
    :try_start_10
    iget-object v0, p0, Landroid/bluetooth/BluetoothAdapter;->mProxyServiceStateCallbacks:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_c

    #@18
    .line 1471
    iget-object v0, p0, Landroid/bluetooth/BluetoothAdapter;->mProxyServiceStateCallbacks:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    goto :goto_c

    #@1e
    .line 1473
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_10 .. :try_end_20} :catchall_1e

    #@20
    throw v0
.end method

.method public getBondedDevices()Ljava/util/Set;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 920
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v1

    #@5
    const/16 v2, 0xc

    #@7
    if-eq v1, v2, :cond_10

    #@9
    .line 921
    new-array v1, v3, [Landroid/bluetooth/BluetoothDevice;

    #@b
    invoke-direct {p0, v1}, Landroid/bluetooth/BluetoothAdapter;->toDeviceSet([Landroid/bluetooth/BluetoothDevice;)Ljava/util/Set;

    #@e
    move-result-object v1

    #@f
    .line 929
    :goto_f
    return-object v1

    #@10
    .line 924
    :cond_10
    :try_start_10
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@12
    monitor-enter v2
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_13} :catch_26

    #@13
    .line 925
    :try_start_13
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@15
    if-eqz v1, :cond_30

    #@17
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@19
    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->getBondedDevices()[Landroid/bluetooth/BluetoothDevice;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {p0, v1}, Landroid/bluetooth/BluetoothAdapter;->toDeviceSet([Landroid/bluetooth/BluetoothDevice;)Ljava/util/Set;

    #@20
    move-result-object v1

    #@21
    monitor-exit v2

    #@22
    goto :goto_f

    #@23
    .line 926
    :catchall_23
    move-exception v1

    #@24
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_13 .. :try_end_25} :catchall_23

    #@25
    :try_start_25
    throw v1
    :try_end_26
    .catch Landroid/os/RemoteException; {:try_start_25 .. :try_end_26} :catch_26

    #@26
    .line 928
    :catch_26
    move-exception v0

    #@27
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@29
    const-string v2, ""

    #@2b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2e
    .line 929
    const/4 v1, 0x0

    #@2f
    goto :goto_f

    #@30
    .line 926
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_30
    :try_start_30
    monitor-exit v2
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_23

    #@31
    .line 927
    const/4 v1, 0x0

    #@32
    :try_start_32
    new-array v1, v1, [Landroid/bluetooth/BluetoothDevice;

    #@34
    invoke-direct {p0, v1}, Landroid/bluetooth/BluetoothAdapter;->toDeviceSet([Landroid/bluetooth/BluetoothDevice;)Ljava/util/Set;
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_32 .. :try_end_37} :catch_26

    #@37
    move-result-object v1

    #@38
    goto :goto_f
.end method

.method public getConnectedDevices()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 393
    :try_start_0
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@2
    if-eqz v1, :cond_13

    #@4
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@6
    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->getConnectedDevices()Ljava/lang/String;
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result-object v1

    #@a
    .line 395
    :goto_a
    return-object v1

    #@b
    .line 394
    :catch_b
    move-exception v0

    #@c
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@e
    const-string v2, ""

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 395
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getConnectionState()I
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 946
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0xc

    #@7
    if-eq v2, v3, :cond_a

    #@9
    .line 952
    :goto_9
    return v1

    #@a
    .line 948
    :cond_a
    :try_start_a
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@c
    monitor-enter v3
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_d} :catch_1f

    #@d
    .line 949
    :try_start_d
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@f
    if-eqz v2, :cond_1a

    #@11
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@13
    invoke-interface {v2}, Landroid/bluetooth/IBluetooth;->getAdapterConnectionState()I

    #@16
    move-result v2

    #@17
    monitor-exit v3

    #@18
    move v1, v2

    #@19
    goto :goto_9

    #@1a
    .line 950
    :cond_1a
    monitor-exit v3

    #@1b
    goto :goto_9

    #@1c
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_1c

    #@1e
    :try_start_1e
    throw v2
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1f} :catch_1f

    #@1f
    .line 951
    :catch_1f
    move-exception v0

    #@20
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@22
    const-string v3, "getConnectionState:"

    #@24
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_9
.end method

.method public getDiscoverableTimeout()I
    .registers 5

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 793
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0xc

    #@7
    if-eq v2, v3, :cond_a

    #@9
    .line 799
    :goto_9
    return v1

    #@a
    .line 795
    :cond_a
    :try_start_a
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@c
    monitor-enter v3
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_d} :catch_1f

    #@d
    .line 796
    :try_start_d
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@f
    if-eqz v2, :cond_1a

    #@11
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@13
    invoke-interface {v2}, Landroid/bluetooth/IBluetooth;->getDiscoverableTimeout()I

    #@16
    move-result v2

    #@17
    monitor-exit v3

    #@18
    move v1, v2

    #@19
    goto :goto_9

    #@1a
    .line 797
    :cond_1a
    monitor-exit v3

    #@1b
    goto :goto_9

    #@1c
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_1c

    #@1e
    :try_start_1e
    throw v2
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1f} :catch_1f

    #@1f
    .line 798
    :catch_1f
    move-exception v0

    #@20
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@22
    const-string v3, ""

    #@24
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_9
.end method

.method public getName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 672
    :try_start_0
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    #@2
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothManager;->getName()Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 674
    :goto_6
    return-object v1

    #@7
    .line 673
    :catch_7
    move-exception v0

    #@8
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@a
    const-string v2, ""

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 674
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public getProfileConnectionState(I)I
    .registers 6
    .parameter "profile"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 971
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0xc

    #@7
    if-eq v2, v3, :cond_a

    #@9
    .line 979
    :goto_9
    return v1

    #@a
    .line 973
    :cond_a
    :try_start_a
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@c
    monitor-enter v3
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_d} :catch_1f

    #@d
    .line 974
    :try_start_d
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@f
    if-eqz v2, :cond_1a

    #@11
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@13
    invoke-interface {v2, p1}, Landroid/bluetooth/IBluetooth;->getProfileConnectionState(I)I

    #@16
    move-result v2

    #@17
    monitor-exit v3

    #@18
    move v1, v2

    #@19
    goto :goto_9

    #@1a
    .line 975
    :cond_1a
    monitor-exit v3

    #@1b
    goto :goto_9

    #@1c
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_1c

    #@1e
    :try_start_1e
    throw v2
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1f} :catch_1f

    #@1f
    .line 976
    :catch_1f
    move-exception v0

    #@20
    .line 977
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@22
    const-string v3, "getProfileConnectionState:"

    #@24
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_9
.end method

.method public getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z
    .registers 12
    .parameter "context"
    .parameter "listener"
    .parameter "profile"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 1234
    if-eqz p1, :cond_6

    #@4
    if-nez p2, :cond_8

    #@6
    :cond_6
    move v5, v6

    #@7
    .line 1252
    :goto_7
    return v5

    #@8
    .line 1236
    :cond_8
    if-ne p3, v5, :cond_10

    #@a
    .line 1237
    new-instance v1, Landroid/bluetooth/BluetoothHeadset;

    #@c
    invoke-direct {v1, p1, p2}, Landroid/bluetooth/BluetoothHeadset;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V

    #@f
    .line 1238
    .local v1, headset:Landroid/bluetooth/BluetoothHeadset;
    goto :goto_7

    #@10
    .line 1239
    .end local v1           #headset:Landroid/bluetooth/BluetoothHeadset;
    :cond_10
    const/4 v7, 0x2

    #@11
    if-ne p3, v7, :cond_19

    #@13
    .line 1240
    new-instance v0, Landroid/bluetooth/BluetoothA2dp;

    #@15
    invoke-direct {v0, p1, p2}, Landroid/bluetooth/BluetoothA2dp;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V

    #@18
    .line 1241
    .local v0, a2dp:Landroid/bluetooth/BluetoothA2dp;
    goto :goto_7

    #@19
    .line 1242
    .end local v0           #a2dp:Landroid/bluetooth/BluetoothA2dp;
    :cond_19
    const/4 v7, 0x4

    #@1a
    if-ne p3, v7, :cond_22

    #@1c
    .line 1243
    new-instance v3, Landroid/bluetooth/BluetoothInputDevice;

    #@1e
    invoke-direct {v3, p1, p2}, Landroid/bluetooth/BluetoothInputDevice;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V

    #@21
    .line 1244
    .local v3, iDev:Landroid/bluetooth/BluetoothInputDevice;
    goto :goto_7

    #@22
    .line 1245
    .end local v3           #iDev:Landroid/bluetooth/BluetoothInputDevice;
    :cond_22
    const/4 v7, 0x5

    #@23
    if-ne p3, v7, :cond_2b

    #@25
    .line 1246
    new-instance v4, Landroid/bluetooth/BluetoothPan;

    #@27
    invoke-direct {v4, p1, p2}, Landroid/bluetooth/BluetoothPan;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V

    #@2a
    .line 1247
    .local v4, pan:Landroid/bluetooth/BluetoothPan;
    goto :goto_7

    #@2b
    .line 1248
    .end local v4           #pan:Landroid/bluetooth/BluetoothPan;
    :cond_2b
    const/4 v7, 0x3

    #@2c
    if-ne p3, v7, :cond_34

    #@2e
    .line 1249
    new-instance v2, Landroid/bluetooth/BluetoothHealth;

    #@30
    invoke-direct {v2, p1, p2}, Landroid/bluetooth/BluetoothHealth;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V

    #@33
    .line 1250
    .local v2, health:Landroid/bluetooth/BluetoothHealth;
    goto :goto_7

    #@34
    .end local v2           #health:Landroid/bluetooth/BluetoothHealth;
    :cond_34
    move v5, v6

    #@35
    .line 1252
    goto :goto_7
.end method

.method public getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .registers 3
    .parameter "address"

    #@0
    .prologue
    .line 448
    new-instance v0, Landroid/bluetooth/BluetoothDevice;

    #@2
    invoke-direct {v0, p1}, Landroid/bluetooth/BluetoothDevice;-><init>(Ljava/lang/String;)V

    #@5
    return-object v0
.end method

.method public getRemoteDevice([B)Landroid/bluetooth/BluetoothDevice;
    .registers 11
    .parameter "address"

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 463
    if-eqz p1, :cond_b

    #@7
    array-length v0, p1

    #@8
    const/4 v1, 0x6

    #@9
    if-eq v0, v1, :cond_13

    #@b
    .line 464
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v1, "Bluetooth address must have 6 bytes"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 466
    :cond_13
    new-instance v0, Landroid/bluetooth/BluetoothDevice;

    #@15
    const-string v1, "%02X:%02X:%02X:%02X:%02X:%02X"

    #@17
    const/4 v2, 0x6

    #@18
    new-array v2, v2, [Ljava/lang/Object;

    #@1a
    aget-byte v3, p1, v4

    #@1c
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1f
    move-result-object v3

    #@20
    aput-object v3, v2, v4

    #@22
    aget-byte v3, p1, v5

    #@24
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@27
    move-result-object v3

    #@28
    aput-object v3, v2, v5

    #@2a
    aget-byte v3, p1, v6

    #@2c
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2f
    move-result-object v3

    #@30
    aput-object v3, v2, v6

    #@32
    aget-byte v3, p1, v7

    #@34
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@37
    move-result-object v3

    #@38
    aput-object v3, v2, v7

    #@3a
    aget-byte v3, p1, v8

    #@3c
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@3f
    move-result-object v3

    #@40
    aput-object v3, v2, v8

    #@42
    const/4 v3, 0x5

    #@43
    const/4 v4, 0x5

    #@44
    aget-byte v4, p1, v4

    #@46
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@49
    move-result-object v4

    #@4a
    aput-object v4, v2, v3

    #@4c
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@4f
    move-result-object v1

    #@50
    invoke-direct {v0, v1}, Landroid/bluetooth/BluetoothDevice;-><init>(Ljava/lang/String;)V

    #@53
    return-object v0
.end method

.method public getScanMode()I
    .registers 5

    #@0
    .prologue
    const/16 v1, 0x14

    #@2
    .line 737
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@5
    move-result v2

    #@6
    const/16 v3, 0xc

    #@8
    if-eq v2, v3, :cond_b

    #@a
    .line 743
    :goto_a
    return v1

    #@b
    .line 739
    :cond_b
    :try_start_b
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@d
    monitor-enter v3
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_e} :catch_20

    #@e
    .line 740
    :try_start_e
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@10
    if-eqz v2, :cond_1b

    #@12
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@14
    invoke-interface {v2}, Landroid/bluetooth/IBluetooth;->getScanMode()I

    #@17
    move-result v2

    #@18
    monitor-exit v3

    #@19
    move v1, v2

    #@1a
    goto :goto_a

    #@1b
    .line 741
    :cond_1b
    monitor-exit v3

    #@1c
    goto :goto_a

    #@1d
    :catchall_1d
    move-exception v2

    #@1e
    monitor-exit v3
    :try_end_1f
    .catchall {:try_start_e .. :try_end_1f} :catchall_1d

    #@1f
    :try_start_1f
    throw v2
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_20} :catch_20

    #@20
    .line 742
    :catch_20
    move-exception v0

    #@21
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@23
    const-string v3, ""

    #@25
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@28
    goto :goto_a
.end method

.method public getState()I
    .registers 7

    #@0
    .prologue
    .line 518
    :try_start_0
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@2
    monitor-enter v3
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_3} :catch_5e

    #@3
    .line 519
    :try_start_3
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@5
    if-eqz v2, :cond_35

    #@7
    .line 521
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@9
    invoke-interface {v2}, Landroid/bluetooth/IBluetooth;->getState()I

    #@c
    move-result v1

    #@d
    .line 522
    .local v1, state:I
    const-string v2, "BluetoothAdapter"

    #@f
    new-instance v4, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v5, ""

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@1d
    move-result v5

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    const-string v5, ": getState(). Returning "

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 523
    monitor-exit v3

    #@34
    .line 530
    .end local v1           #state:I
    :goto_34
    return v1

    #@35
    .line 527
    :cond_35
    monitor-exit v3
    :try_end_36
    .catchall {:try_start_3 .. :try_end_36} :catchall_5b

    #@36
    .line 529
    :goto_36
    const-string v2, "BluetoothAdapter"

    #@38
    new-instance v3, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v4, ""

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@46
    move-result v4

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    const-string v4, ": getState() :  mService = null. Returning STATE_OFF"

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 530
    const/16 v1, 0xa

    #@5a
    goto :goto_34

    #@5b
    .line 527
    :catchall_5b
    move-exception v2

    #@5c
    :try_start_5c
    monitor-exit v3
    :try_end_5d
    .catchall {:try_start_5c .. :try_end_5d} :catchall_5b

    #@5d
    :try_start_5d
    throw v2
    :try_end_5e
    .catch Landroid/os/RemoteException; {:try_start_5d .. :try_end_5e} :catch_5e

    #@5e
    .line 528
    :catch_5e
    move-exception v0

    #@5f
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@61
    const-string v3, ""

    #@63
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@66
    goto :goto_36
.end method

.method public getUuids()[Landroid/os/ParcelUuid;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 686
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0xc

    #@7
    if-eq v2, v3, :cond_a

    #@9
    .line 692
    :goto_9
    return-object v1

    #@a
    .line 688
    :cond_a
    :try_start_a
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@c
    monitor-enter v3
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_d} :catch_1f

    #@d
    .line 689
    :try_start_d
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@f
    if-eqz v2, :cond_1a

    #@11
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@13
    invoke-interface {v2}, Landroid/bluetooth/IBluetooth;->getUuids()[Landroid/os/ParcelUuid;

    #@16
    move-result-object v2

    #@17
    monitor-exit v3

    #@18
    move-object v1, v2

    #@19
    goto :goto_9

    #@1a
    .line 690
    :cond_1a
    monitor-exit v3

    #@1b
    goto :goto_9

    #@1c
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_1c

    #@1e
    :try_start_1e
    throw v2
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1f} :catch_1f

    #@1f
    .line 691
    :catch_1f
    move-exception v0

    #@20
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@22
    const-string v3, ""

    #@24
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_9
.end method

.method public isDiscovering()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 899
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0xc

    #@7
    if-eq v2, v3, :cond_a

    #@9
    .line 905
    :goto_9
    return v1

    #@a
    .line 901
    :cond_a
    :try_start_a
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@c
    monitor-enter v3
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_d} :catch_1f

    #@d
    .line 902
    :try_start_d
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@f
    if-eqz v2, :cond_1a

    #@11
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@13
    invoke-interface {v2}, Landroid/bluetooth/IBluetooth;->isDiscovering()Z

    #@16
    move-result v2

    #@17
    monitor-exit v3

    #@18
    move v1, v2

    #@19
    goto :goto_9

    #@1a
    .line 903
    :cond_1a
    monitor-exit v3

    #@1b
    goto :goto_9

    #@1c
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_1c

    #@1e
    :try_start_1e
    throw v2
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1f} :catch_1f

    #@1f
    .line 904
    :catch_1f
    move-exception v0

    #@20
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@22
    const-string v3, ""

    #@24
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_9
.end method

.method public isEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 481
    :try_start_0
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@2
    monitor-enter v2
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_3} :catch_15

    #@3
    .line 482
    :try_start_3
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@5
    if-eqz v1, :cond_f

    #@7
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@9
    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->isEnabled()Z

    #@c
    move-result v1

    #@d
    monitor-exit v2

    #@e
    .line 485
    :goto_e
    return v1

    #@f
    .line 483
    :cond_f
    monitor-exit v2

    #@10
    .line 485
    :goto_10
    const/4 v1, 0x0

    #@11
    goto :goto_e

    #@12
    .line 483
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    :try_start_14
    throw v1
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_15} :catch_15

    #@15
    .line 484
    :catch_15
    move-exception v0

    #@16
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@18
    const-string v2, ""

    #@1a
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d
    goto :goto_10
.end method

.method public isRadioEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 497
    :try_start_0
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@2
    monitor-enter v2
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_3} :catch_15

    #@3
    .line 498
    :try_start_3
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@5
    if-eqz v1, :cond_f

    #@7
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@9
    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->isRadioEnabled()Z

    #@c
    move-result v1

    #@d
    monitor-exit v2

    #@e
    .line 501
    :goto_e
    return v1

    #@f
    .line 499
    :cond_f
    monitor-exit v2

    #@10
    .line 501
    :goto_10
    const/4 v1, 0x0

    #@11
    goto :goto_e

    #@12
    .line 499
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    :try_start_14
    throw v1
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_15} :catch_15

    #@15
    .line 500
    :catch_15
    move-exception v0

    #@16
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@18
    const-string v2, ""

    #@1a
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d
    goto :goto_10
.end method

.method public listenUsingEncryptedRfcommOn(I)Landroid/bluetooth/BluetoothServerSocket;
    .registers 7
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1153
    new-instance v1, Landroid/bluetooth/BluetoothServerSocket;

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v1, v3, v2, v3, p1}, Landroid/bluetooth/BluetoothServerSocket;-><init>(IZZI)V

    #@7
    .line 1155
    .local v1, socket:Landroid/bluetooth/BluetoothServerSocket;
    iget-object v2, v1, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@9
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->bindListen()I

    #@c
    move-result v0

    #@d
    .line 1156
    .local v0, errno:I
    if-gez v0, :cond_28

    #@f
    .line 1160
    new-instance v2, Ljava/io/IOException;

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "Error: "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@27
    throw v2

    #@28
    .line 1162
    :cond_28
    return-object v1
.end method

.method public listenUsingEncryptedRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;
    .registers 5
    .parameter "name"
    .parameter "uuid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1102
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/bluetooth/BluetoothAdapter;->createNewRfcommSocketAndRecord(Ljava/lang/String;Ljava/util/UUID;ZZ)Landroid/bluetooth/BluetoothServerSocket;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public listenUsingInsecureRfcommOn(I)Landroid/bluetooth/BluetoothServerSocket;
    .registers 7
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1131
    new-instance v1, Landroid/bluetooth/BluetoothServerSocket;

    #@3
    const/4 v2, 0x1

    #@4
    invoke-direct {v1, v2, v3, v3, p1}, Landroid/bluetooth/BluetoothServerSocket;-><init>(IZZI)V

    #@7
    .line 1133
    .local v1, socket:Landroid/bluetooth/BluetoothServerSocket;
    iget-object v2, v1, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@9
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->bindListen()I

    #@c
    move-result v0

    #@d
    .line 1134
    .local v0, errno:I
    if-eqz v0, :cond_28

    #@f
    .line 1138
    new-instance v2, Ljava/io/IOException;

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "Error: "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@27
    throw v2

    #@28
    .line 1140
    :cond_28
    return-object v1
.end method

.method public listenUsingInsecureRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;
    .registers 4
    .parameter "name"
    .parameter "uuid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1064
    invoke-direct {p0, p1, p2, v0, v0}, Landroid/bluetooth/BluetoothAdapter;->createNewRfcommSocketAndRecord(Ljava/lang/String;Ljava/util/UUID;ZZ)Landroid/bluetooth/BluetoothServerSocket;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public listenUsingRfcommOn(I)Landroid/bluetooth/BluetoothServerSocket;
    .registers 7
    .parameter "channel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 997
    new-instance v1, Landroid/bluetooth/BluetoothServerSocket;

    #@3
    invoke-direct {v1, v2, v2, v2, p1}, Landroid/bluetooth/BluetoothServerSocket;-><init>(IZZI)V

    #@6
    .line 999
    .local v1, socket:Landroid/bluetooth/BluetoothServerSocket;
    iget-object v2, v1, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@8
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->bindListen()I

    #@b
    move-result v0

    #@c
    .line 1000
    .local v0, errno:I
    if-eqz v0, :cond_27

    #@e
    .line 1004
    new-instance v2, Ljava/io/IOException;

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "Error: "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@26
    throw v2

    #@27
    .line 1006
    :cond_27
    return-object v1
.end method

.method public listenUsingRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;
    .registers 4
    .parameter "name"
    .parameter "uuid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1033
    invoke-direct {p0, p1, p2, v0, v0}, Landroid/bluetooth/BluetoothAdapter;->createNewRfcommSocketAndRecord(Ljava/lang/String;Ljava/util/UUID;ZZ)Landroid/bluetooth/BluetoothServerSocket;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public readOutOfBandData()Landroid/util/Pair;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<[B[B>;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1194
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v0

    #@5
    const/16 v1, 0xc

    #@7
    if-eq v0, v1, :cond_9

    #@9
    .line 1215
    :cond_9
    return-object v2
.end method

.method removeServiceStateCallback(Landroid/bluetooth/IBluetoothManagerCallback;)V
    .registers 4
    .parameter "cb"

    #@0
    .prologue
    .line 1478
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@2
    monitor-enter v1

    #@3
    .line 1479
    :try_start_3
    iget-object v0, p0, Landroid/bluetooth/BluetoothAdapter;->mProxyServiceStateCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8
    .line 1480
    monitor-exit v1

    #@9
    .line 1481
    return-void

    #@a
    .line 1480
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public setDiscoverableTimeout(I)V
    .registers 5
    .parameter "timeout"

    #@0
    .prologue
    .line 804
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@3
    move-result v1

    #@4
    const/16 v2, 0xc

    #@6
    if-eq v1, v2, :cond_9

    #@8
    .line 810
    :goto_8
    return-void

    #@9
    .line 806
    :cond_9
    :try_start_9
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@b
    monitor-enter v2
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_c} :catch_1a

    #@c
    .line 807
    :try_start_c
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@e
    if-eqz v1, :cond_15

    #@10
    iget-object v1, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@12
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetooth;->setDiscoverableTimeout(I)Z

    #@15
    .line 808
    :cond_15
    monitor-exit v2

    #@16
    goto :goto_8

    #@17
    :catchall_17
    move-exception v1

    #@18
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_c .. :try_end_19} :catchall_17

    #@19
    :try_start_19
    throw v1
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_1a} :catch_1a

    #@1a
    .line 809
    :catch_1a
    move-exception v0

    #@1b
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothAdapter"

    #@1d
    const-string v2, ""

    #@1f
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@22
    goto :goto_8
.end method

.method public setName(Ljava/lang/String;)Z
    .registers 6
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 711
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0xc

    #@7
    if-eq v2, v3, :cond_a

    #@9
    .line 717
    :goto_9
    return v1

    #@a
    .line 713
    :cond_a
    :try_start_a
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@c
    monitor-enter v3
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_d} :catch_1f

    #@d
    .line 714
    :try_start_d
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@f
    if-eqz v2, :cond_1a

    #@11
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@13
    invoke-interface {v2, p1}, Landroid/bluetooth/IBluetooth;->setName(Ljava/lang/String;)Z

    #@16
    move-result v2

    #@17
    monitor-exit v3

    #@18
    move v1, v2

    #@19
    goto :goto_9

    #@1a
    .line 715
    :cond_1a
    monitor-exit v3

    #@1b
    goto :goto_9

    #@1c
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_1c

    #@1e
    :try_start_1e
    throw v2
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1f} :catch_1f

    #@1f
    .line 716
    :catch_1f
    move-exception v0

    #@20
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@22
    const-string v3, ""

    #@24
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_9
.end method

.method public setScanMode(I)Z
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 786
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0xc

    #@6
    if-eq v0, v1, :cond_a

    #@8
    const/4 v0, 0x0

    #@9
    .line 788
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getDiscoverableTimeout()I

    #@d
    move-result v0

    #@e
    invoke-virtual {p0, p1, v0}, Landroid/bluetooth/BluetoothAdapter;->setScanMode(II)Z

    #@11
    move-result v0

    #@12
    goto :goto_9
.end method

.method public setScanMode(II)Z
    .registers 7
    .parameter "mode"
    .parameter "duration"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 775
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0xc

    #@7
    if-eq v2, v3, :cond_a

    #@9
    .line 781
    :goto_9
    return v1

    #@a
    .line 777
    :cond_a
    :try_start_a
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@c
    monitor-enter v3
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_d} :catch_1f

    #@d
    .line 778
    :try_start_d
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@f
    if-eqz v2, :cond_1a

    #@11
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@13
    invoke-interface {v2, p1, p2}, Landroid/bluetooth/IBluetooth;->setScanMode(II)Z

    #@16
    move-result v2

    #@17
    monitor-exit v3

    #@18
    move v1, v2

    #@19
    goto :goto_9

    #@1a
    .line 779
    :cond_1a
    monitor-exit v3

    #@1b
    goto :goto_9

    #@1c
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_1c

    #@1e
    :try_start_1e
    throw v2
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1f} :catch_1f

    #@1f
    .line 780
    :catch_1f
    move-exception v0

    #@20
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@22
    const-string v3, ""

    #@24
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_9
.end method

.method public startDiscovery()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 843
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0xc

    #@7
    if-eq v2, v3, :cond_a

    #@9
    .line 849
    :goto_9
    return v1

    #@a
    .line 845
    :cond_a
    :try_start_a
    iget-object v3, p0, Landroid/bluetooth/BluetoothAdapter;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    #@c
    monitor-enter v3
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_d} :catch_1f

    #@d
    .line 846
    :try_start_d
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@f
    if-eqz v2, :cond_1a

    #@11
    iget-object v2, p0, Landroid/bluetooth/BluetoothAdapter;->mService:Landroid/bluetooth/IBluetooth;

    #@13
    invoke-interface {v2}, Landroid/bluetooth/IBluetooth;->startDiscovery()Z

    #@16
    move-result v2

    #@17
    monitor-exit v3

    #@18
    move v1, v2

    #@19
    goto :goto_9

    #@1a
    .line 847
    :cond_1a
    monitor-exit v3

    #@1b
    goto :goto_9

    #@1c
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_d .. :try_end_1e} :catchall_1c

    #@1e
    :try_start_1e
    throw v2
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1f} :catch_1f

    #@1f
    .line 848
    :catch_1f
    move-exception v0

    #@20
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapter"

    #@22
    const-string v3, ""

    #@24
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_9
.end method
