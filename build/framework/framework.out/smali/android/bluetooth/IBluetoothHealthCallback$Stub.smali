.class public abstract Landroid/bluetooth/IBluetoothHealthCallback$Stub;
.super Landroid/os/Binder;
.source "IBluetoothHealthCallback.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothHealthCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothHealthCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/IBluetoothHealthCallback$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.bluetooth.IBluetoothHealthCallback"

.field static final TRANSACTION_onHealthAppConfigurationStatusChange:I = 0x1

.field static final TRANSACTION_onHealthChannelStateChange:I = 0x2

.field static final TRANSACTION_onHealthDeviceSinkDataTypeResult:I = 0x4

.field static final TRANSACTION_onHealthDeviceSourceDataTypeResult:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.bluetooth.IBluetoothHealthCallback"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/bluetooth/IBluetoothHealthCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothHealthCallback;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.bluetooth.IBluetoothHealthCallback"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/bluetooth/IBluetoothHealthCallback;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/bluetooth/IBluetoothHealthCallback;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/bluetooth/IBluetoothHealthCallback$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/bluetooth/IBluetoothHealthCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 41
    sparse-switch p1, :sswitch_data_e0

    #@4
    .line 145
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    .line 45
    :sswitch_9
    const-string v0, "android.bluetooth.IBluetoothHealthCallback"

    #@b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    move v0, v7

    #@f
    .line 46
    goto :goto_8

    #@10
    .line 50
    :sswitch_10
    const-string v0, "android.bluetooth.IBluetoothHealthCallback"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_2f

    #@1b
    .line 53
    sget-object v0, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@23
    .line 59
    .local v1, _arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_23
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@26
    move-result v2

    #@27
    .line 60
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/bluetooth/IBluetoothHealthCallback$Stub;->onHealthAppConfigurationStatusChange(Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V

    #@2a
    .line 61
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d
    move v0, v7

    #@2e
    .line 62
    goto :goto_8

    #@2f
    .line 56
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v2           #_arg1:I
    :cond_2f
    const/4 v1, 0x0

    #@30
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_23

    #@31
    .line 66
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :sswitch_31
    const-string v0, "android.bluetooth.IBluetoothHealthCallback"

    #@33
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v0

    #@3a
    if-eqz v0, :cond_75

    #@3c
    .line 69
    sget-object v0, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3e
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@41
    move-result-object v1

    #@42
    check-cast v1, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@44
    .line 75
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_44
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v0

    #@48
    if-eqz v0, :cond_77

    #@4a
    .line 76
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4c
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4f
    move-result-object v2

    #@50
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@52
    .line 82
    .local v2, _arg1:Landroid/bluetooth/BluetoothDevice;
    :goto_52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v3

    #@56
    .line 84
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@59
    move-result v4

    #@5a
    .line 86
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5d
    move-result v0

    #@5e
    if-eqz v0, :cond_79

    #@60
    .line 87
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@62
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@65
    move-result-object v5

    #@66
    check-cast v5, Landroid/os/ParcelFileDescriptor;

    #@68
    .line 93
    .local v5, _arg4:Landroid/os/ParcelFileDescriptor;
    :goto_68
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6b
    move-result v6

    #@6c
    .local v6, _arg5:I
    move-object v0, p0

    #@6d
    .line 94
    invoke-virtual/range {v0 .. v6}, Landroid/bluetooth/IBluetoothHealthCallback$Stub;->onHealthChannelStateChange(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V

    #@70
    .line 95
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@73
    move v0, v7

    #@74
    .line 96
    goto :goto_8

    #@75
    .line 72
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v2           #_arg1:Landroid/bluetooth/BluetoothDevice;
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:Landroid/os/ParcelFileDescriptor;
    .end local v6           #_arg5:I
    :cond_75
    const/4 v1, 0x0

    #@76
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_44

    #@77
    .line 79
    :cond_77
    const/4 v2, 0x0

    #@78
    .restart local v2       #_arg1:Landroid/bluetooth/BluetoothDevice;
    goto :goto_52

    #@79
    .line 90
    .restart local v3       #_arg2:I
    .restart local v4       #_arg3:I
    :cond_79
    const/4 v5, 0x0

    #@7a
    .restart local v5       #_arg4:Landroid/os/ParcelFileDescriptor;
    goto :goto_68

    #@7b
    .line 100
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v2           #_arg1:Landroid/bluetooth/BluetoothDevice;
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:Landroid/os/ParcelFileDescriptor;
    :sswitch_7b
    const-string v0, "android.bluetooth.IBluetoothHealthCallback"

    #@7d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@80
    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@83
    move-result v0

    #@84
    if-eqz v0, :cond_a9

    #@86
    .line 103
    sget-object v0, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@88
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@8b
    move-result-object v1

    #@8c
    check-cast v1, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@8e
    .line 109
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_8e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@91
    move-result v0

    #@92
    if-eqz v0, :cond_ab

    #@94
    .line 110
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@96
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@99
    move-result-object v2

    #@9a
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@9c
    .line 116
    .restart local v2       #_arg1:Landroid/bluetooth/BluetoothDevice;
    :goto_9c
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@9f
    move-result-object v3

    #@a0
    .line 117
    .local v3, _arg2:[I
    invoke-virtual {p0, v1, v2, v3}, Landroid/bluetooth/IBluetoothHealthCallback$Stub;->onHealthDeviceSourceDataTypeResult(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V

    #@a3
    .line 118
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a6
    move v0, v7

    #@a7
    .line 119
    goto/16 :goto_8

    #@a9
    .line 106
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v2           #_arg1:Landroid/bluetooth/BluetoothDevice;
    .end local v3           #_arg2:[I
    :cond_a9
    const/4 v1, 0x0

    #@aa
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_8e

    #@ab
    .line 113
    :cond_ab
    const/4 v2, 0x0

    #@ac
    .restart local v2       #_arg1:Landroid/bluetooth/BluetoothDevice;
    goto :goto_9c

    #@ad
    .line 123
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v2           #_arg1:Landroid/bluetooth/BluetoothDevice;
    :sswitch_ad
    const-string v0, "android.bluetooth.IBluetoothHealthCallback"

    #@af
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b2
    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b5
    move-result v0

    #@b6
    if-eqz v0, :cond_db

    #@b8
    .line 126
    sget-object v0, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ba
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@bd
    move-result-object v1

    #@be
    check-cast v1, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@c0
    .line 132
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_c0
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c3
    move-result v0

    #@c4
    if-eqz v0, :cond_dd

    #@c6
    .line 133
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c8
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@cb
    move-result-object v2

    #@cc
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@ce
    .line 139
    .restart local v2       #_arg1:Landroid/bluetooth/BluetoothDevice;
    :goto_ce
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@d1
    move-result-object v3

    #@d2
    .line 140
    .restart local v3       #_arg2:[I
    invoke-virtual {p0, v1, v2, v3}, Landroid/bluetooth/IBluetoothHealthCallback$Stub;->onHealthDeviceSinkDataTypeResult(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V

    #@d5
    .line 141
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d8
    move v0, v7

    #@d9
    .line 142
    goto/16 :goto_8

    #@db
    .line 129
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v2           #_arg1:Landroid/bluetooth/BluetoothDevice;
    .end local v3           #_arg2:[I
    :cond_db
    const/4 v1, 0x0

    #@dc
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_c0

    #@dd
    .line 136
    :cond_dd
    const/4 v2, 0x0

    #@de
    .restart local v2       #_arg1:Landroid/bluetooth/BluetoothDevice;
    goto :goto_ce

    #@df
    .line 41
    nop

    #@e0
    :sswitch_data_e0
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_31
        0x3 -> :sswitch_7b
        0x4 -> :sswitch_ad
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
