.class public abstract Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;
.super Landroid/os/Binder;
.source "IBluetoothHeadsetPhone.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothHeadsetPhone;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothHeadsetPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.bluetooth.IBluetoothHeadsetPhone"

.field static final TRANSACTION_answerCall:I = 0x1

.field static final TRANSACTION_cdmaSetSecondCallState:I = 0xb

.field static final TRANSACTION_cdmaSwapSecondCallState:I = 0xa

.field static final TRANSACTION_getNetworkOperator:I = 0x5

.field static final TRANSACTION_getSubscriberNumber:I = 0x6

.field static final TRANSACTION_hangupCall:I = 0x2

.field static final TRANSACTION_listCurrentCalls:I = 0x7

.field static final TRANSACTION_processChld:I = 0x4

.field static final TRANSACTION_queryPhoneState:I = 0x8

.field static final TRANSACTION_sendDtmf:I = 0x3

.field static final TRANSACTION_updateBtHandsfreeAfterRadioTechnologyChange:I = 0x9


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.bluetooth.IBluetoothHeadsetPhone"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothHeadsetPhone;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.bluetooth.IBluetoothHeadsetPhone"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_dc

    #@5
    .line 142
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v3

    #@9
    :goto_9
    return v3

    #@a
    .line 47
    :sswitch_a
    const-string v2, "android.bluetooth.IBluetoothHeadsetPhone"

    #@c
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v4, "android.bluetooth.IBluetoothHeadsetPhone"

    #@12
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 53
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->answerCall()Z

    #@18
    move-result v1

    #@19
    .line 54
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c
    .line 55
    if-eqz v1, :cond_1f

    #@1e
    move v2, v3

    #@1f
    :cond_1f
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    goto :goto_9

    #@23
    .line 60
    .end local v1           #_result:Z
    :sswitch_23
    const-string v4, "android.bluetooth.IBluetoothHeadsetPhone"

    #@25
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28
    .line 61
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->hangupCall()Z

    #@2b
    move-result v1

    #@2c
    .line 62
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f
    .line 63
    if-eqz v1, :cond_32

    #@31
    move v2, v3

    #@32
    :cond_32
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@35
    goto :goto_9

    #@36
    .line 68
    .end local v1           #_result:Z
    :sswitch_36
    const-string v4, "android.bluetooth.IBluetoothHeadsetPhone"

    #@38
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b
    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3e
    move-result v0

    #@3f
    .line 71
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->sendDtmf(I)Z

    #@42
    move-result v1

    #@43
    .line 72
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@46
    .line 73
    if-eqz v1, :cond_49

    #@48
    move v2, v3

    #@49
    :cond_49
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@4c
    goto :goto_9

    #@4d
    .line 78
    .end local v0           #_arg0:I
    .end local v1           #_result:Z
    :sswitch_4d
    const-string v4, "android.bluetooth.IBluetoothHeadsetPhone"

    #@4f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@52
    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v0

    #@56
    .line 81
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->processChld(I)Z

    #@59
    move-result v1

    #@5a
    .line 82
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5d
    .line 83
    if-eqz v1, :cond_60

    #@5f
    move v2, v3

    #@60
    :cond_60
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@63
    goto :goto_9

    #@64
    .line 88
    .end local v0           #_arg0:I
    .end local v1           #_result:Z
    :sswitch_64
    const-string v2, "android.bluetooth.IBluetoothHeadsetPhone"

    #@66
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@69
    .line 89
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->getNetworkOperator()Ljava/lang/String;

    #@6c
    move-result-object v1

    #@6d
    .line 90
    .local v1, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@70
    .line 91
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@73
    goto :goto_9

    #@74
    .line 96
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_74
    const-string v2, "android.bluetooth.IBluetoothHeadsetPhone"

    #@76
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@79
    .line 97
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->getSubscriberNumber()Ljava/lang/String;

    #@7c
    move-result-object v1

    #@7d
    .line 98
    .restart local v1       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@80
    .line 99
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@83
    goto :goto_9

    #@84
    .line 104
    .end local v1           #_result:Ljava/lang/String;
    :sswitch_84
    const-string v4, "android.bluetooth.IBluetoothHeadsetPhone"

    #@86
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@89
    .line 105
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->listCurrentCalls()Z

    #@8c
    move-result v1

    #@8d
    .line 106
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@90
    .line 107
    if-eqz v1, :cond_93

    #@92
    move v2, v3

    #@93
    :cond_93
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@96
    goto/16 :goto_9

    #@98
    .line 112
    .end local v1           #_result:Z
    :sswitch_98
    const-string v4, "android.bluetooth.IBluetoothHeadsetPhone"

    #@9a
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9d
    .line 113
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->queryPhoneState()Z

    #@a0
    move-result v1

    #@a1
    .line 114
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a4
    .line 115
    if-eqz v1, :cond_a7

    #@a6
    move v2, v3

    #@a7
    :cond_a7
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@aa
    goto/16 :goto_9

    #@ac
    .line 120
    .end local v1           #_result:Z
    :sswitch_ac
    const-string v2, "android.bluetooth.IBluetoothHeadsetPhone"

    #@ae
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b1
    .line 121
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->updateBtHandsfreeAfterRadioTechnologyChange()V

    #@b4
    .line 122
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b7
    goto/16 :goto_9

    #@b9
    .line 127
    :sswitch_b9
    const-string v2, "android.bluetooth.IBluetoothHeadsetPhone"

    #@bb
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@be
    .line 128
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->cdmaSwapSecondCallState()V

    #@c1
    .line 129
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c4
    goto/16 :goto_9

    #@c6
    .line 134
    :sswitch_c6
    const-string v4, "android.bluetooth.IBluetoothHeadsetPhone"

    #@c8
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cb
    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ce
    move-result v4

    #@cf
    if-eqz v4, :cond_da

    #@d1
    move v0, v3

    #@d2
    .line 137
    .local v0, _arg0:Z
    :goto_d2
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->cdmaSetSecondCallState(Z)V

    #@d5
    .line 138
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d8
    goto/16 :goto_9

    #@da
    .end local v0           #_arg0:Z
    :cond_da
    move v0, v2

    #@db
    .line 136
    goto :goto_d2

    #@dc
    .line 43
    :sswitch_data_dc
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_23
        0x3 -> :sswitch_36
        0x4 -> :sswitch_4d
        0x5 -> :sswitch_64
        0x6 -> :sswitch_74
        0x7 -> :sswitch_84
        0x8 -> :sswitch_98
        0x9 -> :sswitch_ac
        0xa -> :sswitch_b9
        0xb -> :sswitch_c6
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
