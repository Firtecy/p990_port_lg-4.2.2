.class public final Landroid/bluetooth/BluetoothHeadset;
.super Ljava/lang/Object;
.source "BluetoothHeadset.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile;


# static fields
.field public static final ACTION_AUDIO_STATE_CHANGED:Ljava/lang/String; = "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

.field public static final ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String; = "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

.field public static final ACTION_VENDOR_SPECIFIC_HEADSET_EVENT:Ljava/lang/String; = "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"

.field public static final AT_CMD_TYPE_ACTION:I = 0x4

.field public static final AT_CMD_TYPE_BASIC:I = 0x3

.field public static final AT_CMD_TYPE_READ:I = 0x0

.field public static final AT_CMD_TYPE_SET:I = 0x2

.field public static final AT_CMD_TYPE_TEST:I = 0x1

.field private static final DBG:Z = true

.field public static final EXTRA_VENDOR_SPECIFIC_HEADSET_EVENT_ARGS:Ljava/lang/String; = "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_ARGS"

.field public static final EXTRA_VENDOR_SPECIFIC_HEADSET_EVENT_CMD:Ljava/lang/String; = "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_CMD"

.field public static final EXTRA_VENDOR_SPECIFIC_HEADSET_EVENT_CMD_TYPE:Ljava/lang/String; = "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_CMD_TYPE"

.field public static final STATE_AUDIO_CONNECTED:I = 0xc

.field public static final STATE_AUDIO_CONNECTING:I = 0xb

.field public static final STATE_AUDIO_DISCONNECTED:I = 0xa

.field private static final TAG:Ljava/lang/String; = "BluetoothHeadset"

.field private static final VDBG:Z = true

.field public static final VENDOR_SPECIFIC_HEADSET_EVENT_COMPANY_ID_CATEGORY:Ljava/lang/String; = "android.bluetooth.headset.intent.category.companyid"


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mService:Landroid/bluetooth/IBluetoothHeadset;

.field private mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V
    .registers 8
    .parameter "context"
    .parameter "l"

    #@0
    .prologue
    .line 259
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 225
    new-instance v2, Landroid/bluetooth/BluetoothHeadset$1;

    #@5
    invoke-direct {v2, p0}, Landroid/bluetooth/BluetoothHeadset$1;-><init>(Landroid/bluetooth/BluetoothHeadset;)V

    #@8
    iput-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@a
    .line 857
    new-instance v2, Landroid/bluetooth/BluetoothHeadset$2;

    #@c
    invoke-direct {v2, p0}, Landroid/bluetooth/BluetoothHeadset$2;-><init>(Landroid/bluetooth/BluetoothHeadset;)V

    #@f
    iput-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mConnection:Landroid/content/ServiceConnection;

    #@11
    .line 260
    iput-object p1, p0, Landroid/bluetooth/BluetoothHeadset;->mContext:Landroid/content/Context;

    #@13
    .line 261
    iput-object p2, p0, Landroid/bluetooth/BluetoothHeadset;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@15
    .line 262
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@18
    move-result-object v2

    #@19
    iput-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@1b
    .line 264
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@1d
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothManager()Landroid/bluetooth/IBluetoothManager;

    #@20
    move-result-object v1

    #@21
    .line 265
    .local v1, mgr:Landroid/bluetooth/IBluetoothManager;
    if-eqz v1, :cond_28

    #@23
    .line 267
    :try_start_23
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@25
    invoke-interface {v1, v2}, Landroid/bluetooth/IBluetoothManager;->registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_28} :catch_44

    #@28
    .line 273
    :cond_28
    :goto_28
    new-instance v2, Landroid/content/Intent;

    #@2a
    const-class v3, Landroid/bluetooth/IBluetoothHeadset;

    #@2c
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@33
    iget-object v3, p0, Landroid/bluetooth/BluetoothHeadset;->mConnection:Landroid/content/ServiceConnection;

    #@35
    const/4 v4, 0x0

    #@36
    invoke-virtual {p1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@39
    move-result v2

    #@3a
    if-nez v2, :cond_43

    #@3c
    .line 274
    const-string v2, "BluetoothHeadset"

    #@3e
    const-string v3, "Could not bind to Bluetooth Headset Service"

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 276
    :cond_43
    return-void

    #@44
    .line 268
    :catch_44
    move-exception v0

    #@45
    .line 269
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothHeadset"

    #@47
    const-string v3, ""

    #@49
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4c
    goto :goto_28
.end method

.method static synthetic access$000(Landroid/bluetooth/BluetoothHeadset;)Landroid/content/ServiceConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Landroid/bluetooth/BluetoothHeadset;->mConnection:Landroid/content/ServiceConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/IBluetoothHeadset;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/IBluetoothHeadset;)Landroid/bluetooth/IBluetoothHeadset;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 46
    iput-object p1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/bluetooth/BluetoothHeadset;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Landroid/bluetooth/BluetoothHeadset;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Landroid/bluetooth/BluetoothHeadset;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@2
    return-object v0
.end method

.method public static isBluetoothVoiceDialingEnabled(Landroid/content/Context;)Z
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 618
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    const v1, 0x111002c

    #@7
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method private isDisabled()Z
    .registers 3

    #@0
    .prologue
    .line 881
    iget-object v0, p0, Landroid/bluetooth/BluetoothHeadset;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@5
    move-result v0

    #@6
    const/16 v1, 0xa

    #@8
    if-ne v0, v1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    .line 882
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private isEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 876
    iget-object v0, p0, Landroid/bluetooth/BluetoothHeadset;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@5
    move-result v0

    #@6
    const/16 v1, 0xc

    #@8
    if-ne v0, v1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    .line 877
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 886
    if-nez p1, :cond_4

    #@3
    .line 889
    :cond_3
    :goto_3
    return v0

    #@4
    .line 888
    :cond_4
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-static {v1}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_3

    #@e
    const/4 v0, 0x1

    #@f
    goto :goto_3
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 893
    const-string v0, "BluetoothHeadset"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 894
    return-void
.end method


# virtual methods
.method public acceptIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 629
    const-string v1, "acceptIncomingConnect"

    #@2
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@5
    .line 630
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@7
    if-eqz v1, :cond_22

    #@9
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_22

    #@f
    .line 632
    :try_start_f
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@11
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->acceptIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_16

    #@14
    move-result v1

    #@15
    .line 638
    :goto_15
    return v1

    #@16
    .line 633
    :catch_16
    move-exception v0

    #@17
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@19
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 638
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_20
    const/4 v1, 0x0

    #@21
    goto :goto_15

    #@22
    .line 635
    :cond_22
    const-string v1, "BluetoothHeadset"

    #@24
    const-string v2, "Proxy not attached to service"

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 636
    const-string v1, "BluetoothHeadset"

    #@2b
    new-instance v2, Ljava/lang/Throwable;

    #@2d
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@30
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_20
.end method

.method public clccResponse(IIIIZLjava/lang/String;I)V
    .registers 17
    .parameter "index"
    .parameter "direction"
    .parameter "status"
    .parameter "mode"
    .parameter "mpty"
    .parameter "number"
    .parameter "type"

    #@0
    .prologue
    .line 845
    iget-object v0, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2
    if-eqz v0, :cond_23

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_23

    #@a
    .line 847
    :try_start_a
    iget-object v0, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@c
    move v1, p1

    #@d
    move v2, p2

    #@e
    move v3, p3

    #@f
    move v4, p4

    #@10
    move v5, p5

    #@11
    move-object v6, p6

    #@12
    move/from16 v7, p7

    #@14
    invoke-interface/range {v0 .. v7}, Landroid/bluetooth/IBluetoothHeadset;->clccResponse(IIIIZLjava/lang/String;I)V
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_17} :catch_18

    #@17
    .line 855
    :goto_17
    return-void

    #@18
    .line 848
    :catch_18
    move-exception v8

    #@19
    .line 849
    .local v8, e:Landroid/os/RemoteException;
    const-string v0, "BluetoothHeadset"

    #@1b
    invoke-virtual {v8}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_17

    #@23
    .line 852
    .end local v8           #e:Landroid/os/RemoteException;
    :cond_23
    const-string v0, "BluetoothHeadset"

    #@25
    const-string v1, "Proxy not attached to service"

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 853
    const-string v0, "BluetoothHeadset"

    #@2c
    new-instance v1, Ljava/lang/Throwable;

    #@2e
    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    #@31
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_17
.end method

.method close()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 285
    const-string v3, "close()"

    #@3
    invoke-static {v3}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@6
    .line 287
    iget-object v3, p0, Landroid/bluetooth/BluetoothHeadset;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@8
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothManager()Landroid/bluetooth/IBluetoothManager;

    #@b
    move-result-object v1

    #@c
    .line 288
    .local v1, mgr:Landroid/bluetooth/IBluetoothManager;
    if-eqz v1, :cond_13

    #@e
    .line 290
    :try_start_e
    iget-object v3, p0, Landroid/bluetooth/BluetoothHeadset;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@10
    invoke-interface {v1, v3}, Landroid/bluetooth/IBluetoothManager;->unregisterStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_13} :catch_28

    #@13
    .line 296
    :cond_13
    :goto_13
    iget-object v4, p0, Landroid/bluetooth/BluetoothHeadset;->mConnection:Landroid/content/ServiceConnection;

    #@15
    monitor-enter v4

    #@16
    .line 297
    :try_start_16
    iget-object v3, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;
    :try_end_18
    .catchall {:try_start_16 .. :try_end_18} :catchall_3a

    #@18
    if-eqz v3, :cond_24

    #@1a
    .line 299
    const/4 v3, 0x0

    #@1b
    :try_start_1b
    iput-object v3, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@1d
    .line 300
    iget-object v3, p0, Landroid/bluetooth/BluetoothHeadset;->mContext:Landroid/content/Context;

    #@1f
    iget-object v5, p0, Landroid/bluetooth/BluetoothHeadset;->mConnection:Landroid/content/ServiceConnection;

    #@21
    invoke-virtual {v3, v5}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_24
    .catchall {:try_start_1b .. :try_end_24} :catchall_3a
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_24} :catch_31

    #@24
    .line 305
    :cond_24
    :goto_24
    :try_start_24
    monitor-exit v4
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_3a

    #@25
    .line 306
    iput-object v6, p0, Landroid/bluetooth/BluetoothHeadset;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@27
    .line 307
    return-void

    #@28
    .line 291
    :catch_28
    move-exception v0

    #@29
    .line 292
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "BluetoothHeadset"

    #@2b
    const-string v4, ""

    #@2d
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@30
    goto :goto_13

    #@31
    .line 301
    .end local v0           #e:Ljava/lang/Exception;
    :catch_31
    move-exception v2

    #@32
    .line 302
    .local v2, re:Ljava/lang/Exception;
    :try_start_32
    const-string v3, "BluetoothHeadset"

    #@34
    const-string v5, ""

    #@36
    invoke-static {v3, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@39
    goto :goto_24

    #@3a
    .line 305
    .end local v2           #re:Ljava/lang/Exception;
    :catchall_3a
    move-exception v3

    #@3b
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_32 .. :try_end_3c} :catchall_3a

    #@3c
    throw v3
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 332
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "connect("

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    const-string v3, ")"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v2}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@1d
    .line 333
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@1f
    if-eqz v2, :cond_44

    #@21
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_44

    #@27
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_44

    #@2d
    .line 336
    :try_start_2d
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2f
    invoke-interface {v2, p1}, Landroid/bluetooth/IBluetoothHeadset;->connect(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_32} :catch_34

    #@32
    move-result v1

    #@33
    .line 343
    :cond_33
    :goto_33
    return v1

    #@34
    .line 337
    :catch_34
    move-exception v0

    #@35
    .line 338
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothHeadset"

    #@37
    new-instance v3, Ljava/lang/Throwable;

    #@39
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@3c
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_33

    #@44
    .line 342
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_44
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@46
    if-nez v2, :cond_33

    #@48
    const-string v2, "BluetoothHeadset"

    #@4a
    const-string v3, "Proxy not attached to service"

    #@4c
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_33
.end method

.method public connectAudio()Z
    .registers 4

    #@0
    .prologue
    .line 710
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2
    if-eqz v1, :cond_1d

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_1d

    #@a
    .line 712
    :try_start_a
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@c
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothHeadset;->connectAudio()Z
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-result v1

    #@10
    .line 720
    :goto_10
    return v1

    #@11
    .line 713
    :catch_11
    move-exception v0

    #@12
    .line 714
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@14
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 720
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_10

    #@1d
    .line 717
    :cond_1d
    const-string v1, "BluetoothHeadset"

    #@1f
    const-string v2, "Proxy not attached to service"

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 718
    const-string v1, "BluetoothHeadset"

    #@26
    new-instance v2, Ljava/lang/Throwable;

    #@28
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@2b
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_1b
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 373
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "disconnect("

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    const-string v3, ")"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v2}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@1d
    .line 374
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@1f
    if-eqz v2, :cond_44

    #@21
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_44

    #@27
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_44

    #@2d
    .line 377
    :try_start_2d
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2f
    invoke-interface {v2, p1}, Landroid/bluetooth/IBluetoothHeadset;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_32} :catch_34

    #@32
    move-result v1

    #@33
    .line 384
    :cond_33
    :goto_33
    return v1

    #@34
    .line 378
    :catch_34
    move-exception v0

    #@35
    .line 379
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothHeadset"

    #@37
    new-instance v3, Ljava/lang/Throwable;

    #@39
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@3c
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_33

    #@44
    .line 383
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_44
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@46
    if-nez v2, :cond_33

    #@48
    const-string v2, "BluetoothHeadset"

    #@4a
    const-string v3, "Proxy not attached to service"

    #@4c
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_33
.end method

.method public disconnectAudio()Z
    .registers 4

    #@0
    .prologue
    .line 733
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2
    if-eqz v1, :cond_1d

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_1d

    #@a
    .line 735
    :try_start_a
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@c
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothHeadset;->disconnectAudio()Z
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-result v1

    #@10
    .line 743
    :goto_10
    return v1

    #@11
    .line 736
    :catch_11
    move-exception v0

    #@12
    .line 737
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@14
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 743
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_10

    #@1d
    .line 740
    :cond_1d
    const-string v1, "BluetoothHeadset"

    #@1f
    const-string v2, "Proxy not attached to service"

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 741
    const-string v1, "BluetoothHeadset"

    #@26
    new-instance v2, Ljava/lang/Throwable;

    #@28
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@2b
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_1b
.end method

.method public getAudioState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 665
    const-string v1, "getAudioState"

    #@2
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@5
    .line 666
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@7
    if-eqz v1, :cond_23

    #@9
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isDisabled()Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_23

    #@f
    .line 668
    :try_start_f
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@11
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->getAudioState(Landroid/bluetooth/BluetoothDevice;)I
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_16

    #@14
    move-result v1

    #@15
    .line 674
    :goto_15
    return v1

    #@16
    .line 669
    :catch_16
    move-exception v0

    #@17
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@19
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 674
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_20
    const/16 v1, 0xa

    #@22
    goto :goto_15

    #@23
    .line 671
    :cond_23
    const-string v1, "BluetoothHeadset"

    #@25
    const-string v2, "Proxy not attached to service"

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 672
    const-string v1, "BluetoothHeadset"

    #@2c
    new-instance v2, Ljava/lang/Throwable;

    #@2e
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@31
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_20
.end method

.method public getBatteryUsageHint(Landroid/bluetooth/BluetoothDevice;)I
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 598
    const-string v1, "getBatteryUsageHint()"

    #@2
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@5
    .line 599
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@7
    if-eqz v1, :cond_2b

    #@9
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_2b

    #@f
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_2b

    #@15
    .line 602
    :try_start_15
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@17
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->getBatteryUsageHint(Landroid/bluetooth/BluetoothDevice;)I
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1a} :catch_1c

    #@1a
    move-result v1

    #@1b
    .line 608
    :goto_1b
    return v1

    #@1c
    .line 603
    :catch_1c
    move-exception v0

    #@1d
    .line 604
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@1f
    new-instance v2, Ljava/lang/Throwable;

    #@21
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@24
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 607
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2b
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2d
    if-nez v1, :cond_36

    #@2f
    const-string v1, "BluetoothHeadset"

    #@31
    const-string v2, "Proxy not attached to service"

    #@33
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 608
    :cond_36
    const/4 v1, -0x1

    #@37
    goto :goto_1b
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 391
    const-string v1, "getConnectedDevices()"

    #@2
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@5
    .line 392
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@7
    if-eqz v1, :cond_2b

    #@9
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_2b

    #@f
    .line 394
    :try_start_f
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@11
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothHeadset;->getConnectedDevices()Ljava/util/List;
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_16

    #@14
    move-result-object v1

    #@15
    .line 401
    :goto_15
    return-object v1

    #@16
    .line 395
    :catch_16
    move-exception v0

    #@17
    .line 396
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@19
    new-instance v2, Ljava/lang/Throwable;

    #@1b
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@1e
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 397
    new-instance v1, Ljava/util/ArrayList;

    #@27
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@2a
    goto :goto_15

    #@2b
    .line 400
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2b
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2d
    if-nez v1, :cond_36

    #@2f
    const-string v1, "BluetoothHeadset"

    #@31
    const-string v2, "Proxy not attached to service"

    #@33
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 401
    :cond_36
    new-instance v1, Ljava/util/ArrayList;

    #@38
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@3b
    goto :goto_15
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 425
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "getConnectionState("

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    const-string v3, ")"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v2}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@1d
    .line 426
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@1f
    if-eqz v2, :cond_44

    #@21
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_44

    #@27
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_44

    #@2d
    .line 429
    :try_start_2d
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2f
    invoke-interface {v2, p1}, Landroid/bluetooth/IBluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_32} :catch_34

    #@32
    move-result v1

    #@33
    .line 436
    :cond_33
    :goto_33
    return v1

    #@34
    .line 430
    :catch_34
    move-exception v0

    #@35
    .line 431
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothHeadset"

    #@37
    new-instance v3, Ljava/lang/Throwable;

    #@39
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@3c
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_33

    #@44
    .line 435
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_44
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@46
    if-nez v2, :cond_33

    #@48
    const-string v2, "BluetoothHeadset"

    #@4a
    const-string v3, "Proxy not attached to service"

    #@4c
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_33
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 5
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 408
    const-string v1, "getDevicesMatchingStates()"

    #@2
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@5
    .line 409
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@7
    if-eqz v1, :cond_2b

    #@9
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_2b

    #@f
    .line 411
    :try_start_f
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@11
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->getDevicesMatchingConnectionStates([I)Ljava/util/List;
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_16

    #@14
    move-result-object v1

    #@15
    .line 418
    :goto_15
    return-object v1

    #@16
    .line 412
    :catch_16
    move-exception v0

    #@17
    .line 413
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@19
    new-instance v2, Ljava/lang/Throwable;

    #@1b
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@1e
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 414
    new-instance v1, Ljava/util/ArrayList;

    #@27
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@2a
    goto :goto_15

    #@2b
    .line 417
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2b
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2d
    if-nez v1, :cond_36

    #@2f
    const-string v1, "BluetoothHeadset"

    #@31
    const-string v2, "Proxy not attached to service"

    #@33
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 418
    :cond_36
    new-instance v1, Ljava/util/ArrayList;

    #@38
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@3b
    goto :goto_15
.end method

.method public getPriority(Landroid/bluetooth/BluetoothDevice;)I
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 487
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "getPriority("

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    const-string v3, ")"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v2}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@1d
    .line 488
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@1f
    if-eqz v2, :cond_44

    #@21
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_44

    #@27
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_44

    #@2d
    .line 491
    :try_start_2d
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2f
    invoke-interface {v2, p1}, Landroid/bluetooth/IBluetoothHeadset;->getPriority(Landroid/bluetooth/BluetoothDevice;)I
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_32} :catch_34

    #@32
    move-result v1

    #@33
    .line 498
    :cond_33
    :goto_33
    return v1

    #@34
    .line 492
    :catch_34
    move-exception v0

    #@35
    .line 493
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothHeadset"

    #@37
    new-instance v3, Ljava/lang/Throwable;

    #@39
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@3c
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_33

    #@44
    .line 497
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_44
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@46
    if-nez v2, :cond_33

    #@48
    const-string v2, "BluetoothHeadset"

    #@4a
    const-string v3, "Proxy not attached to service"

    #@4c
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_33
.end method

.method public isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 570
    const-string v1, "isAudioConnected()"

    #@2
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@5
    .line 571
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@7
    if-eqz v1, :cond_2b

    #@9
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_2b

    #@f
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_2b

    #@15
    .line 574
    :try_start_15
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@17
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1a} :catch_1c

    #@1a
    move-result v1

    #@1b
    .line 580
    :goto_1b
    return v1

    #@1c
    .line 575
    :catch_1c
    move-exception v0

    #@1d
    .line 576
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@1f
    new-instance v2, Ljava/lang/Throwable;

    #@21
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@24
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 579
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2b
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2d
    if-nez v1, :cond_36

    #@2f
    const-string v1, "BluetoothHeadset"

    #@31
    const-string v2, "Proxy not attached to service"

    #@33
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 580
    :cond_36
    const/4 v1, 0x0

    #@37
    goto :goto_1b
.end method

.method public isAudioOn()Z
    .registers 4

    #@0
    .prologue
    .line 687
    const-string v1, "isAudioOn()"

    #@2
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@5
    .line 688
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@7
    if-eqz v1, :cond_25

    #@9
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_25

    #@f
    .line 690
    :try_start_f
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@11
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothHeadset;->isAudioOn()Z
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_16

    #@14
    move-result v1

    #@15
    .line 696
    :goto_15
    return v1

    #@16
    .line 691
    :catch_16
    move-exception v0

    #@17
    .line 692
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@19
    new-instance v2, Ljava/lang/Throwable;

    #@1b
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@1e
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 695
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_25
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@27
    if-nez v1, :cond_30

    #@29
    const-string v1, "BluetoothHeadset"

    #@2b
    const-string v2, "Proxy not attached to service"

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 696
    :cond_30
    const/4 v1, 0x0

    #@31
    goto :goto_15
.end method

.method public phoneStateChanged(IIILjava/lang/String;I)V
    .registers 13
    .parameter "numActive"
    .parameter "numHeld"
    .parameter "callState"
    .parameter "number"
    .parameter "type"

    #@0
    .prologue
    .line 805
    iget-object v0, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2
    if-eqz v0, :cond_20

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_20

    #@a
    .line 807
    :try_start_a
    iget-object v0, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@c
    move v1, p1

    #@d
    move v2, p2

    #@e
    move v3, p3

    #@f
    move-object v4, p4

    #@10
    move v5, p5

    #@11
    invoke-interface/range {v0 .. v5}, Landroid/bluetooth/IBluetoothHeadset;->phoneStateChanged(IIILjava/lang/String;I)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_14} :catch_15

    #@14
    .line 815
    :goto_14
    return-void

    #@15
    .line 808
    :catch_15
    move-exception v6

    #@16
    .line 809
    .local v6, e:Landroid/os/RemoteException;
    const-string v0, "BluetoothHeadset"

    #@18
    invoke-virtual {v6}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_14

    #@20
    .line 812
    .end local v6           #e:Landroid/os/RemoteException;
    :cond_20
    const-string v0, "BluetoothHeadset"

    #@22
    const-string v1, "Proxy not attached to service"

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 813
    const-string v0, "BluetoothHeadset"

    #@29
    new-instance v1, Ljava/lang/Throwable;

    #@2b
    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    #@2e
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_14
.end method

.method public rejectIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 646
    const-string/jumbo v1, "rejectIncomingConnect"

    #@3
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@6
    .line 647
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@8
    if-eqz v1, :cond_1d

    #@a
    .line 649
    :try_start_a
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@c
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->rejectIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-result v1

    #@10
    .line 655
    :goto_10
    return v1

    #@11
    .line 650
    :catch_11
    move-exception v0

    #@12
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@14
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 655
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_10

    #@1d
    .line 652
    :cond_1d
    const-string v1, "BluetoothHeadset"

    #@1f
    const-string v2, "Proxy not attached to service"

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 653
    const-string v1, "BluetoothHeadset"

    #@26
    new-instance v2, Ljava/lang/Throwable;

    #@28
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@2b
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_1b
.end method

.method public roamChanged(Z)V
    .registers 5
    .parameter "roaming"

    #@0
    .prologue
    .line 826
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2
    if-eqz v1, :cond_1b

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_1b

    #@a
    .line 828
    :try_start_a
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@c
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->roamChanged(Z)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_10

    #@f
    .line 836
    :goto_f
    return-void

    #@10
    .line 829
    :catch_10
    move-exception v0

    #@11
    .line 830
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@13
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    goto :goto_f

    #@1b
    .line 833
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_1b
    const-string v1, "BluetoothHeadset"

    #@1d
    const-string v2, "Proxy not attached to service"

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 834
    const-string v1, "BluetoothHeadset"

    #@24
    new-instance v2, Ljava/lang/Throwable;

    #@26
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@29
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_f
.end method

.method public setPriority(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 7
    .parameter "device"
    .parameter "priority"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 455
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string/jumbo v3, "setPriority("

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, ", "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ")"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v2}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@28
    .line 456
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2a
    if-eqz v2, :cond_56

    #@2c
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_56

    #@32
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@35
    move-result v2

    #@36
    if-eqz v2, :cond_56

    #@38
    .line 458
    if-eqz p2, :cond_3f

    #@3a
    const/16 v2, 0x64

    #@3c
    if-eq p2, v2, :cond_3f

    #@3e
    .line 470
    :cond_3e
    :goto_3e
    return v1

    #@3f
    .line 463
    :cond_3f
    :try_start_3f
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@41
    invoke-interface {v2, p1, p2}, Landroid/bluetooth/IBluetoothHeadset;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z
    :try_end_44
    .catch Landroid/os/RemoteException; {:try_start_3f .. :try_end_44} :catch_46

    #@44
    move-result v1

    #@45
    goto :goto_3e

    #@46
    .line 464
    :catch_46
    move-exception v0

    #@47
    .line 465
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothHeadset"

    #@49
    new-instance v3, Ljava/lang/Throwable;

    #@4b
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@4e
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@51
    move-result-object v3

    #@52
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_3e

    #@56
    .line 469
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_56
    iget-object v2, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@58
    if-nez v2, :cond_3e

    #@5a
    const-string v2, "BluetoothHeadset"

    #@5c
    const-string v3, "Proxy not attached to service"

    #@5e
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    goto :goto_3e
.end method

.method public startScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 758
    const-string/jumbo v1, "startScoUsingVirtualVoiceCall()"

    #@3
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@6
    .line 759
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@8
    if-eqz v1, :cond_29

    #@a
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_29

    #@10
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_29

    #@16
    .line 761
    :try_start_16
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@18
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->startScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1b} :catch_1d

    #@1b
    move-result v1

    #@1c
    .line 769
    :goto_1c
    return v1

    #@1d
    .line 762
    :catch_1d
    move-exception v0

    #@1e
    .line 763
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@20
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 769
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_27
    const/4 v1, 0x0

    #@28
    goto :goto_1c

    #@29
    .line 766
    :cond_29
    const-string v1, "BluetoothHeadset"

    #@2b
    const-string v2, "Proxy not attached to service"

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 767
    const-string v1, "BluetoothHeadset"

    #@32
    new-instance v2, Ljava/lang/Throwable;

    #@34
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@37
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_27
.end method

.method public startVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 523
    const-string/jumbo v1, "startVoiceRecognition()"

    #@3
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@6
    .line 524
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@8
    if-eqz v1, :cond_2c

    #@a
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_2c

    #@10
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_2c

    #@16
    .line 527
    :try_start_16
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@18
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->startVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1b} :catch_1d

    #@1b
    move-result v1

    #@1c
    .line 533
    :goto_1c
    return v1

    #@1d
    .line 528
    :catch_1d
    move-exception v0

    #@1e
    .line 529
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@20
    new-instance v2, Ljava/lang/Throwable;

    #@22
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@25
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 532
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2c
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2e
    if-nez v1, :cond_37

    #@30
    const-string v1, "BluetoothHeadset"

    #@32
    const-string v2, "Proxy not attached to service"

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 533
    :cond_37
    const/4 v1, 0x0

    #@38
    goto :goto_1c
.end method

.method public stopScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 781
    const-string/jumbo v1, "stopScoUsingVirtualVoiceCall()"

    #@3
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@6
    .line 782
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@8
    if-eqz v1, :cond_29

    #@a
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_29

    #@10
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_29

    #@16
    .line 784
    :try_start_16
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@18
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->stopScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1b} :catch_1d

    #@1b
    move-result v1

    #@1c
    .line 792
    :goto_1c
    return v1

    #@1d
    .line 785
    :catch_1d
    move-exception v0

    #@1e
    .line 786
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@20
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 792
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_27
    const/4 v1, 0x0

    #@28
    goto :goto_1c

    #@29
    .line 789
    :cond_29
    const-string v1, "BluetoothHeadset"

    #@2b
    const-string v2, "Proxy not attached to service"

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 790
    const-string v1, "BluetoothHeadset"

    #@32
    new-instance v2, Ljava/lang/Throwable;

    #@34
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@37
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_27
.end method

.method public stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 547
    const-string/jumbo v1, "stopVoiceRecognition()"

    #@3
    invoke-static {v1}, Landroid/bluetooth/BluetoothHeadset;->log(Ljava/lang/String;)V

    #@6
    .line 548
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@8
    if-eqz v1, :cond_2c

    #@a
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHeadset;->isEnabled()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_2c

    #@10
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHeadset;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_2c

    #@16
    .line 551
    :try_start_16
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@18
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadset;->stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1b} :catch_1d

    #@1b
    move-result v1

    #@1c
    .line 557
    :goto_1c
    return v1

    #@1d
    .line 552
    :catch_1d
    move-exception v0

    #@1e
    .line 553
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHeadset"

    #@20
    new-instance v2, Ljava/lang/Throwable;

    #@22
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@25
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 556
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2c
    iget-object v1, p0, Landroid/bluetooth/BluetoothHeadset;->mService:Landroid/bluetooth/IBluetoothHeadset;

    #@2e
    if-nez v1, :cond_37

    #@30
    const-string v1, "BluetoothHeadset"

    #@32
    const-string v2, "Proxy not attached to service"

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 557
    :cond_37
    const/4 v1, 0x0

    #@38
    goto :goto_1c
.end method
