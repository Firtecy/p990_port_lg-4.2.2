.class public abstract Landroid/bluetooth/IBluetoothHealth$Stub;
.super Landroid/os/Binder;
.source "IBluetoothHealth.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothHealth;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothHealth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/IBluetoothHealth$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.bluetooth.IBluetoothHealth"

.field static final TRANSACTION_connectChannelToSink:I = 0x4

.field static final TRANSACTION_connectChannelToSource:I = 0x3

.field static final TRANSACTION_disconnectChannel:I = 0x5

.field static final TRANSACTION_getConnectedHealthDevices:I = 0x8

.field static final TRANSACTION_getDeviceDatatype:I = 0x6

.field static final TRANSACTION_getHealthDeviceConnectionState:I = 0xa

.field static final TRANSACTION_getHealthDevicesMatchingConnectionStates:I = 0x9

.field static final TRANSACTION_getMainChannelFd:I = 0x7

.field static final TRANSACTION_registerAppConfiguration:I = 0x1

.field static final TRANSACTION_unregisterAppConfiguration:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.bluetooth.IBluetoothHealth"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/bluetooth/IBluetoothHealth$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothHealth;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.bluetooth.IBluetoothHealth"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/bluetooth/IBluetoothHealth;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/bluetooth/IBluetoothHealth;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/bluetooth/IBluetoothHealth$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/bluetooth/IBluetoothHealth$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_1b8

    #@5
    .line 236
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v6

    #@9
    :goto_9
    return v6

    #@a
    .line 47
    :sswitch_a
    const-string v5, "android.bluetooth.IBluetoothHealth"

    #@c
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v7, "android.bluetooth.IBluetoothHealth"

    #@12
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v7

    #@19
    if-eqz v7, :cond_39

    #@1b
    .line 55
    sget-object v7, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@23
    .line 61
    .local v0, _arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_23
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@26
    move-result-object v7

    #@27
    invoke-static {v7}, Landroid/bluetooth/IBluetoothHealthCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothHealthCallback;

    #@2a
    move-result-object v1

    #@2b
    .line 62
    .local v1, _arg1:Landroid/bluetooth/IBluetoothHealthCallback;
    invoke-virtual {p0, v0, v1}, Landroid/bluetooth/IBluetoothHealth$Stub;->registerAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/IBluetoothHealthCallback;)Z

    #@2e
    move-result v3

    #@2f
    .line 63
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@32
    .line 64
    if-eqz v3, :cond_35

    #@34
    move v5, v6

    #@35
    :cond_35
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    goto :goto_9

    #@39
    .line 58
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v1           #_arg1:Landroid/bluetooth/IBluetoothHealthCallback;
    .end local v3           #_result:Z
    :cond_39
    const/4 v0, 0x0

    #@3a
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_23

    #@3b
    .line 69
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :sswitch_3b
    const-string v7, "android.bluetooth.IBluetoothHealth"

    #@3d
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40
    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v7

    #@44
    if-eqz v7, :cond_5c

    #@46
    .line 72
    sget-object v7, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@48
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4b
    move-result-object v0

    #@4c
    check-cast v0, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@4e
    .line 77
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_4e
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothHealth$Stub;->unregisterAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z

    #@51
    move-result v3

    #@52
    .line 78
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@55
    .line 79
    if-eqz v3, :cond_58

    #@57
    move v5, v6

    #@58
    :cond_58
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@5b
    goto :goto_9

    #@5c
    .line 75
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v3           #_result:Z
    :cond_5c
    const/4 v0, 0x0

    #@5d
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_4e

    #@5e
    .line 84
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :sswitch_5e
    const-string v7, "android.bluetooth.IBluetoothHealth"

    #@60
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@63
    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@66
    move-result v7

    #@67
    if-eqz v7, :cond_8e

    #@69
    .line 87
    sget-object v7, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6b
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6e
    move-result-object v0

    #@6f
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@71
    .line 93
    .local v0, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@74
    move-result v7

    #@75
    if-eqz v7, :cond_90

    #@77
    .line 94
    sget-object v7, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@79
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7c
    move-result-object v1

    #@7d
    check-cast v1, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@7f
    .line 99
    .local v1, _arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_7f
    invoke-virtual {p0, v0, v1}, Landroid/bluetooth/IBluetoothHealth$Stub;->connectChannelToSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z

    #@82
    move-result v3

    #@83
    .line 100
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@86
    .line 101
    if-eqz v3, :cond_89

    #@88
    move v5, v6

    #@89
    :cond_89
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@8c
    goto/16 :goto_9

    #@8e
    .line 90
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v3           #_result:Z
    :cond_8e
    const/4 v0, 0x0

    #@8f
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_71

    #@90
    .line 97
    :cond_90
    const/4 v1, 0x0

    #@91
    .restart local v1       #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_7f

    #@92
    .line 106
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :sswitch_92
    const-string v7, "android.bluetooth.IBluetoothHealth"

    #@94
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@97
    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9a
    move-result v7

    #@9b
    if-eqz v7, :cond_c6

    #@9d
    .line 109
    sget-object v7, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@9f
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@a2
    move-result-object v0

    #@a3
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@a5
    .line 115
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_a5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a8
    move-result v7

    #@a9
    if-eqz v7, :cond_c8

    #@ab
    .line 116
    sget-object v7, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ad
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b0
    move-result-object v1

    #@b1
    check-cast v1, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@b3
    .line 122
    .restart local v1       #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_b3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b6
    move-result v2

    #@b7
    .line 123
    .local v2, _arg2:I
    invoke-virtual {p0, v0, v1, v2}, Landroid/bluetooth/IBluetoothHealth$Stub;->connectChannelToSink(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z

    #@ba
    move-result v3

    #@bb
    .line 124
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@be
    .line 125
    if-eqz v3, :cond_c1

    #@c0
    move v5, v6

    #@c1
    :cond_c1
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@c4
    goto/16 :goto_9

    #@c6
    .line 112
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v2           #_arg2:I
    .end local v3           #_result:Z
    :cond_c6
    const/4 v0, 0x0

    #@c7
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_a5

    #@c8
    .line 119
    :cond_c8
    const/4 v1, 0x0

    #@c9
    .restart local v1       #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_b3

    #@ca
    .line 130
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :sswitch_ca
    const-string v7, "android.bluetooth.IBluetoothHealth"

    #@cc
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cf
    .line 132
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d2
    move-result v7

    #@d3
    if-eqz v7, :cond_fe

    #@d5
    .line 133
    sget-object v7, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@d7
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@da
    move-result-object v0

    #@db
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@dd
    .line 139
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_dd
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e0
    move-result v7

    #@e1
    if-eqz v7, :cond_100

    #@e3
    .line 140
    sget-object v7, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e5
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e8
    move-result-object v1

    #@e9
    check-cast v1, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@eb
    .line 146
    .restart local v1       #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_eb
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ee
    move-result v2

    #@ef
    .line 147
    .restart local v2       #_arg2:I
    invoke-virtual {p0, v0, v1, v2}, Landroid/bluetooth/IBluetoothHealth$Stub;->disconnectChannel(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z

    #@f2
    move-result v3

    #@f3
    .line 148
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f6
    .line 149
    if-eqz v3, :cond_f9

    #@f8
    move v5, v6

    #@f9
    :cond_f9
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@fc
    goto/16 :goto_9

    #@fe
    .line 136
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v2           #_arg2:I
    .end local v3           #_result:Z
    :cond_fe
    const/4 v0, 0x0

    #@ff
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_dd

    #@100
    .line 143
    :cond_100
    const/4 v1, 0x0

    #@101
    .restart local v1       #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_eb

    #@102
    .line 154
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :sswitch_102
    const-string v7, "android.bluetooth.IBluetoothHealth"

    #@104
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@107
    .line 156
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10a
    move-result v7

    #@10b
    if-eqz v7, :cond_132

    #@10d
    .line 157
    sget-object v7, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@10f
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@112
    move-result-object v0

    #@113
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@115
    .line 163
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_115
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@118
    move-result v7

    #@119
    if-eqz v7, :cond_134

    #@11b
    .line 164
    sget-object v7, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@11d
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@120
    move-result-object v1

    #@121
    check-cast v1, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@123
    .line 169
    .restart local v1       #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_123
    invoke-virtual {p0, v0, v1}, Landroid/bluetooth/IBluetoothHealth$Stub;->getDeviceDatatype(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z

    #@126
    move-result v3

    #@127
    .line 170
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@12a
    .line 171
    if-eqz v3, :cond_12d

    #@12c
    move v5, v6

    #@12d
    :cond_12d
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@130
    goto/16 :goto_9

    #@132
    .line 160
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v3           #_result:Z
    :cond_132
    const/4 v0, 0x0

    #@133
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_115

    #@134
    .line 167
    :cond_134
    const/4 v1, 0x0

    #@135
    .restart local v1       #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_123

    #@136
    .line 176
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :sswitch_136
    const-string v7, "android.bluetooth.IBluetoothHealth"

    #@138
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13b
    .line 178
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@13e
    move-result v7

    #@13f
    if-eqz v7, :cond_168

    #@141
    .line 179
    sget-object v7, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@143
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@146
    move-result-object v0

    #@147
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@149
    .line 185
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_149
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@14c
    move-result v7

    #@14d
    if-eqz v7, :cond_16a

    #@14f
    .line 186
    sget-object v7, Landroid/bluetooth/BluetoothHealthAppConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@151
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@154
    move-result-object v1

    #@155
    check-cast v1, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@157
    .line 191
    .restart local v1       #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :goto_157
    invoke-virtual {p0, v0, v1}, Landroid/bluetooth/IBluetoothHealth$Stub;->getMainChannelFd(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/os/ParcelFileDescriptor;

    #@15a
    move-result-object v3

    #@15b
    .line 192
    .local v3, _result:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@15e
    .line 193
    if-eqz v3, :cond_16c

    #@160
    .line 194
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@163
    .line 195
    invoke-virtual {v3, p3, v6}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@166
    goto/16 :goto_9

    #@168
    .line 182
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v3           #_result:Landroid/os/ParcelFileDescriptor;
    :cond_168
    const/4 v0, 0x0

    #@169
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_149

    #@16a
    .line 189
    :cond_16a
    const/4 v1, 0x0

    #@16b
    .restart local v1       #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    goto :goto_157

    #@16c
    .line 198
    .restart local v3       #_result:Landroid/os/ParcelFileDescriptor;
    :cond_16c
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@16f
    goto/16 :goto_9

    #@171
    .line 204
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v3           #_result:Landroid/os/ParcelFileDescriptor;
    :sswitch_171
    const-string v5, "android.bluetooth.IBluetoothHealth"

    #@173
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@176
    .line 205
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHealth$Stub;->getConnectedHealthDevices()Ljava/util/List;

    #@179
    move-result-object v4

    #@17a
    .line 206
    .local v4, _result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@17d
    .line 207
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@180
    goto/16 :goto_9

    #@182
    .line 212
    .end local v4           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :sswitch_182
    const-string v5, "android.bluetooth.IBluetoothHealth"

    #@184
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@187
    .line 214
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@18a
    move-result-object v0

    #@18b
    .line 215
    .local v0, _arg0:[I
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothHealth$Stub;->getHealthDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@18e
    move-result-object v4

    #@18f
    .line 216
    .restart local v4       #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@192
    .line 217
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@195
    goto/16 :goto_9

    #@197
    .line 222
    .end local v0           #_arg0:[I
    .end local v4           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :sswitch_197
    const-string v5, "android.bluetooth.IBluetoothHealth"

    #@199
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19c
    .line 224
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@19f
    move-result v5

    #@1a0
    if-eqz v5, :cond_1b6

    #@1a2
    .line 225
    sget-object v5, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a4
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1a7
    move-result-object v0

    #@1a8
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@1aa
    .line 230
    .local v0, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_1aa
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothHealth$Stub;->getHealthDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@1ad
    move-result v3

    #@1ae
    .line 231
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b1
    .line 232
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b4
    goto/16 :goto_9

    #@1b6
    .line 228
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v3           #_result:I
    :cond_1b6
    const/4 v0, 0x0

    #@1b7
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_1aa

    #@1b8
    .line 43
    :sswitch_data_1b8
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_3b
        0x3 -> :sswitch_5e
        0x4 -> :sswitch_92
        0x5 -> :sswitch_ca
        0x6 -> :sswitch_102
        0x7 -> :sswitch_136
        0x8 -> :sswitch_171
        0x9 -> :sswitch_182
        0xa -> :sswitch_197
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
