.class Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;
.super Ljava/lang/Object;
.source "IBluetoothHeadsetPhone.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothHeadsetPhone;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 148
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 149
    iput-object p1, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 150
    return-void
.end method


# virtual methods
.method public answerCall()Z
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 163
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 164
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 167
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothHeadsetPhone"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 168
    iget-object v4, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v5, 0x1

    #@12
    const/4 v6, 0x0

    #@13
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 169
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 170
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_a .. :try_end_1c} :catchall_28

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_26

    #@1f
    .line 173
    .local v2, _result:Z
    :goto_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 174
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 176
    return v2

    #@26
    .end local v2           #_result:Z
    :cond_26
    move v2, v3

    #@27
    .line 170
    goto :goto_1f

    #@28
    .line 173
    :catchall_28
    move-exception v3

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 174
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v3
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 153
    iget-object v0, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public cdmaSetSecondCallState(Z)V
    .registers 7
    .parameter "state"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 331
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 332
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 334
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.bluetooth.IBluetoothHeadsetPhone"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 335
    if-eqz p1, :cond_11

    #@10
    const/4 v2, 0x1

    #@11
    :cond_11
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 336
    iget-object v2, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v3, 0xb

    #@18
    const/4 v4, 0x0

    #@19
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 337
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_26

    #@1f
    .line 340
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 341
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 343
    return-void

    #@26
    .line 340
    :catchall_26
    move-exception v2

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 341
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v2
.end method

.method public cdmaSwapSecondCallState()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 317
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 318
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 320
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.bluetooth.IBluetoothHeadsetPhone"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 321
    iget-object v2, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0xa

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 322
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 325
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 326
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 328
    return-void

    #@1f
    .line 325
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 326
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 157
    const-string v0, "android.bluetooth.IBluetoothHeadsetPhone"

    #@2
    return-object v0
.end method

.method public getNetworkOperator()Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 233
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 234
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 237
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.bluetooth.IBluetoothHeadsetPhone"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 238
    iget-object v3, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x5

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 239
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 240
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result-object v2

    #@1b
    .line 243
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 244
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 246
    return-object v2

    #@22
    .line 243
    .end local v2           #_result:Ljava/lang/String;
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 244
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public getSubscriberNumber()Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 250
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 251
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 254
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.bluetooth.IBluetoothHeadsetPhone"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 255
    iget-object v3, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x6

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 256
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 257
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result-object v2

    #@1b
    .line 260
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 261
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 263
    return-object v2

    #@22
    .line 260
    .end local v2           #_result:Ljava/lang/String;
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 261
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public hangupCall()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 180
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 181
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 184
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.bluetooth.IBluetoothHeadsetPhone"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 185
    iget-object v3, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x2

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 186
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 187
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 190
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 191
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 193
    return v2

    #@26
    .line 190
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 191
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public listCurrentCalls()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 267
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 268
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 271
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.bluetooth.IBluetoothHeadsetPhone"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 272
    iget-object v3, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x7

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 273
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 274
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 277
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 278
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 280
    return v2

    #@26
    .line 277
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 278
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public processChld(I)Z
    .registers 8
    .parameter "chld"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 215
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 216
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 219
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.bluetooth.IBluetoothHeadsetPhone"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 220
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 221
    iget-object v3, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x4

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 222
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 223
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_29

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 226
    .local v2, _result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 227
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 229
    return v2

    #@29
    .line 226
    .end local v2           #_result:Z
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 227
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public queryPhoneState()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 284
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 285
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 288
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.bluetooth.IBluetoothHeadsetPhone"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 289
    iget-object v3, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0x8

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 290
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 291
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 294
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 295
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 297
    return v2

    #@27
    .line 294
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 295
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public sendDtmf(I)Z
    .registers 8
    .parameter "dtmf"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 197
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 198
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 201
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.bluetooth.IBluetoothHeadsetPhone"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 202
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 203
    iget-object v3, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x3

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 204
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 205
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_29

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 208
    .local v2, _result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 209
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 211
    return v2

    #@29
    .line 208
    .end local v2           #_result:Z
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 209
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public updateBtHandsfreeAfterRadioTechnologyChange()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 303
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 304
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 306
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.bluetooth.IBluetoothHeadsetPhone"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 307
    iget-object v2, p0, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x9

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 308
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 311
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 312
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 314
    return-void

    #@1f
    .line 311
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 312
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method
