.class public final Landroid/bluetooth/BluetoothServerSocket;
.super Ljava/lang/Object;
.source "BluetoothServerSocket.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final mChannel:I

.field private mHandler:Landroid/os/Handler;

.field private mMessage:I

.field final mSocket:Landroid/bluetooth/BluetoothSocket;


# direct methods
.method constructor <init>(IZZI)V
    .registers 13
    .parameter "type"
    .parameter "auth"
    .parameter "encrypt"
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 86
    iput p4, p0, Landroid/bluetooth/BluetoothServerSocket;->mChannel:I

    #@6
    .line 87
    new-instance v0, Landroid/bluetooth/BluetoothSocket;

    #@8
    const/4 v2, -0x1

    #@9
    move v1, p1

    #@a
    move v3, p2

    #@b
    move v4, p3

    #@c
    move v6, p4

    #@d
    move-object v7, v5

    #@e
    invoke-direct/range {v0 .. v7}, Landroid/bluetooth/BluetoothSocket;-><init>(IIZZLandroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;)V

    #@11
    iput-object v0, p0, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@13
    .line 88
    return-void
.end method

.method constructor <init>(IZZLandroid/os/ParcelUuid;)V
    .registers 13
    .parameter "type"
    .parameter "auth"
    .parameter "encrypt"
    .parameter "uuid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 100
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 101
    new-instance v0, Landroid/bluetooth/BluetoothSocket;

    #@6
    const/4 v5, 0x0

    #@7
    move v1, p1

    #@8
    move v3, p2

    #@9
    move v4, p3

    #@a
    move v6, v2

    #@b
    move-object v7, p4

    #@c
    invoke-direct/range {v0 .. v7}, Landroid/bluetooth/BluetoothSocket;-><init>(IIZZLandroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;)V

    #@f
    iput-object v0, p0, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@11
    .line 102
    iget-object v0, p0, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@13
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getPort()I

    #@16
    move-result v0

    #@17
    iput v0, p0, Landroid/bluetooth/BluetoothServerSocket;->mChannel:I

    #@19
    .line 103
    return-void
.end method


# virtual methods
.method public accept()Landroid/bluetooth/BluetoothSocket;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 117
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/bluetooth/BluetoothServerSocket;->accept(I)Landroid/bluetooth/BluetoothSocket;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public accept(I)Landroid/bluetooth/BluetoothSocket;
    .registers 3
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothSocket;->accept(I)Landroid/bluetooth/BluetoothSocket;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 142
    monitor-enter p0

    #@1
    .line 143
    :try_start_1
    iget-object v0, p0, Landroid/bluetooth/BluetoothServerSocket;->mHandler:Landroid/os/Handler;

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 144
    iget-object v0, p0, Landroid/bluetooth/BluetoothServerSocket;->mHandler:Landroid/os/Handler;

    #@7
    iget v1, p0, Landroid/bluetooth/BluetoothServerSocket;->mMessage:I

    #@9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@10
    .line 146
    :cond_10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_17

    #@11
    .line 147
    iget-object v0, p0, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@13
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V

    #@16
    .line 148
    return-void

    #@17
    .line 146
    :catchall_17
    move-exception v0

    #@18
    :try_start_18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_18 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method public getChannel()I
    .registers 2

    #@0
    .prologue
    .line 162
    iget v0, p0, Landroid/bluetooth/BluetoothServerSocket;->mChannel:I

    #@2
    return v0
.end method

.method declared-synchronized setCloseHandler(Landroid/os/Handler;I)V
    .registers 4
    .parameter "handler"
    .parameter "message"

    #@0
    .prologue
    .line 151
    monitor-enter p0

    #@1
    :try_start_1
    iput-object p1, p0, Landroid/bluetooth/BluetoothServerSocket;->mHandler:Landroid/os/Handler;

    #@3
    .line 152
    iput p2, p0, Landroid/bluetooth/BluetoothServerSocket;->mMessage:I
    :try_end_5
    .catchall {:try_start_1 .. :try_end_5} :catchall_7

    #@5
    .line 153
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 151
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method setServiceName(Ljava/lang/String;)V
    .registers 3
    .parameter "ServiceName"

    #@0
    .prologue
    .line 155
    iget-object v0, p0, Landroid/bluetooth/BluetoothServerSocket;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothSocket;->setServiceName(Ljava/lang/String;)V

    #@5
    .line 156
    return-void
.end method
