.class public final Landroid/bluetooth/BluetoothUuid;
.super Ljava/lang/Object;
.source "BluetoothUuid.java"


# static fields
.field public static final AdvAudioDist:Landroid/os/ParcelUuid;

.field public static final AudioSink:Landroid/os/ParcelUuid;

.field public static final AudioSource:Landroid/os/ParcelUuid;

.field public static final AvrcpController:Landroid/os/ParcelUuid;

.field public static final AvrcpTarget:Landroid/os/ParcelUuid;

.field public static final BNEP:Landroid/os/ParcelUuid;

.field public static final HSP:Landroid/os/ParcelUuid;

.field public static final HSP_AG:Landroid/os/ParcelUuid;

.field public static final Handsfree:Landroid/os/ParcelUuid;

.field public static final Handsfree_AG:Landroid/os/ParcelUuid;

.field public static final Hid:Landroid/os/ParcelUuid;

.field public static final NAP:Landroid/os/ParcelUuid;

.field public static final ObexObjectPush:Landroid/os/ParcelUuid;

.field public static final PANU:Landroid/os/ParcelUuid;

.field public static final PBAP_PSE:Landroid/os/ParcelUuid;

.field public static final RESERVED_UUIDS:[Landroid/os/ParcelUuid;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 37
    const-string v0, "0000110B-0000-1000-8000-00805F9B34FB"

    #@2
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->AudioSink:Landroid/os/ParcelUuid;

    #@8
    .line 39
    const-string v0, "0000110A-0000-1000-8000-00805F9B34FB"

    #@a
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->AudioSource:Landroid/os/ParcelUuid;

    #@10
    .line 41
    const-string v0, "0000110D-0000-1000-8000-00805F9B34FB"

    #@12
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@15
    move-result-object v0

    #@16
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->AdvAudioDist:Landroid/os/ParcelUuid;

    #@18
    .line 43
    const-string v0, "00001108-0000-1000-8000-00805F9B34FB"

    #@1a
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@1d
    move-result-object v0

    #@1e
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->HSP:Landroid/os/ParcelUuid;

    #@20
    .line 45
    const-string v0, "00001112-0000-1000-8000-00805F9B34FB"

    #@22
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@25
    move-result-object v0

    #@26
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->HSP_AG:Landroid/os/ParcelUuid;

    #@28
    .line 47
    const-string v0, "0000111E-0000-1000-8000-00805F9B34FB"

    #@2a
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@2d
    move-result-object v0

    #@2e
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->Handsfree:Landroid/os/ParcelUuid;

    #@30
    .line 49
    const-string v0, "0000111F-0000-1000-8000-00805F9B34FB"

    #@32
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@35
    move-result-object v0

    #@36
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->Handsfree_AG:Landroid/os/ParcelUuid;

    #@38
    .line 51
    const-string v0, "0000110E-0000-1000-8000-00805F9B34FB"

    #@3a
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@3d
    move-result-object v0

    #@3e
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->AvrcpController:Landroid/os/ParcelUuid;

    #@40
    .line 53
    const-string v0, "0000110C-0000-1000-8000-00805F9B34FB"

    #@42
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@45
    move-result-object v0

    #@46
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->AvrcpTarget:Landroid/os/ParcelUuid;

    #@48
    .line 55
    const-string v0, "00001105-0000-1000-8000-00805f9b34fb"

    #@4a
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@4d
    move-result-object v0

    #@4e
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->ObexObjectPush:Landroid/os/ParcelUuid;

    #@50
    .line 57
    const-string v0, "00001124-0000-1000-8000-00805f9b34fb"

    #@52
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@55
    move-result-object v0

    #@56
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->Hid:Landroid/os/ParcelUuid;

    #@58
    .line 59
    const-string v0, "00001115-0000-1000-8000-00805F9B34FB"

    #@5a
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@5d
    move-result-object v0

    #@5e
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->PANU:Landroid/os/ParcelUuid;

    #@60
    .line 61
    const-string v0, "00001116-0000-1000-8000-00805F9B34FB"

    #@62
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@65
    move-result-object v0

    #@66
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->NAP:Landroid/os/ParcelUuid;

    #@68
    .line 63
    const-string v0, "0000000f-0000-1000-8000-00805F9B34FB"

    #@6a
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@6d
    move-result-object v0

    #@6e
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->BNEP:Landroid/os/ParcelUuid;

    #@70
    .line 65
    const-string v0, "0000112f-0000-1000-8000-00805F9B34FB"

    #@72
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    #@75
    move-result-object v0

    #@76
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->PBAP_PSE:Landroid/os/ParcelUuid;

    #@78
    .line 68
    const/16 v0, 0xa

    #@7a
    new-array v0, v0, [Landroid/os/ParcelUuid;

    #@7c
    const/4 v1, 0x0

    #@7d
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->AudioSink:Landroid/os/ParcelUuid;

    #@7f
    aput-object v2, v0, v1

    #@81
    const/4 v1, 0x1

    #@82
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->AudioSource:Landroid/os/ParcelUuid;

    #@84
    aput-object v2, v0, v1

    #@86
    const/4 v1, 0x2

    #@87
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->AdvAudioDist:Landroid/os/ParcelUuid;

    #@89
    aput-object v2, v0, v1

    #@8b
    const/4 v1, 0x3

    #@8c
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->HSP:Landroid/os/ParcelUuid;

    #@8e
    aput-object v2, v0, v1

    #@90
    const/4 v1, 0x4

    #@91
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->Handsfree:Landroid/os/ParcelUuid;

    #@93
    aput-object v2, v0, v1

    #@95
    const/4 v1, 0x5

    #@96
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->AvrcpController:Landroid/os/ParcelUuid;

    #@98
    aput-object v2, v0, v1

    #@9a
    const/4 v1, 0x6

    #@9b
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->AvrcpTarget:Landroid/os/ParcelUuid;

    #@9d
    aput-object v2, v0, v1

    #@9f
    const/4 v1, 0x7

    #@a0
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->ObexObjectPush:Landroid/os/ParcelUuid;

    #@a2
    aput-object v2, v0, v1

    #@a4
    const/16 v1, 0x8

    #@a6
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->PANU:Landroid/os/ParcelUuid;

    #@a8
    aput-object v2, v0, v1

    #@aa
    const/16 v1, 0x9

    #@ac
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->NAP:Landroid/os/ParcelUuid;

    #@ae
    aput-object v2, v0, v1

    #@b0
    sput-object v0, Landroid/bluetooth/BluetoothUuid;->RESERVED_UUIDS:[Landroid/os/ParcelUuid;

    #@b2
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static containsAllUuids([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;)Z
    .registers 10
    .parameter "uuidA"
    .parameter "uuidB"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 168
    if-nez p0, :cond_7

    #@4
    if-nez p1, :cond_7

    #@6
    .line 180
    :cond_6
    :goto_6
    return v5

    #@7
    .line 170
    :cond_7
    if-nez p0, :cond_e

    #@9
    .line 171
    array-length v7, p1

    #@a
    if-eqz v7, :cond_6

    #@c
    move v5, v6

    #@d
    goto :goto_6

    #@e
    .line 174
    :cond_e
    if-eqz p1, :cond_6

    #@10
    .line 176
    new-instance v4, Ljava/util/HashSet;

    #@12
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@15
    move-result-object v7

    #@16
    invoke-direct {v4, v7}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@19
    .line 177
    .local v4, uuidSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/os/ParcelUuid;>;"
    move-object v0, p1

    #@1a
    .local v0, arr$:[Landroid/os/ParcelUuid;
    array-length v2, v0

    #@1b
    .local v2, len$:I
    const/4 v1, 0x0

    #@1c
    .local v1, i$:I
    :goto_1c
    if-ge v1, v2, :cond_6

    #@1e
    aget-object v3, v0, v1

    #@20
    .line 178
    .local v3, uuid:Landroid/os/ParcelUuid;
    invoke-virtual {v4, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@23
    move-result v7

    #@24
    if-nez v7, :cond_28

    #@26
    move v5, v6

    #@27
    goto :goto_6

    #@28
    .line 177
    :cond_28
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_1c
.end method

.method public static containsAnyUuid([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;)Z
    .registers 10
    .parameter "uuidA"
    .parameter "uuidB"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 142
    if-nez p0, :cond_7

    #@4
    if-nez p1, :cond_7

    #@6
    .line 156
    :cond_6
    :goto_6
    return v5

    #@7
    .line 144
    :cond_7
    if-nez p0, :cond_e

    #@9
    .line 145
    array-length v7, p1

    #@a
    if-eqz v7, :cond_6

    #@c
    move v5, v6

    #@d
    goto :goto_6

    #@e
    .line 148
    :cond_e
    if-nez p1, :cond_15

    #@10
    .line 149
    array-length v7, p0

    #@11
    if-eqz v7, :cond_6

    #@13
    move v5, v6

    #@14
    goto :goto_6

    #@15
    .line 152
    :cond_15
    new-instance v4, Ljava/util/HashSet;

    #@17
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@1a
    move-result-object v7

    #@1b
    invoke-direct {v4, v7}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@1e
    .line 153
    .local v4, uuidSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/os/ParcelUuid;>;"
    move-object v0, p1

    #@1f
    .local v0, arr$:[Landroid/os/ParcelUuid;
    array-length v2, v0

    #@20
    .local v2, len$:I
    const/4 v1, 0x0

    #@21
    .local v1, i$:I
    :goto_21
    if-ge v1, v2, :cond_2e

    #@23
    aget-object v3, v0, v1

    #@25
    .line 154
    .local v3, uuid:Landroid/os/ParcelUuid;
    invoke-virtual {v4, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@28
    move-result v7

    #@29
    if-nez v7, :cond_6

    #@2b
    .line 153
    add-int/lit8 v1, v1, 0x1

    #@2d
    goto :goto_21

    #@2e
    .end local v3           #uuid:Landroid/os/ParcelUuid;
    :cond_2e
    move v5, v6

    #@2f
    .line 156
    goto :goto_6
.end method

.method public static getServiceIdentifierFromParcelUuid(Landroid/os/ParcelUuid;)I
    .registers 8
    .parameter "parcelUuid"

    #@0
    .prologue
    .line 191
    invoke-virtual {p0}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@3
    move-result-object v0

    #@4
    .line 192
    .local v0, uuid:Ljava/util/UUID;
    invoke-virtual {v0}, Ljava/util/UUID;->getMostSignificantBits()J

    #@7
    move-result-wide v3

    #@8
    const-wide v5, 0xffff00000000L

    #@d
    and-long/2addr v3, v5

    #@e
    const/16 v5, 0x20

    #@10
    ushr-long v1, v3, v5

    #@12
    .line 193
    .local v1, value:J
    long-to-int v3, v1

    #@13
    return v3
.end method

.method public static isAdvAudioDist(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 81
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->AdvAudioDist:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isAudioSink(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 77
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->AudioSink:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isAudioSource(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 73
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->AudioSource:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isAvrcpController(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 93
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->AvrcpController:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isAvrcpTarget(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 97
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->AvrcpTarget:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isBnep(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 113
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->BNEP:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isHandsfree(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 85
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Handsfree:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isHeadset(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 89
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->HSP:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isInputDevice(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 101
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Hid:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isNap(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 109
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->NAP:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isPanu(Landroid/os/ParcelUuid;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 105
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->PANU:Landroid/os/ParcelUuid;

    #@2
    invoke-virtual {p0, v0}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z
    .registers 9
    .parameter "uuidArray"
    .parameter "uuid"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 122
    if-eqz p0, :cond_7

    #@4
    array-length v6, p0

    #@5
    if-nez v6, :cond_b

    #@7
    :cond_7
    if-nez p1, :cond_b

    #@9
    move v4, v5

    #@a
    .line 131
    :cond_a
    :goto_a
    return v4

    #@b
    .line 125
    :cond_b
    if-eqz p0, :cond_a

    #@d
    .line 128
    move-object v0, p0

    #@e
    .local v0, arr$:[Landroid/os/ParcelUuid;
    array-length v3, v0

    #@f
    .local v3, len$:I
    const/4 v2, 0x0

    #@10
    .local v2, i$:I
    :goto_10
    if-ge v2, v3, :cond_a

    #@12
    aget-object v1, v0, v2

    #@14
    .line 129
    .local v1, element:Landroid/os/ParcelUuid;
    invoke-virtual {v1, p1}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v6

    #@18
    if-eqz v6, :cond_1c

    #@1a
    move v4, v5

    #@1b
    goto :goto_a

    #@1c
    .line 128
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    #@1e
    goto :goto_10
.end method
