.class Landroid/bluetooth/BluetoothPan$1;
.super Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;
.source "BluetoothPan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/BluetoothPan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/bluetooth/BluetoothPan;


# direct methods
.method constructor <init>(Landroid/bluetooth/BluetoothPan;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 166
    iput-object p1, p0, Landroid/bluetooth/BluetoothPan$1;->this$0:Landroid/bluetooth/BluetoothPan;

    #@2
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onBluetoothStateChange(Z)V
    .registers 7
    .parameter "on"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 171
    if-eqz p1, :cond_37

    #@2
    .line 172
    const-string v1, "BluetoothPan"

    #@4
    const-string/jumbo v2, "onBluetoothStateChange(on) call bindService"

    #@7
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 173
    iget-object v1, p0, Landroid/bluetooth/BluetoothPan$1;->this$0:Landroid/bluetooth/BluetoothPan;

    #@c
    invoke-static {v1}, Landroid/bluetooth/BluetoothPan;->access$100(Landroid/bluetooth/BluetoothPan;)Landroid/content/Context;

    #@f
    move-result-object v1

    #@10
    new-instance v2, Landroid/content/Intent;

    #@12
    const-class v3, Landroid/bluetooth/IBluetoothPan;

    #@14
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1b
    iget-object v3, p0, Landroid/bluetooth/BluetoothPan$1;->this$0:Landroid/bluetooth/BluetoothPan;

    #@1d
    invoke-static {v3}, Landroid/bluetooth/BluetoothPan;->access$000(Landroid/bluetooth/BluetoothPan;)Landroid/content/ServiceConnection;

    #@20
    move-result-object v3

    #@21
    const/4 v4, 0x0

    #@22
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@25
    move-result v1

    #@26
    if-nez v1, :cond_2f

    #@28
    .line 175
    const-string v1, "BluetoothPan"

    #@2a
    const-string v2, "Could not bind to Bluetooth HID Service"

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 177
    :cond_2f
    const-string v1, "BluetoothPan"

    #@31
    const-string v2, "BluetoothPan(), bindService called"

    #@33
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 189
    :goto_36
    return-void

    #@37
    .line 179
    :cond_37
    const-string v1, "BluetoothPan"

    #@39
    const-string v2, "Unbinding service..."

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 180
    iget-object v1, p0, Landroid/bluetooth/BluetoothPan$1;->this$0:Landroid/bluetooth/BluetoothPan;

    #@40
    invoke-static {v1}, Landroid/bluetooth/BluetoothPan;->access$000(Landroid/bluetooth/BluetoothPan;)Landroid/content/ServiceConnection;

    #@43
    move-result-object v2

    #@44
    monitor-enter v2

    #@45
    .line 182
    :try_start_45
    iget-object v1, p0, Landroid/bluetooth/BluetoothPan$1;->this$0:Landroid/bluetooth/BluetoothPan;

    #@47
    const/4 v3, 0x0

    #@48
    invoke-static {v1, v3}, Landroid/bluetooth/BluetoothPan;->access$202(Landroid/bluetooth/BluetoothPan;Landroid/bluetooth/IBluetoothPan;)Landroid/bluetooth/IBluetoothPan;

    #@4b
    .line 183
    iget-object v1, p0, Landroid/bluetooth/BluetoothPan$1;->this$0:Landroid/bluetooth/BluetoothPan;

    #@4d
    invoke-static {v1}, Landroid/bluetooth/BluetoothPan;->access$100(Landroid/bluetooth/BluetoothPan;)Landroid/content/Context;

    #@50
    move-result-object v1

    #@51
    iget-object v3, p0, Landroid/bluetooth/BluetoothPan$1;->this$0:Landroid/bluetooth/BluetoothPan;

    #@53
    invoke-static {v3}, Landroid/bluetooth/BluetoothPan;->access$000(Landroid/bluetooth/BluetoothPan;)Landroid/content/ServiceConnection;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v1, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_5a
    .catchall {:try_start_45 .. :try_end_5a} :catchall_5c
    .catch Ljava/lang/Exception; {:try_start_45 .. :try_end_5a} :catch_5f

    #@5a
    .line 187
    :goto_5a
    :try_start_5a
    monitor-exit v2

    #@5b
    goto :goto_36

    #@5c
    :catchall_5c
    move-exception v1

    #@5d
    monitor-exit v2
    :try_end_5e
    .catchall {:try_start_5a .. :try_end_5e} :catchall_5c

    #@5e
    throw v1

    #@5f
    .line 184
    :catch_5f
    move-exception v0

    #@60
    .line 185
    .local v0, re:Ljava/lang/Exception;
    :try_start_60
    const-string v1, "BluetoothPan"

    #@62
    const-string v3, ""

    #@64
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_67
    .catchall {:try_start_60 .. :try_end_67} :catchall_5c

    #@67
    goto :goto_5a
.end method
