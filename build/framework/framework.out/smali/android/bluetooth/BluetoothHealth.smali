.class public final Landroid/bluetooth/BluetoothHealth;
.super Ljava/lang/Object;
.source "BluetoothHealth.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/BluetoothHealth$BluetoothHealthCallbackWrapper;
    }
.end annotation


# static fields
.field public static final APP_CONFIG_REGISTRATION_FAILURE:I = 0x1

.field public static final APP_CONFIG_REGISTRATION_SUCCESS:I = 0x0

.field public static final APP_CONFIG_UNREGISTRATION_FAILURE:I = 0x3

.field public static final APP_CONFIG_UNREGISTRATION_SUCCESS:I = 0x2

.field public static final CHANNEL_TYPE_ANY:I = 0xc

.field public static final CHANNEL_TYPE_RELIABLE:I = 0xa

.field public static final CHANNEL_TYPE_STREAMING:I = 0xb

.field private static final DBG:Z = true

.field public static final HEALTH_OPERATION_ERROR:I = 0x1771

.field public static final HEALTH_OPERATION_GENERIC_FAILURE:I = 0x1773

.field public static final HEALTH_OPERATION_INVALID_ARGS:I = 0x1772

.field public static final HEALTH_OPERATION_NOT_ALLOWED:I = 0x1775

.field public static final HEALTH_OPERATION_NOT_FOUND:I = 0x1774

.field public static final HEALTH_OPERATION_SUCCESS:I = 0x1770

.field public static final SINK_ROLE:I = 0x2

.field public static final SOURCE_ROLE:I = 0x1

.field public static final STATE_CHANNEL_CONNECTED:I = 0x2

.field public static final STATE_CHANNEL_CONNECTING:I = 0x1

.field public static final STATE_CHANNEL_DISCONNECTED:I = 0x0

.field public static final STATE_CHANNEL_DISCONNECTING:I = 0x3

.field private static final TAG:Ljava/lang/String; = "BluetoothHealth"

.field private static final VDBG:Z = true


# instance fields
.field mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mService:Landroid/bluetooth/IBluetoothHealth;

.field private mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V
    .registers 8
    .parameter "context"
    .parameter "l"

    #@0
    .prologue
    .line 518
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 102
    new-instance v2, Landroid/bluetooth/BluetoothHealth$1;

    #@5
    invoke-direct {v2, p0}, Landroid/bluetooth/BluetoothHealth$1;-><init>(Landroid/bluetooth/BluetoothHealth;)V

    #@8
    iput-object v2, p0, Landroid/bluetooth/BluetoothHealth;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@a
    .line 560
    new-instance v2, Landroid/bluetooth/BluetoothHealth$2;

    #@c
    invoke-direct {v2, p0}, Landroid/bluetooth/BluetoothHealth$2;-><init>(Landroid/bluetooth/BluetoothHealth;)V

    #@f
    iput-object v2, p0, Landroid/bluetooth/BluetoothHealth;->mConnection:Landroid/content/ServiceConnection;

    #@11
    .line 519
    iput-object p1, p0, Landroid/bluetooth/BluetoothHealth;->mContext:Landroid/content/Context;

    #@13
    .line 520
    iput-object p2, p0, Landroid/bluetooth/BluetoothHealth;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@15
    .line 521
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@18
    move-result-object v2

    #@19
    iput-object v2, p0, Landroid/bluetooth/BluetoothHealth;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@1b
    .line 522
    iget-object v2, p0, Landroid/bluetooth/BluetoothHealth;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@1d
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothManager()Landroid/bluetooth/IBluetoothManager;

    #@20
    move-result-object v1

    #@21
    .line 523
    .local v1, mgr:Landroid/bluetooth/IBluetoothManager;
    if-eqz v1, :cond_28

    #@23
    .line 525
    :try_start_23
    iget-object v2, p0, Landroid/bluetooth/BluetoothHealth;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@25
    invoke-interface {v1, v2}, Landroid/bluetooth/IBluetoothManager;->registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_28} :catch_44

    #@28
    .line 531
    :cond_28
    :goto_28
    new-instance v2, Landroid/content/Intent;

    #@2a
    const-class v3, Landroid/bluetooth/IBluetoothHealth;

    #@2c
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@33
    iget-object v3, p0, Landroid/bluetooth/BluetoothHealth;->mConnection:Landroid/content/ServiceConnection;

    #@35
    const/4 v4, 0x0

    #@36
    invoke-virtual {p1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@39
    move-result v2

    #@3a
    if-nez v2, :cond_43

    #@3c
    .line 532
    const-string v2, "BluetoothHealth"

    #@3e
    const-string v3, "Could not bind to Bluetooth Health Service"

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 534
    :cond_43
    return-void

    #@44
    .line 526
    :catch_44
    move-exception v0

    #@45
    .line 527
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothHealth"

    #@47
    const-string v3, ""

    #@49
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4c
    goto :goto_28
.end method

.method static synthetic access$000(Landroid/bluetooth/BluetoothHealth;)Landroid/content/ServiceConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/bluetooth/BluetoothHealth;->mConnection:Landroid/content/ServiceConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/bluetooth/BluetoothHealth;)Landroid/bluetooth/IBluetoothHealth;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/bluetooth/BluetoothHealth;Landroid/bluetooth/IBluetoothHealth;)Landroid/bluetooth/IBluetoothHealth;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/bluetooth/BluetoothHealth;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/bluetooth/BluetoothHealth;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/bluetooth/BluetoothHealth;)Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/bluetooth/BluetoothHealth;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@2
    return-object v0
.end method

.method private checkAppParam(Ljava/lang/String;IILandroid/bluetooth/BluetoothHealthCallback;)Z
    .registers 9
    .parameter "name"
    .parameter "role"
    .parameter "channelType"
    .parameter "callback"

    #@0
    .prologue
    const/16 v3, 0xc

    #@2
    const/4 v0, 0x0

    #@3
    const/4 v1, 0x1

    #@4
    .line 595
    if-eqz p1, :cond_17

    #@6
    if-eq p2, v1, :cond_b

    #@8
    const/4 v2, 0x2

    #@9
    if-ne p2, v2, :cond_17

    #@b
    :cond_b
    const/16 v2, 0xa

    #@d
    if-eq p3, v2, :cond_15

    #@f
    const/16 v2, 0xb

    #@11
    if-eq p3, v2, :cond_15

    #@13
    if-ne p3, v3, :cond_17

    #@15
    :cond_15
    if-nez p4, :cond_18

    #@17
    .line 602
    :cond_17
    :goto_17
    return v0

    #@18
    .line 601
    :cond_18
    if-ne p2, v1, :cond_1c

    #@1a
    if-eq p3, v3, :cond_17

    #@1c
    :cond_1c
    move v0, v1

    #@1d
    .line 602
    goto :goto_17
.end method

.method private isEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 579
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@3
    move-result-object v0

    #@4
    .line 581
    .local v0, adapter:Landroid/bluetooth/BluetoothAdapter;
    if-eqz v0, :cond_10

    #@6
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@9
    move-result v1

    #@a
    const/16 v2, 0xc

    #@c
    if-ne v1, v2, :cond_10

    #@e
    const/4 v1, 0x1

    #@f
    .line 583
    :goto_f
    return v1

    #@10
    .line 582
    :cond_10
    const-string v1, "Bluetooth is Not enabled"

    #@12
    invoke-static {v1}, Landroid/bluetooth/BluetoothHealth;->log(Ljava/lang/String;)V

    #@15
    .line 583
    const/4 v1, 0x0

    #@16
    goto :goto_f
.end method

.method private isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 587
    if-nez p1, :cond_4

    #@3
    .line 590
    :cond_3
    :goto_3
    return v0

    #@4
    .line 589
    :cond_4
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-static {v1}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_3

    #@e
    const/4 v0, 0x1

    #@f
    goto :goto_3
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 606
    const-string v0, "BluetoothHealth"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 607
    return-void
.end method


# virtual methods
.method close()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 537
    const-string v3, "close()"

    #@3
    invoke-static {v3}, Landroid/bluetooth/BluetoothHealth;->log(Ljava/lang/String;)V

    #@6
    .line 538
    iget-object v3, p0, Landroid/bluetooth/BluetoothHealth;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@8
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothManager()Landroid/bluetooth/IBluetoothManager;

    #@b
    move-result-object v1

    #@c
    .line 539
    .local v1, mgr:Landroid/bluetooth/IBluetoothManager;
    if-eqz v1, :cond_13

    #@e
    .line 541
    :try_start_e
    iget-object v3, p0, Landroid/bluetooth/BluetoothHealth;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@10
    invoke-interface {v1, v3}, Landroid/bluetooth/IBluetoothManager;->unregisterStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_13} :catch_28

    #@13
    .line 547
    :cond_13
    :goto_13
    iget-object v4, p0, Landroid/bluetooth/BluetoothHealth;->mConnection:Landroid/content/ServiceConnection;

    #@15
    monitor-enter v4

    #@16
    .line 548
    :try_start_16
    iget-object v3, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;
    :try_end_18
    .catchall {:try_start_16 .. :try_end_18} :catchall_3a

    #@18
    if-eqz v3, :cond_24

    #@1a
    .line 550
    const/4 v3, 0x0

    #@1b
    :try_start_1b
    iput-object v3, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@1d
    .line 551
    iget-object v3, p0, Landroid/bluetooth/BluetoothHealth;->mContext:Landroid/content/Context;

    #@1f
    iget-object v5, p0, Landroid/bluetooth/BluetoothHealth;->mConnection:Landroid/content/ServiceConnection;

    #@21
    invoke-virtual {v3, v5}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_24
    .catchall {:try_start_1b .. :try_end_24} :catchall_3a
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_24} :catch_31

    #@24
    .line 556
    :cond_24
    :goto_24
    :try_start_24
    monitor-exit v4
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_3a

    #@25
    .line 557
    iput-object v6, p0, Landroid/bluetooth/BluetoothHealth;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@27
    .line 558
    return-void

    #@28
    .line 542
    :catch_28
    move-exception v0

    #@29
    .line 543
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "BluetoothHealth"

    #@2b
    const-string v4, ""

    #@2d
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@30
    goto :goto_13

    #@31
    .line 552
    .end local v0           #e:Ljava/lang/Exception;
    :catch_31
    move-exception v2

    #@32
    .line 553
    .local v2, re:Ljava/lang/Exception;
    :try_start_32
    const-string v3, "BluetoothHealth"

    #@34
    const-string v5, ""

    #@36
    invoke-static {v3, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@39
    goto :goto_24

    #@3a
    .line 556
    .end local v2           #re:Ljava/lang/Exception;
    :catchall_3a
    move-exception v3

    #@3b
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_32 .. :try_end_3c} :catchall_3a

    #@3c
    throw v3
.end method

.method public connectChannelToSink(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z
    .registers 7
    .parameter "device"
    .parameter "config"
    .parameter "channelType"

    #@0
    .prologue
    .line 295
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@2
    if-eqz v1, :cond_25

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_25

    #@a
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHealth;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_25

    #@10
    if-eqz p2, :cond_25

    #@12
    .line 298
    :try_start_12
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@14
    invoke-interface {v1, p1, p2, p3}, Landroid/bluetooth/IBluetoothHealth;->connectChannelToSink(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_17} :catch_19

    #@17
    move-result v1

    #@18
    .line 306
    :goto_18
    return v1

    #@19
    .line 299
    :catch_19
    move-exception v0

    #@1a
    .line 300
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHealth"

    #@1c
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 306
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_23
    const/4 v1, 0x0

    #@24
    goto :goto_18

    #@25
    .line 303
    :cond_25
    const-string v1, "BluetoothHealth"

    #@27
    const-string v2, "Proxy not attached to service"

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 304
    const-string v1, "BluetoothHealth"

    #@2e
    new-instance v2, Ljava/lang/Throwable;

    #@30
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@33
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_23
.end method

.method public connectChannelToSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    .registers 6
    .parameter "device"
    .parameter "config"

    #@0
    .prologue
    .line 266
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@2
    if-eqz v1, :cond_25

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_25

    #@a
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHealth;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_25

    #@10
    if-eqz p2, :cond_25

    #@12
    .line 269
    :try_start_12
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@14
    invoke-interface {v1, p1, p2}, Landroid/bluetooth/IBluetoothHealth;->connectChannelToSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_17} :catch_19

    #@17
    move-result v1

    #@18
    .line 277
    :goto_18
    return v1

    #@19
    .line 270
    :catch_19
    move-exception v0

    #@1a
    .line 271
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHealth"

    #@1c
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 277
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_23
    const/4 v1, 0x0

    #@24
    goto :goto_18

    #@25
    .line 274
    :cond_25
    const-string v1, "BluetoothHealth"

    #@27
    const-string v2, "Proxy not attached to service"

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 275
    const-string v1, "BluetoothHealth"

    #@2e
    new-instance v2, Ljava/lang/Throwable;

    #@30
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@33
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_23
.end method

.method public disconnectChannel(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z
    .registers 7
    .parameter "device"
    .parameter "config"
    .parameter "channelId"

    #@0
    .prologue
    .line 324
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@2
    if-eqz v1, :cond_25

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_25

    #@a
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHealth;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_25

    #@10
    if-eqz p2, :cond_25

    #@12
    .line 327
    :try_start_12
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@14
    invoke-interface {v1, p1, p2, p3}, Landroid/bluetooth/IBluetoothHealth;->disconnectChannel(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_17} :catch_19

    #@17
    move-result v1

    #@18
    .line 335
    :goto_18
    return v1

    #@19
    .line 328
    :catch_19
    move-exception v0

    #@1a
    .line 329
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHealth"

    #@1c
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 335
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_23
    const/4 v1, 0x0

    #@24
    goto :goto_18

    #@25
    .line 332
    :cond_25
    const-string v1, "BluetoothHealth"

    #@27
    const-string v2, "Proxy not attached to service"

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 333
    const-string v1, "BluetoothHealth"

    #@2e
    new-instance v2, Ljava/lang/Throwable;

    #@30
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@33
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_23
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 412
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@2
    if-eqz v1, :cond_39

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_39

    #@a
    .line 414
    :try_start_a
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@c
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothHealth;->getConnectedHealthDevices()Ljava/util/List;
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-result-object v1

    #@10
    .line 421
    :goto_10
    return-object v1

    #@11
    .line 415
    :catch_11
    move-exception v0

    #@12
    .line 416
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHealth"

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "Stack:"

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    new-instance v3, Ljava/lang/Throwable;

    #@21
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@24
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 417
    new-instance v1, Ljava/util/ArrayList;

    #@35
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@38
    goto :goto_10

    #@39
    .line 420
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_39
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@3b
    if-nez v1, :cond_44

    #@3d
    const-string v1, "BluetoothHealth"

    #@3f
    const-string v2, "Proxy not attached to service"

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 421
    :cond_44
    new-instance v1, Ljava/util/ArrayList;

    #@46
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@49
    goto :goto_10
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 384
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@2
    if-eqz v1, :cond_23

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_23

    #@a
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHealth;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_23

    #@10
    .line 386
    :try_start_10
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@12
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHealth;->getHealthDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_15} :catch_17

    #@15
    move-result v1

    #@16
    .line 394
    :goto_16
    return v1

    #@17
    .line 387
    :catch_17
    move-exception v0

    #@18
    .line 388
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHealth"

    #@1a
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 394
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_21
    const/4 v1, 0x0

    #@22
    goto :goto_16

    #@23
    .line 391
    :cond_23
    const-string v1, "BluetoothHealth"

    #@25
    const-string v2, "Proxy not attached to service"

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 392
    const-string v1, "BluetoothHealth"

    #@2c
    new-instance v2, Ljava/lang/Throwable;

    #@2e
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@31
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_21
.end method

.method public getDeviceDatatype(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    .registers 6
    .parameter "device"
    .parameter "config"

    #@0
    .prologue
    .line 237
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@2
    if-eqz v1, :cond_25

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_25

    #@a
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHealth;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_25

    #@10
    if-eqz p2, :cond_25

    #@12
    .line 240
    :try_start_12
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@14
    invoke-interface {v1, p1, p2}, Landroid/bluetooth/IBluetoothHealth;->getDeviceDatatype(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_17} :catch_19

    #@17
    move-result v1

    #@18
    .line 248
    :goto_18
    return v1

    #@19
    .line 241
    :catch_19
    move-exception v0

    #@1a
    .line 242
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHealth"

    #@1c
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 248
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_23
    const/4 v1, 0x0

    #@24
    goto :goto_18

    #@25
    .line 245
    :cond_25
    const-string v1, "BluetoothHealth"

    #@27
    const-string v2, "Proxy not attached to service"

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 246
    const-string v1, "BluetoothHealth"

    #@2e
    new-instance v2, Ljava/lang/Throwable;

    #@30
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@33
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_23
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 6
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 444
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@2
    if-eqz v1, :cond_39

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_39

    #@a
    .line 446
    :try_start_a
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@c
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHealth;->getHealthDevicesMatchingConnectionStates([I)Ljava/util/List;
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-result-object v1

    #@10
    .line 453
    :goto_10
    return-object v1

    #@11
    .line 447
    :catch_11
    move-exception v0

    #@12
    .line 448
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHealth"

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "Stack:"

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    new-instance v3, Ljava/lang/Throwable;

    #@21
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@24
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 449
    new-instance v1, Ljava/util/ArrayList;

    #@35
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@38
    goto :goto_10

    #@39
    .line 452
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_39
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@3b
    if-nez v1, :cond_44

    #@3d
    const-string v1, "BluetoothHealth"

    #@3f
    const-string v2, "Proxy not attached to service"

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 453
    :cond_44
    new-instance v1, Ljava/util/ArrayList;

    #@46
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@49
    goto :goto_10
.end method

.method public getMainChannelFd(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/os/ParcelFileDescriptor;
    .registers 6
    .parameter "device"
    .parameter "config"

    #@0
    .prologue
    .line 353
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@2
    if-eqz v1, :cond_25

    #@4
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_25

    #@a
    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothHealth;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_25

    #@10
    if-eqz p2, :cond_25

    #@12
    .line 356
    :try_start_12
    iget-object v1, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@14
    invoke-interface {v1, p1, p2}, Landroid/bluetooth/IBluetoothHealth;->getMainChannelFd(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/os/ParcelFileDescriptor;
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_17} :catch_19

    #@17
    move-result-object v1

    #@18
    .line 364
    :goto_18
    return-object v1

    #@19
    .line 357
    :catch_19
    move-exception v0

    #@1a
    .line 358
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothHealth"

    #@1c
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 364
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_23
    const/4 v1, 0x0

    #@24
    goto :goto_18

    #@25
    .line 361
    :cond_25
    const-string v1, "BluetoothHealth"

    #@27
    const-string v2, "Proxy not attached to service"

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 362
    const-string v1, "BluetoothHealth"

    #@2e
    new-instance v2, Ljava/lang/Throwable;

    #@30
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@33
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_23
.end method

.method public registerAppConfiguration(Ljava/lang/String;IIILandroid/bluetooth/BluetoothHealthCallback;)Z
    .registers 13
    .parameter "name"
    .parameter "dataType"
    .parameter "role"
    .parameter "channelType"
    .parameter "callback"

    #@0
    .prologue
    .line 176
    const/4 v2, 0x0

    #@1
    .line 177
    .local v2, result:Z
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@4
    move-result v5

    #@5
    if-eqz v5, :cond_d

    #@7
    invoke-direct {p0, p1, p3, p4, p5}, Landroid/bluetooth/BluetoothHealth;->checkAppParam(Ljava/lang/String;IILandroid/bluetooth/BluetoothHealthCallback;)Z

    #@a
    move-result v5

    #@b
    if-nez v5, :cond_f

    #@d
    :cond_d
    move v3, v2

    #@e
    .line 194
    .end local v2           #result:Z
    .local v3, result:I
    :goto_e
    return v3

    #@f
    .line 179
    .end local v3           #result:I
    .restart local v2       #result:Z
    :cond_f
    new-instance v5, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string/jumbo v6, "registerApplication("

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    const-string v6, ":"

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    const-string v6, ")"

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    invoke-static {v5}, Landroid/bluetooth/BluetoothHealth;->log(Ljava/lang/String;)V

    #@36
    .line 180
    new-instance v4, Landroid/bluetooth/BluetoothHealth$BluetoothHealthCallbackWrapper;

    #@38
    invoke-direct {v4, p5}, Landroid/bluetooth/BluetoothHealth$BluetoothHealthCallbackWrapper;-><init>(Landroid/bluetooth/BluetoothHealthCallback;)V

    #@3b
    .line 181
    .local v4, wrapper:Landroid/bluetooth/BluetoothHealth$BluetoothHealthCallbackWrapper;
    new-instance v0, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@3d
    invoke-direct {v0, p1, p2, p3, p4}, Landroid/bluetooth/BluetoothHealthAppConfiguration;-><init>(Ljava/lang/String;III)V

    #@40
    .line 184
    .local v0, config:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    iget-object v5, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@42
    if-eqz v5, :cond_57

    #@44
    .line 186
    :try_start_44
    iget-object v5, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@46
    invoke-interface {v5, v0, v4}, Landroid/bluetooth/IBluetoothHealth;->registerAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/IBluetoothHealthCallback;)Z
    :try_end_49
    .catch Landroid/os/RemoteException; {:try_start_44 .. :try_end_49} :catch_4c

    #@49
    move-result v2

    #@4a
    :goto_4a
    move v3, v2

    #@4b
    .line 194
    .restart local v3       #result:I
    goto :goto_e

    #@4c
    .line 187
    .end local v3           #result:I
    :catch_4c
    move-exception v1

    #@4d
    .line 188
    .local v1, e:Landroid/os/RemoteException;
    const-string v5, "BluetoothHealth"

    #@4f
    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_4a

    #@57
    .line 191
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_57
    const-string v5, "BluetoothHealth"

    #@59
    const-string v6, "Proxy not attached to service"

    #@5b
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 192
    const-string v5, "BluetoothHealth"

    #@60
    new-instance v6, Ljava/lang/Throwable;

    #@62
    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    #@65
    invoke-static {v6}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@68
    move-result-object v6

    #@69
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    goto :goto_4a
.end method

.method public registerSinkAppConfiguration(Ljava/lang/String;ILandroid/bluetooth/BluetoothHealthCallback;)Z
    .registers 10
    .parameter "name"
    .parameter "dataType"
    .parameter "callback"

    #@0
    .prologue
    .line 151
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    if-nez p1, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 154
    :goto_9
    return v0

    #@a
    .line 153
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string/jumbo v1, "registerSinkApplication("

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    const-string v1, ":"

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    const-string v1, ")"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    invoke-static {v0}, Landroid/bluetooth/BluetoothHealth;->log(Ljava/lang/String;)V

    #@31
    .line 154
    const/4 v3, 0x2

    #@32
    const/16 v4, 0xc

    #@34
    move-object v0, p0

    #@35
    move-object v1, p1

    #@36
    move v2, p2

    #@37
    move-object v5, p3

    #@38
    invoke-virtual/range {v0 .. v5}, Landroid/bluetooth/BluetoothHealth;->registerAppConfiguration(Ljava/lang/String;IIILandroid/bluetooth/BluetoothHealthCallback;)Z

    #@3b
    move-result v0

    #@3c
    goto :goto_9
.end method

.method public unregisterAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    .registers 6
    .parameter "config"

    #@0
    .prologue
    .line 207
    const/4 v1, 0x0

    #@1
    .line 208
    .local v1, result:Z
    iget-object v2, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@3
    if-eqz v2, :cond_1f

    #@5
    invoke-direct {p0}, Landroid/bluetooth/BluetoothHealth;->isEnabled()Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_1f

    #@b
    if-eqz p1, :cond_1f

    #@d
    .line 210
    :try_start_d
    iget-object v2, p0, Landroid/bluetooth/BluetoothHealth;->mService:Landroid/bluetooth/IBluetoothHealth;

    #@f
    invoke-interface {v2, p1}, Landroid/bluetooth/IBluetoothHealth;->unregisterAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result v1

    #@13
    .line 219
    :goto_13
    return v1

    #@14
    .line 211
    :catch_14
    move-exception v0

    #@15
    .line 212
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothHealth"

    #@17
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    goto :goto_13

    #@1f
    .line 215
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_1f
    const-string v2, "BluetoothHealth"

    #@21
    const-string v3, "Proxy not attached to service"

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 216
    const-string v2, "BluetoothHealth"

    #@28
    new-instance v3, Ljava/lang/Throwable;

    #@2a
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@2d
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_13
.end method
