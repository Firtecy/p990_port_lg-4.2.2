.class final Landroid/bluetooth/BluetoothDevice$1;
.super Landroid/bluetooth/IBluetoothManagerCallback$Stub;
.source "BluetoothDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/BluetoothDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 495
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothManagerCallback$Stub;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onBluetoothServiceDown()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 506
    const-class v1, Landroid/bluetooth/BluetoothDevice;

    #@2
    monitor-enter v1

    #@3
    .line 507
    const/4 v0, 0x0

    #@4
    :try_start_4
    invoke-static {v0}, Landroid/bluetooth/BluetoothDevice;->access$002(Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;

    #@7
    .line 508
    monitor-exit v1

    #@8
    .line 509
    return-void

    #@9
    .line 508
    :catchall_9
    move-exception v0

    #@a
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public onBluetoothServiceUp(Landroid/bluetooth/IBluetooth;)V
    .registers 4
    .parameter "bluetoothService"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 499
    const-class v1, Landroid/bluetooth/BluetoothDevice;

    #@2
    monitor-enter v1

    #@3
    .line 500
    :try_start_3
    invoke-static {p1}, Landroid/bluetooth/BluetoothDevice;->access$002(Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;

    #@6
    .line 501
    monitor-exit v1

    #@7
    .line 502
    return-void

    #@8
    .line 501
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method
