.class public abstract Landroid/bluetooth/IBluetoothPan$Stub;
.super Landroid/os/Binder;
.source "IBluetoothPan.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothPan;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothPan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/IBluetoothPan$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.bluetooth.IBluetoothPan"

.field static final TRANSACTION_connect:I = 0x3

.field static final TRANSACTION_disconnect:I = 0x4

.field static final TRANSACTION_getConnectedDevices:I = 0x5

.field static final TRANSACTION_getConnectionState:I = 0x7

.field static final TRANSACTION_getDevicesMatchingConnectionStates:I = 0x6

.field static final TRANSACTION_isTetheringOn:I = 0x1

.field static final TRANSACTION_setBluetoothTethering:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.bluetooth.IBluetoothPan"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/bluetooth/IBluetoothPan$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothPan;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.bluetooth.IBluetoothPan"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/bluetooth/IBluetoothPan;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/bluetooth/IBluetoothPan;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/bluetooth/IBluetoothPan$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/bluetooth/IBluetoothPan$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_c6

    #@5
    .line 131
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 47
    :sswitch_a
    const-string v3, "android.bluetooth.IBluetoothPan"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v5, "android.bluetooth.IBluetoothPan"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 53
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothPan$Stub;->isTetheringOn()Z

    #@18
    move-result v1

    #@19
    .line 54
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c
    .line 55
    if-eqz v1, :cond_1f

    #@1e
    move v3, v4

    #@1f
    :cond_1f
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    goto :goto_9

    #@23
    .line 60
    .end local v1           #_result:Z
    :sswitch_23
    const-string v5, "android.bluetooth.IBluetoothPan"

    #@25
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28
    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v5

    #@2c
    if-eqz v5, :cond_36

    #@2e
    move v0, v4

    #@2f
    .line 63
    .local v0, _arg0:Z
    :goto_2f
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothPan$Stub;->setBluetoothTethering(Z)V

    #@32
    .line 64
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@35
    goto :goto_9

    #@36
    .end local v0           #_arg0:Z
    :cond_36
    move v0, v3

    #@37
    .line 62
    goto :goto_2f

    #@38
    .line 69
    :sswitch_38
    const-string v5, "android.bluetooth.IBluetoothPan"

    #@3a
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d
    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@40
    move-result v5

    #@41
    if-eqz v5, :cond_59

    #@43
    .line 72
    sget-object v5, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@45
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@48
    move-result-object v0

    #@49
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@4b
    .line 77
    .local v0, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_4b
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothPan$Stub;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@4e
    move-result v1

    #@4f
    .line 78
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@52
    .line 79
    if-eqz v1, :cond_55

    #@54
    move v3, v4

    #@55
    :cond_55
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@58
    goto :goto_9

    #@59
    .line 75
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_result:Z
    :cond_59
    const/4 v0, 0x0

    #@5a
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_4b

    #@5b
    .line 84
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_5b
    const-string v5, "android.bluetooth.IBluetoothPan"

    #@5d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60
    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v5

    #@64
    if-eqz v5, :cond_7c

    #@66
    .line 87
    sget-object v5, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@68
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6b
    move-result-object v0

    #@6c
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@6e
    .line 92
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_6e
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothPan$Stub;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@71
    move-result v1

    #@72
    .line 93
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@75
    .line 94
    if-eqz v1, :cond_78

    #@77
    move v3, v4

    #@78
    :cond_78
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@7b
    goto :goto_9

    #@7c
    .line 90
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_result:Z
    :cond_7c
    const/4 v0, 0x0

    #@7d
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_6e

    #@7e
    .line 99
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_7e
    const-string v3, "android.bluetooth.IBluetoothPan"

    #@80
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@83
    .line 100
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothPan$Stub;->getConnectedDevices()Ljava/util/List;

    #@86
    move-result-object v2

    #@87
    .line 101
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8a
    .line 102
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@8d
    goto/16 :goto_9

    #@8f
    .line 107
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :sswitch_8f
    const-string v3, "android.bluetooth.IBluetoothPan"

    #@91
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@94
    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@97
    move-result-object v0

    #@98
    .line 110
    .local v0, _arg0:[I
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothPan$Stub;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@9b
    move-result-object v2

    #@9c
    .line 111
    .restart local v2       #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9f
    .line 112
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@a2
    goto/16 :goto_9

    #@a4
    .line 117
    .end local v0           #_arg0:[I
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :sswitch_a4
    const-string v3, "android.bluetooth.IBluetoothPan"

    #@a6
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a9
    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ac
    move-result v3

    #@ad
    if-eqz v3, :cond_c3

    #@af
    .line 120
    sget-object v3, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b1
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b4
    move-result-object v0

    #@b5
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@b7
    .line 125
    .local v0, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_b7
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothPan$Stub;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@ba
    move-result v1

    #@bb
    .line 126
    .local v1, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@be
    .line 127
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@c1
    goto/16 :goto_9

    #@c3
    .line 123
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_result:I
    :cond_c3
    const/4 v0, 0x0

    #@c4
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_b7

    #@c5
    .line 43
    nop

    #@c6
    :sswitch_data_c6
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_23
        0x3 -> :sswitch_38
        0x4 -> :sswitch_5b
        0x5 -> :sswitch_7e
        0x6 -> :sswitch_8f
        0x7 -> :sswitch_a4
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
