.class public abstract Landroid/bluetooth/IBluetoothHeadset$Stub;
.super Landroid/os/Binder;
.source "IBluetoothHeadset.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothHeadset;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothHeadset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/IBluetoothHeadset$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.bluetooth.IBluetoothHeadset"

.field static final TRANSACTION_acceptIncomingConnect:I = 0xc

.field static final TRANSACTION_clccResponse:I = 0x16

.field static final TRANSACTION_connect:I = 0x1

.field static final TRANSACTION_connectAudio:I = 0x10

.field static final TRANSACTION_disconnect:I = 0x2

.field static final TRANSACTION_disconnectAudio:I = 0x11

.field static final TRANSACTION_getAudioState:I = 0xe

.field static final TRANSACTION_getBatteryUsageHint:I = 0xb

.field static final TRANSACTION_getConnectedDevices:I = 0x3

.field static final TRANSACTION_getConnectionState:I = 0x5

.field static final TRANSACTION_getDevicesMatchingConnectionStates:I = 0x4

.field static final TRANSACTION_getPriority:I = 0x7

.field static final TRANSACTION_isAudioConnected:I = 0xa

.field static final TRANSACTION_isAudioOn:I = 0xf

.field static final TRANSACTION_phoneStateChanged:I = 0x14

.field static final TRANSACTION_rejectIncomingConnect:I = 0xd

.field static final TRANSACTION_roamChanged:I = 0x15

.field static final TRANSACTION_setPriority:I = 0x6

.field static final TRANSACTION_startScoUsingVirtualVoiceCall:I = 0x12

.field static final TRANSACTION_startVoiceRecognition:I = 0x8

.field static final TRANSACTION_stopScoUsingVirtualVoiceCall:I = 0x13

.field static final TRANSACTION_stopVoiceRecognition:I = 0x9


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/bluetooth/IBluetoothHeadset$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothHeadset;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.bluetooth.IBluetoothHeadset"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/bluetooth/IBluetoothHeadset;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/bluetooth/IBluetoothHeadset;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/bluetooth/IBluetoothHeadset$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/bluetooth/IBluetoothHeadset$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 15
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 43
    sparse-switch p1, :sswitch_data_2f4

    #@3
    .line 352
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v0

    #@7
    :goto_7
    return v0

    #@8
    .line 47
    :sswitch_8
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 48
    const/4 v0, 0x1

    #@e
    goto :goto_7

    #@f
    .line 52
    :sswitch_f
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_31

    #@1a
    .line 55
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@22
    .line 60
    .local v1, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_22
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@25
    move-result v8

    #@26
    .line 61
    .local v8, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@29
    .line 62
    if-eqz v8, :cond_33

    #@2b
    const/4 v0, 0x1

    #@2c
    :goto_2c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    .line 63
    const/4 v0, 0x1

    #@30
    goto :goto_7

    #@31
    .line 58
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :cond_31
    const/4 v1, 0x0

    #@32
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_22

    #@33
    .line 62
    .restart local v8       #_result:Z
    :cond_33
    const/4 v0, 0x0

    #@34
    goto :goto_2c

    #@35
    .line 67
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :sswitch_35
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@37
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a
    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v0

    #@3e
    if-eqz v0, :cond_57

    #@40
    .line 70
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@42
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@45
    move-result-object v1

    #@46
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@48
    .line 75
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_48
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@4b
    move-result v8

    #@4c
    .line 76
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f
    .line 77
    if-eqz v8, :cond_59

    #@51
    const/4 v0, 0x1

    #@52
    :goto_52
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@55
    .line 78
    const/4 v0, 0x1

    #@56
    goto :goto_7

    #@57
    .line 73
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :cond_57
    const/4 v1, 0x0

    #@58
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_48

    #@59
    .line 77
    .restart local v8       #_result:Z
    :cond_59
    const/4 v0, 0x0

    #@5a
    goto :goto_52

    #@5b
    .line 82
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :sswitch_5b
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@5d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60
    .line 83
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadset$Stub;->getConnectedDevices()Ljava/util/List;

    #@63
    move-result-object v9

    #@64
    .line 84
    .local v9, _result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@67
    .line 85
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@6a
    .line 86
    const/4 v0, 0x1

    #@6b
    goto :goto_7

    #@6c
    .line 90
    .end local v9           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :sswitch_6c
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@6e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@71
    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@74
    move-result-object v1

    #@75
    .line 93
    .local v1, _arg0:[I
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@78
    move-result-object v9

    #@79
    .line 94
    .restart local v9       #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7c
    .line 95
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@7f
    .line 96
    const/4 v0, 0x1

    #@80
    goto :goto_7

    #@81
    .line 100
    .end local v1           #_arg0:[I
    .end local v9           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :sswitch_81
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@83
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@86
    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@89
    move-result v0

    #@8a
    if-eqz v0, :cond_a1

    #@8c
    .line 103
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@8e
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@91
    move-result-object v1

    #@92
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@94
    .line 108
    .local v1, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_94
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@97
    move-result v8

    #@98
    .line 109
    .local v8, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9b
    .line 110
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@9e
    .line 111
    const/4 v0, 0x1

    #@9f
    goto/16 :goto_7

    #@a1
    .line 106
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:I
    :cond_a1
    const/4 v1, 0x0

    #@a2
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_94

    #@a3
    .line 115
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_a3
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@a5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a8
    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ab
    move-result v0

    #@ac
    if-eqz v0, :cond_ca

    #@ae
    .line 118
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b0
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b3
    move-result-object v1

    #@b4
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@b6
    .line 124
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_b6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b9
    move-result v2

    #@ba
    .line 125
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/bluetooth/IBluetoothHeadset$Stub;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@bd
    move-result v8

    #@be
    .line 126
    .local v8, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c1
    .line 127
    if-eqz v8, :cond_cc

    #@c3
    const/4 v0, 0x1

    #@c4
    :goto_c4
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@c7
    .line 128
    const/4 v0, 0x1

    #@c8
    goto/16 :goto_7

    #@ca
    .line 121
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:I
    .end local v8           #_result:Z
    :cond_ca
    const/4 v1, 0x0

    #@cb
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_b6

    #@cc
    .line 127
    .restart local v2       #_arg1:I
    .restart local v8       #_result:Z
    :cond_cc
    const/4 v0, 0x0

    #@cd
    goto :goto_c4

    #@ce
    .line 132
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:I
    .end local v8           #_result:Z
    :sswitch_ce
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@d0
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d3
    .line 134
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d6
    move-result v0

    #@d7
    if-eqz v0, :cond_ee

    #@d9
    .line 135
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@db
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@de
    move-result-object v1

    #@df
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@e1
    .line 140
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_e1
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@e4
    move-result v8

    #@e5
    .line 141
    .local v8, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e8
    .line 142
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@eb
    .line 143
    const/4 v0, 0x1

    #@ec
    goto/16 :goto_7

    #@ee
    .line 138
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:I
    :cond_ee
    const/4 v1, 0x0

    #@ef
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_e1

    #@f0
    .line 147
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_f0
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@f2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f5
    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f8
    move-result v0

    #@f9
    if-eqz v0, :cond_113

    #@fb
    .line 150
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@fd
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@100
    move-result-object v1

    #@101
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@103
    .line 155
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_103
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->startVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    #@106
    move-result v8

    #@107
    .line 156
    .local v8, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@10a
    .line 157
    if-eqz v8, :cond_115

    #@10c
    const/4 v0, 0x1

    #@10d
    :goto_10d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@110
    .line 158
    const/4 v0, 0x1

    #@111
    goto/16 :goto_7

    #@113
    .line 153
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :cond_113
    const/4 v1, 0x0

    #@114
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_103

    #@115
    .line 157
    .restart local v8       #_result:Z
    :cond_115
    const/4 v0, 0x0

    #@116
    goto :goto_10d

    #@117
    .line 162
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :sswitch_117
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@119
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11c
    .line 164
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11f
    move-result v0

    #@120
    if-eqz v0, :cond_13a

    #@122
    .line 165
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@124
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@127
    move-result-object v1

    #@128
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@12a
    .line 170
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_12a
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    #@12d
    move-result v8

    #@12e
    .line 171
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@131
    .line 172
    if-eqz v8, :cond_13c

    #@133
    const/4 v0, 0x1

    #@134
    :goto_134
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@137
    .line 173
    const/4 v0, 0x1

    #@138
    goto/16 :goto_7

    #@13a
    .line 168
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :cond_13a
    const/4 v1, 0x0

    #@13b
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_12a

    #@13c
    .line 172
    .restart local v8       #_result:Z
    :cond_13c
    const/4 v0, 0x0

    #@13d
    goto :goto_134

    #@13e
    .line 177
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :sswitch_13e
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@140
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@143
    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@146
    move-result v0

    #@147
    if-eqz v0, :cond_161

    #@149
    .line 180
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@14b
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@14e
    move-result-object v1

    #@14f
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@151
    .line 185
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_151
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z

    #@154
    move-result v8

    #@155
    .line 186
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@158
    .line 187
    if-eqz v8, :cond_163

    #@15a
    const/4 v0, 0x1

    #@15b
    :goto_15b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@15e
    .line 188
    const/4 v0, 0x1

    #@15f
    goto/16 :goto_7

    #@161
    .line 183
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :cond_161
    const/4 v1, 0x0

    #@162
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_151

    #@163
    .line 187
    .restart local v8       #_result:Z
    :cond_163
    const/4 v0, 0x0

    #@164
    goto :goto_15b

    #@165
    .line 192
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :sswitch_165
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@167
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16a
    .line 194
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16d
    move-result v0

    #@16e
    if-eqz v0, :cond_185

    #@170
    .line 195
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@172
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@175
    move-result-object v1

    #@176
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@178
    .line 200
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_178
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->getBatteryUsageHint(Landroid/bluetooth/BluetoothDevice;)I

    #@17b
    move-result v8

    #@17c
    .line 201
    .local v8, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@17f
    .line 202
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@182
    .line 203
    const/4 v0, 0x1

    #@183
    goto/16 :goto_7

    #@185
    .line 198
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:I
    :cond_185
    const/4 v1, 0x0

    #@186
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_178

    #@187
    .line 207
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_187
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@189
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18c
    .line 209
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18f
    move-result v0

    #@190
    if-eqz v0, :cond_1aa

    #@192
    .line 210
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@194
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@197
    move-result-object v1

    #@198
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@19a
    .line 215
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_19a
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->acceptIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@19d
    move-result v8

    #@19e
    .line 216
    .local v8, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a1
    .line 217
    if-eqz v8, :cond_1ac

    #@1a3
    const/4 v0, 0x1

    #@1a4
    :goto_1a4
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1a7
    .line 218
    const/4 v0, 0x1

    #@1a8
    goto/16 :goto_7

    #@1aa
    .line 213
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :cond_1aa
    const/4 v1, 0x0

    #@1ab
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_19a

    #@1ac
    .line 217
    .restart local v8       #_result:Z
    :cond_1ac
    const/4 v0, 0x0

    #@1ad
    goto :goto_1a4

    #@1ae
    .line 222
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :sswitch_1ae
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@1b0
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b3
    .line 224
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b6
    move-result v0

    #@1b7
    if-eqz v0, :cond_1d1

    #@1b9
    .line 225
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1bb
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1be
    move-result-object v1

    #@1bf
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@1c1
    .line 230
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_1c1
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->rejectIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@1c4
    move-result v8

    #@1c5
    .line 231
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c8
    .line 232
    if-eqz v8, :cond_1d3

    #@1ca
    const/4 v0, 0x1

    #@1cb
    :goto_1cb
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1ce
    .line 233
    const/4 v0, 0x1

    #@1cf
    goto/16 :goto_7

    #@1d1
    .line 228
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :cond_1d1
    const/4 v1, 0x0

    #@1d2
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_1c1

    #@1d3
    .line 232
    .restart local v8       #_result:Z
    :cond_1d3
    const/4 v0, 0x0

    #@1d4
    goto :goto_1cb

    #@1d5
    .line 237
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :sswitch_1d5
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@1d7
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1da
    .line 239
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1dd
    move-result v0

    #@1de
    if-eqz v0, :cond_1f5

    #@1e0
    .line 240
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1e2
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1e5
    move-result-object v1

    #@1e6
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@1e8
    .line 245
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_1e8
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->getAudioState(Landroid/bluetooth/BluetoothDevice;)I

    #@1eb
    move-result v8

    #@1ec
    .line 246
    .local v8, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ef
    .line 247
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@1f2
    .line 248
    const/4 v0, 0x1

    #@1f3
    goto/16 :goto_7

    #@1f5
    .line 243
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:I
    :cond_1f5
    const/4 v1, 0x0

    #@1f6
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_1e8

    #@1f7
    .line 252
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_1f7
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@1f9
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1fc
    .line 253
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadset$Stub;->isAudioOn()Z

    #@1ff
    move-result v8

    #@200
    .line 254
    .local v8, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@203
    .line 255
    if-eqz v8, :cond_20c

    #@205
    const/4 v0, 0x1

    #@206
    :goto_206
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@209
    .line 256
    const/4 v0, 0x1

    #@20a
    goto/16 :goto_7

    #@20c
    .line 255
    :cond_20c
    const/4 v0, 0x0

    #@20d
    goto :goto_206

    #@20e
    .line 260
    .end local v8           #_result:Z
    :sswitch_20e
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@210
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@213
    .line 261
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadset$Stub;->connectAudio()Z

    #@216
    move-result v8

    #@217
    .line 262
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@21a
    .line 263
    if-eqz v8, :cond_223

    #@21c
    const/4 v0, 0x1

    #@21d
    :goto_21d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@220
    .line 264
    const/4 v0, 0x1

    #@221
    goto/16 :goto_7

    #@223
    .line 263
    :cond_223
    const/4 v0, 0x0

    #@224
    goto :goto_21d

    #@225
    .line 268
    .end local v8           #_result:Z
    :sswitch_225
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@227
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@22a
    .line 269
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothHeadset$Stub;->disconnectAudio()Z

    #@22d
    move-result v8

    #@22e
    .line 270
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@231
    .line 271
    if-eqz v8, :cond_23a

    #@233
    const/4 v0, 0x1

    #@234
    :goto_234
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@237
    .line 272
    const/4 v0, 0x1

    #@238
    goto/16 :goto_7

    #@23a
    .line 271
    :cond_23a
    const/4 v0, 0x0

    #@23b
    goto :goto_234

    #@23c
    .line 276
    .end local v8           #_result:Z
    :sswitch_23c
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@23e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@241
    .line 278
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@244
    move-result v0

    #@245
    if-eqz v0, :cond_25f

    #@247
    .line 279
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@249
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@24c
    move-result-object v1

    #@24d
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@24f
    .line 284
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_24f
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->startScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z

    #@252
    move-result v8

    #@253
    .line 285
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@256
    .line 286
    if-eqz v8, :cond_261

    #@258
    const/4 v0, 0x1

    #@259
    :goto_259
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@25c
    .line 287
    const/4 v0, 0x1

    #@25d
    goto/16 :goto_7

    #@25f
    .line 282
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :cond_25f
    const/4 v1, 0x0

    #@260
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_24f

    #@261
    .line 286
    .restart local v8       #_result:Z
    :cond_261
    const/4 v0, 0x0

    #@262
    goto :goto_259

    #@263
    .line 291
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :sswitch_263
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@265
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@268
    .line 293
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@26b
    move-result v0

    #@26c
    if-eqz v0, :cond_286

    #@26e
    .line 294
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@270
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@273
    move-result-object v1

    #@274
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@276
    .line 299
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_276
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->stopScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z

    #@279
    move-result v8

    #@27a
    .line 300
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@27d
    .line 301
    if-eqz v8, :cond_288

    #@27f
    const/4 v0, 0x1

    #@280
    :goto_280
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@283
    .line 302
    const/4 v0, 0x1

    #@284
    goto/16 :goto_7

    #@286
    .line 297
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :cond_286
    const/4 v1, 0x0

    #@287
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_276

    #@288
    .line 301
    .restart local v8       #_result:Z
    :cond_288
    const/4 v0, 0x0

    #@289
    goto :goto_280

    #@28a
    .line 306
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #_result:Z
    :sswitch_28a
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@28c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28f
    .line 308
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@292
    move-result v1

    #@293
    .line 310
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@296
    move-result v2

    #@297
    .line 312
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@29a
    move-result v3

    #@29b
    .line 314
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@29e
    move-result-object v4

    #@29f
    .line 316
    .local v4, _arg3:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2a2
    move-result v5

    #@2a3
    .local v5, _arg4:I
    move-object v0, p0

    #@2a4
    .line 317
    invoke-virtual/range {v0 .. v5}, Landroid/bluetooth/IBluetoothHeadset$Stub;->phoneStateChanged(IIILjava/lang/String;I)V

    #@2a7
    .line 318
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2aa
    .line 319
    const/4 v0, 0x1

    #@2ab
    goto/16 :goto_7

    #@2ad
    .line 323
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:I
    :sswitch_2ad
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@2af
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2b2
    .line 325
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2b5
    move-result v0

    #@2b6
    if-eqz v0, :cond_2c2

    #@2b8
    const/4 v1, 0x1

    #@2b9
    .line 326
    .local v1, _arg0:Z
    :goto_2b9
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetoothHeadset$Stub;->roamChanged(Z)V

    #@2bc
    .line 327
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2bf
    .line 328
    const/4 v0, 0x1

    #@2c0
    goto/16 :goto_7

    #@2c2
    .line 325
    .end local v1           #_arg0:Z
    :cond_2c2
    const/4 v1, 0x0

    #@2c3
    goto :goto_2b9

    #@2c4
    .line 332
    :sswitch_2c4
    const-string v0, "android.bluetooth.IBluetoothHeadset"

    #@2c6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c9
    .line 334
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2cc
    move-result v1

    #@2cd
    .line 336
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d0
    move-result v2

    #@2d1
    .line 338
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d4
    move-result v3

    #@2d5
    .line 340
    .restart local v3       #_arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d8
    move-result v4

    #@2d9
    .line 342
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2dc
    move-result v0

    #@2dd
    if-eqz v0, :cond_2f2

    #@2df
    const/4 v5, 0x1

    #@2e0
    .line 344
    .local v5, _arg4:Z
    :goto_2e0
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2e3
    move-result-object v6

    #@2e4
    .line 346
    .local v6, _arg5:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2e7
    move-result v7

    #@2e8
    .local v7, _arg6:I
    move-object v0, p0

    #@2e9
    .line 347
    invoke-virtual/range {v0 .. v7}, Landroid/bluetooth/IBluetoothHeadset$Stub;->clccResponse(IIIIZLjava/lang/String;I)V

    #@2ec
    .line 348
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ef
    .line 349
    const/4 v0, 0x1

    #@2f0
    goto/16 :goto_7

    #@2f2
    .line 342
    .end local v5           #_arg4:Z
    .end local v6           #_arg5:Ljava/lang/String;
    .end local v7           #_arg6:I
    :cond_2f2
    const/4 v5, 0x0

    #@2f3
    goto :goto_2e0

    #@2f4
    .line 43
    :sswitch_data_2f4
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_35
        0x3 -> :sswitch_5b
        0x4 -> :sswitch_6c
        0x5 -> :sswitch_81
        0x6 -> :sswitch_a3
        0x7 -> :sswitch_ce
        0x8 -> :sswitch_f0
        0x9 -> :sswitch_117
        0xa -> :sswitch_13e
        0xb -> :sswitch_165
        0xc -> :sswitch_187
        0xd -> :sswitch_1ae
        0xe -> :sswitch_1d5
        0xf -> :sswitch_1f7
        0x10 -> :sswitch_20e
        0x11 -> :sswitch_225
        0x12 -> :sswitch_23c
        0x13 -> :sswitch_263
        0x14 -> :sswitch_28a
        0x15 -> :sswitch_2ad
        0x16 -> :sswitch_2c4
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
