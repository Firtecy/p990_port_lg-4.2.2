.class public Landroid/bluetooth/BluetoothPbap;
.super Ljava/lang/Object;
.source "BluetoothPbap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/BluetoothPbap$ServiceListener;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field public static final PBAP_PREVIOUS_STATE:Ljava/lang/String; = "android.bluetooth.pbap.intent.PBAP_PREVIOUS_STATE"

.field public static final PBAP_STATE:Ljava/lang/String; = "android.bluetooth.pbap.intent.PBAP_STATE"

.field public static final PBAP_STATE_CHANGED_ACTION:Ljava/lang/String; = "android.bluetooth.pbap.intent.action.PBAP_STATE_CHANGED"

.field public static final RESULT_CANCELED:I = 0x2

.field public static final RESULT_FAILURE:I = 0x0

.field public static final RESULT_SUCCESS:I = 0x1

.field public static final STATE_CONNECTED:I = 0x2

.field public static final STATE_CONNECTING:I = 0x1

.field public static final STATE_DISCONNECTED:I = 0x0

.field public static final STATE_ERROR:I = -0x1

.field private static final TAG:Ljava/lang/String; = "BluetoothPbap"

.field private static final VDBG:Z = true


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

.field private mConnection:Landroid/content/ServiceConnection;

.field private final mContext:Landroid/content/Context;

.field private mService:Landroid/bluetooth/IBluetoothPbap;

.field private mServiceListener:Landroid/bluetooth/BluetoothPbap$ServiceListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothPbap$ServiceListener;)V
    .registers 8
    .parameter "context"
    .parameter "l"

    #@0
    .prologue
    .line 149
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 113
    new-instance v2, Landroid/bluetooth/BluetoothPbap$1;

    #@5
    invoke-direct {v2, p0}, Landroid/bluetooth/BluetoothPbap$1;-><init>(Landroid/bluetooth/BluetoothPbap;)V

    #@8
    iput-object v2, p0, Landroid/bluetooth/BluetoothPbap;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@a
    .line 298
    new-instance v2, Landroid/bluetooth/BluetoothPbap$2;

    #@c
    invoke-direct {v2, p0}, Landroid/bluetooth/BluetoothPbap$2;-><init>(Landroid/bluetooth/BluetoothPbap;)V

    #@f
    iput-object v2, p0, Landroid/bluetooth/BluetoothPbap;->mConnection:Landroid/content/ServiceConnection;

    #@11
    .line 150
    iput-object p1, p0, Landroid/bluetooth/BluetoothPbap;->mContext:Landroid/content/Context;

    #@13
    .line 151
    iput-object p2, p0, Landroid/bluetooth/BluetoothPbap;->mServiceListener:Landroid/bluetooth/BluetoothPbap$ServiceListener;

    #@15
    .line 152
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@18
    move-result-object v2

    #@19
    iput-object v2, p0, Landroid/bluetooth/BluetoothPbap;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@1b
    .line 153
    iget-object v2, p0, Landroid/bluetooth/BluetoothPbap;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@1d
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothManager()Landroid/bluetooth/IBluetoothManager;

    #@20
    move-result-object v1

    #@21
    .line 154
    .local v1, mgr:Landroid/bluetooth/IBluetoothManager;
    if-eqz v1, :cond_28

    #@23
    .line 156
    :try_start_23
    iget-object v2, p0, Landroid/bluetooth/BluetoothPbap;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@25
    invoke-interface {v1, v2}, Landroid/bluetooth/IBluetoothManager;->registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_28} :catch_44

    #@28
    .line 161
    :cond_28
    :goto_28
    new-instance v2, Landroid/content/Intent;

    #@2a
    const-class v3, Landroid/bluetooth/IBluetoothPbap;

    #@2c
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@33
    iget-object v3, p0, Landroid/bluetooth/BluetoothPbap;->mConnection:Landroid/content/ServiceConnection;

    #@35
    const/4 v4, 0x0

    #@36
    invoke-virtual {p1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@39
    move-result v2

    #@3a
    if-nez v2, :cond_43

    #@3c
    .line 162
    const-string v2, "BluetoothPbap"

    #@3e
    const-string v3, "Could not bind to Bluetooth Pbap Service"

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 164
    :cond_43
    return-void

    #@44
    .line 157
    :catch_44
    move-exception v0

    #@45
    .line 158
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "BluetoothPbap"

    #@47
    const-string v3, ""

    #@49
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4c
    goto :goto_28
.end method

.method static synthetic access$000(Landroid/bluetooth/BluetoothPbap;)Landroid/content/ServiceConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Landroid/bluetooth/BluetoothPbap;->mConnection:Landroid/content/ServiceConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/bluetooth/BluetoothPbap;)Landroid/bluetooth/IBluetoothPbap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/bluetooth/BluetoothPbap;Landroid/bluetooth/IBluetoothPbap;)Landroid/bluetooth/IBluetoothPbap;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 51
    iput-object p1, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/bluetooth/BluetoothPbap;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Landroid/bluetooth/BluetoothPbap;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 51
    invoke-static {p0}, Landroid/bluetooth/BluetoothPbap;->log(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/bluetooth/BluetoothPbap;)Landroid/bluetooth/BluetoothPbap$ServiceListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Landroid/bluetooth/BluetoothPbap;->mServiceListener:Landroid/bluetooth/BluetoothPbap$ServiceListener;

    #@2
    return-object v0
.end method

.method public static doesClassMatchSink(Landroid/bluetooth/BluetoothClass;)Z
    .registers 2
    .parameter "btClass"

    #@0
    .prologue
    .line 287
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    #@3
    move-result v0

    #@4
    sparse-switch v0, :sswitch_data_c

    #@7
    .line 294
    const/4 v0, 0x0

    #@8
    :goto_8
    return v0

    #@9
    .line 292
    :sswitch_9
    const/4 v0, 0x1

    #@a
    goto :goto_8

    #@b
    .line 287
    nop

    #@c
    :sswitch_data_c
    .sparse-switch
        0x100 -> :sswitch_9
        0x104 -> :sswitch_9
        0x108 -> :sswitch_9
        0x10c -> :sswitch_9
    .end sparse-switch
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 316
    const-string v0, "BluetoothPbap"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 317
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .registers 7

    #@0
    .prologue
    .line 181
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v3, p0, Landroid/bluetooth/BluetoothPbap;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@3
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothManager()Landroid/bluetooth/IBluetoothManager;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_31

    #@6
    move-result-object v1

    #@7
    .line 182
    .local v1, mgr:Landroid/bluetooth/IBluetoothManager;
    if-eqz v1, :cond_e

    #@9
    .line 184
    :try_start_9
    iget-object v3, p0, Landroid/bluetooth/BluetoothPbap;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@b
    invoke-interface {v1, v3}, Landroid/bluetooth/IBluetoothManager;->unregisterStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_e
    .catchall {:try_start_9 .. :try_end_e} :catchall_31
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_e} :catch_28

    #@e
    .line 190
    :cond_e
    :goto_e
    :try_start_e
    iget-object v4, p0, Landroid/bluetooth/BluetoothPbap;->mConnection:Landroid/content/ServiceConnection;

    #@10
    monitor-enter v4
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_31

    #@11
    .line 191
    :try_start_11
    iget-object v3, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;
    :try_end_13
    .catchall {:try_start_11 .. :try_end_13} :catchall_3d

    #@13
    if-eqz v3, :cond_22

    #@15
    .line 193
    const/4 v3, 0x0

    #@16
    :try_start_16
    iput-object v3, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@18
    .line 194
    iget-object v3, p0, Landroid/bluetooth/BluetoothPbap;->mContext:Landroid/content/Context;

    #@1a
    iget-object v5, p0, Landroid/bluetooth/BluetoothPbap;->mConnection:Landroid/content/ServiceConnection;

    #@1c
    invoke-virtual {v3, v5}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@1f
    .line 195
    const/4 v3, 0x0

    #@20
    iput-object v3, p0, Landroid/bluetooth/BluetoothPbap;->mConnection:Landroid/content/ServiceConnection;
    :try_end_22
    .catchall {:try_start_16 .. :try_end_22} :catchall_3d
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_22} :catch_34

    #@22
    .line 200
    :cond_22
    :goto_22
    :try_start_22
    monitor-exit v4
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_3d

    #@23
    .line 201
    const/4 v3, 0x0

    #@24
    :try_start_24
    iput-object v3, p0, Landroid/bluetooth/BluetoothPbap;->mServiceListener:Landroid/bluetooth/BluetoothPbap$ServiceListener;
    :try_end_26
    .catchall {:try_start_24 .. :try_end_26} :catchall_31

    #@26
    .line 202
    monitor-exit p0

    #@27
    return-void

    #@28
    .line 185
    :catch_28
    move-exception v0

    #@29
    .line 186
    .local v0, e:Ljava/lang/Exception;
    :try_start_29
    const-string v3, "BluetoothPbap"

    #@2b
    const-string v4, ""

    #@2d
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_30
    .catchall {:try_start_29 .. :try_end_30} :catchall_31

    #@30
    goto :goto_e

    #@31
    .line 181
    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #mgr:Landroid/bluetooth/IBluetoothManager;
    :catchall_31
    move-exception v3

    #@32
    monitor-exit p0

    #@33
    throw v3

    #@34
    .line 196
    .restart local v1       #mgr:Landroid/bluetooth/IBluetoothManager;
    :catch_34
    move-exception v2

    #@35
    .line 197
    .local v2, re:Ljava/lang/Exception;
    :try_start_35
    const-string v3, "BluetoothPbap"

    #@37
    const-string v5, ""

    #@39
    invoke-static {v3, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3c
    goto :goto_22

    #@3d
    .line 200
    .end local v2           #re:Ljava/lang/Exception;
    :catchall_3d
    move-exception v3

    #@3e
    monitor-exit v4
    :try_end_3f
    .catchall {:try_start_35 .. :try_end_3f} :catchall_3d

    #@3f
    :try_start_3f
    throw v3
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_31
.end method

.method public disconnect()Z
    .registers 4

    #@0
    .prologue
    .line 265
    const-string v1, "disconnect()"

    #@2
    invoke-static {v1}, Landroid/bluetooth/BluetoothPbap;->log(Ljava/lang/String;)V

    #@5
    .line 266
    iget-object v1, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@7
    if-eqz v1, :cond_1c

    #@9
    .line 268
    :try_start_9
    iget-object v1, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@b
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothPbap;->disconnect()V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_10

    #@e
    .line 269
    const/4 v1, 0x1

    #@f
    .line 275
    :goto_f
    return v1

    #@10
    .line 270
    :catch_10
    move-exception v0

    #@11
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothPbap"

    #@13
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 275
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_f

    #@1c
    .line 272
    :cond_1c
    const-string v1, "BluetoothPbap"

    #@1e
    const-string v2, "Proxy not attached to service"

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 273
    new-instance v1, Ljava/lang/Throwable;

    #@25
    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    #@28
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v1}, Landroid/bluetooth/BluetoothPbap;->log(Ljava/lang/String;)V

    #@2f
    goto :goto_1a
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 168
    :try_start_0
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothPbap;->close()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    #@3
    .line 170
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 172
    return-void

    #@7
    .line 170
    :catchall_7
    move-exception v0

    #@8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@b
    throw v0
.end method

.method public getClient()Landroid/bluetooth/BluetoothDevice;
    .registers 4

    #@0
    .prologue
    .line 229
    const-string v1, "getClient()"

    #@2
    invoke-static {v1}, Landroid/bluetooth/BluetoothPbap;->log(Ljava/lang/String;)V

    #@5
    .line 230
    iget-object v1, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@7
    if-eqz v1, :cond_1c

    #@9
    .line 232
    :try_start_9
    iget-object v1, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@b
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothPbap;->getClient()Landroid/bluetooth/BluetoothDevice;
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_10

    #@e
    move-result-object v1

    #@f
    .line 238
    :goto_f
    return-object v1

    #@10
    .line 233
    :catch_10
    move-exception v0

    #@11
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothPbap"

    #@13
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 238
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_f

    #@1c
    .line 235
    :cond_1c
    const-string v1, "BluetoothPbap"

    #@1e
    const-string v2, "Proxy not attached to service"

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 236
    new-instance v1, Ljava/lang/Throwable;

    #@25
    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    #@28
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v1}, Landroid/bluetooth/BluetoothPbap;->log(Ljava/lang/String;)V

    #@2f
    goto :goto_1a
.end method

.method public getState()I
    .registers 4

    #@0
    .prologue
    .line 210
    const-string v1, "getState()"

    #@2
    invoke-static {v1}, Landroid/bluetooth/BluetoothPbap;->log(Ljava/lang/String;)V

    #@5
    .line 211
    iget-object v1, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@7
    if-eqz v1, :cond_1c

    #@9
    .line 213
    :try_start_9
    iget-object v1, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@b
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothPbap;->getState()I
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_10

    #@e
    move-result v1

    #@f
    .line 219
    :goto_f
    return v1

    #@10
    .line 214
    :catch_10
    move-exception v0

    #@11
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothPbap"

    #@13
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 219
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_1a
    const/4 v1, -0x1

    #@1b
    goto :goto_f

    #@1c
    .line 216
    :cond_1c
    const-string v1, "BluetoothPbap"

    #@1e
    const-string v2, "Proxy not attached to service"

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 217
    new-instance v1, Ljava/lang/Throwable;

    #@25
    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    #@28
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v1}, Landroid/bluetooth/BluetoothPbap;->log(Ljava/lang/String;)V

    #@2f
    goto :goto_1a
.end method

.method public isConnected(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 247
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "isConnected("

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ")"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v1}, Landroid/bluetooth/BluetoothPbap;->log(Ljava/lang/String;)V

    #@1c
    .line 248
    iget-object v1, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@1e
    if-eqz v1, :cond_33

    #@20
    .line 250
    :try_start_20
    iget-object v1, p0, Landroid/bluetooth/BluetoothPbap;->mService:Landroid/bluetooth/IBluetoothPbap;

    #@22
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothPbap;->isConnected(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_25} :catch_27

    #@25
    move-result v1

    #@26
    .line 256
    :goto_26
    return v1

    #@27
    .line 251
    :catch_27
    move-exception v0

    #@28
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BluetoothPbap"

    #@2a
    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 256
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_31
    const/4 v1, 0x0

    #@32
    goto :goto_26

    #@33
    .line 253
    :cond_33
    const-string v1, "BluetoothPbap"

    #@35
    const-string v2, "Proxy not attached to service"

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 254
    new-instance v1, Ljava/lang/Throwable;

    #@3c
    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    #@3f
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-static {v1}, Landroid/bluetooth/BluetoothPbap;->log(Ljava/lang/String;)V

    #@46
    goto :goto_31
.end method
