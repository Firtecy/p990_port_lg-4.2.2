.class public abstract Landroid/bluetooth/IBluetoothInputDevice$Stub;
.super Landroid/os/Binder;
.source "IBluetoothInputDevice.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothInputDevice;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothInputDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.bluetooth.IBluetoothInputDevice"

.field static final TRANSACTION_connect:I = 0x1

.field static final TRANSACTION_disconnect:I = 0x2

.field static final TRANSACTION_getConnectedDevices:I = 0x3

.field static final TRANSACTION_getConnectionState:I = 0x5

.field static final TRANSACTION_getDevicesMatchingConnectionStates:I = 0x4

.field static final TRANSACTION_getPriority:I = 0x7

.field static final TRANSACTION_getProtocolMode:I = 0x8

.field static final TRANSACTION_getReport:I = 0xb

.field static final TRANSACTION_sendData:I = 0xd

.field static final TRANSACTION_setPriority:I = 0x6

.field static final TRANSACTION_setProtocolMode:I = 0xa

.field static final TRANSACTION_setReport:I = 0xc

.field static final TRANSACTION_virtualUnplug:I = 0x9


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.bluetooth.IBluetoothInputDevice"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothInputDevice;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.bluetooth.IBluetoothInputDevice"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/bluetooth/IBluetoothInputDevice;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/bluetooth/IBluetoothInputDevice;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 14
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_1d8

    #@5
    .line 250
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v7

    #@9
    :goto_9
    return v7

    #@a
    .line 47
    :sswitch_a
    const-string v6, "android.bluetooth.IBluetoothInputDevice"

    #@c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v8, "android.bluetooth.IBluetoothInputDevice"

    #@12
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v8

    #@19
    if-eqz v8, :cond_31

    #@1b
    .line 55
    sget-object v8, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@23
    .line 60
    .local v0, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_23
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@26
    move-result v4

    #@27
    .line 61
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a
    .line 62
    if-eqz v4, :cond_2d

    #@2c
    move v6, v7

    #@2d
    :cond_2d
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    goto :goto_9

    #@31
    .line 58
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #_result:Z
    :cond_31
    const/4 v0, 0x0

    #@32
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_23

    #@33
    .line 67
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_33
    const-string v8, "android.bluetooth.IBluetoothInputDevice"

    #@35
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@38
    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v8

    #@3c
    if-eqz v8, :cond_54

    #@3e
    .line 70
    sget-object v8, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@40
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@43
    move-result-object v0

    #@44
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@46
    .line 75
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_46
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@49
    move-result v4

    #@4a
    .line 76
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d
    .line 77
    if-eqz v4, :cond_50

    #@4f
    move v6, v7

    #@50
    :cond_50
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@53
    goto :goto_9

    #@54
    .line 73
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #_result:Z
    :cond_54
    const/4 v0, 0x0

    #@55
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_46

    #@56
    .line 82
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_56
    const-string v6, "android.bluetooth.IBluetoothInputDevice"

    #@58
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5b
    .line 83
    invoke-virtual {p0}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->getConnectedDevices()Ljava/util/List;

    #@5e
    move-result-object v5

    #@5f
    .line 84
    .local v5, _result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@62
    .line 85
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@65
    goto :goto_9

    #@66
    .line 90
    .end local v5           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :sswitch_66
    const-string v6, "android.bluetooth.IBluetoothInputDevice"

    #@68
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6b
    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@6e
    move-result-object v0

    #@6f
    .line 93
    .local v0, _arg0:[I
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@72
    move-result-object v5

    #@73
    .line 94
    .restart local v5       #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@76
    .line 95
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@79
    goto :goto_9

    #@7a
    .line 100
    .end local v0           #_arg0:[I
    .end local v5           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :sswitch_7a
    const-string v6, "android.bluetooth.IBluetoothInputDevice"

    #@7c
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7f
    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@82
    move-result v6

    #@83
    if-eqz v6, :cond_99

    #@85
    .line 103
    sget-object v6, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@87
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@8a
    move-result-object v0

    #@8b
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@8d
    .line 108
    .local v0, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_8d
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@90
    move-result v4

    #@91
    .line 109
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@94
    .line 110
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@97
    goto/16 :goto_9

    #@99
    .line 106
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #_result:I
    :cond_99
    const/4 v0, 0x0

    #@9a
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_8d

    #@9b
    .line 115
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_9b
    const-string v8, "android.bluetooth.IBluetoothInputDevice"

    #@9d
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a0
    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a3
    move-result v8

    #@a4
    if-eqz v8, :cond_c1

    #@a6
    .line 118
    sget-object v8, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a8
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@ab
    move-result-object v0

    #@ac
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@ae
    .line 124
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_ae
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b1
    move-result v1

    #@b2
    .line 125
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@b5
    move-result v4

    #@b6
    .line 126
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b9
    .line 127
    if-eqz v4, :cond_bc

    #@bb
    move v6, v7

    #@bc
    :cond_bc
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@bf
    goto/16 :goto_9

    #@c1
    .line 121
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:I
    .end local v4           #_result:Z
    :cond_c1
    const/4 v0, 0x0

    #@c2
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_ae

    #@c3
    .line 132
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_c3
    const-string v6, "android.bluetooth.IBluetoothInputDevice"

    #@c5
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c8
    .line 134
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@cb
    move-result v6

    #@cc
    if-eqz v6, :cond_e2

    #@ce
    .line 135
    sget-object v6, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@d0
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@d3
    move-result-object v0

    #@d4
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@d6
    .line 140
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_d6
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@d9
    move-result v4

    #@da
    .line 141
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@dd
    .line 142
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@e0
    goto/16 :goto_9

    #@e2
    .line 138
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #_result:I
    :cond_e2
    const/4 v0, 0x0

    #@e3
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_d6

    #@e4
    .line 147
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_e4
    const-string v8, "android.bluetooth.IBluetoothInputDevice"

    #@e6
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e9
    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ec
    move-result v8

    #@ed
    if-eqz v8, :cond_106

    #@ef
    .line 150
    sget-object v8, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f1
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f4
    move-result-object v0

    #@f5
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@f7
    .line 155
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_f7
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->getProtocolMode(Landroid/bluetooth/BluetoothDevice;)Z

    #@fa
    move-result v4

    #@fb
    .line 156
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fe
    .line 157
    if-eqz v4, :cond_101

    #@100
    move v6, v7

    #@101
    :cond_101
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@104
    goto/16 :goto_9

    #@106
    .line 153
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #_result:Z
    :cond_106
    const/4 v0, 0x0

    #@107
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_f7

    #@108
    .line 162
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_108
    const-string v8, "android.bluetooth.IBluetoothInputDevice"

    #@10a
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10d
    .line 164
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@110
    move-result v8

    #@111
    if-eqz v8, :cond_12a

    #@113
    .line 165
    sget-object v8, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@115
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@118
    move-result-object v0

    #@119
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@11b
    .line 170
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_11b
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->virtualUnplug(Landroid/bluetooth/BluetoothDevice;)Z

    #@11e
    move-result v4

    #@11f
    .line 171
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@122
    .line 172
    if-eqz v4, :cond_125

    #@124
    move v6, v7

    #@125
    :cond_125
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@128
    goto/16 :goto_9

    #@12a
    .line 168
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #_result:Z
    :cond_12a
    const/4 v0, 0x0

    #@12b
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_11b

    #@12c
    .line 177
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_12c
    const-string v8, "android.bluetooth.IBluetoothInputDevice"

    #@12e
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@131
    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@134
    move-result v8

    #@135
    if-eqz v8, :cond_152

    #@137
    .line 180
    sget-object v8, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@139
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@13c
    move-result-object v0

    #@13d
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@13f
    .line 186
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_13f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@142
    move-result v1

    #@143
    .line 187
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->setProtocolMode(Landroid/bluetooth/BluetoothDevice;I)Z

    #@146
    move-result v4

    #@147
    .line 188
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@14a
    .line 189
    if-eqz v4, :cond_14d

    #@14c
    move v6, v7

    #@14d
    :cond_14d
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@150
    goto/16 :goto_9

    #@152
    .line 183
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:I
    .end local v4           #_result:Z
    :cond_152
    const/4 v0, 0x0

    #@153
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_13f

    #@154
    .line 194
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_154
    const-string v8, "android.bluetooth.IBluetoothInputDevice"

    #@156
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@159
    .line 196
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@15c
    move-result v8

    #@15d
    if-eqz v8, :cond_182

    #@15f
    .line 197
    sget-object v8, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@161
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@164
    move-result-object v0

    #@165
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@167
    .line 203
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_167
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@16a
    move-result v1

    #@16b
    .line 205
    .local v1, _arg1:B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@16e
    move-result v2

    #@16f
    .line 207
    .local v2, _arg2:B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@172
    move-result v3

    #@173
    .line 208
    .local v3, _arg3:I
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->getReport(Landroid/bluetooth/BluetoothDevice;BBI)Z

    #@176
    move-result v4

    #@177
    .line 209
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@17a
    .line 210
    if-eqz v4, :cond_17d

    #@17c
    move v6, v7

    #@17d
    :cond_17d
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@180
    goto/16 :goto_9

    #@182
    .line 200
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:B
    .end local v2           #_arg2:B
    .end local v3           #_arg3:I
    .end local v4           #_result:Z
    :cond_182
    const/4 v0, 0x0

    #@183
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_167

    #@184
    .line 215
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_184
    const-string v8, "android.bluetooth.IBluetoothInputDevice"

    #@186
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@189
    .line 217
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18c
    move-result v8

    #@18d
    if-eqz v8, :cond_1ae

    #@18f
    .line 218
    sget-object v8, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@191
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@194
    move-result-object v0

    #@195
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@197
    .line 224
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_197
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@19a
    move-result v1

    #@19b
    .line 226
    .restart local v1       #_arg1:B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@19e
    move-result-object v2

    #@19f
    .line 227
    .local v2, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->setReport(Landroid/bluetooth/BluetoothDevice;BLjava/lang/String;)Z

    #@1a2
    move-result v4

    #@1a3
    .line 228
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a6
    .line 229
    if-eqz v4, :cond_1a9

    #@1a8
    move v6, v7

    #@1a9
    :cond_1a9
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1ac
    goto/16 :goto_9

    #@1ae
    .line 221
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:B
    .end local v2           #_arg2:Ljava/lang/String;
    .end local v4           #_result:Z
    :cond_1ae
    const/4 v0, 0x0

    #@1af
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_197

    #@1b0
    .line 234
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_1b0
    const-string v8, "android.bluetooth.IBluetoothInputDevice"

    #@1b2
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b5
    .line 236
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b8
    move-result v8

    #@1b9
    if-eqz v8, :cond_1d6

    #@1bb
    .line 237
    sget-object v8, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1bd
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1c0
    move-result-object v0

    #@1c1
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@1c3
    .line 243
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_1c3
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c6
    move-result-object v1

    #@1c7
    .line 244
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/bluetooth/IBluetoothInputDevice$Stub;->sendData(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z

    #@1ca
    move-result v4

    #@1cb
    .line 245
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ce
    .line 246
    if-eqz v4, :cond_1d1

    #@1d0
    move v6, v7

    #@1d1
    :cond_1d1
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1d4
    goto/16 :goto_9

    #@1d6
    .line 240
    .end local v0           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v4           #_result:Z
    :cond_1d6
    const/4 v0, 0x0

    #@1d7
    .restart local v0       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_1c3

    #@1d8
    .line 43
    :sswitch_data_1d8
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_33
        0x3 -> :sswitch_56
        0x4 -> :sswitch_66
        0x5 -> :sswitch_7a
        0x6 -> :sswitch_9b
        0x7 -> :sswitch_c3
        0x8 -> :sswitch_e4
        0x9 -> :sswitch_108
        0xa -> :sswitch_12c
        0xb -> :sswitch_154
        0xc -> :sswitch_184
        0xd -> :sswitch_1b0
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
