.class Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;
.super Ljava/lang/Object;
.source "IBluetoothA2dp.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothA2dp;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothA2dp$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 167
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 168
    iput-object p1, p0, Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 169
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 9
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 182
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 183
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 186
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothA2dp"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 187
    if-eqz p1, :cond_30

    #@11
    .line 188
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 189
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 194
    :goto_19
    iget-object v4, p0, Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v5, 0x1

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 195
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 196
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_35

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_3d

    #@29
    .line 199
    .local v2, _result:Z
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 200
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 202
    return v2

    #@30
    .line 192
    .end local v2           #_result:Z
    :cond_30
    const/4 v4, 0x0

    #@31
    :try_start_31
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_35

    #@34
    goto :goto_19

    #@35
    .line 199
    :catchall_35
    move-exception v3

    #@36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 200
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    throw v3

    #@3d
    :cond_3d
    move v2, v3

    #@3e
    .line 196
    goto :goto_29
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 9
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 206
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 207
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 210
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothA2dp"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 211
    if-eqz p1, :cond_30

    #@11
    .line 212
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 213
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 218
    :goto_19
    iget-object v4, p0, Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v5, 0x2

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 219
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 220
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_35

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_3d

    #@29
    .line 223
    .local v2, _result:Z
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 224
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 226
    return v2

    #@30
    .line 216
    .end local v2           #_result:Z
    :cond_30
    const/4 v4, 0x0

    #@31
    :try_start_31
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_35

    #@34
    goto :goto_19

    #@35
    .line 223
    :catchall_35
    move-exception v3

    #@36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 224
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    throw v3

    #@3d
    :cond_3d
    move v2, v3

    #@3e
    .line 220
    goto :goto_29
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 230
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 231
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 234
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.bluetooth.IBluetoothA2dp"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 235
    iget-object v3, p0, Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x3

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 236
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 237
    sget-object v3, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@19
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_24

    #@1c
    move-result-object v2

    #@1d
    .line 240
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 241
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 243
    return-object v2

    #@24
    .line 240
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :catchall_24
    move-exception v3

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 241
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v3
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 8
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 265
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 266
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 269
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.bluetooth.IBluetoothA2dp"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 270
    if-eqz p1, :cond_2c

    #@f
    .line 271
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 272
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 277
    :goto_17
    iget-object v3, p0, Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x5

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 278
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 279
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_31

    #@24
    move-result v2

    #@25
    .line 282
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 283
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 285
    return v2

    #@2c
    .line 275
    .end local v2           #_result:I
    :cond_2c
    const/4 v3, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_17

    #@31
    .line 282
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 283
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 8
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 247
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 248
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 251
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.bluetooth.IBluetoothA2dp"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 252
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeIntArray([I)V

    #@10
    .line 253
    iget-object v3, p0, Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x4

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 254
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 255
    sget-object v3, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    :try_end_1f
    .catchall {:try_start_8 .. :try_end_1f} :catchall_27

    #@1f
    move-result-object v2

    #@20
    .line 258
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 259
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 261
    return-object v2

    #@27
    .line 258
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 259
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 176
    const-string v0, "android.bluetooth.IBluetoothA2dp"

    #@2
    return-object v0
.end method

.method public getPriority(Landroid/bluetooth/BluetoothDevice;)I
    .registers 8
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 314
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 315
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 318
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.bluetooth.IBluetoothA2dp"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 319
    if-eqz p1, :cond_2c

    #@f
    .line 320
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 321
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 326
    :goto_17
    iget-object v3, p0, Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x7

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 327
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 328
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_31

    #@24
    move-result v2

    #@25
    .line 331
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 332
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 334
    return v2

    #@2c
    .line 324
    .end local v2           #_result:I
    :cond_2c
    const/4 v3, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_17

    #@31
    .line 331
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 332
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method

.method public isA2dpPlaying(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 9
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 338
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 339
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 342
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothA2dp"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 343
    if-eqz p1, :cond_31

    #@11
    .line 344
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 345
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 350
    :goto_19
    iget-object v4, p0, Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/16 v5, 0x8

    #@1d
    const/4 v6, 0x0

    #@1e
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 351
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 352
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_27
    .catchall {:try_start_a .. :try_end_27} :catchall_36

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_3e

    #@2a
    .line 355
    .local v2, _result:Z
    :goto_2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 356
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 358
    return v2

    #@31
    .line 348
    .end local v2           #_result:Z
    :cond_31
    const/4 v4, 0x0

    #@32
    :try_start_32
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_35
    .catchall {:try_start_32 .. :try_end_35} :catchall_36

    #@35
    goto :goto_19

    #@36
    .line 355
    :catchall_36
    move-exception v3

    #@37
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 356
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    throw v3

    #@3e
    :cond_3e
    move v2, v3

    #@3f
    .line 352
    goto :goto_2a
.end method

.method public setPriority(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 10
    .parameter "device"
    .parameter "priority"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 289
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 290
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 293
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothA2dp"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 294
    if-eqz p1, :cond_33

    #@11
    .line 295
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 296
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 301
    :goto_19
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 302
    iget-object v4, p0, Landroid/bluetooth/IBluetoothA2dp$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/4 v5, 0x6

    #@1f
    const/4 v6, 0x0

    #@20
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@23
    .line 303
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@26
    .line 304
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_29
    .catchall {:try_start_a .. :try_end_29} :catchall_38

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_40

    #@2c
    .line 307
    .local v2, _result:Z
    :goto_2c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 308
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 310
    return v2

    #@33
    .line 299
    .end local v2           #_result:Z
    :cond_33
    const/4 v4, 0x0

    #@34
    :try_start_34
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_38

    #@37
    goto :goto_19

    #@38
    .line 307
    :catchall_38
    move-exception v3

    #@39
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 308
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    throw v3

    #@40
    :cond_40
    move v2, v3

    #@41
    .line 304
    goto :goto_2c
.end method
