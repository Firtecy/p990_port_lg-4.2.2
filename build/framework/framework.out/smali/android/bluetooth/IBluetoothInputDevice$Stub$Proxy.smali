.class Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;
.super Ljava/lang/Object;
.source "IBluetoothInputDevice.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothInputDevice;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothInputDevice$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 256
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 257
    iput-object p1, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 258
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 261
    iget-object v0, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 9
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 271
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 272
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 275
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothInputDevice"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 276
    if-eqz p1, :cond_30

    #@11
    .line 277
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 278
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 283
    :goto_19
    iget-object v4, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v5, 0x1

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 284
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 285
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_35

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_3d

    #@29
    .line 288
    .local v2, _result:Z
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 289
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 291
    return v2

    #@30
    .line 281
    .end local v2           #_result:Z
    :cond_30
    const/4 v4, 0x0

    #@31
    :try_start_31
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_35

    #@34
    goto :goto_19

    #@35
    .line 288
    :catchall_35
    move-exception v3

    #@36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 289
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    throw v3

    #@3d
    :cond_3d
    move v2, v3

    #@3e
    .line 285
    goto :goto_29
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 9
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 295
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 296
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 299
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothInputDevice"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 300
    if-eqz p1, :cond_30

    #@11
    .line 301
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 302
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 307
    :goto_19
    iget-object v4, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v5, 0x2

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 308
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 309
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_35

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_3d

    #@29
    .line 312
    .local v2, _result:Z
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 313
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 315
    return v2

    #@30
    .line 305
    .end local v2           #_result:Z
    :cond_30
    const/4 v4, 0x0

    #@31
    :try_start_31
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_35

    #@34
    goto :goto_19

    #@35
    .line 312
    :catchall_35
    move-exception v3

    #@36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 313
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    throw v3

    #@3d
    :cond_3d
    move v2, v3

    #@3e
    .line 309
    goto :goto_29
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 319
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 320
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 323
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.bluetooth.IBluetoothInputDevice"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 324
    iget-object v3, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x3

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 325
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 326
    sget-object v3, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@19
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_24

    #@1c
    move-result-object v2

    #@1d
    .line 329
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 330
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 332
    return-object v2

    #@24
    .line 329
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :catchall_24
    move-exception v3

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 330
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v3
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 8
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 354
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 355
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 358
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.bluetooth.IBluetoothInputDevice"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 359
    if-eqz p1, :cond_2c

    #@f
    .line 360
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 361
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 366
    :goto_17
    iget-object v3, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x5

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 367
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 368
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_31

    #@24
    move-result v2

    #@25
    .line 371
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 372
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 374
    return v2

    #@2c
    .line 364
    .end local v2           #_result:I
    :cond_2c
    const/4 v3, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_17

    #@31
    .line 371
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 372
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 8
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 336
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 337
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 340
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.bluetooth.IBluetoothInputDevice"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 341
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeIntArray([I)V

    #@10
    .line 342
    iget-object v3, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x4

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 343
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 344
    sget-object v3, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    :try_end_1f
    .catchall {:try_start_8 .. :try_end_1f} :catchall_27

    #@1f
    move-result-object v2

    #@20
    .line 347
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 348
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 350
    return-object v2

    #@27
    .line 347
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 348
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 265
    const-string v0, "android.bluetooth.IBluetoothInputDevice"

    #@2
    return-object v0
.end method

.method public getPriority(Landroid/bluetooth/BluetoothDevice;)I
    .registers 8
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 403
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 404
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 407
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.bluetooth.IBluetoothInputDevice"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 408
    if-eqz p1, :cond_2c

    #@f
    .line 409
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 410
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 415
    :goto_17
    iget-object v3, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x7

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 416
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 417
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_31

    #@24
    move-result v2

    #@25
    .line 420
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 421
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 423
    return v2

    #@2c
    .line 413
    .end local v2           #_result:I
    :cond_2c
    const/4 v3, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_17

    #@31
    .line 420
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 421
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method

.method public getProtocolMode(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 9
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 430
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 431
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 434
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothInputDevice"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 435
    if-eqz p1, :cond_31

    #@11
    .line 436
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 437
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 442
    :goto_19
    iget-object v4, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/16 v5, 0x8

    #@1d
    const/4 v6, 0x0

    #@1e
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 443
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 444
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_27
    .catchall {:try_start_a .. :try_end_27} :catchall_36

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_3e

    #@2a
    .line 447
    .local v2, _result:Z
    :goto_2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 448
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 450
    return v2

    #@31
    .line 440
    .end local v2           #_result:Z
    :cond_31
    const/4 v4, 0x0

    #@32
    :try_start_32
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_35
    .catchall {:try_start_32 .. :try_end_35} :catchall_36

    #@35
    goto :goto_19

    #@36
    .line 447
    :catchall_36
    move-exception v3

    #@37
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 448
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    throw v3

    #@3e
    :cond_3e
    move v2, v3

    #@3f
    .line 444
    goto :goto_2a
.end method

.method public getReport(Landroid/bluetooth/BluetoothDevice;BBI)Z
    .registers 12
    .parameter "device"
    .parameter "reportType"
    .parameter "reportId"
    .parameter "bufferSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 512
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 513
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 516
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothInputDevice"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 517
    if-eqz p1, :cond_3a

    #@11
    .line 518
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 519
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 524
    :goto_19
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByte(B)V

    #@1c
    .line 525
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeByte(B)V

    #@1f
    .line 526
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 527
    iget-object v4, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/16 v5, 0xb

    #@26
    const/4 v6, 0x0

    #@27
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 528
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 529
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_30
    .catchall {:try_start_a .. :try_end_30} :catchall_3f

    #@30
    move-result v4

    #@31
    if-eqz v4, :cond_47

    #@33
    .line 532
    .local v2, _result:Z
    :goto_33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 533
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 535
    return v2

    #@3a
    .line 522
    .end local v2           #_result:Z
    :cond_3a
    const/4 v4, 0x0

    #@3b
    :try_start_3b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_3f

    #@3e
    goto :goto_19

    #@3f
    .line 532
    :catchall_3f
    move-exception v3

    #@40
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@43
    .line 533
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@46
    throw v3

    #@47
    :cond_47
    move v2, v3

    #@48
    .line 529
    goto :goto_33
.end method

.method public sendData(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z
    .registers 10
    .parameter "device"
    .parameter "report"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 571
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 572
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 575
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothInputDevice"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 576
    if-eqz p1, :cond_34

    #@11
    .line 577
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 578
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 583
    :goto_19
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 584
    iget-object v4, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v5, 0xd

    #@20
    const/4 v6, 0x0

    #@21
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 585
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 586
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2a
    .catchall {:try_start_a .. :try_end_2a} :catchall_39

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_41

    #@2d
    .line 589
    .local v2, _result:Z
    :goto_2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 590
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 592
    return v2

    #@34
    .line 581
    .end local v2           #_result:Z
    :cond_34
    const/4 v4, 0x0

    #@35
    :try_start_35
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_39

    #@38
    goto :goto_19

    #@39
    .line 589
    :catchall_39
    move-exception v3

    #@3a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 590
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    throw v3

    #@41
    :cond_41
    move v2, v3

    #@42
    .line 586
    goto :goto_2d
.end method

.method public setPriority(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 10
    .parameter "device"
    .parameter "priority"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 378
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 379
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 382
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothInputDevice"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 383
    if-eqz p1, :cond_33

    #@11
    .line 384
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 385
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 390
    :goto_19
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 391
    iget-object v4, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/4 v5, 0x6

    #@1f
    const/4 v6, 0x0

    #@20
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@23
    .line 392
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@26
    .line 393
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_29
    .catchall {:try_start_a .. :try_end_29} :catchall_38

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_40

    #@2c
    .line 396
    .local v2, _result:Z
    :goto_2c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 397
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 399
    return v2

    #@33
    .line 388
    .end local v2           #_result:Z
    :cond_33
    const/4 v4, 0x0

    #@34
    :try_start_34
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_38

    #@37
    goto :goto_19

    #@38
    .line 396
    :catchall_38
    move-exception v3

    #@39
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 397
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    throw v3

    #@40
    :cond_40
    move v2, v3

    #@41
    .line 393
    goto :goto_2c
.end method

.method public setProtocolMode(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 10
    .parameter "device"
    .parameter "protocolMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 484
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 485
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 488
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothInputDevice"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 489
    if-eqz p1, :cond_34

    #@11
    .line 490
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 491
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 496
    :goto_19
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 497
    iget-object v4, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v5, 0xa

    #@20
    const/4 v6, 0x0

    #@21
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 498
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 499
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2a
    .catchall {:try_start_a .. :try_end_2a} :catchall_39

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_41

    #@2d
    .line 502
    .local v2, _result:Z
    :goto_2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 503
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 505
    return v2

    #@34
    .line 494
    .end local v2           #_result:Z
    :cond_34
    const/4 v4, 0x0

    #@35
    :try_start_35
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_39

    #@38
    goto :goto_19

    #@39
    .line 502
    :catchall_39
    move-exception v3

    #@3a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 503
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    throw v3

    #@41
    :cond_41
    move v2, v3

    #@42
    .line 499
    goto :goto_2d
.end method

.method public setReport(Landroid/bluetooth/BluetoothDevice;BLjava/lang/String;)Z
    .registers 11
    .parameter "device"
    .parameter "reportType"
    .parameter "report"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 542
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 543
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 546
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothInputDevice"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 547
    if-eqz p1, :cond_37

    #@11
    .line 548
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 549
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 554
    :goto_19
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByte(B)V

    #@1c
    .line 555
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1f
    .line 556
    iget-object v4, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@21
    const/16 v5, 0xc

    #@23
    const/4 v6, 0x0

    #@24
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 557
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 558
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2d
    .catchall {:try_start_a .. :try_end_2d} :catchall_3c

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_44

    #@30
    .line 561
    .local v2, _result:Z
    :goto_30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 562
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 564
    return v2

    #@37
    .line 552
    .end local v2           #_result:Z
    :cond_37
    const/4 v4, 0x0

    #@38
    :try_start_38
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3b
    .catchall {:try_start_38 .. :try_end_3b} :catchall_3c

    #@3b
    goto :goto_19

    #@3c
    .line 561
    :catchall_3c
    move-exception v3

    #@3d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 562
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    throw v3

    #@44
    :cond_44
    move v2, v3

    #@45
    .line 558
    goto :goto_30
.end method

.method public virtualUnplug(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 9
    .parameter "device"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 457
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 458
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 461
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.bluetooth.IBluetoothInputDevice"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 462
    if-eqz p1, :cond_31

    #@11
    .line 463
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 464
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/bluetooth/BluetoothDevice;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 469
    :goto_19
    iget-object v4, p0, Landroid/bluetooth/IBluetoothInputDevice$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/16 v5, 0x9

    #@1d
    const/4 v6, 0x0

    #@1e
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 470
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 471
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_27
    .catchall {:try_start_a .. :try_end_27} :catchall_36

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_3e

    #@2a
    .line 474
    .local v2, _result:Z
    :goto_2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 475
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 477
    return v2

    #@31
    .line 467
    .end local v2           #_result:Z
    :cond_31
    const/4 v4, 0x0

    #@32
    :try_start_32
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_35
    .catchall {:try_start_32 .. :try_end_35} :catchall_36

    #@35
    goto :goto_19

    #@36
    .line 474
    :catchall_36
    move-exception v3

    #@37
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 475
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    throw v3

    #@3e
    :cond_3e
    move v2, v3

    #@3f
    .line 471
    goto :goto_2a
.end method
