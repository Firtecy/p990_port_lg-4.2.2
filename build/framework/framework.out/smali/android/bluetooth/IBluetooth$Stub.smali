.class public abstract Landroid/bluetooth/IBluetooth$Stub;
.super Landroid/os/Binder;
.source "IBluetooth.java"

# interfaces
.implements Landroid/bluetooth/IBluetooth;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetooth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/IBluetooth$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.bluetooth.IBluetooth"

.field static final TRANSACTION_cancelBondProcess:I = 0x18

.field static final TRANSACTION_cancelDiscovery:I = 0x12

.field static final TRANSACTION_connectSocket:I = 0x2b

.field static final TRANSACTION_createBond:I = 0x17

.field static final TRANSACTION_createSocketChannel:I = 0x2c

.field static final TRANSACTION_disable:I = 0x5

.field static final TRANSACTION_disableRadio:I = 0x8

.field static final TRANSACTION_enable:I = 0x3

.field static final TRANSACTION_enableNoAutoConnect:I = 0x4

.field static final TRANSACTION_enableRadio:I = 0x7

.field static final TRANSACTION_fetchRemoteUuids:I = 0x24

.field static final TRANSACTION_getAdapterConnectionState:I = 0x14

.field static final TRANSACTION_getAddress:I = 0x9

.field static final TRANSACTION_getBondState:I = 0x1a

.field static final TRANSACTION_getBondedDevices:I = 0x16

.field static final TRANSACTION_getConnectedDevices:I = 0x1e

.field static final TRANSACTION_getDiscoverableTimeout:I = 0xf

.field static final TRANSACTION_getJustWorks:I = 0x1d

.field static final TRANSACTION_getName:I = 0xc

.field static final TRANSACTION_getProfileConnectionState:I = 0x15

.field static final TRANSACTION_getRemoteAlias:I = 0x20

.field static final TRANSACTION_getRemoteClass:I = 0x22

.field static final TRANSACTION_getRemoteName:I = 0x1f

.field static final TRANSACTION_getRemoteUuids:I = 0x23

.field static final TRANSACTION_getScanMode:I = 0xd

.field static final TRANSACTION_getState:I = 0x2

.field static final TRANSACTION_getTrustState:I = 0x1b

.field static final TRANSACTION_getUuids:I = 0xa

.field static final TRANSACTION_isDiscovering:I = 0x13

.field static final TRANSACTION_isEnabled:I = 0x1

.field static final TRANSACTION_isRadioEnabled:I = 0x6

.field static final TRANSACTION_registerCallback:I = 0x29

.field static final TRANSACTION_removeBond:I = 0x19

.field static final TRANSACTION_sendConnectionStateChange:I = 0x28

.field static final TRANSACTION_setDiscoverableTimeout:I = 0x10

.field static final TRANSACTION_setName:I = 0xb

.field static final TRANSACTION_setPairingConfirmation:I = 0x27

.field static final TRANSACTION_setPasskey:I = 0x26

.field static final TRANSACTION_setPin:I = 0x25

.field static final TRANSACTION_setRemoteAlias:I = 0x21

.field static final TRANSACTION_setScanMode:I = 0xe

.field static final TRANSACTION_setTrustState:I = 0x1c

.field static final TRANSACTION_startDiscovery:I = 0x11

.field static final TRANSACTION_unregisterCallback:I = 0x2a


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.bluetooth.IBluetooth"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/bluetooth/IBluetooth$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetooth;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.bluetooth.IBluetooth"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/bluetooth/IBluetooth;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/bluetooth/IBluetooth;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/bluetooth/IBluetooth$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/bluetooth/IBluetooth$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 14
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_50e

    #@5
    .line 604
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v7

    #@9
    :goto_9
    return v7

    #@a
    .line 47
    :sswitch_a
    const-string v0, "android.bluetooth.IBluetooth"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v0, "android.bluetooth.IBluetooth"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 53
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->isEnabled()Z

    #@18
    move-result v6

    #@19
    .line 54
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c
    .line 55
    if-eqz v6, :cond_23

    #@1e
    move v0, v7

    #@1f
    :goto_1f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    goto :goto_9

    #@23
    :cond_23
    move v0, v8

    #@24
    goto :goto_1f

    #@25
    .line 60
    .end local v6           #_result:Z
    :sswitch_25
    const-string v0, "android.bluetooth.IBluetooth"

    #@27
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a
    .line 61
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->getState()I

    #@2d
    move-result v6

    #@2e
    .line 62
    .local v6, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@31
    .line 63
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    goto :goto_9

    #@35
    .line 68
    .end local v6           #_result:I
    :sswitch_35
    const-string v0, "android.bluetooth.IBluetooth"

    #@37
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a
    .line 69
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->enable()Z

    #@3d
    move-result v6

    #@3e
    .line 70
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@41
    .line 71
    if-eqz v6, :cond_44

    #@43
    move v8, v7

    #@44
    :cond_44
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@47
    goto :goto_9

    #@48
    .line 76
    .end local v6           #_result:Z
    :sswitch_48
    const-string v0, "android.bluetooth.IBluetooth"

    #@4a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4d
    .line 77
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->enableNoAutoConnect()Z

    #@50
    move-result v6

    #@51
    .line 78
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@54
    .line 79
    if-eqz v6, :cond_57

    #@56
    move v8, v7

    #@57
    :cond_57
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@5a
    goto :goto_9

    #@5b
    .line 84
    .end local v6           #_result:Z
    :sswitch_5b
    const-string v0, "android.bluetooth.IBluetooth"

    #@5d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60
    .line 85
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->disable()Z

    #@63
    move-result v6

    #@64
    .line 86
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@67
    .line 87
    if-eqz v6, :cond_6a

    #@69
    move v8, v7

    #@6a
    :cond_6a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@6d
    goto :goto_9

    #@6e
    .line 92
    .end local v6           #_result:Z
    :sswitch_6e
    const-string v0, "android.bluetooth.IBluetooth"

    #@70
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@73
    .line 93
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->isRadioEnabled()Z

    #@76
    move-result v6

    #@77
    .line 94
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7a
    .line 95
    if-eqz v6, :cond_7d

    #@7c
    move v8, v7

    #@7d
    :cond_7d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@80
    goto :goto_9

    #@81
    .line 100
    .end local v6           #_result:Z
    :sswitch_81
    const-string v0, "android.bluetooth.IBluetooth"

    #@83
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@86
    .line 101
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->enableRadio()Z

    #@89
    move-result v6

    #@8a
    .line 102
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8d
    .line 103
    if-eqz v6, :cond_90

    #@8f
    move v8, v7

    #@90
    :cond_90
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@93
    goto/16 :goto_9

    #@95
    .line 108
    .end local v6           #_result:Z
    :sswitch_95
    const-string v0, "android.bluetooth.IBluetooth"

    #@97
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a
    .line 109
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->disableRadio()Z

    #@9d
    move-result v6

    #@9e
    .line 110
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a1
    .line 111
    if-eqz v6, :cond_a4

    #@a3
    move v8, v7

    #@a4
    :cond_a4
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@a7
    goto/16 :goto_9

    #@a9
    .line 116
    .end local v6           #_result:Z
    :sswitch_a9
    const-string v0, "android.bluetooth.IBluetooth"

    #@ab
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ae
    .line 117
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->getAddress()Ljava/lang/String;

    #@b1
    move-result-object v6

    #@b2
    .line 118
    .local v6, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b5
    .line 119
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b8
    goto/16 :goto_9

    #@ba
    .line 124
    .end local v6           #_result:Ljava/lang/String;
    :sswitch_ba
    const-string v0, "android.bluetooth.IBluetooth"

    #@bc
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bf
    .line 125
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->getUuids()[Landroid/os/ParcelUuid;

    #@c2
    move-result-object v6

    #@c3
    .line 126
    .local v6, _result:[Landroid/os/ParcelUuid;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c6
    .line 127
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@c9
    goto/16 :goto_9

    #@cb
    .line 132
    .end local v6           #_result:[Landroid/os/ParcelUuid;
    :sswitch_cb
    const-string v0, "android.bluetooth.IBluetooth"

    #@cd
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d0
    .line 134
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d3
    move-result-object v1

    #@d4
    .line 135
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->setName(Ljava/lang/String;)Z

    #@d7
    move-result v6

    #@d8
    .line 136
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@db
    .line 137
    if-eqz v6, :cond_de

    #@dd
    move v8, v7

    #@de
    :cond_de
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@e1
    goto/16 :goto_9

    #@e3
    .line 142
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v6           #_result:Z
    :sswitch_e3
    const-string v0, "android.bluetooth.IBluetooth"

    #@e5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e8
    .line 143
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->getName()Ljava/lang/String;

    #@eb
    move-result-object v6

    #@ec
    .line 144
    .local v6, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ef
    .line 145
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f2
    goto/16 :goto_9

    #@f4
    .line 150
    .end local v6           #_result:Ljava/lang/String;
    :sswitch_f4
    const-string v0, "android.bluetooth.IBluetooth"

    #@f6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f9
    .line 151
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->getScanMode()I

    #@fc
    move-result v6

    #@fd
    .line 152
    .local v6, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@100
    .line 153
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@103
    goto/16 :goto_9

    #@105
    .line 158
    .end local v6           #_result:I
    :sswitch_105
    const-string v0, "android.bluetooth.IBluetooth"

    #@107
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10a
    .line 160
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10d
    move-result v1

    #@10e
    .line 162
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@111
    move-result v2

    #@112
    .line 163
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/bluetooth/IBluetooth$Stub;->setScanMode(II)Z

    #@115
    move-result v6

    #@116
    .line 164
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@119
    .line 165
    if-eqz v6, :cond_11c

    #@11b
    move v8, v7

    #@11c
    :cond_11c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@11f
    goto/16 :goto_9

    #@121
    .line 170
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v6           #_result:Z
    :sswitch_121
    const-string v0, "android.bluetooth.IBluetooth"

    #@123
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@126
    .line 171
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->getDiscoverableTimeout()I

    #@129
    move-result v6

    #@12a
    .line 172
    .local v6, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@12d
    .line 173
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@130
    goto/16 :goto_9

    #@132
    .line 178
    .end local v6           #_result:I
    :sswitch_132
    const-string v0, "android.bluetooth.IBluetooth"

    #@134
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@137
    .line 180
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@13a
    move-result v1

    #@13b
    .line 181
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->setDiscoverableTimeout(I)Z

    #@13e
    move-result v6

    #@13f
    .line 182
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@142
    .line 183
    if-eqz v6, :cond_145

    #@144
    move v8, v7

    #@145
    :cond_145
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@148
    goto/16 :goto_9

    #@14a
    .line 188
    .end local v1           #_arg0:I
    .end local v6           #_result:Z
    :sswitch_14a
    const-string v0, "android.bluetooth.IBluetooth"

    #@14c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14f
    .line 189
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->startDiscovery()Z

    #@152
    move-result v6

    #@153
    .line 190
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@156
    .line 191
    if-eqz v6, :cond_159

    #@158
    move v8, v7

    #@159
    :cond_159
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@15c
    goto/16 :goto_9

    #@15e
    .line 196
    .end local v6           #_result:Z
    :sswitch_15e
    const-string v0, "android.bluetooth.IBluetooth"

    #@160
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@163
    .line 197
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->cancelDiscovery()Z

    #@166
    move-result v6

    #@167
    .line 198
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@16a
    .line 199
    if-eqz v6, :cond_16d

    #@16c
    move v8, v7

    #@16d
    :cond_16d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@170
    goto/16 :goto_9

    #@172
    .line 204
    .end local v6           #_result:Z
    :sswitch_172
    const-string v0, "android.bluetooth.IBluetooth"

    #@174
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@177
    .line 205
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->isDiscovering()Z

    #@17a
    move-result v6

    #@17b
    .line 206
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@17e
    .line 207
    if-eqz v6, :cond_181

    #@180
    move v8, v7

    #@181
    :cond_181
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@184
    goto/16 :goto_9

    #@186
    .line 212
    .end local v6           #_result:Z
    :sswitch_186
    const-string v0, "android.bluetooth.IBluetooth"

    #@188
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18b
    .line 213
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->getAdapterConnectionState()I

    #@18e
    move-result v6

    #@18f
    .line 214
    .local v6, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@192
    .line 215
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@195
    goto/16 :goto_9

    #@197
    .line 220
    .end local v6           #_result:I
    :sswitch_197
    const-string v0, "android.bluetooth.IBluetooth"

    #@199
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19c
    .line 222
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@19f
    move-result v1

    #@1a0
    .line 223
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->getProfileConnectionState(I)I

    #@1a3
    move-result v6

    #@1a4
    .line 224
    .restart local v6       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a7
    .line 225
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@1aa
    goto/16 :goto_9

    #@1ac
    .line 230
    .end local v1           #_arg0:I
    .end local v6           #_result:I
    :sswitch_1ac
    const-string v0, "android.bluetooth.IBluetooth"

    #@1ae
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b1
    .line 231
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->getBondedDevices()[Landroid/bluetooth/BluetoothDevice;

    #@1b4
    move-result-object v6

    #@1b5
    .line 232
    .local v6, _result:[Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b8
    .line 233
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@1bb
    goto/16 :goto_9

    #@1bd
    .line 238
    .end local v6           #_result:[Landroid/bluetooth/BluetoothDevice;
    :sswitch_1bd
    const-string v0, "android.bluetooth.IBluetooth"

    #@1bf
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c2
    .line 240
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c5
    move-result v0

    #@1c6
    if-eqz v0, :cond_1df

    #@1c8
    .line 241
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1ca
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1cd
    move-result-object v1

    #@1ce
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@1d0
    .line 246
    .local v1, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_1d0
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->createBond(Landroid/bluetooth/BluetoothDevice;)Z

    #@1d3
    move-result v6

    #@1d4
    .line 247
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d7
    .line 248
    if-eqz v6, :cond_1da

    #@1d9
    move v8, v7

    #@1da
    :cond_1da
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@1dd
    goto/16 :goto_9

    #@1df
    .line 244
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:Z
    :cond_1df
    const/4 v1, 0x0

    #@1e0
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_1d0

    #@1e1
    .line 253
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_1e1
    const-string v0, "android.bluetooth.IBluetooth"

    #@1e3
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e6
    .line 255
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1e9
    move-result v0

    #@1ea
    if-eqz v0, :cond_203

    #@1ec
    .line 256
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1ee
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f1
    move-result-object v1

    #@1f2
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@1f4
    .line 261
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_1f4
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->cancelBondProcess(Landroid/bluetooth/BluetoothDevice;)Z

    #@1f7
    move-result v6

    #@1f8
    .line 262
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1fb
    .line 263
    if-eqz v6, :cond_1fe

    #@1fd
    move v8, v7

    #@1fe
    :cond_1fe
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@201
    goto/16 :goto_9

    #@203
    .line 259
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:Z
    :cond_203
    const/4 v1, 0x0

    #@204
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_1f4

    #@205
    .line 268
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_205
    const-string v0, "android.bluetooth.IBluetooth"

    #@207
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20a
    .line 270
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@20d
    move-result v0

    #@20e
    if-eqz v0, :cond_227

    #@210
    .line 271
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@212
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@215
    move-result-object v1

    #@216
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@218
    .line 276
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_218
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->removeBond(Landroid/bluetooth/BluetoothDevice;)Z

    #@21b
    move-result v6

    #@21c
    .line 277
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@21f
    .line 278
    if-eqz v6, :cond_222

    #@221
    move v8, v7

    #@222
    :cond_222
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@225
    goto/16 :goto_9

    #@227
    .line 274
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:Z
    :cond_227
    const/4 v1, 0x0

    #@228
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_218

    #@229
    .line 283
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_229
    const-string v0, "android.bluetooth.IBluetooth"

    #@22b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@22e
    .line 285
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@231
    move-result v0

    #@232
    if-eqz v0, :cond_248

    #@234
    .line 286
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@236
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@239
    move-result-object v1

    #@23a
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@23c
    .line 291
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_23c
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->getBondState(Landroid/bluetooth/BluetoothDevice;)I

    #@23f
    move-result v6

    #@240
    .line 292
    .local v6, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@243
    .line 293
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@246
    goto/16 :goto_9

    #@248
    .line 289
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:I
    :cond_248
    const/4 v1, 0x0

    #@249
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_23c

    #@24a
    .line 298
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_24a
    const-string v0, "android.bluetooth.IBluetooth"

    #@24c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24f
    .line 300
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@252
    move-result v0

    #@253
    if-eqz v0, :cond_26c

    #@255
    .line 301
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@257
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25a
    move-result-object v1

    #@25b
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@25d
    .line 306
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_25d
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->getTrustState(Landroid/bluetooth/BluetoothDevice;)Z

    #@260
    move-result v6

    #@261
    .line 307
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@264
    .line 308
    if-eqz v6, :cond_267

    #@266
    move v8, v7

    #@267
    :cond_267
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@26a
    goto/16 :goto_9

    #@26c
    .line 304
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:Z
    :cond_26c
    const/4 v1, 0x0

    #@26d
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_25d

    #@26e
    .line 313
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_26e
    const-string v0, "android.bluetooth.IBluetooth"

    #@270
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@273
    .line 315
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@276
    move-result v0

    #@277
    if-eqz v0, :cond_297

    #@279
    .line 316
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@27b
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@27e
    move-result-object v1

    #@27f
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@281
    .line 322
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_281
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@284
    move-result v0

    #@285
    if-eqz v0, :cond_299

    #@287
    move v2, v7

    #@288
    .line 323
    .local v2, _arg1:Z
    :goto_288
    invoke-virtual {p0, v1, v2}, Landroid/bluetooth/IBluetooth$Stub;->setTrustState(Landroid/bluetooth/BluetoothDevice;Z)Z

    #@28b
    move-result v6

    #@28c
    .line 324
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@28f
    .line 325
    if-eqz v6, :cond_292

    #@291
    move v8, v7

    #@292
    :cond_292
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@295
    goto/16 :goto_9

    #@297
    .line 319
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:Z
    .end local v6           #_result:Z
    :cond_297
    const/4 v1, 0x0

    #@298
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_281

    #@299
    :cond_299
    move v2, v8

    #@29a
    .line 322
    goto :goto_288

    #@29b
    .line 330
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_29b
    const-string v0, "android.bluetooth.IBluetooth"

    #@29d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a0
    .line 332
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2a3
    move-result v0

    #@2a4
    if-eqz v0, :cond_2bd

    #@2a6
    .line 333
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2a8
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2ab
    move-result-object v1

    #@2ac
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@2ae
    .line 338
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_2ae
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->getJustWorks(Landroid/bluetooth/BluetoothDevice;)Z

    #@2b1
    move-result v6

    #@2b2
    .line 339
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b5
    .line 340
    if-eqz v6, :cond_2b8

    #@2b7
    move v8, v7

    #@2b8
    :cond_2b8
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@2bb
    goto/16 :goto_9

    #@2bd
    .line 336
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:Z
    :cond_2bd
    const/4 v1, 0x0

    #@2be
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_2ae

    #@2bf
    .line 345
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_2bf
    const-string v0, "android.bluetooth.IBluetooth"

    #@2c1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c4
    .line 346
    invoke-virtual {p0}, Landroid/bluetooth/IBluetooth$Stub;->getConnectedDevices()Ljava/lang/String;

    #@2c7
    move-result-object v6

    #@2c8
    .line 347
    .local v6, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2cb
    .line 348
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2ce
    goto/16 :goto_9

    #@2d0
    .line 353
    .end local v6           #_result:Ljava/lang/String;
    :sswitch_2d0
    const-string v0, "android.bluetooth.IBluetooth"

    #@2d2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d5
    .line 355
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d8
    move-result v0

    #@2d9
    if-eqz v0, :cond_2ef

    #@2db
    .line 356
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2dd
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2e0
    move-result-object v1

    #@2e1
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@2e3
    .line 361
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_2e3
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->getRemoteName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@2e6
    move-result-object v6

    #@2e7
    .line 362
    .restart local v6       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ea
    .line 363
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2ed
    goto/16 :goto_9

    #@2ef
    .line 359
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:Ljava/lang/String;
    :cond_2ef
    const/4 v1, 0x0

    #@2f0
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_2e3

    #@2f1
    .line 368
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_2f1
    const-string v0, "android.bluetooth.IBluetooth"

    #@2f3
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f6
    .line 370
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2f9
    move-result v0

    #@2fa
    if-eqz v0, :cond_310

    #@2fc
    .line 371
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2fe
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@301
    move-result-object v1

    #@302
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@304
    .line 376
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_304
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->getRemoteAlias(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@307
    move-result-object v6

    #@308
    .line 377
    .restart local v6       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@30b
    .line 378
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@30e
    goto/16 :goto_9

    #@310
    .line 374
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:Ljava/lang/String;
    :cond_310
    const/4 v1, 0x0

    #@311
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_304

    #@312
    .line 383
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_312
    const-string v0, "android.bluetooth.IBluetooth"

    #@314
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@317
    .line 385
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@31a
    move-result v0

    #@31b
    if-eqz v0, :cond_338

    #@31d
    .line 386
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@31f
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@322
    move-result-object v1

    #@323
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@325
    .line 392
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_325
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@328
    move-result-object v2

    #@329
    .line 393
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/bluetooth/IBluetooth$Stub;->setRemoteAlias(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z

    #@32c
    move-result v6

    #@32d
    .line 394
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@330
    .line 395
    if-eqz v6, :cond_333

    #@332
    move v8, v7

    #@333
    :cond_333
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@336
    goto/16 :goto_9

    #@338
    .line 389
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v6           #_result:Z
    :cond_338
    const/4 v1, 0x0

    #@339
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_325

    #@33a
    .line 400
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_33a
    const-string v0, "android.bluetooth.IBluetooth"

    #@33c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@33f
    .line 402
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@342
    move-result v0

    #@343
    if-eqz v0, :cond_359

    #@345
    .line 403
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@347
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@34a
    move-result-object v1

    #@34b
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@34d
    .line 408
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_34d
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->getRemoteClass(Landroid/bluetooth/BluetoothDevice;)I

    #@350
    move-result v6

    #@351
    .line 409
    .local v6, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@354
    .line 410
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@357
    goto/16 :goto_9

    #@359
    .line 406
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:I
    :cond_359
    const/4 v1, 0x0

    #@35a
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_34d

    #@35b
    .line 415
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_35b
    const-string v0, "android.bluetooth.IBluetooth"

    #@35d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@360
    .line 417
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@363
    move-result v0

    #@364
    if-eqz v0, :cond_37a

    #@366
    .line 418
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@368
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@36b
    move-result-object v1

    #@36c
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@36e
    .line 423
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_36e
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->getRemoteUuids(Landroid/bluetooth/BluetoothDevice;)[Landroid/os/ParcelUuid;

    #@371
    move-result-object v6

    #@372
    .line 424
    .local v6, _result:[Landroid/os/ParcelUuid;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@375
    .line 425
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@378
    goto/16 :goto_9

    #@37a
    .line 421
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:[Landroid/os/ParcelUuid;
    :cond_37a
    const/4 v1, 0x0

    #@37b
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_36e

    #@37c
    .line 430
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_37c
    const-string v0, "android.bluetooth.IBluetooth"

    #@37e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@381
    .line 432
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@384
    move-result v0

    #@385
    if-eqz v0, :cond_39e

    #@387
    .line 433
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@389
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@38c
    move-result-object v1

    #@38d
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@38f
    .line 438
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_38f
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->fetchRemoteUuids(Landroid/bluetooth/BluetoothDevice;)Z

    #@392
    move-result v6

    #@393
    .line 439
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@396
    .line 440
    if-eqz v6, :cond_399

    #@398
    move v8, v7

    #@399
    :cond_399
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@39c
    goto/16 :goto_9

    #@39e
    .line 436
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #_result:Z
    :cond_39e
    const/4 v1, 0x0

    #@39f
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_38f

    #@3a0
    .line 445
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_3a0
    const-string v0, "android.bluetooth.IBluetooth"

    #@3a2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a5
    .line 447
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3a8
    move-result v0

    #@3a9
    if-eqz v0, :cond_3d1

    #@3ab
    .line 448
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3ad
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3b0
    move-result-object v1

    #@3b1
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@3b3
    .line 454
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_3b3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3b6
    move-result v0

    #@3b7
    if-eqz v0, :cond_3d3

    #@3b9
    move v2, v7

    #@3ba
    .line 456
    .local v2, _arg1:Z
    :goto_3ba
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3bd
    move-result v3

    #@3be
    .line 458
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@3c1
    move-result-object v4

    #@3c2
    .line 459
    .local v4, _arg3:[B
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/bluetooth/IBluetooth$Stub;->setPin(Landroid/bluetooth/BluetoothDevice;ZI[B)Z

    #@3c5
    move-result v6

    #@3c6
    .line 460
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c9
    .line 461
    if-eqz v6, :cond_3cc

    #@3cb
    move v8, v7

    #@3cc
    :cond_3cc
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@3cf
    goto/16 :goto_9

    #@3d1
    .line 451
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:I
    .end local v4           #_arg3:[B
    .end local v6           #_result:Z
    :cond_3d1
    const/4 v1, 0x0

    #@3d2
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_3b3

    #@3d3
    :cond_3d3
    move v2, v8

    #@3d4
    .line 454
    goto :goto_3ba

    #@3d5
    .line 466
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_3d5
    const-string v0, "android.bluetooth.IBluetooth"

    #@3d7
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3da
    .line 468
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3dd
    move-result v0

    #@3de
    if-eqz v0, :cond_406

    #@3e0
    .line 469
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3e2
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3e5
    move-result-object v1

    #@3e6
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@3e8
    .line 475
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_3e8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3eb
    move-result v0

    #@3ec
    if-eqz v0, :cond_408

    #@3ee
    move v2, v7

    #@3ef
    .line 477
    .restart local v2       #_arg1:Z
    :goto_3ef
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3f2
    move-result v3

    #@3f3
    .line 479
    .restart local v3       #_arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@3f6
    move-result-object v4

    #@3f7
    .line 480
    .restart local v4       #_arg3:[B
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/bluetooth/IBluetooth$Stub;->setPasskey(Landroid/bluetooth/BluetoothDevice;ZI[B)Z

    #@3fa
    move-result v6

    #@3fb
    .line 481
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3fe
    .line 482
    if-eqz v6, :cond_401

    #@400
    move v8, v7

    #@401
    :cond_401
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@404
    goto/16 :goto_9

    #@406
    .line 472
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:I
    .end local v4           #_arg3:[B
    .end local v6           #_result:Z
    :cond_406
    const/4 v1, 0x0

    #@407
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_3e8

    #@408
    :cond_408
    move v2, v8

    #@409
    .line 475
    goto :goto_3ef

    #@40a
    .line 487
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_40a
    const-string v0, "android.bluetooth.IBluetooth"

    #@40c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40f
    .line 489
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@412
    move-result v0

    #@413
    if-eqz v0, :cond_433

    #@415
    .line 490
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@417
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@41a
    move-result-object v1

    #@41b
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@41d
    .line 496
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_41d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@420
    move-result v0

    #@421
    if-eqz v0, :cond_435

    #@423
    move v2, v7

    #@424
    .line 497
    .restart local v2       #_arg1:Z
    :goto_424
    invoke-virtual {p0, v1, v2}, Landroid/bluetooth/IBluetooth$Stub;->setPairingConfirmation(Landroid/bluetooth/BluetoothDevice;Z)Z

    #@427
    move-result v6

    #@428
    .line 498
    .restart local v6       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@42b
    .line 499
    if-eqz v6, :cond_42e

    #@42d
    move v8, v7

    #@42e
    :cond_42e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@431
    goto/16 :goto_9

    #@433
    .line 493
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:Z
    .end local v6           #_result:Z
    :cond_433
    const/4 v1, 0x0

    #@434
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_41d

    #@435
    :cond_435
    move v2, v8

    #@436
    .line 496
    goto :goto_424

    #@437
    .line 504
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_437
    const-string v0, "android.bluetooth.IBluetooth"

    #@439
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43c
    .line 506
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@43f
    move-result v0

    #@440
    if-eqz v0, :cond_45e

    #@442
    .line 507
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@444
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@447
    move-result-object v1

    #@448
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@44a
    .line 513
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_44a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@44d
    move-result v2

    #@44e
    .line 515
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@451
    move-result v3

    #@452
    .line 517
    .restart local v3       #_arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@455
    move-result v4

    #@456
    .line 518
    .local v4, _arg3:I
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/bluetooth/IBluetooth$Stub;->sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V

    #@459
    .line 519
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@45c
    goto/16 :goto_9

    #@45e
    .line 510
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    :cond_45e
    const/4 v1, 0x0

    #@45f
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_44a

    #@460
    .line 524
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_460
    const-string v0, "android.bluetooth.IBluetooth"

    #@462
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@465
    .line 526
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@468
    move-result-object v0

    #@469
    invoke-static {v0}, Landroid/bluetooth/IBluetoothCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothCallback;

    #@46c
    move-result-object v1

    #@46d
    .line 527
    .local v1, _arg0:Landroid/bluetooth/IBluetoothCallback;
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->registerCallback(Landroid/bluetooth/IBluetoothCallback;)V

    #@470
    .line 528
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@473
    goto/16 :goto_9

    #@475
    .line 533
    .end local v1           #_arg0:Landroid/bluetooth/IBluetoothCallback;
    :sswitch_475
    const-string v0, "android.bluetooth.IBluetooth"

    #@477
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@47a
    .line 535
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@47d
    move-result-object v0

    #@47e
    invoke-static {v0}, Landroid/bluetooth/IBluetoothCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothCallback;

    #@481
    move-result-object v1

    #@482
    .line 536
    .restart local v1       #_arg0:Landroid/bluetooth/IBluetoothCallback;
    invoke-virtual {p0, v1}, Landroid/bluetooth/IBluetooth$Stub;->unregisterCallback(Landroid/bluetooth/IBluetoothCallback;)V

    #@485
    .line 537
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@488
    goto/16 :goto_9

    #@48a
    .line 542
    .end local v1           #_arg0:Landroid/bluetooth/IBluetoothCallback;
    :sswitch_48a
    const-string v0, "android.bluetooth.IBluetooth"

    #@48c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@48f
    .line 544
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@492
    move-result v0

    #@493
    if-eqz v0, :cond_4c9

    #@495
    .line 545
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@497
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@49a
    move-result-object v1

    #@49b
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@49d
    .line 551
    .local v1, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_49d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4a0
    move-result v2

    #@4a1
    .line 553
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4a4
    move-result v0

    #@4a5
    if-eqz v0, :cond_4cb

    #@4a7
    .line 554
    sget-object v0, Landroid/os/ParcelUuid;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4a9
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4ac
    move-result-object v3

    #@4ad
    check-cast v3, Landroid/os/ParcelUuid;

    #@4af
    .line 560
    .local v3, _arg2:Landroid/os/ParcelUuid;
    :goto_4af
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4b2
    move-result v4

    #@4b3
    .line 562
    .restart local v4       #_arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4b6
    move-result v5

    #@4b7
    .local v5, _arg4:I
    move-object v0, p0

    #@4b8
    .line 563
    invoke-virtual/range {v0 .. v5}, Landroid/bluetooth/IBluetooth$Stub;->connectSocket(Landroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;II)Landroid/os/ParcelFileDescriptor;

    #@4bb
    move-result-object v6

    #@4bc
    .line 564
    .local v6, _result:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4bf
    .line 565
    if-eqz v6, :cond_4cd

    #@4c1
    .line 566
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@4c4
    .line 567
    invoke-virtual {v6, p3, v7}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@4c7
    goto/16 :goto_9

    #@4c9
    .line 548
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Landroid/os/ParcelUuid;
    .end local v4           #_arg3:I
    .end local v5           #_arg4:I
    .end local v6           #_result:Landroid/os/ParcelFileDescriptor;
    :cond_4c9
    const/4 v1, 0x0

    #@4ca
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_49d

    #@4cb
    .line 557
    .restart local v2       #_arg1:I
    :cond_4cb
    const/4 v3, 0x0

    #@4cc
    .restart local v3       #_arg2:Landroid/os/ParcelUuid;
    goto :goto_4af

    #@4cd
    .line 570
    .restart local v4       #_arg3:I
    .restart local v5       #_arg4:I
    .restart local v6       #_result:Landroid/os/ParcelFileDescriptor;
    :cond_4cd
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@4d0
    goto/16 :goto_9

    #@4d2
    .line 576
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Landroid/os/ParcelUuid;
    .end local v4           #_arg3:I
    .end local v5           #_arg4:I
    .end local v6           #_result:Landroid/os/ParcelFileDescriptor;
    :sswitch_4d2
    const-string v0, "android.bluetooth.IBluetooth"

    #@4d4
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4d7
    .line 578
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4da
    move-result v1

    #@4db
    .line 580
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4de
    move-result-object v2

    #@4df
    .line 582
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4e2
    move-result v0

    #@4e3
    if-eqz v0, :cond_507

    #@4e5
    .line 583
    sget-object v0, Landroid/os/ParcelUuid;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4e7
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4ea
    move-result-object v3

    #@4eb
    check-cast v3, Landroid/os/ParcelUuid;

    #@4ed
    .line 589
    .restart local v3       #_arg2:Landroid/os/ParcelUuid;
    :goto_4ed
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4f0
    move-result v4

    #@4f1
    .line 591
    .restart local v4       #_arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4f4
    move-result v5

    #@4f5
    .restart local v5       #_arg4:I
    move-object v0, p0

    #@4f6
    .line 592
    invoke-virtual/range {v0 .. v5}, Landroid/bluetooth/IBluetooth$Stub;->createSocketChannel(ILjava/lang/String;Landroid/os/ParcelUuid;II)Landroid/os/ParcelFileDescriptor;

    #@4f9
    move-result-object v6

    #@4fa
    .line 593
    .restart local v6       #_result:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4fd
    .line 594
    if-eqz v6, :cond_509

    #@4ff
    .line 595
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@502
    .line 596
    invoke-virtual {v6, p3, v7}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@505
    goto/16 :goto_9

    #@507
    .line 586
    .end local v3           #_arg2:Landroid/os/ParcelUuid;
    .end local v4           #_arg3:I
    .end local v5           #_arg4:I
    .end local v6           #_result:Landroid/os/ParcelFileDescriptor;
    :cond_507
    const/4 v3, 0x0

    #@508
    .restart local v3       #_arg2:Landroid/os/ParcelUuid;
    goto :goto_4ed

    #@509
    .line 599
    .restart local v4       #_arg3:I
    .restart local v5       #_arg4:I
    .restart local v6       #_result:Landroid/os/ParcelFileDescriptor;
    :cond_509
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@50c
    goto/16 :goto_9

    #@50e
    .line 43
    :sswitch_data_50e
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_25
        0x3 -> :sswitch_35
        0x4 -> :sswitch_48
        0x5 -> :sswitch_5b
        0x6 -> :sswitch_6e
        0x7 -> :sswitch_81
        0x8 -> :sswitch_95
        0x9 -> :sswitch_a9
        0xa -> :sswitch_ba
        0xb -> :sswitch_cb
        0xc -> :sswitch_e3
        0xd -> :sswitch_f4
        0xe -> :sswitch_105
        0xf -> :sswitch_121
        0x10 -> :sswitch_132
        0x11 -> :sswitch_14a
        0x12 -> :sswitch_15e
        0x13 -> :sswitch_172
        0x14 -> :sswitch_186
        0x15 -> :sswitch_197
        0x16 -> :sswitch_1ac
        0x17 -> :sswitch_1bd
        0x18 -> :sswitch_1e1
        0x19 -> :sswitch_205
        0x1a -> :sswitch_229
        0x1b -> :sswitch_24a
        0x1c -> :sswitch_26e
        0x1d -> :sswitch_29b
        0x1e -> :sswitch_2bf
        0x1f -> :sswitch_2d0
        0x20 -> :sswitch_2f1
        0x21 -> :sswitch_312
        0x22 -> :sswitch_33a
        0x23 -> :sswitch_35b
        0x24 -> :sswitch_37c
        0x25 -> :sswitch_3a0
        0x26 -> :sswitch_3d5
        0x27 -> :sswitch_40a
        0x28 -> :sswitch_437
        0x29 -> :sswitch_460
        0x2a -> :sswitch_475
        0x2b -> :sswitch_48a
        0x2c -> :sswitch_4d2
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
