.class public abstract Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;
.super Landroid/os/Binder;
.source "IBluetoothStateChangeCallback.java"

# interfaces
.implements Landroid/bluetooth/IBluetoothStateChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/IBluetoothStateChangeCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/IBluetoothStateChangeCallback$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.bluetooth.IBluetoothStateChangeCallback"

.field static final TRANSACTION_onBluetoothStateChange:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.bluetooth.IBluetoothStateChangeCallback"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothStateChangeCallback;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.bluetooth.IBluetoothStateChangeCallback"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/bluetooth/IBluetoothStateChangeCallback;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/bluetooth/IBluetoothStateChangeCallback$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/bluetooth/IBluetoothStateChangeCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_24

    #@4
    .line 60
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v1

    #@8
    :goto_8
    return v1

    #@9
    .line 47
    :sswitch_9
    const-string v2, "android.bluetooth.IBluetoothStateChangeCallback"

    #@b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v2, "android.bluetooth.IBluetoothStateChangeCallback"

    #@11
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_22

    #@1a
    move v0, v1

    #@1b
    .line 55
    .local v0, _arg0:Z
    :goto_1b
    invoke-virtual {p0, v0}, Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;->onBluetoothStateChange(Z)V

    #@1e
    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@21
    goto :goto_8

    #@22
    .line 54
    .end local v0           #_arg0:Z
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_1b

    #@24
    .line 43
    :sswitch_data_24
    .sparse-switch
        0x1 -> :sswitch_f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
