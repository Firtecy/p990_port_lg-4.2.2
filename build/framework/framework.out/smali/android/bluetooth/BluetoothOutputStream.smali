.class final Landroid/bluetooth/BluetoothOutputStream;
.super Ljava/io/OutputStream;
.source "BluetoothOutputStream.java"


# instance fields
.field private mSocket:Landroid/bluetooth/BluetoothSocket;


# direct methods
.method constructor <init>(Landroid/bluetooth/BluetoothSocket;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    #@3
    .line 33
    iput-object p1, p0, Landroid/bluetooth/BluetoothOutputStream;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@5
    .line 34
    return-void
.end method


# virtual methods
.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 40
    iget-object v0, p0, Landroid/bluetooth/BluetoothOutputStream;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V

    #@5
    .line 41
    return-void
.end method

.method public write(I)V
    .registers 6
    .parameter "oneByte"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 54
    new-array v0, v3, [B

    #@4
    .line 55
    .local v0, b:[B
    int-to-byte v1, p1

    #@5
    aput-byte v1, v0, v2

    #@7
    .line 56
    iget-object v1, p0, Landroid/bluetooth/BluetoothOutputStream;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@9
    invoke-virtual {v1, v0, v2, v3}, Landroid/bluetooth/BluetoothSocket;->write([BII)I

    #@c
    .line 57
    return-void
.end method

.method public write([BII)V
    .registers 6
    .parameter "b"
    .parameter "offset"
    .parameter "count"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 79
    if-nez p1, :cond_a

    #@2
    .line 80
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "buffer is null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 82
    :cond_a
    or-int v0, p2, p3

    #@c
    if-ltz v0, :cond_12

    #@e
    array-length v0, p1

    #@f
    sub-int/2addr v0, p2

    #@10
    if-le p3, v0, :cond_1a

    #@12
    .line 83
    :cond_12
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@14
    const-string v1, "invalid offset or length"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 85
    :cond_1a
    iget-object v0, p0, Landroid/bluetooth/BluetoothOutputStream;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@1c
    invoke-virtual {v0, p1, p2, p3}, Landroid/bluetooth/BluetoothSocket;->write([BII)I

    #@1f
    .line 86
    return-void
.end method
