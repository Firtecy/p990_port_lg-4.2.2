.class public Landroid/util/TimingLogger;
.super Ljava/lang/Object;
.source "TimingLogger.java"


# instance fields
.field private mDisabled:Z

.field private mLabel:Ljava/lang/String;

.field mSplitLabels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mSplits:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "tag"
    .parameter "label"

    #@0
    .prologue
    .line 76
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    invoke-virtual {p0, p1, p2}, Landroid/util/TimingLogger;->reset(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 78
    return-void
.end method


# virtual methods
.method public addSplit(Ljava/lang/String;)V
    .registers 6
    .parameter "splitLabel"

    #@0
    .prologue
    .line 123
    iget-boolean v2, p0, Landroid/util/TimingLogger;->mDisabled:Z

    #@2
    if-eqz v2, :cond_5

    #@4
    .line 127
    :goto_4
    return-void

    #@5
    .line 124
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@8
    move-result-wide v0

    #@9
    .line 125
    .local v0, now:J
    iget-object v2, p0, Landroid/util/TimingLogger;->mSplits:Ljava/util/ArrayList;

    #@b
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@12
    .line 126
    iget-object v2, p0, Landroid/util/TimingLogger;->mSplitLabels:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@17
    goto :goto_4
.end method

.method public dumpToLog()V
    .registers 13

    #@0
    .prologue
    .line 135
    iget-boolean v8, p0, Landroid/util/TimingLogger;->mDisabled:Z

    #@2
    if-eqz v8, :cond_5

    #@4
    .line 147
    :goto_4
    return-void

    #@5
    .line 136
    :cond_5
    iget-object v8, p0, Landroid/util/TimingLogger;->mTag:Ljava/lang/String;

    #@7
    new-instance v9, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    iget-object v10, p0, Landroid/util/TimingLogger;->mLabel:Ljava/lang/String;

    #@e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v9

    #@12
    const-string v10, ": begin"

    #@14
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v9

    #@18
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v9

    #@1c
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 137
    iget-object v8, p0, Landroid/util/TimingLogger;->mSplits:Ljava/util/ArrayList;

    #@21
    const/4 v9, 0x0

    #@22
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v8

    #@26
    check-cast v8, Ljava/lang/Long;

    #@28
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    #@2b
    move-result-wide v0

    #@2c
    .line 138
    .local v0, first:J
    move-wide v3, v0

    #@2d
    .line 139
    .local v3, now:J
    const/4 v2, 0x1

    #@2e
    .local v2, i:I
    :goto_2e
    iget-object v8, p0, Landroid/util/TimingLogger;->mSplits:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@33
    move-result v8

    #@34
    if-ge v2, v8, :cond_85

    #@36
    .line 140
    iget-object v8, p0, Landroid/util/TimingLogger;->mSplits:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3b
    move-result-object v8

    #@3c
    check-cast v8, Ljava/lang/Long;

    #@3e
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    #@41
    move-result-wide v3

    #@42
    .line 141
    iget-object v8, p0, Landroid/util/TimingLogger;->mSplitLabels:Ljava/util/ArrayList;

    #@44
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@47
    move-result-object v7

    #@48
    check-cast v7, Ljava/lang/String;

    #@4a
    .line 142
    .local v7, splitLabel:Ljava/lang/String;
    iget-object v8, p0, Landroid/util/TimingLogger;->mSplits:Ljava/util/ArrayList;

    #@4c
    add-int/lit8 v9, v2, -0x1

    #@4e
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@51
    move-result-object v8

    #@52
    check-cast v8, Ljava/lang/Long;

    #@54
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    #@57
    move-result-wide v5

    #@58
    .line 144
    .local v5, prev:J
    iget-object v8, p0, Landroid/util/TimingLogger;->mTag:Ljava/lang/String;

    #@5a
    new-instance v9, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    iget-object v10, p0, Landroid/util/TimingLogger;->mLabel:Ljava/lang/String;

    #@61
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v9

    #@65
    const-string v10, ":      "

    #@67
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v9

    #@6b
    sub-long v10, v3, v5

    #@6d
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@70
    move-result-object v9

    #@71
    const-string v10, " ms, "

    #@73
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v9

    #@77
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v9

    #@7b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v9

    #@7f
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 139
    add-int/lit8 v2, v2, 0x1

    #@84
    goto :goto_2e

    #@85
    .line 146
    .end local v5           #prev:J
    .end local v7           #splitLabel:Ljava/lang/String;
    :cond_85
    iget-object v8, p0, Landroid/util/TimingLogger;->mTag:Ljava/lang/String;

    #@87
    new-instance v9, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    iget-object v10, p0, Landroid/util/TimingLogger;->mLabel:Ljava/lang/String;

    #@8e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v9

    #@92
    const-string v10, ": end, "

    #@94
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v9

    #@98
    sub-long v10, v3, v0

    #@9a
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v9

    #@9e
    const-string v10, " ms"

    #@a0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v9

    #@a4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v9

    #@a8
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    goto/16 :goto_4
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Landroid/util/TimingLogger;->mTag:Ljava/lang/String;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_11

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    iput-boolean v0, p0, Landroid/util/TimingLogger;->mDisabled:Z

    #@c
    .line 104
    iget-boolean v0, p0, Landroid/util/TimingLogger;->mDisabled:Z

    #@e
    if-eqz v0, :cond_13

    #@10
    .line 113
    :goto_10
    return-void

    #@11
    .line 103
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_a

    #@13
    .line 105
    :cond_13
    iget-object v0, p0, Landroid/util/TimingLogger;->mSplits:Ljava/util/ArrayList;

    #@15
    if-nez v0, :cond_2a

    #@17
    .line 106
    new-instance v0, Ljava/util/ArrayList;

    #@19
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1c
    iput-object v0, p0, Landroid/util/TimingLogger;->mSplits:Ljava/util/ArrayList;

    #@1e
    .line 107
    new-instance v0, Ljava/util/ArrayList;

    #@20
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@23
    iput-object v0, p0, Landroid/util/TimingLogger;->mSplitLabels:Ljava/util/ArrayList;

    #@25
    .line 112
    :goto_25
    const/4 v0, 0x0

    #@26
    invoke-virtual {p0, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    #@29
    goto :goto_10

    #@2a
    .line 109
    :cond_2a
    iget-object v0, p0, Landroid/util/TimingLogger;->mSplits:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@2f
    .line 110
    iget-object v0, p0, Landroid/util/TimingLogger;->mSplitLabels:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@34
    goto :goto_25
.end method

.method public reset(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "tag"
    .parameter "label"

    #@0
    .prologue
    .line 89
    iput-object p1, p0, Landroid/util/TimingLogger;->mTag:Ljava/lang/String;

    #@2
    .line 90
    iput-object p2, p0, Landroid/util/TimingLogger;->mLabel:Ljava/lang/String;

    #@4
    .line 91
    invoke-virtual {p0}, Landroid/util/TimingLogger;->reset()V

    #@7
    .line 92
    return-void
.end method
