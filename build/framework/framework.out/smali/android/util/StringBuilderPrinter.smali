.class public Landroid/util/StringBuilderPrinter;
.super Ljava/lang/Object;
.source "StringBuilderPrinter.java"

# interfaces
.implements Landroid/util/Printer;


# instance fields
.field private final mBuilder:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Ljava/lang/StringBuilder;)V
    .registers 2
    .parameter "builder"

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 32
    iput-object p1, p0, Landroid/util/StringBuilderPrinter;->mBuilder:Ljava/lang/StringBuilder;

    #@5
    .line 33
    return-void
.end method


# virtual methods
.method public println(Ljava/lang/String;)V
    .registers 5
    .parameter "x"

    #@0
    .prologue
    const/16 v2, 0xa

    #@2
    .line 36
    iget-object v1, p0, Landroid/util/StringBuilderPrinter;->mBuilder:Ljava/lang/StringBuilder;

    #@4
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7
    .line 37
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@a
    move-result v0

    #@b
    .line 38
    .local v0, len:I
    if-lez v0, :cond_15

    #@d
    add-int/lit8 v1, v0, -0x1

    #@f
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    #@12
    move-result v1

    #@13
    if-eq v1, v2, :cond_1a

    #@15
    .line 39
    :cond_15
    iget-object v1, p0, Landroid/util/StringBuilderPrinter;->mBuilder:Ljava/lang/StringBuilder;

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1a
    .line 41
    :cond_1a
    return-void
.end method
