.class public Landroid/util/MonthDisplayHelper;
.super Ljava/lang/Object;
.source "MonthDisplayHelper.java"


# instance fields
.field private mCalendar:Ljava/util/Calendar;

.field private mNumDaysInMonth:I

.field private mNumDaysInPrevMonth:I

.field private mOffset:I

.field private final mWeekStartDay:I


# direct methods
.method public constructor <init>(II)V
    .registers 4
    .parameter "year"
    .parameter "month"

    #@0
    .prologue
    .line 67
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/util/MonthDisplayHelper;-><init>(III)V

    #@4
    .line 68
    return-void
.end method

.method public constructor <init>(III)V
    .registers 8
    .parameter "year"
    .parameter "month"
    .parameter "weekStartDay"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 48
    if-lt p3, v3, :cond_a

    #@7
    const/4 v0, 0x7

    #@8
    if-le p3, v0, :cond_10

    #@a
    .line 49
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@f
    throw v0

    #@10
    .line 51
    :cond_10
    iput p3, p0, Landroid/util/MonthDisplayHelper;->mWeekStartDay:I

    #@12
    .line 53
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@18
    .line 54
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@1a
    invoke-virtual {v0, v3, p1}, Ljava/util/Calendar;->set(II)V

    #@1d
    .line 55
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@1f
    const/4 v1, 0x2

    #@20
    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    #@23
    .line 56
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@25
    const/4 v1, 0x5

    #@26
    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    #@29
    .line 57
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@2b
    const/16 v1, 0xb

    #@2d
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    #@30
    .line 58
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@32
    const/16 v1, 0xc

    #@34
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    #@37
    .line 59
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@39
    const/16 v1, 0xd

    #@3b
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    #@3e
    .line 60
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@40
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@43
    .line 62
    invoke-direct {p0}, Landroid/util/MonthDisplayHelper;->recalculate()V

    #@46
    .line 63
    return-void
.end method

.method private recalculate()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x5

    #@1
    const/4 v4, 0x2

    #@2
    .line 200
    iget-object v2, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@4
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->getActualMaximum(I)I

    #@7
    move-result v2

    #@8
    iput v2, p0, Landroid/util/MonthDisplayHelper;->mNumDaysInMonth:I

    #@a
    .line 202
    iget-object v2, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@c
    const/4 v3, -0x1

    #@d
    invoke-virtual {v2, v4, v3}, Ljava/util/Calendar;->add(II)V

    #@10
    .line 203
    iget-object v2, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@12
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->getActualMaximum(I)I

    #@15
    move-result v2

    #@16
    iput v2, p0, Landroid/util/MonthDisplayHelper;->mNumDaysInPrevMonth:I

    #@18
    .line 204
    iget-object v2, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@1a
    const/4 v3, 0x1

    #@1b
    invoke-virtual {v2, v4, v3}, Ljava/util/Calendar;->add(II)V

    #@1e
    .line 206
    invoke-virtual {p0}, Landroid/util/MonthDisplayHelper;->getFirstDayOfMonth()I

    #@21
    move-result v0

    #@22
    .line 207
    .local v0, firstDayOfMonth:I
    iget v2, p0, Landroid/util/MonthDisplayHelper;->mWeekStartDay:I

    #@24
    sub-int v1, v0, v2

    #@26
    .line 208
    .local v1, offset:I
    if-gez v1, :cond_2a

    #@28
    .line 209
    add-int/lit8 v1, v1, 0x7

    #@2a
    .line 211
    :cond_2a
    iput v1, p0, Landroid/util/MonthDisplayHelper;->mOffset:I

    #@2c
    .line 212
    return-void
.end method


# virtual methods
.method public getColumnOf(I)I
    .registers 3
    .parameter "day"

    #@0
    .prologue
    .line 157
    iget v0, p0, Landroid/util/MonthDisplayHelper;->mOffset:I

    #@2
    add-int/2addr v0, p1

    #@3
    add-int/lit8 v0, v0, -0x1

    #@5
    rem-int/lit8 v0, v0, 0x7

    #@7
    return v0
.end method

.method public getDayAt(II)I
    .registers 6
    .parameter "row"
    .parameter "column"

    #@0
    .prologue
    .line 136
    if-nez p1, :cond_f

    #@2
    iget v1, p0, Landroid/util/MonthDisplayHelper;->mOffset:I

    #@4
    if-ge p2, v1, :cond_f

    #@6
    .line 137
    iget v1, p0, Landroid/util/MonthDisplayHelper;->mNumDaysInPrevMonth:I

    #@8
    add-int/2addr v1, p2

    #@9
    iget v2, p0, Landroid/util/MonthDisplayHelper;->mOffset:I

    #@b
    sub-int/2addr v1, v2

    #@c
    add-int/lit8 v0, v1, 0x1

    #@e
    .line 142
    :cond_e
    :goto_e
    return v0

    #@f
    .line 140
    :cond_f
    mul-int/lit8 v1, p1, 0x7

    #@11
    add-int/2addr v1, p2

    #@12
    iget v2, p0, Landroid/util/MonthDisplayHelper;->mOffset:I

    #@14
    sub-int/2addr v1, v2

    #@15
    add-int/lit8 v0, v1, 0x1

    #@17
    .line 142
    .local v0, day:I
    iget v1, p0, Landroid/util/MonthDisplayHelper;->mNumDaysInMonth:I

    #@19
    if-le v0, v1, :cond_e

    #@1b
    iget v1, p0, Landroid/util/MonthDisplayHelper;->mNumDaysInMonth:I

    #@1d
    sub-int/2addr v0, v1

    #@1e
    goto :goto_e
.end method

.method public getDigitsForRow(I)[I
    .registers 7
    .parameter "row"

    #@0
    .prologue
    const/4 v3, 0x7

    #@1
    .line 116
    if-ltz p1, :cond_6

    #@3
    const/4 v2, 0x5

    #@4
    if-le p1, v2, :cond_26

    #@6
    .line 117
    :cond_6
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v4, "row "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    const-string v4, " out of range (0-5)"

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v2

    #@26
    .line 121
    :cond_26
    new-array v1, v3, [I

    #@28
    .line 122
    .local v1, result:[I
    const/4 v0, 0x0

    #@29
    .local v0, column:I
    :goto_29
    if-ge v0, v3, :cond_34

    #@2b
    .line 123
    invoke-virtual {p0, p1, v0}, Landroid/util/MonthDisplayHelper;->getDayAt(II)I

    #@2e
    move-result v2

    #@2f
    aput v2, v1, v0

    #@31
    .line 122
    add-int/lit8 v0, v0, 0x1

    #@33
    goto :goto_29

    #@34
    .line 126
    :cond_34
    return-object v1
.end method

.method public getFirstDayOfMonth()I
    .registers 3

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@2
    const/4 v1, 0x7

    #@3
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getMonth()I
    .registers 3

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getNumberOfDaysInMonth()I
    .registers 2

    #@0
    .prologue
    .line 96
    iget v0, p0, Landroid/util/MonthDisplayHelper;->mNumDaysInMonth:I

    #@2
    return v0
.end method

.method public getOffset()I
    .registers 2

    #@0
    .prologue
    .line 106
    iget v0, p0, Landroid/util/MonthDisplayHelper;->mOffset:I

    #@2
    return v0
.end method

.method public getRowOf(I)I
    .registers 3
    .parameter "day"

    #@0
    .prologue
    .line 150
    iget v0, p0, Landroid/util/MonthDisplayHelper;->mOffset:I

    #@2
    add-int/2addr v0, p1

    #@3
    add-int/lit8 v0, v0, -0x1

    #@5
    div-int/lit8 v0, v0, 0x7

    #@7
    return v0
.end method

.method public getWeekStartDay()I
    .registers 2

    #@0
    .prologue
    .line 81
    iget v0, p0, Landroid/util/MonthDisplayHelper;->mWeekStartDay:I

    #@2
    return v0
.end method

.method public getYear()I
    .registers 3

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public isWithinCurrentMonth(II)Z
    .registers 7
    .parameter "row"
    .parameter "column"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 181
    if-ltz p1, :cond_b

    #@3
    if-ltz p2, :cond_b

    #@5
    const/4 v2, 0x5

    #@6
    if-gt p1, v2, :cond_b

    #@8
    const/4 v2, 0x6

    #@9
    if-le p2, v2, :cond_c

    #@b
    .line 193
    :cond_b
    :goto_b
    return v1

    #@c
    .line 185
    :cond_c
    if-nez p1, :cond_12

    #@e
    iget v2, p0, Landroid/util/MonthDisplayHelper;->mOffset:I

    #@10
    if-lt p2, v2, :cond_b

    #@12
    .line 189
    :cond_12
    mul-int/lit8 v2, p1, 0x7

    #@14
    add-int/2addr v2, p2

    #@15
    iget v3, p0, Landroid/util/MonthDisplayHelper;->mOffset:I

    #@17
    sub-int/2addr v2, v3

    #@18
    add-int/lit8 v0, v2, 0x1

    #@1a
    .line 190
    .local v0, day:I
    iget v2, p0, Landroid/util/MonthDisplayHelper;->mNumDaysInMonth:I

    #@1c
    if-gt v0, v2, :cond_b

    #@1e
    .line 193
    const/4 v1, 0x1

    #@1f
    goto :goto_b
.end method

.method public nextMonth()V
    .registers 4

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@2
    const/4 v1, 0x2

    #@3
    const/4 v2, 0x1

    #@4
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    #@7
    .line 173
    invoke-direct {p0}, Landroid/util/MonthDisplayHelper;->recalculate()V

    #@a
    .line 174
    return-void
.end method

.method public previousMonth()V
    .registers 4

    #@0
    .prologue
    .line 164
    iget-object v0, p0, Landroid/util/MonthDisplayHelper;->mCalendar:Ljava/util/Calendar;

    #@2
    const/4 v1, 0x2

    #@3
    const/4 v2, -0x1

    #@4
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    #@7
    .line 165
    invoke-direct {p0}, Landroid/util/MonthDisplayHelper;->recalculate()V

    #@a
    .line 166
    return-void
.end method
