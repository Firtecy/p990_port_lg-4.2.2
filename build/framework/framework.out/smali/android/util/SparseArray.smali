.class public Landroid/util/SparseArray;
.super Ljava/lang/Object;
.source "SparseArray.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final DELETED:Ljava/lang/Object;


# instance fields
.field private mGarbage:Z

.field private mKeys:[I

.field private mSize:I

.field private mValues:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 27
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/util/SparseArray;->DELETED:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 38
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    const/16 v0, 0xa

    #@2
    invoke-direct {p0, v0}, Landroid/util/SparseArray;-><init>(I)V

    #@5
    .line 39
    return-void
.end method

.method public constructor <init>(I)V
    .registers 4
    .parameter "initialCapacity"

    #@0
    .prologue
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    const/4 v1, 0x0

    #@1
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 28
    iput-boolean v1, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@6
    .line 47
    invoke-static {p1}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@9
    move-result p1

    #@a
    .line 49
    new-array v0, p1, [I

    #@c
    iput-object v0, p0, Landroid/util/SparseArray;->mKeys:[I

    #@e
    .line 50
    new-array v0, p1, [Ljava/lang/Object;

    #@10
    iput-object v0, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@12
    .line 51
    iput v1, p0, Landroid/util/SparseArray;->mSize:I

    #@14
    .line 52
    return-void
.end method

.method private static binarySearch([IIII)I
    .registers 9
    .parameter "a"
    .parameter "start"
    .parameter "len"
    .parameter "key"

    #@0
    .prologue
    .line 337
    add-int v1, p1, p2

    #@2
    .local v1, high:I
    add-int/lit8 v2, p1, -0x1

    #@4
    .line 339
    .local v2, low:I
    :goto_4
    sub-int v3, v1, v2

    #@6
    const/4 v4, 0x1

    #@7
    if-le v3, v4, :cond_15

    #@9
    .line 340
    add-int v3, v1, v2

    #@b
    div-int/lit8 v0, v3, 0x2

    #@d
    .line 342
    .local v0, guess:I
    aget v3, p0, v0

    #@f
    if-ge v3, p3, :cond_13

    #@11
    .line 343
    move v2, v0

    #@12
    goto :goto_4

    #@13
    .line 345
    :cond_13
    move v1, v0

    #@14
    goto :goto_4

    #@15
    .line 348
    .end local v0           #guess:I
    :cond_15
    add-int v3, p1, p2

    #@17
    if-ne v1, v3, :cond_1e

    #@19
    .line 349
    add-int v3, p1, p2

    #@1b
    xor-int/lit8 v1, v3, -0x1

    #@1d
    .line 353
    .end local v1           #high:I
    :cond_1d
    :goto_1d
    return v1

    #@1e
    .line 350
    .restart local v1       #high:I
    :cond_1e
    aget v3, p0, v1

    #@20
    if-eq v3, p3, :cond_1d

    #@22
    .line 353
    xor-int/lit8 v1, v1, -0x1

    #@24
    goto :goto_1d
.end method

.method private gc()V
    .registers 8

    #@0
    .prologue
    .line 125
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    iget v2, p0, Landroid/util/SparseArray;->mSize:I

    #@2
    .line 126
    .local v2, n:I
    const/4 v3, 0x0

    #@3
    .line 127
    .local v3, o:I
    iget-object v1, p0, Landroid/util/SparseArray;->mKeys:[I

    #@5
    .line 128
    .local v1, keys:[I
    iget-object v5, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@7
    .line 130
    .local v5, values:[Ljava/lang/Object;
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v2, :cond_20

    #@a
    .line 131
    aget-object v4, v5, v0

    #@c
    .line 133
    .local v4, val:Ljava/lang/Object;
    sget-object v6, Landroid/util/SparseArray;->DELETED:Ljava/lang/Object;

    #@e
    if-eq v4, v6, :cond_1d

    #@10
    .line 134
    if-eq v0, v3, :cond_1b

    #@12
    .line 135
    aget v6, v1, v0

    #@14
    aput v6, v1, v3

    #@16
    .line 136
    aput-object v4, v5, v3

    #@18
    .line 137
    const/4 v6, 0x0

    #@19
    aput-object v6, v5, v0

    #@1b
    .line 140
    :cond_1b
    add-int/lit8 v3, v3, 0x1

    #@1d
    .line 130
    :cond_1d
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_8

    #@20
    .line 144
    .end local v4           #val:Ljava/lang/Object;
    :cond_20
    const/4 v6, 0x0

    #@21
    iput-boolean v6, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@23
    .line 145
    iput v3, p0, Landroid/util/SparseArray;->mSize:I

    #@25
    .line 148
    return-void
.end method


# virtual methods
.method public append(ILjava/lang/Object;)V
    .registers 10
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    .local p2, value:Ljava/lang/Object;,"TE;"
    const/4 v6, 0x0

    #@1
    .line 307
    iget v4, p0, Landroid/util/SparseArray;->mSize:I

    #@3
    if-eqz v4, :cond_13

    #@5
    iget-object v4, p0, Landroid/util/SparseArray;->mKeys:[I

    #@7
    iget v5, p0, Landroid/util/SparseArray;->mSize:I

    #@9
    add-int/lit8 v5, v5, -0x1

    #@b
    aget v4, v4, v5

    #@d
    if-gt p1, v4, :cond_13

    #@f
    .line 308
    invoke-virtual {p0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@12
    .line 334
    :goto_12
    return-void

    #@13
    .line 312
    :cond_13
    iget-boolean v4, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@15
    if-eqz v4, :cond_21

    #@17
    iget v4, p0, Landroid/util/SparseArray;->mSize:I

    #@19
    iget-object v5, p0, Landroid/util/SparseArray;->mKeys:[I

    #@1b
    array-length v5, v5

    #@1c
    if-lt v4, v5, :cond_21

    #@1e
    .line 313
    invoke-direct {p0}, Landroid/util/SparseArray;->gc()V

    #@21
    .line 316
    :cond_21
    iget v3, p0, Landroid/util/SparseArray;->mSize:I

    #@23
    .line 317
    .local v3, pos:I
    iget-object v4, p0, Landroid/util/SparseArray;->mKeys:[I

    #@25
    array-length v4, v4

    #@26
    if-lt v3, v4, :cond_46

    #@28
    .line 318
    add-int/lit8 v4, v3, 0x1

    #@2a
    invoke-static {v4}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@2d
    move-result v0

    #@2e
    .line 320
    .local v0, n:I
    new-array v1, v0, [I

    #@30
    .line 321
    .local v1, nkeys:[I
    new-array v2, v0, [Ljava/lang/Object;

    #@32
    .line 324
    .local v2, nvalues:[Ljava/lang/Object;
    iget-object v4, p0, Landroid/util/SparseArray;->mKeys:[I

    #@34
    iget-object v5, p0, Landroid/util/SparseArray;->mKeys:[I

    #@36
    array-length v5, v5

    #@37
    invoke-static {v4, v6, v1, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3a
    .line 325
    iget-object v4, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@3c
    iget-object v5, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@3e
    array-length v5, v5

    #@3f
    invoke-static {v4, v6, v2, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@42
    .line 327
    iput-object v1, p0, Landroid/util/SparseArray;->mKeys:[I

    #@44
    .line 328
    iput-object v2, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@46
    .line 331
    .end local v0           #n:I
    .end local v1           #nkeys:[I
    .end local v2           #nvalues:[Ljava/lang/Object;
    :cond_46
    iget-object v4, p0, Landroid/util/SparseArray;->mKeys:[I

    #@48
    aput p1, v4, v3

    #@4a
    .line 332
    iget-object v4, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@4c
    aput-object p2, v4, v3

    #@4e
    .line 333
    add-int/lit8 v4, v3, 0x1

    #@50
    iput v4, p0, Landroid/util/SparseArray;->mSize:I

    #@52
    goto :goto_12
.end method

.method public clear()V
    .registers 6

    #@0
    .prologue
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    const/4 v4, 0x0

    #@1
    .line 291
    iget v1, p0, Landroid/util/SparseArray;->mSize:I

    #@3
    .line 292
    .local v1, n:I
    iget-object v2, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@5
    .line 294
    .local v2, values:[Ljava/lang/Object;
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    if-ge v0, v1, :cond_e

    #@8
    .line 295
    const/4 v3, 0x0

    #@9
    aput-object v3, v2, v0

    #@b
    .line 294
    add-int/lit8 v0, v0, 0x1

    #@d
    goto :goto_6

    #@e
    .line 298
    :cond_e
    iput v4, p0, Landroid/util/SparseArray;->mSize:I

    #@10
    .line 299
    iput-boolean v4, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@12
    .line 300
    return-void
.end method

.method public clone()Landroid/util/SparseArray;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<TE;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 57
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    const/4 v1, 0x0

    #@1
    .line 59
    .local v1, clone:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    :try_start_1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    #@4
    move-result-object v2

    #@5
    move-object v0, v2

    #@6
    check-cast v0, Landroid/util/SparseArray;

    #@8
    move-object v1, v0

    #@9
    .line 60
    iget-object v2, p0, Landroid/util/SparseArray;->mKeys:[I

    #@b
    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, [I

    #@11
    iput-object v2, v1, Landroid/util/SparseArray;->mKeys:[I

    #@13
    .line 61
    iget-object v2, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@15
    invoke-virtual {v2}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, [Ljava/lang/Object;

    #@1b
    iput-object v2, v1, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;
    :try_end_1d
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1d} :catch_1e

    #@1d
    .line 65
    :goto_1d
    return-object v1

    #@1e
    .line 62
    :catch_1e
    move-exception v2

    #@1f
    goto :goto_1d
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 26
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    invoke-virtual {p0}, Landroid/util/SparseArray;->clone()Landroid/util/SparseArray;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public delete(I)V
    .registers 6
    .parameter "key"

    #@0
    .prologue
    .line 95
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    iget-object v1, p0, Landroid/util/SparseArray;->mKeys:[I

    #@2
    const/4 v2, 0x0

    #@3
    iget v3, p0, Landroid/util/SparseArray;->mSize:I

    #@5
    invoke-static {v1, v2, v3, p1}, Landroid/util/SparseArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    .line 97
    .local v0, i:I
    if-ltz v0, :cond_1c

    #@b
    .line 98
    iget-object v1, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@d
    aget-object v1, v1, v0

    #@f
    sget-object v2, Landroid/util/SparseArray;->DELETED:Ljava/lang/Object;

    #@11
    if-eq v1, v2, :cond_1c

    #@13
    .line 99
    iget-object v1, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@15
    sget-object v2, Landroid/util/SparseArray;->DELETED:Ljava/lang/Object;

    #@17
    aput-object v2, v1, v0

    #@19
    .line 100
    const/4 v1, 0x1

    #@1a
    iput-boolean v1, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@1c
    .line 103
    :cond_1c
    return-void
.end method

.method public get(I)Ljava/lang/Object;
    .registers 3
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    #@0
    .prologue
    .line 73
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public get(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 7
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    #@0
    .prologue
    .line 82
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    .local p2, valueIfKeyNotFound:Ljava/lang/Object;,"TE;"
    iget-object v1, p0, Landroid/util/SparseArray;->mKeys:[I

    #@2
    const/4 v2, 0x0

    #@3
    iget v3, p0, Landroid/util/SparseArray;->mSize:I

    #@5
    invoke-static {v1, v2, v3, p1}, Landroid/util/SparseArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    .line 84
    .local v0, i:I
    if-ltz v0, :cond_13

    #@b
    iget-object v1, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@d
    aget-object v1, v1, v0

    #@f
    sget-object v2, Landroid/util/SparseArray;->DELETED:Ljava/lang/Object;

    #@11
    if-ne v1, v2, :cond_14

    #@13
    .line 87
    .end local p2           #valueIfKeyNotFound:Ljava/lang/Object;,"TE;"
    :cond_13
    :goto_13
    return-object p2

    #@14
    .restart local p2       #valueIfKeyNotFound:Ljava/lang/Object;,"TE;"
    :cond_14
    iget-object v1, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@16
    aget-object p2, v1, v0

    #@18
    goto :goto_13
.end method

.method public indexOfKey(I)I
    .registers 5
    .parameter "key"

    #@0
    .prologue
    .line 260
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    iget-boolean v0, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 261
    invoke-direct {p0}, Landroid/util/SparseArray;->gc()V

    #@7
    .line 264
    :cond_7
    iget-object v0, p0, Landroid/util/SparseArray;->mKeys:[I

    #@9
    const/4 v1, 0x0

    #@a
    iget v2, p0, Landroid/util/SparseArray;->mSize:I

    #@c
    invoke-static {v0, v1, v2, p1}, Landroid/util/SparseArray;->binarySearch([IIII)I

    #@f
    move-result v0

    #@10
    return v0
.end method

.method public indexOfValue(Ljava/lang/Object;)I
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)I"
        }
    .end annotation

    #@0
    .prologue
    .line 276
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    .local p1, value:Ljava/lang/Object;,"TE;"
    iget-boolean v1, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 277
    invoke-direct {p0}, Landroid/util/SparseArray;->gc()V

    #@7
    .line 280
    :cond_7
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    iget v1, p0, Landroid/util/SparseArray;->mSize:I

    #@a
    if-ge v0, v1, :cond_16

    #@c
    .line 281
    iget-object v1, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@e
    aget-object v1, v1, v0

    #@10
    if-ne v1, p1, :cond_13

    #@12
    .line 284
    .end local v0           #i:I
    :goto_12
    return v0

    #@13
    .line 280
    .restart local v0       #i:I
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_8

    #@16
    .line 284
    :cond_16
    const/4 v0, -0x1

    #@17
    goto :goto_12
.end method

.method public keyAt(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 220
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    iget-boolean v0, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 221
    invoke-direct {p0}, Landroid/util/SparseArray;->gc()V

    #@7
    .line 224
    :cond_7
    iget-object v0, p0, Landroid/util/SparseArray;->mKeys:[I

    #@9
    aget v0, v0, p1

    #@b
    return v0
.end method

.method public put(ILjava/lang/Object;)V
    .registers 11
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    .local p2, value:Ljava/lang/Object;,"TE;"
    const/4 v6, 0x0

    #@1
    .line 156
    iget-object v4, p0, Landroid/util/SparseArray;->mKeys:[I

    #@3
    iget v5, p0, Landroid/util/SparseArray;->mSize:I

    #@5
    invoke-static {v4, v6, v5, p1}, Landroid/util/SparseArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    .line 158
    .local v0, i:I
    if-ltz v0, :cond_10

    #@b
    .line 159
    iget-object v4, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@d
    aput-object p2, v4, v0

    #@f
    .line 200
    :goto_f
    return-void

    #@10
    .line 161
    :cond_10
    xor-int/lit8 v0, v0, -0x1

    #@12
    .line 163
    iget v4, p0, Landroid/util/SparseArray;->mSize:I

    #@14
    if-ge v0, v4, :cond_27

    #@16
    iget-object v4, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@18
    aget-object v4, v4, v0

    #@1a
    sget-object v5, Landroid/util/SparseArray;->DELETED:Ljava/lang/Object;

    #@1c
    if-ne v4, v5, :cond_27

    #@1e
    .line 164
    iget-object v4, p0, Landroid/util/SparseArray;->mKeys:[I

    #@20
    aput p1, v4, v0

    #@22
    .line 165
    iget-object v4, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@24
    aput-object p2, v4, v0

    #@26
    goto :goto_f

    #@27
    .line 169
    :cond_27
    iget-boolean v4, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@29
    if-eqz v4, :cond_3f

    #@2b
    iget v4, p0, Landroid/util/SparseArray;->mSize:I

    #@2d
    iget-object v5, p0, Landroid/util/SparseArray;->mKeys:[I

    #@2f
    array-length v5, v5

    #@30
    if-lt v4, v5, :cond_3f

    #@32
    .line 170
    invoke-direct {p0}, Landroid/util/SparseArray;->gc()V

    #@35
    .line 173
    iget-object v4, p0, Landroid/util/SparseArray;->mKeys:[I

    #@37
    iget v5, p0, Landroid/util/SparseArray;->mSize:I

    #@39
    invoke-static {v4, v6, v5, p1}, Landroid/util/SparseArray;->binarySearch([IIII)I

    #@3c
    move-result v4

    #@3d
    xor-int/lit8 v0, v4, -0x1

    #@3f
    .line 176
    :cond_3f
    iget v4, p0, Landroid/util/SparseArray;->mSize:I

    #@41
    iget-object v5, p0, Landroid/util/SparseArray;->mKeys:[I

    #@43
    array-length v5, v5

    #@44
    if-lt v4, v5, :cond_66

    #@46
    .line 177
    iget v4, p0, Landroid/util/SparseArray;->mSize:I

    #@48
    add-int/lit8 v4, v4, 0x1

    #@4a
    invoke-static {v4}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@4d
    move-result v1

    #@4e
    .line 179
    .local v1, n:I
    new-array v2, v1, [I

    #@50
    .line 180
    .local v2, nkeys:[I
    new-array v3, v1, [Ljava/lang/Object;

    #@52
    .line 183
    .local v3, nvalues:[Ljava/lang/Object;
    iget-object v4, p0, Landroid/util/SparseArray;->mKeys:[I

    #@54
    iget-object v5, p0, Landroid/util/SparseArray;->mKeys:[I

    #@56
    array-length v5, v5

    #@57
    invoke-static {v4, v6, v2, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@5a
    .line 184
    iget-object v4, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@5c
    iget-object v5, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@5e
    array-length v5, v5

    #@5f
    invoke-static {v4, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@62
    .line 186
    iput-object v2, p0, Landroid/util/SparseArray;->mKeys:[I

    #@64
    .line 187
    iput-object v3, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@66
    .line 190
    .end local v1           #n:I
    .end local v2           #nkeys:[I
    .end local v3           #nvalues:[Ljava/lang/Object;
    :cond_66
    iget v4, p0, Landroid/util/SparseArray;->mSize:I

    #@68
    sub-int/2addr v4, v0

    #@69
    if-eqz v4, :cond_83

    #@6b
    .line 192
    iget-object v4, p0, Landroid/util/SparseArray;->mKeys:[I

    #@6d
    iget-object v5, p0, Landroid/util/SparseArray;->mKeys:[I

    #@6f
    add-int/lit8 v6, v0, 0x1

    #@71
    iget v7, p0, Landroid/util/SparseArray;->mSize:I

    #@73
    sub-int/2addr v7, v0

    #@74
    invoke-static {v4, v0, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@77
    .line 193
    iget-object v4, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@79
    iget-object v5, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@7b
    add-int/lit8 v6, v0, 0x1

    #@7d
    iget v7, p0, Landroid/util/SparseArray;->mSize:I

    #@7f
    sub-int/2addr v7, v0

    #@80
    invoke-static {v4, v0, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@83
    .line 196
    :cond_83
    iget-object v4, p0, Landroid/util/SparseArray;->mKeys:[I

    #@85
    aput p1, v4, v0

    #@87
    .line 197
    iget-object v4, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@89
    aput-object p2, v4, v0

    #@8b
    .line 198
    iget v4, p0, Landroid/util/SparseArray;->mSize:I

    #@8d
    add-int/lit8 v4, v4, 0x1

    #@8f
    iput v4, p0, Landroid/util/SparseArray;->mSize:I

    #@91
    goto/16 :goto_f
.end method

.method public remove(I)V
    .registers 2
    .parameter "key"

    #@0
    .prologue
    .line 109
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    invoke-virtual {p0, p1}, Landroid/util/SparseArray;->delete(I)V

    #@3
    .line 110
    return-void
.end method

.method public removeAt(I)V
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 116
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    iget-object v0, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@2
    aget-object v0, v0, p1

    #@4
    sget-object v1, Landroid/util/SparseArray;->DELETED:Ljava/lang/Object;

    #@6
    if-eq v0, v1, :cond_11

    #@8
    .line 117
    iget-object v0, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@a
    sget-object v1, Landroid/util/SparseArray;->DELETED:Ljava/lang/Object;

    #@c
    aput-object v1, v0, p1

    #@e
    .line 118
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@11
    .line 120
    :cond_11
    return-void
.end method

.method public setValueAt(ILjava/lang/Object;)V
    .registers 4
    .parameter "index"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 247
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    .local p2, value:Ljava/lang/Object;,"TE;"
    iget-boolean v0, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 248
    invoke-direct {p0}, Landroid/util/SparseArray;->gc()V

    #@7
    .line 251
    :cond_7
    iget-object v0, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@9
    aput-object p2, v0, p1

    #@b
    .line 252
    return-void
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 207
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    iget-boolean v0, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 208
    invoke-direct {p0}, Landroid/util/SparseArray;->gc()V

    #@7
    .line 211
    :cond_7
    iget v0, p0, Landroid/util/SparseArray;->mSize:I

    #@9
    return v0
.end method

.method public valueAt(I)Ljava/lang/Object;
    .registers 3
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    #@0
    .prologue
    .line 234
    .local p0, this:Landroid/util/SparseArray;,"Landroid/util/SparseArray<TE;>;"
    iget-boolean v0, p0, Landroid/util/SparseArray;->mGarbage:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 235
    invoke-direct {p0}, Landroid/util/SparseArray;->gc()V

    #@7
    .line 238
    :cond_7
    iget-object v0, p0, Landroid/util/SparseArray;->mValues:[Ljava/lang/Object;

    #@9
    aget-object v0, v0, p1

    #@b
    return-object v0
.end method
