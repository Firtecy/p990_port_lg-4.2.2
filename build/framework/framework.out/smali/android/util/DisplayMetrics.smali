.class public Landroid/util/DisplayMetrics;
.super Ljava/lang/Object;
.source "DisplayMetrics.java"


# static fields
.field public static final DENSITY_DEFAULT:I = 0xa0

.field public static final DENSITY_DEFAULT_SCALE:F = 0.00625f

.field public static DENSITY_DEVICE:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

#the value of this static final field might be set in the static constructor
.field public static final DENSITY_DEVICE_SECONDARY:I = 0x0

.field public static final DENSITY_HIGH:I = 0xf0

.field public static final DENSITY_LOW:I = 0x78

.field public static final DENSITY_MEDIUM:I = 0xa0

.field public static final DENSITY_TV:I = 0xd5

.field public static final DENSITY_XHIGH:I = 0x140

.field public static final DENSITY_XXHIGH:I = 0x1e0


# instance fields
.field public density:F

.field public densityDpi:I

.field public heightPixels:I

.field public noncompatDensity:F

.field public noncompatDensityDpi:I

.field public noncompatHeightPixels:I

.field public noncompatScaledDensity:F

.field public noncompatWidthPixels:I

.field public noncompatXdpi:F

.field public noncompatYdpi:F

.field public scaledDensity:F

.field public widthPixels:I

.field public xdpi:F

.field public ydpi:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 95
    invoke-static {}, Landroid/util/DisplayMetrics;->getDeviceDensity()I

    #@3
    move-result v0

    #@4
    sput v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@6
    .line 101
    invoke-static {}, Landroid/util/DisplayMetrics;->getDeviceDensitySecondary()I

    #@9
    move-result v0

    #@a
    sput v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@c
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 192
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 193
    return-void
.end method

.method private static getDeviceDensity()I
    .registers 3

    #@0
    .prologue
    .line 288
    const-string/jumbo v0, "qemu.sf.lcd_density"

    #@3
    const-string/jumbo v1, "ro.sf.lcd_density"

    #@6
    const/16 v2, 0xa0

    #@8
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@b
    move-result v1

    #@c
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@f
    move-result v0

    #@10
    return v0
.end method

.method private static getDeviceDensitySecondary()I
    .registers 2

    #@0
    .prologue
    .line 294
    const-string/jumbo v0, "ro.sf.lcd_density_secondary"

    #@3
    invoke-static {}, Landroid/util/DisplayMetrics;->getDeviceDensity()I

    #@6
    move-result v1

    #@7
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@a
    move-result v0

    #@b
    return v0
.end method


# virtual methods
.method public equals(Landroid/util/DisplayMetrics;)Z
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 241
    invoke-virtual {p0, p1}, Landroid/util/DisplayMetrics;->equalsPhysical(Landroid/util/DisplayMetrics;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_18

    #@6
    iget v0, p0, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@8
    iget v1, p1, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@a
    cmpl-float v0, v0, v1

    #@c
    if-nez v0, :cond_18

    #@e
    iget v0, p0, Landroid/util/DisplayMetrics;->noncompatScaledDensity:F

    #@10
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatScaledDensity:F

    #@12
    cmpl-float v0, v0, v1

    #@14
    if-nez v0, :cond_18

    #@16
    const/4 v0, 0x1

    #@17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 231
    instance-of v0, p1, Landroid/util/DisplayMetrics;

    #@2
    if-eqz v0, :cond_e

    #@4
    check-cast p1, Landroid/util/DisplayMetrics;

    #@6
    .end local p1
    invoke-virtual {p0, p1}, Landroid/util/DisplayMetrics;->equals(Landroid/util/DisplayMetrics;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public equalsPhysical(Landroid/util/DisplayMetrics;)Z
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 256
    if-eqz p1, :cond_58

    #@2
    iget v0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@4
    iget v1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@6
    if-ne v0, v1, :cond_58

    #@8
    iget v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@a
    iget v1, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@c
    if-ne v0, v1, :cond_58

    #@e
    iget v0, p0, Landroid/util/DisplayMetrics;->density:F

    #@10
    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    #@12
    cmpl-float v0, v0, v1

    #@14
    if-nez v0, :cond_58

    #@16
    iget v0, p0, Landroid/util/DisplayMetrics;->densityDpi:I

    #@18
    iget v1, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@1a
    if-ne v0, v1, :cond_58

    #@1c
    iget v0, p0, Landroid/util/DisplayMetrics;->xdpi:F

    #@1e
    iget v1, p1, Landroid/util/DisplayMetrics;->xdpi:F

    #@20
    cmpl-float v0, v0, v1

    #@22
    if-nez v0, :cond_58

    #@24
    iget v0, p0, Landroid/util/DisplayMetrics;->ydpi:F

    #@26
    iget v1, p1, Landroid/util/DisplayMetrics;->ydpi:F

    #@28
    cmpl-float v0, v0, v1

    #@2a
    if-nez v0, :cond_58

    #@2c
    iget v0, p0, Landroid/util/DisplayMetrics;->noncompatWidthPixels:I

    #@2e
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatWidthPixels:I

    #@30
    if-ne v0, v1, :cond_58

    #@32
    iget v0, p0, Landroid/util/DisplayMetrics;->noncompatHeightPixels:I

    #@34
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatHeightPixels:I

    #@36
    if-ne v0, v1, :cond_58

    #@38
    iget v0, p0, Landroid/util/DisplayMetrics;->noncompatDensity:F

    #@3a
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatDensity:F

    #@3c
    cmpl-float v0, v0, v1

    #@3e
    if-nez v0, :cond_58

    #@40
    iget v0, p0, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@42
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@44
    if-ne v0, v1, :cond_58

    #@46
    iget v0, p0, Landroid/util/DisplayMetrics;->noncompatXdpi:F

    #@48
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatXdpi:F

    #@4a
    cmpl-float v0, v0, v1

    #@4c
    if-nez v0, :cond_58

    #@4e
    iget v0, p0, Landroid/util/DisplayMetrics;->noncompatYdpi:F

    #@50
    iget v1, p1, Landroid/util/DisplayMetrics;->noncompatYdpi:F

    #@52
    cmpl-float v0, v0, v1

    #@54
    if-nez v0, :cond_58

    #@56
    const/4 v0, 0x1

    #@57
    :goto_57
    return v0

    #@58
    :cond_58
    const/4 v0, 0x0

    #@59
    goto :goto_57
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 273
    iget v0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@2
    iget v1, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@4
    mul-int/2addr v0, v1

    #@5
    iget v1, p0, Landroid/util/DisplayMetrics;->densityDpi:I

    #@7
    mul-int/2addr v0, v1

    #@8
    return v0
.end method

.method public setTo(Landroid/util/DisplayMetrics;)V
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 196
    iget v0, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@2
    iput v0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@4
    .line 197
    iget v0, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@6
    iput v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@8
    .line 198
    iget v0, p1, Landroid/util/DisplayMetrics;->density:F

    #@a
    iput v0, p0, Landroid/util/DisplayMetrics;->density:F

    #@c
    .line 199
    iget v0, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@e
    iput v0, p0, Landroid/util/DisplayMetrics;->densityDpi:I

    #@10
    .line 200
    iget v0, p1, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@12
    iput v0, p0, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@14
    .line 201
    iget v0, p1, Landroid/util/DisplayMetrics;->xdpi:F

    #@16
    iput v0, p0, Landroid/util/DisplayMetrics;->xdpi:F

    #@18
    .line 202
    iget v0, p1, Landroid/util/DisplayMetrics;->ydpi:F

    #@1a
    iput v0, p0, Landroid/util/DisplayMetrics;->ydpi:F

    #@1c
    .line 203
    iget v0, p1, Landroid/util/DisplayMetrics;->noncompatWidthPixels:I

    #@1e
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatWidthPixels:I

    #@20
    .line 204
    iget v0, p1, Landroid/util/DisplayMetrics;->noncompatHeightPixels:I

    #@22
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatHeightPixels:I

    #@24
    .line 205
    iget v0, p1, Landroid/util/DisplayMetrics;->noncompatDensity:F

    #@26
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatDensity:F

    #@28
    .line 206
    iget v0, p1, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@2a
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@2c
    .line 207
    iget v0, p1, Landroid/util/DisplayMetrics;->noncompatScaledDensity:F

    #@2e
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatScaledDensity:F

    #@30
    .line 208
    iget v0, p1, Landroid/util/DisplayMetrics;->noncompatXdpi:F

    #@32
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatXdpi:F

    #@34
    .line 209
    iget v0, p1, Landroid/util/DisplayMetrics;->noncompatYdpi:F

    #@36
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatYdpi:F

    #@38
    .line 210
    return-void
.end method

.method public setToDefaults()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 213
    iput v0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@3
    .line 214
    iput v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@5
    .line 215
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@7
    int-to-float v0, v0

    #@8
    const/high16 v1, 0x4320

    #@a
    div-float/2addr v0, v1

    #@b
    iput v0, p0, Landroid/util/DisplayMetrics;->density:F

    #@d
    .line 216
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@f
    iput v0, p0, Landroid/util/DisplayMetrics;->densityDpi:I

    #@11
    .line 217
    iget v0, p0, Landroid/util/DisplayMetrics;->density:F

    #@13
    iput v0, p0, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@15
    .line 218
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@17
    int-to-float v0, v0

    #@18
    iput v0, p0, Landroid/util/DisplayMetrics;->xdpi:F

    #@1a
    .line 219
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@1c
    int-to-float v0, v0

    #@1d
    iput v0, p0, Landroid/util/DisplayMetrics;->ydpi:F

    #@1f
    .line 220
    iget v0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@21
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatWidthPixels:I

    #@23
    .line 221
    iget v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@25
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatHeightPixels:I

    #@27
    .line 222
    iget v0, p0, Landroid/util/DisplayMetrics;->density:F

    #@29
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatDensity:F

    #@2b
    .line 223
    iget v0, p0, Landroid/util/DisplayMetrics;->densityDpi:I

    #@2d
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@2f
    .line 224
    iget v0, p0, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@31
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatScaledDensity:F

    #@33
    .line 225
    iget v0, p0, Landroid/util/DisplayMetrics;->xdpi:F

    #@35
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatXdpi:F

    #@37
    .line 226
    iget v0, p0, Landroid/util/DisplayMetrics;->ydpi:F

    #@39
    iput v0, p0, Landroid/util/DisplayMetrics;->noncompatYdpi:F

    #@3b
    .line 227
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DisplayMetrics{density="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/util/DisplayMetrics;->density:F

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", width="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", height="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", scaledDensity="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ", xdpi="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Landroid/util/DisplayMetrics;->xdpi:F

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, ", ydpi="

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget v1, p0, Landroid/util/DisplayMetrics;->ydpi:F

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string/jumbo v1, "}"

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v0

    #@58
    return-object v0
.end method
