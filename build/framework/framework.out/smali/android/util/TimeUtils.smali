.class public Landroid/util/TimeUtils;
.super Ljava/lang/Object;
.source "TimeUtils.java"


# static fields
.field private static final DBG:Z = false

.field public static final HUNDRED_DAY_FIELD_LEN:I = 0x13

.field private static final LARGEST_DURATION:J = 0x141dd75fffL

.field private static final SECONDS_PER_DAY:I = 0x15180

.field private static final SECONDS_PER_HOUR:I = 0xe10

.field private static final SECONDS_PER_MINUTE:I = 0x3c

.field private static final TAG:Ljava/lang/String; = "TimeUtils"

.field private static sFormatStr:[C

.field private static final sFormatSync:Ljava/lang/Object;

.field private static sLastCountry:Ljava/lang/String;

.field private static final sLastLockObj:Ljava/lang/Object;

.field private static sLastUniqueCountry:Ljava/lang/String;

.field private static final sLastUniqueLockObj:Ljava/lang/Object;

.field private static sLastUniqueZoneOffsets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field

.field private static sLastZones:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 48
    new-instance v0, Ljava/lang/Object;

    #@3
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@6
    sput-object v0, Landroid/util/TimeUtils;->sLastLockObj:Ljava/lang/Object;

    #@8
    .line 49
    sput-object v1, Landroid/util/TimeUtils;->sLastZones:Ljava/util/ArrayList;

    #@a
    .line 50
    sput-object v1, Landroid/util/TimeUtils;->sLastCountry:Ljava/lang/String;

    #@c
    .line 53
    new-instance v0, Ljava/lang/Object;

    #@e
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@11
    sput-object v0, Landroid/util/TimeUtils;->sLastUniqueLockObj:Ljava/lang/Object;

    #@13
    .line 54
    sput-object v1, Landroid/util/TimeUtils;->sLastUniqueZoneOffsets:Ljava/util/ArrayList;

    #@15
    .line 55
    sput-object v1, Landroid/util/TimeUtils;->sLastUniqueCountry:Ljava/lang/String;

    #@17
    .line 248
    new-instance v0, Ljava/lang/Object;

    #@19
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@1c
    sput-object v0, Landroid/util/TimeUtils;->sFormatSync:Ljava/lang/Object;

    #@1e
    .line 249
    const/16 v0, 0x18

    #@20
    new-array v0, v0, [C

    #@22
    sput-object v0, Landroid/util/TimeUtils;->sFormatStr:[C

    #@24
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static accumField(IIZI)I
    .registers 5
    .parameter "amt"
    .parameter "suffix"
    .parameter "always"
    .parameter "zeropad"

    #@0
    .prologue
    .line 254
    const/16 v0, 0x63

    #@2
    if-gt p0, v0, :cond_9

    #@4
    if-eqz p2, :cond_c

    #@6
    const/4 v0, 0x3

    #@7
    if-lt p3, v0, :cond_c

    #@9
    .line 255
    :cond_9
    add-int/lit8 v0, p1, 0x3

    #@b
    .line 263
    :goto_b
    return v0

    #@c
    .line 257
    :cond_c
    const/16 v0, 0x9

    #@e
    if-gt p0, v0, :cond_15

    #@10
    if-eqz p2, :cond_18

    #@12
    const/4 v0, 0x2

    #@13
    if-lt p3, v0, :cond_18

    #@15
    .line 258
    :cond_15
    add-int/lit8 v0, p1, 0x2

    #@17
    goto :goto_b

    #@18
    .line 260
    :cond_18
    if-nez p2, :cond_1c

    #@1a
    if-lez p0, :cond_1f

    #@1c
    .line 261
    :cond_1c
    add-int/lit8 v0, p1, 0x1

    #@1e
    goto :goto_b

    #@1f
    .line 263
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_b
.end method

.method public static formatDuration(JJLjava/io/PrintWriter;)V
    .registers 8
    .parameter "time"
    .parameter "now"
    .parameter "pw"

    #@0
    .prologue
    .line 388
    const-wide/16 v0, 0x0

    #@2
    cmp-long v0, p0, v0

    #@4
    if-nez v0, :cond_c

    #@6
    .line 389
    const-string v0, "--"

    #@8
    invoke-virtual {p4, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b
    .line 393
    :goto_b
    return-void

    #@c
    .line 392
    :cond_c
    sub-long v0, p0, p2

    #@e
    const/4 v2, 0x0

    #@f
    invoke-static {v0, v1, p4, v2}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;I)V

    #@12
    goto :goto_b
.end method

.method public static formatDuration(JLjava/io/PrintWriter;)V
    .registers 4
    .parameter "duration"
    .parameter "pw"

    #@0
    .prologue
    .line 383
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;I)V

    #@4
    .line 384
    return-void
.end method

.method public static formatDuration(JLjava/io/PrintWriter;I)V
    .registers 9
    .parameter "duration"
    .parameter "pw"
    .parameter "fieldLen"

    #@0
    .prologue
    .line 375
    sget-object v2, Landroid/util/TimeUtils;->sFormatSync:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 376
    :try_start_3
    invoke-static {p0, p1, p3}, Landroid/util/TimeUtils;->formatDurationLocked(JI)I

    #@6
    move-result v0

    #@7
    .line 377
    .local v0, len:I
    new-instance v1, Ljava/lang/String;

    #@9
    sget-object v3, Landroid/util/TimeUtils;->sFormatStr:[C

    #@b
    const/4 v4, 0x0

    #@c
    invoke-direct {v1, v3, v4, v0}, Ljava/lang/String;-><init>([CII)V

    #@f
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12
    .line 378
    monitor-exit v2

    #@13
    .line 379
    return-void

    #@14
    .line 378
    .end local v0           #len:I
    :catchall_14
    move-exception v1

    #@15
    monitor-exit v2
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method

.method public static formatDuration(JLjava/lang/StringBuilder;)V
    .registers 7
    .parameter "duration"
    .parameter "builder"

    #@0
    .prologue
    .line 367
    sget-object v2, Landroid/util/TimeUtils;->sFormatSync:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 368
    const/4 v1, 0x0

    #@4
    :try_start_4
    invoke-static {p0, p1, v1}, Landroid/util/TimeUtils;->formatDurationLocked(JI)I

    #@7
    move-result v0

    #@8
    .line 369
    .local v0, len:I
    sget-object v1, Landroid/util/TimeUtils;->sFormatStr:[C

    #@a
    const/4 v3, 0x0

    #@b
    invoke-virtual {p2, v1, v3, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@e
    .line 370
    monitor-exit v2

    #@f
    .line 371
    return-void

    #@10
    .line 370
    .end local v0           #len:I
    :catchall_10
    move-exception v1

    #@11
    monitor-exit v2
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_10

    #@12
    throw v1
.end method

.method private static formatDurationLocked(JI)I
    .registers 24
    .parameter "duration"
    .parameter "fieldLen"

    #@0
    .prologue
    .line 291
    sget-object v4, Landroid/util/TimeUtils;->sFormatStr:[C

    #@2
    array-length v4, v4

    #@3
    move/from16 v0, p2

    #@5
    if-ge v4, v0, :cond_d

    #@7
    .line 292
    move/from16 v0, p2

    #@9
    new-array v4, v0, [C

    #@b
    sput-object v4, Landroid/util/TimeUtils;->sFormatStr:[C

    #@d
    .line 295
    :cond_d
    sget-object v2, Landroid/util/TimeUtils;->sFormatStr:[C

    #@f
    .line 297
    .local v2, formatStr:[C
    const-wide/16 v6, 0x0

    #@11
    cmp-long v4, p0, v6

    #@13
    if-nez v4, :cond_30

    #@15
    .line 298
    const/4 v5, 0x0

    #@16
    .line 299
    .local v5, pos:I
    add-int/lit8 p2, p2, -0x1

    #@18
    move/from16 v16, v5

    #@1a
    .line 300
    .end local v5           #pos:I
    .local v16, pos:I
    :goto_1a
    move/from16 v0, v16

    #@1c
    move/from16 v1, p2

    #@1e
    if-ge v0, v1, :cond_29

    #@20
    .line 301
    add-int/lit8 v5, v16, 0x1

    #@22
    .end local v16           #pos:I
    .restart local v5       #pos:I
    const/16 v4, 0x20

    #@24
    aput-char v4, v2, v16

    #@26
    move/from16 v16, v5

    #@28
    .end local v5           #pos:I
    .restart local v16       #pos:I
    goto :goto_1a

    #@29
    .line 303
    :cond_29
    const/16 v4, 0x30

    #@2b
    aput-char v4, v2, v16

    #@2d
    .line 304
    add-int/lit8 v4, v16, 0x1

    #@2f
    .line 362
    .end local v16           #pos:I
    :goto_2f
    return v4

    #@30
    .line 308
    :cond_30
    const-wide/16 v6, 0x0

    #@32
    cmp-long v4, p0, v6

    #@34
    if-lez v4, :cond_c9

    #@36
    .line 309
    const/16 v17, 0x2b

    #@38
    .line 315
    .local v17, prefix:C
    :goto_38
    const-wide v6, 0x141dd75fffL

    #@3d
    cmp-long v4, p0, v6

    #@3f
    if-lez v4, :cond_46

    #@41
    .line 316
    const-wide p0, 0x141dd75fffL

    #@46
    .line 319
    :cond_46
    const-wide/16 v6, 0x3e8

    #@48
    rem-long v6, p0, v6

    #@4a
    long-to-int v13, v6

    #@4b
    .line 320
    .local v13, millis:I
    const-wide/16 v6, 0x3e8

    #@4d
    div-long v6, p0, v6

    #@4f
    long-to-double v6, v6

    #@50
    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    #@53
    move-result-wide v6

    #@54
    double-to-int v0, v6

    #@55
    move/from16 v18, v0

    #@57
    .line 321
    .local v18, seconds:I
    const/4 v3, 0x0

    #@58
    .local v3, days:I
    const/4 v12, 0x0

    #@59
    .local v12, hours:I
    const/4 v14, 0x0

    #@5a
    .line 323
    .local v14, minutes:I
    const v4, 0x15180

    #@5d
    move/from16 v0, v18

    #@5f
    if-le v0, v4, :cond_6c

    #@61
    .line 324
    const v4, 0x15180

    #@64
    div-int v3, v18, v4

    #@66
    .line 325
    const v4, 0x15180

    #@69
    mul-int/2addr v4, v3

    #@6a
    sub-int v18, v18, v4

    #@6c
    .line 327
    :cond_6c
    const/16 v4, 0xe10

    #@6e
    move/from16 v0, v18

    #@70
    if-le v0, v4, :cond_7a

    #@72
    .line 328
    move/from16 v0, v18

    #@74
    div-int/lit16 v12, v0, 0xe10

    #@76
    .line 329
    mul-int/lit16 v4, v12, 0xe10

    #@78
    sub-int v18, v18, v4

    #@7a
    .line 331
    :cond_7a
    const/16 v4, 0x3c

    #@7c
    move/from16 v0, v18

    #@7e
    if-le v0, v4, :cond_86

    #@80
    .line 332
    div-int/lit8 v14, v18, 0x3c

    #@82
    .line 333
    mul-int/lit8 v4, v14, 0x3c

    #@84
    sub-int v18, v18, v4

    #@86
    .line 336
    :cond_86
    const/4 v5, 0x0

    #@87
    .line 338
    .restart local v5       #pos:I
    if-eqz p2, :cond_da

    #@89
    .line 339
    const/4 v4, 0x1

    #@8a
    const/4 v6, 0x0

    #@8b
    const/4 v7, 0x0

    #@8c
    invoke-static {v3, v4, v6, v7}, Landroid/util/TimeUtils;->accumField(IIZI)I

    #@8f
    move-result v15

    #@90
    .line 340
    .local v15, myLen:I
    const/4 v6, 0x1

    #@91
    if-lez v15, :cond_d2

    #@93
    const/4 v4, 0x1

    #@94
    :goto_94
    const/4 v7, 0x2

    #@95
    invoke-static {v12, v6, v4, v7}, Landroid/util/TimeUtils;->accumField(IIZI)I

    #@98
    move-result v4

    #@99
    add-int/2addr v15, v4

    #@9a
    .line 341
    const/4 v6, 0x1

    #@9b
    if-lez v15, :cond_d4

    #@9d
    const/4 v4, 0x1

    #@9e
    :goto_9e
    const/4 v7, 0x2

    #@9f
    invoke-static {v14, v6, v4, v7}, Landroid/util/TimeUtils;->accumField(IIZI)I

    #@a2
    move-result v4

    #@a3
    add-int/2addr v15, v4

    #@a4
    .line 342
    const/4 v6, 0x1

    #@a5
    if-lez v15, :cond_d6

    #@a7
    const/4 v4, 0x1

    #@a8
    :goto_a8
    const/4 v7, 0x2

    #@a9
    move/from16 v0, v18

    #@ab
    invoke-static {v0, v6, v4, v7}, Landroid/util/TimeUtils;->accumField(IIZI)I

    #@ae
    move-result v4

    #@af
    add-int/2addr v15, v4

    #@b0
    .line 343
    const/4 v6, 0x2

    #@b1
    const/4 v7, 0x1

    #@b2
    if-lez v15, :cond_d8

    #@b4
    const/4 v4, 0x3

    #@b5
    :goto_b5
    invoke-static {v13, v6, v7, v4}, Landroid/util/TimeUtils;->accumField(IIZI)I

    #@b8
    move-result v4

    #@b9
    add-int/lit8 v4, v4, 0x1

    #@bb
    add-int/2addr v15, v4

    #@bc
    .line 344
    :goto_bc
    move/from16 v0, p2

    #@be
    if-ge v15, v0, :cond_da

    #@c0
    .line 345
    const/16 v4, 0x20

    #@c2
    aput-char v4, v2, v5

    #@c4
    .line 346
    add-int/lit8 v5, v5, 0x1

    #@c6
    .line 347
    add-int/lit8 v15, v15, 0x1

    #@c8
    goto :goto_bc

    #@c9
    .line 311
    .end local v3           #days:I
    .end local v5           #pos:I
    .end local v12           #hours:I
    .end local v13           #millis:I
    .end local v14           #minutes:I
    .end local v15           #myLen:I
    .end local v17           #prefix:C
    .end local v18           #seconds:I
    :cond_c9
    const/16 v17, 0x2d

    #@cb
    .line 312
    .restart local v17       #prefix:C
    move-wide/from16 v0, p0

    #@cd
    neg-long v0, v0

    #@ce
    move-wide/from16 p0, v0

    #@d0
    goto/16 :goto_38

    #@d2
    .line 340
    .restart local v3       #days:I
    .restart local v5       #pos:I
    .restart local v12       #hours:I
    .restart local v13       #millis:I
    .restart local v14       #minutes:I
    .restart local v15       #myLen:I
    .restart local v18       #seconds:I
    :cond_d2
    const/4 v4, 0x0

    #@d3
    goto :goto_94

    #@d4
    .line 341
    :cond_d4
    const/4 v4, 0x0

    #@d5
    goto :goto_9e

    #@d6
    .line 342
    :cond_d6
    const/4 v4, 0x0

    #@d7
    goto :goto_a8

    #@d8
    .line 343
    :cond_d8
    const/4 v4, 0x0

    #@d9
    goto :goto_b5

    #@da
    .line 351
    .end local v15           #myLen:I
    :cond_da
    aput-char v17, v2, v5

    #@dc
    .line 352
    add-int/lit8 v5, v5, 0x1

    #@de
    .line 354
    move/from16 v19, v5

    #@e0
    .line 355
    .local v19, start:I
    if-eqz p2, :cond_139

    #@e2
    const/16 v20, 0x1

    #@e4
    .line 356
    .local v20, zeropad:Z
    :goto_e4
    const/16 v4, 0x64

    #@e6
    const/4 v6, 0x0

    #@e7
    const/4 v7, 0x0

    #@e8
    invoke-static/range {v2 .. v7}, Landroid/util/TimeUtils;->printField([CICIZI)I

    #@eb
    move-result v5

    #@ec
    .line 357
    const/16 v8, 0x68

    #@ee
    move/from16 v0, v19

    #@f0
    if-eq v5, v0, :cond_13c

    #@f2
    const/4 v10, 0x1

    #@f3
    :goto_f3
    if-eqz v20, :cond_13e

    #@f5
    const/4 v11, 0x2

    #@f6
    :goto_f6
    move-object v6, v2

    #@f7
    move v7, v12

    #@f8
    move v9, v5

    #@f9
    invoke-static/range {v6 .. v11}, Landroid/util/TimeUtils;->printField([CICIZI)I

    #@fc
    move-result v5

    #@fd
    .line 358
    const/16 v8, 0x6d

    #@ff
    move/from16 v0, v19

    #@101
    if-eq v5, v0, :cond_140

    #@103
    const/4 v10, 0x1

    #@104
    :goto_104
    if-eqz v20, :cond_142

    #@106
    const/4 v11, 0x2

    #@107
    :goto_107
    move-object v6, v2

    #@108
    move v7, v14

    #@109
    move v9, v5

    #@10a
    invoke-static/range {v6 .. v11}, Landroid/util/TimeUtils;->printField([CICIZI)I

    #@10d
    move-result v5

    #@10e
    .line 359
    const/16 v8, 0x73

    #@110
    move/from16 v0, v19

    #@112
    if-eq v5, v0, :cond_144

    #@114
    const/4 v10, 0x1

    #@115
    :goto_115
    if-eqz v20, :cond_146

    #@117
    const/4 v11, 0x2

    #@118
    :goto_118
    move-object v6, v2

    #@119
    move/from16 v7, v18

    #@11b
    move v9, v5

    #@11c
    invoke-static/range {v6 .. v11}, Landroid/util/TimeUtils;->printField([CICIZI)I

    #@11f
    move-result v5

    #@120
    .line 360
    const/16 v8, 0x6d

    #@122
    const/4 v10, 0x1

    #@123
    if-eqz v20, :cond_148

    #@125
    move/from16 v0, v19

    #@127
    if-eq v5, v0, :cond_148

    #@129
    const/4 v11, 0x3

    #@12a
    :goto_12a
    move-object v6, v2

    #@12b
    move v7, v13

    #@12c
    move v9, v5

    #@12d
    invoke-static/range {v6 .. v11}, Landroid/util/TimeUtils;->printField([CICIZI)I

    #@130
    move-result v5

    #@131
    .line 361
    const/16 v4, 0x73

    #@133
    aput-char v4, v2, v5

    #@135
    .line 362
    add-int/lit8 v4, v5, 0x1

    #@137
    goto/16 :goto_2f

    #@139
    .line 355
    .end local v20           #zeropad:Z
    :cond_139
    const/16 v20, 0x0

    #@13b
    goto :goto_e4

    #@13c
    .line 357
    .restart local v20       #zeropad:Z
    :cond_13c
    const/4 v10, 0x0

    #@13d
    goto :goto_f3

    #@13e
    :cond_13e
    const/4 v11, 0x0

    #@13f
    goto :goto_f6

    #@140
    .line 358
    :cond_140
    const/4 v10, 0x0

    #@141
    goto :goto_104

    #@142
    :cond_142
    const/4 v11, 0x0

    #@143
    goto :goto_107

    #@144
    .line 359
    :cond_144
    const/4 v10, 0x0

    #@145
    goto :goto_115

    #@146
    :cond_146
    const/4 v11, 0x0

    #@147
    goto :goto_118

    #@148
    .line 360
    :cond_148
    const/4 v11, 0x0

    #@149
    goto :goto_12a
.end method

.method public static formatUptime(J)Ljava/lang/String;
    .registers 8
    .parameter "time"

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 397
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5
    move-result-wide v2

    #@6
    sub-long v0, p0, v2

    #@8
    .line 398
    .local v0, diff:J
    cmp-long v2, v0, v4

    #@a
    if-lez v2, :cond_2a

    #@c
    .line 399
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, " (in "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " ms)"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    .line 404
    :goto_29
    return-object v2

    #@2a
    .line 401
    :cond_2a
    cmp-long v2, v0, v4

    #@2c
    if-gez v2, :cond_4d

    #@2e
    .line 402
    new-instance v2, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, " ("

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    neg-long v3, v0

    #@3e
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    const-string v3, " ms ago)"

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    goto :goto_29

    #@4d
    .line 404
    :cond_4d
    new-instance v2, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    const-string v3, " (now)"

    #@58
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    goto :goto_29
.end method

.method public static getTimeZone(IZJLjava/lang/String;)Ljava/util/TimeZone;
    .registers 16
    .parameter "offset"
    .parameter "dst"
    .parameter "when"
    .parameter "country"

    #@0
    .prologue
    .line 64
    const/4 v0, 0x0

    #@1
    .line 66
    .local v0, best:Ljava/util/TimeZone;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@4
    move-result-object v8

    #@5
    .line 67
    .local v8, r:Landroid/content/res/Resources;
    const v10, 0x10f0010

    #@8
    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@b
    move-result-object v7

    #@c
    .line 68
    .local v7, parser:Landroid/content/res/XmlResourceParser;
    new-instance v5, Ljava/util/Date;

    #@e
    invoke-direct {v5, p2, p3}, Ljava/util/Date;-><init>(J)V

    #@11
    .line 70
    .local v5, d:Ljava/util/Date;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@14
    move-result-object v1

    #@15
    .line 71
    .local v1, current:Ljava/util/TimeZone;
    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    .line 72
    .local v3, currentName:Ljava/lang/String;
    invoke-virtual {v1, p2, p3}, Ljava/util/TimeZone;->getOffset(J)I

    #@1c
    move-result v4

    #@1d
    .line 73
    .local v4, currentOffset:I
    invoke-virtual {v1, v5}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    #@20
    move-result v2

    #@21
    .line 75
    .local v2, currentDst:Z
    invoke-static {p4}, Landroid/util/TimeUtils;->getTimeZones(Ljava/lang/String;)Ljava/util/ArrayList;

    #@24
    move-result-object v10

    #@25
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@28
    move-result-object v6

    #@29
    .local v6, i$:Ljava/util/Iterator;
    :cond_29
    :goto_29
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@2c
    move-result v10

    #@2d
    if-eqz v10, :cond_54

    #@2f
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@32
    move-result-object v9

    #@33
    check-cast v9, Ljava/util/TimeZone;

    #@35
    .line 80
    .local v9, tz:Ljava/util/TimeZone;
    invoke-virtual {v9}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@38
    move-result-object v10

    #@39
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v10

    #@3d
    if-eqz v10, :cond_44

    #@3f
    .line 81
    if-ne v4, p0, :cond_44

    #@41
    if-ne v2, p1, :cond_44

    #@43
    .line 99
    .end local v1           #current:Ljava/util/TimeZone;
    .end local v9           #tz:Ljava/util/TimeZone;
    :goto_43
    return-object v1

    #@44
    .line 91
    .restart local v1       #current:Ljava/util/TimeZone;
    .restart local v9       #tz:Ljava/util/TimeZone;
    :cond_44
    if-nez v0, :cond_29

    #@46
    .line 92
    invoke-virtual {v9, p2, p3}, Ljava/util/TimeZone;->getOffset(J)I

    #@49
    move-result v10

    #@4a
    if-ne v10, p0, :cond_29

    #@4c
    invoke-virtual {v9, v5}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    #@4f
    move-result v10

    #@50
    if-ne v10, p1, :cond_29

    #@52
    .line 94
    move-object v0, v9

    #@53
    goto :goto_29

    #@54
    .end local v9           #tz:Ljava/util/TimeZone;
    :cond_54
    move-object v1, v0

    #@55
    .line 99
    goto :goto_43
.end method

.method public static getTimeZoneDatabaseVersion()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 238
    invoke-static {}, Llibcore/util/ZoneInfoDB;->getVersion()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getTimeZones(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 12
    .parameter "country"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 159
    sget-object v9, Landroid/util/TimeUtils;->sLastLockObj:Ljava/lang/Object;

    #@2
    monitor-enter v9

    #@3
    .line 160
    if-eqz p0, :cond_11

    #@5
    :try_start_5
    sget-object v8, Landroid/util/TimeUtils;->sLastCountry:Ljava/lang/String;

    #@7
    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v8

    #@b
    if-eqz v8, :cond_11

    #@d
    .line 162
    sget-object v6, Landroid/util/TimeUtils;->sLastZones:Ljava/util/ArrayList;

    #@f
    monitor-exit v9

    #@10
    .line 216
    :cond_10
    :goto_10
    return-object v6

    #@11
    .line 164
    :cond_11
    monitor-exit v9
    :try_end_12
    .catchall {:try_start_5 .. :try_end_12} :catchall_4d

    #@12
    .line 166
    new-instance v6, Ljava/util/ArrayList;

    #@14
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@17
    .line 168
    .local v6, tzs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/TimeZone;>;"
    if-eqz p0, :cond_10

    #@19
    .line 173
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@1c
    move-result-object v4

    #@1d
    .line 174
    .local v4, r:Landroid/content/res/Resources;
    const v8, 0x10f0010

    #@20
    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@23
    move-result-object v3

    #@24
    .line 177
    .local v3, parser:Landroid/content/res/XmlResourceParser;
    :try_start_24
    const-string/jumbo v8, "timezones"

    #@27
    invoke-static {v3, v8}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@2a
    .line 180
    :cond_2a
    :goto_2a
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@2d
    .line 182
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    .line 183
    .local v2, element:Ljava/lang/String;
    if-eqz v2, :cond_3c

    #@33
    const-string/jumbo v8, "timezone"

    #@36
    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_39
    .catchall {:try_start_24 .. :try_end_39} :catchall_c3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_24 .. :try_end_39} :catch_7c
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_39} :catch_9f

    #@39
    move-result v8

    #@3a
    if-nez v8, :cond_50

    #@3c
    .line 209
    :cond_3c
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@3f
    .line 212
    .end local v2           #element:Ljava/lang/String;
    :goto_3f
    sget-object v9, Landroid/util/TimeUtils;->sLastLockObj:Ljava/lang/Object;

    #@41
    monitor-enter v9

    #@42
    .line 214
    :try_start_42
    sput-object v6, Landroid/util/TimeUtils;->sLastZones:Ljava/util/ArrayList;

    #@44
    .line 215
    sput-object p0, Landroid/util/TimeUtils;->sLastCountry:Ljava/lang/String;

    #@46
    .line 216
    sget-object v6, Landroid/util/TimeUtils;->sLastZones:Ljava/util/ArrayList;

    #@48
    .end local v6           #tzs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/TimeZone;>;"
    monitor-exit v9

    #@49
    goto :goto_10

    #@4a
    .line 217
    :catchall_4a
    move-exception v8

    #@4b
    monitor-exit v9
    :try_end_4c
    .catchall {:try_start_42 .. :try_end_4c} :catchall_4a

    #@4c
    throw v8

    #@4d
    .line 164
    .end local v3           #parser:Landroid/content/res/XmlResourceParser;
    .end local v4           #r:Landroid/content/res/Resources;
    :catchall_4d
    move-exception v8

    #@4e
    :try_start_4e
    monitor-exit v9
    :try_end_4f
    .catchall {:try_start_4e .. :try_end_4f} :catchall_4d

    #@4f
    throw v8

    #@50
    .line 187
    .restart local v2       #element:Ljava/lang/String;
    .restart local v3       #parser:Landroid/content/res/XmlResourceParser;
    .restart local v4       #r:Landroid/content/res/Resources;
    .restart local v6       #tzs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/TimeZone;>;"
    :cond_50
    const/4 v8, 0x0

    #@51
    :try_start_51
    const-string v9, "code"

    #@53
    invoke-interface {v3, v8, v9}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@56
    move-result-object v0

    #@57
    .line 189
    .local v0, code:Ljava/lang/String;
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v8

    #@5b
    if-eqz v8, :cond_2a

    #@5d
    .line 190
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->next()I

    #@60
    move-result v8

    #@61
    const/4 v9, 0x4

    #@62
    if-ne v8, v9, :cond_2a

    #@64
    .line 191
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;

    #@67
    move-result-object v7

    #@68
    .line 192
    .local v7, zoneIdString:Ljava/lang/String;
    invoke-static {v7}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@6b
    move-result-object v5

    #@6c
    .line 193
    .local v5, tz:Ljava/util/TimeZone;
    invoke-virtual {v5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@6f
    move-result-object v8

    #@70
    const-string v9, "GMT"

    #@72
    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@75
    move-result v8

    #@76
    if-nez v8, :cond_2a

    #@78
    .line 195
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7b
    .catchall {:try_start_51 .. :try_end_7b} :catchall_c3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_51 .. :try_end_7b} :catch_7c
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_7b} :catch_9f

    #@7b
    goto :goto_2a

    #@7c
    .line 204
    .end local v0           #code:Ljava/lang/String;
    .end local v2           #element:Ljava/lang/String;
    .end local v5           #tz:Ljava/util/TimeZone;
    .end local v7           #zoneIdString:Ljava/lang/String;
    :catch_7c
    move-exception v1

    #@7d
    .line 205
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_7d
    const-string v8, "TimeUtils"

    #@7f
    new-instance v9, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v10, "Got xml parser exception getTimeZone(\'"

    #@86
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v9

    #@8a
    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v9

    #@8e
    const-string v10, "\'): e="

    #@90
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v9

    #@94
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v9

    #@98
    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9b
    .catchall {:try_start_7d .. :try_end_9b} :catchall_c3

    #@9b
    .line 209
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@9e
    goto :goto_3f

    #@9f
    .line 206
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_9f
    move-exception v1

    #@a0
    .line 207
    .local v1, e:Ljava/io/IOException;
    :try_start_a0
    const-string v8, "TimeUtils"

    #@a2
    new-instance v9, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    const-string v10, "Got IO exception getTimeZone(\'"

    #@a9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v9

    #@ad
    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v9

    #@b1
    const-string v10, "\'): e="

    #@b3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v9

    #@b7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v9

    #@bb
    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_be
    .catchall {:try_start_a0 .. :try_end_be} :catchall_c3

    #@be
    .line 209
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@c1
    goto/16 :goto_3f

    #@c3
    .end local v1           #e:Ljava/io/IOException;
    :catchall_c3
    move-exception v8

    #@c4
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@c7
    throw v8
.end method

.method public static getTimeZonesWithUniqueOffsets(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 9
    .parameter "country"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 110
    sget-object v7, Landroid/util/TimeUtils;->sLastUniqueLockObj:Ljava/lang/Object;

    #@2
    monitor-enter v7

    #@3
    .line 111
    if-eqz p0, :cond_11

    #@5
    :try_start_5
    sget-object v6, Landroid/util/TimeUtils;->sLastUniqueCountry:Ljava/lang/String;

    #@7
    invoke-virtual {p0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v6

    #@b
    if-eqz v6, :cond_11

    #@d
    .line 116
    sget-object v6, Landroid/util/TimeUtils;->sLastUniqueZoneOffsets:Ljava/util/ArrayList;

    #@f
    monitor-exit v7

    #@10
    .line 146
    :goto_10
    return-object v6

    #@11
    .line 118
    :cond_11
    monitor-exit v7
    :try_end_12
    .catchall {:try_start_5 .. :try_end_12} :catchall_4a

    #@12
    .line 120
    invoke-static {p0}, Landroid/util/TimeUtils;->getTimeZones(Ljava/lang/String;)Ljava/util/ArrayList;

    #@15
    move-result-object v5

    #@16
    .line 121
    .local v5, zones:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/util/TimeZone;>;"
    new-instance v3, Ljava/util/ArrayList;

    #@18
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@1b
    .line 122
    .local v3, uniqueTimeZones:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/TimeZone;>;"
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1e
    move-result-object v2

    #@1f
    .local v2, i$:Ljava/util/Iterator;
    :cond_1f
    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@22
    move-result v6

    #@23
    if-eqz v6, :cond_50

    #@25
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@28
    move-result-object v4

    #@29
    check-cast v4, Ljava/util/TimeZone;

    #@2b
    .line 125
    .local v4, zone:Ljava/util/TimeZone;
    const/4 v0, 0x0

    #@2c
    .line 126
    .local v0, found:Z
    const/4 v1, 0x0

    #@2d
    .local v1, i:I
    :goto_2d
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@30
    move-result v6

    #@31
    if-ge v1, v6, :cond_44

    #@33
    .line 127
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@36
    move-result-object v6

    #@37
    check-cast v6, Ljava/util/TimeZone;

    #@39
    invoke-virtual {v6}, Ljava/util/TimeZone;->getRawOffset()I

    #@3c
    move-result v6

    #@3d
    invoke-virtual {v4}, Ljava/util/TimeZone;->getRawOffset()I

    #@40
    move-result v7

    #@41
    if-ne v6, v7, :cond_4d

    #@43
    .line 128
    const/4 v0, 0x1

    #@44
    .line 132
    :cond_44
    if-nez v0, :cond_1f

    #@46
    .line 137
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@49
    goto :goto_1f

    #@4a
    .line 118
    .end local v0           #found:Z
    .end local v1           #i:I
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #uniqueTimeZones:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/TimeZone;>;"
    .end local v4           #zone:Ljava/util/TimeZone;
    .end local v5           #zones:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/util/TimeZone;>;"
    :catchall_4a
    move-exception v6

    #@4b
    :try_start_4b
    monitor-exit v7
    :try_end_4c
    .catchall {:try_start_4b .. :try_end_4c} :catchall_4a

    #@4c
    throw v6

    #@4d
    .line 126
    .restart local v0       #found:Z
    .restart local v1       #i:I
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #uniqueTimeZones:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/TimeZone;>;"
    .restart local v4       #zone:Ljava/util/TimeZone;
    .restart local v5       #zones:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/util/TimeZone;>;"
    :cond_4d
    add-int/lit8 v1, v1, 0x1

    #@4f
    goto :goto_2d

    #@50
    .line 141
    .end local v0           #found:Z
    .end local v1           #i:I
    .end local v4           #zone:Ljava/util/TimeZone;
    :cond_50
    sget-object v7, Landroid/util/TimeUtils;->sLastUniqueLockObj:Ljava/lang/Object;

    #@52
    monitor-enter v7

    #@53
    .line 143
    :try_start_53
    sput-object v3, Landroid/util/TimeUtils;->sLastUniqueZoneOffsets:Ljava/util/ArrayList;

    #@55
    .line 144
    sput-object p0, Landroid/util/TimeUtils;->sLastUniqueCountry:Ljava/lang/String;

    #@57
    .line 146
    sget-object v6, Landroid/util/TimeUtils;->sLastUniqueZoneOffsets:Ljava/util/ArrayList;

    #@59
    monitor-exit v7

    #@5a
    goto :goto_10

    #@5b
    .line 147
    :catchall_5b
    move-exception v6

    #@5c
    monitor-exit v7
    :try_end_5d
    .catchall {:try_start_53 .. :try_end_5d} :catchall_5b

    #@5d
    throw v6
.end method

.method public static logTimeOfDay(J)Ljava/lang/String;
    .registers 6
    .parameter "millis"

    #@0
    .prologue
    .line 416
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@3
    move-result-object v0

    #@4
    .line 417
    .local v0, c:Ljava/util/Calendar;
    const-wide/16 v1, 0x0

    #@6
    cmp-long v1, p0, v1

    #@8
    if-ltz v1, :cond_29

    #@a
    .line 418
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@d
    .line 419
    const-string v1, "%tm-%td %tH:%tM:%tS.%tL"

    #@f
    const/4 v2, 0x6

    #@10
    new-array v2, v2, [Ljava/lang/Object;

    #@12
    const/4 v3, 0x0

    #@13
    aput-object v0, v2, v3

    #@15
    const/4 v3, 0x1

    #@16
    aput-object v0, v2, v3

    #@18
    const/4 v3, 0x2

    #@19
    aput-object v0, v2, v3

    #@1b
    const/4 v3, 0x3

    #@1c
    aput-object v0, v2, v3

    #@1e
    const/4 v3, 0x4

    #@1f
    aput-object v0, v2, v3

    #@21
    const/4 v3, 0x5

    #@22
    aput-object v0, v2, v3

    #@24
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    .line 421
    :goto_28
    return-object v1

    #@29
    :cond_29
    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    goto :goto_28
.end method

.method private static printField([CICIZI)I
    .registers 9
    .parameter "formatStr"
    .parameter "amt"
    .parameter "suffix"
    .parameter "pos"
    .parameter "always"
    .parameter "zeropad"

    #@0
    .prologue
    .line 268
    if-nez p4, :cond_4

    #@2
    if-lez p1, :cond_3c

    #@4
    .line 269
    :cond_4
    move v1, p3

    #@5
    .line 270
    .local v1, startPos:I
    if-eqz p4, :cond_a

    #@7
    const/4 v2, 0x3

    #@8
    if-ge p5, v2, :cond_e

    #@a
    :cond_a
    const/16 v2, 0x63

    #@c
    if-le p1, v2, :cond_1a

    #@e
    .line 271
    :cond_e
    div-int/lit8 v0, p1, 0x64

    #@10
    .line 272
    .local v0, dig:I
    add-int/lit8 v2, v0, 0x30

    #@12
    int-to-char v2, v2

    #@13
    aput-char v2, p0, p3

    #@15
    .line 273
    add-int/lit8 p3, p3, 0x1

    #@17
    .line 274
    mul-int/lit8 v2, v0, 0x64

    #@19
    sub-int/2addr p1, v2

    #@1a
    .line 276
    .end local v0           #dig:I
    :cond_1a
    if-eqz p4, :cond_1f

    #@1c
    const/4 v2, 0x2

    #@1d
    if-ge p5, v2, :cond_25

    #@1f
    :cond_1f
    const/16 v2, 0x9

    #@21
    if-gt p1, v2, :cond_25

    #@23
    if-eq v1, p3, :cond_31

    #@25
    .line 277
    :cond_25
    div-int/lit8 v0, p1, 0xa

    #@27
    .line 278
    .restart local v0       #dig:I
    add-int/lit8 v2, v0, 0x30

    #@29
    int-to-char v2, v2

    #@2a
    aput-char v2, p0, p3

    #@2c
    .line 279
    add-int/lit8 p3, p3, 0x1

    #@2e
    .line 280
    mul-int/lit8 v2, v0, 0xa

    #@30
    sub-int/2addr p1, v2

    #@31
    .line 282
    .end local v0           #dig:I
    :cond_31
    add-int/lit8 v2, p1, 0x30

    #@33
    int-to-char v2, v2

    #@34
    aput-char v2, p0, p3

    #@36
    .line 283
    add-int/lit8 p3, p3, 0x1

    #@38
    .line 284
    aput-char p2, p0, p3

    #@3a
    .line 285
    add-int/lit8 p3, p3, 0x1

    #@3c
    .line 287
    .end local v1           #startPos:I
    :cond_3c
    return p3
.end method
