.class public Landroid/util/DebugUtils;
.super Ljava/lang/Object;
.source "DebugUtils.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static buildShortClassTag(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
    .registers 5
    .parameter "cls"
    .parameter "out"

    #@0
    .prologue
    .line 108
    if-nez p0, :cond_9

    #@2
    .line 109
    const-string/jumbo v2, "null"

    #@5
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    .line 123
    :goto_8
    return-void

    #@9
    .line 111
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    .line 112
    .local v1, simpleName:Ljava/lang/String;
    if-eqz v1, :cond_19

    #@13
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_2f

    #@19
    .line 113
    :cond_19
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    .line 114
    const/16 v2, 0x2e

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    #@26
    move-result v0

    #@27
    .line 115
    .local v0, end:I
    if-lez v0, :cond_2f

    #@29
    .line 116
    add-int/lit8 v2, v0, 0x1

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    .line 119
    .end local v0           #end:I
    :cond_2f
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    .line 120
    const/16 v2, 0x7b

    #@34
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@37
    .line 121
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@3a
    move-result v2

    #@3b
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    goto :goto_8
.end method

.method public static isObjectSelected(Ljava/lang/Object;)Z
    .registers 15
    .parameter "object"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 66
    const/4 v4, 0x0

    #@2
    .line 67
    .local v4, match:Z
    const-string v10, "ANDROID_OBJECT_FILTER"

    #@4
    invoke-static {v10}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v7

    #@8
    .line 68
    .local v7, s:Ljava/lang/String;
    if-eqz v7, :cond_a0

    #@a
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@d
    move-result v10

    #@e
    if-lez v10, :cond_a0

    #@10
    .line 69
    const-string v10, "@"

    #@12
    invoke-virtual {v7, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@15
    move-result-object v8

    #@16
    .line 71
    .local v8, selectors:[Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@19
    move-result-object v10

    #@1a
    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@1d
    move-result-object v10

    #@1e
    aget-object v11, v8, v11

    #@20
    invoke-virtual {v10, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@23
    move-result v10

    #@24
    if-eqz v10, :cond_a0

    #@26
    .line 73
    const/4 v2, 0x1

    #@27
    .local v2, i:I
    :goto_27
    array-length v10, v8

    #@28
    if-ge v2, v10, :cond_a0

    #@2a
    .line 74
    aget-object v10, v8, v2

    #@2c
    const-string v11, "="

    #@2e
    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    .line 75
    .local v5, pair:[Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@35
    move-result-object v3

    #@36
    .line 77
    .local v3, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    #@37
    .line 78
    .local v0, declaredMethod:Ljava/lang/reflect/Method;
    move-object v6, v3

    #@38
    .line 80
    .local v6, parent:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_38
    :try_start_38
    new-instance v10, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v11, "get"

    #@3f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v10

    #@43
    const/4 v11, 0x0

    #@44
    aget-object v11, v5, v11

    #@46
    const/4 v12, 0x0

    #@47
    const/4 v13, 0x1

    #@48
    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4b
    move-result-object v11

    #@4c
    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@4f
    move-result-object v11

    #@50
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v10

    #@54
    const/4 v11, 0x0

    #@55
    aget-object v11, v5, v11

    #@57
    const/4 v12, 0x1

    #@58
    invoke-virtual {v11, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@5b
    move-result-object v11

    #@5c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v10

    #@60
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v11

    #@64
    const/4 v10, 0x0

    #@65
    check-cast v10, [Ljava/lang/Class;

    #@67
    invoke-virtual {v6, v11, v10}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@6a
    move-result-object v0

    #@6b
    .line 85
    invoke-virtual {v3}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    #@6e
    move-result-object v6

    #@6f
    if-eqz v6, :cond_73

    #@71
    if-eqz v0, :cond_38

    #@73
    .line 87
    :cond_73
    if-eqz v0, :cond_8a

    #@75
    .line 88
    const/4 v10, 0x0

    #@76
    check-cast v10, [Ljava/lang/Object;

    #@78
    invoke-virtual {v0, p0, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@7b
    move-result-object v9

    #@7c
    .line 90
    .local v9, value:Ljava/lang/Object;
    if-eqz v9, :cond_8d

    #@7e
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@81
    move-result-object v10

    #@82
    :goto_82
    const/4 v11, 0x1

    #@83
    aget-object v11, v5, v11

    #@85
    invoke-virtual {v10, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@88
    move-result v10

    #@89
    or-int/2addr v4, v10

    #@8a
    .line 73
    .end local v9           #value:Ljava/lang/Object;
    :cond_8a
    :goto_8a
    add-int/lit8 v2, v2, 0x1

    #@8c
    goto :goto_27

    #@8d
    .line 90
    .restart local v9       #value:Ljava/lang/Object;
    :cond_8d
    const-string/jumbo v10, "null"
    :try_end_90
    .catch Ljava/lang/NoSuchMethodException; {:try_start_38 .. :try_end_90} :catch_91
    .catch Ljava/lang/IllegalAccessException; {:try_start_38 .. :try_end_90} :catch_96
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_38 .. :try_end_90} :catch_9b

    #@90
    goto :goto_82

    #@91
    .line 93
    .end local v9           #value:Ljava/lang/Object;
    :catch_91
    move-exception v1

    #@92
    .line 94
    .local v1, e:Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    #@95
    goto :goto_8a

    #@96
    .line 95
    .end local v1           #e:Ljava/lang/NoSuchMethodException;
    :catch_96
    move-exception v1

    #@97
    .line 96
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@9a
    goto :goto_8a

    #@9b
    .line 97
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_9b
    move-exception v1

    #@9c
    .line 98
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@9f
    goto :goto_8a

    #@a0
    .line 103
    .end local v0           #declaredMethod:Ljava/lang/reflect/Method;
    .end local v1           #e:Ljava/lang/reflect/InvocationTargetException;
    .end local v2           #i:I
    .end local v3           #klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v5           #pair:[Ljava/lang/String;
    .end local v6           #parent:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v8           #selectors:[Ljava/lang/String;
    :cond_a0
    return v4
.end method
