.class public final Landroid/util/EventLog$Event;
.super Ljava/lang/Object;
.source "EventLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/util/EventLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Event"
.end annotation


# static fields
.field private static final DATA_START:I = 0x18

.field private static final INT_TYPE:B = 0x0t

.field private static final LENGTH_OFFSET:I = 0x0

.field private static final LIST_TYPE:B = 0x3t

.field private static final LONG_TYPE:B = 0x1t

.field private static final NANOSECONDS_OFFSET:I = 0x10

.field private static final PAYLOAD_START:I = 0x14

.field private static final PROCESS_OFFSET:I = 0x4

.field private static final SECONDS_OFFSET:I = 0xc

.field private static final STRING_TYPE:B = 0x2t

.field private static final TAG_OFFSET:I = 0x14

.field private static final THREAD_OFFSET:I = 0x8


# instance fields
.field private final mBuffer:Ljava/nio/ByteBuffer;


# direct methods
.method constructor <init>([B)V
    .registers 4
    .parameter "data"

    #@0
    .prologue
    .line 77
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 78
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@9
    .line 79
    iget-object v0, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@b
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@12
    .line 80
    return-void
.end method

.method private decodeObject()Ljava/lang/Object;
    .registers 10

    #@0
    .prologue
    .line 120
    iget-object v6, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@2
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->get()B

    #@5
    move-result v5

    #@6
    .line 121
    .local v5, type:B
    packed-switch v5, :pswitch_data_7c

    #@9
    .line 147
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v7, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v8, "Unknown entry type: "

    #@12
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v7

    #@16
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v6

    #@22
    .line 123
    :pswitch_22
    iget-object v6, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@24
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    #@27
    move-result v6

    #@28
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v0

    #@2c
    .line 144
    :cond_2c
    :goto_2c
    return-object v0

    #@2d
    .line 126
    :pswitch_2d
    iget-object v6, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@2f
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getLong()J

    #@32
    move-result-wide v6

    #@33
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@36
    move-result-object v0

    #@37
    goto :goto_2c

    #@38
    .line 130
    :pswitch_38
    :try_start_38
    iget-object v6, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@3a
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    #@3d
    move-result v3

    #@3e
    .line 131
    .local v3, length:I
    iget-object v6, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@40
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->position()I

    #@43
    move-result v4

    #@44
    .line 132
    .local v4, start:I
    iget-object v6, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@46
    add-int v7, v4, v3

    #@48
    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    #@4b
    .line 133
    new-instance v0, Ljava/lang/String;

    #@4d
    iget-object v6, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@4f
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    #@52
    move-result-object v6

    #@53
    const-string v7, "UTF-8"

    #@55
    invoke-direct {v0, v6, v4, v3, v7}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_58
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_38 .. :try_end_58} :catch_59

    #@58
    goto :goto_2c

    #@59
    .line 134
    .end local v3           #length:I
    .end local v4           #start:I
    :catch_59
    move-exception v1

    #@5a
    .line 135
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    const-string v6, "EventLog"

    #@5c
    const-string v7, "UTF-8 is not supported"

    #@5e
    invoke-static {v6, v7, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@61
    .line 136
    const/4 v0, 0x0

    #@62
    goto :goto_2c

    #@63
    .line 140
    .end local v1           #e:Ljava/io/UnsupportedEncodingException;
    :pswitch_63
    iget-object v6, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@65
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->get()B

    #@68
    move-result v3

    #@69
    .line 141
    .restart local v3       #length:I
    if-gez v3, :cond_6d

    #@6b
    add-int/lit16 v3, v3, 0x100

    #@6d
    .line 142
    :cond_6d
    new-array v0, v3, [Ljava/lang/Object;

    #@6f
    .line 143
    .local v0, array:[Ljava/lang/Object;
    const/4 v2, 0x0

    #@70
    .local v2, i:I
    :goto_70
    if-ge v2, v3, :cond_2c

    #@72
    invoke-direct {p0}, Landroid/util/EventLog$Event;->decodeObject()Ljava/lang/Object;

    #@75
    move-result-object v6

    #@76
    aput-object v6, v0, v2

    #@78
    add-int/lit8 v2, v2, 0x1

    #@7a
    goto :goto_70

    #@7b
    .line 121
    nop

    #@7c
    :pswitch_data_7c
    .packed-switch 0x0
        :pswitch_22
        :pswitch_2d
        :pswitch_38
        :pswitch_63
    .end packed-switch
.end method


# virtual methods
.method public declared-synchronized getData()Ljava/lang/Object;
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 106
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v2, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@4
    iget-object v3, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@6
    const/4 v4, 0x0

    #@7
    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getShort(I)S

    #@a
    move-result v3

    #@b
    add-int/lit8 v3, v3, 0x14

    #@d
    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    #@10
    .line 107
    iget-object v2, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@12
    const/16 v3, 0x18

    #@14
    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    #@17
    .line 108
    invoke-direct {p0}, Landroid/util/EventLog$Event;->decodeObject()Ljava/lang/Object;
    :try_end_1a
    .catchall {:try_start_2 .. :try_end_1a} :catchall_3b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_1a} :catch_1d
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2 .. :try_end_1a} :catch_3e

    #@1a
    move-result-object v1

    #@1b
    .line 114
    :goto_1b
    monitor-exit p0

    #@1c
    return-object v1

    #@1d
    .line 109
    :catch_1d
    move-exception v0

    #@1e
    .line 110
    .local v0, e:Ljava/lang/IllegalArgumentException;
    :try_start_1e
    const-string v2, "EventLog"

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "Illegal entry payload: tag="

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {p0}, Landroid/util/EventLog$Event;->getTag()I

    #@2e
    move-result v4

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v2, v3, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3a
    .catchall {:try_start_1e .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_1b

    #@3b
    .line 106
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    :catchall_3b
    move-exception v1

    #@3c
    monitor-exit p0

    #@3d
    throw v1

    #@3e
    .line 112
    :catch_3e
    move-exception v0

    #@3f
    .line 113
    .local v0, e:Ljava/nio/BufferUnderflowException;
    :try_start_3f
    const-string v2, "EventLog"

    #@41
    new-instance v3, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v4, "Truncated entry payload: tag="

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {p0}, Landroid/util/EventLog$Event;->getTag()I

    #@4f
    move-result v4

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    invoke-static {v2, v3, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5b
    .catchall {:try_start_3f .. :try_end_5b} :catchall_3b

    #@5b
    goto :goto_1b
.end method

.method public getProcessId()I
    .registers 3

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@2
    const/4 v1, 0x4

    #@3
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getTag()I
    .registers 3

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@2
    const/16 v1, 0x14

    #@4
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getThreadId()I
    .registers 3

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@2
    const/16 v1, 0x8

    #@4
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getTimeNanos()J
    .registers 5

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@2
    const/16 v1, 0xc

    #@4
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    #@7
    move-result v0

    #@8
    int-to-long v0, v0

    #@9
    const-wide/32 v2, 0x3b9aca00

    #@c
    mul-long/2addr v0, v2

    #@d
    iget-object v2, p0, Landroid/util/EventLog$Event;->mBuffer:Ljava/nio/ByteBuffer;

    #@f
    const/16 v3, 0x10

    #@11
    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    #@14
    move-result v2

    #@15
    int-to-long v2, v2

    #@16
    add-long/2addr v0, v2

    #@17
    return-wide v0
.end method
