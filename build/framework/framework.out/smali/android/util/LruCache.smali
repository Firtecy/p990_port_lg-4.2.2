.class public Landroid/util/LruCache;
.super Ljava/lang/Object;
.source "LruCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private createCount:I

.field private evictionCount:I

.field private hitCount:I

.field private final map:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private maxSize:I

.field private missCount:I

.field private putCount:I

.field private size:I


# direct methods
.method public constructor <init>(I)V
    .registers 6
    .parameter "maxSize"

    #@0
    .prologue
    .line 80
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 81
    if-gtz p1, :cond_e

    #@5
    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string/jumbo v1, "maxSize <= 0"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 84
    :cond_e
    iput p1, p0, Landroid/util/LruCache;->maxSize:I

    #@10
    .line 85
    new-instance v0, Ljava/util/LinkedHashMap;

    #@12
    const/4 v1, 0x0

    #@13
    const/high16 v2, 0x3f40

    #@15
    const/4 v3, 0x1

    #@16
    invoke-direct {v0, v1, v2, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    #@19
    iput-object v0, p0, Landroid/util/LruCache;->map:Ljava/util/LinkedHashMap;

    #@1b
    .line 86
    return-void
.end method

.method private safeSizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)I"
        }
    .end annotation

    #@0
    .prologue
    .line 287
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    .local p2, value:Ljava/lang/Object;,"TV;"
    invoke-virtual {p0, p1, p2}, Landroid/util/LruCache;->sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I

    #@3
    move-result v0

    #@4
    .line 288
    .local v0, result:I
    if-gez v0, :cond_29

    #@6
    .line 289
    new-instance v1, Ljava/lang/IllegalStateException;

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Negative size: "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, "="

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@28
    throw v1

    #@29
    .line 291
    :cond_29
    return v0
.end method


# virtual methods
.method protected create(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    #@0
    .prologue
    .line 283
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public final declared-synchronized createCount()I
    .registers 2

    #@0
    .prologue
    .line 350
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/util/LruCache;->createCount:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method protected entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5
    .parameter "evicted"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTK;TV;TV;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 265
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    .local p2, key:Ljava/lang/Object;,"TK;"
    .local p3, oldValue:Ljava/lang/Object;,"TV;"
    .local p4, newValue:Ljava/lang/Object;,"TV;"
    return-void
.end method

.method public final evictAll()V
    .registers 2

    #@0
    .prologue
    .line 309
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/util/LruCache;->trimToSize(I)V

    #@4
    .line 310
    return-void
.end method

.method public final declared-synchronized evictionCount()I
    .registers 2

    #@0
    .prologue
    .line 364
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/util/LruCache;->evictionCount:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    #@0
    .prologue
    .line 112
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    if-nez p1, :cond_b

    #@2
    .line 113
    new-instance v2, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v3, "key == null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 117
    :cond_b
    monitor-enter p0

    #@c
    .line 118
    :try_start_c
    iget-object v2, p0, Landroid/util/LruCache;->map:Ljava/util/LinkedHashMap;

    #@e
    invoke-virtual {v2, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    .line 119
    .local v1, mapValue:Ljava/lang/Object;,"TV;"
    if-eqz v1, :cond_1d

    #@14
    .line 120
    iget v2, p0, Landroid/util/LruCache;->hitCount:I

    #@16
    add-int/lit8 v2, v2, 0x1

    #@18
    iput v2, p0, Landroid/util/LruCache;->hitCount:I

    #@1a
    .line 121
    monitor-exit p0

    #@1b
    move-object v0, v1

    #@1c
    .line 155
    :goto_1c
    return-object v0

    #@1d
    .line 123
    :cond_1d
    iget v2, p0, Landroid/util/LruCache;->missCount:I

    #@1f
    add-int/lit8 v2, v2, 0x1

    #@21
    iput v2, p0, Landroid/util/LruCache;->missCount:I

    #@23
    .line 124
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_c .. :try_end_24} :catchall_2c

    #@24
    .line 133
    invoke-virtual {p0, p1}, Landroid/util/LruCache;->create(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    .line 134
    .local v0, createdValue:Ljava/lang/Object;,"TV;"
    if-nez v0, :cond_2f

    #@2a
    .line 135
    const/4 v0, 0x0

    #@2b
    goto :goto_1c

    #@2c
    .line 124
    .end local v0           #createdValue:Ljava/lang/Object;,"TV;"
    .end local v1           #mapValue:Ljava/lang/Object;,"TV;"
    :catchall_2c
    move-exception v2

    #@2d
    :try_start_2d
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_2c

    #@2e
    throw v2

    #@2f
    .line 138
    .restart local v0       #createdValue:Ljava/lang/Object;,"TV;"
    .restart local v1       #mapValue:Ljava/lang/Object;,"TV;"
    :cond_2f
    monitor-enter p0

    #@30
    .line 139
    :try_start_30
    iget v2, p0, Landroid/util/LruCache;->createCount:I

    #@32
    add-int/lit8 v2, v2, 0x1

    #@34
    iput v2, p0, Landroid/util/LruCache;->createCount:I

    #@36
    .line 140
    iget-object v2, p0, Landroid/util/LruCache;->map:Ljava/util/LinkedHashMap;

    #@38
    invoke-virtual {v2, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    move-result-object v1

    #@3c
    .line 142
    if-eqz v1, :cond_4c

    #@3e
    .line 144
    iget-object v2, p0, Landroid/util/LruCache;->map:Ljava/util/LinkedHashMap;

    #@40
    invoke-virtual {v2, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@43
    .line 148
    :goto_43
    monitor-exit p0
    :try_end_44
    .catchall {:try_start_30 .. :try_end_44} :catchall_56

    #@44
    .line 150
    if-eqz v1, :cond_59

    #@46
    .line 151
    const/4 v2, 0x0

    #@47
    invoke-virtual {p0, v2, p1, v0, v1}, Landroid/util/LruCache;->entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    #@4a
    move-object v0, v1

    #@4b
    .line 152
    goto :goto_1c

    #@4c
    .line 146
    :cond_4c
    :try_start_4c
    iget v2, p0, Landroid/util/LruCache;->size:I

    #@4e
    invoke-direct {p0, p1, v0}, Landroid/util/LruCache;->safeSizeOf(Ljava/lang/Object;Ljava/lang/Object;)I

    #@51
    move-result v3

    #@52
    add-int/2addr v2, v3

    #@53
    iput v2, p0, Landroid/util/LruCache;->size:I

    #@55
    goto :goto_43

    #@56
    .line 148
    :catchall_56
    move-exception v2

    #@57
    monitor-exit p0
    :try_end_58
    .catchall {:try_start_4c .. :try_end_58} :catchall_56

    #@58
    throw v2

    #@59
    .line 154
    :cond_59
    iget v2, p0, Landroid/util/LruCache;->maxSize:I

    #@5b
    invoke-virtual {p0, v2}, Landroid/util/LruCache;->trimToSize(I)V

    #@5e
    goto :goto_1c
.end method

.method public final declared-synchronized hitCount()I
    .registers 2

    #@0
    .prologue
    .line 335
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/util/LruCache;->hitCount:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public final declared-synchronized maxSize()I
    .registers 2

    #@0
    .prologue
    .line 327
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/util/LruCache;->maxSize:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public final declared-synchronized missCount()I
    .registers 2

    #@0
    .prologue
    .line 343
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/util/LruCache;->missCount:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    #@0
    .prologue
    .line 166
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    .local p2, value:Ljava/lang/Object;,"TV;"
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_d

    #@4
    .line 167
    :cond_4
    new-instance v1, Ljava/lang/NullPointerException;

    #@6
    const-string/jumbo v2, "key == null || value == null"

    #@9
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1

    #@d
    .line 171
    :cond_d
    monitor-enter p0

    #@e
    .line 172
    :try_start_e
    iget v1, p0, Landroid/util/LruCache;->putCount:I

    #@10
    add-int/lit8 v1, v1, 0x1

    #@12
    iput v1, p0, Landroid/util/LruCache;->putCount:I

    #@14
    .line 173
    iget v1, p0, Landroid/util/LruCache;->size:I

    #@16
    invoke-direct {p0, p1, p2}, Landroid/util/LruCache;->safeSizeOf(Ljava/lang/Object;Ljava/lang/Object;)I

    #@19
    move-result v2

    #@1a
    add-int/2addr v1, v2

    #@1b
    iput v1, p0, Landroid/util/LruCache;->size:I

    #@1d
    .line 174
    iget-object v1, p0, Landroid/util/LruCache;->map:Ljava/util/LinkedHashMap;

    #@1f
    invoke-virtual {v1, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    .line 175
    .local v0, previous:Ljava/lang/Object;,"TV;"
    if-eqz v0, :cond_2e

    #@25
    .line 176
    iget v1, p0, Landroid/util/LruCache;->size:I

    #@27
    invoke-direct {p0, p1, v0}, Landroid/util/LruCache;->safeSizeOf(Ljava/lang/Object;Ljava/lang/Object;)I

    #@2a
    move-result v2

    #@2b
    sub-int/2addr v1, v2

    #@2c
    iput v1, p0, Landroid/util/LruCache;->size:I

    #@2e
    .line 178
    :cond_2e
    monitor-exit p0
    :try_end_2f
    .catchall {:try_start_e .. :try_end_2f} :catchall_3b

    #@2f
    .line 180
    if-eqz v0, :cond_35

    #@31
    .line 181
    const/4 v1, 0x0

    #@32
    invoke-virtual {p0, v1, p1, v0, p2}, Landroid/util/LruCache;->entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    #@35
    .line 184
    :cond_35
    iget v1, p0, Landroid/util/LruCache;->maxSize:I

    #@37
    invoke-virtual {p0, v1}, Landroid/util/LruCache;->trimToSize(I)V

    #@3a
    .line 185
    return-object v0

    #@3b
    .line 178
    .end local v0           #previous:Ljava/lang/Object;,"TV;"
    :catchall_3b
    move-exception v1

    #@3c
    :try_start_3c
    monitor-exit p0
    :try_end_3d
    .catchall {:try_start_3c .. :try_end_3d} :catchall_3b

    #@3d
    throw v1
.end method

.method public final declared-synchronized putCount()I
    .registers 2

    #@0
    .prologue
    .line 357
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/util/LruCache;->putCount:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    #@0
    .prologue
    .line 231
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    if-nez p1, :cond_b

    #@2
    .line 232
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v2, "key == null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 236
    :cond_b
    monitor-enter p0

    #@c
    .line 237
    :try_start_c
    iget-object v1, p0, Landroid/util/LruCache;->map:Ljava/util/LinkedHashMap;

    #@e
    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    .line 238
    .local v0, previous:Ljava/lang/Object;,"TV;"
    if-eqz v0, :cond_1d

    #@14
    .line 239
    iget v1, p0, Landroid/util/LruCache;->size:I

    #@16
    invoke-direct {p0, p1, v0}, Landroid/util/LruCache;->safeSizeOf(Ljava/lang/Object;Ljava/lang/Object;)I

    #@19
    move-result v2

    #@1a
    sub-int/2addr v1, v2

    #@1b
    iput v1, p0, Landroid/util/LruCache;->size:I

    #@1d
    .line 241
    :cond_1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_c .. :try_end_1e} :catchall_26

    #@1e
    .line 243
    if-eqz v0, :cond_25

    #@20
    .line 244
    const/4 v1, 0x0

    #@21
    const/4 v2, 0x0

    #@22
    invoke-virtual {p0, v1, p1, v0, v2}, Landroid/util/LruCache;->entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    #@25
    .line 247
    :cond_25
    return-object v0

    #@26
    .line 241
    .end local v0           #previous:Ljava/lang/Object;,"TV;"
    :catchall_26
    move-exception v1

    #@27
    :try_start_27
    monitor-exit p0
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_26

    #@28
    throw v1
.end method

.method public resize(I)V
    .registers 4
    .parameter "maxSize"

    #@0
    .prologue
    .line 95
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    if-gtz p1, :cond_b

    #@2
    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "maxSize <= 0"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 99
    :cond_b
    monitor-enter p0

    #@c
    .line 100
    :try_start_c
    iput p1, p0, Landroid/util/LruCache;->maxSize:I

    #@e
    .line 101
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_13

    #@f
    .line 102
    invoke-virtual {p0, p1}, Landroid/util/LruCache;->trimToSize(I)V

    #@12
    .line 103
    return-void

    #@13
    .line 101
    :catchall_13
    move-exception v0

    #@14
    :try_start_14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public final declared-synchronized size()I
    .registers 2

    #@0
    .prologue
    .line 318
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/util/LruCache;->size:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method protected sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)I"
        }
    .end annotation

    #@0
    .prologue
    .line 302
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    .local p2, value:Ljava/lang/Object;,"TV;"
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public final declared-synchronized snapshot()Ljava/util/Map;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 372
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Ljava/util/LinkedHashMap;

    #@3
    iget-object v1, p0, Landroid/util/LruCache;->map:Ljava/util/LinkedHashMap;

    #@5
    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    #@8
    monitor-exit p0

    #@9
    return-object v0

    #@a
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method public final declared-synchronized toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    const/4 v1, 0x0

    #@1
    .line 376
    monitor-enter p0

    #@2
    :try_start_2
    iget v2, p0, Landroid/util/LruCache;->hitCount:I

    #@4
    iget v3, p0, Landroid/util/LruCache;->missCount:I

    #@6
    add-int v0, v2, v3

    #@8
    .line 377
    .local v0, accesses:I
    if-eqz v0, :cond_10

    #@a
    iget v2, p0, Landroid/util/LruCache;->hitCount:I

    #@c
    mul-int/lit8 v2, v2, 0x64

    #@e
    div-int v1, v2, v0

    #@10
    .line 378
    .local v1, hitPercent:I
    :cond_10
    const-string v2, "LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]"

    #@12
    const/4 v3, 0x4

    #@13
    new-array v3, v3, [Ljava/lang/Object;

    #@15
    const/4 v4, 0x0

    #@16
    iget v5, p0, Landroid/util/LruCache;->maxSize:I

    #@18
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v5

    #@1c
    aput-object v5, v3, v4

    #@1e
    const/4 v4, 0x1

    #@1f
    iget v5, p0, Landroid/util/LruCache;->hitCount:I

    #@21
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v5

    #@25
    aput-object v5, v3, v4

    #@27
    const/4 v4, 0x2

    #@28
    iget v5, p0, Landroid/util/LruCache;->missCount:I

    #@2a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d
    move-result-object v5

    #@2e
    aput-object v5, v3, v4

    #@30
    const/4 v4, 0x3

    #@31
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v5

    #@35
    aput-object v5, v3, v4

    #@37
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_3a
    .catchall {:try_start_2 .. :try_end_3a} :catchall_3d

    #@3a
    move-result-object v2

    #@3b
    monitor-exit p0

    #@3c
    return-object v2

    #@3d
    .line 376
    .end local v0           #accesses:I
    .end local v1           #hitPercent:I
    :catchall_3d
    move-exception v2

    #@3e
    monitor-exit p0

    #@3f
    throw v2
.end method

.method public trimToSize(I)V
    .registers 8
    .parameter "maxSize"

    #@0
    .prologue
    .line 199
    .local p0, this:Landroid/util/LruCache;,"Landroid/util/LruCache<TK;TV;>;"
    :goto_0
    monitor-enter p0

    #@1
    .line 200
    :try_start_1
    iget v3, p0, Landroid/util/LruCache;->size:I

    #@3
    if-ltz v3, :cond_11

    #@5
    iget-object v3, p0, Landroid/util/LruCache;->map:Ljava/util/LinkedHashMap;

    #@7
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->isEmpty()Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_35

    #@d
    iget v3, p0, Landroid/util/LruCache;->size:I

    #@f
    if-eqz v3, :cond_35

    #@11
    .line 201
    :cond_11
    new-instance v3, Ljava/lang/IllegalStateException;

    #@13
    new-instance v4, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    const-string v5, ".sizeOf() is reporting inconsistent results!"

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@31
    throw v3

    #@32
    .line 219
    :catchall_32
    move-exception v3

    #@33
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_1 .. :try_end_34} :catchall_32

    #@34
    throw v3

    #@35
    .line 205
    :cond_35
    :try_start_35
    iget v3, p0, Landroid/util/LruCache;->size:I

    #@37
    if-gt v3, p1, :cond_3b

    #@39
    .line 206
    monitor-exit p0

    #@3a
    .line 223
    :goto_3a
    return-void

    #@3b
    .line 209
    :cond_3b
    iget-object v3, p0, Landroid/util/LruCache;->map:Ljava/util/LinkedHashMap;

    #@3d
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    #@40
    move-result-object v1

    #@41
    .line 210
    .local v1, toEvict:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<TK;TV;>;"
    if-nez v1, :cond_45

    #@43
    .line 211
    monitor-exit p0

    #@44
    goto :goto_3a

    #@45
    .line 214
    :cond_45
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@48
    move-result-object v0

    #@49
    .line 215
    .local v0, key:Ljava/lang/Object;,"TK;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@4c
    move-result-object v2

    #@4d
    .line 216
    .local v2, value:Ljava/lang/Object;,"TV;"
    iget-object v3, p0, Landroid/util/LruCache;->map:Ljava/util/LinkedHashMap;

    #@4f
    invoke-virtual {v3, v0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@52
    .line 217
    iget v3, p0, Landroid/util/LruCache;->size:I

    #@54
    invoke-direct {p0, v0, v2}, Landroid/util/LruCache;->safeSizeOf(Ljava/lang/Object;Ljava/lang/Object;)I

    #@57
    move-result v4

    #@58
    sub-int/2addr v3, v4

    #@59
    iput v3, p0, Landroid/util/LruCache;->size:I

    #@5b
    .line 218
    iget v3, p0, Landroid/util/LruCache;->evictionCount:I

    #@5d
    add-int/lit8 v3, v3, 0x1

    #@5f
    iput v3, p0, Landroid/util/LruCache;->evictionCount:I

    #@61
    .line 219
    monitor-exit p0
    :try_end_62
    .catchall {:try_start_35 .. :try_end_62} :catchall_32

    #@62
    .line 221
    const/4 v3, 0x1

    #@63
    const/4 v4, 0x0

    #@64
    invoke-virtual {p0, v3, v0, v2, v4}, Landroid/util/LruCache;->entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    #@67
    goto :goto_0
.end method
