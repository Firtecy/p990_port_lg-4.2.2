.class Landroid/util/ReflectiveProperty;
.super Landroid/util/Property;
.source "ReflectiveProperty.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/util/Property",
        "<TT;TV;>;"
    }
.end annotation


# static fields
.field private static final PREFIX_GET:Ljava/lang/String; = "get"

.field private static final PREFIX_IS:Ljava/lang/String; = "is"

.field private static final PREFIX_SET:Ljava/lang/String; = "set"


# instance fields
.field private mField:Ljava/lang/reflect/Field;

.field private mGetter:Ljava/lang/reflect/Method;

.field private mSetter:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)V
    .registers 19
    .parameter
    .parameter
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 47
    .local p0, this:Landroid/util/ReflectiveProperty;,"Landroid/util/ReflectiveProperty<TT;TV;>;"
    .local p1, propertyHolder:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    .local p2, valueType:Ljava/lang/Class;,"Ljava/lang/Class<TV;>;"
    move-object/from16 v0, p2

    #@2
    move-object/from16 v1, p3

    #@4
    invoke-direct {p0, v0, v1}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    #@7
    .line 48
    const/4 v12, 0x0

    #@8
    move-object/from16 v0, p3

    #@a
    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    #@d
    move-result v12

    #@e
    invoke-static {v12}, Ljava/lang/Character;->toUpperCase(C)C

    #@11
    move-result v7

    #@12
    .line 49
    .local v7, firstLetter:C
    const/4 v12, 0x1

    #@13
    move-object/from16 v0, p3

    #@15
    invoke-virtual {v0, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@18
    move-result-object v11

    #@19
    .line 50
    .local v11, theRest:Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@21
    move-result-object v12

    #@22
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v12

    #@26
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    .line 51
    .local v2, capitalizedName:Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v13, "get"

    #@31
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v12

    #@35
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v12

    #@39
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v8

    #@3d
    .line 53
    .local v8, getterName:Ljava/lang/String;
    const/4 v12, 0x0

    #@3e
    :try_start_3e
    check-cast v12, [Ljava/lang/Class;

    #@40
    move-object/from16 v0, p1

    #@42
    invoke-virtual {v0, v8, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@45
    move-result-object v12

    #@46
    iput-object v12, p0, Landroid/util/ReflectiveProperty;->mGetter:Ljava/lang/reflect/Method;
    :try_end_48
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3e .. :try_end_48} :catch_87

    #@48
    .line 76
    :goto_48
    iget-object v12, p0, Landroid/util/ReflectiveProperty;->mGetter:Ljava/lang/reflect/Method;

    #@4a
    invoke-virtual {v12}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    #@4d
    move-result-object v9

    #@4e
    .line 78
    .local v9, getterType:Ljava/lang/Class;
    move-object/from16 v0, p2

    #@50
    invoke-direct {p0, v0, v9}, Landroid/util/ReflectiveProperty;->typesMatch(Ljava/lang/Class;Ljava/lang/Class;)Z

    #@53
    move-result v12

    #@54
    if-nez v12, :cond_10d

    #@56
    .line 79
    new-instance v12, Landroid/util/NoSuchPropertyException;

    #@58
    new-instance v13, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v14, "Underlying type ("

    #@5f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v13

    #@63
    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v13

    #@67
    const-string v14, ") "

    #@69
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v13

    #@6d
    const-string v14, "does not match Property type ("

    #@6f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v13

    #@73
    move-object/from16 v0, p2

    #@75
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v13

    #@79
    const-string v14, ")"

    #@7b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v13

    #@7f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v13

    #@83
    invoke-direct {v12, v13}, Landroid/util/NoSuchPropertyException;-><init>(Ljava/lang/String;)V

    #@86
    throw v12

    #@87
    .line 54
    .end local v9           #getterType:Ljava/lang/Class;
    :catch_87
    move-exception v3

    #@88
    .line 56
    .local v3, e:Ljava/lang/NoSuchMethodException;
    new-instance v12, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v13, "is"

    #@8f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v12

    #@93
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v12

    #@97
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v8

    #@9b
    .line 58
    const/4 v12, 0x0

    #@9c
    :try_start_9c
    check-cast v12, [Ljava/lang/Class;

    #@9e
    move-object/from16 v0, p1

    #@a0
    invoke-virtual {v0, v8, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@a3
    move-result-object v12

    #@a4
    iput-object v12, p0, Landroid/util/ReflectiveProperty;->mGetter:Ljava/lang/reflect/Method;
    :try_end_a6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_9c .. :try_end_a6} :catch_a7

    #@a6
    goto :goto_48

    #@a7
    .line 59
    :catch_a7
    move-exception v4

    #@a8
    .line 62
    .local v4, e1:Ljava/lang/NoSuchMethodException;
    :try_start_a8
    move-object/from16 v0, p1

    #@aa
    move-object/from16 v1, p3

    #@ac
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@af
    move-result-object v12

    #@b0
    iput-object v12, p0, Landroid/util/ReflectiveProperty;->mField:Ljava/lang/reflect/Field;

    #@b2
    .line 63
    iget-object v12, p0, Landroid/util/ReflectiveProperty;->mField:Ljava/lang/reflect/Field;

    #@b4
    invoke-virtual {v12}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    #@b7
    move-result-object v6

    #@b8
    .line 64
    .local v6, fieldType:Ljava/lang/Class;
    move-object/from16 v0, p2

    #@ba
    invoke-direct {p0, v0, v6}, Landroid/util/ReflectiveProperty;->typesMatch(Ljava/lang/Class;Ljava/lang/Class;)Z

    #@bd
    move-result v12

    #@be
    if-nez v12, :cond_12f

    #@c0
    .line 65
    new-instance v12, Landroid/util/NoSuchPropertyException;

    #@c2
    new-instance v13, Ljava/lang/StringBuilder;

    #@c4
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@c7
    const-string v14, "Underlying type ("

    #@c9
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v13

    #@cd
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v13

    #@d1
    const-string v14, ") "

    #@d3
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v13

    #@d7
    const-string v14, "does not match Property type ("

    #@d9
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v13

    #@dd
    move-object/from16 v0, p2

    #@df
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v13

    #@e3
    const-string v14, ")"

    #@e5
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v13

    #@e9
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v13

    #@ed
    invoke-direct {v12, v13}, Landroid/util/NoSuchPropertyException;-><init>(Ljava/lang/String;)V

    #@f0
    throw v12
    :try_end_f1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_a8 .. :try_end_f1} :catch_f1

    #@f1
    .line 69
    .end local v6           #fieldType:Ljava/lang/Class;
    :catch_f1
    move-exception v5

    #@f2
    .line 71
    .local v5, e2:Ljava/lang/NoSuchFieldException;
    new-instance v12, Landroid/util/NoSuchPropertyException;

    #@f4
    new-instance v13, Ljava/lang/StringBuilder;

    #@f6
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@f9
    const-string v14, "No accessor method or field found for property with name "

    #@fb
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v13

    #@ff
    move-object/from16 v0, p3

    #@101
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v13

    #@105
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@108
    move-result-object v13

    #@109
    invoke-direct {v12, v13}, Landroid/util/NoSuchPropertyException;-><init>(Ljava/lang/String;)V

    #@10c
    throw v12

    #@10d
    .line 82
    .end local v3           #e:Ljava/lang/NoSuchMethodException;
    .end local v4           #e1:Ljava/lang/NoSuchMethodException;
    .end local v5           #e2:Ljava/lang/NoSuchFieldException;
    .restart local v9       #getterType:Ljava/lang/Class;
    :cond_10d
    new-instance v12, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    const-string/jumbo v13, "set"

    #@115
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v12

    #@119
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v12

    #@11d
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v10

    #@121
    .line 84
    .local v10, setterName:Ljava/lang/String;
    const/4 v12, 0x1

    #@122
    :try_start_122
    new-array v12, v12, [Ljava/lang/Class;

    #@124
    const/4 v13, 0x0

    #@125
    aput-object v9, v12, v13

    #@127
    move-object/from16 v0, p1

    #@129
    invoke-virtual {v0, v10, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@12c
    move-result-object v12

    #@12d
    iput-object v12, p0, Landroid/util/ReflectiveProperty;->mSetter:Ljava/lang/reflect/Method;
    :try_end_12f
    .catch Ljava/lang/NoSuchMethodException; {:try_start_122 .. :try_end_12f} :catch_130

    #@12f
    .line 88
    .end local v9           #getterType:Ljava/lang/Class;
    .end local v10           #setterName:Ljava/lang/String;
    :cond_12f
    :goto_12f
    return-void

    #@130
    .line 85
    .restart local v9       #getterType:Ljava/lang/Class;
    .restart local v10       #setterName:Ljava/lang/String;
    :catch_130
    move-exception v12

    #@131
    goto :goto_12f
.end method

.method private typesMatch(Ljava/lang/Class;Ljava/lang/Class;)Z
    .registers 6
    .parameter
    .parameter "getterType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TV;>;",
            "Ljava/lang/Class;",
            ")Z"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/util/ReflectiveProperty;,"Landroid/util/ReflectiveProperty<TT;TV;>;"
    .local p1, valueType:Ljava/lang/Class;,"Ljava/lang/Class<TV;>;"
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 98
    if-eq p2, p1, :cond_4c

    #@4
    .line 99
    invoke-virtual {p2}, Ljava/lang/Class;->isPrimitive()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_4b

    #@a
    .line 100
    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@c
    if-ne p2, v2, :cond_12

    #@e
    const-class v2, Ljava/lang/Float;

    #@10
    if-eq p1, v2, :cond_4a

    #@12
    :cond_12
    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@14
    if-ne p2, v2, :cond_1a

    #@16
    const-class v2, Ljava/lang/Integer;

    #@18
    if-eq p1, v2, :cond_4a

    #@1a
    :cond_1a
    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@1c
    if-ne p2, v2, :cond_22

    #@1e
    const-class v2, Ljava/lang/Boolean;

    #@20
    if-eq p1, v2, :cond_4a

    #@22
    :cond_22
    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    #@24
    if-ne p2, v2, :cond_2a

    #@26
    const-class v2, Ljava/lang/Long;

    #@28
    if-eq p1, v2, :cond_4a

    #@2a
    :cond_2a
    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    #@2c
    if-ne p2, v2, :cond_32

    #@2e
    const-class v2, Ljava/lang/Double;

    #@30
    if-eq p1, v2, :cond_4a

    #@32
    :cond_32
    sget-object v2, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    #@34
    if-ne p2, v2, :cond_3a

    #@36
    const-class v2, Ljava/lang/Short;

    #@38
    if-eq p1, v2, :cond_4a

    #@3a
    :cond_3a
    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    #@3c
    if-ne p2, v2, :cond_42

    #@3e
    const-class v2, Ljava/lang/Byte;

    #@40
    if-eq p1, v2, :cond_4a

    #@42
    :cond_42
    sget-object v2, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    #@44
    if-ne p2, v2, :cond_4b

    #@46
    const-class v2, Ljava/lang/Character;

    #@48
    if-ne p1, v2, :cond_4b

    #@4a
    :cond_4a
    move v0, v1

    #@4b
    .line 111
    :cond_4b
    :goto_4b
    return v0

    #@4c
    :cond_4c
    move v0, v1

    #@4d
    goto :goto_4b
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TV;"
        }
    .end annotation

    #@0
    .prologue
    .line 137
    .local p0, this:Landroid/util/ReflectiveProperty;,"Landroid/util/ReflectiveProperty<TT;TV;>;"
    .local p1, object:Ljava/lang/Object;,"TT;"
    iget-object v1, p0, Landroid/util/ReflectiveProperty;->mGetter:Ljava/lang/reflect/Method;

    #@2
    if-eqz v1, :cond_20

    #@4
    .line 139
    :try_start_4
    iget-object v2, p0, Landroid/util/ReflectiveProperty;->mGetter:Ljava/lang/reflect/Method;

    #@6
    const/4 v1, 0x0

    #@7
    check-cast v1, [Ljava/lang/Object;

    #@9
    invoke-virtual {v2, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_c
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_c} :catch_e
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_c} :catch_15

    #@c
    move-result-object v1

    #@d
    .line 147
    :goto_d
    return-object v1

    #@e
    .line 140
    :catch_e
    move-exception v0

    #@f
    .line 141
    .local v0, e:Ljava/lang/IllegalAccessException;
    new-instance v1, Ljava/lang/AssertionError;

    #@11
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@14
    throw v1

    #@15
    .line 142
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :catch_15
    move-exception v0

    #@16
    .line 143
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@18
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@1b
    move-result-object v2

    #@1c
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@1f
    throw v1

    #@20
    .line 145
    .end local v0           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_20
    iget-object v1, p0, Landroid/util/ReflectiveProperty;->mField:Ljava/lang/reflect/Field;

    #@22
    if-eqz v1, :cond_32

    #@24
    .line 147
    :try_start_24
    iget-object v1, p0, Landroid/util/ReflectiveProperty;->mField:Ljava/lang/reflect/Field;

    #@26
    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_29
    .catch Ljava/lang/IllegalAccessException; {:try_start_24 .. :try_end_29} :catch_2b

    #@29
    move-result-object v1

    #@2a
    goto :goto_d

    #@2b
    .line 148
    :catch_2b
    move-exception v0

    #@2c
    .line 149
    .local v0, e:Ljava/lang/IllegalAccessException;
    new-instance v1, Ljava/lang/AssertionError;

    #@2e
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@31
    throw v1

    #@32
    .line 153
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :cond_32
    new-instance v1, Ljava/lang/AssertionError;

    #@34
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@37
    throw v1
.end method

.method public isReadOnly()Z
    .registers 2

    #@0
    .prologue
    .line 161
    .local p0, this:Landroid/util/ReflectiveProperty;,"Landroid/util/ReflectiveProperty<TT;TV;>;"
    iget-object v0, p0, Landroid/util/ReflectiveProperty;->mSetter:Ljava/lang/reflect/Method;

    #@2
    if-nez v0, :cond_a

    #@4
    iget-object v0, p0, Landroid/util/ReflectiveProperty;->mField:Ljava/lang/reflect/Field;

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public set(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TV;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 116
    .local p0, this:Landroid/util/ReflectiveProperty;,"Landroid/util/ReflectiveProperty<TT;TV;>;"
    .local p1, object:Ljava/lang/Object;,"TT;"
    .local p2, value:Ljava/lang/Object;,"TV;"
    iget-object v1, p0, Landroid/util/ReflectiveProperty;->mSetter:Ljava/lang/reflect/Method;

    #@2
    if-eqz v1, :cond_22

    #@4
    .line 118
    :try_start_4
    iget-object v1, p0, Landroid/util/ReflectiveProperty;->mSetter:Ljava/lang/reflect/Method;

    #@6
    const/4 v2, 0x1

    #@7
    new-array v2, v2, [Ljava/lang/Object;

    #@9
    const/4 v3, 0x0

    #@a
    aput-object p2, v2, v3

    #@c
    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_f
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_f} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_f} :catch_17

    #@f
    .line 133
    :goto_f
    return-void

    #@10
    .line 119
    :catch_10
    move-exception v0

    #@11
    .line 120
    .local v0, e:Ljava/lang/IllegalAccessException;
    new-instance v1, Ljava/lang/AssertionError;

    #@13
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@16
    throw v1

    #@17
    .line 121
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :catch_17
    move-exception v0

    #@18
    .line 122
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@1a
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@21
    throw v1

    #@22
    .line 124
    .end local v0           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_22
    iget-object v1, p0, Landroid/util/ReflectiveProperty;->mField:Ljava/lang/reflect/Field;

    #@24
    if-eqz v1, :cond_33

    #@26
    .line 126
    :try_start_26
    iget-object v1, p0, Landroid/util/ReflectiveProperty;->mField:Ljava/lang/reflect/Field;

    #@28
    invoke-virtual {v1, p1, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2b
    .catch Ljava/lang/IllegalAccessException; {:try_start_26 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_f

    #@2c
    .line 127
    :catch_2c
    move-exception v0

    #@2d
    .line 128
    .local v0, e:Ljava/lang/IllegalAccessException;
    new-instance v1, Ljava/lang/AssertionError;

    #@2f
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@32
    throw v1

    #@33
    .line 131
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :cond_33
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    #@35
    new-instance v2, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v3, "Property "

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {p0}, Landroid/util/ReflectiveProperty;->getName()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    const-string v3, " is read-only"

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@55
    throw v1
.end method
