.class public final Landroid/util/JsonWriter;
.super Ljava/lang/Object;
.source "JsonWriter.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/util/JsonWriter$1;
    }
.end annotation


# instance fields
.field private indent:Ljava/lang/String;

.field private lenient:Z

.field private final out:Ljava/io/Writer;

.field private separator:Ljava/lang/String;

.field private final stack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/JsonScope;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .registers 4
    .parameter "out"

    #@0
    .prologue
    .line 148
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 125
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@a
    .line 127
    iget-object v0, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@c
    sget-object v1, Landroid/util/JsonScope;->EMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@11
    .line 139
    const-string v0, ":"

    #@13
    iput-object v0, p0, Landroid/util/JsonWriter;->separator:Ljava/lang/String;

    #@15
    .line 149
    if-nez p1, :cond_20

    #@17
    .line 150
    new-instance v0, Ljava/lang/NullPointerException;

    #@19
    const-string/jumbo v1, "out == null"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 152
    :cond_20
    iput-object p1, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@22
    .line 153
    return-void
.end method

.method private beforeName()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 477
    invoke-direct {p0}, Landroid/util/JsonWriter;->peek()Landroid/util/JsonScope;

    #@3
    move-result-object v0

    #@4
    .line 478
    .local v0, context:Landroid/util/JsonScope;
    sget-object v1, Landroid/util/JsonScope;->NONEMPTY_OBJECT:Landroid/util/JsonScope;

    #@6
    if-ne v0, v1, :cond_18

    #@8
    .line 479
    iget-object v1, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@a
    const/16 v2, 0x2c

    #@c
    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(I)V

    #@f
    .line 483
    :cond_f
    invoke-direct {p0}, Landroid/util/JsonWriter;->newline()V

    #@12
    .line 484
    sget-object v1, Landroid/util/JsonScope;->DANGLING_NAME:Landroid/util/JsonScope;

    #@14
    invoke-direct {p0, v1}, Landroid/util/JsonWriter;->replaceTop(Landroid/util/JsonScope;)V

    #@17
    .line 485
    return-void

    #@18
    .line 480
    :cond_18
    sget-object v1, Landroid/util/JsonScope;->EMPTY_OBJECT:Landroid/util/JsonScope;

    #@1a
    if-eq v0, v1, :cond_f

    #@1c
    .line 481
    new-instance v1, Ljava/lang/IllegalStateException;

    #@1e
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "Nesting problem: "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    iget-object v3, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@36
    throw v1
.end method

.method private beforeValue(Z)V
    .registers 5
    .parameter "root"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 496
    sget-object v0, Landroid/util/JsonWriter$1;->$SwitchMap$android$util$JsonScope:[I

    #@2
    invoke-direct {p0}, Landroid/util/JsonWriter;->peek()Landroid/util/JsonScope;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/util/JsonScope;->ordinal()I

    #@9
    move-result v1

    #@a
    aget v0, v0, v1

    #@c
    packed-switch v0, :pswitch_data_68

    #@f
    .line 525
    new-instance v0, Ljava/lang/IllegalStateException;

    #@11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "Nesting problem: "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    iget-object v2, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0

    #@2a
    .line 498
    :pswitch_2a
    iget-boolean v0, p0, Landroid/util/JsonWriter;->lenient:Z

    #@2c
    if-nez v0, :cond_38

    #@2e
    if-nez p1, :cond_38

    #@30
    .line 499
    new-instance v0, Ljava/lang/IllegalStateException;

    #@32
    const-string v1, "JSON must start with an array or an object."

    #@34
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@37
    throw v0

    #@38
    .line 502
    :cond_38
    sget-object v0, Landroid/util/JsonScope;->NONEMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@3a
    invoke-direct {p0, v0}, Landroid/util/JsonWriter;->replaceTop(Landroid/util/JsonScope;)V

    #@3d
    .line 527
    :goto_3d
    return-void

    #@3e
    .line 506
    :pswitch_3e
    sget-object v0, Landroid/util/JsonScope;->NONEMPTY_ARRAY:Landroid/util/JsonScope;

    #@40
    invoke-direct {p0, v0}, Landroid/util/JsonWriter;->replaceTop(Landroid/util/JsonScope;)V

    #@43
    .line 507
    invoke-direct {p0}, Landroid/util/JsonWriter;->newline()V

    #@46
    goto :goto_3d

    #@47
    .line 511
    :pswitch_47
    iget-object v0, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@49
    const/16 v1, 0x2c

    #@4b
    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    #@4e
    .line 512
    invoke-direct {p0}, Landroid/util/JsonWriter;->newline()V

    #@51
    goto :goto_3d

    #@52
    .line 516
    :pswitch_52
    iget-object v0, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@54
    iget-object v1, p0, Landroid/util/JsonWriter;->separator:Ljava/lang/String;

    #@56
    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    #@59
    .line 517
    sget-object v0, Landroid/util/JsonScope;->NONEMPTY_OBJECT:Landroid/util/JsonScope;

    #@5b
    invoke-direct {p0, v0}, Landroid/util/JsonWriter;->replaceTop(Landroid/util/JsonScope;)V

    #@5e
    goto :goto_3d

    #@5f
    .line 521
    :pswitch_5f
    new-instance v0, Ljava/lang/IllegalStateException;

    #@61
    const-string v1, "JSON must have only one top-level value."

    #@63
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@66
    throw v0

    #@67
    .line 496
    nop

    #@68
    :pswitch_data_68
    .packed-switch 0x1
        :pswitch_2a
        :pswitch_3e
        :pswitch_47
        :pswitch_52
        :pswitch_5f
    .end packed-switch
.end method

.method private close(Landroid/util/JsonScope;Landroid/util/JsonScope;Ljava/lang/String;)Landroid/util/JsonWriter;
    .registers 8
    .parameter "empty"
    .parameter "nonempty"
    .parameter "closeBracket"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 251
    invoke-direct {p0}, Landroid/util/JsonWriter;->peek()Landroid/util/JsonScope;

    #@3
    move-result-object v0

    #@4
    .line 252
    .local v0, context:Landroid/util/JsonScope;
    if-eq v0, p2, :cond_23

    #@6
    if-eq v0, p1, :cond_23

    #@8
    .line 253
    new-instance v1, Ljava/lang/IllegalStateException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Nesting problem: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    iget-object v3, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@22
    throw v1

    #@23
    .line 256
    :cond_23
    iget-object v1, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@25
    iget-object v2, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@27
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@2a
    move-result v2

    #@2b
    add-int/lit8 v2, v2, -0x1

    #@2d
    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@30
    .line 257
    if-ne v0, p2, :cond_35

    #@32
    .line 258
    invoke-direct {p0}, Landroid/util/JsonWriter;->newline()V

    #@35
    .line 260
    :cond_35
    iget-object v1, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@37
    invoke-virtual {v1, p3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@3a
    .line 261
    return-object p0
.end method

.method private newline()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 462
    iget-object v1, p0, Landroid/util/JsonWriter;->indent:Ljava/lang/String;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 470
    :cond_4
    return-void

    #@5
    .line 466
    :cond_5
    iget-object v1, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@7
    const-string v2, "\n"

    #@9
    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@c
    .line 467
    const/4 v0, 0x1

    #@d
    .local v0, i:I
    :goto_d
    iget-object v1, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@f
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@12
    move-result v1

    #@13
    if-ge v0, v1, :cond_4

    #@15
    .line 468
    iget-object v1, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@17
    iget-object v2, p0, Landroid/util/JsonWriter;->indent:Ljava/lang/String;

    #@19
    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@1c
    .line 467
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_d
.end method

.method private open(Landroid/util/JsonScope;Ljava/lang/String;)Landroid/util/JsonWriter;
    .registers 4
    .parameter "empty"
    .parameter "openBracket"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 239
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/util/JsonWriter;->beforeValue(Z)V

    #@4
    .line 240
    iget-object v0, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@6
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@9
    .line 241
    iget-object v0, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@b
    invoke-virtual {v0, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@e
    .line 242
    return-object p0
.end method

.method private peek()Landroid/util/JsonScope;
    .registers 3

    #@0
    .prologue
    .line 268
    iget-object v0, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@2
    iget-object v1, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@4
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@7
    move-result v1

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/util/JsonScope;

    #@10
    return-object v0
.end method

.method private replaceTop(Landroid/util/JsonScope;)V
    .registers 4
    .parameter "topOfStack"

    #@0
    .prologue
    .line 275
    iget-object v0, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@2
    iget-object v1, p0, Landroid/util/JsonWriter;->stack:Ljava/util/List;

    #@4
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@7
    move-result v1

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 276
    return-void
.end method

.method private string(Ljava/lang/String;)V
    .registers 11
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 401
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@4
    const-string v4, "\""

    #@6
    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@9
    .line 402
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@d
    move-result v2

    #@e
    .local v2, length:I
    :goto_e
    if-ge v1, v2, :cond_80

    #@10
    .line 403
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v0

    #@14
    .line 416
    .local v0, c:C
    sparse-switch v0, :sswitch_data_88

    #@17
    .line 449
    const/16 v3, 0x1f

    #@19
    if-gt v0, v3, :cond_7a

    #@1b
    .line 450
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@1d
    const-string v4, "\\u%04x"

    #@1f
    new-array v5, v8, [Ljava/lang/Object;

    #@21
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v6

    #@25
    aput-object v6, v5, v7

    #@27
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@2e
    .line 402
    :goto_2e
    add-int/lit8 v1, v1, 0x1

    #@30
    goto :goto_e

    #@31
    .line 419
    :sswitch_31
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@33
    const/16 v4, 0x5c

    #@35
    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(I)V

    #@38
    .line 420
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@3a
    invoke-virtual {v3, v0}, Ljava/io/Writer;->write(I)V

    #@3d
    goto :goto_2e

    #@3e
    .line 424
    :sswitch_3e
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@40
    const-string v4, "\\t"

    #@42
    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@45
    goto :goto_2e

    #@46
    .line 428
    :sswitch_46
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@48
    const-string v4, "\\b"

    #@4a
    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@4d
    goto :goto_2e

    #@4e
    .line 432
    :sswitch_4e
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@50
    const-string v4, "\\n"

    #@52
    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@55
    goto :goto_2e

    #@56
    .line 436
    :sswitch_56
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@58
    const-string v4, "\\r"

    #@5a
    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@5d
    goto :goto_2e

    #@5e
    .line 440
    :sswitch_5e
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@60
    const-string v4, "\\f"

    #@62
    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@65
    goto :goto_2e

    #@66
    .line 445
    :sswitch_66
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@68
    const-string v4, "\\u%04x"

    #@6a
    new-array v5, v8, [Ljava/lang/Object;

    #@6c
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6f
    move-result-object v6

    #@70
    aput-object v6, v5, v7

    #@72
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@79
    goto :goto_2e

    #@7a
    .line 452
    :cond_7a
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@7c
    invoke-virtual {v3, v0}, Ljava/io/Writer;->write(I)V

    #@7f
    goto :goto_2e

    #@80
    .line 458
    .end local v0           #c:C
    :cond_80
    iget-object v3, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@82
    const-string v4, "\""

    #@84
    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@87
    .line 459
    return-void

    #@88
    .line 416
    :sswitch_data_88
    .sparse-switch
        0x8 -> :sswitch_46
        0x9 -> :sswitch_3e
        0xa -> :sswitch_4e
        0xc -> :sswitch_5e
        0xd -> :sswitch_56
        0x22 -> :sswitch_31
        0x5c -> :sswitch_31
        0x2028 -> :sswitch_66
        0x2029 -> :sswitch_66
    .end sparse-switch
.end method


# virtual methods
.method public beginArray()Landroid/util/JsonWriter;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 203
    sget-object v0, Landroid/util/JsonScope;->EMPTY_ARRAY:Landroid/util/JsonScope;

    #@2
    const-string v1, "["

    #@4
    invoke-direct {p0, v0, v1}, Landroid/util/JsonWriter;->open(Landroid/util/JsonScope;Ljava/lang/String;)Landroid/util/JsonWriter;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public beginObject()Landroid/util/JsonWriter;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 222
    sget-object v0, Landroid/util/JsonScope;->EMPTY_OBJECT:Landroid/util/JsonScope;

    #@2
    const-string/jumbo v1, "{"

    #@5
    invoke-direct {p0, v0, v1}, Landroid/util/JsonWriter;->open(Landroid/util/JsonScope;Ljava/lang/String;)Landroid/util/JsonWriter;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 393
    iget-object v0, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@2
    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    #@5
    .line 395
    invoke-direct {p0}, Landroid/util/JsonWriter;->peek()Landroid/util/JsonScope;

    #@8
    move-result-object v0

    #@9
    sget-object v1, Landroid/util/JsonScope;->NONEMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@b
    if-eq v0, v1, :cond_15

    #@d
    .line 396
    new-instance v0, Ljava/io/IOException;

    #@f
    const-string v1, "Incomplete document"

    #@11
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 398
    :cond_15
    return-void
.end method

.method public endArray()Landroid/util/JsonWriter;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 212
    sget-object v0, Landroid/util/JsonScope;->EMPTY_ARRAY:Landroid/util/JsonScope;

    #@2
    sget-object v1, Landroid/util/JsonScope;->NONEMPTY_ARRAY:Landroid/util/JsonScope;

    #@4
    const-string v2, "]"

    #@6
    invoke-direct {p0, v0, v1, v2}, Landroid/util/JsonWriter;->close(Landroid/util/JsonScope;Landroid/util/JsonScope;Ljava/lang/String;)Landroid/util/JsonWriter;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public endObject()Landroid/util/JsonWriter;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 231
    sget-object v0, Landroid/util/JsonScope;->EMPTY_OBJECT:Landroid/util/JsonScope;

    #@2
    sget-object v1, Landroid/util/JsonScope;->NONEMPTY_OBJECT:Landroid/util/JsonScope;

    #@4
    const-string/jumbo v2, "}"

    #@7
    invoke-direct {p0, v0, v1, v2}, Landroid/util/JsonWriter;->close(Landroid/util/JsonScope;Landroid/util/JsonScope;Ljava/lang/String;)Landroid/util/JsonWriter;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public flush()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 384
    iget-object v0, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@2
    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    #@5
    .line 385
    return-void
.end method

.method public isLenient()Z
    .registers 2

    #@0
    .prologue
    .line 193
    iget-boolean v0, p0, Landroid/util/JsonWriter;->lenient:Z

    #@2
    return v0
.end method

.method public name(Ljava/lang/String;)Landroid/util/JsonWriter;
    .registers 4
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 285
    if-nez p1, :cond_b

    #@2
    .line 286
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "name == null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 288
    :cond_b
    invoke-direct {p0}, Landroid/util/JsonWriter;->beforeName()V

    #@e
    .line 289
    invoke-direct {p0, p1}, Landroid/util/JsonWriter;->string(Ljava/lang/String;)V

    #@11
    .line 290
    return-object p0
.end method

.method public nullValue()Landroid/util/JsonWriter;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 314
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/util/JsonWriter;->beforeValue(Z)V

    #@4
    .line 315
    iget-object v0, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@6
    const-string/jumbo v1, "null"

    #@9
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@c
    .line 316
    return-object p0
.end method

.method public setIndent(Ljava/lang/String;)V
    .registers 3
    .parameter "indent"

    #@0
    .prologue
    .line 164
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 165
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/util/JsonWriter;->indent:Ljava/lang/String;

    #@9
    .line 166
    const-string v0, ":"

    #@b
    iput-object v0, p0, Landroid/util/JsonWriter;->separator:Ljava/lang/String;

    #@d
    .line 171
    :goto_d
    return-void

    #@e
    .line 168
    :cond_e
    iput-object p1, p0, Landroid/util/JsonWriter;->indent:Ljava/lang/String;

    #@10
    .line 169
    const-string v0, ": "

    #@12
    iput-object v0, p0, Landroid/util/JsonWriter;->separator:Ljava/lang/String;

    #@14
    goto :goto_d
.end method

.method public setLenient(Z)V
    .registers 2
    .parameter "lenient"

    #@0
    .prologue
    .line 186
    iput-boolean p1, p0, Landroid/util/JsonWriter;->lenient:Z

    #@2
    .line 187
    return-void
.end method

.method public value(D)Landroid/util/JsonWriter;
    .registers 6
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 338
    iget-boolean v0, p0, Landroid/util/JsonWriter;->lenient:Z

    #@2
    if-nez v0, :cond_29

    #@4
    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_10

    #@a
    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_29

    #@10
    .line 339
    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v2, "Numeric values must be finite, but was "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 341
    :cond_29
    const/4 v0, 0x0

    #@2a
    invoke-direct {p0, v0}, Landroid/util/JsonWriter;->beforeValue(Z)V

    #@2d
    .line 342
    iget-object v0, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@2f
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    #@36
    .line 343
    return-object p0
.end method

.method public value(J)Landroid/util/JsonWriter;
    .registers 5
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 352
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/util/JsonWriter;->beforeValue(Z)V

    #@4
    .line 353
    iget-object v0, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@6
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@d
    .line 354
    return-object p0
.end method

.method public value(Ljava/lang/Number;)Landroid/util/JsonWriter;
    .registers 6
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 365
    if-nez p1, :cond_7

    #@2
    .line 366
    invoke-virtual {p0}, Landroid/util/JsonWriter;->nullValue()Landroid/util/JsonWriter;

    #@5
    move-result-object p0

    #@6
    .line 376
    .end local p0
    :goto_6
    return-object p0

    #@7
    .line 369
    .restart local p0
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 370
    .local v0, string:Ljava/lang/String;
    iget-boolean v1, p0, Landroid/util/JsonWriter;->lenient:Z

    #@d
    if-nez v1, :cond_40

    #@f
    const-string v1, "-Infinity"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_27

    #@17
    const-string v1, "Infinity"

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v1

    #@1d
    if-nez v1, :cond_27

    #@1f
    const-string v1, "NaN"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v1

    #@25
    if-eqz v1, :cond_40

    #@27
    .line 372
    :cond_27
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "Numeric values must be finite, but was "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v1

    #@40
    .line 374
    :cond_40
    const/4 v1, 0x0

    #@41
    invoke-direct {p0, v1}, Landroid/util/JsonWriter;->beforeValue(Z)V

    #@44
    .line 375
    iget-object v1, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@46
    invoke-virtual {v1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    #@49
    goto :goto_6
.end method

.method public value(Ljava/lang/String;)Landroid/util/JsonWriter;
    .registers 3
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 300
    if-nez p1, :cond_7

    #@2
    .line 301
    invoke-virtual {p0}, Landroid/util/JsonWriter;->nullValue()Landroid/util/JsonWriter;

    #@5
    move-result-object p0

    #@6
    .line 305
    .end local p0
    :goto_6
    return-object p0

    #@7
    .line 303
    .restart local p0
    :cond_7
    const/4 v0, 0x0

    #@8
    invoke-direct {p0, v0}, Landroid/util/JsonWriter;->beforeValue(Z)V

    #@b
    .line 304
    invoke-direct {p0, p1}, Landroid/util/JsonWriter;->string(Ljava/lang/String;)V

    #@e
    goto :goto_6
.end method

.method public value(Z)Landroid/util/JsonWriter;
    .registers 4
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 325
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/util/JsonWriter;->beforeValue(Z)V

    #@4
    .line 326
    iget-object v1, p0, Landroid/util/JsonWriter;->out:Ljava/io/Writer;

    #@6
    if-eqz p1, :cond_f

    #@8
    const-string/jumbo v0, "true"

    #@b
    :goto_b
    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    #@e
    .line 327
    return-object p0

    #@f
    .line 326
    :cond_f
    const-string v0, "false"

    #@11
    goto :goto_b
.end method
