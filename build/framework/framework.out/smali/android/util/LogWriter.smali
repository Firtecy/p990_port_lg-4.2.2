.class public Landroid/util/LogWriter;
.super Ljava/io/Writer;
.source "LogWriter.java"


# instance fields
.field private final mBuffer:I

.field private mBuilder:Ljava/lang/StringBuilder;

.field private final mPriority:I

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .registers 5
    .parameter "priority"
    .parameter "tag"

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Ljava/io/Writer;-><init>()V

    #@3
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    const/16 v1, 0x80

    #@7
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@a
    iput-object v0, p0, Landroid/util/LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@c
    .line 41
    iput p1, p0, Landroid/util/LogWriter;->mPriority:I

    #@e
    .line 42
    iput-object p2, p0, Landroid/util/LogWriter;->mTag:Ljava/lang/String;

    #@10
    .line 43
    const/4 v0, 0x0

    #@11
    iput v0, p0, Landroid/util/LogWriter;->mBuffer:I

    #@13
    .line 44
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;I)V
    .registers 6
    .parameter "priority"
    .parameter "tag"
    .parameter "buffer"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Ljava/io/Writer;-><init>()V

    #@3
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    const/16 v1, 0x80

    #@7
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@a
    iput-object v0, p0, Landroid/util/LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@c
    .line 51
    iput p1, p0, Landroid/util/LogWriter;->mPriority:I

    #@e
    .line 52
    iput-object p2, p0, Landroid/util/LogWriter;->mTag:Ljava/lang/String;

    #@10
    .line 53
    iput p3, p0, Landroid/util/LogWriter;->mBuffer:I

    #@12
    .line 54
    return-void
.end method

.method private flushBuilder()V
    .registers 5

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Landroid/util/LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@5
    move-result v0

    #@6
    if-lez v0, :cond_23

    #@8
    .line 78
    iget v0, p0, Landroid/util/LogWriter;->mBuffer:I

    #@a
    iget v1, p0, Landroid/util/LogWriter;->mPriority:I

    #@c
    iget-object v2, p0, Landroid/util/LogWriter;->mTag:Ljava/lang/String;

    #@e
    iget-object v3, p0, Landroid/util/LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@10
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    invoke-static {v0, v1, v2, v3}, Landroid/util/Log;->println_native(IILjava/lang/String;Ljava/lang/String;)I

    #@17
    .line 79
    iget-object v0, p0, Landroid/util/LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@19
    const/4 v1, 0x0

    #@1a
    iget-object v2, p0, Landroid/util/LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    #@1f
    move-result v2

    #@20
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    #@23
    .line 81
    :cond_23
    return-void
.end method


# virtual methods
.method public close()V
    .registers 1

    #@0
    .prologue
    .line 57
    invoke-direct {p0}, Landroid/util/LogWriter;->flushBuilder()V

    #@3
    .line 58
    return-void
.end method

.method public flush()V
    .registers 1

    #@0
    .prologue
    .line 61
    invoke-direct {p0}, Landroid/util/LogWriter;->flushBuilder()V

    #@3
    .line 62
    return-void
.end method

.method public write([CII)V
    .registers 7
    .parameter "buf"
    .parameter "offset"
    .parameter "count"

    #@0
    .prologue
    .line 65
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    if-ge v1, p3, :cond_17

    #@3
    .line 66
    add-int v2, p2, v1

    #@5
    aget-char v0, p1, v2

    #@7
    .line 67
    .local v0, c:C
    const/16 v2, 0xa

    #@9
    if-ne v0, v2, :cond_11

    #@b
    .line 68
    invoke-direct {p0}, Landroid/util/LogWriter;->flushBuilder()V

    #@e
    .line 65
    :goto_e
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_1

    #@11
    .line 71
    :cond_11
    iget-object v2, p0, Landroid/util/LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    goto :goto_e

    #@17
    .line 74
    .end local v0           #c:C
    :cond_17
    return-void
.end method
