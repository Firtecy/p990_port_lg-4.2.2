.class public Landroid/util/NtpTrustedTime;
.super Ljava/lang/Object;
.source "NtpTrustedTime.java"

# interfaces
.implements Landroid/util/TrustedTime;


# static fields
.field private static final LOGD:Z = false

.field private static final TAG:Ljava/lang/String; = "NtpTrustedTime"

.field private static sSingleton:Landroid/util/NtpTrustedTime;


# instance fields
.field private mCachedNtpCertainty:J

.field private mCachedNtpElapsedRealtime:J

.field private mCachedNtpTime:J

.field private mHasCache:Z

.field private final mServer:Ljava/lang/String;

.field private final mTimeout:J


# direct methods
.method private constructor <init>(Ljava/lang/String;J)V
    .registers 4
    .parameter "server"
    .parameter "timeout"

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 48
    iput-object p1, p0, Landroid/util/NtpTrustedTime;->mServer:Ljava/lang/String;

    #@5
    .line 49
    iput-wide p2, p0, Landroid/util/NtpTrustedTime;->mTimeout:J

    #@7
    .line 50
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Landroid/util/NtpTrustedTime;
    .registers 12
    .parameter "context"

    #@0
    .prologue
    .line 53
    const-class v10, Landroid/util/NtpTrustedTime;

    #@2
    monitor-enter v10

    #@3
    :try_start_3
    sget-object v9, Landroid/util/NtpTrustedTime;->sSingleton:Landroid/util/NtpTrustedTime;

    #@5
    if-nez v9, :cond_36

    #@7
    .line 54
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a
    move-result-object v3

    #@b
    .line 55
    .local v3, res:Landroid/content/res/Resources;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e
    move-result-object v4

    #@f
    .line 57
    .local v4, resolver:Landroid/content/ContentResolver;
    const v9, 0x1040042

    #@12
    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 59
    .local v0, defaultServer:Ljava/lang/String;
    const v9, 0x10e0036

    #@19
    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getInteger(I)I

    #@1c
    move-result v9

    #@1d
    int-to-long v1, v9

    #@1e
    .line 62
    .local v1, defaultTimeout:J
    const-string/jumbo v9, "ntp_server"

    #@21
    invoke-static {v4, v9}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    .line 64
    .local v5, secureServer:Ljava/lang/String;
    const-string/jumbo v9, "ntp_timeout"

    #@28
    invoke-static {v4, v9, v1, v2}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@2b
    move-result-wide v7

    #@2c
    .line 67
    .local v7, timeout:J
    if-eqz v5, :cond_3a

    #@2e
    move-object v6, v5

    #@2f
    .line 68
    .local v6, server:Ljava/lang/String;
    :goto_2f
    new-instance v9, Landroid/util/NtpTrustedTime;

    #@31
    invoke-direct {v9, v6, v7, v8}, Landroid/util/NtpTrustedTime;-><init>(Ljava/lang/String;J)V

    #@34
    sput-object v9, Landroid/util/NtpTrustedTime;->sSingleton:Landroid/util/NtpTrustedTime;

    #@36
    .line 71
    .end local v0           #defaultServer:Ljava/lang/String;
    .end local v1           #defaultTimeout:J
    .end local v3           #res:Landroid/content/res/Resources;
    .end local v4           #resolver:Landroid/content/ContentResolver;
    .end local v5           #secureServer:Ljava/lang/String;
    .end local v6           #server:Ljava/lang/String;
    .end local v7           #timeout:J
    :cond_36
    sget-object v9, Landroid/util/NtpTrustedTime;->sSingleton:Landroid/util/NtpTrustedTime;
    :try_end_38
    .catchall {:try_start_3 .. :try_end_38} :catchall_3c

    #@38
    monitor-exit v10

    #@39
    return-object v9

    #@3a
    .restart local v0       #defaultServer:Ljava/lang/String;
    .restart local v1       #defaultTimeout:J
    .restart local v3       #res:Landroid/content/res/Resources;
    .restart local v4       #resolver:Landroid/content/ContentResolver;
    .restart local v5       #secureServer:Ljava/lang/String;
    .restart local v7       #timeout:J
    :cond_3a
    move-object v6, v0

    #@3b
    .line 67
    goto :goto_2f

    #@3c
    .line 53
    .end local v0           #defaultServer:Ljava/lang/String;
    .end local v1           #defaultTimeout:J
    .end local v3           #res:Landroid/content/res/Resources;
    .end local v4           #resolver:Landroid/content/ContentResolver;
    .end local v5           #secureServer:Ljava/lang/String;
    .end local v7           #timeout:J
    :catchall_3c
    move-exception v9

    #@3d
    monitor-exit v10

    #@3e
    throw v9
.end method


# virtual methods
.method public currentTimeMillis()J
    .registers 5

    #@0
    .prologue
    .line 119
    iget-boolean v0, p0, Landroid/util/NtpTrustedTime;->mHasCache:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 120
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Missing authoritative time source"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 126
    :cond_c
    iget-wide v0, p0, Landroid/util/NtpTrustedTime;->mCachedNtpTime:J

    #@e
    invoke-virtual {p0}, Landroid/util/NtpTrustedTime;->getCacheAge()J

    #@11
    move-result-wide v2

    #@12
    add-long/2addr v0, v2

    #@13
    return-wide v0
.end method

.method public forceRefresh()Z
    .registers 8

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 76
    iget-object v3, p0, Landroid/util/NtpTrustedTime;->mServer:Ljava/lang/String;

    #@4
    if-nez v3, :cond_7

    #@6
    .line 90
    :cond_6
    :goto_6
    return v1

    #@7
    .line 82
    :cond_7
    new-instance v0, Landroid/net/SntpClient;

    #@9
    invoke-direct {v0}, Landroid/net/SntpClient;-><init>()V

    #@c
    .line 83
    .local v0, client:Landroid/net/SntpClient;
    iget-object v3, p0, Landroid/util/NtpTrustedTime;->mServer:Ljava/lang/String;

    #@e
    iget-wide v4, p0, Landroid/util/NtpTrustedTime;->mTimeout:J

    #@10
    long-to-int v4, v4

    #@11
    invoke-virtual {v0, v3, v4}, Landroid/net/SntpClient;->requestTime(Ljava/lang/String;I)Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_6

    #@17
    .line 84
    iput-boolean v2, p0, Landroid/util/NtpTrustedTime;->mHasCache:Z

    #@19
    .line 85
    invoke-virtual {v0}, Landroid/net/SntpClient;->getNtpTime()J

    #@1c
    move-result-wide v3

    #@1d
    iput-wide v3, p0, Landroid/util/NtpTrustedTime;->mCachedNtpTime:J

    #@1f
    .line 86
    invoke-virtual {v0}, Landroid/net/SntpClient;->getNtpTimeReference()J

    #@22
    move-result-wide v3

    #@23
    iput-wide v3, p0, Landroid/util/NtpTrustedTime;->mCachedNtpElapsedRealtime:J

    #@25
    .line 87
    invoke-virtual {v0}, Landroid/net/SntpClient;->getRoundTripTime()J

    #@28
    move-result-wide v3

    #@29
    const-wide/16 v5, 0x2

    #@2b
    div-long/2addr v3, v5

    #@2c
    iput-wide v3, p0, Landroid/util/NtpTrustedTime;->mCachedNtpCertainty:J

    #@2e
    move v1, v2

    #@2f
    .line 88
    goto :goto_6
.end method

.method public getCacheAge()J
    .registers 5

    #@0
    .prologue
    .line 101
    iget-boolean v0, p0, Landroid/util/NtpTrustedTime;->mHasCache:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 102
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@7
    move-result-wide v0

    #@8
    iget-wide v2, p0, Landroid/util/NtpTrustedTime;->mCachedNtpElapsedRealtime:J

    #@a
    sub-long/2addr v0, v2

    #@b
    .line 104
    :goto_b
    return-wide v0

    #@c
    :cond_c
    const-wide v0, 0x7fffffffffffffffL

    #@11
    goto :goto_b
.end method

.method public getCacheCertainty()J
    .registers 3

    #@0
    .prologue
    .line 110
    iget-boolean v0, p0, Landroid/util/NtpTrustedTime;->mHasCache:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 111
    iget-wide v0, p0, Landroid/util/NtpTrustedTime;->mCachedNtpCertainty:J

    #@6
    .line 113
    :goto_6
    return-wide v0

    #@7
    :cond_7
    const-wide v0, 0x7fffffffffffffffL

    #@c
    goto :goto_6
.end method

.method public getCachedNtpTime()J
    .registers 3

    #@0
    .prologue
    .line 131
    iget-wide v0, p0, Landroid/util/NtpTrustedTime;->mCachedNtpTime:J

    #@2
    return-wide v0
.end method

.method public getCachedNtpTimeReference()J
    .registers 3

    #@0
    .prologue
    .line 135
    iget-wide v0, p0, Landroid/util/NtpTrustedTime;->mCachedNtpElapsedRealtime:J

    #@2
    return-wide v0
.end method

.method public hasCache()Z
    .registers 2

    #@0
    .prologue
    .line 96
    iget-boolean v0, p0, Landroid/util/NtpTrustedTime;->mHasCache:Z

    #@2
    return v0
.end method
