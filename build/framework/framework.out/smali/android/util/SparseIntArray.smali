.class public Landroid/util/SparseIntArray;
.super Ljava/lang/Object;
.source "SparseIntArray.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private mKeys:[I

.field private mSize:I

.field private mValues:[I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 36
    const/16 v0, 0xa

    #@2
    invoke-direct {p0, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    #@5
    .line 37
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "initialCapacity"

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    invoke-static {p1}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@6
    move-result p1

    #@7
    .line 47
    new-array v0, p1, [I

    #@9
    iput-object v0, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@b
    .line 48
    new-array v0, p1, [I

    #@d
    iput-object v0, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@f
    .line 49
    const/4 v0, 0x0

    #@10
    iput v0, p0, Landroid/util/SparseIntArray;->mSize:I

    #@12
    .line 50
    return-void
.end method

.method private static binarySearch([IIII)I
    .registers 9
    .parameter "a"
    .parameter "start"
    .parameter "len"
    .parameter "key"

    #@0
    .prologue
    .line 235
    add-int v1, p1, p2

    #@2
    .local v1, high:I
    add-int/lit8 v2, p1, -0x1

    #@4
    .line 237
    .local v2, low:I
    :goto_4
    sub-int v3, v1, v2

    #@6
    const/4 v4, 0x1

    #@7
    if-le v3, v4, :cond_15

    #@9
    .line 238
    add-int v3, v1, v2

    #@b
    div-int/lit8 v0, v3, 0x2

    #@d
    .line 240
    .local v0, guess:I
    aget v3, p0, v0

    #@f
    if-ge v3, p3, :cond_13

    #@11
    .line 241
    move v2, v0

    #@12
    goto :goto_4

    #@13
    .line 243
    :cond_13
    move v1, v0

    #@14
    goto :goto_4

    #@15
    .line 246
    .end local v0           #guess:I
    :cond_15
    add-int v3, p1, p2

    #@17
    if-ne v1, v3, :cond_1e

    #@19
    .line 247
    add-int v3, p1, p2

    #@1b
    xor-int/lit8 v1, v3, -0x1

    #@1d
    .line 251
    .end local v1           #high:I
    :cond_1d
    :goto_1d
    return v1

    #@1e
    .line 248
    .restart local v1       #high:I
    :cond_1e
    aget v3, p0, v1

    #@20
    if-eq v3, p3, :cond_1d

    #@22
    .line 251
    xor-int/lit8 v1, v1, -0x1

    #@24
    goto :goto_1d
.end method


# virtual methods
.method public append(II)V
    .registers 10
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 209
    iget v4, p0, Landroid/util/SparseIntArray;->mSize:I

    #@3
    if-eqz v4, :cond_13

    #@5
    iget-object v4, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@7
    iget v5, p0, Landroid/util/SparseIntArray;->mSize:I

    #@9
    add-int/lit8 v5, v5, -0x1

    #@b
    aget v4, v4, v5

    #@d
    if-gt p1, v4, :cond_13

    #@f
    .line 210
    invoke-virtual {p0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    #@12
    .line 232
    :goto_12
    return-void

    #@13
    .line 214
    :cond_13
    iget v3, p0, Landroid/util/SparseIntArray;->mSize:I

    #@15
    .line 215
    .local v3, pos:I
    iget-object v4, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@17
    array-length v4, v4

    #@18
    if-lt v3, v4, :cond_38

    #@1a
    .line 216
    add-int/lit8 v4, v3, 0x1

    #@1c
    invoke-static {v4}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@1f
    move-result v0

    #@20
    .line 218
    .local v0, n:I
    new-array v1, v0, [I

    #@22
    .line 219
    .local v1, nkeys:[I
    new-array v2, v0, [I

    #@24
    .line 222
    .local v2, nvalues:[I
    iget-object v4, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@26
    iget-object v5, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@28
    array-length v5, v5

    #@29
    invoke-static {v4, v6, v1, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2c
    .line 223
    iget-object v4, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@2e
    iget-object v5, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@30
    array-length v5, v5

    #@31
    invoke-static {v4, v6, v2, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@34
    .line 225
    iput-object v1, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@36
    .line 226
    iput-object v2, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@38
    .line 229
    .end local v0           #n:I
    .end local v1           #nkeys:[I
    .end local v2           #nvalues:[I
    :cond_38
    iget-object v4, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@3a
    aput p1, v4, v3

    #@3c
    .line 230
    iget-object v4, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@3e
    aput p2, v4, v3

    #@40
    .line 231
    add-int/lit8 v4, v3, 0x1

    #@42
    iput v4, p0, Landroid/util/SparseIntArray;->mSize:I

    #@44
    goto :goto_12
.end method

.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 201
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/util/SparseIntArray;->mSize:I

    #@3
    .line 202
    return-void
.end method

.method public clone()Landroid/util/SparseIntArray;
    .registers 4

    #@0
    .prologue
    .line 54
    const/4 v1, 0x0

    #@1
    .line 56
    .local v1, clone:Landroid/util/SparseIntArray;
    :try_start_1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    #@4
    move-result-object v2

    #@5
    move-object v0, v2

    #@6
    check-cast v0, Landroid/util/SparseIntArray;

    #@8
    move-object v1, v0

    #@9
    .line 57
    iget-object v2, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@b
    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, [I

    #@11
    iput-object v2, v1, Landroid/util/SparseIntArray;->mKeys:[I

    #@13
    .line 58
    iget-object v2, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@15
    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, [I

    #@1b
    iput-object v2, v1, Landroid/util/SparseIntArray;->mValues:[I
    :try_end_1d
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1d} :catch_1e

    #@1d
    .line 62
    :goto_1d
    return-object v1

    #@1e
    .line 59
    :catch_1e
    move-exception v2

    #@1f
    goto :goto_1d
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 26
    invoke-virtual {p0}, Landroid/util/SparseIntArray;->clone()Landroid/util/SparseIntArray;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public delete(I)V
    .registers 6
    .parameter "key"

    #@0
    .prologue
    .line 91
    iget-object v1, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@2
    const/4 v2, 0x0

    #@3
    iget v3, p0, Landroid/util/SparseIntArray;->mSize:I

    #@5
    invoke-static {v1, v2, v3, p1}, Landroid/util/SparseIntArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    .line 93
    .local v0, i:I
    if-ltz v0, :cond_e

    #@b
    .line 94
    invoke-virtual {p0, v0}, Landroid/util/SparseIntArray;->removeAt(I)V

    #@e
    .line 96
    :cond_e
    return-void
.end method

.method public get(I)I
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 70
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/util/SparseIntArray;->get(II)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public get(II)I
    .registers 7
    .parameter "key"
    .parameter "valueIfKeyNotFound"

    #@0
    .prologue
    .line 78
    iget-object v1, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@2
    const/4 v2, 0x0

    #@3
    iget v3, p0, Landroid/util/SparseIntArray;->mSize:I

    #@5
    invoke-static {v1, v2, v3, p1}, Landroid/util/SparseIntArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    .line 80
    .local v0, i:I
    if-gez v0, :cond_c

    #@b
    .line 83
    .end local p2
    :goto_b
    return p2

    #@c
    .restart local p2
    :cond_c
    iget-object v1, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@e
    aget p2, v1, v0

    #@10
    goto :goto_b
.end method

.method public indexOfKey(I)I
    .registers 5
    .parameter "key"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@2
    const/4 v1, 0x0

    #@3
    iget v2, p0, Landroid/util/SparseIntArray;->mSize:I

    #@5
    invoke-static {v0, v1, v2, p1}, Landroid/util/SparseIntArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public indexOfValue(I)I
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 190
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Landroid/util/SparseIntArray;->mSize:I

    #@3
    if-ge v0, v1, :cond_f

    #@5
    .line 191
    iget-object v1, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@7
    aget v1, v1, v0

    #@9
    if-ne v1, p1, :cond_c

    #@b
    .line 194
    .end local v0           #i:I
    :goto_b
    return v0

    #@c
    .line 190
    .restart local v0       #i:I
    :cond_c
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_1

    #@f
    .line 194
    :cond_f
    const/4 v0, -0x1

    #@10
    goto :goto_b
.end method

.method public keyAt(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 160
    iget-object v0, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@2
    aget v0, v0, p1

    #@4
    return v0
.end method

.method public put(II)V
    .registers 11
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 113
    iget-object v4, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@3
    iget v5, p0, Landroid/util/SparseIntArray;->mSize:I

    #@5
    invoke-static {v4, v6, v5, p1}, Landroid/util/SparseIntArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    .line 115
    .local v0, i:I
    if-ltz v0, :cond_10

    #@b
    .line 116
    iget-object v4, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@d
    aput p2, v4, v0

    #@f
    .line 144
    :goto_f
    return-void

    #@10
    .line 118
    :cond_10
    xor-int/lit8 v0, v0, -0x1

    #@12
    .line 120
    iget v4, p0, Landroid/util/SparseIntArray;->mSize:I

    #@14
    iget-object v5, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@16
    array-length v5, v5

    #@17
    if-lt v4, v5, :cond_39

    #@19
    .line 121
    iget v4, p0, Landroid/util/SparseIntArray;->mSize:I

    #@1b
    add-int/lit8 v4, v4, 0x1

    #@1d
    invoke-static {v4}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@20
    move-result v1

    #@21
    .line 123
    .local v1, n:I
    new-array v2, v1, [I

    #@23
    .line 124
    .local v2, nkeys:[I
    new-array v3, v1, [I

    #@25
    .line 127
    .local v3, nvalues:[I
    iget-object v4, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@27
    iget-object v5, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@29
    array-length v5, v5

    #@2a
    invoke-static {v4, v6, v2, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2d
    .line 128
    iget-object v4, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@2f
    iget-object v5, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@31
    array-length v5, v5

    #@32
    invoke-static {v4, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@35
    .line 130
    iput-object v2, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@37
    .line 131
    iput-object v3, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@39
    .line 134
    .end local v1           #n:I
    .end local v2           #nkeys:[I
    .end local v3           #nvalues:[I
    :cond_39
    iget v4, p0, Landroid/util/SparseIntArray;->mSize:I

    #@3b
    sub-int/2addr v4, v0

    #@3c
    if-eqz v4, :cond_56

    #@3e
    .line 136
    iget-object v4, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@40
    iget-object v5, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@42
    add-int/lit8 v6, v0, 0x1

    #@44
    iget v7, p0, Landroid/util/SparseIntArray;->mSize:I

    #@46
    sub-int/2addr v7, v0

    #@47
    invoke-static {v4, v0, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@4a
    .line 137
    iget-object v4, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@4c
    iget-object v5, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@4e
    add-int/lit8 v6, v0, 0x1

    #@50
    iget v7, p0, Landroid/util/SparseIntArray;->mSize:I

    #@52
    sub-int/2addr v7, v0

    #@53
    invoke-static {v4, v0, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@56
    .line 140
    :cond_56
    iget-object v4, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@58
    aput p1, v4, v0

    #@5a
    .line 141
    iget-object v4, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@5c
    aput p2, v4, v0

    #@5e
    .line 142
    iget v4, p0, Landroid/util/SparseIntArray;->mSize:I

    #@60
    add-int/lit8 v4, v4, 0x1

    #@62
    iput v4, p0, Landroid/util/SparseIntArray;->mSize:I

    #@64
    goto :goto_f
.end method

.method public removeAt(I)V
    .registers 7
    .parameter "index"

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@2
    add-int/lit8 v1, p1, 0x1

    #@4
    iget-object v2, p0, Landroid/util/SparseIntArray;->mKeys:[I

    #@6
    iget v3, p0, Landroid/util/SparseIntArray;->mSize:I

    #@8
    add-int/lit8 v4, p1, 0x1

    #@a
    sub-int/2addr v3, v4

    #@b
    invoke-static {v0, v1, v2, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@e
    .line 103
    iget-object v0, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@10
    add-int/lit8 v1, p1, 0x1

    #@12
    iget-object v2, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@14
    iget v3, p0, Landroid/util/SparseIntArray;->mSize:I

    #@16
    add-int/lit8 v4, p1, 0x1

    #@18
    sub-int/2addr v3, v4

    #@19
    invoke-static {v0, v1, v2, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1c
    .line 104
    iget v0, p0, Landroid/util/SparseIntArray;->mSize:I

    #@1e
    add-int/lit8 v0, v0, -0x1

    #@20
    iput v0, p0, Landroid/util/SparseIntArray;->mSize:I

    #@22
    .line 105
    return-void
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 151
    iget v0, p0, Landroid/util/SparseIntArray;->mSize:I

    #@2
    return v0
.end method

.method public valueAt(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Landroid/util/SparseIntArray;->mValues:[I

    #@2
    aget v0, v0, p1

    #@4
    return v0
.end method
