.class public final Landroid/util/CharsetUtils;
.super Ljava/lang/Object;
.source "CharsetUtils.java"


# static fields
.field private static final VENDOR_DOCOMO:Ljava/lang/String; = "docomo"

.field private static final VENDOR_KDDI:Ljava/lang/String; = "kddi"

.field private static final VENDOR_SOFTBANK:Ljava/lang/String; = "softbank"

.field private static final sVendorShiftJisMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 63
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Landroid/util/CharsetUtils;->sVendorShiftJisMap:Ljava/util/Map;

    #@7
    .line 67
    sget-object v0, Landroid/util/CharsetUtils;->sVendorShiftJisMap:Ljava/util/Map;

    #@9
    const-string v1, "docomo"

    #@b
    const-string v2, "docomo-shift_jis-2007"

    #@d
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    .line 68
    sget-object v0, Landroid/util/CharsetUtils;->sVendorShiftJisMap:Ljava/util/Map;

    #@12
    const-string/jumbo v1, "kddi"

    #@15
    const-string/jumbo v2, "kddi-shift_jis-2007"

    #@18
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 69
    sget-object v0, Landroid/util/CharsetUtils;->sVendorShiftJisMap:Ljava/util/Map;

    #@1d
    const-string/jumbo v1, "softbank"

    #@20
    const-string/jumbo v2, "softbank-shift_jis-2007"

    #@23
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@26
    .line 70
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    return-void
.end method

.method public static charsetForVendor(Ljava/lang/String;)Ljava/nio/charset/Charset;
    .registers 2
    .parameter "charsetName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/charset/UnsupportedCharsetException;,
            Ljava/nio/charset/IllegalCharsetNameException;
        }
    .end annotation

    #@0
    .prologue
    .line 164
    invoke-static {}, Landroid/util/CharsetUtils;->getDefaultVendor()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, v0}, Landroid/util/CharsetUtils;->charsetForVendor(Ljava/lang/String;Ljava/lang/String;)Ljava/nio/charset/Charset;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static charsetForVendor(Ljava/lang/String;Ljava/lang/String;)Ljava/nio/charset/Charset;
    .registers 3
    .parameter "charsetName"
    .parameter "vendor"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/charset/UnsupportedCharsetException;,
            Ljava/nio/charset/IllegalCharsetNameException;
        }
    .end annotation

    #@0
    .prologue
    .line 141
    invoke-static {p0, p1}, Landroid/util/CharsetUtils;->nameForVendor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object p0

    #@4
    .line 142
    invoke-static {p0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private static getDefaultVendor()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 196
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method private static isShiftJis(Ljava/lang/String;)Z
    .registers 4
    .parameter "charsetName"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 177
    if-nez p0, :cond_4

    #@3
    .line 185
    :cond_3
    :goto_3
    return v1

    #@4
    .line 180
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v0

    #@8
    .line 181
    .local v0, length:I
    const/4 v2, 0x4

    #@9
    if-eq v0, v2, :cond_f

    #@b
    const/16 v2, 0x9

    #@d
    if-ne v0, v2, :cond_3

    #@f
    .line 185
    :cond_f
    const-string/jumbo v2, "shift_jis"

    #@12
    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_2a

    #@18
    const-string/jumbo v2, "shift-jis"

    #@1b
    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1e
    move-result v2

    #@1f
    if-nez v2, :cond_2a

    #@21
    const-string/jumbo v2, "sjis"

    #@24
    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@27
    move-result v2

    #@28
    if-eqz v2, :cond_3

    #@2a
    :cond_2a
    const/4 v1, 0x1

    #@2b
    goto :goto_3
.end method

.method public static nameForDefaultVendor(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "charsetName"

    #@0
    .prologue
    .line 117
    invoke-static {}, Landroid/util/CharsetUtils;->getDefaultVendor()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, v0}, Landroid/util/CharsetUtils;->nameForVendor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static nameForVendor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "charsetName"
    .parameter "vendor"

    #@0
    .prologue
    .line 91
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_1d

    #@6
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_1d

    #@c
    .line 93
    invoke-static {p0}, Landroid/util/CharsetUtils;->isShiftJis(Ljava/lang/String;)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_1d

    #@12
    .line 94
    sget-object v1, Landroid/util/CharsetUtils;->sVendorShiftJisMap:Ljava/util/Map;

    #@14
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Ljava/lang/String;

    #@1a
    .line 95
    .local v0, vendorShiftJis:Ljava/lang/String;
    if-eqz v0, :cond_1d

    #@1c
    .line 101
    .end local v0           #vendorShiftJis:Ljava/lang/String;
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    move-object v0, p0

    #@1e
    goto :goto_1c
.end method
