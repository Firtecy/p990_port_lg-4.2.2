.class public Landroid/util/TypedValue;
.super Ljava/lang/Object;
.source "TypedValue.java"


# static fields
.field public static final COMPLEX_MANTISSA_MASK:I = 0xffffff

.field public static final COMPLEX_MANTISSA_SHIFT:I = 0x8

.field public static final COMPLEX_RADIX_0p23:I = 0x3

.field public static final COMPLEX_RADIX_16p7:I = 0x1

.field public static final COMPLEX_RADIX_23p0:I = 0x0

.field public static final COMPLEX_RADIX_8p15:I = 0x2

.field public static final COMPLEX_RADIX_MASK:I = 0x3

.field public static final COMPLEX_RADIX_SHIFT:I = 0x4

.field public static final COMPLEX_UNIT_DIP:I = 0x1

.field public static final COMPLEX_UNIT_FRACTION:I = 0x0

.field public static final COMPLEX_UNIT_FRACTION_PARENT:I = 0x1

.field public static final COMPLEX_UNIT_IN:I = 0x4

.field public static final COMPLEX_UNIT_MASK:I = 0xf

.field public static final COMPLEX_UNIT_MM:I = 0x5

.field public static final COMPLEX_UNIT_PT:I = 0x3

.field public static final COMPLEX_UNIT_PX:I = 0x0

.field public static final COMPLEX_UNIT_SHIFT:I = 0x0

.field public static final COMPLEX_UNIT_SP:I = 0x2

.field public static final DENSITY_DEFAULT:I = 0x0

.field public static final DENSITY_NONE:I = 0xffff

.field private static final DIMENSION_UNIT_STRS:[Ljava/lang/String; = null

.field private static final FRACTION_UNIT_STRS:[Ljava/lang/String; = null

.field private static final MANTISSA_MULT:F = 0.00390625f

.field private static final RADIX_MULTS:[F = null

.field public static final TYPE_ATTRIBUTE:I = 0x2

.field public static final TYPE_DIMENSION:I = 0x5

.field public static final TYPE_FIRST_COLOR_INT:I = 0x1c

.field public static final TYPE_FIRST_INT:I = 0x10

.field public static final TYPE_FLOAT:I = 0x4

.field public static final TYPE_FRACTION:I = 0x6

.field public static final TYPE_INT_BOOLEAN:I = 0x12

.field public static final TYPE_INT_COLOR_ARGB4:I = 0x1e

.field public static final TYPE_INT_COLOR_ARGB8:I = 0x1c

.field public static final TYPE_INT_COLOR_RGB4:I = 0x1f

.field public static final TYPE_INT_COLOR_RGB8:I = 0x1d

.field public static final TYPE_INT_DEC:I = 0x10

.field public static final TYPE_INT_HEX:I = 0x11

.field public static final TYPE_LAST_COLOR_INT:I = 0x1f

.field public static final TYPE_LAST_INT:I = 0x1f

.field public static final TYPE_NULL:I = 0x0

.field public static final TYPE_REFERENCE:I = 0x1

.field public static final TYPE_STRING:I = 0x3


# instance fields
.field public assetCookie:I

.field public changingConfigurations:I

.field public data:I

.field public density:I

.field public resourceId:I

.field public string:Ljava/lang/CharSequence;

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 191
    new-array v0, v6, [F

    #@6
    fill-array-data v0, :array_3c

    #@9
    sput-object v0, Landroid/util/TypedValue;->RADIX_MULTS:[F

    #@b
    .line 417
    const/4 v0, 0x6

    #@c
    new-array v0, v0, [Ljava/lang/String;

    #@e
    const-string/jumbo v1, "px"

    #@11
    aput-object v1, v0, v3

    #@13
    const-string v1, "dip"

    #@15
    aput-object v1, v0, v4

    #@17
    const-string/jumbo v1, "sp"

    #@1a
    aput-object v1, v0, v5

    #@1c
    const/4 v1, 0x3

    #@1d
    const-string/jumbo v2, "pt"

    #@20
    aput-object v2, v0, v1

    #@22
    const-string v1, "in"

    #@24
    aput-object v1, v0, v6

    #@26
    const/4 v1, 0x5

    #@27
    const-string/jumbo v2, "mm"

    #@2a
    aput-object v2, v0, v1

    #@2c
    sput-object v0, Landroid/util/TypedValue;->DIMENSION_UNIT_STRS:[Ljava/lang/String;

    #@2e
    .line 420
    new-array v0, v5, [Ljava/lang/String;

    #@30
    const-string v1, "%"

    #@32
    aput-object v1, v0, v3

    #@34
    const-string v1, "%p"

    #@36
    aput-object v1, v0, v4

    #@38
    sput-object v0, Landroid/util/TypedValue;->FRACTION_UNIT_STRS:[Ljava/lang/String;

    #@3a
    return-void

    #@3b
    .line 191
    nop

    #@3c
    :array_3c
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3bt
        0x0t 0x0t 0x0t 0x38t
        0x0t 0x0t 0x0t 0x34t
        0x0t 0x0t 0x0t 0x30t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 174
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/util/TypedValue;->changingConfigurations:I

    #@6
    return-void
.end method

.method public static applyDimension(IFLandroid/util/DisplayMetrics;)F
    .registers 5
    .parameter "unit"
    .parameter "value"
    .parameter "metrics"

    #@0
    .prologue
    .line 323
    packed-switch p0, :pswitch_data_24

    #@3
    .line 337
    const/4 p1, 0x0

    #@4
    .end local p1
    :goto_4
    :pswitch_4
    return p1

    #@5
    .line 327
    .restart local p1
    :pswitch_5
    iget v0, p2, Landroid/util/DisplayMetrics;->density:F

    #@7
    mul-float/2addr p1, v0

    #@8
    goto :goto_4

    #@9
    .line 329
    :pswitch_9
    iget v0, p2, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@b
    mul-float/2addr p1, v0

    #@c
    goto :goto_4

    #@d
    .line 331
    :pswitch_d
    iget v0, p2, Landroid/util/DisplayMetrics;->xdpi:F

    #@f
    mul-float/2addr v0, p1

    #@10
    const v1, 0x3c638e39

    #@13
    mul-float p1, v0, v1

    #@15
    goto :goto_4

    #@16
    .line 333
    :pswitch_16
    iget v0, p2, Landroid/util/DisplayMetrics;->xdpi:F

    #@18
    mul-float/2addr p1, v0

    #@19
    goto :goto_4

    #@1a
    .line 335
    :pswitch_1a
    iget v0, p2, Landroid/util/DisplayMetrics;->xdpi:F

    #@1c
    mul-float/2addr v0, p1

    #@1d
    const v1, 0x3d214285

    #@20
    mul-float p1, v0, v1

    #@22
    goto :goto_4

    #@23
    .line 323
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_9
        :pswitch_d
        :pswitch_16
        :pswitch_1a
    .end packed-switch
.end method

.method public static final coerceToString(II)Ljava/lang/String;
    .registers 5
    .parameter "type"
    .parameter "data"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/16 v2, 0x1f

    #@3
    .line 436
    packed-switch p0, :pswitch_data_cc

    #@6
    .line 457
    :pswitch_6
    const/16 v1, 0x1c

    #@8
    if-lt p0, v1, :cond_c0

    #@a
    if-gt p0, v2, :cond_c0

    #@c
    .line 458
    new-instance v0, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v1, "#"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 463
    :cond_23
    :goto_23
    :pswitch_23
    return-object v0

    #@24
    .line 440
    :pswitch_24
    new-instance v0, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v1, "@"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    goto :goto_23

    #@38
    .line 442
    :pswitch_38
    new-instance v0, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v1, "?"

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    goto :goto_23

    #@4c
    .line 444
    :pswitch_4c
    invoke-static {p1}, Ljava/lang/Float;->intBitsToFloat(I)F

    #@4f
    move-result v0

    #@50
    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    #@53
    move-result-object v0

    #@54
    goto :goto_23

    #@55
    .line 446
    :pswitch_55
    new-instance v0, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    invoke-static {p1}, Landroid/util/TypedValue;->complexToFloat(I)F

    #@5d
    move-result v1

    #@5e
    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v0

    #@66
    sget-object v1, Landroid/util/TypedValue;->DIMENSION_UNIT_STRS:[Ljava/lang/String;

    #@68
    shr-int/lit8 v2, p1, 0x0

    #@6a
    and-int/lit8 v2, v2, 0xf

    #@6c
    aget-object v1, v1, v2

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    goto :goto_23

    #@77
    .line 449
    :pswitch_77
    new-instance v0, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    invoke-static {p1}, Landroid/util/TypedValue;->complexToFloat(I)F

    #@7f
    move-result v1

    #@80
    const/high16 v2, 0x42c8

    #@82
    mul-float/2addr v1, v2

    #@83
    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    sget-object v1, Landroid/util/TypedValue;->FRACTION_UNIT_STRS:[Ljava/lang/String;

    #@8d
    shr-int/lit8 v2, p1, 0x0

    #@8f
    and-int/lit8 v2, v2, 0xf

    #@91
    aget-object v1, v1, v2

    #@93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v0

    #@97
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v0

    #@9b
    goto :goto_23

    #@9c
    .line 452
    :pswitch_9c
    new-instance v0, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v1, "0x"

    #@a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v0

    #@a7
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@aa
    move-result-object v1

    #@ab
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v0

    #@af
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v0

    #@b3
    goto/16 :goto_23

    #@b5
    .line 454
    :pswitch_b5
    if-eqz p1, :cond_bc

    #@b7
    const-string/jumbo v0, "true"

    #@ba
    goto/16 :goto_23

    #@bc
    :cond_bc
    const-string v0, "false"

    #@be
    goto/16 :goto_23

    #@c0
    .line 459
    :cond_c0
    const/16 v1, 0x10

    #@c2
    if-lt p0, v1, :cond_23

    #@c4
    if-gt p0, v2, :cond_23

    #@c6
    .line 460
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c9
    move-result-object v0

    #@ca
    goto/16 :goto_23

    #@cc
    .line 436
    :pswitch_data_cc
    .packed-switch 0x0
        :pswitch_23
        :pswitch_24
        :pswitch_38
        :pswitch_6
        :pswitch_4c
        :pswitch_55
        :pswitch_77
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_9c
        :pswitch_b5
    .end packed-switch
.end method

.method public static complexToDimension(ILandroid/util/DisplayMetrics;)F
    .registers 4
    .parameter "data"
    .parameter "metrics"

    #@0
    .prologue
    .line 229
    shr-int/lit8 v0, p0, 0x0

    #@2
    and-int/lit8 v0, v0, 0xf

    #@4
    invoke-static {p0}, Landroid/util/TypedValue;->complexToFloat(I)F

    #@7
    move-result v1

    #@8
    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public static complexToDimensionNoisy(ILandroid/util/DisplayMetrics;)F
    .registers 7
    .parameter "data"
    .parameter "metrics"

    #@0
    .prologue
    .line 295
    invoke-static {p0, p1}, Landroid/util/TypedValue;->complexToDimension(ILandroid/util/DisplayMetrics;)F

    #@3
    move-result v0

    #@4
    .line 296
    .local v0, res:F
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Dimension (0x"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    shr-int/lit8 v3, p0, 0x8

    #@13
    const v4, 0xffffff

    #@16
    and-int/2addr v3, v4

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, "*"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    sget-object v3, Landroid/util/TypedValue;->RADIX_MULTS:[F

    #@23
    shr-int/lit8 v4, p0, 0x4

    #@25
    and-int/lit8 v4, v4, 0x3

    #@27
    aget v3, v3, v4

    #@29
    const/high16 v4, 0x3b80

    #@2b
    div-float/2addr v3, v4

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, ")"

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    sget-object v3, Landroid/util/TypedValue;->DIMENSION_UNIT_STRS:[Ljava/lang/String;

    #@38
    shr-int/lit8 v4, p0, 0x0

    #@3a
    and-int/lit8 v4, v4, 0xf

    #@3c
    aget-object v3, v3, v4

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    const-string v3, " = "

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@53
    .line 304
    return v0
.end method

.method public static complexToDimensionPixelOffset(ILandroid/util/DisplayMetrics;)I
    .registers 4
    .parameter "data"
    .parameter "metrics"

    #@0
    .prologue
    .line 254
    shr-int/lit8 v0, p0, 0x0

    #@2
    and-int/lit8 v0, v0, 0xf

    #@4
    invoke-static {p0}, Landroid/util/TypedValue;->complexToFloat(I)F

    #@7
    move-result v1

    #@8
    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@b
    move-result v0

    #@c
    float-to-int v0, v0

    #@d
    return v0
.end method

.method public static complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I
    .registers 7
    .parameter "data"
    .parameter "metrics"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 281
    invoke-static {p0}, Landroid/util/TypedValue;->complexToFloat(I)F

    #@4
    move-result v2

    #@5
    .line 282
    .local v2, value:F
    shr-int/lit8 v3, p0, 0x0

    #@7
    and-int/lit8 v3, v3, 0xf

    #@9
    invoke-static {v3, v2, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@c
    move-result v0

    #@d
    .line 286
    .local v0, f:F
    const/high16 v3, 0x3f00

    #@f
    add-float/2addr v3, v0

    #@10
    float-to-int v1, v3

    #@11
    .line 287
    .local v1, res:I
    if-eqz v1, :cond_14

    #@13
    .line 290
    .end local v1           #res:I
    :goto_13
    return v1

    #@14
    .line 288
    .restart local v1       #res:I
    :cond_14
    cmpl-float v3, v2, v4

    #@16
    if-nez v3, :cond_1a

    #@18
    const/4 v1, 0x0

    #@19
    goto :goto_13

    #@1a
    .line 289
    :cond_1a
    cmpl-float v3, v2, v4

    #@1c
    if-lez v3, :cond_20

    #@1e
    const/4 v1, 0x1

    #@1f
    goto :goto_13

    #@20
    .line 290
    :cond_20
    const/4 v1, -0x1

    #@21
    goto :goto_13
.end method

.method public static complexToFloat(I)F
    .registers 4
    .parameter "complex"

    #@0
    .prologue
    .line 208
    and-int/lit16 v0, p0, -0x100

    #@2
    int-to-float v0, v0

    #@3
    sget-object v1, Landroid/util/TypedValue;->RADIX_MULTS:[F

    #@5
    shr-int/lit8 v2, p0, 0x4

    #@7
    and-int/lit8 v2, v2, 0x3

    #@9
    aget v1, v1, v2

    #@b
    mul-float/2addr v0, v1

    #@c
    return v0
.end method

.method public static complexToFraction(IFF)F
    .registers 4
    .parameter "data"
    .parameter "base"
    .parameter "pbase"

    #@0
    .prologue
    .line 373
    shr-int/lit8 v0, p0, 0x0

    #@2
    and-int/lit8 v0, v0, 0xf

    #@4
    packed-switch v0, :pswitch_data_16

    #@7
    .line 379
    const/4 v0, 0x0

    #@8
    :goto_8
    return v0

    #@9
    .line 375
    :pswitch_9
    invoke-static {p0}, Landroid/util/TypedValue;->complexToFloat(I)F

    #@c
    move-result v0

    #@d
    mul-float/2addr v0, p1

    #@e
    goto :goto_8

    #@f
    .line 377
    :pswitch_f
    invoke-static {p0}, Landroid/util/TypedValue;->complexToFloat(I)F

    #@12
    move-result v0

    #@13
    mul-float/2addr v0, p2

    #@14
    goto :goto_8

    #@15
    .line 373
    nop

    #@16
    :pswitch_data_16
    .packed-switch 0x0
        :pswitch_9
        :pswitch_f
    .end packed-switch
.end method


# virtual methods
.method public final coerceToString()Ljava/lang/CharSequence;
    .registers 3

    #@0
    .prologue
    .line 410
    iget v0, p0, Landroid/util/TypedValue;->type:I

    #@2
    .line 411
    .local v0, t:I
    const/4 v1, 0x3

    #@3
    if-ne v0, v1, :cond_8

    #@5
    .line 412
    iget-object v1, p0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@7
    .line 414
    :goto_7
    return-object v1

    #@8
    :cond_8
    iget v1, p0, Landroid/util/TypedValue;->data:I

    #@a
    invoke-static {v0, v1}, Landroid/util/TypedValue;->coerceToString(II)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    goto :goto_7
.end method

.method public getDimension(Landroid/util/DisplayMetrics;)F
    .registers 3
    .parameter "metrics"

    #@0
    .prologue
    .line 352
    iget v0, p0, Landroid/util/TypedValue;->data:I

    #@2
    invoke-static {v0, p1}, Landroid/util/TypedValue;->complexToDimension(ILandroid/util/DisplayMetrics;)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getFloat()F
    .registers 2

    #@0
    .prologue
    .line 186
    iget v0, p0, Landroid/util/TypedValue;->data:I

    #@2
    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getFraction(FF)F
    .registers 4
    .parameter "base"
    .parameter "pbase"

    #@0
    .prologue
    .line 397
    iget v0, p0, Landroid/util/TypedValue;->data:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/util/TypedValue;->complexToFraction(IFF)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public setTo(Landroid/util/TypedValue;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 468
    iget v0, p1, Landroid/util/TypedValue;->type:I

    #@2
    iput v0, p0, Landroid/util/TypedValue;->type:I

    #@4
    .line 469
    iget-object v0, p1, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@6
    iput-object v0, p0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@8
    .line 470
    iget v0, p1, Landroid/util/TypedValue;->data:I

    #@a
    iput v0, p0, Landroid/util/TypedValue;->data:I

    #@c
    .line 471
    iget v0, p1, Landroid/util/TypedValue;->assetCookie:I

    #@e
    iput v0, p0, Landroid/util/TypedValue;->assetCookie:I

    #@10
    .line 472
    iget v0, p1, Landroid/util/TypedValue;->resourceId:I

    #@12
    iput v0, p0, Landroid/util/TypedValue;->resourceId:I

    #@14
    .line 473
    iget v0, p1, Landroid/util/TypedValue;->density:I

    #@16
    iput v0, p0, Landroid/util/TypedValue;->density:I

    #@18
    .line 474
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 478
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 479
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "TypedValue{t=0x"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget v2, p0, Landroid/util/TypedValue;->type:I

    #@d
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 480
    const-string v1, "/d=0x"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    iget v2, p0, Landroid/util/TypedValue;->data:I

    #@1c
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 481
    iget v1, p0, Landroid/util/TypedValue;->type:I

    #@25
    const/4 v2, 0x3

    #@26
    if-ne v1, v2, :cond_3d

    #@28
    .line 482
    const-string v1, " \""

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    iget-object v1, p0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@30
    if-eqz v1, :cond_6a

    #@32
    iget-object v1, p0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@34
    :goto_34
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    const-string v2, "\""

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    .line 484
    :cond_3d
    iget v1, p0, Landroid/util/TypedValue;->assetCookie:I

    #@3f
    if-eqz v1, :cond_4c

    #@41
    .line 485
    const-string v1, " a="

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    iget v2, p0, Landroid/util/TypedValue;->assetCookie:I

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    .line 487
    :cond_4c
    iget v1, p0, Landroid/util/TypedValue;->resourceId:I

    #@4e
    if-eqz v1, :cond_5f

    #@50
    .line 488
    const-string v1, " r=0x"

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    iget v2, p0, Landroid/util/TypedValue;->resourceId:I

    #@58
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    .line 490
    :cond_5f
    const-string/jumbo v1, "}"

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    .line 491
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    return-object v1

    #@6a
    .line 482
    :cond_6a
    const-string v1, "<null>"

    #@6c
    goto :goto_34
.end method
