.class Landroid/util/XmlPullAttributes;
.super Ljava/lang/Object;
.source "XmlPullAttributes.java"

# interfaces
.implements Landroid/util/AttributeSet;


# instance fields
.field mParser:Lorg/xmlpull/v1/XmlPullParser;


# direct methods
.method public constructor <init>(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 2
    .parameter "parser"

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    iput-object p1, p0, Landroid/util/XmlPullAttributes;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    #@5
    .line 31
    return-void
.end method


# virtual methods
.method public getAttributeBooleanValue(IZ)Z
    .registers 4
    .parameter "index"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 103
    invoke-virtual {p0, p1}, Landroid/util/XmlPullAttributes;->getAttributeValue(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p2}, Lcom/android/internal/util/XmlUtils;->convertValueToBoolean(Ljava/lang/CharSequence;Z)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 5
    .parameter "namespace"
    .parameter "attribute"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 65
    invoke-virtual {p0, p1, p2}, Landroid/util/XmlPullAttributes;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p3}, Lcom/android/internal/util/XmlUtils;->convertValueToBoolean(Ljava/lang/CharSequence;Z)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getAttributeCount()I
    .registers 2

    #@0
    .prologue
    .line 34
    iget-object v0, p0, Landroid/util/XmlPullAttributes;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    #@2
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getAttributeFloatValue(IF)F
    .registers 4
    .parameter "index"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 123
    invoke-virtual {p0, p1}, Landroid/util/XmlPullAttributes;->getAttributeValue(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 124
    .local v0, s:Ljava/lang/String;
    if-eqz v0, :cond_a

    #@6
    .line 125
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@9
    move-result p2

    #@a
    .line 127
    .end local p2
    :cond_a
    return p2
.end method

.method public getAttributeFloatValue(Ljava/lang/String;Ljava/lang/String;F)F
    .registers 5
    .parameter "namespace"
    .parameter "attribute"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 89
    invoke-virtual {p0, p1, p2}, Landroid/util/XmlPullAttributes;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 90
    .local v0, s:Ljava/lang/String;
    if-eqz v0, :cond_a

    #@6
    .line 91
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@9
    move-result p3

    #@a
    .line 93
    .end local p3
    :cond_a
    return p3
.end method

.method public getAttributeIntValue(II)I
    .registers 4
    .parameter "index"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 113
    invoke-virtual {p0, p1}, Landroid/util/XmlPullAttributes;->getAttributeValue(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p2}, Lcom/android/internal/util/XmlUtils;->convertValueToInt(Ljava/lang/CharSequence;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 5
    .parameter "namespace"
    .parameter "attribute"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 77
    invoke-virtual {p0, p1, p2}, Landroid/util/XmlPullAttributes;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p3}, Lcom/android/internal/util/XmlUtils;->convertValueToInt(Ljava/lang/CharSequence;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getAttributeListValue(I[Ljava/lang/String;I)I
    .registers 5
    .parameter "index"
    .parameter "options"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 98
    invoke-virtual {p0, p1}, Landroid/util/XmlPullAttributes;->getAttributeValue(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p2, p3}, Lcom/android/internal/util/XmlUtils;->convertValueToList(Ljava/lang/CharSequence;[Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getAttributeListValue(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)I
    .registers 6
    .parameter "namespace"
    .parameter "attribute"
    .parameter "options"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 59
    invoke-virtual {p0, p1, p2}, Landroid/util/XmlPullAttributes;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p3, p4}, Lcom/android/internal/util/XmlUtils;->convertValueToList(Ljava/lang/CharSequence;[Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getAttributeName(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Landroid/util/XmlPullAttributes;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    #@2
    invoke-interface {v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAttributeNameResource(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 54
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAttributeResourceValue(II)I
    .registers 4
    .parameter "index"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 108
    invoke-virtual {p0, p1}, Landroid/util/XmlPullAttributes;->getAttributeValue(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p2}, Lcom/android/internal/util/XmlUtils;->convertValueToInt(Ljava/lang/CharSequence;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 5
    .parameter "namespace"
    .parameter "attribute"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 71
    invoke-virtual {p0, p1, p2}, Landroid/util/XmlPullAttributes;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p3}, Lcom/android/internal/util/XmlUtils;->convertValueToInt(Ljava/lang/CharSequence;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getAttributeUnsignedIntValue(II)I
    .registers 4
    .parameter "index"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 118
    invoke-virtual {p0, p1}, Landroid/util/XmlPullAttributes;->getAttributeValue(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p2}, Lcom/android/internal/util/XmlUtils;->convertValueToUnsignedInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getAttributeUnsignedIntValue(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 5
    .parameter "namespace"
    .parameter "attribute"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 83
    invoke-virtual {p0, p1, p2}, Landroid/util/XmlPullAttributes;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p3}, Lcom/android/internal/util/XmlUtils;->convertValueToUnsignedInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getAttributeValue(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Landroid/util/XmlPullAttributes;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    #@2
    invoke-interface {v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "namespace"
    .parameter "name"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Landroid/util/XmlPullAttributes;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    #@2
    invoke-interface {v0, p1, p2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getClassAttribute()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 135
    const/4 v0, 0x0

    #@1
    const-string v1, "class"

    #@3
    invoke-virtual {p0, v0, v1}, Landroid/util/XmlPullAttributes;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getIdAttribute()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 131
    const/4 v0, 0x0

    #@1
    const-string v1, "id"

    #@3
    invoke-virtual {p0, v0, v1}, Landroid/util/XmlPullAttributes;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getIdAttributeResourceValue(I)I
    .registers 4
    .parameter "defaultValue"

    #@0
    .prologue
    .line 139
    const/4 v0, 0x0

    #@1
    const-string v1, "id"

    #@3
    invoke-virtual {p0, v0, v1, p1}, Landroid/util/XmlPullAttributes;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getPositionDescription()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/util/XmlPullAttributes;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    #@2
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getStyleAttribute()I
    .registers 4

    #@0
    .prologue
    .line 143
    const/4 v0, 0x0

    #@1
    const-string/jumbo v1, "style"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {p0, v0, v1, v2}, Landroid/util/XmlPullAttributes;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    #@8
    move-result v0

    #@9
    return v0
.end method
