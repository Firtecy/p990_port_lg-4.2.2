.class public final enum Landroid/util/JsonToken;
.super Ljava/lang/Enum;
.source "JsonToken.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/util/JsonToken;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/util/JsonToken;

.field public static final enum BEGIN_ARRAY:Landroid/util/JsonToken;

.field public static final enum BEGIN_OBJECT:Landroid/util/JsonToken;

.field public static final enum BOOLEAN:Landroid/util/JsonToken;

.field public static final enum END_ARRAY:Landroid/util/JsonToken;

.field public static final enum END_DOCUMENT:Landroid/util/JsonToken;

.field public static final enum END_OBJECT:Landroid/util/JsonToken;

.field public static final enum NAME:Landroid/util/JsonToken;

.field public static final enum NULL:Landroid/util/JsonToken;

.field public static final enum NUMBER:Landroid/util/JsonToken;

.field public static final enum STRING:Landroid/util/JsonToken;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 28
    new-instance v0, Landroid/util/JsonToken;

    #@7
    const-string v1, "BEGIN_ARRAY"

    #@9
    invoke-direct {v0, v1, v3}, Landroid/util/JsonToken;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    #@e
    .line 34
    new-instance v0, Landroid/util/JsonToken;

    #@10
    const-string v1, "END_ARRAY"

    #@12
    invoke-direct {v0, v1, v4}, Landroid/util/JsonToken;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    #@17
    .line 40
    new-instance v0, Landroid/util/JsonToken;

    #@19
    const-string v1, "BEGIN_OBJECT"

    #@1b
    invoke-direct {v0, v1, v5}, Landroid/util/JsonToken;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    #@20
    .line 46
    new-instance v0, Landroid/util/JsonToken;

    #@22
    const-string v1, "END_OBJECT"

    #@24
    invoke-direct {v0, v1, v6}, Landroid/util/JsonToken;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    #@29
    .line 53
    new-instance v0, Landroid/util/JsonToken;

    #@2b
    const-string v1, "NAME"

    #@2d
    invoke-direct {v0, v1, v7}, Landroid/util/JsonToken;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Landroid/util/JsonToken;->NAME:Landroid/util/JsonToken;

    #@32
    .line 58
    new-instance v0, Landroid/util/JsonToken;

    #@34
    const-string v1, "STRING"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Landroid/util/JsonToken;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@3c
    .line 64
    new-instance v0, Landroid/util/JsonToken;

    #@3e
    const-string v1, "NUMBER"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Landroid/util/JsonToken;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Landroid/util/JsonToken;->NUMBER:Landroid/util/JsonToken;

    #@46
    .line 69
    new-instance v0, Landroid/util/JsonToken;

    #@48
    const-string v1, "BOOLEAN"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Landroid/util/JsonToken;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Landroid/util/JsonToken;->BOOLEAN:Landroid/util/JsonToken;

    #@50
    .line 74
    new-instance v0, Landroid/util/JsonToken;

    #@52
    const-string v1, "NULL"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Landroid/util/JsonToken;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    #@5b
    .line 81
    new-instance v0, Landroid/util/JsonToken;

    #@5d
    const-string v1, "END_DOCUMENT"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Landroid/util/JsonToken;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Landroid/util/JsonToken;->END_DOCUMENT:Landroid/util/JsonToken;

    #@66
    .line 22
    const/16 v0, 0xa

    #@68
    new-array v0, v0, [Landroid/util/JsonToken;

    #@6a
    sget-object v1, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    #@6c
    aput-object v1, v0, v3

    #@6e
    sget-object v1, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    #@70
    aput-object v1, v0, v4

    #@72
    sget-object v1, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    #@74
    aput-object v1, v0, v5

    #@76
    sget-object v1, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    #@78
    aput-object v1, v0, v6

    #@7a
    sget-object v1, Landroid/util/JsonToken;->NAME:Landroid/util/JsonToken;

    #@7c
    aput-object v1, v0, v7

    #@7e
    const/4 v1, 0x5

    #@7f
    sget-object v2, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@81
    aput-object v2, v0, v1

    #@83
    const/4 v1, 0x6

    #@84
    sget-object v2, Landroid/util/JsonToken;->NUMBER:Landroid/util/JsonToken;

    #@86
    aput-object v2, v0, v1

    #@88
    const/4 v1, 0x7

    #@89
    sget-object v2, Landroid/util/JsonToken;->BOOLEAN:Landroid/util/JsonToken;

    #@8b
    aput-object v2, v0, v1

    #@8d
    const/16 v1, 0x8

    #@8f
    sget-object v2, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    #@91
    aput-object v2, v0, v1

    #@93
    const/16 v1, 0x9

    #@95
    sget-object v2, Landroid/util/JsonToken;->END_DOCUMENT:Landroid/util/JsonToken;

    #@97
    aput-object v2, v0, v1

    #@99
    sput-object v0, Landroid/util/JsonToken;->$VALUES:[Landroid/util/JsonToken;

    #@9b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/util/JsonToken;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 22
    const-class v0, Landroid/util/JsonToken;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/util/JsonToken;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/util/JsonToken;
    .registers 1

    #@0
    .prologue
    .line 22
    sget-object v0, Landroid/util/JsonToken;->$VALUES:[Landroid/util/JsonToken;

    #@2
    invoke-virtual {v0}, [Landroid/util/JsonToken;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/util/JsonToken;

    #@8
    return-object v0
.end method
