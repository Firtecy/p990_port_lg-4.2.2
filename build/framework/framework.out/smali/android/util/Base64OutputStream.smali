.class public Landroid/util/Base64OutputStream;
.super Ljava/io/FilterOutputStream;
.source "Base64OutputStream.java"


# static fields
.field private static EMPTY:[B


# instance fields
.field private bpos:I

.field private buffer:[B

.field private final coder:Landroid/util/Base64$Coder;

.field private final flags:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 34
    const/4 v0, 0x0

    #@1
    new-array v0, v0, [B

    #@3
    sput-object v0, Landroid/util/Base64OutputStream;->EMPTY:[B

    #@5
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 45
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/util/Base64OutputStream;-><init>(Ljava/io/OutputStream;IZ)V

    #@4
    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;IZ)V
    .registers 6
    .parameter "out"
    .parameter "flags"
    .parameter "encode"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 61
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@4
    .line 31
    iput-object v1, p0, Landroid/util/Base64OutputStream;->buffer:[B

    #@6
    .line 32
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/util/Base64OutputStream;->bpos:I

    #@9
    .line 62
    iput p2, p0, Landroid/util/Base64OutputStream;->flags:I

    #@b
    .line 63
    if-eqz p3, :cond_15

    #@d
    .line 64
    new-instance v0, Landroid/util/Base64$Encoder;

    #@f
    invoke-direct {v0, p2, v1}, Landroid/util/Base64$Encoder;-><init>(I[B)V

    #@12
    iput-object v0, p0, Landroid/util/Base64OutputStream;->coder:Landroid/util/Base64$Coder;

    #@14
    .line 68
    :goto_14
    return-void

    #@15
    .line 66
    :cond_15
    new-instance v0, Landroid/util/Base64$Decoder;

    #@17
    invoke-direct {v0, p2, v1}, Landroid/util/Base64$Decoder;-><init>(I[B)V

    #@1a
    iput-object v0, p0, Landroid/util/Base64OutputStream;->coder:Landroid/util/Base64$Coder;

    #@1c
    goto :goto_14
.end method

.method private embiggen([BI)[B
    .registers 4
    .parameter "b"
    .parameter "len"

    #@0
    .prologue
    .line 149
    if-eqz p1, :cond_5

    #@2
    array-length v0, p1

    #@3
    if-ge v0, p2, :cond_7

    #@5
    .line 150
    :cond_5
    new-array p1, p2, [B

    #@7
    .line 152
    .end local p1
    :cond_7
    return-object p1
.end method

.method private flushBuffer()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 92
    iget v0, p0, Landroid/util/Base64OutputStream;->bpos:I

    #@3
    if-lez v0, :cond_e

    #@5
    .line 93
    iget-object v0, p0, Landroid/util/Base64OutputStream;->buffer:[B

    #@7
    iget v1, p0, Landroid/util/Base64OutputStream;->bpos:I

    #@9
    invoke-direct {p0, v0, v2, v1, v2}, Landroid/util/Base64OutputStream;->internalWrite([BIIZ)V

    #@c
    .line 94
    iput v2, p0, Landroid/util/Base64OutputStream;->bpos:I

    #@e
    .line 96
    :cond_e
    return-void
.end method

.method private internalWrite([BIIZ)V
    .registers 9
    .parameter "b"
    .parameter "off"
    .parameter "len"
    .parameter "finish"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Landroid/util/Base64OutputStream;->coder:Landroid/util/Base64$Coder;

    #@2
    iget-object v1, p0, Landroid/util/Base64OutputStream;->coder:Landroid/util/Base64$Coder;

    #@4
    iget-object v1, v1, Landroid/util/Base64$Coder;->output:[B

    #@6
    iget-object v2, p0, Landroid/util/Base64OutputStream;->coder:Landroid/util/Base64$Coder;

    #@8
    invoke-virtual {v2, p3}, Landroid/util/Base64$Coder;->maxOutputSize(I)I

    #@b
    move-result v2

    #@c
    invoke-direct {p0, v1, v2}, Landroid/util/Base64OutputStream;->embiggen([BI)[B

    #@f
    move-result-object v1

    #@10
    iput-object v1, v0, Landroid/util/Base64$Coder;->output:[B

    #@12
    .line 138
    iget-object v0, p0, Landroid/util/Base64OutputStream;->coder:Landroid/util/Base64$Coder;

    #@14
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/util/Base64$Coder;->process([BIIZ)Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_22

    #@1a
    .line 139
    new-instance v0, Landroid/util/Base64DataException;

    #@1c
    const-string v1, "bad base-64"

    #@1e
    invoke-direct {v0, v1}, Landroid/util/Base64DataException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 141
    :cond_22
    iget-object v0, p0, Landroid/util/Base64OutputStream;->out:Ljava/io/OutputStream;

    #@24
    iget-object v1, p0, Landroid/util/Base64OutputStream;->coder:Landroid/util/Base64$Coder;

    #@26
    iget-object v1, v1, Landroid/util/Base64$Coder;->output:[B

    #@28
    const/4 v2, 0x0

    #@29
    iget-object v3, p0, Landroid/util/Base64OutputStream;->coder:Landroid/util/Base64$Coder;

    #@2b
    iget v3, v3, Landroid/util/Base64$Coder;->op:I

    #@2d
    invoke-virtual {v0, v1, v2, v3}, Ljava/io/OutputStream;->write([BII)V

    #@30
    .line 142
    return-void
.end method


# virtual methods
.method public close()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 105
    const/4 v1, 0x0

    #@1
    .line 107
    .local v1, thrown:Ljava/io/IOException;
    :try_start_1
    invoke-direct {p0}, Landroid/util/Base64OutputStream;->flushBuffer()V

    #@4
    .line 108
    sget-object v2, Landroid/util/Base64OutputStream;->EMPTY:[B

    #@6
    const/4 v3, 0x0

    #@7
    const/4 v4, 0x0

    #@8
    const/4 v5, 0x1

    #@9
    invoke-direct {p0, v2, v3, v4, v5}, Landroid/util/Base64OutputStream;->internalWrite([BIIZ)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_c} :catch_1a

    #@c
    .line 114
    :goto_c
    :try_start_c
    iget v2, p0, Landroid/util/Base64OutputStream;->flags:I

    #@e
    and-int/lit8 v2, v2, 0x10

    #@10
    if-nez v2, :cond_1d

    #@12
    .line 115
    iget-object v2, p0, Landroid/util/Base64OutputStream;->out:Ljava/io/OutputStream;

    #@14
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_17} :catch_23

    #@17
    .line 125
    :cond_17
    :goto_17
    if-eqz v1, :cond_28

    #@19
    .line 126
    throw v1

    #@1a
    .line 109
    :catch_1a
    move-exception v0

    #@1b
    .line 110
    .local v0, e:Ljava/io/IOException;
    move-object v1, v0

    #@1c
    goto :goto_c

    #@1d
    .line 117
    .end local v0           #e:Ljava/io/IOException;
    :cond_1d
    :try_start_1d
    iget-object v2, p0, Landroid/util/Base64OutputStream;->out:Ljava/io/OutputStream;

    #@1f
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_22} :catch_23

    #@22
    goto :goto_17

    #@23
    .line 119
    :catch_23
    move-exception v0

    #@24
    .line 120
    .restart local v0       #e:Ljava/io/IOException;
    if-eqz v1, :cond_17

    #@26
    .line 121
    move-object v1, v0

    #@27
    goto :goto_17

    #@28
    .line 128
    .end local v0           #e:Ljava/io/IOException;
    :cond_28
    return-void
.end method

.method public write(I)V
    .registers 5
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 76
    iget-object v0, p0, Landroid/util/Base64OutputStream;->buffer:[B

    #@3
    if-nez v0, :cond_b

    #@5
    .line 77
    const/16 v0, 0x400

    #@7
    new-array v0, v0, [B

    #@9
    iput-object v0, p0, Landroid/util/Base64OutputStream;->buffer:[B

    #@b
    .line 79
    :cond_b
    iget v0, p0, Landroid/util/Base64OutputStream;->bpos:I

    #@d
    iget-object v1, p0, Landroid/util/Base64OutputStream;->buffer:[B

    #@f
    array-length v1, v1

    #@10
    if-lt v0, v1, :cond_1b

    #@12
    .line 81
    iget-object v0, p0, Landroid/util/Base64OutputStream;->buffer:[B

    #@14
    iget v1, p0, Landroid/util/Base64OutputStream;->bpos:I

    #@16
    invoke-direct {p0, v0, v2, v1, v2}, Landroid/util/Base64OutputStream;->internalWrite([BIIZ)V

    #@19
    .line 82
    iput v2, p0, Landroid/util/Base64OutputStream;->bpos:I

    #@1b
    .line 84
    :cond_1b
    iget-object v0, p0, Landroid/util/Base64OutputStream;->buffer:[B

    #@1d
    iget v1, p0, Landroid/util/Base64OutputStream;->bpos:I

    #@1f
    add-int/lit8 v2, v1, 0x1

    #@21
    iput v2, p0, Landroid/util/Base64OutputStream;->bpos:I

    #@23
    int-to-byte v2, p1

    #@24
    aput-byte v2, v0, v1

    #@26
    .line 85
    return-void
.end method

.method public write([BII)V
    .registers 5
    .parameter "b"
    .parameter "off"
    .parameter "len"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 99
    if-gtz p3, :cond_3

    #@2
    .line 102
    :goto_2
    return-void

    #@3
    .line 100
    :cond_3
    invoke-direct {p0}, Landroid/util/Base64OutputStream;->flushBuffer()V

    #@6
    .line 101
    const/4 v0, 0x0

    #@7
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/util/Base64OutputStream;->internalWrite([BIIZ)V

    #@a
    goto :goto_2
.end method
