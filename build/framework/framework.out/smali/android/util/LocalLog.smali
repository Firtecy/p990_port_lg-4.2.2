.class public final Landroid/util/LocalLog;
.super Ljava/lang/Object;
.source "LocalLog.java"


# instance fields
.field private mLog:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxLines:I

.field private mNow:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "maxLines"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    new-instance v0, Ljava/util/LinkedList;

    #@5
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/util/LocalLog;->mLog:Ljava/util/LinkedList;

    #@a
    .line 38
    iput p1, p0, Landroid/util/LocalLog;->mMaxLines:I

    #@c
    .line 39
    new-instance v0, Landroid/text/format/Time;

    #@e
    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    #@11
    iput-object v0, p0, Landroid/util/LocalLog;->mNow:Landroid/text/format/Time;

    #@13
    .line 40
    return-void
.end method


# virtual methods
.method public declared-synchronized dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 51
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Landroid/util/LocalLog;->mLog:Ljava/util/LinkedList;

    #@3
    const/4 v2, 0x0

    #@4
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    #@7
    move-result-object v0

    #@8
    .line 52
    .local v0, itr:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_1b

    #@e
    .line 53
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Ljava/lang/String;

    #@14
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_18

    #@17
    goto :goto_8

    #@18
    .line 51
    .end local v0           #itr:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :catchall_18
    move-exception v1

    #@19
    monitor-exit p0

    #@1a
    throw v1

    #@1b
    .line 55
    .restart local v0       #itr:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1b
    monitor-exit p0

    #@1c
    return-void
.end method

.method public declared-synchronized log(Ljava/lang/String;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 43
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/util/LocalLog;->mMaxLines:I

    #@3
    if-lez v0, :cond_41

    #@5
    .line 44
    iget-object v0, p0, Landroid/util/LocalLog;->mNow:Landroid/text/format/Time;

    #@7
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    #@a
    .line 45
    iget-object v0, p0, Landroid/util/LocalLog;->mLog:Ljava/util/LinkedList;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    iget-object v2, p0, Landroid/util/LocalLog;->mNow:Landroid/text/format/Time;

    #@13
    const-string v3, "%H:%M:%S"

    #@15
    invoke-virtual {v2, v3}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, " - "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@2e
    .line 46
    :goto_2e
    iget-object v0, p0, Landroid/util/LocalLog;->mLog:Ljava/util/LinkedList;

    #@30
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    #@33
    move-result v0

    #@34
    iget v1, p0, Landroid/util/LocalLog;->mMaxLines:I

    #@36
    if-le v0, v1, :cond_41

    #@38
    iget-object v0, p0, Landroid/util/LocalLog;->mLog:Ljava/util/LinkedList;

    #@3a
    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;
    :try_end_3d
    .catchall {:try_start_1 .. :try_end_3d} :catchall_3e

    #@3d
    goto :goto_2e

    #@3e
    .line 43
    :catchall_3e
    move-exception v0

    #@3f
    monitor-exit p0

    #@40
    throw v0

    #@41
    .line 48
    :cond_41
    monitor-exit p0

    #@42
    return-void
.end method
