.class Landroid/util/FinitePool;
.super Ljava/lang/Object;
.source "FinitePool.java"

# interfaces
.implements Landroid/util/Pool;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/util/Poolable",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "Landroid/util/Pool",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "FinitePool"


# instance fields
.field private final mInfinite:Z

.field private final mLimit:I

.field private final mManager:Landroid/util/PoolableManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/PoolableManager",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mPoolCount:I

.field private mRoot:Landroid/util/Poolable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/util/PoolableManager;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/PoolableManager",
            "<TT;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 47
    .local p0, this:Landroid/util/FinitePool;,"Landroid/util/FinitePool<TT;>;"
    .local p1, manager:Landroid/util/PoolableManager;,"Landroid/util/PoolableManager<TT;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 48
    iput-object p1, p0, Landroid/util/FinitePool;->mManager:Landroid/util/PoolableManager;

    #@5
    .line 49
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/util/FinitePool;->mLimit:I

    #@8
    .line 50
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Landroid/util/FinitePool;->mInfinite:Z

    #@b
    .line 51
    return-void
.end method

.method constructor <init>(Landroid/util/PoolableManager;I)V
    .registers 5
    .parameter
    .parameter "limit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/PoolableManager",
            "<TT;>;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 53
    .local p0, this:Landroid/util/FinitePool;,"Landroid/util/FinitePool<TT;>;"
    .local p1, manager:Landroid/util/PoolableManager;,"Landroid/util/PoolableManager<TT;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    if-gtz p2, :cond_d

    #@5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "The pool limit must be > 0"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 56
    :cond_d
    iput-object p1, p0, Landroid/util/FinitePool;->mManager:Landroid/util/PoolableManager;

    #@f
    .line 57
    iput p2, p0, Landroid/util/FinitePool;->mLimit:I

    #@11
    .line 58
    const/4 v0, 0x0

    #@12
    iput-boolean v0, p0, Landroid/util/FinitePool;->mInfinite:Z

    #@14
    .line 59
    return-void
.end method


# virtual methods
.method public acquire()Landroid/util/Poolable;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 64
    .local p0, this:Landroid/util/FinitePool;,"Landroid/util/FinitePool<TT;>;"
    iget-object v1, p0, Landroid/util/FinitePool;->mRoot:Landroid/util/Poolable;

    #@2
    if-eqz v1, :cond_24

    #@4
    .line 65
    iget-object v0, p0, Landroid/util/FinitePool;->mRoot:Landroid/util/Poolable;

    #@6
    .line 66
    .local v0, element:Landroid/util/Poolable;,"TT;"
    invoke-interface {v0}, Landroid/util/Poolable;->getNextPoolable()Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/util/Poolable;

    #@c
    iput-object v1, p0, Landroid/util/FinitePool;->mRoot:Landroid/util/Poolable;

    #@e
    .line 67
    iget v1, p0, Landroid/util/FinitePool;->mPoolCount:I

    #@10
    add-int/lit8 v1, v1, -0x1

    #@12
    iput v1, p0, Landroid/util/FinitePool;->mPoolCount:I

    #@14
    .line 72
    :goto_14
    if-eqz v0, :cond_23

    #@16
    .line 73
    const/4 v1, 0x0

    #@17
    invoke-interface {v0, v1}, Landroid/util/Poolable;->setNextPoolable(Ljava/lang/Object;)V

    #@1a
    .line 74
    const/4 v1, 0x0

    #@1b
    invoke-interface {v0, v1}, Landroid/util/Poolable;->setPooled(Z)V

    #@1e
    .line 75
    iget-object v1, p0, Landroid/util/FinitePool;->mManager:Landroid/util/PoolableManager;

    #@20
    invoke-interface {v1, v0}, Landroid/util/PoolableManager;->onAcquired(Landroid/util/Poolable;)V

    #@23
    .line 78
    :cond_23
    return-object v0

    #@24
    .line 69
    .end local v0           #element:Landroid/util/Poolable;,"TT;"
    :cond_24
    iget-object v1, p0, Landroid/util/FinitePool;->mManager:Landroid/util/PoolableManager;

    #@26
    invoke-interface {v1}, Landroid/util/PoolableManager;->newInstance()Landroid/util/Poolable;

    #@29
    move-result-object v0

    #@2a
    .restart local v0       #element:Landroid/util/Poolable;,"TT;"
    goto :goto_14
.end method

.method public release(Landroid/util/Poolable;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 82
    .local p0, this:Landroid/util/FinitePool;,"Landroid/util/FinitePool<TT;>;"
    .local p1, element:Landroid/util/Poolable;,"TT;"
    invoke-interface {p1}, Landroid/util/Poolable;->isPooled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_27

    #@6
    .line 83
    iget-boolean v0, p0, Landroid/util/FinitePool;->mInfinite:Z

    #@8
    if-nez v0, :cond_10

    #@a
    iget v0, p0, Landroid/util/FinitePool;->mPoolCount:I

    #@c
    iget v1, p0, Landroid/util/FinitePool;->mLimit:I

    #@e
    if-ge v0, v1, :cond_21

    #@10
    .line 84
    :cond_10
    iget v0, p0, Landroid/util/FinitePool;->mPoolCount:I

    #@12
    add-int/lit8 v0, v0, 0x1

    #@14
    iput v0, p0, Landroid/util/FinitePool;->mPoolCount:I

    #@16
    .line 85
    iget-object v0, p0, Landroid/util/FinitePool;->mRoot:Landroid/util/Poolable;

    #@18
    invoke-interface {p1, v0}, Landroid/util/Poolable;->setNextPoolable(Ljava/lang/Object;)V

    #@1b
    .line 86
    const/4 v0, 0x1

    #@1c
    invoke-interface {p1, v0}, Landroid/util/Poolable;->setPooled(Z)V

    #@1f
    .line 87
    iput-object p1, p0, Landroid/util/FinitePool;->mRoot:Landroid/util/Poolable;

    #@21
    .line 89
    :cond_21
    iget-object v0, p0, Landroid/util/FinitePool;->mManager:Landroid/util/PoolableManager;

    #@23
    invoke-interface {v0, p1}, Landroid/util/PoolableManager;->onReleased(Landroid/util/Poolable;)V

    #@26
    .line 93
    :goto_26
    return-void

    #@27
    .line 91
    :cond_27
    const-string v0, "FinitePool"

    #@29
    new-instance v1, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v2, "Element is already in pool: "

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_26
.end method
