.class public Landroid/util/EventLog;
.super Ljava/lang/Object;
.source "EventLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/util/EventLog$Event;
    }
.end annotation


# static fields
.field private static final COMMENT_PATTERN:Ljava/lang/String; = "^\\s*(#.*)?$"

.field private static final TAG:Ljava/lang/String; = "EventLog"

.field private static final TAGS_FILE:Ljava/lang/String; = "/system/etc/event-log-tags"

.field private static final TAG_PATTERN:Ljava/lang/String; = "^\\s*(\\d+)\\s+(\\w+)\\s*(\\(.*\\))?\\s*$"

.field private static sTagCodes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sTagNames:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 52
    sput-object v0, Landroid/util/EventLog;->sTagCodes:Ljava/util/HashMap;

    #@3
    .line 53
    sput-object v0, Landroid/util/EventLog;->sTagNames:Ljava/util/HashMap;

    #@5
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getTagCode(Ljava/lang/String;)I
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 211
    invoke-static {}, Landroid/util/EventLog;->readTagsFile()V

    #@3
    .line 212
    sget-object v1, Landroid/util/EventLog;->sTagCodes:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/lang/Integer;

    #@b
    .line 213
    .local v0, code:Ljava/lang/Integer;
    if-eqz v0, :cond_12

    #@d
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@10
    move-result v1

    #@11
    :goto_11
    return v1

    #@12
    :cond_12
    const/4 v1, -0x1

    #@13
    goto :goto_11
.end method

.method public static getTagName(I)Ljava/lang/String;
    .registers 3
    .parameter "tag"

    #@0
    .prologue
    .line 201
    invoke-static {}, Landroid/util/EventLog;->readTagsFile()V

    #@3
    .line 202
    sget-object v0, Landroid/util/EventLog;->sTagNames:Ljava/util/HashMap;

    #@5
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Ljava/lang/String;

    #@f
    return-object v0
.end method

.method public static native readEvents([ILjava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/Collection",
            "<",
            "Landroid/util/EventLog$Event;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static declared-synchronized readTagsFile()V
    .registers 13

    #@0
    .prologue
    .line 220
    const-class v10, Landroid/util/EventLog;

    #@2
    monitor-enter v10

    #@3
    :try_start_3
    sget-object v9, Landroid/util/EventLog;->sTagCodes:Ljava/util/HashMap;

    #@5
    if-eqz v9, :cond_d

    #@7
    sget-object v9, Landroid/util/EventLog;->sTagNames:Ljava/util/HashMap;
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_bd

    #@9
    if-eqz v9, :cond_d

    #@b
    .line 256
    .local v0, comment:Ljava/util/regex/Pattern;
    .local v6, reader:Ljava/io/BufferedReader;
    .local v8, tag:Ljava/util/regex/Pattern;
    :cond_b
    :goto_b
    monitor-exit v10

    #@c
    return-void

    #@d
    .line 222
    .end local v0           #comment:Ljava/util/regex/Pattern;
    .end local v6           #reader:Ljava/io/BufferedReader;
    .end local v8           #tag:Ljava/util/regex/Pattern;
    :cond_d
    :try_start_d
    new-instance v9, Ljava/util/HashMap;

    #@f
    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    #@12
    sput-object v9, Landroid/util/EventLog;->sTagCodes:Ljava/util/HashMap;

    #@14
    .line 223
    new-instance v9, Ljava/util/HashMap;

    #@16
    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    #@19
    sput-object v9, Landroid/util/EventLog;->sTagNames:Ljava/util/HashMap;

    #@1b
    .line 225
    const-string v9, "^\\s*(#.*)?$"

    #@1d
    invoke-static {v9}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@20
    move-result-object v0

    #@21
    .line 226
    .restart local v0       #comment:Ljava/util/regex/Pattern;
    const-string v9, "^\\s*(\\d+)\\s+(\\w+)\\s*(\\(.*\\))?\\s*$"

    #@23
    invoke-static {v9}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
    :try_end_26
    .catchall {:try_start_d .. :try_end_26} :catchall_bd

    #@26
    move-result-object v8

    #@27
    .line 227
    .restart local v8       #tag:Ljava/util/regex/Pattern;
    const/4 v6, 0x0

    #@28
    .line 231
    .restart local v6       #reader:Ljava/io/BufferedReader;
    :try_start_28
    new-instance v7, Ljava/io/BufferedReader;

    #@2a
    new-instance v9, Ljava/io/FileReader;

    #@2c
    const-string v11, "/system/etc/event-log-tags"

    #@2e
    invoke-direct {v9, v11}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@31
    const/16 v11, 0x100

    #@33
    invoke-direct {v7, v9, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_36
    .catchall {:try_start_28 .. :try_end_36} :catchall_ce
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_36} :catch_d0

    #@36
    .line 232
    .end local v6           #reader:Ljava/io/BufferedReader;
    .local v7, reader:Ljava/io/BufferedReader;
    :cond_36
    :goto_36
    :try_start_36
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    .local v2, line:Ljava/lang/String;
    if-eqz v2, :cond_c0

    #@3c
    .line 233
    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@3f
    move-result-object v9

    #@40
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->matches()Z

    #@43
    move-result v9

    #@44
    if-nez v9, :cond_36

    #@46
    .line 235
    invoke-virtual {v8, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@49
    move-result-object v3

    #@4a
    .line 236
    .local v3, m:Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    #@4d
    move-result v9

    #@4e
    if-nez v9, :cond_7a

    #@50
    .line 237
    const-string v9, "EventLog"

    #@52
    new-instance v11, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v12, "Bad entry in /system/etc/event-log-tags: "

    #@59
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v11

    #@5d
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v11

    #@61
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v11

    #@65
    invoke-static {v9, v11}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_68
    .catchall {:try_start_36 .. :try_end_68} :catchall_b5
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_68} :catch_69

    #@68
    goto :goto_36

    #@69
    .line 250
    .end local v2           #line:Ljava/lang/String;
    .end local v3           #m:Ljava/util/regex/Matcher;
    :catch_69
    move-exception v1

    #@6a
    move-object v6, v7

    #@6b
    .line 251
    .end local v7           #reader:Ljava/io/BufferedReader;
    .local v1, e:Ljava/io/IOException;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    :goto_6b
    :try_start_6b
    const-string v9, "EventLog"

    #@6d
    const-string v11, "Error reading /system/etc/event-log-tags"

    #@6f
    invoke-static {v9, v11, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_72
    .catchall {:try_start_6b .. :try_end_72} :catchall_ce

    #@72
    .line 254
    if-eqz v6, :cond_b

    #@74
    :try_start_74
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_77
    .catchall {:try_start_74 .. :try_end_77} :catchall_bd
    .catch Ljava/io/IOException; {:try_start_74 .. :try_end_77} :catch_78

    #@77
    goto :goto_b

    #@78
    :catch_78
    move-exception v9

    #@79
    goto :goto_b

    #@7a
    .line 242
    .end local v1           #e:Ljava/io/IOException;
    .end local v6           #reader:Ljava/io/BufferedReader;
    .restart local v2       #line:Ljava/lang/String;
    .restart local v3       #m:Ljava/util/regex/Matcher;
    .restart local v7       #reader:Ljava/io/BufferedReader;
    :cond_7a
    const/4 v9, 0x1

    #@7b
    :try_start_7b
    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@7e
    move-result-object v9

    #@7f
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@82
    move-result v5

    #@83
    .line 243
    .local v5, num:I
    const/4 v9, 0x2

    #@84
    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@87
    move-result-object v4

    #@88
    .line 244
    .local v4, name:Ljava/lang/String;
    sget-object v9, Landroid/util/EventLog;->sTagCodes:Ljava/util/HashMap;

    #@8a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8d
    move-result-object v11

    #@8e
    invoke-virtual {v9, v4, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@91
    .line 245
    sget-object v9, Landroid/util/EventLog;->sTagNames:Ljava/util/HashMap;

    #@93
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@96
    move-result-object v11

    #@97
    invoke-virtual {v9, v11, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_9a
    .catchall {:try_start_7b .. :try_end_9a} :catchall_b5
    .catch Ljava/lang/NumberFormatException; {:try_start_7b .. :try_end_9a} :catch_9b
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_9a} :catch_69

    #@9a
    goto :goto_36

    #@9b
    .line 246
    .end local v4           #name:Ljava/lang/String;
    .end local v5           #num:I
    :catch_9b
    move-exception v1

    #@9c
    .line 247
    .local v1, e:Ljava/lang/NumberFormatException;
    :try_start_9c
    const-string v9, "EventLog"

    #@9e
    new-instance v11, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v12, "Error in /system/etc/event-log-tags: "

    #@a5
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v11

    #@a9
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v11

    #@ad
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v11

    #@b1
    invoke-static {v9, v11, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b4
    .catchall {:try_start_9c .. :try_end_b4} :catchall_b5
    .catch Ljava/io/IOException; {:try_start_9c .. :try_end_b4} :catch_69

    #@b4
    goto :goto_36

    #@b5
    .line 254
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .end local v2           #line:Ljava/lang/String;
    .end local v3           #m:Ljava/util/regex/Matcher;
    :catchall_b5
    move-exception v9

    #@b6
    move-object v6, v7

    #@b7
    .end local v7           #reader:Ljava/io/BufferedReader;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    :goto_b7
    if-eqz v6, :cond_bc

    #@b9
    :try_start_b9
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_bc
    .catchall {:try_start_b9 .. :try_end_bc} :catchall_bd
    .catch Ljava/io/IOException; {:try_start_b9 .. :try_end_bc} :catch_cc

    #@bc
    :cond_bc
    :goto_bc
    :try_start_bc
    throw v9
    :try_end_bd
    .catchall {:try_start_bc .. :try_end_bd} :catchall_bd

    #@bd
    .line 220
    .end local v6           #reader:Ljava/io/BufferedReader;
    .end local v8           #tag:Ljava/util/regex/Pattern;
    :catchall_bd
    move-exception v9

    #@be
    monitor-exit v10

    #@bf
    throw v9

    #@c0
    .line 254
    .restart local v2       #line:Ljava/lang/String;
    .restart local v7       #reader:Ljava/io/BufferedReader;
    .restart local v8       #tag:Ljava/util/regex/Pattern;
    :cond_c0
    if-eqz v7, :cond_c5

    #@c2
    :try_start_c2
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_c5
    .catchall {:try_start_c2 .. :try_end_c5} :catchall_bd
    .catch Ljava/io/IOException; {:try_start_c2 .. :try_end_c5} :catch_c8

    #@c5
    :cond_c5
    move-object v6, v7

    #@c6
    .end local v7           #reader:Ljava/io/BufferedReader;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    goto/16 :goto_b

    #@c8
    .end local v6           #reader:Ljava/io/BufferedReader;
    .restart local v7       #reader:Ljava/io/BufferedReader;
    :catch_c8
    move-exception v9

    #@c9
    move-object v6, v7

    #@ca
    .line 255
    .end local v7           #reader:Ljava/io/BufferedReader;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    goto/16 :goto_b

    #@cc
    .line 254
    .end local v2           #line:Ljava/lang/String;
    :catch_cc
    move-exception v11

    #@cd
    goto :goto_bc

    #@ce
    :catchall_ce
    move-exception v9

    #@cf
    goto :goto_b7

    #@d0
    .line 250
    :catch_d0
    move-exception v1

    #@d1
    goto :goto_6b
.end method

.method public static native writeEvent(II)I
.end method

.method public static native writeEvent(IJ)I
.end method

.method public static native writeEvent(ILjava/lang/String;)I
.end method

.method public static varargs native writeEvent(I[Ljava/lang/Object;)I
.end method
