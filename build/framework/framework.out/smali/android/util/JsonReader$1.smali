.class synthetic Landroid/util/JsonReader$1;
.super Ljava/lang/Object;
.source "JsonReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/util/JsonReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$android$util$JsonScope:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 331
    invoke-static {}, Landroid/util/JsonScope;->values()[Landroid/util/JsonScope;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Landroid/util/JsonReader$1;->$SwitchMap$android$util$JsonScope:[I

    #@9
    :try_start_9
    sget-object v0, Landroid/util/JsonReader$1;->$SwitchMap$android$util$JsonScope:[I

    #@b
    sget-object v1, Landroid/util/JsonScope;->EMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@d
    invoke-virtual {v1}, Landroid/util/JsonScope;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_71

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Landroid/util/JsonReader$1;->$SwitchMap$android$util$JsonScope:[I

    #@16
    sget-object v1, Landroid/util/JsonScope;->EMPTY_ARRAY:Landroid/util/JsonScope;

    #@18
    invoke-virtual {v1}, Landroid/util/JsonScope;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_6f

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Landroid/util/JsonReader$1;->$SwitchMap$android$util$JsonScope:[I

    #@21
    sget-object v1, Landroid/util/JsonScope;->NONEMPTY_ARRAY:Landroid/util/JsonScope;

    #@23
    invoke-virtual {v1}, Landroid/util/JsonScope;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_6d

    #@2a
    :goto_2a
    :try_start_2a
    sget-object v0, Landroid/util/JsonReader$1;->$SwitchMap$android$util$JsonScope:[I

    #@2c
    sget-object v1, Landroid/util/JsonScope;->EMPTY_OBJECT:Landroid/util/JsonScope;

    #@2e
    invoke-virtual {v1}, Landroid/util/JsonScope;->ordinal()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_6b

    #@35
    :goto_35
    :try_start_35
    sget-object v0, Landroid/util/JsonReader$1;->$SwitchMap$android$util$JsonScope:[I

    #@37
    sget-object v1, Landroid/util/JsonScope;->DANGLING_NAME:Landroid/util/JsonScope;

    #@39
    invoke-virtual {v1}, Landroid/util/JsonScope;->ordinal()I

    #@3c
    move-result v1

    #@3d
    const/4 v2, 0x5

    #@3e
    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_40} :catch_69

    #@40
    :goto_40
    :try_start_40
    sget-object v0, Landroid/util/JsonReader$1;->$SwitchMap$android$util$JsonScope:[I

    #@42
    sget-object v1, Landroid/util/JsonScope;->NONEMPTY_OBJECT:Landroid/util/JsonScope;

    #@44
    invoke-virtual {v1}, Landroid/util/JsonScope;->ordinal()I

    #@47
    move-result v1

    #@48
    const/4 v2, 0x6

    #@49
    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_4b} :catch_67

    #@4b
    :goto_4b
    :try_start_4b
    sget-object v0, Landroid/util/JsonReader$1;->$SwitchMap$android$util$JsonScope:[I

    #@4d
    sget-object v1, Landroid/util/JsonScope;->NONEMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@4f
    invoke-virtual {v1}, Landroid/util/JsonScope;->ordinal()I

    #@52
    move-result v1

    #@53
    const/4 v2, 0x7

    #@54
    aput v2, v0, v1
    :try_end_56
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4b .. :try_end_56} :catch_65

    #@56
    :goto_56
    :try_start_56
    sget-object v0, Landroid/util/JsonReader$1;->$SwitchMap$android$util$JsonScope:[I

    #@58
    sget-object v1, Landroid/util/JsonScope;->CLOSED:Landroid/util/JsonScope;

    #@5a
    invoke-virtual {v1}, Landroid/util/JsonScope;->ordinal()I

    #@5d
    move-result v1

    #@5e
    const/16 v2, 0x8

    #@60
    aput v2, v0, v1
    :try_end_62
    .catch Ljava/lang/NoSuchFieldError; {:try_start_56 .. :try_end_62} :catch_63

    #@62
    :goto_62
    return-void

    #@63
    :catch_63
    move-exception v0

    #@64
    goto :goto_62

    #@65
    :catch_65
    move-exception v0

    #@66
    goto :goto_56

    #@67
    :catch_67
    move-exception v0

    #@68
    goto :goto_4b

    #@69
    :catch_69
    move-exception v0

    #@6a
    goto :goto_40

    #@6b
    :catch_6b
    move-exception v0

    #@6c
    goto :goto_35

    #@6d
    :catch_6d
    move-exception v0

    #@6e
    goto :goto_2a

    #@6f
    :catch_6f
    move-exception v0

    #@70
    goto :goto_1f

    #@71
    :catch_71
    move-exception v0

    #@72
    goto :goto_14
.end method
