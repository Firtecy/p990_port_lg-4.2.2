.class public Landroid/util/SparseLongArray;
.super Ljava/lang/Object;
.source "SparseLongArray.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private mKeys:[I

.field private mSize:I

.field private mValues:[J


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 38
    const/16 v0, 0xa

    #@2
    invoke-direct {p0, v0}, Landroid/util/SparseLongArray;-><init>(I)V

    #@5
    .line 39
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "initialCapacity"

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 47
    invoke-static {p1}, Lcom/android/internal/util/ArrayUtils;->idealLongArraySize(I)I

    #@6
    move-result p1

    #@7
    .line 49
    new-array v0, p1, [I

    #@9
    iput-object v0, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@b
    .line 50
    new-array v0, p1, [J

    #@d
    iput-object v0, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@f
    .line 51
    const/4 v0, 0x0

    #@10
    iput v0, p0, Landroid/util/SparseLongArray;->mSize:I

    #@12
    .line 52
    return-void
.end method

.method private static binarySearch([IIIJ)I
    .registers 10
    .parameter "a"
    .parameter "start"
    .parameter "len"
    .parameter "key"

    #@0
    .prologue
    .line 229
    add-int v1, p1, p2

    #@2
    .local v1, high:I
    add-int/lit8 v2, p1, -0x1

    #@4
    .line 231
    .local v2, low:I
    :goto_4
    sub-int v3, v1, v2

    #@6
    const/4 v4, 0x1

    #@7
    if-le v3, v4, :cond_18

    #@9
    .line 232
    add-int v3, v1, v2

    #@b
    div-int/lit8 v0, v3, 0x2

    #@d
    .line 234
    .local v0, guess:I
    aget v3, p0, v0

    #@f
    int-to-long v3, v3

    #@10
    cmp-long v3, v3, p3

    #@12
    if-gez v3, :cond_16

    #@14
    .line 235
    move v2, v0

    #@15
    goto :goto_4

    #@16
    .line 237
    :cond_16
    move v1, v0

    #@17
    goto :goto_4

    #@18
    .line 240
    .end local v0           #guess:I
    :cond_18
    add-int v3, p1, p2

    #@1a
    if-ne v1, v3, :cond_21

    #@1c
    .line 241
    add-int v3, p1, p2

    #@1e
    xor-int/lit8 v1, v3, -0x1

    #@20
    .line 245
    .end local v1           #high:I
    :cond_20
    :goto_20
    return v1

    #@21
    .line 242
    .restart local v1       #high:I
    :cond_21
    aget v3, p0, v1

    #@23
    int-to-long v3, v3

    #@24
    cmp-long v3, v3, p3

    #@26
    if-eqz v3, :cond_20

    #@28
    .line 245
    xor-int/lit8 v1, v1, -0x1

    #@2a
    goto :goto_20
.end method

.method private growKeyAndValueArrays(I)V
    .registers 8
    .parameter "minNeededSize"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 216
    invoke-static {p1}, Lcom/android/internal/util/ArrayUtils;->idealLongArraySize(I)I

    #@4
    move-result v0

    #@5
    .line 218
    .local v0, n:I
    new-array v1, v0, [I

    #@7
    .line 219
    .local v1, nkeys:[I
    new-array v2, v0, [J

    #@9
    .line 221
    .local v2, nvalues:[J
    iget-object v3, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@b
    iget-object v4, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@d
    array-length v4, v4

    #@e
    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@11
    .line 222
    iget-object v3, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@13
    iget-object v4, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@15
    array-length v4, v4

    #@16
    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 224
    iput-object v1, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@1b
    .line 225
    iput-object v2, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@1d
    .line 226
    return-void
.end method


# virtual methods
.method public append(IJ)V
    .registers 7
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 200
    iget v1, p0, Landroid/util/SparseLongArray;->mSize:I

    #@2
    if-eqz v1, :cond_12

    #@4
    iget-object v1, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@6
    iget v2, p0, Landroid/util/SparseLongArray;->mSize:I

    #@8
    add-int/lit8 v2, v2, -0x1

    #@a
    aget v1, v1, v2

    #@c
    if-gt p1, v1, :cond_12

    #@e
    .line 201
    invoke-virtual {p0, p1, p2, p3}, Landroid/util/SparseLongArray;->put(IJ)V

    #@11
    .line 213
    :goto_11
    return-void

    #@12
    .line 205
    :cond_12
    iget v0, p0, Landroid/util/SparseLongArray;->mSize:I

    #@14
    .line 206
    .local v0, pos:I
    iget-object v1, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@16
    array-length v1, v1

    #@17
    if-lt v0, v1, :cond_1e

    #@19
    .line 207
    add-int/lit8 v1, v0, 0x1

    #@1b
    invoke-direct {p0, v1}, Landroid/util/SparseLongArray;->growKeyAndValueArrays(I)V

    #@1e
    .line 210
    :cond_1e
    iget-object v1, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@20
    aput p1, v1, v0

    #@22
    .line 211
    iget-object v1, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@24
    aput-wide p2, v1, v0

    #@26
    .line 212
    add-int/lit8 v1, v0, 0x1

    #@28
    iput v1, p0, Landroid/util/SparseLongArray;->mSize:I

    #@2a
    goto :goto_11
.end method

.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 192
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/util/SparseLongArray;->mSize:I

    #@3
    .line 193
    return-void
.end method

.method public clone()Landroid/util/SparseLongArray;
    .registers 4

    #@0
    .prologue
    .line 56
    const/4 v1, 0x0

    #@1
    .line 58
    .local v1, clone:Landroid/util/SparseLongArray;
    :try_start_1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    #@4
    move-result-object v2

    #@5
    move-object v0, v2

    #@6
    check-cast v0, Landroid/util/SparseLongArray;

    #@8
    move-object v1, v0

    #@9
    .line 59
    iget-object v2, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@b
    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, [I

    #@11
    iput-object v2, v1, Landroid/util/SparseLongArray;->mKeys:[I

    #@13
    .line 60
    iget-object v2, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@15
    invoke-virtual {v2}, [J->clone()Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, [J

    #@1b
    iput-object v2, v1, Landroid/util/SparseLongArray;->mValues:[J
    :try_end_1d
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1d} :catch_1e

    #@1d
    .line 64
    :goto_1d
    return-object v1

    #@1e
    .line 61
    :catch_1e
    move-exception v2

    #@1f
    goto :goto_1d
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 28
    invoke-virtual {p0}, Landroid/util/SparseLongArray;->clone()Landroid/util/SparseLongArray;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public delete(I)V
    .registers 8
    .parameter "key"

    #@0
    .prologue
    .line 93
    iget-object v1, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@2
    const/4 v2, 0x0

    #@3
    iget v3, p0, Landroid/util/SparseLongArray;->mSize:I

    #@5
    int-to-long v4, p1

    #@6
    invoke-static {v1, v2, v3, v4, v5}, Landroid/util/SparseLongArray;->binarySearch([IIIJ)I

    #@9
    move-result v0

    #@a
    .line 95
    .local v0, i:I
    if-ltz v0, :cond_f

    #@c
    .line 96
    invoke-virtual {p0, v0}, Landroid/util/SparseLongArray;->removeAt(I)V

    #@f
    .line 98
    :cond_f
    return-void
.end method

.method public get(I)J
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 72
    const-wide/16 v0, 0x0

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/util/SparseLongArray;->get(IJ)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public get(IJ)J
    .registers 10
    .parameter "key"
    .parameter "valueIfKeyNotFound"

    #@0
    .prologue
    .line 80
    iget-object v1, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@2
    const/4 v2, 0x0

    #@3
    iget v3, p0, Landroid/util/SparseLongArray;->mSize:I

    #@5
    int-to-long v4, p1

    #@6
    invoke-static {v1, v2, v3, v4, v5}, Landroid/util/SparseLongArray;->binarySearch([IIIJ)I

    #@9
    move-result v0

    #@a
    .line 82
    .local v0, i:I
    if-gez v0, :cond_d

    #@c
    .line 85
    .end local p2
    :goto_c
    return-wide p2

    #@d
    .restart local p2
    :cond_d
    iget-object v1, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@f
    aget-wide p2, v1, v0

    #@11
    goto :goto_c
.end method

.method public indexOfKey(I)I
    .registers 7
    .parameter "key"

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@2
    const/4 v1, 0x0

    #@3
    iget v2, p0, Landroid/util/SparseLongArray;->mSize:I

    #@5
    int-to-long v3, p1

    #@6
    invoke-static {v0, v1, v2, v3, v4}, Landroid/util/SparseLongArray;->binarySearch([IIIJ)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public indexOfValue(J)I
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 181
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Landroid/util/SparseLongArray;->mSize:I

    #@3
    if-ge v0, v1, :cond_11

    #@5
    .line 182
    iget-object v1, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@7
    aget-wide v1, v1, v0

    #@9
    cmp-long v1, v1, p1

    #@b
    if-nez v1, :cond_e

    #@d
    .line 185
    .end local v0           #i:I
    :goto_d
    return v0

    #@e
    .line 181
    .restart local v0       #i:I
    :cond_e
    add-int/lit8 v0, v0, 0x1

    #@10
    goto :goto_1

    #@11
    .line 185
    :cond_11
    const/4 v0, -0x1

    #@12
    goto :goto_d
.end method

.method public keyAt(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@2
    aget v0, v0, p1

    #@4
    return v0
.end method

.method public put(IJ)V
    .registers 10
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 115
    iget-object v1, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@2
    const/4 v2, 0x0

    #@3
    iget v3, p0, Landroid/util/SparseLongArray;->mSize:I

    #@5
    int-to-long v4, p1

    #@6
    invoke-static {v1, v2, v3, v4, v5}, Landroid/util/SparseLongArray;->binarySearch([IIIJ)I

    #@9
    move-result v0

    #@a
    .line 117
    .local v0, i:I
    if-ltz v0, :cond_11

    #@c
    .line 118
    iget-object v1, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@e
    aput-wide p2, v1, v0

    #@10
    .line 135
    :goto_10
    return-void

    #@11
    .line 120
    :cond_11
    xor-int/lit8 v0, v0, -0x1

    #@13
    .line 122
    iget v1, p0, Landroid/util/SparseLongArray;->mSize:I

    #@15
    iget-object v2, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@17
    array-length v2, v2

    #@18
    if-lt v1, v2, :cond_21

    #@1a
    .line 123
    iget v1, p0, Landroid/util/SparseLongArray;->mSize:I

    #@1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    invoke-direct {p0, v1}, Landroid/util/SparseLongArray;->growKeyAndValueArrays(I)V

    #@21
    .line 126
    :cond_21
    iget v1, p0, Landroid/util/SparseLongArray;->mSize:I

    #@23
    sub-int/2addr v1, v0

    #@24
    if-eqz v1, :cond_3e

    #@26
    .line 127
    iget-object v1, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@28
    iget-object v2, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@2a
    add-int/lit8 v3, v0, 0x1

    #@2c
    iget v4, p0, Landroid/util/SparseLongArray;->mSize:I

    #@2e
    sub-int/2addr v4, v0

    #@2f
    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@32
    .line 128
    iget-object v1, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@34
    iget-object v2, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@36
    add-int/lit8 v3, v0, 0x1

    #@38
    iget v4, p0, Landroid/util/SparseLongArray;->mSize:I

    #@3a
    sub-int/2addr v4, v0

    #@3b
    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3e
    .line 131
    :cond_3e
    iget-object v1, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@40
    aput p1, v1, v0

    #@42
    .line 132
    iget-object v1, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@44
    aput-wide p2, v1, v0

    #@46
    .line 133
    iget v1, p0, Landroid/util/SparseLongArray;->mSize:I

    #@48
    add-int/lit8 v1, v1, 0x1

    #@4a
    iput v1, p0, Landroid/util/SparseLongArray;->mSize:I

    #@4c
    goto :goto_10
.end method

.method public removeAt(I)V
    .registers 7
    .parameter "index"

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@2
    add-int/lit8 v1, p1, 0x1

    #@4
    iget-object v2, p0, Landroid/util/SparseLongArray;->mKeys:[I

    #@6
    iget v3, p0, Landroid/util/SparseLongArray;->mSize:I

    #@8
    add-int/lit8 v4, p1, 0x1

    #@a
    sub-int/2addr v3, v4

    #@b
    invoke-static {v0, v1, v2, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@e
    .line 105
    iget-object v0, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@10
    add-int/lit8 v1, p1, 0x1

    #@12
    iget-object v2, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@14
    iget v3, p0, Landroid/util/SparseLongArray;->mSize:I

    #@16
    add-int/lit8 v4, p1, 0x1

    #@18
    sub-int/2addr v3, v4

    #@19
    invoke-static {v0, v1, v2, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1c
    .line 106
    iget v0, p0, Landroid/util/SparseLongArray;->mSize:I

    #@1e
    add-int/lit8 v0, v0, -0x1

    #@20
    iput v0, p0, Landroid/util/SparseLongArray;->mSize:I

    #@22
    .line 107
    return-void
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 142
    iget v0, p0, Landroid/util/SparseLongArray;->mSize:I

    #@2
    return v0
.end method

.method public valueAt(I)J
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 160
    iget-object v0, p0, Landroid/util/SparseLongArray;->mValues:[J

    #@2
    aget-wide v0, v0, p1

    #@4
    return-wide v0
.end method
