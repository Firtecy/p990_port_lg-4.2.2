.class public Landroid/util/SparseBooleanArray;
.super Ljava/lang/Object;
.source "SparseBooleanArray.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private mKeys:[I

.field private mSize:I

.field private mValues:[Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 32
    const/16 v0, 0xa

    #@2
    invoke-direct {p0, v0}, Landroid/util/SparseBooleanArray;-><init>(I)V

    #@5
    .line 33
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "initialCapacity"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    invoke-static {p1}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@6
    move-result p1

    #@7
    .line 43
    new-array v0, p1, [I

    #@9
    iput-object v0, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@b
    .line 44
    new-array v0, p1, [Z

    #@d
    iput-object v0, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@f
    .line 45
    const/4 v0, 0x0

    #@10
    iput v0, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@12
    .line 46
    return-void
.end method

.method private static binarySearch([IIII)I
    .registers 9
    .parameter "a"
    .parameter "start"
    .parameter "len"
    .parameter "key"

    #@0
    .prologue
    .line 224
    add-int v1, p1, p2

    #@2
    .local v1, high:I
    add-int/lit8 v2, p1, -0x1

    #@4
    .line 226
    .local v2, low:I
    :goto_4
    sub-int v3, v1, v2

    #@6
    const/4 v4, 0x1

    #@7
    if-le v3, v4, :cond_15

    #@9
    .line 227
    add-int v3, v1, v2

    #@b
    div-int/lit8 v0, v3, 0x2

    #@d
    .line 229
    .local v0, guess:I
    aget v3, p0, v0

    #@f
    if-ge v3, p3, :cond_13

    #@11
    .line 230
    move v2, v0

    #@12
    goto :goto_4

    #@13
    .line 232
    :cond_13
    move v1, v0

    #@14
    goto :goto_4

    #@15
    .line 235
    .end local v0           #guess:I
    :cond_15
    add-int v3, p1, p2

    #@17
    if-ne v1, v3, :cond_1e

    #@19
    .line 236
    add-int v3, p1, p2

    #@1b
    xor-int/lit8 v1, v3, -0x1

    #@1d
    .line 240
    .end local v1           #high:I
    :cond_1d
    :goto_1d
    return v1

    #@1e
    .line 237
    .restart local v1       #high:I
    :cond_1e
    aget v3, p0, v1

    #@20
    if-eq v3, p3, :cond_1d

    #@22
    .line 240
    xor-int/lit8 v1, v1, -0x1

    #@24
    goto :goto_1d
.end method


# virtual methods
.method public append(IZ)V
    .registers 10
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 198
    iget v4, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@3
    if-eqz v4, :cond_13

    #@5
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@7
    iget v5, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@9
    add-int/lit8 v5, v5, -0x1

    #@b
    aget v4, v4, v5

    #@d
    if-gt p1, v4, :cond_13

    #@f
    .line 199
    invoke-virtual {p0, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@12
    .line 221
    :goto_12
    return-void

    #@13
    .line 203
    :cond_13
    iget v3, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@15
    .line 204
    .local v3, pos:I
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@17
    array-length v4, v4

    #@18
    if-lt v3, v4, :cond_38

    #@1a
    .line 205
    add-int/lit8 v4, v3, 0x1

    #@1c
    invoke-static {v4}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@1f
    move-result v0

    #@20
    .line 207
    .local v0, n:I
    new-array v1, v0, [I

    #@22
    .line 208
    .local v1, nkeys:[I
    new-array v2, v0, [Z

    #@24
    .line 211
    .local v2, nvalues:[Z
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@26
    iget-object v5, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@28
    array-length v5, v5

    #@29
    invoke-static {v4, v6, v1, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2c
    .line 212
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@2e
    iget-object v5, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@30
    array-length v5, v5

    #@31
    invoke-static {v4, v6, v2, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@34
    .line 214
    iput-object v1, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@36
    .line 215
    iput-object v2, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@38
    .line 218
    .end local v0           #n:I
    .end local v1           #nkeys:[I
    .end local v2           #nvalues:[Z
    :cond_38
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@3a
    aput p1, v4, v3

    #@3c
    .line 219
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@3e
    aput-boolean p2, v4, v3

    #@40
    .line 220
    add-int/lit8 v4, v3, 0x1

    #@42
    iput v4, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@44
    goto :goto_12
.end method

.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 190
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@3
    .line 191
    return-void
.end method

.method public clone()Landroid/util/SparseBooleanArray;
    .registers 4

    #@0
    .prologue
    .line 50
    const/4 v1, 0x0

    #@1
    .line 52
    .local v1, clone:Landroid/util/SparseBooleanArray;
    :try_start_1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    #@4
    move-result-object v2

    #@5
    move-object v0, v2

    #@6
    check-cast v0, Landroid/util/SparseBooleanArray;

    #@8
    move-object v1, v0

    #@9
    .line 53
    iget-object v2, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@b
    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, [I

    #@11
    iput-object v2, v1, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@13
    .line 54
    iget-object v2, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@15
    invoke-virtual {v2}, [Z->clone()Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, [Z

    #@1b
    iput-object v2, v1, Landroid/util/SparseBooleanArray;->mValues:[Z
    :try_end_1d
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1d} :catch_1e

    #@1d
    .line 58
    :goto_1d
    return-object v1

    #@1e
    .line 55
    :catch_1e
    move-exception v2

    #@1f
    goto :goto_1d
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 27
    invoke-virtual {p0}, Landroid/util/SparseBooleanArray;->clone()Landroid/util/SparseBooleanArray;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public delete(I)V
    .registers 8
    .parameter "key"

    #@0
    .prologue
    .line 87
    iget-object v1, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@2
    const/4 v2, 0x0

    #@3
    iget v3, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@5
    invoke-static {v1, v2, v3, p1}, Landroid/util/SparseBooleanArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    .line 89
    .local v0, i:I
    if-ltz v0, :cond_2d

    #@b
    .line 90
    iget-object v1, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@d
    add-int/lit8 v2, v0, 0x1

    #@f
    iget-object v3, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@11
    iget v4, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@13
    add-int/lit8 v5, v0, 0x1

    #@15
    sub-int/2addr v4, v5

    #@16
    invoke-static {v1, v2, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 91
    iget-object v1, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@1b
    add-int/lit8 v2, v0, 0x1

    #@1d
    iget-object v3, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@1f
    iget v4, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@21
    add-int/lit8 v5, v0, 0x1

    #@23
    sub-int/2addr v4, v5

    #@24
    invoke-static {v1, v2, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@27
    .line 92
    iget v1, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@29
    add-int/lit8 v1, v1, -0x1

    #@2b
    iput v1, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@2d
    .line 94
    :cond_2d
    return-void
.end method

.method public get(I)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 66
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public get(IZ)Z
    .registers 7
    .parameter "key"
    .parameter "valueIfKeyNotFound"

    #@0
    .prologue
    .line 74
    iget-object v1, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@2
    const/4 v2, 0x0

    #@3
    iget v3, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@5
    invoke-static {v1, v2, v3, p1}, Landroid/util/SparseBooleanArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    .line 76
    .local v0, i:I
    if-gez v0, :cond_c

    #@b
    .line 79
    .end local p2
    :goto_b
    return p2

    #@c
    .restart local p2
    :cond_c
    iget-object v1, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@e
    aget-boolean p2, v1, v0

    #@10
    goto :goto_b
.end method

.method public indexOfKey(I)I
    .registers 5
    .parameter "key"

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@2
    const/4 v1, 0x0

    #@3
    iget v2, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@5
    invoke-static {v0, v1, v2, p1}, Landroid/util/SparseBooleanArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public indexOfValue(Z)I
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 179
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@3
    if-ge v0, v1, :cond_f

    #@5
    .line 180
    iget-object v1, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@7
    aget-boolean v1, v1, v0

    #@9
    if-ne v1, p1, :cond_c

    #@b
    .line 183
    .end local v0           #i:I
    :goto_b
    return v0

    #@c
    .line 179
    .restart local v0       #i:I
    :cond_c
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_1

    #@f
    .line 183
    :cond_f
    const/4 v0, -0x1

    #@10
    goto :goto_b
.end method

.method public keyAt(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 149
    iget-object v0, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@2
    aget v0, v0, p1

    #@4
    return v0
.end method

.method public put(IZ)V
    .registers 11
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 102
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@3
    iget v5, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@5
    invoke-static {v4, v6, v5, p1}, Landroid/util/SparseBooleanArray;->binarySearch([IIII)I

    #@8
    move-result v0

    #@9
    .line 104
    .local v0, i:I
    if-ltz v0, :cond_10

    #@b
    .line 105
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@d
    aput-boolean p2, v4, v0

    #@f
    .line 133
    :goto_f
    return-void

    #@10
    .line 107
    :cond_10
    xor-int/lit8 v0, v0, -0x1

    #@12
    .line 109
    iget v4, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@14
    iget-object v5, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@16
    array-length v5, v5

    #@17
    if-lt v4, v5, :cond_39

    #@19
    .line 110
    iget v4, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@1b
    add-int/lit8 v4, v4, 0x1

    #@1d
    invoke-static {v4}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@20
    move-result v1

    #@21
    .line 112
    .local v1, n:I
    new-array v2, v1, [I

    #@23
    .line 113
    .local v2, nkeys:[I
    new-array v3, v1, [Z

    #@25
    .line 116
    .local v3, nvalues:[Z
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@27
    iget-object v5, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@29
    array-length v5, v5

    #@2a
    invoke-static {v4, v6, v2, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2d
    .line 117
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@2f
    iget-object v5, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@31
    array-length v5, v5

    #@32
    invoke-static {v4, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@35
    .line 119
    iput-object v2, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@37
    .line 120
    iput-object v3, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@39
    .line 123
    .end local v1           #n:I
    .end local v2           #nkeys:[I
    .end local v3           #nvalues:[Z
    :cond_39
    iget v4, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@3b
    sub-int/2addr v4, v0

    #@3c
    if-eqz v4, :cond_56

    #@3e
    .line 125
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@40
    iget-object v5, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@42
    add-int/lit8 v6, v0, 0x1

    #@44
    iget v7, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@46
    sub-int/2addr v7, v0

    #@47
    invoke-static {v4, v0, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@4a
    .line 126
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@4c
    iget-object v5, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@4e
    add-int/lit8 v6, v0, 0x1

    #@50
    iget v7, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@52
    sub-int/2addr v7, v0

    #@53
    invoke-static {v4, v0, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@56
    .line 129
    :cond_56
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mKeys:[I

    #@58
    aput p1, v4, v0

    #@5a
    .line 130
    iget-object v4, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@5c
    aput-boolean p2, v4, v0

    #@5e
    .line 131
    iget v4, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@60
    add-int/lit8 v4, v4, 0x1

    #@62
    iput v4, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@64
    goto :goto_f
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 140
    iget v0, p0, Landroid/util/SparseBooleanArray;->mSize:I

    #@2
    return v0
.end method

.method public valueAt(I)Z
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Landroid/util/SparseBooleanArray;->mValues:[Z

    #@2
    aget-boolean v0, v0, p1

    #@4
    return v0
.end method
