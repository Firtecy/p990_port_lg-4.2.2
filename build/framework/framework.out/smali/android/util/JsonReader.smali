.class public final Landroid/util/JsonReader;
.super Ljava/lang/Object;
.source "JsonReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/util/JsonReader$1;
    }
.end annotation


# static fields
.field private static final FALSE:Ljava/lang/String; = "false"

.field private static final TRUE:Ljava/lang/String; = "true"


# instance fields
.field private final buffer:[C

.field private bufferStartColumn:I

.field private bufferStartLine:I

.field private final in:Ljava/io/Reader;

.field private lenient:Z

.field private limit:I

.field private name:Ljava/lang/String;

.field private pos:I

.field private skipping:Z

.field private final stack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/JsonScope;",
            ">;"
        }
    .end annotation
.end field

.field private final stringPool:Llibcore/internal/StringPool;

.field private token:Landroid/util/JsonToken;

.field private value:Ljava/lang/String;

.field private valueLength:I

.field private valuePos:I


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .registers 5
    .parameter "in"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 233
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 181
    new-instance v0, Llibcore/internal/StringPool;

    #@7
    invoke-direct {v0}, Llibcore/internal/StringPool;-><init>()V

    #@a
    iput-object v0, p0, Landroid/util/JsonReader;->stringPool:Llibcore/internal/StringPool;

    #@c
    .line 187
    iput-boolean v1, p0, Landroid/util/JsonReader;->lenient:Z

    #@e
    .line 195
    const/16 v0, 0x400

    #@10
    new-array v0, v0, [C

    #@12
    iput-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@14
    .line 196
    iput v1, p0, Landroid/util/JsonReader;->pos:I

    #@16
    .line 197
    iput v1, p0, Landroid/util/JsonReader;->limit:I

    #@18
    .line 202
    iput v2, p0, Landroid/util/JsonReader;->bufferStartLine:I

    #@1a
    .line 203
    iput v2, p0, Landroid/util/JsonReader;->bufferStartColumn:I

    #@1c
    .line 205
    new-instance v0, Ljava/util/ArrayList;

    #@1e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@21
    iput-object v0, p0, Landroid/util/JsonReader;->stack:Ljava/util/List;

    #@23
    .line 207
    sget-object v0, Landroid/util/JsonScope;->EMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@25
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->push(Landroid/util/JsonScope;)V

    #@28
    .line 228
    iput-boolean v1, p0, Landroid/util/JsonReader;->skipping:Z

    #@2a
    .line 234
    if-nez p1, :cond_34

    #@2c
    .line 235
    new-instance v0, Ljava/lang/NullPointerException;

    #@2e
    const-string v1, "in == null"

    #@30
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@33
    throw v0

    #@34
    .line 237
    :cond_34
    iput-object p1, p0, Landroid/util/JsonReader;->in:Ljava/io/Reader;

    #@36
    .line 238
    return-void
.end method

.method private advance()Landroid/util/JsonToken;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 371
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@4
    .line 373
    iget-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@6
    .line 374
    .local v0, result:Landroid/util/JsonToken;
    iput-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@8
    .line 375
    iput-object v1, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@a
    .line 376
    iput-object v1, p0, Landroid/util/JsonReader;->name:Ljava/lang/String;

    #@c
    .line 377
    return-object v0
.end method

.method private checkLenient()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 834
    iget-boolean v0, p0, Landroid/util/JsonReader;->lenient:Z

    #@2
    if-nez v0, :cond_b

    #@4
    .line 835
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    #@6
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@9
    move-result-object v0

    #@a
    throw v0

    #@b
    .line 837
    :cond_b
    return-void
.end method

.method private decodeLiteral()Landroid/util/JsonToken;
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v7, 0x55

    #@2
    const/16 v6, 0x45

    #@4
    const/4 v5, 0x4

    #@5
    const/16 v4, 0x6c

    #@7
    const/16 v3, 0x4c

    #@9
    .line 1066
    iget v0, p0, Landroid/util/JsonReader;->valuePos:I

    #@b
    const/4 v1, -0x1

    #@c
    if-ne v0, v1, :cond_11

    #@e
    .line 1068
    sget-object v0, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@10
    .line 1093
    :goto_10
    return-object v0

    #@11
    .line 1069
    :cond_11
    iget v0, p0, Landroid/util/JsonReader;->valueLength:I

    #@13
    if-ne v0, v5, :cond_6f

    #@15
    const/16 v0, 0x6e

    #@17
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@19
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@1b
    aget-char v1, v1, v2

    #@1d
    if-eq v0, v1, :cond_29

    #@1f
    const/16 v0, 0x4e

    #@21
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@23
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@25
    aget-char v1, v1, v2

    #@27
    if-ne v0, v1, :cond_6f

    #@29
    :cond_29
    const/16 v0, 0x75

    #@2b
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@2d
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@2f
    add-int/lit8 v2, v2, 0x1

    #@31
    aget-char v1, v1, v2

    #@33
    if-eq v0, v1, :cond_3f

    #@35
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@37
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@39
    add-int/lit8 v1, v1, 0x1

    #@3b
    aget-char v0, v0, v1

    #@3d
    if-ne v7, v0, :cond_6f

    #@3f
    :cond_3f
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@41
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@43
    add-int/lit8 v1, v1, 0x2

    #@45
    aget-char v0, v0, v1

    #@47
    if-eq v4, v0, :cond_53

    #@49
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@4b
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@4d
    add-int/lit8 v1, v1, 0x2

    #@4f
    aget-char v0, v0, v1

    #@51
    if-ne v3, v0, :cond_6f

    #@53
    :cond_53
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@55
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@57
    add-int/lit8 v1, v1, 0x3

    #@59
    aget-char v0, v0, v1

    #@5b
    if-eq v4, v0, :cond_67

    #@5d
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@5f
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@61
    add-int/lit8 v1, v1, 0x3

    #@63
    aget-char v0, v0, v1

    #@65
    if-ne v3, v0, :cond_6f

    #@67
    .line 1074
    :cond_67
    const-string/jumbo v0, "null"

    #@6a
    iput-object v0, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@6c
    .line 1075
    sget-object v0, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    #@6e
    goto :goto_10

    #@6f
    .line 1076
    :cond_6f
    iget v0, p0, Landroid/util/JsonReader;->valueLength:I

    #@71
    if-ne v0, v5, :cond_d4

    #@73
    const/16 v0, 0x74

    #@75
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@77
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@79
    aget-char v1, v1, v2

    #@7b
    if-eq v0, v1, :cond_87

    #@7d
    const/16 v0, 0x54

    #@7f
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@81
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@83
    aget-char v1, v1, v2

    #@85
    if-ne v0, v1, :cond_d4

    #@87
    :cond_87
    const/16 v0, 0x72

    #@89
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@8b
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@8d
    add-int/lit8 v2, v2, 0x1

    #@8f
    aget-char v1, v1, v2

    #@91
    if-eq v0, v1, :cond_9f

    #@93
    const/16 v0, 0x52

    #@95
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@97
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@99
    add-int/lit8 v2, v2, 0x1

    #@9b
    aget-char v1, v1, v2

    #@9d
    if-ne v0, v1, :cond_d4

    #@9f
    :cond_9f
    const/16 v0, 0x75

    #@a1
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@a3
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@a5
    add-int/lit8 v2, v2, 0x2

    #@a7
    aget-char v1, v1, v2

    #@a9
    if-eq v0, v1, :cond_b5

    #@ab
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@ad
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@af
    add-int/lit8 v1, v1, 0x2

    #@b1
    aget-char v0, v0, v1

    #@b3
    if-ne v7, v0, :cond_d4

    #@b5
    :cond_b5
    const/16 v0, 0x65

    #@b7
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@b9
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@bb
    add-int/lit8 v2, v2, 0x3

    #@bd
    aget-char v1, v1, v2

    #@bf
    if-eq v0, v1, :cond_cb

    #@c1
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@c3
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@c5
    add-int/lit8 v1, v1, 0x3

    #@c7
    aget-char v0, v0, v1

    #@c9
    if-ne v6, v0, :cond_d4

    #@cb
    .line 1081
    :cond_cb
    const-string/jumbo v0, "true"

    #@ce
    iput-object v0, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@d0
    .line 1082
    sget-object v0, Landroid/util/JsonToken;->BOOLEAN:Landroid/util/JsonToken;

    #@d2
    goto/16 :goto_10

    #@d4
    .line 1083
    :cond_d4
    iget v0, p0, Landroid/util/JsonReader;->valueLength:I

    #@d6
    const/4 v1, 0x5

    #@d7
    if-ne v0, v1, :cond_14f

    #@d9
    const/16 v0, 0x66

    #@db
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@dd
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@df
    aget-char v1, v1, v2

    #@e1
    if-eq v0, v1, :cond_ed

    #@e3
    const/16 v0, 0x46

    #@e5
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@e7
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@e9
    aget-char v1, v1, v2

    #@eb
    if-ne v0, v1, :cond_14f

    #@ed
    :cond_ed
    const/16 v0, 0x61

    #@ef
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@f1
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@f3
    add-int/lit8 v2, v2, 0x1

    #@f5
    aget-char v1, v1, v2

    #@f7
    if-eq v0, v1, :cond_105

    #@f9
    const/16 v0, 0x41

    #@fb
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@fd
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@ff
    add-int/lit8 v2, v2, 0x1

    #@101
    aget-char v1, v1, v2

    #@103
    if-ne v0, v1, :cond_14f

    #@105
    :cond_105
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@107
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@109
    add-int/lit8 v1, v1, 0x2

    #@10b
    aget-char v0, v0, v1

    #@10d
    if-eq v4, v0, :cond_119

    #@10f
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@111
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@113
    add-int/lit8 v1, v1, 0x2

    #@115
    aget-char v0, v0, v1

    #@117
    if-ne v3, v0, :cond_14f

    #@119
    :cond_119
    const/16 v0, 0x73

    #@11b
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@11d
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@11f
    add-int/lit8 v2, v2, 0x3

    #@121
    aget-char v1, v1, v2

    #@123
    if-eq v0, v1, :cond_131

    #@125
    const/16 v0, 0x53

    #@127
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@129
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@12b
    add-int/lit8 v2, v2, 0x3

    #@12d
    aget-char v1, v1, v2

    #@12f
    if-ne v0, v1, :cond_14f

    #@131
    :cond_131
    const/16 v0, 0x65

    #@133
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@135
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@137
    add-int/lit8 v2, v2, 0x4

    #@139
    aget-char v1, v1, v2

    #@13b
    if-eq v0, v1, :cond_147

    #@13d
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@13f
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@141
    add-int/lit8 v1, v1, 0x4

    #@143
    aget-char v0, v0, v1

    #@145
    if-ne v6, v0, :cond_14f

    #@147
    .line 1089
    :cond_147
    const-string v0, "false"

    #@149
    iput-object v0, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@14b
    .line 1090
    sget-object v0, Landroid/util/JsonToken;->BOOLEAN:Landroid/util/JsonToken;

    #@14d
    goto/16 :goto_10

    #@14f
    .line 1092
    :cond_14f
    iget-object v0, p0, Landroid/util/JsonReader;->stringPool:Llibcore/internal/StringPool;

    #@151
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@153
    iget v2, p0, Landroid/util/JsonReader;->valuePos:I

    #@155
    iget v3, p0, Landroid/util/JsonReader;->valueLength:I

    #@157
    invoke-virtual {v0, v1, v2, v3}, Llibcore/internal/StringPool;->get([CII)Ljava/lang/String;

    #@15a
    move-result-object v0

    #@15b
    iput-object v0, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@15d
    .line 1093
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@15f
    iget v1, p0, Landroid/util/JsonReader;->valuePos:I

    #@161
    iget v2, p0, Landroid/util/JsonReader;->valueLength:I

    #@163
    invoke-direct {p0, v0, v1, v2}, Landroid/util/JsonReader;->decodeNumber([CII)Landroid/util/JsonToken;

    #@166
    move-result-object v0

    #@167
    goto/16 :goto_10
.end method

.method private decodeNumber([CII)Landroid/util/JsonToken;
    .registers 10
    .parameter "chars"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    const/16 v5, 0x2d

    #@2
    const/16 v4, 0x39

    #@4
    const/16 v3, 0x30

    #@6
    .line 1104
    move v1, p2

    #@7
    .line 1105
    .local v1, i:I
    aget-char v0, p1, v1

    #@9
    .line 1107
    .local v0, c:I
    if-ne v0, v5, :cond_f

    #@b
    .line 1108
    add-int/lit8 v1, v1, 0x1

    #@d
    aget-char v0, p1, v1

    #@f
    .line 1111
    :cond_f
    if-ne v0, v3, :cond_26

    #@11
    .line 1112
    add-int/lit8 v1, v1, 0x1

    #@13
    aget-char v0, p1, v1

    #@15
    .line 1122
    :cond_15
    const/16 v2, 0x2e

    #@17
    if-ne v0, v2, :cond_3c

    #@19
    .line 1123
    add-int/lit8 v1, v1, 0x1

    #@1b
    aget-char v0, p1, v1

    #@1d
    .line 1124
    :goto_1d
    if-lt v0, v3, :cond_3c

    #@1f
    if-gt v0, v4, :cond_3c

    #@21
    .line 1125
    add-int/lit8 v1, v1, 0x1

    #@23
    aget-char v0, p1, v1

    #@25
    goto :goto_1d

    #@26
    .line 1113
    :cond_26
    const/16 v2, 0x31

    #@28
    if-lt v0, v2, :cond_39

    #@2a
    if-gt v0, v4, :cond_39

    #@2c
    .line 1114
    add-int/lit8 v1, v1, 0x1

    #@2e
    aget-char v0, p1, v1

    #@30
    .line 1115
    :goto_30
    if-lt v0, v3, :cond_15

    #@32
    if-gt v0, v4, :cond_15

    #@34
    .line 1116
    add-int/lit8 v1, v1, 0x1

    #@36
    aget-char v0, p1, v1

    #@38
    goto :goto_30

    #@39
    .line 1119
    :cond_39
    sget-object v2, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@3b
    .line 1147
    :goto_3b
    return-object v2

    #@3c
    .line 1129
    :cond_3c
    const/16 v2, 0x65

    #@3e
    if-eq v0, v2, :cond_44

    #@40
    const/16 v2, 0x45

    #@42
    if-ne v0, v2, :cond_66

    #@44
    .line 1130
    :cond_44
    add-int/lit8 v1, v1, 0x1

    #@46
    aget-char v0, p1, v1

    #@48
    .line 1131
    const/16 v2, 0x2b

    #@4a
    if-eq v0, v2, :cond_4e

    #@4c
    if-ne v0, v5, :cond_52

    #@4e
    .line 1132
    :cond_4e
    add-int/lit8 v1, v1, 0x1

    #@50
    aget-char v0, p1, v1

    #@52
    .line 1134
    :cond_52
    if-lt v0, v3, :cond_63

    #@54
    if-gt v0, v4, :cond_63

    #@56
    .line 1135
    add-int/lit8 v1, v1, 0x1

    #@58
    aget-char v0, p1, v1

    #@5a
    .line 1136
    :goto_5a
    if-lt v0, v3, :cond_66

    #@5c
    if-gt v0, v4, :cond_66

    #@5e
    .line 1137
    add-int/lit8 v1, v1, 0x1

    #@60
    aget-char v0, p1, v1

    #@62
    goto :goto_5a

    #@63
    .line 1140
    :cond_63
    sget-object v2, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@65
    goto :goto_3b

    #@66
    .line 1144
    :cond_66
    add-int v2, p2, p3

    #@68
    if-ne v1, v2, :cond_6d

    #@6a
    .line 1145
    sget-object v2, Landroid/util/JsonToken;->NUMBER:Landroid/util/JsonToken;

    #@6c
    goto :goto_3b

    #@6d
    .line 1147
    :cond_6d
    sget-object v2, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@6f
    goto :goto_3b
.end method

.method private expect(Landroid/util/JsonToken;)V
    .registers 5
    .parameter "expected"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 308
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@3
    .line 309
    iget-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@5
    if-eq v0, p1, :cond_2e

    #@7
    .line 310
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "Expected "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " but was "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v0

    #@2e
    .line 312
    :cond_2e
    invoke-direct {p0}, Landroid/util/JsonReader;->advance()Landroid/util/JsonToken;

    #@31
    .line 313
    return-void
.end method

.method private fillBuffer(I)Z
    .registers 11
    .parameter "minimum"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 721
    const/4 v0, 0x0

    #@3
    .local v0, i:I
    :goto_3
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@5
    if-ge v0, v4, :cond_21

    #@7
    .line 722
    iget-object v4, p0, Landroid/util/JsonReader;->buffer:[C

    #@9
    aget-char v4, v4, v0

    #@b
    const/16 v5, 0xa

    #@d
    if-ne v4, v5, :cond_1a

    #@f
    .line 723
    iget v4, p0, Landroid/util/JsonReader;->bufferStartLine:I

    #@11
    add-int/lit8 v4, v4, 0x1

    #@13
    iput v4, p0, Landroid/util/JsonReader;->bufferStartLine:I

    #@15
    .line 724
    iput v2, p0, Landroid/util/JsonReader;->bufferStartColumn:I

    #@17
    .line 721
    :goto_17
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_3

    #@1a
    .line 726
    :cond_1a
    iget v4, p0, Landroid/util/JsonReader;->bufferStartColumn:I

    #@1c
    add-int/lit8 v4, v4, 0x1

    #@1e
    iput v4, p0, Landroid/util/JsonReader;->bufferStartColumn:I

    #@20
    goto :goto_17

    #@21
    .line 730
    :cond_21
    iget v4, p0, Landroid/util/JsonReader;->limit:I

    #@23
    iget v5, p0, Landroid/util/JsonReader;->pos:I

    #@25
    if-eq v4, v5, :cond_79

    #@27
    .line 731
    iget v4, p0, Landroid/util/JsonReader;->limit:I

    #@29
    iget v5, p0, Landroid/util/JsonReader;->pos:I

    #@2b
    sub-int/2addr v4, v5

    #@2c
    iput v4, p0, Landroid/util/JsonReader;->limit:I

    #@2e
    .line 732
    iget-object v4, p0, Landroid/util/JsonReader;->buffer:[C

    #@30
    iget v5, p0, Landroid/util/JsonReader;->pos:I

    #@32
    iget-object v6, p0, Landroid/util/JsonReader;->buffer:[C

    #@34
    iget v7, p0, Landroid/util/JsonReader;->limit:I

    #@36
    invoke-static {v4, v5, v6, v3, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@39
    .line 737
    :goto_39
    iput v3, p0, Landroid/util/JsonReader;->pos:I

    #@3b
    .line 739
    :cond_3b
    iget-object v4, p0, Landroid/util/JsonReader;->in:Ljava/io/Reader;

    #@3d
    iget-object v5, p0, Landroid/util/JsonReader;->buffer:[C

    #@3f
    iget v6, p0, Landroid/util/JsonReader;->limit:I

    #@41
    iget-object v7, p0, Landroid/util/JsonReader;->buffer:[C

    #@43
    array-length v7, v7

    #@44
    iget v8, p0, Landroid/util/JsonReader;->limit:I

    #@46
    sub-int/2addr v7, v8

    #@47
    invoke-virtual {v4, v5, v6, v7}, Ljava/io/Reader;->read([CII)I

    #@4a
    move-result v1

    #@4b
    .local v1, total:I
    const/4 v4, -0x1

    #@4c
    if-eq v1, v4, :cond_7c

    #@4e
    .line 740
    iget v4, p0, Landroid/util/JsonReader;->limit:I

    #@50
    add-int/2addr v4, v1

    #@51
    iput v4, p0, Landroid/util/JsonReader;->limit:I

    #@53
    .line 743
    iget v4, p0, Landroid/util/JsonReader;->bufferStartLine:I

    #@55
    if-ne v4, v2, :cond_74

    #@57
    iget v4, p0, Landroid/util/JsonReader;->bufferStartColumn:I

    #@59
    if-ne v4, v2, :cond_74

    #@5b
    iget v4, p0, Landroid/util/JsonReader;->limit:I

    #@5d
    if-lez v4, :cond_74

    #@5f
    iget-object v4, p0, Landroid/util/JsonReader;->buffer:[C

    #@61
    aget-char v4, v4, v3

    #@63
    const v5, 0xfeff

    #@66
    if-ne v4, v5, :cond_74

    #@68
    .line 745
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@6a
    add-int/lit8 v4, v4, 0x1

    #@6c
    iput v4, p0, Landroid/util/JsonReader;->pos:I

    #@6e
    .line 746
    iget v4, p0, Landroid/util/JsonReader;->bufferStartColumn:I

    #@70
    add-int/lit8 v4, v4, -0x1

    #@72
    iput v4, p0, Landroid/util/JsonReader;->bufferStartColumn:I

    #@74
    .line 749
    :cond_74
    iget v4, p0, Landroid/util/JsonReader;->limit:I

    #@76
    if-lt v4, p1, :cond_3b

    #@78
    .line 753
    :goto_78
    return v2

    #@79
    .line 734
    .end local v1           #total:I
    :cond_79
    iput v3, p0, Landroid/util/JsonReader;->limit:I

    #@7b
    goto :goto_39

    #@7c
    .restart local v1       #total:I
    :cond_7c
    move v2, v3

    #@7d
    .line 753
    goto :goto_78
.end method

.method private getColumnNumber()I
    .registers 5

    #@0
    .prologue
    .line 767
    iget v1, p0, Landroid/util/JsonReader;->bufferStartColumn:I

    #@2
    .line 768
    .local v1, result:I
    const/4 v0, 0x0

    #@3
    .local v0, i:I
    :goto_3
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@5
    if-ge v0, v2, :cond_16

    #@7
    .line 769
    iget-object v2, p0, Landroid/util/JsonReader;->buffer:[C

    #@9
    aget-char v2, v2, v0

    #@b
    const/16 v3, 0xa

    #@d
    if-ne v2, v3, :cond_13

    #@f
    .line 770
    const/4 v1, 0x1

    #@10
    .line 768
    :goto_10
    add-int/lit8 v0, v0, 0x1

    #@12
    goto :goto_3

    #@13
    .line 772
    :cond_13
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_10

    #@16
    .line 775
    :cond_16
    return v1
.end method

.method private getLineNumber()I
    .registers 5

    #@0
    .prologue
    .line 757
    iget v1, p0, Landroid/util/JsonReader;->bufferStartLine:I

    #@2
    .line 758
    .local v1, result:I
    const/4 v0, 0x0

    #@3
    .local v0, i:I
    :goto_3
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@5
    if-ge v0, v2, :cond_14

    #@7
    .line 759
    iget-object v2, p0, Landroid/util/JsonReader;->buffer:[C

    #@9
    aget-char v2, v2, v0

    #@b
    const/16 v3, 0xa

    #@d
    if-ne v2, v3, :cond_11

    #@f
    .line 760
    add-int/lit8 v1, v1, 0x1

    #@11
    .line 758
    :cond_11
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_3

    #@14
    .line 763
    :cond_14
    return v1
.end method

.method private getSnippet()Ljava/lang/CharSequence;
    .registers 7

    #@0
    .prologue
    const/16 v5, 0x14

    #@2
    .line 1161
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 1162
    .local v2, snippet:Ljava/lang/StringBuilder;
    iget v3, p0, Landroid/util/JsonReader;->pos:I

    #@9
    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    #@c
    move-result v1

    #@d
    .line 1163
    .local v1, beforePos:I
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@f
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@11
    sub-int/2addr v4, v1

    #@12
    invoke-virtual {v2, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@15
    .line 1164
    iget v3, p0, Landroid/util/JsonReader;->limit:I

    #@17
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@19
    sub-int/2addr v3, v4

    #@1a
    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    #@1d
    move-result v0

    #@1e
    .line 1165
    .local v0, afterPos:I
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@20
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@22
    invoke-virtual {v2, v3, v4, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@25
    .line 1166
    return-object v2
.end method

.method private nextInArray(Z)Landroid/util/JsonToken;
    .registers 3
    .parameter "firstElement"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 583
    if-eqz p1, :cond_19

    #@2
    .line 584
    sget-object v0, Landroid/util/JsonScope;->NONEMPTY_ARRAY:Landroid/util/JsonScope;

    #@4
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->replaceTop(Landroid/util/JsonScope;)V

    #@7
    .line 600
    :goto_7
    :sswitch_7
    invoke-direct {p0}, Landroid/util/JsonReader;->nextNonWhitespace()I

    #@a
    move-result v0

    #@b
    sparse-switch v0, :sswitch_data_50

    #@e
    .line 615
    iget v0, p0, Landroid/util/JsonReader;->pos:I

    #@10
    add-int/lit8 v0, v0, -0x1

    #@12
    iput v0, p0, Landroid/util/JsonReader;->pos:I

    #@14
    .line 616
    invoke-direct {p0}, Landroid/util/JsonReader;->nextValue()Landroid/util/JsonToken;

    #@17
    move-result-object v0

    #@18
    :goto_18
    return-object v0

    #@19
    .line 587
    :cond_19
    invoke-direct {p0}, Landroid/util/JsonReader;->nextNonWhitespace()I

    #@1c
    move-result v0

    #@1d
    sparse-switch v0, :sswitch_data_5e

    #@20
    .line 596
    const-string v0, "Unterminated array"

    #@22
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@25
    move-result-object v0

    #@26
    throw v0

    #@27
    .line 589
    :sswitch_27
    invoke-direct {p0}, Landroid/util/JsonReader;->pop()Landroid/util/JsonScope;

    #@2a
    .line 590
    sget-object v0, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    #@2c
    iput-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@2e
    goto :goto_18

    #@2f
    .line 592
    :sswitch_2f
    invoke-direct {p0}, Landroid/util/JsonReader;->checkLenient()V

    #@32
    goto :goto_7

    #@33
    .line 602
    :sswitch_33
    if-eqz p1, :cond_3d

    #@35
    .line 603
    invoke-direct {p0}, Landroid/util/JsonReader;->pop()Landroid/util/JsonScope;

    #@38
    .line 604
    sget-object v0, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    #@3a
    iput-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@3c
    goto :goto_18

    #@3d
    .line 610
    :cond_3d
    :sswitch_3d
    invoke-direct {p0}, Landroid/util/JsonReader;->checkLenient()V

    #@40
    .line 611
    iget v0, p0, Landroid/util/JsonReader;->pos:I

    #@42
    add-int/lit8 v0, v0, -0x1

    #@44
    iput v0, p0, Landroid/util/JsonReader;->pos:I

    #@46
    .line 612
    const-string/jumbo v0, "null"

    #@49
    iput-object v0, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@4b
    .line 613
    sget-object v0, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    #@4d
    iput-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@4f
    goto :goto_18

    #@50
    .line 600
    :sswitch_data_50
    .sparse-switch
        0x2c -> :sswitch_3d
        0x3b -> :sswitch_3d
        0x5d -> :sswitch_33
    .end sparse-switch

    #@5e
    .line 587
    :sswitch_data_5e
    .sparse-switch
        0x2c -> :sswitch_7
        0x3b -> :sswitch_2f
        0x5d -> :sswitch_27
    .end sparse-switch
.end method

.method private nextInObject(Z)Landroid/util/JsonToken;
    .registers 4
    .parameter "firstElement"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 626
    if-eqz p1, :cond_3d

    #@2
    .line 628
    invoke-direct {p0}, Landroid/util/JsonReader;->nextNonWhitespace()I

    #@5
    move-result v1

    #@6
    packed-switch v1, :pswitch_data_68

    #@9
    .line 633
    iget v1, p0, Landroid/util/JsonReader;->pos:I

    #@b
    add-int/lit8 v1, v1, -0x1

    #@d
    iput v1, p0, Landroid/util/JsonReader;->pos:I

    #@f
    .line 649
    :sswitch_f
    invoke-direct {p0}, Landroid/util/JsonReader;->nextNonWhitespace()I

    #@12
    move-result v0

    #@13
    .line 650
    .local v0, quote:I
    sparse-switch v0, :sswitch_data_6e

    #@16
    .line 657
    invoke-direct {p0}, Landroid/util/JsonReader;->checkLenient()V

    #@19
    .line 658
    iget v1, p0, Landroid/util/JsonReader;->pos:I

    #@1b
    add-int/lit8 v1, v1, -0x1

    #@1d
    iput v1, p0, Landroid/util/JsonReader;->pos:I

    #@1f
    .line 659
    const/4 v1, 0x0

    #@20
    invoke-direct {p0, v1}, Landroid/util/JsonReader;->nextLiteral(Z)Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    iput-object v1, p0, Landroid/util/JsonReader;->name:Ljava/lang/String;

    #@26
    .line 660
    iget-object v1, p0, Landroid/util/JsonReader;->name:Ljava/lang/String;

    #@28
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_5d

    #@2e
    .line 661
    const-string v1, "Expected name"

    #@30
    invoke-direct {p0, v1}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@33
    move-result-object v1

    #@34
    throw v1

    #@35
    .line 630
    .end local v0           #quote:I
    :pswitch_35
    invoke-direct {p0}, Landroid/util/JsonReader;->pop()Landroid/util/JsonScope;

    #@38
    .line 631
    sget-object v1, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    #@3a
    iput-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@3c
    .line 666
    :goto_3c
    return-object v1

    #@3d
    .line 636
    :cond_3d
    invoke-direct {p0}, Landroid/util/JsonReader;->nextNonWhitespace()I

    #@40
    move-result v1

    #@41
    sparse-switch v1, :sswitch_data_78

    #@44
    .line 644
    const-string v1, "Unterminated object"

    #@46
    invoke-direct {p0, v1}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@49
    move-result-object v1

    #@4a
    throw v1

    #@4b
    .line 638
    :sswitch_4b
    invoke-direct {p0}, Landroid/util/JsonReader;->pop()Landroid/util/JsonScope;

    #@4e
    .line 639
    sget-object v1, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    #@50
    iput-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@52
    goto :goto_3c

    #@53
    .line 652
    .restart local v0       #quote:I
    :sswitch_53
    invoke-direct {p0}, Landroid/util/JsonReader;->checkLenient()V

    #@56
    .line 654
    :sswitch_56
    int-to-char v1, v0

    #@57
    invoke-direct {p0, v1}, Landroid/util/JsonReader;->nextString(C)Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    iput-object v1, p0, Landroid/util/JsonReader;->name:Ljava/lang/String;

    #@5d
    .line 665
    :cond_5d
    sget-object v1, Landroid/util/JsonScope;->DANGLING_NAME:Landroid/util/JsonScope;

    #@5f
    invoke-direct {p0, v1}, Landroid/util/JsonReader;->replaceTop(Landroid/util/JsonScope;)V

    #@62
    .line 666
    sget-object v1, Landroid/util/JsonToken;->NAME:Landroid/util/JsonToken;

    #@64
    iput-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@66
    goto :goto_3c

    #@67
    .line 628
    nop

    #@68
    :pswitch_data_68
    .packed-switch 0x7d
        :pswitch_35
    .end packed-switch

    #@6e
    .line 650
    :sswitch_data_6e
    .sparse-switch
        0x22 -> :sswitch_56
        0x27 -> :sswitch_53
    .end sparse-switch

    #@78
    .line 636
    :sswitch_data_78
    .sparse-switch
        0x2c -> :sswitch_f
        0x3b -> :sswitch_f
        0x7d -> :sswitch_4b
    .end sparse-switch
.end method

.method private nextLiteral(Z)Ljava/lang/String;
    .registers 8
    .parameter "assignOffsetsOnly"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 922
    const/4 v0, 0x0

    #@2
    .line 923
    .local v0, builder:Ljava/lang/StringBuilder;
    const/4 v3, -0x1

    #@3
    iput v3, p0, Landroid/util/JsonReader;->valuePos:I

    #@5
    .line 924
    iput v5, p0, Landroid/util/JsonReader;->valueLength:I

    #@7
    .line 925
    const/4 v1, 0x0

    #@8
    .line 929
    .local v1, i:I
    :cond_8
    :goto_8
    iget v3, p0, Landroid/util/JsonReader;->pos:I

    #@a
    add-int/2addr v3, v1

    #@b
    iget v4, p0, Landroid/util/JsonReader;->limit:I

    #@d
    if-ge v3, v4, :cond_33

    #@f
    .line 930
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@11
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@13
    add-int/2addr v4, v1

    #@14
    aget-char v3, v3, v4

    #@16
    sparse-switch v3, :sswitch_data_8a

    #@19
    .line 929
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_8

    #@1c
    .line 936
    :sswitch_1c
    invoke-direct {p0}, Landroid/util/JsonReader;->checkLenient()V

    #@1f
    .line 980
    :goto_1f
    :sswitch_1f
    if-eqz p1, :cond_68

    #@21
    if-nez v0, :cond_68

    #@23
    .line 981
    iget v3, p0, Landroid/util/JsonReader;->pos:I

    #@25
    iput v3, p0, Landroid/util/JsonReader;->valuePos:I

    #@27
    .line 982
    const/4 v2, 0x0

    #@28
    .line 991
    .local v2, result:Ljava/lang/String;
    :goto_28
    iget v3, p0, Landroid/util/JsonReader;->valueLength:I

    #@2a
    add-int/2addr v3, v1

    #@2b
    iput v3, p0, Landroid/util/JsonReader;->valueLength:I

    #@2d
    .line 992
    iget v3, p0, Landroid/util/JsonReader;->pos:I

    #@2f
    add-int/2addr v3, v1

    #@30
    iput v3, p0, Landroid/util/JsonReader;->pos:I

    #@32
    .line 993
    return-object v2

    #@33
    .line 957
    .end local v2           #result:Ljava/lang/String;
    :cond_33
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@35
    array-length v3, v3

    #@36
    if-ge v1, v3, :cond_47

    #@38
    .line 958
    add-int/lit8 v3, v1, 0x1

    #@3a
    invoke-direct {p0, v3}, Landroid/util/JsonReader;->fillBuffer(I)Z

    #@3d
    move-result v3

    #@3e
    if-nez v3, :cond_8

    #@40
    .line 961
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@42
    iget v4, p0, Landroid/util/JsonReader;->limit:I

    #@44
    aput-char v5, v3, v4

    #@46
    goto :goto_1f

    #@47
    .line 967
    :cond_47
    if-nez v0, :cond_4e

    #@49
    .line 968
    new-instance v0, Ljava/lang/StringBuilder;

    #@4b
    .end local v0           #builder:Ljava/lang/StringBuilder;
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    .line 970
    .restart local v0       #builder:Ljava/lang/StringBuilder;
    :cond_4e
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@50
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@52
    invoke-virtual {v0, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@55
    .line 971
    iget v3, p0, Landroid/util/JsonReader;->valueLength:I

    #@57
    add-int/2addr v3, v1

    #@58
    iput v3, p0, Landroid/util/JsonReader;->valueLength:I

    #@5a
    .line 972
    iget v3, p0, Landroid/util/JsonReader;->pos:I

    #@5c
    add-int/2addr v3, v1

    #@5d
    iput v3, p0, Landroid/util/JsonReader;->pos:I

    #@5f
    .line 973
    const/4 v1, 0x0

    #@60
    .line 974
    const/4 v3, 0x1

    #@61
    invoke-direct {p0, v3}, Landroid/util/JsonReader;->fillBuffer(I)Z

    #@64
    move-result v3

    #@65
    if-nez v3, :cond_8

    #@67
    goto :goto_1f

    #@68
    .line 983
    :cond_68
    iget-boolean v3, p0, Landroid/util/JsonReader;->skipping:Z

    #@6a
    if-eqz v3, :cond_70

    #@6c
    .line 984
    const-string/jumbo v2, "skipped!"

    #@6f
    .restart local v2       #result:Ljava/lang/String;
    goto :goto_28

    #@70
    .line 985
    .end local v2           #result:Ljava/lang/String;
    :cond_70
    if-nez v0, :cond_7d

    #@72
    .line 986
    iget-object v3, p0, Landroid/util/JsonReader;->stringPool:Llibcore/internal/StringPool;

    #@74
    iget-object v4, p0, Landroid/util/JsonReader;->buffer:[C

    #@76
    iget v5, p0, Landroid/util/JsonReader;->pos:I

    #@78
    invoke-virtual {v3, v4, v5, v1}, Llibcore/internal/StringPool;->get([CII)Ljava/lang/String;

    #@7b
    move-result-object v2

    #@7c
    .restart local v2       #result:Ljava/lang/String;
    goto :goto_28

    #@7d
    .line 988
    .end local v2           #result:Ljava/lang/String;
    :cond_7d
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@7f
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@81
    invoke-virtual {v0, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@84
    .line 989
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v2

    #@88
    .restart local v2       #result:Ljava/lang/String;
    goto :goto_28

    #@89
    .line 930
    nop

    #@8a
    :sswitch_data_8a
    .sparse-switch
        0x9 -> :sswitch_1f
        0xa -> :sswitch_1f
        0xc -> :sswitch_1f
        0xd -> :sswitch_1f
        0x20 -> :sswitch_1f
        0x23 -> :sswitch_1c
        0x2c -> :sswitch_1f
        0x2f -> :sswitch_1c
        0x3a -> :sswitch_1f
        0x3b -> :sswitch_1c
        0x3d -> :sswitch_1c
        0x5b -> :sswitch_1f
        0x5c -> :sswitch_1c
        0x5d -> :sswitch_1f
        0x7b -> :sswitch_1f
        0x7d -> :sswitch_1f
    .end sparse-switch
.end method

.method private nextNonWhitespace()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 779
    :goto_1
    :sswitch_1
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@3
    iget v3, p0, Landroid/util/JsonReader;->limit:I

    #@5
    if-lt v2, v3, :cond_d

    #@7
    invoke-direct {p0, v5}, Landroid/util/JsonReader;->fillBuffer(I)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_61

    #@d
    .line 780
    :cond_d
    iget-object v2, p0, Landroid/util/JsonReader;->buffer:[C

    #@f
    iget v3, p0, Landroid/util/JsonReader;->pos:I

    #@11
    add-int/lit8 v4, v3, 0x1

    #@13
    iput v4, p0, Landroid/util/JsonReader;->pos:I

    #@15
    aget-char v0, v2, v3

    #@17
    .line 781
    .local v0, c:I
    sparse-switch v0, :sswitch_data_6a

    #@1a
    .line 826
    :cond_1a
    :goto_1a
    return v0

    #@1b
    .line 789
    :sswitch_1b
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@1d
    iget v3, p0, Landroid/util/JsonReader;->limit:I

    #@1f
    if-ne v2, v3, :cond_27

    #@21
    invoke-direct {p0, v5}, Landroid/util/JsonReader;->fillBuffer(I)Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_1a

    #@27
    .line 793
    :cond_27
    invoke-direct {p0}, Landroid/util/JsonReader;->checkLenient()V

    #@2a
    .line 794
    iget-object v2, p0, Landroid/util/JsonReader;->buffer:[C

    #@2c
    iget v3, p0, Landroid/util/JsonReader;->pos:I

    #@2e
    aget-char v1, v2, v3

    #@30
    .line 795
    .local v1, peek:C
    sparse-switch v1, :sswitch_data_84

    #@33
    goto :goto_1a

    #@34
    .line 798
    :sswitch_34
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@36
    add-int/lit8 v2, v2, 0x1

    #@38
    iput v2, p0, Landroid/util/JsonReader;->pos:I

    #@3a
    .line 799
    const-string v2, "*/"

    #@3c
    invoke-direct {p0, v2}, Landroid/util/JsonReader;->skipTo(Ljava/lang/String;)Z

    #@3f
    move-result v2

    #@40
    if-nez v2, :cond_49

    #@42
    .line 800
    const-string v2, "Unterminated comment"

    #@44
    invoke-direct {p0, v2}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@47
    move-result-object v2

    #@48
    throw v2

    #@49
    .line 802
    :cond_49
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@4b
    add-int/lit8 v2, v2, 0x2

    #@4d
    iput v2, p0, Landroid/util/JsonReader;->pos:I

    #@4f
    goto :goto_1

    #@50
    .line 807
    :sswitch_50
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@52
    add-int/lit8 v2, v2, 0x1

    #@54
    iput v2, p0, Landroid/util/JsonReader;->pos:I

    #@56
    .line 808
    invoke-direct {p0}, Landroid/util/JsonReader;->skipToEndOfLine()V

    #@59
    goto :goto_1

    #@5a
    .line 821
    .end local v1           #peek:C
    :sswitch_5a
    invoke-direct {p0}, Landroid/util/JsonReader;->checkLenient()V

    #@5d
    .line 822
    invoke-direct {p0}, Landroid/util/JsonReader;->skipToEndOfLine()V

    #@60
    goto :goto_1

    #@61
    .line 830
    .end local v0           #c:I
    :cond_61
    new-instance v2, Ljava/io/EOFException;

    #@63
    const-string v3, "End of input"

    #@65
    invoke-direct {v2, v3}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    #@68
    throw v2

    #@69
    .line 781
    nop

    #@6a
    :sswitch_data_6a
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_5a
        0x2f -> :sswitch_1b
    .end sparse-switch

    #@84
    .line 795
    :sswitch_data_84
    .sparse-switch
        0x2a -> :sswitch_34
        0x2f -> :sswitch_50
    .end sparse-switch
.end method

.method private nextString(C)Ljava/lang/String;
    .registers 8
    .parameter "quote"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 877
    const/4 v0, 0x0

    #@1
    .line 880
    .local v0, builder:Ljava/lang/StringBuilder;
    :cond_1
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@3
    .line 881
    .local v2, start:I
    :cond_3
    :goto_3
    iget v3, p0, Landroid/util/JsonReader;->pos:I

    #@5
    iget v4, p0, Landroid/util/JsonReader;->limit:I

    #@7
    if-ge v3, v4, :cond_5b

    #@9
    .line 882
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@b
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@d
    add-int/lit8 v5, v4, 0x1

    #@f
    iput v5, p0, Landroid/util/JsonReader;->pos:I

    #@11
    aget-char v1, v3, v4

    #@13
    .line 884
    .local v1, c:I
    if-ne v1, p1, :cond_3c

    #@15
    .line 885
    iget-boolean v3, p0, Landroid/util/JsonReader;->skipping:Z

    #@17
    if-eqz v3, :cond_1d

    #@19
    .line 886
    const-string/jumbo v3, "skipped!"

    #@1c
    .line 891
    :goto_1c
    return-object v3

    #@1d
    .line 887
    :cond_1d
    if-nez v0, :cond_2d

    #@1f
    .line 888
    iget-object v3, p0, Landroid/util/JsonReader;->stringPool:Llibcore/internal/StringPool;

    #@21
    iget-object v4, p0, Landroid/util/JsonReader;->buffer:[C

    #@23
    iget v5, p0, Landroid/util/JsonReader;->pos:I

    #@25
    sub-int/2addr v5, v2

    #@26
    add-int/lit8 v5, v5, -0x1

    #@28
    invoke-virtual {v3, v4, v2, v5}, Llibcore/internal/StringPool;->get([CII)Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    goto :goto_1c

    #@2d
    .line 890
    :cond_2d
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@2f
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@31
    sub-int/2addr v4, v2

    #@32
    add-int/lit8 v4, v4, -0x1

    #@34
    invoke-virtual {v0, v3, v2, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@37
    .line 891
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    goto :goto_1c

    #@3c
    .line 894
    :cond_3c
    const/16 v3, 0x5c

    #@3e
    if-ne v1, v3, :cond_3

    #@40
    .line 895
    if-nez v0, :cond_47

    #@42
    .line 896
    new-instance v0, Ljava/lang/StringBuilder;

    #@44
    .end local v0           #builder:Ljava/lang/StringBuilder;
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    .line 898
    .restart local v0       #builder:Ljava/lang/StringBuilder;
    :cond_47
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@49
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@4b
    sub-int/2addr v4, v2

    #@4c
    add-int/lit8 v4, v4, -0x1

    #@4e
    invoke-virtual {v0, v3, v2, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@51
    .line 899
    invoke-direct {p0}, Landroid/util/JsonReader;->readEscapeCharacter()C

    #@54
    move-result v3

    #@55
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@58
    .line 900
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@5a
    goto :goto_3

    #@5b
    .line 904
    .end local v1           #c:I
    :cond_5b
    if-nez v0, :cond_62

    #@5d
    .line 905
    new-instance v0, Ljava/lang/StringBuilder;

    #@5f
    .end local v0           #builder:Ljava/lang/StringBuilder;
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    .line 907
    .restart local v0       #builder:Ljava/lang/StringBuilder;
    :cond_62
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@64
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@66
    sub-int/2addr v4, v2

    #@67
    invoke-virtual {v0, v3, v2, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    #@6a
    .line 908
    const/4 v3, 0x1

    #@6b
    invoke-direct {p0, v3}, Landroid/util/JsonReader;->fillBuffer(I)Z

    #@6e
    move-result v3

    #@6f
    if-nez v3, :cond_1

    #@71
    .line 910
    const-string v3, "Unterminated string"

    #@73
    invoke-direct {p0, v3}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@76
    move-result-object v3

    #@77
    throw v3
.end method

.method private nextValue()Landroid/util/JsonToken;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 692
    invoke-direct {p0}, Landroid/util/JsonReader;->nextNonWhitespace()I

    #@3
    move-result v0

    #@4
    .line 693
    .local v0, c:I
    sparse-switch v0, :sswitch_data_36

    #@7
    .line 709
    iget v1, p0, Landroid/util/JsonReader;->pos:I

    #@9
    add-int/lit8 v1, v1, -0x1

    #@b
    iput v1, p0, Landroid/util/JsonReader;->pos:I

    #@d
    .line 710
    invoke-direct {p0}, Landroid/util/JsonReader;->readLiteral()Landroid/util/JsonToken;

    #@10
    move-result-object v1

    #@11
    :goto_11
    return-object v1

    #@12
    .line 695
    :sswitch_12
    sget-object v1, Landroid/util/JsonScope;->EMPTY_OBJECT:Landroid/util/JsonScope;

    #@14
    invoke-direct {p0, v1}, Landroid/util/JsonReader;->push(Landroid/util/JsonScope;)V

    #@17
    .line 696
    sget-object v1, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    #@19
    iput-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@1b
    goto :goto_11

    #@1c
    .line 699
    :sswitch_1c
    sget-object v1, Landroid/util/JsonScope;->EMPTY_ARRAY:Landroid/util/JsonScope;

    #@1e
    invoke-direct {p0, v1}, Landroid/util/JsonReader;->push(Landroid/util/JsonScope;)V

    #@21
    .line 700
    sget-object v1, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    #@23
    iput-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@25
    goto :goto_11

    #@26
    .line 703
    :sswitch_26
    invoke-direct {p0}, Landroid/util/JsonReader;->checkLenient()V

    #@29
    .line 705
    :sswitch_29
    int-to-char v1, v0

    #@2a
    invoke-direct {p0, v1}, Landroid/util/JsonReader;->nextString(C)Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    iput-object v1, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@30
    .line 706
    sget-object v1, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@32
    iput-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@34
    goto :goto_11

    #@35
    .line 693
    nop

    #@36
    :sswitch_data_36
    .sparse-switch
        0x22 -> :sswitch_29
        0x27 -> :sswitch_26
        0x5b -> :sswitch_1c
        0x7b -> :sswitch_12
    .end sparse-switch
.end method

.method private objectValue()Landroid/util/JsonToken;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 674
    invoke-direct {p0}, Landroid/util/JsonReader;->nextNonWhitespace()I

    #@3
    move-result v0

    #@4
    packed-switch v0, :pswitch_data_38

    #@7
    .line 684
    :pswitch_7
    const-string v0, "Expected \':\'"

    #@9
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@c
    move-result-object v0

    #@d
    throw v0

    #@e
    .line 678
    :pswitch_e
    invoke-direct {p0}, Landroid/util/JsonReader;->checkLenient()V

    #@11
    .line 679
    iget v0, p0, Landroid/util/JsonReader;->pos:I

    #@13
    iget v1, p0, Landroid/util/JsonReader;->limit:I

    #@15
    if-lt v0, v1, :cond_1e

    #@17
    const/4 v0, 0x1

    #@18
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->fillBuffer(I)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_2e

    #@1e
    :cond_1e
    iget-object v0, p0, Landroid/util/JsonReader;->buffer:[C

    #@20
    iget v1, p0, Landroid/util/JsonReader;->pos:I

    #@22
    aget-char v0, v0, v1

    #@24
    const/16 v1, 0x3e

    #@26
    if-ne v0, v1, :cond_2e

    #@28
    .line 680
    iget v0, p0, Landroid/util/JsonReader;->pos:I

    #@2a
    add-int/lit8 v0, v0, 0x1

    #@2c
    iput v0, p0, Landroid/util/JsonReader;->pos:I

    #@2e
    .line 687
    :cond_2e
    :pswitch_2e
    sget-object v0, Landroid/util/JsonScope;->NONEMPTY_OBJECT:Landroid/util/JsonScope;

    #@30
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->replaceTop(Landroid/util/JsonScope;)V

    #@33
    .line 688
    invoke-direct {p0}, Landroid/util/JsonReader;->nextValue()Landroid/util/JsonToken;

    #@36
    move-result-object v0

    #@37
    return-object v0

    #@38
    .line 674
    :pswitch_data_38
    .packed-switch 0x3a
        :pswitch_2e
        :pswitch_7
        :pswitch_7
        :pswitch_e
    .end packed-switch
.end method

.method private peekStack()Landroid/util/JsonScope;
    .registers 3

    #@0
    .prologue
    .line 564
    iget-object v0, p0, Landroid/util/JsonReader;->stack:Ljava/util/List;

    #@2
    iget-object v1, p0, Landroid/util/JsonReader;->stack:Ljava/util/List;

    #@4
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@7
    move-result v1

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/util/JsonScope;

    #@10
    return-object v0
.end method

.method private pop()Landroid/util/JsonScope;
    .registers 3

    #@0
    .prologue
    .line 568
    iget-object v0, p0, Landroid/util/JsonReader;->stack:Ljava/util/List;

    #@2
    iget-object v1, p0, Landroid/util/JsonReader;->stack:Ljava/util/List;

    #@4
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@7
    move-result v1

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/util/JsonScope;

    #@10
    return-object v0
.end method

.method private push(Landroid/util/JsonScope;)V
    .registers 3
    .parameter "newTop"

    #@0
    .prologue
    .line 572
    iget-object v0, p0, Landroid/util/JsonReader;->stack:Ljava/util/List;

    #@2
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@5
    .line 573
    return-void
.end method

.method private readEscapeCharacter()C
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x4

    #@1
    .line 1010
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@3
    iget v3, p0, Landroid/util/JsonReader;->limit:I

    #@5
    if-ne v2, v3, :cond_15

    #@7
    const/4 v2, 0x1

    #@8
    invoke-direct {p0, v2}, Landroid/util/JsonReader;->fillBuffer(I)Z

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_15

    #@e
    .line 1011
    const-string v2, "Unterminated escape sequence"

    #@10
    invoke-direct {p0, v2}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@13
    move-result-object v2

    #@14
    throw v2

    #@15
    .line 1014
    :cond_15
    iget-object v2, p0, Landroid/util/JsonReader;->buffer:[C

    #@17
    iget v3, p0, Landroid/util/JsonReader;->pos:I

    #@19
    add-int/lit8 v4, v3, 0x1

    #@1b
    iput v4, p0, Landroid/util/JsonReader;->pos:I

    #@1d
    aget-char v0, v2, v3

    #@1f
    .line 1015
    .local v0, escaped:C
    sparse-switch v0, :sswitch_data_60

    #@22
    .line 1043
    .end local v0           #escaped:C
    :goto_22
    return v0

    #@23
    .line 1017
    .restart local v0       #escaped:C
    :sswitch_23
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@25
    add-int/lit8 v2, v2, 0x4

    #@27
    iget v3, p0, Landroid/util/JsonReader;->limit:I

    #@29
    if-le v2, v3, :cond_38

    #@2b
    invoke-direct {p0, v5}, Landroid/util/JsonReader;->fillBuffer(I)Z

    #@2e
    move-result v2

    #@2f
    if-nez v2, :cond_38

    #@31
    .line 1018
    const-string v2, "Unterminated escape sequence"

    #@33
    invoke-direct {p0, v2}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@36
    move-result-object v2

    #@37
    throw v2

    #@38
    .line 1020
    :cond_38
    iget-object v2, p0, Landroid/util/JsonReader;->stringPool:Llibcore/internal/StringPool;

    #@3a
    iget-object v3, p0, Landroid/util/JsonReader;->buffer:[C

    #@3c
    iget v4, p0, Landroid/util/JsonReader;->pos:I

    #@3e
    invoke-virtual {v2, v3, v4, v5}, Llibcore/internal/StringPool;->get([CII)Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    .line 1021
    .local v1, hex:Ljava/lang/String;
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@44
    add-int/lit8 v2, v2, 0x4

    #@46
    iput v2, p0, Landroid/util/JsonReader;->pos:I

    #@48
    .line 1022
    const/16 v2, 0x10

    #@4a
    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@4d
    move-result v2

    #@4e
    int-to-char v0, v2

    #@4f
    goto :goto_22

    #@50
    .line 1025
    .end local v1           #hex:Ljava/lang/String;
    :sswitch_50
    const/16 v0, 0x9

    #@52
    goto :goto_22

    #@53
    .line 1028
    :sswitch_53
    const/16 v0, 0x8

    #@55
    goto :goto_22

    #@56
    .line 1031
    :sswitch_56
    const/16 v0, 0xa

    #@58
    goto :goto_22

    #@59
    .line 1034
    :sswitch_59
    const/16 v0, 0xd

    #@5b
    goto :goto_22

    #@5c
    .line 1037
    :sswitch_5c
    const/16 v0, 0xc

    #@5e
    goto :goto_22

    #@5f
    .line 1015
    nop

    #@60
    :sswitch_data_60
    .sparse-switch
        0x62 -> :sswitch_53
        0x66 -> :sswitch_5c
        0x6e -> :sswitch_56
        0x72 -> :sswitch_59
        0x74 -> :sswitch_50
        0x75 -> :sswitch_23
    .end sparse-switch
.end method

.method private readLiteral()Landroid/util/JsonToken;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1051
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->nextLiteral(Z)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    iput-object v0, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@7
    .line 1052
    iget v0, p0, Landroid/util/JsonReader;->valueLength:I

    #@9
    if-nez v0, :cond_12

    #@b
    .line 1053
    const-string v0, "Expected literal value"

    #@d
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@10
    move-result-object v0

    #@11
    throw v0

    #@12
    .line 1055
    :cond_12
    invoke-direct {p0}, Landroid/util/JsonReader;->decodeLiteral()Landroid/util/JsonToken;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@18
    .line 1056
    iget-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@1a
    sget-object v1, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@1c
    if-ne v0, v1, :cond_21

    #@1e
    .line 1057
    invoke-direct {p0}, Landroid/util/JsonReader;->checkLenient()V

    #@21
    .line 1059
    :cond_21
    iget-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@23
    return-object v0
.end method

.method private replaceTop(Landroid/util/JsonScope;)V
    .registers 4
    .parameter "newTop"

    #@0
    .prologue
    .line 579
    iget-object v0, p0, Landroid/util/JsonReader;->stack:Ljava/util/List;

    #@2
    iget-object v1, p0, Landroid/util/JsonReader;->stack:Ljava/util/List;

    #@4
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@7
    move-result v1

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 580
    return-void
.end method

.method private skipTo(Ljava/lang/String;)Z
    .registers 5
    .parameter "toFind"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 855
    :goto_0
    iget v1, p0, Landroid/util/JsonReader;->pos:I

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v2

    #@6
    add-int/2addr v1, v2

    #@7
    iget v2, p0, Landroid/util/JsonReader;->limit:I

    #@9
    if-le v1, v2, :cond_15

    #@b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@e
    move-result v1

    #@f
    invoke-direct {p0, v1}, Landroid/util/JsonReader;->fillBuffer(I)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_35

    #@15
    .line 856
    :cond_15
    const/4 v0, 0x0

    #@16
    .local v0, c:I
    :goto_16
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@19
    move-result v1

    #@1a
    if-ge v0, v1, :cond_33

    #@1c
    .line 857
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@1e
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@20
    add-int/2addr v2, v0

    #@21
    aget-char v1, v1, v2

    #@23
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@26
    move-result v2

    #@27
    if-eq v1, v2, :cond_30

    #@29
    .line 855
    iget v1, p0, Landroid/util/JsonReader;->pos:I

    #@2b
    add-int/lit8 v1, v1, 0x1

    #@2d
    iput v1, p0, Landroid/util/JsonReader;->pos:I

    #@2f
    goto :goto_0

    #@30
    .line 856
    :cond_30
    add-int/lit8 v0, v0, 0x1

    #@32
    goto :goto_16

    #@33
    .line 861
    :cond_33
    const/4 v1, 0x1

    #@34
    .line 863
    .end local v0           #c:I
    :goto_34
    return v1

    #@35
    :cond_35
    const/4 v1, 0x0

    #@36
    goto :goto_34
.end method

.method private skipToEndOfLine()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 845
    :cond_0
    iget v1, p0, Landroid/util/JsonReader;->pos:I

    #@2
    iget v2, p0, Landroid/util/JsonReader;->limit:I

    #@4
    if-lt v1, v2, :cond_d

    #@6
    const/4 v1, 0x1

    #@7
    invoke-direct {p0, v1}, Landroid/util/JsonReader;->fillBuffer(I)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_1f

    #@d
    .line 846
    :cond_d
    iget-object v1, p0, Landroid/util/JsonReader;->buffer:[C

    #@f
    iget v2, p0, Landroid/util/JsonReader;->pos:I

    #@11
    add-int/lit8 v3, v2, 0x1

    #@13
    iput v3, p0, Landroid/util/JsonReader;->pos:I

    #@15
    aget-char v0, v1, v2

    #@17
    .line 847
    .local v0, c:C
    const/16 v1, 0xd

    #@19
    if-eq v0, v1, :cond_1f

    #@1b
    const/16 v1, 0xa

    #@1d
    if-ne v0, v1, :cond_0

    #@1f
    .line 851
    .end local v0           #c:C
    :cond_1f
    return-void
.end method

.method private syntaxError(Ljava/lang/String;)Ljava/io/IOException;
    .registers 5
    .parameter "message"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1156
    new-instance v0, Landroid/util/MalformedJsonException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    const-string v2, " at line "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-direct {p0}, Landroid/util/JsonReader;->getLineNumber()I

    #@14
    move-result v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " column "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {p0}, Landroid/util/JsonReader;->getColumnNumber()I

    #@22
    move-result v2

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-direct {v0, v1}, Landroid/util/MalformedJsonException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v0
.end method


# virtual methods
.method public beginArray()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 277
    sget-object v0, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    #@2
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->expect(Landroid/util/JsonToken;)V

    #@5
    .line 278
    return-void
.end method

.method public beginObject()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 293
    sget-object v0, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    #@2
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->expect(Landroid/util/JsonToken;)V

    #@5
    .line 294
    return-void
.end method

.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 534
    iput-object v0, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@3
    .line 535
    iput-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@5
    .line 536
    iget-object v0, p0, Landroid/util/JsonReader;->stack:Ljava/util/List;

    #@7
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@a
    .line 537
    iget-object v0, p0, Landroid/util/JsonReader;->stack:Ljava/util/List;

    #@c
    sget-object v1, Landroid/util/JsonScope;->CLOSED:Landroid/util/JsonScope;

    #@e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@11
    .line 538
    iget-object v0, p0, Landroid/util/JsonReader;->in:Ljava/io/Reader;

    #@13
    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    #@16
    .line 539
    return-void
.end method

.method public endArray()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 285
    sget-object v0, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    #@2
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->expect(Landroid/util/JsonToken;)V

    #@5
    .line 286
    return-void
.end method

.method public endObject()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 301
    sget-object v0, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    #@2
    invoke-direct {p0, v0}, Landroid/util/JsonReader;->expect(Landroid/util/JsonToken;)V

    #@5
    .line 302
    return-void
.end method

.method public hasNext()Z
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 319
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@3
    .line 320
    iget-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@5
    sget-object v1, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    #@7
    if-eq v0, v1, :cond_11

    #@9
    iget-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@b
    sget-object v1, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    #@d
    if-eq v0, v1, :cond_11

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public isLenient()Z
    .registers 2

    #@0
    .prologue
    .line 269
    iget-boolean v0, p0, Landroid/util/JsonReader;->lenient:Z

    #@2
    return v0
.end method

.method public nextBoolean()Z
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 424
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@3
    .line 425
    iget-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@5
    sget-object v2, Landroid/util/JsonToken;->BOOLEAN:Landroid/util/JsonToken;

    #@7
    if-eq v1, v2, :cond_24

    #@9
    .line 426
    new-instance v1, Ljava/lang/IllegalStateException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Expected a boolean but was "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    iget-object v3, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@23
    throw v1

    #@24
    .line 429
    :cond_24
    iget-object v1, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@26
    const-string/jumbo v2, "true"

    #@29
    if-ne v1, v2, :cond_30

    #@2b
    const/4 v0, 0x1

    #@2c
    .line 430
    .local v0, result:Z
    :goto_2c
    invoke-direct {p0}, Landroid/util/JsonReader;->advance()Landroid/util/JsonToken;

    #@2f
    .line 431
    return v0

    #@30
    .line 429
    .end local v0           #result:Z
    :cond_30
    const/4 v0, 0x0

    #@31
    goto :goto_2c
.end method

.method public nextDouble()D
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 458
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@3
    .line 459
    iget-object v2, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@5
    sget-object v3, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@7
    if-eq v2, v3, :cond_2a

    #@9
    iget-object v2, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@b
    sget-object v3, Landroid/util/JsonToken;->NUMBER:Landroid/util/JsonToken;

    #@d
    if-eq v2, v3, :cond_2a

    #@f
    .line 460
    new-instance v2, Ljava/lang/IllegalStateException;

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "Expected a double but was "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    iget-object v4, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@29
    throw v2

    #@2a
    .line 463
    :cond_2a
    iget-object v2, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@2c
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@2f
    move-result-wide v0

    #@30
    .line 464
    .local v0, result:D
    invoke-direct {p0}, Landroid/util/JsonReader;->advance()Landroid/util/JsonToken;

    #@33
    .line 465
    return-wide v0
.end method

.method public nextInt()I
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 510
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@3
    .line 511
    iget-object v4, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@5
    sget-object v5, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@7
    if-eq v4, v5, :cond_2a

    #@9
    iget-object v4, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@b
    sget-object v5, Landroid/util/JsonToken;->NUMBER:Landroid/util/JsonToken;

    #@d
    if-eq v4, v5, :cond_2a

    #@f
    .line 512
    new-instance v4, Ljava/lang/IllegalStateException;

    #@11
    new-instance v5, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v6, "Expected an int but was "

    #@18
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v5

    #@1c
    iget-object v6, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@1e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@29
    throw v4

    #@2a
    .line 517
    :cond_2a
    :try_start_2a
    iget-object v4, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@2c
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2f
    .catch Ljava/lang/NumberFormatException; {:try_start_2a .. :try_end_2f} :catch_34

    #@2f
    move-result v3

    #@30
    .line 526
    .local v3, result:I
    :cond_30
    invoke-direct {p0}, Landroid/util/JsonReader;->advance()Landroid/util/JsonToken;

    #@33
    .line 527
    return v3

    #@34
    .line 518
    .end local v3           #result:I
    :catch_34
    move-exception v2

    #@35
    .line 519
    .local v2, ignored:Ljava/lang/NumberFormatException;
    iget-object v4, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@37
    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@3a
    move-result-wide v0

    #@3b
    .line 520
    .local v0, asDouble:D
    double-to-int v3, v0

    #@3c
    .line 521
    .restart local v3       #result:I
    int-to-double v4, v3

    #@3d
    cmpl-double v4, v4, v0

    #@3f
    if-eqz v4, :cond_30

    #@41
    .line 522
    new-instance v4, Ljava/lang/NumberFormatException;

    #@43
    iget-object v5, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@45
    invoke-direct {v4, v5}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    #@48
    throw v4
.end method

.method public nextLong()J
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 479
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@3
    .line 480
    iget-object v5, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@5
    sget-object v6, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@7
    if-eq v5, v6, :cond_2a

    #@9
    iget-object v5, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@b
    sget-object v6, Landroid/util/JsonToken;->NUMBER:Landroid/util/JsonToken;

    #@d
    if-eq v5, v6, :cond_2a

    #@f
    .line 481
    new-instance v5, Ljava/lang/IllegalStateException;

    #@11
    new-instance v6, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v7, "Expected a long but was "

    #@18
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v6

    #@1c
    iget-object v7, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@1e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v6

    #@22
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v6

    #@26
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@29
    throw v5

    #@2a
    .line 486
    :cond_2a
    :try_start_2a
    iget-object v5, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@2c
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2f
    .catch Ljava/lang/NumberFormatException; {:try_start_2a .. :try_end_2f} :catch_34

    #@2f
    move-result-wide v3

    #@30
    .line 495
    .local v3, result:J
    :cond_30
    invoke-direct {p0}, Landroid/util/JsonReader;->advance()Landroid/util/JsonToken;

    #@33
    .line 496
    return-wide v3

    #@34
    .line 487
    .end local v3           #result:J
    :catch_34
    move-exception v2

    #@35
    .line 488
    .local v2, ignored:Ljava/lang/NumberFormatException;
    iget-object v5, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@37
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@3a
    move-result-wide v0

    #@3b
    .line 489
    .local v0, asDouble:D
    double-to-long v3, v0

    #@3c
    .line 490
    .restart local v3       #result:J
    long-to-double v5, v3

    #@3d
    cmpl-double v5, v5, v0

    #@3f
    if-eqz v5, :cond_30

    #@41
    .line 491
    new-instance v5, Ljava/lang/NumberFormatException;

    #@43
    iget-object v6, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@45
    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    #@48
    throw v5
.end method

.method public nextName()Ljava/lang/String;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 388
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@3
    .line 389
    iget-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@5
    sget-object v2, Landroid/util/JsonToken;->NAME:Landroid/util/JsonToken;

    #@7
    if-eq v1, v2, :cond_26

    #@9
    .line 390
    new-instance v1, Ljava/lang/IllegalStateException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Expected a name but was "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1

    #@26
    .line 392
    :cond_26
    iget-object v0, p0, Landroid/util/JsonReader;->name:Ljava/lang/String;

    #@28
    .line 393
    .local v0, result:Ljava/lang/String;
    invoke-direct {p0}, Landroid/util/JsonReader;->advance()Landroid/util/JsonToken;

    #@2b
    .line 394
    return-object v0
.end method

.method public nextNull()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 442
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@3
    .line 443
    iget-object v0, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@5
    sget-object v1, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    #@7
    if-eq v0, v1, :cond_24

    #@9
    .line 444
    new-instance v0, Ljava/lang/IllegalStateException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Expected null but was "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    iget-object v2, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 447
    :cond_24
    invoke-direct {p0}, Landroid/util/JsonReader;->advance()Landroid/util/JsonToken;

    #@27
    .line 448
    return-void
.end method

.method public nextString()Ljava/lang/String;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 406
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@3
    .line 407
    iget-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@5
    sget-object v2, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    #@7
    if-eq v1, v2, :cond_2c

    #@9
    iget-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@b
    sget-object v2, Landroid/util/JsonToken;->NUMBER:Landroid/util/JsonToken;

    #@d
    if-eq v1, v2, :cond_2c

    #@f
    .line 408
    new-instance v1, Ljava/lang/IllegalStateException;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Expected a string but was "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v1

    #@2c
    .line 411
    :cond_2c
    iget-object v0, p0, Landroid/util/JsonReader;->value:Ljava/lang/String;

    #@2e
    .line 412
    .local v0, result:Ljava/lang/String;
    invoke-direct {p0}, Landroid/util/JsonReader;->advance()Landroid/util/JsonToken;

    #@31
    .line 413
    return-object v0
.end method

.method public peek()Landroid/util/JsonToken;
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 327
    iget-object v3, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@4
    if-eqz v3, :cond_9

    #@6
    .line 328
    iget-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@8
    .line 358
    :cond_8
    :goto_8
    return-object v1

    #@9
    .line 331
    :cond_9
    sget-object v3, Landroid/util/JsonReader$1;->$SwitchMap$android$util$JsonScope:[I

    #@b
    invoke-direct {p0}, Landroid/util/JsonReader;->peekStack()Landroid/util/JsonScope;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4}, Landroid/util/JsonScope;->ordinal()I

    #@12
    move-result v4

    #@13
    aget v3, v3, v4

    #@15
    packed-switch v3, :pswitch_data_8a

    #@18
    .line 363
    new-instance v3, Ljava/lang/AssertionError;

    #@1a
    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    #@1d
    throw v3

    #@1e
    .line 333
    :pswitch_1e
    sget-object v3, Landroid/util/JsonScope;->NONEMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@20
    invoke-direct {p0, v3}, Landroid/util/JsonReader;->replaceTop(Landroid/util/JsonScope;)V

    #@23
    .line 334
    invoke-direct {p0}, Landroid/util/JsonReader;->nextValue()Landroid/util/JsonToken;

    #@26
    move-result-object v1

    #@27
    .line 335
    .local v1, firstToken:Landroid/util/JsonToken;
    iget-boolean v3, p0, Landroid/util/JsonReader;->lenient:Z

    #@29
    if-nez v3, :cond_8

    #@2b
    iget-object v3, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@2d
    sget-object v4, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    #@2f
    if-eq v3, v4, :cond_8

    #@31
    iget-object v3, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@33
    sget-object v4, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    #@35
    if-eq v3, v4, :cond_8

    #@37
    .line 336
    new-instance v3, Ljava/io/IOException;

    #@39
    new-instance v4, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v5, "Expected JSON document to start with \'[\' or \'{\' but was "

    #@40
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    iget-object v5, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@46
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@51
    throw v3

    #@52
    .line 341
    .end local v1           #firstToken:Landroid/util/JsonToken;
    :pswitch_52
    invoke-direct {p0, v6}, Landroid/util/JsonReader;->nextInArray(Z)Landroid/util/JsonToken;

    #@55
    move-result-object v1

    #@56
    goto :goto_8

    #@57
    .line 343
    :pswitch_57
    invoke-direct {p0, v5}, Landroid/util/JsonReader;->nextInArray(Z)Landroid/util/JsonToken;

    #@5a
    move-result-object v1

    #@5b
    goto :goto_8

    #@5c
    .line 345
    :pswitch_5c
    invoke-direct {p0, v6}, Landroid/util/JsonReader;->nextInObject(Z)Landroid/util/JsonToken;

    #@5f
    move-result-object v1

    #@60
    goto :goto_8

    #@61
    .line 347
    :pswitch_61
    invoke-direct {p0}, Landroid/util/JsonReader;->objectValue()Landroid/util/JsonToken;

    #@64
    move-result-object v1

    #@65
    goto :goto_8

    #@66
    .line 349
    :pswitch_66
    invoke-direct {p0, v5}, Landroid/util/JsonReader;->nextInObject(Z)Landroid/util/JsonToken;

    #@69
    move-result-object v1

    #@6a
    goto :goto_8

    #@6b
    .line 352
    :pswitch_6b
    :try_start_6b
    invoke-direct {p0}, Landroid/util/JsonReader;->nextValue()Landroid/util/JsonToken;

    #@6e
    move-result-object v2

    #@6f
    .line 353
    .local v2, token:Landroid/util/JsonToken;
    iget-boolean v3, p0, Landroid/util/JsonReader;->lenient:Z

    #@71
    if-eqz v3, :cond_75

    #@73
    move-object v1, v2

    #@74
    .line 354
    goto :goto_8

    #@75
    .line 356
    :cond_75
    const-string v3, "Expected EOF"

    #@77
    invoke-direct {p0, v3}, Landroid/util/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    #@7a
    move-result-object v3

    #@7b
    throw v3
    :try_end_7c
    .catch Ljava/io/EOFException; {:try_start_6b .. :try_end_7c} :catch_7c

    #@7c
    .line 357
    .end local v2           #token:Landroid/util/JsonToken;
    :catch_7c
    move-exception v0

    #@7d
    .line 358
    .local v0, e:Ljava/io/EOFException;
    sget-object v1, Landroid/util/JsonToken;->END_DOCUMENT:Landroid/util/JsonToken;

    #@7f
    iput-object v1, p0, Landroid/util/JsonReader;->token:Landroid/util/JsonToken;

    #@81
    goto :goto_8

    #@82
    .line 361
    .end local v0           #e:Ljava/io/EOFException;
    :pswitch_82
    new-instance v3, Ljava/lang/IllegalStateException;

    #@84
    const-string v4, "JsonReader is closed"

    #@86
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@89
    throw v3

    #@8a
    .line 331
    :pswitch_data_8a
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_52
        :pswitch_57
        :pswitch_5c
        :pswitch_61
        :pswitch_66
        :pswitch_6b
        :pswitch_82
    .end packed-switch
.end method

.method public setLenient(Z)V
    .registers 2
    .parameter "lenient"

    #@0
    .prologue
    .line 262
    iput-boolean p1, p0, Landroid/util/JsonReader;->lenient:Z

    #@2
    .line 263
    return-void
.end method

.method public skipValue()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 547
    const/4 v2, 0x1

    #@2
    iput-boolean v2, p0, Landroid/util/JsonReader;->skipping:Z

    #@4
    .line 549
    const/4 v0, 0x0

    #@5
    .line 551
    .local v0, count:I
    :cond_5
    :try_start_5
    invoke-direct {p0}, Landroid/util/JsonReader;->advance()Landroid/util/JsonToken;

    #@8
    move-result-object v1

    #@9
    .line 552
    .local v1, token:Landroid/util/JsonToken;
    sget-object v2, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    #@b
    if-eq v1, v2, :cond_11

    #@d
    sget-object v2, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_23

    #@f
    if-ne v1, v2, :cond_18

    #@11
    .line 553
    :cond_11
    add-int/lit8 v0, v0, 0x1

    #@13
    .line 557
    :cond_13
    :goto_13
    if-nez v0, :cond_5

    #@15
    .line 559
    iput-boolean v3, p0, Landroid/util/JsonReader;->skipping:Z

    #@17
    .line 561
    return-void

    #@18
    .line 554
    :cond_18
    :try_start_18
    sget-object v2, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    #@1a
    if-eq v1, v2, :cond_20

    #@1c
    sget-object v2, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;
    :try_end_1e
    .catchall {:try_start_18 .. :try_end_1e} :catchall_23

    #@1e
    if-ne v1, v2, :cond_13

    #@20
    .line 555
    :cond_20
    add-int/lit8 v0, v0, -0x1

    #@22
    goto :goto_13

    #@23
    .line 559
    .end local v1           #token:Landroid/util/JsonToken;
    :catchall_23
    move-exception v2

    #@24
    iput-boolean v3, p0, Landroid/util/JsonReader;->skipping:Z

    #@26
    throw v2
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 997
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " near "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-direct {p0}, Landroid/util/JsonReader;->getSnippet()Ljava/lang/CharSequence;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    return-object v0
.end method
