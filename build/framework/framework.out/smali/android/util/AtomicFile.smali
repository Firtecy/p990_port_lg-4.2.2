.class public Landroid/util/AtomicFile;
.super Ljava/lang/Object;
.source "AtomicFile.java"


# instance fields
.field private final mBackupName:Ljava/io/File;

.field private final mBaseName:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .registers 5
    .parameter "baseName"

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    iput-object p1, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@5
    .line 55
    new-instance v0, Ljava/io/File;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v2, ".bak"

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@21
    iput-object v0, p0, Landroid/util/AtomicFile;->mBackupName:Ljava/io/File;

    #@23
    .line 56
    return-void
.end method


# virtual methods
.method public delete()V
    .registers 2

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@5
    .line 71
    iget-object v0, p0, Landroid/util/AtomicFile;->mBackupName:Ljava/io/File;

    #@7
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@a
    .line 72
    return-void
.end method

.method public failWrite(Ljava/io/FileOutputStream;)V
    .registers 5
    .parameter "str"

    #@0
    .prologue
    .line 145
    if-eqz p1, :cond_14

    #@2
    .line 146
    invoke-static {p1}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@5
    .line 148
    :try_start_5
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V

    #@8
    .line 149
    iget-object v1, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@a
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@d
    .line 150
    iget-object v1, p0, Landroid/util/AtomicFile;->mBackupName:Ljava/io/File;

    #@f
    iget-object v2, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@11
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_14} :catch_15

    #@14
    .line 155
    :cond_14
    :goto_14
    return-void

    #@15
    .line 151
    :catch_15
    move-exception v0

    #@16
    .line 152
    .local v0, e:Ljava/io/IOException;
    const-string v1, "AtomicFile"

    #@18
    const-string v2, "failWrite: Got exception:"

    #@1a
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d
    goto :goto_14
.end method

.method public finishWrite(Ljava/io/FileOutputStream;)V
    .registers 5
    .parameter "str"

    #@0
    .prologue
    .line 128
    if-eqz p1, :cond_d

    #@2
    .line 129
    invoke-static {p1}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@5
    .line 131
    :try_start_5
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V

    #@8
    .line 132
    iget-object v1, p0, Landroid/util/AtomicFile;->mBackupName:Ljava/io/File;

    #@a
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_d} :catch_e

    #@d
    .line 137
    :cond_d
    :goto_d
    return-void

    #@e
    .line 133
    :catch_e
    move-exception v0

    #@f
    .line 134
    .local v0, e:Ljava/io/IOException;
    const-string v1, "AtomicFile"

    #@11
    const-string v2, "finishWrite: Got exception:"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public getBaseFile()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@2
    return-object v0
.end method

.method public openAppend()Ljava/io/FileOutputStream;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 176
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    #@2
    iget-object v2, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@4
    const/4 v3, 0x1

    #@5
    invoke-direct {v1, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_8} :catch_9

    #@8
    return-object v1

    #@9
    .line 177
    :catch_9
    move-exception v0

    #@a
    .line 178
    .local v0, e:Ljava/io/FileNotFoundException;
    new-instance v1, Ljava/io/IOException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "Couldn\'t append "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget-object v3, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1
.end method

.method public openRead()Ljava/io/FileInputStream;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 195
    iget-object v0, p0, Landroid/util/AtomicFile;->mBackupName:Ljava/io/File;

    #@2
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_14

    #@8
    .line 196
    iget-object v0, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@a
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@d
    .line 197
    iget-object v0, p0, Landroid/util/AtomicFile;->mBackupName:Ljava/io/File;

    #@f
    iget-object v1, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@11
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@14
    .line 199
    :cond_14
    new-instance v0, Ljava/io/FileInputStream;

    #@16
    iget-object v1, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@18
    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@1b
    return-object v0
.end method

.method public readFully()[B
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 207
    invoke-virtual {p0}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    #@3
    move-result-object v5

    #@4
    .line 209
    .local v5, stream:Ljava/io/FileInputStream;
    const/4 v4, 0x0

    #@5
    .line 210
    .local v4, pos:I
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileInputStream;->available()I

    #@8
    move-result v1

    #@9
    .line 211
    .local v1, avail:I
    new-array v2, v1, [B

    #@b
    .line 213
    .local v2, data:[B
    :cond_b
    :goto_b
    array-length v6, v2

    #@c
    sub-int/2addr v6, v4

    #@d
    invoke-virtual {v5, v2, v4, v6}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_10
    .catchall {:try_start_5 .. :try_end_10} :catchall_2b

    #@10
    move-result v0

    #@11
    .line 216
    .local v0, amt:I
    if-gtz v0, :cond_17

    #@13
    .line 230
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    #@16
    return-object v2

    #@17
    .line 221
    :cond_17
    add-int/2addr v4, v0

    #@18
    .line 222
    :try_start_18
    invoke-virtual {v5}, Ljava/io/FileInputStream;->available()I

    #@1b
    move-result v1

    #@1c
    .line 223
    array-length v6, v2

    #@1d
    sub-int/2addr v6, v4

    #@1e
    if-le v1, v6, :cond_b

    #@20
    .line 224
    add-int v6, v4, v1

    #@22
    new-array v3, v6, [B

    #@24
    .line 225
    .local v3, newData:[B
    const/4 v6, 0x0

    #@25
    const/4 v7, 0x0

    #@26
    invoke-static {v2, v6, v3, v7, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_29
    .catchall {:try_start_18 .. :try_end_29} :catchall_2b

    #@29
    .line 226
    move-object v2, v3

    #@2a
    goto :goto_b

    #@2b
    .line 230
    .end local v0           #amt:I
    .end local v1           #avail:I
    .end local v2           #data:[B
    .end local v3           #newData:[B
    :catchall_2b
    move-exception v6

    #@2c
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    #@2f
    throw v6
.end method

.method public startWrite()Ljava/io/FileOutputStream;
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 90
    iget-object v4, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@3
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@6
    move-result v4

    #@7
    if-eqz v4, :cond_41

    #@9
    .line 91
    iget-object v4, p0, Landroid/util/AtomicFile;->mBackupName:Ljava/io/File;

    #@b
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@e
    move-result v4

    #@f
    if-nez v4, :cond_4a

    #@11
    .line 92
    iget-object v4, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@13
    iget-object v5, p0, Landroid/util/AtomicFile;->mBackupName:Ljava/io/File;

    #@15
    invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_41

    #@1b
    .line 93
    const-string v4, "AtomicFile"

    #@1d
    new-instance v5, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v6, "Couldn\'t rename file "

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    iget-object v6, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    const-string v6, " to backup file "

    #@30
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    iget-object v6, p0, Landroid/util/AtomicFile;->mBackupName:Ljava/io/File;

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 100
    :cond_41
    :goto_41
    const/4 v3, 0x0

    #@42
    .line 102
    .local v3, str:Ljava/io/FileOutputStream;
    :try_start_42
    new-instance v3, Ljava/io/FileOutputStream;

    #@44
    .end local v3           #str:Ljava/io/FileOutputStream;
    iget-object v4, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@46
    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_49
    .catch Ljava/io/FileNotFoundException; {:try_start_42 .. :try_end_49} :catch_50

    #@49
    .line 118
    .restart local v3       #str:Ljava/io/FileOutputStream;
    :goto_49
    return-object v3

    #@4a
    .line 97
    .end local v3           #str:Ljava/io/FileOutputStream;
    :cond_4a
    iget-object v4, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@4c
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    #@4f
    goto :goto_41

    #@50
    .line 103
    :catch_50
    move-exception v0

    #@51
    .line 104
    .local v0, e:Ljava/io/FileNotFoundException;
    iget-object v4, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@53
    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@56
    move-result-object v2

    #@57
    .line 105
    .local v2, parent:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    #@5a
    move-result v4

    #@5b
    if-nez v4, :cond_78

    #@5d
    .line 106
    new-instance v4, Ljava/io/IOException;

    #@5f
    new-instance v5, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v6, "Couldn\'t create directory "

    #@66
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    iget-object v6, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@6c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v5

    #@74
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@77
    throw v4

    #@78
    .line 108
    :cond_78
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    const/16 v5, 0x1f9

    #@7e
    invoke-static {v4, v5, v7, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@81
    .line 113
    :try_start_81
    new-instance v3, Ljava/io/FileOutputStream;

    #@83
    iget-object v4, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@85
    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_88
    .catch Ljava/io/FileNotFoundException; {:try_start_81 .. :try_end_88} :catch_89

    #@88
    .restart local v3       #str:Ljava/io/FileOutputStream;
    goto :goto_49

    #@89
    .line 114
    .end local v3           #str:Ljava/io/FileOutputStream;
    :catch_89
    move-exception v1

    #@8a
    .line 115
    .local v1, e2:Ljava/io/FileNotFoundException;
    new-instance v4, Ljava/io/IOException;

    #@8c
    new-instance v5, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v6, "Couldn\'t create "

    #@93
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v5

    #@97
    iget-object v6, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@99
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v5

    #@9d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v5

    #@a1
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@a4
    throw v4
.end method

.method public truncate()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 162
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    #@2
    iget-object v2, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@4
    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@7
    .line 163
    .local v1, fos:Ljava/io/FileOutputStream;
    invoke-static {v1}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@a
    .line 164
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_d} :catch_e
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_d} :catch_2a

    #@d
    .line 169
    .end local v1           #fos:Ljava/io/FileOutputStream;
    :goto_d
    return-void

    #@e
    .line 165
    :catch_e
    move-exception v0

    #@f
    .line 166
    .local v0, e:Ljava/io/FileNotFoundException;
    new-instance v2, Ljava/io/IOException;

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "Couldn\'t append "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    iget-object v4, p0, Landroid/util/AtomicFile;->mBaseName:Ljava/io/File;

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@29
    throw v2

    #@2a
    .line 167
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_2a
    move-exception v2

    #@2b
    goto :goto_d
.end method
