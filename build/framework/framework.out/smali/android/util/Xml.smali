.class public Landroid/util/Xml;
.super Ljava/lang/Object;
.source "Xml.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/util/Xml$Encoding;,
        Landroid/util/Xml$XmlSerializerFactory;
    }
.end annotation


# static fields
.field public static FEATURE_RELAXED:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 47
    const-string v0, "http://xmlpull.org/v1/doc/features.html#relaxed"

    #@2
    sput-object v0, Landroid/util/Xml;->FEATURE_RELAXED:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
    .registers 2
    .parameter "parser"

    #@0
    .prologue
    .line 176
    instance-of v0, p0, Landroid/util/AttributeSet;

    #@2
    if-eqz v0, :cond_7

    #@4
    check-cast p0, Landroid/util/AttributeSet;

    #@6
    .end local p0
    :goto_6
    return-object p0

    #@7
    .restart local p0
    :cond_7
    new-instance v0, Landroid/util/XmlPullAttributes;

    #@9
    invoke-direct {v0, p0}, Landroid/util/XmlPullAttributes;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    #@c
    move-object p0, v0

    #@d
    goto :goto_6
.end method

.method public static findEncodingByName(Ljava/lang/String;)Landroid/util/Xml$Encoding;
    .registers 6
    .parameter "encodingName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 148
    if-nez p0, :cond_5

    #@2
    .line 149
    sget-object v1, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    #@4
    .line 154
    :cond_4
    return-object v1

    #@5
    .line 152
    :cond_5
    invoke-static {}, Landroid/util/Xml$Encoding;->values()[Landroid/util/Xml$Encoding;

    #@8
    move-result-object v0

    #@9
    .local v0, arr$:[Landroid/util/Xml$Encoding;
    array-length v3, v0

    #@a
    .local v3, len$:I
    const/4 v2, 0x0

    #@b
    .local v2, i$:I
    :goto_b
    if-ge v2, v3, :cond_1a

    #@d
    aget-object v1, v0, v2

    #@f
    .line 153
    .local v1, encoding:Landroid/util/Xml$Encoding;
    iget-object v4, v1, Landroid/util/Xml$Encoding;->expatName:Ljava/lang/String;

    #@11
    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@14
    move-result v4

    #@15
    if-nez v4, :cond_4

    #@17
    .line 152
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_b

    #@1a
    .line 156
    .end local v1           #encoding:Landroid/util/Xml$Encoding;
    :cond_1a
    new-instance v4, Ljava/io/UnsupportedEncodingException;

    #@1c
    invoke-direct {v4, p0}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v4
.end method

.method public static newPullParser()Lorg/xmlpull/v1/XmlPullParser;
    .registers 4

    #@0
    .prologue
    .line 92
    :try_start_0
    new-instance v1, Lorg/kxml2/io/KXmlParser;

    #@2
    invoke-direct {v1}, Lorg/kxml2/io/KXmlParser;-><init>()V

    #@5
    .line 93
    .local v1, parser:Lorg/kxml2/io/KXmlParser;
    const-string v2, "http://xmlpull.org/v1/doc/features.html#process-docdecl"

    #@7
    const/4 v3, 0x1

    #@8
    invoke-virtual {v1, v2, v3}, Lorg/kxml2/io/KXmlParser;->setFeature(Ljava/lang/String;Z)V

    #@b
    .line 94
    const-string v2, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    #@d
    const/4 v3, 0x1

    #@e
    invoke-virtual {v1, v2, v3}, Lorg/kxml2/io/KXmlParser;->setFeature(Ljava/lang/String;Z)V
    :try_end_11
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_11} :catch_12

    #@11
    .line 95
    return-object v1

    #@12
    .line 96
    :catch_12
    move-exception v0

    #@13
    .line 97
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    new-instance v2, Ljava/lang/AssertionError;

    #@15
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@18
    throw v2
.end method

.method public static newSerializer()Lorg/xmlpull/v1/XmlSerializer;
    .registers 2

    #@0
    .prologue
    .line 106
    :try_start_0
    sget-object v1, Landroid/util/Xml$XmlSerializerFactory;->instance:Lorg/xmlpull/v1/XmlPullParserFactory;

    #@2
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    return-object v1

    #@7
    .line 107
    :catch_7
    move-exception v0

    #@8
    .line 108
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    new-instance v1, Ljava/lang/AssertionError;

    #@a
    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@d
    throw v1
.end method

.method public static parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
    .registers 6
    .parameter "in"
    .parameter "encoding"
    .parameter "contentHandler"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    new-instance v0, Lorg/apache/harmony/xml/ExpatReader;

    #@2
    invoke-direct {v0}, Lorg/apache/harmony/xml/ExpatReader;-><init>()V

    #@5
    .line 81
    .local v0, reader:Lorg/xml/sax/XMLReader;
    invoke-interface {v0, p2}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    #@8
    .line 82
    new-instance v1, Lorg/xml/sax/InputSource;

    #@a
    invoke-direct {v1, p0}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    #@d
    .line 83
    .local v1, source:Lorg/xml/sax/InputSource;
    iget-object v2, p1, Landroid/util/Xml$Encoding;->expatName:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    #@12
    .line 84
    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    #@15
    .line 85
    return-void
.end method

.method public static parse(Ljava/io/Reader;Lorg/xml/sax/ContentHandler;)V
    .registers 4
    .parameter "in"
    .parameter "contentHandler"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 69
    new-instance v0, Lorg/apache/harmony/xml/ExpatReader;

    #@2
    invoke-direct {v0}, Lorg/apache/harmony/xml/ExpatReader;-><init>()V

    #@5
    .line 70
    .local v0, reader:Lorg/xml/sax/XMLReader;
    invoke-interface {v0, p1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    #@8
    .line 71
    new-instance v1, Lorg/xml/sax/InputSource;

    #@a
    invoke-direct {v1, p0}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    #@d
    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    #@10
    .line 72
    return-void
.end method

.method public static parse(Ljava/lang/String;Lorg/xml/sax/ContentHandler;)V
    .registers 6
    .parameter "xml"
    .parameter "contentHandler"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    #@0
    .prologue
    .line 55
    :try_start_0
    new-instance v1, Lorg/apache/harmony/xml/ExpatReader;

    #@2
    invoke-direct {v1}, Lorg/apache/harmony/xml/ExpatReader;-><init>()V

    #@5
    .line 56
    .local v1, reader:Lorg/xml/sax/XMLReader;
    invoke-interface {v1, p1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    #@8
    .line 57
    new-instance v2, Lorg/xml/sax/InputSource;

    #@a
    new-instance v3, Ljava/io/StringReader;

    #@c
    invoke-direct {v3, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    #@f
    invoke-direct {v2, v3}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    #@12
    invoke-interface {v1, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_15} :catch_16

    #@15
    .line 61
    return-void

    #@16
    .line 58
    .end local v1           #reader:Lorg/xml/sax/XMLReader;
    :catch_16
    move-exception v0

    #@17
    .line 59
    .local v0, e:Ljava/io/IOException;
    new-instance v2, Ljava/lang/AssertionError;

    #@19
    invoke-direct {v2, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@1c
    throw v2
.end method
