.class public final Landroid/util/MathUtils;
.super Ljava/lang/Object;
.source "MathUtils.java"


# static fields
.field private static final DEG_TO_RAD:F = 0.017453292f

.field private static final RAD_TO_DEG:F = 57.295784f

.field private static final sRandom:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 27
    new-instance v0, Ljava/util/Random;

    #@2
    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    #@5
    sput-object v0, Landroid/util/MathUtils;->sRandom:Ljava/util/Random;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 32
    return-void
.end method

.method public static abs(F)F
    .registers 2
    .parameter "v"

    #@0
    .prologue
    .line 35
    const/4 v0, 0x0

    #@1
    cmpl-float v0, p0, v0

    #@3
    if-lez v0, :cond_6

    #@5
    .end local p0
    :goto_5
    return p0

    #@6
    .restart local p0
    :cond_6
    neg-float p0, p0

    #@7
    goto :goto_5
.end method

.method public static acos(F)F
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 128
    float-to-double v0, p0

    #@1
    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    #@4
    move-result-wide v0

    #@5
    double-to-float v0, v0

    #@6
    return v0
.end method

.method public static asin(F)F
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 132
    float-to-double v0, p0

    #@1
    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    #@4
    move-result-wide v0

    #@5
    double-to-float v0, v0

    #@6
    return v0
.end method

.method public static atan(F)F
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 136
    float-to-double v0, p0

    #@1
    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    #@4
    move-result-wide v0

    #@5
    double-to-float v0, v0

    #@6
    return v0
.end method

.method public static atan2(FF)F
    .registers 6
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 140
    float-to-double v0, p0

    #@1
    float-to-double v2, p1

    #@2
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@5
    move-result-wide v0

    #@6
    double-to-float v0, v0

    #@7
    return v0
.end method

.method public static constrain(FFF)F
    .registers 4
    .parameter "amount"
    .parameter "low"
    .parameter "high"

    #@0
    .prologue
    .line 47
    cmpg-float v0, p0, p1

    #@2
    if-gez v0, :cond_5

    #@4
    .end local p1
    :goto_4
    return p1

    #@5
    .restart local p1
    :cond_5
    cmpl-float v0, p0, p2

    #@7
    if-lez v0, :cond_b

    #@9
    move p1, p2

    #@a
    goto :goto_4

    #@b
    :cond_b
    move p1, p0

    #@c
    goto :goto_4
.end method

.method public static constrain(III)I
    .registers 3
    .parameter "amount"
    .parameter "low"
    .parameter "high"

    #@0
    .prologue
    .line 39
    if-ge p0, p1, :cond_3

    #@2
    .end local p1
    :goto_2
    return p1

    #@3
    .restart local p1
    :cond_3
    if-le p0, p2, :cond_7

    #@5
    move p1, p2

    #@6
    goto :goto_2

    #@7
    :cond_7
    move p1, p0

    #@8
    goto :goto_2
.end method

.method public static constrain(JJJ)J
    .registers 7
    .parameter "amount"
    .parameter "low"
    .parameter "high"

    #@0
    .prologue
    .line 43
    cmp-long v0, p0, p2

    #@2
    if-gez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return-wide p2

    #@5
    .restart local p2
    :cond_5
    cmp-long v0, p0, p4

    #@7
    if-lez v0, :cond_b

    #@9
    move-wide p2, p4

    #@a
    goto :goto_4

    #@b
    :cond_b
    move-wide p2, p0

    #@c
    goto :goto_4
.end method

.method public static degrees(F)F
    .registers 2
    .parameter "radians"

    #@0
    .prologue
    .line 124
    const v0, 0x42652ee2

    #@3
    mul-float/2addr v0, p0

    #@4
    return v0
.end method

.method public static dist(FFFF)F
    .registers 8
    .parameter "x1"
    .parameter "y1"
    .parameter "x2"
    .parameter "y2"

    #@0
    .prologue
    .line 95
    sub-float v0, p2, p0

    #@2
    .line 96
    .local v0, x:F
    sub-float v1, p3, p1

    #@4
    .line 97
    .local v1, y:F
    mul-float v2, v0, v0

    #@6
    mul-float v3, v1, v1

    #@8
    add-float/2addr v2, v3

    #@9
    float-to-double v2, v2

    #@a
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    #@d
    move-result-wide v2

    #@e
    double-to-float v2, v2

    #@f
    return v2
.end method

.method public static dist(FFFFFF)F
    .registers 11
    .parameter "x1"
    .parameter "y1"
    .parameter "z1"
    .parameter "x2"
    .parameter "y2"
    .parameter "z2"

    #@0
    .prologue
    .line 101
    sub-float v0, p3, p0

    #@2
    .line 102
    .local v0, x:F
    sub-float v1, p4, p1

    #@4
    .line 103
    .local v1, y:F
    sub-float v2, p5, p2

    #@6
    .line 104
    .local v2, z:F
    mul-float v3, v0, v0

    #@8
    mul-float v4, v1, v1

    #@a
    add-float/2addr v3, v4

    #@b
    mul-float v4, v2, v2

    #@d
    add-float/2addr v3, v4

    #@e
    float-to-double v3, v3

    #@f
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    #@12
    move-result-wide v3

    #@13
    double-to-float v3, v3

    #@14
    return v3
.end method

.method public static exp(F)F
    .registers 3
    .parameter "a"

    #@0
    .prologue
    .line 55
    float-to-double v0, p0

    #@1
    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    #@4
    move-result-wide v0

    #@5
    double-to-float v0, v0

    #@6
    return v0
.end method

.method public static lerp(FFF)F
    .registers 4
    .parameter "start"
    .parameter "stop"
    .parameter "amount"

    #@0
    .prologue
    .line 148
    sub-float v0, p1, p0

    #@2
    mul-float/2addr v0, p2

    #@3
    add-float/2addr v0, p0

    #@4
    return v0
.end method

.method public static log(F)F
    .registers 3
    .parameter "a"

    #@0
    .prologue
    .line 51
    float-to-double v0, p0

    #@1
    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    #@4
    move-result-wide v0

    #@5
    double-to-float v0, v0

    #@6
    return v0
.end method

.method public static mag(FF)F
    .registers 4
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 108
    mul-float v0, p0, p0

    #@2
    mul-float v1, p1, p1

    #@4
    add-float/2addr v0, v1

    #@5
    float-to-double v0, v0

    #@6
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    #@9
    move-result-wide v0

    #@a
    double-to-float v0, v0

    #@b
    return v0
.end method

.method public static mag(FFF)F
    .registers 5
    .parameter "a"
    .parameter "b"
    .parameter "c"

    #@0
    .prologue
    .line 112
    mul-float v0, p0, p0

    #@2
    mul-float v1, p1, p1

    #@4
    add-float/2addr v0, v1

    #@5
    mul-float v1, p2, p2

    #@7
    add-float/2addr v0, v1

    #@8
    float-to-double v0, v0

    #@9
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    #@c
    move-result-wide v0

    #@d
    double-to-float v0, v0

    #@e
    return v0
.end method

.method public static map(FFFFF)F
    .registers 8
    .parameter "minStart"
    .parameter "minStop"
    .parameter "maxStart"
    .parameter "maxStop"
    .parameter "value"

    #@0
    .prologue
    .line 156
    sub-float v0, p2, p3

    #@2
    sub-float v1, p4, p0

    #@4
    sub-float v2, p1, p0

    #@6
    div-float/2addr v1, v2

    #@7
    mul-float/2addr v0, v1

    #@8
    add-float/2addr v0, p2

    #@9
    return v0
.end method

.method public static max(FF)F
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 63
    cmpl-float v0, p0, p1

    #@2
    if-lez v0, :cond_5

    #@4
    .end local p0
    :goto_4
    return p0

    #@5
    .restart local p0
    :cond_5
    move p0, p1

    #@6
    goto :goto_4
.end method

.method public static max(FFF)F
    .registers 4
    .parameter "a"
    .parameter "b"
    .parameter "c"

    #@0
    .prologue
    .line 71
    cmpl-float v0, p0, p1

    #@2
    if-lez v0, :cond_a

    #@4
    cmpl-float v0, p0, p2

    #@6
    if-lez v0, :cond_9

    #@8
    move p2, p0

    #@9
    .end local p2
    :cond_9
    :goto_9
    return p2

    #@a
    .restart local p2
    :cond_a
    cmpl-float v0, p1, p2

    #@c
    if-lez v0, :cond_9

    #@e
    move p2, p1

    #@f
    goto :goto_9
.end method

.method public static max(II)F
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 67
    if-le p0, p1, :cond_4

    #@2
    int-to-float v0, p0

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    int-to-float v0, p1

    #@5
    goto :goto_3
.end method

.method public static max(III)F
    .registers 4
    .parameter "a"
    .parameter "b"
    .parameter "c"

    #@0
    .prologue
    .line 75
    if-le p0, p1, :cond_8

    #@2
    if-le p0, p2, :cond_6

    #@4
    .end local p0
    :goto_4
    int-to-float v0, p0

    #@5
    .end local p1
    :goto_5
    return v0

    #@6
    .restart local p0
    .restart local p1
    :cond_6
    move p0, p2

    #@7
    goto :goto_4

    #@8
    :cond_8
    if-le p1, p2, :cond_c

    #@a
    .end local p1
    :goto_a
    int-to-float v0, p1

    #@b
    goto :goto_5

    #@c
    .restart local p1
    :cond_c
    move p1, p2

    #@d
    goto :goto_a
.end method

.method public static min(FF)F
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 79
    cmpg-float v0, p0, p1

    #@2
    if-gez v0, :cond_5

    #@4
    .end local p0
    :goto_4
    return p0

    #@5
    .restart local p0
    :cond_5
    move p0, p1

    #@6
    goto :goto_4
.end method

.method public static min(FFF)F
    .registers 4
    .parameter "a"
    .parameter "b"
    .parameter "c"

    #@0
    .prologue
    .line 87
    cmpg-float v0, p0, p1

    #@2
    if-gez v0, :cond_a

    #@4
    cmpg-float v0, p0, p2

    #@6
    if-gez v0, :cond_9

    #@8
    move p2, p0

    #@9
    .end local p2
    :cond_9
    :goto_9
    return p2

    #@a
    .restart local p2
    :cond_a
    cmpg-float v0, p1, p2

    #@c
    if-gez v0, :cond_9

    #@e
    move p2, p1

    #@f
    goto :goto_9
.end method

.method public static min(II)F
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 83
    if-ge p0, p1, :cond_4

    #@2
    int-to-float v0, p0

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    int-to-float v0, p1

    #@5
    goto :goto_3
.end method

.method public static min(III)F
    .registers 4
    .parameter "a"
    .parameter "b"
    .parameter "c"

    #@0
    .prologue
    .line 91
    if-ge p0, p1, :cond_8

    #@2
    if-ge p0, p2, :cond_6

    #@4
    .end local p0
    :goto_4
    int-to-float v0, p0

    #@5
    .end local p1
    :goto_5
    return v0

    #@6
    .restart local p0
    .restart local p1
    :cond_6
    move p0, p2

    #@7
    goto :goto_4

    #@8
    :cond_8
    if-ge p1, p2, :cond_c

    #@a
    .end local p1
    :goto_a
    int-to-float v0, p1

    #@b
    goto :goto_5

    #@c
    .restart local p1
    :cond_c
    move p1, p2

    #@d
    goto :goto_a
.end method

.method public static norm(FFF)F
    .registers 5
    .parameter "start"
    .parameter "stop"
    .parameter "value"

    #@0
    .prologue
    .line 152
    sub-float v0, p2, p0

    #@2
    sub-float v1, p1, p0

    #@4
    div-float/2addr v0, v1

    #@5
    return v0
.end method

.method public static pow(FF)F
    .registers 6
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 59
    float-to-double v0, p0

    #@1
    float-to-double v2, p1

    #@2
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    #@5
    move-result-wide v0

    #@6
    double-to-float v0, v0

    #@7
    return v0
.end method

.method public static radians(F)F
    .registers 2
    .parameter "degrees"

    #@0
    .prologue
    .line 120
    const v0, 0x3c8efa35

    #@3
    mul-float/2addr v0, p0

    #@4
    return v0
.end method

.method public static random(F)F
    .registers 2
    .parameter "howbig"

    #@0
    .prologue
    .line 169
    sget-object v0, Landroid/util/MathUtils;->sRandom:Ljava/util/Random;

    #@2
    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    #@5
    move-result v0

    #@6
    mul-float/2addr v0, p0

    #@7
    return v0
.end method

.method public static random(FF)F
    .registers 4
    .parameter "howsmall"
    .parameter "howbig"

    #@0
    .prologue
    .line 173
    cmpl-float v0, p0, p1

    #@2
    if-ltz v0, :cond_5

    #@4
    .line 174
    .end local p0
    :goto_4
    return p0

    #@5
    .restart local p0
    :cond_5
    sget-object v0, Landroid/util/MathUtils;->sRandom:Ljava/util/Random;

    #@7
    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    #@a
    move-result v0

    #@b
    sub-float v1, p1, p0

    #@d
    mul-float/2addr v0, v1

    #@e
    add-float/2addr p0, v0

    #@f
    goto :goto_4
.end method

.method public static random(I)I
    .registers 3
    .parameter "howbig"

    #@0
    .prologue
    .line 160
    sget-object v0, Landroid/util/MathUtils;->sRandom:Ljava/util/Random;

    #@2
    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    #@5
    move-result v0

    #@6
    int-to-float v1, p0

    #@7
    mul-float/2addr v0, v1

    #@8
    float-to-int v0, v0

    #@9
    return v0
.end method

.method public static random(II)I
    .registers 4
    .parameter "howsmall"
    .parameter "howbig"

    #@0
    .prologue
    .line 164
    if-lt p0, p1, :cond_3

    #@2
    .line 165
    .end local p0
    :goto_2
    return p0

    #@3
    .restart local p0
    :cond_3
    sget-object v0, Landroid/util/MathUtils;->sRandom:Ljava/util/Random;

    #@5
    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    #@8
    move-result v0

    #@9
    sub-int v1, p1, p0

    #@b
    int-to-float v1, v1

    #@c
    mul-float/2addr v0, v1

    #@d
    int-to-float v1, p0

    #@e
    add-float/2addr v0, v1

    #@f
    float-to-int p0, v0

    #@10
    goto :goto_2
.end method

.method public static randomSeed(J)V
    .registers 3
    .parameter "seed"

    #@0
    .prologue
    .line 178
    sget-object v0, Landroid/util/MathUtils;->sRandom:Ljava/util/Random;

    #@2
    invoke-virtual {v0, p0, p1}, Ljava/util/Random;->setSeed(J)V

    #@5
    .line 179
    return-void
.end method

.method public static sq(F)F
    .registers 2
    .parameter "v"

    #@0
    .prologue
    .line 116
    mul-float v0, p0, p0

    #@2
    return v0
.end method

.method public static tan(F)F
    .registers 3
    .parameter "angle"

    #@0
    .prologue
    .line 144
    float-to-double v0, p0

    #@1
    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    #@4
    move-result-wide v0

    #@5
    double-to-float v0, v0

    #@6
    return v0
.end method
