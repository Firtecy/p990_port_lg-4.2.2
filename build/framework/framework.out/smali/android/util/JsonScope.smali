.class final enum Landroid/util/JsonScope;
.super Ljava/lang/Enum;
.source "JsonScope.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/util/JsonScope;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/util/JsonScope;

.field public static final enum CLOSED:Landroid/util/JsonScope;

.field public static final enum DANGLING_NAME:Landroid/util/JsonScope;

.field public static final enum EMPTY_ARRAY:Landroid/util/JsonScope;

.field public static final enum EMPTY_DOCUMENT:Landroid/util/JsonScope;

.field public static final enum EMPTY_OBJECT:Landroid/util/JsonScope;

.field public static final enum NONEMPTY_ARRAY:Landroid/util/JsonScope;

.field public static final enum NONEMPTY_DOCUMENT:Landroid/util/JsonScope;

.field public static final enum NONEMPTY_OBJECT:Landroid/util/JsonScope;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 28
    new-instance v0, Landroid/util/JsonScope;

    #@7
    const-string v1, "EMPTY_ARRAY"

    #@9
    invoke-direct {v0, v1, v3}, Landroid/util/JsonScope;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Landroid/util/JsonScope;->EMPTY_ARRAY:Landroid/util/JsonScope;

    #@e
    .line 34
    new-instance v0, Landroid/util/JsonScope;

    #@10
    const-string v1, "NONEMPTY_ARRAY"

    #@12
    invoke-direct {v0, v1, v4}, Landroid/util/JsonScope;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Landroid/util/JsonScope;->NONEMPTY_ARRAY:Landroid/util/JsonScope;

    #@17
    .line 40
    new-instance v0, Landroid/util/JsonScope;

    #@19
    const-string v1, "EMPTY_OBJECT"

    #@1b
    invoke-direct {v0, v1, v5}, Landroid/util/JsonScope;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Landroid/util/JsonScope;->EMPTY_OBJECT:Landroid/util/JsonScope;

    #@20
    .line 46
    new-instance v0, Landroid/util/JsonScope;

    #@22
    const-string v1, "DANGLING_NAME"

    #@24
    invoke-direct {v0, v1, v6}, Landroid/util/JsonScope;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Landroid/util/JsonScope;->DANGLING_NAME:Landroid/util/JsonScope;

    #@29
    .line 52
    new-instance v0, Landroid/util/JsonScope;

    #@2b
    const-string v1, "NONEMPTY_OBJECT"

    #@2d
    invoke-direct {v0, v1, v7}, Landroid/util/JsonScope;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Landroid/util/JsonScope;->NONEMPTY_OBJECT:Landroid/util/JsonScope;

    #@32
    .line 57
    new-instance v0, Landroid/util/JsonScope;

    #@34
    const-string v1, "EMPTY_DOCUMENT"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Landroid/util/JsonScope;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Landroid/util/JsonScope;->EMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@3c
    .line 62
    new-instance v0, Landroid/util/JsonScope;

    #@3e
    const-string v1, "NONEMPTY_DOCUMENT"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Landroid/util/JsonScope;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Landroid/util/JsonScope;->NONEMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@46
    .line 67
    new-instance v0, Landroid/util/JsonScope;

    #@48
    const-string v1, "CLOSED"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Landroid/util/JsonScope;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Landroid/util/JsonScope;->CLOSED:Landroid/util/JsonScope;

    #@50
    .line 22
    const/16 v0, 0x8

    #@52
    new-array v0, v0, [Landroid/util/JsonScope;

    #@54
    sget-object v1, Landroid/util/JsonScope;->EMPTY_ARRAY:Landroid/util/JsonScope;

    #@56
    aput-object v1, v0, v3

    #@58
    sget-object v1, Landroid/util/JsonScope;->NONEMPTY_ARRAY:Landroid/util/JsonScope;

    #@5a
    aput-object v1, v0, v4

    #@5c
    sget-object v1, Landroid/util/JsonScope;->EMPTY_OBJECT:Landroid/util/JsonScope;

    #@5e
    aput-object v1, v0, v5

    #@60
    sget-object v1, Landroid/util/JsonScope;->DANGLING_NAME:Landroid/util/JsonScope;

    #@62
    aput-object v1, v0, v6

    #@64
    sget-object v1, Landroid/util/JsonScope;->NONEMPTY_OBJECT:Landroid/util/JsonScope;

    #@66
    aput-object v1, v0, v7

    #@68
    const/4 v1, 0x5

    #@69
    sget-object v2, Landroid/util/JsonScope;->EMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@6b
    aput-object v2, v0, v1

    #@6d
    const/4 v1, 0x6

    #@6e
    sget-object v2, Landroid/util/JsonScope;->NONEMPTY_DOCUMENT:Landroid/util/JsonScope;

    #@70
    aput-object v2, v0, v1

    #@72
    const/4 v1, 0x7

    #@73
    sget-object v2, Landroid/util/JsonScope;->CLOSED:Landroid/util/JsonScope;

    #@75
    aput-object v2, v0, v1

    #@77
    sput-object v0, Landroid/util/JsonScope;->$VALUES:[Landroid/util/JsonScope;

    #@79
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/util/JsonScope;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 22
    const-class v0, Landroid/util/JsonScope;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/util/JsonScope;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/util/JsonScope;
    .registers 1

    #@0
    .prologue
    .line 22
    sget-object v0, Landroid/util/JsonScope;->$VALUES:[Landroid/util/JsonScope;

    #@2
    invoke-virtual {v0}, [Landroid/util/JsonScope;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/util/JsonScope;

    #@8
    return-object v0
.end method
