.class public final Landroid/util/Spline;
.super Ljava/lang/Object;
.source "Spline.java"


# instance fields
.field private final mM:[F

.field private final mX:[F

.field private final mY:[F


# direct methods
.method private constructor <init>([F[F[F)V
    .registers 4
    .parameter "x"
    .parameter "y"
    .parameter "m"

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    iput-object p1, p0, Landroid/util/Spline;->mX:[F

    #@5
    .line 30
    iput-object p2, p0, Landroid/util/Spline;->mY:[F

    #@7
    .line 31
    iput-object p3, p0, Landroid/util/Spline;->mM:[F

    #@9
    .line 32
    return-void
.end method

.method public static createMonotoneCubicSpline([F[F)Landroid/util/Spline;
    .registers 14
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v11, 0x0

    #@2
    .line 53
    if-eqz p0, :cond_e

    #@4
    if-eqz p1, :cond_e

    #@6
    array-length v8, p0

    #@7
    array-length v9, p1

    #@8
    if-ne v8, v9, :cond_e

    #@a
    array-length v8, p0

    #@b
    const/4 v9, 0x2

    #@c
    if-ge v8, v9, :cond_16

    #@e
    .line 54
    :cond_e
    new-instance v8, Ljava/lang/IllegalArgumentException;

    #@10
    const-string v9, "There must be at least two control points and the arrays must be of equal length."

    #@12
    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v8

    #@16
    .line 58
    :cond_16
    array-length v6, p0

    #@17
    .line 59
    .local v6, n:I
    add-int/lit8 v8, v6, -0x1

    #@19
    new-array v2, v8, [F

    #@1b
    .line 60
    .local v2, d:[F
    new-array v5, v6, [F

    #@1d
    .line 63
    .local v5, m:[F
    const/4 v4, 0x0

    #@1e
    .local v4, i:I
    :goto_1e
    add-int/lit8 v8, v6, -0x1

    #@20
    if-ge v4, v8, :cond_43

    #@22
    .line 64
    add-int/lit8 v8, v4, 0x1

    #@24
    aget v8, p0, v8

    #@26
    aget v9, p0, v4

    #@28
    sub-float v3, v8, v9

    #@2a
    .line 65
    .local v3, h:F
    cmpg-float v8, v3, v11

    #@2c
    if-gtz v8, :cond_36

    #@2e
    .line 66
    new-instance v8, Ljava/lang/IllegalArgumentException;

    #@30
    const-string v9, "The control points must all have strictly increasing X values."

    #@32
    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@35
    throw v8

    #@36
    .line 69
    :cond_36
    add-int/lit8 v8, v4, 0x1

    #@38
    aget v8, p1, v8

    #@3a
    aget v9, p1, v4

    #@3c
    sub-float/2addr v8, v9

    #@3d
    div-float/2addr v8, v3

    #@3e
    aput v8, v2, v4

    #@40
    .line 63
    add-int/lit8 v4, v4, 0x1

    #@42
    goto :goto_1e

    #@43
    .line 73
    .end local v3           #h:F
    :cond_43
    aget v8, v2, v10

    #@45
    aput v8, v5, v10

    #@47
    .line 74
    const/4 v4, 0x1

    #@48
    :goto_48
    add-int/lit8 v8, v6, -0x1

    #@4a
    if-ge v4, v8, :cond_5b

    #@4c
    .line 75
    add-int/lit8 v8, v4, -0x1

    #@4e
    aget v8, v2, v8

    #@50
    aget v9, v2, v4

    #@52
    add-float/2addr v8, v9

    #@53
    const/high16 v9, 0x3f00

    #@55
    mul-float/2addr v8, v9

    #@56
    aput v8, v5, v4

    #@58
    .line 74
    add-int/lit8 v4, v4, 0x1

    #@5a
    goto :goto_48

    #@5b
    .line 77
    :cond_5b
    add-int/lit8 v8, v6, -0x1

    #@5d
    add-int/lit8 v9, v6, -0x2

    #@5f
    aget v9, v2, v9

    #@61
    aput v9, v5, v8

    #@63
    .line 80
    const/4 v4, 0x0

    #@64
    :goto_64
    add-int/lit8 v8, v6, -0x1

    #@66
    if-ge v4, v8, :cond_b4

    #@68
    .line 81
    aget v8, v2, v4

    #@6a
    cmpl-float v8, v8, v11

    #@6c
    if-nez v8, :cond_77

    #@6e
    .line 82
    aput v11, v5, v4

    #@70
    .line 83
    add-int/lit8 v8, v4, 0x1

    #@72
    aput v11, v5, v8

    #@74
    .line 80
    :cond_74
    :goto_74
    add-int/lit8 v4, v4, 0x1

    #@76
    goto :goto_64

    #@77
    .line 85
    :cond_77
    aget v8, v5, v4

    #@79
    aget v9, v2, v4

    #@7b
    div-float v0, v8, v9

    #@7d
    .line 86
    .local v0, a:F
    add-int/lit8 v8, v4, 0x1

    #@7f
    aget v8, v5, v8

    #@81
    aget v9, v2, v4

    #@83
    div-float v1, v8, v9

    #@85
    .line 87
    .local v1, b:F
    cmpg-float v8, v0, v11

    #@87
    if-ltz v8, :cond_8d

    #@89
    cmpg-float v8, v1, v11

    #@8b
    if-gez v8, :cond_95

    #@8d
    .line 88
    :cond_8d
    new-instance v8, Ljava/lang/IllegalArgumentException;

    #@8f
    const-string v9, "The control points must have monotonic Y values."

    #@91
    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@94
    throw v8

    #@95
    .line 91
    :cond_95
    invoke-static {v0, v1}, Landroid/util/FloatMath;->hypot(FF)F

    #@98
    move-result v3

    #@99
    .line 92
    .restart local v3       #h:F
    const/high16 v8, 0x4110

    #@9b
    cmpl-float v8, v3, v8

    #@9d
    if-lez v8, :cond_74

    #@9f
    .line 93
    const/high16 v8, 0x4040

    #@a1
    div-float v7, v8, v3

    #@a3
    .line 94
    .local v7, t:F
    mul-float v8, v7, v0

    #@a5
    aget v9, v2, v4

    #@a7
    mul-float/2addr v8, v9

    #@a8
    aput v8, v5, v4

    #@aa
    .line 95
    add-int/lit8 v8, v4, 0x1

    #@ac
    mul-float v9, v7, v1

    #@ae
    aget v10, v2, v4

    #@b0
    mul-float/2addr v9, v10

    #@b1
    aput v9, v5, v8

    #@b3
    goto :goto_74

    #@b4
    .line 99
    .end local v0           #a:F
    .end local v1           #b:F
    .end local v3           #h:F
    .end local v7           #t:F
    :cond_b4
    new-instance v8, Landroid/util/Spline;

    #@b6
    invoke-direct {v8, p0, p1, v5}, Landroid/util/Spline;-><init>([F[F[F)V

    #@b9
    return-object v8
.end method


# virtual methods
.method public interpolate(F)F
    .registers 11
    .parameter "x"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/high16 v7, 0x4000

    #@3
    const/high16 v8, 0x3f80

    #@5
    .line 111
    iget-object v4, p0, Landroid/util/Spline;->mX:[F

    #@7
    array-length v2, v4

    #@8
    .line 112
    .local v2, n:I
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    #@b
    move-result v4

    #@c
    if-eqz v4, :cond_f

    #@e
    .line 135
    .end local p1
    :goto_e
    return p1

    #@f
    .line 115
    .restart local p1
    :cond_f
    iget-object v4, p0, Landroid/util/Spline;->mX:[F

    #@11
    aget v4, v4, v5

    #@13
    cmpg-float v4, p1, v4

    #@15
    if-gtz v4, :cond_1c

    #@17
    .line 116
    iget-object v4, p0, Landroid/util/Spline;->mY:[F

    #@19
    aget p1, v4, v5

    #@1b
    goto :goto_e

    #@1c
    .line 118
    :cond_1c
    iget-object v4, p0, Landroid/util/Spline;->mX:[F

    #@1e
    add-int/lit8 v5, v2, -0x1

    #@20
    aget v4, v4, v5

    #@22
    cmpl-float v4, p1, v4

    #@24
    if-ltz v4, :cond_2d

    #@26
    .line 119
    iget-object v4, p0, Landroid/util/Spline;->mY:[F

    #@28
    add-int/lit8 v5, v2, -0x1

    #@2a
    aget p1, v4, v5

    #@2c
    goto :goto_e

    #@2d
    .line 124
    :cond_2d
    const/4 v1, 0x0

    #@2e
    .line 125
    .local v1, i:I
    :cond_2e
    iget-object v4, p0, Landroid/util/Spline;->mX:[F

    #@30
    add-int/lit8 v5, v1, 0x1

    #@32
    aget v4, v4, v5

    #@34
    cmpl-float v4, p1, v4

    #@36
    if-ltz v4, :cond_47

    #@38
    .line 126
    add-int/lit8 v1, v1, 0x1

    #@3a
    .line 127
    iget-object v4, p0, Landroid/util/Spline;->mX:[F

    #@3c
    aget v4, v4, v1

    #@3e
    cmpl-float v4, p1, v4

    #@40
    if-nez v4, :cond_2e

    #@42
    .line 128
    iget-object v4, p0, Landroid/util/Spline;->mY:[F

    #@44
    aget p1, v4, v1

    #@46
    goto :goto_e

    #@47
    .line 133
    :cond_47
    iget-object v4, p0, Landroid/util/Spline;->mX:[F

    #@49
    add-int/lit8 v5, v1, 0x1

    #@4b
    aget v4, v4, v5

    #@4d
    iget-object v5, p0, Landroid/util/Spline;->mX:[F

    #@4f
    aget v5, v5, v1

    #@51
    sub-float v0, v4, v5

    #@53
    .line 134
    .local v0, h:F
    iget-object v4, p0, Landroid/util/Spline;->mX:[F

    #@55
    aget v4, v4, v1

    #@57
    sub-float v4, p1, v4

    #@59
    div-float v3, v4, v0

    #@5b
    .line 135
    .local v3, t:F
    iget-object v4, p0, Landroid/util/Spline;->mY:[F

    #@5d
    aget v4, v4, v1

    #@5f
    mul-float v5, v7, v3

    #@61
    add-float/2addr v5, v8

    #@62
    mul-float/2addr v4, v5

    #@63
    iget-object v5, p0, Landroid/util/Spline;->mM:[F

    #@65
    aget v5, v5, v1

    #@67
    mul-float/2addr v5, v0

    #@68
    mul-float/2addr v5, v3

    #@69
    add-float/2addr v4, v5

    #@6a
    sub-float v5, v8, v3

    #@6c
    mul-float/2addr v4, v5

    #@6d
    sub-float v5, v8, v3

    #@6f
    mul-float/2addr v4, v5

    #@70
    iget-object v5, p0, Landroid/util/Spline;->mY:[F

    #@72
    add-int/lit8 v6, v1, 0x1

    #@74
    aget v5, v5, v6

    #@76
    const/high16 v6, 0x4040

    #@78
    mul-float/2addr v7, v3

    #@79
    sub-float/2addr v6, v7

    #@7a
    mul-float/2addr v5, v6

    #@7b
    iget-object v6, p0, Landroid/util/Spline;->mM:[F

    #@7d
    add-int/lit8 v7, v1, 0x1

    #@7f
    aget v6, v6, v7

    #@81
    mul-float/2addr v6, v0

    #@82
    sub-float v7, v3, v8

    #@84
    mul-float/2addr v6, v7

    #@85
    add-float/2addr v5, v6

    #@86
    mul-float/2addr v5, v3

    #@87
    mul-float/2addr v5, v3

    #@88
    add-float p1, v4, v5

    #@8a
    goto :goto_e
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 143
    .local v2, str:Ljava/lang/StringBuilder;
    iget-object v3, p0, Landroid/util/Spline;->mX:[F

    #@7
    array-length v1, v3

    #@8
    .line 144
    .local v1, n:I
    const-string v3, "["

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 145
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    if-ge v0, v1, :cond_47

    #@10
    .line 146
    if-eqz v0, :cond_17

    #@12
    .line 147
    const-string v3, ", "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 149
    :cond_17
    const-string v3, "("

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    iget-object v4, p0, Landroid/util/Spline;->mX:[F

    #@1f
    aget v4, v4, v0

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@24
    .line 150
    const-string v3, ", "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    iget-object v4, p0, Landroid/util/Spline;->mY:[F

    #@2c
    aget v4, v4, v0

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@31
    .line 151
    const-string v3, ": "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    iget-object v4, p0, Landroid/util/Spline;->mM:[F

    #@39
    aget v4, v4, v0

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    const-string v4, ")"

    #@41
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    .line 145
    add-int/lit8 v0, v0, 0x1

    #@46
    goto :goto_e

    #@47
    .line 153
    :cond_47
    const-string v3, "]"

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    .line 154
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    return-object v3
.end method
