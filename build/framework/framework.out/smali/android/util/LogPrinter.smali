.class public Landroid/util/LogPrinter;
.super Ljava/lang/Object;
.source "LogPrinter.java"

# interfaces
.implements Landroid/util/Printer;


# instance fields
.field private final mBuffer:I

.field private final mPriority:I

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .registers 4
    .parameter "priority"
    .parameter "tag"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    iput p1, p0, Landroid/util/LogPrinter;->mPriority:I

    #@5
    .line 42
    iput-object p2, p0, Landroid/util/LogPrinter;->mTag:Ljava/lang/String;

    #@7
    .line 43
    const/4 v0, 0x0

    #@8
    iput v0, p0, Landroid/util/LogPrinter;->mBuffer:I

    #@a
    .line 44
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;I)V
    .registers 4
    .parameter "priority"
    .parameter "tag"
    .parameter "buffer"

    #@0
    .prologue
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    iput p1, p0, Landroid/util/LogPrinter;->mPriority:I

    #@5
    .line 52
    iput-object p2, p0, Landroid/util/LogPrinter;->mTag:Ljava/lang/String;

    #@7
    .line 53
    iput p3, p0, Landroid/util/LogPrinter;->mBuffer:I

    #@9
    .line 54
    return-void
.end method


# virtual methods
.method public println(Ljava/lang/String;)V
    .registers 5
    .parameter "x"

    #@0
    .prologue
    .line 57
    iget v0, p0, Landroid/util/LogPrinter;->mBuffer:I

    #@2
    iget v1, p0, Landroid/util/LogPrinter;->mPriority:I

    #@4
    iget-object v2, p0, Landroid/util/LogPrinter;->mTag:Ljava/lang/String;

    #@6
    invoke-static {v0, v1, v2, p1}, Landroid/util/Log;->println_native(IILjava/lang/String;Ljava/lang/String;)I

    #@9
    .line 58
    return-void
.end method
