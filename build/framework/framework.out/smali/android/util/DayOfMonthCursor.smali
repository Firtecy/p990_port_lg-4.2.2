.class public Landroid/util/DayOfMonthCursor;
.super Landroid/util/MonthDisplayHelper;
.source "DayOfMonthCursor.java"


# instance fields
.field private mColumn:I

.field private mRow:I


# direct methods
.method public constructor <init>(IIII)V
    .registers 6
    .parameter "year"
    .parameter "month"
    .parameter "dayOfMonth"
    .parameter "weekStartDay"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p4}, Landroid/util/MonthDisplayHelper;-><init>(III)V

    #@3
    .line 50
    invoke-virtual {p0, p3}, Landroid/util/DayOfMonthCursor;->getRowOf(I)I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@9
    .line 51
    invoke-virtual {p0, p3}, Landroid/util/DayOfMonthCursor;->getColumnOf(I)I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@f
    .line 52
    return-void
.end method


# virtual methods
.method public down()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 121
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@3
    add-int/lit8 v1, v1, 0x1

    #@5
    iget v2, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@7
    invoke-virtual {p0, v1, v2}, Landroid/util/DayOfMonthCursor;->isWithinCurrentMonth(II)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_14

    #@d
    .line 123
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@f
    add-int/lit8 v1, v1, 0x1

    #@11
    iput v1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@13
    .line 132
    :goto_13
    return v0

    #@14
    .line 127
    :cond_14
    invoke-virtual {p0}, Landroid/util/DayOfMonthCursor;->nextMonth()V

    #@17
    .line 128
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@19
    .line 129
    :goto_19
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@1b
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@1d
    invoke-virtual {p0, v0, v1}, Landroid/util/DayOfMonthCursor;->isWithinCurrentMonth(II)Z

    #@20
    move-result v0

    #@21
    if-nez v0, :cond_2a

    #@23
    .line 130
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@25
    add-int/lit8 v0, v0, 0x1

    #@27
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@29
    goto :goto_19

    #@2a
    .line 132
    :cond_2a
    const/4 v0, 0x1

    #@2b
    goto :goto_13
.end method

.method public getSelectedColumn()I
    .registers 2

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@2
    return v0
.end method

.method public getSelectedDayOfMonth()I
    .registers 3

    #@0
    .prologue
    .line 69
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@2
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/util/DayOfMonthCursor;->getDayAt(II)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getSelectedMonthOffset()I
    .registers 3

    #@0
    .prologue
    .line 77
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@2
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/util/DayOfMonthCursor;->isWithinCurrentMonth(II)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    .line 78
    const/4 v0, 0x0

    #@b
    .line 83
    :goto_b
    return v0

    #@c
    .line 80
    :cond_c
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@e
    if-nez v0, :cond_12

    #@10
    .line 81
    const/4 v0, -0x1

    #@11
    goto :goto_b

    #@12
    .line 83
    :cond_12
    const/4 v0, 0x1

    #@13
    goto :goto_b
.end method

.method public getSelectedRow()I
    .registers 2

    #@0
    .prologue
    .line 56
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@2
    return v0
.end method

.method public isSelected(II)Z
    .registers 4
    .parameter "row"
    .parameter "column"

    #@0
    .prologue
    .line 92
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@2
    if-ne v0, p1, :cond_a

    #@4
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@6
    if-ne v0, p2, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public left()Z
    .registers 4

    #@0
    .prologue
    .line 141
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@2
    if-nez v1, :cond_19

    #@4
    .line 142
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@6
    add-int/lit8 v1, v1, -0x1

    #@8
    iput v1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@a
    .line 143
    const/4 v1, 0x6

    #@b
    iput v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@d
    .line 148
    :goto_d
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@f
    iget v2, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@11
    invoke-virtual {p0, v1, v2}, Landroid/util/DayOfMonthCursor;->isWithinCurrentMonth(II)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_20

    #@17
    .line 149
    const/4 v1, 0x0

    #@18
    .line 157
    :goto_18
    return v1

    #@19
    .line 145
    :cond_19
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@1b
    add-int/lit8 v1, v1, -0x1

    #@1d
    iput v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@1f
    goto :goto_d

    #@20
    .line 153
    :cond_20
    invoke-virtual {p0}, Landroid/util/DayOfMonthCursor;->previousMonth()V

    #@23
    .line 154
    invoke-virtual {p0}, Landroid/util/DayOfMonthCursor;->getNumberOfDaysInMonth()I

    #@26
    move-result v0

    #@27
    .line 155
    .local v0, lastDay:I
    invoke-virtual {p0, v0}, Landroid/util/DayOfMonthCursor;->getRowOf(I)I

    #@2a
    move-result v1

    #@2b
    iput v1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@2d
    .line 156
    invoke-virtual {p0, v0}, Landroid/util/DayOfMonthCursor;->getColumnOf(I)I

    #@30
    move-result v1

    #@31
    iput v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@33
    .line 157
    const/4 v1, 0x1

    #@34
    goto :goto_18
.end method

.method public right()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 166
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@3
    const/4 v2, 0x6

    #@4
    if-ne v1, v2, :cond_19

    #@6
    .line 167
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@8
    add-int/lit8 v1, v1, 0x1

    #@a
    iput v1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@c
    .line 168
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@e
    .line 173
    :goto_e
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@10
    iget v2, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@12
    invoke-virtual {p0, v1, v2}, Landroid/util/DayOfMonthCursor;->isWithinCurrentMonth(II)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_20

    #@18
    .line 184
    :goto_18
    return v0

    #@19
    .line 170
    :cond_19
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    iput v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@1f
    goto :goto_e

    #@20
    .line 178
    :cond_20
    invoke-virtual {p0}, Landroid/util/DayOfMonthCursor;->nextMonth()V

    #@23
    .line 179
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@25
    .line 180
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@27
    .line 181
    :goto_27
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@29
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@2b
    invoke-virtual {p0, v0, v1}, Landroid/util/DayOfMonthCursor;->isWithinCurrentMonth(II)Z

    #@2e
    move-result v0

    #@2f
    if-nez v0, :cond_38

    #@31
    .line 182
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@33
    add-int/lit8 v0, v0, 0x1

    #@35
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@37
    goto :goto_27

    #@38
    .line 184
    :cond_38
    const/4 v0, 0x1

    #@39
    goto :goto_18
.end method

.method public setSelectedDayOfMonth(I)V
    .registers 3
    .parameter "dayOfMonth"

    #@0
    .prologue
    .line 87
    invoke-virtual {p0, p1}, Landroid/util/DayOfMonthCursor;->getRowOf(I)I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@6
    .line 88
    invoke-virtual {p0, p1}, Landroid/util/DayOfMonthCursor;->getColumnOf(I)I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@c
    .line 89
    return-void
.end method

.method public setSelectedRowColumn(II)V
    .registers 3
    .parameter "row"
    .parameter "col"

    #@0
    .prologue
    .line 64
    iput p1, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@2
    .line 65
    iput p2, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@4
    .line 66
    return-void
.end method

.method public up()Z
    .registers 3

    #@0
    .prologue
    .line 101
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/util/DayOfMonthCursor;->isWithinCurrentMonth(II)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_14

    #@c
    .line 103
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@e
    add-int/lit8 v0, v0, -0x1

    #@10
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@12
    .line 104
    const/4 v0, 0x0

    #@13
    .line 112
    :goto_13
    return v0

    #@14
    .line 107
    :cond_14
    invoke-virtual {p0}, Landroid/util/DayOfMonthCursor;->previousMonth()V

    #@17
    .line 108
    const/4 v0, 0x5

    #@18
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@1a
    .line 109
    :goto_1a
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@1c
    iget v1, p0, Landroid/util/DayOfMonthCursor;->mColumn:I

    #@1e
    invoke-virtual {p0, v0, v1}, Landroid/util/DayOfMonthCursor;->isWithinCurrentMonth(II)Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_2b

    #@24
    .line 110
    iget v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@26
    add-int/lit8 v0, v0, -0x1

    #@28
    iput v0, p0, Landroid/util/DayOfMonthCursor;->mRow:I

    #@2a
    goto :goto_1a

    #@2b
    .line 112
    :cond_2b
    const/4 v0, 0x1

    #@2c
    goto :goto_13
.end method
