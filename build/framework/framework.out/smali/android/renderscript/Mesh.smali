.class public Landroid/renderscript/Mesh;
.super Landroid/renderscript/BaseObj;
.source "Mesh.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Mesh$TriangleMeshBuilder;,
        Landroid/renderscript/Mesh$AllocationBuilder;,
        Landroid/renderscript/Mesh$Builder;,
        Landroid/renderscript/Mesh$Primitive;
    }
.end annotation


# instance fields
.field mIndexBuffers:[Landroid/renderscript/Allocation;

.field mPrimitives:[Landroid/renderscript/Mesh$Primitive;

.field mVertexBuffers:[Landroid/renderscript/Allocation;


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 95
    return-void
.end method


# virtual methods
.method public getIndexSetAllocation(I)Landroid/renderscript/Allocation;
    .registers 3
    .parameter "slot"

    #@0
    .prologue
    .line 138
    iget-object v0, p0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    #@2
    aget-object v0, v0, p1

    #@4
    return-object v0
.end method

.method public getPrimitive(I)Landroid/renderscript/Mesh$Primitive;
    .registers 3
    .parameter "slot"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Landroid/renderscript/Mesh;->mPrimitives:[Landroid/renderscript/Mesh$Primitive;

    #@2
    aget-object v0, v0, p1

    #@4
    return-object v0
.end method

.method public getPrimitiveCount()I
    .registers 2

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 125
    const/4 v0, 0x0

    #@5
    .line 127
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    #@8
    array-length v0, v0

    #@9
    goto :goto_5
.end method

.method public getVertexAllocation(I)Landroid/renderscript/Allocation;
    .registers 3
    .parameter "slot"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    #@2
    aget-object v0, v0, p1

    #@4
    return-object v0
.end method

.method public getVertexAllocationCount()I
    .registers 2

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 104
    const/4 v0, 0x0

    #@5
    .line 106
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    #@8
    array-length v0, v0

    #@9
    goto :goto_5
.end method

.method updateFromNative()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 152
    invoke-super {p0}, Landroid/renderscript/BaseObj;->updateFromNative()V

    #@5
    .line 153
    iget-object v6, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@7
    iget-object v7, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@9
    invoke-virtual {p0, v7}, Landroid/renderscript/Mesh;->getID(Landroid/renderscript/RenderScript;)I

    #@c
    move-result v7

    #@d
    invoke-virtual {v6, v7}, Landroid/renderscript/RenderScript;->nMeshGetVertexBufferCount(I)I

    #@10
    move-result v4

    #@11
    .line 154
    .local v4, vtxCount:I
    iget-object v6, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@13
    iget-object v7, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@15
    invoke-virtual {p0, v7}, Landroid/renderscript/Mesh;->getID(Landroid/renderscript/RenderScript;)I

    #@18
    move-result v7

    #@19
    invoke-virtual {v6, v7}, Landroid/renderscript/RenderScript;->nMeshGetIndexCount(I)I

    #@1c
    move-result v1

    #@1d
    .line 156
    .local v1, idxCount:I
    new-array v5, v4, [I

    #@1f
    .line 157
    .local v5, vtxIDs:[I
    new-array v2, v1, [I

    #@21
    .line 158
    .local v2, idxIDs:[I
    new-array v3, v1, [I

    #@23
    .line 160
    .local v3, primitives:[I
    iget-object v6, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@25
    iget-object v7, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@27
    invoke-virtual {p0, v7}, Landroid/renderscript/Mesh;->getID(Landroid/renderscript/RenderScript;)I

    #@2a
    move-result v7

    #@2b
    invoke-virtual {v6, v7, v5, v4}, Landroid/renderscript/RenderScript;->nMeshGetVertices(I[II)V

    #@2e
    .line 161
    iget-object v6, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@30
    iget-object v7, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@32
    invoke-virtual {p0, v7}, Landroid/renderscript/Mesh;->getID(Landroid/renderscript/RenderScript;)I

    #@35
    move-result v7

    #@36
    invoke-virtual {v6, v7, v2, v3, v1}, Landroid/renderscript/RenderScript;->nMeshGetIndices(I[I[II)V

    #@39
    .line 163
    new-array v6, v4, [Landroid/renderscript/Allocation;

    #@3b
    iput-object v6, p0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    #@3d
    .line 164
    new-array v6, v1, [Landroid/renderscript/Allocation;

    #@3f
    iput-object v6, p0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    #@41
    .line 165
    new-array v6, v1, [Landroid/renderscript/Mesh$Primitive;

    #@43
    iput-object v6, p0, Landroid/renderscript/Mesh;->mPrimitives:[Landroid/renderscript/Mesh$Primitive;

    #@45
    .line 167
    const/4 v0, 0x0

    #@46
    .local v0, i:I
    :goto_46
    if-ge v0, v4, :cond_63

    #@48
    .line 168
    aget v6, v5, v0

    #@4a
    if-eqz v6, :cond_60

    #@4c
    .line 169
    iget-object v6, p0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    #@4e
    new-instance v7, Landroid/renderscript/Allocation;

    #@50
    aget v8, v5, v0

    #@52
    iget-object v9, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@54
    invoke-direct {v7, v8, v9, v11, v10}, Landroid/renderscript/Allocation;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Type;I)V

    #@57
    aput-object v7, v6, v0

    #@59
    .line 170
    iget-object v6, p0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    #@5b
    aget-object v6, v6, v0

    #@5d
    invoke-virtual {v6}, Landroid/renderscript/Allocation;->updateFromNative()V

    #@60
    .line 167
    :cond_60
    add-int/lit8 v0, v0, 0x1

    #@62
    goto :goto_46

    #@63
    .line 174
    :cond_63
    const/4 v0, 0x0

    #@64
    :goto_64
    if-ge v0, v1, :cond_8d

    #@66
    .line 175
    aget v6, v2, v0

    #@68
    if-eqz v6, :cond_7e

    #@6a
    .line 176
    iget-object v6, p0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    #@6c
    new-instance v7, Landroid/renderscript/Allocation;

    #@6e
    aget v8, v2, v0

    #@70
    iget-object v9, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@72
    invoke-direct {v7, v8, v9, v11, v10}, Landroid/renderscript/Allocation;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Type;I)V

    #@75
    aput-object v7, v6, v0

    #@77
    .line 177
    iget-object v6, p0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    #@79
    aget-object v6, v6, v0

    #@7b
    invoke-virtual {v6}, Landroid/renderscript/Allocation;->updateFromNative()V

    #@7e
    .line 179
    :cond_7e
    iget-object v6, p0, Landroid/renderscript/Mesh;->mPrimitives:[Landroid/renderscript/Mesh$Primitive;

    #@80
    invoke-static {}, Landroid/renderscript/Mesh$Primitive;->values()[Landroid/renderscript/Mesh$Primitive;

    #@83
    move-result-object v7

    #@84
    aget v8, v3, v0

    #@86
    aget-object v7, v7, v8

    #@88
    aput-object v7, v6, v0

    #@8a
    .line 174
    add-int/lit8 v0, v0, 0x1

    #@8c
    goto :goto_64

    #@8d
    .line 181
    :cond_8d
    return-void
.end method
