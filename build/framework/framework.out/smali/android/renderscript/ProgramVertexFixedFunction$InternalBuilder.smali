.class Landroid/renderscript/ProgramVertexFixedFunction$InternalBuilder;
.super Landroid/renderscript/Program$BaseProgramBuilder;
.source "ProgramVertexFixedFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/ProgramVertexFixedFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InternalBuilder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;)V
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/renderscript/Program$BaseProgramBuilder;-><init>(Landroid/renderscript/RenderScript;)V

    #@3
    .line 55
    return-void
.end method


# virtual methods
.method public addInput(Landroid/renderscript/Element;)Landroid/renderscript/ProgramVertexFixedFunction$InternalBuilder;
    .registers 5
    .parameter "e"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 62
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputCount:I

    #@2
    const/16 v1, 0x8

    #@4
    if-lt v0, v1, :cond_e

    #@6
    .line 63
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@8
    const-string v1, "Max input count exceeded."

    #@a
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 65
    :cond_e
    invoke-virtual {p1}, Landroid/renderscript/Element;->isComplex()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1c

    #@14
    .line 66
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@16
    const-string v1, "Complex elements not allowed."

    #@18
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 68
    :cond_1c
    iget-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputs:[Landroid/renderscript/Element;

    #@1e
    iget v1, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputCount:I

    #@20
    add-int/lit8 v2, v1, 0x1

    #@22
    iput v2, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputCount:I

    #@24
    aput-object p1, v0, v1

    #@26
    .line 69
    return-object p0
.end method

.method public create()Landroid/renderscript/ProgramVertexFixedFunction;
    .registers 10

    #@0
    .prologue
    .line 80
    iget-object v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v7}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 81
    iget v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputCount:I

    #@7
    iget v8, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mOutputCount:I

    #@9
    add-int/2addr v7, v8

    #@a
    iget v8, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstantCount:I

    #@c
    add-int/2addr v7, v8

    #@d
    iget v8, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@f
    add-int/2addr v7, v8

    #@10
    mul-int/lit8 v7, v7, 0x2

    #@12
    new-array v6, v7, [I

    #@14
    .line 82
    .local v6, tmp:[I
    iget v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@16
    new-array v5, v7, [Ljava/lang/String;

    #@18
    .line 83
    .local v5, texNames:[Ljava/lang/String;
    const/4 v2, 0x0

    #@19
    .line 85
    .local v2, idx:I
    const/4 v0, 0x0

    #@1a
    .local v0, i:I
    :goto_1a
    iget v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputCount:I

    #@1c
    if-ge v0, v7, :cond_37

    #@1e
    .line 86
    add-int/lit8 v3, v2, 0x1

    #@20
    .end local v2           #idx:I
    .local v3, idx:I
    sget-object v7, Landroid/renderscript/Program$ProgramParam;->INPUT:Landroid/renderscript/Program$ProgramParam;

    #@22
    iget v7, v7, Landroid/renderscript/Program$ProgramParam;->mID:I

    #@24
    aput v7, v6, v2

    #@26
    .line 87
    add-int/lit8 v2, v3, 0x1

    #@28
    .end local v3           #idx:I
    .restart local v2       #idx:I
    iget-object v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputs:[Landroid/renderscript/Element;

    #@2a
    aget-object v7, v7, v0

    #@2c
    iget-object v8, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@2e
    invoke-virtual {v7, v8}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@31
    move-result v7

    #@32
    aput v7, v6, v3

    #@34
    .line 85
    add-int/lit8 v0, v0, 0x1

    #@36
    goto :goto_1a

    #@37
    .line 89
    :cond_37
    const/4 v0, 0x0

    #@38
    :goto_38
    iget v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mOutputCount:I

    #@3a
    if-ge v0, v7, :cond_55

    #@3c
    .line 90
    add-int/lit8 v3, v2, 0x1

    #@3e
    .end local v2           #idx:I
    .restart local v3       #idx:I
    sget-object v7, Landroid/renderscript/Program$ProgramParam;->OUTPUT:Landroid/renderscript/Program$ProgramParam;

    #@40
    iget v7, v7, Landroid/renderscript/Program$ProgramParam;->mID:I

    #@42
    aput v7, v6, v2

    #@44
    .line 91
    add-int/lit8 v2, v3, 0x1

    #@46
    .end local v3           #idx:I
    .restart local v2       #idx:I
    iget-object v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mOutputs:[Landroid/renderscript/Element;

    #@48
    aget-object v7, v7, v0

    #@4a
    iget-object v8, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@4c
    invoke-virtual {v7, v8}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@4f
    move-result v7

    #@50
    aput v7, v6, v3

    #@52
    .line 89
    add-int/lit8 v0, v0, 0x1

    #@54
    goto :goto_38

    #@55
    .line 93
    :cond_55
    const/4 v0, 0x0

    #@56
    :goto_56
    iget v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstantCount:I

    #@58
    if-ge v0, v7, :cond_73

    #@5a
    .line 94
    add-int/lit8 v3, v2, 0x1

    #@5c
    .end local v2           #idx:I
    .restart local v3       #idx:I
    sget-object v7, Landroid/renderscript/Program$ProgramParam;->CONSTANT:Landroid/renderscript/Program$ProgramParam;

    #@5e
    iget v7, v7, Landroid/renderscript/Program$ProgramParam;->mID:I

    #@60
    aput v7, v6, v2

    #@62
    .line 95
    add-int/lit8 v2, v3, 0x1

    #@64
    .end local v3           #idx:I
    .restart local v2       #idx:I
    iget-object v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstants:[Landroid/renderscript/Type;

    #@66
    aget-object v7, v7, v0

    #@68
    iget-object v8, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@6a
    invoke-virtual {v7, v8}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)I

    #@6d
    move-result v7

    #@6e
    aput v7, v6, v3

    #@70
    .line 93
    add-int/lit8 v0, v0, 0x1

    #@72
    goto :goto_56

    #@73
    .line 97
    :cond_73
    const/4 v0, 0x0

    #@74
    :goto_74
    iget v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@76
    if-ge v0, v7, :cond_93

    #@78
    .line 98
    add-int/lit8 v3, v2, 0x1

    #@7a
    .end local v2           #idx:I
    .restart local v3       #idx:I
    sget-object v7, Landroid/renderscript/Program$ProgramParam;->TEXTURE_TYPE:Landroid/renderscript/Program$ProgramParam;

    #@7c
    iget v7, v7, Landroid/renderscript/Program$ProgramParam;->mID:I

    #@7e
    aput v7, v6, v2

    #@80
    .line 99
    add-int/lit8 v2, v3, 0x1

    #@82
    .end local v3           #idx:I
    .restart local v2       #idx:I
    iget-object v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureTypes:[Landroid/renderscript/Program$TextureType;

    #@84
    aget-object v7, v7, v0

    #@86
    iget v7, v7, Landroid/renderscript/Program$TextureType;->mID:I

    #@88
    aput v7, v6, v3

    #@8a
    .line 100
    iget-object v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureNames:[Ljava/lang/String;

    #@8c
    aget-object v7, v7, v0

    #@8e
    aput-object v7, v5, v0

    #@90
    .line 97
    add-int/lit8 v0, v0, 0x1

    #@92
    goto :goto_74

    #@93
    .line 103
    :cond_93
    iget-object v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@95
    iget-object v8, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mShader:Ljava/lang/String;

    #@97
    invoke-virtual {v7, v8, v5, v6}, Landroid/renderscript/RenderScript;->nProgramVertexCreate(Ljava/lang/String;[Ljava/lang/String;[I)I

    #@9a
    move-result v1

    #@9b
    .line 104
    .local v1, id:I
    new-instance v4, Landroid/renderscript/ProgramVertexFixedFunction;

    #@9d
    iget-object v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@9f
    invoke-direct {v4, v1, v7}, Landroid/renderscript/ProgramVertexFixedFunction;-><init>(ILandroid/renderscript/RenderScript;)V

    #@a2
    .line 105
    .local v4, pv:Landroid/renderscript/ProgramVertexFixedFunction;
    invoke-virtual {p0, v4}, Landroid/renderscript/ProgramVertexFixedFunction$InternalBuilder;->initProgram(Landroid/renderscript/Program;)V

    #@a5
    .line 106
    return-object v4
.end method
