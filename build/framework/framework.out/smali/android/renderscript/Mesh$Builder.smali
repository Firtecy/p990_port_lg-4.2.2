.class public Landroid/renderscript/Mesh$Builder;
.super Ljava/lang/Object;
.source "Mesh.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Mesh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Mesh$Builder$Entry;
    }
.end annotation


# instance fields
.field mIndexTypes:Ljava/util/Vector;

.field mRS:Landroid/renderscript/RenderScript;

.field mUsage:I

.field mVertexTypeCount:I

.field mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;I)V
    .registers 4
    .parameter "rs"
    .parameter "usage"

    #@0
    .prologue
    .line 214
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 215
    iput-object p1, p0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@5
    .line 216
    iput p2, p0, Landroid/renderscript/Mesh$Builder;->mUsage:I

    #@7
    .line 217
    const/4 v0, 0x0

    #@8
    iput v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@a
    .line 218
    const/16 v0, 0x10

    #@c
    new-array v0, v0, [Landroid/renderscript/Mesh$Builder$Entry;

    #@e
    iput-object v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@10
    .line 219
    new-instance v0, Ljava/util/Vector;

    #@12
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    #@15
    iput-object v0, p0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@17
    .line 220
    return-void
.end method


# virtual methods
.method public addIndexSetType(Landroid/renderscript/Element;ILandroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$Builder;
    .registers 6
    .parameter "e"
    .parameter "size"
    .parameter "p"

    #@0
    .prologue
    .line 330
    new-instance v0, Landroid/renderscript/Mesh$Builder$Entry;

    #@2
    invoke-direct {v0, p0}, Landroid/renderscript/Mesh$Builder$Entry;-><init>(Landroid/renderscript/Mesh$Builder;)V

    #@5
    .line 331
    .local v0, indexType:Landroid/renderscript/Mesh$Builder$Entry;
    const/4 v1, 0x0

    #@6
    iput-object v1, v0, Landroid/renderscript/Mesh$Builder$Entry;->t:Landroid/renderscript/Type;

    #@8
    .line 332
    iput-object p1, v0, Landroid/renderscript/Mesh$Builder$Entry;->e:Landroid/renderscript/Element;

    #@a
    .line 333
    iput p2, v0, Landroid/renderscript/Mesh$Builder$Entry;->size:I

    #@c
    .line 334
    iput-object p3, v0, Landroid/renderscript/Mesh$Builder$Entry;->prim:Landroid/renderscript/Mesh$Primitive;

    #@e
    .line 335
    iget-object v1, p0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@10
    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    #@13
    .line 336
    return-object p0
.end method

.method public addIndexSetType(Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$Builder;
    .registers 4
    .parameter "p"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 310
    new-instance v0, Landroid/renderscript/Mesh$Builder$Entry;

    #@3
    invoke-direct {v0, p0}, Landroid/renderscript/Mesh$Builder$Entry;-><init>(Landroid/renderscript/Mesh$Builder;)V

    #@6
    .line 311
    .local v0, indexType:Landroid/renderscript/Mesh$Builder$Entry;
    iput-object v1, v0, Landroid/renderscript/Mesh$Builder$Entry;->t:Landroid/renderscript/Type;

    #@8
    .line 312
    iput-object v1, v0, Landroid/renderscript/Mesh$Builder$Entry;->e:Landroid/renderscript/Element;

    #@a
    .line 313
    const/4 v1, 0x0

    #@b
    iput v1, v0, Landroid/renderscript/Mesh$Builder$Entry;->size:I

    #@d
    .line 314
    iput-object p1, v0, Landroid/renderscript/Mesh$Builder$Entry;->prim:Landroid/renderscript/Mesh$Primitive;

    #@f
    .line 315
    iget-object v1, p0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@11
    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    #@14
    .line 316
    return-object p0
.end method

.method public addIndexSetType(Landroid/renderscript/Type;Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$Builder;
    .registers 5
    .parameter "t"
    .parameter "p"

    #@0
    .prologue
    .line 292
    new-instance v0, Landroid/renderscript/Mesh$Builder$Entry;

    #@2
    invoke-direct {v0, p0}, Landroid/renderscript/Mesh$Builder$Entry;-><init>(Landroid/renderscript/Mesh$Builder;)V

    #@5
    .line 293
    .local v0, indexType:Landroid/renderscript/Mesh$Builder$Entry;
    iput-object p1, v0, Landroid/renderscript/Mesh$Builder$Entry;->t:Landroid/renderscript/Type;

    #@7
    .line 294
    const/4 v1, 0x0

    #@8
    iput-object v1, v0, Landroid/renderscript/Mesh$Builder$Entry;->e:Landroid/renderscript/Element;

    #@a
    .line 295
    const/4 v1, 0x0

    #@b
    iput v1, v0, Landroid/renderscript/Mesh$Builder$Entry;->size:I

    #@d
    .line 296
    iput-object p2, v0, Landroid/renderscript/Mesh$Builder$Entry;->prim:Landroid/renderscript/Mesh$Primitive;

    #@f
    .line 297
    iget-object v1, p0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@11
    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    #@14
    .line 298
    return-object p0
.end method

.method public addVertexType(Landroid/renderscript/Element;I)Landroid/renderscript/Mesh$Builder;
    .registers 6
    .parameter "e"
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 270
    iget v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@2
    iget-object v1, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@4
    array-length v1, v1

    #@5
    if-lt v0, v1, :cond_f

    #@7
    .line 271
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v1, "Max vertex types exceeded."

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 274
    :cond_f
    iget-object v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@11
    iget v1, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@13
    new-instance v2, Landroid/renderscript/Mesh$Builder$Entry;

    #@15
    invoke-direct {v2, p0}, Landroid/renderscript/Mesh$Builder$Entry;-><init>(Landroid/renderscript/Mesh$Builder;)V

    #@18
    aput-object v2, v0, v1

    #@1a
    .line 275
    iget-object v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@1c
    iget v1, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@1e
    aget-object v0, v0, v1

    #@20
    const/4 v1, 0x0

    #@21
    iput-object v1, v0, Landroid/renderscript/Mesh$Builder$Entry;->t:Landroid/renderscript/Type;

    #@23
    .line 276
    iget-object v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@25
    iget v1, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@27
    aget-object v0, v0, v1

    #@29
    iput-object p1, v0, Landroid/renderscript/Mesh$Builder$Entry;->e:Landroid/renderscript/Element;

    #@2b
    .line 277
    iget-object v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@2d
    iget v1, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@2f
    aget-object v0, v0, v1

    #@31
    iput p2, v0, Landroid/renderscript/Mesh$Builder$Entry;->size:I

    #@33
    .line 278
    iget v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@35
    add-int/lit8 v0, v0, 0x1

    #@37
    iput v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@39
    .line 279
    return-object p0
.end method

.method public addVertexType(Landroid/renderscript/Type;)Landroid/renderscript/Mesh$Builder;
    .registers 5
    .parameter "t"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 249
    iget v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@2
    iget-object v1, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@4
    array-length v1, v1

    #@5
    if-lt v0, v1, :cond_f

    #@7
    .line 250
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v1, "Max vertex types exceeded."

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 253
    :cond_f
    iget-object v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@11
    iget v1, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@13
    new-instance v2, Landroid/renderscript/Mesh$Builder$Entry;

    #@15
    invoke-direct {v2, p0}, Landroid/renderscript/Mesh$Builder$Entry;-><init>(Landroid/renderscript/Mesh$Builder;)V

    #@18
    aput-object v2, v0, v1

    #@1a
    .line 254
    iget-object v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@1c
    iget v1, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@1e
    aget-object v0, v0, v1

    #@20
    iput-object p1, v0, Landroid/renderscript/Mesh$Builder$Entry;->t:Landroid/renderscript/Type;

    #@22
    .line 255
    iget-object v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@24
    iget v1, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@26
    aget-object v0, v0, v1

    #@28
    const/4 v1, 0x0

    #@29
    iput-object v1, v0, Landroid/renderscript/Mesh$Builder$Entry;->e:Landroid/renderscript/Element;

    #@2b
    .line 256
    iget v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@2d
    add-int/lit8 v0, v0, 0x1

    #@2f
    iput v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@31
    .line 257
    return-object p0
.end method

.method public create()Landroid/renderscript/Mesh;
    .registers 18

    #@0
    .prologue
    .line 351
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {v13}, Landroid/renderscript/RenderScript;->validate()V

    #@7
    .line 352
    move-object/from16 v0, p0

    #@9
    iget v13, v0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@b
    new-array v12, v13, [I

    #@d
    .line 353
    .local v12, vtx:[I
    move-object/from16 v0, p0

    #@f
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@11
    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    #@14
    move-result v13

    #@15
    new-array v6, v13, [I

    #@17
    .line 354
    .local v6, idx:[I
    move-object/from16 v0, p0

    #@19
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@1b
    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    #@1e
    move-result v13

    #@1f
    new-array v9, v13, [I

    #@21
    .line 356
    .local v9, prim:[I
    move-object/from16 v0, p0

    #@23
    iget v13, v0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@25
    new-array v11, v13, [Landroid/renderscript/Allocation;

    #@27
    .line 357
    .local v11, vertexBuffers:[Landroid/renderscript/Allocation;
    move-object/from16 v0, p0

    #@29
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@2b
    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    #@2e
    move-result v13

    #@2f
    new-array v7, v13, [Landroid/renderscript/Allocation;

    #@31
    .line 358
    .local v7, indexBuffers:[Landroid/renderscript/Allocation;
    move-object/from16 v0, p0

    #@33
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@35
    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    #@38
    move-result v13

    #@39
    new-array v10, v13, [Landroid/renderscript/Mesh$Primitive;

    #@3b
    .line 360
    .local v10, primitives:[Landroid/renderscript/Mesh$Primitive;
    const/4 v3, 0x0

    #@3c
    .local v3, ct:I
    :goto_3c
    move-object/from16 v0, p0

    #@3e
    iget v13, v0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@40
    if-ge v3, v13, :cond_81

    #@42
    .line 361
    const/4 v1, 0x0

    #@43
    .line 362
    .local v1, alloc:Landroid/renderscript/Allocation;
    move-object/from16 v0, p0

    #@45
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mVertexTypes:[Landroid/renderscript/Mesh$Builder$Entry;

    #@47
    aget-object v4, v13, v3

    #@49
    .line 363
    .local v4, entry:Landroid/renderscript/Mesh$Builder$Entry;
    iget-object v13, v4, Landroid/renderscript/Mesh$Builder$Entry;->t:Landroid/renderscript/Type;

    #@4b
    if-eqz v13, :cond_6a

    #@4d
    .line 364
    move-object/from16 v0, p0

    #@4f
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@51
    iget-object v14, v4, Landroid/renderscript/Mesh$Builder$Entry;->t:Landroid/renderscript/Type;

    #@53
    move-object/from16 v0, p0

    #@55
    iget v15, v0, Landroid/renderscript/Mesh$Builder;->mUsage:I

    #@57
    invoke-static {v13, v14, v15}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;I)Landroid/renderscript/Allocation;

    #@5a
    move-result-object v1

    #@5b
    .line 368
    :cond_5b
    :goto_5b
    aput-object v1, v11, v3

    #@5d
    .line 369
    move-object/from16 v0, p0

    #@5f
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@61
    invoke-virtual {v1, v13}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@64
    move-result v13

    #@65
    aput v13, v12, v3

    #@67
    .line 360
    add-int/lit8 v3, v3, 0x1

    #@69
    goto :goto_3c

    #@6a
    .line 365
    :cond_6a
    iget-object v13, v4, Landroid/renderscript/Mesh$Builder$Entry;->e:Landroid/renderscript/Element;

    #@6c
    if-eqz v13, :cond_5b

    #@6e
    .line 366
    move-object/from16 v0, p0

    #@70
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@72
    iget-object v14, v4, Landroid/renderscript/Mesh$Builder$Entry;->e:Landroid/renderscript/Element;

    #@74
    iget v15, v4, Landroid/renderscript/Mesh$Builder$Entry;->size:I

    #@76
    move-object/from16 v0, p0

    #@78
    iget v0, v0, Landroid/renderscript/Mesh$Builder;->mUsage:I

    #@7a
    move/from16 v16, v0

    #@7c
    invoke-static/range {v13 .. v16}, Landroid/renderscript/Allocation;->createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;II)Landroid/renderscript/Allocation;

    #@7f
    move-result-object v1

    #@80
    goto :goto_5b

    #@81
    .line 372
    .end local v1           #alloc:Landroid/renderscript/Allocation;
    .end local v4           #entry:Landroid/renderscript/Mesh$Builder$Entry;
    :cond_81
    const/4 v3, 0x0

    #@82
    :goto_82
    move-object/from16 v0, p0

    #@84
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@86
    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    #@89
    move-result v13

    #@8a
    if-ge v3, v13, :cond_dd

    #@8c
    .line 373
    const/4 v1, 0x0

    #@8d
    .line 374
    .restart local v1       #alloc:Landroid/renderscript/Allocation;
    move-object/from16 v0, p0

    #@8f
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@91
    invoke-virtual {v13, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    #@94
    move-result-object v4

    #@95
    check-cast v4, Landroid/renderscript/Mesh$Builder$Entry;

    #@97
    .line 375
    .restart local v4       #entry:Landroid/renderscript/Mesh$Builder$Entry;
    iget-object v13, v4, Landroid/renderscript/Mesh$Builder$Entry;->t:Landroid/renderscript/Type;

    #@99
    if-eqz v13, :cond_bd

    #@9b
    .line 376
    move-object/from16 v0, p0

    #@9d
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@9f
    iget-object v14, v4, Landroid/renderscript/Mesh$Builder$Entry;->t:Landroid/renderscript/Type;

    #@a1
    move-object/from16 v0, p0

    #@a3
    iget v15, v0, Landroid/renderscript/Mesh$Builder;->mUsage:I

    #@a5
    invoke-static {v13, v14, v15}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;I)Landroid/renderscript/Allocation;

    #@a8
    move-result-object v1

    #@a9
    .line 380
    :cond_a9
    :goto_a9
    if-nez v1, :cond_d4

    #@ab
    const/4 v2, 0x0

    #@ac
    .line 381
    .local v2, allocID:I
    :goto_ac
    aput-object v1, v7, v3

    #@ae
    .line 382
    iget-object v13, v4, Landroid/renderscript/Mesh$Builder$Entry;->prim:Landroid/renderscript/Mesh$Primitive;

    #@b0
    aput-object v13, v10, v3

    #@b2
    .line 384
    aput v2, v6, v3

    #@b4
    .line 385
    iget-object v13, v4, Landroid/renderscript/Mesh$Builder$Entry;->prim:Landroid/renderscript/Mesh$Primitive;

    #@b6
    iget v13, v13, Landroid/renderscript/Mesh$Primitive;->mID:I

    #@b8
    aput v13, v9, v3

    #@ba
    .line 372
    add-int/lit8 v3, v3, 0x1

    #@bc
    goto :goto_82

    #@bd
    .line 377
    .end local v2           #allocID:I
    :cond_bd
    iget-object v13, v4, Landroid/renderscript/Mesh$Builder$Entry;->e:Landroid/renderscript/Element;

    #@bf
    if-eqz v13, :cond_a9

    #@c1
    .line 378
    move-object/from16 v0, p0

    #@c3
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@c5
    iget-object v14, v4, Landroid/renderscript/Mesh$Builder$Entry;->e:Landroid/renderscript/Element;

    #@c7
    iget v15, v4, Landroid/renderscript/Mesh$Builder$Entry;->size:I

    #@c9
    move-object/from16 v0, p0

    #@cb
    iget v0, v0, Landroid/renderscript/Mesh$Builder;->mUsage:I

    #@cd
    move/from16 v16, v0

    #@cf
    invoke-static/range {v13 .. v16}, Landroid/renderscript/Allocation;->createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;II)Landroid/renderscript/Allocation;

    #@d2
    move-result-object v1

    #@d3
    goto :goto_a9

    #@d4
    .line 380
    :cond_d4
    move-object/from16 v0, p0

    #@d6
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@d8
    invoke-virtual {v1, v13}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@db
    move-result v2

    #@dc
    goto :goto_ac

    #@dd
    .line 388
    .end local v1           #alloc:Landroid/renderscript/Allocation;
    .end local v4           #entry:Landroid/renderscript/Mesh$Builder$Entry;
    :cond_dd
    move-object/from16 v0, p0

    #@df
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@e1
    invoke-virtual {v13, v12, v6, v9}, Landroid/renderscript/RenderScript;->nMeshCreate([I[I[I)I

    #@e4
    move-result v5

    #@e5
    .line 389
    .local v5, id:I
    new-instance v8, Landroid/renderscript/Mesh;

    #@e7
    move-object/from16 v0, p0

    #@e9
    iget-object v13, v0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@eb
    invoke-direct {v8, v5, v13}, Landroid/renderscript/Mesh;-><init>(ILandroid/renderscript/RenderScript;)V

    #@ee
    .line 390
    .local v8, newMesh:Landroid/renderscript/Mesh;
    iput-object v11, v8, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    #@f0
    .line 391
    iput-object v7, v8, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    #@f2
    .line 392
    iput-object v10, v8, Landroid/renderscript/Mesh;->mPrimitives:[Landroid/renderscript/Mesh$Primitive;

    #@f4
    .line 394
    return-object v8
.end method

.method public getCurrentIndexSetIndex()I
    .registers 2

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Landroid/renderscript/Mesh$Builder;->mIndexTypes:Ljava/util/Vector;

    #@2
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    #@5
    move-result v0

    #@6
    add-int/lit8 v0, v0, -0x1

    #@8
    return v0
.end method

.method public getCurrentVertexTypeIndex()I
    .registers 2

    #@0
    .prologue
    .line 228
    iget v0, p0, Landroid/renderscript/Mesh$Builder;->mVertexTypeCount:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    return v0
.end method

.method newType(Landroid/renderscript/Element;I)Landroid/renderscript/Type;
    .registers 5
    .parameter "e"
    .parameter "size"

    #@0
    .prologue
    .line 340
    new-instance v0, Landroid/renderscript/Type$Builder;

    #@2
    iget-object v1, p0, Landroid/renderscript/Mesh$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-direct {v0, v1, p1}, Landroid/renderscript/Type$Builder;-><init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V

    #@7
    .line 341
    .local v0, tb:Landroid/renderscript/Type$Builder;
    invoke-virtual {v0, p2}, Landroid/renderscript/Type$Builder;->setX(I)Landroid/renderscript/Type$Builder;

    #@a
    .line 342
    invoke-virtual {v0}, Landroid/renderscript/Type$Builder;->create()Landroid/renderscript/Type;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method
