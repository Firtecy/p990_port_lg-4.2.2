.class public final enum Landroid/renderscript/Type$CubemapFace;
.super Ljava/lang/Enum;
.source "Type.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Type;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CubemapFace"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/renderscript/Type$CubemapFace;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/renderscript/Type$CubemapFace;

.field public static final enum NEGATIVE_X:Landroid/renderscript/Type$CubemapFace;

.field public static final enum NEGATIVE_Y:Landroid/renderscript/Type$CubemapFace;

.field public static final enum NEGATIVE_Z:Landroid/renderscript/Type$CubemapFace;

.field public static final enum POSITIVE_X:Landroid/renderscript/Type$CubemapFace;

.field public static final enum POSITIVE_Y:Landroid/renderscript/Type$CubemapFace;

.field public static final enum POSITIVE_Z:Landroid/renderscript/Type$CubemapFace;

.field public static final enum POSITVE_X:Landroid/renderscript/Type$CubemapFace;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum POSITVE_Y:Landroid/renderscript/Type$CubemapFace;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum POSITVE_Z:Landroid/renderscript/Type$CubemapFace;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field mID:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v6, 0x4

    #@3
    const/4 v5, 0x2

    #@4
    const/4 v4, 0x0

    #@5
    .line 54
    new-instance v0, Landroid/renderscript/Type$CubemapFace;

    #@7
    const-string v1, "POSITIVE_X"

    #@9
    invoke-direct {v0, v1, v4, v4}, Landroid/renderscript/Type$CubemapFace;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Landroid/renderscript/Type$CubemapFace;->POSITIVE_X:Landroid/renderscript/Type$CubemapFace;

    #@e
    .line 55
    new-instance v0, Landroid/renderscript/Type$CubemapFace;

    #@10
    const-string v1, "NEGATIVE_X"

    #@12
    invoke-direct {v0, v1, v7, v7}, Landroid/renderscript/Type$CubemapFace;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Landroid/renderscript/Type$CubemapFace;->NEGATIVE_X:Landroid/renderscript/Type$CubemapFace;

    #@17
    .line 56
    new-instance v0, Landroid/renderscript/Type$CubemapFace;

    #@19
    const-string v1, "POSITIVE_Y"

    #@1b
    invoke-direct {v0, v1, v5, v5}, Landroid/renderscript/Type$CubemapFace;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Landroid/renderscript/Type$CubemapFace;->POSITIVE_Y:Landroid/renderscript/Type$CubemapFace;

    #@20
    .line 57
    new-instance v0, Landroid/renderscript/Type$CubemapFace;

    #@22
    const-string v1, "NEGATIVE_Y"

    #@24
    invoke-direct {v0, v1, v8, v8}, Landroid/renderscript/Type$CubemapFace;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v0, Landroid/renderscript/Type$CubemapFace;->NEGATIVE_Y:Landroid/renderscript/Type$CubemapFace;

    #@29
    .line 58
    new-instance v0, Landroid/renderscript/Type$CubemapFace;

    #@2b
    const-string v1, "POSITIVE_Z"

    #@2d
    invoke-direct {v0, v1, v6, v6}, Landroid/renderscript/Type$CubemapFace;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v0, Landroid/renderscript/Type$CubemapFace;->POSITIVE_Z:Landroid/renderscript/Type$CubemapFace;

    #@32
    .line 59
    new-instance v0, Landroid/renderscript/Type$CubemapFace;

    #@34
    const-string v1, "NEGATIVE_Z"

    #@36
    const/4 v2, 0x5

    #@37
    const/4 v3, 0x5

    #@38
    invoke-direct {v0, v1, v2, v3}, Landroid/renderscript/Type$CubemapFace;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Landroid/renderscript/Type$CubemapFace;->NEGATIVE_Z:Landroid/renderscript/Type$CubemapFace;

    #@3d
    .line 60
    new-instance v0, Landroid/renderscript/Type$CubemapFace;

    #@3f
    const-string v1, "POSITVE_X"

    #@41
    const/4 v2, 0x6

    #@42
    invoke-direct {v0, v1, v2, v4}, Landroid/renderscript/Type$CubemapFace;-><init>(Ljava/lang/String;II)V

    #@45
    sput-object v0, Landroid/renderscript/Type$CubemapFace;->POSITVE_X:Landroid/renderscript/Type$CubemapFace;

    #@47
    .line 62
    new-instance v0, Landroid/renderscript/Type$CubemapFace;

    #@49
    const-string v1, "POSITVE_Y"

    #@4b
    const/4 v2, 0x7

    #@4c
    invoke-direct {v0, v1, v2, v5}, Landroid/renderscript/Type$CubemapFace;-><init>(Ljava/lang/String;II)V

    #@4f
    sput-object v0, Landroid/renderscript/Type$CubemapFace;->POSITVE_Y:Landroid/renderscript/Type$CubemapFace;

    #@51
    .line 64
    new-instance v0, Landroid/renderscript/Type$CubemapFace;

    #@53
    const-string v1, "POSITVE_Z"

    #@55
    const/16 v2, 0x8

    #@57
    invoke-direct {v0, v1, v2, v6}, Landroid/renderscript/Type$CubemapFace;-><init>(Ljava/lang/String;II)V

    #@5a
    sput-object v0, Landroid/renderscript/Type$CubemapFace;->POSITVE_Z:Landroid/renderscript/Type$CubemapFace;

    #@5c
    .line 53
    const/16 v0, 0x9

    #@5e
    new-array v0, v0, [Landroid/renderscript/Type$CubemapFace;

    #@60
    sget-object v1, Landroid/renderscript/Type$CubemapFace;->POSITIVE_X:Landroid/renderscript/Type$CubemapFace;

    #@62
    aput-object v1, v0, v4

    #@64
    sget-object v1, Landroid/renderscript/Type$CubemapFace;->NEGATIVE_X:Landroid/renderscript/Type$CubemapFace;

    #@66
    aput-object v1, v0, v7

    #@68
    sget-object v1, Landroid/renderscript/Type$CubemapFace;->POSITIVE_Y:Landroid/renderscript/Type$CubemapFace;

    #@6a
    aput-object v1, v0, v5

    #@6c
    sget-object v1, Landroid/renderscript/Type$CubemapFace;->NEGATIVE_Y:Landroid/renderscript/Type$CubemapFace;

    #@6e
    aput-object v1, v0, v8

    #@70
    sget-object v1, Landroid/renderscript/Type$CubemapFace;->POSITIVE_Z:Landroid/renderscript/Type$CubemapFace;

    #@72
    aput-object v1, v0, v6

    #@74
    const/4 v1, 0x5

    #@75
    sget-object v2, Landroid/renderscript/Type$CubemapFace;->NEGATIVE_Z:Landroid/renderscript/Type$CubemapFace;

    #@77
    aput-object v2, v0, v1

    #@79
    const/4 v1, 0x6

    #@7a
    sget-object v2, Landroid/renderscript/Type$CubemapFace;->POSITVE_X:Landroid/renderscript/Type$CubemapFace;

    #@7c
    aput-object v2, v0, v1

    #@7e
    const/4 v1, 0x7

    #@7f
    sget-object v2, Landroid/renderscript/Type$CubemapFace;->POSITVE_Y:Landroid/renderscript/Type$CubemapFace;

    #@81
    aput-object v2, v0, v1

    #@83
    const/16 v1, 0x8

    #@85
    sget-object v2, Landroid/renderscript/Type$CubemapFace;->POSITVE_Z:Landroid/renderscript/Type$CubemapFace;

    #@87
    aput-object v2, v0, v1

    #@89
    sput-object v0, Landroid/renderscript/Type$CubemapFace;->$VALUES:[Landroid/renderscript/Type$CubemapFace;

    #@8b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 69
    iput p3, p0, Landroid/renderscript/Type$CubemapFace;->mID:I

    #@5
    .line 70
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/renderscript/Type$CubemapFace;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 53
    const-class v0, Landroid/renderscript/Type$CubemapFace;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/Type$CubemapFace;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/renderscript/Type$CubemapFace;
    .registers 1

    #@0
    .prologue
    .line 53
    sget-object v0, Landroid/renderscript/Type$CubemapFace;->$VALUES:[Landroid/renderscript/Type$CubemapFace;

    #@2
    invoke-virtual {v0}, [Landroid/renderscript/Type$CubemapFace;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/renderscript/Type$CubemapFace;

    #@8
    return-object v0
.end method
