.class public final enum Landroid/renderscript/Element$DataType;
.super Ljava/lang/Enum;
.source "Element.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Element;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/renderscript/Element$DataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/renderscript/Element$DataType;

.field public static final enum BOOLEAN:Landroid/renderscript/Element$DataType;

.field public static final enum FLOAT_32:Landroid/renderscript/Element$DataType;

.field public static final enum FLOAT_64:Landroid/renderscript/Element$DataType;

.field public static final enum MATRIX_2X2:Landroid/renderscript/Element$DataType;

.field public static final enum MATRIX_3X3:Landroid/renderscript/Element$DataType;

.field public static final enum MATRIX_4X4:Landroid/renderscript/Element$DataType;

.field public static final enum NONE:Landroid/renderscript/Element$DataType;

.field public static final enum RS_ALLOCATION:Landroid/renderscript/Element$DataType;

.field public static final enum RS_ELEMENT:Landroid/renderscript/Element$DataType;

.field public static final enum RS_FONT:Landroid/renderscript/Element$DataType;

.field public static final enum RS_MESH:Landroid/renderscript/Element$DataType;

.field public static final enum RS_PROGRAM_FRAGMENT:Landroid/renderscript/Element$DataType;

.field public static final enum RS_PROGRAM_RASTER:Landroid/renderscript/Element$DataType;

.field public static final enum RS_PROGRAM_STORE:Landroid/renderscript/Element$DataType;

.field public static final enum RS_PROGRAM_VERTEX:Landroid/renderscript/Element$DataType;

.field public static final enum RS_SAMPLER:Landroid/renderscript/Element$DataType;

.field public static final enum RS_SCRIPT:Landroid/renderscript/Element$DataType;

.field public static final enum RS_TYPE:Landroid/renderscript/Element$DataType;

.field public static final enum SIGNED_16:Landroid/renderscript/Element$DataType;

.field public static final enum SIGNED_32:Landroid/renderscript/Element$DataType;

.field public static final enum SIGNED_64:Landroid/renderscript/Element$DataType;

.field public static final enum SIGNED_8:Landroid/renderscript/Element$DataType;

.field public static final enum UNSIGNED_16:Landroid/renderscript/Element$DataType;

.field public static final enum UNSIGNED_32:Landroid/renderscript/Element$DataType;

.field public static final enum UNSIGNED_4_4_4_4:Landroid/renderscript/Element$DataType;

.field public static final enum UNSIGNED_5_5_5_1:Landroid/renderscript/Element$DataType;

.field public static final enum UNSIGNED_5_6_5:Landroid/renderscript/Element$DataType;

.field public static final enum UNSIGNED_64:Landroid/renderscript/Element$DataType;

.field public static final enum UNSIGNED_8:Landroid/renderscript/Element$DataType;


# instance fields
.field mID:I

.field mSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 10

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    const/16 v7, 0x8

    #@4
    const/4 v6, 0x2

    #@5
    const/4 v5, 0x4

    #@6
    .line 117
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@8
    const-string v1, "NONE"

    #@a
    invoke-direct {v0, v1, v9, v9, v9}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@d
    sput-object v0, Landroid/renderscript/Element$DataType;->NONE:Landroid/renderscript/Element$DataType;

    #@f
    .line 119
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@11
    const-string v1, "FLOAT_32"

    #@13
    invoke-direct {v0, v1, v8, v6, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@16
    sput-object v0, Landroid/renderscript/Element$DataType;->FLOAT_32:Landroid/renderscript/Element$DataType;

    #@18
    .line 120
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@1a
    const-string v1, "FLOAT_64"

    #@1c
    const/4 v2, 0x3

    #@1d
    invoke-direct {v0, v1, v6, v2, v7}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@20
    sput-object v0, Landroid/renderscript/Element$DataType;->FLOAT_64:Landroid/renderscript/Element$DataType;

    #@22
    .line 121
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@24
    const-string v1, "SIGNED_8"

    #@26
    const/4 v2, 0x3

    #@27
    invoke-direct {v0, v1, v2, v5, v8}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@2a
    sput-object v0, Landroid/renderscript/Element$DataType;->SIGNED_8:Landroid/renderscript/Element$DataType;

    #@2c
    .line 122
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@2e
    const-string v1, "SIGNED_16"

    #@30
    const/4 v2, 0x5

    #@31
    invoke-direct {v0, v1, v5, v2, v6}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@34
    sput-object v0, Landroid/renderscript/Element$DataType;->SIGNED_16:Landroid/renderscript/Element$DataType;

    #@36
    .line 123
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@38
    const-string v1, "SIGNED_32"

    #@3a
    const/4 v2, 0x5

    #@3b
    const/4 v3, 0x6

    #@3c
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@3f
    sput-object v0, Landroid/renderscript/Element$DataType;->SIGNED_32:Landroid/renderscript/Element$DataType;

    #@41
    .line 124
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@43
    const-string v1, "SIGNED_64"

    #@45
    const/4 v2, 0x6

    #@46
    const/4 v3, 0x7

    #@47
    invoke-direct {v0, v1, v2, v3, v7}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@4a
    sput-object v0, Landroid/renderscript/Element$DataType;->SIGNED_64:Landroid/renderscript/Element$DataType;

    #@4c
    .line 125
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@4e
    const-string v1, "UNSIGNED_8"

    #@50
    const/4 v2, 0x7

    #@51
    invoke-direct {v0, v1, v2, v7, v8}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@54
    sput-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@56
    .line 126
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@58
    const-string v1, "UNSIGNED_16"

    #@5a
    const/16 v2, 0x9

    #@5c
    invoke-direct {v0, v1, v7, v2, v6}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@5f
    sput-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_16:Landroid/renderscript/Element$DataType;

    #@61
    .line 127
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@63
    const-string v1, "UNSIGNED_32"

    #@65
    const/16 v2, 0x9

    #@67
    const/16 v3, 0xa

    #@69
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@6c
    sput-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_32:Landroid/renderscript/Element$DataType;

    #@6e
    .line 128
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@70
    const-string v1, "UNSIGNED_64"

    #@72
    const/16 v2, 0xa

    #@74
    const/16 v3, 0xb

    #@76
    invoke-direct {v0, v1, v2, v3, v7}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@79
    sput-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_64:Landroid/renderscript/Element$DataType;

    #@7b
    .line 130
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@7d
    const-string v1, "BOOLEAN"

    #@7f
    const/16 v2, 0xb

    #@81
    const/16 v3, 0xc

    #@83
    invoke-direct {v0, v1, v2, v3, v8}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@86
    sput-object v0, Landroid/renderscript/Element$DataType;->BOOLEAN:Landroid/renderscript/Element$DataType;

    #@88
    .line 132
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@8a
    const-string v1, "UNSIGNED_5_6_5"

    #@8c
    const/16 v2, 0xc

    #@8e
    const/16 v3, 0xd

    #@90
    invoke-direct {v0, v1, v2, v3, v6}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@93
    sput-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_5_6_5:Landroid/renderscript/Element$DataType;

    #@95
    .line 133
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@97
    const-string v1, "UNSIGNED_5_5_5_1"

    #@99
    const/16 v2, 0xd

    #@9b
    const/16 v3, 0xe

    #@9d
    invoke-direct {v0, v1, v2, v3, v6}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@a0
    sput-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_5_5_5_1:Landroid/renderscript/Element$DataType;

    #@a2
    .line 134
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@a4
    const-string v1, "UNSIGNED_4_4_4_4"

    #@a6
    const/16 v2, 0xe

    #@a8
    const/16 v3, 0xf

    #@aa
    invoke-direct {v0, v1, v2, v3, v6}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@ad
    sput-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_4_4_4_4:Landroid/renderscript/Element$DataType;

    #@af
    .line 136
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@b1
    const-string v1, "MATRIX_4X4"

    #@b3
    const/16 v2, 0xf

    #@b5
    const/16 v3, 0x10

    #@b7
    const/16 v4, 0x40

    #@b9
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@bc
    sput-object v0, Landroid/renderscript/Element$DataType;->MATRIX_4X4:Landroid/renderscript/Element$DataType;

    #@be
    .line 137
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@c0
    const-string v1, "MATRIX_3X3"

    #@c2
    const/16 v2, 0x10

    #@c4
    const/16 v3, 0x11

    #@c6
    const/16 v4, 0x24

    #@c8
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@cb
    sput-object v0, Landroid/renderscript/Element$DataType;->MATRIX_3X3:Landroid/renderscript/Element$DataType;

    #@cd
    .line 138
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@cf
    const-string v1, "MATRIX_2X2"

    #@d1
    const/16 v2, 0x11

    #@d3
    const/16 v3, 0x12

    #@d5
    const/16 v4, 0x10

    #@d7
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@da
    sput-object v0, Landroid/renderscript/Element$DataType;->MATRIX_2X2:Landroid/renderscript/Element$DataType;

    #@dc
    .line 140
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@de
    const-string v1, "RS_ELEMENT"

    #@e0
    const/16 v2, 0x12

    #@e2
    const/16 v3, 0x3e8

    #@e4
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@e7
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_ELEMENT:Landroid/renderscript/Element$DataType;

    #@e9
    .line 141
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@eb
    const-string v1, "RS_TYPE"

    #@ed
    const/16 v2, 0x13

    #@ef
    const/16 v3, 0x3e9

    #@f1
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@f4
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_TYPE:Landroid/renderscript/Element$DataType;

    #@f6
    .line 142
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@f8
    const-string v1, "RS_ALLOCATION"

    #@fa
    const/16 v2, 0x14

    #@fc
    const/16 v3, 0x3ea

    #@fe
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@101
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_ALLOCATION:Landroid/renderscript/Element$DataType;

    #@103
    .line 143
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@105
    const-string v1, "RS_SAMPLER"

    #@107
    const/16 v2, 0x15

    #@109
    const/16 v3, 0x3eb

    #@10b
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@10e
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_SAMPLER:Landroid/renderscript/Element$DataType;

    #@110
    .line 144
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@112
    const-string v1, "RS_SCRIPT"

    #@114
    const/16 v2, 0x16

    #@116
    const/16 v3, 0x3ec

    #@118
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@11b
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_SCRIPT:Landroid/renderscript/Element$DataType;

    #@11d
    .line 145
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@11f
    const-string v1, "RS_MESH"

    #@121
    const/16 v2, 0x17

    #@123
    const/16 v3, 0x3ed

    #@125
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@128
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_MESH:Landroid/renderscript/Element$DataType;

    #@12a
    .line 146
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@12c
    const-string v1, "RS_PROGRAM_FRAGMENT"

    #@12e
    const/16 v2, 0x18

    #@130
    const/16 v3, 0x3ee

    #@132
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@135
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_PROGRAM_FRAGMENT:Landroid/renderscript/Element$DataType;

    #@137
    .line 147
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@139
    const-string v1, "RS_PROGRAM_VERTEX"

    #@13b
    const/16 v2, 0x19

    #@13d
    const/16 v3, 0x3ef

    #@13f
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@142
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_PROGRAM_VERTEX:Landroid/renderscript/Element$DataType;

    #@144
    .line 148
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@146
    const-string v1, "RS_PROGRAM_RASTER"

    #@148
    const/16 v2, 0x1a

    #@14a
    const/16 v3, 0x3f0

    #@14c
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@14f
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_PROGRAM_RASTER:Landroid/renderscript/Element$DataType;

    #@151
    .line 149
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@153
    const-string v1, "RS_PROGRAM_STORE"

    #@155
    const/16 v2, 0x1b

    #@157
    const/16 v3, 0x3f1

    #@159
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@15c
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_PROGRAM_STORE:Landroid/renderscript/Element$DataType;

    #@15e
    .line 150
    new-instance v0, Landroid/renderscript/Element$DataType;

    #@160
    const-string v1, "RS_FONT"

    #@162
    const/16 v2, 0x1c

    #@164
    const/16 v3, 0x3f2

    #@166
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/renderscript/Element$DataType;-><init>(Ljava/lang/String;III)V

    #@169
    sput-object v0, Landroid/renderscript/Element$DataType;->RS_FONT:Landroid/renderscript/Element$DataType;

    #@16b
    .line 116
    const/16 v0, 0x1d

    #@16d
    new-array v0, v0, [Landroid/renderscript/Element$DataType;

    #@16f
    sget-object v1, Landroid/renderscript/Element$DataType;->NONE:Landroid/renderscript/Element$DataType;

    #@171
    aput-object v1, v0, v9

    #@173
    sget-object v1, Landroid/renderscript/Element$DataType;->FLOAT_32:Landroid/renderscript/Element$DataType;

    #@175
    aput-object v1, v0, v8

    #@177
    sget-object v1, Landroid/renderscript/Element$DataType;->FLOAT_64:Landroid/renderscript/Element$DataType;

    #@179
    aput-object v1, v0, v6

    #@17b
    const/4 v1, 0x3

    #@17c
    sget-object v2, Landroid/renderscript/Element$DataType;->SIGNED_8:Landroid/renderscript/Element$DataType;

    #@17e
    aput-object v2, v0, v1

    #@180
    sget-object v1, Landroid/renderscript/Element$DataType;->SIGNED_16:Landroid/renderscript/Element$DataType;

    #@182
    aput-object v1, v0, v5

    #@184
    const/4 v1, 0x5

    #@185
    sget-object v2, Landroid/renderscript/Element$DataType;->SIGNED_32:Landroid/renderscript/Element$DataType;

    #@187
    aput-object v2, v0, v1

    #@189
    const/4 v1, 0x6

    #@18a
    sget-object v2, Landroid/renderscript/Element$DataType;->SIGNED_64:Landroid/renderscript/Element$DataType;

    #@18c
    aput-object v2, v0, v1

    #@18e
    const/4 v1, 0x7

    #@18f
    sget-object v2, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@191
    aput-object v2, v0, v1

    #@193
    sget-object v1, Landroid/renderscript/Element$DataType;->UNSIGNED_16:Landroid/renderscript/Element$DataType;

    #@195
    aput-object v1, v0, v7

    #@197
    const/16 v1, 0x9

    #@199
    sget-object v2, Landroid/renderscript/Element$DataType;->UNSIGNED_32:Landroid/renderscript/Element$DataType;

    #@19b
    aput-object v2, v0, v1

    #@19d
    const/16 v1, 0xa

    #@19f
    sget-object v2, Landroid/renderscript/Element$DataType;->UNSIGNED_64:Landroid/renderscript/Element$DataType;

    #@1a1
    aput-object v2, v0, v1

    #@1a3
    const/16 v1, 0xb

    #@1a5
    sget-object v2, Landroid/renderscript/Element$DataType;->BOOLEAN:Landroid/renderscript/Element$DataType;

    #@1a7
    aput-object v2, v0, v1

    #@1a9
    const/16 v1, 0xc

    #@1ab
    sget-object v2, Landroid/renderscript/Element$DataType;->UNSIGNED_5_6_5:Landroid/renderscript/Element$DataType;

    #@1ad
    aput-object v2, v0, v1

    #@1af
    const/16 v1, 0xd

    #@1b1
    sget-object v2, Landroid/renderscript/Element$DataType;->UNSIGNED_5_5_5_1:Landroid/renderscript/Element$DataType;

    #@1b3
    aput-object v2, v0, v1

    #@1b5
    const/16 v1, 0xe

    #@1b7
    sget-object v2, Landroid/renderscript/Element$DataType;->UNSIGNED_4_4_4_4:Landroid/renderscript/Element$DataType;

    #@1b9
    aput-object v2, v0, v1

    #@1bb
    const/16 v1, 0xf

    #@1bd
    sget-object v2, Landroid/renderscript/Element$DataType;->MATRIX_4X4:Landroid/renderscript/Element$DataType;

    #@1bf
    aput-object v2, v0, v1

    #@1c1
    const/16 v1, 0x10

    #@1c3
    sget-object v2, Landroid/renderscript/Element$DataType;->MATRIX_3X3:Landroid/renderscript/Element$DataType;

    #@1c5
    aput-object v2, v0, v1

    #@1c7
    const/16 v1, 0x11

    #@1c9
    sget-object v2, Landroid/renderscript/Element$DataType;->MATRIX_2X2:Landroid/renderscript/Element$DataType;

    #@1cb
    aput-object v2, v0, v1

    #@1cd
    const/16 v1, 0x12

    #@1cf
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_ELEMENT:Landroid/renderscript/Element$DataType;

    #@1d1
    aput-object v2, v0, v1

    #@1d3
    const/16 v1, 0x13

    #@1d5
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_TYPE:Landroid/renderscript/Element$DataType;

    #@1d7
    aput-object v2, v0, v1

    #@1d9
    const/16 v1, 0x14

    #@1db
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_ALLOCATION:Landroid/renderscript/Element$DataType;

    #@1dd
    aput-object v2, v0, v1

    #@1df
    const/16 v1, 0x15

    #@1e1
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_SAMPLER:Landroid/renderscript/Element$DataType;

    #@1e3
    aput-object v2, v0, v1

    #@1e5
    const/16 v1, 0x16

    #@1e7
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_SCRIPT:Landroid/renderscript/Element$DataType;

    #@1e9
    aput-object v2, v0, v1

    #@1eb
    const/16 v1, 0x17

    #@1ed
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_MESH:Landroid/renderscript/Element$DataType;

    #@1ef
    aput-object v2, v0, v1

    #@1f1
    const/16 v1, 0x18

    #@1f3
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_PROGRAM_FRAGMENT:Landroid/renderscript/Element$DataType;

    #@1f5
    aput-object v2, v0, v1

    #@1f7
    const/16 v1, 0x19

    #@1f9
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_PROGRAM_VERTEX:Landroid/renderscript/Element$DataType;

    #@1fb
    aput-object v2, v0, v1

    #@1fd
    const/16 v1, 0x1a

    #@1ff
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_PROGRAM_RASTER:Landroid/renderscript/Element$DataType;

    #@201
    aput-object v2, v0, v1

    #@203
    const/16 v1, 0x1b

    #@205
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_PROGRAM_STORE:Landroid/renderscript/Element$DataType;

    #@207
    aput-object v2, v0, v1

    #@209
    const/16 v1, 0x1c

    #@20b
    sget-object v2, Landroid/renderscript/Element$DataType;->RS_FONT:Landroid/renderscript/Element$DataType;

    #@20d
    aput-object v2, v0, v1

    #@20f
    sput-object v0, Landroid/renderscript/Element$DataType;->$VALUES:[Landroid/renderscript/Element$DataType;

    #@211
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .registers 5
    .parameter
    .parameter
    .parameter "id"
    .parameter "size"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    #@0
    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 155
    iput p3, p0, Landroid/renderscript/Element$DataType;->mID:I

    #@5
    .line 156
    iput p4, p0, Landroid/renderscript/Element$DataType;->mSize:I

    #@7
    .line 157
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/renderscript/Element$DataType;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 116
    const-class v0, Landroid/renderscript/Element$DataType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/Element$DataType;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/renderscript/Element$DataType;
    .registers 1

    #@0
    .prologue
    .line 116
    sget-object v0, Landroid/renderscript/Element$DataType;->$VALUES:[Landroid/renderscript/Element$DataType;

    #@2
    invoke-virtual {v0}, [Landroid/renderscript/Element$DataType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/renderscript/Element$DataType;

    #@8
    return-object v0
.end method
