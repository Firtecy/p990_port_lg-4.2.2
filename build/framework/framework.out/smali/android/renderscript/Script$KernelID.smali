.class public final Landroid/renderscript/Script$KernelID;
.super Landroid/renderscript/BaseObj;
.source "Script.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Script;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "KernelID"
.end annotation


# instance fields
.field mScript:Landroid/renderscript/Script;

.field mSig:I

.field mSlot:I


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Script;II)V
    .registers 6
    .parameter "id"
    .parameter "rs"
    .parameter "s"
    .parameter "slot"
    .parameter "sig"

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 40
    iput-object p3, p0, Landroid/renderscript/Script$KernelID;->mScript:Landroid/renderscript/Script;

    #@5
    .line 41
    iput p4, p0, Landroid/renderscript/Script$KernelID;->mSlot:I

    #@7
    .line 42
    iput p5, p0, Landroid/renderscript/Script$KernelID;->mSig:I

    #@9
    .line 43
    return-void
.end method
