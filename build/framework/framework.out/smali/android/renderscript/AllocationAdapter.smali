.class public Landroid/renderscript/AllocationAdapter;
.super Landroid/renderscript/Allocation;
.source "AllocationAdapter.java"


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Allocation;)V
    .registers 6
    .parameter "id"
    .parameter "rs"
    .parameter "alloc"

    #@0
    .prologue
    .line 29
    iget-object v0, p3, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    iget v1, p3, Landroid/renderscript/Allocation;->mUsage:I

    #@4
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/renderscript/Allocation;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Type;I)V

    #@7
    .line 30
    iput-object p3, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@9
    .line 31
    return-void
.end method

.method public static create1D(Landroid/renderscript/RenderScript;Landroid/renderscript/Allocation;)Landroid/renderscript/AllocationAdapter;
    .registers 5
    .parameter "rs"
    .parameter "a"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 215
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 216
    new-instance v0, Landroid/renderscript/AllocationAdapter;

    #@7
    invoke-direct {v0, v2, p0, p1}, Landroid/renderscript/AllocationAdapter;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Allocation;)V

    #@a
    .line 217
    .local v0, aa:Landroid/renderscript/AllocationAdapter;
    iput-boolean v1, v0, Landroid/renderscript/Allocation;->mConstrainedLOD:Z

    #@c
    .line 218
    iput-boolean v1, v0, Landroid/renderscript/Allocation;->mConstrainedFace:Z

    #@e
    .line 219
    iput-boolean v1, v0, Landroid/renderscript/Allocation;->mConstrainedY:Z

    #@10
    .line 220
    iput-boolean v1, v0, Landroid/renderscript/Allocation;->mConstrainedZ:Z

    #@12
    .line 221
    invoke-virtual {v0, v2}, Landroid/renderscript/AllocationAdapter;->initLOD(I)V

    #@15
    .line 222
    return-object v0
.end method

.method public static create2D(Landroid/renderscript/RenderScript;Landroid/renderscript/Allocation;)Landroid/renderscript/AllocationAdapter;
    .registers 8
    .parameter "rs"
    .parameter "a"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 226
    const-string/jumbo v1, "rs"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "create2d "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 227
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@1e
    .line 228
    new-instance v0, Landroid/renderscript/AllocationAdapter;

    #@20
    invoke-direct {v0, v4, p0, p1}, Landroid/renderscript/AllocationAdapter;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Allocation;)V

    #@23
    .line 229
    .local v0, aa:Landroid/renderscript/AllocationAdapter;
    iput-boolean v5, v0, Landroid/renderscript/Allocation;->mConstrainedLOD:Z

    #@25
    .line 230
    iput-boolean v5, v0, Landroid/renderscript/Allocation;->mConstrainedFace:Z

    #@27
    .line 231
    iput-boolean v4, v0, Landroid/renderscript/Allocation;->mConstrainedY:Z

    #@29
    .line 232
    iput-boolean v5, v0, Landroid/renderscript/Allocation;->mConstrainedZ:Z

    #@2b
    .line 233
    invoke-virtual {v0, v4}, Landroid/renderscript/AllocationAdapter;->initLOD(I)V

    #@2e
    .line 234
    return-object v0
.end method


# virtual methods
.method getID(Landroid/renderscript/RenderScript;)I
    .registers 4
    .parameter "rs"

    #@0
    .prologue
    .line 34
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@2
    const-string v1, "This operation is not supported with adapters at this time."

    #@4
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method initLOD(I)V
    .registers 10
    .parameter "lod"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 100
    if-gez p1, :cond_23

    #@4
    .line 101
    new-instance v4, Landroid/renderscript/RSIllegalArgumentException;

    #@6
    new-instance v5, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v6, "Attempting to set negative lod ("

    #@d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    const-string v6, ")."

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    invoke-direct {v4, v5}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v4

    #@23
    .line 104
    :cond_23
    iget-object v4, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@25
    iget-object v4, v4, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@27
    invoke-virtual {v4}, Landroid/renderscript/Type;->getX()I

    #@2a
    move-result v1

    #@2b
    .line 105
    .local v1, tx:I
    iget-object v4, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@2d
    iget-object v4, v4, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2f
    invoke-virtual {v4}, Landroid/renderscript/Type;->getY()I

    #@32
    move-result v2

    #@33
    .line 106
    .local v2, ty:I
    iget-object v4, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@35
    iget-object v4, v4, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@37
    invoke-virtual {v4}, Landroid/renderscript/Type;->getZ()I

    #@3a
    move-result v3

    #@3b
    .line 108
    .local v3, tz:I
    const/4 v0, 0x0

    #@3c
    .local v0, ct:I
    :goto_3c
    if-ge v0, p1, :cond_72

    #@3e
    .line 109
    if-ne v1, v6, :cond_63

    #@40
    if-ne v2, v6, :cond_63

    #@42
    if-ne v3, v6, :cond_63

    #@44
    .line 110
    new-instance v4, Landroid/renderscript/RSIllegalArgumentException;

    #@46
    new-instance v5, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v6, "Attempting to set lod ("

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    const-string v6, ") out of range."

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v5

    #@5f
    invoke-direct {v4, v5}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@62
    throw v4

    #@63
    .line 113
    :cond_63
    if-le v1, v6, :cond_67

    #@65
    shr-int/lit8 v1, v1, 0x1

    #@67
    .line 114
    :cond_67
    if-le v2, v6, :cond_6b

    #@69
    shr-int/lit8 v2, v2, 0x1

    #@6b
    .line 115
    :cond_6b
    if-le v3, v6, :cond_6f

    #@6d
    shr-int/lit8 v3, v3, 0x1

    #@6f
    .line 108
    :cond_6f
    add-int/lit8 v0, v0, 0x1

    #@71
    goto :goto_3c

    #@72
    .line 118
    :cond_72
    iput v1, p0, Landroid/renderscript/Allocation;->mCurrentDimX:I

    #@74
    .line 119
    iput v2, p0, Landroid/renderscript/Allocation;->mCurrentDimY:I

    #@76
    .line 120
    iput v3, p0, Landroid/renderscript/Allocation;->mCurrentDimZ:I

    #@78
    .line 121
    iget v4, p0, Landroid/renderscript/Allocation;->mCurrentDimX:I

    #@7a
    iput v4, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@7c
    .line 122
    iget v4, p0, Landroid/renderscript/Allocation;->mCurrentDimY:I

    #@7e
    if-le v4, v6, :cond_87

    #@80
    .line 123
    iget v4, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@82
    iget v5, p0, Landroid/renderscript/Allocation;->mCurrentDimY:I

    #@84
    mul-int/2addr v4, v5

    #@85
    iput v4, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@87
    .line 125
    :cond_87
    iget v4, p0, Landroid/renderscript/Allocation;->mCurrentDimZ:I

    #@89
    if-le v4, v6, :cond_92

    #@8b
    .line 126
    iget v4, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@8d
    iget v5, p0, Landroid/renderscript/Allocation;->mCurrentDimZ:I

    #@8f
    mul-int/2addr v4, v5

    #@90
    iput v4, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@92
    .line 128
    :cond_92
    iput v7, p0, Landroid/renderscript/Allocation;->mSelectedY:I

    #@94
    .line 129
    iput v7, p0, Landroid/renderscript/Allocation;->mSelectedZ:I

    #@96
    .line 130
    return-void
.end method

.method public readData([F)V
    .registers 2
    .parameter "d"

    #@0
    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/renderscript/Allocation;->copyTo([F)V

    #@3
    .line 97
    return-void
.end method

.method public readData([I)V
    .registers 2
    .parameter "d"

    #@0
    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/renderscript/Allocation;->copyTo([I)V

    #@3
    .line 91
    return-void
.end method

.method public declared-synchronized resize(I)V
    .registers 4
    .parameter "dimX"

    #@0
    .prologue
    .line 245
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@3
    const-string v1, "Resize not allowed for Adapters."

    #@5
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@8
    throw v0
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_9

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public setFace(Landroid/renderscript/Type$CubemapFace;)V
    .registers 4
    .parameter "cf"

    #@0
    .prologue
    .line 159
    iget-object v0, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/renderscript/Type;->hasFaces()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 160
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@e
    const-string v1, "Cannot set Face when the allocation type does not include faces."

    #@10
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 162
    :cond_14
    iget-boolean v0, p0, Landroid/renderscript/Allocation;->mConstrainedFace:Z

    #@16
    if-nez v0, :cond_20

    #@18
    .line 163
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@1a
    const-string v1, "Cannot set LOD when the adapter includes mipmaps."

    #@1c
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 165
    :cond_20
    if-nez p1, :cond_2a

    #@22
    .line 166
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@24
    const-string v1, "Cannot set null face."

    #@26
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0

    #@2a
    .line 169
    :cond_2a
    iput-object p1, p0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@2c
    .line 170
    return-void
.end method

.method public setLOD(I)V
    .registers 4
    .parameter "lod"

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/renderscript/Type;->hasMipmaps()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 143
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@e
    const-string v1, "Cannot set LOD when the allocation type does not include mipmaps."

    #@10
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 145
    :cond_14
    iget-boolean v0, p0, Landroid/renderscript/Allocation;->mConstrainedLOD:Z

    #@16
    if-nez v0, :cond_20

    #@18
    .line 146
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@1a
    const-string v1, "Cannot set LOD when the adapter includes mipmaps."

    #@1c
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 149
    :cond_20
    invoke-virtual {p0, p1}, Landroid/renderscript/AllocationAdapter;->initLOD(I)V

    #@23
    .line 150
    return-void
.end method

.method public setY(I)V
    .registers 4
    .parameter "y"

    #@0
    .prologue
    .line 180
    iget-object v0, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/renderscript/Type;->getY()I

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 181
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@e
    const-string v1, "Cannot set Y when the allocation type does not include Y dim."

    #@10
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 183
    :cond_14
    iget-object v0, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@16
    invoke-virtual {v0}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Landroid/renderscript/Type;->getY()I

    #@1d
    move-result v0

    #@1e
    if-gt v0, p1, :cond_28

    #@20
    .line 184
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@22
    const-string v1, "Cannot set Y greater than dimension of allocation."

    #@24
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@27
    throw v0

    #@28
    .line 186
    :cond_28
    iget-boolean v0, p0, Landroid/renderscript/Allocation;->mConstrainedY:Z

    #@2a
    if-nez v0, :cond_34

    #@2c
    .line 187
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@2e
    const-string v1, "Cannot set Y when the adapter includes Y."

    #@30
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@33
    throw v0

    #@34
    .line 190
    :cond_34
    iput p1, p0, Landroid/renderscript/Allocation;->mSelectedY:I

    #@36
    .line 191
    return-void
.end method

.method public setZ(I)V
    .registers 4
    .parameter "z"

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/renderscript/Type;->getZ()I

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 202
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@e
    const-string v1, "Cannot set Z when the allocation type does not include Z dim."

    #@10
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 204
    :cond_14
    iget-object v0, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@16
    invoke-virtual {v0}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Landroid/renderscript/Type;->getZ()I

    #@1d
    move-result v0

    #@1e
    if-gt v0, p1, :cond_28

    #@20
    .line 205
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@22
    const-string v1, "Cannot set Z greater than dimension of allocation."

    #@24
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@27
    throw v0

    #@28
    .line 207
    :cond_28
    iget-boolean v0, p0, Landroid/renderscript/Allocation;->mConstrainedZ:Z

    #@2a
    if-nez v0, :cond_34

    #@2c
    .line 208
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@2e
    const-string v1, "Cannot set Z when the adapter includes Z."

    #@30
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@33
    throw v0

    #@34
    .line 211
    :cond_34
    iput p1, p0, Landroid/renderscript/Allocation;->mSelectedZ:I

    #@36
    .line 212
    return-void
.end method

.method public subData(ILandroid/renderscript/FieldPacker;)V
    .registers 3
    .parameter "xoff"
    .parameter "fp"

    #@0
    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Landroid/renderscript/Allocation;->setFromFieldPacker(ILandroid/renderscript/FieldPacker;)V

    #@3
    .line 43
    return-void
.end method

.method public subData1D(II[B)V
    .registers 4
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 66
    invoke-super {p0, p1, p2, p3}, Landroid/renderscript/Allocation;->copy1DRangeFrom(II[B)V

    #@3
    .line 67
    return-void
.end method

.method public subData1D(II[F)V
    .registers 4
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 72
    invoke-super {p0, p1, p2, p3}, Landroid/renderscript/Allocation;->copy1DRangeFrom(II[F)V

    #@3
    .line 73
    return-void
.end method

.method public subData1D(II[I)V
    .registers 4
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3}, Landroid/renderscript/Allocation;->copy1DRangeFrom(II[I)V

    #@3
    .line 55
    return-void
.end method

.method public subData1D(II[S)V
    .registers 4
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 60
    invoke-super {p0, p1, p2, p3}, Landroid/renderscript/Allocation;->copy1DRangeFrom(II[S)V

    #@3
    .line 61
    return-void
.end method

.method public subData2D(IIII[F)V
    .registers 6
    .parameter "xoff"
    .parameter "yoff"
    .parameter "w"
    .parameter "h"
    .parameter "d"

    #@0
    .prologue
    .line 84
    invoke-super/range {p0 .. p5}, Landroid/renderscript/Allocation;->copy2DRangeFrom(IIII[F)V

    #@3
    .line 85
    return-void
.end method

.method public subData2D(IIII[I)V
    .registers 6
    .parameter "xoff"
    .parameter "yoff"
    .parameter "w"
    .parameter "h"
    .parameter "d"

    #@0
    .prologue
    .line 78
    invoke-super/range {p0 .. p5}, Landroid/renderscript/Allocation;->copy2DRangeFrom(IIII[I)V

    #@3
    .line 79
    return-void
.end method

.method public subElementData(IILandroid/renderscript/FieldPacker;)V
    .registers 4
    .parameter "xoff"
    .parameter "component_number"
    .parameter "fp"

    #@0
    .prologue
    .line 48
    invoke-super {p0, p1, p2, p3}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V

    #@3
    .line 49
    return-void
.end method
