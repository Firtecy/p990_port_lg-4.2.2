.class public Landroid/renderscript/Program$BaseProgramBuilder;
.super Ljava/lang/Object;
.source "Program.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Program;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BaseProgramBuilder"
.end annotation


# instance fields
.field mConstantCount:I

.field mConstants:[Landroid/renderscript/Type;

.field mInputCount:I

.field mInputs:[Landroid/renderscript/Element;

.field mOutputCount:I

.field mOutputs:[Landroid/renderscript/Element;

.field mRS:Landroid/renderscript/RenderScript;

.field mShader:Ljava/lang/String;

.field mTextureCount:I

.field mTextureNames:[Ljava/lang/String;

.field mTextureTypes:[Landroid/renderscript/Program$TextureType;

.field mTextures:[Landroid/renderscript/Type;


# direct methods
.method protected constructor <init>(Landroid/renderscript/RenderScript;)V
    .registers 5
    .parameter "rs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/16 v1, 0x8

    #@3
    .line 214
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 215
    iput-object p1, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@8
    .line 216
    new-array v0, v1, [Landroid/renderscript/Element;

    #@a
    iput-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputs:[Landroid/renderscript/Element;

    #@c
    .line 217
    new-array v0, v1, [Landroid/renderscript/Element;

    #@e
    iput-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mOutputs:[Landroid/renderscript/Element;

    #@10
    .line 218
    new-array v0, v1, [Landroid/renderscript/Type;

    #@12
    iput-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstants:[Landroid/renderscript/Type;

    #@14
    .line 219
    iput v2, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputCount:I

    #@16
    .line 220
    iput v2, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mOutputCount:I

    #@18
    .line 221
    iput v2, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstantCount:I

    #@1a
    .line 222
    iput v2, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@1c
    .line 223
    new-array v0, v1, [Landroid/renderscript/Program$TextureType;

    #@1e
    iput-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureTypes:[Landroid/renderscript/Program$TextureType;

    #@20
    .line 224
    new-array v0, v1, [Ljava/lang/String;

    #@22
    iput-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureNames:[Ljava/lang/String;

    #@24
    .line 225
    return-void
.end method


# virtual methods
.method public addConstant(Landroid/renderscript/Type;)Landroid/renderscript/Program$BaseProgramBuilder;
    .registers 4
    .parameter "t"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 309
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstantCount:I

    #@2
    const/16 v1, 0x8

    #@4
    if-lt v0, v1, :cond_e

    #@6
    .line 310
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@8
    const-string v1, "Max input count exceeded."

    #@a
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 312
    :cond_e
    invoke-virtual {p1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/renderscript/Element;->isComplex()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_20

    #@18
    .line 313
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@1a
    const-string v1, "Complex elements not allowed."

    #@1c
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 315
    :cond_20
    iget-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstants:[Landroid/renderscript/Type;

    #@22
    iget v1, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstantCount:I

    #@24
    aput-object p1, v0, v1

    #@26
    .line 316
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstantCount:I

    #@28
    add-int/lit8 v0, v0, 0x1

    #@2a
    iput v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstantCount:I

    #@2c
    .line 317
    return-object p0
.end method

.method public addTexture(Landroid/renderscript/Program$TextureType;)Landroid/renderscript/Program$BaseProgramBuilder;
    .registers 4
    .parameter "texType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Tex"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p0, p1, v0}, Landroid/renderscript/Program$BaseProgramBuilder;->addTexture(Landroid/renderscript/Program$TextureType;Ljava/lang/String;)Landroid/renderscript/Program$BaseProgramBuilder;

    #@18
    .line 329
    return-object p0
.end method

.method public addTexture(Landroid/renderscript/Program$TextureType;Ljava/lang/String;)Landroid/renderscript/Program$BaseProgramBuilder;
    .registers 5
    .parameter "texType"
    .parameter "texName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 343
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@2
    const/16 v1, 0x8

    #@4
    if-lt v0, v1, :cond_e

    #@6
    .line 344
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Max texture count exceeded."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 346
    :cond_e
    iget-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureTypes:[Landroid/renderscript/Program$TextureType;

    #@10
    iget v1, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@12
    aput-object p1, v0, v1

    #@14
    .line 347
    iget-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureNames:[Ljava/lang/String;

    #@16
    iget v1, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@18
    aput-object p2, v0, v1

    #@1a
    .line 348
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@1c
    add-int/lit8 v0, v0, 0x1

    #@1e
    iput v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@20
    .line 349
    return-object p0
.end method

.method public getCurrentConstantIndex()I
    .registers 2

    #@0
    .prologue
    .line 289
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstantCount:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    return v0
.end method

.method public getCurrentTextureIndex()I
    .registers 2

    #@0
    .prologue
    .line 297
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    return v0
.end method

.method protected initProgram(Landroid/renderscript/Program;)V
    .registers 6
    .parameter "p"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 353
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputCount:I

    #@3
    new-array v0, v0, [Landroid/renderscript/Element;

    #@5
    iput-object v0, p1, Landroid/renderscript/Program;->mInputs:[Landroid/renderscript/Element;

    #@7
    .line 354
    iget-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputs:[Landroid/renderscript/Element;

    #@9
    iget-object v1, p1, Landroid/renderscript/Program;->mInputs:[Landroid/renderscript/Element;

    #@b
    iget v2, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mInputCount:I

    #@d
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@10
    .line 355
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mOutputCount:I

    #@12
    new-array v0, v0, [Landroid/renderscript/Element;

    #@14
    iput-object v0, p1, Landroid/renderscript/Program;->mOutputs:[Landroid/renderscript/Element;

    #@16
    .line 356
    iget-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mOutputs:[Landroid/renderscript/Element;

    #@18
    iget-object v1, p1, Landroid/renderscript/Program;->mOutputs:[Landroid/renderscript/Element;

    #@1a
    iget v2, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mOutputCount:I

    #@1c
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1f
    .line 357
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstantCount:I

    #@21
    new-array v0, v0, [Landroid/renderscript/Type;

    #@23
    iput-object v0, p1, Landroid/renderscript/Program;->mConstants:[Landroid/renderscript/Type;

    #@25
    .line 358
    iget-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstants:[Landroid/renderscript/Type;

    #@27
    iget-object v1, p1, Landroid/renderscript/Program;->mConstants:[Landroid/renderscript/Type;

    #@29
    iget v2, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mConstantCount:I

    #@2b
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2e
    .line 359
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@30
    iput v0, p1, Landroid/renderscript/Program;->mTextureCount:I

    #@32
    .line 360
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@34
    new-array v0, v0, [Landroid/renderscript/Program$TextureType;

    #@36
    iput-object v0, p1, Landroid/renderscript/Program;->mTextures:[Landroid/renderscript/Program$TextureType;

    #@38
    .line 361
    iget-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureTypes:[Landroid/renderscript/Program$TextureType;

    #@3a
    iget-object v1, p1, Landroid/renderscript/Program;->mTextures:[Landroid/renderscript/Program$TextureType;

    #@3c
    iget v2, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@3e
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@41
    .line 362
    iget v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@43
    new-array v0, v0, [Ljava/lang/String;

    #@45
    iput-object v0, p1, Landroid/renderscript/Program;->mTextureNames:[Ljava/lang/String;

    #@47
    .line 363
    iget-object v0, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureNames:[Ljava/lang/String;

    #@49
    iget-object v1, p1, Landroid/renderscript/Program;->mTextureNames:[Ljava/lang/String;

    #@4b
    iget v2, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mTextureCount:I

    #@4d
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@50
    .line 364
    return-void
.end method

.method public setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;
    .registers 13
    .parameter "resources"
    .parameter "resourceID"

    #@0
    .prologue
    .line 249
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    #@3
    move-result-object v4

    #@4
    .line 252
    .local v4, is:Ljava/io/InputStream;
    const/16 v7, 0x400

    #@6
    :try_start_6
    new-array v5, v7, [B

    #@8
    .line 253
    .local v5, str:[B
    const/4 v6, 0x0

    #@9
    .line 255
    .local v6, strLength:I
    :goto_9
    array-length v7, v5

    #@a
    sub-int v1, v7, v6

    #@c
    .line 256
    .local v1, bytesLeft:I
    if-nez v1, :cond_1d

    #@e
    .line 257
    array-length v7, v5

    #@f
    mul-int/lit8 v7, v7, 0x2

    #@11
    new-array v0, v7, [B

    #@13
    .line 258
    .local v0, buf2:[B
    const/4 v7, 0x0

    #@14
    const/4 v8, 0x0

    #@15
    array-length v9, v5

    #@16
    invoke-static {v5, v7, v0, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 259
    move-object v5, v0

    #@1a
    .line 260
    array-length v7, v5

    #@1b
    sub-int v1, v7, v6

    #@1d
    .line 262
    .end local v0           #buf2:[B
    :cond_1d
    invoke-virtual {v4, v5, v6, v1}, Ljava/io/InputStream;->read([BII)I
    :try_end_20
    .catchall {:try_start_6 .. :try_end_20} :catchall_33

    #@20
    move-result v2

    #@21
    .line 263
    .local v2, bytesRead:I
    if-gtz v2, :cond_31

    #@23
    .line 269
    :try_start_23
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_26} :catch_38

    #@26
    .line 276
    :try_start_26
    new-instance v7, Ljava/lang/String;

    #@28
    const/4 v8, 0x0

    #@29
    const-string v9, "UTF-8"

    #@2b
    invoke-direct {v7, v5, v8, v6, v9}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@2e
    iput-object v7, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mShader:Ljava/lang/String;
    :try_end_30
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_26 .. :try_end_30} :catch_3f

    #@30
    .line 281
    :goto_30
    return-object p0

    #@31
    .line 266
    :cond_31
    add-int/2addr v6, v2

    #@32
    .line 267
    goto :goto_9

    #@33
    .line 269
    .end local v1           #bytesLeft:I
    .end local v2           #bytesRead:I
    .end local v5           #str:[B
    .end local v6           #strLength:I
    :catchall_33
    move-exception v7

    #@34
    :try_start_34
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    #@37
    throw v7
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_38} :catch_38

    #@38
    .line 271
    :catch_38
    move-exception v3

    #@39
    .line 272
    .local v3, e:Ljava/io/IOException;
    new-instance v7, Landroid/content/res/Resources$NotFoundException;

    #@3b
    invoke-direct {v7}, Landroid/content/res/Resources$NotFoundException;-><init>()V

    #@3e
    throw v7

    #@3f
    .line 277
    .end local v3           #e:Ljava/io/IOException;
    .restart local v1       #bytesLeft:I
    .restart local v2       #bytesRead:I
    .restart local v5       #str:[B
    .restart local v6       #strLength:I
    :catch_3f
    move-exception v3

    #@40
    .line 278
    .local v3, e:Ljava/io/UnsupportedEncodingException;
    const-string v7, "Renderscript shader creation"

    #@42
    const-string v8, "Could not decode shader string"

    #@44
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    goto :goto_30
.end method

.method public setShader(Ljava/lang/String;)Landroid/renderscript/Program$BaseProgramBuilder;
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 234
    iput-object p1, p0, Landroid/renderscript/Program$BaseProgramBuilder;->mShader:Ljava/lang/String;

    #@2
    .line 235
    return-object p0
.end method
