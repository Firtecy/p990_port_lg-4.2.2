.class public final enum Landroid/renderscript/Font$Style;
.super Ljava/lang/Enum;
.source "Font.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Font;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Style"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/renderscript/Font$Style;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/renderscript/Font$Style;

.field public static final enum BOLD:Landroid/renderscript/Font$Style;

.field public static final enum BOLD_ITALIC:Landroid/renderscript/Font$Style;

.field public static final enum ITALIC:Landroid/renderscript/Font$Style;

.field public static final enum NORMAL:Landroid/renderscript/Font$Style;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 82
    new-instance v0, Landroid/renderscript/Font$Style;

    #@6
    const-string v1, "NORMAL"

    #@8
    invoke-direct {v0, v1, v2}, Landroid/renderscript/Font$Style;-><init>(Ljava/lang/String;I)V

    #@b
    sput-object v0, Landroid/renderscript/Font$Style;->NORMAL:Landroid/renderscript/Font$Style;

    #@d
    .line 86
    new-instance v0, Landroid/renderscript/Font$Style;

    #@f
    const-string v1, "BOLD"

    #@11
    invoke-direct {v0, v1, v3}, Landroid/renderscript/Font$Style;-><init>(Ljava/lang/String;I)V

    #@14
    sput-object v0, Landroid/renderscript/Font$Style;->BOLD:Landroid/renderscript/Font$Style;

    #@16
    .line 90
    new-instance v0, Landroid/renderscript/Font$Style;

    #@18
    const-string v1, "ITALIC"

    #@1a
    invoke-direct {v0, v1, v4}, Landroid/renderscript/Font$Style;-><init>(Ljava/lang/String;I)V

    #@1d
    sput-object v0, Landroid/renderscript/Font$Style;->ITALIC:Landroid/renderscript/Font$Style;

    #@1f
    .line 94
    new-instance v0, Landroid/renderscript/Font$Style;

    #@21
    const-string v1, "BOLD_ITALIC"

    #@23
    invoke-direct {v0, v1, v5}, Landroid/renderscript/Font$Style;-><init>(Ljava/lang/String;I)V

    #@26
    sput-object v0, Landroid/renderscript/Font$Style;->BOLD_ITALIC:Landroid/renderscript/Font$Style;

    #@28
    .line 78
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Landroid/renderscript/Font$Style;

    #@2b
    sget-object v1, Landroid/renderscript/Font$Style;->NORMAL:Landroid/renderscript/Font$Style;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Landroid/renderscript/Font$Style;->BOLD:Landroid/renderscript/Font$Style;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Landroid/renderscript/Font$Style;->ITALIC:Landroid/renderscript/Font$Style;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Landroid/renderscript/Font$Style;->BOLD_ITALIC:Landroid/renderscript/Font$Style;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Landroid/renderscript/Font$Style;->$VALUES:[Landroid/renderscript/Font$Style;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/renderscript/Font$Style;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 78
    const-class v0, Landroid/renderscript/Font$Style;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/Font$Style;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/renderscript/Font$Style;
    .registers 1

    #@0
    .prologue
    .line 78
    sget-object v0, Landroid/renderscript/Font$Style;->$VALUES:[Landroid/renderscript/Font$Style;

    #@2
    invoke-virtual {v0}, [Landroid/renderscript/Font$Style;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/renderscript/Font$Style;

    #@8
    return-object v0
.end method
