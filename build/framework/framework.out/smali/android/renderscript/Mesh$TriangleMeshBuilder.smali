.class public Landroid/renderscript/Mesh$TriangleMeshBuilder;
.super Ljava/lang/Object;
.source "Mesh.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Mesh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TriangleMeshBuilder"
.end annotation


# static fields
.field public static final COLOR:I = 0x1

.field public static final NORMAL:I = 0x2

.field public static final TEXTURE_0:I = 0x100


# instance fields
.field mA:F

.field mB:F

.field mElement:Landroid/renderscript/Element;

.field mFlags:I

.field mG:F

.field mIndexCount:I

.field mIndexData:[S

.field mMaxIndex:I

.field mNX:F

.field mNY:F

.field mNZ:F

.field mR:F

.field mRS:Landroid/renderscript/RenderScript;

.field mS0:F

.field mT0:F

.field mVtxCount:I

.field mVtxData:[F

.field mVtxSize:I


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;II)V
    .registers 9
    .parameter "rs"
    .parameter "vtxSize"
    .parameter "flags"

    #@0
    .prologue
    const/16 v4, 0x80

    #@2
    const/4 v3, 0x0

    #@3
    const/high16 v2, 0x3f80

    #@5
    const/4 v1, 0x0

    #@6
    .line 593
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 557
    iput v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mNX:F

    #@b
    .line 558
    iput v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mNY:F

    #@d
    .line 559
    const/high16 v0, -0x4080

    #@f
    iput v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mNZ:F

    #@11
    .line 560
    iput v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mS0:F

    #@13
    .line 561
    iput v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mT0:F

    #@15
    .line 562
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mR:F

    #@17
    .line 563
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mG:F

    #@19
    .line 564
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mB:F

    #@1b
    .line 565
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mA:F

    #@1d
    .line 594
    iput-object p1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@1f
    .line 595
    iput v3, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@21
    .line 596
    iput v3, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mMaxIndex:I

    #@23
    .line 597
    iput v3, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexCount:I

    #@25
    .line 598
    new-array v0, v4, [F

    #@27
    iput-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@29
    .line 599
    new-array v0, v4, [S

    #@2b
    iput-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexData:[S

    #@2d
    .line 600
    iput p2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxSize:I

    #@2f
    .line 601
    iput p3, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mFlags:I

    #@31
    .line 603
    const/4 v0, 0x2

    #@32
    if-lt p2, v0, :cond_37

    #@34
    const/4 v0, 0x3

    #@35
    if-le p2, v0, :cond_3f

    #@37
    .line 604
    :cond_37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@39
    const-string v1, "Vertex size out of range."

    #@3b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v0

    #@3f
    .line 606
    :cond_3f
    return-void
.end method

.method private latch()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x4

    #@1
    .line 617
    iget v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mFlags:I

    #@3
    and-int/lit8 v0, v0, 0x1

    #@5
    if-eqz v0, :cond_3a

    #@7
    .line 618
    invoke-direct {p0, v3}, Landroid/renderscript/Mesh$TriangleMeshBuilder;->makeSpace(I)V

    #@a
    .line 619
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@c
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@e
    add-int/lit8 v2, v1, 0x1

    #@10
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@12
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mR:F

    #@14
    aput v2, v0, v1

    #@16
    .line 620
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@18
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@1a
    add-int/lit8 v2, v1, 0x1

    #@1c
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@1e
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mG:F

    #@20
    aput v2, v0, v1

    #@22
    .line 621
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@24
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@26
    add-int/lit8 v2, v1, 0x1

    #@28
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@2a
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mB:F

    #@2c
    aput v2, v0, v1

    #@2e
    .line 622
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@30
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@32
    add-int/lit8 v2, v1, 0x1

    #@34
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@36
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mA:F

    #@38
    aput v2, v0, v1

    #@3a
    .line 624
    :cond_3a
    iget v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mFlags:I

    #@3c
    and-int/lit16 v0, v0, 0x100

    #@3e
    if-eqz v0, :cond_5c

    #@40
    .line 625
    const/4 v0, 0x2

    #@41
    invoke-direct {p0, v0}, Landroid/renderscript/Mesh$TriangleMeshBuilder;->makeSpace(I)V

    #@44
    .line 626
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@46
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@48
    add-int/lit8 v2, v1, 0x1

    #@4a
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@4c
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mS0:F

    #@4e
    aput v2, v0, v1

    #@50
    .line 627
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@52
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@54
    add-int/lit8 v2, v1, 0x1

    #@56
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@58
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mT0:F

    #@5a
    aput v2, v0, v1

    #@5c
    .line 629
    :cond_5c
    iget v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mFlags:I

    #@5e
    and-int/lit8 v0, v0, 0x2

    #@60
    if-eqz v0, :cond_94

    #@62
    .line 630
    invoke-direct {p0, v3}, Landroid/renderscript/Mesh$TriangleMeshBuilder;->makeSpace(I)V

    #@65
    .line 631
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@67
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@69
    add-int/lit8 v2, v1, 0x1

    #@6b
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@6d
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mNX:F

    #@6f
    aput v2, v0, v1

    #@71
    .line 632
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@73
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@75
    add-int/lit8 v2, v1, 0x1

    #@77
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@79
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mNY:F

    #@7b
    aput v2, v0, v1

    #@7d
    .line 633
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@7f
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@81
    add-int/lit8 v2, v1, 0x1

    #@83
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@85
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mNZ:F

    #@87
    aput v2, v0, v1

    #@89
    .line 634
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@8b
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@8d
    add-int/lit8 v2, v1, 0x1

    #@8f
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@91
    const/4 v2, 0x0

    #@92
    aput v2, v0, v1

    #@94
    .line 636
    :cond_94
    iget v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mMaxIndex:I

    #@96
    add-int/lit8 v0, v0, 0x1

    #@98
    iput v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mMaxIndex:I

    #@9a
    .line 637
    return-void
.end method

.method private makeSpace(I)V
    .registers 6
    .parameter "count"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 609
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@3
    add-int/2addr v1, p1

    #@4
    iget-object v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@6
    array-length v2, v2

    #@7
    if-lt v1, v2, :cond_1a

    #@9
    .line 610
    iget-object v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@b
    array-length v1, v1

    #@c
    mul-int/lit8 v1, v1, 0x2

    #@e
    new-array v0, v1, [F

    #@10
    .line 611
    .local v0, t:[F
    iget-object v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@12
    iget-object v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@14
    array-length v2, v2

    #@15
    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@18
    .line 612
    iput-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@1a
    .line 614
    .end local v0           #t:[F
    :cond_1a
    return-void
.end method


# virtual methods
.method public addTriangle(III)Landroid/renderscript/Mesh$TriangleMeshBuilder;
    .registers 8
    .parameter "idx1"
    .parameter "idx2"
    .parameter "idx3"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 755
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mMaxIndex:I

    #@3
    if-ge p1, v1, :cond_13

    #@5
    if-ltz p1, :cond_13

    #@7
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mMaxIndex:I

    #@9
    if-ge p2, v1, :cond_13

    #@b
    if-ltz p2, :cond_13

    #@d
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mMaxIndex:I

    #@f
    if-ge p3, v1, :cond_13

    #@11
    if-gez p3, :cond_1b

    #@13
    .line 758
    :cond_13
    new-instance v1, Ljava/lang/IllegalStateException;

    #@15
    const-string v2, "Index provided greater than vertex count."

    #@17
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 760
    :cond_1b
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexCount:I

    #@1d
    add-int/lit8 v1, v1, 0x3

    #@1f
    iget-object v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexData:[S

    #@21
    array-length v2, v2

    #@22
    if-lt v1, v2, :cond_35

    #@24
    .line 761
    iget-object v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexData:[S

    #@26
    array-length v1, v1

    #@27
    mul-int/lit8 v1, v1, 0x2

    #@29
    new-array v0, v1, [S

    #@2b
    .line 762
    .local v0, t:[S
    iget-object v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexData:[S

    #@2d
    iget-object v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexData:[S

    #@2f
    array-length v2, v2

    #@30
    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@33
    .line 763
    iput-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexData:[S

    #@35
    .line 765
    .end local v0           #t:[S
    :cond_35
    iget-object v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexData:[S

    #@37
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexCount:I

    #@39
    add-int/lit8 v3, v2, 0x1

    #@3b
    iput v3, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexCount:I

    #@3d
    int-to-short v3, p1

    #@3e
    aput-short v3, v1, v2

    #@40
    .line 766
    iget-object v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexData:[S

    #@42
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexCount:I

    #@44
    add-int/lit8 v3, v2, 0x1

    #@46
    iput v3, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexCount:I

    #@48
    int-to-short v3, p2

    #@49
    aput-short v3, v1, v2

    #@4b
    .line 767
    iget-object v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexData:[S

    #@4d
    iget v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexCount:I

    #@4f
    add-int/lit8 v3, v2, 0x1

    #@51
    iput v3, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexCount:I

    #@53
    int-to-short v3, p3

    #@54
    aput-short v3, v1, v2

    #@56
    .line 768
    return-object p0
.end method

.method public addVertex(FF)Landroid/renderscript/Mesh$TriangleMeshBuilder;
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 650
    iget v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxSize:I

    #@3
    if-eq v0, v1, :cond_d

    #@5
    .line 651
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string v1, "add mistmatch with declared components."

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 653
    :cond_d
    invoke-direct {p0, v1}, Landroid/renderscript/Mesh$TriangleMeshBuilder;->makeSpace(I)V

    #@10
    .line 654
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@12
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@14
    add-int/lit8 v2, v1, 0x1

    #@16
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@18
    aput p1, v0, v1

    #@1a
    .line 655
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@1c
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@1e
    add-int/lit8 v2, v1, 0x1

    #@20
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@22
    aput p2, v0, v1

    #@24
    .line 656
    invoke-direct {p0}, Landroid/renderscript/Mesh$TriangleMeshBuilder;->latch()V

    #@27
    .line 657
    return-object p0
.end method

.method public addVertex(FFF)Landroid/renderscript/Mesh$TriangleMeshBuilder;
    .registers 7
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 672
    iget v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxSize:I

    #@2
    const/4 v1, 0x3

    #@3
    if-eq v0, v1, :cond_d

    #@5
    .line 673
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string v1, "add mistmatch with declared components."

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 675
    :cond_d
    const/4 v0, 0x4

    #@e
    invoke-direct {p0, v0}, Landroid/renderscript/Mesh$TriangleMeshBuilder;->makeSpace(I)V

    #@11
    .line 676
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@13
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@15
    add-int/lit8 v2, v1, 0x1

    #@17
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@19
    aput p1, v0, v1

    #@1b
    .line 677
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@1d
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@1f
    add-int/lit8 v2, v1, 0x1

    #@21
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@23
    aput p2, v0, v1

    #@25
    .line 678
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@27
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@29
    add-int/lit8 v2, v1, 0x1

    #@2b
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@2d
    aput p3, v0, v1

    #@2f
    .line 679
    iget-object v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@31
    iget v1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@33
    add-int/lit8 v2, v1, 0x1

    #@35
    iput v2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxCount:I

    #@37
    const/high16 v2, 0x3f80

    #@39
    aput v2, v0, v1

    #@3b
    .line 680
    invoke-direct {p0}, Landroid/renderscript/Mesh$TriangleMeshBuilder;->latch()V

    #@3e
    .line 681
    return-object p0
.end method

.method public create(Z)Landroid/renderscript/Mesh;
    .registers 11
    .parameter "uploadToBufferObject"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 787
    new-instance v0, Landroid/renderscript/Element$Builder;

    #@4
    iget-object v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@6
    invoke-direct {v0, v4}, Landroid/renderscript/Element$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    #@9
    .line 788
    .local v0, b:Landroid/renderscript/Element$Builder;
    iget-object v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@b
    sget-object v5, Landroid/renderscript/Element$DataType;->FLOAT_32:Landroid/renderscript/Element$DataType;

    #@d
    iget v6, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxSize:I

    #@f
    invoke-static {v4, v5, v6}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@12
    move-result-object v4

    #@13
    const-string/jumbo v5, "position"

    #@16
    invoke-virtual {v0, v4, v5}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    #@19
    .line 791
    iget v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mFlags:I

    #@1b
    and-int/lit8 v4, v4, 0x1

    #@1d
    if-eqz v4, :cond_2a

    #@1f
    .line 792
    iget-object v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@21
    invoke-static {v4}, Landroid/renderscript/Element;->F32_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@24
    move-result-object v4

    #@25
    const-string v5, "color"

    #@27
    invoke-virtual {v0, v4, v5}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    #@2a
    .line 794
    :cond_2a
    iget v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mFlags:I

    #@2c
    and-int/lit16 v4, v4, 0x100

    #@2e
    if-eqz v4, :cond_3c

    #@30
    .line 795
    iget-object v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@32
    invoke-static {v4}, Landroid/renderscript/Element;->F32_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@35
    move-result-object v4

    #@36
    const-string/jumbo v5, "texture0"

    #@39
    invoke-virtual {v0, v4, v5}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    #@3c
    .line 797
    :cond_3c
    iget v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mFlags:I

    #@3e
    and-int/lit8 v4, v4, 0x2

    #@40
    if-eqz v4, :cond_4e

    #@42
    .line 798
    iget-object v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@44
    invoke-static {v4}, Landroid/renderscript/Element;->F32_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@47
    move-result-object v4

    #@48
    const-string/jumbo v5, "normal"

    #@4b
    invoke-virtual {v0, v4, v5}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    #@4e
    .line 800
    :cond_4e
    invoke-virtual {v0}, Landroid/renderscript/Element$Builder;->create()Landroid/renderscript/Element;

    #@51
    move-result-object v4

    #@52
    iput-object v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mElement:Landroid/renderscript/Element;

    #@54
    .line 802
    const/4 v3, 0x1

    #@55
    .line 803
    .local v3, usage:I
    if-eqz p1, :cond_59

    #@57
    .line 804
    or-int/lit8 v3, v3, 0x4

    #@59
    .line 807
    :cond_59
    new-instance v2, Landroid/renderscript/Mesh$Builder;

    #@5b
    iget-object v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@5d
    invoke-direct {v2, v4, v3}, Landroid/renderscript/Mesh$Builder;-><init>(Landroid/renderscript/RenderScript;I)V

    #@60
    .line 808
    .local v2, smb:Landroid/renderscript/Mesh$Builder;
    iget-object v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mElement:Landroid/renderscript/Element;

    #@62
    iget v5, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mMaxIndex:I

    #@64
    invoke-virtual {v2, v4, v5}, Landroid/renderscript/Mesh$Builder;->addVertexType(Landroid/renderscript/Element;I)Landroid/renderscript/Mesh$Builder;

    #@67
    .line 809
    iget-object v4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@69
    invoke-static {v4}, Landroid/renderscript/Element;->U16(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@6c
    move-result-object v4

    #@6d
    iget v5, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexCount:I

    #@6f
    sget-object v6, Landroid/renderscript/Mesh$Primitive;->TRIANGLE:Landroid/renderscript/Mesh$Primitive;

    #@71
    invoke-virtual {v2, v4, v5, v6}, Landroid/renderscript/Mesh$Builder;->addIndexSetType(Landroid/renderscript/Element;ILandroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$Builder;

    #@74
    .line 811
    invoke-virtual {v2}, Landroid/renderscript/Mesh$Builder;->create()Landroid/renderscript/Mesh;

    #@77
    move-result-object v1

    #@78
    .line 813
    .local v1, sm:Landroid/renderscript/Mesh;
    invoke-virtual {v1, v7}, Landroid/renderscript/Mesh;->getVertexAllocation(I)Landroid/renderscript/Allocation;

    #@7b
    move-result-object v4

    #@7c
    iget v5, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mMaxIndex:I

    #@7e
    iget-object v6, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mVtxData:[F

    #@80
    invoke-virtual {v4, v7, v5, v6}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[F)V

    #@83
    .line 814
    if-eqz p1, :cond_8e

    #@85
    .line 815
    if-eqz p1, :cond_8e

    #@87
    .line 816
    invoke-virtual {v1, v7}, Landroid/renderscript/Mesh;->getVertexAllocation(I)Landroid/renderscript/Allocation;

    #@8a
    move-result-object v4

    #@8b
    invoke-virtual {v4, v8}, Landroid/renderscript/Allocation;->syncAll(I)V

    #@8e
    .line 820
    :cond_8e
    invoke-virtual {v1, v7}, Landroid/renderscript/Mesh;->getIndexSetAllocation(I)Landroid/renderscript/Allocation;

    #@91
    move-result-object v4

    #@92
    iget v5, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexCount:I

    #@94
    iget-object v6, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mIndexData:[S

    #@96
    invoke-virtual {v4, v7, v5, v6}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[S)V

    #@99
    .line 821
    if-eqz p1, :cond_a2

    #@9b
    .line 822
    invoke-virtual {v1, v7}, Landroid/renderscript/Mesh;->getIndexSetAllocation(I)Landroid/renderscript/Allocation;

    #@9e
    move-result-object v4

    #@9f
    invoke-virtual {v4, v8}, Landroid/renderscript/Allocation;->syncAll(I)V

    #@a2
    .line 825
    :cond_a2
    return-object v1
.end method

.method public setColor(FFFF)Landroid/renderscript/Mesh$TriangleMeshBuilder;
    .registers 7
    .parameter "r"
    .parameter "g"
    .parameter "b"
    .parameter "a"

    #@0
    .prologue
    .line 734
    iget v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mFlags:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-nez v0, :cond_e

    #@6
    .line 735
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "add mistmatch with declared components."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 737
    :cond_e
    iput p1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mR:F

    #@10
    .line 738
    iput p2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mG:F

    #@12
    .line 739
    iput p3, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mB:F

    #@14
    .line 740
    iput p4, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mA:F

    #@16
    .line 741
    return-object p0
.end method

.method public setNormal(FFF)Landroid/renderscript/Mesh$TriangleMeshBuilder;
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 713
    iget v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mFlags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-nez v0, :cond_e

    #@6
    .line 714
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "add mistmatch with declared components."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 716
    :cond_e
    iput p1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mNX:F

    #@10
    .line 717
    iput p2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mNY:F

    #@12
    .line 718
    iput p3, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mNZ:F

    #@14
    .line 719
    return-object p0
.end method

.method public setTexture(FF)Landroid/renderscript/Mesh$TriangleMeshBuilder;
    .registers 5
    .parameter "s"
    .parameter "t"

    #@0
    .prologue
    .line 694
    iget v0, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mFlags:I

    #@2
    and-int/lit16 v0, v0, 0x100

    #@4
    if-nez v0, :cond_e

    #@6
    .line 695
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "add mistmatch with declared components."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 697
    :cond_e
    iput p1, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mS0:F

    #@10
    .line 698
    iput p2, p0, Landroid/renderscript/Mesh$TriangleMeshBuilder;->mT0:F

    #@12
    .line 699
    return-object p0
.end method
