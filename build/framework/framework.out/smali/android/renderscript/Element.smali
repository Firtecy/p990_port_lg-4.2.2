.class public Landroid/renderscript/Element;
.super Landroid/renderscript/BaseObj;
.source "Element.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Element$1;,
        Landroid/renderscript/Element$Builder;,
        Landroid/renderscript/Element$DataKind;,
        Landroid/renderscript/Element$DataType;
    }
.end annotation


# instance fields
.field mArraySizes:[I

.field mElementNames:[Ljava/lang/String;

.field mElements:[Landroid/renderscript/Element;

.field mKind:Landroid/renderscript/Element$DataKind;

.field mNormalized:Z

.field mOffsetInBytes:[I

.field mSize:I

.field mType:Landroid/renderscript/Element$DataType;

.field mVectorSize:I

.field mVisibleElementMap:[I


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 785
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 786
    return-void
.end method

.method constructor <init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;ZI)V
    .registers 8
    .parameter "id"
    .parameter "rs"
    .parameter "dt"
    .parameter "dk"
    .parameter "norm"
    .parameter "size"

    #@0
    .prologue
    .line 766
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 767
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_5_6_5:Landroid/renderscript/Element$DataType;

    #@5
    if-eq p3, v0, :cond_27

    #@7
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_4_4_4_4:Landroid/renderscript/Element$DataType;

    #@9
    if-eq p3, v0, :cond_27

    #@b
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_5_5_5_1:Landroid/renderscript/Element$DataType;

    #@d
    if-eq p3, v0, :cond_27

    #@f
    .line 770
    const/4 v0, 0x3

    #@10
    if-ne p6, v0, :cond_21

    #@12
    .line 771
    iget v0, p3, Landroid/renderscript/Element$DataType;->mSize:I

    #@14
    mul-int/lit8 v0, v0, 0x4

    #@16
    iput v0, p0, Landroid/renderscript/Element;->mSize:I

    #@18
    .line 778
    :goto_18
    iput-object p3, p0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@1a
    .line 779
    iput-object p4, p0, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@1c
    .line 780
    iput-boolean p5, p0, Landroid/renderscript/Element;->mNormalized:Z

    #@1e
    .line 781
    iput p6, p0, Landroid/renderscript/Element;->mVectorSize:I

    #@20
    .line 782
    return-void

    #@21
    .line 773
    :cond_21
    iget v0, p3, Landroid/renderscript/Element$DataType;->mSize:I

    #@23
    mul-int/2addr v0, p6

    #@24
    iput v0, p0, Landroid/renderscript/Element;->mSize:I

    #@26
    goto :goto_18

    #@27
    .line 776
    :cond_27
    iget v0, p3, Landroid/renderscript/Element$DataType;->mSize:I

    #@29
    iput v0, p0, Landroid/renderscript/Element;->mSize:I

    #@2b
    goto :goto_18
.end method

.method constructor <init>(ILandroid/renderscript/RenderScript;[Landroid/renderscript/Element;[Ljava/lang/String;[I)V
    .registers 10
    .parameter "id"
    .parameter "rs"
    .parameter "e"
    .parameter "n"
    .parameter "as"

    #@0
    .prologue
    .line 749
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 750
    const/4 v1, 0x0

    #@4
    iput v1, p0, Landroid/renderscript/Element;->mSize:I

    #@6
    .line 751
    const/4 v1, 0x1

    #@7
    iput v1, p0, Landroid/renderscript/Element;->mVectorSize:I

    #@9
    .line 752
    iput-object p3, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@b
    .line 753
    iput-object p4, p0, Landroid/renderscript/Element;->mElementNames:[Ljava/lang/String;

    #@d
    .line 754
    iput-object p5, p0, Landroid/renderscript/Element;->mArraySizes:[I

    #@f
    .line 755
    sget-object v1, Landroid/renderscript/Element$DataType;->NONE:Landroid/renderscript/Element$DataType;

    #@11
    iput-object v1, p0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@13
    .line 756
    sget-object v1, Landroid/renderscript/Element$DataKind;->USER:Landroid/renderscript/Element$DataKind;

    #@15
    iput-object v1, p0, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@17
    .line 757
    iget-object v1, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@19
    array-length v1, v1

    #@1a
    new-array v1, v1, [I

    #@1c
    iput-object v1, p0, Landroid/renderscript/Element;->mOffsetInBytes:[I

    #@1e
    .line 758
    const/4 v0, 0x0

    #@1f
    .local v0, ct:I
    :goto_1f
    iget-object v1, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@21
    array-length v1, v1

    #@22
    if-ge v0, v1, :cond_3d

    #@24
    .line 759
    iget-object v1, p0, Landroid/renderscript/Element;->mOffsetInBytes:[I

    #@26
    iget v2, p0, Landroid/renderscript/Element;->mSize:I

    #@28
    aput v2, v1, v0

    #@2a
    .line 760
    iget v1, p0, Landroid/renderscript/Element;->mSize:I

    #@2c
    iget-object v2, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@2e
    aget-object v2, v2, v0

    #@30
    iget v2, v2, Landroid/renderscript/Element;->mSize:I

    #@32
    iget-object v3, p0, Landroid/renderscript/Element;->mArraySizes:[I

    #@34
    aget v3, v3, v0

    #@36
    mul-int/2addr v2, v3

    #@37
    add-int/2addr v1, v2

    #@38
    iput v1, p0, Landroid/renderscript/Element;->mSize:I

    #@3a
    .line 758
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_1f

    #@3d
    .line 762
    :cond_3d
    invoke-direct {p0}, Landroid/renderscript/Element;->updateVisibleSubElements()V

    #@40
    .line 763
    return-void
.end method

.method public static ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 406
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ALLOCATION:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 407
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_ALLOCATION:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ALLOCATION:Landroid/renderscript/Element;

    #@c
    .line 409
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ALLOCATION:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static A_8(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 470
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_A_8:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 471
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_A:Landroid/renderscript/Element$DataKind;

    #@8
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createPixel(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_A_8:Landroid/renderscript/Element;

    #@e
    .line 473
    :cond_e
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_A_8:Landroid/renderscript/Element;

    #@10
    return-object v0
.end method

.method public static BOOLEAN(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 301
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_BOOLEAN:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 302
    sget-object v0, Landroid/renderscript/Element$DataType;->BOOLEAN:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_BOOLEAN:Landroid/renderscript/Element;

    #@c
    .line 304
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_BOOLEAN:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static ELEMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 392
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ELEMENT:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 393
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_ELEMENT:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ELEMENT:Landroid/renderscript/Element;

    #@c
    .line 395
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ELEMENT:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 378
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_F32:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 379
    sget-object v0, Landroid/renderscript/Element$DataType;->FLOAT_32:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_F32:Landroid/renderscript/Element;

    #@c
    .line 381
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_F32:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static F32_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 512
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FLOAT_2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 513
    sget-object v0, Landroid/renderscript/Element$DataType;->FLOAT_32:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FLOAT_2:Landroid/renderscript/Element;

    #@d
    .line 515
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FLOAT_2:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static F32_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 519
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FLOAT_3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 520
    sget-object v0, Landroid/renderscript/Element$DataType;->FLOAT_32:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FLOAT_3:Landroid/renderscript/Element;

    #@d
    .line 522
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FLOAT_3:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static F32_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 526
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FLOAT_4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 527
    sget-object v0, Landroid/renderscript/Element$DataType;->FLOAT_32:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FLOAT_4:Landroid/renderscript/Element;

    #@d
    .line 529
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FLOAT_4:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static F64(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 385
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_F64:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 386
    sget-object v0, Landroid/renderscript/Element$DataType;->FLOAT_64:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_F64:Landroid/renderscript/Element;

    #@c
    .line 388
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_F64:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static F64_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 533
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_DOUBLE_2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 534
    sget-object v0, Landroid/renderscript/Element$DataType;->FLOAT_64:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_DOUBLE_2:Landroid/renderscript/Element;

    #@d
    .line 536
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_DOUBLE_2:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static F64_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 540
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_DOUBLE_3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 541
    sget-object v0, Landroid/renderscript/Element$DataType;->FLOAT_64:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_DOUBLE_3:Landroid/renderscript/Element;

    #@d
    .line 543
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_DOUBLE_3:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static F64_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 547
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_DOUBLE_4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 548
    sget-object v0, Landroid/renderscript/Element$DataType;->FLOAT_64:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_DOUBLE_4:Landroid/renderscript/Element;

    #@d
    .line 550
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_DOUBLE_4:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static FONT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 462
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FONT:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 463
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_FONT:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FONT:Landroid/renderscript/Element;

    #@c
    .line 465
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_FONT:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static I16(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 343
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I16:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 344
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_16:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I16:Landroid/renderscript/Element;

    #@c
    .line 346
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I16:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static I16_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 617
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SHORT_2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 618
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_16:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SHORT_2:Landroid/renderscript/Element;

    #@d
    .line 620
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SHORT_2:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I16_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 624
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SHORT_3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 625
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_16:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SHORT_3:Landroid/renderscript/Element;

    #@d
    .line 627
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SHORT_3:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I16_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 631
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SHORT_4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 632
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_16:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SHORT_4:Landroid/renderscript/Element;

    #@d
    .line 634
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SHORT_4:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 357
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I32:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 358
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_32:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I32:Landroid/renderscript/Element;

    #@c
    .line 360
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I32:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static I32_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 659
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_INT_2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 660
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_32:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_INT_2:Landroid/renderscript/Element;

    #@d
    .line 662
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_INT_2:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I32_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 666
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_INT_3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 667
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_32:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_INT_3:Landroid/renderscript/Element;

    #@d
    .line 669
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_INT_3:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I32_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 673
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_INT_4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 674
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_32:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_INT_4:Landroid/renderscript/Element;

    #@d
    .line 676
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_INT_4:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I64(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 371
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I64:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 372
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_64:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I64:Landroid/renderscript/Element;

    #@c
    .line 374
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I64:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static I64_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 701
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_LONG_2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 702
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_64:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_LONG_2:Landroid/renderscript/Element;

    #@d
    .line 704
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_LONG_2:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I64_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 708
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_LONG_3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 709
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_64:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_LONG_3:Landroid/renderscript/Element;

    #@d
    .line 711
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_LONG_3:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I64_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 715
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_LONG_4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 716
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_64:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_LONG_4:Landroid/renderscript/Element;

    #@d
    .line 718
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_LONG_4:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I8(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 329
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I8:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 330
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I8:Landroid/renderscript/Element;

    #@c
    .line 332
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_I8:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static I8_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 575
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_CHAR_2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 576
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_CHAR_2:Landroid/renderscript/Element;

    #@d
    .line 578
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_CHAR_2:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I8_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 582
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_CHAR_3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 583
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_CHAR_3:Landroid/renderscript/Element;

    #@d
    .line 585
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_CHAR_3:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static I8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 589
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_CHAR_4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 590
    sget-object v0, Landroid/renderscript/Element$DataType;->SIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_CHAR_4:Landroid/renderscript/Element;

    #@d
    .line 592
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_CHAR_4:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static MATRIX4X4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 731
    invoke-static {p0}, Landroid/renderscript/Element;->MATRIX_4X4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static MATRIX_2X2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 742
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MATRIX_2X2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 743
    sget-object v0, Landroid/renderscript/Element$DataType;->MATRIX_2X2:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MATRIX_2X2:Landroid/renderscript/Element;

    #@c
    .line 745
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MATRIX_2X2:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static MATRIX_3X3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 735
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MATRIX_3X3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 736
    sget-object v0, Landroid/renderscript/Element$DataType;->MATRIX_3X3:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MATRIX_3X3:Landroid/renderscript/Element;

    #@c
    .line 738
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MATRIX_3X3:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static MATRIX_4X4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 722
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MATRIX_4X4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 723
    sget-object v0, Landroid/renderscript/Element$DataType;->MATRIX_4X4:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MATRIX_4X4:Landroid/renderscript/Element;

    #@c
    .line 725
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MATRIX_4X4:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 427
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MESH:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 428
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_MESH:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MESH:Landroid/renderscript/Element;

    #@c
    .line 430
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_MESH:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 434
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 435
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_PROGRAM_FRAGMENT:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    #@c
    .line 437
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static PROGRAM_RASTER(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 448
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_RASTER:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 449
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_PROGRAM_RASTER:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_RASTER:Landroid/renderscript/Element;

    #@c
    .line 451
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_RASTER:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 455
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_STORE:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 456
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_PROGRAM_STORE:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_STORE:Landroid/renderscript/Element;

    #@c
    .line 458
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_STORE:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 441
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_VERTEX:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 442
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_PROGRAM_VERTEX:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_VERTEX:Landroid/renderscript/Element;

    #@c
    .line 444
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_PROGRAM_VERTEX:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static RGBA_4444(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 498
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGBA_4444:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 499
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_4_4_4_4:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@8
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createPixel(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGBA_4444:Landroid/renderscript/Element;

    #@e
    .line 501
    :cond_e
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGBA_4444:Landroid/renderscript/Element;

    #@10
    return-object v0
.end method

.method public static RGBA_5551(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 491
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGBA_5551:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 492
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_5_5_5_1:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@8
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createPixel(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGBA_5551:Landroid/renderscript/Element;

    #@e
    .line 494
    :cond_e
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGBA_5551:Landroid/renderscript/Element;

    #@10
    return-object v0
.end method

.method public static RGBA_8888(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 505
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGBA_8888:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 506
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@8
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createPixel(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGBA_8888:Landroid/renderscript/Element;

    #@e
    .line 508
    :cond_e
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGBA_8888:Landroid/renderscript/Element;

    #@10
    return-object v0
.end method

.method public static RGB_565(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 477
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGB_565:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 478
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_5_6_5:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_RGB:Landroid/renderscript/Element$DataKind;

    #@8
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createPixel(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGB_565:Landroid/renderscript/Element;

    #@e
    .line 480
    :cond_e
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGB_565:Landroid/renderscript/Element;

    #@10
    return-object v0
.end method

.method public static RGB_888(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 484
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGB_888:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 485
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_RGB:Landroid/renderscript/Element$DataKind;

    #@8
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createPixel(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGB_888:Landroid/renderscript/Element;

    #@e
    .line 487
    :cond_e
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_RGB_888:Landroid/renderscript/Element;

    #@10
    return-object v0
.end method

.method public static SAMPLER(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 413
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SAMPLER:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 414
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_SAMPLER:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SAMPLER:Landroid/renderscript/Element;

    #@c
    .line 416
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SAMPLER:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static SCRIPT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 420
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SCRIPT:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 421
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_SCRIPT:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SCRIPT:Landroid/renderscript/Element;

    #@c
    .line 423
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_SCRIPT:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static TYPE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 399
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_TYPE:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 400
    sget-object v0, Landroid/renderscript/Element$DataType;->RS_TYPE:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_TYPE:Landroid/renderscript/Element;

    #@c
    .line 402
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_TYPE:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static U16(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 336
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U16:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 337
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_16:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U16:Landroid/renderscript/Element;

    #@c
    .line 339
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U16:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static U16_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 596
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_USHORT_2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 597
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_16:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_USHORT_2:Landroid/renderscript/Element;

    #@d
    .line 599
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_USHORT_2:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U16_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 603
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_USHORT_3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 604
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_16:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_USHORT_3:Landroid/renderscript/Element;

    #@d
    .line 606
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_USHORT_3:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U16_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 610
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_USHORT_4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 611
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_16:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_USHORT_4:Landroid/renderscript/Element;

    #@d
    .line 613
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_USHORT_4:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 350
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U32:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 351
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_32:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U32:Landroid/renderscript/Element;

    #@c
    .line 353
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U32:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static U32_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 638
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UINT_2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 639
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_32:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UINT_2:Landroid/renderscript/Element;

    #@d
    .line 641
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UINT_2:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U32_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 645
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UINT_3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 646
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_32:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UINT_3:Landroid/renderscript/Element;

    #@d
    .line 648
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UINT_3:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U32_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 652
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UINT_4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 653
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_32:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UINT_4:Landroid/renderscript/Element;

    #@d
    .line 655
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UINT_4:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U64(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 364
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U64:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 365
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_64:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U64:Landroid/renderscript/Element;

    #@c
    .line 367
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U64:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static U64_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 680
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ULONG_2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 681
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_64:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ULONG_2:Landroid/renderscript/Element;

    #@d
    .line 683
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ULONG_2:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U64_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 687
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ULONG_3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 688
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_64:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ULONG_3:Landroid/renderscript/Element;

    #@d
    .line 690
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ULONG_3:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U64_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 694
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ULONG_4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 695
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_64:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ULONG_4:Landroid/renderscript/Element;

    #@d
    .line 697
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_ULONG_4:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U8(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 315
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U8:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 316
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    invoke-static {p0, v0}, Landroid/renderscript/Element;->createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U8:Landroid/renderscript/Element;

    #@c
    .line 318
    :cond_c
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_U8:Landroid/renderscript/Element;

    #@e
    return-object v0
.end method

.method public static U8_2(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 554
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UCHAR_2:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 555
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UCHAR_2:Landroid/renderscript/Element;

    #@d
    .line 557
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UCHAR_2:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U8_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 561
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UCHAR_3:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 562
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UCHAR_3:Landroid/renderscript/Element;

    #@d
    .line 564
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UCHAR_3:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 568
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UCHAR_4:Landroid/renderscript/Element;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 569
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {p0, v0, v1}, Landroid/renderscript/Element;->createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UCHAR_4:Landroid/renderscript/Element;

    #@d
    .line 571
    :cond_d
    iget-object v0, p0, Landroid/renderscript/RenderScript;->mElement_UCHAR_4:Landroid/renderscript/Element;

    #@f
    return-object v0
.end method

.method public static createPixel(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;
    .registers 10
    .parameter "rs"
    .parameter "dt"
    .parameter "dk"

    #@0
    .prologue
    .line 903
    sget-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_L:Landroid/renderscript/Element$DataKind;

    #@2
    if-eq p2, v0, :cond_20

    #@4
    sget-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_A:Landroid/renderscript/Element$DataKind;

    #@6
    if-eq p2, v0, :cond_20

    #@8
    sget-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_LA:Landroid/renderscript/Element$DataKind;

    #@a
    if-eq p2, v0, :cond_20

    #@c
    sget-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_RGB:Landroid/renderscript/Element$DataKind;

    #@e
    if-eq p2, v0, :cond_20

    #@10
    sget-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@12
    if-eq p2, v0, :cond_20

    #@14
    sget-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_DEPTH:Landroid/renderscript/Element$DataKind;

    #@16
    if-eq p2, v0, :cond_20

    #@18
    .line 909
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@1a
    const-string v2, "Unsupported DataKind"

    #@1c
    invoke-direct {v0, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 911
    :cond_20
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@22
    if-eq p1, v0, :cond_3c

    #@24
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_16:Landroid/renderscript/Element$DataType;

    #@26
    if-eq p1, v0, :cond_3c

    #@28
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_5_6_5:Landroid/renderscript/Element$DataType;

    #@2a
    if-eq p1, v0, :cond_3c

    #@2c
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_4_4_4_4:Landroid/renderscript/Element$DataType;

    #@2e
    if-eq p1, v0, :cond_3c

    #@30
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_5_5_5_1:Landroid/renderscript/Element$DataType;

    #@32
    if-eq p1, v0, :cond_3c

    #@34
    .line 916
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@36
    const-string v2, "Unsupported DataType"

    #@38
    invoke-direct {v0, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v0

    #@3c
    .line 918
    :cond_3c
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_5_6_5:Landroid/renderscript/Element$DataType;

    #@3e
    if-ne p1, v0, :cond_4c

    #@40
    sget-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_RGB:Landroid/renderscript/Element$DataKind;

    #@42
    if-eq p2, v0, :cond_4c

    #@44
    .line 919
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@46
    const-string v2, "Bad kind and type combo"

    #@48
    invoke-direct {v0, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v0

    #@4c
    .line 921
    :cond_4c
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_5_5_5_1:Landroid/renderscript/Element$DataType;

    #@4e
    if-ne p1, v0, :cond_5c

    #@50
    sget-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@52
    if-eq p2, v0, :cond_5c

    #@54
    .line 922
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@56
    const-string v2, "Bad kind and type combo"

    #@58
    invoke-direct {v0, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v0

    #@5c
    .line 924
    :cond_5c
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_4_4_4_4:Landroid/renderscript/Element$DataType;

    #@5e
    if-ne p1, v0, :cond_6c

    #@60
    sget-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@62
    if-eq p2, v0, :cond_6c

    #@64
    .line 925
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@66
    const-string v2, "Bad kind and type combo"

    #@68
    invoke-direct {v0, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v0

    #@6c
    .line 927
    :cond_6c
    sget-object v0, Landroid/renderscript/Element$DataType;->UNSIGNED_16:Landroid/renderscript/Element$DataType;

    #@6e
    if-ne p1, v0, :cond_7c

    #@70
    sget-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_DEPTH:Landroid/renderscript/Element$DataKind;

    #@72
    if-eq p2, v0, :cond_7c

    #@74
    .line 929
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@76
    const-string v2, "Bad kind and type combo"

    #@78
    invoke-direct {v0, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7b
    throw v0

    #@7c
    .line 932
    :cond_7c
    const/4 v6, 0x1

    #@7d
    .line 933
    .local v6, size:I
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataKind:[I

    #@7f
    invoke-virtual {p2}, Landroid/renderscript/Element$DataKind;->ordinal()I

    #@82
    move-result v2

    #@83
    aget v0, v0, v2

    #@85
    packed-switch v0, :pswitch_data_a2

    #@88
    .line 948
    :goto_88
    const/4 v5, 0x1

    #@89
    .line 949
    .local v5, norm:Z
    iget v0, p1, Landroid/renderscript/Element$DataType;->mID:I

    #@8b
    iget v2, p2, Landroid/renderscript/Element$DataKind;->mID:I

    #@8d
    invoke-virtual {p0, v0, v2, v5, v6}, Landroid/renderscript/RenderScript;->nElementCreate(IIZI)I

    #@90
    move-result v1

    #@91
    .line 950
    .local v1, id:I
    new-instance v0, Landroid/renderscript/Element;

    #@93
    move-object v2, p0

    #@94
    move-object v3, p1

    #@95
    move-object v4, p2

    #@96
    invoke-direct/range {v0 .. v6}, Landroid/renderscript/Element;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;ZI)V

    #@99
    return-object v0

    #@9a
    .line 935
    .end local v1           #id:I
    .end local v5           #norm:Z
    :pswitch_9a
    const/4 v6, 0x2

    #@9b
    .line 936
    goto :goto_88

    #@9c
    .line 938
    :pswitch_9c
    const/4 v6, 0x3

    #@9d
    .line 939
    goto :goto_88

    #@9e
    .line 941
    :pswitch_9e
    const/4 v6, 0x4

    #@9f
    .line 942
    goto :goto_88

    #@a0
    .line 944
    :pswitch_a0
    const/4 v6, 0x2

    #@a1
    goto :goto_88

    #@a2
    .line 933
    :pswitch_data_a2
    .packed-switch 0x1
        :pswitch_9a
        :pswitch_9c
        :pswitch_9e
        :pswitch_a0
    .end packed-switch
.end method

.method static createUser(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;)Landroid/renderscript/Element;
    .registers 9
    .parameter "rs"
    .parameter "dt"

    #@0
    .prologue
    .line 839
    sget-object v4, Landroid/renderscript/Element$DataKind;->USER:Landroid/renderscript/Element$DataKind;

    #@2
    .line 840
    .local v4, dk:Landroid/renderscript/Element$DataKind;
    const/4 v5, 0x0

    #@3
    .line 841
    .local v5, norm:Z
    const/4 v6, 0x1

    #@4
    .line 842
    .local v6, vecSize:I
    iget v0, p1, Landroid/renderscript/Element$DataType;->mID:I

    #@6
    iget v2, v4, Landroid/renderscript/Element$DataKind;->mID:I

    #@8
    invoke-virtual {p0, v0, v2, v5, v6}, Landroid/renderscript/RenderScript;->nElementCreate(IIZI)I

    #@b
    move-result v1

    #@c
    .line 843
    .local v1, id:I
    new-instance v0, Landroid/renderscript/Element;

    #@e
    move-object v2, p0

    #@f
    move-object v3, p1

    #@10
    invoke-direct/range {v0 .. v6}, Landroid/renderscript/Element;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;ZI)V

    #@13
    return-object v0
.end method

.method public static createVector(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;I)Landroid/renderscript/Element;
    .registers 10
    .parameter "rs"
    .parameter "dt"
    .parameter "size"

    #@0
    .prologue
    .line 860
    const/4 v0, 0x2

    #@1
    if-lt p2, v0, :cond_6

    #@3
    const/4 v0, 0x4

    #@4
    if-le p2, v0, :cond_e

    #@6
    .line 861
    :cond_6
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@8
    const-string v2, "Vector size out of range 2-4."

    #@a
    invoke-direct {v0, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 864
    :cond_e
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@10
    invoke-virtual {p1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@13
    move-result v2

    #@14
    aget v0, v0, v2

    #@16
    packed-switch v0, :pswitch_data_36

    #@19
    .line 884
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@1b
    const-string v2, "Cannot create vector of non-primitive type."

    #@1d
    invoke-direct {v0, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 877
    :pswitch_21
    sget-object v4, Landroid/renderscript/Element$DataKind;->USER:Landroid/renderscript/Element$DataKind;

    #@23
    .line 878
    .local v4, dk:Landroid/renderscript/Element$DataKind;
    const/4 v5, 0x0

    #@24
    .line 879
    .local v5, norm:Z
    iget v0, p1, Landroid/renderscript/Element$DataType;->mID:I

    #@26
    iget v2, v4, Landroid/renderscript/Element$DataKind;->mID:I

    #@28
    invoke-virtual {p0, v0, v2, v5, p2}, Landroid/renderscript/RenderScript;->nElementCreate(IIZI)I

    #@2b
    move-result v1

    #@2c
    .line 880
    .local v1, id:I
    new-instance v0, Landroid/renderscript/Element;

    #@2e
    move-object v2, p0

    #@2f
    move-object v3, p1

    #@30
    move v6, p2

    #@31
    invoke-direct/range {v0 .. v6}, Landroid/renderscript/Element;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;ZI)V

    #@34
    return-object v0

    #@35
    .line 864
    nop

    #@36
    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
    .end packed-switch
.end method

.method private updateVisibleSubElements()V
    .registers 9

    #@0
    .prologue
    const/16 v7, 0x23

    #@2
    const/4 v6, 0x0

    #@3
    .line 65
    iget-object v5, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@5
    if-nez v5, :cond_8

    #@7
    .line 85
    :cond_7
    return-void

    #@8
    .line 69
    :cond_8
    const/4 v4, 0x0

    #@9
    .line 70
    .local v4, noPaddingFieldCount:I
    iget-object v5, p0, Landroid/renderscript/Element;->mElementNames:[Ljava/lang/String;

    #@b
    array-length v3, v5

    #@c
    .line 72
    .local v3, fieldCount:I
    const/4 v0, 0x0

    #@d
    .local v0, ct:I
    :goto_d
    if-ge v0, v3, :cond_1e

    #@f
    .line 73
    iget-object v5, p0, Landroid/renderscript/Element;->mElementNames:[Ljava/lang/String;

    #@11
    aget-object v5, v5, v0

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v5

    #@17
    if-eq v5, v7, :cond_1b

    #@19
    .line 74
    add-int/lit8 v4, v4, 0x1

    #@1b
    .line 72
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_d

    #@1e
    .line 77
    :cond_1e
    new-array v5, v4, [I

    #@20
    iput-object v5, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@22
    .line 80
    const/4 v0, 0x0

    #@23
    const/4 v1, 0x0

    #@24
    .local v1, ctNoPadding:I
    move v2, v1

    #@25
    .end local v1           #ctNoPadding:I
    .local v2, ctNoPadding:I
    :goto_25
    if-ge v0, v3, :cond_7

    #@27
    .line 81
    iget-object v5, p0, Landroid/renderscript/Element;->mElementNames:[Ljava/lang/String;

    #@29
    aget-object v5, v5, v0

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    #@2e
    move-result v5

    #@2f
    if-eq v5, v7, :cond_3b

    #@31
    .line 82
    iget-object v5, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@33
    add-int/lit8 v1, v2, 0x1

    #@35
    .end local v2           #ctNoPadding:I
    .restart local v1       #ctNoPadding:I
    aput v0, v5, v2

    #@37
    .line 80
    :goto_37
    add-int/lit8 v0, v0, 0x1

    #@39
    move v2, v1

    #@3a
    .end local v1           #ctNoPadding:I
    .restart local v2       #ctNoPadding:I
    goto :goto_25

    #@3b
    :cond_3b
    move v1, v2

    #@3c
    .end local v2           #ctNoPadding:I
    .restart local v1       #ctNoPadding:I
    goto :goto_37
.end method


# virtual methods
.method public getBytesSize()I
    .registers 2

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/renderscript/Element;->mSize:I

    #@2
    return v0
.end method

.method public getDataKind()Landroid/renderscript/Element$DataKind;
    .registers 2

    #@0
    .prologue
    .line 290
    iget-object v0, p0, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@2
    return-object v0
.end method

.method public getDataType()Landroid/renderscript/Element$DataType;
    .registers 2

    #@0
    .prologue
    .line 283
    iget-object v0, p0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@2
    return-object v0
.end method

.method public getSubElement(I)Landroid/renderscript/Element;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 221
    iget-object v0, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 222
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@6
    const-string v1, "Element contains no sub-elements"

    #@8
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 224
    :cond_c
    if-ltz p1, :cond_13

    #@e
    iget-object v0, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@10
    array-length v0, v0

    #@11
    if-lt p1, v0, :cond_1b

    #@13
    .line 225
    :cond_13
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@15
    const-string v1, "Illegal sub-element index"

    #@17
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 227
    :cond_1b
    iget-object v0, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@1d
    iget-object v1, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@1f
    aget v1, v1, p1

    #@21
    aget-object v0, v0, v1

    #@23
    return-object v0
.end method

.method public getSubElementArraySize(I)I
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 254
    iget-object v0, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 255
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@6
    const-string v1, "Element contains no sub-elements"

    #@8
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 257
    :cond_c
    if-ltz p1, :cond_13

    #@e
    iget-object v0, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@10
    array-length v0, v0

    #@11
    if-lt p1, v0, :cond_1b

    #@13
    .line 258
    :cond_13
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@15
    const-string v1, "Illegal sub-element index"

    #@17
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 260
    :cond_1b
    iget-object v0, p0, Landroid/renderscript/Element;->mArraySizes:[I

    #@1d
    iget-object v1, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@1f
    aget v1, v1, p1

    #@21
    aget v0, v0, v1

    #@23
    return v0
.end method

.method public getSubElementCount()I
    .registers 2

    #@0
    .prologue
    .line 208
    iget-object v0, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@2
    if-nez v0, :cond_6

    #@4
    .line 209
    const/4 v0, 0x0

    #@5
    .line 211
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@8
    array-length v0, v0

    #@9
    goto :goto_5
.end method

.method public getSubElementName(I)Ljava/lang/String;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 238
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@6
    const-string v1, "Element contains no sub-elements"

    #@8
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 240
    :cond_c
    if-ltz p1, :cond_13

    #@e
    iget-object v0, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@10
    array-length v0, v0

    #@11
    if-lt p1, v0, :cond_1b

    #@13
    .line 241
    :cond_13
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@15
    const-string v1, "Illegal sub-element index"

    #@17
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 243
    :cond_1b
    iget-object v0, p0, Landroid/renderscript/Element;->mElementNames:[Ljava/lang/String;

    #@1d
    iget-object v1, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@1f
    aget v1, v1, p1

    #@21
    aget-object v0, v0, v1

    #@23
    return-object v0
.end method

.method public getSubElementOffsetBytes(I)I
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 270
    iget-object v0, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 271
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@6
    const-string v1, "Element contains no sub-elements"

    #@8
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 273
    :cond_c
    if-ltz p1, :cond_13

    #@e
    iget-object v0, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@10
    array-length v0, v0

    #@11
    if-lt p1, v0, :cond_1b

    #@13
    .line 274
    :cond_13
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@15
    const-string v1, "Illegal sub-element index"

    #@17
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 276
    :cond_1b
    iget-object v0, p0, Landroid/renderscript/Element;->mOffsetInBytes:[I

    #@1d
    iget-object v1, p0, Landroid/renderscript/Element;->mVisibleElementMap:[I

    #@1f
    aget v1, v1, p1

    #@21
    aget v0, v0, v1

    #@23
    return v0
.end method

.method public getVectorSize()I
    .registers 2

    #@0
    .prologue
    .line 97
    iget v0, p0, Landroid/renderscript/Element;->mVectorSize:I

    #@2
    return v0
.end method

.method public isCompatible(Landroid/renderscript/Element;)Z
    .registers 5
    .parameter "e"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 966
    invoke-virtual {p0, p1}, Landroid/renderscript/Element;->equals(Ljava/lang/Object;)Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_8

    #@7
    .line 974
    :cond_7
    :goto_7
    return v0

    #@8
    :cond_8
    iget v1, p0, Landroid/renderscript/Element;->mSize:I

    #@a
    iget v2, p1, Landroid/renderscript/Element;->mSize:I

    #@c
    if-ne v1, v2, :cond_20

    #@e
    iget-object v1, p0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@10
    sget-object v2, Landroid/renderscript/Element$DataType;->NONE:Landroid/renderscript/Element$DataType;

    #@12
    if-eq v1, v2, :cond_20

    #@14
    iget-object v1, p0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@16
    iget-object v2, p1, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@18
    if-ne v1, v2, :cond_20

    #@1a
    iget v1, p0, Landroid/renderscript/Element;->mVectorSize:I

    #@1c
    iget v2, p1, Landroid/renderscript/Element;->mVectorSize:I

    #@1e
    if-eq v1, v2, :cond_7

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_7
.end method

.method public isComplex()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 189
    iget-object v2, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 197
    :cond_5
    :goto_5
    return v1

    #@6
    .line 192
    :cond_6
    const/4 v0, 0x0

    #@7
    .local v0, ct:I
    :goto_7
    iget-object v2, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@9
    array-length v2, v2

    #@a
    if-ge v0, v2, :cond_5

    #@c
    .line 193
    iget-object v2, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@e
    aget-object v2, v2, v0

    #@10
    iget-object v2, v2, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@12
    if-eqz v2, :cond_16

    #@14
    .line 194
    const/4 v1, 0x1

    #@15
    goto :goto_5

    #@16
    .line 192
    :cond_16
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_7
.end method

.method updateFromNative()V
    .registers 14

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 790
    invoke-super {p0}, Landroid/renderscript/BaseObj;->updateFromNative()V

    #@5
    .line 793
    const/4 v9, 0x5

    #@6
    new-array v1, v9, [I

    #@8
    .line 794
    .local v1, dataBuffer:[I
    iget-object v9, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    iget-object v12, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@c
    invoke-virtual {p0, v12}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@f
    move-result v12

    #@10
    invoke-virtual {v9, v12, v1}, Landroid/renderscript/RenderScript;->nElementGetNativeData(I[I)V

    #@13
    .line 796
    const/4 v9, 0x2

    #@14
    aget v9, v1, v9

    #@16
    if-ne v9, v10, :cond_40

    #@18
    move v9, v10

    #@19
    :goto_19
    iput-boolean v9, p0, Landroid/renderscript/Element;->mNormalized:Z

    #@1b
    .line 797
    const/4 v9, 0x3

    #@1c
    aget v9, v1, v9

    #@1e
    iput v9, p0, Landroid/renderscript/Element;->mVectorSize:I

    #@20
    .line 798
    iput v11, p0, Landroid/renderscript/Element;->mSize:I

    #@22
    .line 799
    invoke-static {}, Landroid/renderscript/Element$DataType;->values()[Landroid/renderscript/Element$DataType;

    #@25
    move-result-object v0

    #@26
    .local v0, arr$:[Landroid/renderscript/Element$DataType;
    array-length v6, v0

    #@27
    .local v6, len$:I
    const/4 v5, 0x0

    #@28
    .local v5, i$:I
    :goto_28
    if-ge v5, v6, :cond_42

    #@2a
    aget-object v3, v0, v5

    #@2c
    .line 800
    .local v3, dt:Landroid/renderscript/Element$DataType;
    iget v9, v3, Landroid/renderscript/Element$DataType;->mID:I

    #@2e
    aget v12, v1, v11

    #@30
    if-ne v9, v12, :cond_3d

    #@32
    .line 801
    iput-object v3, p0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@34
    .line 802
    iget-object v9, p0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@36
    iget v9, v9, Landroid/renderscript/Element$DataType;->mSize:I

    #@38
    iget v12, p0, Landroid/renderscript/Element;->mVectorSize:I

    #@3a
    mul-int/2addr v9, v12

    #@3b
    iput v9, p0, Landroid/renderscript/Element;->mSize:I

    #@3d
    .line 799
    :cond_3d
    add-int/lit8 v5, v5, 0x1

    #@3f
    goto :goto_28

    #@40
    .end local v0           #arr$:[Landroid/renderscript/Element$DataType;
    .end local v3           #dt:Landroid/renderscript/Element$DataType;
    .end local v5           #i$:I
    .end local v6           #len$:I
    :cond_40
    move v9, v11

    #@41
    .line 796
    goto :goto_19

    #@42
    .line 805
    .restart local v0       #arr$:[Landroid/renderscript/Element$DataType;
    .restart local v5       #i$:I
    .restart local v6       #len$:I
    :cond_42
    invoke-static {}, Landroid/renderscript/Element$DataKind;->values()[Landroid/renderscript/Element$DataKind;

    #@45
    move-result-object v0

    #@46
    .local v0, arr$:[Landroid/renderscript/Element$DataKind;
    array-length v6, v0

    #@47
    const/4 v5, 0x0

    #@48
    :goto_48
    if-ge v5, v6, :cond_57

    #@4a
    aget-object v2, v0, v5

    #@4c
    .line 806
    .local v2, dk:Landroid/renderscript/Element$DataKind;
    iget v9, v2, Landroid/renderscript/Element$DataKind;->mID:I

    #@4e
    aget v11, v1, v10

    #@50
    if-ne v9, v11, :cond_54

    #@52
    .line 807
    iput-object v2, p0, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@54
    .line 805
    :cond_54
    add-int/lit8 v5, v5, 0x1

    #@56
    goto :goto_48

    #@57
    .line 811
    .end local v2           #dk:Landroid/renderscript/Element$DataKind;
    :cond_57
    const/4 v9, 0x4

    #@58
    aget v7, v1, v9

    #@5a
    .line 812
    .local v7, numSubElements:I
    if-lez v7, :cond_ad

    #@5c
    .line 813
    new-array v9, v7, [Landroid/renderscript/Element;

    #@5e
    iput-object v9, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@60
    .line 814
    new-array v9, v7, [Ljava/lang/String;

    #@62
    iput-object v9, p0, Landroid/renderscript/Element;->mElementNames:[Ljava/lang/String;

    #@64
    .line 815
    new-array v9, v7, [I

    #@66
    iput-object v9, p0, Landroid/renderscript/Element;->mArraySizes:[I

    #@68
    .line 816
    new-array v9, v7, [I

    #@6a
    iput-object v9, p0, Landroid/renderscript/Element;->mOffsetInBytes:[I

    #@6c
    .line 818
    new-array v8, v7, [I

    #@6e
    .line 819
    .local v8, subElementIds:[I
    iget-object v9, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@70
    iget-object v10, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@72
    invoke-virtual {p0, v10}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@75
    move-result v10

    #@76
    iget-object v11, p0, Landroid/renderscript/Element;->mElementNames:[Ljava/lang/String;

    #@78
    iget-object v12, p0, Landroid/renderscript/Element;->mArraySizes:[I

    #@7a
    invoke-virtual {v9, v10, v8, v11, v12}, Landroid/renderscript/RenderScript;->nElementGetSubElements(I[I[Ljava/lang/String;[I)V

    #@7d
    .line 820
    const/4 v4, 0x0

    #@7e
    .local v4, i:I
    :goto_7e
    if-ge v4, v7, :cond_ad

    #@80
    .line 821
    iget-object v9, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@82
    new-instance v10, Landroid/renderscript/Element;

    #@84
    aget v11, v8, v4

    #@86
    iget-object v12, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@88
    invoke-direct {v10, v11, v12}, Landroid/renderscript/Element;-><init>(ILandroid/renderscript/RenderScript;)V

    #@8b
    aput-object v10, v9, v4

    #@8d
    .line 822
    iget-object v9, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@8f
    aget-object v9, v9, v4

    #@91
    invoke-virtual {v9}, Landroid/renderscript/Element;->updateFromNative()V

    #@94
    .line 823
    iget-object v9, p0, Landroid/renderscript/Element;->mOffsetInBytes:[I

    #@96
    iget v10, p0, Landroid/renderscript/Element;->mSize:I

    #@98
    aput v10, v9, v4

    #@9a
    .line 824
    iget v9, p0, Landroid/renderscript/Element;->mSize:I

    #@9c
    iget-object v10, p0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@9e
    aget-object v10, v10, v4

    #@a0
    iget v10, v10, Landroid/renderscript/Element;->mSize:I

    #@a2
    iget-object v11, p0, Landroid/renderscript/Element;->mArraySizes:[I

    #@a4
    aget v11, v11, v4

    #@a6
    mul-int/2addr v10, v11

    #@a7
    add-int/2addr v9, v10

    #@a8
    iput v9, p0, Landroid/renderscript/Element;->mSize:I

    #@aa
    .line 820
    add-int/lit8 v4, v4, 0x1

    #@ac
    goto :goto_7e

    #@ad
    .line 827
    .end local v4           #i:I
    .end local v8           #subElementIds:[I
    :cond_ad
    invoke-direct {p0}, Landroid/renderscript/Element;->updateVisibleSubElements()V

    #@b0
    .line 828
    return-void
.end method
