.class public Landroid/renderscript/Double2;
.super Ljava/lang/Object;
.source "Double2.java"


# instance fields
.field public x:D

.field public y:D


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    return-void
.end method

.method public constructor <init>(DD)V
    .registers 5
    .parameter "initX"
    .parameter "initY"

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    iput-wide p1, p0, Landroid/renderscript/Double2;->x:D

    #@5
    .line 34
    iput-wide p3, p0, Landroid/renderscript/Double2;->y:D

    #@7
    .line 35
    return-void
.end method
