.class public Landroid/renderscript/Mesh$AllocationBuilder;
.super Ljava/lang/Object;
.source "Mesh.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Mesh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AllocationBuilder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Mesh$AllocationBuilder$Entry;
    }
.end annotation


# instance fields
.field mIndexTypes:Ljava/util/Vector;

.field mRS:Landroid/renderscript/RenderScript;

.field mVertexTypeCount:I

.field mVertexTypes:[Landroid/renderscript/Mesh$AllocationBuilder$Entry;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 421
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 422
    iput-object p1, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@5
    .line 423
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypeCount:I

    #@8
    .line 424
    const/16 v0, 0x10

    #@a
    new-array v0, v0, [Landroid/renderscript/Mesh$AllocationBuilder$Entry;

    #@c
    iput-object v0, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypes:[Landroid/renderscript/Mesh$AllocationBuilder$Entry;

    #@e
    .line 425
    new-instance v0, Ljava/util/Vector;

    #@10
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    #@13
    iput-object v0, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mIndexTypes:Ljava/util/Vector;

    #@15
    .line 426
    return-void
.end method


# virtual methods
.method public addIndexSetAllocation(Landroid/renderscript/Allocation;Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;
    .registers 5
    .parameter "a"
    .parameter "p"

    #@0
    .prologue
    .line 477
    new-instance v0, Landroid/renderscript/Mesh$AllocationBuilder$Entry;

    #@2
    invoke-direct {v0, p0}, Landroid/renderscript/Mesh$AllocationBuilder$Entry;-><init>(Landroid/renderscript/Mesh$AllocationBuilder;)V

    #@5
    .line 478
    .local v0, indexType:Landroid/renderscript/Mesh$AllocationBuilder$Entry;
    iput-object p1, v0, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->a:Landroid/renderscript/Allocation;

    #@7
    .line 479
    iput-object p2, v0, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->prim:Landroid/renderscript/Mesh$Primitive;

    #@9
    .line 480
    iget-object v1, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mIndexTypes:Ljava/util/Vector;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    #@e
    .line 481
    return-object p0
.end method

.method public addIndexSetType(Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;
    .registers 4
    .parameter "p"

    #@0
    .prologue
    .line 493
    new-instance v0, Landroid/renderscript/Mesh$AllocationBuilder$Entry;

    #@2
    invoke-direct {v0, p0}, Landroid/renderscript/Mesh$AllocationBuilder$Entry;-><init>(Landroid/renderscript/Mesh$AllocationBuilder;)V

    #@5
    .line 494
    .local v0, indexType:Landroid/renderscript/Mesh$AllocationBuilder$Entry;
    const/4 v1, 0x0

    #@6
    iput-object v1, v0, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->a:Landroid/renderscript/Allocation;

    #@8
    .line 495
    iput-object p1, v0, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->prim:Landroid/renderscript/Mesh$Primitive;

    #@a
    .line 496
    iget-object v1, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mIndexTypes:Ljava/util/Vector;

    #@c
    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    #@f
    .line 497
    return-object p0
.end method

.method public addVertexAllocation(Landroid/renderscript/Allocation;)Landroid/renderscript/Mesh$AllocationBuilder;
    .registers 5
    .parameter "a"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 456
    iget v0, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypeCount:I

    #@2
    iget-object v1, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypes:[Landroid/renderscript/Mesh$AllocationBuilder$Entry;

    #@4
    array-length v1, v1

    #@5
    if-lt v0, v1, :cond_f

    #@7
    .line 457
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v1, "Max vertex types exceeded."

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 460
    :cond_f
    iget-object v0, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypes:[Landroid/renderscript/Mesh$AllocationBuilder$Entry;

    #@11
    iget v1, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypeCount:I

    #@13
    new-instance v2, Landroid/renderscript/Mesh$AllocationBuilder$Entry;

    #@15
    invoke-direct {v2, p0}, Landroid/renderscript/Mesh$AllocationBuilder$Entry;-><init>(Landroid/renderscript/Mesh$AllocationBuilder;)V

    #@18
    aput-object v2, v0, v1

    #@1a
    .line 461
    iget-object v0, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypes:[Landroid/renderscript/Mesh$AllocationBuilder$Entry;

    #@1c
    iget v1, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypeCount:I

    #@1e
    aget-object v0, v0, v1

    #@20
    iput-object p1, v0, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->a:Landroid/renderscript/Allocation;

    #@22
    .line 462
    iget v0, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypeCount:I

    #@24
    add-int/lit8 v0, v0, 0x1

    #@26
    iput v0, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypeCount:I

    #@28
    .line 463
    return-object p0
.end method

.method public create()Landroid/renderscript/Mesh;
    .registers 14

    #@0
    .prologue
    .line 506
    iget-object v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v11}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 508
    iget v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypeCount:I

    #@7
    new-array v10, v11, [I

    #@9
    .line 509
    .local v10, vtx:[I
    iget-object v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mIndexTypes:Ljava/util/Vector;

    #@b
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    #@e
    move-result v11

    #@f
    new-array v4, v11, [I

    #@11
    .line 510
    .local v4, idx:[I
    iget-object v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mIndexTypes:Ljava/util/Vector;

    #@13
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    #@16
    move-result v11

    #@17
    new-array v7, v11, [I

    #@19
    .line 512
    .local v7, prim:[I
    iget-object v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mIndexTypes:Ljava/util/Vector;

    #@1b
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    #@1e
    move-result v11

    #@1f
    new-array v5, v11, [Landroid/renderscript/Allocation;

    #@21
    .line 513
    .local v5, indexBuffers:[Landroid/renderscript/Allocation;
    iget-object v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mIndexTypes:Ljava/util/Vector;

    #@23
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    #@26
    move-result v11

    #@27
    new-array v8, v11, [Landroid/renderscript/Mesh$Primitive;

    #@29
    .line 514
    .local v8, primitives:[Landroid/renderscript/Mesh$Primitive;
    iget v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypeCount:I

    #@2b
    new-array v9, v11, [Landroid/renderscript/Allocation;

    #@2d
    .line 516
    .local v9, vertexBuffers:[Landroid/renderscript/Allocation;
    const/4 v1, 0x0

    #@2e
    .local v1, ct:I
    :goto_2e
    iget v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypeCount:I

    #@30
    if-ge v1, v11, :cond_47

    #@32
    .line 517
    iget-object v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypes:[Landroid/renderscript/Mesh$AllocationBuilder$Entry;

    #@34
    aget-object v2, v11, v1

    #@36
    .line 518
    .local v2, entry:Landroid/renderscript/Mesh$AllocationBuilder$Entry;
    iget-object v11, v2, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->a:Landroid/renderscript/Allocation;

    #@38
    aput-object v11, v9, v1

    #@3a
    .line 519
    iget-object v11, v2, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->a:Landroid/renderscript/Allocation;

    #@3c
    iget-object v12, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@3e
    invoke-virtual {v11, v12}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@41
    move-result v11

    #@42
    aput v11, v10, v1

    #@44
    .line 516
    add-int/lit8 v1, v1, 0x1

    #@46
    goto :goto_2e

    #@47
    .line 522
    .end local v2           #entry:Landroid/renderscript/Mesh$AllocationBuilder$Entry;
    :cond_47
    const/4 v1, 0x0

    #@48
    :goto_48
    iget-object v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mIndexTypes:Ljava/util/Vector;

    #@4a
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    #@4d
    move-result v11

    #@4e
    if-ge v1, v11, :cond_79

    #@50
    .line 523
    iget-object v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mIndexTypes:Ljava/util/Vector;

    #@52
    invoke-virtual {v11, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    #@55
    move-result-object v2

    #@56
    check-cast v2, Landroid/renderscript/Mesh$AllocationBuilder$Entry;

    #@58
    .line 524
    .restart local v2       #entry:Landroid/renderscript/Mesh$AllocationBuilder$Entry;
    iget-object v11, v2, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->a:Landroid/renderscript/Allocation;

    #@5a
    if-nez v11, :cond_70

    #@5c
    const/4 v0, 0x0

    #@5d
    .line 525
    .local v0, allocID:I
    :goto_5d
    iget-object v11, v2, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->a:Landroid/renderscript/Allocation;

    #@5f
    aput-object v11, v5, v1

    #@61
    .line 526
    iget-object v11, v2, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->prim:Landroid/renderscript/Mesh$Primitive;

    #@63
    aput-object v11, v8, v1

    #@65
    .line 528
    aput v0, v4, v1

    #@67
    .line 529
    iget-object v11, v2, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->prim:Landroid/renderscript/Mesh$Primitive;

    #@69
    iget v11, v11, Landroid/renderscript/Mesh$Primitive;->mID:I

    #@6b
    aput v11, v7, v1

    #@6d
    .line 522
    add-int/lit8 v1, v1, 0x1

    #@6f
    goto :goto_48

    #@70
    .line 524
    .end local v0           #allocID:I
    :cond_70
    iget-object v11, v2, Landroid/renderscript/Mesh$AllocationBuilder$Entry;->a:Landroid/renderscript/Allocation;

    #@72
    iget-object v12, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@74
    invoke-virtual {v11, v12}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@77
    move-result v0

    #@78
    goto :goto_5d

    #@79
    .line 532
    .end local v2           #entry:Landroid/renderscript/Mesh$AllocationBuilder$Entry;
    :cond_79
    iget-object v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@7b
    invoke-virtual {v11, v10, v4, v7}, Landroid/renderscript/RenderScript;->nMeshCreate([I[I[I)I

    #@7e
    move-result v3

    #@7f
    .line 533
    .local v3, id:I
    new-instance v6, Landroid/renderscript/Mesh;

    #@81
    iget-object v11, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mRS:Landroid/renderscript/RenderScript;

    #@83
    invoke-direct {v6, v3, v11}, Landroid/renderscript/Mesh;-><init>(ILandroid/renderscript/RenderScript;)V

    #@86
    .line 534
    .local v6, newMesh:Landroid/renderscript/Mesh;
    iput-object v9, v6, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    #@88
    .line 535
    iput-object v5, v6, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    #@8a
    .line 536
    iput-object v8, v6, Landroid/renderscript/Mesh;->mPrimitives:[Landroid/renderscript/Mesh$Primitive;

    #@8c
    .line 538
    return-object v6
.end method

.method public getCurrentIndexSetIndex()I
    .registers 2

    #@0
    .prologue
    .line 443
    iget-object v0, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mIndexTypes:Ljava/util/Vector;

    #@2
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    #@5
    move-result v0

    #@6
    add-int/lit8 v0, v0, -0x1

    #@8
    return v0
.end method

.method public getCurrentVertexTypeIndex()I
    .registers 2

    #@0
    .prologue
    .line 434
    iget v0, p0, Landroid/renderscript/Mesh$AllocationBuilder;->mVertexTypeCount:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    return v0
.end method
