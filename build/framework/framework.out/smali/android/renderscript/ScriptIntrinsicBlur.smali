.class public final Landroid/renderscript/ScriptIntrinsicBlur;
.super Landroid/renderscript/ScriptIntrinsic;
.source "ScriptIntrinsicBlur.java"


# instance fields
.field private mInput:Landroid/renderscript/Allocation;

.field private final mValues:[F


# direct methods
.method private constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 4
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/renderscript/ScriptIntrinsic;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 30
    const/16 v0, 0x9

    #@5
    new-array v0, v0, [F

    #@7
    iput-object v0, p0, Landroid/renderscript/ScriptIntrinsicBlur;->mValues:[F

    #@9
    .line 35
    return-void
.end method

.method public static create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicBlur;
    .registers 6
    .parameter "rs"
    .parameter "e"

    #@0
    .prologue
    .line 49
    invoke-static {p0}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@3
    move-result-object v2

    #@4
    if-eq p1, v2, :cond_e

    #@6
    .line 50
    new-instance v2, Landroid/renderscript/RSIllegalArgumentException;

    #@8
    const-string v3, "Unsuported element type."

    #@a
    invoke-direct {v2, v3}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v2

    #@e
    .line 52
    :cond_e
    const/4 v2, 0x5

    #@f
    invoke-virtual {p1, p0}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@12
    move-result v3

    #@13
    invoke-virtual {p0, v2, v3}, Landroid/renderscript/RenderScript;->nScriptIntrinsicCreate(II)I

    #@16
    move-result v0

    #@17
    .line 53
    .local v0, id:I
    new-instance v1, Landroid/renderscript/ScriptIntrinsicBlur;

    #@19
    invoke-direct {v1, v0, p0}, Landroid/renderscript/ScriptIntrinsicBlur;-><init>(ILandroid/renderscript/RenderScript;)V

    #@1c
    .line 54
    .local v1, sib:Landroid/renderscript/ScriptIntrinsicBlur;
    const/high16 v2, 0x40a0

    #@1e
    invoke-virtual {v1, v2}, Landroid/renderscript/ScriptIntrinsicBlur;->setRadius(F)V

    #@21
    .line 55
    return-object v1
.end method


# virtual methods
.method public forEach(Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "aout"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 91
    const/4 v0, 0x0

    #@2
    invoke-virtual {p0, v0, v1, p1, v1}, Landroid/renderscript/ScriptIntrinsicBlur;->forEach(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;Landroid/renderscript/FieldPacker;)V

    #@5
    .line 92
    return-void
.end method

.method public getFieldID_Input()Landroid/renderscript/Script$FieldID;
    .registers 3

    #@0
    .prologue
    .line 109
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/ScriptIntrinsicBlur;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getKernelID()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 100
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlur;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public setInput(Landroid/renderscript/Allocation;)V
    .registers 3
    .parameter "ain"

    #@0
    .prologue
    .line 65
    iput-object p1, p0, Landroid/renderscript/ScriptIntrinsicBlur;->mInput:Landroid/renderscript/Allocation;

    #@2
    .line 66
    const/4 v0, 0x1

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/ScriptIntrinsicBlur;->setVar(ILandroid/renderscript/BaseObj;)V

    #@6
    .line 67
    return-void
.end method

.method public setRadius(F)V
    .registers 4
    .parameter "radius"

    #@0
    .prologue
    .line 77
    const/4 v0, 0x0

    #@1
    cmpg-float v0, p1, v0

    #@3
    if-lez v0, :cond_b

    #@5
    const/high16 v0, 0x41c8

    #@7
    cmpl-float v0, p1, v0

    #@9
    if-lez v0, :cond_13

    #@b
    .line 78
    :cond_b
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@d
    const-string v1, "Radius out of range (0 < r <= 25)."

    #@f
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 80
    :cond_13
    const/4 v0, 0x0

    #@14
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/ScriptIntrinsicBlur;->setVar(IF)V

    #@17
    .line 81
    return-void
.end method
