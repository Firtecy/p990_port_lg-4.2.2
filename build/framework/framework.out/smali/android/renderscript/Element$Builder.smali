.class public Landroid/renderscript/Element$Builder;
.super Ljava/lang/Object;
.source "Element.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Element;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field mArraySizes:[I

.field mCount:I

.field mElementNames:[Ljava/lang/String;

.field mElements:[Landroid/renderscript/Element;

.field mRS:Landroid/renderscript/RenderScript;

.field mSkipPadding:I


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;)V
    .registers 4
    .parameter "rs"

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 999
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1000
    iput-object p1, p0, Landroid/renderscript/Element$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@7
    .line 1001
    const/4 v0, 0x0

    #@8
    iput v0, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@a
    .line 1002
    new-array v0, v1, [Landroid/renderscript/Element;

    #@c
    iput-object v0, p0, Landroid/renderscript/Element$Builder;->mElements:[Landroid/renderscript/Element;

    #@e
    .line 1003
    new-array v0, v1, [Ljava/lang/String;

    #@10
    iput-object v0, p0, Landroid/renderscript/Element$Builder;->mElementNames:[Ljava/lang/String;

    #@12
    .line 1004
    new-array v0, v1, [I

    #@14
    iput-object v0, p0, Landroid/renderscript/Element$Builder;->mArraySizes:[I

    #@16
    .line 1005
    return-void
.end method


# virtual methods
.method public add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;
    .registers 4
    .parameter "element"
    .parameter "name"

    #@0
    .prologue
    .line 1058
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;I)Landroid/renderscript/Element$Builder;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public add(Landroid/renderscript/Element;Ljava/lang/String;I)Landroid/renderscript/Element$Builder;
    .registers 11
    .parameter "element"
    .parameter "name"
    .parameter "arraySize"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1015
    if-ge p3, v6, :cond_c

    #@4
    .line 1016
    new-instance v3, Landroid/renderscript/RSIllegalArgumentException;

    #@6
    const-string v4, "Array size cannot be less than 1."

    #@8
    invoke-direct {v3, v4}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v3

    #@c
    .line 1020
    :cond_c
    iget v3, p0, Landroid/renderscript/Element$Builder;->mSkipPadding:I

    #@e
    if-eqz v3, :cond_1b

    #@10
    .line 1021
    const-string v3, "#padding_"

    #@12
    invoke-virtual {p2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_1b

    #@18
    .line 1022
    iput v5, p0, Landroid/renderscript/Element$Builder;->mSkipPadding:I

    #@1a
    .line 1048
    :goto_1a
    return-object p0

    #@1b
    .line 1027
    :cond_1b
    iget v3, p1, Landroid/renderscript/Element;->mVectorSize:I

    #@1d
    const/4 v4, 0x3

    #@1e
    if-ne v3, v4, :cond_6f

    #@20
    .line 1028
    iput v6, p0, Landroid/renderscript/Element$Builder;->mSkipPadding:I

    #@22
    .line 1033
    :goto_22
    iget v3, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@24
    iget-object v4, p0, Landroid/renderscript/Element$Builder;->mElements:[Landroid/renderscript/Element;

    #@26
    array-length v4, v4

    #@27
    if-ne v3, v4, :cond_56

    #@29
    .line 1034
    iget v3, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@2b
    add-int/lit8 v3, v3, 0x8

    #@2d
    new-array v1, v3, [Landroid/renderscript/Element;

    #@2f
    .line 1035
    .local v1, e:[Landroid/renderscript/Element;
    iget v3, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@31
    add-int/lit8 v3, v3, 0x8

    #@33
    new-array v2, v3, [Ljava/lang/String;

    #@35
    .line 1036
    .local v2, s:[Ljava/lang/String;
    iget v3, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@37
    add-int/lit8 v3, v3, 0x8

    #@39
    new-array v0, v3, [I

    #@3b
    .line 1037
    .local v0, as:[I
    iget-object v3, p0, Landroid/renderscript/Element$Builder;->mElements:[Landroid/renderscript/Element;

    #@3d
    iget v4, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@3f
    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@42
    .line 1038
    iget-object v3, p0, Landroid/renderscript/Element$Builder;->mElementNames:[Ljava/lang/String;

    #@44
    iget v4, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@46
    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@49
    .line 1039
    iget-object v3, p0, Landroid/renderscript/Element$Builder;->mArraySizes:[I

    #@4b
    iget v4, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@4d
    invoke-static {v3, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@50
    .line 1040
    iput-object v1, p0, Landroid/renderscript/Element$Builder;->mElements:[Landroid/renderscript/Element;

    #@52
    .line 1041
    iput-object v2, p0, Landroid/renderscript/Element$Builder;->mElementNames:[Ljava/lang/String;

    #@54
    .line 1042
    iput-object v0, p0, Landroid/renderscript/Element$Builder;->mArraySizes:[I

    #@56
    .line 1044
    .end local v0           #as:[I
    .end local v1           #e:[Landroid/renderscript/Element;
    .end local v2           #s:[Ljava/lang/String;
    :cond_56
    iget-object v3, p0, Landroid/renderscript/Element$Builder;->mElements:[Landroid/renderscript/Element;

    #@58
    iget v4, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@5a
    aput-object p1, v3, v4

    #@5c
    .line 1045
    iget-object v3, p0, Landroid/renderscript/Element$Builder;->mElementNames:[Ljava/lang/String;

    #@5e
    iget v4, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@60
    aput-object p2, v3, v4

    #@62
    .line 1046
    iget-object v3, p0, Landroid/renderscript/Element$Builder;->mArraySizes:[I

    #@64
    iget v4, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@66
    aput p3, v3, v4

    #@68
    .line 1047
    iget v3, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@6a
    add-int/lit8 v3, v3, 0x1

    #@6c
    iput v3, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@6e
    goto :goto_1a

    #@6f
    .line 1030
    :cond_6f
    iput v5, p0, Landroid/renderscript/Element$Builder;->mSkipPadding:I

    #@71
    goto :goto_22
.end method

.method public create()Landroid/renderscript/Element;
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1068
    iget-object v0, p0, Landroid/renderscript/Element$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@3
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@6
    .line 1069
    iget v0, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@8
    new-array v3, v0, [Landroid/renderscript/Element;

    #@a
    .line 1070
    .local v3, ein:[Landroid/renderscript/Element;
    iget v0, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@c
    new-array v4, v0, [Ljava/lang/String;

    #@e
    .line 1071
    .local v4, sin:[Ljava/lang/String;
    iget v0, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@10
    new-array v5, v0, [I

    #@12
    .line 1072
    .local v5, asin:[I
    iget-object v0, p0, Landroid/renderscript/Element$Builder;->mElements:[Landroid/renderscript/Element;

    #@14
    iget v2, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@16
    invoke-static {v0, v8, v3, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 1073
    iget-object v0, p0, Landroid/renderscript/Element$Builder;->mElementNames:[Ljava/lang/String;

    #@1b
    iget v2, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@1d
    invoke-static {v0, v8, v4, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@20
    .line 1074
    iget-object v0, p0, Landroid/renderscript/Element$Builder;->mArraySizes:[I

    #@22
    iget v2, p0, Landroid/renderscript/Element$Builder;->mCount:I

    #@24
    invoke-static {v0, v8, v5, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@27
    .line 1076
    array-length v0, v3

    #@28
    new-array v7, v0, [I

    #@2a
    .line 1077
    .local v7, ids:[I
    const/4 v6, 0x0

    #@2b
    .local v6, ct:I
    :goto_2b
    array-length v0, v3

    #@2c
    if-ge v6, v0, :cond_3b

    #@2e
    .line 1078
    aget-object v0, v3, v6

    #@30
    iget-object v2, p0, Landroid/renderscript/Element$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@32
    invoke-virtual {v0, v2}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@35
    move-result v0

    #@36
    aput v0, v7, v6

    #@38
    .line 1077
    add-int/lit8 v6, v6, 0x1

    #@3a
    goto :goto_2b

    #@3b
    .line 1080
    :cond_3b
    iget-object v0, p0, Landroid/renderscript/Element$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@3d
    invoke-virtual {v0, v7, v4, v5}, Landroid/renderscript/RenderScript;->nElementCreate2([I[Ljava/lang/String;[I)I

    #@40
    move-result v1

    #@41
    .line 1081
    .local v1, id:I
    new-instance v0, Landroid/renderscript/Element;

    #@43
    iget-object v2, p0, Landroid/renderscript/Element$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@45
    invoke-direct/range {v0 .. v5}, Landroid/renderscript/Element;-><init>(ILandroid/renderscript/RenderScript;[Landroid/renderscript/Element;[Ljava/lang/String;[I)V

    #@48
    return-object v0
.end method
