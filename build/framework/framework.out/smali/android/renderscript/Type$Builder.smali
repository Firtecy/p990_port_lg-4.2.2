.class public Landroid/renderscript/Type$Builder;
.super Ljava/lang/Object;
.source "Type.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Type;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field mDimFaces:Z

.field mDimMipmaps:Z

.field mDimX:I

.field mDimY:I

.field mDimZ:I

.field mElement:Landroid/renderscript/Element;

.field mRS:Landroid/renderscript/RenderScript;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V
    .registers 4
    .parameter "rs"
    .parameter "e"

    #@0
    .prologue
    .line 219
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 205
    const/4 v0, 0x1

    #@4
    iput v0, p0, Landroid/renderscript/Type$Builder;->mDimX:I

    #@6
    .line 220
    invoke-virtual {p2}, Landroid/renderscript/Element;->checkValid()V

    #@9
    .line 221
    iput-object p1, p0, Landroid/renderscript/Type$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@b
    .line 222
    iput-object p2, p0, Landroid/renderscript/Type$Builder;->mElement:Landroid/renderscript/Element;

    #@d
    .line 223
    return-void
.end method


# virtual methods
.method public create()Landroid/renderscript/Type;
    .registers 10

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 264
    iget v0, p0, Landroid/renderscript/Type$Builder;->mDimZ:I

    #@3
    if-lez v0, :cond_21

    #@5
    .line 265
    iget v0, p0, Landroid/renderscript/Type$Builder;->mDimX:I

    #@7
    if-lt v0, v1, :cond_d

    #@9
    iget v0, p0, Landroid/renderscript/Type$Builder;->mDimY:I

    #@b
    if-ge v0, v1, :cond_15

    #@d
    .line 266
    :cond_d
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@f
    const-string v1, "Both X and Y dimension required when Z is present."

    #@11
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 268
    :cond_15
    iget-boolean v0, p0, Landroid/renderscript/Type$Builder;->mDimFaces:Z

    #@17
    if-eqz v0, :cond_21

    #@19
    .line 269
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@1b
    const-string v1, "Cube maps not supported with 3D types."

    #@1d
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 272
    :cond_21
    iget v0, p0, Landroid/renderscript/Type$Builder;->mDimY:I

    #@23
    if-lez v0, :cond_31

    #@25
    .line 273
    iget v0, p0, Landroid/renderscript/Type$Builder;->mDimX:I

    #@27
    if-ge v0, v1, :cond_31

    #@29
    .line 274
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@2b
    const-string v1, "X dimension required when Y is present."

    #@2d
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@30
    throw v0

    #@31
    .line 277
    :cond_31
    iget-boolean v0, p0, Landroid/renderscript/Type$Builder;->mDimFaces:Z

    #@33
    if-eqz v0, :cond_41

    #@35
    .line 278
    iget v0, p0, Landroid/renderscript/Type$Builder;->mDimY:I

    #@37
    if-ge v0, v1, :cond_41

    #@39
    .line 279
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@3b
    const-string v1, "Cube maps require 2D Types."

    #@3d
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@40
    throw v0

    #@41
    .line 283
    :cond_41
    iget-object v0, p0, Landroid/renderscript/Type$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@43
    iget-object v1, p0, Landroid/renderscript/Type$Builder;->mElement:Landroid/renderscript/Element;

    #@45
    iget-object v2, p0, Landroid/renderscript/Type$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@47
    invoke-virtual {v1, v2}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@4a
    move-result v1

    #@4b
    iget v2, p0, Landroid/renderscript/Type$Builder;->mDimX:I

    #@4d
    iget v3, p0, Landroid/renderscript/Type$Builder;->mDimY:I

    #@4f
    iget v4, p0, Landroid/renderscript/Type$Builder;->mDimZ:I

    #@51
    iget-boolean v5, p0, Landroid/renderscript/Type$Builder;->mDimMipmaps:Z

    #@53
    iget-boolean v6, p0, Landroid/renderscript/Type$Builder;->mDimFaces:Z

    #@55
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/RenderScript;->nTypeCreate(IIIIZZ)I

    #@58
    move-result v7

    #@59
    .line 285
    .local v7, id:I
    new-instance v8, Landroid/renderscript/Type;

    #@5b
    iget-object v0, p0, Landroid/renderscript/Type$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@5d
    invoke-direct {v8, v7, v0}, Landroid/renderscript/Type;-><init>(ILandroid/renderscript/RenderScript;)V

    #@60
    .line 286
    .local v8, t:Landroid/renderscript/Type;
    iget-object v0, p0, Landroid/renderscript/Type$Builder;->mElement:Landroid/renderscript/Element;

    #@62
    iput-object v0, v8, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@64
    .line 287
    iget v0, p0, Landroid/renderscript/Type$Builder;->mDimX:I

    #@66
    iput v0, v8, Landroid/renderscript/Type;->mDimX:I

    #@68
    .line 288
    iget v0, p0, Landroid/renderscript/Type$Builder;->mDimY:I

    #@6a
    iput v0, v8, Landroid/renderscript/Type;->mDimY:I

    #@6c
    .line 289
    iget v0, p0, Landroid/renderscript/Type$Builder;->mDimZ:I

    #@6e
    iput v0, v8, Landroid/renderscript/Type;->mDimZ:I

    #@70
    .line 290
    iget-boolean v0, p0, Landroid/renderscript/Type$Builder;->mDimMipmaps:Z

    #@72
    iput-boolean v0, v8, Landroid/renderscript/Type;->mDimMipmaps:Z

    #@74
    .line 291
    iget-boolean v0, p0, Landroid/renderscript/Type$Builder;->mDimFaces:Z

    #@76
    iput-boolean v0, v8, Landroid/renderscript/Type;->mDimFaces:Z

    #@78
    .line 293
    invoke-virtual {v8}, Landroid/renderscript/Type;->calcElementCount()V

    #@7b
    .line 294
    return-object v8
.end method

.method public setFaces(Z)Landroid/renderscript/Type$Builder;
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 253
    iput-boolean p1, p0, Landroid/renderscript/Type$Builder;->mDimFaces:Z

    #@2
    .line 254
    return-object p0
.end method

.method public setMipmaps(Z)Landroid/renderscript/Type$Builder;
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 248
    iput-boolean p1, p0, Landroid/renderscript/Type$Builder;->mDimMipmaps:Z

    #@2
    .line 249
    return-object p0
.end method

.method public setX(I)Landroid/renderscript/Type$Builder;
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 232
    const/4 v0, 0x1

    #@1
    if-ge p1, v0, :cond_b

    #@3
    .line 233
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@5
    const-string v1, "Values of less than 1 for Dimension X are not valid."

    #@7
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 235
    :cond_b
    iput p1, p0, Landroid/renderscript/Type$Builder;->mDimX:I

    #@d
    .line 236
    return-object p0
.end method

.method public setY(I)Landroid/renderscript/Type$Builder;
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 240
    const/4 v0, 0x1

    #@1
    if-ge p1, v0, :cond_b

    #@3
    .line 241
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@5
    const-string v1, "Values of less than 1 for Dimension Y are not valid."

    #@7
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 243
    :cond_b
    iput p1, p0, Landroid/renderscript/Type$Builder;->mDimY:I

    #@d
    .line 244
    return-object p0
.end method
