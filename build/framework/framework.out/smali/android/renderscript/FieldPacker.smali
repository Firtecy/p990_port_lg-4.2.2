.class public Landroid/renderscript/FieldPacker;
.super Ljava/lang/Object;
.source "FieldPacker.java"


# instance fields
.field private final mData:[B

.field private mLen:I

.field private mPos:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "len"

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 27
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@6
    .line 28
    iput p1, p0, Landroid/renderscript/FieldPacker;->mLen:I

    #@8
    .line 29
    new-array v0, p1, [B

    #@a
    iput-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@c
    .line 30
    return-void
.end method


# virtual methods
.method public addBoolean(Z)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 331
    if-eqz p1, :cond_8

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    int-to-byte v0, v0

    #@4
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI8(B)V

    #@7
    .line 332
    return-void

    #@8
    .line 331
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_3
.end method

.method public addF32(F)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 137
    invoke-static {p1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@7
    .line 138
    return-void
.end method

.method public addF32(Landroid/renderscript/Float2;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 153
    iget v0, p1, Landroid/renderscript/Float2;->x:F

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@5
    .line 154
    iget v0, p1, Landroid/renderscript/Float2;->y:F

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@a
    .line 155
    return-void
.end method

.method public addF32(Landroid/renderscript/Float3;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 157
    iget v0, p1, Landroid/renderscript/Float3;->x:F

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@5
    .line 158
    iget v0, p1, Landroid/renderscript/Float3;->y:F

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@a
    .line 159
    iget v0, p1, Landroid/renderscript/Float3;->z:F

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@f
    .line 160
    return-void
.end method

.method public addF32(Landroid/renderscript/Float4;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 162
    iget v0, p1, Landroid/renderscript/Float4;->x:F

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@5
    .line 163
    iget v0, p1, Landroid/renderscript/Float4;->y:F

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@a
    .line 164
    iget v0, p1, Landroid/renderscript/Float4;->z:F

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@f
    .line 165
    iget v0, p1, Landroid/renderscript/Float4;->w:F

    #@11
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@14
    .line 166
    return-void
.end method

.method public addF64(D)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 141
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    #@3
    move-result-wide v0

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addI64(J)V

    #@7
    .line 142
    return-void
.end method

.method public addF64(Landroid/renderscript/Double2;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 169
    iget-wide v0, p1, Landroid/renderscript/Double2;->x:D

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addF64(D)V

    #@5
    .line 170
    iget-wide v0, p1, Landroid/renderscript/Double2;->y:D

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addF64(D)V

    #@a
    .line 171
    return-void
.end method

.method public addF64(Landroid/renderscript/Double3;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 173
    iget-wide v0, p1, Landroid/renderscript/Double3;->x:D

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addF64(D)V

    #@5
    .line 174
    iget-wide v0, p1, Landroid/renderscript/Double3;->y:D

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addF64(D)V

    #@a
    .line 175
    iget-wide v0, p1, Landroid/renderscript/Double3;->z:D

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addF64(D)V

    #@f
    .line 176
    return-void
.end method

.method public addF64(Landroid/renderscript/Double4;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 178
    iget-wide v0, p1, Landroid/renderscript/Double4;->x:D

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addF64(D)V

    #@5
    .line 179
    iget-wide v0, p1, Landroid/renderscript/Double4;->y:D

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addF64(D)V

    #@a
    .line 180
    iget-wide v0, p1, Landroid/renderscript/Double4;->z:D

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addF64(D)V

    #@f
    .line 181
    iget-wide v0, p1, Landroid/renderscript/Double4;->w:D

    #@11
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addF64(D)V

    #@14
    .line 182
    return-void
.end method

.method public addI16(Landroid/renderscript/Short2;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 217
    iget-short v0, p1, Landroid/renderscript/Short2;->x:S

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI16(S)V

    #@5
    .line 218
    iget-short v0, p1, Landroid/renderscript/Short2;->y:S

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI16(S)V

    #@a
    .line 219
    return-void
.end method

.method public addI16(Landroid/renderscript/Short3;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 221
    iget-short v0, p1, Landroid/renderscript/Short3;->x:S

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI16(S)V

    #@5
    .line 222
    iget-short v0, p1, Landroid/renderscript/Short3;->y:S

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI16(S)V

    #@a
    .line 223
    iget-short v0, p1, Landroid/renderscript/Short3;->z:S

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI16(S)V

    #@f
    .line 224
    return-void
.end method

.method public addI16(Landroid/renderscript/Short4;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 226
    iget-short v0, p1, Landroid/renderscript/Short4;->x:S

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI16(S)V

    #@5
    .line 227
    iget-short v0, p1, Landroid/renderscript/Short4;->y:S

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI16(S)V

    #@a
    .line 228
    iget-short v0, p1, Landroid/renderscript/Short4;->z:S

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI16(S)V

    #@f
    .line 229
    iget-short v0, p1, Landroid/renderscript/Short4;->w:S

    #@11
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI16(S)V

    #@14
    .line 230
    return-void
.end method

.method public addI16(S)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 65
    const/4 v0, 0x2

    #@1
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->align(I)V

    #@4
    .line 66
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@6
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@8
    add-int/lit8 v2, v1, 0x1

    #@a
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@c
    and-int/lit16 v2, p1, 0xff

    #@e
    int-to-byte v2, v2

    #@f
    aput-byte v2, v0, v1

    #@11
    .line 67
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@13
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@15
    add-int/lit8 v2, v1, 0x1

    #@17
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@19
    shr-int/lit8 v2, p1, 0x8

    #@1b
    int-to-byte v2, v2

    #@1c
    aput-byte v2, v0, v1

    #@1e
    .line 68
    return-void
.end method

.method public addI32(I)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 71
    const/4 v0, 0x4

    #@1
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->align(I)V

    #@4
    .line 72
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@6
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@8
    add-int/lit8 v2, v1, 0x1

    #@a
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@c
    and-int/lit16 v2, p1, 0xff

    #@e
    int-to-byte v2, v2

    #@f
    aput-byte v2, v0, v1

    #@11
    .line 73
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@13
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@15
    add-int/lit8 v2, v1, 0x1

    #@17
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@19
    shr-int/lit8 v2, p1, 0x8

    #@1b
    and-int/lit16 v2, v2, 0xff

    #@1d
    int-to-byte v2, v2

    #@1e
    aput-byte v2, v0, v1

    #@20
    .line 74
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@22
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@24
    add-int/lit8 v2, v1, 0x1

    #@26
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@28
    shr-int/lit8 v2, p1, 0x10

    #@2a
    and-int/lit16 v2, v2, 0xff

    #@2c
    int-to-byte v2, v2

    #@2d
    aput-byte v2, v0, v1

    #@2f
    .line 75
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@31
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@33
    add-int/lit8 v2, v1, 0x1

    #@35
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@37
    shr-int/lit8 v2, p1, 0x18

    #@39
    and-int/lit16 v2, v2, 0xff

    #@3b
    int-to-byte v2, v2

    #@3c
    aput-byte v2, v0, v1

    #@3e
    .line 76
    return-void
.end method

.method public addI32(Landroid/renderscript/Int2;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 249
    iget v0, p1, Landroid/renderscript/Int2;->x:I

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@5
    .line 250
    iget v0, p1, Landroid/renderscript/Int2;->y:I

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@a
    .line 251
    return-void
.end method

.method public addI32(Landroid/renderscript/Int3;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 253
    iget v0, p1, Landroid/renderscript/Int3;->x:I

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@5
    .line 254
    iget v0, p1, Landroid/renderscript/Int3;->y:I

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@a
    .line 255
    iget v0, p1, Landroid/renderscript/Int3;->z:I

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@f
    .line 256
    return-void
.end method

.method public addI32(Landroid/renderscript/Int4;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 258
    iget v0, p1, Landroid/renderscript/Int4;->x:I

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@5
    .line 259
    iget v0, p1, Landroid/renderscript/Int4;->y:I

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@a
    .line 260
    iget v0, p1, Landroid/renderscript/Int4;->z:I

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@f
    .line 261
    iget v0, p1, Landroid/renderscript/Int4;->w:I

    #@11
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@14
    .line 262
    return-void
.end method

.method public addI64(J)V
    .registers 10
    .parameter "v"

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const-wide/16 v4, 0xff

    #@4
    .line 79
    invoke-virtual {p0, v6}, Landroid/renderscript/FieldPacker;->align(I)V

    #@7
    .line 80
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@9
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@b
    add-int/lit8 v2, v1, 0x1

    #@d
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@f
    and-long v2, p1, v4

    #@11
    long-to-int v2, v2

    #@12
    int-to-byte v2, v2

    #@13
    aput-byte v2, v0, v1

    #@15
    .line 81
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@17
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@19
    add-int/lit8 v2, v1, 0x1

    #@1b
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@1d
    shr-long v2, p1, v6

    #@1f
    and-long/2addr v2, v4

    #@20
    long-to-int v2, v2

    #@21
    int-to-byte v2, v2

    #@22
    aput-byte v2, v0, v1

    #@24
    .line 82
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@26
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@28
    add-int/lit8 v2, v1, 0x1

    #@2a
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@2c
    const/16 v2, 0x10

    #@2e
    shr-long v2, p1, v2

    #@30
    and-long/2addr v2, v4

    #@31
    long-to-int v2, v2

    #@32
    int-to-byte v2, v2

    #@33
    aput-byte v2, v0, v1

    #@35
    .line 83
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@37
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@39
    add-int/lit8 v2, v1, 0x1

    #@3b
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@3d
    const/16 v2, 0x18

    #@3f
    shr-long v2, p1, v2

    #@41
    and-long/2addr v2, v4

    #@42
    long-to-int v2, v2

    #@43
    int-to-byte v2, v2

    #@44
    aput-byte v2, v0, v1

    #@46
    .line 84
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@48
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@4a
    add-int/lit8 v2, v1, 0x1

    #@4c
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@4e
    const/16 v2, 0x20

    #@50
    shr-long v2, p1, v2

    #@52
    and-long/2addr v2, v4

    #@53
    long-to-int v2, v2

    #@54
    int-to-byte v2, v2

    #@55
    aput-byte v2, v0, v1

    #@57
    .line 85
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@59
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@5b
    add-int/lit8 v2, v1, 0x1

    #@5d
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@5f
    const/16 v2, 0x28

    #@61
    shr-long v2, p1, v2

    #@63
    and-long/2addr v2, v4

    #@64
    long-to-int v2, v2

    #@65
    int-to-byte v2, v2

    #@66
    aput-byte v2, v0, v1

    #@68
    .line 86
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@6a
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@6c
    add-int/lit8 v2, v1, 0x1

    #@6e
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@70
    const/16 v2, 0x30

    #@72
    shr-long v2, p1, v2

    #@74
    and-long/2addr v2, v4

    #@75
    long-to-int v2, v2

    #@76
    int-to-byte v2, v2

    #@77
    aput-byte v2, v0, v1

    #@79
    .line 87
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@7b
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@7d
    add-int/lit8 v2, v1, 0x1

    #@7f
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@81
    const/16 v2, 0x38

    #@83
    shr-long v2, p1, v2

    #@85
    and-long/2addr v2, v4

    #@86
    long-to-int v2, v2

    #@87
    int-to-byte v2, v2

    #@88
    aput-byte v2, v0, v1

    #@8a
    .line 88
    return-void
.end method

.method public addI64(Landroid/renderscript/Long2;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 281
    iget-wide v0, p1, Landroid/renderscript/Long2;->x:J

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addI64(J)V

    #@5
    .line 282
    iget-wide v0, p1, Landroid/renderscript/Long2;->y:J

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addI64(J)V

    #@a
    .line 283
    return-void
.end method

.method public addI64(Landroid/renderscript/Long3;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 285
    iget-wide v0, p1, Landroid/renderscript/Long3;->x:J

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addI64(J)V

    #@5
    .line 286
    iget-wide v0, p1, Landroid/renderscript/Long3;->y:J

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addI64(J)V

    #@a
    .line 287
    iget-wide v0, p1, Landroid/renderscript/Long3;->z:J

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addI64(J)V

    #@f
    .line 288
    return-void
.end method

.method public addI64(Landroid/renderscript/Long4;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 290
    iget-wide v0, p1, Landroid/renderscript/Long4;->x:J

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addI64(J)V

    #@5
    .line 291
    iget-wide v0, p1, Landroid/renderscript/Long4;->y:J

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addI64(J)V

    #@a
    .line 292
    iget-wide v0, p1, Landroid/renderscript/Long4;->z:J

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addI64(J)V

    #@f
    .line 293
    iget-wide v0, p1, Landroid/renderscript/Long4;->w:J

    #@11
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addI64(J)V

    #@14
    .line 294
    return-void
.end method

.method public addI8(B)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@2
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@4
    add-int/lit8 v2, v1, 0x1

    #@6
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@8
    aput-byte p1, v0, v1

    #@a
    .line 62
    return-void
.end method

.method public addI8(Landroid/renderscript/Byte2;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 185
    iget-byte v0, p1, Landroid/renderscript/Byte2;->x:B

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI8(B)V

    #@5
    .line 186
    iget-byte v0, p1, Landroid/renderscript/Byte2;->y:B

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI8(B)V

    #@a
    .line 187
    return-void
.end method

.method public addI8(Landroid/renderscript/Byte3;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 189
    iget-byte v0, p1, Landroid/renderscript/Byte3;->x:B

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI8(B)V

    #@5
    .line 190
    iget-byte v0, p1, Landroid/renderscript/Byte3;->y:B

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI8(B)V

    #@a
    .line 191
    iget-byte v0, p1, Landroid/renderscript/Byte3;->z:B

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI8(B)V

    #@f
    .line 192
    return-void
.end method

.method public addI8(Landroid/renderscript/Byte4;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 194
    iget-byte v0, p1, Landroid/renderscript/Byte4;->x:B

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI8(B)V

    #@5
    .line 195
    iget-byte v0, p1, Landroid/renderscript/Byte4;->y:B

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI8(B)V

    #@a
    .line 196
    iget-byte v0, p1, Landroid/renderscript/Byte4;->z:B

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI8(B)V

    #@f
    .line 197
    iget-byte v0, p1, Landroid/renderscript/Byte4;->w:B

    #@11
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI8(B)V

    #@14
    .line 198
    return-void
.end method

.method public addMatrix(Landroid/renderscript/Matrix2f;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 325
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p1, Landroid/renderscript/Matrix2f;->mMat:[F

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_10

    #@6
    .line 326
    iget-object v1, p1, Landroid/renderscript/Matrix2f;->mMat:[F

    #@8
    aget v1, v1, v0

    #@a
    invoke-virtual {p0, v1}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@d
    .line 325
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_1

    #@10
    .line 328
    :cond_10
    return-void
.end method

.method public addMatrix(Landroid/renderscript/Matrix3f;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 319
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_10

    #@6
    .line 320
    iget-object v1, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@8
    aget v1, v1, v0

    #@a
    invoke-virtual {p0, v1}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@d
    .line 319
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_1

    #@10
    .line 322
    :cond_10
    return-void
.end method

.method public addMatrix(Landroid/renderscript/Matrix4f;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 313
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p1, Landroid/renderscript/Matrix4f;->mMat:[F

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_10

    #@6
    .line 314
    iget-object v1, p1, Landroid/renderscript/Matrix4f;->mMat:[F

    #@8
    aget v1, v1, v0

    #@a
    invoke-virtual {p0, v1}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@d
    .line 313
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_1

    #@10
    .line 316
    :cond_10
    return-void
.end method

.method public addObj(Landroid/renderscript/BaseObj;)V
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 145
    if-eqz p1, :cond_b

    #@2
    .line 146
    const/4 v0, 0x0

    #@3
    invoke-virtual {p1, v0}, Landroid/renderscript/BaseObj;->getID(Landroid/renderscript/RenderScript;)I

    #@6
    move-result v0

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@a
    .line 150
    :goto_a
    return-void

    #@b
    .line 148
    :cond_b
    const/4 v0, 0x0

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    #@f
    goto :goto_a
.end method

.method public addU16(I)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 99
    if-ltz p1, :cond_7

    #@2
    const v0, 0xffff

    #@5
    if-le p1, v0, :cond_2e

    #@7
    .line 100
    :cond_7
    const-string/jumbo v0, "rs"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "FieldPacker.addU16( "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " )"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@28
    const-string v1, "Saving value out of range for type"

    #@2a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v0

    #@2e
    .line 103
    :cond_2e
    const/4 v0, 0x2

    #@2f
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->align(I)V

    #@32
    .line 104
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@34
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@36
    add-int/lit8 v2, v1, 0x1

    #@38
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@3a
    and-int/lit16 v2, p1, 0xff

    #@3c
    int-to-byte v2, v2

    #@3d
    aput-byte v2, v0, v1

    #@3f
    .line 105
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@41
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@43
    add-int/lit8 v2, v1, 0x1

    #@45
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@47
    shr-int/lit8 v2, p1, 0x8

    #@49
    int-to-byte v2, v2

    #@4a
    aput-byte v2, v0, v1

    #@4c
    .line 106
    return-void
.end method

.method public addU16(Landroid/renderscript/Int2;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 233
    iget v0, p1, Landroid/renderscript/Int2;->x:I

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU16(I)V

    #@5
    .line 234
    iget v0, p1, Landroid/renderscript/Int2;->y:I

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU16(I)V

    #@a
    .line 235
    return-void
.end method

.method public addU16(Landroid/renderscript/Int3;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 237
    iget v0, p1, Landroid/renderscript/Int3;->x:I

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU16(I)V

    #@5
    .line 238
    iget v0, p1, Landroid/renderscript/Int3;->y:I

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU16(I)V

    #@a
    .line 239
    iget v0, p1, Landroid/renderscript/Int3;->z:I

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU16(I)V

    #@f
    .line 240
    return-void
.end method

.method public addU16(Landroid/renderscript/Int4;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 242
    iget v0, p1, Landroid/renderscript/Int4;->x:I

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU16(I)V

    #@5
    .line 243
    iget v0, p1, Landroid/renderscript/Int4;->y:I

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU16(I)V

    #@a
    .line 244
    iget v0, p1, Landroid/renderscript/Int4;->z:I

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU16(I)V

    #@f
    .line 245
    iget v0, p1, Landroid/renderscript/Int4;->w:I

    #@11
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU16(I)V

    #@14
    .line 246
    return-void
.end method

.method public addU32(J)V
    .registers 9
    .parameter "v"

    #@0
    .prologue
    const-wide/16 v4, 0xff

    #@2
    .line 109
    const-wide/16 v0, 0x0

    #@4
    cmp-long v0, p1, v0

    #@6
    if-ltz v0, :cond_11

    #@8
    const-wide v0, 0xffffffffL

    #@d
    cmp-long v0, p1, v0

    #@f
    if-lez v0, :cond_38

    #@11
    .line 110
    :cond_11
    const-string/jumbo v0, "rs"

    #@14
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "FieldPacker.addU32( "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, " )"

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@32
    const-string v1, "Saving value out of range for type"

    #@34
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@37
    throw v0

    #@38
    .line 113
    :cond_38
    const/4 v0, 0x4

    #@39
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->align(I)V

    #@3c
    .line 114
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@3e
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@40
    add-int/lit8 v2, v1, 0x1

    #@42
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@44
    and-long v2, p1, v4

    #@46
    long-to-int v2, v2

    #@47
    int-to-byte v2, v2

    #@48
    aput-byte v2, v0, v1

    #@4a
    .line 115
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@4c
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@4e
    add-int/lit8 v2, v1, 0x1

    #@50
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@52
    const/16 v2, 0x8

    #@54
    shr-long v2, p1, v2

    #@56
    and-long/2addr v2, v4

    #@57
    long-to-int v2, v2

    #@58
    int-to-byte v2, v2

    #@59
    aput-byte v2, v0, v1

    #@5b
    .line 116
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@5d
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@5f
    add-int/lit8 v2, v1, 0x1

    #@61
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@63
    const/16 v2, 0x10

    #@65
    shr-long v2, p1, v2

    #@67
    and-long/2addr v2, v4

    #@68
    long-to-int v2, v2

    #@69
    int-to-byte v2, v2

    #@6a
    aput-byte v2, v0, v1

    #@6c
    .line 117
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@6e
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@70
    add-int/lit8 v2, v1, 0x1

    #@72
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@74
    const/16 v2, 0x18

    #@76
    shr-long v2, p1, v2

    #@78
    and-long/2addr v2, v4

    #@79
    long-to-int v2, v2

    #@7a
    int-to-byte v2, v2

    #@7b
    aput-byte v2, v0, v1

    #@7d
    .line 118
    return-void
.end method

.method public addU32(Landroid/renderscript/Long2;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 265
    iget-wide v0, p1, Landroid/renderscript/Long2;->x:J

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU32(J)V

    #@5
    .line 266
    iget-wide v0, p1, Landroid/renderscript/Long2;->y:J

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU32(J)V

    #@a
    .line 267
    return-void
.end method

.method public addU32(Landroid/renderscript/Long3;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 269
    iget-wide v0, p1, Landroid/renderscript/Long3;->x:J

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU32(J)V

    #@5
    .line 270
    iget-wide v0, p1, Landroid/renderscript/Long3;->y:J

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU32(J)V

    #@a
    .line 271
    iget-wide v0, p1, Landroid/renderscript/Long3;->z:J

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU32(J)V

    #@f
    .line 272
    return-void
.end method

.method public addU32(Landroid/renderscript/Long4;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 274
    iget-wide v0, p1, Landroid/renderscript/Long4;->x:J

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU32(J)V

    #@5
    .line 275
    iget-wide v0, p1, Landroid/renderscript/Long4;->y:J

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU32(J)V

    #@a
    .line 276
    iget-wide v0, p1, Landroid/renderscript/Long4;->z:J

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU32(J)V

    #@f
    .line 277
    iget-wide v0, p1, Landroid/renderscript/Long4;->w:J

    #@11
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU32(J)V

    #@14
    .line 278
    return-void
.end method

.method public addU64(J)V
    .registers 10
    .parameter "v"

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const-wide/16 v4, 0xff

    #@4
    .line 121
    const-wide/16 v0, 0x0

    #@6
    cmp-long v0, p1, v0

    #@8
    if-gez v0, :cond_31

    #@a
    .line 122
    const-string/jumbo v0, "rs"

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "FieldPacker.addU64( "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, " )"

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2b
    const-string v1, "Saving value out of range for type"

    #@2d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@30
    throw v0

    #@31
    .line 125
    :cond_31
    invoke-virtual {p0, v6}, Landroid/renderscript/FieldPacker;->align(I)V

    #@34
    .line 126
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@36
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@38
    add-int/lit8 v2, v1, 0x1

    #@3a
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@3c
    and-long v2, p1, v4

    #@3e
    long-to-int v2, v2

    #@3f
    int-to-byte v2, v2

    #@40
    aput-byte v2, v0, v1

    #@42
    .line 127
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@44
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@46
    add-int/lit8 v2, v1, 0x1

    #@48
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@4a
    shr-long v2, p1, v6

    #@4c
    and-long/2addr v2, v4

    #@4d
    long-to-int v2, v2

    #@4e
    int-to-byte v2, v2

    #@4f
    aput-byte v2, v0, v1

    #@51
    .line 128
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@53
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@55
    add-int/lit8 v2, v1, 0x1

    #@57
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@59
    const/16 v2, 0x10

    #@5b
    shr-long v2, p1, v2

    #@5d
    and-long/2addr v2, v4

    #@5e
    long-to-int v2, v2

    #@5f
    int-to-byte v2, v2

    #@60
    aput-byte v2, v0, v1

    #@62
    .line 129
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@64
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@66
    add-int/lit8 v2, v1, 0x1

    #@68
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@6a
    const/16 v2, 0x18

    #@6c
    shr-long v2, p1, v2

    #@6e
    and-long/2addr v2, v4

    #@6f
    long-to-int v2, v2

    #@70
    int-to-byte v2, v2

    #@71
    aput-byte v2, v0, v1

    #@73
    .line 130
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@75
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@77
    add-int/lit8 v2, v1, 0x1

    #@79
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@7b
    const/16 v2, 0x20

    #@7d
    shr-long v2, p1, v2

    #@7f
    and-long/2addr v2, v4

    #@80
    long-to-int v2, v2

    #@81
    int-to-byte v2, v2

    #@82
    aput-byte v2, v0, v1

    #@84
    .line 131
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@86
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@88
    add-int/lit8 v2, v1, 0x1

    #@8a
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@8c
    const/16 v2, 0x28

    #@8e
    shr-long v2, p1, v2

    #@90
    and-long/2addr v2, v4

    #@91
    long-to-int v2, v2

    #@92
    int-to-byte v2, v2

    #@93
    aput-byte v2, v0, v1

    #@95
    .line 132
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@97
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@99
    add-int/lit8 v2, v1, 0x1

    #@9b
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@9d
    const/16 v2, 0x30

    #@9f
    shr-long v2, p1, v2

    #@a1
    and-long/2addr v2, v4

    #@a2
    long-to-int v2, v2

    #@a3
    int-to-byte v2, v2

    #@a4
    aput-byte v2, v0, v1

    #@a6
    .line 133
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@a8
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@aa
    add-int/lit8 v2, v1, 0x1

    #@ac
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@ae
    const/16 v2, 0x38

    #@b0
    shr-long v2, p1, v2

    #@b2
    and-long/2addr v2, v4

    #@b3
    long-to-int v2, v2

    #@b4
    int-to-byte v2, v2

    #@b5
    aput-byte v2, v0, v1

    #@b7
    .line 134
    return-void
.end method

.method public addU64(Landroid/renderscript/Long2;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 297
    iget-wide v0, p1, Landroid/renderscript/Long2;->x:J

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU64(J)V

    #@5
    .line 298
    iget-wide v0, p1, Landroid/renderscript/Long2;->y:J

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU64(J)V

    #@a
    .line 299
    return-void
.end method

.method public addU64(Landroid/renderscript/Long3;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 301
    iget-wide v0, p1, Landroid/renderscript/Long3;->x:J

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU64(J)V

    #@5
    .line 302
    iget-wide v0, p1, Landroid/renderscript/Long3;->y:J

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU64(J)V

    #@a
    .line 303
    iget-wide v0, p1, Landroid/renderscript/Long3;->z:J

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU64(J)V

    #@f
    .line 304
    return-void
.end method

.method public addU64(Landroid/renderscript/Long4;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 306
    iget-wide v0, p1, Landroid/renderscript/Long4;->x:J

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU64(J)V

    #@5
    .line 307
    iget-wide v0, p1, Landroid/renderscript/Long4;->y:J

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU64(J)V

    #@a
    .line 308
    iget-wide v0, p1, Landroid/renderscript/Long4;->z:J

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU64(J)V

    #@f
    .line 309
    iget-wide v0, p1, Landroid/renderscript/Long4;->w:J

    #@11
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/FieldPacker;->addU64(J)V

    #@14
    .line 310
    return-void
.end method

.method public addU8(Landroid/renderscript/Short2;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 201
    iget-short v0, p1, Landroid/renderscript/Short2;->x:S

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU8(S)V

    #@5
    .line 202
    iget-short v0, p1, Landroid/renderscript/Short2;->y:S

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU8(S)V

    #@a
    .line 203
    return-void
.end method

.method public addU8(Landroid/renderscript/Short3;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 205
    iget-short v0, p1, Landroid/renderscript/Short3;->x:S

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU8(S)V

    #@5
    .line 206
    iget-short v0, p1, Landroid/renderscript/Short3;->y:S

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU8(S)V

    #@a
    .line 207
    iget-short v0, p1, Landroid/renderscript/Short3;->z:S

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU8(S)V

    #@f
    .line 208
    return-void
.end method

.method public addU8(Landroid/renderscript/Short4;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 210
    iget-short v0, p1, Landroid/renderscript/Short4;->x:S

    #@2
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU8(S)V

    #@5
    .line 211
    iget-short v0, p1, Landroid/renderscript/Short4;->y:S

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU8(S)V

    #@a
    .line 212
    iget-short v0, p1, Landroid/renderscript/Short4;->z:S

    #@c
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU8(S)V

    #@f
    .line 213
    iget-short v0, p1, Landroid/renderscript/Short4;->w:S

    #@11
    invoke-virtual {p0, v0}, Landroid/renderscript/FieldPacker;->addU8(S)V

    #@14
    .line 214
    return-void
.end method

.method public addU8(S)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 91
    if-ltz p1, :cond_6

    #@2
    const/16 v0, 0xff

    #@4
    if-le p1, v0, :cond_2d

    #@6
    .line 92
    :cond_6
    const-string/jumbo v0, "rs"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "FieldPacker.addU8( "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " )"

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@27
    const-string v1, "Saving value out of range for type"

    #@29
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 95
    :cond_2d
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@2f
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@31
    add-int/lit8 v2, v1, 0x1

    #@33
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@35
    int-to-byte v2, p1

    #@36
    aput-byte v2, v0, v1

    #@38
    .line 96
    return-void
.end method

.method public align(I)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 33
    if-lez p1, :cond_7

    #@2
    add-int/lit8 v0, p1, -0x1

    #@4
    and-int/2addr v0, p1

    #@5
    if-eqz v0, :cond_20

    #@7
    .line 34
    :cond_7
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "argument must be a non-negative non-zero power of 2: "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 37
    :cond_20
    :goto_20
    iget v0, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@22
    add-int/lit8 v1, p1, -0x1

    #@24
    and-int/2addr v0, v1

    #@25
    if-eqz v0, :cond_33

    #@27
    .line 38
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@29
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@2b
    add-int/lit8 v2, v1, 0x1

    #@2d
    iput v2, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@2f
    const/4 v2, 0x0

    #@30
    aput-byte v2, v0, v1

    #@32
    goto :goto_20

    #@33
    .line 40
    :cond_33
    return-void
.end method

.method public final getData()[B
    .registers 2

    #@0
    .prologue
    .line 335
    iget-object v0, p0, Landroid/renderscript/FieldPacker;->mData:[B

    #@2
    return-object v0
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 43
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@3
    .line 44
    return-void
.end method

.method public reset(I)V
    .registers 5
    .parameter "i"

    #@0
    .prologue
    .line 46
    if-ltz p1, :cond_6

    #@2
    iget v0, p0, Landroid/renderscript/FieldPacker;->mLen:I

    #@4
    if-lt p1, v0, :cond_20

    #@6
    .line 47
    :cond_6
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v2, "out of range argument: "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 49
    :cond_20
    iput p1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@22
    .line 50
    return-void
.end method

.method public skip(I)V
    .registers 6
    .parameter "i"

    #@0
    .prologue
    .line 53
    iget v1, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@2
    add-int v0, v1, p1

    #@4
    .line 54
    .local v0, res:I
    if-ltz v0, :cond_a

    #@6
    iget v1, p0, Landroid/renderscript/FieldPacker;->mLen:I

    #@8
    if-le v0, v1, :cond_24

    #@a
    .line 55
    :cond_a
    new-instance v1, Landroid/renderscript/RSIllegalArgumentException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string/jumbo v3, "out of range argument: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-direct {v1, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@23
    throw v1

    #@24
    .line 57
    :cond_24
    iput v0, p0, Landroid/renderscript/FieldPacker;->mPos:I

    #@26
    .line 58
    return-void
.end method
