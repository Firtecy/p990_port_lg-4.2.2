.class public Landroid/renderscript/Matrix4f;
.super Ljava/lang/Object;
.source "Matrix4f.java"


# instance fields
.field final mMat:[F


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    const/16 v0, 0x10

    #@5
    new-array v0, v0, [F

    #@7
    iput-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@9
    .line 34
    invoke-virtual {p0}, Landroid/renderscript/Matrix4f;->loadIdentity()V

    #@c
    .line 35
    return-void
.end method

.method public constructor <init>([F)V
    .registers 5
    .parameter "dataArray"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 45
    const/16 v0, 0x10

    #@6
    new-array v0, v0, [F

    #@8
    iput-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@a
    .line 46
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@c
    iget-object v1, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@e
    array-length v1, v1

    #@f
    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@12
    .line 47
    return-void
.end method

.method private computeCofactor(II)F
    .registers 17
    .parameter "i"
    .parameter "j"

    #@0
    .prologue
    .line 405
    add-int/lit8 v8, p1, 0x1

    #@2
    rem-int/lit8 v0, v8, 0x4

    #@4
    .line 406
    .local v0, c0:I
    add-int/lit8 v8, p1, 0x2

    #@6
    rem-int/lit8 v1, v8, 0x4

    #@8
    .line 407
    .local v1, c1:I
    add-int/lit8 v8, p1, 0x3

    #@a
    rem-int/lit8 v2, v8, 0x4

    #@c
    .line 408
    .local v2, c2:I
    add-int/lit8 v8, p2, 0x1

    #@e
    rem-int/lit8 v5, v8, 0x4

    #@10
    .line 409
    .local v5, r0:I
    add-int/lit8 v8, p2, 0x2

    #@12
    rem-int/lit8 v6, v8, 0x4

    #@14
    .line 410
    .local v6, r1:I
    add-int/lit8 v8, p2, 0x3

    #@16
    rem-int/lit8 v7, v8, 0x4

    #@18
    .line 412
    .local v7, r2:I
    iget-object v8, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@1a
    mul-int/lit8 v9, v5, 0x4

    #@1c
    add-int/2addr v9, v0

    #@1d
    aget v8, v8, v9

    #@1f
    iget-object v9, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@21
    mul-int/lit8 v10, v6, 0x4

    #@23
    add-int/2addr v10, v1

    #@24
    aget v9, v9, v10

    #@26
    iget-object v10, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@28
    mul-int/lit8 v11, v7, 0x4

    #@2a
    add-int/2addr v11, v2

    #@2b
    aget v10, v10, v11

    #@2d
    mul-float/2addr v9, v10

    #@2e
    iget-object v10, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@30
    mul-int/lit8 v11, v7, 0x4

    #@32
    add-int/2addr v11, v1

    #@33
    aget v10, v10, v11

    #@35
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@37
    mul-int/lit8 v12, v6, 0x4

    #@39
    add-int/2addr v12, v2

    #@3a
    aget v11, v11, v12

    #@3c
    mul-float/2addr v10, v11

    #@3d
    sub-float/2addr v9, v10

    #@3e
    mul-float/2addr v8, v9

    #@3f
    iget-object v9, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@41
    mul-int/lit8 v10, v6, 0x4

    #@43
    add-int/2addr v10, v0

    #@44
    aget v9, v9, v10

    #@46
    iget-object v10, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@48
    mul-int/lit8 v11, v5, 0x4

    #@4a
    add-int/2addr v11, v1

    #@4b
    aget v10, v10, v11

    #@4d
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@4f
    mul-int/lit8 v12, v7, 0x4

    #@51
    add-int/2addr v12, v2

    #@52
    aget v11, v11, v12

    #@54
    mul-float/2addr v10, v11

    #@55
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@57
    mul-int/lit8 v12, v7, 0x4

    #@59
    add-int/2addr v12, v1

    #@5a
    aget v11, v11, v12

    #@5c
    iget-object v12, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@5e
    mul-int/lit8 v13, v5, 0x4

    #@60
    add-int/2addr v13, v2

    #@61
    aget v12, v12, v13

    #@63
    mul-float/2addr v11, v12

    #@64
    sub-float/2addr v10, v11

    #@65
    mul-float/2addr v9, v10

    #@66
    sub-float/2addr v8, v9

    #@67
    iget-object v9, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@69
    mul-int/lit8 v10, v7, 0x4

    #@6b
    add-int/2addr v10, v0

    #@6c
    aget v9, v9, v10

    #@6e
    iget-object v10, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@70
    mul-int/lit8 v11, v5, 0x4

    #@72
    add-int/2addr v11, v1

    #@73
    aget v10, v10, v11

    #@75
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@77
    mul-int/lit8 v12, v6, 0x4

    #@79
    add-int/2addr v12, v2

    #@7a
    aget v11, v11, v12

    #@7c
    mul-float/2addr v10, v11

    #@7d
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@7f
    mul-int/lit8 v12, v6, 0x4

    #@81
    add-int/2addr v12, v1

    #@82
    aget v11, v11, v12

    #@84
    iget-object v12, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@86
    mul-int/lit8 v13, v5, 0x4

    #@88
    add-int/2addr v13, v2

    #@89
    aget v12, v12, v13

    #@8b
    mul-float/2addr v11, v12

    #@8c
    sub-float/2addr v10, v11

    #@8d
    mul-float/2addr v9, v10

    #@8e
    add-float v4, v8, v9

    #@90
    .line 419
    .local v4, minor:F
    add-int v8, p1, p2

    #@92
    and-int/lit8 v8, v8, 0x1

    #@94
    if-eqz v8, :cond_98

    #@96
    neg-float v3, v4

    #@97
    .line 420
    .local v3, cofactor:F
    :goto_97
    return v3

    #@98
    .end local v3           #cofactor:F
    :cond_98
    move v3, v4

    #@99
    .line 419
    goto :goto_97
.end method


# virtual methods
.method public get(II)F
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@2
    mul-int/lit8 v1, p1, 0x4

    #@4
    add-int/2addr v1, p2

    #@5
    aget v0, v0, v1

    #@7
    return v0
.end method

.method public getArray()[F
    .registers 2

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@2
    return-object v0
.end method

.method public inverse()Z
    .registers 11

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v9, 0x4

    #@2
    const/4 v4, 0x0

    #@3
    .line 428
    new-instance v3, Landroid/renderscript/Matrix4f;

    #@5
    invoke-direct {v3}, Landroid/renderscript/Matrix4f;-><init>()V

    #@8
    .line 430
    .local v3, result:Landroid/renderscript/Matrix4f;
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v9, :cond_1f

    #@b
    .line 431
    const/4 v2, 0x0

    #@c
    .local v2, j:I
    :goto_c
    if-ge v2, v9, :cond_1c

    #@e
    .line 432
    iget-object v6, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@10
    mul-int/lit8 v7, v1, 0x4

    #@12
    add-int/2addr v7, v2

    #@13
    invoke-direct {p0, v1, v2}, Landroid/renderscript/Matrix4f;->computeCofactor(II)F

    #@16
    move-result v8

    #@17
    aput v8, v6, v7

    #@19
    .line 431
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_c

    #@1c
    .line 430
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_9

    #@1f
    .line 437
    .end local v2           #j:I
    :cond_1f
    iget-object v6, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@21
    aget v6, v6, v4

    #@23
    iget-object v7, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@25
    aget v7, v7, v4

    #@27
    mul-float/2addr v6, v7

    #@28
    iget-object v7, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@2a
    aget v7, v7, v9

    #@2c
    iget-object v8, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@2e
    aget v8, v8, v5

    #@30
    mul-float/2addr v7, v8

    #@31
    add-float/2addr v6, v7

    #@32
    iget-object v7, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@34
    const/16 v8, 0x8

    #@36
    aget v7, v7, v8

    #@38
    iget-object v8, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@3a
    const/4 v9, 0x2

    #@3b
    aget v8, v8, v9

    #@3d
    mul-float/2addr v7, v8

    #@3e
    add-float/2addr v6, v7

    #@3f
    iget-object v7, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@41
    const/16 v8, 0xc

    #@43
    aget v7, v7, v8

    #@45
    iget-object v8, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@47
    const/4 v9, 0x3

    #@48
    aget v8, v8, v9

    #@4a
    mul-float/2addr v7, v8

    #@4b
    add-float v0, v6, v7

    #@4d
    .line 440
    .local v0, det:F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@50
    move-result v6

    #@51
    float-to-double v6, v6

    #@52
    const-wide v8, 0x3eb0c6f7a0b5ed8dL

    #@57
    cmpg-double v6, v6, v8

    #@59
    if-gez v6, :cond_5c

    #@5b
    .line 449
    :goto_5b
    return v4

    #@5c
    .line 444
    :cond_5c
    const/high16 v4, 0x3f80

    #@5e
    div-float v0, v4, v0

    #@60
    .line 445
    const/4 v1, 0x0

    #@61
    :goto_61
    const/16 v4, 0x10

    #@63
    if-ge v1, v4, :cond_71

    #@65
    .line 446
    iget-object v4, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@67
    iget-object v6, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@69
    aget v6, v6, v1

    #@6b
    mul-float/2addr v6, v0

    #@6c
    aput v6, v4, v1

    #@6e
    .line 445
    add-int/lit8 v1, v1, 0x1

    #@70
    goto :goto_61

    #@71
    :cond_71
    move v4, v5

    #@72
    .line 449
    goto :goto_5b
.end method

.method public inverseTranspose()Z
    .registers 12

    #@0
    .prologue
    const/16 v10, 0xc

    #@2
    const/16 v9, 0x8

    #@4
    const/4 v4, 0x0

    #@5
    const/4 v8, 0x4

    #@6
    .line 457
    new-instance v3, Landroid/renderscript/Matrix4f;

    #@8
    invoke-direct {v3}, Landroid/renderscript/Matrix4f;-><init>()V

    #@b
    .line 459
    .local v3, result:Landroid/renderscript/Matrix4f;
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v8, :cond_22

    #@e
    .line 460
    const/4 v2, 0x0

    #@f
    .local v2, j:I
    :goto_f
    if-ge v2, v8, :cond_1f

    #@11
    .line 461
    iget-object v5, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@13
    mul-int/lit8 v6, v2, 0x4

    #@15
    add-int/2addr v6, v1

    #@16
    invoke-direct {p0, v1, v2}, Landroid/renderscript/Matrix4f;->computeCofactor(II)F

    #@19
    move-result v7

    #@1a
    aput v7, v5, v6

    #@1c
    .line 460
    add-int/lit8 v2, v2, 0x1

    #@1e
    goto :goto_f

    #@1f
    .line 459
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_c

    #@22
    .line 465
    .end local v2           #j:I
    :cond_22
    iget-object v5, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@24
    aget v5, v5, v4

    #@26
    iget-object v6, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@28
    aget v6, v6, v4

    #@2a
    mul-float/2addr v5, v6

    #@2b
    iget-object v6, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@2d
    aget v6, v6, v8

    #@2f
    iget-object v7, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@31
    aget v7, v7, v8

    #@33
    mul-float/2addr v6, v7

    #@34
    add-float/2addr v5, v6

    #@35
    iget-object v6, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@37
    aget v6, v6, v9

    #@39
    iget-object v7, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@3b
    aget v7, v7, v9

    #@3d
    mul-float/2addr v6, v7

    #@3e
    add-float/2addr v5, v6

    #@3f
    iget-object v6, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@41
    aget v6, v6, v10

    #@43
    iget-object v7, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@45
    aget v7, v7, v10

    #@47
    mul-float/2addr v6, v7

    #@48
    add-float v0, v5, v6

    #@4a
    .line 468
    .local v0, det:F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@4d
    move-result v5

    #@4e
    float-to-double v5, v5

    #@4f
    const-wide v7, 0x3eb0c6f7a0b5ed8dL

    #@54
    cmpg-double v5, v5, v7

    #@56
    if-gez v5, :cond_59

    #@58
    .line 477
    :goto_58
    return v4

    #@59
    .line 472
    :cond_59
    const/high16 v4, 0x3f80

    #@5b
    div-float v0, v4, v0

    #@5d
    .line 473
    const/4 v1, 0x0

    #@5e
    :goto_5e
    const/16 v4, 0x10

    #@60
    if-ge v1, v4, :cond_6e

    #@62
    .line 474
    iget-object v4, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@64
    iget-object v5, v3, Landroid/renderscript/Matrix4f;->mMat:[F

    #@66
    aget v5, v5, v1

    #@68
    mul-float/2addr v5, v0

    #@69
    aput v5, v4, v1

    #@6b
    .line 473
    add-int/lit8 v1, v1, 0x1

    #@6d
    goto :goto_5e

    #@6e
    .line 477
    :cond_6e
    const/4 v4, 0x1

    #@6f
    goto :goto_58
.end method

.method public load(Landroid/renderscript/Matrix3f;)V
    .registers 9
    .parameter "src"

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v4, 0x0

    #@5
    .line 122
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@7
    iget-object v1, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@9
    aget v1, v1, v2

    #@b
    aput v1, v0, v2

    #@d
    .line 123
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@f
    iget-object v1, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@11
    aget v1, v1, v3

    #@13
    aput v1, v0, v3

    #@15
    .line 124
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@17
    iget-object v1, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@19
    aget v1, v1, v5

    #@1b
    aput v1, v0, v5

    #@1d
    .line 125
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@1f
    aput v4, v0, v6

    #@21
    .line 127
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@23
    const/4 v1, 0x4

    #@24
    iget-object v2, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@26
    aget v2, v2, v6

    #@28
    aput v2, v0, v1

    #@2a
    .line 128
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@2c
    const/4 v1, 0x5

    #@2d
    iget-object v2, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@2f
    const/4 v3, 0x4

    #@30
    aget v2, v2, v3

    #@32
    aput v2, v0, v1

    #@34
    .line 129
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@36
    const/4 v1, 0x6

    #@37
    iget-object v2, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@39
    const/4 v3, 0x5

    #@3a
    aget v2, v2, v3

    #@3c
    aput v2, v0, v1

    #@3e
    .line 130
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@40
    const/4 v1, 0x7

    #@41
    aput v4, v0, v1

    #@43
    .line 132
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@45
    const/16 v1, 0x8

    #@47
    iget-object v2, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@49
    const/4 v3, 0x6

    #@4a
    aget v2, v2, v3

    #@4c
    aput v2, v0, v1

    #@4e
    .line 133
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@50
    const/16 v1, 0x9

    #@52
    iget-object v2, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@54
    const/4 v3, 0x7

    #@55
    aget v2, v2, v3

    #@57
    aput v2, v0, v1

    #@59
    .line 134
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@5b
    const/16 v1, 0xa

    #@5d
    iget-object v2, p1, Landroid/renderscript/Matrix3f;->mMat:[F

    #@5f
    const/16 v3, 0x8

    #@61
    aget v2, v2, v3

    #@63
    aput v2, v0, v1

    #@65
    .line 135
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@67
    const/16 v1, 0xb

    #@69
    aput v4, v0, v1

    #@6b
    .line 137
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@6d
    const/16 v1, 0xc

    #@6f
    aput v4, v0, v1

    #@71
    .line 138
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@73
    const/16 v1, 0xd

    #@75
    aput v4, v0, v1

    #@77
    .line 139
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@79
    const/16 v1, 0xe

    #@7b
    aput v4, v0, v1

    #@7d
    .line 140
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@7f
    const/16 v1, 0xf

    #@81
    const/high16 v2, 0x3f80

    #@83
    aput v2, v0, v1

    #@85
    .line 141
    return-void
.end method

.method public load(Landroid/renderscript/Matrix4f;)V
    .registers 6
    .parameter "src"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 112
    invoke-virtual {p1}, Landroid/renderscript/Matrix4f;->getArray()[F

    #@4
    move-result-object v0

    #@5
    iget-object v1, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@7
    iget-object v2, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@9
    array-length v2, v2

    #@a
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@d
    .line 113
    return-void
.end method

.method public loadFrustum(FFFFFF)V
    .registers 12
    .parameter "l"
    .parameter "r"
    .parameter "b"
    .parameter "t"
    .parameter "n"
    .parameter "f"

    #@0
    .prologue
    const/high16 v4, 0x4000

    #@2
    .line 292
    invoke-virtual {p0}, Landroid/renderscript/Matrix4f;->loadIdentity()V

    #@5
    .line 293
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@7
    const/4 v1, 0x0

    #@8
    mul-float v2, v4, p5

    #@a
    sub-float v3, p2, p1

    #@c
    div-float/2addr v2, v3

    #@d
    aput v2, v0, v1

    #@f
    .line 294
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@11
    const/4 v1, 0x5

    #@12
    mul-float v2, v4, p5

    #@14
    sub-float v3, p4, p3

    #@16
    div-float/2addr v2, v3

    #@17
    aput v2, v0, v1

    #@19
    .line 295
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@1b
    const/16 v1, 0x8

    #@1d
    add-float v2, p2, p1

    #@1f
    sub-float v3, p2, p1

    #@21
    div-float/2addr v2, v3

    #@22
    aput v2, v0, v1

    #@24
    .line 296
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@26
    const/16 v1, 0x9

    #@28
    add-float v2, p4, p3

    #@2a
    sub-float v3, p4, p3

    #@2c
    div-float/2addr v2, v3

    #@2d
    aput v2, v0, v1

    #@2f
    .line 297
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@31
    const/16 v1, 0xa

    #@33
    add-float v2, p6, p5

    #@35
    neg-float v2, v2

    #@36
    sub-float v3, p6, p5

    #@38
    div-float/2addr v2, v3

    #@39
    aput v2, v0, v1

    #@3b
    .line 298
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@3d
    const/16 v1, 0xb

    #@3f
    const/high16 v2, -0x4080

    #@41
    aput v2, v0, v1

    #@43
    .line 299
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@45
    const/16 v1, 0xe

    #@47
    const/high16 v2, -0x4000

    #@49
    mul-float/2addr v2, p6

    #@4a
    mul-float/2addr v2, p5

    #@4b
    sub-float v3, p6, p5

    #@4d
    div-float/2addr v2, v3

    #@4e
    aput v2, v0, v1

    #@50
    .line 300
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@52
    const/16 v1, 0xf

    #@54
    const/4 v2, 0x0

    #@55
    aput v2, v0, v1

    #@57
    .line 301
    return-void
.end method

.method public loadIdentity()V
    .registers 5

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    const/4 v2, 0x0

    #@3
    .line 85
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@5
    const/4 v1, 0x0

    #@6
    aput v3, v0, v1

    #@8
    .line 86
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@a
    const/4 v1, 0x1

    #@b
    aput v2, v0, v1

    #@d
    .line 87
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@f
    const/4 v1, 0x2

    #@10
    aput v2, v0, v1

    #@12
    .line 88
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@14
    const/4 v1, 0x3

    #@15
    aput v2, v0, v1

    #@17
    .line 90
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@19
    const/4 v1, 0x4

    #@1a
    aput v2, v0, v1

    #@1c
    .line 91
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@1e
    const/4 v1, 0x5

    #@1f
    aput v3, v0, v1

    #@21
    .line 92
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@23
    const/4 v1, 0x6

    #@24
    aput v2, v0, v1

    #@26
    .line 93
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@28
    const/4 v1, 0x7

    #@29
    aput v2, v0, v1

    #@2b
    .line 95
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@2d
    const/16 v1, 0x8

    #@2f
    aput v2, v0, v1

    #@31
    .line 96
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@33
    const/16 v1, 0x9

    #@35
    aput v2, v0, v1

    #@37
    .line 97
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@39
    const/16 v1, 0xa

    #@3b
    aput v3, v0, v1

    #@3d
    .line 98
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@3f
    const/16 v1, 0xb

    #@41
    aput v2, v0, v1

    #@43
    .line 100
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@45
    const/16 v1, 0xc

    #@47
    aput v2, v0, v1

    #@49
    .line 101
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@4b
    const/16 v1, 0xd

    #@4d
    aput v2, v0, v1

    #@4f
    .line 102
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@51
    const/16 v1, 0xe

    #@53
    aput v2, v0, v1

    #@55
    .line 103
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@57
    const/16 v1, 0xf

    #@59
    aput v3, v0, v1

    #@5b
    .line 104
    return-void
.end method

.method public loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V
    .registers 16
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    const/4 v12, 0x4

    #@1
    const/4 v11, 0x3

    #@2
    const/4 v10, 0x2

    #@3
    const/4 v9, 0x1

    #@4
    const/4 v8, 0x0

    #@5
    .line 227
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    if-ge v0, v12, :cond_3d

    #@8
    .line 228
    const/4 v3, 0x0

    #@9
    .line 229
    .local v3, ri0:F
    const/4 v4, 0x0

    #@a
    .line 230
    .local v4, ri1:F
    const/4 v5, 0x0

    #@b
    .line 231
    .local v5, ri2:F
    const/4 v6, 0x0

    #@c
    .line 232
    .local v6, ri3:F
    const/4 v1, 0x0

    #@d
    .local v1, j:I
    :goto_d
    if-ge v1, v12, :cond_2e

    #@f
    .line 233
    invoke-virtual {p2, v0, v1}, Landroid/renderscript/Matrix4f;->get(II)F

    #@12
    move-result v2

    #@13
    .line 234
    .local v2, rhs_ij:F
    invoke-virtual {p1, v1, v8}, Landroid/renderscript/Matrix4f;->get(II)F

    #@16
    move-result v7

    #@17
    mul-float/2addr v7, v2

    #@18
    add-float/2addr v3, v7

    #@19
    .line 235
    invoke-virtual {p1, v1, v9}, Landroid/renderscript/Matrix4f;->get(II)F

    #@1c
    move-result v7

    #@1d
    mul-float/2addr v7, v2

    #@1e
    add-float/2addr v4, v7

    #@1f
    .line 236
    invoke-virtual {p1, v1, v10}, Landroid/renderscript/Matrix4f;->get(II)F

    #@22
    move-result v7

    #@23
    mul-float/2addr v7, v2

    #@24
    add-float/2addr v5, v7

    #@25
    .line 237
    invoke-virtual {p1, v1, v11}, Landroid/renderscript/Matrix4f;->get(II)F

    #@28
    move-result v7

    #@29
    mul-float/2addr v7, v2

    #@2a
    add-float/2addr v6, v7

    #@2b
    .line 232
    add-int/lit8 v1, v1, 0x1

    #@2d
    goto :goto_d

    #@2e
    .line 239
    .end local v2           #rhs_ij:F
    :cond_2e
    invoke-virtual {p0, v0, v8, v3}, Landroid/renderscript/Matrix4f;->set(IIF)V

    #@31
    .line 240
    invoke-virtual {p0, v0, v9, v4}, Landroid/renderscript/Matrix4f;->set(IIF)V

    #@34
    .line 241
    invoke-virtual {p0, v0, v10, v5}, Landroid/renderscript/Matrix4f;->set(IIF)V

    #@37
    .line 242
    invoke-virtual {p0, v0, v11, v6}, Landroid/renderscript/Matrix4f;->set(IIF)V

    #@3a
    .line 227
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_6

    #@3d
    .line 244
    .end local v1           #j:I
    .end local v3           #ri0:F
    .end local v4           #ri1:F
    .end local v5           #ri2:F
    .end local v6           #ri3:F
    :cond_3d
    return-void
.end method

.method public loadOrtho(FFFFFF)V
    .registers 11
    .parameter "l"
    .parameter "r"
    .parameter "b"
    .parameter "t"
    .parameter "n"
    .parameter "f"

    #@0
    .prologue
    const/high16 v3, 0x4000

    #@2
    .line 257
    invoke-virtual {p0}, Landroid/renderscript/Matrix4f;->loadIdentity()V

    #@5
    .line 258
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@7
    const/4 v1, 0x0

    #@8
    sub-float v2, p2, p1

    #@a
    div-float v2, v3, v2

    #@c
    aput v2, v0, v1

    #@e
    .line 259
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@10
    const/4 v1, 0x5

    #@11
    sub-float v2, p4, p3

    #@13
    div-float v2, v3, v2

    #@15
    aput v2, v0, v1

    #@17
    .line 260
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@19
    const/16 v1, 0xa

    #@1b
    const/high16 v2, -0x4000

    #@1d
    sub-float v3, p6, p5

    #@1f
    div-float/2addr v2, v3

    #@20
    aput v2, v0, v1

    #@22
    .line 261
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@24
    const/16 v1, 0xc

    #@26
    add-float v2, p2, p1

    #@28
    neg-float v2, v2

    #@29
    sub-float v3, p2, p1

    #@2b
    div-float/2addr v2, v3

    #@2c
    aput v2, v0, v1

    #@2e
    .line 262
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@30
    const/16 v1, 0xd

    #@32
    add-float v2, p4, p3

    #@34
    neg-float v2, v2

    #@35
    sub-float v3, p4, p3

    #@37
    div-float/2addr v2, v3

    #@38
    aput v2, v0, v1

    #@3a
    .line 263
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@3c
    const/16 v1, 0xe

    #@3e
    add-float v2, p6, p5

    #@40
    neg-float v2, v2

    #@41
    sub-float v3, p6, p5

    #@43
    div-float/2addr v2, v3

    #@44
    aput v2, v0, v1

    #@46
    .line 264
    return-void
.end method

.method public loadOrthoWindow(II)V
    .registers 10
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 277
    int-to-float v2, p1

    #@2
    int-to-float v3, p2

    #@3
    const/high16 v5, -0x4080

    #@5
    const/high16 v6, 0x3f80

    #@7
    move-object v0, p0

    #@8
    move v4, v1

    #@9
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/Matrix4f;->loadOrtho(FFFFFF)V

    #@c
    .line 278
    return-void
.end method

.method public loadPerspective(FFFF)V
    .registers 14
    .parameter "fovy"
    .parameter "aspect"
    .parameter "near"
    .parameter "far"

    #@0
    .prologue
    .line 312
    float-to-double v5, p1

    #@1
    const-wide v7, 0x400921fb54442d18L

    #@6
    mul-double/2addr v5, v7

    #@7
    const-wide v7, 0x4076800000000000L

    #@c
    div-double/2addr v5, v7

    #@d
    double-to-float v0, v5

    #@e
    float-to-double v5, v0

    #@f
    invoke-static {v5, v6}, Ljava/lang/Math;->tan(D)D

    #@12
    move-result-wide v5

    #@13
    double-to-float v0, v5

    #@14
    mul-float v4, p3, v0

    #@16
    .line 313
    .local v4, top:F
    neg-float v3, v4

    #@17
    .line 314
    .local v3, bottom:F
    mul-float v1, v3, p2

    #@19
    .line 315
    .local v1, left:F
    mul-float v2, v4, p2

    #@1b
    .local v2, right:F
    move-object v0, p0

    #@1c
    move v5, p3

    #@1d
    move v6, p4

    #@1e
    .line 316
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/Matrix4f;->loadFrustum(FFFFFF)V

    #@21
    .line 317
    return-void
.end method

.method public loadProjectionNormalized(II)V
    .registers 14
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 329
    new-instance v0, Landroid/renderscript/Matrix4f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    #@5
    .line 330
    .local v0, m1:Landroid/renderscript/Matrix4f;
    new-instance v10, Landroid/renderscript/Matrix4f;

    #@7
    invoke-direct {v10}, Landroid/renderscript/Matrix4f;-><init>()V

    #@a
    .line 332
    .local v10, m2:Landroid/renderscript/Matrix4f;
    if-le p1, p2, :cond_42

    #@c
    .line 333
    int-to-float v1, p1

    #@d
    int-to-float v3, p2

    #@e
    div-float v2, v1, v3

    #@10
    .line 334
    .local v2, aspect:F
    neg-float v1, v2

    #@11
    const/high16 v3, -0x4080

    #@13
    const/high16 v4, 0x3f80

    #@15
    const/high16 v5, 0x3f80

    #@17
    const/high16 v6, 0x42c8

    #@19
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/Matrix4f;->loadFrustum(FFFFFF)V

    #@1c
    .line 340
    :goto_1c
    const/high16 v1, 0x4334

    #@1e
    const/4 v3, 0x0

    #@1f
    const/high16 v4, 0x3f80

    #@21
    const/4 v5, 0x0

    #@22
    invoke-virtual {v10, v1, v3, v4, v5}, Landroid/renderscript/Matrix4f;->loadRotate(FFFF)V

    #@25
    .line 341
    invoke-virtual {v0, v0, v10}, Landroid/renderscript/Matrix4f;->loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V

    #@28
    .line 343
    const/high16 v1, -0x4000

    #@2a
    const/high16 v3, 0x4000

    #@2c
    const/high16 v4, 0x3f80

    #@2e
    invoke-virtual {v10, v1, v3, v4}, Landroid/renderscript/Matrix4f;->loadScale(FFF)V

    #@31
    .line 344
    invoke-virtual {v0, v0, v10}, Landroid/renderscript/Matrix4f;->loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V

    #@34
    .line 346
    const/4 v1, 0x0

    #@35
    const/4 v3, 0x0

    #@36
    const/high16 v4, 0x4000

    #@38
    invoke-virtual {v10, v1, v3, v4}, Landroid/renderscript/Matrix4f;->loadTranslate(FFF)V

    #@3b
    .line 347
    invoke-virtual {v0, v0, v10}, Landroid/renderscript/Matrix4f;->loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V

    #@3e
    .line 349
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix4f;->load(Landroid/renderscript/Matrix4f;)V

    #@41
    .line 350
    return-void

    #@42
    .line 336
    .end local v2           #aspect:F
    :cond_42
    int-to-float v1, p2

    #@43
    int-to-float v3, p1

    #@44
    div-float v2, v1, v3

    #@46
    .line 337
    .restart local v2       #aspect:F
    const/high16 v4, -0x4080

    #@48
    const/high16 v5, 0x3f80

    #@4a
    neg-float v6, v2

    #@4b
    const/high16 v8, 0x3f80

    #@4d
    const/high16 v9, 0x42c8

    #@4f
    move-object v3, v0

    #@50
    move v7, v2

    #@51
    invoke-virtual/range {v3 .. v9}, Landroid/renderscript/Matrix4f;->loadFrustum(FFFFFF)V

    #@54
    goto :goto_1c
.end method

.method public loadRotate(FFFF)V
    .registers 19
    .parameter "rot"
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 154
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@2
    const/4 v12, 0x3

    #@3
    const/4 v13, 0x0

    #@4
    aput v13, v11, v12

    #@6
    .line 155
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@8
    const/4 v12, 0x7

    #@9
    const/4 v13, 0x0

    #@a
    aput v13, v11, v12

    #@c
    .line 156
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@e
    const/16 v12, 0xb

    #@10
    const/4 v13, 0x0

    #@11
    aput v13, v11, v12

    #@13
    .line 157
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@15
    const/16 v12, 0xc

    #@17
    const/4 v13, 0x0

    #@18
    aput v13, v11, v12

    #@1a
    .line 158
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@1c
    const/16 v12, 0xd

    #@1e
    const/4 v13, 0x0

    #@1f
    aput v13, v11, v12

    #@21
    .line 159
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@23
    const/16 v12, 0xe

    #@25
    const/4 v13, 0x0

    #@26
    aput v13, v11, v12

    #@28
    .line 160
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@2a
    const/16 v12, 0xf

    #@2c
    const/high16 v13, 0x3f80

    #@2e
    aput v13, v11, v12

    #@30
    .line 161
    const v11, 0x3c8efa35

    #@33
    mul-float/2addr p1, v11

    #@34
    .line 162
    float-to-double v11, p1

    #@35
    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    #@38
    move-result-wide v11

    #@39
    double-to-float v0, v11

    #@3a
    .line 163
    .local v0, c:F
    float-to-double v11, p1

    #@3b
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    #@3e
    move-result-wide v11

    #@3f
    double-to-float v4, v11

    #@40
    .line 165
    .local v4, s:F
    mul-float v11, p2, p2

    #@42
    mul-float v12, p3, p3

    #@44
    add-float/2addr v11, v12

    #@45
    mul-float v12, p4, p4

    #@47
    add-float/2addr v11, v12

    #@48
    float-to-double v11, v11

    #@49
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    #@4c
    move-result-wide v11

    #@4d
    double-to-float v1, v11

    #@4e
    .line 166
    .local v1, len:F
    const/high16 v11, 0x3f80

    #@50
    cmpl-float v11, v1, v11

    #@52
    if-nez v11, :cond_5e

    #@54
    .line 167
    const/high16 v11, 0x3f80

    #@56
    div-float v3, v11, v1

    #@58
    .line 168
    .local v3, recipLen:F
    mul-float p2, p2, v3

    #@5a
    .line 169
    mul-float p3, p3, v3

    #@5c
    .line 170
    mul-float p4, p4, v3

    #@5e
    .line 172
    .end local v3           #recipLen:F
    :cond_5e
    const/high16 v11, 0x3f80

    #@60
    sub-float v2, v11, v0

    #@62
    .line 173
    .local v2, nc:F
    mul-float v6, p2, p3

    #@64
    .line 174
    .local v6, xy:F
    mul-float v8, p3, p4

    #@66
    .line 175
    .local v8, yz:F
    mul-float v10, p4, p2

    #@68
    .line 176
    .local v10, zx:F
    mul-float v5, p2, v4

    #@6a
    .line 177
    .local v5, xs:F
    mul-float v7, p3, v4

    #@6c
    .line 178
    .local v7, ys:F
    mul-float v9, p4, v4

    #@6e
    .line 179
    .local v9, zs:F
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@70
    const/4 v12, 0x0

    #@71
    mul-float v13, p2, p2

    #@73
    mul-float/2addr v13, v2

    #@74
    add-float/2addr v13, v0

    #@75
    aput v13, v11, v12

    #@77
    .line 180
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@79
    const/4 v12, 0x4

    #@7a
    mul-float v13, v6, v2

    #@7c
    sub-float/2addr v13, v9

    #@7d
    aput v13, v11, v12

    #@7f
    .line 181
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@81
    const/16 v12, 0x8

    #@83
    mul-float v13, v10, v2

    #@85
    add-float/2addr v13, v7

    #@86
    aput v13, v11, v12

    #@88
    .line 182
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@8a
    const/4 v12, 0x1

    #@8b
    mul-float v13, v6, v2

    #@8d
    add-float/2addr v13, v9

    #@8e
    aput v13, v11, v12

    #@90
    .line 183
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@92
    const/4 v12, 0x5

    #@93
    mul-float v13, p3, p3

    #@95
    mul-float/2addr v13, v2

    #@96
    add-float/2addr v13, v0

    #@97
    aput v13, v11, v12

    #@99
    .line 184
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@9b
    const/16 v12, 0x9

    #@9d
    mul-float v13, v8, v2

    #@9f
    sub-float/2addr v13, v5

    #@a0
    aput v13, v11, v12

    #@a2
    .line 185
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@a4
    const/4 v12, 0x2

    #@a5
    mul-float v13, v10, v2

    #@a7
    sub-float/2addr v13, v7

    #@a8
    aput v13, v11, v12

    #@aa
    .line 186
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@ac
    const/4 v12, 0x6

    #@ad
    mul-float v13, v8, v2

    #@af
    add-float/2addr v13, v5

    #@b0
    aput v13, v11, v12

    #@b2
    .line 187
    iget-object v11, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@b4
    const/16 v12, 0xa

    #@b6
    mul-float v13, p4, p4

    #@b8
    mul-float/2addr v13, v2

    #@b9
    add-float/2addr v13, v0

    #@ba
    aput v13, v11, v12

    #@bc
    .line 188
    return-void
.end method

.method public loadScale(FFF)V
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 198
    invoke-virtual {p0}, Landroid/renderscript/Matrix4f;->loadIdentity()V

    #@3
    .line 199
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@5
    const/4 v1, 0x0

    #@6
    aput p1, v0, v1

    #@8
    .line 200
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@a
    const/4 v1, 0x5

    #@b
    aput p2, v0, v1

    #@d
    .line 201
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@f
    const/16 v1, 0xa

    #@11
    aput p3, v0, v1

    #@13
    .line 202
    return-void
.end method

.method public loadTranslate(FFF)V
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 213
    invoke-virtual {p0}, Landroid/renderscript/Matrix4f;->loadIdentity()V

    #@3
    .line 214
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@5
    const/16 v1, 0xc

    #@7
    aput p1, v0, v1

    #@9
    .line 215
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@b
    const/16 v1, 0xd

    #@d
    aput p2, v0, v1

    #@f
    .line 216
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@11
    const/16 v1, 0xe

    #@13
    aput p3, v0, v1

    #@15
    .line 217
    return-void
.end method

.method public multiply(Landroid/renderscript/Matrix4f;)V
    .registers 3
    .parameter "rhs"

    #@0
    .prologue
    .line 358
    new-instance v0, Landroid/renderscript/Matrix4f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    #@5
    .line 359
    .local v0, tmp:Landroid/renderscript/Matrix4f;
    invoke-virtual {v0, p0, p1}, Landroid/renderscript/Matrix4f;->loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V

    #@8
    .line 360
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix4f;->load(Landroid/renderscript/Matrix4f;)V

    #@b
    .line 361
    return-void
.end method

.method public rotate(FFFF)V
    .registers 6
    .parameter "rot"
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 372
    new-instance v0, Landroid/renderscript/Matrix4f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    #@5
    .line 373
    .local v0, tmp:Landroid/renderscript/Matrix4f;
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/renderscript/Matrix4f;->loadRotate(FFFF)V

    #@8
    .line 374
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix4f;->multiply(Landroid/renderscript/Matrix4f;)V

    #@b
    .line 375
    return-void
.end method

.method public scale(FFF)V
    .registers 5
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 386
    new-instance v0, Landroid/renderscript/Matrix4f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    #@5
    .line 387
    .local v0, tmp:Landroid/renderscript/Matrix4f;
    invoke-virtual {v0, p1, p2, p3}, Landroid/renderscript/Matrix4f;->loadScale(FFF)V

    #@8
    .line 388
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix4f;->multiply(Landroid/renderscript/Matrix4f;)V

    #@b
    .line 389
    return-void
.end method

.method public set(IIF)V
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "v"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@2
    mul-int/lit8 v1, p1, 0x4

    #@4
    add-int/2addr v1, p2

    #@5
    aput p3, v0, v1

    #@7
    .line 79
    return-void
.end method

.method public translate(FFF)V
    .registers 5
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 400
    new-instance v0, Landroid/renderscript/Matrix4f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    #@5
    .line 401
    .local v0, tmp:Landroid/renderscript/Matrix4f;
    invoke-virtual {v0, p1, p2, p3}, Landroid/renderscript/Matrix4f;->loadTranslate(FFF)V

    #@8
    .line 402
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix4f;->multiply(Landroid/renderscript/Matrix4f;)V

    #@b
    .line 403
    return-void
.end method

.method public transpose()V
    .registers 8

    #@0
    .prologue
    .line 484
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    const/4 v3, 0x3

    #@2
    if-ge v0, v3, :cond_2b

    #@4
    .line 485
    add-int/lit8 v1, v0, 0x1

    #@6
    .local v1, j:I
    :goto_6
    const/4 v3, 0x4

    #@7
    if-ge v1, v3, :cond_28

    #@9
    .line 486
    iget-object v3, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@b
    mul-int/lit8 v4, v0, 0x4

    #@d
    add-int/2addr v4, v1

    #@e
    aget v2, v3, v4

    #@10
    .line 487
    .local v2, temp:F
    iget-object v3, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@12
    mul-int/lit8 v4, v0, 0x4

    #@14
    add-int/2addr v4, v1

    #@15
    iget-object v5, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@17
    mul-int/lit8 v6, v1, 0x4

    #@19
    add-int/2addr v6, v0

    #@1a
    aget v5, v5, v6

    #@1c
    aput v5, v3, v4

    #@1e
    .line 488
    iget-object v3, p0, Landroid/renderscript/Matrix4f;->mMat:[F

    #@20
    mul-int/lit8 v4, v1, 0x4

    #@22
    add-int/2addr v4, v0

    #@23
    aput v2, v3, v4

    #@25
    .line 485
    add-int/lit8 v1, v1, 0x1

    #@27
    goto :goto_6

    #@28
    .line 484
    .end local v2           #temp:F
    :cond_28
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_1

    #@2b
    .line 491
    .end local v1           #j:I
    :cond_2b
    return-void
.end method
