.class public Landroid/renderscript/ProgramVertex;
.super Landroid/renderscript/Program;
.source "ProgramVertex.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/ProgramVertex$Builder;
    }
.end annotation


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/renderscript/Program;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 56
    return-void
.end method


# virtual methods
.method public getInput(I)Landroid/renderscript/Element;
    .registers 4
    .parameter "slot"

    #@0
    .prologue
    .line 72
    if-ltz p1, :cond_7

    #@2
    iget-object v0, p0, Landroid/renderscript/Program;->mInputs:[Landroid/renderscript/Element;

    #@4
    array-length v0, v0

    #@5
    if-lt p1, v0, :cond_f

    #@7
    .line 73
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v1, "Slot ID out of range."

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 75
    :cond_f
    iget-object v0, p0, Landroid/renderscript/Program;->mInputs:[Landroid/renderscript/Element;

    #@11
    aget-object v0, v0, p1

    #@13
    return-object v0
.end method

.method public getInputCount()I
    .registers 2

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/renderscript/Program;->mInputs:[Landroid/renderscript/Element;

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-object v0, p0, Landroid/renderscript/Program;->mInputs:[Landroid/renderscript/Element;

    #@6
    array-length v0, v0

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method
