.class public final Landroid/renderscript/ScriptIntrinsicLUT;
.super Landroid/renderscript/ScriptIntrinsic;
.source "ScriptIntrinsicLUT.java"


# instance fields
.field private final mCache:[B

.field private mDirty:Z

.field private final mMatrix:Landroid/renderscript/Matrix4f;

.field private mTables:Landroid/renderscript/Allocation;


# direct methods
.method private constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 7
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    const/16 v2, 0x400

    #@2
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/renderscript/ScriptIntrinsic;-><init>(ILandroid/renderscript/RenderScript;)V

    #@5
    .line 30
    new-instance v1, Landroid/renderscript/Matrix4f;

    #@7
    invoke-direct {v1}, Landroid/renderscript/Matrix4f;-><init>()V

    #@a
    iput-object v1, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mMatrix:Landroid/renderscript/Matrix4f;

    #@c
    .line 32
    new-array v1, v2, [B

    #@e
    iput-object v1, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mCache:[B

    #@10
    .line 33
    const/4 v1, 0x1

    #@11
    iput-boolean v1, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mDirty:Z

    #@13
    .line 37
    invoke-static {p2}, Landroid/renderscript/Element;->U8(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@16
    move-result-object v1

    #@17
    invoke-static {p2, v1, v2}, Landroid/renderscript/Allocation;->createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;I)Landroid/renderscript/Allocation;

    #@1a
    move-result-object v1

    #@1b
    iput-object v1, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mTables:Landroid/renderscript/Allocation;

    #@1d
    .line 38
    const/4 v0, 0x0

    #@1e
    .local v0, ct:I
    :goto_1e
    const/16 v1, 0x100

    #@20
    if-ge v0, v1, :cond_3f

    #@22
    .line 39
    iget-object v1, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mCache:[B

    #@24
    int-to-byte v2, v0

    #@25
    aput-byte v2, v1, v0

    #@27
    .line 40
    iget-object v1, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mCache:[B

    #@29
    add-int/lit16 v2, v0, 0x100

    #@2b
    int-to-byte v3, v0

    #@2c
    aput-byte v3, v1, v2

    #@2e
    .line 41
    iget-object v1, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mCache:[B

    #@30
    add-int/lit16 v2, v0, 0x200

    #@32
    int-to-byte v3, v0

    #@33
    aput-byte v3, v1, v2

    #@35
    .line 42
    iget-object v1, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mCache:[B

    #@37
    add-int/lit16 v2, v0, 0x300

    #@39
    int-to-byte v3, v0

    #@3a
    aput-byte v3, v1, v2

    #@3c
    .line 38
    add-int/lit8 v0, v0, 0x1

    #@3e
    goto :goto_1e

    #@3f
    .line 44
    :cond_3f
    const/4 v1, 0x0

    #@40
    iget-object v2, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mTables:Landroid/renderscript/Allocation;

    #@42
    invoke-virtual {p0, v1, v2}, Landroid/renderscript/ScriptIntrinsicLUT;->setVar(ILandroid/renderscript/BaseObj;)V

    #@45
    .line 45
    return-void
.end method

.method public static create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicLUT;
    .registers 5
    .parameter "rs"
    .parameter "e"

    #@0
    .prologue
    .line 58
    const/4 v1, 0x3

    #@1
    invoke-virtual {p1, p0}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@4
    move-result v2

    #@5
    invoke-virtual {p0, v1, v2}, Landroid/renderscript/RenderScript;->nScriptIntrinsicCreate(II)I

    #@8
    move-result v0

    #@9
    .line 59
    .local v0, id:I
    new-instance v1, Landroid/renderscript/ScriptIntrinsicLUT;

    #@b
    invoke-direct {v1, v0, p0}, Landroid/renderscript/ScriptIntrinsicLUT;-><init>(ILandroid/renderscript/RenderScript;)V

    #@e
    return-object v1
.end method

.method private validate(II)V
    .registers 5
    .parameter "index"
    .parameter "value"

    #@0
    .prologue
    const/16 v0, 0xff

    #@2
    .line 65
    if-ltz p1, :cond_6

    #@4
    if-le p1, v0, :cond_e

    #@6
    .line 66
    :cond_6
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@8
    const-string v1, "Index out of range (0-255)."

    #@a
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 68
    :cond_e
    if-ltz p2, :cond_12

    #@10
    if-le p2, v0, :cond_1a

    #@12
    .line 69
    :cond_12
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@14
    const-string v1, "Value out of range (0-255)."

    #@16
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 71
    :cond_1a
    return-void
.end method


# virtual methods
.method public forEach(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 6
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 130
    iget-boolean v0, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mDirty:Z

    #@3
    if-eqz v0, :cond_e

    #@5
    .line 131
    iput-boolean v2, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mDirty:Z

    #@7
    .line 132
    iget-object v0, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mTables:Landroid/renderscript/Allocation;

    #@9
    iget-object v1, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mCache:[B

    #@b
    invoke-virtual {v0, v1}, Landroid/renderscript/Allocation;->copyFromUnchecked([B)V

    #@e
    .line 134
    :cond_e
    const/4 v0, 0x0

    #@f
    invoke-virtual {p0, v2, p1, p2, v0}, Landroid/renderscript/ScriptIntrinsicLUT;->forEach(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;Landroid/renderscript/FieldPacker;)V

    #@12
    .line 135
    return-void
.end method

.method public getKernelID()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 143
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicLUT;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public setAlpha(II)V
    .registers 6
    .parameter "index"
    .parameter "value"

    #@0
    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Landroid/renderscript/ScriptIntrinsicLUT;->validate(II)V

    #@3
    .line 117
    iget-object v0, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mCache:[B

    #@5
    add-int/lit16 v1, p1, 0x300

    #@7
    int-to-byte v2, p2

    #@8
    aput-byte v2, v0, v1

    #@a
    .line 118
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mDirty:Z

    #@d
    .line 119
    return-void
.end method

.method public setBlue(II)V
    .registers 6
    .parameter "index"
    .parameter "value"

    #@0
    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Landroid/renderscript/ScriptIntrinsicLUT;->validate(II)V

    #@3
    .line 105
    iget-object v0, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mCache:[B

    #@5
    add-int/lit16 v1, p1, 0x200

    #@7
    int-to-byte v2, p2

    #@8
    aput-byte v2, v0, v1

    #@a
    .line 106
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mDirty:Z

    #@d
    .line 107
    return-void
.end method

.method public setGreen(II)V
    .registers 6
    .parameter "index"
    .parameter "value"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Landroid/renderscript/ScriptIntrinsicLUT;->validate(II)V

    #@3
    .line 93
    iget-object v0, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mCache:[B

    #@5
    add-int/lit16 v1, p1, 0x100

    #@7
    int-to-byte v2, p2

    #@8
    aput-byte v2, v0, v1

    #@a
    .line 94
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mDirty:Z

    #@d
    .line 95
    return-void
.end method

.method public setRed(II)V
    .registers 5
    .parameter "index"
    .parameter "value"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Landroid/renderscript/ScriptIntrinsicLUT;->validate(II)V

    #@3
    .line 81
    iget-object v0, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mCache:[B

    #@5
    int-to-byte v1, p2

    #@6
    aput-byte v1, v0, p1

    #@8
    .line 82
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Landroid/renderscript/ScriptIntrinsicLUT;->mDirty:Z

    #@b
    .line 83
    return-void
.end method
