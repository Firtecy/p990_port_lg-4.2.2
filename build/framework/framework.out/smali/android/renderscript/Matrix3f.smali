.class public Landroid/renderscript/Matrix3f;
.super Ljava/lang/Object;
.source "Matrix3f.java"


# instance fields
.field final mMat:[F


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    const/16 v0, 0x9

    #@5
    new-array v0, v0, [F

    #@7
    iput-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@9
    .line 34
    invoke-virtual {p0}, Landroid/renderscript/Matrix3f;->loadIdentity()V

    #@c
    .line 35
    return-void
.end method

.method public constructor <init>([F)V
    .registers 5
    .parameter "dataArray"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 45
    const/16 v0, 0x9

    #@6
    new-array v0, v0, [F

    #@8
    iput-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@a
    .line 46
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@c
    iget-object v1, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@e
    array-length v1, v1

    #@f
    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@12
    .line 47
    return-void
.end method


# virtual methods
.method public get(II)F
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@2
    mul-int/lit8 v1, p1, 0x3

    #@4
    add-int/2addr v1, p2

    #@5
    aget v0, v0, v1

    #@7
    return v0
.end method

.method public getArray()[F
    .registers 2

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@2
    return-object v0
.end method

.method public load(Landroid/renderscript/Matrix3f;)V
    .registers 6
    .parameter "src"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 104
    invoke-virtual {p1}, Landroid/renderscript/Matrix3f;->getArray()[F

    #@4
    move-result-object v0

    #@5
    iget-object v1, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@7
    iget-object v2, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@9
    array-length v2, v2

    #@a
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@d
    .line 105
    return-void
.end method

.method public loadIdentity()V
    .registers 5

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    const/4 v2, 0x0

    #@3
    .line 85
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@5
    const/4 v1, 0x0

    #@6
    aput v3, v0, v1

    #@8
    .line 86
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@a
    const/4 v1, 0x1

    #@b
    aput v2, v0, v1

    #@d
    .line 87
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@f
    const/4 v1, 0x2

    #@10
    aput v2, v0, v1

    #@12
    .line 89
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@14
    const/4 v1, 0x3

    #@15
    aput v2, v0, v1

    #@17
    .line 90
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@19
    const/4 v1, 0x4

    #@1a
    aput v3, v0, v1

    #@1c
    .line 91
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@1e
    const/4 v1, 0x5

    #@1f
    aput v2, v0, v1

    #@21
    .line 93
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@23
    const/4 v1, 0x6

    #@24
    aput v2, v0, v1

    #@26
    .line 94
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@28
    const/4 v1, 0x7

    #@29
    aput v2, v0, v1

    #@2b
    .line 95
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@2d
    const/16 v1, 0x8

    #@2f
    aput v3, v0, v1

    #@31
    .line 96
    return-void
.end method

.method public loadMultiply(Landroid/renderscript/Matrix3f;Landroid/renderscript/Matrix3f;)V
    .registers 14
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    const/4 v9, 0x2

    #@2
    const/4 v8, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    .line 211
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v10, :cond_32

    #@7
    .line 212
    const/4 v3, 0x0

    #@8
    .line 213
    .local v3, ri0:F
    const/4 v4, 0x0

    #@9
    .line 214
    .local v4, ri1:F
    const/4 v5, 0x0

    #@a
    .line 215
    .local v5, ri2:F
    const/4 v1, 0x0

    #@b
    .local v1, j:I
    :goto_b
    if-ge v1, v10, :cond_26

    #@d
    .line 216
    invoke-virtual {p2, v0, v1}, Landroid/renderscript/Matrix3f;->get(II)F

    #@10
    move-result v2

    #@11
    .line 217
    .local v2, rhs_ij:F
    invoke-virtual {p1, v1, v7}, Landroid/renderscript/Matrix3f;->get(II)F

    #@14
    move-result v6

    #@15
    mul-float/2addr v6, v2

    #@16
    add-float/2addr v3, v6

    #@17
    .line 218
    invoke-virtual {p1, v1, v8}, Landroid/renderscript/Matrix3f;->get(II)F

    #@1a
    move-result v6

    #@1b
    mul-float/2addr v6, v2

    #@1c
    add-float/2addr v4, v6

    #@1d
    .line 219
    invoke-virtual {p1, v1, v9}, Landroid/renderscript/Matrix3f;->get(II)F

    #@20
    move-result v6

    #@21
    mul-float/2addr v6, v2

    #@22
    add-float/2addr v5, v6

    #@23
    .line 215
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_b

    #@26
    .line 221
    .end local v2           #rhs_ij:F
    :cond_26
    invoke-virtual {p0, v0, v7, v3}, Landroid/renderscript/Matrix3f;->set(IIF)V

    #@29
    .line 222
    invoke-virtual {p0, v0, v8, v4}, Landroid/renderscript/Matrix3f;->set(IIF)V

    #@2c
    .line 223
    invoke-virtual {p0, v0, v9, v5}, Landroid/renderscript/Matrix3f;->set(IIF)V

    #@2f
    .line 211
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_5

    #@32
    .line 225
    .end local v1           #j:I
    .end local v3           #ri0:F
    .end local v4           #ri1:F
    .end local v5           #ri2:F
    :cond_32
    return-void
.end method

.method public loadRotate(F)V
    .registers 7
    .parameter "rot"

    #@0
    .prologue
    .line 153
    invoke-virtual {p0}, Landroid/renderscript/Matrix3f;->loadIdentity()V

    #@3
    .line 155
    const v2, 0x3c8efa35

    #@6
    mul-float/2addr p1, v2

    #@7
    .line 156
    float-to-double v2, p1

    #@8
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    #@b
    move-result-wide v2

    #@c
    double-to-float v0, v2

    #@d
    .line 157
    .local v0, c:F
    float-to-double v2, p1

    #@e
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    #@11
    move-result-wide v2

    #@12
    double-to-float v1, v2

    #@13
    .line 158
    .local v1, s:F
    iget-object v2, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@15
    const/4 v3, 0x0

    #@16
    aput v0, v2, v3

    #@18
    .line 159
    iget-object v2, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@1a
    const/4 v3, 0x1

    #@1b
    neg-float v4, v1

    #@1c
    aput v4, v2, v3

    #@1e
    .line 160
    iget-object v2, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@20
    const/4 v3, 0x3

    #@21
    aput v1, v2, v3

    #@23
    .line 161
    iget-object v2, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@25
    const/4 v3, 0x4

    #@26
    aput v0, v2, v3

    #@28
    .line 162
    return-void
.end method

.method public loadRotate(FFFF)V
    .registers 19
    .parameter "rot"
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 118
    const v11, 0x3c8efa35

    #@3
    mul-float/2addr p1, v11

    #@4
    .line 119
    float-to-double v11, p1

    #@5
    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    #@8
    move-result-wide v11

    #@9
    double-to-float v0, v11

    #@a
    .line 120
    .local v0, c:F
    float-to-double v11, p1

    #@b
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    #@e
    move-result-wide v11

    #@f
    double-to-float v4, v11

    #@10
    .line 122
    .local v4, s:F
    mul-float v11, p2, p2

    #@12
    mul-float v12, p3, p3

    #@14
    add-float/2addr v11, v12

    #@15
    mul-float v12, p4, p4

    #@17
    add-float/2addr v11, v12

    #@18
    float-to-double v11, v11

    #@19
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    #@1c
    move-result-wide v11

    #@1d
    double-to-float v1, v11

    #@1e
    .line 123
    .local v1, len:F
    const/high16 v11, 0x3f80

    #@20
    cmpl-float v11, v1, v11

    #@22
    if-nez v11, :cond_2e

    #@24
    .line 124
    const/high16 v11, 0x3f80

    #@26
    div-float v3, v11, v1

    #@28
    .line 125
    .local v3, recipLen:F
    mul-float p2, p2, v3

    #@2a
    .line 126
    mul-float p3, p3, v3

    #@2c
    .line 127
    mul-float p4, p4, v3

    #@2e
    .line 129
    .end local v3           #recipLen:F
    :cond_2e
    const/high16 v11, 0x3f80

    #@30
    sub-float v2, v11, v0

    #@32
    .line 130
    .local v2, nc:F
    mul-float v6, p2, p3

    #@34
    .line 131
    .local v6, xy:F
    mul-float v8, p3, p4

    #@36
    .line 132
    .local v8, yz:F
    mul-float v10, p4, p2

    #@38
    .line 133
    .local v10, zx:F
    mul-float v5, p2, v4

    #@3a
    .line 134
    .local v5, xs:F
    mul-float v7, p3, v4

    #@3c
    .line 135
    .local v7, ys:F
    mul-float v9, p4, v4

    #@3e
    .line 136
    .local v9, zs:F
    iget-object v11, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@40
    const/4 v12, 0x0

    #@41
    mul-float v13, p2, p2

    #@43
    mul-float/2addr v13, v2

    #@44
    add-float/2addr v13, v0

    #@45
    aput v13, v11, v12

    #@47
    .line 137
    iget-object v11, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@49
    const/4 v12, 0x3

    #@4a
    mul-float v13, v6, v2

    #@4c
    sub-float/2addr v13, v9

    #@4d
    aput v13, v11, v12

    #@4f
    .line 138
    iget-object v11, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@51
    const/4 v12, 0x6

    #@52
    mul-float v13, v10, v2

    #@54
    add-float/2addr v13, v7

    #@55
    aput v13, v11, v12

    #@57
    .line 139
    iget-object v11, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@59
    const/4 v12, 0x1

    #@5a
    mul-float v13, v6, v2

    #@5c
    add-float/2addr v13, v9

    #@5d
    aput v13, v11, v12

    #@5f
    .line 140
    iget-object v11, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@61
    const/4 v12, 0x4

    #@62
    mul-float v13, p3, p3

    #@64
    mul-float/2addr v13, v2

    #@65
    add-float/2addr v13, v0

    #@66
    aput v13, v11, v12

    #@68
    .line 141
    iget-object v11, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@6a
    const/16 v12, 0x9

    #@6c
    mul-float v13, v8, v2

    #@6e
    sub-float/2addr v13, v5

    #@6f
    aput v13, v11, v12

    #@71
    .line 142
    iget-object v11, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@73
    const/4 v12, 0x2

    #@74
    mul-float v13, v10, v2

    #@76
    sub-float/2addr v13, v7

    #@77
    aput v13, v11, v12

    #@79
    .line 143
    iget-object v11, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@7b
    const/4 v12, 0x6

    #@7c
    mul-float v13, v8, v2

    #@7e
    add-float/2addr v13, v5

    #@7f
    aput v13, v11, v12

    #@81
    .line 144
    iget-object v11, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@83
    const/16 v12, 0x8

    #@85
    mul-float v13, p4, p4

    #@87
    mul-float/2addr v13, v2

    #@88
    add-float/2addr v13, v0

    #@89
    aput v13, v11, v12

    #@8b
    .line 145
    return-void
.end method

.method public loadScale(FF)V
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 171
    invoke-virtual {p0}, Landroid/renderscript/Matrix3f;->loadIdentity()V

    #@3
    .line 172
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@5
    const/4 v1, 0x0

    #@6
    aput p1, v0, v1

    #@8
    .line 173
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@a
    const/4 v1, 0x4

    #@b
    aput p2, v0, v1

    #@d
    .line 174
    return-void
.end method

.method public loadScale(FFF)V
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 184
    invoke-virtual {p0}, Landroid/renderscript/Matrix3f;->loadIdentity()V

    #@3
    .line 185
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@5
    const/4 v1, 0x0

    #@6
    aput p1, v0, v1

    #@8
    .line 186
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@a
    const/4 v1, 0x4

    #@b
    aput p2, v0, v1

    #@d
    .line 187
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@f
    const/16 v1, 0x8

    #@11
    aput p3, v0, v1

    #@13
    .line 188
    return-void
.end method

.method public loadTranslate(FF)V
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 198
    invoke-virtual {p0}, Landroid/renderscript/Matrix3f;->loadIdentity()V

    #@3
    .line 199
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@5
    const/4 v1, 0x6

    #@6
    aput p1, v0, v1

    #@8
    .line 200
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@a
    const/4 v1, 0x7

    #@b
    aput p2, v0, v1

    #@d
    .line 201
    return-void
.end method

.method public multiply(Landroid/renderscript/Matrix3f;)V
    .registers 3
    .parameter "rhs"

    #@0
    .prologue
    .line 233
    new-instance v0, Landroid/renderscript/Matrix3f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix3f;-><init>()V

    #@5
    .line 234
    .local v0, tmp:Landroid/renderscript/Matrix3f;
    invoke-virtual {v0, p0, p1}, Landroid/renderscript/Matrix3f;->loadMultiply(Landroid/renderscript/Matrix3f;Landroid/renderscript/Matrix3f;)V

    #@8
    .line 235
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix3f;->load(Landroid/renderscript/Matrix3f;)V

    #@b
    .line 236
    return-void
.end method

.method public rotate(F)V
    .registers 3
    .parameter "rot"

    #@0
    .prologue
    .line 260
    new-instance v0, Landroid/renderscript/Matrix3f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix3f;-><init>()V

    #@5
    .line 261
    .local v0, tmp:Landroid/renderscript/Matrix3f;
    invoke-virtual {v0, p1}, Landroid/renderscript/Matrix3f;->loadRotate(F)V

    #@8
    .line 262
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix3f;->multiply(Landroid/renderscript/Matrix3f;)V

    #@b
    .line 263
    return-void
.end method

.method public rotate(FFFF)V
    .registers 6
    .parameter "rot"
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 248
    new-instance v0, Landroid/renderscript/Matrix3f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix3f;-><init>()V

    #@5
    .line 249
    .local v0, tmp:Landroid/renderscript/Matrix3f;
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/renderscript/Matrix3f;->loadRotate(FFFF)V

    #@8
    .line 250
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix3f;->multiply(Landroid/renderscript/Matrix3f;)V

    #@b
    .line 251
    return-void
.end method

.method public scale(FF)V
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 273
    new-instance v0, Landroid/renderscript/Matrix3f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix3f;-><init>()V

    #@5
    .line 274
    .local v0, tmp:Landroid/renderscript/Matrix3f;
    invoke-virtual {v0, p1, p2}, Landroid/renderscript/Matrix3f;->loadScale(FF)V

    #@8
    .line 275
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix3f;->multiply(Landroid/renderscript/Matrix3f;)V

    #@b
    .line 276
    return-void
.end method

.method public scale(FFF)V
    .registers 5
    .parameter "x"
    .parameter "y"
    .parameter "z"

    #@0
    .prologue
    .line 287
    new-instance v0, Landroid/renderscript/Matrix3f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix3f;-><init>()V

    #@5
    .line 288
    .local v0, tmp:Landroid/renderscript/Matrix3f;
    invoke-virtual {v0, p1, p2, p3}, Landroid/renderscript/Matrix3f;->loadScale(FFF)V

    #@8
    .line 289
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix3f;->multiply(Landroid/renderscript/Matrix3f;)V

    #@b
    .line 290
    return-void
.end method

.method public set(IIF)V
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "v"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@2
    mul-int/lit8 v1, p1, 0x3

    #@4
    add-int/2addr v1, p2

    #@5
    aput p3, v0, v1

    #@7
    .line 79
    return-void
.end method

.method public translate(FF)V
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 300
    new-instance v0, Landroid/renderscript/Matrix3f;

    #@2
    invoke-direct {v0}, Landroid/renderscript/Matrix3f;-><init>()V

    #@5
    .line 301
    .local v0, tmp:Landroid/renderscript/Matrix3f;
    invoke-virtual {v0, p1, p2}, Landroid/renderscript/Matrix3f;->loadTranslate(FF)V

    #@8
    .line 302
    invoke-virtual {p0, v0}, Landroid/renderscript/Matrix3f;->multiply(Landroid/renderscript/Matrix3f;)V

    #@b
    .line 303
    return-void
.end method

.method public transpose()V
    .registers 8

    #@0
    .prologue
    .line 309
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    const/4 v3, 0x2

    #@2
    if-ge v0, v3, :cond_2b

    #@4
    .line 310
    add-int/lit8 v1, v0, 0x1

    #@6
    .local v1, j:I
    :goto_6
    const/4 v3, 0x3

    #@7
    if-ge v1, v3, :cond_28

    #@9
    .line 311
    iget-object v3, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@b
    mul-int/lit8 v4, v0, 0x3

    #@d
    add-int/2addr v4, v1

    #@e
    aget v2, v3, v4

    #@10
    .line 312
    .local v2, temp:F
    iget-object v3, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@12
    mul-int/lit8 v4, v0, 0x3

    #@14
    add-int/2addr v4, v1

    #@15
    iget-object v5, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@17
    mul-int/lit8 v6, v1, 0x3

    #@19
    add-int/2addr v6, v0

    #@1a
    aget v5, v5, v6

    #@1c
    aput v5, v3, v4

    #@1e
    .line 313
    iget-object v3, p0, Landroid/renderscript/Matrix3f;->mMat:[F

    #@20
    mul-int/lit8 v4, v1, 0x3

    #@22
    add-int/2addr v4, v0

    #@23
    aput v2, v3, v4

    #@25
    .line 310
    add-int/lit8 v1, v1, 0x1

    #@27
    goto :goto_6

    #@28
    .line 309
    .end local v2           #temp:F
    :cond_28
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_1

    #@2b
    .line 316
    .end local v1           #j:I
    :cond_2b
    return-void
.end method
