.class public final Landroid/renderscript/ScriptGroup$Builder;
.super Ljava/lang/Object;
.source "ScriptGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/ScriptGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mKernelCount:I

.field private mLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/renderscript/ScriptGroup$ConnectLine;",
            ">;"
        }
    .end annotation
.end field

.field private mNodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/renderscript/ScriptGroup$Node;",
            ">;"
        }
    .end annotation
.end field

.field private mRS:Landroid/renderscript/RenderScript;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 174
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 164
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@a
    .line 165
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Landroid/renderscript/ScriptGroup$Builder;->mLines:Ljava/util/ArrayList;

    #@11
    .line 175
    iput-object p1, p0, Landroid/renderscript/ScriptGroup$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@13
    .line 176
    return-void
.end method

.method private findNode(Landroid/renderscript/Script$KernelID;)Landroid/renderscript/ScriptGroup$Node;
    .registers 6
    .parameter "k"

    #@0
    .prologue
    .line 256
    const/4 v0, 0x0

    #@1
    .local v0, ct:I
    :goto_1
    iget-object v3, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v3

    #@7
    if-ge v0, v3, :cond_29

    #@9
    .line 257
    iget-object v3, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/renderscript/ScriptGroup$Node;

    #@11
    .line 258
    .local v2, n:Landroid/renderscript/ScriptGroup$Node;
    const/4 v1, 0x0

    #@12
    .local v1, ct2:I
    :goto_12
    iget-object v3, v2, Landroid/renderscript/ScriptGroup$Node;->mKernels:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v3

    #@18
    if-ge v1, v3, :cond_26

    #@1a
    .line 259
    iget-object v3, v2, Landroid/renderscript/ScriptGroup$Node;->mKernels:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    if-ne p1, v3, :cond_23

    #@22
    .line 264
    .end local v1           #ct2:I
    .end local v2           #n:Landroid/renderscript/ScriptGroup$Node;
    :goto_22
    return-object v2

    #@23
    .line 258
    .restart local v1       #ct2:I
    .restart local v2       #n:Landroid/renderscript/ScriptGroup$Node;
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_12

    #@26
    .line 256
    :cond_26
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_1

    #@29
    .line 264
    .end local v1           #ct2:I
    .end local v2           #n:Landroid/renderscript/ScriptGroup$Node;
    :cond_29
    const/4 v2, 0x0

    #@2a
    goto :goto_22
.end method

.method private findNode(Landroid/renderscript/Script;)Landroid/renderscript/ScriptGroup$Node;
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 247
    const/4 v0, 0x0

    #@1
    .local v0, ct:I
    :goto_1
    iget-object v1, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_21

    #@9
    .line 248
    iget-object v1, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/renderscript/ScriptGroup$Node;

    #@11
    iget-object v1, v1, Landroid/renderscript/ScriptGroup$Node;->mScript:Landroid/renderscript/Script;

    #@13
    if-ne p1, v1, :cond_1e

    #@15
    .line 249
    iget-object v1, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Landroid/renderscript/ScriptGroup$Node;

    #@1d
    .line 252
    :goto_1d
    return-object v1

    #@1e
    .line 247
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_1

    #@21
    .line 252
    :cond_21
    const/4 v1, 0x0

    #@22
    goto :goto_1d
.end method

.method private mergeDAGs(II)V
    .registers 5
    .parameter "valueUsed"
    .parameter "valueKilled"

    #@0
    .prologue
    .line 201
    const/4 v0, 0x0

    #@1
    .local v0, ct:I
    :goto_1
    iget-object v1, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_22

    #@9
    .line 202
    iget-object v1, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/renderscript/ScriptGroup$Node;

    #@11
    iget v1, v1, Landroid/renderscript/ScriptGroup$Node;->dagNumber:I

    #@13
    if-ne v1, p2, :cond_1f

    #@15
    .line 203
    iget-object v1, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Landroid/renderscript/ScriptGroup$Node;

    #@1d
    iput p1, v1, Landroid/renderscript/ScriptGroup$Node;->dagNumber:I

    #@1f
    .line 201
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_1

    #@22
    .line 205
    :cond_22
    return-void
.end method

.method private validateCycle(Landroid/renderscript/ScriptGroup$Node;Landroid/renderscript/ScriptGroup$Node;)V
    .registers 8
    .parameter "target"
    .parameter "original"

    #@0
    .prologue
    .line 181
    const/4 v1, 0x0

    #@1
    .local v1, ct:I
    :goto_1
    iget-object v3, p1, Landroid/renderscript/ScriptGroup$Node;->mOutputs:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v3

    #@7
    if-ge v1, v3, :cond_4e

    #@9
    .line 182
    iget-object v3, p1, Landroid/renderscript/ScriptGroup$Node;->mOutputs:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/renderscript/ScriptGroup$ConnectLine;

    #@11
    .line 183
    .local v0, cl:Landroid/renderscript/ScriptGroup$ConnectLine;
    iget-object v3, v0, Landroid/renderscript/ScriptGroup$ConnectLine;->mToK:Landroid/renderscript/Script$KernelID;

    #@13
    if-eqz v3, :cond_2e

    #@15
    .line 184
    iget-object v3, v0, Landroid/renderscript/ScriptGroup$ConnectLine;->mToK:Landroid/renderscript/Script$KernelID;

    #@17
    iget-object v3, v3, Landroid/renderscript/Script$KernelID;->mScript:Landroid/renderscript/Script;

    #@19
    invoke-direct {p0, v3}, Landroid/renderscript/ScriptGroup$Builder;->findNode(Landroid/renderscript/Script;)Landroid/renderscript/ScriptGroup$Node;

    #@1c
    move-result-object v2

    #@1d
    .line 185
    .local v2, tn:Landroid/renderscript/ScriptGroup$Node;
    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_2b

    #@23
    .line 186
    new-instance v3, Landroid/renderscript/RSInvalidStateException;

    #@25
    const-string v4, "Loops in group not allowed."

    #@27
    invoke-direct {v3, v4}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v3

    #@2b
    .line 188
    :cond_2b
    invoke-direct {p0, v2, p2}, Landroid/renderscript/ScriptGroup$Builder;->validateCycle(Landroid/renderscript/ScriptGroup$Node;Landroid/renderscript/ScriptGroup$Node;)V

    #@2e
    .line 190
    .end local v2           #tn:Landroid/renderscript/ScriptGroup$Node;
    :cond_2e
    iget-object v3, v0, Landroid/renderscript/ScriptGroup$ConnectLine;->mToF:Landroid/renderscript/Script$FieldID;

    #@30
    if-eqz v3, :cond_4b

    #@32
    .line 191
    iget-object v3, v0, Landroid/renderscript/ScriptGroup$ConnectLine;->mToF:Landroid/renderscript/Script$FieldID;

    #@34
    iget-object v3, v3, Landroid/renderscript/Script$FieldID;->mScript:Landroid/renderscript/Script;

    #@36
    invoke-direct {p0, v3}, Landroid/renderscript/ScriptGroup$Builder;->findNode(Landroid/renderscript/Script;)Landroid/renderscript/ScriptGroup$Node;

    #@39
    move-result-object v2

    #@3a
    .line 192
    .restart local v2       #tn:Landroid/renderscript/ScriptGroup$Node;
    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v3

    #@3e
    if-eqz v3, :cond_48

    #@40
    .line 193
    new-instance v3, Landroid/renderscript/RSInvalidStateException;

    #@42
    const-string v4, "Loops in group not allowed."

    #@44
    invoke-direct {v3, v4}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@47
    throw v3

    #@48
    .line 195
    :cond_48
    invoke-direct {p0, v2, p2}, Landroid/renderscript/ScriptGroup$Builder;->validateCycle(Landroid/renderscript/ScriptGroup$Node;Landroid/renderscript/ScriptGroup$Node;)V

    #@4b
    .line 181
    .end local v2           #tn:Landroid/renderscript/ScriptGroup$Node;
    :cond_4b
    add-int/lit8 v1, v1, 0x1

    #@4d
    goto :goto_1

    #@4e
    .line 198
    .end local v0           #cl:Landroid/renderscript/ScriptGroup$ConnectLine;
    :cond_4e
    return-void
.end method

.method private validateDAG()V
    .registers 6

    #@0
    .prologue
    .line 229
    const/4 v0, 0x0

    #@1
    .local v0, ct:I
    :goto_1
    iget-object v3, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v3

    #@7
    if-ge v0, v3, :cond_3a

    #@9
    .line 230
    iget-object v3, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/renderscript/ScriptGroup$Node;

    #@11
    .line 231
    .local v2, n:Landroid/renderscript/ScriptGroup$Node;
    iget-object v3, v2, Landroid/renderscript/ScriptGroup$Node;->mInputs:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v3

    #@17
    if-nez v3, :cond_37

    #@19
    .line 232
    iget-object v3, v2, Landroid/renderscript/ScriptGroup$Node;->mOutputs:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v3

    #@1f
    if-nez v3, :cond_32

    #@21
    iget-object v3, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v3

    #@27
    const/4 v4, 0x1

    #@28
    if-le v3, v4, :cond_32

    #@2a
    .line 233
    new-instance v3, Landroid/renderscript/RSInvalidStateException;

    #@2c
    const-string v4, "Groups cannot contain unconnected scripts"

    #@2e
    invoke-direct {v3, v4}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@31
    throw v3

    #@32
    .line 235
    :cond_32
    add-int/lit8 v3, v0, 0x1

    #@34
    invoke-direct {p0, v2, v3}, Landroid/renderscript/ScriptGroup$Builder;->validateDAGRecurse(Landroid/renderscript/ScriptGroup$Node;I)V

    #@37
    .line 229
    :cond_37
    add-int/lit8 v0, v0, 0x1

    #@39
    goto :goto_1

    #@3a
    .line 238
    .end local v2           #n:Landroid/renderscript/ScriptGroup$Node;
    :cond_3a
    iget-object v3, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@3c
    const/4 v4, 0x0

    #@3d
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@40
    move-result-object v3

    #@41
    check-cast v3, Landroid/renderscript/ScriptGroup$Node;

    #@43
    iget v1, v3, Landroid/renderscript/ScriptGroup$Node;->dagNumber:I

    #@45
    .line 239
    .local v1, dagNumber:I
    const/4 v0, 0x0

    #@46
    :goto_46
    iget-object v3, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@48
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@4b
    move-result v3

    #@4c
    if-ge v0, v3, :cond_65

    #@4e
    .line 240
    iget-object v3, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@50
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@53
    move-result-object v3

    #@54
    check-cast v3, Landroid/renderscript/ScriptGroup$Node;

    #@56
    iget v3, v3, Landroid/renderscript/ScriptGroup$Node;->dagNumber:I

    #@58
    if-eq v3, v1, :cond_62

    #@5a
    .line 241
    new-instance v3, Landroid/renderscript/RSInvalidStateException;

    #@5c
    const-string v4, "Multiple DAGs in group not allowed."

    #@5e
    invoke-direct {v3, v4}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@61
    throw v3

    #@62
    .line 239
    :cond_62
    add-int/lit8 v0, v0, 0x1

    #@64
    goto :goto_46

    #@65
    .line 244
    :cond_65
    return-void
.end method

.method private validateDAGRecurse(Landroid/renderscript/ScriptGroup$Node;I)V
    .registers 7
    .parameter "n"
    .parameter "dagNumber"

    #@0
    .prologue
    .line 209
    iget v3, p1, Landroid/renderscript/ScriptGroup$Node;->dagNumber:I

    #@2
    if-eqz v3, :cond_e

    #@4
    iget v3, p1, Landroid/renderscript/ScriptGroup$Node;->dagNumber:I

    #@6
    if-eq v3, p2, :cond_e

    #@8
    .line 210
    iget v3, p1, Landroid/renderscript/ScriptGroup$Node;->dagNumber:I

    #@a
    invoke-direct {p0, v3, p2}, Landroid/renderscript/ScriptGroup$Builder;->mergeDAGs(II)V

    #@d
    .line 226
    :cond_d
    return-void

    #@e
    .line 214
    :cond_e
    iput p2, p1, Landroid/renderscript/ScriptGroup$Node;->dagNumber:I

    #@10
    .line 215
    const/4 v1, 0x0

    #@11
    .local v1, ct:I
    :goto_11
    iget-object v3, p1, Landroid/renderscript/ScriptGroup$Node;->mOutputs:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v3

    #@17
    if-ge v1, v3, :cond_d

    #@19
    .line 216
    iget-object v3, p1, Landroid/renderscript/ScriptGroup$Node;->mOutputs:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/renderscript/ScriptGroup$ConnectLine;

    #@21
    .line 217
    .local v0, cl:Landroid/renderscript/ScriptGroup$ConnectLine;
    iget-object v3, v0, Landroid/renderscript/ScriptGroup$ConnectLine;->mToK:Landroid/renderscript/Script$KernelID;

    #@23
    if-eqz v3, :cond_30

    #@25
    .line 218
    iget-object v3, v0, Landroid/renderscript/ScriptGroup$ConnectLine;->mToK:Landroid/renderscript/Script$KernelID;

    #@27
    iget-object v3, v3, Landroid/renderscript/Script$KernelID;->mScript:Landroid/renderscript/Script;

    #@29
    invoke-direct {p0, v3}, Landroid/renderscript/ScriptGroup$Builder;->findNode(Landroid/renderscript/Script;)Landroid/renderscript/ScriptGroup$Node;

    #@2c
    move-result-object v2

    #@2d
    .line 219
    .local v2, tn:Landroid/renderscript/ScriptGroup$Node;
    invoke-direct {p0, v2, p2}, Landroid/renderscript/ScriptGroup$Builder;->validateDAGRecurse(Landroid/renderscript/ScriptGroup$Node;I)V

    #@30
    .line 221
    .end local v2           #tn:Landroid/renderscript/ScriptGroup$Node;
    :cond_30
    iget-object v3, v0, Landroid/renderscript/ScriptGroup$ConnectLine;->mToF:Landroid/renderscript/Script$FieldID;

    #@32
    if-eqz v3, :cond_3f

    #@34
    .line 222
    iget-object v3, v0, Landroid/renderscript/ScriptGroup$ConnectLine;->mToF:Landroid/renderscript/Script$FieldID;

    #@36
    iget-object v3, v3, Landroid/renderscript/Script$FieldID;->mScript:Landroid/renderscript/Script;

    #@38
    invoke-direct {p0, v3}, Landroid/renderscript/ScriptGroup$Builder;->findNode(Landroid/renderscript/Script;)Landroid/renderscript/ScriptGroup$Node;

    #@3b
    move-result-object v2

    #@3c
    .line 223
    .restart local v2       #tn:Landroid/renderscript/ScriptGroup$Node;
    invoke-direct {p0, v2, p2}, Landroid/renderscript/ScriptGroup$Builder;->validateDAGRecurse(Landroid/renderscript/ScriptGroup$Node;I)V

    #@3f
    .line 215
    .end local v2           #tn:Landroid/renderscript/ScriptGroup$Node;
    :cond_3f
    add-int/lit8 v1, v1, 0x1

    #@41
    goto :goto_11
.end method


# virtual methods
.method public addConnection(Landroid/renderscript/Type;Landroid/renderscript/Script$KernelID;Landroid/renderscript/Script$FieldID;)Landroid/renderscript/ScriptGroup$Builder;
    .registers 9
    .parameter "t"
    .parameter "from"
    .parameter "to"

    #@0
    .prologue
    .line 312
    invoke-direct {p0, p2}, Landroid/renderscript/ScriptGroup$Builder;->findNode(Landroid/renderscript/Script$KernelID;)Landroid/renderscript/ScriptGroup$Node;

    #@3
    move-result-object v1

    #@4
    .line 313
    .local v1, nf:Landroid/renderscript/ScriptGroup$Node;
    if-nez v1, :cond_e

    #@6
    .line 314
    new-instance v3, Landroid/renderscript/RSInvalidStateException;

    #@8
    const-string v4, "From script not found."

    #@a
    invoke-direct {v3, v4}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v3

    #@e
    .line 317
    :cond_e
    iget-object v3, p3, Landroid/renderscript/Script$FieldID;->mScript:Landroid/renderscript/Script;

    #@10
    invoke-direct {p0, v3}, Landroid/renderscript/ScriptGroup$Builder;->findNode(Landroid/renderscript/Script;)Landroid/renderscript/ScriptGroup$Node;

    #@13
    move-result-object v2

    #@14
    .line 318
    .local v2, nt:Landroid/renderscript/ScriptGroup$Node;
    if-nez v2, :cond_1e

    #@16
    .line 319
    new-instance v3, Landroid/renderscript/RSInvalidStateException;

    #@18
    const-string v4, "To script not found."

    #@1a
    invoke-direct {v3, v4}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v3

    #@1e
    .line 322
    :cond_1e
    new-instance v0, Landroid/renderscript/ScriptGroup$ConnectLine;

    #@20
    invoke-direct {v0, p1, p2, p3}, Landroid/renderscript/ScriptGroup$ConnectLine;-><init>(Landroid/renderscript/Type;Landroid/renderscript/Script$KernelID;Landroid/renderscript/Script$FieldID;)V

    #@23
    .line 323
    .local v0, cl:Landroid/renderscript/ScriptGroup$ConnectLine;
    iget-object v3, p0, Landroid/renderscript/ScriptGroup$Builder;->mLines:Ljava/util/ArrayList;

    #@25
    new-instance v4, Landroid/renderscript/ScriptGroup$ConnectLine;

    #@27
    invoke-direct {v4, p1, p2, p3}, Landroid/renderscript/ScriptGroup$ConnectLine;-><init>(Landroid/renderscript/Type;Landroid/renderscript/Script$KernelID;Landroid/renderscript/Script$FieldID;)V

    #@2a
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    .line 325
    iget-object v3, v1, Landroid/renderscript/ScriptGroup$Node;->mOutputs:Ljava/util/ArrayList;

    #@2f
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@32
    .line 326
    iget-object v3, v2, Landroid/renderscript/ScriptGroup$Node;->mInputs:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@37
    .line 328
    invoke-direct {p0, v1, v1}, Landroid/renderscript/ScriptGroup$Builder;->validateCycle(Landroid/renderscript/ScriptGroup$Node;Landroid/renderscript/ScriptGroup$Node;)V

    #@3a
    .line 329
    return-object p0
.end method

.method public addConnection(Landroid/renderscript/Type;Landroid/renderscript/Script$KernelID;Landroid/renderscript/Script$KernelID;)Landroid/renderscript/ScriptGroup$Builder;
    .registers 9
    .parameter "t"
    .parameter "from"
    .parameter "to"

    #@0
    .prologue
    .line 347
    invoke-direct {p0, p2}, Landroid/renderscript/ScriptGroup$Builder;->findNode(Landroid/renderscript/Script$KernelID;)Landroid/renderscript/ScriptGroup$Node;

    #@3
    move-result-object v1

    #@4
    .line 348
    .local v1, nf:Landroid/renderscript/ScriptGroup$Node;
    if-nez v1, :cond_e

    #@6
    .line 349
    new-instance v3, Landroid/renderscript/RSInvalidStateException;

    #@8
    const-string v4, "From script not found."

    #@a
    invoke-direct {v3, v4}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v3

    #@e
    .line 352
    :cond_e
    invoke-direct {p0, p3}, Landroid/renderscript/ScriptGroup$Builder;->findNode(Landroid/renderscript/Script$KernelID;)Landroid/renderscript/ScriptGroup$Node;

    #@11
    move-result-object v2

    #@12
    .line 353
    .local v2, nt:Landroid/renderscript/ScriptGroup$Node;
    if-nez v2, :cond_1c

    #@14
    .line 354
    new-instance v3, Landroid/renderscript/RSInvalidStateException;

    #@16
    const-string v4, "To script not found."

    #@18
    invoke-direct {v3, v4}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v3

    #@1c
    .line 357
    :cond_1c
    new-instance v0, Landroid/renderscript/ScriptGroup$ConnectLine;

    #@1e
    invoke-direct {v0, p1, p2, p3}, Landroid/renderscript/ScriptGroup$ConnectLine;-><init>(Landroid/renderscript/Type;Landroid/renderscript/Script$KernelID;Landroid/renderscript/Script$KernelID;)V

    #@21
    .line 358
    .local v0, cl:Landroid/renderscript/ScriptGroup$ConnectLine;
    iget-object v3, p0, Landroid/renderscript/ScriptGroup$Builder;->mLines:Ljava/util/ArrayList;

    #@23
    new-instance v4, Landroid/renderscript/ScriptGroup$ConnectLine;

    #@25
    invoke-direct {v4, p1, p2, p3}, Landroid/renderscript/ScriptGroup$ConnectLine;-><init>(Landroid/renderscript/Type;Landroid/renderscript/Script$KernelID;Landroid/renderscript/Script$KernelID;)V

    #@28
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2b
    .line 360
    iget-object v3, v1, Landroid/renderscript/ScriptGroup$Node;->mOutputs:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@30
    .line 361
    iget-object v3, v2, Landroid/renderscript/ScriptGroup$Node;->mInputs:Ljava/util/ArrayList;

    #@32
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@35
    .line 363
    invoke-direct {p0, v1, v1}, Landroid/renderscript/ScriptGroup$Builder;->validateCycle(Landroid/renderscript/ScriptGroup$Node;Landroid/renderscript/ScriptGroup$Node;)V

    #@38
    .line 364
    return-object p0
.end method

.method public addKernel(Landroid/renderscript/Script$KernelID;)Landroid/renderscript/ScriptGroup$Builder;
    .registers 5
    .parameter "k"

    #@0
    .prologue
    .line 276
    iget-object v1, p0, Landroid/renderscript/ScriptGroup$Builder;->mLines:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_10

    #@8
    .line 277
    new-instance v1, Landroid/renderscript/RSInvalidStateException;

    #@a
    const-string v2, "Kernels may not be added once connections exist."

    #@c
    invoke-direct {v1, v2}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 282
    :cond_10
    invoke-direct {p0, p1}, Landroid/renderscript/ScriptGroup$Builder;->findNode(Landroid/renderscript/Script$KernelID;)Landroid/renderscript/ScriptGroup$Node;

    #@13
    move-result-object v1

    #@14
    if-eqz v1, :cond_17

    #@16
    .line 294
    :goto_16
    return-object p0

    #@17
    .line 286
    :cond_17
    iget v1, p0, Landroid/renderscript/ScriptGroup$Builder;->mKernelCount:I

    #@19
    add-int/lit8 v1, v1, 0x1

    #@1b
    iput v1, p0, Landroid/renderscript/ScriptGroup$Builder;->mKernelCount:I

    #@1d
    .line 287
    iget-object v1, p1, Landroid/renderscript/Script$KernelID;->mScript:Landroid/renderscript/Script;

    #@1f
    invoke-direct {p0, v1}, Landroid/renderscript/ScriptGroup$Builder;->findNode(Landroid/renderscript/Script;)Landroid/renderscript/ScriptGroup$Node;

    #@22
    move-result-object v0

    #@23
    .line 288
    .local v0, n:Landroid/renderscript/ScriptGroup$Node;
    if-nez v0, :cond_31

    #@25
    .line 290
    new-instance v0, Landroid/renderscript/ScriptGroup$Node;

    #@27
    .end local v0           #n:Landroid/renderscript/ScriptGroup$Node;
    iget-object v1, p1, Landroid/renderscript/Script$KernelID;->mScript:Landroid/renderscript/Script;

    #@29
    invoke-direct {v0, v1}, Landroid/renderscript/ScriptGroup$Node;-><init>(Landroid/renderscript/Script;)V

    #@2c
    .line 291
    .restart local v0       #n:Landroid/renderscript/ScriptGroup$Node;
    iget-object v1, p0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@31
    .line 293
    :cond_31
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Node;->mKernels:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@36
    goto :goto_16
.end method

.method public create()Landroid/renderscript/ScriptGroup;
    .registers 23

    #@0
    .prologue
    .line 377
    move-object/from16 v0, p0

    #@2
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_14

    #@a
    .line 378
    new-instance v1, Landroid/renderscript/RSInvalidStateException;

    #@c
    const-string v21, "Empty script groups are not allowed"

    #@e
    move-object/from16 v0, v21

    #@10
    invoke-direct {v1, v0}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 382
    :cond_14
    const/4 v8, 0x0

    #@15
    .local v8, ct:I
    :goto_15
    move-object/from16 v0, p0

    #@17
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v1

    #@1d
    if-ge v8, v1, :cond_32

    #@1f
    .line 383
    move-object/from16 v0, p0

    #@21
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Landroid/renderscript/ScriptGroup$Node;

    #@29
    const/16 v21, 0x0

    #@2b
    move/from16 v0, v21

    #@2d
    iput v0, v1, Landroid/renderscript/ScriptGroup$Node;->dagNumber:I

    #@2f
    .line 382
    add-int/lit8 v8, v8, 0x1

    #@31
    goto :goto_15

    #@32
    .line 385
    :cond_32
    invoke-direct/range {p0 .. p0}, Landroid/renderscript/ScriptGroup$Builder;->validateDAG()V

    #@35
    .line 387
    new-instance v16, Ljava/util/ArrayList;

    #@37
    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    #@3a
    .line 388
    .local v16, inputs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/renderscript/ScriptGroup$IO;>;"
    new-instance v19, Ljava/util/ArrayList;

    #@3c
    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    #@3f
    .line 390
    .local v19, outputs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/renderscript/ScriptGroup$IO;>;"
    move-object/from16 v0, p0

    #@41
    iget v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mKernelCount:I

    #@43
    new-array v2, v1, [I

    #@45
    .line 391
    .local v2, kernels:[I
    const/4 v14, 0x0

    #@46
    .line 392
    .local v14, idx:I
    const/4 v8, 0x0

    #@47
    :goto_47
    move-object/from16 v0, p0

    #@49
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@4b
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@4e
    move-result v1

    #@4f
    if-ge v8, v1, :cond_e3

    #@51
    .line 393
    move-object/from16 v0, p0

    #@53
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mNodes:Ljava/util/ArrayList;

    #@55
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@58
    move-result-object v18

    #@59
    check-cast v18, Landroid/renderscript/ScriptGroup$Node;

    #@5b
    .line 394
    .local v18, n:Landroid/renderscript/ScriptGroup$Node;
    const/4 v9, 0x0

    #@5c
    .local v9, ct2:I
    :goto_5c
    move-object/from16 v0, v18

    #@5e
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Node;->mKernels:Ljava/util/ArrayList;

    #@60
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@63
    move-result v1

    #@64
    if-ge v9, v1, :cond_df

    #@66
    .line 395
    move-object/from16 v0, v18

    #@68
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Node;->mKernels:Ljava/util/ArrayList;

    #@6a
    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6d
    move-result-object v17

    #@6e
    check-cast v17, Landroid/renderscript/Script$KernelID;

    #@70
    .line 396
    .local v17, kid:Landroid/renderscript/Script$KernelID;
    add-int/lit8 v15, v14, 0x1

    #@72
    .end local v14           #idx:I
    .local v15, idx:I
    move-object/from16 v0, p0

    #@74
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@76
    move-object/from16 v0, v17

    #@78
    invoke-virtual {v0, v1}, Landroid/renderscript/Script$KernelID;->getID(Landroid/renderscript/RenderScript;)I

    #@7b
    move-result v1

    #@7c
    aput v1, v2, v14

    #@7e
    .line 398
    const/4 v11, 0x0

    #@7f
    .line 399
    .local v11, hasInput:Z
    const/4 v12, 0x0

    #@80
    .line 400
    .local v12, hasOutput:Z
    const/4 v10, 0x0

    #@81
    .local v10, ct3:I
    :goto_81
    move-object/from16 v0, v18

    #@83
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Node;->mInputs:Ljava/util/ArrayList;

    #@85
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@88
    move-result v1

    #@89
    if-ge v10, v1, :cond_9f

    #@8b
    .line 401
    move-object/from16 v0, v18

    #@8d
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Node;->mInputs:Ljava/util/ArrayList;

    #@8f
    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@92
    move-result-object v1

    #@93
    check-cast v1, Landroid/renderscript/ScriptGroup$ConnectLine;

    #@95
    iget-object v1, v1, Landroid/renderscript/ScriptGroup$ConnectLine;->mToK:Landroid/renderscript/Script$KernelID;

    #@97
    move-object/from16 v0, v17

    #@99
    if-ne v1, v0, :cond_9c

    #@9b
    .line 402
    const/4 v11, 0x1

    #@9c
    .line 400
    :cond_9c
    add-int/lit8 v10, v10, 0x1

    #@9e
    goto :goto_81

    #@9f
    .line 405
    :cond_9f
    const/4 v10, 0x0

    #@a0
    :goto_a0
    move-object/from16 v0, v18

    #@a2
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Node;->mOutputs:Ljava/util/ArrayList;

    #@a4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@a7
    move-result v1

    #@a8
    if-ge v10, v1, :cond_be

    #@aa
    .line 406
    move-object/from16 v0, v18

    #@ac
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Node;->mOutputs:Ljava/util/ArrayList;

    #@ae
    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b1
    move-result-object v1

    #@b2
    check-cast v1, Landroid/renderscript/ScriptGroup$ConnectLine;

    #@b4
    iget-object v1, v1, Landroid/renderscript/ScriptGroup$ConnectLine;->mFrom:Landroid/renderscript/Script$KernelID;

    #@b6
    move-object/from16 v0, v17

    #@b8
    if-ne v1, v0, :cond_bb

    #@ba
    .line 407
    const/4 v12, 0x1

    #@bb
    .line 405
    :cond_bb
    add-int/lit8 v10, v10, 0x1

    #@bd
    goto :goto_a0

    #@be
    .line 410
    :cond_be
    if-nez v11, :cond_cc

    #@c0
    .line 411
    new-instance v1, Landroid/renderscript/ScriptGroup$IO;

    #@c2
    move-object/from16 v0, v17

    #@c4
    invoke-direct {v1, v0}, Landroid/renderscript/ScriptGroup$IO;-><init>(Landroid/renderscript/Script$KernelID;)V

    #@c7
    move-object/from16 v0, v16

    #@c9
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@cc
    .line 413
    :cond_cc
    if-nez v12, :cond_da

    #@ce
    .line 414
    new-instance v1, Landroid/renderscript/ScriptGroup$IO;

    #@d0
    move-object/from16 v0, v17

    #@d2
    invoke-direct {v1, v0}, Landroid/renderscript/ScriptGroup$IO;-><init>(Landroid/renderscript/Script$KernelID;)V

    #@d5
    move-object/from16 v0, v19

    #@d7
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@da
    .line 394
    :cond_da
    add-int/lit8 v9, v9, 0x1

    #@dc
    move v14, v15

    #@dd
    .end local v15           #idx:I
    .restart local v14       #idx:I
    goto/16 :goto_5c

    #@df
    .line 392
    .end local v10           #ct3:I
    .end local v11           #hasInput:Z
    .end local v12           #hasOutput:Z
    .end local v17           #kid:Landroid/renderscript/Script$KernelID;
    :cond_df
    add-int/lit8 v8, v8, 0x1

    #@e1
    goto/16 :goto_47

    #@e3
    .line 419
    .end local v9           #ct2:I
    .end local v18           #n:Landroid/renderscript/ScriptGroup$Node;
    :cond_e3
    move-object/from16 v0, p0

    #@e5
    iget v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mKernelCount:I

    #@e7
    if-eq v14, v1, :cond_f3

    #@e9
    .line 420
    new-instance v1, Landroid/renderscript/RSRuntimeException;

    #@eb
    const-string v21, "Count mismatch, should not happen."

    #@ed
    move-object/from16 v0, v21

    #@ef
    invoke-direct {v1, v0}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@f2
    throw v1

    #@f3
    .line 423
    :cond_f3
    move-object/from16 v0, p0

    #@f5
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mLines:Ljava/util/ArrayList;

    #@f7
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@fa
    move-result v1

    #@fb
    new-array v3, v1, [I

    #@fd
    .line 424
    .local v3, src:[I
    move-object/from16 v0, p0

    #@ff
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mLines:Ljava/util/ArrayList;

    #@101
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@104
    move-result v1

    #@105
    new-array v4, v1, [I

    #@107
    .line 425
    .local v4, dstk:[I
    move-object/from16 v0, p0

    #@109
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mLines:Ljava/util/ArrayList;

    #@10b
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@10e
    move-result v1

    #@10f
    new-array v5, v1, [I

    #@111
    .line 426
    .local v5, dstf:[I
    move-object/from16 v0, p0

    #@113
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mLines:Ljava/util/ArrayList;

    #@115
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@118
    move-result v1

    #@119
    new-array v6, v1, [I

    #@11b
    .line 428
    .local v6, types:[I
    const/4 v8, 0x0

    #@11c
    :goto_11c
    move-object/from16 v0, p0

    #@11e
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mLines:Ljava/util/ArrayList;

    #@120
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@123
    move-result v1

    #@124
    if-ge v8, v1, :cond_17b

    #@126
    .line 429
    move-object/from16 v0, p0

    #@128
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mLines:Ljava/util/ArrayList;

    #@12a
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12d
    move-result-object v7

    #@12e
    check-cast v7, Landroid/renderscript/ScriptGroup$ConnectLine;

    #@130
    .line 430
    .local v7, cl:Landroid/renderscript/ScriptGroup$ConnectLine;
    iget-object v1, v7, Landroid/renderscript/ScriptGroup$ConnectLine;->mFrom:Landroid/renderscript/Script$KernelID;

    #@132
    move-object/from16 v0, p0

    #@134
    iget-object v0, v0, Landroid/renderscript/ScriptGroup$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@136
    move-object/from16 v21, v0

    #@138
    move-object/from16 v0, v21

    #@13a
    invoke-virtual {v1, v0}, Landroid/renderscript/Script$KernelID;->getID(Landroid/renderscript/RenderScript;)I

    #@13d
    move-result v1

    #@13e
    aput v1, v3, v8

    #@140
    .line 431
    iget-object v1, v7, Landroid/renderscript/ScriptGroup$ConnectLine;->mToK:Landroid/renderscript/Script$KernelID;

    #@142
    if-eqz v1, :cond_154

    #@144
    .line 432
    iget-object v1, v7, Landroid/renderscript/ScriptGroup$ConnectLine;->mToK:Landroid/renderscript/Script$KernelID;

    #@146
    move-object/from16 v0, p0

    #@148
    iget-object v0, v0, Landroid/renderscript/ScriptGroup$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@14a
    move-object/from16 v21, v0

    #@14c
    move-object/from16 v0, v21

    #@14e
    invoke-virtual {v1, v0}, Landroid/renderscript/Script$KernelID;->getID(Landroid/renderscript/RenderScript;)I

    #@151
    move-result v1

    #@152
    aput v1, v4, v8

    #@154
    .line 434
    :cond_154
    iget-object v1, v7, Landroid/renderscript/ScriptGroup$ConnectLine;->mToF:Landroid/renderscript/Script$FieldID;

    #@156
    if-eqz v1, :cond_168

    #@158
    .line 435
    iget-object v1, v7, Landroid/renderscript/ScriptGroup$ConnectLine;->mToF:Landroid/renderscript/Script$FieldID;

    #@15a
    move-object/from16 v0, p0

    #@15c
    iget-object v0, v0, Landroid/renderscript/ScriptGroup$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@15e
    move-object/from16 v21, v0

    #@160
    move-object/from16 v0, v21

    #@162
    invoke-virtual {v1, v0}, Landroid/renderscript/Script$FieldID;->getID(Landroid/renderscript/RenderScript;)I

    #@165
    move-result v1

    #@166
    aput v1, v5, v8

    #@168
    .line 437
    :cond_168
    iget-object v1, v7, Landroid/renderscript/ScriptGroup$ConnectLine;->mAllocationType:Landroid/renderscript/Type;

    #@16a
    move-object/from16 v0, p0

    #@16c
    iget-object v0, v0, Landroid/renderscript/ScriptGroup$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@16e
    move-object/from16 v21, v0

    #@170
    move-object/from16 v0, v21

    #@172
    invoke-virtual {v1, v0}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)I

    #@175
    move-result v1

    #@176
    aput v1, v6, v8

    #@178
    .line 428
    add-int/lit8 v8, v8, 0x1

    #@17a
    goto :goto_11c

    #@17b
    .line 440
    .end local v7           #cl:Landroid/renderscript/ScriptGroup$ConnectLine;
    :cond_17b
    move-object/from16 v0, p0

    #@17d
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@17f
    invoke-virtual/range {v1 .. v6}, Landroid/renderscript/RenderScript;->nScriptGroupCreate([I[I[I[I[I)I

    #@182
    move-result v13

    #@183
    .line 441
    .local v13, id:I
    if-nez v13, :cond_18f

    #@185
    .line 442
    new-instance v1, Landroid/renderscript/RSRuntimeException;

    #@187
    const-string v21, "Object creation error, should not happen."

    #@189
    move-object/from16 v0, v21

    #@18b
    invoke-direct {v1, v0}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@18e
    throw v1

    #@18f
    .line 445
    :cond_18f
    new-instance v20, Landroid/renderscript/ScriptGroup;

    #@191
    move-object/from16 v0, p0

    #@193
    iget-object v1, v0, Landroid/renderscript/ScriptGroup$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@195
    move-object/from16 v0, v20

    #@197
    invoke-direct {v0, v13, v1}, Landroid/renderscript/ScriptGroup;-><init>(ILandroid/renderscript/RenderScript;)V

    #@19a
    .line 446
    .local v20, sg:Landroid/renderscript/ScriptGroup;
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    #@19d
    move-result v1

    #@19e
    new-array v1, v1, [Landroid/renderscript/ScriptGroup$IO;

    #@1a0
    move-object/from16 v0, v20

    #@1a2
    iput-object v1, v0, Landroid/renderscript/ScriptGroup;->mOutputs:[Landroid/renderscript/ScriptGroup$IO;

    #@1a4
    .line 447
    const/4 v8, 0x0

    #@1a5
    :goto_1a5
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    #@1a8
    move-result v1

    #@1a9
    if-ge v8, v1, :cond_1be

    #@1ab
    .line 448
    move-object/from16 v0, v20

    #@1ad
    iget-object v0, v0, Landroid/renderscript/ScriptGroup;->mOutputs:[Landroid/renderscript/ScriptGroup$IO;

    #@1af
    move-object/from16 v21, v0

    #@1b1
    move-object/from16 v0, v19

    #@1b3
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b6
    move-result-object v1

    #@1b7
    check-cast v1, Landroid/renderscript/ScriptGroup$IO;

    #@1b9
    aput-object v1, v21, v8

    #@1bb
    .line 447
    add-int/lit8 v8, v8, 0x1

    #@1bd
    goto :goto_1a5

    #@1be
    .line 451
    :cond_1be
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    #@1c1
    move-result v1

    #@1c2
    new-array v1, v1, [Landroid/renderscript/ScriptGroup$IO;

    #@1c4
    move-object/from16 v0, v20

    #@1c6
    iput-object v1, v0, Landroid/renderscript/ScriptGroup;->mInputs:[Landroid/renderscript/ScriptGroup$IO;

    #@1c8
    .line 452
    const/4 v8, 0x0

    #@1c9
    :goto_1c9
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    #@1cc
    move-result v1

    #@1cd
    if-ge v8, v1, :cond_1e2

    #@1cf
    .line 453
    move-object/from16 v0, v20

    #@1d1
    iget-object v0, v0, Landroid/renderscript/ScriptGroup;->mInputs:[Landroid/renderscript/ScriptGroup$IO;

    #@1d3
    move-object/from16 v21, v0

    #@1d5
    move-object/from16 v0, v16

    #@1d7
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1da
    move-result-object v1

    #@1db
    check-cast v1, Landroid/renderscript/ScriptGroup$IO;

    #@1dd
    aput-object v1, v21, v8

    #@1df
    .line 452
    add-int/lit8 v8, v8, 0x1

    #@1e1
    goto :goto_1c9

    #@1e2
    .line 456
    :cond_1e2
    return-object v20
.end method
