.class public final Landroid/renderscript/ScriptGroup;
.super Landroid/renderscript/BaseObj;
.source "ScriptGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/ScriptGroup$Builder;,
        Landroid/renderscript/ScriptGroup$Node;,
        Landroid/renderscript/ScriptGroup$ConnectLine;,
        Landroid/renderscript/ScriptGroup$IO;
    }
.end annotation


# instance fields
.field mInputs:[Landroid/renderscript/ScriptGroup$IO;

.field mOutputs:[Landroid/renderscript/ScriptGroup$IO;


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 92
    return-void
.end method


# virtual methods
.method public execute()V
    .registers 3

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v1}, Landroid/renderscript/ScriptGroup;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScript;->nScriptGroupExecute(I)V

    #@b
    .line 141
    return-void
.end method

.method public setInput(Landroid/renderscript/Script$KernelID;Landroid/renderscript/Allocation;)V
    .registers 8
    .parameter "s"
    .parameter "a"

    #@0
    .prologue
    .line 104
    const/4 v0, 0x0

    #@1
    .local v0, ct:I
    :goto_1
    iget-object v1, p0, Landroid/renderscript/ScriptGroup;->mInputs:[Landroid/renderscript/ScriptGroup$IO;

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_2f

    #@6
    .line 105
    iget-object v1, p0, Landroid/renderscript/ScriptGroup;->mInputs:[Landroid/renderscript/ScriptGroup$IO;

    #@8
    aget-object v1, v1, v0

    #@a
    iget-object v1, v1, Landroid/renderscript/ScriptGroup$IO;->mKID:Landroid/renderscript/Script$KernelID;

    #@c
    if-ne v1, p1, :cond_2c

    #@e
    .line 106
    iget-object v1, p0, Landroid/renderscript/ScriptGroup;->mInputs:[Landroid/renderscript/ScriptGroup$IO;

    #@10
    aget-object v1, v1, v0

    #@12
    iput-object p2, v1, Landroid/renderscript/ScriptGroup$IO;->mAllocation:Landroid/renderscript/Allocation;

    #@14
    .line 107
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@16
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@18
    invoke-virtual {p0, v2}, Landroid/renderscript/ScriptGroup;->getID(Landroid/renderscript/RenderScript;)I

    #@1b
    move-result v2

    #@1c
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1e
    invoke-virtual {p1, v3}, Landroid/renderscript/Script$KernelID;->getID(Landroid/renderscript/RenderScript;)I

    #@21
    move-result v3

    #@22
    iget-object v4, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@24
    invoke-virtual {v4, p2}, Landroid/renderscript/RenderScript;->safeID(Landroid/renderscript/BaseObj;)I

    #@27
    move-result v4

    #@28
    invoke-virtual {v1, v2, v3, v4}, Landroid/renderscript/RenderScript;->nScriptGroupSetInput(III)V

    #@2b
    .line 108
    return-void

    #@2c
    .line 104
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_1

    #@2f
    .line 111
    :cond_2f
    new-instance v1, Landroid/renderscript/RSIllegalArgumentException;

    #@31
    const-string v2, "Script not found"

    #@33
    invoke-direct {v1, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@36
    throw v1
.end method

.method public setOutput(Landroid/renderscript/Script$KernelID;Landroid/renderscript/Allocation;)V
    .registers 8
    .parameter "s"
    .parameter "a"

    #@0
    .prologue
    .line 124
    const/4 v0, 0x0

    #@1
    .local v0, ct:I
    :goto_1
    iget-object v1, p0, Landroid/renderscript/ScriptGroup;->mOutputs:[Landroid/renderscript/ScriptGroup$IO;

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_2f

    #@6
    .line 125
    iget-object v1, p0, Landroid/renderscript/ScriptGroup;->mOutputs:[Landroid/renderscript/ScriptGroup$IO;

    #@8
    aget-object v1, v1, v0

    #@a
    iget-object v1, v1, Landroid/renderscript/ScriptGroup$IO;->mKID:Landroid/renderscript/Script$KernelID;

    #@c
    if-ne v1, p1, :cond_2c

    #@e
    .line 126
    iget-object v1, p0, Landroid/renderscript/ScriptGroup;->mOutputs:[Landroid/renderscript/ScriptGroup$IO;

    #@10
    aget-object v1, v1, v0

    #@12
    iput-object p2, v1, Landroid/renderscript/ScriptGroup$IO;->mAllocation:Landroid/renderscript/Allocation;

    #@14
    .line 127
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@16
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@18
    invoke-virtual {p0, v2}, Landroid/renderscript/ScriptGroup;->getID(Landroid/renderscript/RenderScript;)I

    #@1b
    move-result v2

    #@1c
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1e
    invoke-virtual {p1, v3}, Landroid/renderscript/Script$KernelID;->getID(Landroid/renderscript/RenderScript;)I

    #@21
    move-result v3

    #@22
    iget-object v4, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@24
    invoke-virtual {v4, p2}, Landroid/renderscript/RenderScript;->safeID(Landroid/renderscript/BaseObj;)I

    #@27
    move-result v4

    #@28
    invoke-virtual {v1, v2, v3, v4}, Landroid/renderscript/RenderScript;->nScriptGroupSetOutput(III)V

    #@2b
    .line 128
    return-void

    #@2c
    .line 124
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_1

    #@2f
    .line 131
    :cond_2f
    new-instance v1, Landroid/renderscript/RSIllegalArgumentException;

    #@31
    const-string v2, "Script not found"

    #@33
    invoke-direct {v1, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@36
    throw v1
.end method
