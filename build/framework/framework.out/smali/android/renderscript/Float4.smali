.class public Landroid/renderscript/Float4;
.super Ljava/lang/Object;
.source "Float4.java"


# instance fields
.field public w:F

.field public x:F

.field public y:F

.field public z:F


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    return-void
.end method

.method public constructor <init>(FFFF)V
    .registers 5
    .parameter "initX"
    .parameter "initY"
    .parameter "initZ"
    .parameter "initW"

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 32
    iput p1, p0, Landroid/renderscript/Float4;->x:F

    #@5
    .line 33
    iput p2, p0, Landroid/renderscript/Float4;->y:F

    #@7
    .line 34
    iput p3, p0, Landroid/renderscript/Float4;->z:F

    #@9
    .line 35
    iput p4, p0, Landroid/renderscript/Float4;->w:F

    #@b
    .line 36
    return-void
.end method
