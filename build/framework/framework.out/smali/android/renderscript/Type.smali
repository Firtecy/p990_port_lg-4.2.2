.class public Landroid/renderscript/Type;
.super Landroid/renderscript/BaseObj;
.source "Type.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Type$Builder;,
        Landroid/renderscript/Type$CubemapFace;
    }
.end annotation


# instance fields
.field mDimFaces:Z

.field mDimMipmaps:Z

.field mDimX:I

.field mDimY:I

.field mDimZ:I

.field mElement:Landroid/renderscript/Element;

.field mElementCount:I


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 175
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 176
    return-void
.end method


# virtual methods
.method calcElementCount()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 137
    invoke-virtual {p0}, Landroid/renderscript/Type;->hasMipmaps()Z

    #@4
    move-result v2

    #@5
    .line 138
    .local v2, hasLod:Z
    invoke-virtual {p0}, Landroid/renderscript/Type;->getX()I

    #@8
    move-result v3

    #@9
    .line 139
    .local v3, x:I
    invoke-virtual {p0}, Landroid/renderscript/Type;->getY()I

    #@c
    move-result v4

    #@d
    .line 140
    .local v4, y:I
    invoke-virtual {p0}, Landroid/renderscript/Type;->getZ()I

    #@10
    move-result v5

    #@11
    .line 141
    .local v5, z:I
    const/4 v1, 0x1

    #@12
    .line 142
    .local v1, faces:I
    invoke-virtual {p0}, Landroid/renderscript/Type;->hasFaces()Z

    #@15
    move-result v6

    #@16
    if-eqz v6, :cond_19

    #@18
    .line 143
    const/4 v1, 0x6

    #@19
    .line 145
    :cond_19
    if-nez v3, :cond_1c

    #@1b
    .line 146
    const/4 v3, 0x1

    #@1c
    .line 148
    :cond_1c
    if-nez v4, :cond_1f

    #@1e
    .line 149
    const/4 v4, 0x1

    #@1f
    .line 151
    :cond_1f
    if-nez v5, :cond_22

    #@21
    .line 152
    const/4 v5, 0x1

    #@22
    .line 155
    :cond_22
    mul-int v6, v3, v4

    #@24
    mul-int/2addr v6, v5

    #@25
    mul-int v0, v6, v1

    #@27
    .line 157
    .local v0, count:I
    :goto_27
    if-eqz v2, :cond_41

    #@29
    if-gt v3, v7, :cond_2f

    #@2b
    if-gt v4, v7, :cond_2f

    #@2d
    if-le v5, v7, :cond_41

    #@2f
    .line 158
    :cond_2f
    if-le v3, v7, :cond_33

    #@31
    .line 159
    shr-int/lit8 v3, v3, 0x1

    #@33
    .line 161
    :cond_33
    if-le v4, v7, :cond_37

    #@35
    .line 162
    shr-int/lit8 v4, v4, 0x1

    #@37
    .line 164
    :cond_37
    if-le v5, v7, :cond_3b

    #@39
    .line 165
    shr-int/lit8 v5, v5, 0x1

    #@3b
    .line 168
    :cond_3b
    mul-int v6, v3, v4

    #@3d
    mul-int/2addr v6, v5

    #@3e
    mul-int/2addr v6, v1

    #@3f
    add-int/2addr v0, v6

    #@40
    goto :goto_27

    #@41
    .line 170
    :cond_41
    iput v0, p0, Landroid/renderscript/Type;->mElementCount:I

    #@43
    .line 171
    return-void
.end method

.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 133
    iget v0, p0, Landroid/renderscript/Type;->mElementCount:I

    #@2
    return v0
.end method

.method public getElement()Landroid/renderscript/Element;
    .registers 2

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@2
    return-object v0
.end method

.method public getX()I
    .registers 2

    #@0
    .prologue
    .line 88
    iget v0, p0, Landroid/renderscript/Type;->mDimX:I

    #@2
    return v0
.end method

.method public getY()I
    .registers 2

    #@0
    .prologue
    .line 97
    iget v0, p0, Landroid/renderscript/Type;->mDimY:I

    #@2
    return v0
.end method

.method public getZ()I
    .registers 2

    #@0
    .prologue
    .line 106
    iget v0, p0, Landroid/renderscript/Type;->mDimZ:I

    #@2
    return v0
.end method

.method public hasFaces()Z
    .registers 2

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Landroid/renderscript/Type;->mDimFaces:Z

    #@2
    return v0
.end method

.method public hasMipmaps()Z
    .registers 2

    #@0
    .prologue
    .line 115
    iget-boolean v0, p0, Landroid/renderscript/Type;->mDimMipmaps:Z

    #@2
    return v0
.end method

.method updateFromNative()V
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 182
    const/4 v2, 0x6

    #@3
    new-array v0, v2, [I

    #@5
    .line 183
    .local v0, dataBuffer:[I
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@7
    iget-object v5, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@9
    invoke-virtual {p0, v5}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)I

    #@c
    move-result v5

    #@d
    invoke-virtual {v2, v5, v0}, Landroid/renderscript/RenderScript;->nTypeGetNativeData(I[I)V

    #@10
    .line 185
    aget v2, v0, v4

    #@12
    iput v2, p0, Landroid/renderscript/Type;->mDimX:I

    #@14
    .line 186
    aget v2, v0, v3

    #@16
    iput v2, p0, Landroid/renderscript/Type;->mDimY:I

    #@18
    .line 187
    const/4 v2, 0x2

    #@19
    aget v2, v0, v2

    #@1b
    iput v2, p0, Landroid/renderscript/Type;->mDimZ:I

    #@1d
    .line 188
    const/4 v2, 0x3

    #@1e
    aget v2, v0, v2

    #@20
    if-ne v2, v3, :cond_43

    #@22
    move v2, v3

    #@23
    :goto_23
    iput-boolean v2, p0, Landroid/renderscript/Type;->mDimMipmaps:Z

    #@25
    .line 189
    const/4 v2, 0x4

    #@26
    aget v2, v0, v2

    #@28
    if-ne v2, v3, :cond_45

    #@2a
    :goto_2a
    iput-boolean v3, p0, Landroid/renderscript/Type;->mDimFaces:Z

    #@2c
    .line 191
    const/4 v2, 0x5

    #@2d
    aget v1, v0, v2

    #@2f
    .line 192
    .local v1, elementID:I
    if-eqz v1, :cond_3f

    #@31
    .line 193
    new-instance v2, Landroid/renderscript/Element;

    #@33
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@35
    invoke-direct {v2, v1, v3}, Landroid/renderscript/Element;-><init>(ILandroid/renderscript/RenderScript;)V

    #@38
    iput-object v2, p0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@3a
    .line 194
    iget-object v2, p0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@3c
    invoke-virtual {v2}, Landroid/renderscript/Element;->updateFromNative()V

    #@3f
    .line 196
    :cond_3f
    invoke-virtual {p0}, Landroid/renderscript/Type;->calcElementCount()V

    #@42
    .line 197
    return-void

    #@43
    .end local v1           #elementID:I
    :cond_43
    move v2, v4

    #@44
    .line 188
    goto :goto_23

    #@45
    :cond_45
    move v3, v4

    #@46
    .line 189
    goto :goto_2a
.end method
