.class public final Landroid/renderscript/ScriptIntrinsicConvolve5x5;
.super Landroid/renderscript/ScriptIntrinsic;
.source "ScriptIntrinsicConvolve5x5.java"


# instance fields
.field private mInput:Landroid/renderscript/Allocation;

.field private final mValues:[F


# direct methods
.method private constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 4
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/renderscript/ScriptIntrinsic;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 26
    const/16 v0, 0x19

    #@5
    new-array v0, v0, [F

    #@7
    iput-object v0, p0, Landroid/renderscript/ScriptIntrinsicConvolve5x5;->mValues:[F

    #@9
    .line 31
    return-void
.end method

.method public static create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicConvolve5x5;
    .registers 5
    .parameter "rs"
    .parameter "e"

    #@0
    .prologue
    .line 51
    const/4 v1, 0x4

    #@1
    invoke-virtual {p1, p0}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@4
    move-result v2

    #@5
    invoke-virtual {p0, v1, v2}, Landroid/renderscript/RenderScript;->nScriptIntrinsicCreate(II)I

    #@8
    move-result v0

    #@9
    .line 52
    .local v0, id:I
    new-instance v1, Landroid/renderscript/ScriptIntrinsicConvolve5x5;

    #@b
    invoke-direct {v1, v0, p0}, Landroid/renderscript/ScriptIntrinsicConvolve5x5;-><init>(ILandroid/renderscript/RenderScript;)V

    #@e
    return-object v1
.end method


# virtual methods
.method public forEach(Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "aout"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 98
    const/4 v0, 0x0

    #@2
    invoke-virtual {p0, v0, v1, p1, v1}, Landroid/renderscript/ScriptIntrinsicConvolve5x5;->forEach(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;Landroid/renderscript/FieldPacker;)V

    #@5
    .line 99
    return-void
.end method

.method public getFieldID_Input()Landroid/renderscript/Script$FieldID;
    .registers 3

    #@0
    .prologue
    .line 116
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/ScriptIntrinsicConvolve5x5;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getKernelID()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 107
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicConvolve5x5;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public setCoefficients([F)V
    .registers 6
    .parameter "v"

    #@0
    .prologue
    .line 82
    new-instance v1, Landroid/renderscript/FieldPacker;

    #@2
    const/16 v2, 0x64

    #@4
    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    #@7
    .line 83
    .local v1, fp:Landroid/renderscript/FieldPacker;
    const/4 v0, 0x0

    #@8
    .local v0, ct:I
    :goto_8
    iget-object v2, p0, Landroid/renderscript/ScriptIntrinsicConvolve5x5;->mValues:[F

    #@a
    array-length v2, v2

    #@b
    if-ge v0, v2, :cond_1d

    #@d
    .line 84
    iget-object v2, p0, Landroid/renderscript/ScriptIntrinsicConvolve5x5;->mValues:[F

    #@f
    aget v3, p1, v0

    #@11
    aput v3, v2, v0

    #@13
    .line 85
    iget-object v2, p0, Landroid/renderscript/ScriptIntrinsicConvolve5x5;->mValues:[F

    #@15
    aget v2, v2, v0

    #@17
    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@1a
    .line 83
    add-int/lit8 v0, v0, 0x1

    #@1c
    goto :goto_8

    #@1d
    .line 87
    :cond_1d
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p0, v2, v1}, Landroid/renderscript/ScriptIntrinsicConvolve5x5;->setVar(ILandroid/renderscript/FieldPacker;)V

    #@21
    .line 88
    return-void
.end method

.method public setInput(Landroid/renderscript/Allocation;)V
    .registers 3
    .parameter "ain"

    #@0
    .prologue
    .line 63
    iput-object p1, p0, Landroid/renderscript/ScriptIntrinsicConvolve5x5;->mInput:Landroid/renderscript/Allocation;

    #@2
    .line 64
    const/4 v0, 0x1

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/ScriptIntrinsicConvolve5x5;->setVar(ILandroid/renderscript/BaseObj;)V

    #@6
    .line 65
    return-void
.end method
