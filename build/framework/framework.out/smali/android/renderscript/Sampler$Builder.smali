.class public Landroid/renderscript/Sampler$Builder;
.super Ljava/lang/Object;
.source "Sampler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Sampler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field mAniso:F

.field mMag:Landroid/renderscript/Sampler$Value;

.field mMin:Landroid/renderscript/Sampler$Value;

.field mRS:Landroid/renderscript/RenderScript;

.field mWrapR:Landroid/renderscript/Sampler$Value;

.field mWrapS:Landroid/renderscript/Sampler$Value;

.field mWrapT:Landroid/renderscript/Sampler$Value;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 231
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 232
    iput-object p1, p0, Landroid/renderscript/Sampler$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@5
    .line 233
    sget-object v0, Landroid/renderscript/Sampler$Value;->NEAREST:Landroid/renderscript/Sampler$Value;

    #@7
    iput-object v0, p0, Landroid/renderscript/Sampler$Builder;->mMin:Landroid/renderscript/Sampler$Value;

    #@9
    .line 234
    sget-object v0, Landroid/renderscript/Sampler$Value;->NEAREST:Landroid/renderscript/Sampler$Value;

    #@b
    iput-object v0, p0, Landroid/renderscript/Sampler$Builder;->mMag:Landroid/renderscript/Sampler$Value;

    #@d
    .line 235
    sget-object v0, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    #@f
    iput-object v0, p0, Landroid/renderscript/Sampler$Builder;->mWrapS:Landroid/renderscript/Sampler$Value;

    #@11
    .line 236
    sget-object v0, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    #@13
    iput-object v0, p0, Landroid/renderscript/Sampler$Builder;->mWrapT:Landroid/renderscript/Sampler$Value;

    #@15
    .line 237
    sget-object v0, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    #@17
    iput-object v0, p0, Landroid/renderscript/Sampler$Builder;->mWrapR:Landroid/renderscript/Sampler$Value;

    #@19
    .line 238
    const/high16 v0, 0x3f80

    #@1b
    iput v0, p0, Landroid/renderscript/Sampler$Builder;->mAniso:F

    #@1d
    .line 239
    return-void
.end method


# virtual methods
.method public create()Landroid/renderscript/Sampler;
    .registers 10

    #@0
    .prologue
    .line 285
    iget-object v0, p0, Landroid/renderscript/Sampler$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 286
    iget-object v0, p0, Landroid/renderscript/Sampler$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@7
    iget-object v1, p0, Landroid/renderscript/Sampler$Builder;->mMag:Landroid/renderscript/Sampler$Value;

    #@9
    iget v1, v1, Landroid/renderscript/Sampler$Value;->mID:I

    #@b
    iget-object v2, p0, Landroid/renderscript/Sampler$Builder;->mMin:Landroid/renderscript/Sampler$Value;

    #@d
    iget v2, v2, Landroid/renderscript/Sampler$Value;->mID:I

    #@f
    iget-object v3, p0, Landroid/renderscript/Sampler$Builder;->mWrapS:Landroid/renderscript/Sampler$Value;

    #@11
    iget v3, v3, Landroid/renderscript/Sampler$Value;->mID:I

    #@13
    iget-object v4, p0, Landroid/renderscript/Sampler$Builder;->mWrapT:Landroid/renderscript/Sampler$Value;

    #@15
    iget v4, v4, Landroid/renderscript/Sampler$Value;->mID:I

    #@17
    iget-object v5, p0, Landroid/renderscript/Sampler$Builder;->mWrapR:Landroid/renderscript/Sampler$Value;

    #@19
    iget v5, v5, Landroid/renderscript/Sampler$Value;->mID:I

    #@1b
    iget v6, p0, Landroid/renderscript/Sampler$Builder;->mAniso:F

    #@1d
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/RenderScript;->nSamplerCreate(IIIIIF)I

    #@20
    move-result v7

    #@21
    .line 288
    .local v7, id:I
    new-instance v8, Landroid/renderscript/Sampler;

    #@23
    iget-object v0, p0, Landroid/renderscript/Sampler$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@25
    invoke-direct {v8, v7, v0}, Landroid/renderscript/Sampler;-><init>(ILandroid/renderscript/RenderScript;)V

    #@28
    .line 289
    .local v8, sampler:Landroid/renderscript/Sampler;
    iget-object v0, p0, Landroid/renderscript/Sampler$Builder;->mMin:Landroid/renderscript/Sampler$Value;

    #@2a
    iput-object v0, v8, Landroid/renderscript/Sampler;->mMin:Landroid/renderscript/Sampler$Value;

    #@2c
    .line 290
    iget-object v0, p0, Landroid/renderscript/Sampler$Builder;->mMag:Landroid/renderscript/Sampler$Value;

    #@2e
    iput-object v0, v8, Landroid/renderscript/Sampler;->mMag:Landroid/renderscript/Sampler$Value;

    #@30
    .line 291
    iget-object v0, p0, Landroid/renderscript/Sampler$Builder;->mWrapS:Landroid/renderscript/Sampler$Value;

    #@32
    iput-object v0, v8, Landroid/renderscript/Sampler;->mWrapS:Landroid/renderscript/Sampler$Value;

    #@34
    .line 292
    iget-object v0, p0, Landroid/renderscript/Sampler$Builder;->mWrapT:Landroid/renderscript/Sampler$Value;

    #@36
    iput-object v0, v8, Landroid/renderscript/Sampler;->mWrapT:Landroid/renderscript/Sampler$Value;

    #@38
    .line 293
    iget-object v0, p0, Landroid/renderscript/Sampler$Builder;->mWrapR:Landroid/renderscript/Sampler$Value;

    #@3a
    iput-object v0, v8, Landroid/renderscript/Sampler;->mWrapR:Landroid/renderscript/Sampler$Value;

    #@3c
    .line 294
    iget v0, p0, Landroid/renderscript/Sampler$Builder;->mAniso:F

    #@3e
    iput v0, v8, Landroid/renderscript/Sampler;->mAniso:F

    #@40
    .line 295
    return-object v8
.end method

.method public setAnisotropy(F)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 277
    const/4 v0, 0x0

    #@1
    cmpl-float v0, p1, v0

    #@3
    if-ltz v0, :cond_8

    #@5
    .line 278
    iput p1, p0, Landroid/renderscript/Sampler$Builder;->mAniso:F

    #@7
    .line 282
    return-void

    #@8
    .line 280
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v1, "Invalid value"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0
.end method

.method public setMagnification(Landroid/renderscript/Sampler$Value;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 253
    sget-object v0, Landroid/renderscript/Sampler$Value;->NEAREST:Landroid/renderscript/Sampler$Value;

    #@2
    if-eq p1, v0, :cond_8

    #@4
    sget-object v0, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    #@6
    if-ne p1, v0, :cond_b

    #@8
    .line 254
    :cond_8
    iput-object p1, p0, Landroid/renderscript/Sampler$Builder;->mMag:Landroid/renderscript/Sampler$Value;

    #@a
    .line 258
    return-void

    #@b
    .line 256
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v1, "Invalid value"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0
.end method

.method public setMinification(Landroid/renderscript/Sampler$Value;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 242
    sget-object v0, Landroid/renderscript/Sampler$Value;->NEAREST:Landroid/renderscript/Sampler$Value;

    #@2
    if-eq p1, v0, :cond_10

    #@4
    sget-object v0, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    #@6
    if-eq p1, v0, :cond_10

    #@8
    sget-object v0, Landroid/renderscript/Sampler$Value;->LINEAR_MIP_LINEAR:Landroid/renderscript/Sampler$Value;

    #@a
    if-eq p1, v0, :cond_10

    #@c
    sget-object v0, Landroid/renderscript/Sampler$Value;->LINEAR_MIP_NEAREST:Landroid/renderscript/Sampler$Value;

    #@e
    if-ne p1, v0, :cond_13

    #@10
    .line 246
    :cond_10
    iput-object p1, p0, Landroid/renderscript/Sampler$Builder;->mMin:Landroid/renderscript/Sampler$Value;

    #@12
    .line 250
    return-void

    #@13
    .line 248
    :cond_13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@15
    const-string v1, "Invalid value"

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0
.end method

.method public setWrapS(Landroid/renderscript/Sampler$Value;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 261
    sget-object v0, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    #@2
    if-eq p1, v0, :cond_8

    #@4
    sget-object v0, Landroid/renderscript/Sampler$Value;->CLAMP:Landroid/renderscript/Sampler$Value;

    #@6
    if-ne p1, v0, :cond_b

    #@8
    .line 262
    :cond_8
    iput-object p1, p0, Landroid/renderscript/Sampler$Builder;->mWrapS:Landroid/renderscript/Sampler$Value;

    #@a
    .line 266
    return-void

    #@b
    .line 264
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v1, "Invalid value"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0
.end method

.method public setWrapT(Landroid/renderscript/Sampler$Value;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 269
    sget-object v0, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    #@2
    if-eq p1, v0, :cond_8

    #@4
    sget-object v0, Landroid/renderscript/Sampler$Value;->CLAMP:Landroid/renderscript/Sampler$Value;

    #@6
    if-ne p1, v0, :cond_b

    #@8
    .line 270
    :cond_8
    iput-object p1, p0, Landroid/renderscript/Sampler$Builder;->mWrapT:Landroid/renderscript/Sampler$Value;

    #@a
    .line 274
    return-void

    #@b
    .line 272
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v1, "Invalid value"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0
.end method
