.class public final Landroid/renderscript/ScriptIntrinsicYuvToRGB;
.super Landroid/renderscript/ScriptIntrinsic;
.source "ScriptIntrinsicYuvToRGB.java"


# instance fields
.field private mInput:Landroid/renderscript/Allocation;


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/renderscript/ScriptIntrinsic;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 32
    return-void
.end method

.method public static create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicYuvToRGB;
    .registers 6
    .parameter "rs"
    .parameter "e"

    #@0
    .prologue
    .line 46
    const/4 v2, 0x6

    #@1
    invoke-virtual {p1, p0}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@4
    move-result v3

    #@5
    invoke-virtual {p0, v2, v3}, Landroid/renderscript/RenderScript;->nScriptIntrinsicCreate(II)I

    #@8
    move-result v0

    #@9
    .line 47
    .local v0, id:I
    new-instance v1, Landroid/renderscript/ScriptIntrinsicYuvToRGB;

    #@b
    invoke-direct {v1, v0, p0}, Landroid/renderscript/ScriptIntrinsicYuvToRGB;-><init>(ILandroid/renderscript/RenderScript;)V

    #@e
    .line 48
    .local v1, si:Landroid/renderscript/ScriptIntrinsicYuvToRGB;
    return-object v1
.end method


# virtual methods
.method public forEach(Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "aout"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 69
    const/4 v0, 0x0

    #@2
    invoke-virtual {p0, v0, v1, p1, v1}, Landroid/renderscript/ScriptIntrinsicYuvToRGB;->forEach(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;Landroid/renderscript/FieldPacker;)V

    #@5
    .line 70
    return-void
.end method

.method public getFieldID_Input()Landroid/renderscript/Script$FieldID;
    .registers 3

    #@0
    .prologue
    .line 87
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/ScriptIntrinsicYuvToRGB;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getKernelID()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 78
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicYuvToRGB;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public setInput(Landroid/renderscript/Allocation;)V
    .registers 3
    .parameter "ain"

    #@0
    .prologue
    .line 58
    iput-object p1, p0, Landroid/renderscript/ScriptIntrinsicYuvToRGB;->mInput:Landroid/renderscript/Allocation;

    #@2
    .line 59
    const/4 v0, 0x0

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/ScriptIntrinsicYuvToRGB;->setVar(ILandroid/renderscript/BaseObj;)V

    #@6
    .line 60
    return-void
.end method
