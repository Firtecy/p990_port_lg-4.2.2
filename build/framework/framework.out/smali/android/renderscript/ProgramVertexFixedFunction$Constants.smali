.class public Landroid/renderscript/ProgramVertexFixedFunction$Constants;
.super Ljava/lang/Object;
.source "ProgramVertexFixedFunction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/ProgramVertexFixedFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Constants"
.end annotation


# static fields
.field static final MODELVIEW_OFFSET:I = 0x0

.field static final PROJECTION_OFFSET:I = 0x10

.field static final TEXTURE_OFFSET:I = 0x20


# instance fields
.field mAlloc:Landroid/renderscript/Allocation;

.field private mIOBuffer:Landroid/renderscript/FieldPacker;

.field mModel:Landroid/renderscript/Matrix4f;

.field mProjection:Landroid/renderscript/Matrix4f;

.field mTexture:Landroid/renderscript/Matrix4f;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;)V
    .registers 6
    .parameter "rs"

    #@0
    .prologue
    .line 221
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 222
    invoke-static {p1}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;->getConstantInputType(Landroid/renderscript/RenderScript;)Landroid/renderscript/Type;

    #@6
    move-result-object v1

    #@7
    .line 223
    .local v1, constInputType:Landroid/renderscript/Type;
    invoke-static {p1, v1}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;)Landroid/renderscript/Allocation;

    #@a
    move-result-object v2

    #@b
    iput-object v2, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mAlloc:Landroid/renderscript/Allocation;

    #@d
    .line 224
    invoke-virtual {v1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Landroid/renderscript/Element;->getBytesSize()I

    #@14
    move-result v2

    #@15
    invoke-virtual {v1}, Landroid/renderscript/Type;->getCount()I

    #@18
    move-result v3

    #@19
    mul-int v0, v2, v3

    #@1b
    .line 226
    .local v0, bufferSize:I
    new-instance v2, Landroid/renderscript/FieldPacker;

    #@1d
    invoke-direct {v2, v0}, Landroid/renderscript/FieldPacker;-><init>(I)V

    #@20
    iput-object v2, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mIOBuffer:Landroid/renderscript/FieldPacker;

    #@22
    .line 227
    new-instance v2, Landroid/renderscript/Matrix4f;

    #@24
    invoke-direct {v2}, Landroid/renderscript/Matrix4f;-><init>()V

    #@27
    iput-object v2, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mModel:Landroid/renderscript/Matrix4f;

    #@29
    .line 228
    new-instance v2, Landroid/renderscript/Matrix4f;

    #@2b
    invoke-direct {v2}, Landroid/renderscript/Matrix4f;-><init>()V

    #@2e
    iput-object v2, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mProjection:Landroid/renderscript/Matrix4f;

    #@30
    .line 229
    new-instance v2, Landroid/renderscript/Matrix4f;

    #@32
    invoke-direct {v2}, Landroid/renderscript/Matrix4f;-><init>()V

    #@35
    iput-object v2, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mTexture:Landroid/renderscript/Matrix4f;

    #@37
    .line 230
    new-instance v2, Landroid/renderscript/Matrix4f;

    #@39
    invoke-direct {v2}, Landroid/renderscript/Matrix4f;-><init>()V

    #@3c
    invoke-virtual {p0, v2}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setModelview(Landroid/renderscript/Matrix4f;)V

    #@3f
    .line 231
    new-instance v2, Landroid/renderscript/Matrix4f;

    #@41
    invoke-direct {v2}, Landroid/renderscript/Matrix4f;-><init>()V

    #@44
    invoke-virtual {p0, v2}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    #@47
    .line 232
    new-instance v2, Landroid/renderscript/Matrix4f;

    #@49
    invoke-direct {v2}, Landroid/renderscript/Matrix4f;-><init>()V

    #@4c
    invoke-virtual {p0, v2}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setTexture(Landroid/renderscript/Matrix4f;)V

    #@4f
    .line 233
    return-void
.end method

.method private addToBuffer(ILandroid/renderscript/Matrix4f;)V
    .registers 7
    .parameter "offset"
    .parameter "m"

    #@0
    .prologue
    .line 247
    iget-object v1, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mIOBuffer:Landroid/renderscript/FieldPacker;

    #@2
    invoke-virtual {v1, p1}, Landroid/renderscript/FieldPacker;->reset(I)V

    #@5
    .line 248
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    const/16 v1, 0x10

    #@8
    if-ge v0, v1, :cond_16

    #@a
    .line 249
    iget-object v1, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mIOBuffer:Landroid/renderscript/FieldPacker;

    #@c
    iget-object v2, p2, Landroid/renderscript/Matrix4f;->mMat:[F

    #@e
    aget v2, v2, v0

    #@10
    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@13
    .line 248
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_6

    #@16
    .line 251
    :cond_16
    iget-object v1, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mAlloc:Landroid/renderscript/Allocation;

    #@18
    const/4 v2, 0x0

    #@19
    iget-object v3, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mIOBuffer:Landroid/renderscript/FieldPacker;

    #@1b
    invoke-virtual {v1, v2, v3}, Landroid/renderscript/Allocation;->setFromFieldPacker(ILandroid/renderscript/FieldPacker;)V

    #@1e
    .line 252
    return-void
.end method


# virtual methods
.method public destroy()V
    .registers 2

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mAlloc:Landroid/renderscript/Allocation;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/Allocation;->destroy()V

    #@5
    .line 243
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mAlloc:Landroid/renderscript/Allocation;

    #@8
    .line 244
    return-void
.end method

.method getAllocation()Landroid/renderscript/Allocation;
    .registers 2

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mAlloc:Landroid/renderscript/Allocation;

    #@2
    return-object v0
.end method

.method public setModelview(Landroid/renderscript/Matrix4f;)V
    .registers 3
    .parameter "m"

    #@0
    .prologue
    .line 261
    iget-object v0, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mModel:Landroid/renderscript/Matrix4f;

    #@2
    invoke-virtual {v0, p1}, Landroid/renderscript/Matrix4f;->load(Landroid/renderscript/Matrix4f;)V

    #@5
    .line 262
    const/4 v0, 0x0

    #@6
    invoke-direct {p0, v0, p1}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->addToBuffer(ILandroid/renderscript/Matrix4f;)V

    #@9
    .line 263
    return-void
.end method

.method public setProjection(Landroid/renderscript/Matrix4f;)V
    .registers 3
    .parameter "m"

    #@0
    .prologue
    .line 272
    iget-object v0, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mProjection:Landroid/renderscript/Matrix4f;

    #@2
    invoke-virtual {v0, p1}, Landroid/renderscript/Matrix4f;->load(Landroid/renderscript/Matrix4f;)V

    #@5
    .line 273
    const/16 v0, 0x40

    #@7
    invoke-direct {p0, v0, p1}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->addToBuffer(ILandroid/renderscript/Matrix4f;)V

    #@a
    .line 274
    return-void
.end method

.method public setTexture(Landroid/renderscript/Matrix4f;)V
    .registers 3
    .parameter "m"

    #@0
    .prologue
    .line 286
    iget-object v0, p0, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->mTexture:Landroid/renderscript/Matrix4f;

    #@2
    invoke-virtual {v0, p1}, Landroid/renderscript/Matrix4f;->load(Landroid/renderscript/Matrix4f;)V

    #@5
    .line 287
    const/16 v0, 0x80

    #@7
    invoke-direct {p0, v0, p1}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->addToBuffer(ILandroid/renderscript/Matrix4f;)V

    #@a
    .line 288
    return-void
.end method
