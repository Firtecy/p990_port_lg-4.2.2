.class public final enum Landroid/renderscript/ProgramStore$BlendSrcFunc;
.super Ljava/lang/Enum;
.source "ProgramStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/ProgramStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BlendSrcFunc"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/renderscript/ProgramStore$BlendSrcFunc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/renderscript/ProgramStore$BlendSrcFunc;

.field public static final enum DST_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

.field public static final enum DST_COLOR:Landroid/renderscript/ProgramStore$BlendSrcFunc;

.field public static final enum ONE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

.field public static final enum ONE_MINUS_DST_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

.field public static final enum ONE_MINUS_DST_COLOR:Landroid/renderscript/ProgramStore$BlendSrcFunc;

.field public static final enum ONE_MINUS_SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

.field public static final enum SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

.field public static final enum SRC_ALPHA_SATURATE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

.field public static final enum ZERO:Landroid/renderscript/ProgramStore$BlendSrcFunc;


# instance fields
.field mID:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 97
    new-instance v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@7
    const-string v1, "ZERO"

    #@9
    invoke-direct {v0, v1, v4, v4}, Landroid/renderscript/ProgramStore$BlendSrcFunc;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ZERO:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@e
    .line 98
    new-instance v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@10
    const-string v1, "ONE"

    #@12
    invoke-direct {v0, v1, v5, v5}, Landroid/renderscript/ProgramStore$BlendSrcFunc;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@17
    .line 99
    new-instance v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@19
    const-string v1, "DST_COLOR"

    #@1b
    invoke-direct {v0, v1, v6, v6}, Landroid/renderscript/ProgramStore$BlendSrcFunc;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->DST_COLOR:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@20
    .line 100
    new-instance v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@22
    const-string v1, "ONE_MINUS_DST_COLOR"

    #@24
    invoke-direct {v0, v1, v7, v7}, Landroid/renderscript/ProgramStore$BlendSrcFunc;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE_MINUS_DST_COLOR:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@29
    .line 101
    new-instance v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@2b
    const-string v1, "SRC_ALPHA"

    #@2d
    invoke-direct {v0, v1, v8, v8}, Landroid/renderscript/ProgramStore$BlendSrcFunc;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@32
    .line 102
    new-instance v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@34
    const-string v1, "ONE_MINUS_SRC_ALPHA"

    #@36
    const/4 v2, 0x5

    #@37
    const/4 v3, 0x5

    #@38
    invoke-direct {v0, v1, v2, v3}, Landroid/renderscript/ProgramStore$BlendSrcFunc;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE_MINUS_SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@3d
    .line 103
    new-instance v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@3f
    const-string v1, "DST_ALPHA"

    #@41
    const/4 v2, 0x6

    #@42
    const/4 v3, 0x6

    #@43
    invoke-direct {v0, v1, v2, v3}, Landroid/renderscript/ProgramStore$BlendSrcFunc;-><init>(Ljava/lang/String;II)V

    #@46
    sput-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->DST_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@48
    .line 104
    new-instance v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@4a
    const-string v1, "ONE_MINUS_DST_ALPHA"

    #@4c
    const/4 v2, 0x7

    #@4d
    const/4 v3, 0x7

    #@4e
    invoke-direct {v0, v1, v2, v3}, Landroid/renderscript/ProgramStore$BlendSrcFunc;-><init>(Ljava/lang/String;II)V

    #@51
    sput-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE_MINUS_DST_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@53
    .line 105
    new-instance v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@55
    const-string v1, "SRC_ALPHA_SATURATE"

    #@57
    const/16 v2, 0x8

    #@59
    const/16 v3, 0x8

    #@5b
    invoke-direct {v0, v1, v2, v3}, Landroid/renderscript/ProgramStore$BlendSrcFunc;-><init>(Ljava/lang/String;II)V

    #@5e
    sput-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->SRC_ALPHA_SATURATE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@60
    .line 96
    const/16 v0, 0x9

    #@62
    new-array v0, v0, [Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@64
    sget-object v1, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ZERO:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@66
    aput-object v1, v0, v4

    #@68
    sget-object v1, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@6a
    aput-object v1, v0, v5

    #@6c
    sget-object v1, Landroid/renderscript/ProgramStore$BlendSrcFunc;->DST_COLOR:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@6e
    aput-object v1, v0, v6

    #@70
    sget-object v1, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE_MINUS_DST_COLOR:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@72
    aput-object v1, v0, v7

    #@74
    sget-object v1, Landroid/renderscript/ProgramStore$BlendSrcFunc;->SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@76
    aput-object v1, v0, v8

    #@78
    const/4 v1, 0x5

    #@79
    sget-object v2, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE_MINUS_SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@7b
    aput-object v2, v0, v1

    #@7d
    const/4 v1, 0x6

    #@7e
    sget-object v2, Landroid/renderscript/ProgramStore$BlendSrcFunc;->DST_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@80
    aput-object v2, v0, v1

    #@82
    const/4 v1, 0x7

    #@83
    sget-object v2, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE_MINUS_DST_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@85
    aput-object v2, v0, v1

    #@87
    const/16 v1, 0x8

    #@89
    sget-object v2, Landroid/renderscript/ProgramStore$BlendSrcFunc;->SRC_ALPHA_SATURATE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@8b
    aput-object v2, v0, v1

    #@8d
    sput-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->$VALUES:[Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@8f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 109
    iput p3, p0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->mID:I

    #@5
    .line 110
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/renderscript/ProgramStore$BlendSrcFunc;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 96
    const-class v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/renderscript/ProgramStore$BlendSrcFunc;
    .registers 1

    #@0
    .prologue
    .line 96
    sget-object v0, Landroid/renderscript/ProgramStore$BlendSrcFunc;->$VALUES:[Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@2
    invoke-virtual {v0}, [Landroid/renderscript/ProgramStore$BlendSrcFunc;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/renderscript/ProgramStore$BlendSrcFunc;

    #@8
    return-object v0
.end method
