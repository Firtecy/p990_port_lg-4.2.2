.class public Landroid/renderscript/Script$FieldBase;
.super Ljava/lang/Object;
.source "Script.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Script;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FieldBase"
.end annotation


# instance fields
.field protected mAllocation:Landroid/renderscript/Allocation;

.field protected mElement:Landroid/renderscript/Element;


# direct methods
.method protected constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 302
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 303
    return-void
.end method


# virtual methods
.method public getAllocation()Landroid/renderscript/Allocation;
    .registers 2

    #@0
    .prologue
    .line 314
    iget-object v0, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    #@2
    return-object v0
.end method

.method public getElement()Landroid/renderscript/Element;
    .registers 2

    #@0
    .prologue
    .line 306
    iget-object v0, p0, Landroid/renderscript/Script$FieldBase;->mElement:Landroid/renderscript/Element;

    #@2
    return-object v0
.end method

.method public getType()Landroid/renderscript/Type;
    .registers 2

    #@0
    .prologue
    .line 310
    iget-object v0, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected init(Landroid/renderscript/RenderScript;I)V
    .registers 5
    .parameter "rs"
    .parameter "dimx"

    #@0
    .prologue
    .line 295
    iget-object v0, p0, Landroid/renderscript/Script$FieldBase;->mElement:Landroid/renderscript/Element;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {p1, v0, p2, v1}, Landroid/renderscript/Allocation;->createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;II)Landroid/renderscript/Allocation;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    #@9
    .line 296
    return-void
.end method

.method protected init(Landroid/renderscript/RenderScript;II)V
    .registers 6
    .parameter "rs"
    .parameter "dimx"
    .parameter "usages"

    #@0
    .prologue
    .line 299
    iget-object v0, p0, Landroid/renderscript/Script$FieldBase;->mElement:Landroid/renderscript/Element;

    #@2
    or-int/lit8 v1, p3, 0x1

    #@4
    invoke-static {p1, v0, p2, v1}, Landroid/renderscript/Allocation;->createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;II)Landroid/renderscript/Allocation;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    #@a
    .line 300
    return-void
.end method

.method public updateAllocation()V
    .registers 1

    #@0
    .prologue
    .line 319
    return-void
.end method
