.class public Landroid/renderscript/ProgramRaster$Builder;
.super Ljava/lang/Object;
.source "ProgramRaster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/ProgramRaster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field mCullMode:Landroid/renderscript/ProgramRaster$CullMode;

.field mPointSprite:Z

.field mRS:Landroid/renderscript/RenderScript;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 129
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 130
    iput-object p1, p0, Landroid/renderscript/ProgramRaster$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@5
    .line 131
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/renderscript/ProgramRaster$Builder;->mPointSprite:Z

    #@8
    .line 132
    sget-object v0, Landroid/renderscript/ProgramRaster$CullMode;->BACK:Landroid/renderscript/ProgramRaster$CullMode;

    #@a
    iput-object v0, p0, Landroid/renderscript/ProgramRaster$Builder;->mCullMode:Landroid/renderscript/ProgramRaster$CullMode;

    #@c
    .line 133
    return-void
.end method


# virtual methods
.method public create()Landroid/renderscript/ProgramRaster;
    .registers 6

    #@0
    .prologue
    .line 155
    iget-object v2, p0, Landroid/renderscript/ProgramRaster$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v2}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 156
    iget-object v2, p0, Landroid/renderscript/ProgramRaster$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@7
    iget-boolean v3, p0, Landroid/renderscript/ProgramRaster$Builder;->mPointSprite:Z

    #@9
    iget-object v4, p0, Landroid/renderscript/ProgramRaster$Builder;->mCullMode:Landroid/renderscript/ProgramRaster$CullMode;

    #@b
    iget v4, v4, Landroid/renderscript/ProgramRaster$CullMode;->mID:I

    #@d
    invoke-virtual {v2, v3, v4}, Landroid/renderscript/RenderScript;->nProgramRasterCreate(ZI)I

    #@10
    move-result v0

    #@11
    .line 157
    .local v0, id:I
    new-instance v1, Landroid/renderscript/ProgramRaster;

    #@13
    iget-object v2, p0, Landroid/renderscript/ProgramRaster$Builder;->mRS:Landroid/renderscript/RenderScript;

    #@15
    invoke-direct {v1, v0, v2}, Landroid/renderscript/ProgramRaster;-><init>(ILandroid/renderscript/RenderScript;)V

    #@18
    .line 158
    .local v1, programRaster:Landroid/renderscript/ProgramRaster;
    iget-boolean v2, p0, Landroid/renderscript/ProgramRaster$Builder;->mPointSprite:Z

    #@1a
    iput-boolean v2, v1, Landroid/renderscript/ProgramRaster;->mPointSprite:Z

    #@1c
    .line 159
    iget-object v2, p0, Landroid/renderscript/ProgramRaster$Builder;->mCullMode:Landroid/renderscript/ProgramRaster$CullMode;

    #@1e
    iput-object v2, v1, Landroid/renderscript/ProgramRaster;->mCullMode:Landroid/renderscript/ProgramRaster$CullMode;

    #@20
    .line 160
    return-object v1
.end method

.method public setCullMode(Landroid/renderscript/ProgramRaster$CullMode;)Landroid/renderscript/ProgramRaster$Builder;
    .registers 2
    .parameter "m"

    #@0
    .prologue
    .line 147
    iput-object p1, p0, Landroid/renderscript/ProgramRaster$Builder;->mCullMode:Landroid/renderscript/ProgramRaster$CullMode;

    #@2
    .line 148
    return-object p0
.end method

.method public setPointSpriteEnabled(Z)Landroid/renderscript/ProgramRaster$Builder;
    .registers 2
    .parameter "enable"

    #@0
    .prologue
    .line 139
    iput-boolean p1, p0, Landroid/renderscript/ProgramRaster$Builder;->mPointSprite:Z

    #@2
    .line 140
    return-object p0
.end method
