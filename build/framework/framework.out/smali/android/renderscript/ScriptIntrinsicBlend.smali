.class public Landroid/renderscript/ScriptIntrinsicBlend;
.super Landroid/renderscript/ScriptIntrinsic;
.source "ScriptIntrinsicBlend.java"


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/renderscript/ScriptIntrinsic;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 27
    return-void
.end method

.method private blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 6
    .parameter "id"
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 46
    invoke-virtual {p2}, Landroid/renderscript/Allocation;->getElement()Landroid/renderscript/Element;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@6
    invoke-static {v1}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Landroid/renderscript/Element;->isCompatible(Landroid/renderscript/Element;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_18

    #@10
    .line 47
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@12
    const-string v1, "Input is not of expected format."

    #@14
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 49
    :cond_18
    invoke-virtual {p3}, Landroid/renderscript/Allocation;->getElement()Landroid/renderscript/Element;

    #@1b
    move-result-object v0

    #@1c
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1e
    invoke-static {v1}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v0, v1}, Landroid/renderscript/Element;->isCompatible(Landroid/renderscript/Element;)Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_30

    #@28
    .line 50
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@2a
    const-string v1, "Output is not of expected format."

    #@2c
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2f
    throw v0

    #@30
    .line 52
    :cond_30
    const/4 v0, 0x0

    #@31
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/renderscript/ScriptIntrinsicBlend;->forEach(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;Landroid/renderscript/FieldPacker;)V

    #@34
    .line 53
    return-void
.end method

.method public static create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicBlend;
    .registers 5
    .parameter "rs"
    .parameter "e"

    #@0
    .prologue
    .line 40
    const/4 v1, 0x7

    #@1
    invoke-virtual {p1, p0}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@4
    move-result v2

    #@5
    invoke-virtual {p0, v1, v2}, Landroid/renderscript/RenderScript;->nScriptIntrinsicCreate(II)I

    #@8
    move-result v0

    #@9
    .line 41
    .local v0, id:I
    new-instance v1, Landroid/renderscript/ScriptIntrinsicBlend;

    #@b
    invoke-direct {v1, v0, p0}, Landroid/renderscript/ScriptIntrinsicBlend;-><init>(ILandroid/renderscript/RenderScript;)V

    #@e
    return-object v1
.end method


# virtual methods
.method public forEachAdd(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 400
    const/16 v0, 0x22

    #@2
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@5
    .line 401
    return-void
.end method

.method public forEachClear(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 62
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@4
    .line 63
    return-void
.end method

.method public forEachDst(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 3
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 103
    return-void
.end method

.method public forEachDstAtop(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 256
    const/16 v0, 0xa

    #@2
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@5
    .line 257
    return-void
.end method

.method public forEachDstIn(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 178
    const/4 v0, 0x6

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@4
    .line 179
    return-void
.end method

.method public forEachDstOut(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 216
    const/16 v0, 0x8

    #@2
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@5
    .line 217
    return-void
.end method

.method public forEachDstOver(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 140
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@4
    .line 141
    return-void
.end method

.method public forEachMultiply(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 304
    const/16 v0, 0xe

    #@2
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@5
    .line 305
    return-void
.end method

.method public forEachSrc(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 82
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@4
    .line 83
    return-void
.end method

.method public forEachSrcAtop(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 236
    const/16 v0, 0x9

    #@2
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@5
    .line 237
    return-void
.end method

.method public forEachSrcIn(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 159
    const/4 v0, 0x5

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@4
    .line 160
    return-void
.end method

.method public forEachSrcOut(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 197
    const/4 v0, 0x7

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@4
    .line 198
    return-void
.end method

.method public forEachSrcOver(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 121
    const/4 v0, 0x3

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@4
    .line 122
    return-void
.end method

.method public forEachSubtract(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 419
    const/16 v0, 0x23

    #@2
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@5
    .line 420
    return-void
.end method

.method public forEachXor(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "ain"
    .parameter "aout"

    #@0
    .prologue
    .line 275
    const/16 v0, 0xb

    #@2
    invoke-direct {p0, v0, p1, p2}, Landroid/renderscript/ScriptIntrinsicBlend;->blend(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    #@5
    .line 276
    return-void
.end method

.method public getKernelIDAdd()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 409
    const/16 v0, 0x22

    #@3
    const/4 v1, 0x3

    #@4
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getKernelIDClear()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 71
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getKernelIDDst()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 111
    const/4 v0, 0x2

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getKernelIDDstAtop()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 265
    const/16 v0, 0xa

    #@3
    const/4 v1, 0x3

    #@4
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getKernelIDDstIn()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 187
    const/4 v0, 0x6

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getKernelIDDstOut()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 225
    const/16 v0, 0x8

    #@3
    const/4 v1, 0x3

    #@4
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getKernelIDDstOver()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 149
    const/4 v0, 0x4

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getKernelIDMultiply()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 313
    const/16 v0, 0xe

    #@3
    const/4 v1, 0x3

    #@4
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getKernelIDSrc()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 91
    const/4 v0, 0x1

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getKernelIDSrcAtop()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 245
    const/16 v0, 0x9

    #@3
    const/4 v1, 0x3

    #@4
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getKernelIDSrcIn()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 168
    const/4 v0, 0x5

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getKernelIDSrcOut()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 206
    const/4 v0, 0x7

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getKernelIDSrcOver()Landroid/renderscript/Script$KernelID;
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x3

    #@2
    .line 130
    invoke-virtual {p0, v0, v0, v1, v1}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getKernelIDSubtract()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 428
    const/16 v0, 0x23

    #@3
    const/4 v1, 0x3

    #@4
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getKernelIDXor()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 284
    const/16 v0, 0xb

    #@3
    const/4 v1, 0x3

    #@4
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicBlend;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method
