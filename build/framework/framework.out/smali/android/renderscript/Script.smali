.class public Landroid/renderscript/Script;
.super Landroid/renderscript/BaseObj;
.source "Script.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Script$FieldBase;,
        Landroid/renderscript/Script$Builder;,
        Landroid/renderscript/Script$FieldID;,
        Landroid/renderscript/Script$KernelID;
    }
.end annotation


# instance fields
.field private final mFIDs:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/renderscript/Script$FieldID;",
            ">;"
        }
    .end annotation
.end field

.field private final mKIDs:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/renderscript/Script$KernelID;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 4
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 171
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 46
    new-instance v0, Landroid/util/SparseArray;

    #@5
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@8
    iput-object v0, p0, Landroid/renderscript/Script;->mKIDs:Landroid/util/SparseArray;

    #@a
    .line 92
    new-instance v0, Landroid/util/SparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@f
    iput-object v0, p0, Landroid/renderscript/Script;->mFIDs:Landroid/util/SparseArray;

    #@11
    .line 172
    return-void
.end method


# virtual methods
.method public bindAllocation(Landroid/renderscript/Allocation;I)V
    .registers 6
    .parameter "va"
    .parameter "slot"

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 183
    if-eqz p1, :cond_19

    #@7
    .line 184
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@9
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@b
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@e
    move-result v1

    #@f
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@11
    invoke-virtual {p1, v2}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@14
    move-result v2

    #@15
    invoke-virtual {v0, v1, v2, p2}, Landroid/renderscript/RenderScript;->nScriptBindAllocation(III)V

    #@18
    .line 188
    :goto_18
    return-void

    #@19
    .line 186
    :cond_19
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1b
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1d
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@20
    move-result v1

    #@21
    const/4 v2, 0x0

    #@22
    invoke-virtual {v0, v1, v2, p2}, Landroid/renderscript/RenderScript;->nScriptBindAllocation(III)V

    #@25
    goto :goto_18
.end method

.method protected createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;
    .registers 8
    .parameter "slot"
    .parameter "e"

    #@0
    .prologue
    .line 102
    iget-object v3, p0, Landroid/renderscript/Script;->mFIDs:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/Script$FieldID;

    #@8
    .line 103
    .local v0, f:Landroid/renderscript/Script$FieldID;
    if-eqz v0, :cond_c

    #@a
    move-object v1, v0

    #@b
    .line 114
    .end local v0           #f:Landroid/renderscript/Script$FieldID;
    .local v1, f:Ljava/lang/Object;
    :goto_b
    return-object v1

    #@c
    .line 107
    .end local v1           #f:Ljava/lang/Object;
    .restart local v0       #f:Landroid/renderscript/Script$FieldID;
    :cond_c
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@e
    iget-object v4, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@10
    invoke-virtual {p0, v4}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@13
    move-result v4

    #@14
    invoke-virtual {v3, v4, p1}, Landroid/renderscript/RenderScript;->nScriptFieldIDCreate(II)I

    #@17
    move-result v2

    #@18
    .line 108
    .local v2, id:I
    if-nez v2, :cond_22

    #@1a
    .line 109
    new-instance v3, Landroid/renderscript/RSDriverException;

    #@1c
    const-string v4, "Failed to create FieldID"

    #@1e
    invoke-direct {v3, v4}, Landroid/renderscript/RSDriverException;-><init>(Ljava/lang/String;)V

    #@21
    throw v3

    #@22
    .line 112
    :cond_22
    new-instance v0, Landroid/renderscript/Script$FieldID;

    #@24
    .end local v0           #f:Landroid/renderscript/Script$FieldID;
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@26
    invoke-direct {v0, v2, v3, p0, p1}, Landroid/renderscript/Script$FieldID;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Script;I)V

    #@29
    .line 113
    .restart local v0       #f:Landroid/renderscript/Script$FieldID;
    iget-object v3, p0, Landroid/renderscript/Script;->mFIDs:Landroid/util/SparseArray;

    #@2b
    invoke-virtual {v3, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@2e
    move-object v1, v0

    #@2f
    .line 114
    .restart local v1       #f:Ljava/lang/Object;
    goto :goto_b
.end method

.method protected createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;
    .registers 12
    .parameter "slot"
    .parameter "sig"
    .parameter "ein"
    .parameter "eout"

    #@0
    .prologue
    .line 59
    iget-object v2, p0, Landroid/renderscript/Script;->mKIDs:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/Script$KernelID;

    #@8
    .line 60
    .local v0, k:Landroid/renderscript/Script$KernelID;
    if-eqz v0, :cond_c

    #@a
    move-object v6, v0

    #@b
    .line 71
    .end local v0           #k:Landroid/renderscript/Script$KernelID;
    .local v6, k:Ljava/lang/Object;
    :goto_b
    return-object v6

    #@c
    .line 64
    .end local v6           #k:Ljava/lang/Object;
    .restart local v0       #k:Landroid/renderscript/Script$KernelID;
    :cond_c
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@e
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@10
    invoke-virtual {p0, v3}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@13
    move-result v3

    #@14
    invoke-virtual {v2, v3, p1, p2}, Landroid/renderscript/RenderScript;->nScriptKernelIDCreate(III)I

    #@17
    move-result v1

    #@18
    .line 65
    .local v1, id:I
    if-nez v1, :cond_22

    #@1a
    .line 66
    new-instance v2, Landroid/renderscript/RSDriverException;

    #@1c
    const-string v3, "Failed to create KernelID"

    #@1e
    invoke-direct {v2, v3}, Landroid/renderscript/RSDriverException;-><init>(Ljava/lang/String;)V

    #@21
    throw v2

    #@22
    .line 69
    :cond_22
    new-instance v0, Landroid/renderscript/Script$KernelID;

    #@24
    .end local v0           #k:Landroid/renderscript/Script$KernelID;
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@26
    move-object v3, p0

    #@27
    move v4, p1

    #@28
    move v5, p2

    #@29
    invoke-direct/range {v0 .. v5}, Landroid/renderscript/Script$KernelID;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Script;II)V

    #@2c
    .line 70
    .restart local v0       #k:Landroid/renderscript/Script$KernelID;
    iget-object v2, p0, Landroid/renderscript/Script;->mKIDs:Landroid/util/SparseArray;

    #@2e
    invoke-virtual {v2, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@31
    move-object v6, v0

    #@32
    .line 71
    .restart local v6       #k:Ljava/lang/Object;
    goto :goto_b
.end method

.method protected forEach(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;Landroid/renderscript/FieldPacker;)V
    .registers 11
    .parameter "slot"
    .parameter "ain"
    .parameter "aout"
    .parameter "v"

    #@0
    .prologue
    .line 150
    if-nez p2, :cond_c

    #@2
    if-nez p3, :cond_c

    #@4
    .line 151
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@6
    const-string v1, "At least one of ain or aout is required to be non-null."

    #@8
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 154
    :cond_c
    const/4 v3, 0x0

    #@d
    .line 155
    .local v3, in_id:I
    if-eqz p2, :cond_15

    #@f
    .line 156
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@11
    invoke-virtual {p2, v0}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@14
    move-result v3

    #@15
    .line 158
    :cond_15
    const/4 v4, 0x0

    #@16
    .line 159
    .local v4, out_id:I
    if-eqz p3, :cond_1e

    #@18
    .line 160
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1a
    invoke-virtual {p3, v0}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@1d
    move-result v4

    #@1e
    .line 162
    :cond_1e
    const/4 v5, 0x0

    #@1f
    .line 163
    .local v5, params:[B
    if-eqz p4, :cond_25

    #@21
    .line 164
    invoke-virtual {p4}, Landroid/renderscript/FieldPacker;->getData()[B

    #@24
    move-result-object v5

    #@25
    .line 166
    :cond_25
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@27
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@29
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@2c
    move-result v1

    #@2d
    move v2, p1

    #@2e
    invoke-virtual/range {v0 .. v5}, Landroid/renderscript/RenderScript;->nScriptForEach(IIII[B)V

    #@31
    .line 167
    return-void
.end method

.method protected invoke(I)V
    .registers 4
    .parameter "slot"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1, p1}, Landroid/renderscript/RenderScript;->nScriptInvoke(II)V

    #@b
    .line 125
    return-void
.end method

.method protected invoke(ILandroid/renderscript/FieldPacker;)V
    .registers 6
    .parameter "slot"
    .parameter "v"

    #@0
    .prologue
    .line 134
    if-eqz p2, :cond_12

    #@2
    .line 135
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@6
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@9
    move-result v1

    #@a
    invoke-virtual {p2}, Landroid/renderscript/FieldPacker;->getData()[B

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v0, v1, p1, v2}, Landroid/renderscript/RenderScript;->nScriptInvokeV(II[B)V

    #@11
    .line 139
    :goto_11
    return-void

    #@12
    .line 137
    :cond_12
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@14
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@16
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@19
    move-result v1

    #@1a
    invoke-virtual {v0, v1, p1}, Landroid/renderscript/RenderScript;->nScriptInvoke(II)V

    #@1d
    goto :goto_11
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .registers 6
    .parameter "timeZone"

    #@0
    .prologue
    .line 273
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v1}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 275
    :try_start_5
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@7
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@9
    invoke-virtual {p0, v2}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@c
    move-result v2

    #@d
    const-string v3, "UTF-8"

    #@f
    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v1, v2, v3}, Landroid/renderscript/RenderScript;->nScriptSetTimeZone(I[B)V
    :try_end_16
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_16} :catch_17

    #@16
    .line 279
    return-void

    #@17
    .line 276
    :catch_17
    move-exception v0

    #@18
    .line 277
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@1a
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@1d
    throw v1
.end method

.method public setVar(ID)V
    .registers 6
    .parameter "index"
    .parameter "v"

    #@0
    .prologue
    .line 207
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1, p1, p2, p3}, Landroid/renderscript/RenderScript;->nScriptSetVarD(IID)V

    #@b
    .line 208
    return-void
.end method

.method public setVar(IF)V
    .registers 5
    .parameter "index"
    .parameter "v"

    #@0
    .prologue
    .line 197
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1, p1, p2}, Landroid/renderscript/RenderScript;->nScriptSetVarF(IIF)V

    #@b
    .line 198
    return-void
.end method

.method public setVar(II)V
    .registers 5
    .parameter "index"
    .parameter "v"

    #@0
    .prologue
    .line 217
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1, p1, p2}, Landroid/renderscript/RenderScript;->nScriptSetVarI(III)V

    #@b
    .line 218
    return-void
.end method

.method public setVar(IJ)V
    .registers 6
    .parameter "index"
    .parameter "v"

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1, p1, p2, p3}, Landroid/renderscript/RenderScript;->nScriptSetVarJ(IIJ)V

    #@b
    .line 228
    return-void
.end method

.method public setVar(ILandroid/renderscript/BaseObj;)V
    .registers 6
    .parameter "index"
    .parameter "o"

    #@0
    .prologue
    .line 247
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v0}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v2

    #@8
    if-nez p2, :cond_f

    #@a
    const/4 v0, 0x0

    #@b
    :goto_b
    invoke-virtual {v1, v2, p1, v0}, Landroid/renderscript/RenderScript;->nScriptSetVarObj(III)V

    #@e
    .line 248
    return-void

    #@f
    .line 247
    :cond_f
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@11
    invoke-virtual {p2, v0}, Landroid/renderscript/BaseObj;->getID(Landroid/renderscript/RenderScript;)I

    #@14
    move-result v0

    #@15
    goto :goto_b
.end method

.method public setVar(ILandroid/renderscript/FieldPacker;)V
    .registers 6
    .parameter "index"
    .parameter "v"

    #@0
    .prologue
    .line 257
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p2}, Landroid/renderscript/FieldPacker;->getData()[B

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v0, v1, p1, v2}, Landroid/renderscript/RenderScript;->nScriptSetVarV(II[B)V

    #@f
    .line 258
    return-void
.end method

.method public setVar(ILandroid/renderscript/FieldPacker;Landroid/renderscript/Element;[I)V
    .registers 11
    .parameter "index"
    .parameter "v"
    .parameter "e"
    .parameter "dims"

    #@0
    .prologue
    .line 269
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v1}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p2}, Landroid/renderscript/FieldPacker;->getData()[B

    #@b
    move-result-object v3

    #@c
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@e
    invoke-virtual {p3, v2}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@11
    move-result v4

    #@12
    move v2, p1

    #@13
    move-object v5, p4

    #@14
    invoke-virtual/range {v0 .. v5}, Landroid/renderscript/RenderScript;->nScriptSetVarVE(II[BI[I)V

    #@17
    .line 270
    return-void
.end method

.method public setVar(IZ)V
    .registers 6
    .parameter "index"
    .parameter "v"

    #@0
    .prologue
    .line 237
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v0}, Landroid/renderscript/Script;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v2

    #@8
    if-eqz p2, :cond_f

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    invoke-virtual {v1, v2, p1, v0}, Landroid/renderscript/RenderScript;->nScriptSetVarI(III)V

    #@e
    .line 238
    return-void

    #@f
    .line 237
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_b
.end method
