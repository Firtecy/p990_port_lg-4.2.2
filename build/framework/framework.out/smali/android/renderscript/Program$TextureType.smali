.class public final enum Landroid/renderscript/Program$TextureType;
.super Ljava/lang/Enum;
.source "Program.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Program;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TextureType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/renderscript/Program$TextureType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/renderscript/Program$TextureType;

.field public static final enum TEXTURE_2D:Landroid/renderscript/Program$TextureType;

.field public static final enum TEXTURE_CUBE:Landroid/renderscript/Program$TextureType;


# instance fields
.field mID:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 47
    new-instance v0, Landroid/renderscript/Program$TextureType;

    #@4
    const-string v1, "TEXTURE_2D"

    #@6
    invoke-direct {v0, v1, v2, v2}, Landroid/renderscript/Program$TextureType;-><init>(Ljava/lang/String;II)V

    #@9
    sput-object v0, Landroid/renderscript/Program$TextureType;->TEXTURE_2D:Landroid/renderscript/Program$TextureType;

    #@b
    .line 48
    new-instance v0, Landroid/renderscript/Program$TextureType;

    #@d
    const-string v1, "TEXTURE_CUBE"

    #@f
    invoke-direct {v0, v1, v3, v3}, Landroid/renderscript/Program$TextureType;-><init>(Ljava/lang/String;II)V

    #@12
    sput-object v0, Landroid/renderscript/Program$TextureType;->TEXTURE_CUBE:Landroid/renderscript/Program$TextureType;

    #@14
    .line 46
    const/4 v0, 0x2

    #@15
    new-array v0, v0, [Landroid/renderscript/Program$TextureType;

    #@17
    sget-object v1, Landroid/renderscript/Program$TextureType;->TEXTURE_2D:Landroid/renderscript/Program$TextureType;

    #@19
    aput-object v1, v0, v2

    #@1b
    sget-object v1, Landroid/renderscript/Program$TextureType;->TEXTURE_CUBE:Landroid/renderscript/Program$TextureType;

    #@1d
    aput-object v1, v0, v3

    #@1f
    sput-object v0, Landroid/renderscript/Program$TextureType;->$VALUES:[Landroid/renderscript/Program$TextureType;

    #@21
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 52
    iput p3, p0, Landroid/renderscript/Program$TextureType;->mID:I

    #@5
    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/renderscript/Program$TextureType;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 46
    const-class v0, Landroid/renderscript/Program$TextureType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/Program$TextureType;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/renderscript/Program$TextureType;
    .registers 1

    #@0
    .prologue
    .line 46
    sget-object v0, Landroid/renderscript/Program$TextureType;->$VALUES:[Landroid/renderscript/Program$TextureType;

    #@2
    invoke-virtual {v0}, [Landroid/renderscript/Program$TextureType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/renderscript/Program$TextureType;

    #@8
    return-object v0
.end method
