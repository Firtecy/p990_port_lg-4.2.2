.class public final Landroid/renderscript/ScriptIntrinsicConvolve3x3;
.super Landroid/renderscript/ScriptIntrinsic;
.source "ScriptIntrinsicConvolve3x3.java"


# instance fields
.field private mInput:Landroid/renderscript/Allocation;

.field private final mValues:[F


# direct methods
.method private constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 4
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/renderscript/ScriptIntrinsic;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 28
    const/16 v0, 0x9

    #@5
    new-array v0, v0, [F

    #@7
    iput-object v0, p0, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->mValues:[F

    #@9
    .line 33
    return-void
.end method

.method public static create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicConvolve3x3;
    .registers 7
    .parameter "rs"
    .parameter "e"

    #@0
    .prologue
    .line 52
    const/16 v3, 0x9

    #@2
    new-array v0, v3, [F

    #@4
    fill-array-data v0, :array_28

    #@7
    .line 53
    .local v0, f:[F
    invoke-static {p0}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@a
    move-result-object v3

    #@b
    if-eq p1, v3, :cond_15

    #@d
    .line 54
    new-instance v3, Landroid/renderscript/RSIllegalArgumentException;

    #@f
    const-string v4, "Unsuported element type."

    #@11
    invoke-direct {v3, v4}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v3

    #@15
    .line 56
    :cond_15
    const/4 v3, 0x1

    #@16
    invoke-virtual {p1, p0}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)I

    #@19
    move-result v4

    #@1a
    invoke-virtual {p0, v3, v4}, Landroid/renderscript/RenderScript;->nScriptIntrinsicCreate(II)I

    #@1d
    move-result v1

    #@1e
    .line 57
    .local v1, id:I
    new-instance v2, Landroid/renderscript/ScriptIntrinsicConvolve3x3;

    #@20
    invoke-direct {v2, v1, p0}, Landroid/renderscript/ScriptIntrinsicConvolve3x3;-><init>(ILandroid/renderscript/RenderScript;)V

    #@23
    .line 58
    .local v2, si:Landroid/renderscript/ScriptIntrinsicConvolve3x3;
    invoke-virtual {v2, v0}, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->setCoefficients([F)V

    #@26
    .line 59
    return-object v2

    #@27
    .line 52
    nop

    #@28
    :array_28
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method


# virtual methods
.method public forEach(Landroid/renderscript/Allocation;)V
    .registers 4
    .parameter "aout"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 103
    const/4 v0, 0x0

    #@2
    invoke-virtual {p0, v0, v1, p1, v1}, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->forEach(ILandroid/renderscript/Allocation;Landroid/renderscript/Allocation;Landroid/renderscript/FieldPacker;)V

    #@5
    .line 104
    return-void
.end method

.method public getFieldID_Input()Landroid/renderscript/Script$FieldID;
    .registers 3

    #@0
    .prologue
    .line 121
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getKernelID()Landroid/renderscript/Script$KernelID;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 112
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {p0, v0, v1, v2, v2}, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->createKernelID(IILandroid/renderscript/Element;Landroid/renderscript/Element;)Landroid/renderscript/Script$KernelID;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public setCoefficients([F)V
    .registers 6
    .parameter "v"

    #@0
    .prologue
    .line 87
    new-instance v1, Landroid/renderscript/FieldPacker;

    #@2
    const/16 v2, 0x24

    #@4
    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    #@7
    .line 88
    .local v1, fp:Landroid/renderscript/FieldPacker;
    const/4 v0, 0x0

    #@8
    .local v0, ct:I
    :goto_8
    iget-object v2, p0, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->mValues:[F

    #@a
    array-length v2, v2

    #@b
    if-ge v0, v2, :cond_1d

    #@d
    .line 89
    iget-object v2, p0, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->mValues:[F

    #@f
    aget v3, p1, v0

    #@11
    aput v3, v2, v0

    #@13
    .line 90
    iget-object v2, p0, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->mValues:[F

    #@15
    aget v2, v2, v0

    #@17
    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    #@1a
    .line 88
    add-int/lit8 v0, v0, 0x1

    #@1c
    goto :goto_8

    #@1d
    .line 92
    :cond_1d
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p0, v2, v1}, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->setVar(ILandroid/renderscript/FieldPacker;)V

    #@21
    .line 93
    return-void
.end method

.method public setInput(Landroid/renderscript/Allocation;)V
    .registers 3
    .parameter "ain"

    #@0
    .prologue
    .line 70
    iput-object p1, p0, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->mInput:Landroid/renderscript/Allocation;

    #@2
    .line 71
    const/4 v0, 0x1

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/ScriptIntrinsicConvolve3x3;->setVar(ILandroid/renderscript/BaseObj;)V

    #@6
    .line 72
    return-void
.end method
