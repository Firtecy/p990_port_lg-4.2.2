.class public final enum Landroid/renderscript/Element$DataKind;
.super Ljava/lang/Enum;
.source "Element.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Element;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataKind"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/renderscript/Element$DataKind;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/renderscript/Element$DataKind;

.field public static final enum PIXEL_A:Landroid/renderscript/Element$DataKind;

.field public static final enum PIXEL_DEPTH:Landroid/renderscript/Element$DataKind;

.field public static final enum PIXEL_L:Landroid/renderscript/Element$DataKind;

.field public static final enum PIXEL_LA:Landroid/renderscript/Element$DataKind;

.field public static final enum PIXEL_RGB:Landroid/renderscript/Element$DataKind;

.field public static final enum PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

.field public static final enum USER:Landroid/renderscript/Element$DataKind;


# instance fields
.field mID:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 167
    new-instance v0, Landroid/renderscript/Element$DataKind;

    #@7
    const-string v1, "USER"

    #@9
    invoke-direct {v0, v1, v4, v4}, Landroid/renderscript/Element$DataKind;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Landroid/renderscript/Element$DataKind;->USER:Landroid/renderscript/Element$DataKind;

    #@e
    .line 169
    new-instance v0, Landroid/renderscript/Element$DataKind;

    #@10
    const-string v1, "PIXEL_L"

    #@12
    const/4 v2, 0x7

    #@13
    invoke-direct {v0, v1, v5, v2}, Landroid/renderscript/Element$DataKind;-><init>(Ljava/lang/String;II)V

    #@16
    sput-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_L:Landroid/renderscript/Element$DataKind;

    #@18
    .line 170
    new-instance v0, Landroid/renderscript/Element$DataKind;

    #@1a
    const-string v1, "PIXEL_A"

    #@1c
    const/16 v2, 0x8

    #@1e
    invoke-direct {v0, v1, v6, v2}, Landroid/renderscript/Element$DataKind;-><init>(Ljava/lang/String;II)V

    #@21
    sput-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_A:Landroid/renderscript/Element$DataKind;

    #@23
    .line 171
    new-instance v0, Landroid/renderscript/Element$DataKind;

    #@25
    const-string v1, "PIXEL_LA"

    #@27
    const/16 v2, 0x9

    #@29
    invoke-direct {v0, v1, v7, v2}, Landroid/renderscript/Element$DataKind;-><init>(Ljava/lang/String;II)V

    #@2c
    sput-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_LA:Landroid/renderscript/Element$DataKind;

    #@2e
    .line 172
    new-instance v0, Landroid/renderscript/Element$DataKind;

    #@30
    const-string v1, "PIXEL_RGB"

    #@32
    const/16 v2, 0xa

    #@34
    invoke-direct {v0, v1, v8, v2}, Landroid/renderscript/Element$DataKind;-><init>(Ljava/lang/String;II)V

    #@37
    sput-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_RGB:Landroid/renderscript/Element$DataKind;

    #@39
    .line 173
    new-instance v0, Landroid/renderscript/Element$DataKind;

    #@3b
    const-string v1, "PIXEL_RGBA"

    #@3d
    const/4 v2, 0x5

    #@3e
    const/16 v3, 0xb

    #@40
    invoke-direct {v0, v1, v2, v3}, Landroid/renderscript/Element$DataKind;-><init>(Ljava/lang/String;II)V

    #@43
    sput-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@45
    .line 174
    new-instance v0, Landroid/renderscript/Element$DataKind;

    #@47
    const-string v1, "PIXEL_DEPTH"

    #@49
    const/4 v2, 0x6

    #@4a
    const/16 v3, 0xc

    #@4c
    invoke-direct {v0, v1, v2, v3}, Landroid/renderscript/Element$DataKind;-><init>(Ljava/lang/String;II)V

    #@4f
    sput-object v0, Landroid/renderscript/Element$DataKind;->PIXEL_DEPTH:Landroid/renderscript/Element$DataKind;

    #@51
    .line 166
    const/4 v0, 0x7

    #@52
    new-array v0, v0, [Landroid/renderscript/Element$DataKind;

    #@54
    sget-object v1, Landroid/renderscript/Element$DataKind;->USER:Landroid/renderscript/Element$DataKind;

    #@56
    aput-object v1, v0, v4

    #@58
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_L:Landroid/renderscript/Element$DataKind;

    #@5a
    aput-object v1, v0, v5

    #@5c
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_A:Landroid/renderscript/Element$DataKind;

    #@5e
    aput-object v1, v0, v6

    #@60
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_LA:Landroid/renderscript/Element$DataKind;

    #@62
    aput-object v1, v0, v7

    #@64
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_RGB:Landroid/renderscript/Element$DataKind;

    #@66
    aput-object v1, v0, v8

    #@68
    const/4 v1, 0x5

    #@69
    sget-object v2, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@6b
    aput-object v2, v0, v1

    #@6d
    const/4 v1, 0x6

    #@6e
    sget-object v2, Landroid/renderscript/Element$DataKind;->PIXEL_DEPTH:Landroid/renderscript/Element$DataKind;

    #@70
    aput-object v2, v0, v1

    #@72
    sput-object v0, Landroid/renderscript/Element$DataKind;->$VALUES:[Landroid/renderscript/Element$DataKind;

    #@74
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 178
    iput p3, p0, Landroid/renderscript/Element$DataKind;->mID:I

    #@5
    .line 179
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/renderscript/Element$DataKind;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 166
    const-class v0, Landroid/renderscript/Element$DataKind;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/Element$DataKind;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/renderscript/Element$DataKind;
    .registers 1

    #@0
    .prologue
    .line 166
    sget-object v0, Landroid/renderscript/Element$DataKind;->$VALUES:[Landroid/renderscript/Element$DataKind;

    #@2
    invoke-virtual {v0}, [Landroid/renderscript/Element$DataKind;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/renderscript/Element$DataKind;

    #@8
    return-object v0
.end method
