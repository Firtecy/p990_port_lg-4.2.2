.class public final enum Landroid/renderscript/ProgramStore$BlendDstFunc;
.super Ljava/lang/Enum;
.source "ProgramStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/ProgramStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BlendDstFunc"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/renderscript/ProgramStore$BlendDstFunc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/renderscript/ProgramStore$BlendDstFunc;

.field public static final enum DST_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

.field public static final enum ONE:Landroid/renderscript/ProgramStore$BlendDstFunc;

.field public static final enum ONE_MINUS_DST_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

.field public static final enum ONE_MINUS_SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

.field public static final enum ONE_MINUS_SRC_COLOR:Landroid/renderscript/ProgramStore$BlendDstFunc;

.field public static final enum SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

.field public static final enum SRC_COLOR:Landroid/renderscript/ProgramStore$BlendDstFunc;

.field public static final enum ZERO:Landroid/renderscript/ProgramStore$BlendDstFunc;


# instance fields
.field mID:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 123
    new-instance v0, Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@7
    const-string v1, "ZERO"

    #@9
    invoke-direct {v0, v1, v4, v4}, Landroid/renderscript/ProgramStore$BlendDstFunc;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Landroid/renderscript/ProgramStore$BlendDstFunc;->ZERO:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@e
    .line 124
    new-instance v0, Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@10
    const-string v1, "ONE"

    #@12
    invoke-direct {v0, v1, v5, v5}, Landroid/renderscript/ProgramStore$BlendDstFunc;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@17
    .line 125
    new-instance v0, Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@19
    const-string v1, "SRC_COLOR"

    #@1b
    invoke-direct {v0, v1, v6, v6}, Landroid/renderscript/ProgramStore$BlendDstFunc;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Landroid/renderscript/ProgramStore$BlendDstFunc;->SRC_COLOR:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@20
    .line 126
    new-instance v0, Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@22
    const-string v1, "ONE_MINUS_SRC_COLOR"

    #@24
    invoke-direct {v0, v1, v7, v7}, Landroid/renderscript/ProgramStore$BlendDstFunc;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v0, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE_MINUS_SRC_COLOR:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@29
    .line 127
    new-instance v0, Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@2b
    const-string v1, "SRC_ALPHA"

    #@2d
    invoke-direct {v0, v1, v8, v8}, Landroid/renderscript/ProgramStore$BlendDstFunc;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v0, Landroid/renderscript/ProgramStore$BlendDstFunc;->SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@32
    .line 128
    new-instance v0, Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@34
    const-string v1, "ONE_MINUS_SRC_ALPHA"

    #@36
    const/4 v2, 0x5

    #@37
    const/4 v3, 0x5

    #@38
    invoke-direct {v0, v1, v2, v3}, Landroid/renderscript/ProgramStore$BlendDstFunc;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE_MINUS_SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@3d
    .line 129
    new-instance v0, Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@3f
    const-string v1, "DST_ALPHA"

    #@41
    const/4 v2, 0x6

    #@42
    const/4 v3, 0x6

    #@43
    invoke-direct {v0, v1, v2, v3}, Landroid/renderscript/ProgramStore$BlendDstFunc;-><init>(Ljava/lang/String;II)V

    #@46
    sput-object v0, Landroid/renderscript/ProgramStore$BlendDstFunc;->DST_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@48
    .line 130
    new-instance v0, Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@4a
    const-string v1, "ONE_MINUS_DST_ALPHA"

    #@4c
    const/4 v2, 0x7

    #@4d
    const/4 v3, 0x7

    #@4e
    invoke-direct {v0, v1, v2, v3}, Landroid/renderscript/ProgramStore$BlendDstFunc;-><init>(Ljava/lang/String;II)V

    #@51
    sput-object v0, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE_MINUS_DST_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@53
    .line 122
    const/16 v0, 0x8

    #@55
    new-array v0, v0, [Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@57
    sget-object v1, Landroid/renderscript/ProgramStore$BlendDstFunc;->ZERO:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@59
    aput-object v1, v0, v4

    #@5b
    sget-object v1, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@5d
    aput-object v1, v0, v5

    #@5f
    sget-object v1, Landroid/renderscript/ProgramStore$BlendDstFunc;->SRC_COLOR:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@61
    aput-object v1, v0, v6

    #@63
    sget-object v1, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE_MINUS_SRC_COLOR:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@65
    aput-object v1, v0, v7

    #@67
    sget-object v1, Landroid/renderscript/ProgramStore$BlendDstFunc;->SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@69
    aput-object v1, v0, v8

    #@6b
    const/4 v1, 0x5

    #@6c
    sget-object v2, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE_MINUS_SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@6e
    aput-object v2, v0, v1

    #@70
    const/4 v1, 0x6

    #@71
    sget-object v2, Landroid/renderscript/ProgramStore$BlendDstFunc;->DST_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@73
    aput-object v2, v0, v1

    #@75
    const/4 v1, 0x7

    #@76
    sget-object v2, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE_MINUS_DST_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@78
    aput-object v2, v0, v1

    #@7a
    sput-object v0, Landroid/renderscript/ProgramStore$BlendDstFunc;->$VALUES:[Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@7c
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 134
    iput p3, p0, Landroid/renderscript/ProgramStore$BlendDstFunc;->mID:I

    #@5
    .line 135
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/renderscript/ProgramStore$BlendDstFunc;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 122
    const-class v0, Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/renderscript/ProgramStore$BlendDstFunc;
    .registers 1

    #@0
    .prologue
    .line 122
    sget-object v0, Landroid/renderscript/ProgramStore$BlendDstFunc;->$VALUES:[Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@2
    invoke-virtual {v0}, [Landroid/renderscript/ProgramStore$BlendDstFunc;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/renderscript/ProgramStore$BlendDstFunc;

    #@8
    return-object v0
.end method
