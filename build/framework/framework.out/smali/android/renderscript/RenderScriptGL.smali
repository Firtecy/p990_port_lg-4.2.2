.class public Landroid/renderscript/RenderScriptGL;
.super Landroid/renderscript/RenderScript;
.source "RenderScriptGL.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/RenderScriptGL$SurfaceConfig;
    }
.end annotation


# instance fields
.field mHeight:I

.field mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

.field mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/renderscript/RenderScriptGL$SurfaceConfig;)V
    .registers 20
    .parameter "ctx"
    .parameter "sc"

    #@0
    .prologue
    .line 179
    invoke-direct/range {p0 .. p1}, Landroid/renderscript/RenderScript;-><init>(Landroid/content/Context;)V

    #@3
    .line 180
    new-instance v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@5
    move-object/from16 v0, p2

    #@7
    invoke-direct {v1, v0}, Landroid/renderscript/RenderScriptGL$SurfaceConfig;-><init>(Landroid/renderscript/RenderScriptGL$SurfaceConfig;)V

    #@a
    move-object/from16 v0, p0

    #@c
    iput-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@e
    .line 182
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@11
    move-result-object v1

    #@12
    iget v4, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@14
    .line 184
    .local v4, sdkVersion:I
    const/4 v1, 0x0

    #@15
    move-object/from16 v0, p0

    #@17
    iput v1, v0, Landroid/renderscript/RenderScriptGL;->mWidth:I

    #@19
    .line 185
    const/4 v1, 0x0

    #@1a
    move-object/from16 v0, p0

    #@1c
    iput v1, v0, Landroid/renderscript/RenderScriptGL;->mHeight:I

    #@1e
    .line 186
    invoke-virtual/range {p0 .. p0}, Landroid/renderscript/RenderScriptGL;->nDeviceCreate()I

    #@21
    move-result v1

    #@22
    move-object/from16 v0, p0

    #@24
    iput v1, v0, Landroid/renderscript/RenderScript;->mDev:I

    #@26
    .line 187
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@2d
    move-result-object v1

    #@2e
    iget v0, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@30
    move/from16 v16, v0

    #@32
    .line 188
    .local v16, dpi:I
    move-object/from16 v0, p0

    #@34
    iget v2, v0, Landroid/renderscript/RenderScript;->mDev:I

    #@36
    const/4 v3, 0x0

    #@37
    move-object/from16 v0, p0

    #@39
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@3b
    iget v5, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mColorMin:I

    #@3d
    move-object/from16 v0, p0

    #@3f
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@41
    iget v6, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mColorPref:I

    #@43
    move-object/from16 v0, p0

    #@45
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@47
    iget v7, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mAlphaMin:I

    #@49
    move-object/from16 v0, p0

    #@4b
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@4d
    iget v8, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mAlphaPref:I

    #@4f
    move-object/from16 v0, p0

    #@51
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@53
    iget v9, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mDepthMin:I

    #@55
    move-object/from16 v0, p0

    #@57
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@59
    iget v10, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mDepthPref:I

    #@5b
    move-object/from16 v0, p0

    #@5d
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@5f
    iget v11, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mStencilMin:I

    #@61
    move-object/from16 v0, p0

    #@63
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@65
    iget v12, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mStencilPref:I

    #@67
    move-object/from16 v0, p0

    #@69
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@6b
    iget v13, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mSamplesMin:I

    #@6d
    move-object/from16 v0, p0

    #@6f
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@71
    iget v14, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mSamplesPref:I

    #@73
    move-object/from16 v0, p0

    #@75
    iget-object v1, v0, Landroid/renderscript/RenderScriptGL;->mSurfaceConfig:Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    #@77
    iget v15, v1, Landroid/renderscript/RenderScriptGL$SurfaceConfig;->mSamplesQ:F

    #@79
    move-object/from16 v1, p0

    #@7b
    invoke-virtual/range {v1 .. v16}, Landroid/renderscript/RenderScriptGL;->nContextCreateGL(IIIIIIIIIIIIIFI)I

    #@7e
    move-result v1

    #@7f
    move-object/from16 v0, p0

    #@81
    iput v1, v0, Landroid/renderscript/RenderScript;->mContext:I

    #@83
    .line 195
    move-object/from16 v0, p0

    #@85
    iget v1, v0, Landroid/renderscript/RenderScript;->mContext:I

    #@87
    if-nez v1, :cond_91

    #@89
    .line 196
    new-instance v1, Landroid/renderscript/RSDriverException;

    #@8b
    const-string v2, "Failed to create RS context."

    #@8d
    invoke-direct {v1, v2}, Landroid/renderscript/RSDriverException;-><init>(Ljava/lang/String;)V

    #@90
    throw v1

    #@91
    .line 198
    :cond_91
    new-instance v1, Landroid/renderscript/RenderScript$MessageThread;

    #@93
    move-object/from16 v0, p0

    #@95
    invoke-direct {v1, v0}, Landroid/renderscript/RenderScript$MessageThread;-><init>(Landroid/renderscript/RenderScript;)V

    #@98
    move-object/from16 v0, p0

    #@9a
    iput-object v1, v0, Landroid/renderscript/RenderScript;->mMessageThread:Landroid/renderscript/RenderScript$MessageThread;

    #@9c
    .line 199
    move-object/from16 v0, p0

    #@9e
    iget-object v1, v0, Landroid/renderscript/RenderScript;->mMessageThread:Landroid/renderscript/RenderScript$MessageThread;

    #@a0
    invoke-virtual {v1}, Landroid/renderscript/RenderScript$MessageThread;->start()V

    #@a3
    .line 200
    return-void
.end method


# virtual methods
.method public bindProgramFragment(Landroid/renderscript/ProgramFragment;)V
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 311
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->validate()V

    #@3
    .line 312
    invoke-virtual {p0, p1}, Landroid/renderscript/RenderScriptGL;->safeID(Landroid/renderscript/BaseObj;)I

    #@6
    move-result v0

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/RenderScriptGL;->nContextBindProgramFragment(I)V

    #@a
    .line 313
    return-void
.end method

.method public bindProgramRaster(Landroid/renderscript/ProgramRaster;)V
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 323
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->validate()V

    #@3
    .line 324
    invoke-virtual {p0, p1}, Landroid/renderscript/RenderScriptGL;->safeID(Landroid/renderscript/BaseObj;)I

    #@6
    move-result v0

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/RenderScriptGL;->nContextBindProgramRaster(I)V

    #@a
    .line 325
    return-void
.end method

.method public bindProgramStore(Landroid/renderscript/ProgramStore;)V
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 299
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->validate()V

    #@3
    .line 300
    invoke-virtual {p0, p1}, Landroid/renderscript/RenderScriptGL;->safeID(Landroid/renderscript/BaseObj;)I

    #@6
    move-result v0

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/RenderScriptGL;->nContextBindProgramStore(I)V

    #@a
    .line 301
    return-void
.end method

.method public bindProgramVertex(Landroid/renderscript/ProgramVertex;)V
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 335
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->validate()V

    #@3
    .line 336
    invoke-virtual {p0, p1}, Landroid/renderscript/RenderScriptGL;->safeID(Landroid/renderscript/BaseObj;)I

    #@6
    move-result v0

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/RenderScriptGL;->nContextBindProgramVertex(I)V

    #@a
    .line 337
    return-void
.end method

.method public bindRootScript(Landroid/renderscript/Script;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 287
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->validate()V

    #@3
    .line 288
    invoke-virtual {p0, p1}, Landroid/renderscript/RenderScriptGL;->safeID(Landroid/renderscript/BaseObj;)I

    #@6
    move-result v0

    #@7
    invoke-virtual {p0, v0}, Landroid/renderscript/RenderScriptGL;->nContextBindRootScript(I)V

    #@a
    .line 289
    return-void
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 246
    iget v0, p0, Landroid/renderscript/RenderScriptGL;->mHeight:I

    #@2
    return v0
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 256
    iget v0, p0, Landroid/renderscript/RenderScriptGL;->mWidth:I

    #@2
    return v0
.end method

.method public pause()V
    .registers 1

    #@0
    .prologue
    .line 265
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->validate()V

    #@3
    .line 266
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->nContextPause()V

    #@6
    .line 267
    return-void
.end method

.method public resume()V
    .registers 1

    #@0
    .prologue
    .line 275
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->validate()V

    #@3
    .line 276
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->nContextResume()V

    #@6
    .line 277
    return-void
.end method

.method public setSurface(Landroid/view/SurfaceHolder;II)V
    .registers 5
    .parameter "sur"
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 212
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->validate()V

    #@3
    .line 213
    const/4 v0, 0x0

    #@4
    .line 214
    .local v0, s:Landroid/view/Surface;
    if-eqz p1, :cond_a

    #@6
    .line 215
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@9
    move-result-object v0

    #@a
    .line 217
    :cond_a
    iput p2, p0, Landroid/renderscript/RenderScriptGL;->mWidth:I

    #@c
    .line 218
    iput p3, p0, Landroid/renderscript/RenderScriptGL;->mHeight:I

    #@e
    .line 219
    invoke-virtual {p0, p2, p3, v0}, Landroid/renderscript/RenderScriptGL;->nContextSetSurface(IILandroid/view/Surface;)V

    #@11
    .line 220
    return-void
.end method

.method public setSurfaceTexture(Landroid/graphics/SurfaceTexture;II)V
    .registers 4
    .parameter "sur"
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 231
    invoke-virtual {p0}, Landroid/renderscript/RenderScriptGL;->validate()V

    #@3
    .line 234
    iput p2, p0, Landroid/renderscript/RenderScriptGL;->mWidth:I

    #@5
    .line 235
    iput p3, p0, Landroid/renderscript/RenderScriptGL;->mHeight:I

    #@7
    .line 236
    invoke-virtual {p0, p2, p3, p1}, Landroid/renderscript/RenderScriptGL;->nContextSetSurfaceTexture(IILandroid/graphics/SurfaceTexture;)V

    #@a
    .line 237
    return-void
.end method
