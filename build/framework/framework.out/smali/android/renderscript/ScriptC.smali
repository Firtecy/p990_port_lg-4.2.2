.class public Landroid/renderscript/ScriptC;
.super Landroid/renderscript/Script;
.source "ScriptC.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ScriptC"


# direct methods
.method protected constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/renderscript/Script;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 46
    return-void
.end method

.method protected constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .registers 7
    .parameter "rs"
    .parameter "resources"
    .parameter "resourceID"

    #@0
    .prologue
    .line 57
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, v1, p1}, Landroid/renderscript/Script;-><init>(ILandroid/renderscript/RenderScript;)V

    #@4
    .line 58
    invoke-static {p1, p2, p3}, Landroid/renderscript/ScriptC;->internalCreate(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)I

    #@7
    move-result v0

    #@8
    .line 59
    .local v0, id:I
    if-nez v0, :cond_12

    #@a
    .line 60
    new-instance v1, Landroid/renderscript/RSRuntimeException;

    #@c
    const-string v2, "Loading of ScriptC script failed."

    #@e
    invoke-direct {v1, v2}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@11
    throw v1

    #@12
    .line 62
    :cond_12
    invoke-virtual {p0, v0}, Landroid/renderscript/ScriptC;->setID(I)V

    #@15
    .line 63
    return-void
.end method

.method private static declared-synchronized internalCreate(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)I
    .registers 15
    .parameter "rs"
    .parameter "resources"
    .parameter "resourceID"

    #@0
    .prologue
    .line 69
    const-class v9, Landroid/renderscript/ScriptC;

    #@2
    monitor-enter v9

    #@3
    :try_start_3
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_5b

    #@6
    move-result-object v4

    #@7
    .line 72
    .local v4, is:Ljava/io/InputStream;
    const/16 v8, 0x400

    #@9
    :try_start_9
    new-array v5, v8, [B

    #@b
    .line 73
    .local v5, pgm:[B
    const/4 v6, 0x0

    #@c
    .line 75
    .local v6, pgmLength:I
    :goto_c
    array-length v8, v5

    #@d
    sub-int v1, v8, v6

    #@f
    .line 76
    .local v1, bytesLeft:I
    if-nez v1, :cond_20

    #@11
    .line 77
    array-length v8, v5

    #@12
    mul-int/lit8 v8, v8, 0x2

    #@14
    new-array v0, v8, [B

    #@16
    .line 78
    .local v0, buf2:[B
    const/4 v8, 0x0

    #@17
    const/4 v10, 0x0

    #@18
    array-length v11, v5

    #@19
    invoke-static {v5, v8, v0, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1c
    .line 79
    move-object v5, v0

    #@1d
    .line 80
    array-length v8, v5

    #@1e
    sub-int v1, v8, v6

    #@20
    .line 82
    .end local v0           #buf2:[B
    :cond_20
    invoke-virtual {v4, v5, v6, v1}, Ljava/io/InputStream;->read([BII)I
    :try_end_23
    .catchall {:try_start_9 .. :try_end_23} :catchall_4f

    #@23
    move-result v2

    #@24
    .line 83
    .local v2, bytesRead:I
    if-gtz v2, :cond_4d

    #@26
    .line 89
    :try_start_26
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_5b
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_29} :catch_54

    #@29
    .line 95
    :try_start_29
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    #@2c
    move-result-object v7

    #@2d
    .line 97
    .local v7, resName:Ljava/lang/String;
    const-string v8, "ScriptC"

    #@2f
    new-instance v10, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v11, "Create script for resource = "

    #@36
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v10

    #@3a
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v10

    #@3e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v10

    #@42
    invoke-static {v8, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 98
    sget-object v8, Landroid/renderscript/RenderScript;->mCachePath:Ljava/lang/String;

    #@47
    invoke-virtual {p0, v7, v8, v5, v6}, Landroid/renderscript/RenderScript;->nScriptCCreate(Ljava/lang/String;Ljava/lang/String;[BI)I
    :try_end_4a
    .catchall {:try_start_29 .. :try_end_4a} :catchall_5b

    #@4a
    move-result v8

    #@4b
    monitor-exit v9

    #@4c
    return v8

    #@4d
    .line 86
    .end local v7           #resName:Ljava/lang/String;
    :cond_4d
    add-int/2addr v6, v2

    #@4e
    .line 87
    goto :goto_c

    #@4f
    .line 89
    .end local v1           #bytesLeft:I
    .end local v2           #bytesRead:I
    .end local v5           #pgm:[B
    .end local v6           #pgmLength:I
    :catchall_4f
    move-exception v8

    #@50
    :try_start_50
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    #@53
    throw v8
    :try_end_54
    .catchall {:try_start_50 .. :try_end_54} :catchall_5b
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_54} :catch_54

    #@54
    .line 91
    :catch_54
    move-exception v3

    #@55
    .line 92
    .local v3, e:Ljava/io/IOException;
    :try_start_55
    new-instance v8, Landroid/content/res/Resources$NotFoundException;

    #@57
    invoke-direct {v8}, Landroid/content/res/Resources$NotFoundException;-><init>()V

    #@5a
    throw v8
    :try_end_5b
    .catchall {:try_start_55 .. :try_end_5b} :catchall_5b

    #@5b
    .line 69
    .end local v3           #e:Ljava/io/IOException;
    .end local v4           #is:Ljava/io/InputStream;
    :catchall_5b
    move-exception v8

    #@5c
    monitor-exit v9

    #@5d
    throw v8
.end method
