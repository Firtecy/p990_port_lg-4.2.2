.class public Landroid/renderscript/Allocation;
.super Landroid/renderscript/BaseObj;
.source "Allocation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Allocation$1;,
        Landroid/renderscript/Allocation$MipmapControl;
    }
.end annotation


# static fields
.field public static final USAGE_GRAPHICS_CONSTANTS:I = 0x8

.field public static final USAGE_GRAPHICS_RENDER_TARGET:I = 0x10

.field public static final USAGE_GRAPHICS_TEXTURE:I = 0x2

.field public static final USAGE_GRAPHICS_VERTEX:I = 0x4

.field public static final USAGE_IO_INPUT:I = 0x20

.field public static final USAGE_IO_OUTPUT:I = 0x40

.field public static final USAGE_SCRIPT:I = 0x1

.field static mBitmapOptions:Landroid/graphics/BitmapFactory$Options;


# instance fields
.field mAdaptedAllocation:Landroid/renderscript/Allocation;

.field mBitmap:Landroid/graphics/Bitmap;

.field mConstrainedFace:Z

.field mConstrainedLOD:Z

.field mConstrainedY:Z

.field mConstrainedZ:Z

.field mCurrentCount:I

.field mCurrentDimX:I

.field mCurrentDimY:I

.field mCurrentDimZ:I

.field mReadAllowed:Z

.field mSelectedFace:Landroid/renderscript/Type$CubemapFace;

.field mSelectedLOD:I

.field mSelectedY:I

.field mSelectedZ:I

.field mType:Landroid/renderscript/Type;

.field mUsage:I

.field mWriteAllowed:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 1000
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    #@2
    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@5
    sput-object v0, Landroid/renderscript/Allocation;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    #@7
    .line 1002
    sget-object v0, Landroid/renderscript/Allocation;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    #@9
    const/4 v1, 0x0

    #@a
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    #@c
    .line 1003
    return-void
.end method

.method constructor <init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Type;I)V
    .registers 7
    .parameter "id"
    .parameter "rs"
    .parameter "t"
    .parameter "usage"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 237
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@4
    .line 83
    iput-boolean v0, p0, Landroid/renderscript/Allocation;->mReadAllowed:Z

    #@6
    .line 84
    iput-boolean v0, p0, Landroid/renderscript/Allocation;->mWriteAllowed:Z

    #@8
    .line 88
    sget-object v0, Landroid/renderscript/Type$CubemapFace;->POSITIVE_X:Landroid/renderscript/Type$CubemapFace;

    #@a
    iput-object v0, p0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@c
    .line 238
    and-int/lit8 v0, p4, -0x80

    #@e
    if-eqz v0, :cond_18

    #@10
    .line 245
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@12
    const-string v1, "Unknown usage specified."

    #@14
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 248
    :cond_18
    and-int/lit8 v0, p4, 0x20

    #@1a
    if-eqz v0, :cond_2b

    #@1c
    .line 249
    const/4 v0, 0x0

    #@1d
    iput-boolean v0, p0, Landroid/renderscript/Allocation;->mWriteAllowed:Z

    #@1f
    .line 251
    and-int/lit8 v0, p4, -0x24

    #@21
    if-eqz v0, :cond_2b

    #@23
    .line 254
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@25
    const-string v1, "Invalid usage combination."

    #@27
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v0

    #@2b
    .line 258
    :cond_2b
    iput-object p3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2d
    .line 259
    iput p4, p0, Landroid/renderscript/Allocation;->mUsage:I

    #@2f
    .line 261
    if-eqz p3, :cond_34

    #@31
    .line 262
    invoke-direct {p0, p3}, Landroid/renderscript/Allocation;->updateCacheInfo(Landroid/renderscript/Type;)V

    #@34
    .line 264
    :cond_34
    return-void
.end method

.method public static createCubemapFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;
    .registers 4
    .parameter "rs"
    .parameter "b"

    #@0
    .prologue
    .line 1282
    sget-object v0, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_NONE:Landroid/renderscript/Allocation$MipmapControl;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {p0, p1, v0, v1}, Landroid/renderscript/Allocation;->createCubemapFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public static createCubemapFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;
    .registers 14
    .parameter "rs"
    .parameter "b"
    .parameter "mips"
    .parameter "usage"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 1236
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 1238
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@8
    move-result v1

    #@9
    .line 1239
    .local v1, height:I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@c
    move-result v6

    #@d
    .line 1241
    .local v6, width:I
    rem-int/lit8 v9, v6, 0x6

    #@f
    if-eqz v9, :cond_19

    #@11
    .line 1242
    new-instance v7, Landroid/renderscript/RSIllegalArgumentException;

    #@13
    const-string v8, "Cubemap height must be multiple of 6"

    #@15
    invoke-direct {v7, v8}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v7

    #@19
    .line 1244
    :cond_19
    div-int/lit8 v9, v6, 0x6

    #@1b
    if-eq v9, v1, :cond_25

    #@1d
    .line 1245
    new-instance v7, Landroid/renderscript/RSIllegalArgumentException;

    #@1f
    const-string v8, "Only square cube map faces supported"

    #@21
    invoke-direct {v7, v8}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v7

    #@25
    .line 1247
    :cond_25
    add-int/lit8 v9, v1, -0x1

    #@27
    and-int/2addr v9, v1

    #@28
    if-nez v9, :cond_35

    #@2a
    move v3, v7

    #@2b
    .line 1248
    .local v3, isPow2:Z
    :goto_2b
    if-nez v3, :cond_37

    #@2d
    .line 1249
    new-instance v7, Landroid/renderscript/RSIllegalArgumentException;

    #@2f
    const-string v8, "Only power of 2 cube faces supported"

    #@31
    invoke-direct {v7, v8}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@34
    throw v7

    #@35
    .end local v3           #isPow2:Z
    :cond_35
    move v3, v8

    #@36
    .line 1247
    goto :goto_2b

    #@37
    .line 1252
    .restart local v3       #isPow2:Z
    :cond_37
    invoke-static {p0, p1}, Landroid/renderscript/Allocation;->elementFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Element;

    #@3a
    move-result-object v0

    #@3b
    .line 1253
    .local v0, e:Landroid/renderscript/Element;
    new-instance v5, Landroid/renderscript/Type$Builder;

    #@3d
    invoke-direct {v5, p0, v0}, Landroid/renderscript/Type$Builder;-><init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V

    #@40
    .line 1254
    .local v5, tb:Landroid/renderscript/Type$Builder;
    invoke-virtual {v5, v1}, Landroid/renderscript/Type$Builder;->setX(I)Landroid/renderscript/Type$Builder;

    #@43
    .line 1255
    invoke-virtual {v5, v1}, Landroid/renderscript/Type$Builder;->setY(I)Landroid/renderscript/Type$Builder;

    #@46
    .line 1256
    invoke-virtual {v5, v7}, Landroid/renderscript/Type$Builder;->setFaces(Z)Landroid/renderscript/Type$Builder;

    #@49
    .line 1257
    sget-object v9, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_FULL:Landroid/renderscript/Allocation$MipmapControl;

    #@4b
    if-ne p2, v9, :cond_83

    #@4d
    :goto_4d
    invoke-virtual {v5, v7}, Landroid/renderscript/Type$Builder;->setMipmaps(Z)Landroid/renderscript/Type$Builder;

    #@50
    .line 1258
    invoke-virtual {v5}, Landroid/renderscript/Type$Builder;->create()Landroid/renderscript/Type;

    #@53
    move-result-object v4

    #@54
    .line 1260
    .local v4, t:Landroid/renderscript/Type;
    invoke-virtual {v4, p0}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)I

    #@57
    move-result v7

    #@58
    iget v8, p2, Landroid/renderscript/Allocation$MipmapControl;->mID:I

    #@5a
    invoke-virtual {p0, v7, v8, p1, p3}, Landroid/renderscript/RenderScript;->nAllocationCubeCreateFromBitmap(IILandroid/graphics/Bitmap;I)I

    #@5d
    move-result v2

    #@5e
    .line 1261
    .local v2, id:I
    if-nez v2, :cond_85

    #@60
    .line 1262
    new-instance v7, Landroid/renderscript/RSRuntimeException;

    #@62
    new-instance v8, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v9, "Load failed for bitmap "

    #@69
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v8

    #@6d
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v8

    #@71
    const-string v9, " element "

    #@73
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v8

    #@77
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v8

    #@7b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v8

    #@7f
    invoke-direct {v7, v8}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@82
    throw v7

    #@83
    .end local v2           #id:I
    .end local v4           #t:Landroid/renderscript/Type;
    :cond_83
    move v7, v8

    #@84
    .line 1257
    goto :goto_4d

    #@85
    .line 1264
    .restart local v2       #id:I
    .restart local v4       #t:Landroid/renderscript/Type;
    :cond_85
    new-instance v7, Landroid/renderscript/Allocation;

    #@87
    invoke-direct {v7, v2, p0, v4, p3}, Landroid/renderscript/Allocation;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Type;I)V

    #@8a
    return-object v7
.end method

.method public static createCubemapFromCubeFaces(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;
    .registers 16
    .parameter "rs"
    .parameter "xpos"
    .parameter "xneg"
    .parameter "ypos"
    .parameter "yneg"
    .parameter "zpos"
    .parameter "zneg"

    #@0
    .prologue
    .line 1377
    sget-object v7, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_NONE:Landroid/renderscript/Allocation$MipmapControl;

    #@2
    const/4 v8, 0x2

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move-object v2, p2

    #@6
    move-object v3, p3

    #@7
    move-object v4, p4

    #@8
    move-object v5, p5

    #@9
    move-object v6, p6

    #@a
    invoke-static/range {v0 .. v8}, Landroid/renderscript/Allocation;->createCubemapFromCubeFaces(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    #@d
    move-result-object v0

    #@e
    return-object v0
.end method

.method public static createCubemapFromCubeFaces(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;
    .registers 20
    .parameter "rs"
    .parameter "xpos"
    .parameter "xneg"
    .parameter "ypos"
    .parameter "yneg"
    .parameter "zpos"
    .parameter "zneg"
    .parameter "mips"
    .parameter "usage"

    #@0
    .prologue
    .line 1313
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@3
    move-result v5

    #@4
    .line 1314
    .local v5, height:I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@7
    move-result v9

    #@8
    if-ne v9, v5, :cond_46

    #@a
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    #@d
    move-result v9

    #@e
    if-ne v9, v5, :cond_46

    #@10
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    #@13
    move-result v9

    #@14
    if-ne v9, v5, :cond_46

    #@16
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    #@19
    move-result v9

    #@1a
    if-ne v9, v5, :cond_46

    #@1c
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    #@1f
    move-result v9

    #@20
    if-ne v9, v5, :cond_46

    #@22
    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getWidth()I

    #@25
    move-result v9

    #@26
    if-ne v9, v5, :cond_46

    #@28
    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getHeight()I

    #@2b
    move-result v9

    #@2c
    if-ne v9, v5, :cond_46

    #@2e
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Bitmap;->getWidth()I

    #@31
    move-result v9

    #@32
    if-ne v9, v5, :cond_46

    #@34
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Bitmap;->getHeight()I

    #@37
    move-result v9

    #@38
    if-ne v9, v5, :cond_46

    #@3a
    invoke-virtual/range {p6 .. p6}, Landroid/graphics/Bitmap;->getWidth()I

    #@3d
    move-result v9

    #@3e
    if-ne v9, v5, :cond_46

    #@40
    invoke-virtual/range {p6 .. p6}, Landroid/graphics/Bitmap;->getHeight()I

    #@43
    move-result v9

    #@44
    if-eq v9, v5, :cond_4e

    #@46
    .line 1320
    :cond_46
    new-instance v9, Landroid/renderscript/RSIllegalArgumentException;

    #@48
    const-string v10, "Only square cube map faces supported"

    #@4a
    invoke-direct {v9, v10}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4d
    throw v9

    #@4e
    .line 1322
    :cond_4e
    add-int/lit8 v9, v5, -0x1

    #@50
    and-int/2addr v9, v5

    #@51
    if-nez v9, :cond_5e

    #@53
    const/4 v6, 0x1

    #@54
    .line 1323
    .local v6, isPow2:Z
    :goto_54
    if-nez v6, :cond_60

    #@56
    .line 1324
    new-instance v9, Landroid/renderscript/RSIllegalArgumentException;

    #@58
    const-string v10, "Only power of 2 cube faces supported"

    #@5a
    invoke-direct {v9, v10}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5d
    throw v9

    #@5e
    .line 1322
    .end local v6           #isPow2:Z
    :cond_5e
    const/4 v6, 0x0

    #@5f
    goto :goto_54

    #@60
    .line 1327
    .restart local v6       #isPow2:Z
    :cond_60
    invoke-static {p0, p1}, Landroid/renderscript/Allocation;->elementFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Element;

    #@63
    move-result-object v4

    #@64
    .line 1328
    .local v4, e:Landroid/renderscript/Element;
    new-instance v8, Landroid/renderscript/Type$Builder;

    #@66
    invoke-direct {v8, p0, v4}, Landroid/renderscript/Type$Builder;-><init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V

    #@69
    .line 1329
    .local v8, tb:Landroid/renderscript/Type$Builder;
    invoke-virtual {v8, v5}, Landroid/renderscript/Type$Builder;->setX(I)Landroid/renderscript/Type$Builder;

    #@6c
    .line 1330
    invoke-virtual {v8, v5}, Landroid/renderscript/Type$Builder;->setY(I)Landroid/renderscript/Type$Builder;

    #@6f
    .line 1331
    const/4 v9, 0x1

    #@70
    invoke-virtual {v8, v9}, Landroid/renderscript/Type$Builder;->setFaces(Z)Landroid/renderscript/Type$Builder;

    #@73
    .line 1332
    sget-object v9, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_FULL:Landroid/renderscript/Allocation$MipmapControl;

    #@75
    move-object/from16 v0, p7

    #@77
    if-ne v0, v9, :cond_c2

    #@79
    const/4 v9, 0x1

    #@7a
    :goto_7a
    invoke-virtual {v8, v9}, Landroid/renderscript/Type$Builder;->setMipmaps(Z)Landroid/renderscript/Type$Builder;

    #@7d
    .line 1333
    invoke-virtual {v8}, Landroid/renderscript/Type$Builder;->create()Landroid/renderscript/Type;

    #@80
    move-result-object v7

    #@81
    .line 1334
    .local v7, t:Landroid/renderscript/Type;
    move-object/from16 v0, p7

    #@83
    move/from16 v1, p8

    #@85
    invoke-static {p0, v7, v0, v1}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    #@88
    move-result-object v3

    #@89
    .line 1336
    .local v3, cubemap:Landroid/renderscript/Allocation;
    invoke-static {p0, v3}, Landroid/renderscript/AllocationAdapter;->create2D(Landroid/renderscript/RenderScript;Landroid/renderscript/Allocation;)Landroid/renderscript/AllocationAdapter;

    #@8c
    move-result-object v2

    #@8d
    .line 1337
    .local v2, adapter:Landroid/renderscript/AllocationAdapter;
    sget-object v9, Landroid/renderscript/Type$CubemapFace;->POSITIVE_X:Landroid/renderscript/Type$CubemapFace;

    #@8f
    invoke-virtual {v2, v9}, Landroid/renderscript/AllocationAdapter;->setFace(Landroid/renderscript/Type$CubemapFace;)V

    #@92
    .line 1338
    invoke-virtual {v2, p1}, Landroid/renderscript/AllocationAdapter;->copyFrom(Landroid/graphics/Bitmap;)V

    #@95
    .line 1339
    sget-object v9, Landroid/renderscript/Type$CubemapFace;->NEGATIVE_X:Landroid/renderscript/Type$CubemapFace;

    #@97
    invoke-virtual {v2, v9}, Landroid/renderscript/AllocationAdapter;->setFace(Landroid/renderscript/Type$CubemapFace;)V

    #@9a
    .line 1340
    invoke-virtual {v2, p2}, Landroid/renderscript/AllocationAdapter;->copyFrom(Landroid/graphics/Bitmap;)V

    #@9d
    .line 1341
    sget-object v9, Landroid/renderscript/Type$CubemapFace;->POSITIVE_Y:Landroid/renderscript/Type$CubemapFace;

    #@9f
    invoke-virtual {v2, v9}, Landroid/renderscript/AllocationAdapter;->setFace(Landroid/renderscript/Type$CubemapFace;)V

    #@a2
    .line 1342
    invoke-virtual {v2, p3}, Landroid/renderscript/AllocationAdapter;->copyFrom(Landroid/graphics/Bitmap;)V

    #@a5
    .line 1343
    sget-object v9, Landroid/renderscript/Type$CubemapFace;->NEGATIVE_Y:Landroid/renderscript/Type$CubemapFace;

    #@a7
    invoke-virtual {v2, v9}, Landroid/renderscript/AllocationAdapter;->setFace(Landroid/renderscript/Type$CubemapFace;)V

    #@aa
    .line 1344
    invoke-virtual {v2, p4}, Landroid/renderscript/AllocationAdapter;->copyFrom(Landroid/graphics/Bitmap;)V

    #@ad
    .line 1345
    sget-object v9, Landroid/renderscript/Type$CubemapFace;->POSITIVE_Z:Landroid/renderscript/Type$CubemapFace;

    #@af
    invoke-virtual {v2, v9}, Landroid/renderscript/AllocationAdapter;->setFace(Landroid/renderscript/Type$CubemapFace;)V

    #@b2
    .line 1346
    move-object/from16 v0, p5

    #@b4
    invoke-virtual {v2, v0}, Landroid/renderscript/AllocationAdapter;->copyFrom(Landroid/graphics/Bitmap;)V

    #@b7
    .line 1347
    sget-object v9, Landroid/renderscript/Type$CubemapFace;->NEGATIVE_Z:Landroid/renderscript/Type$CubemapFace;

    #@b9
    invoke-virtual {v2, v9}, Landroid/renderscript/AllocationAdapter;->setFace(Landroid/renderscript/Type$CubemapFace;)V

    #@bc
    .line 1348
    move-object/from16 v0, p6

    #@be
    invoke-virtual {v2, v0}, Landroid/renderscript/AllocationAdapter;->copyFrom(Landroid/graphics/Bitmap;)V

    #@c1
    .line 1350
    return-object v3

    #@c2
    .line 1332
    .end local v2           #adapter:Landroid/renderscript/AllocationAdapter;
    .end local v3           #cubemap:Landroid/renderscript/Allocation;
    .end local v7           #t:Landroid/renderscript/Type;
    :cond_c2
    const/4 v9, 0x0

    #@c3
    goto :goto_7a
.end method

.method public static createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;
    .registers 4
    .parameter "rs"
    .parameter "b"

    #@0
    .prologue
    .line 1215
    sget-object v0, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_NONE:Landroid/renderscript/Allocation$MipmapControl;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {p0, p1, v0, v1}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public static createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;
    .registers 8
    .parameter "rs"
    .parameter "b"
    .parameter "mips"
    .parameter "usage"

    #@0
    .prologue
    .line 1137
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@3
    .line 1138
    invoke-static {p0, p1, p2}, Landroid/renderscript/Allocation;->typeFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/renderscript/Allocation$MipmapControl;)Landroid/renderscript/Type;

    #@6
    move-result-object v1

    #@7
    .line 1140
    .local v1, t:Landroid/renderscript/Type;
    invoke-virtual {v1, p0}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)I

    #@a
    move-result v2

    #@b
    iget v3, p2, Landroid/renderscript/Allocation$MipmapControl;->mID:I

    #@d
    invoke-virtual {p0, v2, v3, p1, p3}, Landroid/renderscript/RenderScript;->nAllocationCreateFromBitmap(IILandroid/graphics/Bitmap;I)I

    #@10
    move-result v0

    #@11
    .line 1141
    .local v0, id:I
    if-nez v0, :cond_1b

    #@13
    .line 1142
    new-instance v2, Landroid/renderscript/RSRuntimeException;

    #@15
    const-string v3, "Load failed."

    #@17
    invoke-direct {v2, v3}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 1144
    :cond_1b
    new-instance v2, Landroid/renderscript/Allocation;

    #@1d
    invoke-direct {v2, v0, p0, v1, p3}, Landroid/renderscript/Allocation;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Type;I)V

    #@20
    return-object v2
.end method

.method public static createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;
    .registers 5
    .parameter "rs"
    .parameter "res"
    .parameter "id"

    #@0
    .prologue
    .line 1424
    sget-object v0, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_NONE:Landroid/renderscript/Allocation$MipmapControl;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {p0, p1, p2, v0, v1}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;ILandroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public static createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;ILandroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;
    .registers 7
    .parameter "rs"
    .parameter "res"
    .parameter "id"
    .parameter "mips"
    .parameter "usage"

    #@0
    .prologue
    .line 1403
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@3
    .line 1404
    invoke-static {p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    #@6
    move-result-object v1

    #@7
    .line 1405
    .local v1, b:Landroid/graphics/Bitmap;
    invoke-static {p0, v1, p3, p4}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    #@a
    move-result-object v0

    #@b
    .line 1406
    .local v0, alloc:Landroid/renderscript/Allocation;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    #@e
    .line 1407
    return-object v0
.end method

.method public static createFromString(Landroid/renderscript/RenderScript;Ljava/lang/String;I)Landroid/renderscript/Allocation;
    .registers 8
    .parameter "rs"
    .parameter "str"
    .parameter "usage"

    #@0
    .prologue
    .line 1442
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@3
    .line 1443
    const/4 v1, 0x0

    #@4
    .line 1445
    .local v1, allocArray:[B
    :try_start_4
    const-string v3, "UTF-8"

    #@6
    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@9
    move-result-object v1

    #@a
    .line 1446
    invoke-static {p0}, Landroid/renderscript/Element;->U8(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@d
    move-result-object v3

    #@e
    array-length v4, v1

    #@f
    invoke-static {p0, v3, v4, p2}, Landroid/renderscript/Allocation;->createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;II)Landroid/renderscript/Allocation;

    #@12
    move-result-object v0

    #@13
    .line 1447
    .local v0, alloc:Landroid/renderscript/Allocation;
    invoke-virtual {v0, v1}, Landroid/renderscript/Allocation;->copyFrom([B)V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_16} :catch_17

    #@16
    .line 1448
    return-object v0

    #@17
    .line 1450
    .end local v0           #alloc:Landroid/renderscript/Allocation;
    :catch_17
    move-exception v2

    #@18
    .line 1451
    .local v2, e:Ljava/lang/Exception;
    new-instance v3, Landroid/renderscript/RSRuntimeException;

    #@1a
    const-string v4, "Could not convert string to utf-8."

    #@1c
    invoke-direct {v3, v4}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v3
.end method

.method public static createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;I)Landroid/renderscript/Allocation;
    .registers 4
    .parameter "rs"
    .parameter "e"
    .parameter "count"

    #@0
    .prologue
    .line 1091
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, p2, v0}, Landroid/renderscript/Allocation;->createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;II)Landroid/renderscript/Allocation;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;II)Landroid/renderscript/Allocation;
    .registers 10
    .parameter "rs"
    .parameter "e"
    .parameter "count"
    .parameter "usage"

    #@0
    .prologue
    .line 1068
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@3
    .line 1069
    new-instance v0, Landroid/renderscript/Type$Builder;

    #@5
    invoke-direct {v0, p0, p1}, Landroid/renderscript/Type$Builder;-><init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V

    #@8
    .line 1070
    .local v0, b:Landroid/renderscript/Type$Builder;
    invoke-virtual {v0, p2}, Landroid/renderscript/Type$Builder;->setX(I)Landroid/renderscript/Type$Builder;

    #@b
    .line 1071
    invoke-virtual {v0}, Landroid/renderscript/Type$Builder;->create()Landroid/renderscript/Type;

    #@e
    move-result-object v2

    #@f
    .line 1073
    .local v2, t:Landroid/renderscript/Type;
    invoke-virtual {v2, p0}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)I

    #@12
    move-result v3

    #@13
    sget-object v4, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_NONE:Landroid/renderscript/Allocation$MipmapControl;

    #@15
    iget v4, v4, Landroid/renderscript/Allocation$MipmapControl;->mID:I

    #@17
    const/4 v5, 0x0

    #@18
    invoke-virtual {p0, v3, v4, p3, v5}, Landroid/renderscript/RenderScript;->nAllocationCreateTyped(IIII)I

    #@1b
    move-result v1

    #@1c
    .line 1074
    .local v1, id:I
    if-nez v1, :cond_26

    #@1e
    .line 1075
    new-instance v3, Landroid/renderscript/RSRuntimeException;

    #@20
    const-string v4, "Allocation creation failed."

    #@22
    invoke-direct {v3, v4}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v3

    #@26
    .line 1077
    :cond_26
    new-instance v3, Landroid/renderscript/Allocation;

    #@28
    invoke-direct {v3, v1, p0, v2, p3}, Landroid/renderscript/Allocation;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Type;I)V

    #@2b
    return-object v3
.end method

.method public static createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;)Landroid/renderscript/Allocation;
    .registers 4
    .parameter "rs"
    .parameter "type"

    #@0
    .prologue
    .line 1051
    sget-object v0, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_NONE:Landroid/renderscript/Allocation$MipmapControl;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {p0, p1, v0, v1}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public static createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;I)Landroid/renderscript/Allocation;
    .registers 4
    .parameter "rs"
    .parameter "type"
    .parameter "usage"

    #@0
    .prologue
    .line 1037
    sget-object v0, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_NONE:Landroid/renderscript/Allocation$MipmapControl;

    #@2
    invoke-static {p0, p1, v0, p2}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;
    .registers 8
    .parameter "rs"
    .parameter "type"
    .parameter "mips"
    .parameter "usage"

    #@0
    .prologue
    .line 1014
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@3
    .line 1015
    invoke-virtual {p1, p0}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)I

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_11

    #@9
    .line 1016
    new-instance v1, Landroid/renderscript/RSInvalidStateException;

    #@b
    const-string v2, "Bad Type"

    #@d
    invoke-direct {v1, v2}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v1

    #@11
    .line 1018
    :cond_11
    invoke-virtual {p1, p0}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)I

    #@14
    move-result v1

    #@15
    iget v2, p2, Landroid/renderscript/Allocation$MipmapControl;->mID:I

    #@17
    const/4 v3, 0x0

    #@18
    invoke-virtual {p0, v1, v2, p3, v3}, Landroid/renderscript/RenderScript;->nAllocationCreateTyped(IIII)I

    #@1b
    move-result v0

    #@1c
    .line 1019
    .local v0, id:I
    if-nez v0, :cond_26

    #@1e
    .line 1020
    new-instance v1, Landroid/renderscript/RSRuntimeException;

    #@20
    const-string v2, "Allocation creation failed."

    #@22
    invoke-direct {v1, v2}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1

    #@26
    .line 1022
    :cond_26
    new-instance v1, Landroid/renderscript/Allocation;

    #@28
    invoke-direct {v1, v0, p0, p1, p3}, Landroid/renderscript/Allocation;-><init>(ILandroid/renderscript/RenderScript;Landroid/renderscript/Type;I)V

    #@2b
    return-object v1
.end method

.method private data1DChecks(IIII)V
    .registers 8
    .parameter "off"
    .parameter "count"
    .parameter "len"
    .parameter "dataSize"

    #@0
    .prologue
    .line 621
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 622
    if-gez p1, :cond_f

    #@7
    .line 623
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@9
    const-string v1, "Offset must be >= 0."

    #@b
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 625
    :cond_f
    const/4 v0, 0x1

    #@10
    if-ge p2, v0, :cond_1a

    #@12
    .line 626
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@14
    const-string v1, "Count must be >= 1."

    #@16
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 628
    :cond_1a
    add-int v0, p1, p2

    #@1c
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@1e
    if-le v0, v1, :cond_55

    #@20
    .line 629
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@22
    new-instance v1, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v2, "Overflow, Available count "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    iget v2, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    const-string v2, ", got "

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, " at offset "

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    const-string v2, "."

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v1

    #@51
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@54
    throw v0

    #@55
    .line 632
    :cond_55
    if-ge p3, p4, :cond_5f

    #@57
    .line 633
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@59
    const-string v1, "Array too small for allocation type."

    #@5b
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5e
    throw v0

    #@5f
    .line 635
    :cond_5f
    return-void
.end method

.method static elementFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Element;
    .registers 6
    .parameter "rs"
    .parameter "b"

    #@0
    .prologue
    .line 1095
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@3
    move-result-object v0

    #@4
    .line 1096
    .local v0, bc:Landroid/graphics/Bitmap$Config;
    sget-object v1, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    #@6
    if-ne v0, v1, :cond_d

    #@8
    .line 1097
    invoke-static {p0}, Landroid/renderscript/Element;->A_8(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@b
    move-result-object v1

    #@c
    .line 1106
    :goto_c
    return-object v1

    #@d
    .line 1099
    :cond_d
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    #@f
    if-ne v0, v1, :cond_16

    #@11
    .line 1100
    invoke-static {p0}, Landroid/renderscript/Element;->RGBA_4444(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@14
    move-result-object v1

    #@15
    goto :goto_c

    #@16
    .line 1102
    :cond_16
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@18
    if-ne v0, v1, :cond_1f

    #@1a
    .line 1103
    invoke-static {p0}, Landroid/renderscript/Element;->RGBA_8888(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@1d
    move-result-object v1

    #@1e
    goto :goto_c

    #@1f
    .line 1105
    :cond_1f
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@21
    if-ne v0, v1, :cond_28

    #@23
    .line 1106
    invoke-static {p0}, Landroid/renderscript/Element;->RGB_565(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    #@26
    move-result-object v1

    #@27
    goto :goto_c

    #@28
    .line 1108
    :cond_28
    new-instance v1, Landroid/renderscript/RSInvalidStateException;

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "Bad bitmap type: "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-direct {v1, v2}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@40
    throw v1
.end method

.method private getIDSafe()I
    .registers 3

    #@0
    .prologue
    .line 184
    iget-object v0, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 185
    iget-object v0, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@6
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@8
    invoke-virtual {v0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@b
    move-result v0

    #@c
    .line 187
    :goto_c
    return v0

    #@d
    :cond_d
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@f
    invoke-virtual {p0, v0}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@12
    move-result v0

    #@13
    goto :goto_c
.end method

.method static typeFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/renderscript/Allocation$MipmapControl;)Landroid/renderscript/Type;
    .registers 6
    .parameter "rs"
    .parameter "b"
    .parameter "mip"

    #@0
    .prologue
    .line 1113
    invoke-static {p0, p1}, Landroid/renderscript/Allocation;->elementFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Element;

    #@3
    move-result-object v0

    #@4
    .line 1114
    .local v0, e:Landroid/renderscript/Element;
    new-instance v1, Landroid/renderscript/Type$Builder;

    #@6
    invoke-direct {v1, p0, v0}, Landroid/renderscript/Type$Builder;-><init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V

    #@9
    .line 1115
    .local v1, tb:Landroid/renderscript/Type$Builder;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@c
    move-result v2

    #@d
    invoke-virtual {v1, v2}, Landroid/renderscript/Type$Builder;->setX(I)Landroid/renderscript/Type$Builder;

    #@10
    .line 1116
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@13
    move-result v2

    #@14
    invoke-virtual {v1, v2}, Landroid/renderscript/Type$Builder;->setY(I)Landroid/renderscript/Type$Builder;

    #@17
    .line 1117
    sget-object v2, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_FULL:Landroid/renderscript/Allocation$MipmapControl;

    #@19
    if-ne p2, v2, :cond_24

    #@1b
    const/4 v2, 0x1

    #@1c
    :goto_1c
    invoke-virtual {v1, v2}, Landroid/renderscript/Type$Builder;->setMipmaps(Z)Landroid/renderscript/Type$Builder;

    #@1f
    .line 1118
    invoke-virtual {v1}, Landroid/renderscript/Type$Builder;->create()Landroid/renderscript/Type;

    #@22
    move-result-object v2

    #@23
    return-object v2

    #@24
    .line 1117
    :cond_24
    const/4 v2, 0x0

    #@25
    goto :goto_1c
.end method

.method private updateCacheInfo(Landroid/renderscript/Type;)V
    .registers 5
    .parameter "t"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 224
    invoke-virtual {p1}, Landroid/renderscript/Type;->getX()I

    #@4
    move-result v0

    #@5
    iput v0, p0, Landroid/renderscript/Allocation;->mCurrentDimX:I

    #@7
    .line 225
    invoke-virtual {p1}, Landroid/renderscript/Type;->getY()I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/renderscript/Allocation;->mCurrentDimY:I

    #@d
    .line 226
    invoke-virtual {p1}, Landroid/renderscript/Type;->getZ()I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/renderscript/Allocation;->mCurrentDimZ:I

    #@13
    .line 227
    iget v0, p0, Landroid/renderscript/Allocation;->mCurrentDimX:I

    #@15
    iput v0, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@17
    .line 228
    iget v0, p0, Landroid/renderscript/Allocation;->mCurrentDimY:I

    #@19
    if-le v0, v2, :cond_22

    #@1b
    .line 229
    iget v0, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@1d
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentDimY:I

    #@1f
    mul-int/2addr v0, v1

    #@20
    iput v0, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@22
    .line 231
    :cond_22
    iget v0, p0, Landroid/renderscript/Allocation;->mCurrentDimZ:I

    #@24
    if-le v0, v2, :cond_2d

    #@26
    .line 232
    iget v0, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@28
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentDimZ:I

    #@2a
    mul-int/2addr v0, v1

    #@2b
    iput v0, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@2d
    .line 234
    :cond_2d
    return-void
.end method

.method private validate2DRange(IIII)V
    .registers 7
    .parameter "xoff"
    .parameter "yoff"
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 781
    iget-object v0, p0, Landroid/renderscript/Allocation;->mAdaptedAllocation:Landroid/renderscript/Allocation;

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 795
    :cond_4
    return-void

    #@5
    .line 785
    :cond_5
    if-ltz p1, :cond_9

    #@7
    if-gez p2, :cond_11

    #@9
    .line 786
    :cond_9
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@b
    const-string v1, "Offset cannot be negative."

    #@d
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 788
    :cond_11
    if-ltz p4, :cond_15

    #@13
    if-gez p3, :cond_1d

    #@15
    .line 789
    :cond_15
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@17
    const-string v1, "Height or width cannot be negative."

    #@19
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v0

    #@1d
    .line 791
    :cond_1d
    add-int v0, p1, p3

    #@1f
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentDimX:I

    #@21
    if-gt v0, v1, :cond_29

    #@23
    add-int v0, p2, p4

    #@25
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentDimY:I

    #@27
    if-le v0, v1, :cond_4

    #@29
    .line 792
    :cond_29
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@2b
    const-string v1, "Updated region larger than allocation."

    #@2d
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@30
    throw v0
.end method

.method private validateBitmapFormat(Landroid/graphics/Bitmap;)V
    .registers 6
    .parameter "b"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 413
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@4
    move-result-object v0

    #@5
    .line 414
    .local v0, bc:Landroid/graphics/Bitmap$Config;
    sget-object v1, Landroid/renderscript/Allocation$1;->$SwitchMap$android$graphics$Bitmap$Config:[I

    #@7
    invoke-virtual {v0}, Landroid/graphics/Bitmap$Config;->ordinal()I

    #@a
    move-result v2

    #@b
    aget v1, v1, v2

    #@d
    packed-switch v1, :pswitch_data_1aa

    #@10
    .line 456
    :cond_10
    return-void

    #@11
    .line 416
    :pswitch_11
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@13
    invoke-virtual {v1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@16
    move-result-object v1

    #@17
    iget-object v1, v1, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@19
    sget-object v2, Landroid/renderscript/Element$DataKind;->PIXEL_A:Landroid/renderscript/Element$DataKind;

    #@1b
    if-eq v1, v2, :cond_10

    #@1d
    .line 417
    new-instance v1, Landroid/renderscript/RSIllegalArgumentException;

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "Allocation kind is "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2c
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@2f
    move-result-object v3

    #@30
    iget-object v3, v3, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    const-string v3, ", type "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@3e
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@41
    move-result-object v3

    #@42
    iget-object v3, v3, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    const-string v3, " of "

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@50
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3}, Landroid/renderscript/Element;->getBytesSize()I

    #@57
    move-result v3

    #@58
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    const-string v3, " bytes, passed bitmap was "

    #@5e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v2

    #@6a
    invoke-direct {v1, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6d
    throw v1

    #@6e
    .line 425
    :pswitch_6e
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@70
    invoke-virtual {v1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@73
    move-result-object v1

    #@74
    iget-object v1, v1, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@76
    sget-object v2, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@78
    if-ne v1, v2, :cond_87

    #@7a
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@7c
    invoke-virtual {v1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@7f
    move-result-object v1

    #@80
    invoke-virtual {v1}, Landroid/renderscript/Element;->getBytesSize()I

    #@83
    move-result v1

    #@84
    const/4 v2, 0x4

    #@85
    if-eq v1, v2, :cond_10

    #@87
    .line 427
    :cond_87
    new-instance v1, Landroid/renderscript/RSIllegalArgumentException;

    #@89
    new-instance v2, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v3, "Allocation kind is "

    #@90
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v2

    #@94
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@96
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@99
    move-result-object v3

    #@9a
    iget-object v3, v3, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@9c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v2

    #@a0
    const-string v3, ", type "

    #@a2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v2

    #@a6
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@a8
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@ab
    move-result-object v3

    #@ac
    iget-object v3, v3, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@ae
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v2

    #@b2
    const-string v3, " of "

    #@b4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v2

    #@b8
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@ba
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@bd
    move-result-object v3

    #@be
    invoke-virtual {v3}, Landroid/renderscript/Element;->getBytesSize()I

    #@c1
    move-result v3

    #@c2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v2

    #@c6
    const-string v3, " bytes, passed bitmap was "

    #@c8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v2

    #@cc
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v2

    #@d0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v2

    #@d4
    invoke-direct {v1, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d7
    throw v1

    #@d8
    .line 435
    :pswitch_d8
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@da
    invoke-virtual {v1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@dd
    move-result-object v1

    #@de
    iget-object v1, v1, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@e0
    sget-object v2, Landroid/renderscript/Element$DataKind;->PIXEL_RGB:Landroid/renderscript/Element$DataKind;

    #@e2
    if-ne v1, v2, :cond_f0

    #@e4
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@e6
    invoke-virtual {v1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@e9
    move-result-object v1

    #@ea
    invoke-virtual {v1}, Landroid/renderscript/Element;->getBytesSize()I

    #@ed
    move-result v1

    #@ee
    if-eq v1, v3, :cond_10

    #@f0
    .line 437
    :cond_f0
    new-instance v1, Landroid/renderscript/RSIllegalArgumentException;

    #@f2
    new-instance v2, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v3, "Allocation kind is "

    #@f9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v2

    #@fd
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@ff
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@102
    move-result-object v3

    #@103
    iget-object v3, v3, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@105
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v2

    #@109
    const-string v3, ", type "

    #@10b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v2

    #@10f
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@111
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@114
    move-result-object v3

    #@115
    iget-object v3, v3, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@117
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v2

    #@11b
    const-string v3, " of "

    #@11d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v2

    #@121
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@123
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@126
    move-result-object v3

    #@127
    invoke-virtual {v3}, Landroid/renderscript/Element;->getBytesSize()I

    #@12a
    move-result v3

    #@12b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v2

    #@12f
    const-string v3, " bytes, passed bitmap was "

    #@131
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v2

    #@135
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v2

    #@139
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13c
    move-result-object v2

    #@13d
    invoke-direct {v1, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@140
    throw v1

    #@141
    .line 445
    :pswitch_141
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@143
    invoke-virtual {v1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@146
    move-result-object v1

    #@147
    iget-object v1, v1, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@149
    sget-object v2, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@14b
    if-ne v1, v2, :cond_159

    #@14d
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@14f
    invoke-virtual {v1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@152
    move-result-object v1

    #@153
    invoke-virtual {v1}, Landroid/renderscript/Element;->getBytesSize()I

    #@156
    move-result v1

    #@157
    if-eq v1, v3, :cond_10

    #@159
    .line 447
    :cond_159
    new-instance v1, Landroid/renderscript/RSIllegalArgumentException;

    #@15b
    new-instance v2, Ljava/lang/StringBuilder;

    #@15d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@160
    const-string v3, "Allocation kind is "

    #@162
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v2

    #@166
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@168
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@16b
    move-result-object v3

    #@16c
    iget-object v3, v3, Landroid/renderscript/Element;->mKind:Landroid/renderscript/Element$DataKind;

    #@16e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@171
    move-result-object v2

    #@172
    const-string v3, ", type "

    #@174
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v2

    #@178
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@17a
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@17d
    move-result-object v3

    #@17e
    iget-object v3, v3, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@180
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@183
    move-result-object v2

    #@184
    const-string v3, " of "

    #@186
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v2

    #@18a
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@18c
    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@18f
    move-result-object v3

    #@190
    invoke-virtual {v3}, Landroid/renderscript/Element;->getBytesSize()I

    #@193
    move-result v3

    #@194
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@197
    move-result-object v2

    #@198
    const-string v3, " bytes, passed bitmap was "

    #@19a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v2

    #@19e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v2

    #@1a2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a5
    move-result-object v2

    #@1a6
    invoke-direct {v1, v2}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a9
    throw v1

    #@1aa
    .line 414
    :pswitch_data_1aa
    .packed-switch 0x1
        :pswitch_11
        :pswitch_6e
        :pswitch_d8
        :pswitch_141
    .end packed-switch
.end method

.method private validateBitmapSize(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "b"

    #@0
    .prologue
    .line 459
    iget v0, p0, Landroid/renderscript/Allocation;->mCurrentDimX:I

    #@2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@5
    move-result v1

    #@6
    if-ne v0, v1, :cond_10

    #@8
    iget v0, p0, Landroid/renderscript/Allocation;->mCurrentDimY:I

    #@a
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@d
    move-result v1

    #@e
    if-eq v0, v1, :cond_18

    #@10
    .line 460
    :cond_10
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@12
    const-string v1, "Cannot update allocation from bitmap, sizes mismatch"

    #@14
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 462
    :cond_18
    return-void
.end method

.method private validateIsFloat32()V
    .registers 4

    #@0
    .prologue
    .line 294
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataType;->FLOAT_32:Landroid/renderscript/Element$DataType;

    #@8
    if-ne v0, v1, :cond_b

    #@a
    .line 295
    return-void

    #@b
    .line 297
    :cond_b
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "32 bit float source does not match allocation type "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    iget-object v2, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@1a
    iget-object v2, v2, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@1c
    iget-object v2, v2, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0
.end method

.method private validateIsInt16()V
    .registers 4

    #@0
    .prologue
    .line 276
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataType;->SIGNED_16:Landroid/renderscript/Element$DataType;

    #@8
    if-eq v0, v1, :cond_14

    #@a
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@c
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@e
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@10
    sget-object v1, Landroid/renderscript/Element$DataType;->UNSIGNED_16:Landroid/renderscript/Element$DataType;

    #@12
    if-ne v0, v1, :cond_15

    #@14
    .line 278
    :cond_14
    return-void

    #@15
    .line 280
    :cond_15
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "16 bit integer source does not match allocation type "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    iget-object v2, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@24
    iget-object v2, v2, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@26
    iget-object v2, v2, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@33
    throw v0
.end method

.method private validateIsInt32()V
    .registers 4

    #@0
    .prologue
    .line 267
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataType;->SIGNED_32:Landroid/renderscript/Element$DataType;

    #@8
    if-eq v0, v1, :cond_14

    #@a
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@c
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@e
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@10
    sget-object v1, Landroid/renderscript/Element$DataType;->UNSIGNED_32:Landroid/renderscript/Element$DataType;

    #@12
    if-ne v0, v1, :cond_15

    #@14
    .line 269
    :cond_14
    return-void

    #@15
    .line 271
    :cond_15
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "32 bit integer source does not match allocation type "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    iget-object v2, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@24
    iget-object v2, v2, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@26
    iget-object v2, v2, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@33
    throw v0
.end method

.method private validateIsInt8()V
    .registers 4

    #@0
    .prologue
    .line 285
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataType;->SIGNED_8:Landroid/renderscript/Element$DataType;

    #@8
    if-eq v0, v1, :cond_14

    #@a
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@c
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@e
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@10
    sget-object v1, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@12
    if-ne v0, v1, :cond_15

    #@14
    .line 287
    :cond_14
    return-void

    #@15
    .line 289
    :cond_15
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "8 bit integer source does not match allocation type "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    iget-object v2, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@24
    iget-object v2, v2, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@26
    iget-object v2, v2, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@33
    throw v0
.end method

.method private validateIsObject()V
    .registers 4

    #@0
    .prologue
    .line 302
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@6
    sget-object v1, Landroid/renderscript/Element$DataType;->RS_ELEMENT:Landroid/renderscript/Element$DataType;

    #@8
    if-eq v0, v1, :cond_64

    #@a
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@c
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@e
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@10
    sget-object v1, Landroid/renderscript/Element$DataType;->RS_TYPE:Landroid/renderscript/Element$DataType;

    #@12
    if-eq v0, v1, :cond_64

    #@14
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@16
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@18
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@1a
    sget-object v1, Landroid/renderscript/Element$DataType;->RS_ALLOCATION:Landroid/renderscript/Element$DataType;

    #@1c
    if-eq v0, v1, :cond_64

    #@1e
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@20
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@22
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@24
    sget-object v1, Landroid/renderscript/Element$DataType;->RS_SAMPLER:Landroid/renderscript/Element$DataType;

    #@26
    if-eq v0, v1, :cond_64

    #@28
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2a
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@2c
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@2e
    sget-object v1, Landroid/renderscript/Element$DataType;->RS_SCRIPT:Landroid/renderscript/Element$DataType;

    #@30
    if-eq v0, v1, :cond_64

    #@32
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@34
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@36
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@38
    sget-object v1, Landroid/renderscript/Element$DataType;->RS_MESH:Landroid/renderscript/Element$DataType;

    #@3a
    if-eq v0, v1, :cond_64

    #@3c
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@3e
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@40
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@42
    sget-object v1, Landroid/renderscript/Element$DataType;->RS_PROGRAM_FRAGMENT:Landroid/renderscript/Element$DataType;

    #@44
    if-eq v0, v1, :cond_64

    #@46
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@48
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4a
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@4c
    sget-object v1, Landroid/renderscript/Element$DataType;->RS_PROGRAM_VERTEX:Landroid/renderscript/Element$DataType;

    #@4e
    if-eq v0, v1, :cond_64

    #@50
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@52
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@54
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@56
    sget-object v1, Landroid/renderscript/Element$DataType;->RS_PROGRAM_RASTER:Landroid/renderscript/Element$DataType;

    #@58
    if-eq v0, v1, :cond_64

    #@5a
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@5c
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@5e
    iget-object v0, v0, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@60
    sget-object v1, Landroid/renderscript/Element$DataType;->RS_PROGRAM_STORE:Landroid/renderscript/Element$DataType;

    #@62
    if-ne v0, v1, :cond_65

    #@64
    .line 312
    :cond_64
    return-void

    #@65
    .line 314
    :cond_65
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@67
    new-instance v1, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v2, "Object source does not match allocation type "

    #@6e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    iget-object v2, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@74
    iget-object v2, v2, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@76
    iget-object v2, v2, Landroid/renderscript/Element;->mType:Landroid/renderscript/Element$DataType;

    #@78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v1

    #@80
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@83
    throw v0
.end method


# virtual methods
.method public copy1DRangeFrom(IILandroid/renderscript/Allocation;I)V
    .registers 19
    .parameter "off"
    .parameter "count"
    .parameter "data"
    .parameter "dataOff"

    #@0
    .prologue
    .line 774
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@5
    move-result v2

    #@6
    const/4 v4, 0x0

    #@7
    iget v5, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@9
    iget-object v3, p0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@b
    iget v6, v3, Landroid/renderscript/Type$CubemapFace;->mID:I

    #@d
    const/4 v8, 0x1

    #@e
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@10
    move-object/from16 v0, p3

    #@12
    invoke-virtual {v0, v3}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@15
    move-result v9

    #@16
    const/4 v11, 0x0

    #@17
    move-object/from16 v0, p3

    #@19
    iget v12, v0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@1b
    move-object/from16 v0, p3

    #@1d
    iget-object v3, v0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@1f
    iget v13, v3, Landroid/renderscript/Type$CubemapFace;->mID:I

    #@21
    move v3, p1

    #@22
    move/from16 v7, p2

    #@24
    move/from16 v10, p4

    #@26
    invoke-virtual/range {v1 .. v13}, Landroid/renderscript/RenderScript;->nAllocationData2D(IIIIIIIIIIII)V

    #@29
    .line 778
    return-void
.end method

.method public copy1DRangeFrom(II[B)V
    .registers 4
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 746
    invoke-direct {p0}, Landroid/renderscript/Allocation;->validateIsInt8()V

    #@3
    .line 747
    invoke-virtual {p0, p1, p2, p3}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[B)V

    #@6
    .line 748
    return-void
.end method

.method public copy1DRangeFrom(II[F)V
    .registers 4
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 760
    invoke-direct {p0}, Landroid/renderscript/Allocation;->validateIsFloat32()V

    #@3
    .line 761
    invoke-virtual {p0, p1, p2, p3}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[F)V

    #@6
    .line 762
    return-void
.end method

.method public copy1DRangeFrom(II[I)V
    .registers 4
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 718
    invoke-direct {p0}, Landroid/renderscript/Allocation;->validateIsInt32()V

    #@3
    .line 719
    invoke-virtual {p0, p1, p2, p3}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[I)V

    #@6
    .line 720
    return-void
.end method

.method public copy1DRangeFrom(II[S)V
    .registers 4
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 732
    invoke-direct {p0}, Landroid/renderscript/Allocation;->validateIsInt16()V

    #@3
    .line 733
    invoke-virtual {p0, p1, p2, p3}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[S)V

    #@6
    .line 734
    return-void
.end method

.method public copy1DRangeFromUnchecked(II[B)V
    .registers 11
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 689
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4
    invoke-virtual {v0}, Landroid/renderscript/Element;->getBytesSize()I

    #@7
    move-result v0

    #@8
    mul-int v6, v0, p2

    #@a
    .line 690
    .local v6, dataSize:I
    array-length v0, p3

    #@b
    invoke-direct {p0, p1, p2, v0, v6}, Landroid/renderscript/Allocation;->data1DChecks(IIII)V

    #@e
    .line 691
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@10
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@13
    move-result v1

    #@14
    iget v3, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@16
    move v2, p1

    #@17
    move v4, p2

    #@18
    move-object v5, p3

    #@19
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/RenderScript;->nAllocationData1D(IIII[BI)V

    #@1c
    .line 692
    return-void
.end method

.method public copy1DRangeFromUnchecked(II[F)V
    .registers 11
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 703
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4
    invoke-virtual {v0}, Landroid/renderscript/Element;->getBytesSize()I

    #@7
    move-result v0

    #@8
    mul-int v6, v0, p2

    #@a
    .line 704
    .local v6, dataSize:I
    array-length v0, p3

    #@b
    mul-int/lit8 v0, v0, 0x4

    #@d
    invoke-direct {p0, p1, p2, v0, v6}, Landroid/renderscript/Allocation;->data1DChecks(IIII)V

    #@10
    .line 705
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@12
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@15
    move-result v1

    #@16
    iget v3, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@18
    move v2, p1

    #@19
    move v4, p2

    #@1a
    move-object v5, p3

    #@1b
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/RenderScript;->nAllocationData1D(IIII[FI)V

    #@1e
    .line 706
    return-void
.end method

.method public copy1DRangeFromUnchecked(II[I)V
    .registers 11
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 661
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4
    invoke-virtual {v0}, Landroid/renderscript/Element;->getBytesSize()I

    #@7
    move-result v0

    #@8
    mul-int v6, v0, p2

    #@a
    .line 662
    .local v6, dataSize:I
    array-length v0, p3

    #@b
    mul-int/lit8 v0, v0, 0x4

    #@d
    invoke-direct {p0, p1, p2, v0, v6}, Landroid/renderscript/Allocation;->data1DChecks(IIII)V

    #@10
    .line 663
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@12
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@15
    move-result v1

    #@16
    iget v3, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@18
    move v2, p1

    #@19
    move v4, p2

    #@1a
    move-object v5, p3

    #@1b
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/RenderScript;->nAllocationData1D(IIII[II)V

    #@1e
    .line 664
    return-void
.end method

.method public copy1DRangeFromUnchecked(II[S)V
    .registers 11
    .parameter "off"
    .parameter "count"
    .parameter "d"

    #@0
    .prologue
    .line 675
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4
    invoke-virtual {v0}, Landroid/renderscript/Element;->getBytesSize()I

    #@7
    move-result v0

    #@8
    mul-int v6, v0, p2

    #@a
    .line 676
    .local v6, dataSize:I
    array-length v0, p3

    #@b
    mul-int/lit8 v0, v0, 0x2

    #@d
    invoke-direct {p0, p1, p2, v0, v6}, Landroid/renderscript/Allocation;->data1DChecks(IIII)V

    #@10
    .line 677
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@12
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@15
    move-result v1

    #@16
    iget v3, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@18
    move v2, p1

    #@19
    move v4, p2

    #@1a
    move-object v5, p3

    #@1b
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/RenderScript;->nAllocationData1D(IIII[SI)V

    #@1e
    .line 678
    return-void
.end method

.method public copy2DRangeFrom(IIIILandroid/renderscript/Allocation;II)V
    .registers 22
    .parameter "xoff"
    .parameter "yoff"
    .parameter "w"
    .parameter "h"
    .parameter "data"
    .parameter "dataXoff"
    .parameter "dataYoff"

    #@0
    .prologue
    .line 849
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v1}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 850
    invoke-direct/range {p0 .. p4}, Landroid/renderscript/Allocation;->validate2DRange(IIII)V

    #@8
    .line 851
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@d
    move-result v2

    #@e
    iget v5, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@10
    iget-object v3, p0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@12
    iget v6, v3, Landroid/renderscript/Type$CubemapFace;->mID:I

    #@14
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@16
    move-object/from16 v0, p5

    #@18
    invoke-virtual {v0, v3}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@1b
    move-result v9

    #@1c
    move-object/from16 v0, p5

    #@1e
    iget v12, v0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@20
    move-object/from16 v0, p5

    #@22
    iget-object v3, v0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@24
    iget v13, v3, Landroid/renderscript/Type$CubemapFace;->mID:I

    #@26
    move v3, p1

    #@27
    move/from16 v4, p2

    #@29
    move/from16 v7, p3

    #@2b
    move/from16 v8, p4

    #@2d
    move/from16 v10, p6

    #@2f
    move/from16 v11, p7

    #@31
    invoke-virtual/range {v1 .. v13}, Landroid/renderscript/RenderScript;->nAllocationData2D(IIIIIIIIIIII)V

    #@34
    .line 855
    return-void
.end method

.method public copy2DRangeFrom(IIII[B)V
    .registers 16
    .parameter "xoff"
    .parameter "yoff"
    .parameter "w"
    .parameter "h"
    .parameter "data"

    #@0
    .prologue
    .line 808
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 809
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/renderscript/Allocation;->validate2DRange(IIII)V

    #@8
    .line 810
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@d
    move-result v1

    #@e
    iget v4, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@10
    iget-object v2, p0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@12
    iget v5, v2, Landroid/renderscript/Type$CubemapFace;->mID:I

    #@14
    array-length v9, p5

    #@15
    move v2, p1

    #@16
    move v3, p2

    #@17
    move v6, p3

    #@18
    move v7, p4

    #@19
    move-object v8, p5

    #@1a
    invoke-virtual/range {v0 .. v9}, Landroid/renderscript/RenderScript;->nAllocationData2D(IIIIIII[BI)V

    #@1d
    .line 812
    return-void
.end method

.method public copy2DRangeFrom(IIII[F)V
    .registers 16
    .parameter "xoff"
    .parameter "yoff"
    .parameter "w"
    .parameter "h"
    .parameter "data"

    #@0
    .prologue
    .line 829
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 830
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/renderscript/Allocation;->validate2DRange(IIII)V

    #@8
    .line 831
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@d
    move-result v1

    #@e
    iget v4, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@10
    iget-object v2, p0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@12
    iget v5, v2, Landroid/renderscript/Type$CubemapFace;->mID:I

    #@14
    array-length v2, p5

    #@15
    mul-int/lit8 v9, v2, 0x4

    #@17
    move v2, p1

    #@18
    move v3, p2

    #@19
    move v6, p3

    #@1a
    move v7, p4

    #@1b
    move-object v8, p5

    #@1c
    invoke-virtual/range {v0 .. v9}, Landroid/renderscript/RenderScript;->nAllocationData2D(IIIIIII[FI)V

    #@1f
    .line 833
    return-void
.end method

.method public copy2DRangeFrom(IIII[I)V
    .registers 16
    .parameter "xoff"
    .parameter "yoff"
    .parameter "w"
    .parameter "h"
    .parameter "data"

    #@0
    .prologue
    .line 822
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 823
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/renderscript/Allocation;->validate2DRange(IIII)V

    #@8
    .line 824
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@d
    move-result v1

    #@e
    iget v4, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@10
    iget-object v2, p0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@12
    iget v5, v2, Landroid/renderscript/Type$CubemapFace;->mID:I

    #@14
    array-length v2, p5

    #@15
    mul-int/lit8 v9, v2, 0x4

    #@17
    move v2, p1

    #@18
    move v3, p2

    #@19
    move v6, p3

    #@1a
    move v7, p4

    #@1b
    move-object v8, p5

    #@1c
    invoke-virtual/range {v0 .. v9}, Landroid/renderscript/RenderScript;->nAllocationData2D(IIIIIII[II)V

    #@1f
    .line 826
    return-void
.end method

.method public copy2DRangeFrom(IIII[S)V
    .registers 16
    .parameter "xoff"
    .parameter "yoff"
    .parameter "w"
    .parameter "h"
    .parameter "data"

    #@0
    .prologue
    .line 815
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 816
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/renderscript/Allocation;->validate2DRange(IIII)V

    #@8
    .line 817
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@d
    move-result v1

    #@e
    iget v4, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@10
    iget-object v2, p0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@12
    iget v5, v2, Landroid/renderscript/Type$CubemapFace;->mID:I

    #@14
    array-length v2, p5

    #@15
    mul-int/lit8 v9, v2, 0x2

    #@17
    move v2, p1

    #@18
    move v3, p2

    #@19
    move v6, p3

    #@1a
    move v7, p4

    #@1b
    move-object v8, p5

    #@1c
    invoke-virtual/range {v0 .. v9}, Landroid/renderscript/RenderScript;->nAllocationData2D(IIIIIII[SI)V

    #@1f
    .line 819
    return-void
.end method

.method public copy2DRangeFrom(IILandroid/graphics/Bitmap;)V
    .registers 11
    .parameter "xoff"
    .parameter "yoff"
    .parameter "data"

    #@0
    .prologue
    .line 867
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 868
    invoke-direct {p0, p3}, Landroid/renderscript/Allocation;->validateBitmapFormat(Landroid/graphics/Bitmap;)V

    #@8
    .line 869
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    #@b
    move-result v0

    #@c
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    #@f
    move-result v1

    #@10
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/renderscript/Allocation;->validate2DRange(IIII)V

    #@13
    .line 870
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@15
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@18
    move-result v1

    #@19
    iget v4, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@1b
    iget-object v2, p0, Landroid/renderscript/Allocation;->mSelectedFace:Landroid/renderscript/Type$CubemapFace;

    #@1d
    iget v5, v2, Landroid/renderscript/Type$CubemapFace;->mID:I

    #@1f
    move v2, p1

    #@20
    move v3, p2

    #@21
    move-object v6, p3

    #@22
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/RenderScript;->nAllocationData2D(IIIIILandroid/graphics/Bitmap;)V

    #@25
    .line 871
    return-void
.end method

.method public copyFrom(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "b"

    #@0
    .prologue
    .line 564
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 565
    invoke-direct {p0, p1}, Landroid/renderscript/Allocation;->validateBitmapSize(Landroid/graphics/Bitmap;)V

    #@8
    .line 566
    invoke-direct {p0, p1}, Landroid/renderscript/Allocation;->validateBitmapFormat(Landroid/graphics/Bitmap;)V

    #@b
    .line 567
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@d
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@f
    invoke-virtual {p0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@12
    move-result v1

    #@13
    invoke-virtual {v0, v1, p1}, Landroid/renderscript/RenderScript;->nAllocationCopyFromBitmap(ILandroid/graphics/Bitmap;)V

    #@16
    .line 568
    return-void
.end method

.method public copyFrom([B)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 541
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 542
    const/4 v0, 0x0

    #@6
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@8
    invoke-virtual {p0, v0, v1, p1}, Landroid/renderscript/Allocation;->copy1DRangeFrom(II[B)V

    #@b
    .line 543
    return-void
.end method

.method public copyFrom([F)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 553
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 554
    const/4 v0, 0x0

    #@6
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@8
    invoke-virtual {p0, v0, v1, p1}, Landroid/renderscript/Allocation;->copy1DRangeFrom(II[F)V

    #@b
    .line 555
    return-void
.end method

.method public copyFrom([I)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 517
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 518
    const/4 v0, 0x0

    #@6
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@8
    invoke-virtual {p0, v0, v1, p1}, Landroid/renderscript/Allocation;->copy1DRangeFrom(II[I)V

    #@b
    .line 519
    return-void
.end method

.method public copyFrom([Landroid/renderscript/BaseObj;)V
    .registers 7
    .parameter "d"

    #@0
    .prologue
    .line 399
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v2}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 400
    invoke-direct {p0}, Landroid/renderscript/Allocation;->validateIsObject()V

    #@8
    .line 401
    array-length v2, p1

    #@9
    iget v3, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@b
    if-eq v2, v3, :cond_33

    #@d
    .line 402
    new-instance v2, Landroid/renderscript/RSIllegalArgumentException;

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "Array size mismatch, allocation sizeX = "

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    iget v4, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, ", array length = "

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    array-length v4, p1

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-direct {v2, v3}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@32
    throw v2

    #@33
    .line 405
    :cond_33
    array-length v2, p1

    #@34
    new-array v1, v2, [I

    #@36
    .line 406
    .local v1, i:[I
    const/4 v0, 0x0

    #@37
    .local v0, ct:I
    :goto_37
    array-length v2, p1

    #@38
    if-ge v0, v2, :cond_47

    #@3a
    .line 407
    aget-object v2, p1, v0

    #@3c
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@3e
    invoke-virtual {v2, v3}, Landroid/renderscript/BaseObj;->getID(Landroid/renderscript/RenderScript;)I

    #@41
    move-result v2

    #@42
    aput v2, v1, v0

    #@44
    .line 406
    add-int/lit8 v0, v0, 0x1

    #@46
    goto :goto_37

    #@47
    .line 409
    :cond_47
    const/4 v2, 0x0

    #@48
    iget v3, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@4a
    invoke-virtual {p0, v2, v3, v1}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[I)V

    #@4d
    .line 410
    return-void
.end method

.method public copyFrom([S)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 529
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 530
    const/4 v0, 0x0

    #@6
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@8
    invoke-virtual {p0, v0, v1, p1}, Landroid/renderscript/Allocation;->copy1DRangeFrom(II[S)V

    #@b
    .line 531
    return-void
.end method

.method public copyFromUnchecked([B)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 494
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 495
    const/4 v0, 0x0

    #@6
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@8
    invoke-virtual {p0, v0, v1, p1}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[B)V

    #@b
    .line 496
    return-void
.end method

.method public copyFromUnchecked([F)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 505
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 506
    const/4 v0, 0x0

    #@6
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@8
    invoke-virtual {p0, v0, v1, p1}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[F)V

    #@b
    .line 507
    return-void
.end method

.method public copyFromUnchecked([I)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 472
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 473
    const/4 v0, 0x0

    #@6
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@8
    invoke-virtual {p0, v0, v1, p1}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[I)V

    #@b
    .line 474
    return-void
.end method

.method public copyFromUnchecked([S)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 483
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 484
    const/4 v0, 0x0

    #@6
    iget v1, p0, Landroid/renderscript/Allocation;->mCurrentCount:I

    #@8
    invoke-virtual {p0, v0, v1, p1}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[S)V

    #@b
    .line 485
    return-void
.end method

.method public copyTo(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "b"

    #@0
    .prologue
    .line 881
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 882
    invoke-direct {p0, p1}, Landroid/renderscript/Allocation;->validateBitmapFormat(Landroid/graphics/Bitmap;)V

    #@8
    .line 883
    invoke-direct {p0, p1}, Landroid/renderscript/Allocation;->validateBitmapSize(Landroid/graphics/Bitmap;)V

    #@b
    .line 884
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@d
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@f
    invoke-virtual {p0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@12
    move-result v1

    #@13
    invoke-virtual {v0, v1, p1}, Landroid/renderscript/RenderScript;->nAllocationCopyToBitmap(ILandroid/graphics/Bitmap;)V

    #@16
    .line 885
    return-void
.end method

.method public copyTo([B)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 895
    invoke-direct {p0}, Landroid/renderscript/Allocation;->validateIsInt8()V

    #@3
    .line 896
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@5
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@8
    .line 897
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@c
    invoke-virtual {p0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@f
    move-result v1

    #@10
    invoke-virtual {v0, v1, p1}, Landroid/renderscript/RenderScript;->nAllocationRead(I[B)V

    #@13
    .line 898
    return-void
.end method

.method public copyTo([F)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 934
    invoke-direct {p0}, Landroid/renderscript/Allocation;->validateIsFloat32()V

    #@3
    .line 935
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@5
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@8
    .line 936
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@c
    invoke-virtual {p0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@f
    move-result v1

    #@10
    invoke-virtual {v0, v1, p1}, Landroid/renderscript/RenderScript;->nAllocationRead(I[F)V

    #@13
    .line 937
    return-void
.end method

.method public copyTo([I)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 921
    invoke-direct {p0}, Landroid/renderscript/Allocation;->validateIsInt32()V

    #@3
    .line 922
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@5
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@8
    .line 923
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@c
    invoke-virtual {p0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@f
    move-result v1

    #@10
    invoke-virtual {v0, v1, p1}, Landroid/renderscript/RenderScript;->nAllocationRead(I[I)V

    #@13
    .line 924
    return-void
.end method

.method public copyTo([S)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 908
    invoke-direct {p0}, Landroid/renderscript/Allocation;->validateIsInt16()V

    #@3
    .line 909
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@5
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@8
    .line 910
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@c
    invoke-virtual {p0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@f
    move-result v1

    #@10
    invoke-virtual {v0, v1, p1}, Landroid/renderscript/RenderScript;->nAllocationRead(I[S)V

    #@13
    .line 911
    return-void
.end method

.method public generateMipmaps()V
    .registers 3

    #@0
    .prologue
    .line 648
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScript;->nAllocationGenerateMipmaps(I)V

    #@b
    .line 649
    return-void
.end method

.method public getBytesSize()I
    .registers 3

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/Type;->getCount()I

    #@5
    move-result v0

    #@6
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@8
    invoke-virtual {v1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Landroid/renderscript/Element;->getBytesSize()I

    #@f
    move-result v1

    #@10
    mul-int/2addr v0, v1

    #@11
    return v0
.end method

.method public getElement()Landroid/renderscript/Element;
    .registers 2

    #@0
    .prologue
    .line 199
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSurface()Landroid/view/Surface;
    .registers 3

    #@0
    .prologue
    .line 1174
    new-instance v0, Landroid/view/Surface;

    #@2
    invoke-virtual {p0}, Landroid/renderscript/Allocation;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    #@9
    return-object v0
.end method

.method public getSurfaceTexture()Landroid/graphics/SurfaceTexture;
    .registers 5

    #@0
    .prologue
    .line 1154
    iget v2, p0, Landroid/renderscript/Allocation;->mUsage:I

    #@2
    and-int/lit8 v2, v2, 0x20

    #@4
    if-nez v2, :cond_e

    #@6
    .line 1155
    new-instance v2, Landroid/renderscript/RSInvalidStateException;

    #@8
    const-string v3, "Allocation is not a surface texture."

    #@a
    invoke-direct {v2, v3}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v2

    #@e
    .line 1158
    :cond_e
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@10
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@12
    invoke-virtual {p0, v3}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@15
    move-result v3

    #@16
    invoke-virtual {v2, v3}, Landroid/renderscript/RenderScript;->nAllocationGetSurfaceTextureID(I)I

    #@19
    move-result v0

    #@1a
    .line 1159
    .local v0, id:I
    new-instance v1, Landroid/graphics/SurfaceTexture;

    #@1c
    invoke-direct {v1, v0}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    #@1f
    .line 1160
    .local v1, st:Landroid/graphics/SurfaceTexture;
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@21
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@23
    invoke-virtual {p0, v3}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@26
    move-result v3

    #@27
    invoke-virtual {v2, v3, v1}, Landroid/renderscript/RenderScript;->nAllocationGetSurfaceTextureID2(ILandroid/graphics/SurfaceTexture;)V

    #@2a
    .line 1162
    return-object v1
.end method

.method public getType()Landroid/renderscript/Type;
    .registers 2

    #@0
    .prologue
    .line 336
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    return-object v0
.end method

.method public getUsage()I
    .registers 2

    #@0
    .prologue
    .line 210
    iget v0, p0, Landroid/renderscript/Allocation;->mUsage:I

    #@2
    return v0
.end method

.method public ioReceive()V
    .registers 3

    #@0
    .prologue
    .line 385
    iget v0, p0, Landroid/renderscript/Allocation;->mUsage:I

    #@2
    and-int/lit8 v0, v0, 0x20

    #@4
    if-nez v0, :cond_e

    #@6
    .line 386
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@8
    const-string v1, "Can only receive if IO_INPUT usage specified."

    #@a
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 389
    :cond_e
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@10
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@13
    .line 390
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@15
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@17
    invoke-virtual {p0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@1a
    move-result v1

    #@1b
    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScript;->nAllocationIoReceive(I)V

    #@1e
    .line 391
    return-void
.end method

.method public ioSend()V
    .registers 3

    #@0
    .prologue
    .line 364
    iget v0, p0, Landroid/renderscript/Allocation;->mUsage:I

    #@2
    and-int/lit8 v0, v0, 0x40

    #@4
    if-nez v0, :cond_e

    #@6
    .line 365
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@8
    const-string v1, "Can only send buffer if IO_OUTPUT usage specified."

    #@a
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 368
    :cond_e
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@10
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@13
    .line 369
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@15
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@17
    invoke-virtual {p0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@1a
    move-result v1

    #@1b
    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScript;->nAllocationIoSend(I)V

    #@1e
    .line 370
    return-void
.end method

.method public ioSendOutput()V
    .registers 1

    #@0
    .prologue
    .line 377
    invoke-virtual {p0}, Landroid/renderscript/Allocation;->ioSend()V

    #@3
    .line 378
    return-void
.end method

.method public declared-synchronized resize(I)V
    .registers 5
    .parameter "dimX"

    #@0
    .prologue
    .line 952
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@3
    invoke-virtual {v1}, Landroid/renderscript/Type;->getY()I

    #@6
    move-result v1

    #@7
    if-gtz v1, :cond_21

    #@9
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@b
    invoke-virtual {v1}, Landroid/renderscript/Type;->getZ()I

    #@e
    move-result v1

    #@f
    if-gtz v1, :cond_21

    #@11
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@13
    invoke-virtual {v1}, Landroid/renderscript/Type;->hasFaces()Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_21

    #@19
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@1b
    invoke-virtual {v1}, Landroid/renderscript/Type;->hasMipmaps()Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_2c

    #@21
    .line 953
    :cond_21
    new-instance v1, Landroid/renderscript/RSInvalidStateException;

    #@23
    const-string v2, "Resize only support for 1D allocations at this time."

    #@25
    invoke-direct {v1, v2}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@28
    throw v1
    :try_end_29
    .catchall {:try_start_1 .. :try_end_29} :catchall_29

    #@29
    .line 952
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit p0

    #@2b
    throw v1

    #@2c
    .line 955
    :cond_2c
    :try_start_2c
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2e
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@30
    invoke-virtual {p0, v2}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@33
    move-result v2

    #@34
    invoke-virtual {v1, v2, p1}, Landroid/renderscript/RenderScript;->nAllocationResize1D(II)V

    #@37
    .line 956
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@39
    invoke-virtual {v1}, Landroid/renderscript/RenderScript;->finish()V

    #@3c
    .line 958
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@3e
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@40
    invoke-virtual {p0, v2}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@43
    move-result v2

    #@44
    invoke-virtual {v1, v2}, Landroid/renderscript/RenderScript;->nAllocationGetType(I)I

    #@47
    move-result v0

    #@48
    .line 959
    .local v0, typeID:I
    new-instance v1, Landroid/renderscript/Type;

    #@4a
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4c
    invoke-direct {v1, v0, v2}, Landroid/renderscript/Type;-><init>(ILandroid/renderscript/RenderScript;)V

    #@4f
    iput-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@51
    .line 960
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@53
    invoke-virtual {v1}, Landroid/renderscript/Type;->updateFromNative()V

    #@56
    .line 961
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@58
    invoke-direct {p0, v1}, Landroid/renderscript/Allocation;->updateCacheInfo(Landroid/renderscript/Type;)V
    :try_end_5b
    .catchall {:try_start_2c .. :try_end_5b} :catchall_29

    #@5b
    .line 962
    monitor-exit p0

    #@5c
    return-void
.end method

.method public resize(II)V
    .registers 6
    .parameter "dimX"
    .parameter "dimY"

    #@0
    .prologue
    .line 979
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@2
    invoke-virtual {v1}, Landroid/renderscript/Type;->getZ()I

    #@5
    move-result v1

    #@6
    if-gtz v1, :cond_18

    #@8
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@a
    invoke-virtual {v1}, Landroid/renderscript/Type;->hasFaces()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_18

    #@10
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@12
    invoke-virtual {v1}, Landroid/renderscript/Type;->hasMipmaps()Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_20

    #@18
    .line 980
    :cond_18
    new-instance v1, Landroid/renderscript/RSInvalidStateException;

    #@1a
    const-string v2, "Resize only support for 2D allocations at this time."

    #@1c
    invoke-direct {v1, v2}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v1

    #@20
    .line 983
    :cond_20
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@22
    invoke-virtual {v1}, Landroid/renderscript/Type;->getY()I

    #@25
    move-result v1

    #@26
    if-nez v1, :cond_30

    #@28
    .line 984
    new-instance v1, Landroid/renderscript/RSInvalidStateException;

    #@2a
    const-string v2, "Resize only support for 2D allocations at this time."

    #@2c
    invoke-direct {v1, v2}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@2f
    throw v1

    #@30
    .line 987
    :cond_30
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@32
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@34
    invoke-virtual {p0, v2}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@37
    move-result v2

    #@38
    invoke-virtual {v1, v2, p1, p2}, Landroid/renderscript/RenderScript;->nAllocationResize2D(III)V

    #@3b
    .line 988
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@3d
    invoke-virtual {v1}, Landroid/renderscript/RenderScript;->finish()V

    #@40
    .line 990
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@42
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@44
    invoke-virtual {p0, v2}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@47
    move-result v2

    #@48
    invoke-virtual {v1, v2}, Landroid/renderscript/RenderScript;->nAllocationGetType(I)I

    #@4b
    move-result v0

    #@4c
    .line 991
    .local v0, typeID:I
    new-instance v1, Landroid/renderscript/Type;

    #@4e
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@50
    invoke-direct {v1, v0, v2}, Landroid/renderscript/Type;-><init>(ILandroid/renderscript/RenderScript;)V

    #@53
    iput-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@55
    .line 992
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@57
    invoke-virtual {v1}, Landroid/renderscript/Type;->updateFromNative()V

    #@5a
    .line 993
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@5c
    invoke-direct {p0, v1}, Landroid/renderscript/Allocation;->updateCacheInfo(Landroid/renderscript/Type;)V

    #@5f
    .line 994
    return-void
.end method

.method public setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    .registers 12
    .parameter "xoff"
    .parameter "component_number"
    .parameter "fp"

    #@0
    .prologue
    .line 599
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 600
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@7
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@9
    iget-object v0, v0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@b
    array-length v0, v0

    #@c
    if-lt p2, v0, :cond_2d

    #@e
    .line 601
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "Component_number "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " out of range."

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 603
    :cond_2d
    if-gez p1, :cond_37

    #@2f
    .line 604
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@31
    const-string v1, "Offset must be >= 0."

    #@33
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@36
    throw v0

    #@37
    .line 607
    :cond_37
    invoke-virtual {p3}, Landroid/renderscript/FieldPacker;->getData()[B

    #@3a
    move-result-object v5

    #@3b
    .line 608
    .local v5, data:[B
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@3d
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@3f
    iget-object v0, v0, Landroid/renderscript/Element;->mElements:[Landroid/renderscript/Element;

    #@41
    aget-object v0, v0, p2

    #@43
    invoke-virtual {v0}, Landroid/renderscript/Element;->getBytesSize()I

    #@46
    move-result v7

    #@47
    .line 609
    .local v7, eSize:I
    iget-object v0, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@49
    iget-object v0, v0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@4b
    iget-object v0, v0, Landroid/renderscript/Element;->mArraySizes:[I

    #@4d
    aget v0, v0, p2

    #@4f
    mul-int/2addr v7, v0

    #@50
    .line 611
    array-length v0, v5

    #@51
    if-eq v0, v7, :cond_7d

    #@53
    .line 612
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@55
    new-instance v1, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v2, "Field packer sizelength "

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    array-length v2, v5

    #@61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    const-string v2, " does not match component size "

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    const-string v2, "."

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v1

    #@79
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7c
    throw v0

    #@7d
    .line 616
    :cond_7d
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@7f
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@82
    move-result v1

    #@83
    iget v3, p0, Landroid/renderscript/Allocation;->mSelectedLOD:I

    #@85
    array-length v6, v5

    #@86
    move v2, p1

    #@87
    move v4, p2

    #@88
    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/RenderScript;->nAllocationElementData1D(IIII[BI)V

    #@8b
    .line 618
    return-void
.end method

.method public setFromFieldPacker(ILandroid/renderscript/FieldPacker;)V
    .registers 9
    .parameter "xoff"
    .parameter "fp"

    #@0
    .prologue
    .line 578
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v3}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 579
    iget-object v3, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@7
    iget-object v3, v3, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    #@9
    invoke-virtual {v3}, Landroid/renderscript/Element;->getBytesSize()I

    #@c
    move-result v2

    #@d
    .line 580
    .local v2, eSize:I
    invoke-virtual {p2}, Landroid/renderscript/FieldPacker;->getData()[B

    #@10
    move-result-object v1

    #@11
    .line 582
    .local v1, data:[B
    array-length v3, v1

    #@12
    div-int v0, v3, v2

    #@14
    .line 583
    .local v0, count:I
    mul-int v3, v2, v0

    #@16
    array-length v4, v1

    #@17
    if-eq v3, v4, :cond_43

    #@19
    .line 584
    new-instance v3, Landroid/renderscript/RSIllegalArgumentException;

    #@1b
    new-instance v4, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v5, "Field packer length "

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    array-length v5, v1

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, " not divisible by element size "

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, "."

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-direct {v3, v4}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@42
    throw v3

    #@43
    .line 587
    :cond_43
    invoke-virtual {p0, p1, v0, v1}, Landroid/renderscript/Allocation;->copy1DRangeFromUnchecked(II[B)V

    #@46
    .line 588
    return-void
.end method

.method public setSurface(Landroid/view/Surface;)V
    .registers 4
    .parameter "sur"

    #@0
    .prologue
    .line 1183
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 1184
    iget v0, p0, Landroid/renderscript/Allocation;->mUsage:I

    #@7
    and-int/lit8 v0, v0, 0x40

    #@9
    if-nez v0, :cond_13

    #@b
    .line 1185
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@d
    const-string v1, "Allocation is not USAGE_IO_OUTPUT."

    #@f
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 1188
    :cond_13
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@15
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@17
    invoke-virtual {p0, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@1a
    move-result v1

    #@1b
    invoke-virtual {v0, v1, p1}, Landroid/renderscript/RenderScript;->nAllocationSetSurface(ILandroid/view/Surface;)V

    #@1e
    .line 1189
    return-void
.end method

.method public setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    .registers 5
    .parameter "st"

    #@0
    .prologue
    .line 1195
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v1}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 1196
    iget v1, p0, Landroid/renderscript/Allocation;->mUsage:I

    #@7
    and-int/lit8 v1, v1, 0x40

    #@9
    if-nez v1, :cond_13

    #@b
    .line 1197
    new-instance v1, Landroid/renderscript/RSInvalidStateException;

    #@d
    const-string v2, "Allocation is not USAGE_IO_OUTPUT."

    #@f
    invoke-direct {v1, v2}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@12
    throw v1

    #@13
    .line 1200
    :cond_13
    new-instance v0, Landroid/view/Surface;

    #@15
    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    #@18
    .line 1201
    .local v0, s:Landroid/view/Surface;
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1a
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1c
    invoke-virtual {p0, v2}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@1f
    move-result v2

    #@20
    invoke-virtual {v1, v2, v0}, Landroid/renderscript/RenderScript;->nAllocationSetSurface(ILandroid/view/Surface;)V

    #@23
    .line 1202
    return-void
.end method

.method public syncAll(I)V
    .registers 4
    .parameter "srcLocation"

    #@0
    .prologue
    .line 345
    packed-switch p1, :pswitch_data_1a

    #@3
    .line 352
    :pswitch_3
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@5
    const-string v1, "Source must be exactly one usage type."

    #@7
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 354
    :pswitch_b
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@d
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@10
    .line 355
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@12
    invoke-direct {p0}, Landroid/renderscript/Allocation;->getIDSafe()I

    #@15
    move-result v1

    #@16
    invoke-virtual {v0, v1, p1}, Landroid/renderscript/RenderScript;->nAllocationSyncAll(II)V

    #@19
    .line 356
    return-void

    #@1a
    .line 345
    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_b
        :pswitch_b
        :pswitch_3
        :pswitch_b
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_b
    .end packed-switch
.end method

.method updateFromNative()V
    .registers 4

    #@0
    .prologue
    .line 320
    invoke-super {p0}, Landroid/renderscript/BaseObj;->updateFromNative()V

    #@3
    .line 321
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@5
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@7
    invoke-virtual {p0, v2}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@a
    move-result v2

    #@b
    invoke-virtual {v1, v2}, Landroid/renderscript/RenderScript;->nAllocationGetType(I)I

    #@e
    move-result v0

    #@f
    .line 322
    .local v0, typeID:I
    if-eqz v0, :cond_24

    #@11
    .line 323
    new-instance v1, Landroid/renderscript/Type;

    #@13
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@15
    invoke-direct {v1, v0, v2}, Landroid/renderscript/Type;-><init>(ILandroid/renderscript/RenderScript;)V

    #@18
    iput-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@1a
    .line 324
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@1c
    invoke-virtual {v1}, Landroid/renderscript/Type;->updateFromNative()V

    #@1f
    .line 325
    iget-object v1, p0, Landroid/renderscript/Allocation;->mType:Landroid/renderscript/Type;

    #@21
    invoke-direct {p0, v1}, Landroid/renderscript/Allocation;->updateCacheInfo(Landroid/renderscript/Type;)V

    #@24
    .line 327
    :cond_24
    return-void
.end method
