.class public Landroid/renderscript/RSSurfaceView;
.super Landroid/view/SurfaceView;
.source "RSSurfaceView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field private mRS:Landroid/renderscript/RenderScriptGL;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    #@3
    .line 54
    invoke-direct {p0}, Landroid/renderscript/RSSurfaceView;->init()V

    #@6
    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 66
    invoke-direct {p0}, Landroid/renderscript/RSSurfaceView;->init()V

    #@6
    .line 68
    return-void
.end method

.method private init()V
    .registers 2

    #@0
    .prologue
    .line 73
    invoke-virtual {p0}, Landroid/renderscript/RSSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@3
    move-result-object v0

    #@4
    .line 74
    .local v0, holder:Landroid/view/SurfaceHolder;
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    #@7
    .line 75
    return-void
.end method


# virtual methods
.method public createRenderScriptGL(Landroid/renderscript/RenderScriptGL$SurfaceConfig;)Landroid/renderscript/RenderScriptGL;
    .registers 4
    .parameter "sc"

    #@0
    .prologue
    .line 144
    new-instance v0, Landroid/renderscript/RenderScriptGL;

    #@2
    invoke-virtual {p0}, Landroid/renderscript/RSSurfaceView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/renderscript/RenderScriptGL;-><init>(Landroid/content/Context;Landroid/renderscript/RenderScriptGL$SurfaceConfig;)V

    #@9
    .line 145
    .local v0, rs:Landroid/renderscript/RenderScriptGL;
    invoke-virtual {p0, v0}, Landroid/renderscript/RSSurfaceView;->setRenderScriptGL(Landroid/renderscript/RenderScriptGL;)V

    #@c
    .line 146
    return-object v0
.end method

.method public destroyRenderScriptGL()V
    .registers 2

    #@0
    .prologue
    .line 153
    monitor-enter p0

    #@1
    .line 154
    :try_start_1
    iget-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@3
    invoke-virtual {v0}, Landroid/renderscript/RenderScriptGL;->destroy()V

    #@6
    .line 155
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@9
    .line 156
    monitor-exit p0

    #@a
    .line 157
    return-void

    #@b
    .line 156
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public getRenderScriptGL()Landroid/renderscript/RenderScriptGL;
    .registers 2

    #@0
    .prologue
    .line 170
    iget-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@2
    return-object v0
.end method

.method public pause()V
    .registers 2

    #@0
    .prologue
    .line 121
    iget-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 122
    iget-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@6
    invoke-virtual {v0}, Landroid/renderscript/RenderScriptGL;->pause()V

    #@9
    .line 124
    :cond_9
    return-void
.end method

.method public resume()V
    .registers 2

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 136
    iget-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@6
    invoke-virtual {v0}, Landroid/renderscript/RenderScriptGL;->resume()V

    #@9
    .line 138
    :cond_9
    return-void
.end method

.method public setRenderScriptGL(Landroid/renderscript/RenderScriptGL;)V
    .registers 2
    .parameter "rs"

    #@0
    .prologue
    .line 163
    iput-object p1, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@2
    .line 164
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .registers 6
    .parameter "holder"
    .parameter "format"
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 106
    monitor-enter p0

    #@1
    .line 107
    :try_start_1
    iget-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 108
    iget-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@7
    invoke-virtual {v0, p1, p3, p4}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    #@a
    .line 110
    :cond_a
    monitor-exit p0

    #@b
    .line 111
    return-void

    #@c
    .line 110
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .registers 2
    .parameter "holder"

    #@0
    .prologue
    .line 83
    iput-object p1, p0, Landroid/renderscript/RSSurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@2
    .line 84
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .registers 6
    .parameter "holder"

    #@0
    .prologue
    .line 92
    monitor-enter p0

    #@1
    .line 94
    :try_start_1
    iget-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 95
    iget-object v0, p0, Landroid/renderscript/RSSurfaceView;->mRS:Landroid/renderscript/RenderScriptGL;

    #@7
    const/4 v1, 0x0

    #@8
    const/4 v2, 0x0

    #@9
    const/4 v3, 0x0

    #@a
    invoke-virtual {v0, v1, v2, v3}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    #@d
    .line 97
    :cond_d
    monitor-exit p0

    #@e
    .line 98
    return-void

    #@f
    .line 97
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method
