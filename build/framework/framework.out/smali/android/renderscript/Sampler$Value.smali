.class public final enum Landroid/renderscript/Sampler$Value;
.super Ljava/lang/Enum;
.source "Sampler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Sampler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Value"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/renderscript/Sampler$Value;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/renderscript/Sampler$Value;

.field public static final enum CLAMP:Landroid/renderscript/Sampler$Value;

.field public static final enum LINEAR:Landroid/renderscript/Sampler$Value;

.field public static final enum LINEAR_MIP_LINEAR:Landroid/renderscript/Sampler$Value;

.field public static final enum LINEAR_MIP_NEAREST:Landroid/renderscript/Sampler$Value;

.field public static final enum NEAREST:Landroid/renderscript/Sampler$Value;

.field public static final enum WRAP:Landroid/renderscript/Sampler$Value;


# instance fields
.field mID:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 37
    new-instance v0, Landroid/renderscript/Sampler$Value;

    #@7
    const-string v1, "NEAREST"

    #@9
    invoke-direct {v0, v1, v3, v3}, Landroid/renderscript/Sampler$Value;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Landroid/renderscript/Sampler$Value;->NEAREST:Landroid/renderscript/Sampler$Value;

    #@e
    .line 38
    new-instance v0, Landroid/renderscript/Sampler$Value;

    #@10
    const-string v1, "LINEAR"

    #@12
    invoke-direct {v0, v1, v4, v4}, Landroid/renderscript/Sampler$Value;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    #@17
    .line 39
    new-instance v0, Landroid/renderscript/Sampler$Value;

    #@19
    const-string v1, "LINEAR_MIP_LINEAR"

    #@1b
    invoke-direct {v0, v1, v5, v5}, Landroid/renderscript/Sampler$Value;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Landroid/renderscript/Sampler$Value;->LINEAR_MIP_LINEAR:Landroid/renderscript/Sampler$Value;

    #@20
    .line 40
    new-instance v0, Landroid/renderscript/Sampler$Value;

    #@22
    const-string v1, "LINEAR_MIP_NEAREST"

    #@24
    const/4 v2, 0x5

    #@25
    invoke-direct {v0, v1, v6, v2}, Landroid/renderscript/Sampler$Value;-><init>(Ljava/lang/String;II)V

    #@28
    sput-object v0, Landroid/renderscript/Sampler$Value;->LINEAR_MIP_NEAREST:Landroid/renderscript/Sampler$Value;

    #@2a
    .line 41
    new-instance v0, Landroid/renderscript/Sampler$Value;

    #@2c
    const-string v1, "WRAP"

    #@2e
    invoke-direct {v0, v1, v7, v6}, Landroid/renderscript/Sampler$Value;-><init>(Ljava/lang/String;II)V

    #@31
    sput-object v0, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    #@33
    .line 42
    new-instance v0, Landroid/renderscript/Sampler$Value;

    #@35
    const-string v1, "CLAMP"

    #@37
    const/4 v2, 0x5

    #@38
    invoke-direct {v0, v1, v2, v7}, Landroid/renderscript/Sampler$Value;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Landroid/renderscript/Sampler$Value;->CLAMP:Landroid/renderscript/Sampler$Value;

    #@3d
    .line 36
    const/4 v0, 0x6

    #@3e
    new-array v0, v0, [Landroid/renderscript/Sampler$Value;

    #@40
    sget-object v1, Landroid/renderscript/Sampler$Value;->NEAREST:Landroid/renderscript/Sampler$Value;

    #@42
    aput-object v1, v0, v3

    #@44
    sget-object v1, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    #@46
    aput-object v1, v0, v4

    #@48
    sget-object v1, Landroid/renderscript/Sampler$Value;->LINEAR_MIP_LINEAR:Landroid/renderscript/Sampler$Value;

    #@4a
    aput-object v1, v0, v5

    #@4c
    sget-object v1, Landroid/renderscript/Sampler$Value;->LINEAR_MIP_NEAREST:Landroid/renderscript/Sampler$Value;

    #@4e
    aput-object v1, v0, v6

    #@50
    sget-object v1, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    #@52
    aput-object v1, v0, v7

    #@54
    const/4 v1, 0x5

    #@55
    sget-object v2, Landroid/renderscript/Sampler$Value;->CLAMP:Landroid/renderscript/Sampler$Value;

    #@57
    aput-object v2, v0, v1

    #@59
    sput-object v0, Landroid/renderscript/Sampler$Value;->$VALUES:[Landroid/renderscript/Sampler$Value;

    #@5b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 46
    iput p3, p0, Landroid/renderscript/Sampler$Value;->mID:I

    #@5
    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/renderscript/Sampler$Value;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 36
    const-class v0, Landroid/renderscript/Sampler$Value;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/Sampler$Value;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/renderscript/Sampler$Value;
    .registers 1

    #@0
    .prologue
    .line 36
    sget-object v0, Landroid/renderscript/Sampler$Value;->$VALUES:[Landroid/renderscript/Sampler$Value;

    #@2
    invoke-virtual {v0}, [Landroid/renderscript/Sampler$Value;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/renderscript/Sampler$Value;

    #@8
    return-object v0
.end method
