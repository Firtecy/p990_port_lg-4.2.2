.class public Landroid/renderscript/BaseObj;
.super Ljava/lang/Object;
.source "BaseObj.java"


# instance fields
.field private mDestroyed:Z

.field private mID:I

.field private mName:Ljava/lang/String;

.field mRS:Landroid/renderscript/RenderScript;


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 4
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    invoke-virtual {p2}, Landroid/renderscript/RenderScript;->validate()V

    #@6
    .line 30
    iput-object p2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@8
    .line 31
    iput p1, p0, Landroid/renderscript/BaseObj;->mID:I

    #@a
    .line 32
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/renderscript/BaseObj;->mDestroyed:Z

    #@d
    .line 33
    return-void
.end method


# virtual methods
.method checkValid()V
    .registers 3

    #@0
    .prologue
    .line 66
    iget v0, p0, Landroid/renderscript/BaseObj;->mID:I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 67
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    #@6
    const-string v1, "Invalid object."

    #@8
    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 69
    :cond_c
    return-void
.end method

.method public declared-synchronized destroy()V
    .registers 3

    #@0
    .prologue
    .line 134
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/renderscript/BaseObj;->mDestroyed:Z

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 135
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@7
    const-string v1, "Object already destroyed."

    #@9
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_d

    #@d
    .line 134
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0

    #@f
    throw v0

    #@10
    .line 137
    :cond_10
    const/4 v0, 0x1

    #@11
    :try_start_11
    iput-boolean v0, p0, Landroid/renderscript/BaseObj;->mDestroyed:Z

    #@13
    .line 138
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@15
    iget v1, p0, Landroid/renderscript/BaseObj;->mID:I

    #@17
    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScript;->nObjDestroy(I)V
    :try_end_1a
    .catchall {:try_start_11 .. :try_end_1a} :catchall_d

    #@1a
    .line 139
    monitor-exit p0

    #@1b
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 170
    if-ne p0, p1, :cond_5

    #@4
    .line 178
    :cond_4
    :goto_4
    return v1

    #@5
    .line 173
    :cond_5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@c
    move-result-object v4

    #@d
    if-eq v3, v4, :cond_11

    #@f
    move v1, v2

    #@10
    .line 174
    goto :goto_4

    #@11
    :cond_11
    move-object v0, p1

    #@12
    .line 177
    check-cast v0, Landroid/renderscript/BaseObj;

    #@14
    .line 178
    .local v0, b:Landroid/renderscript/BaseObj;
    iget v3, p0, Landroid/renderscript/BaseObj;->mID:I

    #@16
    iget v4, v0, Landroid/renderscript/BaseObj;->mID:I

    #@18
    if-eq v3, v4, :cond_4

    #@1a
    move v1, v2

    #@1b
    goto :goto_4
.end method

.method protected finalize()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Landroid/renderscript/BaseObj;->mDestroyed:Z

    #@2
    if-nez v0, :cond_20

    #@4
    .line 115
    iget v0, p0, Landroid/renderscript/BaseObj;->mID:I

    #@6
    if-eqz v0, :cond_17

    #@8
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@a
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->isAlive()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 116
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@12
    iget v1, p0, Landroid/renderscript/BaseObj;->mID:I

    #@14
    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScript;->nObjDestroy(I)V

    #@17
    .line 118
    :cond_17
    const/4 v0, 0x0

    #@18
    iput-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1a
    .line 119
    const/4 v0, 0x0

    #@1b
    iput v0, p0, Landroid/renderscript/BaseObj;->mID:I

    #@1d
    .line 120
    const/4 v0, 0x1

    #@1e
    iput-boolean v0, p0, Landroid/renderscript/BaseObj;->mDestroyed:Z

    #@20
    .line 124
    :cond_20
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@23
    .line 125
    return-void
.end method

.method getID(Landroid/renderscript/RenderScript;)I
    .registers 4
    .parameter "rs"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 53
    iget-boolean v0, p0, Landroid/renderscript/BaseObj;->mDestroyed:Z

    #@7
    if-eqz v0, :cond_12

    #@9
    .line 54
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@b
    const-string/jumbo v1, "using a destroyed object."

    #@e
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 56
    :cond_12
    iget v0, p0, Landroid/renderscript/BaseObj;->mID:I

    #@14
    if-nez v0, :cond_1e

    #@16
    .line 57
    new-instance v0, Landroid/renderscript/RSRuntimeException;

    #@18
    const-string v1, "Internal error: Object id 0."

    #@1a
    invoke-direct {v0, v1}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 59
    :cond_1e
    if-eqz p1, :cond_2d

    #@20
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@22
    if-eq p1, v0, :cond_2d

    #@24
    .line 60
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    #@26
    const-string/jumbo v1, "using object with mismatched context."

    #@29
    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 62
    :cond_2d
    iget v0, p0, Landroid/renderscript/BaseObj;->mID:I

    #@2f
    return v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 157
    iget v0, p0, Landroid/renderscript/BaseObj;->mID:I

    #@2
    return v0
.end method

.method setID(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 36
    iget v0, p0, Landroid/renderscript/BaseObj;->mID:I

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 37
    new-instance v0, Landroid/renderscript/RSRuntimeException;

    #@6
    const-string v1, "Internal Error, reset of object ID."

    #@8
    invoke-direct {v0, v1}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 39
    :cond_c
    iput p1, p0, Landroid/renderscript/BaseObj;->mID:I

    #@e
    .line 40
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .registers 6
    .parameter "name"

    #@0
    .prologue
    .line 84
    if-nez p1, :cond_b

    #@2
    .line 85
    new-instance v2, Landroid/renderscript/RSIllegalArgumentException;

    #@4
    const-string/jumbo v3, "setName requires a string of non-zero length."

    #@7
    invoke-direct {v2, v3}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 88
    :cond_b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@e
    move-result v2

    #@f
    const/4 v3, 0x1

    #@10
    if-ge v2, v3, :cond_1b

    #@12
    .line 89
    new-instance v2, Landroid/renderscript/RSIllegalArgumentException;

    #@14
    const-string/jumbo v3, "setName does not accept a zero length string."

    #@17
    invoke-direct {v2, v3}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 92
    :cond_1b
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mName:Ljava/lang/String;

    #@1d
    if-eqz v2, :cond_28

    #@1f
    .line 93
    new-instance v2, Landroid/renderscript/RSIllegalArgumentException;

    #@21
    const-string/jumbo v3, "setName object already has a name."

    #@24
    invoke-direct {v2, v3}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v2

    #@28
    .line 98
    :cond_28
    :try_start_28
    const-string v2, "UTF-8"

    #@2a
    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@2d
    move-result-object v0

    #@2e
    .line 99
    .local v0, bytes:[B
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@30
    iget v3, p0, Landroid/renderscript/BaseObj;->mID:I

    #@32
    invoke-virtual {v2, v3, v0}, Landroid/renderscript/RenderScript;->nAssignName(I[B)V

    #@35
    .line 100
    iput-object p1, p0, Landroid/renderscript/BaseObj;->mName:Ljava/lang/String;
    :try_end_37
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_28 .. :try_end_37} :catch_38

    #@37
    .line 104
    return-void

    #@38
    .line 101
    .end local v0           #bytes:[B
    :catch_38
    move-exception v1

    #@39
    .line 102
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@3b
    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@3e
    throw v2
.end method

.method updateFromNative()V
    .registers 3

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 147
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@7
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@9
    invoke-virtual {p0, v1}, Landroid/renderscript/BaseObj;->getID(Landroid/renderscript/RenderScript;)I

    #@c
    move-result v1

    #@d
    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScript;->nGetName(I)Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/renderscript/BaseObj;->mName:Ljava/lang/String;

    #@13
    .line 148
    return-void
.end method
