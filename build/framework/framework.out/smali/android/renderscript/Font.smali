.class public Landroid/renderscript/Font;
.super Landroid/renderscript/BaseObj;
.source "Font.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Font$1;,
        Landroid/renderscript/Font$Style;,
        Landroid/renderscript/Font$FontFamily;
    }
.end annotation


# static fields
.field private static sFontFamilyMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/renderscript/Font$FontFamily;",
            ">;"
        }
    .end annotation
.end field

.field private static final sMonoNames:[Ljava/lang/String;

.field private static final sSansNames:[Ljava/lang/String;

.field private static final sSerifNames:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 52
    const/4 v0, 0x5

    #@6
    new-array v0, v0, [Ljava/lang/String;

    #@8
    const-string/jumbo v1, "sans-serif"

    #@b
    aput-object v1, v0, v3

    #@d
    const-string v1, "arial"

    #@f
    aput-object v1, v0, v4

    #@11
    const-string v1, "helvetica"

    #@13
    aput-object v1, v0, v5

    #@15
    const-string/jumbo v1, "tahoma"

    #@18
    aput-object v1, v0, v6

    #@1a
    const-string/jumbo v1, "verdana"

    #@1d
    aput-object v1, v0, v7

    #@1f
    sput-object v0, Landroid/renderscript/Font;->sSansNames:[Ljava/lang/String;

    #@21
    .line 56
    const/16 v0, 0xa

    #@23
    new-array v0, v0, [Ljava/lang/String;

    #@25
    const-string/jumbo v1, "serif"

    #@28
    aput-object v1, v0, v3

    #@2a
    const-string/jumbo v1, "times"

    #@2d
    aput-object v1, v0, v4

    #@2f
    const-string/jumbo v1, "times new roman"

    #@32
    aput-object v1, v0, v5

    #@34
    const-string/jumbo v1, "palatino"

    #@37
    aput-object v1, v0, v6

    #@39
    const-string v1, "georgia"

    #@3b
    aput-object v1, v0, v7

    #@3d
    const/4 v1, 0x5

    #@3e
    const-string v2, "baskerville"

    #@40
    aput-object v2, v0, v1

    #@42
    const/4 v1, 0x6

    #@43
    const-string v2, "goudy"

    #@45
    aput-object v2, v0, v1

    #@47
    const/4 v1, 0x7

    #@48
    const-string v2, "fantasy"

    #@4a
    aput-object v2, v0, v1

    #@4c
    const/16 v1, 0x8

    #@4e
    const-string v2, "cursive"

    #@50
    aput-object v2, v0, v1

    #@52
    const/16 v1, 0x9

    #@54
    const-string v2, "ITC Stone Serif"

    #@56
    aput-object v2, v0, v1

    #@58
    sput-object v0, Landroid/renderscript/Font;->sSerifNames:[Ljava/lang/String;

    #@5a
    .line 61
    new-array v0, v7, [Ljava/lang/String;

    #@5c
    const-string/jumbo v1, "monospace"

    #@5f
    aput-object v1, v0, v3

    #@61
    const-string v1, "courier"

    #@63
    aput-object v1, v0, v4

    #@65
    const-string v1, "courier new"

    #@67
    aput-object v1, v0, v5

    #@69
    const-string/jumbo v1, "monaco"

    #@6c
    aput-object v1, v0, v6

    #@6e
    sput-object v0, Landroid/renderscript/Font;->sMonoNames:[Ljava/lang/String;

    #@70
    .line 132
    invoke-static {}, Landroid/renderscript/Font;->initFontFamilyMap()V

    #@73
    .line 133
    return-void
.end method

.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 155
    return-void
.end method

.method private static addFamilyToMap(Landroid/renderscript/Font$FontFamily;)V
    .registers 4
    .parameter "family"

    #@0
    .prologue
    .line 98
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Landroid/renderscript/Font$FontFamily;->mNames:[Ljava/lang/String;

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_12

    #@6
    .line 99
    sget-object v1, Landroid/renderscript/Font;->sFontFamilyMap:Ljava/util/Map;

    #@8
    iget-object v2, p0, Landroid/renderscript/Font$FontFamily;->mNames:[Ljava/lang/String;

    #@a
    aget-object v2, v2, v0

    #@c
    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 98
    add-int/lit8 v0, v0, 0x1

    #@11
    goto :goto_1

    #@12
    .line 101
    :cond_12
    return-void
.end method

.method public static create(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;Ljava/lang/String;Landroid/renderscript/Font$Style;F)Landroid/renderscript/Font;
    .registers 9
    .parameter "rs"
    .parameter "res"
    .parameter "familyName"
    .parameter "fontStyle"
    .parameter "pointSize"

    #@0
    .prologue
    .line 241
    invoke-static {p2, p3}, Landroid/renderscript/Font;->getFontFileName(Ljava/lang/String;Landroid/renderscript/Font$Style;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 242
    .local v0, fileName:Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 243
    .local v1, fontPath:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "/fonts/"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    .line 244
    invoke-static {p0, p1, v1, p4}, Landroid/renderscript/Font;->createFromFile(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;Ljava/lang/String;F)Landroid/renderscript/Font;

    #@26
    move-result-object v2

    #@27
    return-object v2
.end method

.method public static createFromAsset(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;Ljava/lang/String;F)Landroid/renderscript/Font;
    .registers 11
    .parameter "rs"
    .parameter "res"
    .parameter "path"
    .parameter "pointSize"

    #@0
    .prologue
    .line 185
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@3
    .line 186
    invoke-virtual {p1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@6
    move-result-object v2

    #@7
    .line 187
    .local v2, mgr:Landroid/content/res/AssetManager;
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@a
    move-result-object v4

    #@b
    iget v0, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    #@d
    .line 189
    .local v0, dpi:I
    invoke-virtual {p0, v2, p2, p3, v0}, Landroid/renderscript/RenderScript;->nFontCreateFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;FI)I

    #@10
    move-result v1

    #@11
    .line 190
    .local v1, fontId:I
    if-nez v1, :cond_2c

    #@13
    .line 191
    new-instance v4, Landroid/renderscript/RSRuntimeException;

    #@15
    new-instance v5, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v6, "Unable to create font from asset "

    #@1c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v5

    #@20
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v5

    #@28
    invoke-direct {v4, v5}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v4

    #@2c
    .line 193
    :cond_2c
    new-instance v3, Landroid/renderscript/Font;

    #@2e
    invoke-direct {v3, v1, p0}, Landroid/renderscript/Font;-><init>(ILandroid/renderscript/RenderScript;)V

    #@31
    .line 194
    .local v3, rsFont:Landroid/renderscript/Font;
    return-object v3
.end method

.method public static createFromFile(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;Ljava/io/File;F)Landroid/renderscript/Font;
    .registers 5
    .parameter "rs"
    .parameter "res"
    .parameter "path"
    .parameter "pointSize"

    #@0
    .prologue
    .line 178
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, p1, v0, p3}, Landroid/renderscript/Font;->createFromFile(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;Ljava/lang/String;F)Landroid/renderscript/Font;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static createFromFile(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;Ljava/lang/String;F)Landroid/renderscript/Font;
    .registers 10
    .parameter "rs"
    .parameter "res"
    .parameter "path"
    .parameter "pointSize"

    #@0
    .prologue
    .line 162
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@3
    .line 163
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@6
    move-result-object v3

    #@7
    iget v0, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    #@9
    .line 164
    .local v0, dpi:I
    invoke-virtual {p0, p2, p3, v0}, Landroid/renderscript/RenderScript;->nFontCreateFromFile(Ljava/lang/String;FI)I

    #@c
    move-result v1

    #@d
    .line 166
    .local v1, fontId:I
    if-nez v1, :cond_28

    #@f
    .line 167
    new-instance v3, Landroid/renderscript/RSRuntimeException;

    #@11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "Unable to create font from file "

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-direct {v3, v4}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@27
    throw v3

    #@28
    .line 169
    :cond_28
    new-instance v2, Landroid/renderscript/Font;

    #@2a
    invoke-direct {v2, v1, p0}, Landroid/renderscript/Font;-><init>(ILandroid/renderscript/RenderScript;)V

    #@2d
    .line 171
    .local v2, rsFont:Landroid/renderscript/Font;
    return-object v2
.end method

.method public static createFromResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;IF)Landroid/renderscript/Font;
    .registers 14
    .parameter "rs"
    .parameter "res"
    .parameter "id"
    .parameter "pointSize"

    #@0
    .prologue
    .line 201
    new-instance v7, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v8, "R."

    #@7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v7

    #@b
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e
    move-result-object v8

    #@f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v7

    #@13
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v5

    #@17
    .line 203
    .local v5, name:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@1a
    .line 204
    const/4 v4, 0x0

    #@1b
    .line 206
    .local v4, is:Ljava/io/InputStream;
    :try_start_1b
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1e} :catch_4f

    #@1e
    move-result-object v4

    #@1f
    .line 211
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@22
    move-result-object v7

    #@23
    iget v1, v7, Landroid/util/DisplayMetrics;->densityDpi:I

    #@25
    .line 213
    .local v1, dpi:I
    const/4 v3, 0x0

    #@26
    .line 214
    .local v3, fontId:I
    instance-of v7, v4, Landroid/content/res/AssetManager$AssetInputStream;

    #@28
    if-eqz v7, :cond_69

    #@2a
    .line 215
    check-cast v4, Landroid/content/res/AssetManager$AssetInputStream;

    #@2c
    .end local v4           #is:Ljava/io/InputStream;
    invoke-virtual {v4}, Landroid/content/res/AssetManager$AssetInputStream;->getAssetInt()I

    #@2f
    move-result v0

    #@30
    .line 216
    .local v0, asset:I
    invoke-virtual {p0, v5, p3, v1, v0}, Landroid/renderscript/RenderScript;->nFontCreateFromAssetStream(Ljava/lang/String;FII)I

    #@33
    move-result v3

    #@34
    .line 221
    if-nez v3, :cond_71

    #@36
    .line 222
    new-instance v7, Landroid/renderscript/RSRuntimeException;

    #@38
    new-instance v8, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v9, "Unable to create font from resource "

    #@3f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v8

    #@4b
    invoke-direct {v7, v8}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@4e
    throw v7

    #@4f
    .line 207
    .end local v0           #asset:I
    .end local v1           #dpi:I
    .end local v3           #fontId:I
    .restart local v4       #is:Ljava/io/InputStream;
    :catch_4f
    move-exception v2

    #@50
    .line 208
    .local v2, e:Ljava/lang/Exception;
    new-instance v7, Landroid/renderscript/RSRuntimeException;

    #@52
    new-instance v8, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v9, "Unable to open resource "

    #@59
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v8

    #@61
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v8

    #@65
    invoke-direct {v7, v8}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@68
    throw v7

    #@69
    .line 218
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v1       #dpi:I
    .restart local v3       #fontId:I
    :cond_69
    new-instance v7, Landroid/renderscript/RSRuntimeException;

    #@6b
    const-string v8, "Unsupported asset stream created"

    #@6d
    invoke-direct {v7, v8}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@70
    throw v7

    #@71
    .line 224
    .end local v4           #is:Ljava/io/InputStream;
    .restart local v0       #asset:I
    :cond_71
    new-instance v6, Landroid/renderscript/Font;

    #@73
    invoke-direct {v6, v3, p0}, Landroid/renderscript/Font;-><init>(ILandroid/renderscript/RenderScript;)V

    #@76
    .line 225
    .local v6, rsFont:Landroid/renderscript/Font;
    return-object v6
.end method

.method static getFontFileName(Ljava/lang/String;Landroid/renderscript/Font$Style;)Ljava/lang/String;
    .registers 5
    .parameter "familyName"
    .parameter "style"

    #@0
    .prologue
    .line 136
    sget-object v1, Landroid/renderscript/Font;->sFontFamilyMap:Ljava/util/Map;

    #@2
    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/renderscript/Font$FontFamily;

    #@8
    .line 137
    .local v0, family:Landroid/renderscript/Font$FontFamily;
    if-eqz v0, :cond_15

    #@a
    .line 138
    sget-object v1, Landroid/renderscript/Font$1;->$SwitchMap$android$renderscript$Font$Style:[I

    #@c
    invoke-virtual {p1}, Landroid/renderscript/Font$Style;->ordinal()I

    #@f
    move-result v2

    #@10
    aget v1, v1, v2

    #@12
    packed-switch v1, :pswitch_data_24

    #@15
    .line 150
    :cond_15
    const-string v1, "DroidSans.ttf"

    #@17
    :goto_17
    return-object v1

    #@18
    .line 140
    :pswitch_18
    iget-object v1, v0, Landroid/renderscript/Font$FontFamily;->mNormalFileName:Ljava/lang/String;

    #@1a
    goto :goto_17

    #@1b
    .line 142
    :pswitch_1b
    iget-object v1, v0, Landroid/renderscript/Font$FontFamily;->mBoldFileName:Ljava/lang/String;

    #@1d
    goto :goto_17

    #@1e
    .line 144
    :pswitch_1e
    iget-object v1, v0, Landroid/renderscript/Font$FontFamily;->mItalicFileName:Ljava/lang/String;

    #@20
    goto :goto_17

    #@21
    .line 146
    :pswitch_21
    iget-object v1, v0, Landroid/renderscript/Font$FontFamily;->mBoldItalicFileName:Ljava/lang/String;

    #@23
    goto :goto_17

    #@24
    .line 138
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
    .end packed-switch
.end method

.method private static initFontFamilyMap()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 104
    new-instance v3, Ljava/util/HashMap;

    #@3
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    #@6
    sput-object v3, Landroid/renderscript/Font;->sFontFamilyMap:Ljava/util/Map;

    #@8
    .line 106
    new-instance v1, Landroid/renderscript/Font$FontFamily;

    #@a
    invoke-direct {v1, v4}, Landroid/renderscript/Font$FontFamily;-><init>(Landroid/renderscript/Font$1;)V

    #@d
    .line 107
    .local v1, sansFamily:Landroid/renderscript/Font$FontFamily;
    sget-object v3, Landroid/renderscript/Font;->sSansNames:[Ljava/lang/String;

    #@f
    iput-object v3, v1, Landroid/renderscript/Font$FontFamily;->mNames:[Ljava/lang/String;

    #@11
    .line 108
    const-string v3, "Roboto-Regular.ttf"

    #@13
    iput-object v3, v1, Landroid/renderscript/Font$FontFamily;->mNormalFileName:Ljava/lang/String;

    #@15
    .line 109
    const-string v3, "Roboto-Bold.ttf"

    #@17
    iput-object v3, v1, Landroid/renderscript/Font$FontFamily;->mBoldFileName:Ljava/lang/String;

    #@19
    .line 110
    const-string v3, "Roboto-Italic.ttf"

    #@1b
    iput-object v3, v1, Landroid/renderscript/Font$FontFamily;->mItalicFileName:Ljava/lang/String;

    #@1d
    .line 111
    const-string v3, "Roboto-BoldItalic.ttf"

    #@1f
    iput-object v3, v1, Landroid/renderscript/Font$FontFamily;->mBoldItalicFileName:Ljava/lang/String;

    #@21
    .line 112
    invoke-static {v1}, Landroid/renderscript/Font;->addFamilyToMap(Landroid/renderscript/Font$FontFamily;)V

    #@24
    .line 114
    new-instance v2, Landroid/renderscript/Font$FontFamily;

    #@26
    invoke-direct {v2, v4}, Landroid/renderscript/Font$FontFamily;-><init>(Landroid/renderscript/Font$1;)V

    #@29
    .line 115
    .local v2, serifFamily:Landroid/renderscript/Font$FontFamily;
    sget-object v3, Landroid/renderscript/Font;->sSerifNames:[Ljava/lang/String;

    #@2b
    iput-object v3, v2, Landroid/renderscript/Font$FontFamily;->mNames:[Ljava/lang/String;

    #@2d
    .line 116
    const-string v3, "DroidSerif-Regular.ttf"

    #@2f
    iput-object v3, v2, Landroid/renderscript/Font$FontFamily;->mNormalFileName:Ljava/lang/String;

    #@31
    .line 117
    const-string v3, "DroidSerif-Bold.ttf"

    #@33
    iput-object v3, v2, Landroid/renderscript/Font$FontFamily;->mBoldFileName:Ljava/lang/String;

    #@35
    .line 118
    const-string v3, "DroidSerif-Italic.ttf"

    #@37
    iput-object v3, v2, Landroid/renderscript/Font$FontFamily;->mItalicFileName:Ljava/lang/String;

    #@39
    .line 119
    const-string v3, "DroidSerif-BoldItalic.ttf"

    #@3b
    iput-object v3, v2, Landroid/renderscript/Font$FontFamily;->mBoldItalicFileName:Ljava/lang/String;

    #@3d
    .line 120
    invoke-static {v2}, Landroid/renderscript/Font;->addFamilyToMap(Landroid/renderscript/Font$FontFamily;)V

    #@40
    .line 122
    new-instance v0, Landroid/renderscript/Font$FontFamily;

    #@42
    invoke-direct {v0, v4}, Landroid/renderscript/Font$FontFamily;-><init>(Landroid/renderscript/Font$1;)V

    #@45
    .line 123
    .local v0, monoFamily:Landroid/renderscript/Font$FontFamily;
    sget-object v3, Landroid/renderscript/Font;->sMonoNames:[Ljava/lang/String;

    #@47
    iput-object v3, v0, Landroid/renderscript/Font$FontFamily;->mNames:[Ljava/lang/String;

    #@49
    .line 124
    const-string v3, "DroidSansMono.ttf"

    #@4b
    iput-object v3, v0, Landroid/renderscript/Font$FontFamily;->mNormalFileName:Ljava/lang/String;

    #@4d
    .line 125
    const-string v3, "DroidSansMono.ttf"

    #@4f
    iput-object v3, v0, Landroid/renderscript/Font$FontFamily;->mBoldFileName:Ljava/lang/String;

    #@51
    .line 126
    const-string v3, "DroidSansMono.ttf"

    #@53
    iput-object v3, v0, Landroid/renderscript/Font$FontFamily;->mItalicFileName:Ljava/lang/String;

    #@55
    .line 127
    const-string v3, "DroidSansMono.ttf"

    #@57
    iput-object v3, v0, Landroid/renderscript/Font$FontFamily;->mBoldItalicFileName:Ljava/lang/String;

    #@59
    .line 128
    invoke-static {v0}, Landroid/renderscript/Font;->addFamilyToMap(Landroid/renderscript/Font$FontFamily;)V

    #@5c
    .line 129
    return-void
.end method
