.class public Landroid/renderscript/Program;
.super Landroid/renderscript/BaseObj;
.source "Program.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Program$BaseProgramBuilder;,
        Landroid/renderscript/Program$ProgramParam;,
        Landroid/renderscript/Program$TextureType;
    }
.end annotation


# static fields
.field static final MAX_CONSTANT:I = 0x8

.field static final MAX_INPUT:I = 0x8

.field static final MAX_OUTPUT:I = 0x8

.field static final MAX_TEXTURE:I = 0x8


# instance fields
.field mConstants:[Landroid/renderscript/Type;

.field mInputs:[Landroid/renderscript/Element;

.field mOutputs:[Landroid/renderscript/Element;

.field mShader:Ljava/lang/String;

.field mTextureCount:I

.field mTextureNames:[Ljava/lang/String;

.field mTextures:[Landroid/renderscript/Program$TextureType;


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "id"
    .parameter "rs"

    #@0
    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 78
    return-void
.end method


# virtual methods
.method public bindConstants(Landroid/renderscript/Allocation;I)V
    .registers 7
    .parameter "a"
    .parameter "slot"

    #@0
    .prologue
    .line 145
    if-ltz p2, :cond_7

    #@2
    iget-object v1, p0, Landroid/renderscript/Program;->mConstants:[Landroid/renderscript/Type;

    #@4
    array-length v1, v1

    #@5
    if-lt p2, v1, :cond_f

    #@7
    .line 146
    :cond_7
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v2, "Slot ID out of range."

    #@b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v1

    #@f
    .line 148
    :cond_f
    if-eqz p1, :cond_2f

    #@11
    invoke-virtual {p1}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    #@14
    move-result-object v1

    #@15
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@17
    invoke-virtual {v1, v2}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)I

    #@1a
    move-result v1

    #@1b
    iget-object v2, p0, Landroid/renderscript/Program;->mConstants:[Landroid/renderscript/Type;

    #@1d
    aget-object v2, v2, p2

    #@1f
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@21
    invoke-virtual {v2, v3}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)I

    #@24
    move-result v2

    #@25
    if-eq v1, v2, :cond_2f

    #@27
    .line 150
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@29
    const-string v2, "Allocation type does not match slot type."

    #@2b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v1

    #@2f
    .line 152
    :cond_2f
    if-eqz p1, :cond_43

    #@31
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@33
    invoke-virtual {p1, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@36
    move-result v0

    #@37
    .line 153
    .local v0, id:I
    :goto_37
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@39
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@3b
    invoke-virtual {p0, v2}, Landroid/renderscript/Program;->getID(Landroid/renderscript/RenderScript;)I

    #@3e
    move-result v2

    #@3f
    invoke-virtual {v1, v2, p2, v0}, Landroid/renderscript/RenderScript;->nProgramBindConstants(III)V

    #@42
    .line 154
    return-void

    #@43
    .line 152
    .end local v0           #id:I
    :cond_43
    const/4 v0, 0x0

    #@44
    goto :goto_37
.end method

.method public bindSampler(Landroid/renderscript/Sampler;I)V
    .registers 6
    .parameter "vs"
    .parameter "slot"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 189
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v1}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 190
    if-ltz p2, :cond_b

    #@7
    iget v1, p0, Landroid/renderscript/Program;->mTextureCount:I

    #@9
    if-lt p2, v1, :cond_13

    #@b
    .line 191
    :cond_b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v2, "Slot ID out of range."

    #@f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v1

    #@13
    .line 194
    :cond_13
    if-eqz p1, :cond_27

    #@15
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@17
    invoke-virtual {p1, v1}, Landroid/renderscript/Sampler;->getID(Landroid/renderscript/RenderScript;)I

    #@1a
    move-result v0

    #@1b
    .line 195
    .local v0, id:I
    :goto_1b
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1d
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1f
    invoke-virtual {p0, v2}, Landroid/renderscript/Program;->getID(Landroid/renderscript/RenderScript;)I

    #@22
    move-result v2

    #@23
    invoke-virtual {v1, v2, p2, v0}, Landroid/renderscript/RenderScript;->nProgramBindSampler(III)V

    #@26
    .line 196
    return-void

    #@27
    .line 194
    .end local v0           #id:I
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_1b
.end method

.method public bindTexture(Landroid/renderscript/Allocation;I)V
    .registers 6
    .parameter "va"
    .parameter "slot"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 165
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v1}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 166
    if-ltz p2, :cond_b

    #@7
    iget v1, p0, Landroid/renderscript/Program;->mTextureCount:I

    #@9
    if-lt p2, v1, :cond_13

    #@b
    .line 167
    :cond_b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v2, "Slot ID out of range."

    #@f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v1

    #@13
    .line 169
    :cond_13
    if-eqz p1, :cond_2f

    #@15
    invoke-virtual {p1}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Landroid/renderscript/Type;->hasFaces()Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_2f

    #@1f
    iget-object v1, p0, Landroid/renderscript/Program;->mTextures:[Landroid/renderscript/Program$TextureType;

    #@21
    aget-object v1, v1, p2

    #@23
    sget-object v2, Landroid/renderscript/Program$TextureType;->TEXTURE_CUBE:Landroid/renderscript/Program$TextureType;

    #@25
    if-eq v1, v2, :cond_2f

    #@27
    .line 171
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@29
    const-string v2, "Cannot bind cubemap to 2d texture slot"

    #@2b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v1

    #@2f
    .line 174
    :cond_2f
    if-eqz p1, :cond_43

    #@31
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@33
    invoke-virtual {p1, v1}, Landroid/renderscript/Allocation;->getID(Landroid/renderscript/RenderScript;)I

    #@36
    move-result v0

    #@37
    .line 175
    .local v0, id:I
    :goto_37
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@39
    iget-object v2, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@3b
    invoke-virtual {p0, v2}, Landroid/renderscript/Program;->getID(Landroid/renderscript/RenderScript;)I

    #@3e
    move-result v2

    #@3f
    invoke-virtual {v1, v2, p2, v0}, Landroid/renderscript/RenderScript;->nProgramBindTexture(III)V

    #@42
    .line 176
    return-void

    #@43
    .line 174
    .end local v0           #id:I
    :cond_43
    const/4 v0, 0x0

    #@44
    goto :goto_37
.end method

.method public getConstant(I)Landroid/renderscript/Type;
    .registers 4
    .parameter "slot"

    #@0
    .prologue
    .line 97
    if-ltz p1, :cond_7

    #@2
    iget-object v0, p0, Landroid/renderscript/Program;->mConstants:[Landroid/renderscript/Type;

    #@4
    array-length v0, v0

    #@5
    if-lt p1, v0, :cond_f

    #@7
    .line 98
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v1, "Slot ID out of range."

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 100
    :cond_f
    iget-object v0, p0, Landroid/renderscript/Program;->mConstants:[Landroid/renderscript/Type;

    #@11
    aget-object v0, v0, p1

    #@13
    return-object v0
.end method

.method public getConstantCount()I
    .registers 2

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/renderscript/Program;->mConstants:[Landroid/renderscript/Type;

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-object v0, p0, Landroid/renderscript/Program;->mConstants:[Landroid/renderscript/Type;

    #@6
    array-length v0, v0

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getTextureCount()I
    .registers 2

    #@0
    .prologue
    .line 108
    iget v0, p0, Landroid/renderscript/Program;->mTextureCount:I

    #@2
    return v0
.end method

.method public getTextureName(I)Ljava/lang/String;
    .registers 4
    .parameter "slot"

    #@0
    .prologue
    .line 130
    if-ltz p1, :cond_6

    #@2
    iget v0, p0, Landroid/renderscript/Program;->mTextureCount:I

    #@4
    if-lt p1, v0, :cond_e

    #@6
    .line 131
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Slot ID out of range."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 133
    :cond_e
    iget-object v0, p0, Landroid/renderscript/Program;->mTextureNames:[Ljava/lang/String;

    #@10
    aget-object v0, v0, p1

    #@12
    return-object v0
.end method

.method public getTextureType(I)Landroid/renderscript/Program$TextureType;
    .registers 4
    .parameter "slot"

    #@0
    .prologue
    .line 117
    if-ltz p1, :cond_6

    #@2
    iget v0, p0, Landroid/renderscript/Program;->mTextureCount:I

    #@4
    if-lt p1, v0, :cond_e

    #@6
    .line 118
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Slot ID out of range."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 120
    :cond_e
    iget-object v0, p0, Landroid/renderscript/Program;->mTextures:[Landroid/renderscript/Program$TextureType;

    #@10
    aget-object v0, v0, p1

    #@12
    return-object v0
.end method
