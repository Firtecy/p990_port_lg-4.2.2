.class public Landroid/renderscript/FileA3D;
.super Landroid/renderscript/BaseObj;
.source "FileA3D.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/FileA3D$1;,
        Landroid/renderscript/FileA3D$IndexEntry;,
        Landroid/renderscript/FileA3D$EntryType;
    }
.end annotation


# instance fields
.field mFileEntries:[Landroid/renderscript/FileA3D$IndexEntry;

.field mInputStream:Ljava/io/InputStream;


# direct methods
.method constructor <init>(ILandroid/renderscript/RenderScript;Ljava/io/InputStream;)V
    .registers 4
    .parameter "id"
    .parameter "rs"
    .parameter "stream"

    #@0
    .prologue
    .line 172
    invoke-direct {p0, p1, p2}, Landroid/renderscript/BaseObj;-><init>(ILandroid/renderscript/RenderScript;)V

    #@3
    .line 173
    iput-object p3, p0, Landroid/renderscript/FileA3D;->mInputStream:Ljava/io/InputStream;

    #@5
    .line 174
    return-void
.end method

.method public static createFromAsset(Landroid/renderscript/RenderScript;Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/renderscript/FileA3D;
    .registers 8
    .parameter "rs"
    .parameter "mgr"
    .parameter "path"

    #@0
    .prologue
    .line 233
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@3
    .line 234
    invoke-virtual {p0, p1, p2}, Landroid/renderscript/RenderScript;->nFileA3DCreateFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)I

    #@6
    move-result v1

    #@7
    .line 236
    .local v1, fileId:I
    if-nez v1, :cond_22

    #@9
    .line 237
    new-instance v2, Landroid/renderscript/RSRuntimeException;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Unable to create a3d file from asset "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-direct {v2, v3}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v2

    #@22
    .line 239
    :cond_22
    new-instance v0, Landroid/renderscript/FileA3D;

    #@24
    const/4 v2, 0x0

    #@25
    invoke-direct {v0, v1, p0, v2}, Landroid/renderscript/FileA3D;-><init>(ILandroid/renderscript/RenderScript;Ljava/io/InputStream;)V

    #@28
    .line 240
    .local v0, fa3d:Landroid/renderscript/FileA3D;
    invoke-direct {v0}, Landroid/renderscript/FileA3D;->initEntries()V

    #@2b
    .line 241
    return-object v0
.end method

.method public static createFromFile(Landroid/renderscript/RenderScript;Ljava/io/File;)Landroid/renderscript/FileA3D;
    .registers 3
    .parameter "rs"
    .parameter "path"

    #@0
    .prologue
    .line 274
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, v0}, Landroid/renderscript/FileA3D;->createFromFile(Landroid/renderscript/RenderScript;Ljava/lang/String;)Landroid/renderscript/FileA3D;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static createFromFile(Landroid/renderscript/RenderScript;Ljava/lang/String;)Landroid/renderscript/FileA3D;
    .registers 7
    .parameter "rs"
    .parameter "path"

    #@0
    .prologue
    .line 254
    invoke-virtual {p0, p1}, Landroid/renderscript/RenderScript;->nFileA3DCreateFromFile(Ljava/lang/String;)I

    #@3
    move-result v1

    #@4
    .line 256
    .local v1, fileId:I
    if-nez v1, :cond_1f

    #@6
    .line 257
    new-instance v2, Landroid/renderscript/RSRuntimeException;

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "Unable to create a3d file from "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-direct {v2, v3}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v2

    #@1f
    .line 259
    :cond_1f
    new-instance v0, Landroid/renderscript/FileA3D;

    #@21
    const/4 v2, 0x0

    #@22
    invoke-direct {v0, v1, p0, v2}, Landroid/renderscript/FileA3D;-><init>(ILandroid/renderscript/RenderScript;Ljava/io/InputStream;)V

    #@25
    .line 260
    .local v0, fa3d:Landroid/renderscript/FileA3D;
    invoke-direct {v0}, Landroid/renderscript/FileA3D;->initEntries()V

    #@28
    .line 261
    return-object v0
.end method

.method public static createFromResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/FileA3D;
    .registers 11
    .parameter "rs"
    .parameter "res"
    .parameter "id"

    #@0
    .prologue
    .line 289
    invoke-virtual {p0}, Landroid/renderscript/RenderScript;->validate()V

    #@3
    .line 290
    const/4 v4, 0x0

    #@4
    .line 292
    .local v4, is:Ljava/io/InputStream;
    :try_start_4
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_7} :catch_33

    #@7
    move-result-object v4

    #@8
    .line 297
    const/4 v3, 0x0

    #@9
    .line 298
    .local v3, fileId:I
    instance-of v5, v4, Landroid/content/res/AssetManager$AssetInputStream;

    #@b
    if-eqz v5, :cond_4d

    #@d
    move-object v5, v4

    #@e
    .line 299
    check-cast v5, Landroid/content/res/AssetManager$AssetInputStream;

    #@10
    invoke-virtual {v5}, Landroid/content/res/AssetManager$AssetInputStream;->getAssetInt()I

    #@13
    move-result v0

    #@14
    .line 300
    .local v0, asset:I
    invoke-virtual {p0, v0}, Landroid/renderscript/RenderScript;->nFileA3DCreateFromAssetStream(I)I

    #@17
    move-result v3

    #@18
    .line 305
    if-nez v3, :cond_55

    #@1a
    .line 306
    new-instance v5, Landroid/renderscript/RSRuntimeException;

    #@1c
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v7, "Unable to create a3d file from resource "

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-direct {v5, v6}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@32
    throw v5

    #@33
    .line 293
    .end local v0           #asset:I
    .end local v3           #fileId:I
    :catch_33
    move-exception v1

    #@34
    .line 294
    .local v1, e:Ljava/lang/Exception;
    new-instance v5, Landroid/renderscript/RSRuntimeException;

    #@36
    new-instance v6, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v7, "Unable to open resource "

    #@3d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    invoke-direct {v5, v6}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@4c
    throw v5

    #@4d
    .line 302
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v3       #fileId:I
    :cond_4d
    new-instance v5, Landroid/renderscript/RSRuntimeException;

    #@4f
    const-string v6, "Unsupported asset stream"

    #@51
    invoke-direct {v5, v6}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@54
    throw v5

    #@55
    .line 308
    .restart local v0       #asset:I
    :cond_55
    new-instance v2, Landroid/renderscript/FileA3D;

    #@57
    invoke-direct {v2, v3, p0, v4}, Landroid/renderscript/FileA3D;-><init>(ILandroid/renderscript/RenderScript;Ljava/io/InputStream;)V

    #@5a
    .line 309
    .local v2, fa3d:Landroid/renderscript/FileA3D;
    invoke-direct {v2}, Landroid/renderscript/FileA3D;->initEntries()V

    #@5d
    .line 310
    return-object v2
.end method

.method private initEntries()V
    .registers 11

    #@0
    .prologue
    .line 177
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@4
    invoke-virtual {p0, v1}, Landroid/renderscript/FileA3D;->getID(Landroid/renderscript/RenderScript;)I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScript;->nFileA3DGetNumIndexEntries(I)I

    #@b
    move-result v8

    #@c
    .line 178
    .local v8, numFileEntries:I
    if-gtz v8, :cond_f

    #@e
    .line 191
    :cond_e
    return-void

    #@f
    .line 182
    :cond_f
    new-array v0, v8, [Landroid/renderscript/FileA3D$IndexEntry;

    #@11
    iput-object v0, p0, Landroid/renderscript/FileA3D;->mFileEntries:[Landroid/renderscript/FileA3D$IndexEntry;

    #@13
    .line 183
    new-array v6, v8, [I

    #@15
    .line 184
    .local v6, ids:[I
    new-array v7, v8, [Ljava/lang/String;

    #@17
    .line 186
    .local v7, names:[Ljava/lang/String;
    iget-object v0, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@19
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@1b
    invoke-virtual {p0, v1}, Landroid/renderscript/FileA3D;->getID(Landroid/renderscript/RenderScript;)I

    #@1e
    move-result v1

    #@1f
    invoke-virtual {v0, v1, v8, v6, v7}, Landroid/renderscript/RenderScript;->nFileA3DGetIndexEntries(II[I[Ljava/lang/String;)V

    #@22
    .line 188
    const/4 v2, 0x0

    #@23
    .local v2, i:I
    :goto_23
    if-ge v2, v8, :cond_e

    #@25
    .line 189
    iget-object v9, p0, Landroid/renderscript/FileA3D;->mFileEntries:[Landroid/renderscript/FileA3D$IndexEntry;

    #@27
    new-instance v0, Landroid/renderscript/FileA3D$IndexEntry;

    #@29
    iget-object v1, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2b
    iget-object v3, p0, Landroid/renderscript/BaseObj;->mRS:Landroid/renderscript/RenderScript;

    #@2d
    invoke-virtual {p0, v3}, Landroid/renderscript/FileA3D;->getID(Landroid/renderscript/RenderScript;)I

    #@30
    move-result v3

    #@31
    aget-object v4, v7, v2

    #@33
    aget v5, v6, v2

    #@35
    invoke-static {v5}, Landroid/renderscript/FileA3D$EntryType;->toEntryType(I)Landroid/renderscript/FileA3D$EntryType;

    #@38
    move-result-object v5

    #@39
    invoke-direct/range {v0 .. v5}, Landroid/renderscript/FileA3D$IndexEntry;-><init>(Landroid/renderscript/RenderScript;IILjava/lang/String;Landroid/renderscript/FileA3D$EntryType;)V

    #@3c
    aput-object v0, v9, v2

    #@3e
    .line 188
    add-int/lit8 v2, v2, 0x1

    #@40
    goto :goto_23
.end method


# virtual methods
.method public getIndexEntry(I)Landroid/renderscript/FileA3D$IndexEntry;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 216
    invoke-virtual {p0}, Landroid/renderscript/FileA3D;->getIndexEntryCount()I

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    if-ltz p1, :cond_d

    #@8
    iget-object v0, p0, Landroid/renderscript/FileA3D;->mFileEntries:[Landroid/renderscript/FileA3D$IndexEntry;

    #@a
    array-length v0, v0

    #@b
    if-lt p1, v0, :cond_f

    #@d
    .line 217
    :cond_d
    const/4 v0, 0x0

    #@e
    .line 219
    :goto_e
    return-object v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/renderscript/FileA3D;->mFileEntries:[Landroid/renderscript/FileA3D$IndexEntry;

    #@11
    aget-object v0, v0, p1

    #@13
    goto :goto_e
.end method

.method public getIndexEntryCount()I
    .registers 2

    #@0
    .prologue
    .line 200
    iget-object v0, p0, Landroid/renderscript/FileA3D;->mFileEntries:[Landroid/renderscript/FileA3D$IndexEntry;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 201
    const/4 v0, 0x0

    #@5
    .line 203
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/renderscript/FileA3D;->mFileEntries:[Landroid/renderscript/FileA3D$IndexEntry;

    #@8
    array-length v0, v0

    #@9
    goto :goto_5
.end method
