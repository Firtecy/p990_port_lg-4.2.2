.class synthetic Landroid/renderscript/Element$1;
.super Ljava/lang/Object;
.source "Element.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/Element;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$android$renderscript$Element$DataKind:[I

.field static final synthetic $SwitchMap$android$renderscript$Element$DataType:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 933
    invoke-static {}, Landroid/renderscript/Element$DataKind;->values()[Landroid/renderscript/Element$DataKind;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataKind:[I

    #@9
    :try_start_9
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataKind:[I

    #@b
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_LA:Landroid/renderscript/Element$DataKind;

    #@d
    invoke-virtual {v1}, Landroid/renderscript/Element$DataKind;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_dc

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataKind:[I

    #@16
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_RGB:Landroid/renderscript/Element$DataKind;

    #@18
    invoke-virtual {v1}, Landroid/renderscript/Element$DataKind;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_d9

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataKind:[I

    #@21
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_RGBA:Landroid/renderscript/Element$DataKind;

    #@23
    invoke-virtual {v1}, Landroid/renderscript/Element$DataKind;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_d6

    #@2a
    :goto_2a
    :try_start_2a
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataKind:[I

    #@2c
    sget-object v1, Landroid/renderscript/Element$DataKind;->PIXEL_DEPTH:Landroid/renderscript/Element$DataKind;

    #@2e
    invoke-virtual {v1}, Landroid/renderscript/Element$DataKind;->ordinal()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_d3

    #@35
    .line 864
    :goto_35
    invoke-static {}, Landroid/renderscript/Element$DataType;->values()[Landroid/renderscript/Element$DataType;

    #@38
    move-result-object v0

    #@39
    array-length v0, v0

    #@3a
    new-array v0, v0, [I

    #@3c
    sput-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@3e
    :try_start_3e
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@40
    sget-object v1, Landroid/renderscript/Element$DataType;->FLOAT_32:Landroid/renderscript/Element$DataType;

    #@42
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@45
    move-result v1

    #@46
    const/4 v2, 0x1

    #@47
    aput v2, v0, v1
    :try_end_49
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3e .. :try_end_49} :catch_d0

    #@49
    :goto_49
    :try_start_49
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@4b
    sget-object v1, Landroid/renderscript/Element$DataType;->FLOAT_64:Landroid/renderscript/Element$DataType;

    #@4d
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@50
    move-result v1

    #@51
    const/4 v2, 0x2

    #@52
    aput v2, v0, v1
    :try_end_54
    .catch Ljava/lang/NoSuchFieldError; {:try_start_49 .. :try_end_54} :catch_ce

    #@54
    :goto_54
    :try_start_54
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@56
    sget-object v1, Landroid/renderscript/Element$DataType;->SIGNED_8:Landroid/renderscript/Element$DataType;

    #@58
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@5b
    move-result v1

    #@5c
    const/4 v2, 0x3

    #@5d
    aput v2, v0, v1
    :try_end_5f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_54 .. :try_end_5f} :catch_cc

    #@5f
    :goto_5f
    :try_start_5f
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@61
    sget-object v1, Landroid/renderscript/Element$DataType;->SIGNED_16:Landroid/renderscript/Element$DataType;

    #@63
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@66
    move-result v1

    #@67
    const/4 v2, 0x4

    #@68
    aput v2, v0, v1
    :try_end_6a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5f .. :try_end_6a} :catch_ca

    #@6a
    :goto_6a
    :try_start_6a
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@6c
    sget-object v1, Landroid/renderscript/Element$DataType;->SIGNED_32:Landroid/renderscript/Element$DataType;

    #@6e
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@71
    move-result v1

    #@72
    const/4 v2, 0x5

    #@73
    aput v2, v0, v1
    :try_end_75
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6a .. :try_end_75} :catch_c8

    #@75
    :goto_75
    :try_start_75
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@77
    sget-object v1, Landroid/renderscript/Element$DataType;->SIGNED_64:Landroid/renderscript/Element$DataType;

    #@79
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@7c
    move-result v1

    #@7d
    const/4 v2, 0x6

    #@7e
    aput v2, v0, v1
    :try_end_80
    .catch Ljava/lang/NoSuchFieldError; {:try_start_75 .. :try_end_80} :catch_c6

    #@80
    :goto_80
    :try_start_80
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@82
    sget-object v1, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    #@84
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@87
    move-result v1

    #@88
    const/4 v2, 0x7

    #@89
    aput v2, v0, v1
    :try_end_8b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_80 .. :try_end_8b} :catch_c4

    #@8b
    :goto_8b
    :try_start_8b
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@8d
    sget-object v1, Landroid/renderscript/Element$DataType;->UNSIGNED_16:Landroid/renderscript/Element$DataType;

    #@8f
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@92
    move-result v1

    #@93
    const/16 v2, 0x8

    #@95
    aput v2, v0, v1
    :try_end_97
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8b .. :try_end_97} :catch_c2

    #@97
    :goto_97
    :try_start_97
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@99
    sget-object v1, Landroid/renderscript/Element$DataType;->UNSIGNED_32:Landroid/renderscript/Element$DataType;

    #@9b
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@9e
    move-result v1

    #@9f
    const/16 v2, 0x9

    #@a1
    aput v2, v0, v1
    :try_end_a3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_97 .. :try_end_a3} :catch_c0

    #@a3
    :goto_a3
    :try_start_a3
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@a5
    sget-object v1, Landroid/renderscript/Element$DataType;->UNSIGNED_64:Landroid/renderscript/Element$DataType;

    #@a7
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@aa
    move-result v1

    #@ab
    const/16 v2, 0xa

    #@ad
    aput v2, v0, v1
    :try_end_af
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a3 .. :try_end_af} :catch_be

    #@af
    :goto_af
    :try_start_af
    sget-object v0, Landroid/renderscript/Element$1;->$SwitchMap$android$renderscript$Element$DataType:[I

    #@b1
    sget-object v1, Landroid/renderscript/Element$DataType;->BOOLEAN:Landroid/renderscript/Element$DataType;

    #@b3
    invoke-virtual {v1}, Landroid/renderscript/Element$DataType;->ordinal()I

    #@b6
    move-result v1

    #@b7
    const/16 v2, 0xb

    #@b9
    aput v2, v0, v1
    :try_end_bb
    .catch Ljava/lang/NoSuchFieldError; {:try_start_af .. :try_end_bb} :catch_bc

    #@bb
    :goto_bb
    return-void

    #@bc
    :catch_bc
    move-exception v0

    #@bd
    goto :goto_bb

    #@be
    :catch_be
    move-exception v0

    #@bf
    goto :goto_af

    #@c0
    :catch_c0
    move-exception v0

    #@c1
    goto :goto_a3

    #@c2
    :catch_c2
    move-exception v0

    #@c3
    goto :goto_97

    #@c4
    :catch_c4
    move-exception v0

    #@c5
    goto :goto_8b

    #@c6
    :catch_c6
    move-exception v0

    #@c7
    goto :goto_80

    #@c8
    :catch_c8
    move-exception v0

    #@c9
    goto :goto_75

    #@ca
    :catch_ca
    move-exception v0

    #@cb
    goto :goto_6a

    #@cc
    :catch_cc
    move-exception v0

    #@cd
    goto :goto_5f

    #@ce
    :catch_ce
    move-exception v0

    #@cf
    goto :goto_54

    #@d0
    :catch_d0
    move-exception v0

    #@d1
    goto/16 :goto_49

    #@d3
    .line 933
    :catch_d3
    move-exception v0

    #@d4
    goto/16 :goto_35

    #@d6
    :catch_d6
    move-exception v0

    #@d7
    goto/16 :goto_2a

    #@d9
    :catch_d9
    move-exception v0

    #@da
    goto/16 :goto_1f

    #@dc
    :catch_dc
    move-exception v0

    #@dd
    goto/16 :goto_14
.end method
