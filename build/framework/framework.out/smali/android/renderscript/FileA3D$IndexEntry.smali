.class public Landroid/renderscript/FileA3D$IndexEntry;
.super Ljava/lang/Object;
.source "FileA3D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/FileA3D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IndexEntry"
.end annotation


# instance fields
.field mEntryType:Landroid/renderscript/FileA3D$EntryType;

.field mID:I

.field mIndex:I

.field mLoadedObj:Landroid/renderscript/BaseObj;

.field mName:Ljava/lang/String;

.field mRS:Landroid/renderscript/RenderScript;


# direct methods
.method constructor <init>(Landroid/renderscript/RenderScript;IILjava/lang/String;Landroid/renderscript/FileA3D$EntryType;)V
    .registers 7
    .parameter "rs"
    .parameter "index"
    .parameter "id"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    .line 158
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 159
    iput-object p1, p0, Landroid/renderscript/FileA3D$IndexEntry;->mRS:Landroid/renderscript/RenderScript;

    #@5
    .line 160
    iput p2, p0, Landroid/renderscript/FileA3D$IndexEntry;->mIndex:I

    #@7
    .line 161
    iput p3, p0, Landroid/renderscript/FileA3D$IndexEntry;->mID:I

    #@9
    .line 162
    iput-object p4, p0, Landroid/renderscript/FileA3D$IndexEntry;->mName:Ljava/lang/String;

    #@b
    .line 163
    iput-object p5, p0, Landroid/renderscript/FileA3D$IndexEntry;->mEntryType:Landroid/renderscript/FileA3D$EntryType;

    #@d
    .line 164
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Landroid/renderscript/FileA3D$IndexEntry;->mLoadedObj:Landroid/renderscript/BaseObj;

    #@10
    .line 165
    return-void
.end method

.method static declared-synchronized internalCreate(Landroid/renderscript/RenderScript;Landroid/renderscript/FileA3D$IndexEntry;)Landroid/renderscript/BaseObj;
    .registers 7
    .parameter "rs"
    .parameter "entry"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 134
    const-class v2, Landroid/renderscript/FileA3D$IndexEntry;

    #@3
    monitor-enter v2

    #@4
    :try_start_4
    iget-object v3, p1, Landroid/renderscript/FileA3D$IndexEntry;->mLoadedObj:Landroid/renderscript/BaseObj;

    #@6
    if-eqz v3, :cond_c

    #@8
    .line 135
    iget-object v1, p1, Landroid/renderscript/FileA3D$IndexEntry;->mLoadedObj:Landroid/renderscript/BaseObj;
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_39

    #@a
    .line 155
    :cond_a
    :goto_a
    monitor-exit v2

    #@b
    return-object v1

    #@c
    .line 139
    :cond_c
    :try_start_c
    iget-object v3, p1, Landroid/renderscript/FileA3D$IndexEntry;->mEntryType:Landroid/renderscript/FileA3D$EntryType;

    #@e
    sget-object v4, Landroid/renderscript/FileA3D$EntryType;->UNKNOWN:Landroid/renderscript/FileA3D$EntryType;

    #@10
    if-eq v3, v4, :cond_a

    #@12
    .line 143
    iget v3, p1, Landroid/renderscript/FileA3D$IndexEntry;->mID:I

    #@14
    iget v4, p1, Landroid/renderscript/FileA3D$IndexEntry;->mIndex:I

    #@16
    invoke-virtual {p0, v3, v4}, Landroid/renderscript/RenderScript;->nFileA3DGetEntryByIndex(II)I

    #@19
    move-result v0

    #@1a
    .line 144
    .local v0, objectID:I
    if-eqz v0, :cond_a

    #@1c
    .line 148
    sget-object v1, Landroid/renderscript/FileA3D$1;->$SwitchMap$android$renderscript$FileA3D$EntryType:[I

    #@1e
    iget-object v3, p1, Landroid/renderscript/FileA3D$IndexEntry;->mEntryType:Landroid/renderscript/FileA3D$EntryType;

    #@20
    invoke-virtual {v3}, Landroid/renderscript/FileA3D$EntryType;->ordinal()I

    #@23
    move-result v3

    #@24
    aget v1, v1, v3

    #@26
    packed-switch v1, :pswitch_data_3c

    #@29
    .line 154
    :goto_29
    iget-object v1, p1, Landroid/renderscript/FileA3D$IndexEntry;->mLoadedObj:Landroid/renderscript/BaseObj;

    #@2b
    invoke-virtual {v1}, Landroid/renderscript/BaseObj;->updateFromNative()V

    #@2e
    .line 155
    iget-object v1, p1, Landroid/renderscript/FileA3D$IndexEntry;->mLoadedObj:Landroid/renderscript/BaseObj;

    #@30
    goto :goto_a

    #@31
    .line 150
    :pswitch_31
    new-instance v1, Landroid/renderscript/Mesh;

    #@33
    invoke-direct {v1, v0, p0}, Landroid/renderscript/Mesh;-><init>(ILandroid/renderscript/RenderScript;)V

    #@36
    iput-object v1, p1, Landroid/renderscript/FileA3D$IndexEntry;->mLoadedObj:Landroid/renderscript/BaseObj;
    :try_end_38
    .catchall {:try_start_c .. :try_end_38} :catchall_39

    #@38
    goto :goto_29

    #@39
    .line 134
    .end local v0           #objectID:I
    :catchall_39
    move-exception v1

    #@3a
    monitor-exit v2

    #@3b
    throw v1

    #@3c
    .line 148
    :pswitch_data_3c
    .packed-switch 0x1
        :pswitch_31
    .end packed-switch
.end method


# virtual methods
.method public getEntryType()Landroid/renderscript/FileA3D$EntryType;
    .registers 2

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/renderscript/FileA3D$IndexEntry;->mEntryType:Landroid/renderscript/FileA3D$EntryType;

    #@2
    return-object v0
.end method

.method public getMesh()Landroid/renderscript/Mesh;
    .registers 2

    #@0
    .prologue
    .line 130
    invoke-virtual {p0}, Landroid/renderscript/FileA3D$IndexEntry;->getObject()Landroid/renderscript/BaseObj;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/renderscript/Mesh;

    #@6
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Landroid/renderscript/FileA3D$IndexEntry;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getObject()Landroid/renderscript/BaseObj;
    .registers 3

    #@0
    .prologue
    .line 117
    iget-object v1, p0, Landroid/renderscript/FileA3D$IndexEntry;->mRS:Landroid/renderscript/RenderScript;

    #@2
    invoke-virtual {v1}, Landroid/renderscript/RenderScript;->validate()V

    #@5
    .line 118
    iget-object v1, p0, Landroid/renderscript/FileA3D$IndexEntry;->mRS:Landroid/renderscript/RenderScript;

    #@7
    invoke-static {v1, p0}, Landroid/renderscript/FileA3D$IndexEntry;->internalCreate(Landroid/renderscript/RenderScript;Landroid/renderscript/FileA3D$IndexEntry;)Landroid/renderscript/BaseObj;

    #@a
    move-result-object v0

    #@b
    .line 119
    .local v0, obj:Landroid/renderscript/BaseObj;
    return-object v0
.end method
