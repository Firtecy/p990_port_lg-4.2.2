.class Landroid/renderscript/RenderScript$MessageThread;
.super Ljava/lang/Thread;
.source "RenderScript.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/RenderScript;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MessageThread"
.end annotation


# static fields
.field static final RS_ERROR_FATAL_UNKNOWN:I = 0x1000

.field static final RS_MESSAGE_TO_CLIENT_ERROR:I = 0x3

.field static final RS_MESSAGE_TO_CLIENT_EXCEPTION:I = 0x1

.field static final RS_MESSAGE_TO_CLIENT_NONE:I = 0x0

.field static final RS_MESSAGE_TO_CLIENT_RESIZE:I = 0x2

.field static final RS_MESSAGE_TO_CLIENT_USER:I = 0x4


# instance fields
.field mAuxData:[I

.field mRS:Landroid/renderscript/RenderScript;

.field mRun:Z


# direct methods
.method constructor <init>(Landroid/renderscript/RenderScript;)V
    .registers 3
    .parameter "rs"

    #@0
    .prologue
    .line 894
    const-string v0, "RSMessageThread"

    #@2
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@5
    .line 882
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/renderscript/RenderScript$MessageThread;->mRun:Z

    #@8
    .line 883
    const/4 v0, 0x2

    #@9
    new-array v0, v0, [I

    #@b
    iput-object v0, p0, Landroid/renderscript/RenderScript$MessageThread;->mAuxData:[I

    #@d
    .line 895
    iput-object p1, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@f
    .line 897
    return-void
.end method


# virtual methods
.method public run()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x4

    #@1
    const/4 v8, 0x0

    #@2
    .line 902
    const/16 v5, 0x10

    #@4
    new-array v2, v5, [I

    #@6
    .line 903
    .local v2, rbuf:[I
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@8
    iget-object v6, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@a
    iget v6, v6, Landroid/renderscript/RenderScript;->mContext:I

    #@c
    invoke-virtual {v5, v6}, Landroid/renderscript/RenderScript;->nContextInitToClient(I)V

    #@f
    .line 904
    :cond_f
    :goto_f
    iget-boolean v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRun:Z

    #@11
    if-eqz v5, :cond_cd

    #@13
    .line 905
    aput v8, v2, v8

    #@15
    .line 906
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@17
    iget-object v6, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@19
    iget v6, v6, Landroid/renderscript/RenderScript;->mContext:I

    #@1b
    iget-object v7, p0, Landroid/renderscript/RenderScript$MessageThread;->mAuxData:[I

    #@1d
    invoke-virtual {v5, v6, v7}, Landroid/renderscript/RenderScript;->nContextPeekMessage(I[I)I

    #@20
    move-result v1

    #@21
    .line 907
    .local v1, msg:I
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mAuxData:[I

    #@23
    const/4 v6, 0x1

    #@24
    aget v3, v5, v6

    #@26
    .line 908
    .local v3, size:I
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mAuxData:[I

    #@28
    aget v4, v5, v8

    #@2a
    .line 910
    .local v4, subID:I
    if-ne v1, v9, :cond_73

    #@2c
    .line 911
    shr-int/lit8 v5, v3, 0x2

    #@2e
    array-length v6, v2

    #@2f
    if-lt v5, v6, :cond_37

    #@31
    .line 912
    add-int/lit8 v5, v3, 0x3

    #@33
    shr-int/lit8 v5, v5, 0x2

    #@35
    new-array v2, v5, [I

    #@37
    .line 914
    :cond_37
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@39
    iget-object v6, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@3b
    iget v6, v6, Landroid/renderscript/RenderScript;->mContext:I

    #@3d
    invoke-virtual {v5, v6, v2}, Landroid/renderscript/RenderScript;->nContextGetUserMessage(I[I)I

    #@40
    move-result v5

    #@41
    if-eq v5, v9, :cond_4b

    #@43
    .line 916
    new-instance v5, Landroid/renderscript/RSDriverException;

    #@45
    const-string v6, "Error processing message from Renderscript."

    #@47
    invoke-direct {v5, v6}, Landroid/renderscript/RSDriverException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v5

    #@4b
    .line 919
    :cond_4b
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@4d
    iget-object v5, v5, Landroid/renderscript/RenderScript;->mMessageCallback:Landroid/renderscript/RenderScript$RSMessageHandler;

    #@4f
    if-eqz v5, :cond_6b

    #@51
    .line 920
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@53
    iget-object v5, v5, Landroid/renderscript/RenderScript;->mMessageCallback:Landroid/renderscript/RenderScript$RSMessageHandler;

    #@55
    iput-object v2, v5, Landroid/renderscript/RenderScript$RSMessageHandler;->mData:[I

    #@57
    .line 921
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@59
    iget-object v5, v5, Landroid/renderscript/RenderScript;->mMessageCallback:Landroid/renderscript/RenderScript$RSMessageHandler;

    #@5b
    iput v4, v5, Landroid/renderscript/RenderScript$RSMessageHandler;->mID:I

    #@5d
    .line 922
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@5f
    iget-object v5, v5, Landroid/renderscript/RenderScript;->mMessageCallback:Landroid/renderscript/RenderScript$RSMessageHandler;

    #@61
    iput v3, v5, Landroid/renderscript/RenderScript$RSMessageHandler;->mLength:I

    #@63
    .line 923
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@65
    iget-object v5, v5, Landroid/renderscript/RenderScript;->mMessageCallback:Landroid/renderscript/RenderScript$RSMessageHandler;

    #@67
    invoke-virtual {v5}, Landroid/renderscript/RenderScript$RSMessageHandler;->run()V

    #@6a
    goto :goto_f

    #@6b
    .line 925
    :cond_6b
    new-instance v5, Landroid/renderscript/RSInvalidStateException;

    #@6d
    const-string v6, "Received a message from the script with no message handler installed."

    #@6f
    invoke-direct {v5, v6}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    #@72
    throw v5

    #@73
    .line 930
    :cond_73
    const/4 v5, 0x3

    #@74
    if-ne v1, v5, :cond_c2

    #@76
    .line 931
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@78
    iget-object v6, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@7a
    iget v6, v6, Landroid/renderscript/RenderScript;->mContext:I

    #@7c
    invoke-virtual {v5, v6}, Landroid/renderscript/RenderScript;->nContextGetErrorMessage(I)Ljava/lang/String;

    #@7f
    move-result-object v0

    #@80
    .line 933
    .local v0, e:Ljava/lang/String;
    const/16 v5, 0x1000

    #@82
    if-lt v4, v5, :cond_a7

    #@84
    .line 934
    new-instance v5, Landroid/renderscript/RSRuntimeException;

    #@86
    new-instance v6, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v7, "Fatal error "

    #@8d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v6

    #@91
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v6

    #@95
    const-string v7, ", details: "

    #@97
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v6

    #@9f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v6

    #@a3
    invoke-direct {v5, v6}, Landroid/renderscript/RSRuntimeException;-><init>(Ljava/lang/String;)V

    #@a6
    throw v5

    #@a7
    .line 937
    :cond_a7
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@a9
    iget-object v5, v5, Landroid/renderscript/RenderScript;->mErrorCallback:Landroid/renderscript/RenderScript$RSErrorHandler;

    #@ab
    if-eqz v5, :cond_f

    #@ad
    .line 938
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@af
    iget-object v5, v5, Landroid/renderscript/RenderScript;->mErrorCallback:Landroid/renderscript/RenderScript$RSErrorHandler;

    #@b1
    iput-object v0, v5, Landroid/renderscript/RenderScript$RSErrorHandler;->mErrorMessage:Ljava/lang/String;

    #@b3
    .line 939
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@b5
    iget-object v5, v5, Landroid/renderscript/RenderScript;->mErrorCallback:Landroid/renderscript/RenderScript$RSErrorHandler;

    #@b7
    iput v4, v5, Landroid/renderscript/RenderScript$RSErrorHandler;->mErrorNum:I

    #@b9
    .line 940
    iget-object v5, p0, Landroid/renderscript/RenderScript$MessageThread;->mRS:Landroid/renderscript/RenderScript;

    #@bb
    iget-object v5, v5, Landroid/renderscript/RenderScript;->mErrorCallback:Landroid/renderscript/RenderScript$RSErrorHandler;

    #@bd
    invoke-virtual {v5}, Landroid/renderscript/RenderScript$RSErrorHandler;->run()V

    #@c0
    goto/16 :goto_f

    #@c2
    .line 953
    .end local v0           #e:Ljava/lang/String;
    :cond_c2
    const-wide/16 v5, 0x1

    #@c4
    const/4 v7, 0x0

    #@c5
    :try_start_c5
    invoke-static {v5, v6, v7}, Landroid/renderscript/RenderScript$MessageThread;->sleep(JI)V
    :try_end_c8
    .catch Ljava/lang/InterruptedException; {:try_start_c5 .. :try_end_c8} :catch_ca

    #@c8
    goto/16 :goto_f

    #@ca
    .line 954
    :catch_ca
    move-exception v5

    #@cb
    goto/16 :goto_f

    #@cd
    .line 957
    .end local v1           #msg:I
    .end local v3           #size:I
    .end local v4           #subID:I
    :cond_cd
    const-string v5, "RenderScript_jni"

    #@cf
    const-string v6, "MessageThread exiting."

    #@d1
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    .line 958
    return-void
.end method
