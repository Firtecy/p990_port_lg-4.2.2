.class Landroid/drm/DrmManagerClient$InfoHandler;
.super Landroid/os/Handler;
.source "DrmManagerClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/drm/DrmManagerClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InfoHandler"
.end annotation


# static fields
.field public static final INFO_EVENT_TYPE:I = 0x1


# instance fields
.field final synthetic this$0:Landroid/drm/DrmManagerClient;


# direct methods
.method public constructor <init>(Landroid/drm/DrmManagerClient;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 189
    iput-object p1, p0, Landroid/drm/DrmManagerClient$InfoHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@2
    .line 190
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 191
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    .line 194
    const/4 v2, 0x0

    #@1
    .line 195
    .local v2, info:Landroid/drm/DrmInfoEvent;
    const/4 v1, 0x0

    #@2
    .line 197
    .local v1, error:Landroid/drm/DrmErrorEvent;
    iget v6, p1, Landroid/os/Message;->what:I

    #@4
    packed-switch v6, :pswitch_data_74

    #@7
    .line 234
    const-string v6, "DrmManagerClient"

    #@9
    new-instance v7, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v8, "Unknown message type "

    #@10
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v7

    #@14
    iget v8, p1, Landroid/os/Message;->what:I

    #@16
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 235
    :cond_21
    :goto_21
    return-void

    #@22
    .line 199
    :pswitch_22
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@24
    .line 200
    .local v5, uniqueId:I
    iget v3, p1, Landroid/os/Message;->arg2:I

    #@26
    .line 201
    .local v3, infoType:I
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@28
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    .line 203
    .local v4, message:Ljava/lang/String;
    packed-switch v3, :pswitch_data_7a

    #@2f
    .line 222
    new-instance v1, Landroid/drm/DrmErrorEvent;

    #@31
    .end local v1           #error:Landroid/drm/DrmErrorEvent;
    invoke-direct {v1, v5, v3, v4}, Landroid/drm/DrmErrorEvent;-><init>(IILjava/lang/String;)V

    #@34
    .line 226
    .restart local v1       #error:Landroid/drm/DrmErrorEvent;
    :goto_34
    iget-object v6, p0, Landroid/drm/DrmManagerClient$InfoHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@36
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$700(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnInfoListener;

    #@39
    move-result-object v6

    #@3a
    if-eqz v6, :cond_49

    #@3c
    if-eqz v2, :cond_49

    #@3e
    .line 227
    iget-object v6, p0, Landroid/drm/DrmManagerClient$InfoHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@40
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$700(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnInfoListener;

    #@43
    move-result-object v6

    #@44
    iget-object v7, p0, Landroid/drm/DrmManagerClient$InfoHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@46
    invoke-interface {v6, v7, v2}, Landroid/drm/DrmManagerClient$OnInfoListener;->onInfo(Landroid/drm/DrmManagerClient;Landroid/drm/DrmInfoEvent;)V

    #@49
    .line 229
    :cond_49
    iget-object v6, p0, Landroid/drm/DrmManagerClient$InfoHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@4b
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$600(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnErrorListener;

    #@4e
    move-result-object v6

    #@4f
    if-eqz v6, :cond_21

    #@51
    if-eqz v1, :cond_21

    #@53
    .line 230
    iget-object v6, p0, Landroid/drm/DrmManagerClient$InfoHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@55
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$600(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnErrorListener;

    #@58
    move-result-object v6

    #@59
    iget-object v7, p0, Landroid/drm/DrmManagerClient$InfoHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@5b
    invoke-interface {v6, v7, v1}, Landroid/drm/DrmManagerClient$OnErrorListener;->onError(Landroid/drm/DrmManagerClient;Landroid/drm/DrmErrorEvent;)V

    #@5e
    goto :goto_21

    #@5f
    .line 206
    :pswitch_5f
    :try_start_5f
    invoke-static {v4}, Landroid/drm/DrmUtils;->removeFile(Ljava/lang/String;)V
    :try_end_62
    .catch Ljava/io/IOException; {:try_start_5f .. :try_end_62} :catch_68

    #@62
    .line 210
    :goto_62
    new-instance v2, Landroid/drm/DrmInfoEvent;

    #@64
    .end local v2           #info:Landroid/drm/DrmInfoEvent;
    invoke-direct {v2, v5, v3, v4}, Landroid/drm/DrmInfoEvent;-><init>(IILjava/lang/String;)V

    #@67
    .line 211
    .restart local v2       #info:Landroid/drm/DrmInfoEvent;
    goto :goto_34

    #@68
    .line 207
    :catch_68
    move-exception v0

    #@69
    .line 208
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@6c
    goto :goto_62

    #@6d
    .line 218
    .end local v0           #e:Ljava/io/IOException;
    :pswitch_6d
    new-instance v2, Landroid/drm/DrmInfoEvent;

    #@6f
    .end local v2           #info:Landroid/drm/DrmInfoEvent;
    invoke-direct {v2, v5, v3, v4}, Landroid/drm/DrmInfoEvent;-><init>(IILjava/lang/String;)V

    #@72
    .line 219
    .restart local v2       #info:Landroid/drm/DrmInfoEvent;
    goto :goto_34

    #@73
    .line 197
    nop

    #@74
    :pswitch_data_74
    .packed-switch 0x1
        :pswitch_22
    .end packed-switch

    #@7a
    .line 203
    :pswitch_data_7a
    .packed-switch 0x1
        :pswitch_6d
        :pswitch_5f
        :pswitch_6d
        :pswitch_6d
        :pswitch_6d
        :pswitch_6d
    .end packed-switch
.end method
