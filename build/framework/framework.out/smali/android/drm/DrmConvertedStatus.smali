.class public Landroid/drm/DrmConvertedStatus;
.super Ljava/lang/Object;
.source "DrmConvertedStatus.java"


# static fields
.field public static final STATUS_ERROR:I = 0x3

.field public static final STATUS_INPUTDATA_ERROR:I = 0x2

.field public static final STATUS_OK:I = 0x1


# instance fields
.field public final convertedData:[B

.field public final offset:I

.field public final statusCode:I


# direct methods
.method public constructor <init>(I[BI)V
    .registers 7
    .parameter "statusCode"
    .parameter "convertedData"
    .parameter "offset"

    #@0
    .prologue
    .line 68
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 69
    invoke-direct {p0, p1}, Landroid/drm/DrmConvertedStatus;->isValidStatusCode(I)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_22

    #@9
    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Unsupported status code: "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 73
    :cond_22
    iput p1, p0, Landroid/drm/DrmConvertedStatus;->statusCode:I

    #@24
    .line 74
    iput-object p2, p0, Landroid/drm/DrmConvertedStatus;->convertedData:[B

    #@26
    .line 75
    iput p3, p0, Landroid/drm/DrmConvertedStatus;->offset:I

    #@28
    .line 76
    return-void
.end method

.method private isValidStatusCode(I)Z
    .registers 4
    .parameter "statusCode"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 79
    if-eq p1, v0, :cond_9

    #@3
    const/4 v1, 0x2

    #@4
    if-eq p1, v1, :cond_9

    #@6
    const/4 v1, 0x3

    #@7
    if-ne p1, v1, :cond_a

    #@9
    :cond_9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method
