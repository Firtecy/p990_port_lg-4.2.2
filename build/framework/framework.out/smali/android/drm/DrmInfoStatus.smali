.class public Landroid/drm/DrmInfoStatus;
.super Ljava/lang/Object;
.source "DrmInfoStatus.java"


# static fields
.field public static final STATUS_ERROR:I = 0x2

.field public static final STATUS_OK:I = 0x1


# instance fields
.field public final data:Landroid/drm/ProcessedData;

.field public final infoType:I

.field public final mimeType:Ljava/lang/String;

.field public final statusCode:I


# direct methods
.method public constructor <init>(IILandroid/drm/ProcessedData;Ljava/lang/String;)V
    .registers 8
    .parameter "statusCode"
    .parameter "infoType"
    .parameter "data"
    .parameter "mimeType"

    #@0
    .prologue
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 75
    invoke-static {p2}, Landroid/drm/DrmInfoRequest;->isValidType(I)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_22

    #@9
    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "infoType: "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 79
    :cond_22
    invoke-direct {p0, p1}, Landroid/drm/DrmInfoStatus;->isValidStatusCode(I)Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_41

    #@28
    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2a
    new-instance v1, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v2, "Unsupported status code: "

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@40
    throw v0

    #@41
    .line 83
    :cond_41
    if-eqz p4, :cond_47

    #@43
    const-string v0, ""

    #@45
    if-ne p4, v0, :cond_50

    #@47
    .line 84
    :cond_47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@49
    const-string/jumbo v1, "mimeType is null or an empty string"

    #@4c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4f
    throw v0

    #@50
    .line 87
    :cond_50
    iput p1, p0, Landroid/drm/DrmInfoStatus;->statusCode:I

    #@52
    .line 88
    iput p2, p0, Landroid/drm/DrmInfoStatus;->infoType:I

    #@54
    .line 89
    iput-object p3, p0, Landroid/drm/DrmInfoStatus;->data:Landroid/drm/ProcessedData;

    #@56
    .line 90
    iput-object p4, p0, Landroid/drm/DrmInfoStatus;->mimeType:Ljava/lang/String;

    #@58
    .line 91
    return-void
.end method

.method private isValidStatusCode(I)Z
    .registers 4
    .parameter "statusCode"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 94
    if-eq p1, v0, :cond_6

    #@3
    const/4 v1, 0x2

    #@4
    if-ne p1, v1, :cond_7

    #@6
    :cond_6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method
