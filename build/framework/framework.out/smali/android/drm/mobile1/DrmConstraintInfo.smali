.class public Landroid/drm/mobile1/DrmConstraintInfo;
.super Ljava/lang/Object;
.source "DrmConstraintInfo.java"


# instance fields
.field private count:I

.field private endDate:J

.field private interval:J

.field private startDate:J


# direct methods
.method constructor <init>()V
    .registers 4

    #@0
    .prologue
    const-wide/16 v1, -0x1

    #@2
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 49
    const/4 v0, -0x1

    #@6
    iput v0, p0, Landroid/drm/mobile1/DrmConstraintInfo;->count:I

    #@8
    .line 50
    iput-wide v1, p0, Landroid/drm/mobile1/DrmConstraintInfo;->startDate:J

    #@a
    .line 51
    iput-wide v1, p0, Landroid/drm/mobile1/DrmConstraintInfo;->endDate:J

    #@c
    .line 52
    iput-wide v1, p0, Landroid/drm/mobile1/DrmConstraintInfo;->interval:J

    #@e
    .line 53
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 61
    iget v0, p0, Landroid/drm/mobile1/DrmConstraintInfo;->count:I

    #@2
    return v0
.end method

.method public getEndDate()Ljava/util/Date;
    .registers 5

    #@0
    .prologue
    .line 82
    iget-wide v0, p0, Landroid/drm/mobile1/DrmConstraintInfo;->endDate:J

    #@2
    const-wide/16 v2, -0x1

    #@4
    cmp-long v0, v0, v2

    #@6
    if-nez v0, :cond_a

    #@8
    .line 83
    const/4 v0, 0x0

    #@9
    .line 85
    :goto_9
    return-object v0

    #@a
    :cond_a
    new-instance v0, Ljava/util/Date;

    #@c
    iget-wide v1, p0, Landroid/drm/mobile1/DrmConstraintInfo;->endDate:J

    #@e
    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    #@11
    goto :goto_9
.end method

.method public getInterval()J
    .registers 3

    #@0
    .prologue
    .line 94
    iget-wide v0, p0, Landroid/drm/mobile1/DrmConstraintInfo;->interval:J

    #@2
    return-wide v0
.end method

.method public getStartDate()Ljava/util/Date;
    .registers 5

    #@0
    .prologue
    .line 70
    iget-wide v0, p0, Landroid/drm/mobile1/DrmConstraintInfo;->startDate:J

    #@2
    const-wide/16 v2, -0x1

    #@4
    cmp-long v0, v0, v2

    #@6
    if-nez v0, :cond_a

    #@8
    .line 71
    const/4 v0, 0x0

    #@9
    .line 73
    :goto_9
    return-object v0

    #@a
    :cond_a
    new-instance v0, Ljava/util/Date;

    #@c
    iget-wide v1, p0, Landroid/drm/mobile1/DrmConstraintInfo;->startDate:J

    #@e
    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    #@11
    goto :goto_9
.end method
