.class public Landroid/drm/DrmManagerClient;
.super Ljava/lang/Object;
.source "DrmManagerClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/drm/DrmManagerClient$InfoHandler;,
        Landroid/drm/DrmManagerClient$EventHandler;,
        Landroid/drm/DrmManagerClient$OnErrorListener;,
        Landroid/drm/DrmManagerClient$OnEventListener;,
        Landroid/drm/DrmManagerClient$OnInfoListener;
    }
.end annotation


# static fields
.field private static final ACTION_PROCESS_DRM_INFO:I = 0x3ea

.field private static final ACTION_REMOVE_ALL_RIGHTS:I = 0x3e9

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_UNKNOWN:I = -0x7d0

.field private static final TAG:Ljava/lang/String; = "DrmManagerClient"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

.field mEventThread:Landroid/os/HandlerThread;

.field private mInfoHandler:Landroid/drm/DrmManagerClient$InfoHandler;

.field mInfoThread:Landroid/os/HandlerThread;

.field private mNativeContext:I

.field private mOnErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

.field private mOnEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

.field private mOnInfoListener:Landroid/drm/DrmManagerClient$OnInfoListener;

.field private mReleased:Z

.field private mUniqueId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 61
    const-string v0, "drmframework_jni"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 245
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 246
    iput-object p1, p0, Landroid/drm/DrmManagerClient;->mContext:Landroid/content/Context;

    #@5
    .line 247
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/drm/DrmManagerClient;->mReleased:Z

    #@8
    .line 248
    invoke-direct {p0}, Landroid/drm/DrmManagerClient;->createEventThreads()V

    #@b
    .line 251
    invoke-direct {p0}, Landroid/drm/DrmManagerClient;->_initialize()I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@11
    .line 252
    return-void
.end method

.method private native _acquireDrmInfo(ILandroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;
.end method

.method private native _canHandle(ILjava/lang/String;Ljava/lang/String;)Z
.end method

.method private native _checkRightsStatus(ILjava/lang/String;I)I
.end method

.method private native _closeConvertSession(II)Landroid/drm/DrmConvertedStatus;
.end method

.method private native _convertData(II[B)Landroid/drm/DrmConvertedStatus;
.end method

.method private native _getAllSupportInfo(I)[Landroid/drm/DrmSupportInfo;
.end method

.method private native _getConstraints(ILjava/lang/String;I)Landroid/content/ContentValues;
.end method

.method private native _getDrmObjectType(ILjava/lang/String;Ljava/lang/String;)I
.end method

.method private native _getMetadata(ILjava/lang/String;)Landroid/content/ContentValues;
.end method

.method private native _getOriginalMimeType(ILjava/lang/String;Ljava/io/FileDescriptor;)Ljava/lang/String;
.end method

.method private native _initialize()I
.end method

.method private native _installDrmEngine(ILjava/lang/String;)V
.end method

.method private native _openConvertSession(ILjava/lang/String;)I
.end method

.method private native _processDrmInfo(ILandroid/drm/DrmInfo;)Landroid/drm/DrmInfoStatus;
.end method

.method private native _release(I)V
.end method

.method private native _removeAllRights(I)I
.end method

.method private native _removeRights(ILjava/lang/String;)I
.end method

.method private native _saveRights(ILandroid/drm/DrmRights;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private native _setListeners(ILjava/lang/Object;)V
.end method

.method static synthetic access$000(Landroid/drm/DrmManagerClient;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/drm/DrmManagerClient;ILandroid/drm/DrmInfo;)Landroid/drm/DrmInfoStatus;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/drm/DrmManagerClient;->_processDrmInfo(ILandroid/drm/DrmInfo;)Landroid/drm/DrmInfoStatus;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(Landroid/drm/DrmManagerClient;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->getEventType(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Landroid/drm/DrmManagerClient;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->getErrorType(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Landroid/drm/DrmManagerClient;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->_removeAllRights(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnEventListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mOnEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnErrorListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mOnErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnInfoListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mOnInfoListener:Landroid/drm/DrmManagerClient$OnInfoListener;

    #@2
    return-object v0
.end method

.method private convertUriToPath(Landroid/net/Uri;)Ljava/lang/String;
    .registers 13
    .parameter "uri"

    #@0
    .prologue
    .line 814
    const/4 v8, 0x0

    #@1
    .line 815
    .local v8, path:Ljava/lang/String;
    if-eqz p1, :cond_1d

    #@3
    .line 816
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@6
    move-result-object v10

    #@7
    .line 817
    .local v10, scheme:Ljava/lang/String;
    if-eqz v10, :cond_19

    #@9
    const-string v0, ""

    #@b
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_19

    #@11
    const-string v0, "file"

    #@13
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_1e

    #@19
    .line 819
    :cond_19
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@1c
    move-result-object v8

    #@1d
    .line 848
    .end local v10           #scheme:Ljava/lang/String;
    :cond_1d
    :goto_1d
    return-object v8

    #@1e
    .line 821
    .restart local v10       #scheme:Ljava/lang/String;
    :cond_1e
    const-string v0, "http"

    #@20
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_2b

    #@26
    .line 822
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    goto :goto_1d

    #@2b
    .line 824
    :cond_2b
    const-string v0, "content"

    #@2d
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_80

    #@33
    .line 825
    const/4 v0, 0x1

    #@34
    new-array v2, v0, [Ljava/lang/String;

    #@36
    const/4 v0, 0x0

    #@37
    const-string v1, "_data"

    #@39
    aput-object v1, v2, v0

    #@3b
    .line 826
    .local v2, projection:[Ljava/lang/String;
    const/4 v6, 0x0

    #@3c
    .line 828
    .local v6, cursor:Landroid/database/Cursor;
    :try_start_3c
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mContext:Landroid/content/Context;

    #@3e
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@41
    move-result-object v0

    #@42
    const/4 v3, 0x0

    #@43
    const/4 v4, 0x0

    #@44
    const/4 v5, 0x0

    #@45
    move-object v1, p1

    #@46
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@49
    move-result-object v6

    #@4a
    .line 830
    if-eqz v6, :cond_58

    #@4c
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@4f
    move-result v0

    #@50
    if-eqz v0, :cond_58

    #@52
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@55
    move-result v0

    #@56
    if-nez v0, :cond_70

    #@58
    .line 831
    :cond_58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5a
    const-string v1, "Given Uri could not be found in media store"

    #@5c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v0
    :try_end_60
    .catchall {:try_start_3c .. :try_end_60} :catchall_69
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3c .. :try_end_60} :catch_60

    #@60
    .line 836
    :catch_60
    move-exception v7

    #@61
    .line 837
    .local v7, e:Landroid/database/sqlite/SQLiteException;
    :try_start_61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@63
    const-string v1, "Given Uri is not formatted in a way so that it can be found in media store."

    #@65
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@68
    throw v0
    :try_end_69
    .catchall {:try_start_61 .. :try_end_69} :catchall_69

    #@69
    .line 840
    .end local v7           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_69
    move-exception v0

    #@6a
    if-eqz v6, :cond_6f

    #@6c
    .line 841
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@6f
    .line 840
    :cond_6f
    throw v0

    #@70
    .line 834
    :cond_70
    :try_start_70
    const-string v0, "_data"

    #@72
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@75
    move-result v9

    #@76
    .line 835
    .local v9, pathIndex:I
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_79
    .catchall {:try_start_70 .. :try_end_79} :catchall_69
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_70 .. :try_end_79} :catch_60

    #@79
    move-result-object v8

    #@7a
    .line 840
    if-eqz v6, :cond_1d

    #@7c
    .line 841
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7f
    goto :goto_1d

    #@80
    .line 845
    .end local v2           #projection:[Ljava/lang/String;
    .end local v6           #cursor:Landroid/database/Cursor;
    .end local v9           #pathIndex:I
    :cond_80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@82
    const-string v1, "Given Uri scheme is not supported"

    #@84
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@87
    throw v0
.end method

.method private createEventThreads()V
    .registers 3

    #@0
    .prologue
    .line 893
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

    #@2
    if-nez v0, :cond_3e

    #@4
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mInfoHandler:Landroid/drm/DrmManagerClient$InfoHandler;

    #@6
    if-nez v0, :cond_3e

    #@8
    .line 894
    new-instance v0, Landroid/os/HandlerThread;

    #@a
    const-string v1, "DrmManagerClient.InfoHandler"

    #@c
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@f
    iput-object v0, p0, Landroid/drm/DrmManagerClient;->mInfoThread:Landroid/os/HandlerThread;

    #@11
    .line 895
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mInfoThread:Landroid/os/HandlerThread;

    #@13
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@16
    .line 896
    new-instance v0, Landroid/drm/DrmManagerClient$InfoHandler;

    #@18
    iget-object v1, p0, Landroid/drm/DrmManagerClient;->mInfoThread:Landroid/os/HandlerThread;

    #@1a
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, p0, v1}, Landroid/drm/DrmManagerClient$InfoHandler;-><init>(Landroid/drm/DrmManagerClient;Landroid/os/Looper;)V

    #@21
    iput-object v0, p0, Landroid/drm/DrmManagerClient;->mInfoHandler:Landroid/drm/DrmManagerClient$InfoHandler;

    #@23
    .line 898
    new-instance v0, Landroid/os/HandlerThread;

    #@25
    const-string v1, "DrmManagerClient.EventHandler"

    #@27
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@2a
    iput-object v0, p0, Landroid/drm/DrmManagerClient;->mEventThread:Landroid/os/HandlerThread;

    #@2c
    .line 899
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mEventThread:Landroid/os/HandlerThread;

    #@2e
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@31
    .line 900
    new-instance v0, Landroid/drm/DrmManagerClient$EventHandler;

    #@33
    iget-object v1, p0, Landroid/drm/DrmManagerClient;->mEventThread:Landroid/os/HandlerThread;

    #@35
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@38
    move-result-object v1

    #@39
    invoke-direct {v0, p0, v1}, Landroid/drm/DrmManagerClient$EventHandler;-><init>(Landroid/drm/DrmManagerClient;Landroid/os/Looper;)V

    #@3c
    iput-object v0, p0, Landroid/drm/DrmManagerClient;->mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

    #@3e
    .line 902
    :cond_3e
    return-void
.end method

.method private createListeners()V
    .registers 3

    #@0
    .prologue
    .line 905
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@2
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@4
    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@7
    invoke-direct {p0, v0, v1}, Landroid/drm/DrmManagerClient;->_setListeners(ILjava/lang/Object;)V

    #@a
    .line 906
    return-void
.end method

.method private getErrorType(I)I
    .registers 3
    .parameter "infoType"

    #@0
    .prologue
    .line 792
    const/4 v0, -0x1

    #@1
    .line 794
    .local v0, error:I
    packed-switch p1, :pswitch_data_8

    #@4
    .line 801
    :goto_4
    return v0

    #@5
    .line 798
    :pswitch_5
    const/16 v0, 0x7d6

    #@7
    goto :goto_4

    #@8
    .line 794
    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private getEventType(I)I
    .registers 3
    .parameter "infoType"

    #@0
    .prologue
    .line 779
    const/4 v0, -0x1

    #@1
    .line 781
    .local v0, eventType:I
    packed-switch p1, :pswitch_data_8

    #@4
    .line 788
    :goto_4
    return v0

    #@5
    .line 785
    :pswitch_5
    const/16 v0, 0x3ea

    #@7
    goto :goto_4

    #@8
    .line 781
    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static notify(Ljava/lang/Object;IILjava/lang/String;)V
    .registers 8
    .parameter "thisReference"
    .parameter "uniqueId"
    .parameter "infoType"
    .parameter "message"

    #@0
    .prologue
    .line 177
    check-cast p0, Ljava/lang/ref/WeakReference;

    #@2
    .end local p0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/drm/DrmManagerClient;

    #@8
    .line 179
    .local v0, instance:Landroid/drm/DrmManagerClient;
    if-eqz v0, :cond_1a

    #@a
    iget-object v2, v0, Landroid/drm/DrmManagerClient;->mInfoHandler:Landroid/drm/DrmManagerClient$InfoHandler;

    #@c
    if-eqz v2, :cond_1a

    #@e
    .line 180
    iget-object v2, v0, Landroid/drm/DrmManagerClient;->mInfoHandler:Landroid/drm/DrmManagerClient$InfoHandler;

    #@10
    const/4 v3, 0x1

    #@11
    invoke-virtual {v2, v3, p1, p2, p3}, Landroid/drm/DrmManagerClient$InfoHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v1

    #@15
    .line 182
    .local v1, m:Landroid/os/Message;
    iget-object v2, v0, Landroid/drm/DrmManagerClient;->mInfoHandler:Landroid/drm/DrmManagerClient$InfoHandler;

    #@17
    invoke-virtual {v2, v1}, Landroid/drm/DrmManagerClient$InfoHandler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    .line 184
    .end local v1           #m:Landroid/os/Message;
    :cond_1a
    return-void
.end method


# virtual methods
.method public acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;
    .registers 4
    .parameter "drmInfoRequest"

    #@0
    .prologue
    .line 508
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Landroid/drm/DrmInfoRequest;->isValid()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    .line 509
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v1, "Given drmInfoRequest is invalid/null"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 511
    :cond_10
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@12
    invoke-direct {p0, v0, p1}, Landroid/drm/DrmManagerClient;->_acquireDrmInfo(ILandroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    #@15
    move-result-object v0

    #@16
    return-object v0
.end method

.method public acquireRights(Landroid/drm/DrmInfoRequest;)I
    .registers 4
    .parameter "drmInfoRequest"

    #@0
    .prologue
    .line 529
    invoke-virtual {p0, p1}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    #@3
    move-result-object v0

    #@4
    .line 530
    .local v0, drmInfo:Landroid/drm/DrmInfo;
    if-nez v0, :cond_9

    #@6
    .line 531
    const/16 v1, -0x7d0

    #@8
    .line 533
    :goto_8
    return v1

    #@9
    :cond_9
    invoke-virtual {p0, v0}, Landroid/drm/DrmManagerClient;->processDrmInfo(Landroid/drm/DrmInfo;)I

    #@c
    move-result v1

    #@d
    goto :goto_8
.end method

.method public canHandle(Landroid/net/Uri;Ljava/lang/String;)Z
    .registers 5
    .parameter "uri"
    .parameter "mimeType"

    #@0
    .prologue
    .line 475
    if-eqz p1, :cond_6

    #@2
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    #@4
    if-ne v0, p1, :cond_18

    #@6
    :cond_6
    if-eqz p2, :cond_10

    #@8
    const-string v0, ""

    #@a
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_18

    #@10
    .line 476
    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "Uri or the mimetype should be non null"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 478
    :cond_18
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->convertUriToPath(Landroid/net/Uri;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {p0, v0, p2}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    #@1f
    move-result v0

    #@20
    return v0
.end method

.method public canHandle(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter "path"
    .parameter "mimeType"

    #@0
    .prologue
    .line 460
    if-eqz p1, :cond_a

    #@2
    const-string v0, ""

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1c

    #@a
    :cond_a
    if-eqz p2, :cond_14

    #@c
    const-string v0, ""

    #@e
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1c

    #@14
    .line 461
    :cond_14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v1, "Path or the mimetype should be non null"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 463
    :cond_1c
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@1e
    invoke-direct {p0, v0, p1, p2}, Landroid/drm/DrmManagerClient;->_canHandle(ILjava/lang/String;Ljava/lang/String;)Z

    #@21
    move-result v0

    #@22
    return v0
.end method

.method public checkRightsStatus(Landroid/net/Uri;)I
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 645
    if-eqz p1, :cond_6

    #@2
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    #@4
    if-ne v0, p1, :cond_e

    #@6
    .line 646
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Given uri is not valid"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 648
    :cond_e
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->convertUriToPath(Landroid/net/Uri;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {p0, v0}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    #@15
    move-result v0

    #@16
    return v0
.end method

.method public checkRightsStatus(Landroid/net/Uri;I)I
    .registers 5
    .parameter "uri"
    .parameter "action"

    #@0
    .prologue
    .line 677
    if-eqz p1, :cond_6

    #@2
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    #@4
    if-ne v0, p1, :cond_e

    #@6
    .line 678
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Given uri is not valid"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 680
    :cond_e
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->convertUriToPath(Landroid/net/Uri;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {p0, v0, p2}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;I)I

    #@15
    move-result v0

    #@16
    return v0
.end method

.method public checkRightsStatus(Ljava/lang/String;)I
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 634
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;I)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public checkRightsStatus(Ljava/lang/String;I)I
    .registers 5
    .parameter "path"
    .parameter "action"

    #@0
    .prologue
    .line 661
    if-eqz p1, :cond_10

    #@2
    const-string v0, ""

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_10

    #@a
    invoke-static {p2}, Landroid/drm/DrmStore$Action;->isValid(I)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_18

    #@10
    .line 662
    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "Given path or action is not valid"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 664
    :cond_18
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@1a
    invoke-direct {p0, v0, p1, p2}, Landroid/drm/DrmManagerClient;->_checkRightsStatus(ILjava/lang/String;I)I

    #@1d
    move-result v0

    #@1e
    return v0
.end method

.method public closeConvertSession(I)Landroid/drm/DrmConvertedStatus;
    .registers 3
    .parameter "convertId"

    #@0
    .prologue
    .line 775
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@2
    invoke-direct {p0, v0, p1}, Landroid/drm/DrmManagerClient;->_closeConvertSession(II)Landroid/drm/DrmConvertedStatus;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public convertData(I[B)Landroid/drm/DrmConvertedStatus;
    .registers 5
    .parameter "convertId"
    .parameter "inputData"

    #@0
    .prologue
    .line 757
    if-eqz p2, :cond_5

    #@2
    array-length v0, p2

    #@3
    if-gtz v0, :cond_d

    #@5
    .line 758
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "Given inputData should be non null"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 760
    :cond_d
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@f
    invoke-direct {p0, v0, p1, p2}, Landroid/drm/DrmManagerClient;->_convertData(II[B)Landroid/drm/DrmConvertedStatus;

    #@12
    move-result-object v0

    #@13
    return-object v0
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 255
    iget-boolean v0, p0, Landroid/drm/DrmManagerClient;->mReleased:Z

    #@2
    if-nez v0, :cond_e

    #@4
    .line 256
    const-string v0, "DrmManagerClient"

    #@6
    const-string v1, "You should have called release()"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 257
    invoke-virtual {p0}, Landroid/drm/DrmManagerClient;->release()V

    #@e
    .line 259
    :cond_e
    return-void
.end method

.method public getAvailableDrmEngines()[Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 336
    iget v4, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@2
    invoke-direct {p0, v4}, Landroid/drm/DrmManagerClient;->_getAllSupportInfo(I)[Landroid/drm/DrmSupportInfo;

    #@5
    move-result-object v3

    #@6
    .line 337
    .local v3, supportInfos:[Landroid/drm/DrmSupportInfo;
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    .line 339
    .local v0, descriptions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    #@c
    .local v2, i:I
    :goto_c
    array-length v4, v3

    #@d
    if-ge v2, v4, :cond_1b

    #@f
    .line 340
    aget-object v4, v3, v2

    #@11
    invoke-virtual {v4}, Landroid/drm/DrmSupportInfo;->getDescriprition()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@18
    .line 339
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_c

    #@1b
    .line 343
    :cond_1b
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v4

    #@1f
    new-array v1, v4, [Ljava/lang/String;

    #@21
    .line 344
    .local v1, drmEngines:[Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@24
    move-result-object v4

    #@25
    check-cast v4, [Ljava/lang/String;

    #@27
    return-object v4
.end method

.method public getConstraints(Landroid/net/Uri;I)Landroid/content/ContentValues;
    .registers 5
    .parameter "uri"
    .parameter "action"

    #@0
    .prologue
    .line 389
    if-eqz p1, :cond_6

    #@2
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    #@4
    if-ne v0, p1, :cond_e

    #@6
    .line 390
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Uri should be non null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 392
    :cond_e
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->convertUriToPath(Landroid/net/Uri;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {p0, v0, p2}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    #@15
    move-result-object v0

    #@16
    return-object v0
.end method

.method public getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;
    .registers 5
    .parameter "path"
    .parameter "action"

    #@0
    .prologue
    .line 358
    if-eqz p1, :cond_10

    #@2
    const-string v0, ""

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_10

    #@a
    invoke-static {p2}, Landroid/drm/DrmStore$Action;->isValid(I)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_18

    #@10
    .line 359
    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "Given usage or path is invalid/null"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 361
    :cond_18
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@1a
    invoke-direct {p0, v0, p1, p2}, Landroid/drm/DrmManagerClient;->_getConstraints(ILjava/lang/String;I)Landroid/content/ContentValues;

    #@1d
    move-result-object v0

    #@1e
    return-object v0
.end method

.method public getDrmObjectType(Landroid/net/Uri;Ljava/lang/String;)I
    .registers 7
    .parameter "uri"
    .parameter "mimeType"

    #@0
    .prologue
    .line 564
    if-eqz p1, :cond_6

    #@2
    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    #@4
    if-ne v2, p1, :cond_18

    #@6
    :cond_6
    if-eqz p2, :cond_10

    #@8
    const-string v2, ""

    #@a
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_18

    #@10
    .line 565
    :cond_10
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v3, "Uri or the mimetype should be non null"

    #@14
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2

    #@18
    .line 567
    :cond_18
    const-string v1, ""

    #@1a
    .line 569
    .local v1, path:Ljava/lang/String;
    :try_start_1a
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->convertUriToPath(Landroid/net/Uri;)Ljava/lang/String;
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1d} :catch_23

    #@1d
    move-result-object v1

    #@1e
    .line 574
    :goto_1e
    invoke-virtual {p0, v1, p2}, Landroid/drm/DrmManagerClient;->getDrmObjectType(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    move-result v2

    #@22
    return v2

    #@23
    .line 570
    :catch_23
    move-exception v0

    #@24
    .line 572
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "DrmManagerClient"

    #@26
    const-string v3, "Given Uri could not be found in media store"

    #@28
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_1e
.end method

.method public getDrmObjectType(Ljava/lang/String;Ljava/lang/String;)I
    .registers 5
    .parameter "path"
    .parameter "mimeType"

    #@0
    .prologue
    .line 547
    if-eqz p1, :cond_a

    #@2
    const-string v0, ""

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1c

    #@a
    :cond_a
    if-eqz p2, :cond_14

    #@c
    const-string v0, ""

    #@e
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1c

    #@14
    .line 548
    :cond_14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v1, "Path or the mimetype should be non null"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 550
    :cond_1c
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@1e
    invoke-direct {p0, v0, p1, p2}, Landroid/drm/DrmManagerClient;->_getDrmObjectType(ILjava/lang/String;Ljava/lang/String;)I

    #@21
    move-result v0

    #@22
    return v0
.end method

.method public getMetadata(Landroid/net/Uri;)Landroid/content/ContentValues;
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 404
    if-eqz p1, :cond_6

    #@2
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    #@4
    if-ne v0, p1, :cond_e

    #@6
    .line 405
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Uri should be non null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 407
    :cond_e
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->convertUriToPath(Landroid/net/Uri;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {p0, v0}, Landroid/drm/DrmManagerClient;->getMetadata(Ljava/lang/String;)Landroid/content/ContentValues;

    #@15
    move-result-object v0

    #@16
    return-object v0
.end method

.method public getMetadata(Ljava/lang/String;)Landroid/content/ContentValues;
    .registers 4
    .parameter "path"

    #@0
    .prologue
    .line 373
    if-eqz p1, :cond_a

    #@2
    const-string v0, ""

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_12

    #@a
    .line 374
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    const-string v1, "Given path is invalid/null"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 376
    :cond_12
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@14
    invoke-direct {p0, v0, p1}, Landroid/drm/DrmManagerClient;->_getMetadata(ILjava/lang/String;)Landroid/content/ContentValues;

    #@17
    move-result-object v0

    #@18
    return-object v0
.end method

.method public getOriginalMimeType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 620
    if-eqz p1, :cond_6

    #@2
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    #@4
    if-ne v0, p1, :cond_e

    #@6
    .line 621
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Given uri is not valid"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 623
    :cond_e
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->convertUriToPath(Landroid/net/Uri;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {p0, v0}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    return-object v0
.end method

.method public getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "path"

    #@0
    .prologue
    .line 585
    if-eqz p1, :cond_a

    #@2
    const-string v5, ""

    #@4
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_12

    #@a
    .line 586
    :cond_a
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@c
    const-string v6, "Given path should be non null"

    #@e
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v5

    #@12
    .line 589
    :cond_12
    const/4 v4, 0x0

    #@13
    .line 591
    .local v4, mime:Ljava/lang/String;
    const/4 v2, 0x0

    #@14
    .line 593
    .local v2, is:Ljava/io/FileInputStream;
    const/4 v0, 0x0

    #@15
    .line 594
    .local v0, fd:Ljava/io/FileDescriptor;
    :try_start_15
    new-instance v1, Ljava/io/File;

    #@17
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1a
    .line 595
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@1d
    move-result v5

    #@1e
    if-eqz v5, :cond_2a

    #@20
    .line 596
    new-instance v3, Ljava/io/FileInputStream;

    #@22
    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_25
    .catchall {:try_start_15 .. :try_end_25} :catchall_36
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_25} :catch_3d

    #@25
    .line 597
    .end local v2           #is:Ljava/io/FileInputStream;
    .local v3, is:Ljava/io/FileInputStream;
    :try_start_25
    invoke-virtual {v3}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;
    :try_end_28
    .catchall {:try_start_25 .. :try_end_28} :catchall_48
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_28} :catch_4b

    #@28
    move-result-object v0

    #@29
    move-object v2, v3

    #@2a
    .line 599
    .end local v3           #is:Ljava/io/FileInputStream;
    .restart local v2       #is:Ljava/io/FileInputStream;
    :cond_2a
    :try_start_2a
    iget v5, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@2c
    invoke-direct {p0, v5, p1, v0}, Landroid/drm/DrmManagerClient;->_getOriginalMimeType(ILjava/lang/String;Ljava/io/FileDescriptor;)Ljava/lang/String;
    :try_end_2f
    .catchall {:try_start_2a .. :try_end_2f} :catchall_36
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_2f} :catch_3d

    #@2f
    move-result-object v4

    #@30
    .line 602
    if-eqz v2, :cond_35

    #@32
    .line 604
    :try_start_32
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_35
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_35} :catch_44

    #@35
    .line 609
    .end local v1           #file:Ljava/io/File;
    :cond_35
    :goto_35
    return-object v4

    #@36
    .line 602
    :catchall_36
    move-exception v5

    #@37
    :goto_37
    if-eqz v2, :cond_3c

    #@39
    .line 604
    :try_start_39
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_39 .. :try_end_3c} :catch_46

    #@3c
    .line 602
    :cond_3c
    :goto_3c
    throw v5

    #@3d
    .line 600
    :catch_3d
    move-exception v5

    #@3e
    .line 602
    :goto_3e
    if-eqz v2, :cond_35

    #@40
    .line 604
    :try_start_40
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_43
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_43} :catch_44

    #@43
    goto :goto_35

    #@44
    .line 605
    :catch_44
    move-exception v5

    #@45
    goto :goto_35

    #@46
    :catch_46
    move-exception v6

    #@47
    goto :goto_3c

    #@48
    .line 602
    .end local v2           #is:Ljava/io/FileInputStream;
    .restart local v1       #file:Ljava/io/File;
    .restart local v3       #is:Ljava/io/FileInputStream;
    :catchall_48
    move-exception v5

    #@49
    move-object v2, v3

    #@4a
    .end local v3           #is:Ljava/io/FileInputStream;
    .restart local v2       #is:Ljava/io/FileInputStream;
    goto :goto_37

    #@4b
    .line 600
    .end local v2           #is:Ljava/io/FileInputStream;
    .restart local v3       #is:Ljava/io/FileInputStream;
    :catch_4b
    move-exception v5

    #@4c
    move-object v2, v3

    #@4d
    .end local v3           #is:Ljava/io/FileInputStream;
    .restart local v2       #is:Ljava/io/FileInputStream;
    goto :goto_3e
.end method

.method public installDrmEngine(Ljava/lang/String;)V
    .registers 5
    .parameter "engineFilePath"

    #@0
    .prologue
    .line 444
    if-eqz p1, :cond_a

    #@2
    const-string v0, ""

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_29

    #@a
    .line 445
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Given engineFilePath: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "is not valid"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 448
    :cond_29
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@2b
    invoke-direct {p0, v0, p1}, Landroid/drm/DrmManagerClient;->_installDrmEngine(ILjava/lang/String;)V

    #@2e
    .line 449
    return-void
.end method

.method public openConvertSession(Ljava/lang/String;)I
    .registers 4
    .parameter "mimeType"

    #@0
    .prologue
    .line 737
    if-eqz p1, :cond_a

    #@2
    const-string v0, ""

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_12

    #@a
    .line 738
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    const-string v1, "Path or the mimeType should be non null"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 740
    :cond_12
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@14
    invoke-direct {p0, v0, p1}, Landroid/drm/DrmManagerClient;->_openConvertSession(ILjava/lang/String;)I

    #@17
    move-result v0

    #@18
    return v0
.end method

.method public processDrmInfo(Landroid/drm/DrmInfo;)I
    .registers 6
    .parameter "drmInfo"

    #@0
    .prologue
    .line 488
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Landroid/drm/DrmInfo;->isValid()Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_10

    #@8
    .line 489
    :cond_8
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v3, "Given drmInfo is invalid/null"

    #@c
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 491
    :cond_10
    const/16 v1, -0x7d0

    #@12
    .line 492
    .local v1, result:I
    iget-object v2, p0, Landroid/drm/DrmManagerClient;->mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

    #@14
    if-eqz v2, :cond_27

    #@16
    .line 493
    iget-object v2, p0, Landroid/drm/DrmManagerClient;->mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

    #@18
    const/16 v3, 0x3ea

    #@1a
    invoke-virtual {v2, v3, p1}, Landroid/drm/DrmManagerClient$EventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1d
    move-result-object v0

    #@1e
    .line 494
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Landroid/drm/DrmManagerClient;->mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

    #@20
    invoke-virtual {v2, v0}, Landroid/drm/DrmManagerClient$EventHandler;->sendMessage(Landroid/os/Message;)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_27

    #@26
    const/4 v1, 0x0

    #@27
    .line 496
    .end local v0           #msg:Landroid/os/Message;
    :cond_27
    return v1
.end method

.method public release()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 269
    iget-boolean v0, p0, Landroid/drm/DrmManagerClient;->mReleased:Z

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 270
    const-string v0, "DrmManagerClient"

    #@7
    const-string v1, "You have already called release()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 288
    :goto_c
    return-void

    #@d
    .line 273
    :cond_d
    const/4 v0, 0x1

    #@e
    iput-boolean v0, p0, Landroid/drm/DrmManagerClient;->mReleased:Z

    #@10
    .line 274
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

    #@12
    if-eqz v0, :cond_1b

    #@14
    .line 275
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mEventThread:Landroid/os/HandlerThread;

    #@16
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    #@19
    .line 276
    iput-object v1, p0, Landroid/drm/DrmManagerClient;->mEventThread:Landroid/os/HandlerThread;

    #@1b
    .line 278
    :cond_1b
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mInfoHandler:Landroid/drm/DrmManagerClient$InfoHandler;

    #@1d
    if-eqz v0, :cond_26

    #@1f
    .line 279
    iget-object v0, p0, Landroid/drm/DrmManagerClient;->mInfoThread:Landroid/os/HandlerThread;

    #@21
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    #@24
    .line 280
    iput-object v1, p0, Landroid/drm/DrmManagerClient;->mInfoThread:Landroid/os/HandlerThread;

    #@26
    .line 282
    :cond_26
    iput-object v1, p0, Landroid/drm/DrmManagerClient;->mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

    #@28
    .line 283
    iput-object v1, p0, Landroid/drm/DrmManagerClient;->mInfoHandler:Landroid/drm/DrmManagerClient$InfoHandler;

    #@2a
    .line 284
    iput-object v1, p0, Landroid/drm/DrmManagerClient;->mOnEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

    #@2c
    .line 285
    iput-object v1, p0, Landroid/drm/DrmManagerClient;->mOnInfoListener:Landroid/drm/DrmManagerClient$OnInfoListener;

    #@2e
    .line 286
    iput-object v1, p0, Landroid/drm/DrmManagerClient;->mOnErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

    #@30
    .line 287
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@32
    invoke-direct {p0, v0}, Landroid/drm/DrmManagerClient;->_release(I)V

    #@35
    goto :goto_c
.end method

.method public removeAllRights()I
    .registers 5

    #@0
    .prologue
    .line 718
    const/16 v1, -0x7d0

    #@2
    .line 719
    .local v1, result:I
    iget-object v2, p0, Landroid/drm/DrmManagerClient;->mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

    #@4
    if-eqz v2, :cond_17

    #@6
    .line 720
    iget-object v2, p0, Landroid/drm/DrmManagerClient;->mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

    #@8
    const/16 v3, 0x3e9

    #@a
    invoke-virtual {v2, v3}, Landroid/drm/DrmManagerClient$EventHandler;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 721
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Landroid/drm/DrmManagerClient;->mEventHandler:Landroid/drm/DrmManagerClient$EventHandler;

    #@10
    invoke-virtual {v2, v0}, Landroid/drm/DrmManagerClient$EventHandler;->sendMessage(Landroid/os/Message;)Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_17

    #@16
    const/4 v1, 0x0

    #@17
    .line 723
    .end local v0           #msg:Landroid/os/Message;
    :cond_17
    return v1
.end method

.method public removeRights(Landroid/net/Uri;)I
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 705
    if-eqz p1, :cond_6

    #@2
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    #@4
    if-ne v0, p1, :cond_e

    #@6
    .line 706
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Given uri is not valid"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 708
    :cond_e
    invoke-direct {p0, p1}, Landroid/drm/DrmManagerClient;->convertUriToPath(Landroid/net/Uri;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {p0, v0}, Landroid/drm/DrmManagerClient;->removeRights(Ljava/lang/String;)I

    #@15
    move-result v0

    #@16
    return v0
.end method

.method public removeRights(Ljava/lang/String;)I
    .registers 4
    .parameter "path"

    #@0
    .prologue
    .line 691
    if-eqz p1, :cond_a

    #@2
    const-string v0, ""

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_12

    #@a
    .line 692
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    const-string v1, "Given path should be non null"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 694
    :cond_12
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@14
    invoke-direct {p0, v0, p1}, Landroid/drm/DrmManagerClient;->_removeRights(ILjava/lang/String;)I

    #@17
    move-result v0

    #@18
    return v0
.end method

.method public saveRights(Landroid/drm/DrmRights;Ljava/lang/String;Ljava/lang/String;)I
    .registers 6
    .parameter "drmRights"
    .parameter "rightsPath"
    .parameter "contentPath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 427
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Landroid/drm/DrmRights;->isValid()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    .line 428
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v1, "Given drmRights or contentPath is not valid"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 430
    :cond_10
    if-eqz p2, :cond_21

    #@12
    const-string v0, ""

    #@14
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_21

    #@1a
    .line 431
    invoke-virtual {p1}, Landroid/drm/DrmRights;->getData()[B

    #@1d
    move-result-object v0

    #@1e
    invoke-static {p2, v0}, Landroid/drm/DrmUtils;->writeToFile(Ljava/lang/String;[B)V

    #@21
    .line 433
    :cond_21
    iget v0, p0, Landroid/drm/DrmManagerClient;->mUniqueId:I

    #@23
    invoke-direct {p0, v0, p1, p2, p3}, Landroid/drm/DrmManagerClient;->_saveRights(ILandroid/drm/DrmRights;Ljava/lang/String;Ljava/lang/String;)I

    #@26
    move-result v0

    #@27
    return v0
.end method

.method public declared-synchronized setOnErrorListener(Landroid/drm/DrmManagerClient$OnErrorListener;)V
    .registers 3
    .parameter "errorListener"

    #@0
    .prologue
    .line 323
    monitor-enter p0

    #@1
    :try_start_1
    iput-object p1, p0, Landroid/drm/DrmManagerClient;->mOnErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

    #@3
    .line 324
    if-eqz p1, :cond_8

    #@5
    .line 325
    invoke-direct {p0}, Landroid/drm/DrmManagerClient;->createListeners()V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    #@8
    .line 327
    :cond_8
    monitor-exit p0

    #@9
    return-void

    #@a
    .line 323
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method public declared-synchronized setOnEventListener(Landroid/drm/DrmManagerClient$OnEventListener;)V
    .registers 3
    .parameter "eventListener"

    #@0
    .prologue
    .line 310
    monitor-enter p0

    #@1
    :try_start_1
    iput-object p1, p0, Landroid/drm/DrmManagerClient;->mOnEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

    #@3
    .line 311
    if-eqz p1, :cond_8

    #@5
    .line 312
    invoke-direct {p0}, Landroid/drm/DrmManagerClient;->createListeners()V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    #@8
    .line 314
    :cond_8
    monitor-exit p0

    #@9
    return-void

    #@a
    .line 310
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method public declared-synchronized setOnInfoListener(Landroid/drm/DrmManagerClient$OnInfoListener;)V
    .registers 3
    .parameter "infoListener"

    #@0
    .prologue
    .line 297
    monitor-enter p0

    #@1
    :try_start_1
    iput-object p1, p0, Landroid/drm/DrmManagerClient;->mOnInfoListener:Landroid/drm/DrmManagerClient$OnInfoListener;

    #@3
    .line 298
    if-eqz p1, :cond_8

    #@5
    .line 299
    invoke-direct {p0}, Landroid/drm/DrmManagerClient;->createListeners()V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    #@8
    .line 301
    :cond_8
    monitor-exit p0

    #@9
    return-void

    #@a
    .line 297
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method
