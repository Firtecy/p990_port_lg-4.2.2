.class Landroid/drm/DrmManagerClient$EventHandler;
.super Landroid/os/Handler;
.source "DrmManagerClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/drm/DrmManagerClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/drm/DrmManagerClient;


# direct methods
.method public constructor <init>(Landroid/drm/DrmManagerClient;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 123
    iput-object p1, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@2
    .line 124
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 125
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 12
    .parameter "msg"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 128
    const/4 v3, 0x0

    #@2
    .line 129
    .local v3, event:Landroid/drm/DrmEvent;
    const/4 v2, 0x0

    #@3
    .line 130
    .local v2, error:Landroid/drm/DrmErrorEvent;
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    .line 132
    .local v0, attributes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget v6, p1, Landroid/os/Message;->what:I

    #@a
    packed-switch v6, :pswitch_data_cc

    #@d
    .line 160
    const-string v6, "DrmManagerClient"

    #@f
    new-instance v7, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v8, "Unknown message type "

    #@16
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    iget v8, p1, Landroid/os/Message;->what:I

    #@1c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v7

    #@24
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 169
    :cond_27
    :goto_27
    return-void

    #@28
    .line 134
    :pswitch_28
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a
    check-cast v1, Landroid/drm/DrmInfo;

    #@2c
    .line 135
    .local v1, drmInfo:Landroid/drm/DrmInfo;
    iget-object v6, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@2e
    iget-object v7, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@30
    invoke-static {v7}, Landroid/drm/DrmManagerClient;->access$000(Landroid/drm/DrmManagerClient;)I

    #@33
    move-result v7

    #@34
    invoke-static {v6, v7, v1}, Landroid/drm/DrmManagerClient;->access$100(Landroid/drm/DrmManagerClient;ILandroid/drm/DrmInfo;)Landroid/drm/DrmInfoStatus;

    #@37
    move-result-object v5

    #@38
    .line 137
    .local v5, status:Landroid/drm/DrmInfoStatus;
    const-string v6, "drm_info_status_object"

    #@3a
    invoke-virtual {v0, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 138
    const-string v6, "drm_info_object"

    #@3f
    invoke-virtual {v0, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@42
    .line 140
    if-eqz v5, :cond_87

    #@44
    const/4 v6, 0x1

    #@45
    iget v7, v5, Landroid/drm/DrmInfoStatus;->statusCode:I

    #@47
    if-ne v6, v7, :cond_87

    #@49
    .line 141
    new-instance v3, Landroid/drm/DrmEvent;

    #@4b
    .end local v3           #event:Landroid/drm/DrmEvent;
    iget-object v6, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@4d
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$000(Landroid/drm/DrmManagerClient;)I

    #@50
    move-result v6

    #@51
    iget-object v7, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@53
    iget v8, v5, Landroid/drm/DrmInfoStatus;->infoType:I

    #@55
    invoke-static {v7, v8}, Landroid/drm/DrmManagerClient;->access$200(Landroid/drm/DrmManagerClient;I)I

    #@58
    move-result v7

    #@59
    invoke-direct {v3, v6, v7, v9, v0}, Landroid/drm/DrmEvent;-><init>(IILjava/lang/String;Ljava/util/HashMap;)V

    #@5c
    .line 163
    .end local v1           #drmInfo:Landroid/drm/DrmInfo;
    .end local v5           #status:Landroid/drm/DrmInfoStatus;
    .restart local v3       #event:Landroid/drm/DrmEvent;
    :goto_5c
    iget-object v6, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@5e
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$500(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnEventListener;

    #@61
    move-result-object v6

    #@62
    if-eqz v6, :cond_71

    #@64
    if-eqz v3, :cond_71

    #@66
    .line 164
    iget-object v6, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@68
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$500(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnEventListener;

    #@6b
    move-result-object v6

    #@6c
    iget-object v7, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@6e
    invoke-interface {v6, v7, v3}, Landroid/drm/DrmManagerClient$OnEventListener;->onEvent(Landroid/drm/DrmManagerClient;Landroid/drm/DrmEvent;)V

    #@71
    .line 166
    :cond_71
    iget-object v6, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@73
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$600(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnErrorListener;

    #@76
    move-result-object v6

    #@77
    if-eqz v6, :cond_27

    #@79
    if-eqz v2, :cond_27

    #@7b
    .line 167
    iget-object v6, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@7d
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$600(Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient$OnErrorListener;

    #@80
    move-result-object v6

    #@81
    iget-object v7, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@83
    invoke-interface {v6, v7, v2}, Landroid/drm/DrmManagerClient$OnErrorListener;->onError(Landroid/drm/DrmManagerClient;Landroid/drm/DrmErrorEvent;)V

    #@86
    goto :goto_27

    #@87
    .line 144
    .restart local v1       #drmInfo:Landroid/drm/DrmInfo;
    .restart local v5       #status:Landroid/drm/DrmInfoStatus;
    :cond_87
    if-eqz v5, :cond_9d

    #@89
    iget v4, v5, Landroid/drm/DrmInfoStatus;->infoType:I

    #@8b
    .line 145
    .local v4, infoType:I
    :goto_8b
    new-instance v2, Landroid/drm/DrmErrorEvent;

    #@8d
    .end local v2           #error:Landroid/drm/DrmErrorEvent;
    iget-object v6, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@8f
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$000(Landroid/drm/DrmManagerClient;)I

    #@92
    move-result v6

    #@93
    iget-object v7, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@95
    invoke-static {v7, v4}, Landroid/drm/DrmManagerClient;->access$300(Landroid/drm/DrmManagerClient;I)I

    #@98
    move-result v7

    #@99
    invoke-direct {v2, v6, v7, v9, v0}, Landroid/drm/DrmErrorEvent;-><init>(IILjava/lang/String;Ljava/util/HashMap;)V

    #@9c
    .line 148
    .restart local v2       #error:Landroid/drm/DrmErrorEvent;
    goto :goto_5c

    #@9d
    .line 144
    .end local v4           #infoType:I
    :cond_9d
    invoke-virtual {v1}, Landroid/drm/DrmInfo;->getInfoType()I

    #@a0
    move-result v4

    #@a1
    goto :goto_8b

    #@a2
    .line 151
    .end local v1           #drmInfo:Landroid/drm/DrmInfo;
    .end local v5           #status:Landroid/drm/DrmInfoStatus;
    :pswitch_a2
    iget-object v6, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@a4
    iget-object v7, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@a6
    invoke-static {v7}, Landroid/drm/DrmManagerClient;->access$000(Landroid/drm/DrmManagerClient;)I

    #@a9
    move-result v7

    #@aa
    invoke-static {v6, v7}, Landroid/drm/DrmManagerClient;->access$400(Landroid/drm/DrmManagerClient;I)I

    #@ad
    move-result v6

    #@ae
    if-nez v6, :cond_be

    #@b0
    .line 152
    new-instance v3, Landroid/drm/DrmEvent;

    #@b2
    .end local v3           #event:Landroid/drm/DrmEvent;
    iget-object v6, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@b4
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$000(Landroid/drm/DrmManagerClient;)I

    #@b7
    move-result v6

    #@b8
    const/16 v7, 0x3e9

    #@ba
    invoke-direct {v3, v6, v7, v9}, Landroid/drm/DrmEvent;-><init>(IILjava/lang/String;)V

    #@bd
    .restart local v3       #event:Landroid/drm/DrmEvent;
    goto :goto_5c

    #@be
    .line 154
    :cond_be
    new-instance v2, Landroid/drm/DrmErrorEvent;

    #@c0
    .end local v2           #error:Landroid/drm/DrmErrorEvent;
    iget-object v6, p0, Landroid/drm/DrmManagerClient$EventHandler;->this$0:Landroid/drm/DrmManagerClient;

    #@c2
    invoke-static {v6}, Landroid/drm/DrmManagerClient;->access$000(Landroid/drm/DrmManagerClient;)I

    #@c5
    move-result v6

    #@c6
    const/16 v7, 0x7d7

    #@c8
    invoke-direct {v2, v6, v7, v9}, Landroid/drm/DrmErrorEvent;-><init>(IILjava/lang/String;)V

    #@cb
    .line 157
    .restart local v2       #error:Landroid/drm/DrmErrorEvent;
    goto :goto_5c

    #@cc
    .line 132
    :pswitch_data_cc
    .packed-switch 0x3e9
        :pswitch_a2
        :pswitch_28
    .end packed-switch
.end method
