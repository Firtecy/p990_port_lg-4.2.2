.class public Landroid/drm/DrmRights;
.super Ljava/lang/Object;
.source "DrmRights.java"


# instance fields
.field private mAccountId:Ljava/lang/String;

.field private mData:[B

.field private mMimeType:Ljava/lang/String;

.field private mSubscriptionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/drm/ProcessedData;Ljava/lang/String;)V
    .registers 6
    .parameter "data"
    .parameter "mimeType"

    #@0
    .prologue
    .line 118
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 119
    if-nez p1, :cond_d

    #@5
    .line 120
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v2, "data is null"

    #@9
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1

    #@d
    .line 123
    :cond_d
    invoke-virtual {p1}, Landroid/drm/ProcessedData;->getData()[B

    #@10
    move-result-object v1

    #@11
    iput-object v1, p0, Landroid/drm/DrmRights;->mData:[B

    #@13
    .line 124
    invoke-virtual {p1}, Landroid/drm/ProcessedData;->getAccountId()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    iput-object v1, p0, Landroid/drm/DrmRights;->mAccountId:Ljava/lang/String;

    #@19
    .line 125
    invoke-virtual {p1}, Landroid/drm/ProcessedData;->getSubscriptionId()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    iput-object v1, p0, Landroid/drm/DrmRights;->mSubscriptionId:Ljava/lang/String;

    #@1f
    .line 126
    iput-object p2, p0, Landroid/drm/DrmRights;->mMimeType:Ljava/lang/String;

    #@21
    .line 128
    invoke-virtual {p0}, Landroid/drm/DrmRights;->isValid()Z

    #@24
    move-result v1

    #@25
    if-nez v1, :cond_55

    #@27
    .line 129
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string/jumbo v2, "mimeType: "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    iget-object v2, p0, Landroid/drm/DrmRights;->mMimeType:Ljava/lang/String;

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    const-string v2, ","

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    const-string v2, "data: "

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    iget-object v2, p0, Landroid/drm/DrmRights;->mData:[B

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v0

    #@4f
    .line 131
    .local v0, msg:Ljava/lang/String;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@51
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@54
    throw v1

    #@55
    .line 133
    .end local v0           #msg:Ljava/lang/String;
    :cond_55
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;)V
    .registers 3
    .parameter "rightsFile"
    .parameter "mimeType"

    #@0
    .prologue
    .line 92
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 93
    invoke-direct {p0, p1, p2}, Landroid/drm/DrmRights;->instantiate(Ljava/io/File;Ljava/lang/String;)V

    #@6
    .line 94
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "rightsFilePath"
    .parameter "mimeType"

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    new-instance v0, Ljava/io/File;

    #@5
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@8
    .line 54
    .local v0, file:Ljava/io/File;
    invoke-direct {p0, v0, p2}, Landroid/drm/DrmRights;->instantiate(Ljava/io/File;Ljava/lang/String;)V

    #@b
    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "rightsFilePath"
    .parameter "mimeType"
    .parameter "accountId"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/drm/DrmRights;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    .line 67
    iput-object p3, p0, Landroid/drm/DrmRights;->mAccountId:Ljava/lang/String;

    #@5
    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "rightsFilePath"
    .parameter "mimeType"
    .parameter "accountId"
    .parameter "subscriptionId"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Landroid/drm/DrmRights;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    .line 82
    iput-object p3, p0, Landroid/drm/DrmRights;->mAccountId:Ljava/lang/String;

    #@5
    .line 83
    iput-object p4, p0, Landroid/drm/DrmRights;->mSubscriptionId:Ljava/lang/String;

    #@7
    .line 84
    return-void
.end method

.method private instantiate(Ljava/io/File;Ljava/lang/String;)V
    .registers 7
    .parameter "rightsFile"
    .parameter "mimeType"

    #@0
    .prologue
    .line 98
    :try_start_0
    invoke-static {p1}, Landroid/drm/DrmUtils;->readBytes(Ljava/io/File;)[B

    #@3
    move-result-object v2

    #@4
    iput-object v2, p0, Landroid/drm/DrmRights;->mData:[B
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_6} :catch_3c

    #@6
    .line 103
    :goto_6
    iput-object p2, p0, Landroid/drm/DrmRights;->mMimeType:Ljava/lang/String;

    #@8
    .line 104
    invoke-virtual {p0}, Landroid/drm/DrmRights;->isValid()Z

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_41

    #@e
    .line 105
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string/jumbo v3, "mimeType: "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    iget-object v3, p0, Landroid/drm/DrmRights;->mMimeType:Ljava/lang/String;

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, ","

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    const-string v3, "data: "

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    iget-object v3, p0, Landroid/drm/DrmRights;->mData:[B

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    .line 107
    .local v1, msg:Ljava/lang/String;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@38
    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v2

    #@3c
    .line 99
    .end local v1           #msg:Ljava/lang/String;
    :catch_3c
    move-exception v0

    #@3d
    .line 100
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@40
    goto :goto_6

    #@41
    .line 109
    .end local v0           #e:Ljava/io/IOException;
    :cond_41
    return-void
.end method


# virtual methods
.method public getAccountId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 159
    iget-object v0, p0, Landroid/drm/DrmRights;->mAccountId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getData()[B
    .registers 2

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Landroid/drm/DrmRights;->mData:[B

    #@2
    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Landroid/drm/DrmRights;->mMimeType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSubscriptionId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Landroid/drm/DrmRights;->mSubscriptionId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method isValid()Z
    .registers 3

    #@0
    .prologue
    .line 177
    iget-object v0, p0, Landroid/drm/DrmRights;->mMimeType:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_19

    #@4
    iget-object v0, p0, Landroid/drm/DrmRights;->mMimeType:Ljava/lang/String;

    #@6
    const-string v1, ""

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_19

    #@e
    iget-object v0, p0, Landroid/drm/DrmRights;->mData:[B

    #@10
    if-eqz v0, :cond_19

    #@12
    iget-object v0, p0, Landroid/drm/DrmRights;->mData:[B

    #@14
    array-length v0, v0

    #@15
    if-lez v0, :cond_19

    #@17
    const/4 v0, 0x1

    #@18
    :goto_18
    return v0

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_18
.end method
