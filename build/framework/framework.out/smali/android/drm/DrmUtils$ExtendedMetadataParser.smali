.class public Landroid/drm/DrmUtils$ExtendedMetadataParser;
.super Ljava/lang/Object;
.source "DrmUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/drm/DrmUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExtendedMetadataParser"
.end annotation


# instance fields
.field mMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>([B)V
    .registers 8
    .parameter "constraintData"

    #@0
    .prologue
    .line 151
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 131
    new-instance v5, Ljava/util/HashMap;

    #@5
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v5, p0, Landroid/drm/DrmUtils$ExtendedMetadataParser;->mMap:Ljava/util/HashMap;

    #@a
    .line 153
    const/4 v0, 0x0

    #@b
    .line 155
    .local v0, index:I
    :goto_b
    array-length v5, p1

    #@c
    if-ge v0, v5, :cond_34

    #@e
    .line 157
    invoke-direct {p0, p1, v0}, Landroid/drm/DrmUtils$ExtendedMetadataParser;->readByte([BI)I

    #@11
    move-result v1

    #@12
    .line 158
    .local v1, keyLength:I
    add-int/lit8 v0, v0, 0x1

    #@14
    .line 161
    invoke-direct {p0, p1, v0}, Landroid/drm/DrmUtils$ExtendedMetadataParser;->readByte([BI)I

    #@17
    move-result v4

    #@18
    .line 162
    .local v4, valueLength:I
    add-int/lit8 v0, v0, 0x1

    #@1a
    .line 165
    invoke-direct {p0, p1, v1, v0}, Landroid/drm/DrmUtils$ExtendedMetadataParser;->readMultipleBytes([BII)Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    .line 166
    .local v2, strKey:Ljava/lang/String;
    add-int/2addr v0, v1

    #@1f
    .line 169
    invoke-direct {p0, p1, v4, v0}, Landroid/drm/DrmUtils$ExtendedMetadataParser;->readMultipleBytes([BII)Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    .line 170
    .local v3, strValue:Ljava/lang/String;
    const-string v5, " "

    #@25
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v5

    #@29
    if-eqz v5, :cond_2d

    #@2b
    .line 171
    const-string v3, ""

    #@2d
    .line 173
    :cond_2d
    add-int/2addr v0, v4

    #@2e
    .line 174
    iget-object v5, p0, Landroid/drm/DrmUtils$ExtendedMetadataParser;->mMap:Ljava/util/HashMap;

    #@30
    invoke-virtual {v5, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    goto :goto_b

    #@34
    .line 176
    .end local v1           #keyLength:I
    .end local v2           #strKey:Ljava/lang/String;
    .end local v3           #strValue:Ljava/lang/String;
    .end local v4           #valueLength:I
    :cond_34
    return-void
.end method

.method synthetic constructor <init>([BLandroid/drm/DrmUtils$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 130
    invoke-direct {p0, p1}, Landroid/drm/DrmUtils$ExtendedMetadataParser;-><init>([B)V

    #@3
    return-void
.end method

.method private readByte([BI)I
    .registers 4
    .parameter "constraintData"
    .parameter "arrayIndex"

    #@0
    .prologue
    .line 135
    aget-byte v0, p1, p2

    #@2
    return v0
.end method

.method private readMultipleBytes([BII)Ljava/lang/String;
    .registers 8
    .parameter "constraintData"
    .parameter "numberOfBytes"
    .parameter "arrayIndex"

    #@0
    .prologue
    .line 140
    new-array v2, p2, [B

    #@2
    .line 141
    .local v2, returnBytes:[B
    move v1, p3

    #@3
    .local v1, j:I
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    add-int v3, p3, p2

    #@6
    if-ge v1, v3, :cond_11

    #@8
    .line 142
    aget-byte v3, p1, v1

    #@a
    aput-byte v3, v2, v0

    #@c
    .line 141
    add-int/lit8 v1, v1, 0x1

    #@e
    add-int/lit8 v0, v0, 0x1

    #@10
    goto :goto_4

    #@11
    .line 144
    :cond_11
    new-instance v3, Ljava/lang/String;

    #@13
    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([B)V

    #@16
    return-object v3
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 207
    iget-object v0, p0, Landroid/drm/DrmUtils$ExtendedMetadataParser;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 185
    iget-object v0, p0, Landroid/drm/DrmUtils$ExtendedMetadataParser;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public keyIterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 195
    iget-object v0, p0, Landroid/drm/DrmUtils$ExtendedMetadataParser;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method
