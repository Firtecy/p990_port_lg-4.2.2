.class public Landroid/drm/DrmSupportInfo;
.super Ljava/lang/Object;
.source "DrmSupportInfo.java"


# instance fields
.field private mDescription:Ljava/lang/String;

.field private final mFileSuffixList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mMimeTypeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 31
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/drm/DrmSupportInfo;->mFileSuffixList:Ljava/util/ArrayList;

    #@a
    .line 32
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Landroid/drm/DrmSupportInfo;->mMimeTypeList:Ljava/util/ArrayList;

    #@11
    .line 33
    const-string v0, ""

    #@13
    iput-object v0, p0, Landroid/drm/DrmSupportInfo;->mDescription:Ljava/lang/String;

    #@15
    return-void
.end method


# virtual methods
.method public addFileSuffix(Ljava/lang/String;)V
    .registers 4
    .parameter "fileSuffix"

    #@0
    .prologue
    .line 60
    const-string v0, ""

    #@2
    if-ne p1, v0, :cond_c

    #@4
    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v1, "fileSuffix is an empty string"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 64
    :cond_c
    iget-object v0, p0, Landroid/drm/DrmSupportInfo;->mFileSuffixList:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11
    .line 65
    return-void
.end method

.method public addMimeType(Ljava/lang/String;)V
    .registers 4
    .parameter "mimeType"

    #@0
    .prologue
    .line 42
    if-nez p1, :cond_b

    #@2
    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "mimeType is null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 45
    :cond_b
    const-string v0, ""

    #@d
    if-ne p1, v0, :cond_18

    #@f
    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string/jumbo v1, "mimeType is an empty string"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 49
    :cond_18
    iget-object v0, p0, Landroid/drm/DrmSupportInfo;->mMimeTypeList:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    .line 50
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "object"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 145
    instance-of v2, p1, Landroid/drm/DrmSupportInfo;

    #@3
    if-eqz v2, :cond_27

    #@5
    move-object v0, p1

    #@6
    .line 146
    check-cast v0, Landroid/drm/DrmSupportInfo;

    #@8
    .line 147
    .local v0, info:Landroid/drm/DrmSupportInfo;
    iget-object v2, p0, Landroid/drm/DrmSupportInfo;->mFileSuffixList:Ljava/util/ArrayList;

    #@a
    iget-object v3, v0, Landroid/drm/DrmSupportInfo;->mFileSuffixList:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_27

    #@12
    iget-object v2, p0, Landroid/drm/DrmSupportInfo;->mMimeTypeList:Ljava/util/ArrayList;

    #@14
    iget-object v3, v0, Landroid/drm/DrmSupportInfo;->mMimeTypeList:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_27

    #@1c
    iget-object v2, p0, Landroid/drm/DrmSupportInfo;->mDescription:Ljava/lang/String;

    #@1e
    iget-object v3, v0, Landroid/drm/DrmSupportInfo;->mDescription:Ljava/lang/String;

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_27

    #@26
    const/4 v1, 0x1

    #@27
    .line 151
    .end local v0           #info:Landroid/drm/DrmSupportInfo;
    :cond_27
    return v1
.end method

.method public getDescriprition()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Landroid/drm/DrmSupportInfo;->mDescription:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/drm/DrmSupportInfo;->mDescription:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getFileSuffixIterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Landroid/drm/DrmSupportInfo;->mFileSuffixList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMimeTypeIterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/drm/DrmSupportInfo;->mMimeTypeList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 133
    iget-object v0, p0, Landroid/drm/DrmSupportInfo;->mFileSuffixList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    #@5
    move-result v0

    #@6
    iget-object v1, p0, Landroid/drm/DrmSupportInfo;->mMimeTypeList:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    #@b
    move-result v1

    #@c
    add-int/2addr v0, v1

    #@d
    iget-object v1, p0, Landroid/drm/DrmSupportInfo;->mDescription:Ljava/lang/String;

    #@f
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@12
    move-result v1

    #@13
    add-int/2addr v0, v1

    #@14
    return v0
.end method

.method isSupportedFileSuffix(Ljava/lang/String;)Z
    .registers 3
    .parameter "fileSuffix"

    #@0
    .prologue
    .line 185
    iget-object v0, p0, Landroid/drm/DrmSupportInfo;->mFileSuffixList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method isSupportedMimeType(Ljava/lang/String;)Z
    .registers 5
    .parameter "mimeType"

    #@0
    .prologue
    .line 162
    if-eqz p1, :cond_26

    #@2
    const-string v2, ""

    #@4
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_26

    #@a
    .line 163
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    iget-object v2, p0, Landroid/drm/DrmSupportInfo;->mMimeTypeList:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v2

    #@11
    if-ge v1, v2, :cond_26

    #@13
    .line 164
    iget-object v2, p0, Landroid/drm/DrmSupportInfo;->mMimeTypeList:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Ljava/lang/String;

    #@1b
    .line 170
    .local v0, completeMimeType:Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_23

    #@21
    .line 171
    const/4 v2, 0x1

    #@22
    .line 175
    .end local v0           #completeMimeType:Ljava/lang/String;
    .end local v1           #i:I
    :goto_22
    return v2

    #@23
    .line 163
    .restart local v0       #completeMimeType:Ljava/lang/String;
    .restart local v1       #i:I
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_b

    #@26
    .line 175
    .end local v0           #completeMimeType:Ljava/lang/String;
    .end local v1           #i:I
    :cond_26
    const/4 v2, 0x0

    #@27
    goto :goto_22
.end method

.method public setDescription(Ljava/lang/String;)V
    .registers 4
    .parameter "description"

    #@0
    .prologue
    .line 94
    if-nez p1, :cond_a

    #@2
    .line 95
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "description is null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 97
    :cond_a
    const-string v0, ""

    #@c
    if-ne p1, v0, :cond_16

    #@e
    .line 98
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@10
    const-string v1, "description is an empty string"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 101
    :cond_16
    iput-object p1, p0, Landroid/drm/DrmSupportInfo;->mDescription:Ljava/lang/String;

    #@18
    .line 102
    return-void
.end method
