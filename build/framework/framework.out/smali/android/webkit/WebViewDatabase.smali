.class public Landroid/webkit/WebViewDatabase;
.super Ljava/lang/Object;
.source "WebViewDatabase.java"


# static fields
.field protected static final LOGTAG:Ljava/lang/String; = "webviewdatabase"


# direct methods
.method protected constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabase;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 44
    invoke-static {}, Landroid/webkit/WebViewFactory;->getProvider()Landroid/webkit/WebViewFactoryProvider;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p0}, Landroid/webkit/WebViewFactoryProvider;->getWebViewDatabase(Landroid/content/Context;)Landroid/webkit/WebViewDatabase;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method


# virtual methods
.method public clearFormData()V
    .registers 2

    #@0
    .prologue
    .line 109
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public clearHttpAuthUsernamePassword()V
    .registers 2

    #@0
    .prologue
    .line 90
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public clearUsernamePassword()V
    .registers 2

    #@0
    .prologue
    .line 67
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public hasFormData()Z
    .registers 2

    #@0
    .prologue
    .line 100
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public hasHttpAuthUsernamePassword()Z
    .registers 2

    #@0
    .prologue
    .line 79
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public hasUsernamePassword()Z
    .registers 2

    #@0
    .prologue
    .line 56
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method
