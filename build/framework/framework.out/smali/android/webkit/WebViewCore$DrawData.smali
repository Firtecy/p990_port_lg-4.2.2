.class Landroid/webkit/WebViewCore$DrawData;
.super Ljava/lang/Object;
.source "WebViewCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewCore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DrawData"
.end annotation


# instance fields
.field mBaseLayer:I

.field mContentSize:Landroid/graphics/Point;

.field mFirstLayoutForNonStandardLoad:Z

.field mMinPrefWidth:I

.field mViewSize:Landroid/graphics/Point;

.field mViewState:Landroid/webkit/WebViewCore$ViewState;


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 2291
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2292
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/webkit/WebViewCore$DrawData;->mBaseLayer:I

    #@6
    .line 2293
    new-instance v0, Landroid/graphics/Point;

    #@8
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@b
    iput-object v0, p0, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@d
    .line 2294
    return-void
.end method
