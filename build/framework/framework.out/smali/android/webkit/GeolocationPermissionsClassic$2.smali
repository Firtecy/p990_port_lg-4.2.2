.class Landroid/webkit/GeolocationPermissionsClassic$2;
.super Landroid/os/Handler;
.source "GeolocationPermissionsClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/GeolocationPermissionsClassic;->createHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/GeolocationPermissionsClassic;


# direct methods
.method constructor <init>(Landroid/webkit/GeolocationPermissionsClassic;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 97
    iput-object p1, p0, Landroid/webkit/GeolocationPermissionsClassic$2;->this$0:Landroid/webkit/GeolocationPermissionsClassic;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 101
    iget v6, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v6, :pswitch_data_76

    #@6
    .line 130
    :goto_6
    return-void

    #@7
    .line 103
    :pswitch_7
    invoke-static {}, Landroid/webkit/GeolocationPermissionsClassic;->access$000()Ljava/util/Set;

    #@a
    move-result-object v3

    #@b
    .line 104
    .local v3, origins:Ljava/util/Set;
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    check-cast v1, Landroid/webkit/ValueCallback;

    #@f
    .line 105
    .local v1, callback:Landroid/webkit/ValueCallback;
    new-instance v5, Ljava/util/HashMap;

    #@11
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    #@14
    .line 106
    .local v5, values:Ljava/util/Map;
    const-string v6, "callback"

    #@16
    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 107
    const-string/jumbo v6, "origins"

    #@1c
    invoke-interface {v5, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    .line 108
    iget-object v6, p0, Landroid/webkit/GeolocationPermissionsClassic$2;->this$0:Landroid/webkit/GeolocationPermissionsClassic;

    #@21
    const/4 v7, 0x0

    #@22
    invoke-static {v8, v7, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@25
    move-result-object v7

    #@26
    invoke-static {v6, v7}, Landroid/webkit/GeolocationPermissionsClassic;->access$100(Landroid/webkit/GeolocationPermissionsClassic;Landroid/os/Message;)V

    #@29
    goto :goto_6

    #@2a
    .line 111
    .end local v1           #callback:Landroid/webkit/ValueCallback;
    .end local v3           #origins:Ljava/util/Set;
    .end local v5           #values:Ljava/util/Map;
    :pswitch_2a
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2c
    check-cast v5, Ljava/util/Map;

    #@2e
    .line 112
    .restart local v5       #values:Ljava/util/Map;
    const-string/jumbo v6, "origin"

    #@31
    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    move-result-object v2

    #@35
    check-cast v2, Ljava/lang/String;

    #@37
    .line 113
    .local v2, origin:Ljava/lang/String;
    const-string v6, "callback"

    #@39
    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3c
    move-result-object v1

    #@3d
    check-cast v1, Landroid/webkit/ValueCallback;

    #@3f
    .line 114
    .restart local v1       #callback:Landroid/webkit/ValueCallback;
    invoke-static {v2}, Landroid/webkit/GeolocationPermissionsClassic;->access$200(Ljava/lang/String;)Z

    #@42
    move-result v0

    #@43
    .line 115
    .local v0, allowed:Z
    new-instance v4, Ljava/util/HashMap;

    #@45
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@48
    .line 116
    .local v4, retValues:Ljava/util/Map;
    const-string v6, "callback"

    #@4a
    invoke-interface {v4, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4d
    .line 117
    const-string v6, "allowed"

    #@4f
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@52
    move-result-object v7

    #@53
    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@56
    .line 118
    iget-object v6, p0, Landroid/webkit/GeolocationPermissionsClassic$2;->this$0:Landroid/webkit/GeolocationPermissionsClassic;

    #@58
    const/4 v7, 0x1

    #@59
    invoke-static {v8, v7, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@5c
    move-result-object v7

    #@5d
    invoke-static {v6, v7}, Landroid/webkit/GeolocationPermissionsClassic;->access$100(Landroid/webkit/GeolocationPermissionsClassic;Landroid/os/Message;)V

    #@60
    goto :goto_6

    #@61
    .line 121
    .end local v0           #allowed:Z
    .end local v1           #callback:Landroid/webkit/ValueCallback;
    .end local v2           #origin:Ljava/lang/String;
    .end local v4           #retValues:Ljava/util/Map;
    .end local v5           #values:Ljava/util/Map;
    :pswitch_61
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@63
    check-cast v6, Ljava/lang/String;

    #@65
    invoke-static {v6}, Landroid/webkit/GeolocationPermissionsClassic;->access$300(Ljava/lang/String;)V

    #@68
    goto :goto_6

    #@69
    .line 124
    :pswitch_69
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6b
    check-cast v6, Ljava/lang/String;

    #@6d
    invoke-static {v6}, Landroid/webkit/GeolocationPermissionsClassic;->access$400(Ljava/lang/String;)V

    #@70
    goto :goto_6

    #@71
    .line 127
    :pswitch_71
    invoke-static {}, Landroid/webkit/GeolocationPermissionsClassic;->access$500()V

    #@74
    goto :goto_6

    #@75
    .line 101
    nop

    #@76
    :pswitch_data_76
    .packed-switch 0x0
        :pswitch_7
        :pswitch_2a
        :pswitch_61
        :pswitch_69
        :pswitch_71
    .end packed-switch
.end method
