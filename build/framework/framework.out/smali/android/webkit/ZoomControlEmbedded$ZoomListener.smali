.class Landroid/webkit/ZoomControlEmbedded$ZoomListener;
.super Ljava/lang/Object;
.source "ZoomControlEmbedded.java"

# interfaces
.implements Landroid/widget/ZoomButtonsController$OnZoomListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/ZoomControlEmbedded;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ZoomListener"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/ZoomControlEmbedded;


# direct methods
.method private constructor <init>(Landroid/webkit/ZoomControlEmbedded;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 99
    iput-object p1, p0, Landroid/webkit/ZoomControlEmbedded$ZoomListener;->this$0:Landroid/webkit/ZoomControlEmbedded;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/ZoomControlEmbedded;Landroid/webkit/ZoomControlEmbedded$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Landroid/webkit/ZoomControlEmbedded$ZoomListener;-><init>(Landroid/webkit/ZoomControlEmbedded;)V

    #@3
    return-void
.end method


# virtual methods
.method public onVisibilityChanged(Z)V
    .registers 4
    .parameter "visible"

    #@0
    .prologue
    .line 102
    if-eqz p1, :cond_1e

    #@2
    .line 103
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded$ZoomListener;->this$0:Landroid/webkit/ZoomControlEmbedded;

    #@4
    invoke-static {v0}, Landroid/webkit/ZoomControlEmbedded;->access$100(Landroid/webkit/ZoomControlEmbedded;)Landroid/webkit/WebViewClassic;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@b
    .line 105
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded$ZoomListener;->this$0:Landroid/webkit/ZoomControlEmbedded;

    #@d
    invoke-static {v0}, Landroid/webkit/ZoomControlEmbedded;->access$200(Landroid/webkit/ZoomControlEmbedded;)Landroid/widget/ZoomButtonsController;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Landroid/widget/ZoomButtonsController;->getZoomControls()Landroid/view/View;

    #@14
    move-result-object v0

    #@15
    const/4 v1, 0x0

    #@16
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@19
    .line 106
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded$ZoomListener;->this$0:Landroid/webkit/ZoomControlEmbedded;

    #@1b
    invoke-virtual {v0}, Landroid/webkit/ZoomControlEmbedded;->update()V

    #@1e
    .line 108
    :cond_1e
    return-void
.end method

.method public onZoom(Z)V
    .registers 3
    .parameter "zoomIn"

    #@0
    .prologue
    .line 111
    if-eqz p1, :cond_11

    #@2
    .line 112
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded$ZoomListener;->this$0:Landroid/webkit/ZoomControlEmbedded;

    #@4
    invoke-static {v0}, Landroid/webkit/ZoomControlEmbedded;->access$100(Landroid/webkit/ZoomControlEmbedded;)Landroid/webkit/WebViewClassic;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->zoomIn()Z

    #@b
    .line 116
    :goto_b
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded$ZoomListener;->this$0:Landroid/webkit/ZoomControlEmbedded;

    #@d
    invoke-virtual {v0}, Landroid/webkit/ZoomControlEmbedded;->update()V

    #@10
    .line 117
    return-void

    #@11
    .line 114
    :cond_11
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded$ZoomListener;->this$0:Landroid/webkit/ZoomControlEmbedded;

    #@13
    invoke-static {v0}, Landroid/webkit/ZoomControlEmbedded;->access$100(Landroid/webkit/ZoomControlEmbedded;)Landroid/webkit/WebViewClassic;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->zoomOut()Z

    #@1a
    goto :goto_b
.end method
