.class public Landroid/webkit/OverScrollGlow;
.super Ljava/lang/Object;
.source "OverScrollGlow.java"


# instance fields
.field private mEdgeGlowBottom:Landroid/widget/EdgeEffect;

.field private mEdgeGlowLeft:Landroid/widget/EdgeEffect;

.field private mEdgeGlowRight:Landroid/widget/EdgeEffect;

.field private mEdgeGlowTop:Landroid/widget/EdgeEffect;

.field private mHostView:Landroid/webkit/WebViewClassic;

.field private mOverScrollDeltaX:I

.field private mOverScrollDeltaY:I


# direct methods
.method public constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 4
    .parameter "host"

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    iput-object p1, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@5
    .line 44
    invoke-virtual {p1}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@8
    move-result-object v0

    #@9
    .line 45
    .local v0, context:Landroid/content/Context;
    new-instance v1, Landroid/widget/EdgeEffect;

    #@b
    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    #@e
    iput-object v1, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@10
    .line 46
    new-instance v1, Landroid/widget/EdgeEffect;

    #@12
    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    #@15
    iput-object v1, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@17
    .line 47
    new-instance v1, Landroid/widget/EdgeEffect;

    #@19
    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    #@1c
    iput-object v1, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@1e
    .line 48
    new-instance v1, Landroid/widget/EdgeEffect;

    #@20
    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    #@23
    iput-object v1, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@25
    .line 49
    return-void
.end method


# virtual methods
.method public absorbGlow(IIIIII)V
    .registers 9
    .parameter "x"
    .parameter "y"
    .parameter "oldX"
    .parameter "oldY"
    .parameter "rangeX"
    .parameter "rangeY"

    #@0
    .prologue
    .line 123
    if-gtz p6, :cond_e

    #@2
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@4
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/webkit/WebView;->getOverScrollMode()I

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_2d

    #@e
    .line 124
    :cond_e
    if-gez p2, :cond_4f

    #@10
    if-ltz p4, :cond_4f

    #@12
    .line 125
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@14
    iget-object v1, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@16
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@18
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@1b
    move-result v1

    #@1c
    float-to-int v1, v1

    #@1d
    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    #@20
    .line 126
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@22
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_2d

    #@28
    .line 127
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@2a
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    #@2d
    .line 137
    :cond_2d
    :goto_2d
    if-lez p5, :cond_4e

    #@2f
    .line 138
    if-gez p1, :cond_6f

    #@31
    if-ltz p3, :cond_6f

    #@33
    .line 139
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@35
    iget-object v1, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@37
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@39
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@3c
    move-result v1

    #@3d
    float-to-int v1, v1

    #@3e
    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    #@41
    .line 140
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@43
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@46
    move-result v0

    #@47
    if-nez v0, :cond_4e

    #@49
    .line 141
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@4b
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    #@4e
    .line 150
    :cond_4e
    :goto_4e
    return-void

    #@4f
    .line 129
    :cond_4f
    if-le p2, p6, :cond_2d

    #@51
    if-gt p4, p6, :cond_2d

    #@53
    .line 130
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@55
    iget-object v1, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@57
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@59
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@5c
    move-result v1

    #@5d
    float-to-int v1, v1

    #@5e
    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    #@61
    .line 131
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@63
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@66
    move-result v0

    #@67
    if-nez v0, :cond_2d

    #@69
    .line 132
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@6b
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    #@6e
    goto :goto_2d

    #@6f
    .line 143
    :cond_6f
    if-le p1, p5, :cond_4e

    #@71
    if-gt p3, p5, :cond_4e

    #@73
    .line 144
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@75
    iget-object v1, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@77
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@79
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@7c
    move-result v1

    #@7d
    float-to-int v1, v1

    #@7e
    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    #@81
    .line 145
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@83
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@86
    move-result v0

    #@87
    if-nez v0, :cond_4e

    #@89
    .line 146
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@8b
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    #@8e
    goto :goto_4e
.end method

.method public drawEdgeGlows(Landroid/graphics/Canvas;)Z
    .registers 12
    .parameter "canvas"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 159
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@3
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@6
    move-result v3

    #@7
    .line 160
    .local v3, scrollX:I
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@9
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@c
    move-result v4

    #@d
    .line 161
    .local v4, scrollY:I
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@f
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getWidth()I

    #@12
    move-result v5

    #@13
    .line 162
    .local v5, width:I
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@15
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@18
    move-result v0

    #@19
    .line 164
    .local v0, height:I
    const/4 v1, 0x0

    #@1a
    .line 165
    .local v1, invalidateForGlow:Z
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@1c
    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@1f
    move-result v6

    #@20
    if-nez v6, :cond_45

    #@22
    .line 166
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@25
    move-result v2

    #@26
    .line 168
    .local v2, restoreCount:I
    int-to-float v6, v3

    #@27
    iget-object v7, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@29
    invoke-virtual {v7}, Landroid/webkit/WebViewClassic;->getVisibleTitleHeight()I

    #@2c
    move-result v7

    #@2d
    invoke-static {v9, v4}, Ljava/lang/Math;->min(II)I

    #@30
    move-result v8

    #@31
    add-int/2addr v7, v8

    #@32
    int-to-float v7, v7

    #@33
    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    #@36
    .line 169
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@38
    invoke-virtual {v6, v5, v0}, Landroid/widget/EdgeEffect;->setSize(II)V

    #@3b
    .line 170
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@3d
    invoke-virtual {v6, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    #@40
    move-result v6

    #@41
    or-int/2addr v1, v6

    #@42
    .line 171
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@45
    .line 173
    .end local v2           #restoreCount:I
    :cond_45
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@47
    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@4a
    move-result v6

    #@4b
    if-nez v6, :cond_79

    #@4d
    .line 174
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@50
    move-result v2

    #@51
    .line 176
    .restart local v2       #restoreCount:I
    neg-int v6, v5

    #@52
    add-int/2addr v6, v3

    #@53
    int-to-float v6, v6

    #@54
    iget-object v7, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@56
    invoke-virtual {v7}, Landroid/webkit/WebViewClassic;->computeMaxScrollY()I

    #@59
    move-result v7

    #@5a
    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    #@5d
    move-result v7

    #@5e
    add-int/2addr v7, v0

    #@5f
    int-to-float v7, v7

    #@60
    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    #@63
    .line 178
    const/high16 v6, 0x4334

    #@65
    int-to-float v7, v5

    #@66
    const/4 v8, 0x0

    #@67
    invoke-virtual {p1, v6, v7, v8}, Landroid/graphics/Canvas;->rotate(FFF)V

    #@6a
    .line 179
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@6c
    invoke-virtual {v6, v5, v0}, Landroid/widget/EdgeEffect;->setSize(II)V

    #@6f
    .line 180
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@71
    invoke-virtual {v6, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    #@74
    move-result v6

    #@75
    or-int/2addr v1, v6

    #@76
    .line 181
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@79
    .line 183
    .end local v2           #restoreCount:I
    :cond_79
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@7b
    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@7e
    move-result v6

    #@7f
    if-nez v6, :cond_a4

    #@81
    .line 184
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@84
    move-result v2

    #@85
    .line 186
    .restart local v2       #restoreCount:I
    const/high16 v6, 0x4387

    #@87
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->rotate(F)V

    #@8a
    .line 187
    neg-int v6, v0

    #@8b
    sub-int/2addr v6, v4

    #@8c
    int-to-float v6, v6

    #@8d
    invoke-static {v9, v3}, Ljava/lang/Math;->min(II)I

    #@90
    move-result v7

    #@91
    int-to-float v7, v7

    #@92
    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    #@95
    .line 188
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@97
    invoke-virtual {v6, v0, v5}, Landroid/widget/EdgeEffect;->setSize(II)V

    #@9a
    .line 189
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@9c
    invoke-virtual {v6, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    #@9f
    move-result v6

    #@a0
    or-int/2addr v1, v6

    #@a1
    .line 190
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@a4
    .line 192
    .end local v2           #restoreCount:I
    :cond_a4
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@a6
    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@a9
    move-result v6

    #@aa
    if-nez v6, :cond_d5

    #@ac
    .line 193
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@af
    move-result v2

    #@b0
    .line 195
    .restart local v2       #restoreCount:I
    const/high16 v6, 0x42b4

    #@b2
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->rotate(F)V

    #@b5
    .line 196
    int-to-float v6, v4

    #@b6
    iget-object v7, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@b8
    invoke-virtual {v7}, Landroid/webkit/WebViewClassic;->computeMaxScrollX()I

    #@bb
    move-result v7

    #@bc
    invoke-static {v7, v3}, Ljava/lang/Math;->max(II)I

    #@bf
    move-result v7

    #@c0
    add-int/2addr v7, v5

    #@c1
    neg-int v7, v7

    #@c2
    int-to-float v7, v7

    #@c3
    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    #@c6
    .line 198
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@c8
    invoke-virtual {v6, v0, v5}, Landroid/widget/EdgeEffect;->setSize(II)V

    #@cb
    .line 199
    iget-object v6, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@cd
    invoke-virtual {v6, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    #@d0
    move-result v6

    #@d1
    or-int/2addr v1, v6

    #@d2
    .line 200
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@d5
    .line 202
    .end local v2           #restoreCount:I
    :cond_d5
    return v1
.end method

.method public isAnimating()Z
    .registers 2

    #@0
    .prologue
    .line 209
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@2
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_20

    #@8
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@a
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_20

    #@10
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@12
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_20

    #@18
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@1a
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_22

    #@20
    :cond_20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method public pullGlow(IIIIII)V
    .registers 13
    .parameter "x"
    .parameter "y"
    .parameter "oldX"
    .parameter "oldY"
    .parameter "maxX"
    .parameter "maxY"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 64
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@3
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@6
    move-result v2

    #@7
    if-ne p3, v2, :cond_6b

    #@9
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@b
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@e
    move-result v2

    #@f
    if-ne p4, v2, :cond_6b

    #@11
    .line 67
    if-lez p5, :cond_38

    #@13
    .line 68
    iget v2, p0, Landroid/webkit/OverScrollGlow;->mOverScrollDeltaX:I

    #@15
    add-int v0, p3, v2

    #@17
    .line 69
    .local v0, pulledToX:I
    if-gez v0, :cond_6c

    #@19
    .line 70
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@1b
    iget v3, p0, Landroid/webkit/OverScrollGlow;->mOverScrollDeltaX:I

    #@1d
    int-to-float v3, v3

    #@1e
    iget-object v4, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@20
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWidth()I

    #@23
    move-result v4

    #@24
    int-to-float v4, v4

    #@25
    div-float/2addr v3, v4

    #@26
    invoke-virtual {v2, v3}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@29
    .line 71
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@2b
    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@2e
    move-result v2

    #@2f
    if-nez v2, :cond_36

    #@31
    .line 72
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@33
    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->onRelease()V

    #@36
    .line 80
    :cond_36
    :goto_36
    iput v5, p0, Landroid/webkit/OverScrollGlow;->mOverScrollDeltaX:I

    #@38
    .line 83
    .end local v0           #pulledToX:I
    :cond_38
    if-gtz p6, :cond_46

    #@3a
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@3c
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Landroid/webkit/WebView;->getOverScrollMode()I

    #@43
    move-result v2

    #@44
    if-nez v2, :cond_6b

    #@46
    .line 84
    :cond_46
    iget v2, p0, Landroid/webkit/OverScrollGlow;->mOverScrollDeltaY:I

    #@48
    add-int v1, p4, v2

    #@4a
    .line 85
    .local v1, pulledToY:I
    if-gez v1, :cond_8c

    #@4c
    .line 86
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@4e
    iget v3, p0, Landroid/webkit/OverScrollGlow;->mOverScrollDeltaY:I

    #@50
    int-to-float v3, v3

    #@51
    iget-object v4, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@53
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@56
    move-result v4

    #@57
    int-to-float v4, v4

    #@58
    div-float/2addr v3, v4

    #@59
    invoke-virtual {v2, v3}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@5c
    .line 87
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@5e
    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@61
    move-result v2

    #@62
    if-nez v2, :cond_69

    #@64
    .line 88
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@66
    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->onRelease()V

    #@69
    .line 96
    :cond_69
    :goto_69
    iput v5, p0, Landroid/webkit/OverScrollGlow;->mOverScrollDeltaY:I

    #@6b
    .line 99
    .end local v1           #pulledToY:I
    :cond_6b
    return-void

    #@6c
    .line 74
    .restart local v0       #pulledToX:I
    :cond_6c
    if-le v0, p5, :cond_36

    #@6e
    .line 75
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@70
    iget v3, p0, Landroid/webkit/OverScrollGlow;->mOverScrollDeltaX:I

    #@72
    int-to-float v3, v3

    #@73
    iget-object v4, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@75
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWidth()I

    #@78
    move-result v4

    #@79
    int-to-float v4, v4

    #@7a
    div-float/2addr v3, v4

    #@7b
    invoke-virtual {v2, v3}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@7e
    .line 76
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@80
    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@83
    move-result v2

    #@84
    if-nez v2, :cond_36

    #@86
    .line 77
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@88
    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->onRelease()V

    #@8b
    goto :goto_36

    #@8c
    .line 90
    .end local v0           #pulledToX:I
    .restart local v1       #pulledToY:I
    :cond_8c
    if-le v1, p6, :cond_69

    #@8e
    .line 91
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@90
    iget v3, p0, Landroid/webkit/OverScrollGlow;->mOverScrollDeltaY:I

    #@92
    int-to-float v3, v3

    #@93
    iget-object v4, p0, Landroid/webkit/OverScrollGlow;->mHostView:Landroid/webkit/WebViewClassic;

    #@95
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@98
    move-result v4

    #@99
    int-to-float v4, v4

    #@9a
    div-float/2addr v3, v4

    #@9b
    invoke-virtual {v2, v3}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@9e
    .line 92
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@a0
    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@a3
    move-result v2

    #@a4
    if-nez v2, :cond_69

    #@a6
    .line 93
    iget-object v2, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@a8
    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->onRelease()V

    #@ab
    goto :goto_69
.end method

.method public releaseAll()V
    .registers 2

    #@0
    .prologue
    .line 217
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@2
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    #@5
    .line 218
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@7
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    #@a
    .line 219
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowLeft:Landroid/widget/EdgeEffect;

    #@c
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    #@f
    .line 220
    iget-object v0, p0, Landroid/webkit/OverScrollGlow;->mEdgeGlowRight:Landroid/widget/EdgeEffect;

    #@11
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    #@14
    .line 221
    return-void
.end method

.method public setOverScrollDeltas(II)V
    .registers 3
    .parameter "deltaX"
    .parameter "deltaY"

    #@0
    .prologue
    .line 108
    iput p1, p0, Landroid/webkit/OverScrollGlow;->mOverScrollDeltaX:I

    #@2
    .line 109
    iput p2, p0, Landroid/webkit/OverScrollGlow;->mOverScrollDeltaY:I

    #@4
    .line 110
    return-void
.end method
