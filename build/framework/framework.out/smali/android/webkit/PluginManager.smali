.class public Landroid/webkit/PluginManager;
.super Ljava/lang/Object;
.source "PluginManager.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "PluginManager"

.field public static final PLUGIN_ACTION:Ljava/lang/String; = "android.webkit.PLUGIN"

.field public static final PLUGIN_PERMISSION:Ljava/lang/String; = "android.webkit.permission.PLUGIN"

.field private static final PLUGIN_SYSTEM_LIB:Ljava/lang/String; = "/system/lib/plugins/"

.field private static final PLUGIN_TYPE:Ljava/lang/String; = "type"

.field private static final SIGNATURES:[Landroid/content/pm/Signature; = null

.field private static final SIGNATURE_1:Ljava/lang/String; = "308204c5308203ada003020102020900d7cb412f75f4887e300d06092a864886f70d010105050030819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f7261746564301e170d3039313030313030323331345a170d3337303231363030323331345a30819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f726174656430820120300d06092a864886f70d01010105000382010d0030820108028201010099724f3e05bbd78843794f357776e04b340e13cb1c9ccb3044865180d7d8fec8166c5bbd876da8b80aa71eb6ba3d4d3455c9a8de162d24a25c4c1cd04c9523affd06a279fc8f0d018f242486bdbb2dbfbf6fcb21ed567879091928b876f7ccebc7bccef157366ebe74e33ae1d7e9373091adab8327482154afc0693a549522f8c796dd84d16e24bb221f5dbb809ca56dd2b6e799c5fa06b6d9c5c09ada54ea4c5db1523a9794ed22a3889e5e05b29f8ee0a8d61efe07ae28f65dece2ff7edc5b1416d7c7aad7f0d35e8f4a4b964dbf50ae9aa6d620157770d974131b3e7e3abd6d163d65758e2f0822db9c88598b9db6263d963d13942c91fc5efe34fc1e06e3020103a382010630820102301d0603551d0e041604145af418e419a639e1657db960996364a37ef20d403081d20603551d230481ca3081c780145af418e419a639e1657db960996364a37ef20d40a181a3a481a030819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f7261746564820900d7cb412f75f4887e300c0603551d13040530030101ff300d06092a864886f70d0101050500038201010076c2a11fe303359689c2ebc7b2c398eff8c3f9ad545cdbac75df63bf7b5395b6988d1842d6aa1556d595b5692e08224d667a4c9c438f05e74906c53dd8016dde7004068866f01846365efd146e9bfaa48c9ecf657f87b97c757da11f225c4a24177bf2d7188e6cce2a70a1e8a841a14471eb51457398b8a0addd8b6c8c1538ca8f1e40b4d8b960009ea22c188d28924813d2c0b4a4d334b7cf05507e1fcf0a06fe946c7ffc435e173af6fc3e3400643710acc806f830a14788291d46f2feed9fb5c70423ca747ed1572d752894ac1f19f93989766308579393fabb43649aa8806a313b1ab9a50922a44c2467b9062037f2da0d484d9ffd8fe628eeea629ba637"

.field private static final TYPE_NATIVE:Ljava/lang/String; = "native"

.field private static mInstance:Landroid/webkit/PluginManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mPackageInfoCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 69
    const/4 v0, 0x0

    #@1
    sput-object v0, Landroid/webkit/PluginManager;->mInstance:Landroid/webkit/PluginManager;

    #@3
    .line 79
    const/4 v0, 0x1

    #@4
    new-array v0, v0, [Landroid/content/pm/Signature;

    #@6
    const/4 v1, 0x0

    #@7
    new-instance v2, Landroid/content/pm/Signature;

    #@9
    const-string v3, "308204c5308203ada003020102020900d7cb412f75f4887e300d06092a864886f70d010105050030819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f7261746564301e170d3039313030313030323331345a170d3337303231363030323331345a30819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f726174656430820120300d06092a864886f70d01010105000382010d0030820108028201010099724f3e05bbd78843794f357776e04b340e13cb1c9ccb3044865180d7d8fec8166c5bbd876da8b80aa71eb6ba3d4d3455c9a8de162d24a25c4c1cd04c9523affd06a279fc8f0d018f242486bdbb2dbfbf6fcb21ed567879091928b876f7ccebc7bccef157366ebe74e33ae1d7e9373091adab8327482154afc0693a549522f8c796dd84d16e24bb221f5dbb809ca56dd2b6e799c5fa06b6d9c5c09ada54ea4c5db1523a9794ed22a3889e5e05b29f8ee0a8d61efe07ae28f65dece2ff7edc5b1416d7c7aad7f0d35e8f4a4b964dbf50ae9aa6d620157770d974131b3e7e3abd6d163d65758e2f0822db9c88598b9db6263d963d13942c91fc5efe34fc1e06e3020103a382010630820102301d0603551d0e041604145af418e419a639e1657db960996364a37ef20d403081d20603551d230481ca3081c780145af418e419a639e1657db960996364a37ef20d40a181a3a481a030819d310b3009060355040613025553311330110603550408130a43616c69666f726e69613111300f0603550407130853616e204a6f736531233021060355040a131a41646f62652053797374656d7320496e636f72706f7261746564311c301a060355040b1313496e666f726d6174696f6e2053797374656d73312330210603550403131a41646f62652053797374656d7320496e636f72706f7261746564820900d7cb412f75f4887e300c0603551d13040530030101ff300d06092a864886f70d0101050500038201010076c2a11fe303359689c2ebc7b2c398eff8c3f9ad545cdbac75df63bf7b5395b6988d1842d6aa1556d595b5692e08224d667a4c9c438f05e74906c53dd8016dde7004068866f01846365efd146e9bfaa48c9ecf657f87b97c757da11f225c4a24177bf2d7188e6cce2a70a1e8a841a14471eb51457398b8a0addd8b6c8c1538ca8f1e40b4d8b960009ea22c188d28924813d2c0b4a4d334b7cf05507e1fcf0a06fe946c7ffc435e173af6fc3e3400643710acc806f830a14788291d46f2feed9fb5c70423ca747ed1572d752894ac1f19f93989766308579393fabb43649aa8806a313b1ab9a50922a44c2467b9062037f2da0d484d9ffd8fe628eeea629ba637"

    #@b
    invoke-direct {v2, v3}, Landroid/content/pm/Signature;-><init>(Ljava/lang/String;)V

    #@e
    aput-object v2, v0, v1

    #@10
    sput-object v0, Landroid/webkit/PluginManager;->SIGNATURES:[Landroid/content/pm/Signature;

    #@12
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 83
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 84
    iput-object p1, p0, Landroid/webkit/PluginManager;->mContext:Landroid/content/Context;

    #@5
    .line 85
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/webkit/PluginManager;->mPackageInfoCache:Ljava/util/ArrayList;

    #@c
    .line 86
    return-void
.end method

.method private static containsPluginPermissionAndSignatures(Landroid/content/pm/PackageInfo;)Z
    .registers 13
    .parameter "pkgInfo"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 228
    iget-object v5, p0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    #@3
    .line 229
    .local v5, permissions:[Ljava/lang/String;
    if-nez v5, :cond_6

    #@5
    .line 263
    :cond_5
    :goto_5
    return v10

    #@6
    .line 232
    :cond_6
    const/4 v4, 0x0

    #@7
    .line 233
    .local v4, permissionOk:Z
    move-object v0, v5

    #@8
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@9
    .local v3, len$:I
    const/4 v2, 0x0

    #@a
    .local v2, i$:I
    :goto_a
    if-ge v2, v3, :cond_17

    #@c
    aget-object v6, v0, v2

    #@e
    .line 234
    .local v6, permit:Ljava/lang/String;
    const-string v11, "android.webkit.permission.PLUGIN"

    #@10
    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v11

    #@14
    if-eqz v11, :cond_42

    #@16
    .line 235
    const/4 v4, 0x1

    #@17
    .line 239
    .end local v6           #permit:Ljava/lang/String;
    :cond_17
    if-eqz v4, :cond_5

    #@19
    .line 244
    iget-object v9, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@1b
    .line 245
    .local v9, signatures:[Landroid/content/pm/Signature;
    if-eqz v9, :cond_5

    #@1d
    .line 248
    const-string/jumbo v11, "ro.secure"

    #@20
    invoke-static {v11, v10}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@23
    move-result v11

    #@24
    if-eqz v11, :cond_4a

    #@26
    .line 249
    const/4 v8, 0x0

    #@27
    .line 250
    .local v8, signatureMatch:Z
    move-object v0, v9

    #@28
    .local v0, arr$:[Landroid/content/pm/Signature;
    array-length v3, v0

    #@29
    const/4 v2, 0x0

    #@2a
    :goto_2a
    if-ge v2, v3, :cond_48

    #@2c
    aget-object v7, v0, v2

    #@2e
    .line 251
    .local v7, signature:Landroid/content/pm/Signature;
    const/4 v1, 0x0

    #@2f
    .local v1, i:I
    :goto_2f
    sget-object v11, Landroid/webkit/PluginManager;->SIGNATURES:[Landroid/content/pm/Signature;

    #@31
    array-length v11, v11

    #@32
    if-ge v1, v11, :cond_3f

    #@34
    .line 252
    sget-object v11, Landroid/webkit/PluginManager;->SIGNATURES:[Landroid/content/pm/Signature;

    #@36
    aget-object v11, v11, v1

    #@38
    invoke-virtual {v11, v7}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v11

    #@3c
    if-eqz v11, :cond_45

    #@3e
    .line 253
    const/4 v8, 0x1

    #@3f
    .line 250
    :cond_3f
    add-int/lit8 v2, v2, 0x1

    #@41
    goto :goto_2a

    #@42
    .line 233
    .end local v1           #i:I
    .end local v7           #signature:Landroid/content/pm/Signature;
    .end local v8           #signatureMatch:Z
    .end local v9           #signatures:[Landroid/content/pm/Signature;
    .local v0, arr$:[Ljava/lang/String;
    .restart local v6       #permit:Ljava/lang/String;
    :cond_42
    add-int/lit8 v2, v2, 0x1

    #@44
    goto :goto_a

    #@45
    .line 251
    .end local v6           #permit:Ljava/lang/String;
    .local v0, arr$:[Landroid/content/pm/Signature;
    .restart local v1       #i:I
    .restart local v7       #signature:Landroid/content/pm/Signature;
    .restart local v8       #signatureMatch:Z
    .restart local v9       #signatures:[Landroid/content/pm/Signature;
    :cond_45
    add-int/lit8 v1, v1, 0x1

    #@47
    goto :goto_2f

    #@48
    .line 258
    .end local v1           #i:I
    .end local v7           #signature:Landroid/content/pm/Signature;
    :cond_48
    if-eqz v8, :cond_5

    #@4a
    .line 263
    .end local v0           #arr$:[Landroid/content/pm/Signature;
    .end local v8           #signatureMatch:Z
    :cond_4a
    const/4 v10, 0x1

    #@4b
    goto :goto_5
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Landroid/webkit/PluginManager;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 89
    const-class v1, Landroid/webkit/PluginManager;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/PluginManager;->mInstance:Landroid/webkit/PluginManager;

    #@5
    if-nez v0, :cond_1f

    #@7
    .line 90
    if-nez p0, :cond_14

    #@9
    .line 91
    new-instance v0, Ljava/lang/IllegalStateException;

    #@b
    const-string v2, "First call to PluginManager need a valid context."

    #@d
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_11

    #@11
    .line 89
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1

    #@13
    throw v0

    #@14
    .line 94
    :cond_14
    :try_start_14
    new-instance v0, Landroid/webkit/PluginManager;

    #@16
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@19
    move-result-object v2

    #@1a
    invoke-direct {v0, v2}, Landroid/webkit/PluginManager;-><init>(Landroid/content/Context;)V

    #@1d
    sput-object v0, Landroid/webkit/PluginManager;->mInstance:Landroid/webkit/PluginManager;

    #@1f
    .line 96
    :cond_1f
    sget-object v0, Landroid/webkit/PluginManager;->mInstance:Landroid/webkit/PluginManager;
    :try_end_21
    .catchall {:try_start_14 .. :try_end_21} :catchall_11

    #@21
    monitor-exit v1

    #@22
    return-object v0
.end method


# virtual methods
.method containsPluginPermissionAndSignatures(Ljava/lang/String;)Z
    .registers 8
    .parameter "pluginAPKName"

    #@0
    .prologue
    .line 210
    iget-object v3, p0, Landroid/webkit/PluginManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v2

    #@6
    .line 214
    .local v2, pm:Landroid/content/pm/PackageManager;
    const/16 v3, 0x1040

    #@8
    :try_start_8
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@b
    move-result-object v1

    #@c
    .line 216
    .local v1, pkgInfo:Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_2c

    #@e
    .line 217
    invoke-static {v1}, Landroid/webkit/PluginManager;->containsPluginPermissionAndSignatures(Landroid/content/pm/PackageInfo;)Z
    :try_end_11
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_11} :catch_13

    #@11
    move-result v3

    #@12
    .line 222
    .end local v1           #pkgInfo:Landroid/content/pm/PackageInfo;
    :goto_12
    return v3

    #@13
    .line 219
    :catch_13
    move-exception v0

    #@14
    .line 220
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "PluginManager"

    #@16
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v5, "Can\'t find plugin: "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 222
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2c
    const/4 v3, 0x0

    #@2d
    goto :goto_12
.end method

.method getPluginClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Class;
    .registers 7
    .parameter "packageName"
    .parameter "className"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 294
    iget-object v2, p0, Landroid/webkit/PluginManager;->mContext:Landroid/content/Context;

    #@2
    const/4 v3, 0x3

    #@3
    invoke-virtual {v2, p1, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@6
    move-result-object v1

    #@7
    .line 297
    .local v1, pluginContext:Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@a
    move-result-object v0

    #@b
    .line 298
    .local v0, pluginCL:Ljava/lang/ClassLoader;
    invoke-virtual {v0, p2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@e
    move-result-object v2

    #@f
    return-object v2
.end method

.method getPluginDirectories()[Ljava/lang/String;
    .registers 22

    #@0
    .prologue
    .line 114
    new-instance v6, Ljava/util/ArrayList;

    #@2
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 115
    .local v6, directories:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    #@7
    iget-object v0, v0, Landroid/webkit/PluginManager;->mContext:Landroid/content/Context;

    #@9
    move-object/from16 v17, v0

    #@b
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@e
    move-result-object v14

    #@f
    .line 116
    .local v14, pm:Landroid/content/pm/PackageManager;
    new-instance v17, Landroid/content/Intent;

    #@11
    const-string v18, "android.webkit.PLUGIN"

    #@13
    invoke-direct/range {v17 .. v18}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@16
    const/16 v18, 0x84

    #@18
    move-object/from16 v0, v17

    #@1a
    move/from16 v1, v18

    #@1c
    invoke-virtual {v14, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    #@1f
    move-result-object v13

    #@20
    .line 119
    .local v13, plugins:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    move-object/from16 v0, p0

    #@22
    iget-object v0, v0, Landroid/webkit/PluginManager;->mPackageInfoCache:Ljava/util/ArrayList;

    #@24
    move-object/from16 v18, v0

    #@26
    monitor-enter v18

    #@27
    .line 122
    :try_start_27
    move-object/from16 v0, p0

    #@29
    iget-object v0, v0, Landroid/webkit/PluginManager;->mPackageInfoCache:Ljava/util/ArrayList;

    #@2b
    move-object/from16 v17, v0

    #@2d
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    #@30
    .line 124
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@33
    move-result-object v9

    #@34
    .local v9, i$:Ljava/util/Iterator;
    :cond_34
    :goto_34
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@37
    move-result v17

    #@38
    if-eqz v17, :cond_1e1

    #@3a
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3d
    move-result-object v10

    #@3e
    check-cast v10, Landroid/content/pm/ResolveInfo;

    #@40
    .line 127
    .local v10, info:Landroid/content/pm/ResolveInfo;
    iget-object v15, v10, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@42
    .line 128
    .local v15, serviceInfo:Landroid/content/pm/ServiceInfo;
    if-nez v15, :cond_53

    #@44
    .line 129
    const-string v17, "PluginManager"

    #@46
    const-string v19, "Ignore bad plugin"

    #@48
    move-object/from16 v0, v17

    #@4a
    move-object/from16 v1, v19

    #@4c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_34

    #@50
    .line 203
    .end local v9           #i$:Ljava/util/Iterator;
    .end local v10           #info:Landroid/content/pm/ResolveInfo;
    .end local v15           #serviceInfo:Landroid/content/pm/ServiceInfo;
    :catchall_50
    move-exception v17

    #@51
    monitor-exit v18
    :try_end_52
    .catchall {:try_start_27 .. :try_end_52} :catchall_50

    #@52
    throw v17

    #@53
    .line 136
    .restart local v9       #i$:Ljava/util/Iterator;
    .restart local v10       #info:Landroid/content/pm/ResolveInfo;
    .restart local v15       #serviceInfo:Landroid/content/pm/ServiceInfo;
    :cond_53
    :try_start_53
    iget-object v0, v15, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@55
    move-object/from16 v17, v0

    #@57
    const/16 v19, 0x1040

    #@59
    move-object/from16 v0, v17

    #@5b
    move/from16 v1, v19

    #@5d
    invoke-virtual {v14, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_60
    .catchall {:try_start_53 .. :try_end_60} :catchall_50
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_53 .. :try_end_60} :catch_f1

    #@60
    move-result-object v11

    #@61
    .line 143
    .local v11, pkgInfo:Landroid/content/pm/PackageInfo;
    if-eqz v11, :cond_34

    #@63
    .line 153
    :try_start_63
    new-instance v17, Ljava/lang/StringBuilder;

    #@65
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@6a
    move-object/from16 v19, v0

    #@6c
    move-object/from16 v0, v19

    #@6e
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@70
    move-object/from16 v19, v0

    #@72
    move-object/from16 v0, v17

    #@74
    move-object/from16 v1, v19

    #@76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v17

    #@7a
    const-string v19, "/lib"

    #@7c
    move-object/from16 v0, v17

    #@7e
    move-object/from16 v1, v19

    #@80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v17

    #@84
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v7

    #@88
    .line 154
    .local v7, directory:Ljava/lang/String;
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@8a
    move-object/from16 v17, v0

    #@8c
    move-object/from16 v0, v17

    #@8e
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@90
    .line 155
    .local v3, appFlags:I
    const/16 v16, 0x81

    #@92
    .line 158
    .local v16, updatedSystemFlags:I
    and-int/lit16 v0, v3, 0x81

    #@94
    move/from16 v17, v0

    #@96
    const/16 v19, 0x1

    #@98
    move/from16 v0, v17

    #@9a
    move/from16 v1, v19

    #@9c
    if-ne v0, v1, :cond_bd

    #@9e
    .line 159
    new-instance v17, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v19, "/system/lib/plugins/"

    #@a5
    move-object/from16 v0, v17

    #@a7
    move-object/from16 v1, v19

    #@a9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v17

    #@ad
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@af
    move-object/from16 v19, v0

    #@b1
    move-object/from16 v0, v17

    #@b3
    move-object/from16 v1, v19

    #@b5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v17

    #@b9
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v7

    #@bd
    .line 164
    :cond_bd
    invoke-static {v11}, Landroid/webkit/PluginManager;->containsPluginPermissionAndSignatures(Landroid/content/pm/PackageInfo;)Z

    #@c0
    move-result v17

    #@c1
    if-eqz v17, :cond_34

    #@c3
    .line 169
    iget-object v0, v15, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@c5
    move-object/from16 v17, v0

    #@c7
    if-nez v17, :cond_114

    #@c9
    .line 170
    const-string v17, "PluginManager"

    #@cb
    new-instance v19, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v20, "The plugin \'"

    #@d2
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v19

    #@d6
    iget-object v0, v15, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@d8
    move-object/from16 v20, v0

    #@da
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v19

    #@de
    const-string v20, "\' has no type defined"

    #@e0
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v19

    #@e4
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e7
    move-result-object v19

    #@e8
    move-object/from16 v0, v17

    #@ea
    move-object/from16 v1, v19

    #@ec
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    goto/16 :goto_34

    #@f1
    .line 139
    .end local v3           #appFlags:I
    .end local v7           #directory:Ljava/lang/String;
    .end local v11           #pkgInfo:Landroid/content/pm/PackageInfo;
    .end local v16           #updatedSystemFlags:I
    :catch_f1
    move-exception v8

    #@f2
    .line 140
    .local v8, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v17, "PluginManager"

    #@f4
    new-instance v19, Ljava/lang/StringBuilder;

    #@f6
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@f9
    const-string v20, "Can\'t find plugin: "

    #@fb
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v19

    #@ff
    iget-object v0, v15, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@101
    move-object/from16 v20, v0

    #@103
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v19

    #@107
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v19

    #@10b
    move-object/from16 v0, v17

    #@10d
    move-object/from16 v1, v19

    #@10f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@112
    goto/16 :goto_34

    #@114
    .line 174
    .end local v8           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v3       #appFlags:I
    .restart local v7       #directory:Ljava/lang/String;
    .restart local v11       #pkgInfo:Landroid/content/pm/PackageInfo;
    .restart local v16       #updatedSystemFlags:I
    :cond_114
    iget-object v0, v15, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@116
    move-object/from16 v17, v0

    #@118
    const-string/jumbo v19, "type"

    #@11b
    move-object/from16 v0, v17

    #@11d
    move-object/from16 v1, v19

    #@11f
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@122
    move-result-object v12

    #@123
    .line 175
    .local v12, pluginType:Ljava/lang/String;
    const-string/jumbo v17, "native"

    #@126
    move-object/from16 v0, v17

    #@128
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12b
    move-result v17

    #@12c
    if-nez v17, :cond_14e

    #@12e
    .line 176
    const-string v17, "PluginManager"

    #@130
    new-instance v19, Ljava/lang/StringBuilder;

    #@132
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@135
    const-string v20, "Unrecognized plugin type: "

    #@137
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v19

    #@13b
    move-object/from16 v0, v19

    #@13d
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v19

    #@141
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@144
    move-result-object v19

    #@145
    move-object/from16 v0, v17

    #@147
    move-object/from16 v1, v19

    #@149
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14c
    .catchall {:try_start_63 .. :try_end_14c} :catchall_50

    #@14c
    goto/16 :goto_34

    #@14e
    .line 181
    :cond_14e
    :try_start_14e
    iget-object v0, v15, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@150
    move-object/from16 v17, v0

    #@152
    iget-object v0, v15, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@154
    move-object/from16 v19, v0

    #@156
    move-object/from16 v0, p0

    #@158
    move-object/from16 v1, v17

    #@15a
    move-object/from16 v2, v19

    #@15c
    invoke-virtual {v0, v1, v2}, Landroid/webkit/PluginManager;->getPluginClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Class;

    #@15f
    move-result-object v5

    #@160
    .line 184
    .local v5, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v4, 0x1

    #@161
    .line 186
    .local v4, classFound:Z
    if-nez v4, :cond_1d1

    #@163
    .line 187
    const-string v17, "PluginManager"

    #@165
    new-instance v19, Ljava/lang/StringBuilder;

    #@167
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@16a
    const-string v20, "The plugin\'s class\' "

    #@16c
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v19

    #@170
    iget-object v0, v15, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@172
    move-object/from16 v20, v0

    #@174
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v19

    #@178
    const-string v20, "\' does not extend the appropriate class."

    #@17a
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v19

    #@17e
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@181
    move-result-object v19

    #@182
    move-object/from16 v0, v17

    #@184
    move-object/from16 v1, v19

    #@186
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_189
    .catchall {:try_start_14e .. :try_end_189} :catchall_50
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_14e .. :try_end_189} :catch_18b
    .catch Ljava/lang/ClassNotFoundException; {:try_start_14e .. :try_end_189} :catch_1ae

    #@189
    goto/16 :goto_34

    #@18b
    .line 191
    .end local v4           #classFound:Z
    .end local v5           #cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_18b
    move-exception v8

    #@18c
    .line 192
    .restart local v8       #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_18c
    const-string v17, "PluginManager"

    #@18e
    new-instance v19, Ljava/lang/StringBuilder;

    #@190
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@193
    const-string v20, "Can\'t find plugin: "

    #@195
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    move-result-object v19

    #@199
    iget-object v0, v15, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@19b
    move-object/from16 v20, v0

    #@19d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v19

    #@1a1
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a4
    move-result-object v19

    #@1a5
    move-object/from16 v0, v17

    #@1a7
    move-object/from16 v1, v19

    #@1a9
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1ac
    goto/16 :goto_34

    #@1ae
    .line 194
    .end local v8           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1ae
    move-exception v8

    #@1af
    .line 195
    .local v8, e:Ljava/lang/ClassNotFoundException;
    const-string v17, "PluginManager"

    #@1b1
    new-instance v19, Ljava/lang/StringBuilder;

    #@1b3
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1b6
    const-string v20, "Can\'t find plugin\'s class: "

    #@1b8
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v19

    #@1bc
    iget-object v0, v15, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@1be
    move-object/from16 v20, v0

    #@1c0
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c3
    move-result-object v19

    #@1c4
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c7
    move-result-object v19

    #@1c8
    move-object/from16 v0, v17

    #@1ca
    move-object/from16 v1, v19

    #@1cc
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1cf
    goto/16 :goto_34

    #@1d1
    .line 200
    .end local v8           #e:Ljava/lang/ClassNotFoundException;
    .restart local v4       #classFound:Z
    .restart local v5       #cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_1d1
    move-object/from16 v0, p0

    #@1d3
    iget-object v0, v0, Landroid/webkit/PluginManager;->mPackageInfoCache:Ljava/util/ArrayList;

    #@1d5
    move-object/from16 v17, v0

    #@1d7
    move-object/from16 v0, v17

    #@1d9
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1dc
    .line 201
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1df
    goto/16 :goto_34

    #@1e1
    .line 203
    .end local v3           #appFlags:I
    .end local v4           #classFound:Z
    .end local v5           #cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v7           #directory:Ljava/lang/String;
    .end local v10           #info:Landroid/content/pm/ResolveInfo;
    .end local v11           #pkgInfo:Landroid/content/pm/PackageInfo;
    .end local v12           #pluginType:Ljava/lang/String;
    .end local v15           #serviceInfo:Landroid/content/pm/ServiceInfo;
    .end local v16           #updatedSystemFlags:I
    :cond_1e1
    monitor-exit v18
    :try_end_1e2
    .catchall {:try_start_18c .. :try_end_1e2} :catchall_50

    #@1e2
    .line 205
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@1e5
    move-result v17

    #@1e6
    move/from16 v0, v17

    #@1e8
    new-array v0, v0, [Ljava/lang/String;

    #@1ea
    move-object/from16 v17, v0

    #@1ec
    move-object/from16 v0, v17

    #@1ee
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@1f1
    move-result-object v17

    #@1f2
    check-cast v17, [Ljava/lang/String;

    #@1f4
    return-object v17
.end method

.method getPluginSharedDataDirectory()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 288
    iget-object v0, p0, Landroid/webkit/PluginManager;->mContext:Landroid/content/Context;

    #@2
    const-string/jumbo v1, "plugins"

    #@5
    const/4 v2, 0x0

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    return-object v0
.end method

.method getPluginsAPKName(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "pluginLib"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 270
    if-eqz p1, :cond_9

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@6
    move-result v3

    #@7
    if-nez v3, :cond_a

    #@9
    .line 284
    :cond_9
    :goto_9
    return-object v2

    #@a
    .line 275
    :cond_a
    iget-object v3, p0, Landroid/webkit/PluginManager;->mPackageInfoCache:Ljava/util/ArrayList;

    #@c
    monitor-enter v3

    #@d
    .line 276
    :try_start_d
    iget-object v4, p0, Landroid/webkit/PluginManager;->mPackageInfoCache:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v0

    #@13
    .local v0, i$:Ljava/util/Iterator;
    :cond_13
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_2e

    #@19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Landroid/content/pm/PackageInfo;

    #@1f
    .line 277
    .local v1, pkgInfo:Landroid/content/pm/PackageInfo;
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@21
    invoke-virtual {p1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@24
    move-result v4

    #@25
    if-eqz v4, :cond_13

    #@27
    .line 278
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@29
    monitor-exit v3

    #@2a
    goto :goto_9

    #@2b
    .line 281
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #pkgInfo:Landroid/content/pm/PackageInfo;
    :catchall_2b
    move-exception v2

    #@2c
    monitor-exit v3
    :try_end_2d
    .catchall {:try_start_d .. :try_end_2d} :catchall_2b

    #@2d
    throw v2

    #@2e
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_2e
    :try_start_2e
    monitor-exit v3
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2b

    #@2f
    goto :goto_9
.end method

.method public refreshPlugins(Z)V
    .registers 5
    .parameter "reloadOpenPages"

    #@0
    .prologue
    .line 107
    sget-object v0, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@2
    const/16 v1, 0x64

    #@4
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/webkit/JWebCoreJavaBridge;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@f
    .line 110
    return-void
.end method
