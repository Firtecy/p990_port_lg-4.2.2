.class Landroid/webkit/WebViewClassic$FocusTransitionDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "WebViewClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FocusTransitionDrawable"
.end annotation


# instance fields
.field mMaxAlpha:I

.field mNewRegion:Landroid/graphics/Region;

.field mPaint:Landroid/graphics/Paint;

.field mPreviousRegion:Landroid/graphics/Region;

.field mProgress:F

.field mTranslate:Landroid/graphics/Point;

.field mWebView:Landroid/webkit/WebViewClassic;


# direct methods
.method public constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 9450
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 9444
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mProgress:F

    #@6
    .line 9451
    iput-object p1, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mWebView:Landroid/webkit/WebViewClassic;

    #@8
    .line 9452
    new-instance v0, Landroid/graphics/Paint;

    #@a
    iget-object v1, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mWebView:Landroid/webkit/WebViewClassic;

    #@c
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->access$8800(Landroid/webkit/WebViewClassic;)Landroid/graphics/Paint;

    #@f
    move-result-object v1

    #@10
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    #@13
    iput-object v0, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mPaint:Landroid/graphics/Paint;

    #@15
    .line 9453
    iget-object v0, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mPaint:Landroid/graphics/Paint;

    #@17
    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    #@1a
    move-result v0

    #@1b
    iput v0, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mMaxAlpha:I

    #@1d
    .line 9454
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 16
    .parameter "canvas"

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    .line 9484
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mTranslate:Landroid/graphics/Point;

    #@3
    if-nez v9, :cond_3b

    #@5
    .line 9485
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mPreviousRegion:Landroid/graphics/Region;

    #@7
    invoke-virtual {v9}, Landroid/graphics/Region;->getBounds()Landroid/graphics/Rect;

    #@a
    move-result-object v1

    #@b
    .line 9486
    .local v1, bounds:Landroid/graphics/Rect;
    new-instance v2, Landroid/graphics/Point;

    #@d
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    #@10
    move-result v9

    #@11
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    #@14
    move-result v10

    #@15
    invoke-direct {v2, v9, v10}, Landroid/graphics/Point;-><init>(II)V

    #@18
    .line 9487
    .local v2, from:Landroid/graphics/Point;
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mNewRegion:Landroid/graphics/Region;

    #@1a
    invoke-virtual {v9, v1}, Landroid/graphics/Region;->getBounds(Landroid/graphics/Rect;)Z

    #@1d
    .line 9488
    new-instance v6, Landroid/graphics/Point;

    #@1f
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    #@22
    move-result v9

    #@23
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    #@26
    move-result v10

    #@27
    invoke-direct {v6, v9, v10}, Landroid/graphics/Point;-><init>(II)V

    #@2a
    .line 9489
    .local v6, to:Landroid/graphics/Point;
    new-instance v9, Landroid/graphics/Point;

    #@2c
    iget v10, v2, Landroid/graphics/Point;->x:I

    #@2e
    iget v11, v6, Landroid/graphics/Point;->x:I

    #@30
    sub-int/2addr v10, v11

    #@31
    iget v11, v2, Landroid/graphics/Point;->y:I

    #@33
    iget v12, v6, Landroid/graphics/Point;->y:I

    #@35
    sub-int/2addr v11, v12

    #@36
    invoke-direct {v9, v10, v11}, Landroid/graphics/Point;-><init>(II)V

    #@39
    iput-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mTranslate:Landroid/graphics/Point;

    #@3b
    .line 9491
    .end local v1           #bounds:Landroid/graphics/Rect;
    .end local v2           #from:Landroid/graphics/Point;
    .end local v6           #to:Landroid/graphics/Point;
    :cond_3b
    iget v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mProgress:F

    #@3d
    iget v10, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mMaxAlpha:I

    #@3f
    int-to-float v10, v10

    #@40
    mul-float/2addr v9, v10

    #@41
    float-to-int v0, v9

    #@42
    .line 9492
    .local v0, alpha:I
    new-instance v3, Landroid/graphics/RegionIterator;

    #@44
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mPreviousRegion:Landroid/graphics/Region;

    #@46
    invoke-direct {v3, v9}, Landroid/graphics/RegionIterator;-><init>(Landroid/graphics/Region;)V

    #@49
    .line 9493
    .local v3, iter:Landroid/graphics/RegionIterator;
    new-instance v4, Landroid/graphics/Rect;

    #@4b
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@4e
    .line 9494
    .local v4, r:Landroid/graphics/Rect;
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mPaint:Landroid/graphics/Paint;

    #@50
    iget v10, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mMaxAlpha:I

    #@52
    sub-int/2addr v10, v0

    #@53
    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setAlpha(I)V

    #@56
    .line 9495
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mTranslate:Landroid/graphics/Point;

    #@58
    iget v9, v9, Landroid/graphics/Point;->x:I

    #@5a
    int-to-float v9, v9

    #@5b
    iget v10, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mProgress:F

    #@5d
    mul-float v7, v9, v10

    #@5f
    .line 9496
    .local v7, tx:F
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mTranslate:Landroid/graphics/Point;

    #@61
    iget v9, v9, Landroid/graphics/Point;->y:I

    #@63
    int-to-float v9, v9

    #@64
    iget v10, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mProgress:F

    #@66
    mul-float v8, v9, v10

    #@68
    .line 9497
    .local v8, ty:F
    invoke-virtual {p1, v13}, Landroid/graphics/Canvas;->save(I)I

    #@6b
    move-result v5

    #@6c
    .line 9498
    .local v5, save:I
    neg-float v9, v7

    #@6d
    neg-float v10, v8

    #@6e
    invoke-virtual {p1, v9, v10}, Landroid/graphics/Canvas;->translate(FF)V

    #@71
    .line 9499
    :goto_71
    invoke-virtual {v3, v4}, Landroid/graphics/RegionIterator;->next(Landroid/graphics/Rect;)Z

    #@74
    move-result v9

    #@75
    if-eqz v9, :cond_7d

    #@77
    .line 9500
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mPaint:Landroid/graphics/Paint;

    #@79
    invoke-virtual {p1, v4, v9}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@7c
    goto :goto_71

    #@7d
    .line 9502
    :cond_7d
    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@80
    .line 9503
    new-instance v3, Landroid/graphics/RegionIterator;

    #@82
    .end local v3           #iter:Landroid/graphics/RegionIterator;
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mNewRegion:Landroid/graphics/Region;

    #@84
    invoke-direct {v3, v9}, Landroid/graphics/RegionIterator;-><init>(Landroid/graphics/Region;)V

    #@87
    .line 9504
    .restart local v3       #iter:Landroid/graphics/RegionIterator;
    new-instance v4, Landroid/graphics/Rect;

    #@89
    .end local v4           #r:Landroid/graphics/Rect;
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@8c
    .line 9505
    .restart local v4       #r:Landroid/graphics/Rect;
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mPaint:Landroid/graphics/Paint;

    #@8e
    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    #@91
    .line 9506
    invoke-virtual {p1, v13}, Landroid/graphics/Canvas;->save(I)I

    #@94
    move-result v5

    #@95
    .line 9507
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mTranslate:Landroid/graphics/Point;

    #@97
    iget v9, v9, Landroid/graphics/Point;->x:I

    #@99
    int-to-float v9, v9

    #@9a
    sub-float v7, v9, v7

    #@9c
    .line 9508
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mTranslate:Landroid/graphics/Point;

    #@9e
    iget v9, v9, Landroid/graphics/Point;->y:I

    #@a0
    int-to-float v9, v9

    #@a1
    sub-float v8, v9, v8

    #@a3
    .line 9509
    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    #@a6
    .line 9510
    :goto_a6
    invoke-virtual {v3, v4}, Landroid/graphics/RegionIterator;->next(Landroid/graphics/Rect;)Z

    #@a9
    move-result v9

    #@aa
    if-eqz v9, :cond_b2

    #@ac
    .line 9511
    iget-object v9, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mPaint:Landroid/graphics/Paint;

    #@ae
    invoke-virtual {p1, v4, v9}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@b1
    goto :goto_a6

    #@b2
    .line 9513
    :cond_b2
    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@b5
    .line 9514
    return-void
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 9466
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getProgress()F
    .registers 2

    #@0
    .prologue
    .line 9479
    iget v0, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mProgress:F

    #@2
    return v0
.end method

.method public setAlpha(I)V
    .registers 2
    .parameter "alpha"

    #@0
    .prologue
    .line 9462
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 2
    .parameter "cf"

    #@0
    .prologue
    .line 9458
    return-void
.end method

.method public setProgress(F)V
    .registers 4
    .parameter "p"

    #@0
    .prologue
    .line 9470
    iput p1, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mProgress:F

    #@2
    .line 9471
    iget-object v0, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mWebView:Landroid/webkit/WebViewClassic;

    #@4
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->access$8900(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

    #@7
    move-result-object v0

    #@8
    if-ne v0, p0, :cond_1d

    #@a
    .line 9472
    iget v0, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mProgress:F

    #@c
    const/high16 v1, 0x3f80

    #@e
    cmpl-float v0, v0, v1

    #@10
    if-nez v0, :cond_18

    #@12
    .line 9473
    iget-object v0, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mWebView:Landroid/webkit/WebViewClassic;

    #@14
    const/4 v1, 0x0

    #@15
    invoke-static {v0, v1}, Landroid/webkit/WebViewClassic;->access$8902(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewClassic$FocusTransitionDrawable;)Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

    #@18
    .line 9474
    :cond_18
    iget-object v0, p0, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mWebView:Landroid/webkit/WebViewClassic;

    #@1a
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@1d
    .line 9476
    :cond_1d
    return-void
.end method
