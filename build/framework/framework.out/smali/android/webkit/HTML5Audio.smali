.class Landroid/webkit/HTML5Audio;
.super Landroid/os/Handler;
.source "HTML5Audio.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/HTML5Audio$1;,
        Landroid/webkit/HTML5Audio$IsPrivateBrowsingEnabledGetter;,
        Landroid/webkit/HTML5Audio$TimeupdateTask;
    }
.end annotation


# static fields
.field private static COMPLETE:I = 0x0

.field private static final COOKIE:Ljava/lang/String; = "Cookie"

.field private static ERROR:I = 0x0

.field private static final HIDE_URL_LOGS:Ljava/lang/String; = "x-hide-urls-from-log"

.field private static IDLE:I = 0x0

.field private static INITIALIZED:I = 0x0

.field private static final LOGTAG:Ljava/lang/String; = "HTML5Audio"

.field private static PAUSED:I = 0x0

.field private static PAUSED_TRANSIENT:I = 0x0

.field private static PREPARED:I = 0x0

.field private static STARTED:I = 0x0

.field private static STOPPED:I = 0x0

.field private static final TIMEUPDATE:I = 0x64

.field private static final TIMEUPDATE_PERIOD:I = 0xfa


# instance fields
.field private mAskToPlay:Z

.field private mContext:Landroid/content/Context;

.field private mIsPrivateBrowsingEnabledGetter:Landroid/webkit/HTML5Audio$IsPrivateBrowsingEnabledGetter;

.field private mLoopEnabled:Z

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mNativePointer:I

.field private mProcessingOnEnd:Z

.field private mState:I

.field private mTimer:Ljava/util/Timer;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 57
    const/4 v0, 0x0

    #@1
    sput v0, Landroid/webkit/HTML5Audio;->IDLE:I

    #@3
    .line 58
    const/4 v0, 0x1

    #@4
    sput v0, Landroid/webkit/HTML5Audio;->INITIALIZED:I

    #@6
    .line 59
    const/4 v0, 0x2

    #@7
    sput v0, Landroid/webkit/HTML5Audio;->PREPARED:I

    #@9
    .line 60
    const/4 v0, 0x4

    #@a
    sput v0, Landroid/webkit/HTML5Audio;->STARTED:I

    #@c
    .line 61
    const/4 v0, 0x5

    #@d
    sput v0, Landroid/webkit/HTML5Audio;->COMPLETE:I

    #@f
    .line 62
    const/4 v0, 0x6

    #@10
    sput v0, Landroid/webkit/HTML5Audio;->PAUSED:I

    #@12
    .line 63
    const/4 v0, 0x7

    #@13
    sput v0, Landroid/webkit/HTML5Audio;->PAUSED_TRANSIENT:I

    #@15
    .line 64
    const/4 v0, -0x2

    #@16
    sput v0, Landroid/webkit/HTML5Audio;->STOPPED:I

    #@18
    .line 65
    const/4 v0, -0x1

    #@19
    sput v0, Landroid/webkit/HTML5Audio;->ERROR:I

    #@1b
    return-void
.end method

.method public constructor <init>(Landroid/webkit/WebViewCore;I)V
    .registers 6
    .parameter "webViewCore"
    .parameter "nativePtr"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 189
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@4
    .line 67
    sget v0, Landroid/webkit/HTML5Audio;->IDLE:I

    #@6
    iput v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@8
    .line 70
    iput-boolean v1, p0, Landroid/webkit/HTML5Audio;->mAskToPlay:Z

    #@a
    .line 71
    iput-boolean v1, p0, Landroid/webkit/HTML5Audio;->mLoopEnabled:Z

    #@c
    .line 72
    iput-boolean v1, p0, Landroid/webkit/HTML5Audio;->mProcessingOnEnd:Z

    #@e
    .line 191
    iput p2, p0, Landroid/webkit/HTML5Audio;->mNativePointer:I

    #@10
    .line 192
    invoke-direct {p0}, Landroid/webkit/HTML5Audio;->resetMediaPlayer()V

    #@13
    .line 193
    invoke-virtual {p1}, Landroid/webkit/WebViewCore;->getContext()Landroid/content/Context;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Landroid/webkit/HTML5Audio;->mContext:Landroid/content/Context;

    #@19
    .line 194
    new-instance v0, Landroid/webkit/HTML5Audio$IsPrivateBrowsingEnabledGetter;

    #@1b
    invoke-virtual {p1}, Landroid/webkit/WebViewCore;->getContext()Landroid/content/Context;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {p1}, Landroid/webkit/WebViewCore;->getWebViewClassic()Landroid/webkit/WebViewClassic;

    #@26
    move-result-object v2

    #@27
    invoke-direct {v0, p0, v1, v2}, Landroid/webkit/HTML5Audio$IsPrivateBrowsingEnabledGetter;-><init>(Landroid/webkit/HTML5Audio;Landroid/os/Looper;Landroid/webkit/WebViewClassic;)V

    #@2a
    iput-object v0, p0, Landroid/webkit/HTML5Audio;->mIsPrivateBrowsingEnabledGetter:Landroid/webkit/HTML5Audio$IsPrivateBrowsingEnabledGetter;

    #@2c
    .line 196
    return-void
.end method

.method private getMaxTimeSeekable()F
    .registers 3

    #@0
    .prologue
    .line 346
    iget v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@2
    sget v1, Landroid/webkit/HTML5Audio;->PREPARED:I

    #@4
    if-lt v0, v1, :cond_11

    #@6
    .line 347
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@8
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    #@b
    move-result v0

    #@c
    int-to-float v0, v0

    #@d
    const/high16 v1, 0x447a

    #@f
    div-float/2addr v0, v1

    #@10
    .line 349
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method private native nativeOnBuffering(II)V
.end method

.method private native nativeOnEnded(I)V
.end method

.method private native nativeOnPrepared(IIII)V
.end method

.method private native nativeOnRequestPlay(I)V
.end method

.method private native nativeOnTimeupdate(II)V
.end method

.method private pause()V
    .registers 3

    #@0
    .prologue
    .line 316
    iget v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@2
    sget v1, Landroid/webkit/HTML5Audio;->STARTED:I

    #@4
    if-ne v0, v1, :cond_18

    #@6
    .line 317
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mTimer:Ljava/util/Timer;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 318
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mTimer:Ljava/util/Timer;

    #@c
    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    #@f
    .line 320
    :cond_f
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@11
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    #@14
    .line 321
    sget v0, Landroid/webkit/HTML5Audio;->PAUSED:I

    #@16
    iput v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@18
    .line 323
    :cond_18
    return-void
.end method

.method private play()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 290
    iget v2, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@3
    sget v3, Landroid/webkit/HTML5Audio;->COMPLETE:I

    #@5
    if-ne v2, v3, :cond_15

    #@7
    iget-boolean v2, p0, Landroid/webkit/HTML5Audio;->mLoopEnabled:Z

    #@9
    if-ne v2, v4, :cond_15

    #@b
    .line 292
    iget-object v2, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@d
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    #@10
    .line 293
    sget v2, Landroid/webkit/HTML5Audio;->STARTED:I

    #@12
    iput v2, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@14
    .line 313
    :cond_14
    :goto_14
    return-void

    #@15
    .line 297
    :cond_15
    iget v2, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@17
    sget v3, Landroid/webkit/HTML5Audio;->ERROR:I

    #@19
    if-lt v2, v3, :cond_2f

    #@1b
    iget v2, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@1d
    sget v3, Landroid/webkit/HTML5Audio;->PREPARED:I

    #@1f
    if-ge v2, v3, :cond_2f

    #@21
    iget-object v2, p0, Landroid/webkit/HTML5Audio;->mUrl:Ljava/lang/String;

    #@23
    if-eqz v2, :cond_2f

    #@25
    .line 298
    invoke-direct {p0}, Landroid/webkit/HTML5Audio;->resetMediaPlayer()V

    #@28
    .line 299
    iget-object v2, p0, Landroid/webkit/HTML5Audio;->mUrl:Ljava/lang/String;

    #@2a
    invoke-direct {p0, v2}, Landroid/webkit/HTML5Audio;->setDataSource(Ljava/lang/String;)V

    #@2d
    .line 300
    iput-boolean v4, p0, Landroid/webkit/HTML5Audio;->mAskToPlay:Z

    #@2f
    .line 303
    :cond_2f
    iget v2, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@31
    sget v3, Landroid/webkit/HTML5Audio;->PREPARED:I

    #@33
    if-lt v2, v3, :cond_14

    #@35
    .line 304
    iget-object v2, p0, Landroid/webkit/HTML5Audio;->mContext:Landroid/content/Context;

    #@37
    const-string v3, "audio"

    #@39
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3c
    move-result-object v0

    #@3d
    check-cast v0, Landroid/media/AudioManager;

    #@3f
    .line 305
    .local v0, audioManager:Landroid/media/AudioManager;
    const/4 v2, 0x3

    #@40
    invoke-virtual {v0, p0, v2, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    #@43
    move-result v1

    #@44
    .line 308
    .local v1, result:I
    if-ne v1, v4, :cond_14

    #@46
    .line 309
    iget-object v2, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@48
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    #@4b
    .line 310
    sget v2, Landroid/webkit/HTML5Audio;->STARTED:I

    #@4d
    iput v2, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@4f
    goto :goto_14
.end method

.method private resetMediaPlayer()V
    .registers 2

    #@0
    .prologue
    .line 199
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2
    if-nez v0, :cond_39

    #@4
    .line 200
    new-instance v0, Landroid/media/MediaPlayer;

    #@6
    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    #@9
    iput-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@b
    .line 204
    :goto_b
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@d
    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    #@10
    .line 205
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@12
    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    #@15
    .line 206
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@17
    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    #@1a
    .line 207
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@1c
    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    #@1f
    .line 208
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@21
    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    #@24
    .line 210
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mTimer:Ljava/util/Timer;

    #@26
    if-eqz v0, :cond_2d

    #@28
    .line 211
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mTimer:Ljava/util/Timer;

    #@2a
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    #@2d
    .line 213
    :cond_2d
    new-instance v0, Ljava/util/Timer;

    #@2f
    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    #@32
    iput-object v0, p0, Landroid/webkit/HTML5Audio;->mTimer:Ljava/util/Timer;

    #@34
    .line 214
    sget v0, Landroid/webkit/HTML5Audio;->IDLE:I

    #@36
    iput v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@38
    .line 215
    return-void

    #@39
    .line 202
    :cond_39
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@3b
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    #@3e
    goto :goto_b
.end method

.method private seek(I)V
    .registers 5
    .parameter "msec"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 326
    iget-boolean v0, p0, Landroid/webkit/HTML5Audio;->mProcessingOnEnd:Z

    #@3
    if-ne v0, v2, :cond_f

    #@5
    iget v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@7
    sget v1, Landroid/webkit/HTML5Audio;->COMPLETE:I

    #@9
    if-ne v0, v1, :cond_f

    #@b
    if-nez p1, :cond_f

    #@d
    .line 327
    iput-boolean v2, p0, Landroid/webkit/HTML5Audio;->mLoopEnabled:Z

    #@f
    .line 329
    :cond_f
    iget v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@11
    sget v1, Landroid/webkit/HTML5Audio;->PREPARED:I

    #@13
    if-lt v0, v1, :cond_1a

    #@15
    .line 330
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@17
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    #@1a
    .line 332
    :cond_1a
    return-void
.end method

.method private setDataSource(Ljava/lang/String;)V
    .registers 9
    .parameter "url"

    #@0
    .prologue
    const/16 v6, 0x80

    #@2
    .line 218
    iput-object p1, p0, Landroid/webkit/HTML5Audio;->mUrl:Ljava/lang/String;

    #@4
    .line 220
    :try_start_4
    iget v4, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@6
    sget v5, Landroid/webkit/HTML5Audio;->IDLE:I

    #@8
    if-eq v4, v5, :cond_d

    #@a
    .line 221
    invoke-direct {p0}, Landroid/webkit/HTML5Audio;->resetMediaPlayer()V

    #@d
    .line 223
    :cond_d
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    #@10
    move-result-object v4

    #@11
    iget-object v5, p0, Landroid/webkit/HTML5Audio;->mIsPrivateBrowsingEnabledGetter:Landroid/webkit/HTML5Audio$IsPrivateBrowsingEnabledGetter;

    #@13
    invoke-virtual {v5}, Landroid/webkit/HTML5Audio$IsPrivateBrowsingEnabledGetter;->get()Z

    #@16
    move-result v5

    #@17
    invoke-virtual {v4, p1, v5}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;Z)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 225
    .local v0, cookieValue:Ljava/lang/String;
    new-instance v3, Ljava/util/HashMap;

    #@1d
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    #@20
    .line 227
    .local v3, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_27

    #@22
    .line 228
    const-string v4, "Cookie"

    #@24
    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    .line 230
    :cond_27
    iget-object v4, p0, Landroid/webkit/HTML5Audio;->mIsPrivateBrowsingEnabledGetter:Landroid/webkit/HTML5Audio$IsPrivateBrowsingEnabledGetter;

    #@29
    invoke-virtual {v4}, Landroid/webkit/HTML5Audio$IsPrivateBrowsingEnabledGetter;->get()Z

    #@2c
    move-result v4

    #@2d
    if-eqz v4, :cond_38

    #@2f
    .line 231
    const-string/jumbo v4, "x-hide-urls-from-log"

    #@32
    const-string/jumbo v5, "true"

    #@35
    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    .line 234
    :cond_38
    iget-object v4, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@3a
    invoke-virtual {v4, p1, v3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;Ljava/util/Map;)V

    #@3d
    .line 235
    sget v4, Landroid/webkit/HTML5Audio;->INITIALIZED:I

    #@3f
    iput v4, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@41
    .line 236
    iget-object v4, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@43
    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_46
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_46} :catch_47

    #@46
    .line 242
    .end local v0           #cookieValue:Ljava/lang/String;
    .end local v3           #headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_46
    return-void

    #@47
    .line 237
    :catch_47
    move-exception v2

    #@48
    .line 238
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4b
    move-result v4

    #@4c
    if-le v4, v6, :cond_8c

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const/4 v5, 0x0

    #@54
    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    const-string v5, "..."

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    .line 239
    .local v1, debugUrl:Ljava/lang/String;
    :goto_66
    const-string v4, "HTML5Audio"

    #@68
    new-instance v5, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v6, "couldn\'t load the resource: "

    #@6f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v5

    #@73
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    const-string v6, " exc: "

    #@79
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v5

    #@81
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v5

    #@85
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 240
    invoke-direct {p0}, Landroid/webkit/HTML5Audio;->resetMediaPlayer()V

    #@8b
    goto :goto_46

    #@8c
    .end local v1           #debugUrl:Ljava/lang/String;
    :cond_8c
    move-object v1, p1

    #@8d
    .line 238
    goto :goto_66
.end method

.method private teardown()V
    .registers 2

    #@0
    .prologue
    .line 339
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    #@5
    .line 340
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@8
    .line 341
    sget v0, Landroid/webkit/HTML5Audio;->ERROR:I

    #@a
    iput v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@c
    .line 342
    const/4 v0, 0x0

    #@d
    iput v0, p0, Landroid/webkit/HTML5Audio;->mNativePointer:I

    #@f
    .line 343
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 123
    iget v2, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v2, :pswitch_data_26

    #@5
    .line 135
    :cond_5
    :goto_5
    return-void

    #@6
    .line 126
    :pswitch_6
    :try_start_6
    iget v2, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@8
    sget v3, Landroid/webkit/HTML5Audio;->ERROR:I

    #@a
    if-eq v2, v3, :cond_5

    #@c
    iget-object v2, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@e
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_5

    #@14
    .line 127
    iget-object v2, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@16
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    #@19
    move-result v1

    #@1a
    .line 128
    .local v1, position:I
    iget v2, p0, Landroid/webkit/HTML5Audio;->mNativePointer:I

    #@1c
    invoke-direct {p0, v1, v2}, Landroid/webkit/HTML5Audio;->nativeOnTimeupdate(II)V
    :try_end_1f
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_1f} :catch_20

    #@1f
    goto :goto_5

    #@20
    .line 130
    .end local v1           #position:I
    :catch_20
    move-exception v0

    #@21
    .line 131
    .local v0, e:Ljava/lang/IllegalStateException;
    sget v2, Landroid/webkit/HTML5Audio;->ERROR:I

    #@23
    iput v2, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@25
    goto :goto_5

    #@26
    .line 123
    :pswitch_data_26
    .packed-switch 0x64
        :pswitch_6
    .end packed-switch
.end method

.method public onAudioFocusChange(I)V
    .registers 4
    .parameter "focusChange"

    #@0
    .prologue
    .line 246
    packed-switch p1, :pswitch_data_58

    #@3
    .line 286
    :cond_3
    :goto_3
    :pswitch_3
    return-void

    #@4
    .line 249
    :pswitch_4
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@6
    if-nez v0, :cond_c

    #@8
    .line 250
    invoke-direct {p0}, Landroid/webkit/HTML5Audio;->resetMediaPlayer()V

    #@b
    goto :goto_3

    #@c
    .line 251
    :cond_c
    iget v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@e
    sget v1, Landroid/webkit/HTML5Audio;->ERROR:I

    #@10
    if-eq v0, v1, :cond_3

    #@12
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@14
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_3

    #@1a
    .line 253
    iget v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@1c
    sget v1, Landroid/webkit/HTML5Audio;->PAUSED_TRANSIENT:I

    #@1e
    if-ne v0, v1, :cond_3

    #@20
    .line 254
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@22
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    #@25
    .line 255
    sget v0, Landroid/webkit/HTML5Audio;->STARTED:I

    #@27
    iput v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@29
    goto :goto_3

    #@2a
    .line 263
    :pswitch_2a
    iget v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@2c
    sget v1, Landroid/webkit/HTML5Audio;->ERROR:I

    #@2e
    if-eq v0, v1, :cond_3

    #@30
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@32
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_3

    #@38
    .line 267
    invoke-direct {p0}, Landroid/webkit/HTML5Audio;->pause()V

    #@3b
    goto :goto_3

    #@3c
    .line 276
    :pswitch_3c
    iget v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@3e
    sget v1, Landroid/webkit/HTML5Audio;->ERROR:I

    #@40
    if-eq v0, v1, :cond_3

    #@42
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@44
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@47
    move-result v0

    #@48
    if-eqz v0, :cond_3

    #@4a
    .line 278
    invoke-direct {p0}, Landroid/webkit/HTML5Audio;->pause()V

    #@4d
    .line 279
    iget v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@4f
    sget v1, Landroid/webkit/HTML5Audio;->PAUSED:I

    #@51
    if-ne v0, v1, :cond_3

    #@53
    .line 280
    sget v0, Landroid/webkit/HTML5Audio;->PAUSED_TRANSIENT:I

    #@55
    iput v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@57
    goto :goto_3

    #@58
    .line 246
    :pswitch_data_58
    .packed-switch -0x3
        :pswitch_3c
        :pswitch_3c
        :pswitch_2a
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .registers 4
    .parameter "mp"
    .parameter "percent"

    #@0
    .prologue
    .line 143
    iget v0, p0, Landroid/webkit/HTML5Audio;->mNativePointer:I

    #@2
    invoke-direct {p0, p2, v0}, Landroid/webkit/HTML5Audio;->nativeOnBuffering(II)V

    #@5
    .line 144
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .registers 5
    .parameter "mp"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 148
    sget v0, Landroid/webkit/HTML5Audio;->COMPLETE:I

    #@4
    iput v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@6
    .line 149
    iput-boolean v2, p0, Landroid/webkit/HTML5Audio;->mProcessingOnEnd:Z

    #@8
    .line 150
    iget v0, p0, Landroid/webkit/HTML5Audio;->mNativePointer:I

    #@a
    invoke-direct {p0, v0}, Landroid/webkit/HTML5Audio;->nativeOnEnded(I)V

    #@d
    .line 151
    iput-boolean v1, p0, Landroid/webkit/HTML5Audio;->mProcessingOnEnd:Z

    #@f
    .line 152
    iget-boolean v0, p0, Landroid/webkit/HTML5Audio;->mLoopEnabled:Z

    #@11
    if-ne v0, v2, :cond_1a

    #@13
    .line 153
    iget v0, p0, Landroid/webkit/HTML5Audio;->mNativePointer:I

    #@15
    invoke-direct {p0, v0}, Landroid/webkit/HTML5Audio;->nativeOnRequestPlay(I)V

    #@18
    .line 154
    iput-boolean v1, p0, Landroid/webkit/HTML5Audio;->mLoopEnabled:Z

    #@1a
    .line 156
    :cond_1a
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .registers 5
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    #@0
    .prologue
    .line 160
    sget v0, Landroid/webkit/HTML5Audio;->ERROR:I

    #@2
    iput v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@4
    .line 161
    invoke-direct {p0}, Landroid/webkit/HTML5Audio;->resetMediaPlayer()V

    #@7
    .line 162
    sget v0, Landroid/webkit/HTML5Audio;->IDLE:I

    #@9
    iput v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@b
    .line 163
    const/4 v0, 0x0

    #@c
    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .registers 9
    .parameter "mp"

    #@0
    .prologue
    const-wide/16 v2, 0xfa

    #@2
    const/4 v6, 0x0

    #@3
    .line 168
    sget v0, Landroid/webkit/HTML5Audio;->PREPARED:I

    #@5
    iput v0, p0, Landroid/webkit/HTML5Audio;->mState:I

    #@7
    .line 169
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mTimer:Ljava/util/Timer;

    #@9
    if-eqz v0, :cond_17

    #@b
    .line 170
    iget-object v0, p0, Landroid/webkit/HTML5Audio;->mTimer:Ljava/util/Timer;

    #@d
    new-instance v1, Landroid/webkit/HTML5Audio$TimeupdateTask;

    #@f
    const/4 v4, 0x0

    #@10
    invoke-direct {v1, p0, v4}, Landroid/webkit/HTML5Audio$TimeupdateTask;-><init>(Landroid/webkit/HTML5Audio;Landroid/webkit/HTML5Audio$1;)V

    #@13
    move-wide v4, v2

    #@14
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    #@17
    .line 173
    :cond_17
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    #@1a
    move-result v0

    #@1b
    iget v1, p0, Landroid/webkit/HTML5Audio;->mNativePointer:I

    #@1d
    invoke-direct {p0, v0, v6, v6, v1}, Landroid/webkit/HTML5Audio;->nativeOnPrepared(IIII)V

    #@20
    .line 174
    iget-boolean v0, p0, Landroid/webkit/HTML5Audio;->mAskToPlay:Z

    #@22
    if-eqz v0, :cond_29

    #@24
    .line 175
    iput-boolean v6, p0, Landroid/webkit/HTML5Audio;->mAskToPlay:Z

    #@26
    .line 176
    invoke-direct {p0}, Landroid/webkit/HTML5Audio;->play()V

    #@29
    .line 178
    :cond_29
    return-void
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .registers 4
    .parameter "mp"

    #@0
    .prologue
    .line 182
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/webkit/HTML5Audio;->mNativePointer:I

    #@6
    invoke-direct {p0, v0, v1}, Landroid/webkit/HTML5Audio;->nativeOnTimeupdate(II)V

    #@9
    .line 183
    return-void
.end method
