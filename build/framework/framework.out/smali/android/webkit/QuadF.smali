.class Landroid/webkit/QuadF;
.super Ljava/lang/Object;
.source "QuadF.java"


# instance fields
.field public p1:Landroid/graphics/PointF;

.field public p2:Landroid/graphics/PointF;

.field public p3:Landroid/graphics/PointF;

.field public p4:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 32
    new-instance v0, Landroid/graphics/PointF;

    #@5
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/QuadF;->p1:Landroid/graphics/PointF;

    #@a
    .line 33
    new-instance v0, Landroid/graphics/PointF;

    #@c
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@f
    iput-object v0, p0, Landroid/webkit/QuadF;->p2:Landroid/graphics/PointF;

    #@11
    .line 34
    new-instance v0, Landroid/graphics/PointF;

    #@13
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@16
    iput-object v0, p0, Landroid/webkit/QuadF;->p3:Landroid/graphics/PointF;

    #@18
    .line 35
    new-instance v0, Landroid/graphics/PointF;

    #@1a
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@1d
    iput-object v0, p0, Landroid/webkit/QuadF;->p4:Landroid/graphics/PointF;

    #@1f
    .line 36
    return-void
.end method

.method private static isPointInTriangle(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .registers 18
    .parameter "x0"
    .parameter "y0"
    .parameter "r1"
    .parameter "r2"
    .parameter "r3"

    #@0
    .prologue
    .line 72
    iget v11, p2, Landroid/graphics/PointF;->x:F

    #@2
    move-object/from16 v0, p4

    #@4
    iget v12, v0, Landroid/graphics/PointF;->x:F

    #@6
    sub-float v6, v11, v12

    #@8
    .line 73
    .local v6, x13:F
    iget v11, p2, Landroid/graphics/PointF;->y:F

    #@a
    move-object/from16 v0, p4

    #@c
    iget v12, v0, Landroid/graphics/PointF;->y:F

    #@e
    sub-float v9, v11, v12

    #@10
    .line 74
    .local v9, y13:F
    move-object/from16 v0, p3

    #@12
    iget v11, v0, Landroid/graphics/PointF;->x:F

    #@14
    move-object/from16 v0, p4

    #@16
    iget v12, v0, Landroid/graphics/PointF;->x:F

    #@18
    sub-float v7, v11, v12

    #@1a
    .line 75
    .local v7, x23:F
    move-object/from16 v0, p3

    #@1c
    iget v11, v0, Landroid/graphics/PointF;->y:F

    #@1e
    move-object/from16 v0, p4

    #@20
    iget v12, v0, Landroid/graphics/PointF;->y:F

    #@22
    sub-float v10, v11, v12

    #@24
    .line 76
    .local v10, y23:F
    move-object/from16 v0, p4

    #@26
    iget v11, v0, Landroid/graphics/PointF;->x:F

    #@28
    sub-float v5, p0, v11

    #@2a
    .line 77
    .local v5, x03:F
    move-object/from16 v0, p4

    #@2c
    iget v11, v0, Landroid/graphics/PointF;->y:F

    #@2e
    sub-float v8, p1, v11

    #@30
    .line 79
    .local v8, y03:F
    mul-float v11, v10, v6

    #@32
    mul-float v12, v7, v9

    #@34
    sub-float v1, v11, v12

    #@36
    .line 80
    .local v1, determinant:F
    mul-float v11, v10, v5

    #@38
    mul-float v12, v7, v8

    #@3a
    sub-float/2addr v11, v12

    #@3b
    div-float v2, v11, v1

    #@3d
    .line 81
    .local v2, lambda1:F
    mul-float v11, v6, v8

    #@3f
    mul-float v12, v9, v5

    #@41
    sub-float/2addr v11, v12

    #@42
    div-float v3, v11, v1

    #@44
    .line 82
    .local v3, lambda2:F
    const/high16 v11, 0x3f80

    #@46
    sub-float/2addr v11, v2

    #@47
    sub-float v4, v11, v3

    #@49
    .line 83
    .local v4, lambda3:F
    const/4 v11, 0x0

    #@4a
    cmpl-float v11, v2, v11

    #@4c
    if-ltz v11, :cond_5a

    #@4e
    const/4 v11, 0x0

    #@4f
    cmpl-float v11, v3, v11

    #@51
    if-ltz v11, :cond_5a

    #@53
    const/4 v11, 0x0

    #@54
    cmpl-float v11, v4, v11

    #@56
    if-ltz v11, :cond_5a

    #@58
    const/4 v11, 0x1

    #@59
    :goto_59
    return v11

    #@5a
    :cond_5a
    const/4 v11, 0x0

    #@5b
    goto :goto_59
.end method


# virtual methods
.method public containsPoint(FF)Z
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Landroid/webkit/QuadF;->p1:Landroid/graphics/PointF;

    #@2
    iget-object v1, p0, Landroid/webkit/QuadF;->p2:Landroid/graphics/PointF;

    #@4
    iget-object v2, p0, Landroid/webkit/QuadF;->p3:Landroid/graphics/PointF;

    #@6
    invoke-static {p1, p2, v0, v1, v2}, Landroid/webkit/QuadF;->isPointInTriangle(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_18

    #@c
    iget-object v0, p0, Landroid/webkit/QuadF;->p1:Landroid/graphics/PointF;

    #@e
    iget-object v1, p0, Landroid/webkit/QuadF;->p3:Landroid/graphics/PointF;

    #@10
    iget-object v2, p0, Landroid/webkit/QuadF;->p4:Landroid/graphics/PointF;

    #@12
    invoke-static {p1, p2, v0, v1, v2}, Landroid/webkit/QuadF;->isPointInTriangle(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public offset(FF)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Landroid/webkit/QuadF;->p1:Landroid/graphics/PointF;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->offset(FF)V

    #@5
    .line 40
    iget-object v0, p0, Landroid/webkit/QuadF;->p2:Landroid/graphics/PointF;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->offset(FF)V

    #@a
    .line 41
    iget-object v0, p0, Landroid/webkit/QuadF;->p3:Landroid/graphics/PointF;

    #@c
    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->offset(FF)V

    #@f
    .line 42
    iget-object v0, p0, Landroid/webkit/QuadF;->p4:Landroid/graphics/PointF;

    #@11
    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->offset(FF)V

    #@14
    .line 43
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "QuadF("

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 58
    .local v0, s:Ljava/lang/StringBuilder;
    iget-object v1, p0, Landroid/webkit/QuadF;->p1:Landroid/graphics/PointF;

    #@9
    iget v1, v1, Landroid/graphics/PointF;->x:F

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ","

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget-object v2, p0, Landroid/webkit/QuadF;->p1:Landroid/graphics/PointF;

    #@17
    iget v2, v2, Landroid/graphics/PointF;->y:F

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1c
    .line 59
    const-string v1, " - "

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 60
    iget-object v1, p0, Landroid/webkit/QuadF;->p2:Landroid/graphics/PointF;

    #@23
    iget v1, v1, Landroid/graphics/PointF;->x:F

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, ","

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    iget-object v2, p0, Landroid/webkit/QuadF;->p2:Landroid/graphics/PointF;

    #@31
    iget v2, v2, Landroid/graphics/PointF;->y:F

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@36
    .line 61
    const-string v1, " - "

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    .line 62
    iget-object v1, p0, Landroid/webkit/QuadF;->p3:Landroid/graphics/PointF;

    #@3d
    iget v1, v1, Landroid/graphics/PointF;->x:F

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    const-string v2, ","

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    iget-object v2, p0, Landroid/webkit/QuadF;->p3:Landroid/graphics/PointF;

    #@4b
    iget v2, v2, Landroid/graphics/PointF;->y:F

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@50
    .line 63
    const-string v1, " - "

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 64
    iget-object v1, p0, Landroid/webkit/QuadF;->p4:Landroid/graphics/PointF;

    #@57
    iget v1, v1, Landroid/graphics/PointF;->x:F

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    const-string v2, ","

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    iget-object v2, p0, Landroid/webkit/QuadF;->p4:Landroid/graphics/PointF;

    #@65
    iget v2, v2, Landroid/graphics/PointF;->y:F

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@6a
    .line 65
    const-string v1, ")"

    #@6c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    .line 66
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    return-object v1
.end method
