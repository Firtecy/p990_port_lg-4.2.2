.class public final Landroid/webkit/CacheManager;
.super Ljava/lang/Object;
.source "CacheManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/CacheManager$CacheResult;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field static final HEADER_KEY_IFMODIFIEDSINCE:Ljava/lang/String; = "if-modified-since"

.field static final HEADER_KEY_IFNONEMATCH:Ljava/lang/String; = "if-none-match"

.field private static final LOGTAG:Ljava/lang/String; = "cache"

.field private static mBaseDir:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 46
    const-class v0, Landroid/webkit/CacheManager;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/CacheManager;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    return-void
.end method

.method static synthetic access$000()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 47
    sget-object v0, Landroid/webkit/CacheManager;->mBaseDir:Ljava/io/File;

    #@2
    return-object v0
.end method

.method public static cacheDisabled()Z
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 278
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method static createCacheFile(Ljava/lang/String;ILandroid/net/http/Headers;Ljava/lang/String;Z)Landroid/webkit/CacheManager$CacheResult;
    .registers 6
    .parameter "url"
    .parameter "statusCode"
    .parameter "headers"
    .parameter "mimeType"
    .parameter "forceCache"

    #@0
    .prologue
    .line 384
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public static endCacheTransaction()Z
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 303
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method static getCacheFile(Ljava/lang/String;JLjava/util/Map;)Landroid/webkit/CacheManager$CacheResult;
    .registers 12
    .parameter "url"
    .parameter "postIdentifier"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/webkit/CacheManager$CacheResult;"
        }
    .end annotation

    #@0
    .prologue
    .local p3, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@1
    .line 327
    invoke-static {p0}, Landroid/webkit/CacheManager;->nativeGetCacheResult(Ljava/lang/String;)Landroid/webkit/CacheManager$CacheResult;

    #@4
    move-result-object v1

    #@5
    .line 328
    .local v1, result:Landroid/webkit/CacheManager$CacheResult;
    if-nez v1, :cond_9

    #@7
    move-object v1, v3

    #@8
    .line 367
    .end local v1           #result:Landroid/webkit/CacheManager$CacheResult;
    :cond_8
    :goto_8
    return-object v1

    #@9
    .line 333
    .restart local v1       #result:Landroid/webkit/CacheManager$CacheResult;
    :cond_9
    new-instance v2, Ljava/io/File;

    #@b
    sget-object v4, Landroid/webkit/CacheManager;->mBaseDir:Ljava/io/File;

    #@d
    iget-object v5, v1, Landroid/webkit/CacheManager$CacheResult;->localPath:Ljava/lang/String;

    #@f
    invoke-direct {v2, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@12
    .line 337
    .local v2, src:Ljava/io/File;
    :try_start_12
    new-instance v4, Ljava/io/FileInputStream;

    #@14
    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@17
    iput-object v4, v1, Landroid/webkit/CacheManager$CacheResult;->inStream:Ljava/io/InputStream;
    :try_end_19
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_19} :catch_37

    #@19
    .line 348
    if-eqz p3, :cond_8

    #@1b
    iget-wide v4, v1, Landroid/webkit/CacheManager$CacheResult;->expires:J

    #@1d
    const-wide/16 v6, 0x0

    #@1f
    cmp-long v4, v4, v6

    #@21
    if-ltz v4, :cond_8

    #@23
    iget-wide v4, v1, Landroid/webkit/CacheManager$CacheResult;->expires:J

    #@25
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@28
    move-result-wide v6

    #@29
    cmp-long v4, v4, v6

    #@2b
    if-gtz v4, :cond_8

    #@2d
    .line 350
    iget-object v4, v1, Landroid/webkit/CacheManager$CacheResult;->lastModified:Ljava/lang/String;

    #@2f
    if-nez v4, :cond_52

    #@31
    iget-object v4, v1, Landroid/webkit/CacheManager$CacheResult;->etag:Ljava/lang/String;

    #@33
    if-nez v4, :cond_52

    #@35
    move-object v1, v3

    #@36
    .line 351
    goto :goto_8

    #@37
    .line 338
    :catch_37
    move-exception v0

    #@38
    .line 339
    .local v0, e:Ljava/io/FileNotFoundException;
    const-string v4, "cache"

    #@3a
    new-instance v5, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v6, "getCacheFile(): Failed to open file: "

    #@41
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    move-object v1, v3

    #@51
    .line 342
    goto :goto_8

    #@52
    .line 355
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :cond_52
    iget-object v3, v1, Landroid/webkit/CacheManager$CacheResult;->etag:Ljava/lang/String;

    #@54
    if-eqz v3, :cond_5d

    #@56
    .line 356
    const-string v3, "if-none-match"

    #@58
    iget-object v4, v1, Landroid/webkit/CacheManager$CacheResult;->etag:Ljava/lang/String;

    #@5a
    invoke-interface {p3, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5d
    .line 358
    :cond_5d
    iget-object v3, v1, Landroid/webkit/CacheManager$CacheResult;->lastModified:Ljava/lang/String;

    #@5f
    if-eqz v3, :cond_8

    #@61
    .line 359
    const-string v3, "if-modified-since"

    #@63
    iget-object v4, v1, Landroid/webkit/CacheManager$CacheResult;->lastModified:Ljava/lang/String;

    #@65
    invoke-interface {p3, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@68
    goto :goto_8
.end method

.method public static getCacheFile(Ljava/lang/String;Ljava/util/Map;)Landroid/webkit/CacheManager$CacheResult;
    .registers 4
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/webkit/CacheManager$CacheResult;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 322
    .local p1, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-wide/16 v0, 0x0

    #@2
    invoke-static {p0, v0, v1, p1}, Landroid/webkit/CacheManager;->getCacheFile(Ljava/lang/String;JLjava/util/Map;)Landroid/webkit/CacheManager$CacheResult;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static getCacheFileBaseDir()Ljava/io/File;
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 267
    sget-object v0, Landroid/webkit/CacheManager;->mBaseDir:Ljava/io/File;

    #@2
    return-object v0
.end method

.method static init(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 251
    new-instance v0, Ljava/io/File;

    #@2
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    const-string/jumbo v2, "webviewCacheChromiumStaging"

    #@9
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@c
    sput-object v0, Landroid/webkit/CacheManager;->mBaseDir:Ljava/io/File;

    #@e
    .line 252
    sget-object v0, Landroid/webkit/CacheManager;->mBaseDir:Ljava/io/File;

    #@10
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_1b

    #@16
    .line 253
    sget-object v0, Landroid/webkit/CacheManager;->mBaseDir:Ljava/io/File;

    #@18
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@1b
    .line 255
    :cond_1b
    return-void
.end method

.method private static native nativeGetCacheResult(Ljava/lang/String;)Landroid/webkit/CacheManager$CacheResult;
.end method

.method static removeAllCacheFiles()Z
    .registers 2

    #@0
    .prologue
    .line 435
    new-instance v0, Landroid/webkit/CacheManager$1;

    #@2
    invoke-direct {v0}, Landroid/webkit/CacheManager$1;-><init>()V

    #@5
    .line 454
    .local v0, clearCache:Ljava/lang/Runnable;
    new-instance v1, Ljava/lang/Thread;

    #@7
    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@a
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@d
    .line 455
    const/4 v1, 0x1

    #@e
    return v1
.end method

.method static saveCacheFile(Ljava/lang/String;JLandroid/webkit/CacheManager$CacheResult;)V
    .registers 6
    .parameter "url"
    .parameter "postIdentifier"
    .parameter "cacheRet"

    #@0
    .prologue
    .line 403
    :try_start_0
    iget-object v1, p3, Landroid/webkit/CacheManager$CacheResult;->outStream:Ljava/io/OutputStream;

    #@2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_f

    #@5
    .line 425
    sget-boolean v1, Landroid/webkit/CacheManager;->$assertionsDisabled:Z

    #@7
    if-nez v1, :cond_10

    #@9
    new-instance v1, Ljava/lang/AssertionError;

    #@b
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@e
    throw v1

    #@f
    .line 404
    :catch_f
    move-exception v0

    #@10
    .line 426
    :cond_10
    return-void
.end method

.method public static saveCacheFile(Ljava/lang/String;Landroid/webkit/CacheManager$CacheResult;)V
    .registers 4
    .parameter "url"
    .parameter "cacheResult"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 397
    const-wide/16 v0, 0x0

    #@2
    invoke-static {p0, v0, v1, p1}, Landroid/webkit/CacheManager;->saveCacheFile(Ljava/lang/String;JLandroid/webkit/CacheManager$CacheResult;)V

    #@5
    .line 398
    return-void
.end method

.method public static startCacheTransaction()Z
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 291
    const/4 v0, 0x0

    #@1
    return v0
.end method
