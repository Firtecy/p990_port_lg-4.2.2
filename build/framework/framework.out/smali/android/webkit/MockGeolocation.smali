.class public final Landroid/webkit/MockGeolocation;
.super Ljava/lang/Object;
.source "MockGeolocation.java"


# instance fields
.field private mWebViewCore:Landroid/webkit/WebViewCore;


# direct methods
.method public constructor <init>(Landroid/webkit/WebViewCore;)V
    .registers 2
    .parameter "webViewCore"

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 27
    iput-object p1, p0, Landroid/webkit/MockGeolocation;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@5
    .line 28
    return-void
.end method

.method private static native nativeSetError(Landroid/webkit/WebViewCore;ILjava/lang/String;)V
.end method

.method private static native nativeSetPermission(Landroid/webkit/WebViewCore;Z)V
.end method

.method private static native nativeSetPosition(Landroid/webkit/WebViewCore;DDD)V
.end method

.method private static native nativeSetUseMock(Landroid/webkit/WebViewCore;)V
.end method


# virtual methods
.method public setError(ILjava/lang/String;)V
    .registers 4
    .parameter "code"
    .parameter "message"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/webkit/MockGeolocation;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    invoke-static {v0, p1, p2}, Landroid/webkit/MockGeolocation;->nativeSetError(Landroid/webkit/WebViewCore;ILjava/lang/String;)V

    #@5
    .line 51
    return-void
.end method

.method public setPermission(Z)V
    .registers 3
    .parameter "allow"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Landroid/webkit/MockGeolocation;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    invoke-static {v0, p1}, Landroid/webkit/MockGeolocation;->nativeSetPermission(Landroid/webkit/WebViewCore;Z)V

    #@5
    .line 56
    return-void
.end method

.method public setPosition(DDD)V
    .registers 14
    .parameter "latitude"
    .parameter "longitude"
    .parameter "accuracy"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Landroid/webkit/MockGeolocation;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    move-wide v1, p1

    #@3
    move-wide v3, p3

    #@4
    move-wide v5, p5

    #@5
    invoke-static/range {v0 .. v6}, Landroid/webkit/MockGeolocation;->nativeSetPosition(Landroid/webkit/WebViewCore;DDD)V

    #@8
    .line 43
    return-void
.end method

.method public setUseMock()V
    .registers 2

    #@0
    .prologue
    .line 34
    iget-object v0, p0, Landroid/webkit/MockGeolocation;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    invoke-static {v0}, Landroid/webkit/MockGeolocation;->nativeSetUseMock(Landroid/webkit/WebViewCore;)V

    #@5
    .line 35
    return-void
.end method
