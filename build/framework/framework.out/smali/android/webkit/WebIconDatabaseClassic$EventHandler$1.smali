.class Landroid/webkit/WebIconDatabaseClassic$EventHandler$1;
.super Landroid/os/Handler;
.source "WebIconDatabaseClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/WebIconDatabaseClassic$EventHandler;->createHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/WebIconDatabaseClassic$EventHandler;


# direct methods
.method constructor <init>(Landroid/webkit/WebIconDatabaseClassic$EventHandler;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 83
    iput-object p1, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler$1;->this$0:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 88
    iget v2, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v2, :pswitch_data_42

    #@5
    .line 119
    :goto_5
    return-void

    #@6
    .line 90
    :pswitch_6
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v2, Ljava/lang/String;

    #@a
    invoke-static {v2}, Landroid/webkit/WebIconDatabaseClassic;->access$100(Ljava/lang/String;)V

    #@d
    goto :goto_5

    #@e
    .line 94
    :pswitch_e
    invoke-static {}, Landroid/webkit/WebIconDatabaseClassic;->access$200()V

    #@11
    goto :goto_5

    #@12
    .line 98
    :pswitch_12
    invoke-static {}, Landroid/webkit/WebIconDatabaseClassic;->access$300()V

    #@15
    goto :goto_5

    #@16
    .line 102
    :pswitch_16
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18
    check-cast v0, Landroid/webkit/WebIconDatabase$IconListener;

    #@1a
    .line 103
    .local v0, l:Landroid/webkit/WebIconDatabase$IconListener;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1d
    move-result-object v2

    #@1e
    const-string/jumbo v3, "url"

    #@21
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    .line 104
    .local v1, url:Ljava/lang/String;
    iget-object v2, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler$1;->this$0:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@27
    invoke-static {v2, v1, v0}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$400(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V

    #@2a
    goto :goto_5

    #@2b
    .line 108
    .end local v0           #l:Landroid/webkit/WebIconDatabase$IconListener;
    .end local v1           #url:Ljava/lang/String;
    :pswitch_2b
    iget-object v2, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler$1;->this$0:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@2d
    invoke-static {v2, p1}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$500(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Landroid/os/Message;)V

    #@30
    goto :goto_5

    #@31
    .line 112
    :pswitch_31
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@33
    check-cast v2, Ljava/lang/String;

    #@35
    invoke-static {v2}, Landroid/webkit/WebIconDatabaseClassic;->access$600(Ljava/lang/String;)V

    #@38
    goto :goto_5

    #@39
    .line 116
    :pswitch_39
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3b
    check-cast v2, Ljava/lang/String;

    #@3d
    invoke-static {v2}, Landroid/webkit/WebIconDatabaseClassic;->access$700(Ljava/lang/String;)V

    #@40
    goto :goto_5

    #@41
    .line 88
    nop

    #@42
    :pswitch_data_42
    .packed-switch 0x0
        :pswitch_6
        :pswitch_e
        :pswitch_12
        :pswitch_16
        :pswitch_31
        :pswitch_39
        :pswitch_2b
    .end packed-switch
.end method
