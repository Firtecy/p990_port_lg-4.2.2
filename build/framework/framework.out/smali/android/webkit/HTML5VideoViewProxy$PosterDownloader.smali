.class final Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;
.super Ljava/lang/Object;
.source "HTML5VideoViewProxy.java"

# interfaces
.implements Landroid/net/http/EventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/HTML5VideoViewProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PosterDownloader"
.end annotation


# static fields
.field private static mQueueRefCount:I

.field private static mRequestQueue:Landroid/net/http/RequestQueue;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mHeaders:Landroid/net/http/Headers;

.field private mPosterBytes:Ljava/io/ByteArrayOutputStream;

.field private final mProxy:Landroid/webkit/HTML5VideoViewProxy;

.field private mRequestHandle:Landroid/net/http/RequestHandle;

.field private mStatusCode:I

.field private mUrl:Ljava/net/URL;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 672
    const/4 v0, 0x0

    #@1
    sput v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mQueueRefCount:I

    #@3
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 5
    .parameter "url"
    .parameter "proxy"

    #@0
    .prologue
    .line 688
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 690
    :try_start_3
    new-instance v1, Ljava/net/URL;

    #@5
    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@8
    iput-object v1, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mUrl:Ljava/net/URL;
    :try_end_a
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_a} :catch_14

    #@a
    .line 694
    :goto_a
    iput-object p2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@c
    .line 695
    new-instance v1, Landroid/os/Handler;

    #@e
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@11
    iput-object v1, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mHandler:Landroid/os/Handler;

    #@13
    .line 696
    return-void

    #@14
    .line 691
    :catch_14
    move-exception v0

    #@15
    .line 692
    .local v0, e:Ljava/net/MalformedURLException;
    const/4 v1, 0x0

    #@16
    iput-object v1, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mUrl:Ljava/net/URL;

    #@18
    goto :goto_a
.end method

.method static synthetic access$1100(Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;)Landroid/net/http/RequestHandle;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 669
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mRequestHandle:Landroid/net/http/RequestHandle;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;)Ljava/net/URL;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 669
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mUrl:Ljava/net/URL;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 669
    iget v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mStatusCode:I

    #@2
    return v0
.end method

.method private cleanup()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 792
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mPosterBytes:Ljava/io/ByteArrayOutputStream;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 794
    :try_start_5
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mPosterBytes:Ljava/io/ByteArrayOutputStream;

    #@7
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_d
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_a} :catch_11

    #@a
    .line 798
    :goto_a
    iput-object v1, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mPosterBytes:Ljava/io/ByteArrayOutputStream;

    #@c
    .line 801
    :cond_c
    return-void

    #@d
    .line 798
    :catchall_d
    move-exception v0

    #@e
    iput-object v1, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mPosterBytes:Ljava/io/ByteArrayOutputStream;

    #@10
    throw v0

    #@11
    .line 795
    :catch_11
    move-exception v0

    #@12
    goto :goto_a
.end method

.method private releaseQueue()V
    .registers 2

    #@0
    .prologue
    .line 812
    sget v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mQueueRefCount:I

    #@2
    if-nez v0, :cond_5

    #@4
    .line 819
    :cond_4
    :goto_4
    return-void

    #@5
    .line 815
    :cond_5
    sget v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mQueueRefCount:I

    #@7
    add-int/lit8 v0, v0, -0x1

    #@9
    sput v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mQueueRefCount:I

    #@b
    if-nez v0, :cond_4

    #@d
    .line 816
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mRequestQueue:Landroid/net/http/RequestQueue;

    #@f
    invoke-virtual {v0}, Landroid/net/http/RequestQueue;->shutdown()V

    #@12
    .line 817
    const/4 v0, 0x0

    #@13
    sput-object v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mRequestQueue:Landroid/net/http/RequestQueue;

    #@15
    goto :goto_4
.end method

.method private retainQueue()V
    .registers 3

    #@0
    .prologue
    .line 805
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mRequestQueue:Landroid/net/http/RequestQueue;

    #@2
    if-nez v0, :cond_11

    #@4
    .line 806
    new-instance v0, Landroid/net/http/RequestQueue;

    #@6
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@8
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@b
    move-result-object v1

    #@c
    invoke-direct {v0, v1}, Landroid/net/http/RequestQueue;-><init>(Landroid/content/Context;)V

    #@f
    sput-object v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mRequestQueue:Landroid/net/http/RequestQueue;

    #@11
    .line 808
    :cond_11
    sget v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mQueueRefCount:I

    #@13
    add-int/lit8 v0, v0, 0x1

    #@15
    sput v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mQueueRefCount:I

    #@17
    .line 809
    return-void
.end method


# virtual methods
.method public cancelAndReleaseQueue()V
    .registers 2

    #@0
    .prologue
    .line 716
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mRequestHandle:Landroid/net/http/RequestHandle;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 717
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mRequestHandle:Landroid/net/http/RequestHandle;

    #@6
    invoke-virtual {v0}, Landroid/net/http/RequestHandle;->cancel()V

    #@9
    .line 718
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mRequestHandle:Landroid/net/http/RequestHandle;

    #@c
    .line 720
    :cond_c
    invoke-direct {p0}, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->releaseQueue()V

    #@f
    .line 721
    return-void
.end method

.method public certificate(Landroid/net/http/SslCertificate;)V
    .registers 2
    .parameter "certificate"

    #@0
    .prologue
    .line 777
    return-void
.end method

.method public data([BI)V
    .registers 5
    .parameter "data"
    .parameter "len"

    #@0
    .prologue
    .line 738
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mPosterBytes:Ljava/io/ByteArrayOutputStream;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 739
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@6
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@9
    iput-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mPosterBytes:Ljava/io/ByteArrayOutputStream;

    #@b
    .line 741
    :cond_b
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mPosterBytes:Ljava/io/ByteArrayOutputStream;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-virtual {v0, p1, v1, p2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@11
    .line 742
    return-void
.end method

.method public endData()V
    .registers 6

    #@0
    .prologue
    .line 746
    iget v2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mStatusCode:I

    #@2
    const/16 v3, 0xc8

    #@4
    if-ne v2, v3, :cond_28

    #@6
    .line 747
    iget-object v2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mPosterBytes:Ljava/io/ByteArrayOutputStream;

    #@8
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->size()I

    #@b
    move-result v2

    #@c
    if-lez v2, :cond_24

    #@e
    .line 748
    iget-object v2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mPosterBytes:Ljava/io/ByteArrayOutputStream;

    #@10
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@13
    move-result-object v2

    #@14
    const/4 v3, 0x0

    #@15
    iget-object v4, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mPosterBytes:Ljava/io/ByteArrayOutputStream;

    #@17
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    #@1a
    move-result v4

    #@1b
    invoke-static {v2, v3, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    #@1e
    move-result-object v1

    #@1f
    .line 750
    .local v1, poster:Landroid/graphics/Bitmap;
    iget-object v2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@21
    invoke-static {v2, v1}, Landroid/webkit/HTML5VideoViewProxy;->access$1000(Landroid/webkit/HTML5VideoViewProxy;Landroid/graphics/Bitmap;)V

    #@24
    .line 752
    .end local v1           #poster:Landroid/graphics/Bitmap;
    :cond_24
    invoke-direct {p0}, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->cleanup()V

    #@27
    .line 772
    :cond_27
    :goto_27
    return-void

    #@28
    .line 753
    :cond_28
    iget v2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mStatusCode:I

    #@2a
    const/16 v3, 0x12c

    #@2c
    if-lt v2, v3, :cond_27

    #@2e
    iget v2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mStatusCode:I

    #@30
    const/16 v3, 0x190

    #@32
    if-ge v2, v3, :cond_27

    #@34
    .line 756
    :try_start_34
    new-instance v2, Ljava/net/URL;

    #@36
    iget-object v3, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mHeaders:Landroid/net/http/Headers;

    #@38
    invoke-virtual {v3}, Landroid/net/http/Headers;->getLocation()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@3f
    iput-object v2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mUrl:Ljava/net/URL;
    :try_end_41
    .catch Ljava/net/MalformedURLException; {:try_start_34 .. :try_end_41} :catch_50

    #@41
    .line 760
    :goto_41
    iget-object v2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mUrl:Ljava/net/URL;

    #@43
    if-eqz v2, :cond_27

    #@45
    .line 761
    iget-object v2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mHandler:Landroid/os/Handler;

    #@47
    new-instance v3, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader$1;

    #@49
    invoke-direct {v3, p0}, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader$1;-><init>(Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;)V

    #@4c
    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@4f
    goto :goto_27

    #@50
    .line 757
    :catch_50
    move-exception v0

    #@51
    .line 758
    .local v0, e:Ljava/net/MalformedURLException;
    const/4 v2, 0x0

    #@52
    iput-object v2, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mUrl:Ljava/net/URL;

    #@54
    goto :goto_41
.end method

.method public error(ILjava/lang/String;)V
    .registers 3
    .parameter "id"
    .parameter "description"

    #@0
    .prologue
    .line 781
    invoke-direct {p0}, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->cleanup()V

    #@3
    .line 782
    return-void
.end method

.method public handleSslErrorRequest(Landroid/net/http/SslError;)Z
    .registers 3
    .parameter "error"

    #@0
    .prologue
    .line 788
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public headers(Landroid/net/http/Headers;)V
    .registers 2
    .parameter "headers"

    #@0
    .prologue
    .line 733
    iput-object p1, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mHeaders:Landroid/net/http/Headers;

    #@2
    .line 734
    return-void
.end method

.method public start()V
    .registers 9

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 699
    invoke-direct {p0}, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->retainQueue()V

    #@4
    .line 701
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mUrl:Ljava/net/URL;

    #@6
    if-nez v0, :cond_9

    #@8
    .line 713
    :cond_8
    :goto_8
    return-void

    #@9
    .line 708
    :cond_9
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mUrl:Ljava/net/URL;

    #@b
    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    #@e
    move-result-object v7

    #@f
    .line 709
    .local v7, protocol:Ljava/lang/String;
    const-string v0, "http"

    #@11
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_1f

    #@17
    const-string v0, "https"

    #@19
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_8

    #@1f
    .line 710
    :cond_1f
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mRequestQueue:Landroid/net/http/RequestQueue;

    #@21
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mUrl:Ljava/net/URL;

    #@23
    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    const-string v2, "GET"

    #@29
    const/4 v6, 0x0

    #@2a
    move-object v4, p0

    #@2b
    move-object v5, v3

    #@2c
    invoke-virtual/range {v0 .. v6}, Landroid/net/http/RequestQueue;->queueRequest(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Landroid/net/http/EventHandler;Ljava/io/InputStream;I)Landroid/net/http/RequestHandle;

    #@2f
    move-result-object v0

    #@30
    iput-object v0, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mRequestHandle:Landroid/net/http/RequestHandle;

    #@32
    goto :goto_8
.end method

.method public status(IIILjava/lang/String;)V
    .registers 5
    .parameter "major_version"
    .parameter "minor_version"
    .parameter "code"
    .parameter "reason_phrase"

    #@0
    .prologue
    .line 728
    iput p3, p0, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->mStatusCode:I

    #@2
    .line 729
    return-void
.end method
