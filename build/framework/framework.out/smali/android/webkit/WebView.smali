.class public Landroid/webkit/WebView;
.super Landroid/widget/AbsoluteLayout;
.source "WebView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;
.implements Landroid/view/ViewDebug$HierarchyHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebView$PrivateAccess;,
        Landroid/webkit/WebView$HitTestResult;,
        Landroid/webkit/WebView$PictureListener;,
        Landroid/webkit/WebView$FindListener;,
        Landroid/webkit/WebView$WebViewTransport;
    }
.end annotation


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "webview_proxy"

.field public static final SCHEME_GEO:Ljava/lang/String; = "geo:0,0?q="

.field public static final SCHEME_MAILTO:Ljava/lang/String; = "mailto:"

.field public static final SCHEME_TEL:Ljava/lang/String; = "tel:"


# instance fields
.field private mProvider:Landroid/webkit/WebViewProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 440
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 441
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 450
    const v0, 0x1010085

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 451
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 461
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    #@4
    .line 462
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/util/Map;Z)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"
    .parameter
    .parameter "privateBrowsing"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 504
    .local p4, javaScriptInterfaces:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AbsoluteLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 505
    if-nez p1, :cond_d

    #@5
    .line 506
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "Invalid context argument"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 508
    :cond_d
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@10
    .line 510
    invoke-direct {p0}, Landroid/webkit/WebView;->ensureProviderCreated()V

    #@13
    .line 511
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@15
    invoke-interface {v0, p4, p5}, Landroid/webkit/WebViewProvider;->init(Ljava/util/Map;Z)V

    #@18
    .line 512
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V
    .registers 11
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"
    .parameter "privateBrowsing"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 481
    const/4 v4, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move v5, p4

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/util/Map;Z)V

    #@9
    .line 482
    return-void
.end method

.method static synthetic access$001(Landroid/webkit/WebView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 262
    invoke-super {p0}, Landroid/view/View;->getScrollBarStyle()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1001(Landroid/webkit/WebView;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 262
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@3
    return-void
.end method

.method static synthetic access$101(Landroid/webkit/WebView;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 262
    invoke-super {p0, p1, p2}, Landroid/view/View;->scrollTo(II)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Landroid/webkit/WebView;IIIIIIIIZ)Z
    .registers 11
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"
    .parameter "x8"
    .parameter "x9"

    #@0
    .prologue
    .line 262
    invoke-virtual/range {p0 .. p9}, Landroid/webkit/WebView;->overScrollBy(IIIIIIIIZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1200(Landroid/webkit/WebView;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 262
    invoke-virtual {p0, p1}, Landroid/webkit/WebView;->awakenScrollBars(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1300(Landroid/webkit/WebView;IZ)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 262
    invoke-virtual {p0, p1, p2}, Landroid/webkit/WebView;->awakenScrollBars(IZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1400(Landroid/webkit/WebView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 262
    invoke-virtual {p0}, Landroid/webkit/WebView;->getVerticalScrollFactor()F

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1500(Landroid/webkit/WebView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 262
    invoke-virtual {p0}, Landroid/webkit/WebView;->getHorizontalScrollFactor()F

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1600(Landroid/webkit/WebView;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 262
    invoke-virtual {p0, p1, p2}, Landroid/webkit/WebView;->setMeasuredDimension(II)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Landroid/webkit/WebView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 262
    invoke-virtual {p0}, Landroid/webkit/WebView;->getHorizontalScrollbarHeight()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1802(Landroid/webkit/WebView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 262
    iput p1, p0, Landroid/view/View;->mScrollX:I

    #@2
    return p1
.end method

.method static synthetic access$1902(Landroid/webkit/WebView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 262
    iput p1, p0, Landroid/view/View;->mScrollY:I

    #@2
    return p1
.end method

.method static synthetic access$201(Landroid/webkit/WebView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 262
    invoke-super {p0}, Landroid/view/View;->computeScroll()V

    #@3
    return-void
.end method

.method static synthetic access$301(Landroid/webkit/WebView;Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 262
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$401(Landroid/webkit/WebView;ILandroid/os/Bundle;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 262
    invoke-super {p0, p1, p2}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$501(Landroid/webkit/WebView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 262
    invoke-super {p0}, Landroid/view/View;->performLongClick()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$601(Landroid/webkit/WebView;IIII)Z
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 262
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->setFrame(IIII)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$701(Landroid/webkit/WebView;Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 262
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$801(Landroid/webkit/WebView;Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 262
    invoke-super {p0, p1}, Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$901(Landroid/webkit/WebView;ILandroid/graphics/Rect;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 262
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestFocus(ILandroid/graphics/Rect;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static checkThread()V
    .registers 3

    #@0
    .prologue
    .line 2005
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3
    move-result-object v1

    #@4
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@7
    move-result-object v2

    #@8
    if-eq v1, v2, :cond_49

    #@a
    .line 2006
    new-instance v0, Ljava/lang/Throwable;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Warning: A WebView method was called on thread \'"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, "\'. "

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, "All WebView methods must be called on the UI thread. "

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    const-string v2, "Future versions of WebView may not support use on other threads."

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    #@3c
    .line 2011
    .local v0, throwable:Ljava/lang/Throwable;
    const-string/jumbo v1, "webview_proxy"

    #@3f
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 2012
    invoke-static {v0}, Landroid/os/StrictMode;->onWebViewMethodCalledOnWrongThread(Ljava/lang/Throwable;)V

    #@49
    .line 2014
    :cond_49
    return-void
.end method

.method public static disablePlatformNotifications()V
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 680
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 681
    invoke-static {}, Landroid/webkit/WebView;->getFactory()Landroid/webkit/WebViewFactoryProvider;

    #@6
    move-result-object v0

    #@7
    invoke-interface {v0}, Landroid/webkit/WebViewFactoryProvider;->getStatics()Landroid/webkit/WebViewFactoryProvider$Statics;

    #@a
    move-result-object v0

    #@b
    const/4 v1, 0x0

    #@c
    invoke-interface {v0, v1}, Landroid/webkit/WebViewFactoryProvider$Statics;->setPlatformNotificationsEnabled(Z)V

    #@f
    .line 682
    return-void
.end method

.method public static enablePlatformNotifications()V
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 667
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 668
    invoke-static {}, Landroid/webkit/WebView;->getFactory()Landroid/webkit/WebViewFactoryProvider;

    #@6
    move-result-object v0

    #@7
    invoke-interface {v0}, Landroid/webkit/WebViewFactoryProvider;->getStatics()Landroid/webkit/WebViewFactoryProvider$Statics;

    #@a
    move-result-object v0

    #@b
    const/4 v1, 0x1

    #@c
    invoke-interface {v0, v1}, Landroid/webkit/WebViewFactoryProvider$Statics;->setPlatformNotificationsEnabled(Z)V

    #@f
    .line 669
    return-void
.end method

.method private ensureProviderCreated()V
    .registers 3

    #@0
    .prologue
    .line 1989
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1990
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    if-nez v0, :cond_16

    #@7
    .line 1993
    invoke-static {}, Landroid/webkit/WebView;->getFactory()Landroid/webkit/WebViewFactoryProvider;

    #@a
    move-result-object v0

    #@b
    new-instance v1, Landroid/webkit/WebView$PrivateAccess;

    #@d
    invoke-direct {v1, p0}, Landroid/webkit/WebView$PrivateAccess;-><init>(Landroid/webkit/WebView;)V

    #@10
    invoke-interface {v0, p0, v1}, Landroid/webkit/WebViewFactoryProvider;->createWebView(Landroid/webkit/WebView;Landroid/webkit/WebView$PrivateAccess;)Landroid/webkit/WebViewProvider;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@16
    .line 1995
    :cond_16
    return-void
.end method

.method public static findAddress(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "addr"

    #@0
    .prologue
    .line 1432
    invoke-static {}, Landroid/webkit/WebView;->getFactory()Landroid/webkit/WebViewFactoryProvider;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Landroid/webkit/WebViewFactoryProvider;->getStatics()Landroid/webkit/WebViewFactoryProvider$Statics;

    #@7
    move-result-object v0

    #@8
    invoke-interface {v0, p0}, Landroid/webkit/WebViewFactoryProvider$Statics;->findAddress(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method private static declared-synchronized getFactory()Landroid/webkit/WebViewFactoryProvider;
    .registers 2

    #@0
    .prologue
    .line 2000
    const-class v1, Landroid/webkit/WebView;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@6
    .line 2001
    invoke-static {}, Landroid/webkit/WebViewFactory;->getProvider()Landroid/webkit/WebViewFactoryProvider;
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_c

    #@9
    move-result-object v0

    #@a
    monitor-exit v1

    #@b
    return-object v0

    #@c
    .line 2000
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1

    #@e
    throw v0
.end method

.method public static declared-synchronized getPluginList()Landroid/webkit/PluginList;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1601
    const-class v1, Landroid/webkit/WebView;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@6
    .line 1602
    new-instance v0, Landroid/webkit/PluginList;

    #@8
    invoke-direct {v0}, Landroid/webkit/PluginList;-><init>()V
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_d

    #@b
    monitor-exit v1

    #@c
    return-object v0

    #@d
    .line 1601
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1

    #@f
    throw v0
.end method

.method public static setWebViewProxyEnable(Z)V
    .registers 2
    .parameter "enable"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 692
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 693
    invoke-static {}, Landroid/webkit/WebView;->getFactory()Landroid/webkit/WebViewFactoryProvider;

    #@6
    move-result-object v0

    #@7
    invoke-interface {v0}, Landroid/webkit/WebViewFactoryProvider;->getStatics()Landroid/webkit/WebViewFactoryProvider$Statics;

    #@a
    move-result-object v0

    #@b
    invoke-interface {v0, p0}, Landroid/webkit/WebViewFactoryProvider$Statics;->setWebViewProxyEnable(Z)V

    #@e
    .line 694
    return-void
.end method


# virtual methods
.method public addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 4
    .parameter "object"
    .parameter "name"

    #@0
    .prologue
    .line 1564
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1565
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    #@8
    .line 1566
    return-void
.end method

.method public canGoBack()Z
    .registers 2

    #@0
    .prologue
    .line 926
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 927
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->canGoBack()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public canGoBackOrForward(I)Z
    .registers 3
    .parameter "steps"

    #@0
    .prologue
    .line 964
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 965
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->canGoBackOrForward(I)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public canGoForward()Z
    .registers 2

    #@0
    .prologue
    .line 944
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 945
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->canGoForward()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public canZoomIn()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1698
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1699
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->canZoomIn()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public canZoomOut()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1713
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1714
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->canZoomOut()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public capturePicture()Landroid/graphics/Picture;
    .registers 2

    #@0
    .prologue
    .line 1035
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1036
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->capturePicture()Landroid/graphics/Picture;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public clearCache(Z)V
    .registers 3
    .parameter "includeDiskFiles"

    #@0
    .prologue
    .line 1292
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1293
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->clearCache(Z)V

    #@8
    .line 1294
    return-void
.end method

.method public clearFormData()V
    .registers 2

    #@0
    .prologue
    .line 1303
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1304
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->clearFormData()V

    #@8
    .line 1305
    return-void
.end method

.method public clearHistory()V
    .registers 2

    #@0
    .prologue
    .line 1311
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1312
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->clearHistory()V

    #@8
    .line 1313
    return-void
.end method

.method public clearMatches()V
    .registers 2

    #@0
    .prologue
    .line 1440
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1441
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->clearMatches()V

    #@8
    .line 1442
    return-void
.end method

.method public clearSslPreferences()V
    .registers 2

    #@0
    .prologue
    .line 1320
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1321
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->clearSslPreferences()V

    #@8
    .line 1322
    return-void
.end method

.method public clearView()V
    .registers 2

    #@0
    .prologue
    .line 1016
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1017
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->clearView()V

    #@8
    .line 1018
    return-void
.end method

.method public closedFindDialog()Z
    .registers 2

    #@0
    .prologue
    .line 1783
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected computeHorizontalScrollOffset()I
    .registers 2

    #@0
    .prologue
    .line 2063
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getScrollDelegate()Landroid/webkit/WebViewProvider$ScrollDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Landroid/webkit/WebViewProvider$ScrollDelegate;->computeHorizontalScrollOffset()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method protected computeHorizontalScrollRange()I
    .registers 2

    #@0
    .prologue
    .line 2058
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getScrollDelegate()Landroid/webkit/WebViewProvider$ScrollDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Landroid/webkit/WebViewProvider$ScrollDelegate;->computeHorizontalScrollRange()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public computeScroll()V
    .registers 2

    #@0
    .prologue
    .line 2083
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getScrollDelegate()Landroid/webkit/WebViewProvider$ScrollDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Landroid/webkit/WebViewProvider$ScrollDelegate;->computeScroll()V

    #@9
    .line 2084
    return-void
.end method

.method protected computeVerticalScrollExtent()I
    .registers 2

    #@0
    .prologue
    .line 2078
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getScrollDelegate()Landroid/webkit/WebViewProvider$ScrollDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Landroid/webkit/WebViewProvider$ScrollDelegate;->computeVerticalScrollExtent()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method protected computeVerticalScrollOffset()I
    .registers 2

    #@0
    .prologue
    .line 2073
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getScrollDelegate()Landroid/webkit/WebViewProvider$ScrollDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Landroid/webkit/WebViewProvider$ScrollDelegate;->computeVerticalScrollOffset()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method protected computeVerticalScrollRange()I
    .registers 2

    #@0
    .prologue
    .line 2068
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getScrollDelegate()Landroid/webkit/WebViewProvider$ScrollDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Landroid/webkit/WebViewProvider$ScrollDelegate;->computeVerticalScrollRange()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public copyBackForwardList()Landroid/webkit/WebBackForwardList;
    .registers 2

    #@0
    .prologue
    .line 1333
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1334
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public debugDump()V
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1793
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1794
    return-void
.end method

.method public destroy()V
    .registers 2

    #@0
    .prologue
    .line 654
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 655
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->destroy()V

    #@8
    .line 656
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2240
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public documentHasImages(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1452
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1453
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->documentHasImages(Landroid/os/Message;)V

    #@8
    .line 1454
    return-void
.end method

.method public dumpViewHierarchyWithProperties(Ljava/io/BufferedWriter;I)V
    .registers 4
    .parameter "out"
    .parameter "level"

    #@0
    .prologue
    .line 1802
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->dumpViewHierarchyWithProperties(Ljava/io/BufferedWriter;I)V

    #@5
    .line 1803
    return-void
.end method

.method public emulateShiftHeld()V
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1623
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1624
    return-void
.end method

.method public findAll(Ljava/lang/String;)I
    .registers 3
    .parameter "find"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1375
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1376
    const-string v0, "findAll blocks UI: prefer findAllAsync"

    #@5
    invoke-static {v0}, Landroid/os/StrictMode;->noteSlowCall(Ljava/lang/String;)V

    #@8
    .line 1377
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@a
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->findAll(Ljava/lang/String;)I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public findAllAsync(Ljava/lang/String;)V
    .registers 3
    .parameter "find"

    #@0
    .prologue
    .line 1389
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1390
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->findAllAsync(Ljava/lang/String;)V

    #@8
    .line 1391
    return-void
.end method

.method public findHierarchyView(Ljava/lang/String;I)Landroid/view/View;
    .registers 4
    .parameter "className"
    .parameter "hashCode"

    #@0
    .prologue
    .line 1811
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->findHierarchyView(Ljava/lang/String;I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public findNext(Z)V
    .registers 3
    .parameter "forward"

    #@0
    .prologue
    .line 1360
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1361
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->findNext(Z)V

    #@8
    .line 1362
    return-void
.end method

.method public flingScroll(II)V
    .registers 4
    .parameter "vx"
    .parameter "vy"

    #@0
    .prologue
    .line 1665
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1666
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->flingScroll(II)V

    #@8
    .line 1667
    return-void
.end method

.method public freeMemory()V
    .registers 2

    #@0
    .prologue
    .line 1281
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1282
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->freeMemory()V

    #@8
    .line 1283
    return-void
.end method

.method public getCertificate()Landroid/net/http/SslCertificate;
    .registers 2

    #@0
    .prologue
    .line 572
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 573
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getCertificate()Landroid/net/http/SslCertificate;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getContentHeight()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "webview"
    .end annotation

    #@0
    .prologue
    .line 1212
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1213
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getContentHeight()I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getContentWidth()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "webview"
    .end annotation

    #@0
    .prologue
    .line 1224
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getContentWidth()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getFavicon()Landroid/graphics/Bitmap;
    .registers 2

    #@0
    .prologue
    .line 1180
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1181
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getFavicon()Landroid/graphics/Bitmap;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getHitTestResult()Landroid/webkit/WebView$HitTestResult;
    .registers 2

    #@0
    .prologue
    .line 1099
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1100
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getHitTestResult()Landroid/webkit/WebView$HitTestResult;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 4
    .parameter "host"
    .parameter "realm"

    #@0
    .prologue
    .line 644
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 645
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->getHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getMaxZoomScale()F
    .registers 2

    #@0
    .prologue
    .line 1761
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1762
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getMaxZoomScale()F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getMinZoomScale()F
    .registers 2

    #@0
    .prologue
    .line 1770
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1771
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getMinZoomScale()F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getOriginalUrl()Ljava/lang/String;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "webview"
    .end annotation

    #@0
    .prologue
    .line 1157
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1158
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getOriginalUrl()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getProgress()I
    .registers 2

    #@0
    .prologue
    .line 1201
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1202
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getProgress()I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getScale()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "webview"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1051
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1052
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getScale()F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getSettings()Landroid/webkit/WebSettings;
    .registers 2

    #@0
    .prologue
    .line 1588
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1589
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getSettings()Landroid/webkit/WebSettings;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "webview"
    .end annotation

    #@0
    .prologue
    .line 1169
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1170
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getTitle()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getTouchIconUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1192
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getTouchIconUrl()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "webview"
    .end annotation

    #@0
    .prologue
    .line 1142
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1143
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getUrl()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getVisibleTitleHeight()I
    .registers 2

    #@0
    .prologue
    .line 561
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 562
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getVisibleTitleHeight()I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getWebViewProvider()Landroid/webkit/WebViewProvider;
    .registers 2

    #@0
    .prologue
    .line 1872
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    return-object v0
.end method

.method public getZoomControls()Landroid/view/View;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1683
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1684
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getZoomControls()Landroid/view/View;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public goBack()V
    .registers 2

    #@0
    .prologue
    .line 934
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 935
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->goBack()V

    #@8
    .line 936
    return-void
.end method

.method public goBackOrForward(I)V
    .registers 3
    .parameter "steps"

    #@0
    .prologue
    .line 977
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 978
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->goBackOrForward(I)V

    #@8
    .line 979
    return-void
.end method

.method public goForward()V
    .registers 2

    #@0
    .prologue
    .line 952
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 953
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->goForward()V

    #@8
    .line 954
    return-void
.end method

.method public invokeZoomPicker()V
    .registers 2

    #@0
    .prologue
    .line 1076
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1077
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->invokeZoomPicker()V

    #@8
    .line 1078
    return-void
.end method

.method public isPaused()Z
    .registers 2

    #@0
    .prologue
    .line 1273
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->isPaused()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isPrivateBrowsingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 985
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 986
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->isPrivateBrowsingEnabled()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public isUserScalable()Z
    .registers 2

    #@0
    .prologue
    .line 1752
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1753
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->isUserScalable()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "data"
    .parameter "mimeType"
    .parameter "encoding"

    #@0
    .prologue
    .line 844
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 845
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2, p3}, Landroid/webkit/WebViewProvider;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 846
    return-void
.end method

.method public loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "baseUrl"
    .parameter "data"
    .parameter "mimeType"
    .parameter "encoding"
    .parameter "historyUrl"

    #@0
    .prologue
    .line 873
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 874
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    move-object v1, p1

    #@6
    move-object v2, p2

    #@7
    move-object v3, p3

    #@8
    move-object v4, p4

    #@9
    move-object v5, p5

    #@a
    invoke-interface/range {v0 .. v5}, Landroid/webkit/WebViewProvider;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 875
    return-void
.end method

.method public loadUrl(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 797
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 798
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->loadUrl(Ljava/lang/String;)V

    #@8
    .line 799
    return-void
.end method

.method public loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .registers 4
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 787
    .local p2, additionalHttpHeaders:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 788
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    #@8
    .line 789
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 2

    #@0
    .prologue
    .line 2025
    invoke-super {p0}, Landroid/widget/AbsoluteLayout;->onAttachedToWindow()V

    #@3
    .line 2026
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@8
    move-result-object v0

    #@9
    invoke-interface {v0}, Landroid/webkit/WebViewProvider$ViewDelegate;->onAttachedToWindow()V

    #@c
    .line 2027
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter "parent"
    .parameter "child"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1633
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter "p"
    .parameter "child"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1642
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    #@0
    .prologue
    .line 2194
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@9
    .line 2195
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .registers 3
    .parameter "outAttrs"

    #@0
    .prologue
    .line 2199
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 2031
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Landroid/webkit/WebViewProvider$ViewDelegate;->onDetachedFromWindow()V

    #@9
    .line 2032
    invoke-super {p0}, Landroid/widget/AbsoluteLayout;->onDetachedFromWindow()V

    #@c
    .line 2033
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    #@0
    .prologue
    .line 2184
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onDraw(Landroid/graphics/Canvas;)V

    #@9
    .line 2185
    return-void
.end method

.method protected onDrawVerticalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V
    .registers 14
    .parameter "canvas"
    .parameter "scrollBar"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 2168
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    move-object v1, p1

    #@7
    move-object v2, p2

    #@8
    move v3, p3

    #@9
    move v4, p4

    #@a
    move v5, p5

    #@b
    move v6, p6

    #@c
    invoke-interface/range {v0 .. v6}, Landroid/webkit/WebViewProvider$ViewDelegate;->onDrawVerticalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    #@f
    .line 2169
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 5
    .parameter "focused"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 2216
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1, p2, p3}, Landroid/webkit/WebViewProvider$ViewDelegate;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@9
    .line 2217
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AbsoluteLayout;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@c
    .line 2218
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2098
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public onGlobalFocusChanged(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter "oldFocus"
    .parameter "newFocus"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1652
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2088
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onHoverEvent(Landroid/view/MotionEvent;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2154
    invoke-super {p0, p1}, Landroid/widget/AbsoluteLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 2155
    const-class v0, Landroid/webkit/WebView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 2156
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@e
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@11
    move-result-object v0

    #@12
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@15
    .line 2157
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 2147
    invoke-super {p0, p1}, Landroid/widget/AbsoluteLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 2148
    const-class v0, Landroid/webkit/WebView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 2149
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@e
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@11
    move-result-object v0

    #@12
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@15
    .line 2150
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 2108
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider$ViewDelegate;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "repeatCount"
    .parameter "event"

    #@0
    .prologue
    .line 2118
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1, p2, p3}, Landroid/webkit/WebViewProvider$ViewDelegate;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 2113
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider$ViewDelegate;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method protected onMeasure(II)V
    .registers 4
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2251
    invoke-super {p0, p1, p2}, Landroid/widget/AbsoluteLayout;->onMeasure(II)V

    #@3
    .line 2252
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@8
    move-result-object v0

    #@9
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider$ViewDelegate;->onMeasure(II)V

    #@c
    .line 2253
    return-void
.end method

.method protected onOverScrolled(IIZZ)V
    .registers 6
    .parameter "scrollX"
    .parameter "scrollY"
    .parameter "clampedX"
    .parameter "clampedY"

    #@0
    .prologue
    .line 2173
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewProvider$ViewDelegate;->onOverScrolled(IIZZ)V

    #@9
    .line 2174
    return-void
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 1254
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1255
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->onPause()V

    #@8
    .line 1256
    return-void
.end method

.method public onResume()V
    .registers 2

    #@0
    .prologue
    .line 1262
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1263
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->onResume()V

    #@8
    .line 1264
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .registers 6
    .parameter "l"
    .parameter "t"
    .parameter "oldl"
    .parameter "oldt"

    #@0
    .prologue
    .line 2234
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AbsoluteLayout;->onScrollChanged(IIII)V

    #@3
    .line 2235
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@8
    move-result-object v0

    #@9
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewProvider$ViewDelegate;->onScrollChanged(IIII)V

    #@c
    .line 2236
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 6
    .parameter "w"
    .parameter "h"
    .parameter "ow"
    .parameter "oh"

    #@0
    .prologue
    .line 2228
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AbsoluteLayout;->onSizeChanged(IIII)V

    #@3
    .line 2229
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@8
    move-result-object v0

    #@9
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewProvider$ViewDelegate;->onSizeChanged(IIII)V

    #@c
    .line 2230
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2093
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2103
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .registers 4
    .parameter "changedView"
    .parameter "visibility"

    #@0
    .prologue
    .line 2204
    invoke-super {p0, p1, p2}, Landroid/widget/AbsoluteLayout;->onVisibilityChanged(Landroid/view/View;I)V

    #@3
    .line 2205
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@8
    move-result-object v0

    #@9
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider$ViewDelegate;->onVisibilityChanged(Landroid/view/View;I)V

    #@c
    .line 2206
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 3
    .parameter "hasWindowFocus"

    #@0
    .prologue
    .line 2210
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onWindowFocusChanged(Z)V

    #@9
    .line 2211
    invoke-super {p0, p1}, Landroid/widget/AbsoluteLayout;->onWindowFocusChanged(Z)V

    #@c
    .line 2212
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .registers 3
    .parameter "visibility"

    #@0
    .prologue
    .line 2178
    invoke-super {p0, p1}, Landroid/widget/AbsoluteLayout;->onWindowVisibilityChanged(I)V

    #@3
    .line 2179
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@8
    move-result-object v0

    #@9
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->onWindowVisibilityChanged(I)V

    #@c
    .line 2180
    return-void
.end method

.method public overlayHorizontalScrollbar()Z
    .registers 2

    #@0
    .prologue
    .line 540
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 541
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->overlayHorizontalScrollbar()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public overlayVerticalScrollbar()Z
    .registers 2

    #@0
    .prologue
    .line 550
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 551
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->overlayVerticalScrollbar()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public pageDown(Z)Z
    .registers 3
    .parameter "bottom"

    #@0
    .prologue
    .line 1007
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1008
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->pageDown(Z)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public pageUp(Z)Z
    .registers 3
    .parameter "top"

    #@0
    .prologue
    .line 996
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 997
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->pageUp(Z)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public pauseTimers()V
    .registers 2

    #@0
    .prologue
    .line 1233
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1234
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->pauseTimers()V

    #@8
    .line 1235
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 4
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    .line 2161
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider$ViewDelegate;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public performLongClick()Z
    .registers 2

    #@0
    .prologue
    .line 2189
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Landroid/webkit/WebViewProvider$ViewDelegate;->performLongClick()Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public postUrl(Ljava/lang/String;[B)V
    .registers 4
    .parameter "url"
    .parameter "postData"

    #@0
    .prologue
    .line 810
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 811
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->postUrl(Ljava/lang/String;[B)V

    #@8
    .line 812
    return-void
.end method

.method public recalculateVisibleRect()V
    .registers 2

    #@0
    .prologue
    .line 1845
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1846
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->recalculateVisibleRect()V

    #@8
    .line 1847
    return-void
.end method

.method public refreshPlugins(Z)V
    .registers 2
    .parameter "reloadOpenPages"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1611
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1612
    return-void
.end method

.method public reload()V
    .registers 2

    #@0
    .prologue
    .line 916
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 917
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->reload()V

    #@8
    .line 918
    return-void
.end method

.method public removeJavascriptInterface(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 1576
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1577
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->removeJavascriptInterface(Ljava/lang/String;)V

    #@8
    .line 1578
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 5
    .parameter "child"
    .parameter "rect"
    .parameter "immediate"

    #@0
    .prologue
    .line 2257
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1, p2, p3}, Landroid/webkit/WebViewProvider$ViewDelegate;->requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .registers 4
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 2245
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider$ViewDelegate;->requestFocus(ILandroid/graphics/Rect;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public requestFocusNodeHref(Landroid/os/Message;)V
    .registers 3
    .parameter "hrefMsg"

    #@0
    .prologue
    .line 1117
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1118
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->requestFocusNodeHref(Landroid/os/Message;)V

    #@8
    .line 1119
    return-void
.end method

.method public requestImageRef(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 1129
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1130
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->requestImageRef(Landroid/os/Message;)V

    #@8
    .line 1131
    return-void
.end method

.method public restorePicture(Landroid/os/Bundle;Ljava/io/File;)Z
    .registers 4
    .parameter "b"
    .parameter "src"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 754
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 755
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->restorePicture(Landroid/os/Bundle;Ljava/io/File;)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;
    .registers 3
    .parameter "inState"

    #@0
    .prologue
    .line 771
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 772
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public resumeTimers()V
    .registers 2

    #@0
    .prologue
    .line 1242
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1243
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->resumeTimers()V

    #@8
    .line 1244
    return-void
.end method

.method public saveCachedImageToFile(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "imageUrl"
    .parameter "filePath"

    #@0
    .prologue
    .line 1855
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1856
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->saveCachedImageToFile(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 1857
    return-void
.end method

.method public savePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "host"
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    .line 606
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 607
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2, p3}, Landroid/webkit/WebViewProvider;->savePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 608
    return-void
.end method

.method public savePicture(Landroid/os/Bundle;Ljava/io/File;)Z
    .registers 4
    .parameter "b"
    .parameter "dest"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 737
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 738
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->savePicture(Landroid/os/Bundle;Ljava/io/File;)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;
    .registers 3
    .parameter "outState"

    #@0
    .prologue
    .line 721
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 722
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public saveWebArchive(Ljava/lang/String;)V
    .registers 3
    .parameter "filename"

    #@0
    .prologue
    .line 883
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 884
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->saveWebArchive(Ljava/lang/String;)V

    #@8
    .line 885
    return-void
.end method

.method public saveWebArchive(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V
    .registers 5
    .parameter "basename"
    .parameter "autoname"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 900
    .local p3, callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/lang/String;>;"
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 901
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2, p3}, Landroid/webkit/WebViewProvider;->saveWebArchive(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V

    #@8
    .line 902
    return-void
.end method

.method public sendPickerValue(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 1817
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1818
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->sendPickerValue(Ljava/lang/String;)V

    #@8
    .line 1819
    return-void
.end method

.method public setBackgroundColor(I)V
    .registers 3
    .parameter "color"

    #@0
    .prologue
    .line 2262
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->setBackgroundColor(I)V

    #@9
    .line 2263
    return-void
.end method

.method public setCertificate(Landroid/net/http/SslCertificate;)V
    .registers 3
    .parameter "certificate"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 584
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 585
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setCertificate(Landroid/net/http/SslCertificate;)V

    #@8
    .line 586
    return-void
.end method

.method public setDownloadListener(Landroid/webkit/DownloadListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 1475
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1476
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    #@8
    .line 1477
    return-void
.end method

.method public setDownloadListenerExt(Landroid/webkit/DownloadListenerExt;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 1489
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1490
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setDownloadListenerExt(Landroid/webkit/DownloadListenerExt;)V

    #@8
    .line 1491
    return-void
.end method

.method public setFindListener(Landroid/webkit/WebView$FindListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 1345
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1346
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setFindListener(Landroid/webkit/WebView$FindListener;)V

    #@8
    .line 1347
    return-void
.end method

.method public setForeground(Z)V
    .registers 3
    .parameter "foreground"

    #@0
    .prologue
    .line 1835
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1836
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setForeground(Z)V

    #@8
    .line 1837
    return-void
.end method

.method protected setFrame(IIII)Z
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 2223
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewProvider$ViewDelegate;->setFrame(IIII)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public setGestureZoomScale(F)V
    .registers 3
    .parameter "scale"

    #@0
    .prologue
    .line 1743
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1744
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setGestureZoomScale(F)V

    #@8
    .line 1745
    return-void
.end method

.method public setHorizontalScrollbarOverlay(Z)V
    .registers 3
    .parameter "overlay"

    #@0
    .prologue
    .line 520
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 521
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setHorizontalScrollbarOverlay(Z)V

    #@8
    .line 522
    return-void
.end method

.method public setHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "host"
    .parameter "realm"
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    .line 625
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 626
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewProvider;->setHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 627
    return-void
.end method

.method public setInitialScale(I)V
    .registers 3
    .parameter "scaleInPercent"

    #@0
    .prologue
    .line 1066
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1067
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setInitialScale(I)V

    #@8
    .line 1068
    return-void
.end method

.method public setLayerType(ILandroid/graphics/Paint;)V
    .registers 4
    .parameter "layerType"
    .parameter "paint"

    #@0
    .prologue
    .line 2267
    invoke-super {p0, p1, p2}, Landroid/widget/AbsoluteLayout;->setLayerType(ILandroid/graphics/Paint;)V

    #@3
    .line 2268
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@8
    move-result-object v0

    #@9
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider$ViewDelegate;->setLayerType(ILandroid/graphics/Paint;)V

    #@c
    .line 2269
    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter "params"

    #@0
    .prologue
    .line 2037
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@9
    .line 2038
    return-void
.end method

.method public setMapTrackballToArrowKeys(Z)V
    .registers 3
    .parameter "setMap"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1659
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1660
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setMapTrackballToArrowKeys(Z)V

    #@8
    .line 1661
    return-void
.end method

.method public setNetworkAvailable(Z)V
    .registers 3
    .parameter "networkUp"

    #@0
    .prologue
    .line 705
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 706
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setNetworkAvailable(Z)V

    #@8
    .line 707
    return-void
.end method

.method public setOverScrollMode(I)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 2042
    invoke-super {p0, p1}, Landroid/widget/AbsoluteLayout;->setOverScrollMode(I)V

    #@3
    .line 2046
    invoke-direct {p0}, Landroid/webkit/WebView;->ensureProviderCreated()V

    #@6
    .line 2047
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@8
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@b
    move-result-object v0

    #@c
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->setOverScrollMode(I)V

    #@f
    .line 2048
    return-void
.end method

.method public setPictureListener(Landroid/webkit/WebView$PictureListener;)V
    .registers 3
    .parameter "listener"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1515
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1516
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    #@8
    .line 1517
    return-void
.end method

.method public setScrollBarStyle(I)V
    .registers 3
    .parameter "style"

    #@0
    .prologue
    .line 2052
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider$ViewDelegate;->setScrollBarStyle(I)V

    #@9
    .line 2053
    invoke-super {p0, p1}, Landroid/widget/AbsoluteLayout;->setScrollBarStyle(I)V

    #@c
    .line 2054
    return-void
.end method

.method public setVerticalScrollbarOverlay(Z)V
    .registers 3
    .parameter "overlay"

    #@0
    .prologue
    .line 530
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 531
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setVerticalScrollbarOverlay(Z)V

    #@8
    .line 532
    return-void
.end method

.method public setWebChromeClient(Landroid/webkit/WebChromeClient;)V
    .registers 3
    .parameter "client"

    #@0
    .prologue
    .line 1502
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1503
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    #@8
    .line 1504
    return-void
.end method

.method public setWebViewClient(Landroid/webkit/WebViewClient;)V
    .registers 3
    .parameter "client"

    #@0
    .prologue
    .line 1463
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1464
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1}, Landroid/webkit/WebViewProvider;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    #@8
    .line 1465
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2142
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Landroid/webkit/WebViewProvider$ViewDelegate;->shouldDelayChildPressedState()Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public showActionPopup()V
    .registers 2

    #@0
    .prologue
    .line 1827
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1828
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->showActionPopup()V

    #@8
    .line 1829
    return-void
.end method

.method public showFindDialog(Ljava/lang/String;Z)Z
    .registers 4
    .parameter "text"
    .parameter "showIme"

    #@0
    .prologue
    .line 1405
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1406
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0, p1, p2}, Landroid/webkit/WebViewProvider;->showFindDialog(Ljava/lang/String;Z)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public stopLoading()V
    .registers 2

    #@0
    .prologue
    .line 908
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 909
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->stopLoading()V

    #@8
    .line 910
    return-void
.end method

.method public zoomIn()Z
    .registers 2

    #@0
    .prologue
    .line 1723
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1724
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->zoomIn()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public zoomOut()Z
    .registers 2

    #@0
    .prologue
    .line 1733
    invoke-static {}, Landroid/webkit/WebView;->checkThread()V

    #@3
    .line 1734
    iget-object v0, p0, Landroid/webkit/WebView;->mProvider:Landroid/webkit/WebViewProvider;

    #@5
    invoke-interface {v0}, Landroid/webkit/WebViewProvider;->zoomOut()Z

    #@8
    move-result v0

    #@9
    return v0
.end method
