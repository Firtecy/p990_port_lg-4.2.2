.class public final Landroid/webkit/WebViewClassic;
.super Ljava/lang/Object;
.source "WebViewClassic.java"

# interfaces
.implements Landroid/webkit/WebViewProvider;
.implements Landroid/webkit/WebViewProvider$ScrollDelegate;
.implements Landroid/webkit/WebViewProvider$ViewDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebViewClassic$PictureWrapperView;,
        Landroid/webkit/WebViewClassic$InvokeListBox;,
        Landroid/webkit/WebViewClassic$PageSwapDelegate;,
        Landroid/webkit/WebViewClassic$FocusTransitionDrawable;,
        Landroid/webkit/WebViewClassic$PrivateHandler;,
        Landroid/webkit/WebViewClassic$SendScrollToWebCore;,
        Landroid/webkit/WebViewClassic$RequestFormData;,
        Landroid/webkit/WebViewClassic$SelectionHandleAlpha;,
        Landroid/webkit/WebViewClassic$ViewSizeData;,
        Landroid/webkit/WebViewClassic$SaveWebArchiveMessage;,
        Landroid/webkit/WebViewClassic$DestroyNativeRunnable;,
        Landroid/webkit/WebViewClassic$TitleBarDelegate;,
        Landroid/webkit/WebViewClassic$PackageListener;,
        Landroid/webkit/WebViewClassic$ProxyReceiver;,
        Landroid/webkit/WebViewClassic$TrustStorageListener;,
        Landroid/webkit/WebViewClassic$Factory;,
        Landroid/webkit/WebViewClassic$FocusNodeHref;,
        Landroid/webkit/WebViewClassic$OnTrimMemoryListener;,
        Landroid/webkit/WebViewClassic$PastePopupWindow;,
        Landroid/webkit/WebViewClassic$WebViewInputConnection;
    }
.end annotation


# static fields
.field private static final ANGLE_HORIZ:F = 0.0f

.field private static final ANGLE_VERT:F = 2.0f

.field static final AUTOFILL_COMPLETE:I = 0x86

.field static final AUTOFILL_FORM:I = 0x94

.field private static final AUTO_REDRAW_HACK:Z = false

.field private static final BROWSER_PACKAGE:Ljava/lang/String; = "com.lge.browser"

.field static final CACHED_IMAGE_SAVED:I = 0xa0

.field private static final CARET_HANDLE_STAMINA_MS:J = 0xbb8L

.field static final CENTER_FIT_RECT:I = 0x7f

.field static final CLEAR_CARET_HANDLE:I = 0x90

.field static final CLEAR_TEXT_ENTRY:I = 0x6f

.field private static COORDINATE_DIRECTION:I = 0x0

.field static final COPY_TO_CLIPBOARD:I = 0x8d

.field private static final DEBUG_TOUCH_HIGHLIGHT:Z = true

.field static final DEFAULT_VIEWPORT_WIDTH:I = 0x3d4

.field private static final DRAG_HELD_MOTIONLESS:I = 0x8

.field private static final DRAG_LAYER_FINGER_DISTANCE:I = 0x4e20

.field private static final DRAW_EXTRAS_CURSOR_RING:I = 0x2

.field private static final DRAW_EXTRAS_NONE:I = 0x0

.field private static final DRAW_EXTRAS_SELECTION:I = 0x1

.field private static final EDIT_RECT_BUFFER:I = 0xa

.field static final EDIT_TEXT_SIZE_CHANGED:I = 0x96

.field static final EXIT_FULLSCREEN_VIDEO:I = 0x8c

.field private static final FIRST_PACKAGE_MSG_ID:I = 0x65

.field private static final FIRST_PRIVATE_MSG_ID:I = 0x1

.field static final FOCUS_NODE_CHANGED:I = 0x93

.field static final HANDLE_ID_BASE:I = 0x0

.field static final HANDLE_ID_EXTENT:I = 0x1

.field static final HIDE_FULLSCREEN:I = 0x79

.field static final HIGHLIGHT_COLOR:I = 0x6633b5e5

.field static final HIT_TEST_RESULT:I = 0x83

.field private static final HSLOPE_TO_BREAK_SNAP:F = 0.4f

.field private static final HSLOPE_TO_START_SNAP:F = 0.25f

.field static final HandlerPackageDebugString:[Ljava/lang/String; = null

.field static final HandlerPrivateDebugString:[Ljava/lang/String; = null

.field static final INIT_EDIT_FIELD:I = 0x8e

.field static final INVAL_RECT_MSG_ID:I = 0x75

.field static final KEY_PRESS:I = 0x91

.field private static final LAST_PACKAGE_MSG_ID:I = 0x83

.field private static final LAST_PRIVATE_MSG_ID:I = 0xb

.field static final LOGTAG:Ljava/lang/String; = "webview"

.field static final LONG_PRESS_CENTER:I = 0x72

.field private static final LONG_PRESS_TIMEOUT:I = 0x3e8

.field private static final MAX_DURATION:I = 0x2ee

.field private static final MINIMUM_VELOCITY_RATIO_FOR_ACCELERATION:F = 0.2f

.field private static final MIN_DELTA_TIME:I = 0x46

.field private static final MIN_FLING_TIME:I = 0xfa

.field private static final MMA_WEIGHT_N:F = 5.0f

.field private static final MOTIONLESS_FALSE:I = 0x0

.field private static final MOTIONLESS_IGNORE:I = 0x3

.field private static final MOTIONLESS_PENDING:I = 0x1

.field private static final MOTIONLESS_TIME:I = 0x64

.field private static final MOTIONLESS_TRUE:I = 0x2

.field private static final NEVER_REMEMBER_PASSWORD:I = 0x2

.field static final NEW_PICTURE_MSG_ID:I = 0x69

.field static final NO_LEFTEDGE:I = -0x1

.field private static final PAGE_SCROLL_OVERLAP:I = 0x18

.field private static final PREVENT_DEFAULT_TIMEOUT:I = 0xa

.field static final PREVENT_TOUCH_ID:I = 0x73

.field private static final RELEASE_SINGLE_TAP:I = 0x5

.field static final RELOCATE_AUTO_COMPLETE_POPUP:I = 0x92

.field private static final REMEMBER_PASSWORD:I = 0x1

.field static final REPLACE_TEXT:I = 0x8f

.field private static final REQUEST_FORM_DATA:I = 0x6

.field static final REQUEST_KEYBOARD:I = 0x76

.field static final SAVE_WEBARCHIVE_FINISHED:I = 0x84

.field private static final SCALE_VALUE:I = 0x1

.field public static final SCHEME_GEO:Ljava/lang/String; = "geo:0,0?q="

.field public static final SCHEME_MAILTO:Ljava/lang/String; = "mailto:"

.field public static final SCHEME_TEL:Ljava/lang/String; = "tel:"

.field static final SCREEN_ON:I = 0x88

.field private static final SCROLLBAR_ALWAYSOFF:I = 0x1

.field private static final SCROLLBAR_ALWAYSON:I = 0x2

.field private static final SCROLLBAR_AUTO:I = 0x0

.field private static final SCROLL_BITS:I = 0x6

.field static final SCROLL_EDIT_TEXT:I = 0x95

.field static final SCROLL_HANDLE_INTO_VIEW:I = 0x99

.field private static final SCROLL_SELECT_TEXT:I = 0xb

.field private static final SCROLL_TO_BOTTOM_VELOCITY_THRESHOLD:I = -0x1c2

.field static final SCROLL_TO_MSG_ID:I = 0x65

.field private static final SCROLL_TO_TOP_BOTTOM_ANIMATION_DURATION:I = 0x1f4

.field private static final SCROLL_TO_TOP_BOTTOM_XY_VELOCITY_MULTIPLE:D = 1.5

.field private static final SCROLL_TO_TOP_VELOCITY_THRESHOLD:I = 0x384

.field private static final SELECTION_HANDLE_ANIMATION_MS:J = 0x96L

.field static final SELECTION_STRING_CHANGED:I = 0x82

.field private static final SELECT_CURSOR_OFFSET:I = 0x10

.field private static final SELECT_SCROLL:I = 0x5

.field private static final SELECT_SCROLL_INTERVAL:J = 0x10L

.field static final SET_AUTOFILLABLE:I = 0x85

.field static final SET_SCROLLBAR_MODES:I = 0x81

.field static final SHOW_CARET_HANDLE:I = 0x97

.field static final SHOW_FULLSCREEN:I = 0x78

.field static final SHOW_RECT_MSG_ID:I = 0x71

.field private static final SNAP_LOCK:I = 0x1

.field private static final SNAP_NONE:I = 0x0

.field private static final SNAP_X:I = 0x2

.field private static final SNAP_Y:I = 0x4

.field private static final STD_SPEED:I = 0x1e0

.field private static final SWITCH_TO_LONGPRESS:I = 0x4

.field private static final SWITCH_TO_SHORTPRESS:I = 0x3

.field static final TAKE_FOCUS:I = 0x6e

.field private static final TAP_TIMEOUT:I = 0x12c

.field private static final TEXTLINK_ACTION:Ljava/lang/String; = "com.lge.smarttext.action.SEND"

.field private static final TEXT_SCROLL_FIRST_SCROLL_MS:J = 0x10L

.field private static final TEXT_SCROLL_RATE:F = 0.01f

.field private static final TOUCH_DONE_MODE:I = 0x7

.field private static final TOUCH_DOUBLE_TAP_MODE:I = 0x6

.field private static final TOUCH_DRAG_LAYER_MODE:I = 0x9

.field private static final TOUCH_DRAG_MODE:I = 0x3

.field private static final TOUCH_DRAG_START_MODE:I = 0x2

.field private static final TOUCH_DRAG_TEXT_MODE:I = 0xa

.field private static final TOUCH_HIGHLIGHT_ELAPSE_TIME:I = 0x7d0

.field private static final TOUCH_INIT_MODE:I = 0x1

.field private static final TOUCH_PINCH_DRAG:I = 0x8

.field private static final TOUCH_SCROLL_TO_BOTTOM_MODE:I = 0x3e9

.field private static final TOUCH_SCROLL_TO_TOP_MODE:I = 0x3e8

.field private static final TOUCH_SENT_INTERVAL:I = 0x0

.field private static final TOUCH_SHORTPRESS_MODE:I = 0x5

.field private static final TOUCH_SHORTPRESS_START_MODE:I = 0x4

.field private static final TRACKBALL_KEY_TIMEOUT:I = 0x3e8

.field private static final TRACKBALL_MOVE_COUNT:I = 0xa

.field private static final TRACKBALL_MULTIPLIER:I = 0x3

.field private static final TRACKBALL_SCALE:I = 0x190

.field private static final TRACKBALL_SCROLL_COUNT:I = 0x5

.field private static final TRACKBALL_TIMEOUT:I = 0xc8

.field private static final TRACKBALL_WAIT:I = 0x64

.field static final UPDATE_CONTENT_BOUNDS:I = 0x98

.field static final UPDATE_MATCH_COUNT:I = 0x7e

.field static final UPDATE_TEXTFIELD_TEXT_MSG_ID:I = 0x6c

.field static final UPDATE_TEXTSELECTION_POS:I = 0xb9

.field static final UPDATE_TEXT_SELECTION_MSG_ID:I = 0x70

.field static final UPDATE_ZOOM_DENSITY:I = 0x8b

.field static final UPDATE_ZOOM_RANGE:I = 0x6d

.field private static final VSLOPE_TO_BREAK_SNAP:F = 0.95f

.field private static final VSLOPE_TO_START_SNAP:F = 1.25f

.field static final WEBCORE_INITIALIZED_MSG_ID:I = 0x6b

.field static final WEBCORE_NEED_TOUCH_EVENTS:I = 0x74

.field private static final ZOOM_BITS:I = 0x86

.field private static dropFirstDeltaY:Z

.field private static eventMonitor:I

.field private static mCapptouchFlickNoti:Z

.field private static mIsSwitchingToFloating:Z

.field static mLogEvent:Z

.field private static mOverScrollBackground:Landroid/graphics/Paint;

.field private static mOverScrollBorder:Landroid/graphics/Paint;

.field private static moveCounter:I

.field private static sBrowserProxyEnabled:Z

.field private static sGoogleApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static sMaxViewportWidth:I

.field private static sNotificationsEnabled:Z

.field private static sPackageInstallationReceiverAdded:Z

.field private static sProxyReceiver:Landroid/webkit/WebViewClassic$ProxyReceiver;

.field private static sTrustStorageListener:Landroid/webkit/WebViewClassic$TrustStorageListener;


# instance fields
.field private DRAG_LAYER_INVERSE_DENSITY_SQUARED:F

.field private final FLICKMAXVELOCITY:I

.field private final GAP_BETEEN_LAST_BEFORE:I

.field private final LIMIT_Y_GAP:I

.field private final MARK_INVERSION:I

.field private SELECTION_SCROLL_PosX:I

.field private SELECTION_SCROLL_PosY:I

.field private TOUCH_NAV_SLOP_ABSOLUTE:I

.field private final WEIGHTED_VELOCITY:I

.field private final X_Y_VELOCITY_GAP:I

.field private final coorDirection:[I

.field private countFling:I

.field private mAccessibilityInjector:Landroid/webkit/AccessibilityInjector;

.field private mActionPointerUpTime:J

.field private mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

.field private mAutoFillData:Landroid/webkit/WebViewCore$AutoFillData;

.field private mAutoRedraw:Z

.field private mAutoScrollX:I

.field private mAutoScrollY:I

.field private mAverageAngle:F

.field mAverageSwapFps:D

.field private mBackgroundColor:I

.field private mBaseAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

.field private mBaseHandleAlphaAnimator:Landroid/animation/ObjectAnimator;

.field mBatchedTextChanges:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private mBlockWebkitViewMessages:Z

.field private mCachedOverlappingActionModeHeight:I

.field private mCallbackProxy:Landroid/webkit/CallbackProxy;

.field private mCertificate:Landroid/net/http/SslCertificate;

.field private mConfirmMove:Z

.field private mContentHeight:I

.field private mContentWidth:I

.field private final mContext:Landroid/content/Context;

.field private mCurrentScrollingLayerId:I

.field private mCurrentTouchInterval:I

.field private mDatabase:Landroid/webkit/WebViewDatabaseClassic;

.field private mDelaySetPicture:Landroid/webkit/WebViewCore$DrawData;

.field private mDoubleTapSlopSquare:I

.field private mDrawCursorRing:Z

.field private mDrawHistory:Z

.field mEditTextContent:Landroid/graphics/Rect;

.field mEditTextContentBounds:Landroid/graphics/Rect;

.field mEditTextLayerId:I

.field mEditTextScroller:Landroid/widget/Scroller;

.field private mExtentAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

.field private mExtentHandleAlphaAnimator:Landroid/animation/ObjectAnimator;

.field private mFieldPointer:I

.field private mFindCallback:Landroid/webkit/FindActionModeCallback;

.field private mFindIsUp:Z

.field private mFindListener:Landroid/webkit/WebView$FindListener;

.field private mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

.field private mFocusTransition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

.field private mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

.field private mForceScrollEditIntoView:Z

.field mFullScreenHolder:Landroid/webkit/PluginFullScreenHolder;

.field private mGlobalVisibleOffset:Landroid/graphics/Point;

.field private mGlobalVisibleRect:Landroid/graphics/Rect;

.field private mGotCenterDown:Z

.field private mHTML5VideoViewProxy:Landroid/webkit/HTML5VideoViewProxy;

.field private mHardwareAccelSkia:Z

.field mHeightCanMeasure:Z

.field private mHeldMotionless:I

.field private mHistoryHeight:I

.field private mHistoryPicture:Landroid/graphics/Picture;

.field private mHistoryWidth:I

.field private mHorizontalScrollBarMode:I

.field private mInOverScrollMode:Z

.field private mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

.field private mInitialScaleInPercent:I

.field mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

.field private mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

.field private final mInvScreenRect:Landroid/graphics/Rect;

.field mIsBatchingTextChanges:Z

.field private mIsCaretSelection:Z

.field mIsEditingText:Z

.field private mIsFloatingMode:Z

.field private mIsForeground:Z

.field private mIsPaused:Z

.field private mIsProperPointsFor2fingerFlicking:Z

.field private mIsSelectingAll:Z

.field private mIsSelectingInEditText:Z

.field private mIsWebViewVisible:Z

.field private mKeysPressed:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLGSelectActionPopupShower:Ljava/lang/Runnable;

.field private mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

.field mLastActualHeightSent:I

.field private mLastCursorBounds:Landroid/graphics/Rect;

.field private mLastCursorTime:J

.field private mLastEditScroll:J

.field private mLastGlobalRect:Landroid/graphics/Rect;

.field mLastHeightSent:I

.field private mLastPointerUpTime:J

.field private mLastSentTouchTime:J

.field mLastSwapTime:J

.field private mLastTouchTime:J

.field private mLastTouchUpTime:J

.field private mLastTouchX:I

.field private mLastTouchY:I

.field private mLastVelX:F

.field private mLastVelY:F

.field private mLastVelocity:F

.field private mLastVisibleRectSent:Landroid/graphics/Rect;

.field mLastWidthSent:I

.field private mListBoxDialog:Landroid/app/AlertDialog;

.field private mListBoxMessage:Landroid/os/Message;

.field private mLoadedPicture:Landroid/webkit/WebViewCore$DrawData;

.field private mMapTrackballToArrowKeys:Z

.field private mMaxAutoScrollX:I

.field private mMaxAutoScrollY:I

.field private mMaximumFling:I

.field private mMinAutoScrollX:I

.field private mMinAutoScrollY:I

.field private mNativeClass:I

.field private mNavSlop:I

.field private mOrientation:I

.field private mOverScrollGlow:Landroid/webkit/OverScrollGlow;

.field private mOverflingDistance:I

.field private mOverlayHorizontalScrollbar:Z

.field private mOverlayVerticalScrollbar:Z

.field private mOverscrollDistance:I

.field private mParagraphBottomRight:Landroid/graphics/Point;

.field private mParagraphSelectOffset:Landroid/graphics/Point;

.field private mParagraphTopLeft:Landroid/graphics/Point;

.field private mPasteWindow:Landroid/webkit/WebViewClassic$PastePopupWindow;

.field private mPictureListener:Landroid/webkit/WebView$PictureListener;

.field private mPictureUpdatePausedForFocusChange:Z

.field private mPrevAction:I

.field final mPrivateHandler:Landroid/os/Handler;

.field private mResumeMsg:Landroid/os/Message;

.field private mSavePasswordDialog:Landroid/app/AlertDialog;

.field private final mScreenRect:Landroid/graphics/Rect;

.field private final mScrollFilter:Landroid/graphics/DrawFilter;

.field private mScrollOffset:Landroid/graphics/Point;

.field mScroller:Landroid/widget/OverScroller;

.field private mScrollingLayerBounds:Landroid/graphics/Rect;

.field private mScrollingLayerRect:Landroid/graphics/Rect;

.field private mSelectCallback:Landroid/webkit/SelectActionModeCallback;

.field private mSelectCursorBase:Landroid/graphics/Point;

.field private mSelectCursorBaseLayerId:I

.field private mSelectCursorBaseTextQuad:Landroid/webkit/QuadF;

.field private mSelectCursorExtent:Landroid/graphics/Point;

.field private mSelectCursorExtentLayerId:I

.field private mSelectCursorExtentTextQuad:Landroid/webkit/QuadF;

.field private mSelectDraggingCursor:Landroid/graphics/Point;

.field private mSelectDraggingTextQuad:Landroid/webkit/QuadF;

.field private mSelectHandleBaseBounds:Landroid/graphics/Rect;

.field private mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

.field private mSelectHandleExtentBounds:Landroid/graphics/Rect;

.field private mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

.field private mSelectHandleLeftReversed:Landroid/graphics/drawable/Drawable;

.field private mSelectHandlePoint:Landroid/graphics/Bitmap;

.field private mSelectHandleRight:Landroid/graphics/drawable/Drawable;

.field private mSelectHandleRightReversed:Landroid/graphics/drawable/Drawable;

.field private mSelectOffset:Landroid/graphics/Point;

.field private mSelectX:I

.field private mSelectY:I

.field private mSelectingText:Z

.field private mSelectionStarted:Z

.field mSendScroll:Landroid/webkit/WebViewClassic$SendScrollToWebCore;

.field private mSendScrollEvent:Z

.field private mSentAutoScrollMessage:Z

.field private mShowLGPopupNeeded:Z

.field private mShowTapHighlight:Z

.field private mShowTextSelectionExtra:Z

.field private mSnapPositive:Z

.field private mSnapScrollMode:I

.field private mStartTouchX:I

.field private mStartTouchY:I

.field private final mTempContentVisibleRect:Landroid/graphics/Rect;

.field private final mTempVisibleRect:Landroid/graphics/Rect;

.field private final mTempVisibleRectOffset:Landroid/graphics/Point;

.field private mTestHitRect:Landroid/graphics/Rect;

.field private mTextGeneration:I

.field private mTextSelectionPaint:Landroid/graphics/Paint;

.field private mTouchCrossHairColor:Landroid/graphics/Paint;

.field private mTouchHighlightRegion:Landroid/graphics/Region;

.field private mTouchHighlightX:I

.field private mTouchHighlightY:I

.field private mTouchHightlightPaint:Landroid/graphics/Paint;

.field private mTouchInEditText:Z

.field private mTouchMode:I

.field private mTouchSlopSquare:I

.field private mTrackballDown:Z

.field private mTrackballFirstTime:J

.field private mTrackballLastTime:J

.field private mTrackballRemainsX:F

.field private mTrackballRemainsY:F

.field private mTrackballUpTime:J

.field private mTrackballXMove:I

.field private mTrackballYMove:I

.field private mTranslateMode:Z

.field private mVelocityArrayList:[F

.field mVelocityTracker:Landroid/view/VelocityTracker;

.field private mVerticalScrollBarMode:I

.field mViewManager:Landroid/webkit/ViewManager;

.field private final mVisibleContentRect:Landroid/graphics/RectF;

.field private mVisibleRect:Landroid/graphics/Rect;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private final mWebView:Landroid/webkit/WebView;

.field private mWebViewCore:Landroid/webkit/WebViewCore;

.field private final mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

.field mWidthCanMeasure:Z

.field private mWrapContent:Z

.field private final mZoomFilter:Landroid/graphics/DrawFilter;

.field private mZoomManager:Landroid/webkit/ZoomManager;

.field private numDragMove:I

.field private oldFlingTime:J

.field private startDragSystemTime:J

.field private startFlingSystemTime:J


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v6, 0x2

    #@2
    const/16 v5, 0x8

    #@4
    const/4 v4, 0x1

    #@5
    const/4 v3, 0x0

    #@6
    .line 1011
    sput-boolean v3, Landroid/webkit/WebViewClassic;->mIsSwitchingToFloating:Z

    #@8
    .line 1167
    sput-boolean v3, Landroid/webkit/WebViewClassic;->mCapptouchFlickNoti:Z

    #@a
    .line 1168
    sput-boolean v3, Landroid/webkit/WebViewClassic;->dropFirstDeltaY:Z

    #@c
    .line 1169
    sput v5, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@e
    .line 1174
    sput v3, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@10
    .line 1175
    sput v3, Landroid/webkit/WebViewClassic;->moveCounter:I

    #@12
    .line 1413
    const/16 v0, 0xb

    #@14
    new-array v0, v0, [Ljava/lang/String;

    #@16
    const-string v1, "REMEMBER_PASSWORD"

    #@18
    aput-object v1, v0, v3

    #@1a
    const-string v1, "NEVER_REMEMBER_PASSWORD"

    #@1c
    aput-object v1, v0, v4

    #@1e
    const-string v1, "SWITCH_TO_SHORTPRESS"

    #@20
    aput-object v1, v0, v6

    #@22
    const-string v1, "SWITCH_TO_LONGPRESS"

    #@24
    aput-object v1, v0, v7

    #@26
    const/4 v1, 0x4

    #@27
    const-string v2, "RELEASE_SINGLE_TAP"

    #@29
    aput-object v2, v0, v1

    #@2b
    const/4 v1, 0x5

    #@2c
    const-string v2, "REQUEST_FORM_DATA"

    #@2e
    aput-object v2, v0, v1

    #@30
    const/4 v1, 0x6

    #@31
    const-string v2, "RESUME_WEBCORE_PRIORITY"

    #@33
    aput-object v2, v0, v1

    #@35
    const/4 v1, 0x7

    #@36
    const-string v2, "DRAG_HELD_MOTIONLESS"

    #@38
    aput-object v2, v0, v1

    #@3a
    const-string v1, ""

    #@3c
    aput-object v1, v0, v5

    #@3e
    const/16 v1, 0x9

    #@40
    const-string v2, "PREVENT_DEFAULT_TIMEOUT"

    #@42
    aput-object v2, v0, v1

    #@44
    const/16 v1, 0xa

    #@46
    const-string v2, "SCROLL_SELECT_TEXT"

    #@48
    aput-object v2, v0, v1

    #@4a
    sput-object v0, Landroid/webkit/WebViewClassic;->HandlerPrivateDebugString:[Ljava/lang/String;

    #@4c
    .line 1427
    const/16 v0, 0x26

    #@4e
    new-array v0, v0, [Ljava/lang/String;

    #@50
    const-string v1, "SCROLL_TO_MSG_ID"

    #@52
    aput-object v1, v0, v3

    #@54
    const-string v1, "102"

    #@56
    aput-object v1, v0, v4

    #@58
    const-string v1, "103"

    #@5a
    aput-object v1, v0, v6

    #@5c
    const-string v1, "104"

    #@5e
    aput-object v1, v0, v7

    #@60
    const/4 v1, 0x4

    #@61
    const-string v2, "NEW_PICTURE_MSG_ID"

    #@63
    aput-object v2, v0, v1

    #@65
    const/4 v1, 0x5

    #@66
    const-string v2, "UPDATE_TEXT_ENTRY_MSG_ID"

    #@68
    aput-object v2, v0, v1

    #@6a
    const/4 v1, 0x6

    #@6b
    const-string v2, "WEBCORE_INITIALIZED_MSG_ID"

    #@6d
    aput-object v2, v0, v1

    #@6f
    const/4 v1, 0x7

    #@70
    const-string v2, "UPDATE_TEXTFIELD_TEXT_MSG_ID"

    #@72
    aput-object v2, v0, v1

    #@74
    const-string v1, "UPDATE_ZOOM_RANGE"

    #@76
    aput-object v1, v0, v5

    #@78
    const/16 v1, 0x9

    #@7a
    const-string v2, "UNHANDLED_NAV_KEY"

    #@7c
    aput-object v2, v0, v1

    #@7e
    const/16 v1, 0xa

    #@80
    const-string v2, "CLEAR_TEXT_ENTRY"

    #@82
    aput-object v2, v0, v1

    #@84
    const/16 v1, 0xb

    #@86
    const-string v2, "UPDATE_TEXT_SELECTION_MSG_ID"

    #@88
    aput-object v2, v0, v1

    #@8a
    const/16 v1, 0xc

    #@8c
    const-string v2, "SHOW_RECT_MSG_ID"

    #@8e
    aput-object v2, v0, v1

    #@90
    const/16 v1, 0xd

    #@92
    const-string v2, "LONG_PRESS_CENTER"

    #@94
    aput-object v2, v0, v1

    #@96
    const/16 v1, 0xe

    #@98
    const-string v2, "PREVENT_TOUCH_ID"

    #@9a
    aput-object v2, v0, v1

    #@9c
    const/16 v1, 0xf

    #@9e
    const-string v2, "WEBCORE_NEED_TOUCH_EVENTS"

    #@a0
    aput-object v2, v0, v1

    #@a2
    const/16 v1, 0x10

    #@a4
    const-string v2, "INVAL_RECT_MSG_ID"

    #@a6
    aput-object v2, v0, v1

    #@a8
    const/16 v1, 0x11

    #@aa
    const-string v2, "REQUEST_KEYBOARD"

    #@ac
    aput-object v2, v0, v1

    #@ae
    const/16 v1, 0x12

    #@b0
    const-string v2, "DO_MOTION_UP"

    #@b2
    aput-object v2, v0, v1

    #@b4
    const/16 v1, 0x13

    #@b6
    const-string v2, "SHOW_FULLSCREEN"

    #@b8
    aput-object v2, v0, v1

    #@ba
    const/16 v1, 0x14

    #@bc
    const-string v2, "HIDE_FULLSCREEN"

    #@be
    aput-object v2, v0, v1

    #@c0
    const/16 v1, 0x15

    #@c2
    const-string v2, "DOM_FOCUS_CHANGED"

    #@c4
    aput-object v2, v0, v1

    #@c6
    const/16 v1, 0x16

    #@c8
    const-string v2, "REPLACE_BASE_CONTENT"

    #@ca
    aput-object v2, v0, v1

    #@cc
    const/16 v1, 0x17

    #@ce
    const-string v2, "RETURN_LABEL"

    #@d0
    aput-object v2, v0, v1

    #@d2
    const/16 v1, 0x18

    #@d4
    const-string v2, "UPDATE_MATCH_COUNT"

    #@d6
    aput-object v2, v0, v1

    #@d8
    const/16 v1, 0x19

    #@da
    const-string v2, "CENTER_FIT_RECT"

    #@dc
    aput-object v2, v0, v1

    #@de
    const/16 v1, 0x1a

    #@e0
    const-string v2, "REQUEST_KEYBOARD_WITH_SELECTION_MSG_ID"

    #@e2
    aput-object v2, v0, v1

    #@e4
    const/16 v1, 0x1b

    #@e6
    const-string v2, "SET_SCROLLBAR_MODES"

    #@e8
    aput-object v2, v0, v1

    #@ea
    const/16 v1, 0x1c

    #@ec
    const-string v2, "SELECTION_STRING_CHANGED"

    #@ee
    aput-object v2, v0, v1

    #@f0
    const/16 v1, 0x1d

    #@f2
    const-string v2, "SET_TOUCH_HIGHLIGHT_RECTS"

    #@f4
    aput-object v2, v0, v1

    #@f6
    const/16 v1, 0x1e

    #@f8
    const-string v2, "SAVE_WEBARCHIVE_FINISHED"

    #@fa
    aput-object v2, v0, v1

    #@fc
    const/16 v1, 0x1f

    #@fe
    const-string v2, "SET_AUTOFILLABLE"

    #@100
    aput-object v2, v0, v1

    #@102
    const/16 v1, 0x20

    #@104
    const-string v2, "AUTOFILL_COMPLETE"

    #@106
    aput-object v2, v0, v1

    #@108
    const/16 v1, 0x21

    #@10a
    const-string v2, "SELECT_AT"

    #@10c
    aput-object v2, v0, v1

    #@10e
    const/16 v1, 0x22

    #@110
    const-string v2, "SCREEN_ON"

    #@112
    aput-object v2, v0, v1

    #@114
    const/16 v1, 0x23

    #@116
    const-string v2, "ENTER_FULLSCREEN_VIDEO"

    #@118
    aput-object v2, v0, v1

    #@11a
    const/16 v1, 0x24

    #@11c
    const-string v2, "UPDATE_SELECTION"

    #@11e
    aput-object v2, v0, v1

    #@120
    const/16 v1, 0x25

    #@122
    const-string v2, "UPDATE_ZOOM_DENSITY"

    #@124
    aput-object v2, v0, v1

    #@126
    sput-object v0, Landroid/webkit/WebViewClassic;->HandlerPackageDebugString:[Ljava/lang/String;

    #@128
    .line 1476
    const/16 v0, 0x3d4

    #@12a
    sput v0, Landroid/webkit/WebViewClassic;->sMaxViewportWidth:I

    #@12c
    .line 1522
    sput-boolean v4, Landroid/webkit/WebViewClassic;->mLogEvent:Z

    #@12e
    .line 1529
    sput-boolean v4, Landroid/webkit/WebViewClassic;->sNotificationsEnabled:Z

    #@130
    .line 1923
    sput-boolean v3, Landroid/webkit/WebViewClassic;->sBrowserProxyEnabled:Z

    #@132
    .line 1984
    sput-boolean v3, Landroid/webkit/WebViewClassic;->sPackageInstallationReceiverAdded:Z

    #@134
    .line 1993
    new-instance v0, Ljava/util/HashSet;

    #@136
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@139
    sput-object v0, Landroid/webkit/WebViewClassic;->sGoogleApps:Ljava/util/Set;

    #@13b
    .line 1994
    sget-object v0, Landroid/webkit/WebViewClassic;->sGoogleApps:Ljava/util/Set;

    #@13d
    const-string v1, "com.google.android.youtube"

    #@13f
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@142
    .line 1995
    return-void
.end method

.method public constructor <init>(Landroid/webkit/WebView;Landroid/webkit/WebView$PrivateAccess;)V
    .registers 11
    .parameter "webView"
    .parameter "privateAccess"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const-wide/16 v6, 0x0

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    const/4 v3, 0x0

    #@6
    .line 1607
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 984
    iput-object v4, p0, Landroid/webkit/WebViewClassic;->mListBoxDialog:Landroid/app/AlertDialog;

    #@b
    .line 988
    iput-object v4, p0, Landroid/webkit/WebViewClassic;->mSavePasswordDialog:Landroid/app/AlertDialog;

    #@d
    .line 994
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mForceScrollEditIntoView:Z

    #@f
    .line 995
    new-instance v0, Landroid/graphics/Rect;

    #@11
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@14
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mInvScreenRect:Landroid/graphics/Rect;

    #@16
    .line 996
    new-instance v0, Landroid/graphics/Rect;

    #@18
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mScreenRect:Landroid/graphics/Rect;

    #@1d
    .line 997
    new-instance v0, Landroid/graphics/RectF;

    #@1f
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@22
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mVisibleContentRect:Landroid/graphics/RectF;

    #@24
    .line 998
    iput-boolean v5, p0, Landroid/webkit/WebViewClassic;->mIsWebViewVisible:Z

    #@26
    .line 999
    iput-object v4, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@28
    .line 1003
    new-instance v0, Landroid/graphics/Rect;

    #@2a
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@2d
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@2f
    .line 1004
    new-instance v0, Landroid/graphics/Rect;

    #@31
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@34
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mEditTextContent:Landroid/graphics/Rect;

    #@36
    .line 1006
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@38
    .line 1007
    new-instance v0, Ljava/util/ArrayList;

    #@3a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@3d
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mBatchedTextChanges:Ljava/util/ArrayList;

    #@3f
    .line 1008
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mIsBatchingTextChanges:Z

    #@41
    .line 1009
    iput-wide v6, p0, Landroid/webkit/WebViewClassic;->mLastEditScroll:J

    #@43
    .line 1068
    new-instance v0, Landroid/webkit/WebViewClassic$PrivateHandler;

    #@45
    invoke-direct {v0, p0}, Landroid/webkit/WebViewClassic$PrivateHandler;-><init>(Landroid/webkit/WebViewClassic;)V

    #@48
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@4a
    .line 1095
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4d
    move-result-wide v0

    #@4e
    iput-wide v0, p0, Landroid/webkit/WebViewClassic;->oldFlingTime:J

    #@50
    .line 1098
    const/16 v0, 0x12c

    #@52
    new-array v0, v0, [F

    #@54
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mVelocityArrayList:[F

    #@56
    .line 1127
    iput v3, p0, Landroid/webkit/WebViewClassic;->mCurrentTouchInterval:I

    #@58
    .line 1140
    new-instance v0, Landroid/graphics/Rect;

    #@5a
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@5d
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@5f
    .line 1151
    const/4 v0, 0x7

    #@60
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@62
    .line 1170
    const/16 v0, 0x320

    #@64
    iput v0, p0, Landroid/webkit/WebViewClassic;->X_Y_VELOCITY_GAP:I

    #@66
    .line 1171
    const/16 v0, 0x258

    #@68
    iput v0, p0, Landroid/webkit/WebViewClassic;->WEIGHTED_VELOCITY:I

    #@6a
    .line 1172
    iput v2, p0, Landroid/webkit/WebViewClassic;->MARK_INVERSION:I

    #@6c
    .line 1173
    sget v0, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@6e
    new-array v0, v0, [I

    #@70
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->coorDirection:[I

    #@72
    .line 1176
    const/16 v0, 0xfa0

    #@74
    iput v0, p0, Landroid/webkit/WebViewClassic;->FLICKMAXVELOCITY:I

    #@76
    .line 1177
    const/16 v0, 0xd

    #@78
    iput v0, p0, Landroid/webkit/WebViewClassic;->GAP_BETEEN_LAST_BEFORE:I

    #@7a
    .line 1178
    const/4 v0, 0x5

    #@7b
    iput v0, p0, Landroid/webkit/WebViewClassic;->LIMIT_Y_GAP:I

    #@7d
    .line 1198
    iput-boolean v5, p0, Landroid/webkit/WebViewClassic;->mDrawCursorRing:Z

    #@7f
    .line 1215
    const/16 v0, 0x8

    #@81
    iput v0, p0, Landroid/webkit/WebViewClassic;->TOUCH_NAV_SLOP_ABSOLUTE:I

    #@83
    .line 1255
    iput-boolean v5, p0, Landroid/webkit/WebViewClassic;->mOverlayHorizontalScrollbar:Z

    #@85
    .line 1256
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mOverlayVerticalScrollbar:Z

    #@87
    .line 1269
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mInOverScrollMode:Z

    #@89
    .line 1296
    iput-object v4, p0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@8b
    .line 1297
    new-instance v0, Landroid/graphics/Point;

    #@8d
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@90
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@92
    .line 1298
    new-instance v0, Landroid/graphics/Point;

    #@94
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@97
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@99
    .line 1299
    new-instance v0, Landroid/graphics/Point;

    #@9b
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@9e
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mParagraphSelectOffset:Landroid/graphics/Point;

    #@a0
    .line 1302
    new-instance v0, Landroid/graphics/Point;

    #@a2
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@a5
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@a7
    .line 1303
    new-instance v0, Landroid/graphics/Rect;

    #@a9
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@ac
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleBaseBounds:Landroid/graphics/Rect;

    #@ae
    .line 1305
    new-instance v0, Landroid/webkit/QuadF;

    #@b0
    invoke-direct {v0}, Landroid/webkit/QuadF;-><init>()V

    #@b3
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBaseTextQuad:Landroid/webkit/QuadF;

    #@b5
    .line 1306
    new-instance v0, Landroid/graphics/Point;

    #@b7
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@ba
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@bc
    .line 1307
    new-instance v0, Landroid/graphics/Rect;

    #@be
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@c1
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleExtentBounds:Landroid/graphics/Rect;

    #@c3
    .line 1309
    new-instance v0, Landroid/webkit/QuadF;

    #@c5
    invoke-direct {v0}, Landroid/webkit/QuadF;-><init>()V

    #@c8
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtentTextQuad:Landroid/webkit/QuadF;

    #@ca
    .line 1319
    new-instance v0, Landroid/graphics/Region;

    #@cc
    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    #@cf
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@d1
    .line 1321
    new-instance v0, Landroid/graphics/Paint;

    #@d3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@d6
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mTouchHightlightPaint:Landroid/graphics/Paint;

    #@d8
    .line 1335
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@da
    .line 1338
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mHardwareAccelSkia:Z

    #@dc
    .line 1479
    iput v3, p0, Landroid/webkit/WebViewClassic;->mInitialScaleInPercent:I

    #@de
    .line 1483
    iput-boolean v5, p0, Landroid/webkit/WebViewClassic;->mSendScrollEvent:Z

    #@e0
    .line 1485
    iput v3, p0, Landroid/webkit/WebViewClassic;->mSnapScrollMode:I

    #@e2
    .line 1502
    iput v3, p0, Landroid/webkit/WebViewClassic;->mHorizontalScrollBarMode:I

    #@e4
    .line 1503
    iput v3, p0, Landroid/webkit/WebViewClassic;->mVerticalScrollBarMode:I

    #@e6
    .line 1525
    iput-wide v6, p0, Landroid/webkit/WebViewClassic;->mLastTouchUpTime:J

    #@e8
    .line 1544
    iput v2, p0, Landroid/webkit/WebViewClassic;->mBackgroundColor:I

    #@ea
    .line 1547
    iput v3, p0, Landroid/webkit/WebViewClassic;->mAutoScrollX:I

    #@ec
    .line 1548
    iput v3, p0, Landroid/webkit/WebViewClassic;->mAutoScrollY:I

    #@ee
    .line 1549
    iput v3, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollX:I

    #@f0
    .line 1550
    iput v3, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollX:I

    #@f2
    .line 1551
    iput v3, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollY:I

    #@f4
    .line 1552
    iput v3, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollY:I

    #@f6
    .line 1553
    new-instance v0, Landroid/graphics/Rect;

    #@f8
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@fb
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerBounds:Landroid/graphics/Rect;

    #@fd
    .line 1554
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mSentAutoScrollMessage:Z

    #@ff
    .line 1556
    iput v2, p0, Landroid/webkit/WebViewClassic;->SELECTION_SCROLL_PosX:I

    #@101
    .line 1557
    iput v2, p0, Landroid/webkit/WebViewClassic;->SELECTION_SCROLL_PosY:I

    #@103
    .line 1565
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mPictureUpdatePausedForFocusChange:Z

    #@105
    .line 1578
    iput v2, p0, Landroid/webkit/WebViewClassic;->mPrevAction:I

    #@107
    .line 1579
    iput-wide v6, p0, Landroid/webkit/WebViewClassic;->mLastPointerUpTime:J

    #@109
    .line 2443
    iput v2, p0, Landroid/webkit/WebViewClassic;->mCachedOverlappingActionModeHeight:I

    #@10b
    .line 3631
    new-instance v0, Landroid/graphics/Rect;

    #@10d
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@110
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mLastVisibleRectSent:Landroid/graphics/Rect;

    #@112
    .line 3632
    new-instance v0, Landroid/graphics/Rect;

    #@114
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@117
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mLastGlobalRect:Landroid/graphics/Rect;

    #@119
    .line 3633
    new-instance v0, Landroid/graphics/Rect;

    #@11b
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@11e
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mVisibleRect:Landroid/graphics/Rect;

    #@120
    .line 3634
    new-instance v0, Landroid/graphics/Rect;

    #@122
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@125
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleRect:Landroid/graphics/Rect;

    #@127
    .line 3635
    new-instance v0, Landroid/graphics/Point;

    #@129
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@12c
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollOffset:Landroid/graphics/Point;

    #@12e
    .line 3679
    new-instance v0, Landroid/graphics/Point;

    #@130
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@133
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleOffset:Landroid/graphics/Point;

    #@135
    .line 3699
    new-instance v0, Landroid/graphics/Rect;

    #@137
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@13a
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mTempContentVisibleRect:Landroid/graphics/Rect;

    #@13c
    .line 4265
    iput-object v4, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@13e
    .line 5153
    iput v3, p0, Landroid/webkit/WebViewClassic;->mOrientation:I

    #@140
    .line 5154
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mShowLGPopupNeeded:Z

    #@142
    .line 5239
    new-instance v0, Landroid/graphics/PaintFlagsDrawFilter;

    #@144
    const/16 v1, 0x86

    #@146
    const/16 v2, 0x40

    #@148
    invoke-direct {v0, v1, v2}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    #@14b
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomFilter:Landroid/graphics/DrawFilter;

    #@14d
    .line 5242
    new-instance v0, Landroid/graphics/PaintFlagsDrawFilter;

    #@14f
    const/4 v1, 0x6

    #@150
    invoke-direct {v0, v1, v3}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    #@153
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollFilter:Landroid/graphics/DrawFilter;

    #@155
    .line 5474
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@157
    .line 5475
    iput-object v4, p0, Landroid/webkit/WebViewClassic;->mHistoryPicture:Landroid/graphics/Picture;

    #@159
    .line 5476
    iput v3, p0, Landroid/webkit/WebViewClassic;->mHistoryWidth:I

    #@15b
    .line 5477
    iput v3, p0, Landroid/webkit/WebViewClassic;->mHistoryHeight:I

    #@15d
    .line 5754
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mGotCenterDown:Z

    #@15f
    .line 5957
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mIsFloatingMode:Z

    #@161
    .line 5958
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mIsSelectingAll:Z

    #@163
    .line 5959
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mIsSelectingInEditText:Z

    #@165
    .line 6400
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mTranslateMode:Z

    #@167
    .line 6561
    new-instance v0, Landroid/graphics/Point;

    #@169
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@16c
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mTempVisibleRectOffset:Landroid/graphics/Point;

    #@16e
    .line 6562
    new-instance v0, Landroid/graphics/Rect;

    #@170
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@173
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mTempVisibleRect:Landroid/graphics/Rect;

    #@175
    .line 6738
    new-instance v0, Landroid/webkit/WebViewClassic$SendScrollToWebCore;

    #@177
    invoke-direct {v0, p0, v4}, Landroid/webkit/WebViewClassic$SendScrollToWebCore;-><init>(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewClassic$1;)V

    #@17a
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSendScroll:Landroid/webkit/WebViewClassic$SendScrollToWebCore;

    #@17c
    .line 6941
    new-instance v0, Landroid/graphics/Rect;

    #@17e
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@181
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mTestHitRect:Landroid/graphics/Rect;

    #@183
    .line 7887
    iput-wide v6, p0, Landroid/webkit/WebViewClassic;->mTrackballFirstTime:J

    #@185
    .line 7888
    iput-wide v6, p0, Landroid/webkit/WebViewClassic;->mTrackballLastTime:J

    #@187
    .line 7889
    const/4 v0, 0x0

    #@188
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTrackballRemainsX:F

    #@18a
    .line 7890
    const/4 v0, 0x0

    #@18b
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTrackballRemainsY:F

    #@18d
    .line 7891
    iput v3, p0, Landroid/webkit/WebViewClassic;->mTrackballXMove:I

    #@18f
    .line 7892
    iput v3, p0, Landroid/webkit/WebViewClassic;->mTrackballYMove:I

    #@191
    .line 7893
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@193
    .line 7894
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mShowTextSelectionExtra:Z

    #@195
    .line 7895
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@197
    .line 7905
    iput v3, p0, Landroid/webkit/WebViewClassic;->mSelectX:I

    #@199
    .line 7906
    iput v3, p0, Landroid/webkit/WebViewClassic;->mSelectY:I

    #@19b
    .line 7907
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mTrackballDown:Z

    #@19d
    .line 7908
    iput-wide v6, p0, Landroid/webkit/WebViewClassic;->mTrackballUpTime:J

    #@19f
    .line 7909
    iput-wide v6, p0, Landroid/webkit/WebViewClassic;->mLastCursorTime:J

    #@1a1
    .line 7911
    new-instance v0, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@1a3
    invoke-direct {v0, p0, v4}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;-><init>(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewClassic$1;)V

    #@1a6
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mBaseAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@1a8
    .line 7912
    new-instance v0, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@1aa
    invoke-direct {v0, p0, v4}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;-><init>(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewClassic$1;)V

    #@1ad
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mExtentAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@1af
    .line 7913
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mBaseAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@1b1
    const-string v1, "alpha"

    #@1b3
    new-array v2, v5, [I

    #@1b5
    aput v3, v2, v3

    #@1b7
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    #@1ba
    move-result-object v0

    #@1bb
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mBaseHandleAlphaAnimator:Landroid/animation/ObjectAnimator;

    #@1bd
    .line 7915
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mExtentAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@1bf
    const-string v1, "alpha"

    #@1c1
    new-array v2, v5, [I

    #@1c3
    aput v3, v2, v3

    #@1c5
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    #@1c8
    move-result-object v0

    #@1c9
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mExtentHandleAlphaAnimator:Landroid/animation/ObjectAnimator;

    #@1cb
    .line 7921
    iput-boolean v5, p0, Landroid/webkit/WebViewClassic;->mMapTrackballToArrowKeys:Z

    #@1cd
    .line 9440
    iput-object v4, p0, Landroid/webkit/WebViewClassic;->mFocusTransition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

    #@1cf
    .line 10758
    iput-boolean v5, p0, Landroid/webkit/WebViewClassic;->mIsForeground:Z

    #@1d1
    .line 1608
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@1d3
    .line 1609
    iput-object p2, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@1d5
    .line 1610
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    #@1d8
    move-result-object v0

    #@1d9
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@1db
    .line 1611
    return-void
.end method

.method private abortAnimation()V
    .registers 2

    #@0
    .prologue
    .line 3594
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@2
    invoke-virtual {v0}, Landroid/widget/OverScroller;->abortAnimation()V

    #@5
    .line 3595
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/webkit/WebViewClassic;->mLastVelocity:F

    #@8
    .line 3596
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/webkit/WebViewClassic;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget v0, p0, Landroid/webkit/WebViewClassic;->mFieldPointer:I

    #@2
    return v0
.end method

.method static synthetic access$1000(Landroid/webkit/WebViewClassic;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method static synthetic access$10000(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsSelectingAll:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Landroid/webkit/WebViewClassic;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput p1, p0, Landroid/webkit/WebViewClassic;->mFieldPointer:I

    #@2
    return p1
.end method

.method static synthetic access$1100(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-static {p0}, Landroid/webkit/WebViewClassic;->nativeOnTrimMemory(I)V

    #@3
    return-void
.end method

.method static synthetic access$1200()V
    .registers 0

    #@0
    .prologue
    .line 191
    invoke-static {}, Landroid/webkit/WebViewClassic;->handleCertTrustChanged()V

    #@3
    return-void
.end method

.method static synthetic access$1400()Z
    .registers 1

    #@0
    .prologue
    .line 191
    sget-boolean v0, Landroid/webkit/WebViewClassic;->sBrowserProxyEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$1500(Landroid/content/Intent;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-static {p0}, Landroid/webkit/WebViewClassic;->handleProxyBroadcast(Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method static synthetic access$1700()Ljava/util/Set;
    .registers 1

    #@0
    .prologue
    .line 191
    sget-object v0, Landroid/webkit/WebViewClassic;->sGoogleApps:Ljava/util/Set;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/webkit/WebViewClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->beginTextBatch()V

    #@3
    return-void
.end method

.method static synthetic access$2000(Landroid/webkit/WebViewClassic;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mResumeMsg:Landroid/os/Message;

    #@2
    return-object v0
.end method

.method static synthetic access$2002(Landroid/webkit/WebViewClassic;Landroid/os/Message;)Landroid/os/Message;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mResumeMsg:Landroid/os/Message;

    #@2
    return-object p1
.end method

.method static synthetic access$2102(Landroid/webkit/WebViewClassic;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mSavePasswordDialog:Landroid/app/AlertDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$2200(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-static {p0}, Landroid/webkit/WebViewClassic;->nativeDestroy(I)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Landroid/webkit/WebViewClassic;Landroid/graphics/Picture;Landroid/os/Bundle;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewClassic;->restoreHistoryPictureFields(Landroid/graphics/Picture;Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$2400(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$DrawData;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mLoadedPicture:Landroid/webkit/WebViewCore$DrawData;

    #@2
    return-object v0
.end method

.method static synthetic access$2402(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$DrawData;)Landroid/webkit/WebViewCore$DrawData;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mLoadedPicture:Landroid/webkit/WebViewCore$DrawData;

    #@2
    return-object p1
.end method

.method static synthetic access$2500(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$AutoFillData;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoFillData:Landroid/webkit/WebViewCore$AutoFillData;

    #@2
    return-object v0
.end method

.method static synthetic access$2502(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$AutoFillData;)Landroid/webkit/WebViewCore$AutoFillData;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mAutoFillData:Landroid/webkit/WebViewCore$AutoFillData;

    #@2
    return-object p1
.end method

.method static synthetic access$2600(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewDatabaseClassic;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mDatabase:Landroid/webkit/WebViewDatabaseClassic;

    #@2
    return-object v0
.end method

.method static synthetic access$2700(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mInOverScrollMode:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/webkit/WebViewClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->commitTextBatch()V

    #@3
    return-void
.end method

.method static synthetic access$3000(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@2
    return v0
.end method

.method static synthetic access$3100(Landroid/webkit/WebViewClassic;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget v0, p0, Landroid/webkit/WebViewClassic;->mAutoScrollX:I

    #@2
    return v0
.end method

.method static synthetic access$3200(Landroid/webkit/WebViewClassic;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget v0, p0, Landroid/webkit/WebViewClassic;->mAutoScrollY:I

    #@2
    return v0
.end method

.method static synthetic access$3300(Landroid/webkit/WebViewClassic;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$3402(Landroid/webkit/WebViewClassic;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mSentAutoScrollMessage:Z

    #@2
    return p1
.end method

.method static synthetic access$3500(Landroid/webkit/WebViewClassic;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget v0, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@2
    return v0
.end method

.method static synthetic access$3600(Landroid/webkit/WebViewClassic;IIZI)Z
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClassic;->pinScrollBy(IIZI)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3700(Landroid/webkit/WebViewClassic;)Landroid/graphics/Rect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method static synthetic access$3800(Landroid/webkit/WebViewClassic;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewClassic;->scrollLayerTo(II)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@2
    return v0
.end method

.method static synthetic access$4000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewClassic$PastePopupWindow;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPasteWindow:Landroid/webkit/WebViewClassic$PastePopupWindow;

    #@2
    return-object v0
.end method

.method static synthetic access$4100(Landroid/webkit/WebViewClassic;IIZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewClassic;->contentScrollTo(IIZ)V

    #@3
    return-void
.end method

.method static synthetic access$4200(Landroid/webkit/WebViewClassic;)Landroid/webkit/ZoomManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    return-object v0
.end method

.method static synthetic access$4300(Landroid/webkit/WebViewClassic;ILjava/lang/String;Z)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewClassic;->nativeCreate(ILjava/lang/String;Z)V

    #@3
    return-void
.end method

.method static synthetic access$4400(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$DrawData;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mDelaySetPicture:Landroid/webkit/WebViewCore$DrawData;

    #@2
    return-object v0
.end method

.method static synthetic access$4402(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$DrawData;)Landroid/webkit/WebViewCore$DrawData;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mDelaySetPicture:Landroid/webkit/WebViewCore$DrawData;

    #@2
    return-object p1
.end method

.method static synthetic access$4500(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsPaused:Z

    #@2
    return v0
.end method

.method static synthetic access$4600(Landroid/webkit/WebViewClassic;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    return v0
.end method

.method static synthetic access$4700(IZ)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    invoke-static {p0, p1}, Landroid/webkit/WebViewClassic;->nativeSetPauseDrawing(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$4800(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewInputDispatcher;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@2
    return-object v0
.end method

.method static synthetic access$4802(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewInputDispatcher;)Landroid/webkit/WebViewInputDispatcher;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@2
    return-object p1
.end method

.method static synthetic access$4900(Landroid/webkit/WebViewClassic;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@2
    return v0
.end method

.method static synthetic access$4902(Landroid/webkit/WebViewClassic;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput p1, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@2
    return p1
.end method

.method static synthetic access$500(Landroid/webkit/WebViewClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->hideSoftKeyboard()V

    #@3
    return-void
.end method

.method static synthetic access$5000(Landroid/webkit/WebViewClassic;IILandroid/webkit/WebViewCore$TextSelectionData;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewClassic;->updateTextSelectionFromMessage(IILandroid/webkit/WebViewCore$TextSelectionData;)V

    #@3
    return-void
.end method

.method static synthetic access$5100(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsFloatingMode:Z

    #@2
    return v0
.end method

.method static synthetic access$5102(Landroid/webkit/WebViewClassic;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mIsFloatingMode:Z

    #@2
    return p1
.end method

.method static synthetic access$5200(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mShowLGPopupNeeded:Z

    #@2
    return v0
.end method

.method static synthetic access$5202(Landroid/webkit/WebViewClassic;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mShowLGPopupNeeded:Z

    #@2
    return p1
.end method

.method static synthetic access$5300(Landroid/webkit/WebViewClassic;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->showLGSelectActionPopupWindow(I)V

    #@3
    return-void
.end method

.method static synthetic access$5400(Landroid/webkit/WebViewClassic;IIII)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClassic;->viewInvalidate(IIII)V

    #@3
    return-void
.end method

.method static synthetic access$5502(Landroid/webkit/WebViewClassic;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mGotCenterDown:Z

    #@2
    return p1
.end method

.method static synthetic access$5602(Landroid/webkit/WebViewClassic;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mTrackballDown:Z

    #@2
    return p1
.end method

.method static synthetic access$5700(Landroid/webkit/WebViewClassic;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->displaySoftKeyboard(Z)V

    #@3
    return-void
.end method

.method static synthetic access$5802(Landroid/webkit/WebViewClassic;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput p1, p0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@2
    return p1
.end method

.method static synthetic access$5900(Landroid/webkit/WebViewClassic;)Landroid/webkit/HTML5VideoViewProxy;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHTML5VideoViewProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AutoCompletePopup;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@2
    return-object v0
.end method

.method static synthetic access$6000(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->inFullScreenMode()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$6100(Landroid/webkit/WebViewClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->dismissFullScreenMode()V

    #@3
    return-void
.end method

.method static synthetic access$6200(Landroid/webkit/WebViewClassic;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getVisibleTitleHeightImpl()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$6302(Landroid/webkit/WebViewClassic;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput p1, p0, Landroid/webkit/WebViewClassic;->mHorizontalScrollBarMode:I

    #@2
    return p1
.end method

.method static synthetic access$6402(Landroid/webkit/WebViewClassic;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput p1, p0, Landroid/webkit/WebViewClassic;->mVerticalScrollBarMode:I

    #@2
    return p1
.end method

.method static synthetic access$6500(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isAccessibilityInjectionEnabled()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$6600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AccessibilityInjector;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$6702(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$WebKitHitTest;)Landroid/webkit/WebViewCore$WebKitHitTest;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@2
    return-object p1
.end method

.method static synthetic access$6800(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$WebKitHitTest;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->setTouchHighlightRects(Landroid/webkit/WebViewCore$WebKitHitTest;)V

    #@3
    return-void
.end method

.method static synthetic access$6900(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$WebKitHitTest;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->setHitTestResult(Landroid/webkit/WebViewCore$WebKitHitTest;)V

    #@3
    return-void
.end method

.method static synthetic access$700()Z
    .registers 1

    #@0
    .prologue
    .line 191
    sget-boolean v0, Landroid/webkit/WebViewClassic;->mIsSwitchingToFloating:Z

    #@2
    return v0
.end method

.method static synthetic access$7000(Landroid/webkit/WebViewClassic;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->copyToClipboard(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$7100(IILandroid/graphics/Rect;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 191
    invoke-static {p0, p1, p2}, Landroid/webkit/WebViewClassic;->nativeMapLayerRect(IILandroid/graphics/Rect;)V

    #@3
    return-void
.end method

.method static synthetic access$7200(Landroid/webkit/WebViewClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->relocateAutoCompletePopup()V

    #@3
    return-void
.end method

.method static synthetic access$7302(Landroid/webkit/WebViewClassic;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mForceScrollEditIntoView:Z

    #@2
    return p1
.end method

.method static synthetic access$7400(Landroid/webkit/WebViewClassic;)Landroid/webkit/FindActionModeCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$7500(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$FindAllRequest;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@2
    return-object v0
.end method

.method static synthetic access$7600(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView$FindListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFindListener:Landroid/webkit/WebView$FindListener;

    #@2
    return-object v0
.end method

.method static synthetic access$7700(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->setupWebkitSelect()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$7800(Landroid/webkit/WebViewClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->resetCaretTimer()V

    #@3
    return-void
.end method

.method static synthetic access$7900(Landroid/webkit/WebViewClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->showPasteWindow()V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$8000(Landroid/webkit/WebViewClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->scrollEditWithCursor()V

    #@3
    return-void
.end method

.method static synthetic access$8100(Landroid/webkit/WebViewClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->scrollDraggedSelectionHandleIntoView()V

    #@3
    return-void
.end method

.method static synthetic access$8200(Landroid/webkit/WebViewClassic;Landroid/view/MotionEvent;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewClassic;->onHandleUiEvent(Landroid/view/MotionEvent;II)V

    #@3
    return-void
.end method

.method static synthetic access$8300(Landroid/webkit/WebViewClassic;II)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 191
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewClassic;->hitParagraphHandle(II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$8400(Landroid/webkit/WebViewClassic;)Landroid/graphics/Rect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleBaseBounds:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method static synthetic access$8500(Landroid/webkit/WebViewClassic;)Landroid/graphics/Rect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleExtentBounds:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method static synthetic access$8600(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mShowTextSelectionExtra:Z

    #@2
    return v0
.end method

.method static synthetic access$8700(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mShowTapHighlight:Z

    #@2
    return v0
.end method

.method static synthetic access$8702(Landroid/webkit/WebViewClassic;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mShowTapHighlight:Z

    #@2
    return p1
.end method

.method static synthetic access$8800(Landroid/webkit/WebViewClassic;)Landroid/graphics/Paint;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mTouchHightlightPaint:Landroid/graphics/Paint;

    #@2
    return-object v0
.end method

.method static synthetic access$8900(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewClassic$FocusTransitionDrawable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFocusTransition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

    #@2
    return-object v0
.end method

.method static synthetic access$8902(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewClassic$FocusTransitionDrawable;)Landroid/webkit/WebViewClassic$FocusTransitionDrawable;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mFocusTransition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

    #@2
    return-object p1
.end method

.method static synthetic access$900(Landroid/webkit/WebViewClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->ensureSelectionHandles()V

    #@3
    return-void
.end method

.method static synthetic access$9400(Landroid/webkit/WebViewClassic;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mListBoxDialog:Landroid/app/AlertDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$9402(Landroid/webkit/WebViewClassic;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mListBoxDialog:Landroid/app/AlertDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$9502(Landroid/webkit/WebViewClassic;Landroid/os/Message;)Landroid/os/Message;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mListBoxMessage:Landroid/os/Message;

    #@2
    return-object p1
.end method

.method static synthetic access$9800(Landroid/webkit/WebViewClassic;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsForeground:Z

    #@2
    return v0
.end method

.method static synthetic access$9900(Landroid/webkit/WebViewClassic;)Landroid/webkit/LGSelectActionPopupWindow;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@2
    return-object v0
.end method

.method private animateHandle(ZLandroid/animation/ObjectAnimator;Landroid/graphics/Point;ILandroid/webkit/WebViewClassic$SelectionHandleAlpha;)V
    .registers 11
    .parameter "canShow"
    .parameter "animator"
    .parameter "selectionPoint"
    .parameter "selectionLayerId"
    .parameter "alpha"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 5278
    if-eqz p1, :cond_34

    #@4
    iget-boolean v4, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@6
    if-eqz v4, :cond_34

    #@8
    iget-boolean v4, p0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@a
    if-eqz v4, :cond_10

    #@c
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@e
    if-eq v4, p3, :cond_16

    #@10
    :cond_10
    invoke-direct {p0, p3, p4}, Landroid/webkit/WebViewClassic;->isHandleVisible(Landroid/graphics/Point;I)Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_34

    #@16
    :cond_16
    move v0, v3

    #@17
    .line 5281
    .local v0, isVisible:Z
    :goto_17
    if-eqz v0, :cond_36

    #@19
    const/16 v1, 0xff

    #@1b
    .line 5282
    .local v1, targetValue:I
    :goto_1b
    invoke-virtual {p5}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;->getTargetAlpha()I

    #@1e
    move-result v4

    #@1f
    if-eq v1, v4, :cond_33

    #@21
    .line 5283
    invoke-virtual {p5, v1}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;->setTargetAlpha(I)V

    #@24
    .line 5284
    new-array v3, v3, [I

    #@26
    aput v1, v3, v2

    #@28
    invoke-virtual {p2, v3}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    #@2b
    .line 5285
    const-wide/16 v2, 0x96

    #@2d
    invoke-virtual {p2, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@30
    .line 5286
    invoke-virtual {p2}, Landroid/animation/ObjectAnimator;->start()V

    #@33
    .line 5288
    :cond_33
    return-void

    #@34
    .end local v0           #isVisible:Z
    .end local v1           #targetValue:I
    :cond_34
    move v0, v2

    #@35
    .line 5278
    goto :goto_17

    #@36
    .restart local v0       #isVisible:Z
    :cond_36
    move v1, v2

    #@37
    .line 5281
    goto :goto_1b
.end method

.method private animateHandles()V
    .registers 10

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 5291
    iget-boolean v3, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@4
    if-eqz v3, :cond_36

    #@6
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTranslateMode()Z

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_36

    #@c
    move v1, v0

    #@d
    .line 5292
    .local v1, canShowBase:Z
    :goto_d
    iget-boolean v3, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@f
    if-eqz v3, :cond_38

    #@11
    iget-boolean v3, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@13
    if-nez v3, :cond_38

    #@15
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTranslateMode()Z

    #@18
    move-result v3

    #@19
    if-nez v3, :cond_38

    #@1b
    move v8, v0

    #@1c
    .line 5293
    .local v8, canShowExtent:Z
    :goto_1c
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mBaseHandleAlphaAnimator:Landroid/animation/ObjectAnimator;

    #@1e
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@20
    iget v4, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBaseLayerId:I

    #@22
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mBaseAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@24
    move-object v0, p0

    #@25
    invoke-direct/range {v0 .. v5}, Landroid/webkit/WebViewClassic;->animateHandle(ZLandroid/animation/ObjectAnimator;Landroid/graphics/Point;ILandroid/webkit/WebViewClassic$SelectionHandleAlpha;)V

    #@28
    .line 5295
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mExtentHandleAlphaAnimator:Landroid/animation/ObjectAnimator;

    #@2a
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@2c
    iget v6, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtentLayerId:I

    #@2e
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mExtentAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@30
    move-object v2, p0

    #@31
    move v3, v8

    #@32
    invoke-direct/range {v2 .. v7}, Landroid/webkit/WebViewClassic;->animateHandle(ZLandroid/animation/ObjectAnimator;Landroid/graphics/Point;ILandroid/webkit/WebViewClassic$SelectionHandleAlpha;)V

    #@35
    .line 5298
    return-void

    #@36
    .end local v1           #canShowBase:Z
    .end local v8           #canShowExtent:Z
    :cond_36
    move v1, v2

    #@37
    .line 5291
    goto :goto_d

    #@38
    .restart local v1       #canShowBase:Z
    :cond_38
    move v8, v2

    #@39
    .line 5292
    goto :goto_1c
.end method

.method private beginScrollEdit()V
    .registers 5

    #@0
    .prologue
    .line 7551
    iget-wide v0, p0, Landroid/webkit/WebViewClassic;->mLastEditScroll:J

    #@2
    const-wide/16 v2, 0x0

    #@4
    cmp-long v0, v0, v2

    #@6
    if-nez v0, :cond_14

    #@8
    .line 7552
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@b
    move-result-wide v0

    #@c
    const-wide/16 v2, 0x10

    #@e
    sub-long/2addr v0, v2

    #@f
    iput-wide v0, p0, Landroid/webkit/WebViewClassic;->mLastEditScroll:J

    #@11
    .line 7554
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->scrollEditWithCursor()V

    #@14
    .line 7556
    :cond_14
    return-void
.end method

.method private beginTextBatch()V
    .registers 2

    #@0
    .prologue
    .line 9805
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsBatchingTextChanges:Z

    #@3
    .line 9806
    return-void
.end method

.method private calcOurContentVisibleRect(Landroid/graphics/Rect;)V
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 3688
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->calcOurVisibleRect(Landroid/graphics/Rect;)V

    #@3
    .line 3689
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@5
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@8
    move-result v0

    #@9
    iput v0, p1, Landroid/graphics/Rect;->left:I

    #@b
    .line 3694
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@d
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getVisibleTitleHeightImpl()I

    #@10
    move-result v1

    #@11
    add-int/2addr v0, v1

    #@12
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@15
    move-result v0

    #@16
    iput v0, p1, Landroid/graphics/Rect;->top:I

    #@18
    .line 3695
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@1a
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@1d
    move-result v0

    #@1e
    iput v0, p1, Landroid/graphics/Rect;->right:I

    #@20
    .line 3696
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@22
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@25
    move-result v0

    #@26
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    #@28
    .line 3697
    return-void
.end method

.method private calcOurContentVisibleRectF(Landroid/graphics/RectF;)V
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 3705
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mTempContentVisibleRect:Landroid/graphics/Rect;

    #@2
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->calcOurVisibleRect(Landroid/graphics/Rect;)V

    #@5
    .line 3706
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mTempContentVisibleRect:Landroid/graphics/Rect;

    #@7
    invoke-direct {p0, p1, v0}, Landroid/webkit/WebViewClassic;->viewToContentVisibleRect(Landroid/graphics/RectF;Landroid/graphics/Rect;)V

    #@a
    .line 3707
    return-void
.end method

.method private calcOurVisibleRect(Landroid/graphics/Rect;)V
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 3682
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleOffset:Landroid/graphics/Point;

    #@4
    invoke-virtual {v0, p1, v1}, Landroid/webkit/WebView;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    #@7
    .line 3683
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleOffset:Landroid/graphics/Point;

    #@9
    iget v0, v0, Landroid/graphics/Point;->x:I

    #@b
    neg-int v0, v0

    #@c
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleOffset:Landroid/graphics/Point;

    #@e
    iget v1, v1, Landroid/graphics/Point;->y:I

    #@10
    neg-int v1, v1

    #@11
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    #@14
    .line 3684
    return-void
.end method

.method private calculateBaseCaretTop()Landroid/graphics/Point;
    .registers 3

    #@0
    .prologue
    .line 6043
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@2
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBaseTextQuad:Landroid/webkit/QuadF;

    #@4
    invoke-static {v0, v1}, Landroid/webkit/WebViewClassic;->calculateCaretTop(Landroid/graphics/Point;Landroid/webkit/QuadF;)Landroid/graphics/Point;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private static calculateCaretTop(Landroid/graphics/Point;Landroid/webkit/QuadF;)Landroid/graphics/Point;
    .registers 9
    .parameter "base"
    .parameter "quad"

    #@0
    .prologue
    .line 6055
    iget v3, p0, Landroid/graphics/Point;->x:I

    #@2
    iget v4, p0, Landroid/graphics/Point;->y:I

    #@4
    iget-object v5, p1, Landroid/webkit/QuadF;->p4:Landroid/graphics/PointF;

    #@6
    iget-object v6, p1, Landroid/webkit/QuadF;->p3:Landroid/graphics/PointF;

    #@8
    invoke-static {v3, v4, v5, v6}, Landroid/webkit/WebViewClassic;->scaleAlongSegment(IILandroid/graphics/PointF;Landroid/graphics/PointF;)F

    #@b
    move-result v0

    #@c
    .line 6056
    .local v0, scale:F
    iget-object v3, p1, Landroid/webkit/QuadF;->p1:Landroid/graphics/PointF;

    #@e
    iget v3, v3, Landroid/graphics/PointF;->x:F

    #@10
    iget-object v4, p1, Landroid/webkit/QuadF;->p2:Landroid/graphics/PointF;

    #@12
    iget v4, v4, Landroid/graphics/PointF;->x:F

    #@14
    invoke-static {v0, v3, v4}, Landroid/webkit/WebViewClassic;->scaleCoordinate(FFF)F

    #@17
    move-result v3

    #@18
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@1b
    move-result v1

    #@1c
    .line 6057
    .local v1, x:I
    iget-object v3, p1, Landroid/webkit/QuadF;->p1:Landroid/graphics/PointF;

    #@1e
    iget v3, v3, Landroid/graphics/PointF;->y:F

    #@20
    iget-object v4, p1, Landroid/webkit/QuadF;->p2:Landroid/graphics/PointF;

    #@22
    iget v4, v4, Landroid/graphics/PointF;->y:F

    #@24
    invoke-static {v0, v3, v4}, Landroid/webkit/WebViewClassic;->scaleCoordinate(FFF)F

    #@27
    move-result v3

    #@28
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@2b
    move-result v2

    #@2c
    .line 6058
    .local v2, y:I
    new-instance v3, Landroid/graphics/Point;

    #@2e
    invoke-direct {v3, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    #@31
    return-object v3
.end method

.method private calculateDragAngle(II)F
    .registers 7
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 6935
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    #@3
    move-result p1

    #@4
    .line 6936
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    #@7
    move-result p2

    #@8
    .line 6937
    int-to-double v0, p2

    #@9
    int-to-double v2, p1

    #@a
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@d
    move-result-wide v0

    #@e
    double-to-float v0, v0

    #@f
    return v0
.end method

.method private calculateDraggingCaretTop()Landroid/graphics/Point;
    .registers 3

    #@0
    .prologue
    .line 6047
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@2
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@4
    invoke-static {v0, v1}, Landroid/webkit/WebViewClassic;->calculateCaretTop(Landroid/graphics/Point;Landroid/webkit/QuadF;)Landroid/graphics/Point;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private canTextScroll(II)Z
    .registers 11
    .parameter "directionX"
    .parameter "directionY"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 8772
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getTextScrollX()I

    #@5
    move-result v4

    #@6
    .line 8773
    .local v4, scrollX:I
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getTextScrollY()I

    #@9
    move-result v5

    #@a
    .line 8774
    .local v5, scrollY:I
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollX()I

    #@d
    move-result v2

    #@e
    .line 8775
    .local v2, maxScrollX:I
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollY()I

    #@11
    move-result v3

    #@12
    .line 8776
    .local v3, maxScrollY:I
    if-lez p1, :cond_24

    #@14
    if-ge v4, v2, :cond_22

    #@16
    move v0, v7

    #@17
    .line 8779
    .local v0, canScrollX:Z
    :goto_17
    if-lez p2, :cond_2c

    #@19
    if-ge v5, v3, :cond_2a

    #@1b
    move v1, v7

    #@1c
    .line 8782
    .local v1, canScrollY:Z
    :goto_1c
    if-nez v0, :cond_20

    #@1e
    if-eqz v1, :cond_21

    #@20
    :cond_20
    move v6, v7

    #@21
    :cond_21
    return v6

    #@22
    .end local v0           #canScrollX:Z
    .end local v1           #canScrollY:Z
    :cond_22
    move v0, v6

    #@23
    .line 8776
    goto :goto_17

    #@24
    :cond_24
    if-lez v4, :cond_28

    #@26
    move v0, v7

    #@27
    goto :goto_17

    #@28
    :cond_28
    move v0, v6

    #@29
    goto :goto_17

    #@2a
    .restart local v0       #canScrollX:Z
    :cond_2a
    move v1, v6

    #@2b
    .line 8779
    goto :goto_1c

    #@2c
    :cond_2c
    if-lez v5, :cond_30

    #@2e
    move v1, v7

    #@2f
    goto :goto_1c

    #@30
    :cond_30
    move v1, v6

    #@31
    goto :goto_1c
.end method

.method private cancelDialogs()V
    .registers 2

    #@0
    .prologue
    .line 2564
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mListBoxDialog:Landroid/app/AlertDialog;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 2565
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mListBoxDialog:Landroid/app/AlertDialog;

    #@6
    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    #@9
    .line 2568
    :cond_9
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSavePasswordDialog:Landroid/app/AlertDialog;

    #@b
    if-eqz v0, :cond_15

    #@d
    .line 2569
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSavePasswordDialog:Landroid/app/AlertDialog;

    #@f
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    #@12
    .line 2570
    const/4 v0, 0x0

    #@13
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSavePasswordDialog:Landroid/app/AlertDialog;

    #@15
    .line 2572
    :cond_15
    return-void
.end method

.method private cancelTouch()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v3, 0x0

    #@2
    .line 7790
    sget-boolean v1, Landroid/webkit/WebViewClassic;->mCapptouchFlickNoti:Z

    #@4
    if-eqz v1, :cond_18

    #@6
    .line 7791
    sput v3, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@8
    .line 7792
    sput v3, Landroid/webkit/WebViewClassic;->moveCounter:I

    #@a
    .line 7793
    sput-boolean v3, Landroid/webkit/WebViewClassic;->dropFirstDeltaY:Z

    #@c
    .line 7794
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    sget v1, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@f
    if-ge v0, v1, :cond_18

    #@11
    .line 7795
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->coorDirection:[I

    #@13
    aput v3, v1, v0

    #@15
    .line 7794
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_d

    #@18
    .line 7803
    .end local v0           #i:I
    :cond_18
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@1a
    if-eqz v1, :cond_24

    #@1c
    .line 7804
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@1e
    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    #@21
    .line 7805
    const/4 v1, 0x0

    #@22
    iput-object v1, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@24
    .line 7808
    :cond_24
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@26
    if-eqz v1, :cond_2d

    #@28
    .line 7809
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@2a
    invoke-virtual {v1, v3}, Landroid/webkit/WebViewInputDispatcher;->setTouchDragMode(Z)V

    #@2d
    .line 7813
    :cond_2d
    iget v1, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@2f
    if-eq v1, v4, :cond_37

    #@31
    iget v1, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@33
    const/16 v2, 0x9

    #@35
    if-ne v1, v2, :cond_46

    #@37
    :cond_37
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@39
    if-nez v1, :cond_46

    #@3b
    .line 7815
    invoke-static {}, Landroid/webkit/WebViewCore;->resumePriority()V

    #@3e
    .line 7816
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@40
    invoke-static {v1}, Landroid/webkit/WebViewCore;->resumeUpdatePicture(Landroid/webkit/WebViewCore;)V

    #@43
    .line 7817
    invoke-direct {p0, v3}, Landroid/webkit/WebViewClassic;->nativeSetIsScrolling(Z)V

    #@46
    .line 7819
    :cond_46
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@48
    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@4b
    .line 7820
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@4d
    const/4 v2, 0x4

    #@4e
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@51
    .line 7821
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@53
    const/16 v2, 0x8

    #@55
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@58
    .line 7822
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->removeTouchHighlight()V

    #@5b
    .line 7823
    const/4 v1, 0x2

    #@5c
    iput v1, p0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@5e
    .line 7824
    const/4 v1, 0x7

    #@5f
    iput v1, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@61
    .line 7825
    return-void
.end method

.method private static clampBetween(III)I
    .registers 4
    .parameter "value"
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    .line 7586
    invoke-static {p0, p2}, Ljava/lang/Math;->min(II)I

    #@3
    move-result v0

    #@4
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method private clearActionModes()V
    .registers 2

    #@0
    .prologue
    .line 2527
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 2528
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@6
    invoke-virtual {v0}, Landroid/webkit/SelectActionModeCallback;->finish()V

    #@9
    .line 2530
    :cond_9
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 2531
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@f
    invoke-virtual {v0}, Landroid/webkit/FindActionModeCallback;->finish()V

    #@12
    .line 2534
    :cond_12
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->hideLGSelectActionPopupWindow()V

    #@15
    .line 2536
    return-void
.end method

.method private clearHelpers()V
    .registers 4

    #@0
    .prologue
    .line 2544
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->hideSoftKeyboard()V

    #@3
    .line 2545
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->clearActionModes()V

    #@6
    .line 2546
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->dismissFullScreenMode()V

    #@9
    .line 2547
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->cancelDialogs()V

    #@c
    .line 2549
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->clearFormData()V

    #@f
    .line 2553
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@12
    move-result-object v1

    #@13
    .line 2554
    .local v1, settings:Landroid/webkit/WebSettings;
    if-eqz v1, :cond_24

    #@15
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_24

    #@1b
    .line 2555
    invoke-static {}, Landroid/webkit/LGCliptrayManager;->getInstance()Landroid/webkit/LGCliptrayManager;

    #@1e
    move-result-object v0

    #@1f
    .line 2556
    .local v0, cliptray:Landroid/webkit/LGCliptrayManager;
    if-eqz v0, :cond_24

    #@21
    .line 2557
    invoke-virtual {v0}, Landroid/webkit/LGCliptrayManager;->hideCliptray()V

    #@24
    .line 2561
    .end local v0           #cliptray:Landroid/webkit/LGCliptrayManager;
    :cond_24
    return-void
.end method

.method private commitTextBatch()V
    .registers 3

    #@0
    .prologue
    .line 9809
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 9810
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@6
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mBatchedTextChanges:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessages(Ljava/util/ArrayList;)V

    #@b
    .line 9812
    :cond_b
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mBatchedTextChanges:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@10
    .line 9813
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsBatchingTextChanges:Z

    #@13
    .line 9814
    return-void
.end method

.method private static computeDuration(II)I
    .registers 6
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 4486
    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    #@3
    move-result v2

    #@4
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    #@7
    move-result v3

    #@8
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@b
    move-result v0

    #@c
    .line 4487
    .local v0, distance:I
    mul-int/lit16 v2, v0, 0x3e8

    #@e
    div-int/lit16 v1, v2, 0x1e0

    #@10
    .line 4488
    .local v1, duration:I
    const/16 v2, 0x2ee

    #@12
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@15
    move-result v2

    #@16
    return v2
.end method

.method private computeRealHorizontalScrollRange()I
    .registers 3

    #@0
    .prologue
    .line 3801
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 3802
    iget v0, p0, Landroid/webkit/WebViewClassic;->mHistoryWidth:I

    #@6
    .line 3805
    :goto_6
    return v0

    #@7
    :cond_7
    iget v0, p0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@9
    int-to-float v0, v0

    #@a
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@c
    invoke-virtual {v1}, Landroid/webkit/ZoomManager;->getScale()F

    #@f
    move-result v1

    #@10
    mul-float/2addr v0, v1

    #@11
    float-to-double v0, v0

    #@12
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    #@15
    move-result-wide v0

    #@16
    double-to-int v0, v0

    #@17
    goto :goto_6
.end method

.method private computeRealVerticalScrollRange()I
    .registers 3

    #@0
    .prologue
    .line 3831
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 3832
    iget v0, p0, Landroid/webkit/WebViewClassic;->mHistoryHeight:I

    #@6
    .line 3835
    :goto_6
    return v0

    #@7
    :cond_7
    iget v0, p0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@9
    int-to-float v0, v0

    #@a
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@c
    invoke-virtual {v1}, Landroid/webkit/ZoomManager;->getScale()F

    #@f
    move-result v1

    #@10
    mul-float/2addr v0, v1

    #@11
    float-to-double v0, v0

    #@12
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    #@15
    move-result-wide v0

    #@16
    double-to-int v0, v0

    #@17
    goto :goto_6
.end method

.method private contentScrollTo(IIZ)V
    .registers 7
    .parameter "cx"
    .parameter "cy"
    .parameter "animate"

    #@0
    .prologue
    .line 4593
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@2
    if-eqz v2, :cond_5

    #@4
    .line 4601
    :goto_4
    return-void

    #@5
    .line 4598
    :cond_5
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@8
    move-result v0

    #@9
    .line 4599
    .local v0, vx:I
    invoke-virtual {p0, p2}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@c
    move-result v1

    #@d
    .line 4600
    .local v1, vy:I
    const/4 v2, 0x0

    #@e
    invoke-direct {p0, v0, v1, p3, v2}, Landroid/webkit/WebViewClassic;->pinScrollTo(IIZI)Z

    #@11
    goto :goto_4
.end method

.method private contentSizeChanged(Z)V
    .registers 4
    .parameter "updateLayout"

    #@0
    .prologue
    .line 4611
    iget v0, p0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@2
    iget v1, p0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@4
    or-int/2addr v0, v1

    #@5
    if-nez v0, :cond_8

    #@7
    .line 4630
    :cond_7
    :goto_7
    return-void

    #@8
    .line 4615
    :cond_8
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@a
    if-eqz v0, :cond_22

    #@c
    .line 4616
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@e
    invoke-virtual {v0}, Landroid/webkit/WebView;->getMeasuredHeight()I

    #@11
    move-result v0

    #@12
    iget v1, p0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@14
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@17
    move-result v1

    #@18
    if-ne v0, v1, :cond_1c

    #@1a
    if-eqz p1, :cond_7

    #@1c
    .line 4618
    :cond_1c
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@1e
    invoke-virtual {v0}, Landroid/webkit/WebView;->requestLayout()V

    #@21
    goto :goto_7

    #@22
    .line 4620
    :cond_22
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mWidthCanMeasure:Z

    #@24
    if-eqz v0, :cond_3c

    #@26
    .line 4621
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@28
    invoke-virtual {v0}, Landroid/webkit/WebView;->getMeasuredWidth()I

    #@2b
    move-result v0

    #@2c
    iget v1, p0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@2e
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@31
    move-result v1

    #@32
    if-ne v0, v1, :cond_36

    #@34
    if-eqz p1, :cond_7

    #@36
    .line 4623
    :cond_36
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@38
    invoke-virtual {v0}, Landroid/webkit/WebView;->requestLayout()V

    #@3b
    goto :goto_7

    #@3c
    .line 4628
    :cond_3c
    const/4 v0, 0x0

    #@3d
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->sendViewSizeZoom(Z)Z

    #@40
    goto :goto_7
.end method

.method private contentToViewRect(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .registers 7
    .parameter "x"

    #@0
    .prologue
    .line 3546
    new-instance v0, Landroid/graphics/Rect;

    #@2
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@4
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@7
    move-result v1

    #@8
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@a
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@d
    move-result v2

    #@e
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@10
    invoke-virtual {p0, v3}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@13
    move-result v3

    #@14
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@16
    invoke-virtual {p0, v4}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@19
    move-result v4

    #@1a
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    #@1d
    return-object v0
.end method

.method private copyToClipboard(Ljava/lang/String;)V
    .registers 6
    .parameter "text"

    #@0
    .prologue
    .line 10352
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "clipboard"

    #@4
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Landroid/content/ClipboardManager;

    #@a
    .line 10354
    .local v1, cm:Landroid/content/ClipboardManager;
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitle()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    invoke-static {v2, p1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@11
    move-result-object v0

    #@12
    .line 10355
    .local v0, clip:Landroid/content/ClipData;
    invoke-virtual {v1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@15
    .line 10356
    return-void
.end method

.method private decideScrollToTopBottom()V
    .registers 11

    #@0
    .prologue
    const/16 v9, 0x3e8

    #@2
    const/4 v8, 0x0

    #@3
    .line 6829
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsProperPointsFor2fingerFlicking:Z

    #@5
    if-nez v2, :cond_8

    #@7
    .line 6851
    :cond_7
    :goto_7
    return-void

    #@8
    .line 6833
    :cond_8
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a
    if-eqz v2, :cond_7

    #@c
    .line 6837
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@e
    iget v3, p0, Landroid/webkit/WebViewClassic;->mMaximumFling:I

    #@10
    int-to-float v3, v3

    #@11
    invoke-virtual {v2, v9, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@14
    .line 6838
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@16
    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getXVelocity()F

    #@19
    move-result v0

    #@1a
    .line 6839
    .local v0, vx:F
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@1c
    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getYVelocity()F

    #@1f
    move-result v1

    #@20
    .line 6840
    .local v1, vy:F
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    #@23
    move-result v2

    #@24
    float-to-double v2, v2

    #@25
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@28
    move-result v4

    #@29
    float-to-double v4, v4

    #@2a
    const-wide/high16 v6, 0x3ff8

    #@2c
    mul-double/2addr v4, v6

    #@2d
    cmpl-double v2, v2, v4

    #@2f
    if-lez v2, :cond_7

    #@31
    .line 6841
    const/high16 v2, 0x4461

    #@33
    cmpl-float v2, v1, v2

    #@35
    if-lez v2, :cond_3c

    #@37
    .line 6842
    iput v9, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@39
    .line 6843
    iput-boolean v8, p0, Landroid/webkit/WebViewClassic;->mConfirmMove:Z

    #@3b
    goto :goto_7

    #@3c
    .line 6845
    :cond_3c
    const/high16 v2, -0x3c1f

    #@3e
    cmpg-float v2, v1, v2

    #@40
    if-gez v2, :cond_7

    #@42
    .line 6846
    const/16 v2, 0x3e9

    #@44
    iput v2, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@46
    .line 6847
    iput-boolean v8, p0, Landroid/webkit/WebViewClassic;->mConfirmMove:Z

    #@48
    goto :goto_7
.end method

.method private destroyJava()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2610
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@3
    invoke-virtual {v1}, Landroid/webkit/CallbackProxy;->blockMessages()V

    #@6
    .line 2611
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mAccessibilityInjector:Landroid/webkit/AccessibilityInjector;

    #@8
    if-eqz v1, :cond_11

    #@a
    .line 2612
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mAccessibilityInjector:Landroid/webkit/AccessibilityInjector;

    #@c
    invoke-virtual {v1}, Landroid/webkit/AccessibilityInjector;->destroy()V

    #@f
    .line 2613
    iput-object v2, p0, Landroid/webkit/WebViewClassic;->mAccessibilityInjector:Landroid/webkit/AccessibilityInjector;

    #@11
    .line 2615
    :cond_11
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@13
    if-eqz v1, :cond_24

    #@15
    .line 2617
    monitor-enter p0

    #@16
    .line 2618
    :try_start_16
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@18
    .line 2619
    .local v0, webViewCore:Landroid/webkit/WebViewCore;
    const/4 v1, 0x0

    #@19
    iput-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@1b
    .line 2620
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->destroy()V

    #@1e
    .line 2621
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_16 .. :try_end_1f} :catchall_25

    #@1f
    .line 2623
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@21
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@24
    .line 2625
    .end local v0           #webViewCore:Landroid/webkit/WebViewCore;
    :cond_24
    return-void

    #@25
    .line 2621
    :catchall_25
    move-exception v1

    #@26
    :try_start_26
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_25

    #@27
    throw v1
.end method

.method private destroyNative()V
    .registers 4

    #@0
    .prologue
    .line 2628
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v1, :cond_5

    #@4
    .line 2637
    :goto_4
    return-void

    #@5
    .line 2629
    :cond_5
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@7
    .line 2630
    .local v0, nptr:I
    const/4 v1, 0x0

    #@8
    iput v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@a
    .line 2631
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@d
    move-result-object v1

    #@e
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    #@17
    move-result-object v2

    #@18
    if-ne v1, v2, :cond_1e

    #@1a
    .line 2633
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->nativeDestroy(I)V

    #@1d
    goto :goto_4

    #@1e
    .line 2635
    :cond_1e
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@20
    new-instance v2, Landroid/webkit/WebViewClassic$DestroyNativeRunnable;

    #@22
    invoke-direct {v2, v0}, Landroid/webkit/WebViewClassic$DestroyNativeRunnable;-><init>(I)V

    #@25
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@28
    goto :goto_4
.end method

.method public static disablePlatformNotifications()V
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2673
    const-class v2, Landroid/webkit/WebViewClassic;

    #@2
    monitor-enter v2

    #@3
    .line 2674
    const/4 v1, 0x0

    #@4
    :try_start_4
    sput-boolean v1, Landroid/webkit/WebViewClassic;->sNotificationsEnabled:Z

    #@6
    .line 2675
    invoke-static {}, Landroid/webkit/JniUtil;->getContext()Landroid/content/Context;

    #@9
    move-result-object v0

    #@a
    .line 2676
    .local v0, context:Landroid/content/Context;
    if-eqz v0, :cond_f

    #@c
    .line 2677
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->disableProxyListener(Landroid/content/Context;)V

    #@f
    .line 2678
    :cond_f
    monitor-exit v2

    #@10
    .line 2679
    return-void

    #@11
    .line 2678
    :catchall_11
    move-exception v1

    #@12
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_4 .. :try_end_13} :catchall_11

    #@13
    throw v1
.end method

.method private static declared-synchronized disableProxyListener(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 1951
    const-class v1, Landroid/webkit/WebViewClassic;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/WebViewClassic;->sProxyReceiver:Landroid/webkit/WebViewClassic$ProxyReceiver;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_16

    #@5
    if-nez v0, :cond_9

    #@7
    .line 1956
    :goto_7
    monitor-exit v1

    #@8
    return-void

    #@9
    .line 1954
    :cond_9
    :try_start_9
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@c
    move-result-object v0

    #@d
    sget-object v2, Landroid/webkit/WebViewClassic;->sProxyReceiver:Landroid/webkit/WebViewClassic$ProxyReceiver;

    #@f
    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@12
    .line 1955
    const/4 v0, 0x0

    #@13
    sput-object v0, Landroid/webkit/WebViewClassic;->sProxyReceiver:Landroid/webkit/WebViewClassic$ProxyReceiver;
    :try_end_15
    .catchall {:try_start_9 .. :try_end_15} :catchall_16

    #@15
    goto :goto_7

    #@16
    .line 1951
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1

    #@18
    throw v0
.end method

.method private dismissFullScreenMode()V
    .registers 2

    #@0
    .prologue
    .line 6814
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->inFullScreenMode()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 6815
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFullScreenHolder:Landroid/webkit/PluginFullScreenHolder;

    #@8
    invoke-virtual {v0}, Landroid/webkit/PluginFullScreenHolder;->hide()V

    #@b
    .line 6816
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mFullScreenHolder:Landroid/webkit/PluginFullScreenHolder;

    #@e
    .line 6817
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@11
    .line 6819
    :cond_11
    return-void
.end method

.method private displaySoftKeyboard(Z)V
    .registers 8
    .parameter "isTextView"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5556
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@3
    const-string v4, "input_method"

    #@5
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    #@b
    .line 5560
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@d
    invoke-virtual {v3}, Landroid/webkit/ZoomManager;->getScale()F

    #@10
    move-result v3

    #@11
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@13
    invoke-virtual {v4}, Landroid/webkit/ZoomManager;->getDefaultScale()F

    #@16
    move-result v4

    #@17
    cmpg-float v3, v3, v4

    #@19
    if-gez v3, :cond_3a

    #@1b
    const/4 v1, 0x1

    #@1c
    .line 5561
    .local v1, zoom:Z
    :goto_1c
    if-eqz v1, :cond_34

    #@1e
    .line 5562
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@20
    iget v4, p0, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@22
    int-to-float v4, v4

    #@23
    iget v5, p0, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@25
    int-to-float v5, v5

    #@26
    invoke-virtual {v3, v4, v5}, Landroid/webkit/ZoomManager;->setZoomCenter(FF)V

    #@29
    .line 5563
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2b
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2d
    invoke-virtual {v4}, Landroid/webkit/ZoomManager;->getDefaultScale()F

    #@30
    move-result v4

    #@31
    invoke-virtual {v3, v4, v2}, Landroid/webkit/ZoomManager;->setZoomScale(FZ)V

    #@34
    .line 5570
    :cond_34
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@36
    invoke-virtual {v0, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    #@39
    .line 5571
    return-void

    #@3a
    .end local v1           #zoom:Z
    :cond_3a
    move v1, v2

    #@3b
    .line 5560
    goto :goto_1c
.end method

.method private doDrag(II)Z
    .registers 22
    .parameter "deltaX"
    .parameter "deltaY"

    #@0
    .prologue
    .line 7671
    const/4 v12, 0x1

    #@1
    .line 7672
    .local v12, allDrag:Z
    or-int v2, p1, p2

    #@3
    if-eqz v2, :cond_a3

    #@5
    .line 7673
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@8
    move-result v5

    #@9
    .line 7674
    .local v5, oldX:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@c
    move-result v6

    #@d
    .line 7675
    .local v6, oldY:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollX()I

    #@10
    move-result v7

    #@11
    .line 7676
    .local v7, rangeX:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollY()I

    #@14
    move-result v8

    #@15
    .line 7677
    .local v8, rangeY:I
    move/from16 v0, p1

    #@17
    int-to-float v2, v0

    #@18
    move-object/from16 v0, p0

    #@1a
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@1c
    invoke-virtual {v3}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@1f
    move-result v3

    #@20
    mul-float/2addr v2, v3

    #@21
    float-to-double v2, v2

    #@22
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    #@25
    move-result-wide v2

    #@26
    double-to-int v13, v2

    #@27
    .line 7678
    .local v13, contentX:I
    move/from16 v0, p2

    #@29
    int-to-float v2, v0

    #@2a
    move-object/from16 v0, p0

    #@2c
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2e
    invoke-virtual {v3}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@31
    move-result v3

    #@32
    mul-float/2addr v2, v3

    #@33
    float-to-double v2, v2

    #@34
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    #@37
    move-result-wide v2

    #@38
    double-to-int v14, v2

    #@39
    .line 7681
    .local v14, contentY:I
    const/4 v2, 0x3

    #@3a
    move-object/from16 v0, p0

    #@3c
    iput v2, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@3e
    .line 7684
    move-object/from16 v0, p0

    #@40
    iget-boolean v2, v0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@42
    if-eqz v2, :cond_ab

    #@44
    move-object/from16 v0, p0

    #@46
    iget-boolean v2, v0, Landroid/webkit/WebViewClassic;->mTouchInEditText:Z

    #@48
    if-eqz v2, :cond_ab

    #@4a
    invoke-direct/range {p0 .. p2}, Landroid/webkit/WebViewClassic;->canTextScroll(II)Z

    #@4d
    move-result v2

    #@4e
    if-eqz v2, :cond_ab

    #@50
    .line 7686
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTextScrollX()I

    #@53
    move-result v5

    #@54
    .line 7687
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollX()I

    #@57
    move-result v7

    #@58
    .line 7688
    move/from16 p1, v13

    #@5a
    .line 7689
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTextScrollY()I

    #@5d
    move-result v6

    #@5e
    .line 7690
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollY()I

    #@61
    move-result v8

    #@62
    .line 7691
    move/from16 p2, v14

    #@64
    .line 7692
    const/16 v2, 0xa

    #@66
    move-object/from16 v0, p0

    #@68
    iput v2, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@6a
    .line 7693
    const/4 v12, 0x0

    #@6b
    .line 7719
    :cond_6b
    :goto_6b
    move-object/from16 v0, p0

    #@6d
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@6f
    if-eqz v2, :cond_7c

    #@71
    .line 7720
    move-object/from16 v0, p0

    #@73
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@75
    move/from16 v0, p1

    #@77
    move/from16 v1, p2

    #@79
    invoke-virtual {v2, v0, v1}, Landroid/webkit/OverScrollGlow;->setOverScrollDeltas(II)V

    #@7c
    .line 7723
    :cond_7c
    move-object/from16 v0, p0

    #@7e
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@80
    move-object/from16 v0, p0

    #@82
    iget v9, v0, Landroid/webkit/WebViewClassic;->mOverscrollDistance:I

    #@84
    move-object/from16 v0, p0

    #@86
    iget v10, v0, Landroid/webkit/WebViewClassic;->mOverscrollDistance:I

    #@88
    const/4 v11, 0x1

    #@89
    move/from16 v3, p1

    #@8b
    move/from16 v4, p2

    #@8d
    invoke-virtual/range {v2 .. v11}, Landroid/webkit/WebView$PrivateAccess;->overScrollBy(IIIIIIIIZ)V

    #@90
    .line 7726
    move-object/from16 v0, p0

    #@92
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@94
    if-eqz v2, :cond_a3

    #@96
    move-object/from16 v0, p0

    #@98
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@9a
    invoke-virtual {v2}, Landroid/webkit/OverScrollGlow;->isAnimating()Z

    #@9d
    move-result v2

    #@9e
    if-eqz v2, :cond_a3

    #@a0
    .line 7727
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@a3
    .line 7730
    .end local v5           #oldX:I
    .end local v6           #oldY:I
    .end local v7           #rangeX:I
    .end local v8           #rangeY:I
    .end local v13           #contentX:I
    .end local v14           #contentY:I
    :cond_a3
    move-object/from16 v0, p0

    #@a5
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@a7
    invoke-virtual {v2}, Landroid/webkit/ZoomManager;->keepZoomPickerVisible()V

    #@aa
    .line 7731
    return v12

    #@ab
    .line 7694
    .restart local v5       #oldX:I
    .restart local v6       #oldY:I
    .restart local v7       #rangeX:I
    .restart local v8       #rangeY:I
    .restart local v13       #contentX:I
    .restart local v14       #contentY:I
    :cond_ab
    move-object/from16 v0, p0

    #@ad
    iget v2, v0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@af
    if-eqz v2, :cond_6b

    #@b1
    .line 7697
    move-object/from16 v0, p0

    #@b3
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@b5
    iget v15, v2, Landroid/graphics/Rect;->right:I

    #@b7
    .line 7698
    .local v15, maxX:I
    move-object/from16 v0, p0

    #@b9
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@bb
    iget v0, v2, Landroid/graphics/Rect;->bottom:I

    #@bd
    move/from16 v16, v0

    #@bf
    .line 7699
    .local v16, maxY:I
    const/4 v2, 0x0

    #@c0
    move-object/from16 v0, p0

    #@c2
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@c4
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@c6
    add-int/2addr v3, v13

    #@c7
    invoke-static {v15, v2, v3}, Landroid/webkit/WebViewClassic;->clampBetween(III)I

    #@ca
    move-result v17

    #@cb
    .line 7701
    .local v17, resultX:I
    const/4 v2, 0x0

    #@cc
    move-object/from16 v0, p0

    #@ce
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@d0
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@d2
    add-int/2addr v3, v14

    #@d3
    move/from16 v0, v16

    #@d5
    invoke-static {v0, v2, v3}, Landroid/webkit/WebViewClassic;->clampBetween(III)I

    #@d8
    move-result v18

    #@d9
    .line 7704
    .local v18, resultY:I
    move-object/from16 v0, p0

    #@db
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@dd
    iget v2, v2, Landroid/graphics/Rect;->left:I

    #@df
    move/from16 v0, v17

    #@e1
    if-ne v0, v2, :cond_f1

    #@e3
    move-object/from16 v0, p0

    #@e5
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@e7
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@e9
    move/from16 v0, v18

    #@eb
    if-ne v0, v2, :cond_f1

    #@ed
    or-int v2, v13, v14

    #@ef
    if-nez v2, :cond_6b

    #@f1
    .line 7708
    :cond_f1
    const/16 v2, 0x9

    #@f3
    move-object/from16 v0, p0

    #@f5
    iput v2, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@f7
    .line 7709
    move/from16 p1, v13

    #@f9
    .line 7710
    move/from16 p2, v14

    #@fb
    .line 7711
    move-object/from16 v0, p0

    #@fd
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@ff
    iget v5, v2, Landroid/graphics/Rect;->left:I

    #@101
    .line 7712
    move-object/from16 v0, p0

    #@103
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@105
    iget v6, v2, Landroid/graphics/Rect;->top:I

    #@107
    .line 7713
    move v7, v15

    #@108
    .line 7714
    move/from16 v8, v16

    #@10a
    .line 7715
    const/4 v12, 0x0

    #@10b
    goto/16 :goto_6b
.end method

.method private doFling()V
    .registers 31

    #@0
    .prologue
    .line 8274
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@4
    if-nez v3, :cond_7

    #@6
    .line 8393
    :cond_6
    :goto_6
    return-void

    #@7
    .line 8277
    :cond_7
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollX()I

    #@a
    move-result v7

    #@b
    .line 8278
    .local v7, maxX:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollY()I

    #@e
    move-result v9

    #@f
    .line 8280
    .local v9, maxY:I
    move-object/from16 v0, p0

    #@11
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@13
    const/16 v6, 0x3e8

    #@15
    move-object/from16 v0, p0

    #@17
    iget v8, v0, Landroid/webkit/WebViewClassic;->mMaximumFling:I

    #@19
    int-to-float v8, v8

    #@1a
    invoke-virtual {v3, v6, v8}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@1d
    .line 8281
    move-object/from16 v0, p0

    #@1f
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@21
    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getXVelocity()F

    #@24
    move-result v3

    #@25
    float-to-int v0, v3

    #@26
    move/from16 v28, v0

    #@28
    .line 8282
    .local v28, vx:I
    move-object/from16 v0, p0

    #@2a
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2c
    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getYVelocity()F

    #@2f
    move-result v3

    #@30
    float-to-int v0, v3

    #@31
    move/from16 v29, v0

    #@33
    .line 8285
    .local v29, vy:I
    move/from16 v24, v28

    #@35
    .line 8288
    .local v24, initialXVelocity:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@38
    move-result v4

    #@39
    .line 8289
    .local v4, scrollX:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@3c
    move-result v5

    #@3d
    .line 8290
    .local v5, scrollY:I
    move-object/from16 v0, p0

    #@3f
    iget v0, v0, Landroid/webkit/WebViewClassic;->mOverscrollDistance:I

    #@41
    move/from16 v25, v0

    #@43
    .line 8291
    .local v25, overscrollDistance:I
    move-object/from16 v0, p0

    #@45
    iget v0, v0, Landroid/webkit/WebViewClassic;->mOverflingDistance:I

    #@47
    move/from16 v20, v0

    #@49
    .line 8294
    .local v20, overflingDistance:I
    move-object/from16 v0, p0

    #@4b
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@4d
    const/16 v6, 0x9

    #@4f
    if-ne v3, v6, :cond_a7

    #@51
    .line 8295
    move-object/from16 v0, p0

    #@53
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@55
    iget v4, v3, Landroid/graphics/Rect;->left:I

    #@57
    .line 8296
    move-object/from16 v0, p0

    #@59
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@5b
    iget v5, v3, Landroid/graphics/Rect;->top:I

    #@5d
    .line 8297
    move-object/from16 v0, p0

    #@5f
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@61
    iget v7, v3, Landroid/graphics/Rect;->right:I

    #@63
    .line 8298
    move-object/from16 v0, p0

    #@65
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@67
    iget v9, v3, Landroid/graphics/Rect;->bottom:I

    #@69
    .line 8300
    const/16 v20, 0x0

    #@6b
    move/from16 v25, v20

    #@6d
    .line 8310
    :cond_6d
    :goto_6d
    move-object/from16 v0, p0

    #@6f
    iget v3, v0, Landroid/webkit/WebViewClassic;->mSnapScrollMode:I

    #@71
    if-eqz v3, :cond_7e

    #@73
    .line 8311
    move-object/from16 v0, p0

    #@75
    iget v3, v0, Landroid/webkit/WebViewClassic;->mSnapScrollMode:I

    #@77
    and-int/lit8 v3, v3, 0x2

    #@79
    const/4 v6, 0x2

    #@7a
    if-ne v3, v6, :cond_c4

    #@7c
    .line 8312
    const/16 v29, 0x0

    #@7e
    .line 8317
    :cond_7e
    :goto_7e
    if-nez v7, :cond_82

    #@80
    if-eqz v29, :cond_86

    #@82
    :cond_82
    if-nez v9, :cond_c7

    #@84
    if-nez v28, :cond_c7

    #@86
    .line 8318
    :cond_86
    invoke-static {}, Landroid/webkit/WebViewCore;->resumePriority()V

    #@89
    .line 8319
    move-object/from16 v0, p0

    #@8b
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@8d
    if-nez v3, :cond_96

    #@8f
    .line 8320
    move-object/from16 v0, p0

    #@91
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@93
    invoke-static {v3}, Landroid/webkit/WebViewCore;->resumeUpdatePicture(Landroid/webkit/WebViewCore;)V

    #@96
    .line 8322
    :cond_96
    move-object/from16 v0, p0

    #@98
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@9a
    const/4 v6, 0x0

    #@9b
    const/4 v8, 0x0

    #@9c
    invoke-virtual/range {v3 .. v9}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@9f
    move-result v3

    #@a0
    if-eqz v3, :cond_6

    #@a2
    .line 8323
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@a5
    goto/16 :goto_6

    #@a7
    .line 8301
    :cond_a7
    move-object/from16 v0, p0

    #@a9
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@ab
    const/16 v6, 0xa

    #@ad
    if-ne v3, v6, :cond_6d

    #@af
    .line 8302
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTextScrollX()I

    #@b2
    move-result v4

    #@b3
    .line 8303
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTextScrollY()I

    #@b6
    move-result v5

    #@b7
    .line 8304
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollX()I

    #@ba
    move-result v7

    #@bb
    .line 8305
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollY()I

    #@be
    move-result v9

    #@bf
    .line 8307
    const/16 v20, 0x0

    #@c1
    move/from16 v25, v20

    #@c3
    goto :goto_6d

    #@c4
    .line 8314
    :cond_c4
    const/16 v28, 0x0

    #@c6
    goto :goto_7e

    #@c7
    .line 8327
    :cond_c7
    move-object/from16 v0, p0

    #@c9
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@cb
    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@ce
    move-result v22

    #@cf
    .line 8328
    .local v22, currentVelocity:F
    move/from16 v0, v28

    #@d1
    int-to-double v10, v0

    #@d2
    move/from16 v0, v29

    #@d4
    int-to-double v12, v0

    #@d5
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->hypot(DD)D

    #@d8
    move-result-wide v10

    #@d9
    double-to-float v0, v10

    #@da
    move/from16 v27, v0

    #@dc
    .line 8329
    .local v27, velocity:F
    move-object/from16 v0, p0

    #@de
    iget v3, v0, Landroid/webkit/WebViewClassic;->mLastVelocity:F

    #@e0
    const/4 v6, 0x0

    #@e1
    cmpl-float v3, v3, v6

    #@e3
    if-lez v3, :cond_158

    #@e5
    const/4 v3, 0x0

    #@e6
    cmpl-float v3, v22, v3

    #@e8
    if-lez v3, :cond_158

    #@ea
    move-object/from16 v0, p0

    #@ec
    iget v3, v0, Landroid/webkit/WebViewClassic;->mLastVelocity:F

    #@ee
    const v6, 0x3e4ccccd

    #@f1
    mul-float/2addr v3, v6

    #@f2
    cmpl-float v3, v27, v3

    #@f4
    if-lez v3, :cond_158

    #@f6
    .line 8331
    move-object/from16 v0, p0

    #@f8
    iget v3, v0, Landroid/webkit/WebViewClassic;->mLastVelY:F

    #@fa
    float-to-double v10, v3

    #@fb
    move-object/from16 v0, p0

    #@fd
    iget v3, v0, Landroid/webkit/WebViewClassic;->mLastVelX:F

    #@ff
    float-to-double v12, v3

    #@100
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->atan2(DD)D

    #@103
    move-result-wide v10

    #@104
    move/from16 v0, v29

    #@106
    int-to-double v12, v0

    #@107
    move/from16 v0, v28

    #@109
    int-to-double v14, v0

    #@10a
    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->atan2(DD)D

    #@10d
    move-result-wide v12

    #@10e
    sub-double/2addr v10, v12

    #@10f
    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    #@112
    move-result-wide v10

    #@113
    double-to-float v0, v10

    #@114
    move/from16 v23, v0

    #@116
    .line 8333
    .local v23, deltaR:F
    const v21, 0x40c90fdb

    #@119
    .line 8334
    .local v21, circle:F
    const v3, 0x40b4f4ab

    #@11c
    cmpl-float v3, v23, v3

    #@11e
    if-gtz v3, :cond_127

    #@120
    const v3, 0x3f20d97c

    #@123
    cmpg-float v3, v23, v3

    #@125
    if-gez v3, :cond_158

    #@127
    .line 8335
    :cond_127
    move/from16 v0, v28

    #@129
    int-to-float v3, v0

    #@12a
    move-object/from16 v0, p0

    #@12c
    iget v6, v0, Landroid/webkit/WebViewClassic;->mLastVelX:F

    #@12e
    mul-float v6, v6, v22

    #@130
    move-object/from16 v0, p0

    #@132
    iget v8, v0, Landroid/webkit/WebViewClassic;->mLastVelocity:F

    #@134
    div-float/2addr v6, v8

    #@135
    add-float/2addr v3, v6

    #@136
    float-to-int v0, v3

    #@137
    move/from16 v28, v0

    #@139
    .line 8336
    move/from16 v0, v29

    #@13b
    int-to-float v3, v0

    #@13c
    move-object/from16 v0, p0

    #@13e
    iget v6, v0, Landroid/webkit/WebViewClassic;->mLastVelY:F

    #@140
    mul-float v6, v6, v22

    #@142
    move-object/from16 v0, p0

    #@144
    iget v8, v0, Landroid/webkit/WebViewClassic;->mLastVelocity:F

    #@146
    div-float/2addr v6, v8

    #@147
    add-float/2addr v3, v6

    #@148
    float-to-int v0, v3

    #@149
    move/from16 v29, v0

    #@14b
    .line 8337
    move/from16 v0, v28

    #@14d
    int-to-double v10, v0

    #@14e
    move/from16 v0, v29

    #@150
    int-to-double v12, v0

    #@151
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->hypot(DD)D

    #@154
    move-result-wide v10

    #@155
    double-to-float v0, v10

    #@156
    move/from16 v27, v0

    #@158
    .line 8353
    .end local v21           #circle:F
    .end local v23           #deltaR:F
    :cond_158
    sget-boolean v3, Landroid/webkit/WebViewClassic;->mCapptouchFlickNoti:Z

    #@15a
    if-eqz v3, :cond_166

    #@15c
    .line 8354
    move-object/from16 v0, p0

    #@15e
    move/from16 v1, v24

    #@160
    move/from16 v2, v29

    #@162
    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebViewClassic;->getFlickValue(II)I

    #@165
    move-result v29

    #@166
    .line 8359
    :cond_166
    if-eqz v4, :cond_16a

    #@168
    if-ne v4, v7, :cond_176

    #@16a
    :cond_16a
    invoke-static/range {v28 .. v28}, Ljava/lang/Math;->abs(I)I

    #@16d
    move-result v3

    #@16e
    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->abs(I)I

    #@171
    move-result v6

    #@172
    if-ge v3, v6, :cond_176

    #@174
    .line 8360
    const/16 v28, 0x0

    #@176
    .line 8362
    :cond_176
    if-eqz v5, :cond_17a

    #@178
    if-ne v5, v9, :cond_186

    #@17a
    :cond_17a
    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->abs(I)I

    #@17d
    move-result v3

    #@17e
    invoke-static/range {v28 .. v28}, Ljava/lang/Math;->abs(I)I

    #@181
    move-result v6

    #@182
    if-ge v3, v6, :cond_186

    #@184
    .line 8363
    const/16 v29, 0x0

    #@186
    .line 8366
    :cond_186
    move/from16 v0, v25

    #@188
    move/from16 v1, v20

    #@18a
    if-ge v0, v1, :cond_1aa

    #@18c
    .line 8367
    if-lez v28, :cond_193

    #@18e
    move/from16 v0, v25

    #@190
    neg-int v3, v0

    #@191
    if-eq v4, v3, :cond_199

    #@193
    :cond_193
    if-gez v28, :cond_19b

    #@195
    add-int v3, v7, v25

    #@197
    if-ne v4, v3, :cond_19b

    #@199
    .line 8369
    :cond_199
    const/16 v28, 0x0

    #@19b
    .line 8371
    :cond_19b
    if-lez v29, :cond_1a2

    #@19d
    move/from16 v0, v25

    #@19f
    neg-int v3, v0

    #@1a0
    if-eq v5, v3, :cond_1a8

    #@1a2
    :cond_1a2
    if-gez v29, :cond_1aa

    #@1a4
    add-int v3, v9, v25

    #@1a6
    if-ne v5, v3, :cond_1aa

    #@1a8
    .line 8373
    :cond_1a8
    const/16 v29, 0x0

    #@1aa
    .line 8377
    :cond_1aa
    move/from16 v0, v28

    #@1ac
    int-to-float v3, v0

    #@1ad
    move-object/from16 v0, p0

    #@1af
    iput v3, v0, Landroid/webkit/WebViewClassic;->mLastVelX:F

    #@1b1
    .line 8378
    move/from16 v0, v29

    #@1b3
    int-to-float v3, v0

    #@1b4
    move-object/from16 v0, p0

    #@1b6
    iput v3, v0, Landroid/webkit/WebViewClassic;->mLastVelY:F

    #@1b8
    .line 8379
    move/from16 v0, v27

    #@1ba
    move-object/from16 v1, p0

    #@1bc
    iput v0, v1, Landroid/webkit/WebViewClassic;->mLastVelocity:F

    #@1be
    .line 8382
    move-object/from16 v0, p0

    #@1c0
    iget-object v10, v0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@1c2
    move/from16 v0, v28

    #@1c4
    neg-int v13, v0

    #@1c5
    move/from16 v0, v29

    #@1c7
    neg-int v14, v0

    #@1c8
    const/4 v15, 0x0

    #@1c9
    const/16 v17, 0x0

    #@1cb
    if-nez v7, :cond_1f8

    #@1cd
    const/16 v19, 0x0

    #@1cf
    :goto_1cf
    move v11, v4

    #@1d0
    move v12, v5

    #@1d1
    move/from16 v16, v7

    #@1d3
    move/from16 v18, v9

    #@1d5
    invoke-virtual/range {v10 .. v20}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    #@1d8
    .line 8386
    move-object/from16 v0, p0

    #@1da
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@1dc
    if-eqz v3, :cond_1f3

    #@1de
    move-object/from16 v0, p0

    #@1e0
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@1e2
    if-nez v3, :cond_1f3

    #@1e4
    .line 8387
    move-object/from16 v0, p0

    #@1e6
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@1e8
    invoke-virtual {v3}, Landroid/widget/OverScroller;->getDuration()I

    #@1eb
    move-result v26

    #@1ec
    .line 8388
    .local v26, time:I
    move-object/from16 v0, p0

    #@1ee
    move/from16 v1, v26

    #@1f0
    invoke-direct {v0, v1}, Landroid/webkit/WebViewClassic;->showLGSelectActionPopupWindow(I)V

    #@1f3
    .line 8392
    .end local v26           #time:I
    :cond_1f3
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@1f6
    goto/16 :goto_6

    #@1f8
    :cond_1f8
    move/from16 v19, v20

    #@1fa
    .line 8382
    goto :goto_1cf
.end method

.method private doScrollToBottom()V
    .registers 5

    #@0
    .prologue
    .line 6858
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getContentHeight()I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@b
    move-result v1

    #@c
    const/4 v2, 0x1

    #@d
    const/16 v3, 0x1f4

    #@f
    invoke-direct {p0, v0, v1, v2, v3}, Landroid/webkit/WebViewClassic;->pinScrollTo(IIZI)Z

    #@12
    .line 6860
    return-void
.end method

.method private doScrollToTop()V
    .registers 5

    #@0
    .prologue
    .line 6854
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x0

    #@5
    const/4 v2, 0x1

    #@6
    const/16 v3, 0x1f4

    #@8
    invoke-direct {p0, v0, v1, v2, v3}, Landroid/webkit/WebViewClassic;->pinScrollTo(IIZI)Z

    #@b
    .line 6855
    return-void
.end method

.method private doTrackball(JI)V
    .registers 29
    .parameter "time"
    .parameter "metaState"

    #@0
    .prologue
    .line 8052
    move-object/from16 v0, p0

    #@2
    iget-wide v5, v0, Landroid/webkit/WebViewClassic;->mTrackballLastTime:J

    #@4
    move-object/from16 v0, p0

    #@6
    iget-wide v7, v0, Landroid/webkit/WebViewClassic;->mTrackballFirstTime:J

    #@8
    sub-long/2addr v5, v7

    #@9
    long-to-int v12, v5

    #@a
    .line 8053
    .local v12, elapsed:I
    if-nez v12, :cond_e

    #@c
    .line 8054
    const/16 v12, 0xc8

    #@e
    .line 8056
    :cond_e
    move-object/from16 v0, p0

    #@10
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTrackballRemainsX:F

    #@12
    const/high16 v5, 0x447a

    #@14
    mul-float/2addr v3, v5

    #@15
    int-to-float v5, v12

    #@16
    div-float v22, v3, v5

    #@18
    .line 8057
    .local v22, xRate:F
    move-object/from16 v0, p0

    #@1a
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTrackballRemainsY:F

    #@1c
    const/high16 v5, 0x447a

    #@1e
    mul-float/2addr v3, v5

    #@1f
    int-to-float v5, v12

    #@20
    div-float v24, v3, v5

    #@22
    .line 8058
    .local v24, yRate:F
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@25
    move-result v19

    #@26
    .line 8059
    .local v19, viewWidth:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getViewHeight()I

    #@29
    move-result v18

    #@2a
    .line 8060
    .local v18, viewHeight:I
    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->abs(F)F

    #@2d
    move-result v9

    #@2e
    .line 8061
    .local v9, ax:F
    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(F)F

    #@31
    move-result v10

    #@32
    .line 8062
    .local v10, ay:F
    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    #@35
    move-result v15

    #@36
    .line 8070
    .local v15, maxA:F
    move-object/from16 v0, p0

    #@38
    iget v3, v0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@3a
    sub-int v20, v3, v19

    #@3c
    .line 8071
    .local v20, width:I
    move-object/from16 v0, p0

    #@3e
    iget v3, v0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@40
    sub-int v13, v3, v18

    #@42
    .line 8072
    .local v13, height:I
    if-gez v20, :cond_46

    #@44
    const/16 v20, 0x0

    #@46
    .line 8073
    :cond_46
    if-gez v13, :cond_49

    #@48
    const/4 v13, 0x0

    #@49
    .line 8074
    :cond_49
    move-object/from16 v0, p0

    #@4b
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTrackballRemainsX:F

    #@4d
    const/high16 v5, 0x4040

    #@4f
    mul-float/2addr v3, v5

    #@50
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    #@53
    move-result v9

    #@54
    .line 8075
    move-object/from16 v0, p0

    #@56
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTrackballRemainsY:F

    #@58
    const/high16 v5, 0x4040

    #@5a
    mul-float/2addr v3, v5

    #@5b
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    #@5e
    move-result v10

    #@5f
    .line 8076
    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    #@62
    move-result v15

    #@63
    .line 8077
    const/4 v3, 0x0

    #@64
    float-to-int v5, v15

    #@65
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    #@68
    move-result v11

    #@69
    .line 8078
    .local v11, count:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@6c
    move-result v16

    #@6d
    .line 8079
    .local v16, oldScrollX:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@70
    move-result v17

    #@71
    .line 8080
    .local v17, oldScrollY:I
    if-lez v11, :cond_c3

    #@73
    .line 8081
    cmpg-float v3, v9, v10

    #@75
    if-gez v3, :cond_a1

    #@77
    move-object/from16 v0, p0

    #@79
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTrackballRemainsY:F

    #@7b
    const/4 v5, 0x0

    #@7c
    cmpg-float v3, v3, v5

    #@7e
    if-gez v3, :cond_9e

    #@80
    const/16 v4, 0x13

    #@82
    .line 8085
    .local v4, selectKeyCode:I
    :goto_82
    const/16 v3, 0xa

    #@84
    invoke-static {v11, v3}, Ljava/lang/Math;->min(II)I

    #@87
    move-result v11

    #@88
    .line 8092
    move-object/from16 v0, p0

    #@8a
    iget v3, v0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@8c
    if-eqz v3, :cond_ba

    #@8e
    .line 8093
    const/4 v14, 0x0

    #@8f
    .local v14, i:I
    :goto_8f
    if-ge v14, v11, :cond_b0

    #@91
    .line 8094
    const/4 v7, 0x1

    #@92
    move-object/from16 v3, p0

    #@94
    move-wide/from16 v5, p1

    #@96
    move/from16 v8, p3

    #@98
    invoke-direct/range {v3 .. v8}, Landroid/webkit/WebViewClassic;->letPageHandleNavKey(IJZI)V

    #@9b
    .line 8093
    add-int/lit8 v14, v14, 0x1

    #@9d
    goto :goto_8f

    #@9e
    .line 8081
    .end local v4           #selectKeyCode:I
    .end local v14           #i:I
    :cond_9e
    const/16 v4, 0x14

    #@a0
    goto :goto_82

    #@a1
    :cond_a1
    move-object/from16 v0, p0

    #@a3
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTrackballRemainsX:F

    #@a5
    const/4 v5, 0x0

    #@a6
    cmpg-float v3, v3, v5

    #@a8
    if-gez v3, :cond_ad

    #@aa
    const/16 v4, 0x15

    #@ac
    goto :goto_82

    #@ad
    :cond_ad
    const/16 v4, 0x16

    #@af
    goto :goto_82

    #@b0
    .line 8096
    .restart local v4       #selectKeyCode:I
    .restart local v14       #i:I
    :cond_b0
    const/4 v7, 0x0

    #@b1
    move-object/from16 v3, p0

    #@b3
    move-wide/from16 v5, p1

    #@b5
    move/from16 v8, p3

    #@b7
    invoke-direct/range {v3 .. v8}, Landroid/webkit/WebViewClassic;->letPageHandleNavKey(IJZI)V

    #@ba
    .line 8098
    .end local v14           #i:I
    :cond_ba
    const/4 v3, 0x0

    #@bb
    move-object/from16 v0, p0

    #@bd
    iput v3, v0, Landroid/webkit/WebViewClassic;->mTrackballRemainsY:F

    #@bf
    move-object/from16 v0, p0

    #@c1
    iput v3, v0, Landroid/webkit/WebViewClassic;->mTrackballRemainsX:F

    #@c3
    .line 8100
    .end local v4           #selectKeyCode:I
    :cond_c3
    const/4 v3, 0x5

    #@c4
    if-lt v11, v3, :cond_10b

    #@c6
    .line 8101
    move-object/from16 v0, p0

    #@c8
    move/from16 v1, v22

    #@ca
    move/from16 v2, v20

    #@cc
    invoke-direct {v0, v1, v2}, Landroid/webkit/WebViewClassic;->scaleTrackballX(FI)I

    #@cf
    move-result v21

    #@d0
    .line 8102
    .local v21, xMove:I
    move-object/from16 v0, p0

    #@d2
    move/from16 v1, v24

    #@d4
    invoke-direct {v0, v1, v13}, Landroid/webkit/WebViewClassic;->scaleTrackballY(FI)I

    #@d7
    move-result v23

    #@d8
    .line 8111
    .local v23, yMove:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@db
    move-result v3

    #@dc
    sub-int v3, v3, v16

    #@de
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    #@e1
    move-result v3

    #@e2
    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->abs(I)I

    #@e5
    move-result v5

    #@e6
    if-le v3, v5, :cond_ea

    #@e8
    .line 8112
    const/16 v21, 0x0

    #@ea
    .line 8114
    :cond_ea
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@ed
    move-result v3

    #@ee
    sub-int v3, v3, v17

    #@f0
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    #@f3
    move-result v3

    #@f4
    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(I)I

    #@f7
    move-result v5

    #@f8
    if-le v3, v5, :cond_fc

    #@fa
    .line 8115
    const/16 v23, 0x0

    #@fc
    .line 8117
    :cond_fc
    if-nez v21, :cond_100

    #@fe
    if-eqz v23, :cond_10b

    #@100
    .line 8118
    :cond_100
    const/4 v3, 0x1

    #@101
    const/4 v5, 0x0

    #@102
    move-object/from16 v0, p0

    #@104
    move/from16 v1, v21

    #@106
    move/from16 v2, v23

    #@108
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/webkit/WebViewClassic;->pinScrollBy(IIZI)Z

    #@10b
    .line 8121
    .end local v21           #xMove:I
    .end local v23           #yMove:I
    :cond_10b
    return-void
.end method

.method private drawContent(Landroid/graphics/Canvas;)V
    .registers 15
    .parameter "canvas"

    #@0
    .prologue
    .line 4794
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@2
    if-eqz v0, :cond_19

    #@4
    .line 4795
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@6
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->getScale()F

    #@9
    move-result v0

    #@a
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@c
    invoke-virtual {v1}, Landroid/webkit/ZoomManager;->getScale()F

    #@f
    move-result v1

    #@10
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    #@13
    .line 4796
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHistoryPicture:Landroid/graphics/Picture;

    #@15
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawPicture(Landroid/graphics/Picture;)V

    #@18
    .line 4877
    :cond_18
    :goto_18
    return-void

    #@19
    .line 4799
    :cond_19
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@1b
    if-eqz v0, :cond_18

    #@1d
    .line 4801
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@1f
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->isFixedLengthAnimationInProgress()Z

    #@22
    move-result v9

    #@23
    .line 4802
    .local v9, animateZoom:Z
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@25
    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_2f

    #@2b
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2d
    if-eqz v0, :cond_ef

    #@2f
    :cond_2f
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@31
    const/4 v1, 0x3

    #@32
    if-ne v0, v1, :cond_39

    #@34
    iget v0, p0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@36
    const/4 v1, 0x2

    #@37
    if-eq v0, v1, :cond_ef

    #@39
    :cond_39
    const/4 v8, 0x1

    #@3a
    .line 4806
    .local v8, animateScroll:Z
    :goto_3a
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@3c
    const/4 v1, 0x3

    #@3d
    if-ne v0, v1, :cond_64

    #@3f
    .line 4807
    iget v0, p0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@41
    const/4 v1, 0x1

    #@42
    if-ne v0, v1, :cond_4e

    #@44
    .line 4808
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@46
    const/16 v1, 0x8

    #@48
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@4b
    .line 4809
    const/4 v0, 0x0

    #@4c
    iput v0, p0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@4e
    .line 4811
    :cond_4e
    iget v0, p0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@50
    if-nez v0, :cond_64

    #@52
    .line 4812
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@54
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@56
    const/16 v4, 0x8

    #@58
    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@5b
    move-result-object v1

    #@5c
    const-wide/16 v4, 0x64

    #@5e
    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@61
    .line 4814
    const/4 v0, 0x1

    #@62
    iput v0, p0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@64
    .line 4817
    :cond_64
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@67
    move-result v12

    #@68
    .line 4818
    .local v12, saveCount:I
    if-eqz v9, :cond_f2

    #@6a
    .line 4819
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@6c
    invoke-virtual {v0, p1}, Landroid/webkit/ZoomManager;->animateZoom(Landroid/graphics/Canvas;)V

    #@6f
    .line 4824
    :cond_6f
    :goto_6f
    const/4 v7, 0x0

    #@70
    .line 4827
    .local v7, UIAnimationsRunning:Z
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@72
    if-eqz v0, :cond_8d

    #@74
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@77
    move-result v0

    #@78
    if-nez v0, :cond_8d

    #@7a
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@7c
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->nativeEvaluateLayersAnimations(I)Z

    #@7f
    move-result v0

    #@80
    if-eqz v0, :cond_8d

    #@82
    .line 4829
    const/4 v7, 0x1

    #@83
    .line 4834
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@85
    const/16 v1, 0xc4

    #@87
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@8a
    .line 4835
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@8d
    .line 4839
    :cond_8d
    const/4 v6, 0x0

    #@8e
    .line 4840
    .local v6, extras:I
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mFindIsUp:Z

    #@90
    if-nez v0, :cond_97

    #@92
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mShowTextSelectionExtra:Z

    #@94
    if-eqz v0, :cond_97

    #@96
    .line 4841
    const/4 v6, 0x1

    #@97
    .line 4844
    :cond_97
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mVisibleContentRect:Landroid/graphics/RectF;

    #@99
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->calcOurContentVisibleRectF(Landroid/graphics/RectF;)V

    #@9c
    .line 4845
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@9f
    move-result v0

    #@a0
    if-eqz v0, :cond_10d

    #@a2
    .line 4846
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsWebViewVisible:Z

    #@a4
    if-eqz v0, :cond_109

    #@a6
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInvScreenRect:Landroid/graphics/Rect;

    #@a8
    .line 4847
    .local v2, invScreenRect:Landroid/graphics/Rect;
    :goto_a8
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsWebViewVisible:Z

    #@aa
    if-eqz v0, :cond_10b

    #@ac
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mScreenRect:Landroid/graphics/Rect;

    #@ae
    .line 4849
    .local v3, screenRect:Landroid/graphics/Rect;
    :goto_ae
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@b0
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mVisibleContentRect:Landroid/graphics/RectF;

    #@b2
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScale()F

    #@b5
    move-result v5

    #@b6
    move-object v0, p0

    #@b7
    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebViewClassic;->nativeCreateDrawGLFunction(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/RectF;FI)I

    #@ba
    move-result v11

    #@bb
    .local v11, functor:I
    move-object v0, p1

    #@bc
    .line 4851
    check-cast v0, Landroid/view/HardwareCanvas;

    #@be
    invoke-virtual {v0, v11}, Landroid/view/HardwareCanvas;->callDrawGLFunction(I)I

    #@c1
    .line 4852
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mHardwareAccelSkia:Z

    #@c3
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@c6
    move-result-object v1

    #@c7
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getHardwareAccelSkiaEnabled()Z

    #@ca
    move-result v1

    #@cb
    if-eq v0, v1, :cond_dc

    #@cd
    .line 4853
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@d0
    move-result-object v0

    #@d1
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getHardwareAccelSkiaEnabled()Z

    #@d4
    move-result v0

    #@d5
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mHardwareAccelSkia:Z

    #@d7
    .line 4854
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mHardwareAccelSkia:Z

    #@d9
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->nativeUseHardwareAccelSkia(Z)V

    #@dc
    .line 4869
    .end local v2           #invScreenRect:Landroid/graphics/Rect;
    .end local v3           #screenRect:Landroid/graphics/Rect;
    .end local v11           #functor:I
    :cond_dc
    :goto_dc
    invoke-virtual {p1, v12}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@df
    .line 4870
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->drawTextSelectionHandles(Landroid/graphics/Canvas;)V

    #@e2
    .line 4872
    const/4 v0, 0x2

    #@e3
    if-ne v6, v0, :cond_18

    #@e5
    .line 4873
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@e7
    const/4 v1, 0x4

    #@e8
    if-ne v0, v1, :cond_18

    #@ea
    .line 4874
    const/4 v0, 0x5

    #@eb
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@ed
    goto/16 :goto_18

    #@ef
    .line 4802
    .end local v6           #extras:I
    .end local v7           #UIAnimationsRunning:Z
    .end local v8           #animateScroll:Z
    .end local v12           #saveCount:I
    :cond_ef
    const/4 v8, 0x0

    #@f0
    goto/16 :goto_3a

    #@f2
    .line 4820
    .restart local v8       #animateScroll:Z
    .restart local v12       #saveCount:I
    :cond_f2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@f5
    move-result v0

    #@f6
    if-nez v0, :cond_6f

    #@f8
    .line 4821
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@fa
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->getScale()F

    #@fd
    move-result v0

    #@fe
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@100
    invoke-virtual {v1}, Landroid/webkit/ZoomManager;->getScale()F

    #@103
    move-result v1

    #@104
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    #@107
    goto/16 :goto_6f

    #@109
    .line 4846
    .restart local v6       #extras:I
    .restart local v7       #UIAnimationsRunning:Z
    :cond_109
    const/4 v2, 0x0

    #@10a
    goto :goto_a8

    #@10b
    .line 4847
    .restart local v2       #invScreenRect:Landroid/graphics/Rect;
    :cond_10b
    const/4 v3, 0x0

    #@10c
    goto :goto_ae

    #@10d
    .line 4858
    .end local v2           #invScreenRect:Landroid/graphics/Rect;
    :cond_10d
    const/4 v10, 0x0

    #@10e
    .line 4859
    .local v10, df:Landroid/graphics/DrawFilter;
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@110
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->isZoomAnimating()Z

    #@113
    move-result v0

    #@114
    if-nez v0, :cond_118

    #@116
    if-eqz v7, :cond_129

    #@118
    .line 4860
    :cond_118
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mZoomFilter:Landroid/graphics/DrawFilter;

    #@11a
    .line 4864
    :cond_11a
    :goto_11a
    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    #@11d
    .line 4865
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mVisibleContentRect:Landroid/graphics/RectF;

    #@11f
    iget v1, p0, Landroid/webkit/WebViewClassic;->mBackgroundColor:I

    #@121
    invoke-direct {p0, p1, v0, v1, v6}, Landroid/webkit/WebViewClassic;->nativeDraw(Landroid/graphics/Canvas;Landroid/graphics/RectF;II)V

    #@124
    .line 4866
    const/4 v0, 0x0

    #@125
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    #@128
    goto :goto_dc

    #@129
    .line 4861
    :cond_129
    if-eqz v8, :cond_11a

    #@12b
    .line 4862
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mScrollFilter:Landroid/graphics/DrawFilter;

    #@12d
    goto :goto_11a
.end method

.method private drawHandle(Landroid/graphics/Point;ILandroid/graphics/Rect;ILandroid/graphics/Canvas;)V
    .registers 19
    .parameter "point"
    .parameter "handleId"
    .parameter "bounds"
    .parameter "alpha"
    .parameter "canvas"

    #@0
    .prologue
    .line 5333
    iget v10, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    invoke-static {v10, p2}, Landroid/webkit/WebViewClassic;->nativeIsHandleLeft(II)Z

    #@5
    move-result v4

    #@6
    .line 5334
    .local v4, isLeft:Z
    if-eqz v4, :cond_5c

    #@8
    .line 5335
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    #@a
    .line 5336
    .local v1, drawable:Landroid/graphics/drawable/Drawable;
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    #@c
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@f
    move-result v7

    #@10
    .line 5337
    .local v7, width:I
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    #@12
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@15
    move-result v3

    #@16
    .line 5339
    .local v3, height:I
    mul-int/lit8 v10, v7, 0x3

    #@18
    div-int/lit8 v5, v10, 0x4

    #@1a
    .line 5341
    .local v5, offset:I
    iget v10, p1, Landroid/graphics/Point;->x:I

    #@1c
    invoke-virtual {p0, v10}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@1f
    move-result v10

    #@20
    sub-int v6, v10, v5

    #@22
    .line 5342
    .local v6, sx:I
    if-gez v6, :cond_34

    #@24
    .line 5343
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectHandleLeftReversed:Landroid/graphics/drawable/Drawable;

    #@26
    .line 5344
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectHandleLeftReversed:Landroid/graphics/drawable/Drawable;

    #@28
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@2b
    move-result v7

    #@2c
    .line 5345
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectHandleLeftReversed:Landroid/graphics/drawable/Drawable;

    #@2e
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@31
    move-result v3

    #@32
    .line 5347
    div-int/lit8 v5, v7, 0x4

    #@34
    .line 5367
    .end local v6           #sx:I
    :cond_34
    :goto_34
    iget v10, p1, Landroid/graphics/Point;->x:I

    #@36
    invoke-virtual {p0, v10}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@39
    move-result v8

    #@3a
    .line 5368
    .local v8, x:I
    iget v10, p1, Landroid/graphics/Point;->y:I

    #@3c
    invoke-virtual {p0, v10}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@3f
    move-result v9

    #@40
    .line 5369
    .local v9, y:I
    sub-int v10, v8, v5

    #@42
    sub-int v11, v8, v5

    #@44
    add-int/2addr v11, v7

    #@45
    add-int v12, v9, v3

    #@47
    move-object/from16 v0, p3

    #@49
    invoke-virtual {v0, v10, v9, v11, v12}, Landroid/graphics/Rect;->set(IIII)V

    #@4c
    .line 5370
    move-object/from16 v0, p3

    #@4e
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@51
    .line 5371
    move/from16 v0, p4

    #@53
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@56
    .line 5372
    move-object/from16 v0, p5

    #@58
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@5b
    .line 5373
    return-void

    #@5c
    .line 5351
    .end local v1           #drawable:Landroid/graphics/drawable/Drawable;
    .end local v3           #height:I
    .end local v5           #offset:I
    .end local v7           #width:I
    .end local v8           #x:I
    .end local v9           #y:I
    :cond_5c
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@5e
    .line 5352
    .restart local v1       #drawable:Landroid/graphics/drawable/Drawable;
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@60
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@63
    move-result v7

    #@64
    .line 5353
    .restart local v7       #width:I
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@66
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@69
    move-result v3

    #@6a
    .line 5355
    .restart local v3       #height:I
    div-int/lit8 v5, v7, 0x4

    #@6c
    .line 5357
    .restart local v5       #offset:I
    iget v10, p1, Landroid/graphics/Point;->x:I

    #@6e
    invoke-virtual {p0, v10}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@71
    move-result v10

    #@72
    sub-int/2addr v10, v5

    #@73
    add-int v2, v10, v7

    #@75
    .line 5358
    .local v2, ex:I
    iget v10, p0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@77
    invoke-virtual {p0, v10}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@7a
    move-result v10

    #@7b
    if-le v2, v10, :cond_34

    #@7d
    .line 5359
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRightReversed:Landroid/graphics/drawable/Drawable;

    #@7f
    .line 5360
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRightReversed:Landroid/graphics/drawable/Drawable;

    #@81
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@84
    move-result v7

    #@85
    .line 5361
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRightReversed:Landroid/graphics/drawable/Drawable;

    #@87
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@8a
    move-result v3

    #@8b
    .line 5363
    mul-int/lit8 v10, v7, 0x3

    #@8d
    div-int/lit8 v5, v10, 0x4

    #@8f
    goto :goto_34
.end method

.method private drawOverScrollBackground(Landroid/graphics/Canvas;)V
    .registers 12
    .parameter "canvas"

    #@0
    .prologue
    .line 4884
    sget-object v0, Landroid/webkit/WebViewClassic;->mOverScrollBackground:Landroid/graphics/Paint;

    #@2
    if-nez v0, :cond_42

    #@4
    .line 4885
    new-instance v0, Landroid/graphics/Paint;

    #@6
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@9
    sput-object v0, Landroid/webkit/WebViewClassic;->mOverScrollBackground:Landroid/graphics/Paint;

    #@b
    .line 4886
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@10
    move-result-object v0

    #@11
    const v1, 0x1080578

    #@14
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    #@17
    move-result-object v6

    #@18
    .line 4889
    .local v6, bm:Landroid/graphics/Bitmap;
    sget-object v0, Landroid/webkit/WebViewClassic;->mOverScrollBackground:Landroid/graphics/Paint;

    #@1a
    new-instance v1, Landroid/graphics/BitmapShader;

    #@1c
    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    #@1e
    sget-object v3, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    #@20
    invoke-direct {v1, v6, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    #@23
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    #@26
    .line 4891
    new-instance v0, Landroid/graphics/Paint;

    #@28
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@2b
    sput-object v0, Landroid/webkit/WebViewClassic;->mOverScrollBorder:Landroid/graphics/Paint;

    #@2d
    .line 4892
    sget-object v0, Landroid/webkit/WebViewClassic;->mOverScrollBorder:Landroid/graphics/Paint;

    #@2f
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@31
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@34
    .line 4893
    sget-object v0, Landroid/webkit/WebViewClassic;->mOverScrollBorder:Landroid/graphics/Paint;

    #@36
    const/4 v1, 0x0

    #@37
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@3a
    .line 4894
    sget-object v0, Landroid/webkit/WebViewClassic;->mOverScrollBorder:Landroid/graphics/Paint;

    #@3c
    const v1, -0x444445

    #@3f
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    #@42
    .line 4897
    .end local v6           #bm:Landroid/graphics/Bitmap;
    :cond_42
    const/4 v9, 0x0

    #@43
    .line 4898
    .local v9, top:I
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->computeRealHorizontalScrollRange()I

    #@46
    move-result v8

    #@47
    .line 4899
    .local v8, right:I
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->computeRealVerticalScrollRange()I

    #@4a
    move-result v0

    #@4b
    add-int v7, v9, v0

    #@4d
    .line 4901
    .local v7, bottom:I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@50
    .line 4902
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@53
    move-result v0

    #@54
    int-to-float v0, v0

    #@55
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@58
    move-result v1

    #@59
    int-to-float v1, v1

    #@5a
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    #@5d
    .line 4903
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@60
    move-result v0

    #@61
    neg-int v0, v0

    #@62
    int-to-float v1, v0

    #@63
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@66
    move-result v0

    #@67
    sub-int v0, v9, v0

    #@69
    int-to-float v2, v0

    #@6a
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@6d
    move-result v0

    #@6e
    sub-int v0, v8, v0

    #@70
    int-to-float v3, v0

    #@71
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@74
    move-result v0

    #@75
    sub-int v0, v7, v0

    #@77
    int-to-float v4, v0

    #@78
    sget-object v5, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    #@7a
    move-object v0, p1

    #@7b
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    #@7e
    .line 4905
    sget-object v0, Landroid/webkit/WebViewClassic;->mOverScrollBackground:Landroid/graphics/Paint;

    #@80
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    #@83
    .line 4906
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@86
    .line 4908
    const/high16 v1, -0x4080

    #@88
    const/4 v0, -0x1

    #@89
    int-to-float v2, v0

    #@8a
    int-to-float v3, v8

    #@8b
    int-to-float v4, v7

    #@8c
    sget-object v5, Landroid/webkit/WebViewClassic;->mOverScrollBorder:Landroid/graphics/Paint;

    #@8e
    move-object v0, p1

    #@8f
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@92
    .line 4910
    const/4 v0, 0x0

    #@93
    invoke-virtual {p1, v0, v9, v8, v7}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    #@96
    .line 4911
    return-void
.end method

.method private drawTextSelectionHandles(Landroid/graphics/Canvas;)V
    .registers 19
    .parameter "canvas"

    #@0
    .prologue
    .line 5377
    move-object/from16 v0, p0

    #@2
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mBaseAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@4
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;->getAlpha()I

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_25

    #@a
    move-object/from16 v0, p0

    #@c
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mExtentAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@e
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;->getAlpha()I

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_25

    #@14
    .line 5378
    move-object/from16 v0, p0

    #@16
    iget-boolean v1, v0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@18
    if-eqz v1, :cond_24

    #@1a
    move-object/from16 v0, p0

    #@1c
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@1e
    invoke-virtual {v1}, Landroid/webkit/WebViewCore;->inParagraphMode()Z

    #@21
    move-result v1

    #@22
    if-nez v1, :cond_25

    #@24
    .line 5447
    :cond_24
    :goto_24
    return-void

    #@25
    .line 5383
    :cond_25
    move-object/from16 v0, p0

    #@27
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mTextSelectionPaint:Landroid/graphics/Paint;

    #@29
    if-nez v1, :cond_3e

    #@2b
    .line 5384
    new-instance v1, Landroid/graphics/Paint;

    #@2d
    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    #@30
    move-object/from16 v0, p0

    #@32
    iput-object v1, v0, Landroid/webkit/WebViewClassic;->mTextSelectionPaint:Landroid/graphics/Paint;

    #@34
    .line 5385
    move-object/from16 v0, p0

    #@36
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mTextSelectionPaint:Landroid/graphics/Paint;

    #@38
    const v2, 0x6633b5e5

    #@3b
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    #@3e
    .line 5388
    :cond_3e
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->ensureSelectionHandles()V

    #@41
    .line 5389
    move-object/from16 v0, p0

    #@43
    iget-boolean v1, v0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@45
    if-eqz v1, :cond_ac

    #@47
    .line 5391
    move-object/from16 v0, p0

    #@49
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@4b
    iget v1, v1, Landroid/graphics/Point;->x:I

    #@4d
    move-object/from16 v0, p0

    #@4f
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@52
    move-result v1

    #@53
    move-object/from16 v0, p0

    #@55
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@57
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@5a
    move-result v2

    #@5b
    div-int/lit8 v2, v2, 0x2

    #@5d
    sub-int v15, v1, v2

    #@5f
    .line 5393
    .local v15, x:I
    move-object/from16 v0, p0

    #@61
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@63
    iget v1, v1, Landroid/graphics/Point;->y:I

    #@65
    move-object/from16 v0, p0

    #@67
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@6a
    move-result v16

    #@6b
    .line 5394
    .local v16, y:I
    move-object/from16 v0, p0

    #@6d
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandleBaseBounds:Landroid/graphics/Rect;

    #@6f
    move-object/from16 v0, p0

    #@71
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@73
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@76
    move-result v2

    #@77
    add-int/2addr v2, v15

    #@78
    move-object/from16 v0, p0

    #@7a
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@7c
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@7f
    move-result v3

    #@80
    add-int v3, v3, v16

    #@82
    move/from16 v0, v16

    #@84
    invoke-virtual {v1, v15, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@87
    .line 5397
    move-object/from16 v0, p0

    #@89
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@8b
    move-object/from16 v0, p0

    #@8d
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mSelectHandleBaseBounds:Landroid/graphics/Rect;

    #@8f
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@92
    .line 5398
    move-object/from16 v0, p0

    #@94
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@96
    move-object/from16 v0, p0

    #@98
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mBaseAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@9a
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;->getAlpha()I

    #@9d
    move-result v2

    #@9e
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@a1
    .line 5399
    move-object/from16 v0, p0

    #@a3
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@a5
    move-object/from16 v0, p1

    #@a7
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@aa
    goto/16 :goto_24

    #@ac
    .line 5402
    .end local v15           #x:I
    .end local v16           #y:I
    :cond_ac
    move-object/from16 v0, p0

    #@ae
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@b0
    invoke-virtual {v1}, Landroid/webkit/WebViewCore;->inParagraphMode()Z

    #@b3
    move-result v1

    #@b4
    if-eqz v1, :cond_19a

    #@b6
    .line 5403
    move-object/from16 v0, p0

    #@b8
    iget v1, v0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@ba
    move-object/from16 v0, p0

    #@bc
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@be
    move-object/from16 v0, p0

    #@c0
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@c2
    move-object/from16 v0, p0

    #@c4
    invoke-direct {v0, v1, v2, v3}, Landroid/webkit/WebViewClassic;->nativeGetSelectionParagraphBounds(ILandroid/graphics/Point;Landroid/graphics/Point;)Z

    #@c7
    .line 5405
    move-object/from16 v0, p0

    #@c9
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@cb
    iget v1, v1, Landroid/graphics/Point;->x:I

    #@cd
    move-object/from16 v0, p0

    #@cf
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@d2
    move-result v13

    #@d3
    .line 5406
    .local v13, start_x:I
    move-object/from16 v0, p0

    #@d5
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@d7
    iget v1, v1, Landroid/graphics/Point;->y:I

    #@d9
    move-object/from16 v0, p0

    #@db
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@de
    move-result v14

    #@df
    .line 5407
    .local v14, start_y:I
    move-object/from16 v0, p0

    #@e1
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@e3
    iget v1, v1, Landroid/graphics/Point;->x:I

    #@e5
    move-object/from16 v0, p0

    #@e7
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@ea
    move-result v8

    #@eb
    .line 5408
    .local v8, end_x:I
    move-object/from16 v0, p0

    #@ed
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@ef
    iget v1, v1, Landroid/graphics/Point;->y:I

    #@f1
    move-object/from16 v0, p0

    #@f3
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@f6
    move-result v9

    #@f7
    .line 5410
    .local v9, end_y:I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    #@fa
    move-result-object v7

    #@fb
    .line 5411
    .local v7, clip:Landroid/graphics/Rect;
    new-instance v12, Landroid/graphics/Rect;

    #@fd
    invoke-direct {v12, v13, v14, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    #@100
    .line 5412
    .local v12, r:Landroid/graphics/Rect;
    invoke-virtual {v12, v7}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    #@103
    move-result v1

    #@104
    if-eqz v1, :cond_10f

    #@106
    .line 5413
    move-object/from16 v0, p0

    #@108
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mTextSelectionPaint:Landroid/graphics/Paint;

    #@10a
    move-object/from16 v0, p1

    #@10c
    invoke-virtual {v0, v12, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@10f
    .line 5415
    :cond_10f
    move-object/from16 v0, p0

    #@111
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@113
    if-nez v1, :cond_128

    #@115
    .line 5416
    move-object/from16 v0, p0

    #@117
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@119
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@11c
    move-result-object v1

    #@11d
    const v2, 0x2020686

    #@120
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    #@123
    move-result-object v1

    #@124
    move-object/from16 v0, p0

    #@126
    iput-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@128
    .line 5420
    :cond_128
    move-object/from16 v0, p0

    #@12a
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@12c
    if-eqz v1, :cond_24

    #@12e
    .line 5422
    move-object/from16 v0, p0

    #@130
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@132
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    #@135
    move-result v1

    #@136
    div-int/lit8 v1, v1, 0x2

    #@138
    sub-int/2addr v13, v1

    #@139
    .line 5423
    move-object/from16 v0, p0

    #@13b
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@13d
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    #@140
    move-result v1

    #@141
    div-int/lit8 v1, v1, 0x2

    #@143
    sub-int/2addr v8, v1

    #@144
    .line 5424
    move-object/from16 v0, p0

    #@146
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@148
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    #@14b
    move-result v1

    #@14c
    div-int/lit8 v1, v1, 0x2

    #@14e
    sub-int/2addr v14, v1

    #@14f
    .line 5425
    move-object/from16 v0, p0

    #@151
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@153
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    #@156
    move-result v1

    #@157
    div-int/lit8 v1, v1, 0x2

    #@159
    sub-int/2addr v9, v1

    #@15a
    .line 5427
    add-int v1, v13, v8

    #@15c
    div-int/lit8 v10, v1, 0x2

    #@15e
    .line 5428
    .local v10, mx:I
    add-int v1, v14, v9

    #@160
    div-int/lit8 v11, v1, 0x2

    #@162
    .line 5431
    .local v11, my:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTranslateMode()Z

    #@165
    move-result v1

    #@166
    if-nez v1, :cond_24

    #@168
    .line 5432
    move-object/from16 v0, p0

    #@16a
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@16c
    int-to-float v2, v13

    #@16d
    int-to-float v3, v11

    #@16e
    const/4 v4, 0x0

    #@16f
    move-object/from16 v0, p1

    #@171
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@174
    .line 5433
    move-object/from16 v0, p0

    #@176
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@178
    int-to-float v2, v8

    #@179
    int-to-float v3, v11

    #@17a
    const/4 v4, 0x0

    #@17b
    move-object/from16 v0, p1

    #@17d
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@180
    .line 5434
    move-object/from16 v0, p0

    #@182
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@184
    int-to-float v2, v10

    #@185
    int-to-float v3, v14

    #@186
    const/4 v4, 0x0

    #@187
    move-object/from16 v0, p1

    #@189
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@18c
    .line 5435
    move-object/from16 v0, p0

    #@18e
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mSelectHandlePoint:Landroid/graphics/Bitmap;

    #@190
    int-to-float v2, v10

    #@191
    int-to-float v3, v9

    #@192
    const/4 v4, 0x0

    #@193
    move-object/from16 v0, p1

    #@195
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@198
    goto/16 :goto_24

    #@19a
    .line 5442
    .end local v7           #clip:Landroid/graphics/Rect;
    .end local v8           #end_x:I
    .end local v9           #end_y:I
    .end local v10           #mx:I
    .end local v11           #my:I
    .end local v12           #r:Landroid/graphics/Rect;
    .end local v13           #start_x:I
    .end local v14           #start_y:I
    :cond_19a
    move-object/from16 v0, p0

    #@19c
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@19e
    const/4 v3, 0x0

    #@19f
    move-object/from16 v0, p0

    #@1a1
    iget-object v4, v0, Landroid/webkit/WebViewClassic;->mSelectHandleBaseBounds:Landroid/graphics/Rect;

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mBaseAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@1a7
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;->getAlpha()I

    #@1aa
    move-result v5

    #@1ab
    move-object/from16 v1, p0

    #@1ad
    move-object/from16 v6, p1

    #@1af
    invoke-direct/range {v1 .. v6}, Landroid/webkit/WebViewClassic;->drawHandle(Landroid/graphics/Point;ILandroid/graphics/Rect;ILandroid/graphics/Canvas;)V

    #@1b2
    .line 5444
    move-object/from16 v0, p0

    #@1b4
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@1b6
    const/4 v3, 0x1

    #@1b7
    move-object/from16 v0, p0

    #@1b9
    iget-object v4, v0, Landroid/webkit/WebViewClassic;->mSelectHandleExtentBounds:Landroid/graphics/Rect;

    #@1bb
    move-object/from16 v0, p0

    #@1bd
    iget-object v1, v0, Landroid/webkit/WebViewClassic;->mExtentAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@1bf
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;->getAlpha()I

    #@1c2
    move-result v5

    #@1c3
    move-object/from16 v1, p0

    #@1c5
    move-object/from16 v6, p1

    #@1c7
    invoke-direct/range {v1 .. v6}, Landroid/webkit/WebViewClassic;->drawHandle(Landroid/graphics/Point;ILandroid/graphics/Rect;ILandroid/graphics/Canvas;)V

    #@1ca
    goto/16 :goto_24
.end method

.method public static enablePlatformNotifications()V
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2660
    const-class v2, Landroid/webkit/WebViewClassic;

    #@2
    monitor-enter v2

    #@3
    .line 2661
    const/4 v1, 0x1

    #@4
    :try_start_4
    sput-boolean v1, Landroid/webkit/WebViewClassic;->sNotificationsEnabled:Z

    #@6
    .line 2662
    invoke-static {}, Landroid/webkit/JniUtil;->getContext()Landroid/content/Context;

    #@9
    move-result-object v0

    #@a
    .line 2663
    .local v0, context:Landroid/content/Context;
    if-eqz v0, :cond_f

    #@c
    .line 2664
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->setupProxyListener(Landroid/content/Context;)V

    #@f
    .line 2665
    :cond_f
    monitor-exit v2

    #@10
    .line 2666
    return-void

    #@11
    .line 2665
    :catchall_11
    move-exception v1

    #@12
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_4 .. :try_end_13} :catchall_11

    #@13
    throw v1
.end method

.method private endScrollEdit()V
    .registers 3

    #@0
    .prologue
    .line 7582
    const-wide/16 v0, 0x0

    #@2
    iput-wide v0, p0, Landroid/webkit/WebViewClassic;->mLastEditScroll:J

    #@4
    .line 7583
    return-void
.end method

.method private endSelectingText()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 5301
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@3
    .line 5302
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mShowTextSelectionExtra:Z

    #@5
    .line 5303
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->animateHandles()V

    #@8
    .line 5304
    return-void
.end method

.method private ensureFunctorDetached()V
    .registers 4

    #@0
    .prologue
    .line 2600
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    invoke-virtual {v2}, Landroid/webkit/WebView;->isHardwareAccelerated()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_1b

    #@8
    .line 2601
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@a
    invoke-direct {p0, v2}, Landroid/webkit/WebViewClassic;->nativeGetDrawGLFunction(I)I

    #@d
    move-result v0

    #@e
    .line 2602
    .local v0, drawGLFunction:I
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@10
    invoke-virtual {v2}, Landroid/webkit/WebView;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@13
    move-result-object v1

    #@14
    .line 2603
    .local v1, viewRoot:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_1b

    #@16
    if-eqz v1, :cond_1b

    #@18
    .line 2604
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl;->detachFunctor(I)V

    #@1b
    .line 2607
    .end local v0           #drawGLFunction:I
    .end local v1           #viewRoot:Landroid/view/ViewRootImpl;
    :cond_1b
    return-void
.end method

.method private ensureSelectionHandles()V
    .registers 6

    #@0
    .prologue
    const v4, 0x10805d5

    #@3
    const v3, 0x10805d3

    #@6
    .line 5307
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@8
    if-nez v0, :cond_6c

    #@a
    .line 5308
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v0

    #@10
    const v1, 0x10805d4

    #@13
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@1d
    .line 5310
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@1f
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    #@2d
    .line 5312
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@2f
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@3a
    move-result-object v0

    #@3b
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@3d
    .line 5316
    new-instance v0, Landroid/graphics/Point;

    #@3f
    const/4 v1, 0x0

    #@40
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    #@42
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@45
    move-result v2

    #@46
    neg-int v2, v2

    #@47
    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    #@4a
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectOffset:Landroid/graphics/Point;

    #@4c
    .line 5319
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@4e
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@51
    move-result-object v0

    #@52
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@55
    move-result-object v0

    #@56
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@59
    move-result-object v0

    #@5a
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleLeftReversed:Landroid/graphics/drawable/Drawable;

    #@5c
    .line 5321
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@5e
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@61
    move-result-object v0

    #@62
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@65
    move-result-object v0

    #@66
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@69
    move-result-object v0

    #@6a
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRightReversed:Landroid/graphics/drawable/Drawable;

    #@6c
    .line 5325
    :cond_6c
    return-void
.end method

.method private extendScroll(I)Z
    .registers 6
    .parameter "y"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3256
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@3
    invoke-virtual {v3}, Landroid/widget/OverScroller;->getFinalY()I

    #@6
    move-result v0

    #@7
    .line 3257
    .local v0, finalY:I
    add-int v3, v0, p1

    #@9
    invoke-virtual {p0, v3}, Landroid/webkit/WebViewClassic;->pinLocY(I)I

    #@c
    move-result v1

    #@d
    .line 3258
    .local v1, newY:I
    if-ne v1, v0, :cond_10

    #@f
    .line 3261
    :goto_f
    return v2

    #@10
    .line 3259
    :cond_10
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@12
    invoke-virtual {v3, v1}, Landroid/widget/OverScroller;->setFinalY(I)V

    #@15
    .line 3260
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@17
    invoke-static {v2, p1}, Landroid/webkit/WebViewClassic;->computeDuration(II)I

    #@1a
    move-result v2

    #@1b
    invoke-virtual {v3, v2}, Landroid/widget/OverScroller;->extendDuration(I)V

    #@1e
    .line 3261
    const/4 v2, 0x1

    #@1f
    goto :goto_f
.end method

.method public static findAddress(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "addr"

    #@0
    .prologue
    .line 4288
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/webkit/WebViewClassic;->findAddress(Ljava/lang/String;Z)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static findAddress(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 3
    .parameter "addr"
    .parameter "caseInsensitive"

    #@0
    .prologue
    .line 4313
    invoke-static {p0, p1}, Landroid/webkit/WebViewCore;->nativeFindAddress(Ljava/lang/String;Z)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private findAllBody(Ljava/lang/String;Z)I
    .registers 9
    .parameter "find"
    .parameter "isAsync"

    #@0
    .prologue
    const/16 v4, 0xdd

    #@2
    const/4 v1, 0x0

    #@3
    .line 4169
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@5
    if-nez v2, :cond_8

    #@7
    .line 4188
    :cond_7
    :goto_7
    return v1

    #@8
    .line 4170
    :cond_8
    const/4 v2, 0x0

    #@9
    iput-object v2, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@b
    .line 4171
    if-eqz p1, :cond_7

    #@d
    .line 4172
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@f
    invoke-virtual {v2, v4}, Landroid/webkit/WebViewCore;->removeMessages(I)V

    #@12
    .line 4173
    new-instance v2, Landroid/webkit/WebViewCore$FindAllRequest;

    #@14
    invoke-direct {v2, p1}, Landroid/webkit/WebViewCore$FindAllRequest;-><init>(Ljava/lang/String;)V

    #@17
    iput-object v2, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@19
    .line 4174
    if-eqz p2, :cond_23

    #@1b
    .line 4175
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@1d
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@1f
    invoke-virtual {v2, v4, v3}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@22
    goto :goto_7

    #@23
    .line 4178
    :cond_23
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@25
    monitor-enter v2

    #@26
    .line 4180
    :try_start_26
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@28
    const/16 v4, 0xdd

    #@2a
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@2c
    invoke-virtual {v3, v4, v5}, Landroid/webkit/WebViewCore;->sendMessageAtFrontOfQueue(ILjava/lang/Object;)V

    #@2f
    .line 4181
    :goto_2f
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@31
    iget v3, v3, Landroid/webkit/WebViewCore$FindAllRequest;->mMatchCount:I

    #@33
    const/4 v4, -0x1

    #@34
    if-ne v3, v4, :cond_42

    #@36
    .line 4182
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@38
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_3b
    .catchall {:try_start_26 .. :try_end_3b} :catchall_3f
    .catch Ljava/lang/InterruptedException; {:try_start_26 .. :try_end_3b} :catch_3c

    #@3b
    goto :goto_2f

    #@3c
    .line 4185
    :catch_3c
    move-exception v0

    #@3d
    .line 4186
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_3d
    monitor-exit v2

    #@3e
    goto :goto_7

    #@3f
    .line 4189
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_3f
    move-exception v1

    #@40
    monitor-exit v2
    :try_end_41
    .catchall {:try_start_3d .. :try_end_41} :catchall_3f

    #@41
    throw v1

    #@42
    .line 4188
    :cond_42
    :try_start_42
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@44
    iget v1, v1, Landroid/webkit/WebViewCore$FindAllRequest;->mMatchCount:I

    #@46
    monitor-exit v2
    :try_end_47
    .catchall {:try_start_42 .. :try_end_47} :catchall_3f

    #@47
    goto :goto_7
.end method

.method public static fromWebView(Landroid/webkit/WebView;)Landroid/webkit/WebViewClassic;
    .registers 2
    .parameter "webView"

    #@0
    .prologue
    .line 1832
    if-nez p0, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    :goto_3
    return-object v0

    #@4
    :cond_4
    invoke-virtual {p0}, Landroid/webkit/WebView;->getWebViewProvider()Landroid/webkit/WebViewProvider;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/webkit/WebViewClassic;

    #@a
    goto :goto_3
.end method

.method private getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;
    .registers 2

    #@0
    .prologue
    .line 2217
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAccessibilityInjector:Landroid/webkit/AccessibilityInjector;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 2218
    new-instance v0, Landroid/webkit/AccessibilityInjector;

    #@6
    invoke-direct {v0, p0}, Landroid/webkit/AccessibilityInjector;-><init>(Landroid/webkit/WebViewClassic;)V

    #@9
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mAccessibilityInjector:Landroid/webkit/AccessibilityInjector;

    #@b
    .line 2220
    :cond_b
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAccessibilityInjector:Landroid/webkit/AccessibilityInjector;

    #@d
    return-object v0
.end method

.method private getMaxTextScrollX()I
    .registers 4

    #@0
    .prologue
    .line 8794
    const/4 v0, 0x0

    #@1
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mEditTextContent:Landroid/graphics/Rect;

    #@3
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    #@6
    move-result v1

    #@7
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@9
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    #@c
    move-result v2

    #@d
    sub-int/2addr v1, v2

    #@e
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@11
    move-result v0

    #@12
    return v0
.end method

.method private getMaxTextScrollY()I
    .registers 4

    #@0
    .prologue
    .line 8798
    const/4 v0, 0x0

    #@1
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mEditTextContent:Landroid/graphics/Rect;

    #@3
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    #@6
    move-result v1

    #@7
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@9
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    #@c
    move-result v2

    #@d
    sub-int/2addr v1, v2

    #@e
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@11
    move-result v0

    #@12
    return v0
.end method

.method private static getMean([FI)F
    .registers 5
    .parameter "array"
    .parameter "numDragMove"

    #@0
    .prologue
    .line 7499
    const/4 v1, 0x0

    #@1
    .line 7500
    .local v1, sum:F
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, p1, :cond_a

    #@4
    .line 7501
    aget v2, p0, v0

    #@6
    add-float/2addr v1, v2

    #@7
    .line 7500
    add-int/lit8 v0, v0, 0x1

    #@9
    goto :goto_2

    #@a
    .line 7503
    :cond_a
    int-to-float v2, p1

    #@b
    div-float v2, v1, v2

    #@d
    return v2
.end method

.method private getOverlappingActionModeHeight()I
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 2446
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 2454
    :goto_5
    return v0

    #@6
    .line 2449
    :cond_6
    iget v1, p0, Landroid/webkit/WebViewClassic;->mCachedOverlappingActionModeHeight:I

    #@8
    if-gez v1, :cond_24

    #@a
    .line 2450
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@c
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleRect:Landroid/graphics/Rect;

    #@e
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleOffset:Landroid/graphics/Point;

    #@10
    invoke-virtual {v1, v2, v3}, Landroid/webkit/WebView;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    #@13
    .line 2451
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@15
    invoke-virtual {v1}, Landroid/webkit/FindActionModeCallback;->getActionModeGlobalBottom()I

    #@18
    move-result v1

    #@19
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleRect:Landroid/graphics/Rect;

    #@1b
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@1d
    sub-int/2addr v1, v2

    #@1e
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@21
    move-result v0

    #@22
    iput v0, p0, Landroid/webkit/WebViewClassic;->mCachedOverlappingActionModeHeight:I

    #@24
    .line 2454
    :cond_24
    iget v0, p0, Landroid/webkit/WebViewClassic;->mCachedOverlappingActionModeHeight:I

    #@26
    goto :goto_5
.end method

.method public static declared-synchronized getPluginList()Landroid/webkit/PluginList;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 4770
    const-class v1, Landroid/webkit/WebViewClassic;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    new-instance v0, Landroid/webkit/PluginList;

    #@5
    invoke-direct {v0}, Landroid/webkit/PluginList;-><init>()V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    #@8
    monitor-exit v1

    #@9
    return-object v0

    #@a
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1

    #@c
    throw v0
.end method

.method private getScaledMaxXScroll()I
    .registers 4

    #@0
    .prologue
    .line 10197
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@2
    if-nez v2, :cond_f

    #@4
    .line 10198
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@7
    move-result v2

    #@8
    div-int/lit8 v1, v2, 0x4

    #@a
    .line 10205
    .local v1, width:I
    :goto_a
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@d
    move-result v2

    #@e
    return v2

    #@f
    .line 10200
    .end local v1           #width:I
    :cond_f
    new-instance v0, Landroid/graphics/Rect;

    #@11
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@14
    .line 10201
    .local v0, visRect:Landroid/graphics/Rect;
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->calcOurVisibleRect(Landroid/graphics/Rect;)V

    #@17
    .line 10202
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@1a
    move-result v2

    #@1b
    div-int/lit8 v1, v2, 0x2

    #@1d
    .restart local v1       #width:I
    goto :goto_a
.end method

.method private getScaledMaxYScroll()I
    .registers 5

    #@0
    .prologue
    .line 10210
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@2
    if-nez v2, :cond_17

    #@4
    .line 10211
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewHeight()I

    #@7
    move-result v2

    #@8
    div-int/lit8 v0, v2, 0x4

    #@a
    .line 10220
    .local v0, height:I
    :goto_a
    int-to-float v2, v0

    #@b
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@d
    invoke-virtual {v3}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@10
    move-result v3

    #@11
    mul-float/2addr v2, v3

    #@12
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    #@15
    move-result v2

    #@16
    return v2

    #@17
    .line 10213
    .end local v0           #height:I
    :cond_17
    new-instance v1, Landroid/graphics/Rect;

    #@19
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@1c
    .line 10214
    .local v1, visRect:Landroid/graphics/Rect;
    invoke-direct {p0, v1}, Landroid/webkit/WebViewClassic;->calcOurVisibleRect(Landroid/graphics/Rect;)V

    #@1f
    .line 10215
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    #@22
    move-result v2

    #@23
    div-int/lit8 v0, v2, 0x2

    #@25
    .restart local v0       #height:I
    goto :goto_a
.end method

.method private getScreenShot(IIII)Landroid/graphics/Bitmap;
    .registers 17
    .parameter "x"
    .parameter "y"
    .parameter "maxX"
    .parameter "maxY"

    #@0
    .prologue
    .line 6284
    sub-int v8, p3, p1

    #@2
    .line 6285
    .local v8, viewWidth:I
    sub-int v7, p4, p2

    #@4
    .line 6287
    .local v7, viewHeight:I
    if-lez v8, :cond_8

    #@6
    if-gtz v7, :cond_a

    #@8
    .line 6288
    :cond_8
    const/4 v6, 0x0

    #@9
    .line 6328
    :cond_9
    :goto_9
    return-object v6

    #@a
    .line 6291
    :cond_a
    const/high16 v9, 0x3f80

    #@c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScale()F

    #@f
    move-result v10

    #@10
    mul-float v3, v9, v10

    #@12
    .line 6292
    .local v3, scale:F
    int-to-float v9, v8

    #@13
    mul-float/2addr v9, v3

    #@14
    float-to-int v5, v9

    #@15
    .line 6293
    .local v5, screenShotWidth:I
    int-to-float v9, v7

    #@16
    mul-float/2addr v9, v3

    #@17
    float-to-int v4, v9

    #@18
    .line 6295
    .local v4, screenShotHeight:I
    const/4 v6, 0x0

    #@19
    .line 6298
    .local v6, screenshotBitmap:Landroid/graphics/Bitmap;
    :try_start_19
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@1b
    invoke-static {v5, v4, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_19 .. :try_end_1e} :catch_4f
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_1e} :catch_61

    #@1e
    move-result-object v6

    #@1f
    .line 6309
    :goto_1f
    if-eqz v6, :cond_9

    #@21
    .line 6310
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mExtentAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@23
    const/4 v10, 0x0

    #@24
    invoke-virtual {v9, v10}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;->setAlpha(I)V

    #@27
    .line 6311
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mBaseAlpha:Landroid/webkit/WebViewClassic$SelectionHandleAlpha;

    #@29
    const/4 v10, 0x0

    #@2a
    invoke-virtual {v9, v10}, Landroid/webkit/WebViewClassic$SelectionHandleAlpha;->setAlpha(I)V

    #@2d
    .line 6312
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@30
    .line 6314
    new-instance v0, Landroid/graphics/Canvas;

    #@32
    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@35
    .line 6315
    .local v0, canvas:Landroid/graphics/Canvas;
    neg-int v9, p1

    #@36
    int-to-float v9, v9

    #@37
    mul-float/2addr v9, v3

    #@38
    neg-int v10, p2

    #@39
    int-to-float v10, v10

    #@3a
    mul-float/2addr v10, v3

    #@3b
    invoke-virtual {v0, v9, v10}, Landroid/graphics/Canvas;->translate(FF)V

    #@3e
    .line 6316
    invoke-virtual {v0, v3, v3}, Landroid/graphics/Canvas;->scale(FF)V

    #@41
    .line 6319
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->capturePicture()Landroid/graphics/Picture;

    #@44
    move-result-object v2

    #@45
    .line 6320
    .local v2, p:Landroid/graphics/Picture;
    if-eqz v2, :cond_4a

    #@47
    .line 6321
    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawPicture(Landroid/graphics/Picture;)V

    #@4a
    .line 6325
    :cond_4a
    const/4 v9, 0x0

    #@4b
    invoke-virtual {v0, v9}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@4e
    goto :goto_9

    #@4f
    .line 6300
    .end local v0           #canvas:Landroid/graphics/Canvas;
    .end local v2           #p:Landroid/graphics/Picture;
    :catch_4f
    move-exception v1

    #@50
    .line 6301
    .local v1, e:Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    #@53
    .line 6302
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@55
    const v10, 0x20902e3

    #@58
    const/4 v11, 0x0

    #@59
    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@5c
    move-result-object v9

    #@5d
    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    #@60
    goto :goto_1f

    #@61
    .line 6305
    .end local v1           #e:Ljava/lang/OutOfMemoryError;
    :catch_61
    move-exception v1

    #@62
    .line 6306
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@65
    goto :goto_1f
.end method

.method private static getSelectionCoordinate(III)I
    .registers 4
    .parameter "coordinate"
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    .line 7547
    invoke-static {p0, p2}, Ljava/lang/Math;->min(II)I

    #@3
    move-result v0

    #@4
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method private getSelectionHandles([I)V
    .registers 4
    .parameter "handles"

    #@0
    .prologue
    .line 5467
    const/4 v0, 0x0

    #@1
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@3
    iget v1, v1, Landroid/graphics/Point;->x:I

    #@5
    aput v1, p1, v0

    #@7
    .line 5468
    const/4 v0, 0x1

    #@8
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@a
    iget v1, v1, Landroid/graphics/Point;->y:I

    #@c
    aput v1, p1, v0

    #@e
    .line 5469
    const/4 v0, 0x2

    #@f
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@11
    iget v1, v1, Landroid/graphics/Point;->x:I

    #@13
    aput v1, p1, v0

    #@15
    .line 5470
    const/4 v0, 0x3

    #@16
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@18
    iget v1, v1, Landroid/graphics/Point;->y:I

    #@1a
    aput v1, p1, v0

    #@1c
    .line 5471
    return-void
.end method

.method private static getStandardDeviation([FI)F
    .registers 9
    .parameter "array"
    .parameter "numDragMove"

    #@0
    .prologue
    .line 7510
    const/16 v5, 0x12c

    #@2
    if-le p1, v5, :cond_6

    #@4
    .line 7511
    const/16 p1, 0x12c

    #@6
    .line 7513
    :cond_6
    const/4 v4, 0x0

    #@7
    .line 7514
    .local v4, sum:F
    const/4 v3, 0x0

    #@8
    .line 7517
    .local v3, sd:F
    invoke-static {p0, p1}, Landroid/webkit/WebViewClassic;->getMean([FI)F

    #@b
    move-result v2

    #@c
    .line 7518
    .local v2, meanValue:F
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, p1, :cond_1c

    #@f
    .line 7519
    aget v5, p0, v1

    #@11
    sub-float v0, v5, v2

    #@13
    .line 7520
    .local v0, diff:F
    mul-float v5, v0, v0

    #@15
    add-float/2addr v4, v5

    #@16
    .line 7521
    const/4 v5, 0x0

    #@17
    aput v5, p0, v1

    #@19
    .line 7518
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_d

    #@1c
    .line 7523
    .end local v0           #diff:F
    :cond_1c
    int-to-float v5, p1

    #@1d
    div-float v5, v4, v5

    #@1f
    float-to-double v5, v5

    #@20
    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    #@23
    move-result-wide v5

    #@24
    double-to-float v3, v5

    #@25
    .line 7525
    const/high16 v5, 0x447a

    #@27
    mul-float/2addr v5, v3

    #@28
    return v5
.end method

.method private static getTextScrollDelta(FJ)I
    .registers 10
    .parameter "speed"
    .parameter "deltaT"

    #@0
    .prologue
    .line 7590
    long-to-float v3, p1

    #@1
    mul-float v0, p0, v3

    #@3
    .line 7591
    .local v0, distance:F
    float-to-double v3, v0

    #@4
    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    #@7
    move-result-wide v3

    #@8
    double-to-int v1, v3

    #@9
    .line 7592
    .local v1, intDistance:I
    int-to-float v3, v1

    #@a
    sub-float v2, v0, v3

    #@c
    .line 7593
    .local v2, probability:F
    invoke-static {}, Ljava/lang/Math;->random()D

    #@f
    move-result-wide v3

    #@10
    float-to-double v5, v2

    #@11
    cmpg-double v3, v3, v5

    #@13
    if-gez v3, :cond_17

    #@15
    .line 7594
    add-int/lit8 v1, v1, 0x1

    #@17
    .line 7596
    :cond_17
    return v1
.end method

.method private static getTextScrollSpeed(III)F
    .registers 5
    .parameter "coordinate"
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    const v1, 0x3c23d70a

    #@3
    .line 7537
    if-ge p0, p1, :cond_a

    #@5
    .line 7538
    sub-int v0, p0, p1

    #@7
    int-to-float v0, v0

    #@8
    mul-float/2addr v0, v1

    #@9
    .line 7542
    :goto_9
    return v0

    #@a
    .line 7539
    :cond_a
    if-lt p0, p2, :cond_13

    #@c
    .line 7540
    sub-int v0, p0, p2

    #@e
    add-int/lit8 v0, v0, 0x1

    #@10
    int-to-float v0, v0

    #@11
    mul-float/2addr v0, v1

    #@12
    goto :goto_9

    #@13
    .line 7542
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_9
.end method

.method private getTextScrollX()I
    .registers 2

    #@0
    .prologue
    .line 8786
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mEditTextContent:Landroid/graphics/Rect;

    #@2
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@4
    neg-int v0, v0

    #@5
    return v0
.end method

.method private getTextScrollY()I
    .registers 2

    #@0
    .prologue
    .line 8790
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mEditTextContent:Landroid/graphics/Rect;

    #@2
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@4
    neg-int v0, v0

    #@5
    return v0
.end method

.method private getVisibleTitleHeightImpl()I
    .registers 4

    #@0
    .prologue
    .line 2439
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@8
    move-result v2

    #@9
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@c
    move-result v1

    #@d
    sub-int/2addr v0, v1

    #@e
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getOverlappingActionModeHeight()I

    #@11
    move-result v1

    #@12
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@15
    move-result v0

    #@16
    return v0
.end method

.method private goBackOrForward(IZ)V
    .registers 6
    .parameter "steps"
    .parameter "ignoreSnapshot"

    #@0
    .prologue
    .line 3235
    if-eqz p1, :cond_f

    #@2
    .line 3236
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->clearHelpers()V

    #@5
    .line 3237
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@7
    const/16 v2, 0x6a

    #@9
    if-eqz p2, :cond_10

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    invoke-virtual {v1, v2, p1, v0}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@f
    .line 3240
    :cond_f
    return-void

    #@10
    .line 3237
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_c
.end method

.method private goBackOrForwardImpl(I)V
    .registers 3
    .parameter "steps"

    #@0
    .prologue
    .line 3226
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@6
    if-nez v0, :cond_c

    #@8
    .line 3227
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@b
    .line 3232
    :goto_b
    return-void

    #@c
    .line 3231
    :cond_c
    const/4 v0, 0x0

    #@d
    invoke-direct {p0, p1, v0}, Landroid/webkit/WebViewClassic;->goBackOrForward(IZ)V

    #@10
    goto :goto_b
.end method

.method private static handleCertTrustChanged()V
    .registers 2

    #@0
    .prologue
    .line 1884
    const/16 v0, 0xdc

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore;->sendStaticMessage(ILjava/lang/Object;)V

    #@6
    .line 1885
    return-void
.end method

.method private static handleProxyBroadcast(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    const/16 v2, 0xc1

    #@2
    .line 1959
    const-string/jumbo v1, "proxy"

    #@5
    invoke-virtual {p0, v1}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/net/ProxyProperties;

    #@b
    .line 1960
    .local v0, proxyProperties:Landroid/net/ProxyProperties;
    if-eqz v0, :cond_13

    #@d
    invoke-virtual {v0}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    if-nez v1, :cond_18

    #@13
    .line 1961
    :cond_13
    const/4 v1, 0x0

    #@14
    invoke-static {v2, v1}, Landroid/webkit/WebViewCore;->sendStaticMessage(ILjava/lang/Object;)V

    #@17
    .line 1965
    :goto_17
    return-void

    #@18
    .line 1964
    :cond_18
    invoke-static {v2, v0}, Landroid/webkit/WebViewCore;->sendStaticMessage(ILjava/lang/Object;)V

    #@1b
    goto :goto_17
.end method

.method private handleTouchEventCommon(Landroid/view/MotionEvent;III)V
    .registers 41
    .parameter "event"
    .parameter "action"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 7000
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@4
    invoke-virtual {v3}, Landroid/webkit/ZoomManager;->getScaleGestureDetector()Landroid/view/ScaleGestureDetector;

    #@7
    move-result-object v20

    #@8
    .line 7002
    .local v20, detector:Landroid/view/ScaleGestureDetector;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@b
    move-result-wide v21

    #@c
    .line 7008
    .local v21, eventTime:J
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@f
    move-result v3

    #@10
    add-int/lit8 v3, v3, -0x1

    #@12
    move/from16 v0, p3

    #@14
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    #@17
    move-result p3

    #@18
    .line 7009
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@1b
    move-result v3

    #@1c
    add-int/lit8 v3, v3, -0x1

    #@1e
    move/from16 v0, p4

    #@20
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    #@23
    move-result p4

    #@24
    .line 7011
    move-object/from16 v0, p0

    #@26
    iget v3, v0, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@28
    sub-int v18, v3, p3

    #@2a
    .line 7012
    .local v18, deltaX:I
    move-object/from16 v0, p0

    #@2c
    iget v3, v0, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@2e
    sub-int v19, v3, p4

    #@30
    .line 7013
    .local v19, deltaY:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@33
    move-result v3

    #@34
    add-int v3, v3, p3

    #@36
    move-object/from16 v0, p0

    #@38
    invoke-virtual {v0, v3}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@3b
    move-result v14

    #@3c
    .line 7014
    .local v14, contentX:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@3f
    move-result v3

    #@40
    add-int v3, v3, p4

    #@42
    move-object/from16 v0, p0

    #@44
    invoke-virtual {v0, v3}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@47
    move-result v15

    #@48
    .line 7017
    .local v15, contentY:I
    sget-boolean v3, Landroid/webkit/WebViewClassic;->mCapptouchFlickNoti:Z

    #@4a
    if-eqz v3, :cond_5f

    #@4c
    .line 7018
    sget v3, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@4e
    sget v4, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@50
    add-int/lit8 v4, v4, -0x1

    #@52
    and-int/2addr v3, v4

    #@53
    if-nez v3, :cond_58

    #@55
    .line 7019
    const/4 v3, 0x0

    #@56
    sput v3, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@58
    .line 7021
    :cond_58
    sget-boolean v3, Landroid/webkit/WebViewClassic;->dropFirstDeltaY:Z

    #@5a
    if-nez v3, :cond_69

    #@5c
    .line 7022
    const/4 v3, 0x1

    #@5d
    sput-boolean v3, Landroid/webkit/WebViewClassic;->dropFirstDeltaY:Z

    #@5f
    .line 7033
    :cond_5f
    :goto_5f
    packed-switch p2, :pswitch_data_72a

    #@62
    .line 7491
    :cond_62
    :goto_62
    :pswitch_62
    move/from16 v0, p2

    #@64
    move-object/from16 v1, p0

    #@66
    iput v0, v1, Landroid/webkit/WebViewClassic;->mPrevAction:I

    #@68
    .line 7492
    :cond_68
    return-void

    #@69
    .line 7023
    :cond_69
    sget-boolean v3, Landroid/webkit/WebViewClassic;->dropFirstDeltaY:Z

    #@6b
    if-eqz v3, :cond_6f

    #@6d
    if-eqz v19, :cond_5f

    #@6f
    .line 7026
    :cond_6f
    move-object/from16 v0, p0

    #@71
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->coorDirection:[I

    #@73
    sget v4, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@75
    sget v5, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@77
    add-int/lit8 v5, v5, -0x1

    #@79
    and-int/2addr v4, v5

    #@7a
    aput v19, v3, v4

    #@7c
    .line 7027
    sget v3, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@7e
    add-int/lit8 v3, v3, 0x1

    #@80
    sput v3, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@82
    .line 7028
    sget v3, Landroid/webkit/WebViewClassic;->moveCounter:I

    #@84
    add-int/lit8 v3, v3, 0x1

    #@86
    sput v3, Landroid/webkit/WebViewClassic;->moveCounter:I

    #@88
    goto :goto_5f

    #@89
    .line 7039
    :pswitch_89
    const/4 v3, 0x0

    #@8a
    move-object/from16 v0, p0

    #@8c
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mConfirmMove:Z

    #@8e
    .line 7040
    move-object/from16 v0, p0

    #@90
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mEditTextScroller:Landroid/widget/Scroller;

    #@92
    invoke-virtual {v3}, Landroid/widget/Scroller;->isFinished()Z

    #@95
    move-result v3

    #@96
    if-nez v3, :cond_9f

    #@98
    .line 7041
    move-object/from16 v0, p0

    #@9a
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mEditTextScroller:Landroid/widget/Scroller;

    #@9c
    invoke-virtual {v3}, Landroid/widget/Scroller;->abortAnimation()V

    #@9f
    .line 7043
    :cond_9f
    move-object/from16 v0, p0

    #@a1
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@a3
    invoke-virtual {v3}, Landroid/widget/OverScroller;->isFinished()Z

    #@a6
    move-result v3

    #@a7
    if-nez v3, :cond_115

    #@a9
    .line 7047
    move-object/from16 v0, p0

    #@ab
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@ad
    invoke-virtual {v3}, Landroid/widget/OverScroller;->abortAnimation()V

    #@b0
    .line 7048
    const/4 v3, 0x2

    #@b1
    move-object/from16 v0, p0

    #@b3
    iput v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@b5
    .line 7051
    const/4 v3, 0x0

    #@b6
    move-object/from16 v0, p0

    #@b8
    invoke-direct {v0, v3}, Landroid/webkit/WebViewClassic;->nativeSetIsScrolling(Z)V

    #@bb
    .line 7104
    :cond_bb
    :goto_bb
    move-object/from16 v0, p0

    #@bd
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@bf
    if-nez v3, :cond_e3

    #@c1
    move-object/from16 v0, p0

    #@c3
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@c5
    const/4 v4, 0x1

    #@c6
    if-eq v3, v4, :cond_cf

    #@c8
    move-object/from16 v0, p0

    #@ca
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@cc
    const/4 v4, 0x6

    #@cd
    if-ne v3, v4, :cond_e3

    #@cf
    .line 7106
    :cond_cf
    move-object/from16 v0, p0

    #@d1
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@d3
    const/4 v4, 0x3

    #@d4
    const-wide/16 v5, 0x12c

    #@d6
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@d9
    .line 7108
    move-object/from16 v0, p0

    #@db
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@dd
    const/4 v4, 0x4

    #@de
    const-wide/16 v5, 0x3e8

    #@e0
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@e3
    .line 7111
    :cond_e3
    move/from16 v0, p3

    #@e5
    int-to-float v3, v0

    #@e6
    move/from16 v0, p4

    #@e8
    int-to-float v4, v0

    #@e9
    move-object/from16 v0, p0

    #@eb
    move-wide/from16 v1, v21

    #@ed
    invoke-direct {v0, v3, v4, v1, v2}, Landroid/webkit/WebViewClassic;->startTouch(FFJ)V

    #@f0
    .line 7112
    move-object/from16 v0, p0

    #@f2
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@f4
    if-eqz v3, :cond_102

    #@f6
    .line 7113
    move-object/from16 v0, p0

    #@f8
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@fa
    invoke-virtual {v3, v14, v15}, Landroid/graphics/Rect;->contains(II)Z

    #@fd
    move-result v3

    #@fe
    move-object/from16 v0, p0

    #@100
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mTouchInEditText:Z

    #@102
    .line 7118
    :cond_102
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@105
    move-result-object v32

    #@106
    .line 7119
    .local v32, settings:Landroid/webkit/WebSettings;
    if-eqz v32, :cond_62

    #@108
    invoke-virtual/range {v32 .. v32}, Landroid/webkit/WebSettingsClassic;->getScrollToTopBottomEnabled()Z

    #@10b
    move-result v3

    #@10c
    if-eqz v3, :cond_62

    #@10e
    .line 7120
    const/4 v3, 0x1

    #@10f
    move-object/from16 v0, p0

    #@111
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsProperPointsFor2fingerFlicking:Z

    #@113
    goto/16 :goto_62

    #@115
    .line 7052
    .end local v32           #settings:Landroid/webkit/WebSettings;
    :cond_115
    move-object/from16 v0, p0

    #@117
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@119
    const/4 v4, 0x5

    #@11a
    invoke-virtual {v3, v4}, Landroid/os/Handler;->hasMessages(I)Z

    #@11d
    move-result v3

    #@11e
    if-eqz v3, :cond_143

    #@120
    .line 7053
    move-object/from16 v0, p0

    #@122
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@124
    const/4 v4, 0x5

    #@125
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@128
    .line 7054
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->removeTouchHighlight()V

    #@12b
    .line 7055
    mul-int v3, v18, v18

    #@12d
    mul-int v4, v19, v19

    #@12f
    add-int/2addr v3, v4

    #@130
    move-object/from16 v0, p0

    #@132
    iget v4, v0, Landroid/webkit/WebViewClassic;->mDoubleTapSlopSquare:I

    #@134
    if-ge v3, v4, :cond_13c

    #@136
    .line 7056
    const/4 v3, 0x6

    #@137
    move-object/from16 v0, p0

    #@139
    iput v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@13b
    goto :goto_bb

    #@13c
    .line 7058
    :cond_13c
    const/4 v3, 0x1

    #@13d
    move-object/from16 v0, p0

    #@13f
    iput v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@141
    goto/16 :goto_bb

    #@143
    .line 7061
    :cond_143
    const/4 v3, 0x1

    #@144
    move-object/from16 v0, p0

    #@146
    iput v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@148
    .line 7062
    sget-boolean v3, Landroid/webkit/WebViewClassic;->mLogEvent:Z

    #@14a
    if-eqz v3, :cond_175

    #@14c
    move-object/from16 v0, p0

    #@14e
    iget-wide v3, v0, Landroid/webkit/WebViewClassic;->mLastTouchUpTime:J

    #@150
    sub-long v3, v21, v3

    #@152
    const-wide/16 v5, 0x3e8

    #@154
    cmp-long v3, v3, v5

    #@156
    if-gez v3, :cond_175

    #@158
    .line 7063
    const v3, 0x111d6

    #@15b
    const/4 v4, 0x2

    #@15c
    new-array v4, v4, [Ljava/lang/Object;

    #@15e
    const/4 v5, 0x0

    #@15f
    move-object/from16 v0, p0

    #@161
    iget-wide v6, v0, Landroid/webkit/WebViewClassic;->mLastTouchUpTime:J

    #@163
    sub-long v6, v21, v6

    #@165
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@168
    move-result-object v6

    #@169
    aput-object v6, v4, v5

    #@16b
    const/4 v5, 0x1

    #@16c
    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@16f
    move-result-object v6

    #@170
    aput-object v6, v4, v5

    #@172
    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@175
    .line 7066
    :cond_175
    const/4 v3, 0x0

    #@176
    move-object/from16 v0, p0

    #@178
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@17a
    .line 7067
    move-object/from16 v0, p0

    #@17c
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@17e
    if-eqz v3, :cond_bb

    #@180
    .line 7068
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->ensureSelectionHandles()V

    #@183
    .line 7069
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@186
    move-result v3

    #@187
    sub-int v3, p4, v3

    #@189
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@18c
    move-result v4

    #@18d
    add-int v34, v3, v4

    #@18f
    .line 7070
    .local v34, shiftedY:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@192
    move-result v3

    #@193
    add-int v33, p3, v3

    #@195
    .line 7072
    .local v33, shiftedX:I
    move-object/from16 v0, p0

    #@197
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@199
    invoke-virtual {v3}, Landroid/webkit/WebViewCore;->inParagraphMode()Z

    #@19c
    move-result v3

    #@19d
    if-eqz v3, :cond_1bb

    #@19f
    .line 7073
    move-object/from16 v0, p0

    #@1a1
    move/from16 v1, v33

    #@1a3
    move/from16 v2, v34

    #@1a5
    invoke-direct {v0, v1, v2}, Landroid/webkit/WebViewClassic;->hitParagraphHandle(II)Z

    #@1a8
    move-result v3

    #@1a9
    if-eqz v3, :cond_bb

    #@1ab
    .line 7074
    const/4 v3, 0x1

    #@1ac
    move-object/from16 v0, p0

    #@1ae
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@1b0
    .line 7075
    move-object/from16 v0, p0

    #@1b2
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectOffset:Landroid/graphics/Point;

    #@1b4
    const/4 v4, 0x0

    #@1b5
    const/4 v5, 0x0

    #@1b6
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    #@1b9
    goto/16 :goto_bb

    #@1bb
    .line 7080
    :cond_1bb
    move-object/from16 v0, p0

    #@1bd
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectHandleBaseBounds:Landroid/graphics/Rect;

    #@1bf
    move/from16 v0, v33

    #@1c1
    move/from16 v1, v34

    #@1c3
    invoke-virtual {v3, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    #@1c6
    move-result v3

    #@1c7
    if-eqz v3, :cond_215

    #@1c9
    .line 7081
    const/4 v3, 0x1

    #@1ca
    move-object/from16 v0, p0

    #@1cc
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@1ce
    .line 7082
    move-object/from16 v0, p0

    #@1d0
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@1d2
    move-object/from16 v0, p0

    #@1d4
    iput-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@1d6
    .line 7083
    move-object/from16 v0, p0

    #@1d8
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectCursorBaseTextQuad:Landroid/webkit/QuadF;

    #@1da
    move-object/from16 v0, p0

    #@1dc
    iput-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@1de
    .line 7084
    move-object/from16 v0, p0

    #@1e0
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectOffset:Landroid/graphics/Point;

    #@1e2
    move-object/from16 v0, p0

    #@1e4
    iget-object v4, v0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@1e6
    iget v4, v4, Landroid/graphics/Point;->x:I

    #@1e8
    move-object/from16 v0, p0

    #@1ea
    invoke-virtual {v0, v4}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@1ed
    move-result v4

    #@1ee
    sub-int v4, v4, v33

    #@1f0
    move-object/from16 v0, p0

    #@1f2
    iget-object v5, v0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@1f4
    iget v5, v5, Landroid/graphics/Point;->y:I

    #@1f6
    move-object/from16 v0, p0

    #@1f8
    invoke-virtual {v0, v5}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@1fb
    move-result v5

    #@1fc
    sub-int v5, v5, v34

    #@1fe
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    #@201
    .line 7085
    move-object/from16 v0, p0

    #@203
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@205
    if-eqz v3, :cond_bb

    #@207
    .line 7086
    move-object/from16 v0, p0

    #@209
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@20b
    const/16 v4, 0x90

    #@20d
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@210
    .line 7087
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->hidePasteButton()V

    #@213
    goto/16 :goto_bb

    #@215
    .line 7089
    :cond_215
    move-object/from16 v0, p0

    #@217
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectHandleExtentBounds:Landroid/graphics/Rect;

    #@219
    move/from16 v0, v33

    #@21b
    move/from16 v1, v34

    #@21d
    invoke-virtual {v3, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    #@220
    move-result v3

    #@221
    if-eqz v3, :cond_25d

    #@223
    .line 7091
    const/4 v3, 0x1

    #@224
    move-object/from16 v0, p0

    #@226
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@228
    .line 7092
    move-object/from16 v0, p0

    #@22a
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@22c
    move-object/from16 v0, p0

    #@22e
    iput-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@230
    .line 7093
    move-object/from16 v0, p0

    #@232
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectCursorExtentTextQuad:Landroid/webkit/QuadF;

    #@234
    move-object/from16 v0, p0

    #@236
    iput-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@238
    .line 7094
    move-object/from16 v0, p0

    #@23a
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectOffset:Landroid/graphics/Point;

    #@23c
    move-object/from16 v0, p0

    #@23e
    iget-object v4, v0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@240
    iget v4, v4, Landroid/graphics/Point;->x:I

    #@242
    move-object/from16 v0, p0

    #@244
    invoke-virtual {v0, v4}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@247
    move-result v4

    #@248
    sub-int v4, v4, v33

    #@24a
    move-object/from16 v0, p0

    #@24c
    iget-object v5, v0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@24e
    iget v5, v5, Landroid/graphics/Point;->y:I

    #@250
    move-object/from16 v0, p0

    #@252
    invoke-virtual {v0, v5}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@255
    move-result v5

    #@256
    sub-int v5, v5, v34

    #@258
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    #@25b
    goto/16 :goto_bb

    #@25d
    .line 7095
    :cond_25d
    move-object/from16 v0, p0

    #@25f
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@261
    if-eqz v3, :cond_bb

    #@263
    .line 7096
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@266
    goto/16 :goto_bb

    #@268
    .line 7127
    .end local v33           #shiftedX:I
    .end local v34           #shiftedY:I
    :pswitch_268
    move-object/from16 v0, p0

    #@26a
    iget-wide v3, v0, Landroid/webkit/WebViewClassic;->mLastPointerUpTime:J

    #@26c
    sub-long v16, v21, v3

    #@26e
    .line 7128
    .local v16, deltaTime:J
    move-object/from16 v0, p0

    #@270
    iget v3, v0, Landroid/webkit/WebViewClassic;->mPrevAction:I

    #@272
    const/4 v4, 0x6

    #@273
    if-ne v3, v4, :cond_27b

    #@275
    const-wide/16 v3, 0x46

    #@277
    cmp-long v3, v16, v3

    #@279
    if-ltz v3, :cond_68

    #@27b
    .line 7133
    :cond_27b
    move-object/from16 v0, p0

    #@27d
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mConfirmMove:Z

    #@27f
    if-nez v3, :cond_2b0

    #@281
    mul-int v3, v18, v18

    #@283
    mul-int v4, v19, v19

    #@285
    add-int/2addr v3, v4

    #@286
    move-object/from16 v0, p0

    #@288
    iget v4, v0, Landroid/webkit/WebViewClassic;->mTouchSlopSquare:I

    #@28a
    if-lt v3, v4, :cond_2b0

    #@28c
    .line 7135
    move-object/from16 v0, p0

    #@28e
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@290
    const/4 v4, 0x3

    #@291
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@294
    .line 7136
    move-object/from16 v0, p0

    #@296
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@298
    const/4 v4, 0x4

    #@299
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@29c
    .line 7137
    const/4 v3, 0x1

    #@29d
    move-object/from16 v0, p0

    #@29f
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mConfirmMove:Z

    #@2a1
    .line 7138
    move-object/from16 v0, p0

    #@2a3
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@2a5
    const/4 v4, 0x6

    #@2a6
    if-ne v3, v4, :cond_2ad

    #@2a8
    .line 7139
    const/4 v3, 0x1

    #@2a9
    move-object/from16 v0, p0

    #@2ab
    iput v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@2ad
    .line 7141
    :cond_2ad
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->removeTouchHighlight()V

    #@2b0
    .line 7143
    :cond_2b0
    move-object/from16 v0, p0

    #@2b2
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@2b4
    if-eqz v3, :cond_40b

    #@2b6
    move-object/from16 v0, p0

    #@2b8
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@2ba
    if-eqz v3, :cond_40b

    #@2bc
    .line 7147
    move-object/from16 v0, p0

    #@2be
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2c0
    invoke-virtual {v3}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    #@2c3
    move-result-object v28

    #@2c4
    .line 7148
    .local v28, parent:Landroid/view/ViewParent;
    if-eqz v28, :cond_2cc

    #@2c6
    .line 7149
    const/4 v3, 0x1

    #@2c7
    move-object/from16 v0, v28

    #@2c9
    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@2cc
    .line 7152
    :cond_2cc
    move-object/from16 v0, p0

    #@2ce
    iget v3, v0, Landroid/webkit/WebViewClassic;->mMinAutoScrollX:I

    #@2d0
    move/from16 v0, p3

    #@2d2
    if-gt v0, v3, :cond_3df

    #@2d4
    const/4 v3, -0x5

    #@2d5
    :goto_2d5
    move-object/from16 v0, p0

    #@2d7
    iput v3, v0, Landroid/webkit/WebViewClassic;->mAutoScrollX:I

    #@2d9
    .line 7154
    move-object/from16 v0, p0

    #@2db
    iget v3, v0, Landroid/webkit/WebViewClassic;->mMinAutoScrollY:I

    #@2dd
    move/from16 v0, p4

    #@2df
    if-gt v0, v3, :cond_3ed

    #@2e1
    const/4 v3, -0x5

    #@2e2
    :goto_2e2
    move-object/from16 v0, p0

    #@2e4
    iput v3, v0, Landroid/webkit/WebViewClassic;->mAutoScrollY:I

    #@2e6
    .line 7156
    move-object/from16 v0, p0

    #@2e8
    iget v3, v0, Landroid/webkit/WebViewClassic;->mAutoScrollX:I

    #@2ea
    if-nez v3, :cond_2f2

    #@2ec
    move-object/from16 v0, p0

    #@2ee
    iget v3, v0, Landroid/webkit/WebViewClassic;->mAutoScrollY:I

    #@2f0
    if-eqz v3, :cond_336

    #@2f2
    :cond_2f2
    move-object/from16 v0, p0

    #@2f4
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSentAutoScrollMessage:Z

    #@2f6
    if-nez v3, :cond_336

    #@2f8
    move-object/from16 v0, p0

    #@2fa
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@2fc
    if-nez v3, :cond_336

    #@2fe
    move-object/from16 v0, p0

    #@300
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@302
    if-nez v3, :cond_336

    #@304
    .line 7159
    move-object/from16 v0, p0

    #@306
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@308
    const-string/jumbo v4, "power"

    #@30b
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@30e
    move-result-object v29

    #@30f
    check-cast v29, Landroid/os/PowerManager;

    #@311
    .line 7162
    .local v29, pm:Landroid/os/PowerManager;
    const/16 v3, 0xa

    #@313
    :try_start_313
    const-string v4, "WebView"

    #@315
    move-object/from16 v0, v29

    #@317
    invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@31a
    move-result-object v3

    #@31b
    move-object/from16 v0, p0

    #@31d
    iput-object v3, v0, Landroid/webkit/WebViewClassic;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@31f
    .line 7163
    move-object/from16 v0, p0

    #@321
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@323
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_326
    .catch Ljava/lang/SecurityException; {:try_start_313 .. :try_end_326} :catch_3fb

    #@326
    .line 7168
    :goto_326
    const/4 v3, 0x1

    #@327
    move-object/from16 v0, p0

    #@329
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSentAutoScrollMessage:Z

    #@32b
    .line 7169
    move-object/from16 v0, p0

    #@32d
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@32f
    const/16 v4, 0xb

    #@331
    const-wide/16 v5, 0x10

    #@333
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@336
    .line 7173
    .end local v29           #pm:Landroid/os/PowerManager;
    :cond_336
    if-nez v18, :cond_33a

    #@338
    if-eqz v19, :cond_62

    #@33a
    .line 7174
    :cond_33a
    move-object/from16 v0, p0

    #@33c
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectOffset:Landroid/graphics/Point;

    #@33e
    iget v3, v3, Landroid/graphics/Point;->x:I

    #@340
    move-object/from16 v0, p0

    #@342
    invoke-direct {v0, v3}, Landroid/webkit/WebViewClassic;->viewToContentDimension(I)I

    #@345
    move-result v3

    #@346
    add-int v23, v14, v3

    #@348
    .line 7176
    .local v23, handleX:I
    move-object/from16 v0, p0

    #@34a
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectOffset:Landroid/graphics/Point;

    #@34c
    iget v3, v3, Landroid/graphics/Point;->y:I

    #@34e
    move-object/from16 v0, p0

    #@350
    invoke-direct {v0, v3}, Landroid/webkit/WebViewClassic;->viewToContentDimension(I)I

    #@353
    move-result v3

    #@354
    add-int v24, v15, v3

    #@356
    .line 7178
    .local v24, handleY:I
    move-object/from16 v0, p0

    #@358
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@35a
    move/from16 v0, v23

    #@35c
    move/from16 v1, v24

    #@35e
    invoke-virtual {v3, v0, v1}, Landroid/graphics/Point;->set(II)V

    #@361
    .line 7179
    move-object/from16 v0, p0

    #@363
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@365
    move/from16 v0, v23

    #@367
    int-to-float v4, v0

    #@368
    move/from16 v0, v24

    #@36a
    int-to-float v5, v0

    #@36b
    invoke-virtual {v3, v4, v5}, Landroid/webkit/QuadF;->containsPoint(FF)Z

    #@36e
    move-result v25

    #@36f
    .line 7181
    .local v25, inCursorText:Z
    move-object/from16 v0, p0

    #@371
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@373
    move/from16 v0, v23

    #@375
    move/from16 v1, v24

    #@377
    invoke-virtual {v3, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    #@37a
    move-result v26

    #@37b
    .line 7183
    .local v26, inEditBounds:Z
    move-object/from16 v0, p0

    #@37d
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@37f
    if-eqz v3, :cond_406

    #@381
    if-nez v26, :cond_406

    #@383
    .line 7184
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->beginScrollEdit()V

    #@386
    .line 7188
    :goto_386
    const/16 v35, 0x0

    #@388
    .line 7189
    .local v35, snapped:Z
    if-nez v25, :cond_392

    #@38a
    move-object/from16 v0, p0

    #@38c
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@38e
    if-eqz v3, :cond_397

    #@390
    if-nez v26, :cond_397

    #@392
    .line 7190
    :cond_392
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->snapDraggingCursor()V

    #@395
    .line 7191
    const/16 v35, 0x1

    #@397
    .line 7193
    :cond_397
    move-object/from16 v0, p0

    #@399
    move/from16 v1, v35

    #@39b
    invoke-direct {v0, v1}, Landroid/webkit/WebViewClassic;->updateWebkitSelection(Z)V

    #@39e
    .line 7195
    move-object/from16 v0, p0

    #@3a0
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@3a2
    if-eqz v3, :cond_3c1

    #@3a4
    move-object/from16 v0, p0

    #@3a6
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@3a8
    invoke-virtual {v3}, Landroid/webkit/LGSelectActionPopupWindow;->getEnableSelectAll()Z

    #@3ab
    move-result v3

    #@3ac
    if-nez v3, :cond_3c1

    #@3ae
    .line 7196
    move-object/from16 v0, p0

    #@3b0
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsSelectingInEditText:Z

    #@3b2
    if-nez v3, :cond_3c1

    #@3b4
    .line 7197
    move-object/from16 v0, p0

    #@3b6
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@3b8
    const/4 v4, 0x1

    #@3b9
    invoke-virtual {v3, v4}, Landroid/webkit/LGSelectActionPopupWindow;->enableSelectAll(Z)V

    #@3bc
    .line 7198
    const/4 v3, 0x0

    #@3bd
    move-object/from16 v0, p0

    #@3bf
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsSelectingAll:Z

    #@3c1
    .line 7202
    :cond_3c1
    if-nez v25, :cond_3ce

    #@3c3
    move-object/from16 v0, p0

    #@3c5
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@3c7
    if-eqz v3, :cond_3ce

    #@3c9
    if-eqz v26, :cond_3ce

    #@3cb
    .line 7204
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->snapDraggingCursor()V

    #@3ce
    .line 7206
    :cond_3ce
    move/from16 v0, p3

    #@3d0
    move-object/from16 v1, p0

    #@3d2
    iput v0, v1, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@3d4
    .line 7207
    move/from16 v0, p4

    #@3d6
    move-object/from16 v1, p0

    #@3d8
    iput v0, v1, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@3da
    .line 7208
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@3dd
    goto/16 :goto_62

    #@3df
    .line 7152
    .end local v23           #handleX:I
    .end local v24           #handleY:I
    .end local v25           #inCursorText:Z
    .end local v26           #inEditBounds:Z
    .end local v35           #snapped:Z
    :cond_3df
    move-object/from16 v0, p0

    #@3e1
    iget v3, v0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollX:I

    #@3e3
    move/from16 v0, p3

    #@3e5
    if-lt v0, v3, :cond_3ea

    #@3e7
    const/4 v3, 0x5

    #@3e8
    goto/16 :goto_2d5

    #@3ea
    :cond_3ea
    const/4 v3, 0x0

    #@3eb
    goto/16 :goto_2d5

    #@3ed
    .line 7154
    :cond_3ed
    move-object/from16 v0, p0

    #@3ef
    iget v3, v0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollY:I

    #@3f1
    move/from16 v0, p4

    #@3f3
    if-lt v0, v3, :cond_3f8

    #@3f5
    const/4 v3, 0x5

    #@3f6
    goto/16 :goto_2e2

    #@3f8
    :cond_3f8
    const/4 v3, 0x0

    #@3f9
    goto/16 :goto_2e2

    #@3fb
    .line 7165
    .restart local v29       #pm:Landroid/os/PowerManager;
    :catch_3fb
    move-exception v10

    #@3fc
    .line 7166
    .local v10, SE:Ljava/lang/SecurityException;
    const-string/jumbo v3, "webview"

    #@3ff
    const-string v4, "Text Selection Scroll : WakeLock Acquire fail becauseof permission"

    #@401
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@404
    goto/16 :goto_326

    #@406
    .line 7186
    .end local v10           #SE:Ljava/lang/SecurityException;
    .end local v29           #pm:Landroid/os/PowerManager;
    .restart local v23       #handleX:I
    .restart local v24       #handleY:I
    .restart local v25       #inCursorText:Z
    .restart local v26       #inEditBounds:Z
    :cond_406
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->endScrollEdit()V

    #@409
    goto/16 :goto_386

    #@40b
    .line 7213
    .end local v23           #handleX:I
    .end local v24           #handleY:I
    .end local v25           #inCursorText:Z
    .end local v26           #inEditBounds:Z
    .end local v28           #parent:Landroid/view/ViewParent;
    :cond_40b
    move-object/from16 v0, p0

    #@40d
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@40f
    const/4 v4, 0x7

    #@410
    if-eq v3, v4, :cond_62

    #@412
    .line 7218
    move-object/from16 v0, p0

    #@414
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@416
    if-nez v3, :cond_4b9

    #@418
    .line 7219
    const-string/jumbo v3, "webview"

    #@41b
    new-instance v4, Ljava/lang/StringBuilder;

    #@41d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@420
    const-string v5, "Got null mVelocityTracker when  mTouchMode = "

    #@422
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@425
    move-result-object v4

    #@426
    move-object/from16 v0, p0

    #@428
    iget v5, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@42a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42d
    move-result-object v4

    #@42e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@431
    move-result-object v4

    #@432
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@435
    .line 7225
    :goto_435
    move-object/from16 v0, p0

    #@437
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@439
    const/4 v4, 0x3

    #@43a
    if-eq v3, v4, :cond_4af

    #@43c
    move-object/from16 v0, p0

    #@43e
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@440
    const/16 v4, 0x9

    #@442
    if-eq v3, v4, :cond_4af

    #@444
    move-object/from16 v0, p0

    #@446
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@448
    const/16 v4, 0xa

    #@44a
    if-eq v3, v4, :cond_4af

    #@44c
    .line 7229
    move-object/from16 v0, p0

    #@44e
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mConfirmMove:Z

    #@450
    if-eqz v3, :cond_62

    #@452
    .line 7236
    move-object/from16 v0, p0

    #@454
    move/from16 v1, v18

    #@456
    move/from16 v2, v19

    #@458
    invoke-direct {v0, v1, v2}, Landroid/webkit/WebViewClassic;->calculateDragAngle(II)F

    #@45b
    move-result v3

    #@45c
    move-object/from16 v0, p0

    #@45e
    iput v3, v0, Landroid/webkit/WebViewClassic;->mAverageAngle:F

    #@460
    .line 7237
    if-eqz v20, :cond_468

    #@462
    invoke-virtual/range {v20 .. v20}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    #@465
    move-result v3

    #@466
    if-nez v3, :cond_48c

    #@468
    .line 7242
    :cond_468
    move-object/from16 v0, p0

    #@46a
    iget v3, v0, Landroid/webkit/WebViewClassic;->mAverageAngle:F

    #@46c
    const/high16 v4, 0x3e80

    #@46e
    cmpg-float v3, v3, v4

    #@470
    if-gez v3, :cond_4c6

    #@472
    move-object/from16 v0, p0

    #@474
    iget v3, v0, Landroid/webkit/WebViewClassic;->mAverageAngle:F

    #@476
    const/4 v4, 0x0

    #@477
    cmpl-float v3, v3, v4

    #@479
    if-lez v3, :cond_4c6

    #@47b
    .line 7243
    const/4 v3, 0x2

    #@47c
    move-object/from16 v0, p0

    #@47e
    iput v3, v0, Landroid/webkit/WebViewClassic;->mSnapScrollMode:I

    #@480
    .line 7244
    if-lez v18, :cond_4c4

    #@482
    const/4 v3, 0x1

    #@483
    :goto_483
    move-object/from16 v0, p0

    #@485
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSnapPositive:Z

    #@487
    .line 7245
    const/4 v3, 0x0

    #@488
    move-object/from16 v0, p0

    #@48a
    iput v3, v0, Landroid/webkit/WebViewClassic;->mAverageAngle:F

    #@48c
    .line 7253
    :cond_48c
    :goto_48c
    const/4 v3, 0x3

    #@48d
    move-object/from16 v0, p0

    #@48f
    iput v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@491
    .line 7254
    move/from16 v0, p3

    #@493
    move-object/from16 v1, p0

    #@495
    iput v0, v1, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@497
    .line 7255
    move/from16 v0, p4

    #@499
    move-object/from16 v1, p0

    #@49b
    iput v0, v1, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@49d
    .line 7256
    const/16 v18, 0x0

    #@49f
    .line 7257
    const/16 v19, 0x0

    #@4a1
    .line 7259
    move/from16 v0, p3

    #@4a3
    int-to-float v3, v0

    #@4a4
    move/from16 v0, p4

    #@4a6
    int-to-float v4, v0

    #@4a7
    move-object/from16 v0, p0

    #@4a9
    invoke-direct {v0, v3, v4}, Landroid/webkit/WebViewClassic;->startScrollingLayer(FF)V

    #@4ac
    .line 7260
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->startDrag()V

    #@4af
    .line 7268
    :cond_4af
    const/16 v27, 0x0

    #@4b1
    .line 7269
    .local v27, keepScrollBarsVisible:Z
    if-nez v18, :cond_4e5

    #@4b3
    if-nez v19, :cond_4e5

    #@4b5
    .line 7270
    const/16 v27, 0x1

    #@4b7
    goto/16 :goto_62

    #@4b9
    .line 7222
    .end local v27           #keepScrollBarsVisible:Z
    :cond_4b9
    move-object/from16 v0, p0

    #@4bb
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@4bd
    move-object/from16 v0, p1

    #@4bf
    invoke-virtual {v3, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@4c2
    goto/16 :goto_435

    #@4c4
    .line 7244
    :cond_4c4
    const/4 v3, 0x0

    #@4c5
    goto :goto_483

    #@4c6
    .line 7246
    :cond_4c6
    move-object/from16 v0, p0

    #@4c8
    iget v3, v0, Landroid/webkit/WebViewClassic;->mAverageAngle:F

    #@4ca
    const/high16 v4, 0x3fa0

    #@4cc
    cmpl-float v3, v3, v4

    #@4ce
    if-lez v3, :cond_48c

    #@4d0
    .line 7247
    const/4 v3, 0x4

    #@4d1
    move-object/from16 v0, p0

    #@4d3
    iput v3, v0, Landroid/webkit/WebViewClassic;->mSnapScrollMode:I

    #@4d5
    .line 7248
    if-lez v19, :cond_4e3

    #@4d7
    const/4 v3, 0x1

    #@4d8
    :goto_4d8
    move-object/from16 v0, p0

    #@4da
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSnapPositive:Z

    #@4dc
    .line 7249
    const/high16 v3, 0x4000

    #@4de
    move-object/from16 v0, p0

    #@4e0
    iput v3, v0, Landroid/webkit/WebViewClassic;->mAverageAngle:F

    #@4e2
    goto :goto_48c

    #@4e3
    .line 7248
    :cond_4e3
    const/4 v3, 0x0

    #@4e4
    goto :goto_4d8

    #@4e5
    .line 7301
    .restart local v27       #keepScrollBarsVisible:Z
    :cond_4e5
    move-object/from16 v0, p0

    #@4e7
    iget v3, v0, Landroid/webkit/WebViewClassic;->mSnapScrollMode:I

    #@4e9
    if-eqz v3, :cond_4f6

    #@4eb
    .line 7302
    move-object/from16 v0, p0

    #@4ed
    iget v3, v0, Landroid/webkit/WebViewClassic;->mSnapScrollMode:I

    #@4ef
    and-int/lit8 v3, v3, 0x2

    #@4f1
    const/4 v4, 0x2

    #@4f2
    if-ne v3, v4, :cond_526

    #@4f4
    .line 7303
    const/16 v19, 0x0

    #@4f6
    .line 7308
    :cond_4f6
    :goto_4f6
    mul-int v3, v18, v18

    #@4f8
    mul-int v4, v19, v19

    #@4fa
    add-int/2addr v3, v4

    #@4fb
    move-object/from16 v0, p0

    #@4fd
    iget v4, v0, Landroid/webkit/WebViewClassic;->mTouchSlopSquare:I

    #@4ff
    if-le v3, v4, :cond_529

    #@501
    .line 7309
    const/4 v3, 0x0

    #@502
    move-object/from16 v0, p0

    #@504
    iput v3, v0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@506
    .line 7324
    :goto_506
    move-wide/from16 v0, v21

    #@508
    move-object/from16 v2, p0

    #@50a
    iput-wide v0, v2, Landroid/webkit/WebViewClassic;->mLastTouchTime:J

    #@50c
    .line 7325
    move-object/from16 v0, p0

    #@50e
    move/from16 v1, v18

    #@510
    move/from16 v2, v19

    #@512
    invoke-direct {v0, v1, v2}, Landroid/webkit/WebViewClassic;->doDrag(II)Z

    #@515
    move-result v11

    #@516
    .line 7326
    .local v11, allDrag:Z
    if-eqz v11, :cond_531

    #@518
    .line 7327
    move/from16 v0, p3

    #@51a
    move-object/from16 v1, p0

    #@51c
    iput v0, v1, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@51e
    .line 7328
    move/from16 v0, p4

    #@520
    move-object/from16 v1, p0

    #@522
    iput v0, v1, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@524
    goto/16 :goto_62

    #@526
    .line 7305
    .end local v11           #allDrag:Z
    :cond_526
    const/16 v18, 0x0

    #@528
    goto :goto_4f6

    #@529
    .line 7311
    :cond_529
    const/4 v3, 0x2

    #@52a
    move-object/from16 v0, p0

    #@52c
    iput v3, v0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@52e
    .line 7312
    const/16 v27, 0x1

    #@530
    goto :goto_506

    #@531
    .line 7330
    .restart local v11       #allDrag:Z
    :cond_531
    move/from16 v0, v18

    #@533
    int-to-float v3, v0

    #@534
    move-object/from16 v0, p0

    #@536
    iget-object v4, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@538
    invoke-virtual {v4}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@53b
    move-result v4

    #@53c
    mul-float/2addr v3, v4

    #@53d
    float-to-double v3, v3

    #@53e
    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    #@541
    move-result-wide v3

    #@542
    double-to-int v12, v3

    #@543
    .line 7331
    .local v12, contentDeltaX:I
    move-object/from16 v0, p0

    #@545
    invoke-virtual {v0, v12}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@548
    move-result v30

    #@549
    .line 7332
    .local v30, roundedDeltaX:I
    move/from16 v0, v19

    #@54b
    int-to-float v3, v0

    #@54c
    move-object/from16 v0, p0

    #@54e
    iget-object v4, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@550
    invoke-virtual {v4}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@553
    move-result v4

    #@554
    mul-float/2addr v3, v4

    #@555
    float-to-double v3, v3

    #@556
    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    #@559
    move-result-wide v3

    #@55a
    double-to-int v13, v3

    #@55b
    .line 7333
    .local v13, contentDeltaY:I
    move-object/from16 v0, p0

    #@55d
    invoke-virtual {v0, v13}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@560
    move-result v31

    #@561
    .line 7334
    .local v31, roundedDeltaY:I
    move-object/from16 v0, p0

    #@563
    iget v3, v0, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@565
    sub-int v3, v3, v30

    #@567
    move-object/from16 v0, p0

    #@569
    iput v3, v0, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@56b
    .line 7335
    move-object/from16 v0, p0

    #@56d
    iget v3, v0, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@56f
    sub-int v3, v3, v31

    #@571
    move-object/from16 v0, p0

    #@573
    iput v3, v0, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@575
    goto/16 :goto_62

    #@577
    .line 7343
    .end local v11           #allDrag:Z
    .end local v12           #contentDeltaX:I
    .end local v13           #contentDeltaY:I
    .end local v16           #deltaTime:J
    .end local v27           #keepScrollBarsVisible:Z
    .end local v30           #roundedDeltaX:I
    .end local v31           #roundedDeltaY:I
    :pswitch_577
    move-object/from16 v0, p0

    #@579
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@57b
    if-eqz v3, :cond_58e

    #@57d
    move-object/from16 v0, p0

    #@57f
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@581
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@584
    move-result v3

    #@585
    if-eqz v3, :cond_58e

    #@587
    .line 7344
    move-object/from16 v0, p0

    #@589
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@58b
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    #@58e
    .line 7347
    :cond_58e
    move-object/from16 v0, p0

    #@590
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@592
    if-eqz v3, :cond_5bc

    #@594
    move-object/from16 v0, p0

    #@596
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@598
    if-eqz v3, :cond_5bc

    #@59a
    .line 7348
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->endScrollEdit()V

    #@59d
    .line 7349
    move-object/from16 v0, p0

    #@59f
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@5a1
    const/16 v4, 0x99

    #@5a3
    const-wide/16 v5, 0x10

    #@5a5
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@5a8
    .line 7351
    move-object/from16 v0, p0

    #@5aa
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mConfirmMove:Z

    #@5ac
    if-nez v3, :cond_5bc

    #@5ae
    move-object/from16 v0, p0

    #@5b0
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@5b2
    if-eqz v3, :cond_5bc

    #@5b4
    .line 7352
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->showPasteWindow()V

    #@5b7
    .line 7353
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->stopTouch()V

    #@5ba
    goto/16 :goto_62

    #@5bc
    .line 7357
    :cond_5bc
    move-wide/from16 v0, v21

    #@5be
    move-object/from16 v2, p0

    #@5c0
    iput-wide v0, v2, Landroid/webkit/WebViewClassic;->mLastTouchUpTime:J

    #@5c2
    .line 7358
    move-object/from16 v0, p0

    #@5c4
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSentAutoScrollMessage:Z

    #@5c6
    if-eqz v3, :cond_5d1

    #@5c8
    .line 7359
    const/4 v3, 0x0

    #@5c9
    move-object/from16 v0, p0

    #@5cb
    iput v3, v0, Landroid/webkit/WebViewClassic;->mAutoScrollY:I

    #@5cd
    move-object/from16 v0, p0

    #@5cf
    iput v3, v0, Landroid/webkit/WebViewClassic;->mAutoScrollX:I

    #@5d1
    .line 7361
    :cond_5d1
    move-object/from16 v0, p0

    #@5d3
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@5d5
    sparse-switch v3, :sswitch_data_73c

    #@5d8
    .line 7460
    :cond_5d8
    :goto_5d8
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->stopTouch()V

    #@5db
    goto/16 :goto_62

    #@5dd
    .line 7363
    :sswitch_5dd
    move-object/from16 v0, p0

    #@5df
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@5e1
    const/4 v4, 0x3

    #@5e2
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@5e5
    .line 7364
    move-object/from16 v0, p0

    #@5e7
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@5e9
    const/4 v4, 0x4

    #@5ea
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@5ed
    .line 7365
    const/4 v3, 0x7

    #@5ee
    move-object/from16 v0, p0

    #@5f0
    iput v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@5f2
    goto :goto_5d8

    #@5f3
    .line 7370
    :sswitch_5f3
    move-object/from16 v0, p0

    #@5f5
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@5f7
    const/4 v4, 0x3

    #@5f8
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@5fb
    .line 7371
    move-object/from16 v0, p0

    #@5fd
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@5ff
    const/4 v4, 0x4

    #@600
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@603
    .line 7372
    move-object/from16 v0, p0

    #@605
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mConfirmMove:Z

    #@607
    if-nez v3, :cond_63a

    #@609
    .line 7373
    move-object/from16 v0, p0

    #@60b
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@60d
    if-eqz v3, :cond_619

    #@60f
    .line 7375
    move-object/from16 v0, p0

    #@611
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@613
    if-nez v3, :cond_5d8

    #@615
    .line 7376
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@618
    goto :goto_5d8

    #@619
    .line 7382
    :cond_619
    move-object/from16 v0, p0

    #@61b
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@61d
    const/4 v4, 0x1

    #@61e
    if-ne v3, v4, :cond_5d8

    #@620
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->canZoomIn()Z

    #@623
    move-result v3

    #@624
    if-nez v3, :cond_62c

    #@626
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->canZoomOut()Z

    #@629
    move-result v3

    #@62a
    if-eqz v3, :cond_5d8

    #@62c
    .line 7384
    :cond_62c
    move-object/from16 v0, p0

    #@62e
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@630
    const/4 v4, 0x5

    #@631
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    #@634
    move-result v5

    #@635
    int-to-long v5, v5

    #@636
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@639
    goto :goto_5d8

    #@63a
    .line 7405
    :cond_63a
    :sswitch_63a
    move-object/from16 v0, p0

    #@63c
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@63e
    const/16 v4, 0x8

    #@640
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@643
    .line 7408
    move-object/from16 v0, p0

    #@645
    iget-wide v3, v0, Landroid/webkit/WebViewClassic;->mLastTouchTime:J

    #@647
    sub-long v3, v21, v3

    #@649
    const-wide/16 v5, 0xfa

    #@64b
    cmp-long v3, v3, v5

    #@64d
    if-gtz v3, :cond_671

    #@64f
    .line 7409
    move-object/from16 v0, p0

    #@651
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@653
    if-nez v3, :cond_667

    #@655
    .line 7410
    const-string/jumbo v3, "webview"

    #@658
    const-string v4, "Got null mVelocityTracker"

    #@65a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@65d
    .line 7417
    :goto_65d
    const/4 v3, 0x3

    #@65e
    move-object/from16 v0, p0

    #@660
    iput v3, v0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@662
    .line 7418
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->doFling()V

    #@665
    goto/16 :goto_5d8

    #@667
    .line 7412
    :cond_667
    move-object/from16 v0, p0

    #@669
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@66b
    move-object/from16 v0, p1

    #@66d
    invoke-virtual {v3, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@670
    goto :goto_65d

    #@671
    .line 7421
    :cond_671
    move-object/from16 v0, p0

    #@673
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@675
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@678
    move-result v4

    #@679
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@67c
    move-result v5

    #@67d
    const/4 v6, 0x0

    #@67e
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollX()I

    #@681
    move-result v7

    #@682
    const/4 v8, 0x0

    #@683
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollY()I

    #@686
    move-result v9

    #@687
    invoke-virtual/range {v3 .. v9}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@68a
    move-result v3

    #@68b
    if-eqz v3, :cond_690

    #@68d
    .line 7424
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@690
    .line 7428
    :cond_690
    const/4 v3, 0x2

    #@691
    move-object/from16 v0, p0

    #@693
    iput v3, v0, Landroid/webkit/WebViewClassic;->mHeldMotionless:I

    #@695
    .line 7429
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@698
    .line 7435
    :sswitch_698
    const/4 v3, 0x0

    #@699
    move-object/from16 v0, p0

    #@69b
    iput v3, v0, Landroid/webkit/WebViewClassic;->mLastVelocity:F

    #@69d
    .line 7436
    invoke-static {}, Landroid/webkit/WebViewCore;->resumePriority()V

    #@6a0
    .line 7437
    move-object/from16 v0, p0

    #@6a2
    iget-boolean v3, v0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@6a4
    if-nez v3, :cond_5d8

    #@6a6
    .line 7438
    move-object/from16 v0, p0

    #@6a8
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@6aa
    invoke-static {v3}, Landroid/webkit/WebViewCore;->resumeUpdatePicture(Landroid/webkit/WebViewCore;)V

    #@6ad
    goto/16 :goto_5d8

    #@6af
    .line 7444
    :sswitch_6af
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@6b2
    move-result-wide v3

    #@6b3
    move-object/from16 v0, p0

    #@6b5
    iget-wide v5, v0, Landroid/webkit/WebViewClassic;->mActionPointerUpTime:J

    #@6b7
    sub-long/2addr v3, v5

    #@6b8
    const-wide/16 v5, 0x3e8

    #@6ba
    cmp-long v3, v3, v5

    #@6bc
    if-gtz v3, :cond_5d8

    #@6be
    .line 7448
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->doScrollToTop()V

    #@6c1
    goto/16 :goto_5d8

    #@6c3
    .line 7452
    :sswitch_6c3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@6c6
    move-result-wide v3

    #@6c7
    move-object/from16 v0, p0

    #@6c9
    iget-wide v5, v0, Landroid/webkit/WebViewClassic;->mActionPointerUpTime:J

    #@6cb
    sub-long/2addr v3, v5

    #@6cc
    const-wide/16 v5, 0x3e8

    #@6ce
    cmp-long v3, v3, v5

    #@6d0
    if-gtz v3, :cond_5d8

    #@6d2
    .line 7456
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->doScrollToBottom()V

    #@6d5
    goto/16 :goto_5d8

    #@6d7
    .line 7464
    :pswitch_6d7
    move-object/from16 v0, p0

    #@6d9
    iget v3, v0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@6db
    const/4 v4, 0x3

    #@6dc
    if-ne v3, v4, :cond_6fa

    #@6de
    .line 7465
    move-object/from16 v0, p0

    #@6e0
    iget-object v3, v0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@6e2
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@6e5
    move-result v4

    #@6e6
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@6e9
    move-result v5

    #@6ea
    const/4 v6, 0x0

    #@6eb
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollX()I

    #@6ee
    move-result v7

    #@6ef
    const/4 v8, 0x0

    #@6f0
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollY()I

    #@6f3
    move-result v9

    #@6f4
    invoke-virtual/range {v3 .. v9}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    #@6f7
    .line 7467
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@6fa
    .line 7469
    :cond_6fa
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->cancelTouch()V

    #@6fd
    goto/16 :goto_62

    #@6ff
    .line 7474
    :pswitch_6ff
    move-wide/from16 v0, v21

    #@701
    move-object/from16 v2, p0

    #@703
    iput-wide v0, v2, Landroid/webkit/WebViewClassic;->mLastPointerUpTime:J

    #@705
    .line 7476
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@708
    move-result-object v32

    #@709
    .line 7477
    .local v32, settings:Landroid/webkit/WebSettingsClassic;
    if-eqz v32, :cond_62

    #@70b
    invoke-virtual/range {v32 .. v32}, Landroid/webkit/WebSettingsClassic;->getScrollToTopBottomEnabled()Z

    #@70e
    move-result v3

    #@70f
    if-eqz v3, :cond_62

    #@711
    .line 7478
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@714
    move-result v3

    #@715
    const/4 v4, 0x2

    #@716
    if-le v3, v4, :cond_71d

    #@718
    .line 7479
    const/4 v3, 0x0

    #@719
    move-object/from16 v0, p0

    #@71b
    iput-boolean v3, v0, Landroid/webkit/WebViewClassic;->mIsProperPointsFor2fingerFlicking:Z

    #@71d
    .line 7482
    :cond_71d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@720
    move-result-wide v3

    #@721
    move-object/from16 v0, p0

    #@723
    iput-wide v3, v0, Landroid/webkit/WebViewClassic;->mActionPointerUpTime:J

    #@725
    .line 7483
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->decideScrollToTopBottom()V

    #@728
    goto/16 :goto_62

    #@72a
    .line 7033
    :pswitch_data_72a
    .packed-switch 0x0
        :pswitch_89
        :pswitch_577
        :pswitch_268
        :pswitch_6d7
        :pswitch_62
        :pswitch_62
        :pswitch_6ff
    .end packed-switch

    #@73c
    .line 7361
    :sswitch_data_73c
    .sparse-switch
        0x1 -> :sswitch_5f3
        0x2 -> :sswitch_698
        0x3 -> :sswitch_63a
        0x4 -> :sswitch_5f3
        0x5 -> :sswitch_5f3
        0x6 -> :sswitch_5dd
        0x9 -> :sswitch_63a
        0xa -> :sswitch_63a
        0x3e8 -> :sswitch_6af
        0x3e9 -> :sswitch_6c3
    .end sparse-switch
.end method

.method private hideLGSelectActionPopupWindow()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 10605
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@4
    move-result-object v0

    #@5
    .line 10606
    .local v0, settings:Landroid/webkit/WebSettings;
    if-eqz v0, :cond_35

    #@7
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getLGBubbleActionEnabled()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_35

    #@d
    .line 10607
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupShower:Ljava/lang/Runnable;

    #@f
    if-eqz v1, :cond_1c

    #@11
    .line 10608
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@14
    move-result-object v1

    #@15
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupShower:Ljava/lang/Runnable;

    #@17
    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@1a
    .line 10609
    iput-object v3, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupShower:Ljava/lang/Runnable;

    #@1c
    .line 10611
    :cond_1c
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@1e
    if-eqz v1, :cond_35

    #@20
    .line 10612
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@22
    invoke-virtual {v1}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@25
    .line 10613
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@27
    invoke-virtual {v1}, Landroid/webkit/LGSelectActionPopupWindow;->getEnableSelectAll()Z

    #@2a
    move-result v1

    #@2b
    if-nez v1, :cond_33

    #@2d
    .line 10614
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@2f
    const/4 v2, 0x1

    #@30
    invoke-virtual {v1, v2}, Landroid/webkit/LGSelectActionPopupWindow;->enableSelectAll(Z)V

    #@33
    .line 10615
    :cond_33
    iput-object v3, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@35
    .line 10618
    :cond_35
    return-void
.end method

.method private hidePasteButton()V
    .registers 2

    #@0
    .prologue
    .line 6062
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPasteWindow:Landroid/webkit/WebViewClassic$PastePopupWindow;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 6063
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPasteWindow:Landroid/webkit/WebViewClassic$PastePopupWindow;

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic$PastePopupWindow;->hide()V

    #@9
    .line 6065
    :cond_9
    return-void
.end method

.method private hideSoftKeyboard()V
    .registers 4

    #@0
    .prologue
    .line 5575
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@3
    move-result-object v0

    #@4
    .line 5576
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_18

    #@6
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@8
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_18

    #@e
    .line 5577
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@10
    invoke-virtual {v1}, Landroid/webkit/WebView;->getWindowToken()Landroid/os/IBinder;

    #@13
    move-result-object v1

    #@14
    const/4 v2, 0x0

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@18
    .line 5579
    :cond_18
    return-void
.end method

.method private hitParagraphHandle(II)Z
    .registers 16
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 6944
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->viewToContentDimension(I)I

    #@3
    move-result p1

    #@4
    .line 6945
    invoke-direct {p0, p2}, Landroid/webkit/WebViewClassic;->viewToContentDimension(I)I

    #@7
    move-result p2

    #@8
    .line 6947
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@a
    iget v5, v8, Landroid/graphics/Point;->x:I

    #@c
    .line 6948
    .local v5, minX:I
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@e
    iget v1, v8, Landroid/graphics/Point;->x:I

    #@10
    .line 6949
    .local v1, maxX:I
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@12
    iget v6, v8, Landroid/graphics/Point;->y:I

    #@14
    .line 6950
    .local v6, minY:I
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@16
    iget v2, v8, Landroid/graphics/Point;->y:I

    #@18
    .line 6951
    .local v2, maxY:I
    add-int v8, v5, v1

    #@1a
    div-int/lit8 v3, v8, 0x2

    #@1c
    .line 6952
    .local v3, midX:I
    add-int v8, v6, v2

    #@1e
    div-int/lit8 v4, v8, 0x2

    #@20
    .line 6954
    .local v4, midY:I
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@22
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@25
    move-result-object v8

    #@26
    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@29
    move-result-object v8

    #@2a
    iget v0, v8, Landroid/util/DisplayMetrics;->density:F

    #@2c
    .line 6955
    .local v0, density:F
    const/high16 v8, 0x4180

    #@2e
    mul-float/2addr v8, v0

    #@2f
    float-to-int v7, v8

    #@30
    .line 6958
    .local v7, slop:I
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTestHitRect:Landroid/graphics/Rect;

    #@32
    sub-int v9, v3, v7

    #@34
    sub-int v10, v6, v7

    #@36
    add-int v11, v3, v7

    #@38
    add-int v12, v6, v7

    #@3a
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;->set(IIII)V

    #@3d
    .line 6959
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTestHitRect:Landroid/graphics/Rect;

    #@3f
    invoke-virtual {v8, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    #@42
    move-result v8

    #@43
    if-eqz v8, :cond_4f

    #@45
    .line 6961
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@47
    iput-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@49
    .line 6962
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBaseTextQuad:Landroid/webkit/QuadF;

    #@4b
    iput-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@4d
    .line 6963
    const/4 v8, 0x1

    #@4e
    .line 6990
    :goto_4e
    return v8

    #@4f
    .line 6966
    :cond_4f
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTestHitRect:Landroid/graphics/Rect;

    #@51
    sub-int v9, v5, v7

    #@53
    sub-int v10, v4, v7

    #@55
    add-int v11, v5, v7

    #@57
    add-int v12, v4, v7

    #@59
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;->set(IIII)V

    #@5c
    .line 6967
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTestHitRect:Landroid/graphics/Rect;

    #@5e
    invoke-virtual {v8, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    #@61
    move-result v8

    #@62
    if-eqz v8, :cond_6e

    #@64
    .line 6969
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@66
    iput-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@68
    .line 6970
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBaseTextQuad:Landroid/webkit/QuadF;

    #@6a
    iput-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@6c
    .line 6971
    const/4 v8, 0x1

    #@6d
    goto :goto_4e

    #@6e
    .line 6974
    :cond_6e
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTestHitRect:Landroid/graphics/Rect;

    #@70
    sub-int v9, v1, v7

    #@72
    sub-int v10, v4, v7

    #@74
    add-int v11, v1, v7

    #@76
    add-int v12, v4, v7

    #@78
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;->set(IIII)V

    #@7b
    .line 6975
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTestHitRect:Landroid/graphics/Rect;

    #@7d
    invoke-virtual {v8, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    #@80
    move-result v8

    #@81
    if-eqz v8, :cond_8d

    #@83
    .line 6977
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@85
    iput-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@87
    .line 6978
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtentTextQuad:Landroid/webkit/QuadF;

    #@89
    iput-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@8b
    .line 6979
    const/4 v8, 0x1

    #@8c
    goto :goto_4e

    #@8d
    .line 6982
    :cond_8d
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTestHitRect:Landroid/graphics/Rect;

    #@8f
    sub-int v9, v3, v7

    #@91
    sub-int v10, v2, v7

    #@93
    add-int v11, v3, v7

    #@95
    add-int v12, v2, v7

    #@97
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;->set(IIII)V

    #@9a
    .line 6983
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTestHitRect:Landroid/graphics/Rect;

    #@9c
    invoke-virtual {v8, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    #@9f
    move-result v8

    #@a0
    if-eqz v8, :cond_ac

    #@a2
    .line 6985
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@a4
    iput-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@a6
    .line 6986
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtentTextQuad:Landroid/webkit/QuadF;

    #@a8
    iput-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@aa
    .line 6987
    const/4 v8, 0x1

    #@ab
    goto :goto_4e

    #@ac
    .line 6990
    :cond_ac
    const/4 v8, 0x0

    #@ad
    goto :goto_4e
.end method

.method private inFullScreenMode()Z
    .registers 2

    #@0
    .prologue
    .line 6810
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFullScreenHolder:Landroid/webkit/PluginFullScreenHolder;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private init()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 2087
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v3}, Landroid/webkit/WebViewClassic$OnTrimMemoryListener;->init(Landroid/content/Context;)V

    #@6
    .line 2088
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@8
    const/4 v4, 0x0

    #@9
    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setWillNotDraw(Z)V

    #@c
    .line 2089
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@e
    invoke-virtual {v3, v5}, Landroid/webkit/WebView;->setClickable(Z)V

    #@11
    .line 2090
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@13
    invoke-virtual {v3, v5}, Landroid/webkit/WebView;->setLongClickable(Z)V

    #@16
    .line 2092
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@18
    invoke-static {v3}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@1b
    move-result-object v0

    #@1c
    .line 2093
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@1f
    move-result v2

    #@20
    .line 2094
    .local v2, slop:I
    mul-int v3, v2, v2

    #@22
    iput v3, p0, Landroid/webkit/WebViewClassic;->mTouchSlopSquare:I

    #@24
    .line 2095
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    #@27
    move-result v2

    #@28
    .line 2096
    mul-int v3, v2, v2

    #@2a
    iput v3, p0, Landroid/webkit/WebViewClassic;->mDoubleTapSlopSquare:I

    #@2c
    .line 2097
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@2e
    invoke-static {v3}, Landroid/webkit/WebViewCore;->getFixedDisplayDensity(Landroid/content/Context;)F

    #@31
    move-result v1

    #@32
    .line 2100
    .local v1, density:F
    iget v3, p0, Landroid/webkit/WebViewClassic;->TOUCH_NAV_SLOP_ABSOLUTE:I

    #@34
    int-to-float v3, v3

    #@35
    mul-float/2addr v3, v1

    #@36
    float-to-int v3, v3

    #@37
    iput v3, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@39
    .line 2101
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@3b
    invoke-virtual {v3, v1}, Landroid/webkit/ZoomManager;->init(F)V

    #@3e
    .line 2102
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@41
    move-result v3

    #@42
    iput v3, p0, Landroid/webkit/WebViewClassic;->mMaximumFling:I

    #@44
    .line 2105
    const/high16 v3, 0x3f80

    #@46
    mul-float v4, v1, v1

    #@48
    div-float/2addr v3, v4

    #@49
    iput v3, p0, Landroid/webkit/WebViewClassic;->DRAG_LAYER_INVERSE_DENSITY_SQUARED:F

    #@4b
    .line 2107
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    #@4e
    move-result v3

    #@4f
    iput v3, p0, Landroid/webkit/WebViewClassic;->mOverscrollDistance:I

    #@51
    .line 2108
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverflingDistance()I

    #@54
    move-result v3

    #@55
    iput v3, p0, Landroid/webkit/WebViewClassic;->mOverflingDistance:I

    #@57
    .line 2110
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@59
    invoke-virtual {v3}, Landroid/webkit/WebView$PrivateAccess;->super_getScrollBarStyle()I

    #@5c
    move-result v3

    #@5d
    invoke-virtual {p0, v3}, Landroid/webkit/WebViewClassic;->setScrollBarStyle(I)V

    #@60
    .line 2113
    new-instance v3, Ljava/util/Vector;

    #@62
    const/4 v4, 0x2

    #@63
    invoke-direct {v3, v4}, Ljava/util/Vector;-><init>(I)V

    #@66
    iput-object v3, p0, Landroid/webkit/WebViewClassic;->mKeysPressed:Ljava/util/Vector;

    #@68
    .line 2114
    const/4 v3, 0x0

    #@69
    iput-object v3, p0, Landroid/webkit/WebViewClassic;->mHTML5VideoViewProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@6b
    .line 2115
    return-void
.end method

.method private invalidateContentRect(Landroid/graphics/Rect;)V
    .registers 6
    .parameter "r"

    #@0
    .prologue
    .line 3588
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p1, Landroid/graphics/Rect;->top:I

    #@4
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@6
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    #@8
    invoke-direct {p0, v0, v1, v2, v3}, Landroid/webkit/WebViewClassic;->viewInvalidate(IIII)V

    #@b
    .line 3589
    return-void
.end method

.method private isAccessibilityInjectionEnabled()Z
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2200
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v4}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@7
    move-result-object v0

    #@8
    .line 2201
    .local v0, manager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@b
    move-result v4

    #@c
    if-nez v4, :cond_f

    #@e
    .line 2213
    :cond_e
    :goto_e
    return v2

    #@f
    .line 2207
    :cond_f
    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    #@12
    move-result-object v1

    #@13
    .line 2209
    .local v1, services:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    #@16
    move-result v4

    #@17
    if-nez v4, :cond_e

    #@19
    move v2, v3

    #@1a
    .line 2213
    goto :goto_e
.end method

.method private isEnterActionKey(I)Z
    .registers 3
    .parameter "keyCode"

    #@0
    .prologue
    .line 5773
    const/16 v0, 0x17

    #@2
    if-eq p1, v0, :cond_c

    #@4
    const/16 v0, 0x42

    #@6
    if-eq p1, v0, :cond_c

    #@8
    const/16 v0, 0xa0

    #@a
    if-ne p1, v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private isHandleVisible(Landroid/graphics/Point;I)Z
    .registers 7
    .parameter "selectionPoint"
    .parameter "layerId"

    #@0
    .prologue
    .line 5450
    const/4 v0, 0x1

    #@1
    .line 5451
    .local v0, isVisible:Z
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@3
    if-eqz v1, :cond_f

    #@5
    .line 5452
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@7
    iget v2, p1, Landroid/graphics/Point;->x:I

    #@9
    iget v3, p1, Landroid/graphics/Point;->y:I

    #@b
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    #@e
    move-result v0

    #@f
    .line 5455
    :cond_f
    if-eqz v0, :cond_1b

    #@11
    .line 5456
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@13
    iget v2, p1, Landroid/graphics/Point;->x:I

    #@15
    iget v3, p1, Landroid/graphics/Point;->y:I

    #@17
    invoke-static {v1, p2, v2, v3}, Landroid/webkit/WebViewClassic;->nativeIsPointVisible(IIII)Z

    #@1a
    move-result v0

    #@1b
    .line 5459
    :cond_1b
    return v0
.end method

.method private isPrintableAsciiExceptsSpace(I)Z
    .registers 3
    .parameter "unicode"

    #@0
    .prologue
    .line 10311
    const/16 v0, 0x21

    #@2
    if-gt v0, p1, :cond_a

    #@4
    const/16 v0, 0x7e

    #@6
    if-gt p1, v0, :cond_a

    #@8
    .line 10312
    const/4 v0, 0x1

    #@9
    .line 10314
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private isScrollableForAccessibility()Z
    .registers 4

    #@0
    .prologue
    .line 2224
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getContentWidth()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@7
    move-result v0

    #@8
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWidth()I

    #@b
    move-result v1

    #@c
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@e
    invoke-virtual {v2}, Landroid/webkit/WebView;->getPaddingLeft()I

    #@11
    move-result v2

    #@12
    sub-int/2addr v1, v2

    #@13
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@15
    invoke-virtual {v2}, Landroid/webkit/WebView;->getPaddingRight()I

    #@18
    move-result v2

    #@19
    sub-int/2addr v1, v2

    #@1a
    if-gt v0, v1, :cond_38

    #@1c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getContentHeight()I

    #@1f
    move-result v0

    #@20
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@23
    move-result v0

    #@24
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@27
    move-result v1

    #@28
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2a
    invoke-virtual {v2}, Landroid/webkit/WebView;->getPaddingTop()I

    #@2d
    move-result v2

    #@2e
    sub-int/2addr v1, v2

    #@2f
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@31
    invoke-virtual {v2}, Landroid/webkit/WebView;->getPaddingBottom()I

    #@34
    move-result v2

    #@35
    sub-int/2addr v1, v2

    #@36
    if-le v0, v1, :cond_3a

    #@38
    :cond_38
    const/4 v0, 0x1

    #@39
    :goto_39
    return v0

    #@3a
    :cond_3a
    const/4 v0, 0x0

    #@3b
    goto :goto_39
.end method

.method private isTextInputMoveEvent(Landroid/view/KeyEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 10298
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@2
    if-eqz v0, :cond_26

    #@4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@7
    move-result v0

    #@8
    const/16 v1, 0x15

    #@a
    if-eq v0, v1, :cond_24

    #@c
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@f
    move-result v0

    #@10
    const/16 v1, 0x16

    #@12
    if-eq v0, v1, :cond_24

    #@14
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@17
    move-result v0

    #@18
    const/16 v1, 0x13

    #@1a
    if-eq v0, v1, :cond_24

    #@1c
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@1f
    move-result v0

    #@20
    const/16 v1, 0x14

    #@22
    if-ne v0, v1, :cond_26

    #@24
    .line 10303
    :cond_24
    const/4 v0, 0x1

    #@25
    .line 10305
    :goto_25
    return v0

    #@26
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_25
.end method

.method private keyCodeToSoundsEffect(I)I
    .registers 3
    .parameter "keyCode"

    #@0
    .prologue
    .line 8038
    packed-switch p1, :pswitch_data_e

    #@3
    .line 8048
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 8040
    :pswitch_5
    const/4 v0, 0x2

    #@6
    goto :goto_4

    #@7
    .line 8042
    :pswitch_7
    const/4 v0, 0x3

    #@8
    goto :goto_4

    #@9
    .line 8044
    :pswitch_9
    const/4 v0, 0x4

    #@a
    goto :goto_4

    #@b
    .line 8046
    :pswitch_b
    const/4 v0, 0x1

    #@c
    goto :goto_4

    #@d
    .line 8038
    nop

    #@e
    :pswitch_data_e
    .packed-switch 0x13
        :pswitch_5
        :pswitch_9
        :pswitch_b
        :pswitch_7
    .end packed-switch
.end method

.method private letPageHandleNavKey(IJZI)V
    .registers 18
    .parameter "keyCode"
    .parameter "time"
    .parameter "down"
    .parameter "metaState"

    #@0
    .prologue
    .line 10236
    if-eqz p4, :cond_1c

    #@2
    .line 10237
    const/4 v5, 0x0

    #@3
    .line 10242
    .local v5, keyEventAction:I
    :goto_3
    new-instance v0, Landroid/view/KeyEvent;

    #@5
    const/4 v7, 0x1

    #@6
    and-int/lit8 v1, p5, 0x1

    #@8
    and-int/lit8 v2, p5, 0x2

    #@a
    or-int/2addr v1, v2

    #@b
    and-int/lit8 v2, p5, 0x4

    #@d
    or-int v8, v1, v2

    #@f
    const/4 v9, -0x1

    #@10
    const/4 v10, 0x0

    #@11
    const/4 v11, 0x0

    #@12
    move-wide v1, p2

    #@13
    move-wide v3, p2

    #@14
    move v6, p1

    #@15
    invoke-direct/range {v0 .. v11}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@18
    .line 10247
    .local v0, event:Landroid/view/KeyEvent;
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->sendKeyEvent(Landroid/view/KeyEvent;)V

    #@1b
    .line 10248
    return-void

    #@1c
    .line 10239
    .end local v0           #event:Landroid/view/KeyEvent;
    .end local v5           #keyEventAction:I
    :cond_1c
    const/4 v5, 0x1

    #@1d
    .restart local v5       #keyEventAction:I
    goto :goto_3
.end method

.method private loadDataImpl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "data"
    .parameter "mimeType"
    .parameter "encoding"

    #@0
    .prologue
    .line 3061
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "data:"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 3062
    .local v0, dataUrl:Ljava/lang/StringBuilder;
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 3063
    const-string v1, "base64"

    #@c
    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_17

    #@12
    .line 3064
    const-string v1, ";base64"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 3066
    :cond_17
    const-string v1, ","

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 3067
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    .line 3068
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {p0, v1}, Landroid/webkit/WebViewClassic;->loadUrlImpl(Ljava/lang/String;)V

    #@26
    .line 3069
    return-void
.end method

.method private loadUrlImpl(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 3021
    if-nez p1, :cond_3

    #@2
    .line 3025
    :goto_2
    return-void

    #@3
    .line 3024
    :cond_3
    const/4 v0, 0x0

    #@4
    invoke-direct {p0, p1, v0}, Landroid/webkit/WebViewClassic;->loadUrlImpl(Ljava/lang/String;Ljava/util/Map;)V

    #@7
    goto :goto_2
.end method

.method private loadUrlImpl(Ljava/lang/String;Ljava/util/Map;)V
    .registers 6
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2998
    .local p2, extraHeaders:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_11

    #@6
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1, p1}, Lcom/lge/cappuccino/IMdm;->checkWebView(Ljava/lang/String;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_11

    #@10
    .line 3010
    :goto_10
    return-void

    #@11
    .line 3004
    :cond_11
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@14
    .line 3005
    new-instance v0, Landroid/webkit/WebViewCore$GetUrlData;

    #@16
    invoke-direct {v0}, Landroid/webkit/WebViewCore$GetUrlData;-><init>()V

    #@19
    .line 3006
    .local v0, arg:Landroid/webkit/WebViewCore$GetUrlData;
    iput-object p1, v0, Landroid/webkit/WebViewCore$GetUrlData;->mUrl:Ljava/lang/String;

    #@1b
    .line 3007
    iput-object p2, v0, Landroid/webkit/WebViewCore$GetUrlData;->mExtraHeaders:Ljava/util/Map;

    #@1d
    .line 3008
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@1f
    const/16 v2, 0x64

    #@21
    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@24
    .line 3009
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->clearHelpers()V

    #@27
    goto :goto_10
.end method

.method private native nativeCopyBaseContentToPicture(Landroid/graphics/Picture;)V
.end method

.method private native nativeCreate(ILjava/lang/String;Z)V
.end method

.method private native nativeCreateDrawGLFunction(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/RectF;FI)I
.end method

.method private native nativeDebugDump()V
.end method

.method private static native nativeDestroy(I)V
.end method

.method private native nativeDiscardAllTextures()V
.end method

.method private native nativeDraw(Landroid/graphics/Canvas;Landroid/graphics/RectF;II)V
.end method

.method private native nativeDumpDisplayTree(Ljava/lang/String;)V
.end method

.method private native nativeDumpLayerContentToPicture(ILjava/lang/String;ILandroid/graphics/Picture;)Z
.end method

.method private native nativeEvaluateLayersAnimations(I)Z
.end method

.method private static native nativeFindMaxVisibleRect(IILandroid/graphics/Rect;)V
.end method

.method private native nativeGetBackgroundColor(I)I
.end method

.method private native nativeGetBaseLayer(I)I
.end method

.method private native nativeGetDrawGLFunction(I)I
.end method

.method private static native nativeGetHandleLayerId(IILandroid/graphics/Point;Landroid/webkit/QuadF;)I
.end method

.method private native nativeGetSelection()Ljava/lang/String;
.end method

.method private native nativeGetSelectionParagraphBounds(ILandroid/graphics/Point;Landroid/graphics/Point;)Z
.end method

.method private native nativeHasContent()Z
.end method

.method private static native nativeIsHandleLeft(II)Z
.end method

.method private static native nativeIsPointVisible(IIII)Z
.end method

.method private static native nativeMapLayerRect(IILandroid/graphics/Rect;)V
.end method

.method private static native nativeOnTrimMemory(I)V
.end method

.method private native nativeScrollLayer(IIII)Z
.end method

.method private native nativeScrollableLayer(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)I
.end method

.method private native nativeSetBaseLayer(IIZZI)Z
.end method

.method private native nativeSetHeightCanMeasure(Z)V
.end method

.method private static native nativeSetHwAccelerated(IZ)I
.end method

.method private native nativeSetIsScrolling(Z)V
.end method

.method private static native nativeSetPauseDrawing(IZ)V
.end method

.method private static native nativeSetTextSelection(II)V
.end method

.method private native nativeStopGL(I)V
.end method

.method private native nativeTileProfilingClear()V
.end method

.method private native nativeTileProfilingGetFloat(IILjava/lang/String;)F
.end method

.method private native nativeTileProfilingGetInt(IILjava/lang/String;)I
.end method

.method private native nativeTileProfilingNumFrames()I
.end method

.method private native nativeTileProfilingNumTilesInFrame(I)I
.end method

.method private native nativeTileProfilingStart()V
.end method

.method private native nativeTileProfilingStop()F
.end method

.method private native nativeUpdateDrawGLFunction(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/RectF;F)V
.end method

.method private native nativeUseHardwareAccelSkia(Z)V
.end method

.method private onHandleUiEvent(Landroid/view/MotionEvent;II)V
    .registers 8
    .parameter "event"
    .parameter "eventType"
    .parameter "flags"

    #@0
    .prologue
    .line 1714
    packed-switch p2, :pswitch_data_5c

    #@3
    .line 1745
    :cond_3
    :goto_3
    :pswitch_3
    return-void

    #@4
    .line 1716
    :pswitch_4
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getHitTestResult()Landroid/webkit/WebView$HitTestResult;

    #@7
    move-result-object v0

    #@8
    .line 1717
    .local v0, hitTest:Landroid/webkit/WebView$HitTestResult;
    if-eqz v0, :cond_3

    #@a
    .line 1718
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@c
    invoke-virtual {v1}, Landroid/webkit/WebView;->performLongClick()Z

    #@f
    goto :goto_3

    #@10
    .line 1722
    .end local v0           #hitTest:Landroid/webkit/WebView$HitTestResult;
    :pswitch_10
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@12
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@15
    move-result v2

    #@16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@19
    move-result v3

    #@1a
    invoke-virtual {v1, v2, v3}, Landroid/webkit/ZoomManager;->handleDoubleTap(FF)V

    #@1d
    goto :goto_3

    #@1e
    .line 1725
    :pswitch_1e
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->onHandleUiTouchEvent(Landroid/view/MotionEvent;)V

    #@21
    goto :goto_3

    #@22
    .line 1729
    :pswitch_22
    and-int/lit8 v1, p3, 0x10

    #@24
    if-eqz v1, :cond_49

    #@26
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@28
    if-eqz v1, :cond_49

    #@2a
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@2c
    invoke-virtual {v1}, Landroid/webkit/WebView$HitTestResult;->getType()I

    #@2f
    move-result v1

    #@30
    const/16 v2, 0x9

    #@32
    if-eq v1, v2, :cond_49

    #@34
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@36
    invoke-virtual {v1}, Landroid/graphics/Region;->isEmpty()Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_42

    #@3c
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@3e
    iget-boolean v1, v1, Landroid/webkit/WebViewCore$WebKitHitTest;->mHasListener:Z

    #@40
    if-eqz v1, :cond_49

    #@42
    .line 1733
    :cond_42
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@44
    const/4 v2, 0x0

    #@45
    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->playSoundEffect(I)V

    #@48
    goto :goto_3

    #@49
    .line 1734
    :cond_49
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@4b
    if-eqz v1, :cond_3

    #@4d
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@4f
    iget-object v1, v1, Landroid/webkit/WebViewCore$WebKitHitTest;->mIntentUrl:Ljava/lang/String;

    #@51
    if-eqz v1, :cond_3

    #@53
    .line 1735
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@55
    iget-object v1, v1, Landroid/webkit/WebViewCore$WebKitHitTest;->mIntentUrl:Ljava/lang/String;

    #@57
    invoke-direct {p0, v1}, Landroid/webkit/WebViewClassic;->overrideLoading(Ljava/lang/String;)V

    #@5a
    goto :goto_3

    #@5b
    .line 1714
    nop

    #@5c
    :pswitch_data_5c
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_22
        :pswitch_10
    .end packed-switch
.end method

.method private onHandleUiTouchEvent(Landroid/view/MotionEvent;)V
    .registers 16
    .parameter "ev"

    #@0
    .prologue
    .line 1748
    iget-object v12, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v12}, Landroid/webkit/ZoomManager;->getScaleGestureDetector()Landroid/view/ScaleGestureDetector;

    #@5
    move-result-object v3

    #@6
    .line 1751
    .local v3, detector:Landroid/view/ScaleGestureDetector;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@9
    move-result v0

    #@a
    .line 1752
    .local v0, action:I
    const/4 v12, 0x6

    #@b
    if-ne v0, v12, :cond_29

    #@d
    const/4 v6, 0x1

    #@e
    .line 1753
    .local v6, pointerUp:Z
    :goto_e
    const/4 v12, 0x6

    #@f
    if-eq v0, v12, :cond_14

    #@11
    const/4 v12, 0x5

    #@12
    if-ne v0, v12, :cond_2b

    #@14
    :cond_14
    const/4 v1, 0x1

    #@15
    .line 1756
    .local v1, configChanged:Z
    :goto_15
    if-eqz v6, :cond_2d

    #@17
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@1a
    move-result v7

    #@1b
    .line 1759
    .local v7, skipIndex:I
    :goto_1b
    const/4 v8, 0x0

    #@1c
    .local v8, sumX:F
    const/4 v9, 0x0

    #@1d
    .line 1760
    .local v9, sumY:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@20
    move-result v2

    #@21
    .line 1761
    .local v2, count:I
    const/4 v5, 0x0

    #@22
    .local v5, i:I
    :goto_22
    if-ge v5, v2, :cond_3a

    #@24
    .line 1762
    if-ne v7, v5, :cond_2f

    #@26
    .line 1761
    :goto_26
    add-int/lit8 v5, v5, 0x1

    #@28
    goto :goto_22

    #@29
    .line 1752
    .end local v1           #configChanged:Z
    .end local v2           #count:I
    .end local v5           #i:I
    .end local v6           #pointerUp:Z
    .end local v7           #skipIndex:I
    .end local v8           #sumX:F
    .end local v9           #sumY:F
    :cond_29
    const/4 v6, 0x0

    #@2a
    goto :goto_e

    #@2b
    .line 1753
    .restart local v6       #pointerUp:Z
    :cond_2b
    const/4 v1, 0x0

    #@2c
    goto :goto_15

    #@2d
    .line 1756
    .restart local v1       #configChanged:Z
    :cond_2d
    const/4 v7, -0x1

    #@2e
    goto :goto_1b

    #@2f
    .line 1763
    .restart local v2       #count:I
    .restart local v5       #i:I
    .restart local v7       #skipIndex:I
    .restart local v8       #sumX:F
    .restart local v9       #sumY:F
    :cond_2f
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    #@32
    move-result v12

    #@33
    add-float/2addr v8, v12

    #@34
    .line 1764
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    #@37
    move-result v12

    #@38
    add-float/2addr v9, v12

    #@39
    goto :goto_26

    #@3a
    .line 1766
    :cond_3a
    if-eqz v6, :cond_7d

    #@3c
    add-int/lit8 v4, v2, -0x1

    #@3e
    .line 1767
    .local v4, div:I
    :goto_3e
    int-to-float v12, v4

    #@3f
    div-float v10, v8, v12

    #@41
    .line 1768
    .local v10, x:F
    int-to-float v12, v4

    #@42
    div-float v11, v9, v12

    #@44
    .line 1770
    .local v11, y:F
    if-eqz v1, :cond_63

    #@46
    .line 1771
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    #@49
    move-result v12

    #@4a
    iput v12, p0, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@4c
    .line 1772
    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    #@4f
    move-result v12

    #@50
    iput v12, p0, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@52
    .line 1773
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@55
    move-result-wide v12

    #@56
    iput-wide v12, p0, Landroid/webkit/WebViewClassic;->mLastTouchTime:J

    #@58
    .line 1774
    iget-object v12, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@5a
    invoke-virtual {v12}, Landroid/webkit/WebView;->cancelLongPress()V

    #@5d
    .line 1775
    iget-object v12, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@5f
    const/4 v13, 0x4

    #@60
    invoke-virtual {v12, v13}, Landroid/os/Handler;->removeMessages(I)V

    #@63
    .line 1778
    :cond_63
    if-eqz v3, :cond_8c

    #@65
    .line 1779
    invoke-virtual {v3, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@68
    .line 1780
    invoke-virtual {v3}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    #@6b
    move-result v12

    #@6c
    if-eqz v12, :cond_8c

    #@6e
    .line 1781
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@71
    move-result-wide v12

    #@72
    iput-wide v12, p0, Landroid/webkit/WebViewClassic;->mLastTouchTime:J

    #@74
    .line 1783
    iget-object v12, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@76
    invoke-virtual {v12}, Landroid/webkit/ZoomManager;->supportsPanDuringZoom()Z

    #@79
    move-result v12

    #@7a
    if-nez v12, :cond_7f

    #@7c
    .line 1804
    :cond_7c
    :goto_7c
    return-void

    #@7d
    .end local v4           #div:I
    .end local v10           #x:F
    .end local v11           #y:F
    :cond_7d
    move v4, v2

    #@7e
    .line 1766
    goto :goto_3e

    #@7f
    .line 1786
    .restart local v4       #div:I
    .restart local v10       #x:F
    .restart local v11       #y:F
    :cond_7f
    const/4 v12, 0x3

    #@80
    iput v12, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@82
    .line 1787
    iget-object v12, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@84
    if-nez v12, :cond_8c

    #@86
    .line 1788
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@89
    move-result-object v12

    #@8a
    iput-object v12, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@8c
    .line 1793
    :cond_8c
    const/4 v12, 0x5

    #@8d
    if-ne v0, v12, :cond_9f

    #@8f
    .line 1794
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->cancelTouch()V

    #@92
    .line 1795
    const/4 v0, 0x0

    #@93
    .line 1803
    :cond_93
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    #@96
    move-result v12

    #@97
    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    #@9a
    move-result v13

    #@9b
    invoke-direct {p0, p1, v0, v12, v13}, Landroid/webkit/WebViewClassic;->handleTouchEventCommon(Landroid/view/MotionEvent;III)V

    #@9e
    goto :goto_7c

    #@9f
    .line 1796
    :cond_9f
    const/4 v12, 0x2

    #@a0
    if-ne v0, v12, :cond_93

    #@a2
    .line 1798
    const/4 v12, 0x0

    #@a3
    cmpg-float v12, v10, v12

    #@a5
    if-ltz v12, :cond_7c

    #@a7
    const/4 v12, 0x0

    #@a8
    cmpg-float v12, v11, v12

    #@aa
    if-gez v12, :cond_93

    #@ac
    goto :goto_7c
.end method

.method private onZoomAnimationEnd()V
    .registers 3

    #@0
    .prologue
    .line 5218
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@2
    const/16 v1, 0x92

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@7
    .line 5219
    return-void
.end method

.method private onZoomAnimationStart()V
    .registers 1

    #@0
    .prologue
    .line 5215
    return-void
.end method

.method private overrideLoading(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 8580
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->uiOverrideUrlLoading(Ljava/lang/String;)Z

    #@5
    .line 8581
    return-void
.end method

.method static pinLoc(III)I
    .registers 4
    .parameter "x"
    .parameter "viewMax"
    .parameter "docMax"

    #@0
    .prologue
    .line 3443
    if-ge p2, p1, :cond_4

    #@2
    .line 3445
    const/4 p0, 0x0

    #@3
    .line 3454
    :cond_3
    :goto_3
    return p0

    #@4
    .line 3447
    :cond_4
    if-gez p0, :cond_8

    #@6
    .line 3448
    const/4 p0, 0x0

    #@7
    goto :goto_3

    #@8
    .line 3450
    :cond_8
    add-int v0, p0, p1

    #@a
    if-le v0, p2, :cond_3

    #@c
    .line 3451
    sub-int p0, p2, p1

    #@e
    goto :goto_3
.end method

.method private pinScrollBy(IIZI)Z
    .registers 7
    .parameter "dx"
    .parameter "dy"
    .parameter "animate"
    .parameter "animationDuration"

    #@0
    .prologue
    .line 4494
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@3
    move-result v0

    #@4
    add-int/2addr v0, p1

    #@5
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@8
    move-result v1

    #@9
    add-int/2addr v1, p2

    #@a
    invoke-direct {p0, v0, v1, p3, p4}, Landroid/webkit/WebViewClassic;->pinScrollTo(IIZI)Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method private pinScrollTo(IIZI)Z
    .registers 12
    .parameter "x"
    .parameter "y"
    .parameter "animate"
    .parameter "animationDuration"

    #@0
    .prologue
    .line 4499
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->abortAnimation()V

    #@3
    .line 4500
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic;->pinLocX(I)I

    #@6
    move-result p1

    #@7
    .line 4501
    invoke-virtual {p0, p2}, Landroid/webkit/WebViewClassic;->pinLocY(I)I

    #@a
    move-result p2

    #@b
    .line 4502
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@e
    move-result v0

    #@f
    sub-int v3, p1, v0

    #@11
    .line 4503
    .local v3, dx:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@14
    move-result v0

    #@15
    sub-int v4, p2, v0

    #@17
    .line 4505
    .local v4, dy:I
    or-int v0, v3, v4

    #@19
    if-nez v0, :cond_1d

    #@1b
    .line 4506
    const/4 v0, 0x0

    #@1c
    .line 4525
    :goto_1c
    return v0

    #@1d
    .line 4510
    :cond_1d
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getOverlappingActionModeHeight()I

    #@20
    move-result v6

    #@21
    .line 4511
    .local v6, overlappingActionModeHeight:I
    if-lez v6, :cond_25

    #@23
    .line 4512
    sub-int/2addr v4, v6

    #@24
    .line 4513
    sub-int/2addr p2, v6

    #@25
    .line 4517
    :cond_25
    if-eqz p3, :cond_41

    #@27
    .line 4519
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@29
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@2c
    move-result v1

    #@2d
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@30
    move-result v2

    #@31
    if-lez p4, :cond_3c

    #@33
    move v5, p4

    #@34
    :goto_34
    invoke-virtual/range {v0 .. v5}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    #@37
    .line 4521
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@3a
    .line 4525
    :goto_3a
    const/4 v0, 0x1

    #@3b
    goto :goto_1c

    #@3c
    .line 4519
    :cond_3c
    invoke-static {v3, v4}, Landroid/webkit/WebViewClassic;->computeDuration(II)I

    #@3f
    move-result v5

    #@40
    goto :goto_34

    #@41
    .line 4523
    :cond_41
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@43
    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebView;->scrollTo(II)V

    #@46
    goto :goto_3a
.end method

.method private postInvalidate()V
    .registers 2

    #@0
    .prologue
    .line 10453
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebView;->postInvalidate()V

    #@5
    .line 10454
    return-void
.end method

.method private recordNewContentSize(IIZ)V
    .registers 6
    .parameter "w"
    .parameter "h"
    .parameter "updateLayout"

    #@0
    .prologue
    .line 3603
    or-int v0, p1, p2

    #@2
    if-nez v0, :cond_8

    #@4
    .line 3604
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@7
    .line 3628
    :goto_7
    return-void

    #@8
    .line 3609
    :cond_8
    iget v0, p0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@a
    if-ne v0, p1, :cond_10

    #@c
    iget v0, p0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@e
    if-eq v0, p2, :cond_54

    #@10
    .line 3611
    :cond_10
    iput p1, p0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@12
    .line 3612
    iput p2, p0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@14
    .line 3615
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@16
    if-nez v0, :cond_51

    #@18
    .line 3617
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@1b
    move-result v0

    #@1c
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->pinLocX(I)I

    #@1f
    move-result v0

    #@20
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@23
    move-result v1

    #@24
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->pinLocY(I)I

    #@27
    move-result v1

    #@28
    invoke-virtual {p0, v0, v1}, Landroid/webkit/WebViewClassic;->updateScrollCoordinates(II)Z

    #@2b
    .line 3618
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@2d
    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    #@30
    move-result v0

    #@31
    if-nez v0, :cond_51

    #@33
    .line 3621
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@35
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@37
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getFinalX()I

    #@3a
    move-result v1

    #@3b
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->pinLocX(I)I

    #@3e
    move-result v1

    #@3f
    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->setFinalX(I)V

    #@42
    .line 3622
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@44
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@46
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getFinalY()I

    #@49
    move-result v1

    #@4a
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->pinLocY(I)I

    #@4d
    move-result v1

    #@4e
    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->setFinalY(I)V

    #@51
    .line 3625
    :cond_51
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@54
    .line 3627
    :cond_54
    invoke-direct {p0, p3}, Landroid/webkit/WebViewClassic;->contentSizeChanged(Z)V

    #@57
    goto :goto_7
.end method

.method private relocateAutoCompletePopup()V
    .registers 3

    #@0
    .prologue
    .line 5545
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@2
    if-eqz v0, :cond_14

    #@4
    .line 5546
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@6
    invoke-virtual {v0}, Landroid/webkit/AutoCompletePopup;->resetRect()V

    #@9
    .line 5547
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@b
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@d
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Landroid/webkit/AutoCompletePopup;->setText(Ljava/lang/CharSequence;)V

    #@14
    .line 5549
    :cond_14
    return-void
.end method

.method private removeTouchHighlight()V
    .registers 2

    #@0
    .prologue
    .line 4989
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->setTouchHighlightRects(Landroid/webkit/WebViewCore$WebKitHitTest;)V

    #@4
    .line 4990
    return-void
.end method

.method private resetCaretTimer()V
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x90

    #@2
    .line 6111
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@4
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@7
    .line 6112
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@9
    if-nez v0, :cond_12

    #@b
    .line 6113
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@d
    const-wide/16 v1, 0xbb8

    #@f
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@12
    .line 6116
    :cond_12
    return-void
.end method

.method private restoreHistoryPictureFields(Landroid/graphics/Picture;Landroid/os/Bundle;)V
    .registers 8
    .parameter "p"
    .parameter "b"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2806
    const-string/jumbo v3, "scrollX"

    #@4
    invoke-virtual {p2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v1

    #@8
    .line 2807
    .local v1, sx:I
    const-string/jumbo v3, "scrollY"

    #@b
    invoke-virtual {p2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@e
    move-result v2

    #@f
    .line 2809
    .local v2, sy:I
    const/4 v3, 0x1

    #@10
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@12
    .line 2810
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mHistoryPicture:Landroid/graphics/Picture;

    #@14
    .line 2812
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->setScrollXRaw(I)V

    #@17
    .line 2813
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->setScrollYRaw(I)V

    #@1a
    .line 2814
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@1c
    invoke-virtual {v3, p2}, Landroid/webkit/ZoomManager;->restoreZoomState(Landroid/os/Bundle;)V

    #@1f
    .line 2815
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@21
    invoke-virtual {v3}, Landroid/webkit/ZoomManager;->getScale()F

    #@24
    move-result v0

    #@25
    .line 2816
    .local v0, scale:F
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    #@28
    move-result v3

    #@29
    int-to-float v3, v3

    #@2a
    mul-float/2addr v3, v0

    #@2b
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@2e
    move-result v3

    #@2f
    iput v3, p0, Landroid/webkit/WebViewClassic;->mHistoryWidth:I

    #@31
    .line 2817
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    #@34
    move-result v3

    #@35
    int-to-float v3, v3

    #@36
    mul-float/2addr v3, v0

    #@37
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@3a
    move-result v3

    #@3b
    iput v3, p0, Landroid/webkit/WebViewClassic;->mHistoryHeight:I

    #@3d
    .line 2819
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@40
    .line 2820
    return-void
.end method

.method private saveWebArchiveImpl(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V
    .registers 7
    .parameter "basename"
    .parameter "autoname"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3124
    .local p3, callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/lang/String;>;"
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0x93

    #@4
    new-instance v2, Landroid/webkit/WebViewClassic$SaveWebArchiveMessage;

    #@6
    invoke-direct {v2, p1, p2, p3}, Landroid/webkit/WebViewClassic$SaveWebArchiveMessage;-><init>(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@c
    .line 3126
    return-void
.end method

.method private static scaleAlongSegment(IILandroid/graphics/PointF;Landroid/graphics/PointF;)F
    .registers 13
    .parameter "x"
    .parameter "y"
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 6030
    iget v7, p3, Landroid/graphics/PointF;->x:F

    #@2
    iget v8, p2, Landroid/graphics/PointF;->x:F

    #@4
    sub-float v2, v7, v8

    #@6
    .line 6031
    .local v2, abX:F
    iget v7, p3, Landroid/graphics/PointF;->y:F

    #@8
    iget v8, p2, Landroid/graphics/PointF;->y:F

    #@a
    sub-float v3, v7, v8

    #@c
    .line 6032
    .local v3, abY:F
    mul-float v7, v2, v2

    #@e
    mul-float v8, v3, v3

    #@10
    add-float v0, v7, v8

    #@12
    .line 6035
    .local v0, ab2:F
    int-to-float v7, p0

    #@13
    iget v8, p2, Landroid/graphics/PointF;->x:F

    #@15
    sub-float v4, v7, v8

    #@17
    .line 6036
    .local v4, apX:F
    int-to-float v7, p1

    #@18
    iget v8, p2, Landroid/graphics/PointF;->y:F

    #@1a
    sub-float v5, v7, v8

    #@1c
    .line 6037
    .local v5, apY:F
    mul-float v7, v4, v2

    #@1e
    mul-float v8, v5, v3

    #@20
    add-float v1, v7, v8

    #@22
    .line 6038
    .local v1, abDotAP:F
    div-float v6, v1, v0

    #@24
    .line 6039
    .local v6, scale:F
    return v6
.end method

.method private static scaleCoordinate(FFF)F
    .registers 5
    .parameter "scale"
    .parameter "coord1"
    .parameter "coord2"

    #@0
    .prologue
    .line 7850
    sub-float v0, p2, p1

    #@2
    .line 7851
    .local v0, diff:F
    mul-float v1, p0, v0

    #@4
    add-float/2addr v1, p1

    #@5
    return v1
.end method

.method private scaleTrackballX(FI)I
    .registers 7
    .parameter "xRate"
    .parameter "width"

    #@0
    .prologue
    .line 8010
    const/high16 v2, 0x43c8

    #@2
    div-float v2, p1, v2

    #@4
    int-to-float v3, p2

    #@5
    mul-float/2addr v2, v3

    #@6
    float-to-int v1, v2

    #@7
    .line 8011
    .local v1, xMove:I
    move v0, v1

    #@8
    .line 8012
    .local v0, nextXMove:I
    if-lez v1, :cond_14

    #@a
    .line 8013
    iget v2, p0, Landroid/webkit/WebViewClassic;->mTrackballXMove:I

    #@c
    if-le v1, v2, :cond_11

    #@e
    .line 8014
    iget v2, p0, Landroid/webkit/WebViewClassic;->mTrackballXMove:I

    #@10
    sub-int/2addr v1, v2

    #@11
    .line 8019
    :cond_11
    :goto_11
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTrackballXMove:I

    #@13
    .line 8020
    return v1

    #@14
    .line 8016
    :cond_14
    iget v2, p0, Landroid/webkit/WebViewClassic;->mTrackballXMove:I

    #@16
    if-ge v1, v2, :cond_11

    #@18
    .line 8017
    iget v2, p0, Landroid/webkit/WebViewClassic;->mTrackballXMove:I

    #@1a
    sub-int/2addr v1, v2

    #@1b
    goto :goto_11
.end method

.method private scaleTrackballY(FI)I
    .registers 7
    .parameter "yRate"
    .parameter "height"

    #@0
    .prologue
    .line 8024
    const/high16 v2, 0x43c8

    #@2
    div-float v2, p1, v2

    #@4
    int-to-float v3, p2

    #@5
    mul-float/2addr v2, v3

    #@6
    float-to-int v1, v2

    #@7
    .line 8025
    .local v1, yMove:I
    move v0, v1

    #@8
    .line 8026
    .local v0, nextYMove:I
    if-lez v1, :cond_14

    #@a
    .line 8027
    iget v2, p0, Landroid/webkit/WebViewClassic;->mTrackballYMove:I

    #@c
    if-le v1, v2, :cond_11

    #@e
    .line 8028
    iget v2, p0, Landroid/webkit/WebViewClassic;->mTrackballYMove:I

    #@10
    sub-int/2addr v1, v2

    #@11
    .line 8033
    :cond_11
    :goto_11
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTrackballYMove:I

    #@13
    .line 8034
    return v1

    #@14
    .line 8030
    :cond_14
    iget v2, p0, Landroid/webkit/WebViewClassic;->mTrackballYMove:I

    #@16
    if-ge v1, v2, :cond_11

    #@18
    .line 8031
    iget v2, p0, Landroid/webkit/WebViewClassic;->mTrackballYMove:I

    #@1a
    sub-int/2addr v1, v2

    #@1b
    goto :goto_11
.end method

.method private scrollDraggedSelectionHandleIntoView()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 7559
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@3
    if-nez v10, :cond_6

    #@5
    .line 7579
    :cond_5
    :goto_5
    return-void

    #@6
    .line 7562
    :cond_6
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@8
    iget v8, v10, Landroid/graphics/Point;->x:I

    #@a
    .line 7563
    .local v8, x:I
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@c
    iget v9, v10, Landroid/graphics/Point;->y:I

    #@e
    .line 7564
    .local v9, y:I
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@10
    invoke-virtual {v10, v8, v9}, Landroid/graphics/Rect;->contains(II)Z

    #@13
    move-result v10

    #@14
    if-nez v10, :cond_5

    #@16
    .line 7565
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@18
    iget v10, v10, Landroid/graphics/Rect;->left:I

    #@1a
    sub-int v10, v8, v10

    #@1c
    add-int/lit8 v10, v10, -0xa

    #@1e
    invoke-static {v11, v10}, Ljava/lang/Math;->min(II)I

    #@21
    move-result v4

    #@22
    .line 7566
    .local v4, left:I
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@24
    iget v10, v10, Landroid/graphics/Rect;->right:I

    #@26
    sub-int v10, v8, v10

    #@28
    add-int/lit8 v10, v10, 0xa

    #@2a
    invoke-static {v11, v10}, Ljava/lang/Math;->max(II)I

    #@2d
    move-result v5

    #@2e
    .line 7567
    .local v5, right:I
    add-int v2, v4, v5

    #@30
    .line 7568
    .local v2, deltaX:I
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@32
    iget v10, v10, Landroid/graphics/Rect;->top:I

    #@34
    sub-int v10, v9, v10

    #@36
    add-int/lit8 v10, v10, -0xa

    #@38
    invoke-static {v11, v10}, Ljava/lang/Math;->min(II)I

    #@3b
    move-result v0

    #@3c
    .line 7569
    .local v0, above:I
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@3e
    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    #@40
    sub-int v10, v9, v10

    #@42
    add-int/lit8 v10, v10, 0xa

    #@44
    invoke-static {v11, v10}, Ljava/lang/Math;->max(II)I

    #@47
    move-result v1

    #@48
    .line 7570
    .local v1, below:I
    add-int v3, v0, v1

    #@4a
    .line 7571
    .local v3, deltaY:I
    if-nez v2, :cond_4e

    #@4c
    if-eqz v3, :cond_5

    #@4e
    .line 7572
    :cond_4e
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getTextScrollX()I

    #@51
    move-result v10

    #@52
    add-int v6, v10, v2

    #@54
    .line 7573
    .local v6, scrollX:I
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getTextScrollY()I

    #@57
    move-result v10

    #@58
    add-int v7, v10, v3

    #@5a
    .line 7574
    .local v7, scrollY:I
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollX()I

    #@5d
    move-result v10

    #@5e
    invoke-static {v6, v11, v10}, Landroid/webkit/WebViewClassic;->clampBetween(III)I

    #@61
    move-result v6

    #@62
    .line 7575
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollY()I

    #@65
    move-result v10

    #@66
    invoke-static {v7, v11, v10}, Landroid/webkit/WebViewClassic;->clampBetween(III)I

    #@69
    move-result v7

    #@6a
    .line 7576
    invoke-direct {p0, v6, v7}, Landroid/webkit/WebViewClassic;->scrollEditText(II)V

    #@6d
    goto :goto_5
.end method

.method private scrollEditIntoView()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 6660
    new-instance v5, Landroid/graphics/Rect;

    #@3
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@6
    move-result v6

    #@7
    invoke-virtual {p0, v6}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@a
    move-result v6

    #@b
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@e
    move-result v7

    #@f
    invoke-virtual {p0, v7}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@12
    move-result v7

    #@13
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@16
    move-result v8

    #@17
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWidth()I

    #@1a
    move-result v9

    #@1b
    add-int/2addr v8, v9

    #@1c
    invoke-virtual {p0, v8}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@1f
    move-result v8

    #@20
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@23
    move-result v9

    #@24
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@27
    move-result v10

    #@28
    add-int/2addr v9, v10

    #@29
    invoke-virtual {p0, v9}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@2c
    move-result v9

    #@2d
    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    #@30
    .line 6664
    .local v5, visibleRect:Landroid/graphics/Rect;
    iget-boolean v6, p0, Landroid/webkit/WebViewClassic;->mForceScrollEditIntoView:Z

    #@32
    if-nez v6, :cond_35

    #@34
    .line 6718
    :cond_34
    :goto_34
    return-void

    #@35
    .line 6665
    :cond_35
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@37
    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    #@3a
    move-result v6

    #@3b
    if-nez v6, :cond_34

    #@3d
    .line 6668
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->syncSelectionCursors()V

    #@40
    .line 6669
    iget v6, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@42
    iget v7, p0, Landroid/webkit/WebViewClassic;->mEditTextLayerId:I

    #@44
    invoke-static {v6, v7, v5}, Landroid/webkit/WebViewClassic;->nativeFindMaxVisibleRect(IILandroid/graphics/Rect;)V

    #@47
    .line 6670
    const/4 v6, 0x1

    #@48
    const/16 v7, 0xa

    #@4a
    invoke-direct {p0, v7}, Landroid/webkit/WebViewClassic;->viewToContentDimension(I)I

    #@4d
    move-result v7

    #@4e
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@51
    move-result v0

    #@52
    .line 6671
    .local v0, buffer:I
    new-instance v4, Landroid/graphics/Rect;

    #@54
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@56
    iget v6, v6, Landroid/graphics/Rect;->left:I

    #@58
    sub-int/2addr v6, v0

    #@59
    invoke-static {v11, v6}, Ljava/lang/Math;->max(II)I

    #@5c
    move-result v6

    #@5d
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@5f
    iget v7, v7, Landroid/graphics/Rect;->top:I

    #@61
    sub-int/2addr v7, v0

    #@62
    invoke-static {v11, v7}, Ljava/lang/Math;->max(II)I

    #@65
    move-result v7

    #@66
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@68
    iget v8, v8, Landroid/graphics/Rect;->right:I

    #@6a
    add-int/2addr v8, v0

    #@6b
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@6d
    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    #@6f
    add-int/2addr v9, v0

    #@70
    invoke-direct {v4, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    #@73
    .line 6676
    .local v4, showRect:Landroid/graphics/Rect;
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->calculateBaseCaretTop()Landroid/graphics/Point;

    #@76
    move-result-object v1

    #@77
    .line 6677
    .local v1, caretTop:Landroid/graphics/Point;
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    #@7a
    move-result v6

    #@7b
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@7d
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    #@80
    move-result v7

    #@81
    if-ge v6, v7, :cond_9b

    #@83
    .line 6679
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@85
    iget v6, v6, Landroid/graphics/Point;->x:I

    #@87
    iget v7, v1, Landroid/graphics/Point;->x:I

    #@89
    if-ge v6, v7, :cond_f2

    #@8b
    .line 6680
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@8d
    iget v6, v6, Landroid/graphics/Point;->x:I

    #@8f
    sub-int/2addr v6, v0

    #@90
    invoke-static {v11, v6}, Ljava/lang/Math;->max(II)I

    #@93
    move-result v6

    #@94
    iput v6, v4, Landroid/graphics/Rect;->left:I

    #@96
    .line 6681
    iget v6, v1, Landroid/graphics/Point;->x:I

    #@98
    add-int/2addr v6, v0

    #@99
    iput v6, v4, Landroid/graphics/Rect;->right:I

    #@9b
    .line 6687
    :cond_9b
    :goto_9b
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    #@9e
    move-result v6

    #@9f
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@a1
    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    #@a4
    move-result v7

    #@a5
    if-ge v6, v7, :cond_bf

    #@a7
    .line 6689
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@a9
    iget v6, v6, Landroid/graphics/Point;->y:I

    #@ab
    iget v7, v1, Landroid/graphics/Point;->y:I

    #@ad
    if-le v6, v7, :cond_103

    #@af
    .line 6690
    iget v6, v1, Landroid/graphics/Point;->y:I

    #@b1
    sub-int/2addr v6, v0

    #@b2
    invoke-static {v11, v6}, Ljava/lang/Math;->max(II)I

    #@b5
    move-result v6

    #@b6
    iput v6, v4, Landroid/graphics/Rect;->top:I

    #@b8
    .line 6691
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@ba
    iget v6, v6, Landroid/graphics/Point;->y:I

    #@bc
    add-int/2addr v6, v0

    #@bd
    iput v6, v4, Landroid/graphics/Rect;->bottom:I

    #@bf
    .line 6698
    :cond_bf
    :goto_bf
    invoke-virtual {v5, v4}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    #@c2
    move-result v6

    #@c3
    if-nez v6, :cond_34

    #@c5
    .line 6702
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@c8
    move-result v6

    #@c9
    invoke-virtual {p0, v6}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@cc
    move-result v2

    #@cd
    .line 6703
    .local v2, scrollX:I
    iget v6, v5, Landroid/graphics/Rect;->left:I

    #@cf
    iget v7, v4, Landroid/graphics/Rect;->left:I

    #@d1
    if-le v6, v7, :cond_114

    #@d3
    .line 6705
    iget v6, v4, Landroid/graphics/Rect;->left:I

    #@d5
    iget v7, v5, Landroid/graphics/Rect;->left:I

    #@d7
    sub-int/2addr v6, v7

    #@d8
    add-int/2addr v2, v6

    #@d9
    .line 6710
    :cond_d9
    :goto_d9
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@dc
    move-result v6

    #@dd
    invoke-virtual {p0, v6}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@e0
    move-result v3

    #@e1
    .line 6711
    .local v3, scrollY:I
    iget v6, v5, Landroid/graphics/Rect;->top:I

    #@e3
    iget v7, v4, Landroid/graphics/Rect;->top:I

    #@e5
    if-le v6, v7, :cond_121

    #@e7
    .line 6712
    iget v6, v4, Landroid/graphics/Rect;->top:I

    #@e9
    iget v7, v5, Landroid/graphics/Rect;->top:I

    #@eb
    sub-int/2addr v6, v7

    #@ec
    add-int/2addr v3, v6

    #@ed
    .line 6717
    :cond_ed
    :goto_ed
    invoke-direct {p0, v2, v3, v11}, Landroid/webkit/WebViewClassic;->contentScrollTo(IIZ)V

    #@f0
    goto/16 :goto_34

    #@f2
    .line 6683
    .end local v2           #scrollX:I
    .end local v3           #scrollY:I
    :cond_f2
    iget v6, v1, Landroid/graphics/Point;->x:I

    #@f4
    sub-int/2addr v6, v0

    #@f5
    invoke-static {v11, v6}, Ljava/lang/Math;->max(II)I

    #@f8
    move-result v6

    #@f9
    iput v6, v4, Landroid/graphics/Rect;->left:I

    #@fb
    .line 6684
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@fd
    iget v6, v6, Landroid/graphics/Point;->x:I

    #@ff
    add-int/2addr v6, v0

    #@100
    iput v6, v4, Landroid/graphics/Rect;->right:I

    #@102
    goto :goto_9b

    #@103
    .line 6693
    :cond_103
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@105
    iget v6, v6, Landroid/graphics/Point;->y:I

    #@107
    sub-int/2addr v6, v0

    #@108
    invoke-static {v11, v6}, Ljava/lang/Math;->max(II)I

    #@10b
    move-result v6

    #@10c
    iput v6, v4, Landroid/graphics/Rect;->top:I

    #@10e
    .line 6694
    iget v6, v1, Landroid/graphics/Point;->y:I

    #@110
    add-int/2addr v6, v0

    #@111
    iput v6, v4, Landroid/graphics/Rect;->bottom:I

    #@113
    goto :goto_bf

    #@114
    .line 6706
    .restart local v2       #scrollX:I
    :cond_114
    iget v6, v5, Landroid/graphics/Rect;->right:I

    #@116
    iget v7, v4, Landroid/graphics/Rect;->right:I

    #@118
    if-ge v6, v7, :cond_d9

    #@11a
    .line 6708
    iget v6, v4, Landroid/graphics/Rect;->right:I

    #@11c
    iget v7, v5, Landroid/graphics/Rect;->right:I

    #@11e
    sub-int/2addr v6, v7

    #@11f
    add-int/2addr v2, v6

    #@120
    goto :goto_d9

    #@121
    .line 6713
    .restart local v3       #scrollY:I
    :cond_121
    iget v6, v5, Landroid/graphics/Rect;->bottom:I

    #@123
    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    #@125
    if-ge v6, v7, :cond_ed

    #@127
    .line 6714
    iget v6, v4, Landroid/graphics/Rect;->bottom:I

    #@129
    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    #@12b
    sub-int/2addr v6, v7

    #@12c
    add-int/2addr v3, v6

    #@12d
    goto :goto_ed
.end method

.method private scrollEditText(II)V
    .registers 9
    .parameter "scrollX"
    .parameter "scrollY"

    #@0
    .prologue
    const/16 v5, 0x63

    #@2
    .line 9795
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollX()I

    #@5
    move-result v2

    #@6
    int-to-float v0, v2

    #@7
    .line 9796
    .local v0, maxScrollX:F
    int-to-float v2, p1

    #@8
    div-float v1, v2, v0

    #@a
    .line 9797
    .local v1, scrollPercentX:F
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mEditTextContent:Landroid/graphics/Rect;

    #@c
    neg-int v3, p1

    #@d
    neg-int v4, p2

    #@e
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->offsetTo(II)V

    #@11
    .line 9798
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@13
    invoke-virtual {v2, v5}, Landroid/webkit/WebViewCore;->removeMessages(I)V

    #@16
    .line 9799
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@18
    const/4 v3, 0x0

    #@19
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v2, v5, v3, p2, v4}, Landroid/webkit/WebViewCore;->sendMessage(IIILjava/lang/Object;)V

    #@20
    .line 9801
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->animateHandles()V

    #@23
    .line 9802
    return-void
.end method

.method private scrollEditWithCursor()V
    .registers 24

    #@0
    .prologue
    .line 7603
    move-object/from16 v0, p0

    #@2
    iget-wide v0, v0, Landroid/webkit/WebViewClassic;->mLastEditScroll:J

    #@4
    move-wide/from16 v19, v0

    #@6
    const-wide/16 v21, 0x0

    #@8
    cmp-long v19, v19, v21

    #@a
    if-eqz v19, :cond_9d

    #@c
    .line 7604
    move-object/from16 v0, p0

    #@e
    iget v0, v0, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@10
    move/from16 v19, v0

    #@12
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@15
    move-result v20

    #@16
    add-int v19, v19, v20

    #@18
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mSelectOffset:Landroid/graphics/Point;

    #@1c
    move-object/from16 v20, v0

    #@1e
    move-object/from16 v0, v20

    #@20
    iget v0, v0, Landroid/graphics/Point;->x:I

    #@22
    move/from16 v20, v0

    #@24
    add-int v19, v19, v20

    #@26
    move-object/from16 v0, p0

    #@28
    move/from16 v1, v19

    #@2a
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@2d
    move-result v17

    #@2e
    .line 7605
    .local v17, x:I
    move-object/from16 v0, p0

    #@30
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@32
    move-object/from16 v19, v0

    #@34
    move-object/from16 v0, v19

    #@36
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@38
    move/from16 v19, v0

    #@3a
    move-object/from16 v0, p0

    #@3c
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@3e
    move-object/from16 v20, v0

    #@40
    move-object/from16 v0, v20

    #@42
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@44
    move/from16 v20, v0

    #@46
    move/from16 v0, v17

    #@48
    move/from16 v1, v19

    #@4a
    move/from16 v2, v20

    #@4c
    invoke-static {v0, v1, v2}, Landroid/webkit/WebViewClassic;->getTextScrollSpeed(III)F

    #@4f
    move-result v9

    #@50
    .line 7607
    .local v9, scrollSpeedX:F
    move-object/from16 v0, p0

    #@52
    iget v0, v0, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@54
    move/from16 v19, v0

    #@56
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@59
    move-result v20

    #@5a
    add-int v19, v19, v20

    #@5c
    move-object/from16 v0, p0

    #@5e
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mSelectOffset:Landroid/graphics/Point;

    #@60
    move-object/from16 v20, v0

    #@62
    move-object/from16 v0, v20

    #@64
    iget v0, v0, Landroid/graphics/Point;->y:I

    #@66
    move/from16 v20, v0

    #@68
    add-int v19, v19, v20

    #@6a
    move-object/from16 v0, p0

    #@6c
    move/from16 v1, v19

    #@6e
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@71
    move-result v18

    #@72
    .line 7608
    .local v18, y:I
    move-object/from16 v0, p0

    #@74
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@76
    move-object/from16 v19, v0

    #@78
    move-object/from16 v0, v19

    #@7a
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@7c
    move/from16 v19, v0

    #@7e
    move-object/from16 v0, p0

    #@80
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@82
    move-object/from16 v20, v0

    #@84
    move-object/from16 v0, v20

    #@86
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@88
    move/from16 v20, v0

    #@8a
    invoke-static/range {v18 .. v20}, Landroid/webkit/WebViewClassic;->getTextScrollSpeed(III)F

    #@8d
    move-result v10

    #@8e
    .line 7610
    .local v10, scrollSpeedY:F
    const/16 v19, 0x0

    #@90
    cmpl-float v19, v9, v19

    #@92
    if-nez v19, :cond_9e

    #@94
    const/16 v19, 0x0

    #@96
    cmpl-float v19, v10, v19

    #@98
    if-nez v19, :cond_9e

    #@9a
    .line 7611
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->endScrollEdit()V

    #@9d
    .line 7641
    .end local v9           #scrollSpeedX:F
    .end local v10           #scrollSpeedY:F
    .end local v17           #x:I
    .end local v18           #y:I
    :cond_9d
    :goto_9d
    return-void

    #@9e
    .line 7613
    .restart local v9       #scrollSpeedX:F
    .restart local v10       #scrollSpeedY:F
    .restart local v17       #x:I
    .restart local v18       #y:I
    :cond_9e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@a1
    move-result-wide v3

    #@a2
    .line 7614
    .local v3, currentTime:J
    move-object/from16 v0, p0

    #@a4
    iget-wide v0, v0, Landroid/webkit/WebViewClassic;->mLastEditScroll:J

    #@a6
    move-wide/from16 v19, v0

    #@a8
    sub-long v15, v3, v19

    #@aa
    .line 7615
    .local v15, timeSinceLastUpdate:J
    move-wide v0, v15

    #@ab
    invoke-static {v9, v0, v1}, Landroid/webkit/WebViewClassic;->getTextScrollDelta(FJ)I

    #@ae
    move-result v5

    #@af
    .line 7616
    .local v5, deltaX:I
    move-wide v0, v15

    #@b0
    invoke-static {v10, v0, v1}, Landroid/webkit/WebViewClassic;->getTextScrollDelta(FJ)I

    #@b3
    move-result v6

    #@b4
    .line 7617
    .local v6, deltaY:I
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTextScrollX()I

    #@b7
    move-result v19

    #@b8
    add-int v11, v19, v5

    #@ba
    .line 7618
    .local v11, scrollX:I
    const/16 v19, 0x0

    #@bc
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollX()I

    #@bf
    move-result v20

    #@c0
    move/from16 v0, v19

    #@c2
    move/from16 v1, v20

    #@c4
    invoke-static {v11, v0, v1}, Landroid/webkit/WebViewClassic;->clampBetween(III)I

    #@c7
    move-result v11

    #@c8
    .line 7619
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTextScrollY()I

    #@cb
    move-result v19

    #@cc
    add-int v12, v19, v6

    #@ce
    .line 7620
    .local v12, scrollY:I
    const/16 v19, 0x0

    #@d0
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollY()I

    #@d3
    move-result v20

    #@d4
    move/from16 v0, v19

    #@d6
    move/from16 v1, v20

    #@d8
    invoke-static {v12, v0, v1}, Landroid/webkit/WebViewClassic;->clampBetween(III)I

    #@db
    move-result v12

    #@dc
    .line 7622
    move-object/from16 v0, p0

    #@de
    iput-wide v3, v0, Landroid/webkit/WebViewClassic;->mLastEditScroll:J

    #@e0
    .line 7623
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTextScrollX()I

    #@e3
    move-result v19

    #@e4
    move/from16 v0, v19

    #@e6
    if-ne v11, v0, :cond_fe

    #@e8
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTextScrollY()I

    #@eb
    move-result v19

    #@ec
    move/from16 v0, v19

    #@ee
    if-ne v12, v0, :cond_fe

    #@f0
    .line 7625
    move-object/from16 v0, p0

    #@f2
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@f4
    move-object/from16 v19, v0

    #@f6
    const/16 v20, 0x95

    #@f8
    const-wide/16 v21, 0x10

    #@fa
    invoke-virtual/range {v19 .. v22}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@fd
    goto :goto_9d

    #@fe
    .line 7628
    :cond_fe
    move-object/from16 v0, p0

    #@100
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@102
    move-object/from16 v19, v0

    #@104
    move-object/from16 v0, v19

    #@106
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@108
    move/from16 v19, v0

    #@10a
    move-object/from16 v0, p0

    #@10c
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@10e
    move-object/from16 v20, v0

    #@110
    move-object/from16 v0, v20

    #@112
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@114
    move/from16 v20, v0

    #@116
    move/from16 v0, v17

    #@118
    move/from16 v1, v19

    #@11a
    move/from16 v2, v20

    #@11c
    invoke-static {v0, v1, v2}, Landroid/webkit/WebViewClassic;->getSelectionCoordinate(III)I

    #@11f
    move-result v13

    #@120
    .line 7630
    .local v13, selectionX:I
    move-object/from16 v0, p0

    #@122
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@124
    move-object/from16 v19, v0

    #@126
    move-object/from16 v0, v19

    #@128
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@12a
    move/from16 v19, v0

    #@12c
    move-object/from16 v0, p0

    #@12e
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@130
    move-object/from16 v20, v0

    #@132
    move-object/from16 v0, v20

    #@134
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@136
    move/from16 v20, v0

    #@138
    invoke-static/range {v18 .. v20}, Landroid/webkit/WebViewClassic;->getSelectionCoordinate(III)I

    #@13b
    move-result v14

    #@13c
    .line 7632
    .local v14, selectionY:I
    move-object/from16 v0, p0

    #@13e
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@140
    move-object/from16 v19, v0

    #@142
    move-object/from16 v0, v19

    #@144
    iget v7, v0, Landroid/graphics/Point;->x:I

    #@146
    .line 7633
    .local v7, oldX:I
    move-object/from16 v0, p0

    #@148
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@14a
    move-object/from16 v19, v0

    #@14c
    move-object/from16 v0, v19

    #@14e
    iget v8, v0, Landroid/graphics/Point;->y:I

    #@150
    .line 7634
    .local v8, oldY:I
    move-object/from16 v0, p0

    #@152
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@154
    move-object/from16 v19, v0

    #@156
    move-object/from16 v0, v19

    #@158
    invoke-virtual {v0, v13, v14}, Landroid/graphics/Point;->set(II)V

    #@15b
    .line 7635
    const/16 v19, 0x0

    #@15d
    move-object/from16 v0, p0

    #@15f
    move/from16 v1, v19

    #@161
    invoke-direct {v0, v1}, Landroid/webkit/WebViewClassic;->updateWebkitSelection(Z)V

    #@164
    .line 7636
    move-object/from16 v0, p0

    #@166
    invoke-direct {v0, v11, v12}, Landroid/webkit/WebViewClassic;->scrollEditText(II)V

    #@169
    .line 7637
    move-object/from16 v0, p0

    #@16b
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@16d
    move-object/from16 v19, v0

    #@16f
    move-object/from16 v0, v19

    #@171
    invoke-virtual {v0, v7, v8}, Landroid/graphics/Point;->set(II)V

    #@174
    goto/16 :goto_9d
.end method

.method private scrollLayerTo(II)V
    .registers 10
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 4456
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@2
    iget v2, v2, Landroid/graphics/Rect;->left:I

    #@4
    sub-int v0, v2, p1

    #@6
    .line 4457
    .local v0, dx:I
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@8
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@a
    sub-int v1, v2, p2

    #@c
    .line 4458
    .local v1, dy:I
    if-nez v0, :cond_10

    #@e
    if-eqz v1, :cond_14

    #@10
    :cond_10
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@12
    if-nez v2, :cond_15

    #@14
    .line 4483
    :cond_14
    :goto_14
    return-void

    #@15
    .line 4461
    :cond_15
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@17
    if-eqz v2, :cond_3d

    #@19
    .line 4462
    iget v2, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBaseLayerId:I

    #@1b
    iget v3, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@1d
    if-ne v2, v3, :cond_2b

    #@1f
    .line 4463
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@21
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->offset(II)V

    #@24
    .line 4464
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBaseTextQuad:Landroid/webkit/QuadF;

    #@26
    int-to-float v3, v0

    #@27
    int-to-float v4, v1

    #@28
    invoke-virtual {v2, v3, v4}, Landroid/webkit/QuadF;->offset(FF)V

    #@2b
    .line 4466
    :cond_2b
    iget v2, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtentLayerId:I

    #@2d
    iget v3, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@2f
    if-ne v2, v3, :cond_3d

    #@31
    .line 4467
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@33
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->offset(II)V

    #@36
    .line 4468
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtentTextQuad:Landroid/webkit/QuadF;

    #@38
    int-to-float v3, v0

    #@39
    int-to-float v4, v1

    #@3a
    invoke-virtual {v2, v3, v4}, Landroid/webkit/QuadF;->offset(FF)V

    #@3d
    .line 4471
    :cond_3d
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@3f
    if-eqz v2, :cond_51

    #@41
    iget v2, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@43
    iget v3, p0, Landroid/webkit/WebViewClassic;->mEditTextLayerId:I

    #@45
    if-ne v2, v3, :cond_51

    #@47
    .line 4473
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@49
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    #@4c
    .line 4474
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@4e
    invoke-virtual {v2}, Landroid/webkit/AutoCompletePopup;->resetRect()V

    #@51
    .line 4476
    :cond_51
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@53
    iget v3, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@55
    invoke-direct {p0, v2, v3, p1, p2}, Landroid/webkit/WebViewClassic;->nativeScrollLayer(IIII)Z

    #@58
    .line 4477
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@5a
    iput p1, v2, Landroid/graphics/Rect;->left:I

    #@5c
    .line 4478
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@5e
    iput p2, v2, Landroid/graphics/Rect;->top:I

    #@60
    .line 4479
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@62
    const/16 v3, 0xc6

    #@64
    iget v4, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@66
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@68
    invoke-virtual {v2, v3, v4, v5}, Landroid/webkit/WebViewCore;->sendMessage(IILjava/lang/Object;)V

    #@6b
    .line 4481
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@6d
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@70
    move-result v3

    #@71
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@74
    move-result v4

    #@75
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@78
    move-result v5

    #@79
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@7c
    move-result v6

    #@7d
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/webkit/WebView$PrivateAccess;->onScrollChanged(IIII)V

    #@80
    .line 4482
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@83
    goto :goto_14
.end method

.method private sendKeyEvent(Landroid/view/KeyEvent;)V
    .registers 9
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 10251
    const/4 v0, 0x0

    #@3
    .line 10252
    .local v0, direction:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@6
    move-result v4

    #@7
    sparse-switch v4, :sswitch_data_6e

    #@a
    .line 10269
    :goto_a
    if-eqz v0, :cond_15

    #@c
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@e
    invoke-virtual {v4, v0}, Landroid/webkit/WebView;->focusSearch(I)Landroid/view/View;

    #@11
    move-result-object v4

    #@12
    if-nez v4, :cond_15

    #@14
    .line 10271
    const/4 v0, 0x0

    #@15
    .line 10273
    :cond_15
    const/16 v1, 0x68

    #@17
    .line 10274
    .local v1, eventHubAction:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@1a
    move-result v4

    #@1b
    if-nez v4, :cond_2e

    #@1d
    .line 10275
    const/16 v1, 0x67

    #@1f
    .line 10276
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@22
    move-result v4

    #@23
    invoke-direct {p0, v4}, Landroid/webkit/WebViewClassic;->keyCodeToSoundsEffect(I)I

    #@26
    move-result v2

    #@27
    .line 10277
    .local v2, sound:I
    if-eqz v2, :cond_2e

    #@29
    .line 10278
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2b
    invoke-virtual {v4, v2}, Landroid/webkit/WebView;->playSoundEffect(I)V

    #@2e
    .line 10281
    .end local v2           #sound:I
    :cond_2e
    invoke-virtual {p0, v1, v0, v6, p1}, Landroid/webkit/WebViewClassic;->sendBatchableInputMessage(IIILjava/lang/Object;)V

    #@31
    .line 10286
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDeviceId()I

    #@34
    move-result v4

    #@35
    const/4 v5, -0x1

    #@36
    if-eq v4, v5, :cond_42

    #@38
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    #@3b
    move-result v4

    #@3c
    invoke-direct {p0, v4}, Landroid/webkit/WebViewClassic;->isPrintableAsciiExceptsSpace(I)Z

    #@3f
    move-result v4

    #@40
    if-nez v4, :cond_48

    #@42
    :cond_42
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->isTextInputMoveEvent(Landroid/view/KeyEvent;)Z

    #@45
    move-result v4

    #@46
    if-eqz v4, :cond_57

    #@48
    .line 10288
    :cond_48
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@4a
    invoke-virtual {v4, v3}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setKeyBoardInput(Z)V

    #@4d
    .line 10289
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@4f
    invoke-virtual {v3, p1}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@52
    .line 10290
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@54
    invoke-virtual {v3, v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setKeyBoardInput(Z)V

    #@57
    .line 10293
    :cond_57
    return-void

    #@58
    .line 10254
    .end local v1           #eventHubAction:I
    :sswitch_58
    const/16 v0, 0x82

    #@5a
    .line 10255
    goto :goto_a

    #@5b
    .line 10257
    :sswitch_5b
    const/16 v0, 0x21

    #@5d
    .line 10258
    goto :goto_a

    #@5e
    .line 10260
    :sswitch_5e
    const/16 v0, 0x11

    #@60
    .line 10261
    goto :goto_a

    #@61
    .line 10263
    :sswitch_61
    const/16 v0, 0x42

    #@63
    .line 10264
    goto :goto_a

    #@64
    .line 10266
    :sswitch_64
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z

    #@67
    move-result v4

    #@68
    if-eqz v4, :cond_6c

    #@6a
    move v0, v3

    #@6b
    :goto_6b
    goto :goto_a

    #@6c
    :cond_6c
    const/4 v0, 0x2

    #@6d
    goto :goto_6b

    #@6e
    .line 10252
    :sswitch_data_6e
    .sparse-switch
        0x13 -> :sswitch_5b
        0x14 -> :sswitch_58
        0x15 -> :sswitch_5e
        0x16 -> :sswitch_61
        0x3d -> :sswitch_64
    .end sparse-switch
.end method

.method private setContentScrollBy(IIZ)Z
    .registers 7
    .parameter "cx"
    .parameter "cy"
    .parameter "animate"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4531
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@3
    if-eqz v2, :cond_6

    #@5
    .line 4555
    :cond_5
    :goto_5
    return v1

    #@6
    .line 4538
    :cond_6
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@9
    move-result p1

    #@a
    .line 4539
    invoke-virtual {p0, p2}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@d
    move-result p2

    #@e
    .line 4540
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@10
    if-eqz v2, :cond_30

    #@12
    .line 4542
    if-eqz p2, :cond_24

    #@14
    .line 4543
    new-instance v0, Landroid/graphics/Rect;

    #@16
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@19
    .line 4544
    .local v0, tempRect:Landroid/graphics/Rect;
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->calcOurVisibleRect(Landroid/graphics/Rect;)V

    #@1c
    .line 4545
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->offset(II)V

    #@1f
    .line 4546
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@21
    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->requestRectangleOnScreen(Landroid/graphics/Rect;)Z

    #@24
    .line 4553
    .end local v0           #tempRect:Landroid/graphics/Rect;
    :cond_24
    if-nez p2, :cond_5

    #@26
    if-eqz p1, :cond_5

    #@28
    invoke-direct {p0, p1, v1, p3, v1}, Landroid/webkit/WebViewClassic;->pinScrollBy(IIZI)Z

    #@2b
    move-result v2

    #@2c
    if-eqz v2, :cond_5

    #@2e
    const/4 v1, 0x1

    #@2f
    goto :goto_5

    #@30
    .line 4555
    :cond_30
    invoke-direct {p0, p1, p2, p3, v1}, Landroid/webkit/WebViewClassic;->pinScrollBy(IIZI)Z

    #@33
    move-result v1

    #@34
    goto :goto_5
.end method

.method private setFindIsUp(Z)V
    .registers 2
    .parameter "isUp"

    #@0
    .prologue
    .line 4257
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mFindIsUp:Z

    #@2
    .line 4258
    return-void
.end method

.method private setHitTestResult(Landroid/webkit/WebViewCore$WebKitHitTest;)V
    .registers 4
    .parameter "hit"

    #@0
    .prologue
    .line 9397
    if-nez p1, :cond_6

    #@2
    .line 9398
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@5
    .line 9421
    :cond_5
    :goto_5
    return-void

    #@6
    .line 9401
    :cond_6
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mForceScrollEditIntoView:Z

    #@9
    .line 9402
    new-instance v0, Landroid/webkit/WebView$HitTestResult;

    #@b
    invoke-direct {v0}, Landroid/webkit/WebView$HitTestResult;-><init>()V

    #@e
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@10
    .line 9403
    iget-object v0, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mLinkUrl:Ljava/lang/String;

    #@12
    if-eqz v0, :cond_4b

    #@14
    iget-object v0, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mLinkUrl:Ljava/lang/String;

    #@16
    const-string/jumbo v1, "javascript:"

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1c
    move-result v0

    #@1d
    if-nez v0, :cond_4b

    #@1f
    .line 9405
    iget-object v0, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mLinkUrl:Ljava/lang/String;

    #@21
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->setHitTestTypeFromUrl(Ljava/lang/String;)V

    #@24
    .line 9406
    iget-object v0, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mImageUrl:Ljava/lang/String;

    #@26
    if-eqz v0, :cond_5

    #@28
    iget-object v0, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mImageUrl:Ljava/lang/String;

    #@2a
    const-string/jumbo v1, "javascript:"

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@30
    move-result v0

    #@31
    if-nez v0, :cond_5

    #@33
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@35
    invoke-virtual {v0}, Landroid/webkit/WebView$HitTestResult;->getType()I

    #@38
    move-result v0

    #@39
    const/4 v1, 0x7

    #@3a
    if-ne v0, v1, :cond_5

    #@3c
    .line 9409
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@3e
    const/16 v1, 0x8

    #@40
    invoke-virtual {v0, v1}, Landroid/webkit/WebView$HitTestResult;->setType(I)V

    #@43
    .line 9410
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@45
    iget-object v1, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mImageUrl:Ljava/lang/String;

    #@47
    invoke-virtual {v0, v1}, Landroid/webkit/WebView$HitTestResult;->setExtra(Ljava/lang/String;)V

    #@4a
    goto :goto_5

    #@4b
    .line 9412
    :cond_4b
    iget-object v0, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mImageUrl:Ljava/lang/String;

    #@4d
    if-eqz v0, :cond_68

    #@4f
    iget-object v0, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mImageUrl:Ljava/lang/String;

    #@51
    const-string/jumbo v1, "javascript:"

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@57
    move-result v0

    #@58
    if-nez v0, :cond_68

    #@5a
    .line 9414
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@5c
    const/4 v1, 0x5

    #@5d
    invoke-virtual {v0, v1}, Landroid/webkit/WebView$HitTestResult;->setType(I)V

    #@60
    .line 9415
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@62
    iget-object v1, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mImageUrl:Ljava/lang/String;

    #@64
    invoke-virtual {v0, v1}, Landroid/webkit/WebView$HitTestResult;->setExtra(Ljava/lang/String;)V

    #@67
    goto :goto_5

    #@68
    .line 9416
    :cond_68
    iget-boolean v0, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mEditable:Z

    #@6a
    if-eqz v0, :cond_74

    #@6c
    .line 9417
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@6e
    const/16 v1, 0x9

    #@70
    invoke-virtual {v0, v1}, Landroid/webkit/WebView$HitTestResult;->setType(I)V

    #@73
    goto :goto_5

    #@74
    .line 9418
    :cond_74
    iget-object v0, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mIntentUrl:Ljava/lang/String;

    #@76
    if-eqz v0, :cond_5

    #@78
    .line 9419
    iget-object v0, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mIntentUrl:Ljava/lang/String;

    #@7a
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->setHitTestTypeFromUrl(Ljava/lang/String;)V

    #@7d
    goto :goto_5
.end method

.method private setHitTestTypeFromUrl(Ljava/lang/String;)V
    .registers 7
    .parameter "url"

    #@0
    .prologue
    .line 9373
    const/4 v1, 0x0

    #@1
    .line 9374
    .local v1, substr:Ljava/lang/String;
    const-string v2, "geo:0,0?q="

    #@3
    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_25

    #@9
    .line 9375
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@b
    const/4 v3, 0x3

    #@c
    invoke-virtual {v2, v3}, Landroid/webkit/WebView$HitTestResult;->setType(I)V

    #@f
    .line 9376
    const-string v2, "geo:0,0?q="

    #@11
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@14
    move-result v2

    #@15
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    .line 9389
    :goto_19
    :try_start_19
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@1b
    const-string v3, "UTF-8"

    #@1d
    invoke-static {v1, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v2, v3}, Landroid/webkit/WebView$HitTestResult;->setExtra(Ljava/lang/String;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_24} :catch_67

    #@24
    .line 9394
    :goto_24
    return-void

    #@25
    .line 9377
    :cond_25
    const-string/jumbo v2, "tel:"

    #@28
    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2b
    move-result v2

    #@2c
    if-eqz v2, :cond_40

    #@2e
    .line 9378
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@30
    const/4 v3, 0x2

    #@31
    invoke-virtual {v2, v3}, Landroid/webkit/WebView$HitTestResult;->setType(I)V

    #@34
    .line 9379
    const-string/jumbo v2, "tel:"

    #@37
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@3a
    move-result v2

    #@3b
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    goto :goto_19

    #@40
    .line 9380
    :cond_40
    const-string/jumbo v2, "mailto:"

    #@43
    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@46
    move-result v2

    #@47
    if-eqz v2, :cond_5b

    #@49
    .line 9381
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@4b
    const/4 v3, 0x4

    #@4c
    invoke-virtual {v2, v3}, Landroid/webkit/WebView$HitTestResult;->setType(I)V

    #@4f
    .line 9382
    const-string/jumbo v2, "mailto:"

    #@52
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@55
    move-result v2

    #@56
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@59
    move-result-object v1

    #@5a
    goto :goto_19

    #@5b
    .line 9384
    :cond_5b
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@5d
    const/4 v3, 0x7

    #@5e
    invoke-virtual {v2, v3}, Landroid/webkit/WebView$HitTestResult;->setType(I)V

    #@61
    .line 9385
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@63
    invoke-virtual {v2, p1}, Landroid/webkit/WebView$HitTestResult;->setExtra(Ljava/lang/String;)V

    #@66
    goto :goto_24

    #@67
    .line 9390
    :catch_67
    move-exception v0

    #@68
    .line 9391
    .local v0, e:Ljava/lang/Throwable;
    const-string/jumbo v2, "webview"

    #@6b
    new-instance v3, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v4, "Failed to decode URL! "

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v3

    #@7a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v3

    #@7e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@81
    .line 9392
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@83
    const/4 v3, 0x0

    #@84
    invoke-virtual {v2, v3}, Landroid/webkit/WebView$HitTestResult;->setType(I)V

    #@87
    goto :goto_24
.end method

.method public static setShouldMonitorWebCoreThread()V
    .registers 0

    #@0
    .prologue
    .line 10458
    invoke-static {}, Landroid/webkit/WebViewCore;->setShouldMonitorWebCoreThread()V

    #@3
    .line 10459
    return-void
.end method

.method private setTouchHighlightRects(Landroid/webkit/WebViewCore$WebKitHitTest;)V
    .registers 15
    .parameter "hit"

    #@0
    .prologue
    .line 9523
    const/4 v6, 0x0

    #@1
    .line 9524
    .local v6, transition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->shouldAnimateTo(Landroid/webkit/WebViewCore$WebKitHitTest;)Z

    #@4
    move-result v8

    #@5
    if-eqz v8, :cond_c

    #@7
    .line 9525
    new-instance v6, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

    #@9
    .end local v6           #transition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;
    invoke-direct {v6, p0}, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;-><init>(Landroid/webkit/WebViewClassic;)V

    #@c
    .line 9527
    .restart local v6       #transition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;
    :cond_c
    if-eqz p1, :cond_67

    #@e
    iget-object v5, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mTouchRects:[Landroid/graphics/Rect;

    #@10
    .line 9528
    .local v5, rects:[Landroid/graphics/Rect;
    :goto_10
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@12
    invoke-virtual {v8}, Landroid/graphics/Region;->isEmpty()Z

    #@15
    move-result v8

    #@16
    if-nez v8, :cond_33

    #@18
    .line 9529
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@1a
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@1c
    invoke-virtual {v9}, Landroid/graphics/Region;->getBounds()Landroid/graphics/Rect;

    #@1f
    move-result-object v9

    #@20
    invoke-virtual {v8, v9}, Landroid/webkit/WebView;->invalidate(Landroid/graphics/Rect;)V

    #@23
    .line 9530
    if-eqz v6, :cond_2e

    #@25
    .line 9531
    new-instance v8, Landroid/graphics/Region;

    #@27
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@29
    invoke-direct {v8, v9}, Landroid/graphics/Region;-><init>(Landroid/graphics/Region;)V

    #@2c
    iput-object v8, v6, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mPreviousRegion:Landroid/graphics/Region;

    #@2e
    .line 9533
    :cond_2e
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@30
    invoke-virtual {v8}, Landroid/graphics/Region;->setEmpty()V

    #@33
    .line 9535
    :cond_33
    if-eqz v5, :cond_99

    #@35
    .line 9536
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTouchHightlightPaint:Landroid/graphics/Paint;

    #@37
    iget v9, p1, Landroid/webkit/WebViewCore$WebKitHitTest;->mTapHighlightColor:I

    #@39
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setColor(I)V

    #@3c
    .line 9537
    move-object v1, v5

    #@3d
    .local v1, arr$:[Landroid/graphics/Rect;
    array-length v3, v1

    #@3e
    .local v3, len$:I
    const/4 v2, 0x0

    #@3f
    .local v2, i$:I
    :goto_3f
    if-ge v2, v3, :cond_69

    #@41
    aget-object v4, v1, v2

    #@43
    .line 9538
    .local v4, rect:Landroid/graphics/Rect;
    invoke-direct {p0, v4}, Landroid/webkit/WebViewClassic;->contentToViewRect(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    #@46
    move-result-object v7

    #@47
    .line 9543
    .local v7, viewRect:Landroid/graphics/Rect;
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    #@4a
    move-result v8

    #@4b
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWidth()I

    #@4e
    move-result v9

    #@4f
    shr-int/lit8 v9, v9, 0x1

    #@51
    if-lt v8, v9, :cond_5f

    #@53
    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    #@56
    move-result v8

    #@57
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@5a
    move-result v9

    #@5b
    shr-int/lit8 v9, v9, 0x1

    #@5d
    if-ge v8, v9, :cond_64

    #@5f
    .line 9545
    :cond_5f
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@61
    invoke-virtual {v8, v7}, Landroid/graphics/Region;->union(Landroid/graphics/Rect;)Z

    #@64
    .line 9537
    :cond_64
    add-int/lit8 v2, v2, 0x1

    #@66
    goto :goto_3f

    #@67
    .line 9527
    .end local v1           #arr$:[Landroid/graphics/Rect;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v4           #rect:Landroid/graphics/Rect;
    .end local v5           #rects:[Landroid/graphics/Rect;
    .end local v7           #viewRect:Landroid/graphics/Rect;
    :cond_67
    const/4 v5, 0x0

    #@68
    goto :goto_10

    #@69
    .line 9551
    .restart local v1       #arr$:[Landroid/graphics/Rect;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    .restart local v5       #rects:[Landroid/graphics/Rect;
    :cond_69
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@6b
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@6d
    invoke-virtual {v9}, Landroid/graphics/Region;->getBounds()Landroid/graphics/Rect;

    #@70
    move-result-object v9

    #@71
    invoke-virtual {v8, v9}, Landroid/webkit/WebView;->invalidate(Landroid/graphics/Rect;)V

    #@74
    .line 9552
    if-eqz v6, :cond_99

    #@76
    iget-object v8, v6, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mPreviousRegion:Landroid/graphics/Region;

    #@78
    if-eqz v8, :cond_99

    #@7a
    .line 9553
    new-instance v8, Landroid/graphics/Region;

    #@7c
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@7e
    invoke-direct {v8, v9}, Landroid/graphics/Region;-><init>(Landroid/graphics/Region;)V

    #@81
    iput-object v8, v6, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->mNewRegion:Landroid/graphics/Region;

    #@83
    .line 9554
    iput-object v6, p0, Landroid/webkit/WebViewClassic;->mFocusTransition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

    #@85
    .line 9555
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mFocusTransition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

    #@87
    const-string/jumbo v9, "progress"

    #@8a
    const/4 v10, 0x1

    #@8b
    new-array v10, v10, [F

    #@8d
    const/4 v11, 0x0

    #@8e
    const/high16 v12, 0x3f80

    #@90
    aput v12, v10, v11

    #@92
    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@95
    move-result-object v0

    #@96
    .line 9557
    .local v0, animator:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@99
    .line 9560
    .end local v0           #animator:Landroid/animation/ObjectAnimator;
    .end local v1           #arr$:[Landroid/graphics/Rect;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_99
    return-void
.end method

.method public static setWebViewProxyEnable(Z)V
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 1974
    sget-boolean v0, Landroid/webkit/WebViewClassic;->sBrowserProxyEnabled:Z

    #@2
    if-eq v0, p0, :cond_6

    #@4
    .line 1975
    sput-boolean p0, Landroid/webkit/WebViewClassic;->sBrowserProxyEnabled:Z

    #@6
    .line 1976
    :cond_6
    const-string/jumbo v0, "webview"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string/jumbo v2, "setWebViewProxyEnable():sBrowserProxyEnabled="

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    sget-boolean v2, Landroid/webkit/WebViewClassic;->sBrowserProxyEnabled:Z

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1977
    return-void
.end method

.method private setupPackageListener(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 2030
    const-class v4, Landroid/webkit/WebViewClassic;

    #@2
    monitor-enter v4

    #@3
    .line 2034
    :try_start_3
    sget-boolean v3, Landroid/webkit/WebViewClassic;->sPackageInstallationReceiverAdded:Z

    #@5
    if-eqz v3, :cond_9

    #@7
    .line 2035
    monitor-exit v4

    #@8
    .line 2074
    :goto_8
    return-void

    #@9
    .line 2038
    :cond_9
    new-instance v0, Landroid/content/IntentFilter;

    #@b
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    #@d
    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@10
    .line 2039
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    #@12
    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@15
    .line 2040
    const-string/jumbo v3, "package"

    #@18
    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@1b
    .line 2041
    new-instance v1, Landroid/webkit/WebViewClassic$PackageListener;

    #@1d
    const/4 v3, 0x0

    #@1e
    invoke-direct {v1, v3}, Landroid/webkit/WebViewClassic$PackageListener;-><init>(Landroid/webkit/WebViewClassic$1;)V

    #@21
    .line 2042
    .local v1, packageListener:Landroid/content/BroadcastReceiver;
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@28
    .line 2043
    const/4 v3, 0x1

    #@29
    sput-boolean v3, Landroid/webkit/WebViewClassic;->sPackageInstallationReceiverAdded:Z

    #@2b
    .line 2044
    monitor-exit v4
    :try_end_2c
    .catchall {:try_start_3 .. :try_end_2c} :catchall_38

    #@2c
    .line 2047
    new-instance v2, Landroid/webkit/WebViewClassic$1;

    #@2e
    invoke-direct {v2, p0}, Landroid/webkit/WebViewClassic$1;-><init>(Landroid/webkit/WebViewClassic;)V

    #@31
    .line 2073
    .local v2, task:Landroid/os/AsyncTask;,"Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/util/Set<Ljava/lang/String;>;>;"
    const/4 v3, 0x0

    #@32
    new-array v3, v3, [Ljava/lang/Void;

    #@34
    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@37
    goto :goto_8

    #@38
    .line 2044
    .end local v0           #filter:Landroid/content/IntentFilter;
    .end local v1           #packageListener:Landroid/content/BroadcastReceiver;
    .end local v2           #task:Landroid/os/AsyncTask;,"Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/util/Set<Ljava/lang/String;>;>;"
    :catchall_38
    move-exception v3

    #@39
    :try_start_39
    monitor-exit v4
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_38

    #@3a
    throw v3
.end method

.method private static declared-synchronized setupProxyListener(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 1929
    const-class v3, Landroid/webkit/WebViewClassic;

    #@2
    monitor-enter v3

    #@3
    :try_start_3
    sget-object v2, Landroid/webkit/WebViewClassic;->sProxyReceiver:Landroid/webkit/WebViewClassic$ProxyReceiver;

    #@5
    if-nez v2, :cond_b

    #@7
    sget-boolean v2, Landroid/webkit/WebViewClassic;->sNotificationsEnabled:Z
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_38

    #@9
    if-nez v2, :cond_d

    #@b
    .line 1945
    :cond_b
    :goto_b
    monitor-exit v3

    #@c
    return-void

    #@d
    .line 1932
    :cond_d
    :try_start_d
    new-instance v1, Landroid/content/IntentFilter;

    #@f
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@12
    .line 1933
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.PROXY_CHANGE"

    #@14
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 1935
    const-string v2, "com.lge.browser.BROWSER_PROXY_CHANGE"

    #@19
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c
    .line 1937
    new-instance v2, Landroid/webkit/WebViewClassic$ProxyReceiver;

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-direct {v2, v4}, Landroid/webkit/WebViewClassic$ProxyReceiver;-><init>(Landroid/webkit/WebViewClassic$1;)V

    #@22
    sput-object v2, Landroid/webkit/WebViewClassic;->sProxyReceiver:Landroid/webkit/WebViewClassic$ProxyReceiver;

    #@24
    .line 1938
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@27
    move-result-object v2

    #@28
    sget-object v4, Landroid/webkit/WebViewClassic;->sProxyReceiver:Landroid/webkit/WebViewClassic$ProxyReceiver;

    #@2a
    invoke-virtual {v2, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2d
    move-result-object v0

    #@2e
    .line 1941
    .local v0, currentProxy:Landroid/content/Intent;
    sget-boolean v2, Landroid/webkit/WebViewClassic;->sBrowserProxyEnabled:Z

    #@30
    if-nez v2, :cond_b

    #@32
    if-eqz v0, :cond_b

    #@34
    .line 1942
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->handleProxyBroadcast(Landroid/content/Intent;)V
    :try_end_37
    .catchall {:try_start_d .. :try_end_37} :catchall_38

    #@37
    goto :goto_b

    #@38
    .line 1929
    .end local v0           #currentProxy:Landroid/content/Intent;
    .end local v1           #filter:Landroid/content/IntentFilter;
    :catchall_38
    move-exception v2

    #@39
    monitor-exit v3

    #@3a
    throw v2
.end method

.method private setupSelectionScroll(II)V
    .registers 16
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/high16 v1, 0x42f0

    #@3
    const/high16 v12, 0x40a0

    #@5
    .line 5114
    const/4 v7, 0x5

    #@6
    .line 5115
    .local v7, SELECT_SCROLL_MARGIN:I
    const/16 v8, 0xc

    #@8
    .line 5116
    .local v8, TOOLBAR_HEIGHT:I
    const/16 v6, 0x78

    #@a
    .line 5117
    .local v6, DEFAULT_DPI:I
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@13
    move-result-object v9

    #@14
    .line 5118
    .local v9, metrics:Landroid/util/DisplayMetrics;
    iget v0, v9, Landroid/util/DisplayMetrics;->xdpi:F

    #@16
    div-float v10, v0, v1

    #@18
    .line 5119
    .local v10, xdpi_ratio:F
    iget v0, v9, Landroid/util/DisplayMetrics;->ydpi:F

    #@1a
    div-float v11, v0, v1

    #@1c
    .line 5120
    .local v11, ydpi_ratio:F
    iput p1, p0, Landroid/webkit/WebViewClassic;->SELECTION_SCROLL_PosX:I

    #@1e
    .line 5121
    iput p2, p0, Landroid/webkit/WebViewClassic;->SELECTION_SCROLL_PosY:I

    #@20
    .line 5122
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@22
    if-nez v0, :cond_25

    #@24
    .line 5150
    :goto_24
    return-void

    #@25
    .line 5124
    :cond_25
    iput v4, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollX:I

    #@27
    .line 5125
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollX:I

    #@2d
    .line 5126
    iput v4, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollY:I

    #@2f
    .line 5127
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@32
    move-result v0

    #@33
    iput v0, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollY:I

    #@35
    .line 5128
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@38
    move-result v0

    #@39
    add-int/2addr v0, p1

    #@3a
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@3d
    move-result v2

    #@3e
    .line 5129
    .local v2, contentX:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@41
    move-result v0

    #@42
    add-int/2addr v0, p2

    #@43
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@46
    move-result v3

    #@47
    .line 5130
    .local v3, contentY:I
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@49
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@4b
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerBounds:Landroid/graphics/Rect;

    #@4d
    move-object v0, p0

    #@4e
    invoke-direct/range {v0 .. v5}, Landroid/webkit/WebViewClassic;->nativeScrollableLayer(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    #@51
    move-result v0

    #@52
    iput v0, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@54
    .line 5132
    iget v0, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@56
    if-eqz v0, :cond_ac

    #@58
    .line 5133
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@5a
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@5c
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@5e
    iget v1, v1, Landroid/graphics/Rect;->right:I

    #@60
    if-eq v0, v1, :cond_82

    #@62
    .line 5134
    iget v0, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollX:I

    #@64
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerBounds:Landroid/graphics/Rect;

    #@66
    iget v1, v1, Landroid/graphics/Rect;->left:I

    #@68
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@6b
    move-result v1

    #@6c
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@6f
    move-result v0

    #@70
    iput v0, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollX:I

    #@72
    .line 5136
    iget v0, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollX:I

    #@74
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerBounds:Landroid/graphics/Rect;

    #@76
    iget v1, v1, Landroid/graphics/Rect;->right:I

    #@78
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@7b
    move-result v1

    #@7c
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@7f
    move-result v0

    #@80
    iput v0, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollX:I

    #@82
    .line 5139
    :cond_82
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@84
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@86
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@88
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    #@8a
    if-eq v0, v1, :cond_ac

    #@8c
    .line 5140
    iget v0, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollY:I

    #@8e
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerBounds:Landroid/graphics/Rect;

    #@90
    iget v1, v1, Landroid/graphics/Rect;->top:I

    #@92
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@95
    move-result v1

    #@96
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@99
    move-result v0

    #@9a
    iput v0, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollY:I

    #@9c
    .line 5142
    iget v0, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollY:I

    #@9e
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerBounds:Landroid/graphics/Rect;

    #@a0
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    #@a2
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@a5
    move-result v1

    #@a6
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@a9
    move-result v0

    #@aa
    iput v0, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollY:I

    #@ac
    .line 5146
    :cond_ac
    iget v0, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollX:I

    #@ae
    mul-float v1, v12, v10

    #@b0
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@b3
    move-result v1

    #@b4
    add-int/2addr v0, v1

    #@b5
    iput v0, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollX:I

    #@b7
    .line 5147
    iget v0, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollX:I

    #@b9
    mul-float v1, v12, v10

    #@bb
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@be
    move-result v1

    #@bf
    sub-int/2addr v0, v1

    #@c0
    iput v0, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollX:I

    #@c2
    .line 5148
    iget v0, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollY:I

    #@c4
    const/high16 v1, 0x4120

    #@c6
    mul-float/2addr v1, v11

    #@c7
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@ca
    move-result v1

    #@cb
    add-int/2addr v0, v1

    #@cc
    iput v0, p0, Landroid/webkit/WebViewClassic;->mMinAutoScrollY:I

    #@ce
    .line 5149
    iget v0, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollY:I

    #@d0
    mul-float v1, v12, v11

    #@d2
    const/high16 v4, 0x4140

    #@d4
    mul-float/2addr v4, v11

    #@d5
    add-float/2addr v1, v4

    #@d6
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@d9
    move-result v1

    #@da
    sub-int/2addr v0, v1

    #@db
    iput v0, p0, Landroid/webkit/WebViewClassic;->mMaxAutoScrollY:I

    #@dd
    goto/16 :goto_24
.end method

.method private static setupTrustStorageListener(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 1891
    sget-object v2, Landroid/webkit/WebViewClassic;->sTrustStorageListener:Landroid/webkit/WebViewClassic$TrustStorageListener;

    #@2
    if-eqz v2, :cond_5

    #@4
    .line 1902
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1894
    :cond_5
    new-instance v1, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@a
    .line 1895
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v2, "android.security.STORAGE_CHANGED"

    #@c
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 1896
    new-instance v2, Landroid/webkit/WebViewClassic$TrustStorageListener;

    #@11
    const/4 v3, 0x0

    #@12
    invoke-direct {v2, v3}, Landroid/webkit/WebViewClassic$TrustStorageListener;-><init>(Landroid/webkit/WebViewClassic$1;)V

    #@15
    sput-object v2, Landroid/webkit/WebViewClassic;->sTrustStorageListener:Landroid/webkit/WebViewClassic$TrustStorageListener;

    #@17
    .line 1897
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@1a
    move-result-object v2

    #@1b
    sget-object v3, Landroid/webkit/WebViewClassic;->sTrustStorageListener:Landroid/webkit/WebViewClassic$TrustStorageListener;

    #@1d
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@20
    move-result-object v0

    #@21
    .line 1899
    .local v0, current:Landroid/content/Intent;
    if-eqz v0, :cond_4

    #@23
    .line 1900
    invoke-static {}, Landroid/webkit/WebViewClassic;->handleCertTrustChanged()V

    #@26
    goto :goto_4
.end method

.method private setupWebkitSelect()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 6077
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->setTranslateMode(Z)V

    #@4
    .line 6078
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->syncSelectionCursors()V

    #@7
    .line 6079
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@9
    if-nez v1, :cond_15

    #@b
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->startSelectActionMode()Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_15

    #@11
    .line 6080
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@14
    .line 6085
    :goto_14
    return v0

    #@15
    .line 6083
    :cond_15
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->startSelectingText()V

    #@18
    .line 6084
    const/4 v0, 0x3

    #@19
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@1b
    .line 6085
    const/4 v0, 0x1

    #@1c
    goto :goto_14
.end method

.method private shouldAnimateTo(Landroid/webkit/WebViewCore$WebKitHitTest;)Z
    .registers 3
    .parameter "hit"

    #@0
    .prologue
    .line 9519
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method private shouldDrawHighlightRect()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 9424
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@3
    if-eqz v1, :cond_9

    #@5
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@7
    if-nez v1, :cond_a

    #@9
    .line 9436
    :cond_9
    :goto_9
    return v0

    #@a
    .line 9427
    :cond_a
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@c
    invoke-virtual {v1}, Landroid/graphics/Region;->isEmpty()Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_9

    #@12
    .line 9430
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@14
    iget-boolean v1, v1, Landroid/webkit/WebViewCore$WebKitHitTest;->mHasFocus:Z

    #@16
    if-eqz v1, :cond_2c

    #@18
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@1a
    invoke-virtual {v1}, Landroid/webkit/WebView;->isInTouchMode()Z

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_2c

    #@20
    .line 9431
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mDrawCursorRing:Z

    #@22
    if-eqz v1, :cond_9

    #@24
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@26
    iget-boolean v1, v1, Landroid/webkit/WebViewCore$WebKitHitTest;->mEditable:Z

    #@28
    if-nez v1, :cond_9

    #@2a
    const/4 v0, 0x1

    #@2b
    goto :goto_9

    #@2c
    .line 9433
    :cond_2c
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@2e
    iget-boolean v1, v1, Landroid/webkit/WebViewCore$WebKitHitTest;->mHasFocus:Z

    #@30
    if-eqz v1, :cond_38

    #@32
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@34
    iget-boolean v1, v1, Landroid/webkit/WebViewCore$WebKitHitTest;->mEditable:Z

    #@36
    if-nez v1, :cond_9

    #@38
    .line 9436
    :cond_38
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mShowTapHighlight:Z

    #@3a
    goto :goto_9
.end method

.method private showLGSelectActionPopupWindow(I)V
    .registers 7
    .parameter "delay"

    #@0
    .prologue
    .line 10564
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsForeground:Z

    #@2
    if-nez v1, :cond_5

    #@4
    .line 10598
    :cond_4
    :goto_4
    return-void

    #@5
    .line 10566
    :cond_5
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@8
    move-result-object v0

    #@9
    .line 10567
    .local v0, settings:Landroid/webkit/WebSettings;
    if-eqz v0, :cond_4

    #@b
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getLGBubbleActionEnabled()Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_4

    #@11
    .line 10570
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mFindIsUp:Z

    #@13
    if-nez v1, :cond_4

    #@15
    .line 10573
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTranslateMode()Z

    #@18
    move-result v1

    #@19
    if-nez v1, :cond_4

    #@1b
    .line 10578
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@1d
    if-nez v1, :cond_2a

    #@1f
    .line 10579
    new-instance v1, Landroid/webkit/LGSelectActionPopupWindow;

    #@21
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@24
    move-result-object v2

    #@25
    invoke-direct {v1, v2, p0}, Landroid/webkit/LGSelectActionPopupWindow;-><init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;)V

    #@28
    iput-object v1, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@2a
    .line 10581
    :cond_2a
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupShower:Ljava/lang/Runnable;

    #@2c
    if-nez v1, :cond_40

    #@2e
    .line 10582
    new-instance v1, Landroid/webkit/WebViewClassic$9;

    #@30
    invoke-direct {v1, p0}, Landroid/webkit/WebViewClassic$9;-><init>(Landroid/webkit/WebViewClassic;)V

    #@33
    iput-object v1, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupShower:Ljava/lang/Runnable;

    #@35
    .line 10596
    :goto_35
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@38
    move-result-object v1

    #@39
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupShower:Ljava/lang/Runnable;

    #@3b
    int-to-long v3, p1

    #@3c
    invoke-virtual {v1, v2, v3, v4}, Landroid/webkit/WebView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@3f
    goto :goto_4

    #@40
    .line 10594
    :cond_40
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@43
    move-result-object v1

    #@44
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupShower:Ljava/lang/Runnable;

    #@46
    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@49
    goto :goto_35
.end method

.method private showPasteWindow()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 5993
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@4
    const-string v7, "clipboard"

    #@6
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v6

    #@a
    check-cast v6, Landroid/content/ClipboardManager;

    #@c
    move-object v0, v6

    #@d
    check-cast v0, Landroid/content/ClipboardManager;

    #@f
    .line 5995
    .local v0, cm:Landroid/content/ClipboardManager;
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    #@12
    move-result v6

    #@13
    if-eqz v6, :cond_6f

    #@15
    .line 5996
    new-instance v1, Landroid/graphics/Point;

    #@17
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@19
    iget v6, v6, Landroid/graphics/Point;->x:I

    #@1b
    invoke-virtual {p0, v6}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@1e
    move-result v6

    #@1f
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@21
    iget v7, v7, Landroid/graphics/Point;->y:I

    #@23
    invoke-virtual {p0, v7}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@26
    move-result v7

    #@27
    invoke-direct {v1, v6, v7}, Landroid/graphics/Point;-><init>(II)V

    #@2a
    .line 5998
    .local v1, cursorPoint:Landroid/graphics/Point;
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->calculateBaseCaretTop()Landroid/graphics/Point;

    #@2d
    move-result-object v2

    #@2e
    .line 5999
    .local v2, cursorTop:Landroid/graphics/Point;
    iget v6, v2, Landroid/graphics/Point;->x:I

    #@30
    invoke-virtual {p0, v6}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@33
    move-result v6

    #@34
    iget v7, v2, Landroid/graphics/Point;->y:I

    #@36
    invoke-virtual {p0, v7}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@39
    move-result v7

    #@3a
    invoke-virtual {v2, v6, v7}, Landroid/graphics/Point;->set(II)V

    #@3d
    .line 6002
    const/4 v6, 0x2

    #@3e
    new-array v3, v6, [I

    #@40
    .line 6003
    .local v3, location:[I
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@42
    invoke-virtual {v6, v3}, Landroid/webkit/WebView;->getLocationInWindow([I)V

    #@45
    .line 6004
    aget v6, v3, v8

    #@47
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@4a
    move-result v7

    #@4b
    sub-int v4, v6, v7

    #@4d
    .line 6005
    .local v4, offsetX:I
    aget v6, v3, v9

    #@4f
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@52
    move-result v7

    #@53
    sub-int v5, v6, v7

    #@55
    .line 6006
    .local v5, offsetY:I
    invoke-virtual {v1, v4, v5}, Landroid/graphics/Point;->offset(II)V

    #@58
    .line 6007
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Point;->offset(II)V

    #@5b
    .line 6008
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mPasteWindow:Landroid/webkit/WebViewClassic$PastePopupWindow;

    #@5d
    if-nez v6, :cond_66

    #@5f
    .line 6009
    new-instance v6, Landroid/webkit/WebViewClassic$PastePopupWindow;

    #@61
    invoke-direct {v6, p0}, Landroid/webkit/WebViewClassic$PastePopupWindow;-><init>(Landroid/webkit/WebViewClassic;)V

    #@64
    iput-object v6, p0, Landroid/webkit/WebViewClassic;->mPasteWindow:Landroid/webkit/WebViewClassic$PastePopupWindow;

    #@66
    .line 6011
    :cond_66
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mPasteWindow:Landroid/webkit/WebViewClassic$PastePopupWindow;

    #@68
    aget v7, v3, v8

    #@6a
    aget v8, v3, v9

    #@6c
    invoke-virtual {v6, v1, v2, v7, v8}, Landroid/webkit/WebViewClassic$PastePopupWindow;->show(Landroid/graphics/Point;Landroid/graphics/Point;II)V

    #@6f
    .line 6013
    .end local v1           #cursorPoint:Landroid/graphics/Point;
    .end local v2           #cursorTop:Landroid/graphics/Point;
    .end local v3           #location:[I
    .end local v4           #offsetX:I
    .end local v5           #offsetY:I
    :cond_6f
    return-void
.end method

.method private snapDraggingCursor()V
    .registers 10

    #@0
    .prologue
    .line 7828
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@2
    iget v5, v5, Landroid/graphics/Point;->x:I

    #@4
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@6
    iget v6, v6, Landroid/graphics/Point;->y:I

    #@8
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@a
    iget-object v7, v7, Landroid/webkit/QuadF;->p4:Landroid/graphics/PointF;

    #@c
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@e
    iget-object v8, v8, Landroid/webkit/QuadF;->p3:Landroid/graphics/PointF;

    #@10
    invoke-static {v5, v6, v7, v8}, Landroid/webkit/WebViewClassic;->scaleAlongSegment(IILandroid/graphics/PointF;Landroid/graphics/PointF;)F

    #@13
    move-result v2

    #@14
    .line 7832
    .local v2, scale:F
    const/4 v5, 0x0

    #@15
    invoke-static {v5, v2}, Ljava/lang/Math;->max(FF)F

    #@18
    move-result v2

    #@19
    .line 7833
    const/high16 v5, 0x3f80

    #@1b
    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    #@1e
    move-result v2

    #@1f
    .line 7834
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@21
    iget-object v5, v5, Landroid/webkit/QuadF;->p4:Landroid/graphics/PointF;

    #@23
    iget v5, v5, Landroid/graphics/PointF;->x:F

    #@25
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@27
    iget-object v6, v6, Landroid/webkit/QuadF;->p3:Landroid/graphics/PointF;

    #@29
    iget v6, v6, Landroid/graphics/PointF;->x:F

    #@2b
    invoke-static {v2, v5, v6}, Landroid/webkit/WebViewClassic;->scaleCoordinate(FFF)F

    #@2e
    move-result v0

    #@2f
    .line 7836
    .local v0, newX:F
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@31
    iget-object v5, v5, Landroid/webkit/QuadF;->p4:Landroid/graphics/PointF;

    #@33
    iget v5, v5, Landroid/graphics/PointF;->y:F

    #@35
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingTextQuad:Landroid/webkit/QuadF;

    #@37
    iget-object v6, v6, Landroid/webkit/QuadF;->p3:Landroid/graphics/PointF;

    #@39
    iget v6, v6, Landroid/graphics/PointF;->y:F

    #@3b
    invoke-static {v2, v5, v6}, Landroid/webkit/WebViewClassic;->scaleCoordinate(FFF)F

    #@3e
    move-result v1

    #@3f
    .line 7838
    .local v1, newY:F
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    #@42
    move-result v3

    #@43
    .line 7839
    .local v3, x:I
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@46
    move-result v4

    #@47
    .line 7840
    .local v4, y:I
    iget-boolean v5, p0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@49
    if-eqz v5, :cond_63

    #@4b
    .line 7841
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@4d
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@4f
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@51
    iget v6, v6, Landroid/graphics/Rect;->right:I

    #@53
    invoke-static {v3, v5, v6}, Landroid/webkit/WebViewClassic;->clampBetween(III)I

    #@56
    move-result v3

    #@57
    .line 7843
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@59
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@5b
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@5d
    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    #@5f
    invoke-static {v4, v5, v6}, Landroid/webkit/WebViewClassic;->clampBetween(III)I

    #@62
    move-result v4

    #@63
    .line 7846
    :cond_63
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@65
    invoke-virtual {v5, v3, v4}, Landroid/graphics/Point;->set(II)V

    #@68
    .line 7847
    return-void
.end method

.method private startDrag()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 7653
    invoke-static {}, Landroid/webkit/WebViewCore;->reducePriority()V

    #@4
    .line 7655
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@6
    invoke-static {v0}, Landroid/webkit/WebViewCore;->pauseUpdatePicture(Landroid/webkit/WebViewCore;)V

    #@9
    .line 7656
    invoke-direct {p0, v1}, Landroid/webkit/WebViewClassic;->nativeSetIsScrolling(Z)V

    #@c
    .line 7659
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 7660
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@12
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewInputDispatcher;->setTouchDragMode(Z)V

    #@15
    .line 7664
    :cond_15
    iget v0, p0, Landroid/webkit/WebViewClassic;->mHorizontalScrollBarMode:I

    #@17
    if-ne v0, v1, :cond_1d

    #@19
    iget v0, p0, Landroid/webkit/WebViewClassic;->mVerticalScrollBarMode:I

    #@1b
    if-eq v0, v1, :cond_22

    #@1d
    .line 7666
    :cond_1d
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@1f
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->invokeZoomPicker()V

    #@22
    .line 7668
    :cond_22
    return-void
.end method

.method private startPrivateBrowsing()V
    .registers 3

    #@0
    .prologue
    .line 3252
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x1

    #@5
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettingsClassic;->setPrivateBrowsingEnabled(Z)V

    #@8
    .line 3253
    return-void
.end method

.method private startScrollingLayer(FF)V
    .registers 9
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 6877
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v0, :cond_5

    #@4
    .line 6887
    :cond_4
    :goto_4
    return-void

    #@5
    .line 6880
    :cond_5
    float-to-int v0, p1

    #@6
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@9
    move-result v1

    #@a
    add-int/2addr v0, v1

    #@b
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@e
    move-result v2

    #@f
    .line 6881
    .local v2, contentX:I
    float-to-int v0, p2

    #@10
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@13
    move-result v1

    #@14
    add-int/2addr v0, v1

    #@15
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@18
    move-result v3

    #@19
    .line 6882
    .local v3, contentY:I
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@1b
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@1d
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerBounds:Landroid/graphics/Rect;

    #@1f
    move-object v0, p0

    #@20
    invoke-direct/range {v0 .. v5}, Landroid/webkit/WebViewClassic;->nativeScrollableLayer(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    #@23
    move-result v0

    #@24
    iput v0, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@26
    .line 6884
    iget v0, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@28
    if-eqz v0, :cond_4

    #@2a
    .line 6885
    const/16 v0, 0x9

    #@2c
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@2e
    goto :goto_4
.end method

.method private startSelectActionMode()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 5967
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@5
    move-result-object v0

    #@6
    .line 5968
    .local v0, settings:Landroid/webkit/WebSettingsClassic;
    if-eqz v0, :cond_29

    #@8
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getLGBubbleActionEnabled()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_29

    #@e
    .line 5969
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@10
    if-nez v1, :cond_23

    #@12
    .line 5970
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mIsSelectingAll:Z

    #@14
    .line 5971
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getFloatingMode()Z

    #@17
    move-result v1

    #@18
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsFloatingMode:Z

    #@1a
    .line 5972
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->focusCandidateIsEditableText()Z

    #@1d
    move-result v1

    #@1e
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsSelectingInEditText:Z

    #@20
    .line 5973
    invoke-direct {p0, v3}, Landroid/webkit/WebViewClassic;->showLGSelectActionPopupWindow(I)V

    #@23
    .line 5975
    :cond_23
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@25
    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->performHapticFeedback(I)Z

    #@28
    .line 5989
    :goto_28
    return v2

    #@29
    .line 5979
    :cond_29
    new-instance v1, Landroid/webkit/SelectActionModeCallback;

    #@2b
    invoke-direct {v1}, Landroid/webkit/SelectActionModeCallback;-><init>()V

    #@2e
    iput-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@30
    .line 5980
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@32
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@34
    if-nez v1, :cond_4e

    #@36
    move v1, v2

    #@37
    :goto_37
    invoke-virtual {v4, v1}, Landroid/webkit/SelectActionModeCallback;->setTextSelected(Z)V

    #@3a
    .line 5981
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@3c
    invoke-virtual {v1, p0}, Landroid/webkit/SelectActionModeCallback;->setWebView(Landroid/webkit/WebViewClassic;)V

    #@3f
    .line 5982
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@41
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@43
    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@46
    move-result-object v1

    #@47
    if-nez v1, :cond_50

    #@49
    .line 5985
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@4c
    move v2, v3

    #@4d
    .line 5986
    goto :goto_28

    #@4e
    :cond_4e
    move v1, v3

    #@4f
    .line 5980
    goto :goto_37

    #@50
    .line 5988
    :cond_50
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@52
    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->performHapticFeedback(I)Z

    #@55
    goto :goto_28
.end method

.method private startSelectingText()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 5270
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@3
    .line 5271
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mShowTextSelectionExtra:Z

    #@5
    .line 5272
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->animateHandles()V

    #@8
    .line 5273
    return-void
.end method

.method private startTouch(FFJ)V
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "eventTime"

    #@0
    .prologue
    .line 7645
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@6
    iput v0, p0, Landroid/webkit/WebViewClassic;->mStartTouchX:I

    #@8
    .line 7646
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    #@b
    move-result v0

    #@c
    iput v0, p0, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@e
    iput v0, p0, Landroid/webkit/WebViewClassic;->mStartTouchY:I

    #@10
    .line 7647
    iput-wide p3, p0, Landroid/webkit/WebViewClassic;->mLastTouchTime:J

    #@12
    .line 7648
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@18
    .line 7649
    const/4 v0, 0x0

    #@19
    iput v0, p0, Landroid/webkit/WebViewClassic;->mSnapScrollMode:I

    #@1b
    .line 7650
    return-void
.end method

.method private stopTouch()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 7735
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@3
    invoke-virtual {v1}, Landroid/widget/OverScroller;->isFinished()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_39

    #@9
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@b
    if-nez v1, :cond_39

    #@d
    iget v1, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@f
    const/4 v2, 0x3

    #@10
    if-eq v1, v2, :cond_18

    #@12
    iget v1, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@14
    const/16 v2, 0x9

    #@16
    if-ne v1, v2, :cond_39

    #@18
    .line 7739
    :cond_18
    sget-boolean v1, Landroid/webkit/WebViewClassic;->mCapptouchFlickNoti:Z

    #@1a
    if-eqz v1, :cond_2e

    #@1c
    .line 7740
    sput v3, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@1e
    .line 7741
    sput v3, Landroid/webkit/WebViewClassic;->moveCounter:I

    #@20
    .line 7742
    sput-boolean v3, Landroid/webkit/WebViewClassic;->dropFirstDeltaY:Z

    #@22
    .line 7743
    const/4 v0, 0x0

    #@23
    .local v0, i:I
    :goto_23
    sget v1, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@25
    if-ge v0, v1, :cond_2e

    #@27
    .line 7744
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->coorDirection:[I

    #@29
    aput v3, v1, v0

    #@2b
    .line 7743
    add-int/lit8 v0, v0, 0x1

    #@2d
    goto :goto_23

    #@2e
    .line 7748
    .end local v0           #i:I
    :cond_2e
    invoke-static {}, Landroid/webkit/WebViewCore;->resumePriority()V

    #@31
    .line 7749
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@33
    invoke-static {v1}, Landroid/webkit/WebViewCore;->resumeUpdatePicture(Landroid/webkit/WebViewCore;)V

    #@36
    .line 7750
    invoke-direct {p0, v3}, Landroid/webkit/WebViewClassic;->nativeSetIsScrolling(Z)V

    #@39
    .line 7754
    :cond_39
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@3b
    if-eqz v1, :cond_42

    #@3d
    .line 7755
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@3f
    invoke-virtual {v1, v3}, Landroid/webkit/WebViewInputDispatcher;->setTouchDragMode(Z)V

    #@42
    .line 7760
    :cond_42
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@44
    invoke-virtual {v1}, Landroid/widget/OverScroller;->isFinished()Z

    #@47
    move-result v1

    #@48
    if-eqz v1, :cond_55

    #@4a
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@4c
    if-eqz v1, :cond_55

    #@4e
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@50
    if-nez v1, :cond_55

    #@52
    .line 7761
    invoke-direct {p0, v3}, Landroid/webkit/WebViewClassic;->showLGSelectActionPopupWindow(I)V

    #@55
    .line 7768
    :cond_55
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@57
    if-eqz v1, :cond_61

    #@59
    .line 7769
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@5b
    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    #@5e
    .line 7770
    const/4 v1, 0x0

    #@5f
    iput-object v1, p0, Landroid/webkit/WebViewClassic;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@61
    .line 7774
    :cond_61
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@63
    if-eqz v1, :cond_6a

    #@65
    .line 7775
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@67
    invoke-virtual {v1}, Landroid/webkit/OverScrollGlow;->releaseAll()V

    #@6a
    .line 7778
    :cond_6a
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@6c
    if-eqz v1, :cond_7d

    #@6e
    .line 7779
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mSelectionStarted:Z

    #@70
    .line 7780
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->syncSelectionCursors()V

    #@73
    .line 7781
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@75
    if-eqz v1, :cond_7a

    #@77
    .line 7782
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->resetCaretTimer()V

    #@7a
    .line 7784
    :cond_7a
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@7d
    .line 7786
    :cond_7d
    return-void
.end method

.method private syncSelectionCursors()V
    .registers 5

    #@0
    .prologue
    .line 6068
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    const/4 v1, 0x0

    #@3
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@5
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBaseTextQuad:Landroid/webkit/QuadF;

    #@7
    invoke-static {v0, v1, v2, v3}, Landroid/webkit/WebViewClassic;->nativeGetHandleLayerId(IILandroid/graphics/Point;Landroid/webkit/QuadF;)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBaseLayerId:I

    #@d
    .line 6071
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@f
    const/4 v1, 0x1

    #@10
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtent:Landroid/graphics/Point;

    #@12
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtentTextQuad:Landroid/webkit/QuadF;

    #@14
    invoke-static {v0, v1, v2, v3}, Landroid/webkit/WebViewClassic;->nativeGetHandleLayerId(IILandroid/graphics/Point;Landroid/webkit/QuadF;)I

    #@17
    move-result v0

    #@18
    iput v0, p0, Landroid/webkit/WebViewClassic;->mSelectCursorExtentLayerId:I

    #@1a
    .line 6074
    return-void
.end method

.method private syncTextSelectionDone()V
    .registers 7

    #@0
    .prologue
    .line 9693
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@2
    if-eqz v5, :cond_26

    #@4
    .line 9694
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@6
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@9
    move-result-object v0

    #@a
    .line 9695
    .local v0, editable:Landroid/text/Editable;
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@d
    move-result v3

    #@e
    .line 9696
    .local v3, len:I
    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@11
    move-result v4

    #@12
    .line 9697
    .local v4, start:I
    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@15
    move-result v1

    #@16
    .line 9698
    .local v1, end:I
    if-eq v4, v1, :cond_26

    #@18
    .line 9699
    move v2, v3

    #@19
    .line 9700
    .local v2, index:I
    if-lez v1, :cond_1e

    #@1b
    if-gt v1, v3, :cond_1e

    #@1d
    .line 9701
    move v2, v1

    #@1e
    .line 9703
    :cond_1e
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@20
    invoke-virtual {v5, v2, v2}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setSelection(II)Z

    #@23
    .line 9704
    invoke-virtual {p0, v2, v2}, Landroid/webkit/WebViewClassic;->setSelection(II)V

    #@26
    .line 9707
    .end local v0           #editable:Landroid/text/Editable;
    .end local v1           #end:I
    .end local v2           #index:I
    .end local v3           #len:I
    .end local v4           #start:I
    :cond_26
    return-void
.end method

.method private updateHwAccelerated()V
    .registers 5

    #@0
    .prologue
    .line 10385
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 10399
    :cond_4
    :goto_4
    return-void

    #@5
    .line 10388
    :cond_5
    const/4 v0, 0x0

    #@6
    .line 10389
    .local v0, hwAccelerated:Z
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@8
    invoke-virtual {v2}, Landroid/webkit/WebView;->isHardwareAccelerated()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_18

    #@e
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@10
    invoke-virtual {v2}, Landroid/webkit/WebView;->getLayerType()I

    #@13
    move-result v2

    #@14
    const/4 v3, 0x1

    #@15
    if-eq v2, v3, :cond_18

    #@17
    .line 10391
    const/4 v0, 0x1

    #@18
    .line 10395
    :cond_18
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@1a
    invoke-static {v2, v0}, Landroid/webkit/WebViewClassic;->nativeSetHwAccelerated(IZ)I

    #@1d
    move-result v1

    #@1e
    .line 10396
    .local v1, result:I
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@20
    if-eqz v2, :cond_4

    #@22
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@24
    if-nez v2, :cond_4

    #@26
    if-eqz v1, :cond_4

    #@28
    .line 10397
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2a
    invoke-virtual {v2}, Landroid/webkit/WebViewCore;->contentDraw()V

    #@2d
    goto :goto_4
.end method

.method private updateTextSelectionFromMessage(IILandroid/webkit/WebViewCore$TextSelectionData;)V
    .registers 11
    .parameter "nodePointer"
    .parameter "textGeneration"
    .parameter "data"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 9716
    const/4 v1, 0x0

    #@3
    .line 9717
    .local v1, isAllSelected:Z
    iget v2, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@5
    if-ne p2, v2, :cond_4e

    #@7
    .line 9718
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@9
    if-eqz v2, :cond_4e

    #@b
    iget v2, p0, Landroid/webkit/WebViewClassic;->mFieldPointer:I

    #@d
    if-ne v2, p1, :cond_4e

    #@f
    .line 9720
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@11
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@14
    move-result-object v2

    #@15
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    #@18
    move-result v0

    #@19
    .line 9721
    .local v0, end:I
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mSelectionReason:I

    #@1b
    if-nez v2, :cond_32

    #@1d
    iget-object v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mText:Ljava/lang/String;

    #@1f
    if-eqz v2, :cond_32

    #@21
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mStart:I

    #@23
    iget v5, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mEnd:I

    #@25
    if-ne v2, v5, :cond_32

    #@27
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mStart:I

    #@29
    if-le v2, v0, :cond_32

    #@2b
    .line 9723
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@2d
    iget-object v5, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mText:Ljava/lang/String;

    #@2f
    invoke-virtual {v2, v5}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setTextAndKeepSelection(Ljava/lang/CharSequence;)V

    #@32
    .line 9727
    :cond_32
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mSelectTextPtr:I

    #@34
    if-eqz v2, :cond_3f

    #@36
    .line 9728
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@38
    iget v5, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mStart:I

    #@3a
    iget v6, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mEnd:I

    #@3c
    invoke-virtual {v2, v5, v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setSelection(II)Z

    #@3f
    .line 9732
    :cond_3f
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mStart:I

    #@41
    iget v5, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mEnd:I

    #@43
    if-eq v2, v5, :cond_4e

    #@45
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mStart:I

    #@47
    if-nez v2, :cond_4e

    #@49
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mEnd:I

    #@4b
    if-ne v2, v0, :cond_4e

    #@4d
    .line 9733
    const/4 v1, 0x1

    #@4e
    .line 9738
    .end local v0           #end:I
    :cond_4e
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@50
    iget v5, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mSelectTextPtr:I

    #@52
    invoke-static {v2, v5}, Landroid/webkit/WebViewClassic;->nativeSetTextSelection(II)V

    #@55
    .line 9740
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mSelectionReason:I

    #@57
    if-eq v2, v3, :cond_68

    #@59
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@5b
    if-nez v2, :cond_86

    #@5d
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mStart:I

    #@5f
    iget v5, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mEnd:I

    #@61
    if-eq v2, v5, :cond_86

    #@63
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mSelectionReason:I

    #@65
    const/4 v5, 0x2

    #@66
    if-eq v2, v5, :cond_86

    #@68
    .line 9743
    :cond_68
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@6b
    .line 9744
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mShowTextSelectionExtra:Z

    #@6d
    .line 9747
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@6f
    if-nez v2, :cond_82

    #@71
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@73
    if-eqz v2, :cond_82

    #@75
    iget v2, p0, Landroid/webkit/WebViewClassic;->mFieldPointer:I

    #@77
    if-ne v2, p1, :cond_82

    #@79
    iget v2, p0, Landroid/webkit/WebViewClassic;->mFieldPointer:I

    #@7b
    if-eqz v2, :cond_82

    #@7d
    .line 9748
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->syncTextSelectionDone()V

    #@80
    .line 9749
    iput-boolean v4, p0, Landroid/webkit/WebViewClassic;->mShowTextSelectionExtra:Z

    #@82
    .line 9753
    :cond_82
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@85
    .line 9791
    :goto_85
    return-void

    #@86
    .line 9757
    :cond_86
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mSelectTextPtr:I

    #@88
    if-eqz v2, :cond_108

    #@8a
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mStart:I

    #@8c
    iget v5, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mEnd:I

    #@8e
    if-ne v2, v5, :cond_a2

    #@90
    iget v2, p0, Landroid/webkit/WebViewClassic;->mFieldPointer:I

    #@92
    if-ne v2, p1, :cond_98

    #@94
    iget v2, p0, Landroid/webkit/WebViewClassic;->mFieldPointer:I

    #@96
    if-nez v2, :cond_a2

    #@98
    :cond_98
    if-nez p1, :cond_108

    #@9a
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mStart:I

    #@9c
    if-nez v2, :cond_108

    #@9e
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mEnd:I

    #@a0
    if-nez v2, :cond_108

    #@a2
    .line 9761
    :cond_a2
    iget v2, p0, Landroid/webkit/WebViewClassic;->mFieldPointer:I

    #@a4
    if-ne v2, p1, :cond_d1

    #@a6
    if-eqz p1, :cond_d1

    #@a8
    move v2, v3

    #@a9
    :goto_a9
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@ab
    .line 9762
    iget v2, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mStart:I

    #@ad
    iget v5, p3, Landroid/webkit/WebViewCore$TextSelectionData;->mEnd:I

    #@af
    if-ne v2, v5, :cond_d3

    #@b1
    if-eqz p1, :cond_d3

    #@b3
    move v2, v3

    #@b4
    :goto_b4
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@b6
    .line 9763
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@b8
    if-eqz v2, :cond_d5

    #@ba
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@bc
    if-eqz v2, :cond_ca

    #@be
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@c0
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@c3
    move-result-object v2

    #@c4
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    #@c7
    move-result v2

    #@c8
    if-nez v2, :cond_d5

    #@ca
    .line 9767
    :cond_ca
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@cd
    .line 9790
    :cond_cd
    :goto_cd
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@d0
    goto :goto_85

    #@d1
    :cond_d1
    move v2, v4

    #@d2
    .line 9761
    goto :goto_a9

    #@d3
    :cond_d3
    move v2, v4

    #@d4
    .line 9762
    goto :goto_b4

    #@d5
    .line 9769
    :cond_d5
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@d7
    if-nez v2, :cond_102

    #@d9
    .line 9770
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->setupWebkitSelect()Z

    #@dc
    .line 9774
    :goto_dc
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->animateHandles()V

    #@df
    .line 9775
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@e1
    if-eqz v2, :cond_e6

    #@e3
    .line 9776
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->resetCaretTimer()V

    #@e6
    .line 9779
    :cond_e6
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsSelectingInEditText:Z

    #@e8
    if-eqz v2, :cond_cd

    #@ea
    .line 9780
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsSelectingAll:Z

    #@ec
    .line 9781
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@ee
    if-eqz v2, :cond_cd

    #@f0
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@f2
    invoke-virtual {v2}, Landroid/webkit/LGSelectActionPopupWindow;->getEnableSelectAll()Z

    #@f5
    move-result v2

    #@f6
    if-eqz v2, :cond_cd

    #@f8
    .line 9782
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mLGSelectActionPopupWindow:Landroid/webkit/LGSelectActionPopupWindow;

    #@fa
    iget-boolean v5, p0, Landroid/webkit/WebViewClassic;->mIsSelectingAll:Z

    #@fc
    if-nez v5, :cond_106

    #@fe
    :goto_fe
    invoke-virtual {v2, v3}, Landroid/webkit/LGSelectActionPopupWindow;->enableSelectAll(Z)V

    #@101
    goto :goto_cd

    #@102
    .line 9772
    :cond_102
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->syncSelectionCursors()V

    #@105
    goto :goto_dc

    #@106
    :cond_106
    move v3, v4

    #@107
    .line 9782
    goto :goto_fe

    #@108
    .line 9788
    :cond_108
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@10b
    goto :goto_cd
.end method

.method private updateWebkitSelection(Z)V
    .registers 9
    .parameter "isSnapped"

    #@0
    .prologue
    const/16 v6, 0xd5

    #@2
    const/4 v0, 0x0

    #@3
    .line 6090
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTranslateMode()Z

    #@6
    move-result v4

    #@7
    if-eqz v4, :cond_c

    #@9
    .line 6091
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@c
    .line 6093
    :cond_c
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->setTranslateMode(Z)V

    #@f
    .line 6095
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@11
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mSelectCursorBase:Landroid/graphics/Point;

    #@13
    if-ne v4, v5, :cond_46

    #@15
    .line 6097
    .local v0, handleId:I
    :goto_15
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@17
    iget v2, v4, Landroid/graphics/Point;->x:I

    #@19
    .line 6098
    .local v2, x:I
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mSelectDraggingCursor:Landroid/graphics/Point;

    #@1b
    iget v3, v4, Landroid/graphics/Point;->y:I

    #@1d
    .line 6099
    .local v3, y:I
    if-eqz p1, :cond_37

    #@1f
    .line 6101
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->calculateDraggingCaretTop()Landroid/graphics/Point;

    #@22
    move-result-object v1

    #@23
    .line 6102
    .local v1, top:Landroid/graphics/Point;
    iget v4, v1, Landroid/graphics/Point;->x:I

    #@25
    add-int/2addr v4, v2

    #@26
    div-int/lit8 v4, v4, 0x2

    #@28
    int-to-float v4, v4

    #@29
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@2c
    move-result v2

    #@2d
    .line 6103
    iget v4, v1, Landroid/graphics/Point;->y:I

    #@2f
    add-int/2addr v4, v3

    #@30
    div-int/lit8 v4, v4, 0x2

    #@32
    int-to-float v4, v4

    #@33
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@36
    move-result v3

    #@37
    .line 6105
    .end local v1           #top:Landroid/graphics/Point;
    :cond_37
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@39
    invoke-virtual {v4, v6}, Landroid/webkit/WebViewCore;->removeMessages(I)V

    #@3c
    .line 6106
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@3e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v4, v6, v2, v3, v5}, Landroid/webkit/WebViewCore;->sendMessageAtFrontOfQueue(IIILjava/lang/Object;)V

    #@45
    .line 6108
    return-void

    #@46
    .line 6095
    .end local v0           #handleId:I
    .end local v2           #x:I
    .end local v3           #y:I
    :cond_46
    const/4 v0, 0x1

    #@47
    goto :goto_15
.end method

.method private viewInvalidate()V
    .registers 1

    #@0
    .prologue
    .line 10227
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@3
    .line 10228
    return-void
.end method

.method private viewInvalidate(IIII)V
    .registers 13
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 3567
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v2}, Landroid/webkit/ZoomManager;->getScale()F

    #@5
    move-result v1

    #@6
    .line 3568
    .local v1, scale:F
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@9
    move-result v0

    #@a
    .line 3569
    .local v0, dy:I
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@c
    int-to-float v3, p1

    #@d
    mul-float/2addr v3, v1

    #@e
    float-to-double v3, v3

    #@f
    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    #@12
    move-result-wide v3

    #@13
    double-to-int v3, v3

    #@14
    int-to-float v4, p2

    #@15
    mul-float/2addr v4, v1

    #@16
    float-to-double v4, v4

    #@17
    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    #@1a
    move-result-wide v4

    #@1b
    double-to-int v4, v4

    #@1c
    add-int/2addr v4, v0

    #@1d
    int-to-float v5, p3

    #@1e
    mul-float/2addr v5, v1

    #@1f
    float-to-double v5, v5

    #@20
    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    #@23
    move-result-wide v5

    #@24
    double-to-int v5, v5

    #@25
    int-to-float v6, p4

    #@26
    mul-float/2addr v6, v1

    #@27
    float-to-double v6, v6

    #@28
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    #@2b
    move-result-wide v6

    #@2c
    double-to-int v6, v6

    #@2d
    add-int/2addr v6, v0

    #@2e
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/webkit/WebView;->invalidate(IIII)V

    #@31
    .line 3573
    return-void
.end method

.method private viewInvalidateDelayed(JIIII)V
    .registers 16
    .parameter "delay"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 3578
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->getScale()F

    #@5
    move-result v8

    #@6
    .line 3579
    .local v8, scale:F
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@9
    move-result v7

    #@a
    .line 3580
    .local v7, dy:I
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@c
    int-to-float v1, p3

    #@d
    mul-float/2addr v1, v8

    #@e
    float-to-double v1, v1

    #@f
    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    #@12
    move-result-wide v1

    #@13
    double-to-int v3, v1

    #@14
    int-to-float v1, p4

    #@15
    mul-float/2addr v1, v8

    #@16
    float-to-double v1, v1

    #@17
    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    #@1a
    move-result-wide v1

    #@1b
    double-to-int v1, v1

    #@1c
    add-int v4, v1, v7

    #@1e
    int-to-float v1, p5

    #@1f
    mul-float/2addr v1, v8

    #@20
    float-to-double v1, v1

    #@21
    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    #@24
    move-result-wide v1

    #@25
    double-to-int v5, v1

    #@26
    int-to-float v1, p6

    #@27
    mul-float/2addr v1, v8

    #@28
    float-to-double v1, v1

    #@29
    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    #@2c
    move-result-wide v1

    #@2d
    double-to-int v1, v1

    #@2e
    add-int v6, v1, v7

    #@30
    move-wide v1, p1

    #@31
    invoke-virtual/range {v0 .. v6}, Landroid/webkit/WebView;->postInvalidateDelayed(JIIII)V

    #@34
    .line 3585
    return-void
.end method

.method private viewToContentDimension(I)I
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 3482
    int-to-float v0, p1

    #@1
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@3
    invoke-virtual {v1}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@6
    move-result v1

    #@7
    mul-float/2addr v0, v1

    #@8
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method private viewToContentVisibleRect(Landroid/graphics/RectF;Landroid/graphics/Rect;)V
    .registers 5
    .parameter "contentRect"
    .parameter "viewRect"

    #@0
    .prologue
    .line 6594
    iget v0, p2, Landroid/graphics/Rect;->left:I

    #@2
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentXf(I)F

    #@5
    move-result v0

    #@6
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@8
    invoke-virtual {v1}, Landroid/webkit/WebView;->getScaleX()F

    #@b
    move-result v1

    #@c
    div-float/2addr v0, v1

    #@d
    iput v0, p1, Landroid/graphics/RectF;->left:F

    #@f
    .line 6599
    iget v0, p2, Landroid/graphics/Rect;->top:I

    #@11
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getVisibleTitleHeightImpl()I

    #@14
    move-result v1

    #@15
    add-int/2addr v0, v1

    #@16
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentYf(I)F

    #@19
    move-result v0

    #@1a
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@1c
    invoke-virtual {v1}, Landroid/webkit/WebView;->getScaleY()F

    #@1f
    move-result v1

    #@20
    div-float/2addr v0, v1

    #@21
    iput v0, p1, Landroid/graphics/RectF;->top:F

    #@23
    .line 6601
    iget v0, p2, Landroid/graphics/Rect;->right:I

    #@25
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentXf(I)F

    #@28
    move-result v0

    #@29
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2b
    invoke-virtual {v1}, Landroid/webkit/WebView;->getScaleX()F

    #@2e
    move-result v1

    #@2f
    div-float/2addr v0, v1

    #@30
    iput v0, p1, Landroid/graphics/RectF;->right:F

    #@32
    .line 6602
    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    #@34
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentYf(I)F

    #@37
    move-result v0

    #@38
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@3a
    invoke-virtual {v1}, Landroid/webkit/WebView;->getScaleY()F

    #@3d
    move-result v1

    #@3e
    div-float/2addr v0, v1

    #@3f
    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    #@41
    .line 6603
    return-void
.end method

.method private viewToContentXf(I)F
    .registers 4
    .parameter "x"

    #@0
    .prologue
    .line 3507
    int-to-float v0, p1

    #@1
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@3
    invoke-virtual {v1}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@6
    move-result v1

    #@7
    mul-float/2addr v0, v1

    #@8
    return v0
.end method

.method private viewToContentYf(I)F
    .registers 4
    .parameter "y"

    #@0
    .prologue
    .line 3516
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@3
    move-result v0

    #@4
    sub-int v0, p1, v0

    #@6
    int-to-float v0, v0

    #@7
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@9
    invoke-virtual {v1}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@c
    move-result v1

    #@d
    mul-float/2addr v0, v1

    #@e
    return v0
.end method


# virtual methods
.method public addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 6
    .parameter "object"
    .parameter "name"

    #@0
    .prologue
    .line 4725
    if-nez p1, :cond_3

    #@2
    .line 4741
    :goto_2
    return-void

    #@3
    .line 4728
    :cond_3
    new-instance v0, Landroid/webkit/WebViewCore$JSInterfaceData;

    #@5
    invoke-direct {v0}, Landroid/webkit/WebViewCore$JSInterfaceData;-><init>()V

    #@8
    .line 4730
    .local v0, arg:Landroid/webkit/WebViewCore$JSInterfaceData;
    iput-object p1, v0, Landroid/webkit/WebViewCore$JSInterfaceData;->mObject:Ljava/lang/Object;

    #@a
    .line 4731
    iput-object p2, v0, Landroid/webkit/WebViewCore$JSInterfaceData;->mInterfaceName:Ljava/lang/String;

    #@c
    .line 4735
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@11
    move-result-object v1

    #@12
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@14
    const/16 v2, 0x11

    #@16
    if-lt v1, v2, :cond_23

    #@18
    .line 4736
    const/4 v1, 0x1

    #@19
    iput-boolean v1, v0, Landroid/webkit/WebViewCore$JSInterfaceData;->mRequireAnnotation:Z

    #@1b
    .line 4740
    :goto_1b
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@1d
    const/16 v2, 0x8a

    #@1f
    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@22
    goto :goto_2

    #@23
    .line 4738
    :cond_23
    const/4 v1, 0x0

    #@24
    iput-boolean v1, v0, Landroid/webkit/WebViewCore$JSInterfaceData;->mRequireAnnotation:Z

    #@26
    goto :goto_1b
.end method

.method adjustDefaultZoomDensity(I)V
    .registers 5
    .parameter "zoomDensity"

    #@0
    .prologue
    .line 2242
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/webkit/WebViewCore;->getFixedDisplayDensity(Landroid/content/Context;)F

    #@5
    move-result v1

    #@6
    const/high16 v2, 0x42c8

    #@8
    mul-float/2addr v1, v2

    #@9
    int-to-float v2, p1

    #@a
    div-float v0, v1, v2

    #@c
    .line 2244
    .local v0, density:F
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->updateDefaultZoomDensity(F)V

    #@f
    .line 2245
    return-void
.end method

.method autoFillForm(I)V
    .registers 5
    .parameter "autoFillQueryId"

    #@0
    .prologue
    .line 10359
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@2
    const/16 v1, 0x94

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@c
    .line 10361
    return-void
.end method

.method public canGoBack()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 3155
    iget-boolean v3, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@4
    if-eqz v3, :cond_b

    #@6
    iget-boolean v3, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@8
    if-nez v3, :cond_b

    #@a
    .line 3164
    :goto_a
    return v1

    #@b
    .line 3159
    :cond_b
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@d
    invoke-virtual {v3}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@10
    move-result-object v0

    #@11
    .line 3160
    .local v0, l:Landroid/webkit/WebBackForwardListClassic;
    monitor-enter v0

    #@12
    .line 3161
    :try_start_12
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardListClassic;->getClearPending()Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_1b

    #@18
    .line 3162
    monitor-exit v0

    #@19
    move v1, v2

    #@1a
    goto :goto_a

    #@1b
    .line 3164
    :cond_1b
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardListClassic;->getCurrentIndex()I

    #@1e
    move-result v3

    #@1f
    if-lez v3, :cond_26

    #@21
    :goto_21
    monitor-exit v0

    #@22
    goto :goto_a

    #@23
    .line 3166
    :catchall_23
    move-exception v1

    #@24
    monitor-exit v0
    :try_end_25
    .catchall {:try_start_12 .. :try_end_25} :catchall_23

    #@25
    throw v1

    #@26
    :cond_26
    move v1, v2

    #@27
    .line 3164
    goto :goto_21
.end method

.method public canGoBackOrForward(I)Z
    .registers 6
    .parameter "steps"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3205
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@3
    invoke-virtual {v3}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@6
    move-result-object v0

    #@7
    .line 3206
    .local v0, l:Landroid/webkit/WebBackForwardListClassic;
    monitor-enter v0

    #@8
    .line 3207
    :try_start_8
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardListClassic;->getClearPending()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_10

    #@e
    .line 3208
    monitor-exit v0

    #@f
    .line 3211
    :goto_f
    return v2

    #@10
    .line 3210
    :cond_10
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardListClassic;->getCurrentIndex()I

    #@13
    move-result v3

    #@14
    add-int v1, v3, p1

    #@16
    .line 3211
    .local v1, newIndex:I
    if-ltz v1, :cond_1f

    #@18
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardListClassic;->getSize()I

    #@1b
    move-result v3

    #@1c
    if-ge v1, v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    :cond_1f
    monitor-exit v0

    #@20
    goto :goto_f

    #@21
    .line 3213
    .end local v1           #newIndex:I
    :catchall_21
    move-exception v2

    #@22
    monitor-exit v0
    :try_end_23
    .catchall {:try_start_8 .. :try_end_23} :catchall_21

    #@23
    throw v2
.end method

.method public canGoForward()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3182
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@3
    invoke-virtual {v2}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@6
    move-result-object v0

    #@7
    .line 3183
    .local v0, l:Landroid/webkit/WebBackForwardListClassic;
    monitor-enter v0

    #@8
    .line 3184
    :try_start_8
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardListClassic;->getClearPending()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_10

    #@e
    .line 3185
    monitor-exit v0

    #@f
    .line 3187
    :goto_f
    return v1

    #@10
    :cond_10
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardListClassic;->getCurrentIndex()I

    #@13
    move-result v2

    #@14
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardListClassic;->getSize()I

    #@17
    move-result v3

    #@18
    add-int/lit8 v3, v3, -0x1

    #@1a
    if-ge v2, v3, :cond_1d

    #@1c
    const/4 v1, 0x1

    #@1d
    :cond_1d
    monitor-exit v0

    #@1e
    goto :goto_f

    #@1f
    .line 3189
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit v0
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_1f

    #@21
    throw v1
.end method

.method public canZoomIn()Z
    .registers 2

    #@0
    .prologue
    .line 8429
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->canZoomIn()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public canZoomOut()Z
    .registers 2

    #@0
    .prologue
    .line 8437
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->canZoomOut()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public capturePicture()Landroid/graphics/Picture;
    .registers 3

    #@0
    .prologue
    .line 3327
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v1, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 3330
    :goto_5
    return-object v0

    #@6
    .line 3328
    :cond_6
    new-instance v0, Landroid/graphics/Picture;

    #@8
    invoke-direct {v0}, Landroid/graphics/Picture;-><init>()V

    #@b
    .line 3329
    .local v0, result:Landroid/graphics/Picture;
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->nativeCopyBaseContentToPicture(Landroid/graphics/Picture;)V

    #@e
    goto :goto_5
.end method

.method centerFitRect(Landroid/graphics/Rect;)V
    .registers 29
    .parameter "rect"

    #@0
    .prologue
    .line 8532
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    #@3
    move-result v17

    #@4
    .line 8533
    .local v17, rectWidth:I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    #@7
    move-result v12

    #@8
    .line 8534
    .local v12, rectHeight:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@b
    move-result v20

    #@c
    .line 8535
    .local v20, viewWidth:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@f
    move-result v19

    #@10
    .line 8536
    .local v19, viewHeight:I
    move/from16 v0, v20

    #@12
    int-to-float v0, v0

    #@13
    move/from16 v23, v0

    #@15
    move/from16 v0, v17

    #@17
    int-to-float v0, v0

    #@18
    move/from16 v24, v0

    #@1a
    div-float v23, v23, v24

    #@1c
    move/from16 v0, v19

    #@1e
    int-to-float v0, v0

    #@1f
    move/from16 v24, v0

    #@21
    int-to-float v0, v12

    #@22
    move/from16 v25, v0

    #@24
    div-float v24, v24, v25

    #@26
    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->min(FF)F

    #@29
    move-result v18

    #@2a
    .line 8538
    .local v18, scale:F
    move-object/from16 v0, p0

    #@2c
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2e
    move-object/from16 v23, v0

    #@30
    move-object/from16 v0, v23

    #@32
    move/from16 v1, v18

    #@34
    invoke-virtual {v0, v1}, Landroid/webkit/ZoomManager;->computeScaleWithLimits(F)F

    #@37
    move-result v18

    #@38
    .line 8539
    move-object/from16 v0, p0

    #@3a
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@3c
    move-object/from16 v23, v0

    #@3e
    move-object/from16 v0, v23

    #@40
    move/from16 v1, v18

    #@42
    invoke-virtual {v0, v1}, Landroid/webkit/ZoomManager;->willScaleTriggerZoom(F)Z

    #@45
    move-result v23

    #@46
    if-nez v23, :cond_86

    #@48
    .line 8540
    move-object/from16 v0, p1

    #@4a
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@4c
    move/from16 v23, v0

    #@4e
    div-int/lit8 v24, v17, 0x2

    #@50
    add-int v23, v23, v24

    #@52
    move-object/from16 v0, p0

    #@54
    move/from16 v1, v23

    #@56
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@59
    move-result v23

    #@5a
    div-int/lit8 v24, v20, 0x2

    #@5c
    sub-int v23, v23, v24

    #@5e
    move-object/from16 v0, p1

    #@60
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@62
    move/from16 v24, v0

    #@64
    div-int/lit8 v25, v12, 0x2

    #@66
    add-int v24, v24, v25

    #@68
    move-object/from16 v0, p0

    #@6a
    move/from16 v1, v24

    #@6c
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@6f
    move-result v24

    #@70
    div-int/lit8 v25, v19, 0x2

    #@72
    sub-int v24, v24, v25

    #@74
    const/16 v25, 0x1

    #@76
    const/16 v26, 0x0

    #@78
    move-object/from16 v0, p0

    #@7a
    move/from16 v1, v23

    #@7c
    move/from16 v2, v24

    #@7e
    move/from16 v3, v25

    #@80
    move/from16 v4, v26

    #@82
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/webkit/WebViewClassic;->pinScrollTo(IIZI)Z

    #@85
    .line 8575
    :goto_85
    return-void

    #@86
    .line 8544
    :cond_86
    move-object/from16 v0, p0

    #@88
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@8a
    move-object/from16 v23, v0

    #@8c
    invoke-virtual/range {v23 .. v23}, Landroid/webkit/ZoomManager;->getScale()F

    #@8f
    move-result v5

    #@90
    .line 8545
    .local v5, actualScale:F
    move-object/from16 v0, p1

    #@92
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@94
    move/from16 v23, v0

    #@96
    move/from16 v0, v23

    #@98
    int-to-float v0, v0

    #@99
    move/from16 v23, v0

    #@9b
    mul-float v23, v23, v5

    #@9d
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@a0
    move-result v24

    #@a1
    move/from16 v0, v24

    #@a3
    int-to-float v0, v0

    #@a4
    move/from16 v24, v0

    #@a6
    sub-float v10, v23, v24

    #@a8
    .line 8546
    .local v10, oldScreenX:F
    move-object/from16 v0, p1

    #@aa
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@ac
    move/from16 v23, v0

    #@ae
    move/from16 v0, v23

    #@b0
    int-to-float v0, v0

    #@b1
    move/from16 v23, v0

    #@b3
    mul-float v15, v23, v18

    #@b5
    .line 8547
    .local v15, rectViewX:F
    move/from16 v0, v17

    #@b7
    int-to-float v0, v0

    #@b8
    move/from16 v23, v0

    #@ba
    mul-float v14, v23, v18

    #@bc
    .line 8548
    .local v14, rectViewWidth:F
    move-object/from16 v0, p0

    #@be
    iget v0, v0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@c0
    move/from16 v23, v0

    #@c2
    move/from16 v0, v23

    #@c4
    int-to-float v0, v0

    #@c5
    move/from16 v23, v0

    #@c7
    mul-float v7, v23, v18

    #@c9
    .line 8549
    .local v7, newMaxWidth:F
    move/from16 v0, v20

    #@cb
    int-to-float v0, v0

    #@cc
    move/from16 v23, v0

    #@ce
    sub-float v23, v23, v14

    #@d0
    const/high16 v24, 0x4000

    #@d2
    div-float v8, v23, v24

    #@d4
    .line 8551
    .local v8, newScreenX:F
    cmpl-float v23, v8, v15

    #@d6
    if-lez v23, :cond_178

    #@d8
    .line 8552
    move v8, v15

    #@d9
    .line 8556
    :cond_d9
    :goto_d9
    mul-float v23, v10, v18

    #@db
    mul-float v24, v8, v5

    #@dd
    sub-float v23, v23, v24

    #@df
    sub-float v24, v18, v5

    #@e1
    div-float v21, v23, v24

    #@e3
    .line 8558
    .local v21, zoomCenterX:F
    move-object/from16 v0, p1

    #@e5
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@e7
    move/from16 v23, v0

    #@e9
    move/from16 v0, v23

    #@eb
    int-to-float v0, v0

    #@ec
    move/from16 v23, v0

    #@ee
    mul-float v23, v23, v5

    #@f0
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@f3
    move-result v24

    #@f4
    move/from16 v0, v24

    #@f6
    int-to-float v0, v0

    #@f7
    move/from16 v24, v0

    #@f9
    add-float v23, v23, v24

    #@fb
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@fe
    move-result v24

    #@ff
    move/from16 v0, v24

    #@101
    int-to-float v0, v0

    #@102
    move/from16 v24, v0

    #@104
    sub-float v11, v23, v24

    #@106
    .line 8560
    .local v11, oldScreenY:F
    move-object/from16 v0, p1

    #@108
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@10a
    move/from16 v23, v0

    #@10c
    move/from16 v0, v23

    #@10e
    int-to-float v0, v0

    #@10f
    move/from16 v23, v0

    #@111
    mul-float v23, v23, v18

    #@113
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@116
    move-result v24

    #@117
    move/from16 v0, v24

    #@119
    int-to-float v0, v0

    #@11a
    move/from16 v24, v0

    #@11c
    add-float v16, v23, v24

    #@11e
    .line 8561
    .local v16, rectViewY:F
    int-to-float v0, v12

    #@11f
    move/from16 v23, v0

    #@121
    mul-float v13, v23, v18

    #@123
    .line 8562
    .local v13, rectViewHeight:F
    move-object/from16 v0, p0

    #@125
    iget v0, v0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@127
    move/from16 v23, v0

    #@129
    move/from16 v0, v23

    #@12b
    int-to-float v0, v0

    #@12c
    move/from16 v23, v0

    #@12e
    mul-float v23, v23, v18

    #@130
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@133
    move-result v24

    #@134
    move/from16 v0, v24

    #@136
    int-to-float v0, v0

    #@137
    move/from16 v24, v0

    #@139
    add-float v6, v23, v24

    #@13b
    .line 8563
    .local v6, newMaxHeight:F
    move/from16 v0, v19

    #@13d
    int-to-float v0, v0

    #@13e
    move/from16 v23, v0

    #@140
    sub-float v23, v23, v13

    #@142
    const/high16 v24, 0x4000

    #@144
    div-float v9, v23, v24

    #@146
    .line 8565
    .local v9, newScreenY:F
    cmpl-float v23, v9, v16

    #@148
    if-lez v23, :cond_18b

    #@14a
    .line 8566
    move/from16 v9, v16

    #@14c
    .line 8570
    :cond_14c
    :goto_14c
    mul-float v23, v11, v18

    #@14e
    mul-float v24, v9, v5

    #@150
    sub-float v23, v23, v24

    #@152
    sub-float v24, v18, v5

    #@154
    div-float v22, v23, v24

    #@156
    .line 8572
    .local v22, zoomCenterY:F
    move-object/from16 v0, p0

    #@158
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@15a
    move-object/from16 v23, v0

    #@15c
    move-object/from16 v0, v23

    #@15e
    move/from16 v1, v21

    #@160
    move/from16 v2, v22

    #@162
    invoke-virtual {v0, v1, v2}, Landroid/webkit/ZoomManager;->setZoomCenter(FF)V

    #@165
    .line 8573
    move-object/from16 v0, p0

    #@167
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@169
    move-object/from16 v23, v0

    #@16b
    const/16 v24, 0x0

    #@16d
    move-object/from16 v0, v23

    #@16f
    move/from16 v1, v18

    #@171
    move/from16 v2, v24

    #@173
    invoke-virtual {v0, v1, v2}, Landroid/webkit/ZoomManager;->startZoomAnimation(FZ)Z

    #@176
    goto/16 :goto_85

    #@178
    .line 8553
    .end local v6           #newMaxHeight:F
    .end local v9           #newScreenY:F
    .end local v11           #oldScreenY:F
    .end local v13           #rectViewHeight:F
    .end local v16           #rectViewY:F
    .end local v21           #zoomCenterX:F
    .end local v22           #zoomCenterY:F
    :cond_178
    sub-float v23, v7, v15

    #@17a
    sub-float v23, v23, v14

    #@17c
    cmpl-float v23, v8, v23

    #@17e
    if-lez v23, :cond_d9

    #@180
    .line 8554
    move/from16 v0, v20

    #@182
    int-to-float v0, v0

    #@183
    move/from16 v23, v0

    #@185
    sub-float v24, v7, v15

    #@187
    sub-float v8, v23, v24

    #@189
    goto/16 :goto_d9

    #@18b
    .line 8567
    .restart local v6       #newMaxHeight:F
    .restart local v9       #newScreenY:F
    .restart local v11       #oldScreenY:F
    .restart local v13       #rectViewHeight:F
    .restart local v16       #rectViewY:F
    .restart local v21       #zoomCenterX:F
    :cond_18b
    sub-float v23, v6, v16

    #@18d
    sub-float v23, v23, v13

    #@18f
    cmpl-float v23, v9, v23

    #@191
    if-lez v23, :cond_14c

    #@193
    .line 8568
    move/from16 v0, v19

    #@195
    int-to-float v0, v0

    #@196
    move/from16 v23, v0

    #@198
    sub-float v24, v6, v16

    #@19a
    sub-float v9, v23, v24

    #@19c
    goto :goto_14c
.end method

.method public clearCache(Z)V
    .registers 6
    .parameter "includeDiskFiles"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4096
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@3
    const/16 v3, 0x6f

    #@5
    if-eqz p1, :cond_c

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    invoke-virtual {v2, v3, v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@b
    .line 4098
    return-void

    #@c
    :cond_c
    move v0, v1

    #@d
    .line 4096
    goto :goto_8
.end method

.method public clearFormData()V
    .registers 2

    #@0
    .prologue
    .line 4105
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 4106
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@6
    invoke-virtual {v0}, Landroid/webkit/AutoCompletePopup;->clearAdapter()V

    #@9
    .line 4108
    :cond_9
    return-void
.end method

.method public clearHistory()V
    .registers 3

    #@0
    .prologue
    .line 4115
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardListClassic;->setClearPending()V

    #@9
    .line 4116
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@b
    const/16 v1, 0x70

    #@d
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@10
    .line 4117
    return-void
.end method

.method public clearMatches()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0xdd

    #@2
    .line 4321
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@4
    if-nez v0, :cond_7

    #@6
    .line 4325
    :goto_6
    return-void

    #@7
    .line 4323
    :cond_7
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@9
    invoke-virtual {v0, v2}, Landroid/webkit/WebViewCore;->removeMessages(I)V

    #@c
    .line 4324
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@e
    const/4 v1, 0x0

    #@f
    invoke-virtual {v0, v2, v1}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@12
    goto :goto_6
.end method

.method public clearSslPreferences()V
    .registers 3

    #@0
    .prologue
    .line 4124
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0x96

    #@4
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@7
    .line 4125
    return-void
.end method

.method public clearView()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3316
    iput v0, p0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@3
    .line 3317
    iput v0, p0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@5
    .line 3318
    invoke-virtual {p0, v0, v0, v0}, Landroid/webkit/WebViewClassic;->setBaseLayer(IZZ)V

    #@8
    .line 3319
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@a
    const/16 v1, 0x86

    #@c
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@f
    .line 3320
    return-void
.end method

.method public clearViewState()V
    .registers 2

    #@0
    .prologue
    .line 2924
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@3
    .line 2925
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mLoadedPicture:Landroid/webkit/WebViewCore$DrawData;

    #@6
    .line 2926
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@9
    .line 2927
    return-void
.end method

.method public closedFindDialog()Z
    .registers 2

    #@0
    .prologue
    .line 4243
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebView$PrivateAccess;->closedFindDialog()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public computeHorizontalScrollOffset()I
    .registers 3

    #@0
    .prologue
    .line 3827
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public computeHorizontalScrollRange()I
    .registers 5

    #@0
    .prologue
    .line 3811
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->computeRealHorizontalScrollRange()I

    #@3
    move-result v1

    #@4
    .line 3814
    .local v1, range:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@7
    move-result v2

    #@8
    .line 3815
    .local v2, scrollX:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollX()I

    #@b
    move-result v0

    #@c
    .line 3816
    .local v0, overscrollRight:I
    if-gez v2, :cond_10

    #@e
    .line 3817
    sub-int/2addr v1, v2

    #@f
    .line 3822
    :cond_f
    :goto_f
    return v1

    #@10
    .line 3818
    :cond_10
    if-le v2, v0, :cond_f

    #@12
    .line 3819
    sub-int v3, v2, v0

    #@14
    add-int/2addr v1, v3

    #@15
    goto :goto_f
.end method

.method computeMaxScrollX()I
    .registers 3

    #@0
    .prologue
    .line 8128
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->computeRealHorizontalScrollRange()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@7
    move-result v1

    #@8
    sub-int/2addr v0, v1

    #@9
    const/4 v1, 0x0

    #@a
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method computeMaxScrollY()I
    .registers 3

    #@0
    .prologue
    .line 8136
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->computeRealVerticalScrollRange()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@7
    move-result v1

    #@8
    add-int/2addr v0, v1

    #@9
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@c
    move-result v1

    #@d
    sub-int/2addr v0, v1

    #@e
    const/4 v1, 0x0

    #@f
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@12
    move-result v0

    #@13
    return v0
.end method

.method computeReadingLevelScale(F)F
    .registers 3
    .parameter "scale"

    #@0
    .prologue
    .line 3347
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/ZoomManager;->computeReadingLevelScale(F)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public computeScroll()V
    .registers 13

    #@0
    .prologue
    const/16 v8, 0xa

    #@2
    const/16 v2, 0x9

    #@4
    const/4 v1, 0x1

    #@5
    const/4 v9, 0x0

    #@6
    .line 4366
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@8
    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_c0

    #@e
    .line 4367
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@11
    move-result v3

    #@12
    .line 4368
    .local v3, oldX:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@15
    move-result v4

    #@16
    .line 4369
    .local v4, oldY:I
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@18
    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    #@1b
    move-result v10

    #@1c
    .line 4370
    .local v10, x:I
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@1e
    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrY()I

    #@21
    move-result v11

    #@22
    .line 4371
    .local v11, y:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@25
    .line 4386
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@27
    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    #@2a
    move-result v0

    #@2b
    if-nez v0, :cond_82

    #@2d
    .line 4387
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSendScroll:Landroid/webkit/WebViewClassic$SendScrollToWebCore;

    #@2f
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->setPostpone(Z)V

    #@32
    .line 4388
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollX()I

    #@35
    move-result v5

    #@36
    .line 4389
    .local v5, rangeX:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollY()I

    #@39
    move-result v6

    #@3a
    .line 4390
    .local v6, rangeY:I
    iget v7, p0, Landroid/webkit/WebViewClassic;->mOverflingDistance:I

    #@3c
    .line 4393
    .local v7, overflingDistance:I
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@3e
    if-ne v0, v2, :cond_6c

    #@40
    .line 4394
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@42
    iget v3, v0, Landroid/graphics/Rect;->left:I

    #@44
    .line 4395
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@46
    iget v4, v0, Landroid/graphics/Rect;->top:I

    #@48
    .line 4396
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@4a
    iget v5, v0, Landroid/graphics/Rect;->right:I

    #@4c
    .line 4397
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScrollingLayerRect:Landroid/graphics/Rect;

    #@4e
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    #@50
    .line 4399
    const/4 v7, 0x0

    #@51
    .line 4408
    :cond_51
    :goto_51
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@53
    sub-int v1, v10, v3

    #@55
    sub-int v2, v11, v4

    #@57
    move v8, v7

    #@58
    invoke-virtual/range {v0 .. v9}, Landroid/webkit/WebView$PrivateAccess;->overScrollBy(IIIIIIIIZ)V

    #@5b
    .line 4412
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@5d
    if-eqz v0, :cond_66

    #@5f
    .line 4413
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@61
    move v1, v10

    #@62
    move v2, v11

    #@63
    invoke-virtual/range {v0 .. v6}, Landroid/webkit/OverScrollGlow;->absorbGlow(IIIIII)V

    #@66
    .line 4415
    :cond_66
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSendScroll:Landroid/webkit/WebViewClassic$SendScrollToWebCore;

    #@68
    invoke-virtual {v0, v9}, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->setPostpone(Z)V

    #@6b
    .line 4453
    .end local v3           #oldX:I
    .end local v4           #oldY:I
    .end local v5           #rangeX:I
    .end local v6           #rangeY:I
    .end local v7           #overflingDistance:I
    .end local v10           #x:I
    .end local v11           #y:I
    :cond_6b
    :goto_6b
    return-void

    #@6c
    .line 4400
    .restart local v3       #oldX:I
    .restart local v4       #oldY:I
    .restart local v5       #rangeX:I
    .restart local v6       #rangeY:I
    .restart local v7       #overflingDistance:I
    .restart local v10       #x:I
    .restart local v11       #y:I
    :cond_6c
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@6e
    if-ne v0, v8, :cond_51

    #@70
    .line 4401
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getTextScrollX()I

    #@73
    move-result v3

    #@74
    .line 4402
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getTextScrollY()I

    #@77
    move-result v4

    #@78
    .line 4403
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollX()I

    #@7b
    move-result v5

    #@7c
    .line 4404
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getMaxTextScrollY()I

    #@7f
    move-result v6

    #@80
    .line 4405
    const/4 v7, 0x0

    #@81
    goto :goto_51

    #@82
    .line 4429
    .end local v5           #rangeX:I
    .end local v6           #rangeY:I
    .end local v7           #overflingDistance:I
    :cond_82
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@84
    if-ne v0, v2, :cond_b1

    #@86
    .line 4431
    invoke-direct {p0, v10, v11}, Landroid/webkit/WebViewClassic;->scrollLayerTo(II)V

    #@89
    .line 4438
    :goto_89
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->abortAnimation()V

    #@8c
    .line 4439
    invoke-direct {p0, v9}, Landroid/webkit/WebViewClassic;->nativeSetIsScrolling(Z)V

    #@8f
    .line 4440
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@91
    if-nez v0, :cond_9f

    #@93
    .line 4441
    invoke-static {}, Landroid/webkit/WebViewCore;->resumePriority()V

    #@96
    .line 4442
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@98
    if-nez v0, :cond_9f

    #@9a
    .line 4443
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@9c
    invoke-static {v0}, Landroid/webkit/WebViewCore;->resumeUpdatePicture(Landroid/webkit/WebViewCore;)V

    #@9f
    .line 4446
    :cond_9f
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@a2
    move-result v0

    #@a3
    if-ne v3, v0, :cond_ab

    #@a5
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@a8
    move-result v0

    #@a9
    if-eq v4, v0, :cond_6b

    #@ab
    .line 4447
    :cond_ab
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mSendScroll:Landroid/webkit/WebViewClassic$SendScrollToWebCore;

    #@ad
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->send(Z)V

    #@b0
    goto :goto_6b

    #@b1
    .line 4432
    :cond_b1
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@b3
    if-ne v0, v8, :cond_b9

    #@b5
    .line 4433
    invoke-direct {p0, v10, v11}, Landroid/webkit/WebViewClassic;->scrollEditText(II)V

    #@b8
    goto :goto_89

    #@b9
    .line 4435
    :cond_b9
    invoke-virtual {p0, v10}, Landroid/webkit/WebViewClassic;->setScrollXRaw(I)V

    #@bc
    .line 4436
    invoke-virtual {p0, v11}, Landroid/webkit/WebViewClassic;->setScrollYRaw(I)V

    #@bf
    goto :goto_89

    #@c0
    .line 4451
    .end local v3           #oldX:I
    .end local v4           #oldY:I
    .end local v10           #x:I
    .end local v11           #y:I
    :cond_c0
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@c2
    invoke-virtual {v0}, Landroid/webkit/WebView$PrivateAccess;->super_computeScroll()V

    #@c5
    goto :goto_6b
.end method

.method public computeVerticalScrollExtent()I
    .registers 2

    #@0
    .prologue
    .line 3862
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewHeight()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public computeVerticalScrollOffset()I
    .registers 3

    #@0
    .prologue
    .line 3857
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@7
    move-result v1

    #@8
    sub-int/2addr v0, v1

    #@9
    const/4 v1, 0x0

    #@a
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public computeVerticalScrollRange()I
    .registers 5

    #@0
    .prologue
    .line 3841
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->computeRealVerticalScrollRange()I

    #@3
    move-result v1

    #@4
    .line 3844
    .local v1, range:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@7
    move-result v2

    #@8
    .line 3845
    .local v2, scrollY:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollY()I

    #@b
    move-result v0

    #@c
    .line 3846
    .local v0, overscrollBottom:I
    if-gez v2, :cond_10

    #@e
    .line 3847
    sub-int/2addr v1, v2

    #@f
    .line 3852
    :cond_f
    :goto_f
    return v1

    #@10
    .line 3848
    :cond_10
    if-le v2, v0, :cond_f

    #@12
    .line 3849
    sub-int v3, v2, v0

    #@14
    add-int/2addr v1, v3

    #@15
    goto :goto_f
.end method

.method protected contentInvalidateAll()V
    .registers 3

    #@0
    .prologue
    .line 10369
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@6
    if-nez v0, :cond_f

    #@8
    .line 10370
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@a
    const/16 v1, 0xaf

    #@c
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@f
    .line 10372
    :cond_f
    return-void
.end method

.method contentToViewDimension(I)I
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 3526
    int-to-float v0, p1

    #@1
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@3
    invoke-virtual {v1}, Landroid/webkit/ZoomManager;->getScale()F

    #@6
    move-result v1

    #@7
    mul-float/2addr v0, v1

    #@8
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method contentToViewX(I)I
    .registers 3
    .parameter "x"

    #@0
    .prologue
    .line 3534
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method contentToViewY(I)I
    .registers 4
    .parameter "y"

    #@0
    .prologue
    .line 3542
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@7
    move-result v1

    #@8
    add-int/2addr v0, v1

    #@9
    return v0
.end method

.method public bridge synthetic copyBackForwardList()Landroid/webkit/WebBackForwardList;
    .registers 2

    #@0
    .prologue
    .line 189
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->copyBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public copyBackForwardList()Landroid/webkit/WebBackForwardListClassic;
    .registers 2

    #@0
    .prologue
    .line 4132
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebBackForwardListClassic;->clone()Landroid/webkit/WebBackForwardListClassic;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public copySelection()Z
    .registers 11

    #@0
    .prologue
    .line 6185
    const/4 v3, 0x0

    #@1
    .line 6186
    .local v3, copiedSomething:Z
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@4
    move-result-object v5

    #@5
    .line 6189
    .local v5, selection:Ljava/lang/String;
    iget-boolean v7, p0, Landroid/webkit/WebViewClassic;->mIsSelectingInEditText:Z

    #@7
    if-eqz v7, :cond_c

    #@9
    .line 6190
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->syncTextSelectionDone()V

    #@c
    .line 6194
    :cond_c
    if-eqz v5, :cond_3c

    #@e
    const-string v7, ""

    #@10
    if-eq v5, v7, :cond_3c

    #@12
    .line 6199
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitle()Ljava/lang/String;

    #@15
    move-result-object v7

    #@16
    invoke-static {v7, v5}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@19
    move-result-object v0

    #@1a
    .line 6200
    .local v0, clip:Landroid/content/ClipData;
    if-nez v0, :cond_26

    #@1c
    .line 6201
    const-string/jumbo v7, "webview"

    #@1f
    const-string v8, "copySelection(): Fail to get ClipData"

    #@21
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    move v4, v3

    #@25
    .line 6235
    .end local v0           #clip:Landroid/content/ClipData;
    .end local v3           #copiedSomething:Z
    .local v4, copiedSomething:I
    :goto_25
    return v4

    #@26
    .line 6205
    .end local v4           #copiedSomething:I
    .restart local v0       #clip:Landroid/content/ClipData;
    .restart local v3       #copiedSomething:Z
    :cond_26
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@29
    move-result-object v6

    #@2a
    .line 6206
    .local v6, settings:Landroid/webkit/WebSettings;
    if-eqz v6, :cond_41

    #@2c
    invoke-virtual {v6}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@2f
    move-result v7

    #@30
    if-eqz v7, :cond_41

    #@32
    .line 6207
    invoke-static {}, Landroid/webkit/LGCliptrayManager;->getInstance()Landroid/webkit/LGCliptrayManager;

    #@35
    move-result-object v1

    #@36
    .line 6208
    .local v1, cliptray:Landroid/webkit/LGCliptrayManager;
    if-eqz v1, :cond_3c

    #@38
    .line 6209
    invoke-virtual {v1, v0}, Landroid/webkit/LGCliptrayManager;->copyToCliptray(Landroid/content/ClipData;)V

    #@3b
    .line 6210
    const/4 v3, 0x1

    #@3c
    .line 6234
    .end local v0           #clip:Landroid/content/ClipData;
    .end local v1           #cliptray:Landroid/webkit/LGCliptrayManager;
    .end local v6           #settings:Landroid/webkit/WebSettings;
    :cond_3c
    :goto_3c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@3f
    move v4, v3

    #@40
    .line 6235
    .restart local v4       #copiedSomething:I
    goto :goto_25

    #@41
    .line 6213
    .end local v4           #copiedSomething:I
    .restart local v0       #clip:Landroid/content/ClipData;
    .restart local v6       #settings:Landroid/webkit/WebSettings;
    :cond_41
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@43
    const-string v8, "clipboard"

    #@45
    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@48
    move-result-object v2

    #@49
    check-cast v2, Landroid/content/ClipboardManager;

    #@4b
    .line 6215
    .local v2, cm:Landroid/content/ClipboardManager;
    if-eqz v2, :cond_3c

    #@4d
    .line 6216
    invoke-virtual {v2, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@50
    .line 6217
    const/4 v3, 0x1

    #@51
    .line 6221
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@53
    const v8, 0x2090157

    #@56
    const/4 v9, 0x0

    #@57
    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    #@5e
    goto :goto_3c
.end method

.method public cutSelection()V
    .registers 9

    #@0
    .prologue
    .line 6340
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@3
    move-result-object v5

    #@4
    .line 6341
    .local v5, text:Ljava/lang/String;
    if-eqz v5, :cond_32

    #@6
    const-string v6, ""

    #@8
    if-eq v5, v6, :cond_32

    #@a
    .line 6343
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitle()Ljava/lang/String;

    #@d
    move-result-object v6

    #@e
    invoke-static {v6, v5}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@11
    move-result-object v0

    #@12
    .line 6344
    .local v0, clip:Landroid/content/ClipData;
    if-nez v0, :cond_1d

    #@14
    .line 6345
    const-string/jumbo v6, "webview"

    #@17
    const-string v7, "cutSelection(): Fail to get ClipData"

    #@19
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 6375
    .end local v0           #clip:Landroid/content/ClipData;
    :goto_1c
    return-void

    #@1d
    .line 6349
    .restart local v0       #clip:Landroid/content/ClipData;
    :cond_1d
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@20
    move-result-object v4

    #@21
    .line 6350
    .local v4, settings:Landroid/webkit/WebSettings;
    if-eqz v4, :cond_3e

    #@23
    invoke-virtual {v4}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@26
    move-result v6

    #@27
    if-eqz v6, :cond_3e

    #@29
    .line 6351
    invoke-static {}, Landroid/webkit/LGCliptrayManager;->getInstance()Landroid/webkit/LGCliptrayManager;

    #@2c
    move-result-object v1

    #@2d
    .line 6352
    .local v1, cliptray:Landroid/webkit/LGCliptrayManager;
    if-eqz v1, :cond_32

    #@2f
    .line 6353
    invoke-virtual {v1, v0}, Landroid/webkit/LGCliptrayManager;->copyToCliptray(Landroid/content/ClipData;)V

    #@32
    .line 6366
    .end local v0           #clip:Landroid/content/ClipData;
    .end local v1           #cliptray:Landroid/webkit/LGCliptrayManager;
    .end local v4           #settings:Landroid/webkit/WebSettings;
    :cond_32
    :goto_32
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@34
    if-eqz v6, :cond_4e

    #@36
    .line 6367
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@38
    const-string v7, ""

    #@3a
    invoke-virtual {v6, v7}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->replaceSelection(Ljava/lang/CharSequence;)V

    #@3d
    goto :goto_1c

    #@3e
    .line 6356
    .restart local v0       #clip:Landroid/content/ClipData;
    .restart local v4       #settings:Landroid/webkit/WebSettings;
    :cond_3e
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@40
    const-string v7, "clipboard"

    #@42
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@45
    move-result-object v2

    #@46
    check-cast v2, Landroid/content/ClipboardManager;

    #@48
    .line 6358
    .local v2, cm:Landroid/content/ClipboardManager;
    if-eqz v2, :cond_32

    #@4a
    .line 6359
    invoke-virtual {v2, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@4d
    goto :goto_32

    #@4e
    .line 6370
    .end local v0           #clip:Landroid/content/ClipData;
    .end local v2           #cm:Landroid/content/ClipboardManager;
    .end local v4           #settings:Landroid/webkit/WebSettings;
    :cond_4e
    const/4 v6, 0x4

    #@4f
    new-array v3, v6, [I

    #@51
    .line 6371
    .local v3, handles:[I
    invoke-direct {p0, v3}, Landroid/webkit/WebViewClassic;->getSelectionHandles([I)V

    #@54
    .line 6372
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@56
    const/16 v7, 0xd3

    #@58
    invoke-virtual {v6, v7, v3}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@5b
    goto :goto_1c
.end method

.method deleteSelection(II)V
    .registers 8
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 5515
    iget v1, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@3
    add-int/lit8 v1, v1, 0x1

    #@5
    iput v1, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@7
    .line 5516
    new-instance v0, Landroid/webkit/WebViewCore$TextSelectionData;

    #@9
    invoke-direct {v0, p1, p2, v4}, Landroid/webkit/WebViewCore$TextSelectionData;-><init>(III)V

    #@c
    .line 5518
    .local v0, data:Landroid/webkit/WebViewCore$TextSelectionData;
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@e
    const/16 v2, 0x7a

    #@10
    iget v3, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@12
    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/webkit/WebViewCore;->sendMessage(IIILjava/lang/Object;)V

    #@15
    .line 5520
    return-void
.end method

.method public destroy()V
    .registers 3

    #@0
    .prologue
    .line 2580
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->waitForUiThreadSync()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 2581
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@a
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->notifyForUiThread()V

    #@d
    .line 2586
    :cond_d
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHTML5VideoViewProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@f
    if-eqz v0, :cond_17

    #@11
    .line 2587
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHTML5VideoViewProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@13
    const/4 v1, 0x0

    #@14
    invoke-virtual {v0, v1}, Landroid/webkit/HTML5VideoViewProxy;->setBaseLayer(I)V

    #@17
    .line 2591
    :cond_17
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@19
    invoke-virtual {v0}, Landroid/webkit/WebView;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@1c
    move-result-object v0

    #@1d
    if-eqz v0, :cond_27

    #@1f
    .line 2592
    const-string/jumbo v0, "webview"

    #@22
    const-string v1, "Error: WebView.destroy() called while still attached!"

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 2594
    :cond_27
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->ensureFunctorDetached()V

    #@2a
    .line 2595
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->destroyJava()V

    #@2d
    .line 2596
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->destroyNative()V

    #@30
    .line 2597
    return-void
.end method

.method public discardAllTextures()V
    .registers 1

    #@0
    .prologue
    .line 10376
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->nativeDiscardAllTextures()V

    #@3
    .line 10377
    return-void
.end method

.method dismissZoomControl()V
    .registers 2

    #@0
    .prologue
    .line 8409
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->dismissZoomPicker()V

    #@5
    .line 8410
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 6756
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@3
    move-result v1

    #@4
    packed-switch v1, :pswitch_data_36

    #@7
    .line 6780
    :goto_7
    :pswitch_7
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@9
    invoke-virtual {v1, p1}, Landroid/webkit/WebView$PrivateAccess;->super_dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@c
    move-result v1

    #@d
    :goto_d
    return v1

    #@e
    .line 6758
    :pswitch_e
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mKeysPressed:Ljava/util/Vector;

    #@10
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@13
    move-result v2

    #@14
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@1b
    goto :goto_7

    #@1c
    .line 6764
    :pswitch_1c
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mKeysPressed:Ljava/util/Vector;

    #@1e
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@21
    move-result v2

    #@22
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v1, v2}, Ljava/util/Vector;->indexOf(Ljava/lang/Object;)I

    #@29
    move-result v0

    #@2a
    .line 6765
    .local v0, location:I
    const/4 v1, -0x1

    #@2b
    if-ne v0, v1, :cond_2f

    #@2d
    .line 6768
    const/4 v1, 0x0

    #@2e
    goto :goto_d

    #@2f
    .line 6772
    :cond_2f
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mKeysPressed:Ljava/util/Vector;

    #@31
    invoke-virtual {v1, v0}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    #@34
    goto :goto_7

    #@35
    .line 6756
    nop

    #@36
    :pswitch_data_36
    .packed-switch 0x0
        :pswitch_e
        :pswitch_1c
        :pswitch_7
    .end packed-switch
.end method

.method public documentAsText(Landroid/os/Message;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 4716
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0xa1

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@7
    .line 4717
    return-void
.end method

.method public documentHasImages(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 4350
    if-nez p1, :cond_3

    #@2
    .line 4354
    :goto_2
    return-void

    #@3
    .line 4353
    :cond_3
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@5
    const/16 v1, 0x78

    #@7
    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@a
    goto :goto_2
.end method

.method drawHistory()Z
    .registers 2

    #@0
    .prologue
    .line 5481
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@2
    return v0
.end method

.method public dumpDisplayTree()V
    .registers 2

    #@0
    .prologue
    .line 5672
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getUrl()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->nativeDumpDisplayTree(Ljava/lang/String;)V

    #@7
    .line 5673
    return-void
.end method

.method public dumpDomTree(Z)V
    .registers 6
    .parameter "toFile"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 5682
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@3
    const/16 v3, 0xaa

    #@5
    if-eqz p1, :cond_c

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    invoke-virtual {v2, v3, v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@b
    .line 5683
    return-void

    #@c
    :cond_c
    move v0, v1

    #@d
    .line 5682
    goto :goto_8
.end method

.method public dumpRenderTree(Z)V
    .registers 6
    .parameter "toFile"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 5692
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@3
    const/16 v3, 0xab

    #@5
    if-eqz p1, :cond_c

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    invoke-virtual {v2, v3, v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@b
    .line 5693
    return-void

    #@c
    :cond_c
    move v0, v1

    #@d
    .line 5692
    goto :goto_8
.end method

.method public dumpViewHierarchyWithProperties(Ljava/io/BufferedWriter;I)V
    .registers 8
    .parameter "out"
    .parameter "level"

    #@0
    .prologue
    .line 10463
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getBaseLayer()I

    #@3
    move-result v1

    #@4
    .line 10464
    .local v1, layer:I
    if-eqz v1, :cond_1f

    #@6
    .line 10466
    :try_start_6
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    #@8
    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@b
    .line 10467
    .local v2, stream:Ljava/io/ByteArrayOutputStream;
    invoke-static {v1, v2, p2}, Landroid/webkit/ViewStateSerializer;->dumpLayerHierarchy(ILjava/io/OutputStream;I)V

    #@e
    .line 10468
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    #@11
    .line 10469
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@14
    move-result-object v0

    #@15
    .line 10470
    .local v0, buf:[B
    new-instance v3, Ljava/lang/String;

    #@17
    const-string v4, "ascii"

    #@19
    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    #@1c
    invoke-virtual {p1, v3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_1f} :catch_20

    #@1f
    .line 10473
    .end local v0           #buf:[B
    .end local v2           #stream:Ljava/io/ByteArrayOutputStream;
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 10471
    :catch_20
    move-exception v3

    #@21
    goto :goto_1f
.end method

.method public externalRepresentation(Landroid/os/Message;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 4711
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0xa0

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@7
    .line 4712
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 4787
    :try_start_0
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->destroy()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    #@3
    .line 4789
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 4791
    return-void

    #@7
    .line 4789
    :catchall_7
    move-exception v0

    #@8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@b
    throw v0
.end method

.method public findAll(Ljava/lang/String;)I
    .registers 3
    .parameter "find"

    #@0
    .prologue
    .line 4160
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/webkit/WebViewClassic;->findAllBody(Ljava/lang/String;Z)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public findAllAsync(Ljava/lang/String;)V
    .registers 3
    .parameter "find"

    #@0
    .prologue
    .line 4165
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Landroid/webkit/WebViewClassic;->findAllBody(Ljava/lang/String;Z)I

    #@4
    .line 4166
    return-void
.end method

.method public findHierarchyView(Ljava/lang/String;I)Landroid/view/View;
    .registers 7
    .parameter "className"
    .parameter "hashCode"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 10477
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@3
    if-nez v2, :cond_6

    #@5
    .line 10482
    :cond_5
    :goto_5
    return-object v1

    #@6
    .line 10478
    :cond_6
    new-instance v0, Landroid/graphics/Picture;

    #@8
    invoke-direct {v0}, Landroid/graphics/Picture;-><init>()V

    #@b
    .line 10479
    .local v0, pic:Landroid/graphics/Picture;
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@d
    invoke-direct {p0, v2, p1, p2, v0}, Landroid/webkit/WebViewClassic;->nativeDumpLayerContentToPicture(ILjava/lang/String;ILandroid/graphics/Picture;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_5

    #@13
    .line 10482
    new-instance v1, Landroid/webkit/WebViewClassic$PictureWrapperView;

    #@15
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@18
    move-result-object v2

    #@19
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@1b
    invoke-direct {v1, v2, v0, v3}, Landroid/webkit/WebViewClassic$PictureWrapperView;-><init>(Landroid/content/Context;Landroid/graphics/Picture;Landroid/webkit/WebView;)V

    #@1e
    goto :goto_5
.end method

.method public findNext(Z)V
    .registers 6
    .parameter "forward"

    #@0
    .prologue
    .line 4149
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v0, :cond_5

    #@4
    .line 4153
    :cond_4
    :goto_4
    return-void

    #@5
    .line 4150
    :cond_5
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@7
    if-eqz v0, :cond_4

    #@9
    .line 4151
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@b
    const/16 v2, 0xde

    #@d
    if-eqz p1, :cond_16

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@12
    invoke-virtual {v1, v2, v0, v3}, Landroid/webkit/WebViewCore;->sendMessage(IILjava/lang/Object;)V

    #@15
    goto :goto_4

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_10
.end method

.method public flingScroll(II)V
    .registers 14
    .parameter "vx"
    .parameter "vy"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 8155
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@3
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@6
    move-result v1

    #@7
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@a
    move-result v2

    #@b
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollX()I

    #@e
    move-result v6

    #@f
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollY()I

    #@12
    move-result v8

    #@13
    iget v9, p0, Landroid/webkit/WebViewClassic;->mOverflingDistance:I

    #@15
    iget v10, p0, Landroid/webkit/WebViewClassic;->mOverflingDistance:I

    #@17
    move v3, p1

    #@18
    move v4, p2

    #@19
    move v7, v5

    #@1a
    invoke-virtual/range {v0 .. v10}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    #@1d
    .line 8157
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@20
    .line 8158
    return-void
.end method

.method focusCandidateIsEditableText()Z
    .registers 2

    #@0
    .prologue
    .line 10445
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 10446
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@6
    iget-boolean v0, v0, Landroid/webkit/WebViewCore$WebKitHitTest;->mEditable:Z

    #@8
    .line 10448
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public freeMemory()V
    .registers 3

    #@0
    .prologue
    .line 4085
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0x91

    #@4
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@7
    .line 4086
    return-void
.end method

.method getBaseLayer()I
    .registers 2

    #@0
    .prologue
    .line 5208
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v0, :cond_6

    #@4
    .line 5209
    const/4 v0, 0x0

    #@5
    .line 5211
    :goto_5
    return v0

    #@6
    :cond_6
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@8
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->nativeGetBaseLayer(I)I

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method getBlockLeftEdge(IIF)I
    .registers 12
    .parameter "x"
    .parameter "y"
    .parameter "readingScale"

    #@0
    .prologue
    .line 3383
    const/high16 v6, 0x3f80

    #@2
    div-float v1, v6, p3

    #@4
    .line 3384
    .local v1, invReadingScale:F
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@7
    move-result v6

    #@8
    int-to-float v6, v6

    #@9
    mul-float/2addr v6, v1

    #@a
    float-to-int v4, v6

    #@b
    .line 3385
    .local v4, readingWidth:I
    const/4 v2, -0x1

    #@c
    .line 3386
    .local v2, left:I
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@e
    if-eqz v6, :cond_31

    #@10
    .line 3387
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@12
    iget-object v6, v6, Landroid/webkit/WebViewCore$WebKitHitTest;->mEnclosingParentRects:[Landroid/graphics/Rect;

    #@14
    array-length v3, v6

    #@15
    .line 3388
    .local v3, length:I
    const/4 v0, 0x0

    #@16
    .local v0, i:I
    :goto_16
    if-ge v0, v3, :cond_31

    #@18
    .line 3389
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@1a
    iget-object v6, v6, Landroid/webkit/WebViewCore$WebKitHitTest;->mEnclosingParentRects:[Landroid/graphics/Rect;

    #@1c
    aget-object v5, v6, v0

    #@1e
    .line 3390
    .local v5, rect:Landroid/graphics/Rect;
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    #@21
    move-result v6

    #@22
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@24
    iget v7, v7, Landroid/webkit/WebViewCore$WebKitHitTest;->mHitTestSlop:I

    #@26
    if-ge v6, v7, :cond_2b

    #@28
    .line 3388
    :goto_28
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_16

    #@2b
    .line 3393
    :cond_2b
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    #@2e
    move-result v6

    #@2f
    if-le v6, v4, :cond_32

    #@31
    .line 3403
    .end local v0           #i:I
    .end local v3           #length:I
    .end local v5           #rect:Landroid/graphics/Rect;
    :cond_31
    return v2

    #@32
    .line 3399
    .restart local v0       #i:I
    .restart local v3       #length:I
    .restart local v5       #rect:Landroid/graphics/Rect;
    :cond_32
    iget v2, v5, Landroid/graphics/Rect;->left:I

    #@34
    goto :goto_28
.end method

.method public getCertificate()Landroid/net/http/SslCertificate;
    .registers 2

    #@0
    .prologue
    .line 2479
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCertificate:Landroid/net/http/SslCertificate;

    #@2
    return-object v0
.end method

.method public getContentHeight()I
    .registers 2

    #@0
    .prologue
    .line 3973
    iget v0, p0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@2
    return v0
.end method

.method public getContentWidth()I
    .registers 2

    #@0
    .prologue
    .line 3981
    iget v0, p0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@2
    return v0
.end method

.method getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 1853
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method getDefaultZoomScale()F
    .registers 2

    #@0
    .prologue
    .line 8413
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->getDefaultScale()F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getFavicon()Landroid/graphics/Bitmap;
    .registers 3

    #@0
    .prologue
    .line 3947
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v1}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@9
    move-result-object v0

    #@a
    .line 3948
    .local v0, h:Landroid/webkit/WebHistoryItem;
    if-eqz v0, :cond_11

    #@c
    invoke-virtual {v0}, Landroid/webkit/WebHistoryItemClassic;->getFavicon()Landroid/graphics/Bitmap;

    #@f
    move-result-object v1

    #@10
    :goto_10
    return-object v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method public getFlickValue(II)I
    .registers 24
    .parameter "initialXVelocity"
    .parameter "vectorVelocity"

    #@0
    .prologue
    .line 8165
    sget v18, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@2
    add-int/lit8 v18, v18, -0x1

    #@4
    sput v18, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@6
    .line 8167
    move/from16 v0, p2

    #@8
    int-to-float v0, v0

    #@9
    move/from16 v18, v0

    #@b
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    #@e
    move-result v18

    #@f
    move/from16 v0, v18

    #@11
    float-to-int v15, v0

    #@12
    .line 8168
    .local v15, signal:I
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@15
    move-result-object v18

    #@16
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@19
    move-result-object v18

    #@1a
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1d
    move-result-object v18

    #@1e
    move-object/from16 v0, v18

    #@20
    iget v13, v0, Landroid/util/DisplayMetrics;->density:F

    #@22
    .line 8171
    .local v13, mDensityScale:F
    const/4 v6, 0x0

    #@23
    .line 8172
    .local v6, directionUPCount:I
    const/4 v5, 0x0

    #@24
    .line 8173
    .local v5, directionDownCount:I
    const/16 v17, 0x0

    #@26
    .line 8174
    .local v17, up:Z
    const/4 v7, 0x0

    #@27
    .line 8178
    .local v7, down:Z
    sget v18, Landroid/webkit/WebViewClassic;->moveCounter:I

    #@29
    sget v19, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@2b
    move/from16 v0, v18

    #@2d
    move/from16 v1, v19

    #@2f
    if-lt v0, v1, :cond_93

    #@31
    sget v2, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@33
    .line 8181
    .local v2, arrayCount:I
    :goto_33
    sget v18, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@35
    add-int/lit8 v8, v18, -0x1

    #@37
    .line 8183
    .local v8, forBitOperation:I
    const/4 v14, 0x0

    #@38
    .line 8184
    .local v14, rememberFirstSignal:I
    const/4 v4, 0x0

    #@39
    .line 8185
    .local v4, calcFactor:I
    sget v18, Landroid/webkit/WebViewClassic;->moveCounter:I

    #@3b
    sget v19, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@3d
    move/from16 v0, v18

    #@3f
    move/from16 v1, v19

    #@41
    if-le v0, v1, :cond_47

    #@43
    .line 8188
    sget v18, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@45
    add-int v4, v18, v2

    #@47
    .line 8191
    :cond_47
    const/4 v12, 0x0

    #@48
    .line 8192
    .local v12, keepSignal:Z
    const/4 v9, 0x0

    #@49
    .line 8193
    .local v9, gotSignal:I
    const/16 v16, 0x0

    #@4b
    .line 8194
    .local v16, sumDeltaY:I
    const/4 v11, 0x0

    #@4c
    .line 8204
    .local v11, ifFindFirstSignal:Z
    const/4 v10, 0x1

    #@4d
    .local v10, i:I
    :goto_4d
    if-gt v10, v2, :cond_e3

    #@4f
    .line 8206
    if-nez v4, :cond_96

    #@51
    .line 8207
    add-int/lit8 v3, v10, -0x1

    #@53
    .line 8213
    .local v3, arrayIndex:I
    :goto_53
    move-object/from16 v0, p0

    #@55
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->coorDirection:[I

    #@57
    move-object/from16 v18, v0

    #@59
    and-int v19, v3, v8

    #@5b
    aget v9, v18, v19

    #@5d
    .line 8214
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    #@60
    move-result v18

    #@61
    add-int v16, v16, v18

    #@63
    .line 8216
    int-to-float v0, v9

    #@64
    move/from16 v18, v0

    #@66
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    #@69
    move-result v18

    #@6a
    const/16 v19, 0x0

    #@6c
    cmpg-float v18, v18, v19

    #@6e
    if-gez v18, :cond_99

    #@70
    add-int/lit8 v5, v5, 0x1

    #@72
    .line 8219
    :cond_72
    :goto_72
    if-eqz v9, :cond_ae

    #@74
    if-nez v11, :cond_ae

    #@76
    .line 8220
    const/4 v11, 0x1

    #@77
    .line 8221
    move v14, v9

    #@78
    .line 8223
    if-gez v14, :cond_a9

    #@7a
    const/4 v7, 0x1

    #@7b
    .line 8230
    :cond_7b
    :goto_7b
    if-ne v10, v2, :cond_90

    #@7d
    .line 8231
    div-int v18, v16, v2

    #@7f
    const/high16 v19, 0x40a0

    #@81
    mul-float v19, v19, v13

    #@83
    move/from16 v0, v19

    #@85
    float-to-int v0, v0

    #@86
    move/from16 v19, v0

    #@88
    move/from16 v0, v18

    #@8a
    move/from16 v1, v19

    #@8c
    if-gt v0, v1, :cond_90

    #@8e
    .line 8232
    move/from16 p1, p2

    #@90
    .line 8204
    :cond_90
    add-int/lit8 v10, v10, 0x1

    #@92
    goto :goto_4d

    #@93
    .line 8178
    .end local v2           #arrayCount:I
    .end local v3           #arrayIndex:I
    .end local v4           #calcFactor:I
    .end local v8           #forBitOperation:I
    .end local v9           #gotSignal:I
    .end local v10           #i:I
    .end local v11           #ifFindFirstSignal:Z
    .end local v12           #keepSignal:Z
    .end local v14           #rememberFirstSignal:I
    .end local v16           #sumDeltaY:I
    :cond_93
    sget v2, Landroid/webkit/WebViewClassic;->moveCounter:I

    #@95
    goto :goto_33

    #@96
    .line 8209
    .restart local v2       #arrayCount:I
    .restart local v4       #calcFactor:I
    .restart local v8       #forBitOperation:I
    .restart local v9       #gotSignal:I
    .restart local v10       #i:I
    .restart local v11       #ifFindFirstSignal:Z
    .restart local v12       #keepSignal:Z
    .restart local v14       #rememberFirstSignal:I
    .restart local v16       #sumDeltaY:I
    :cond_96
    add-int v3, v4, v10

    #@98
    .restart local v3       #arrayIndex:I
    goto :goto_53

    #@99
    .line 8217
    :cond_99
    int-to-float v0, v9

    #@9a
    move/from16 v18, v0

    #@9c
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    #@9f
    move-result v18

    #@a0
    const/16 v19, 0x0

    #@a2
    cmpl-float v18, v18, v19

    #@a4
    if-lez v18, :cond_72

    #@a6
    add-int/lit8 v6, v6, 0x1

    #@a8
    goto :goto_72

    #@a9
    .line 8224
    :cond_a9
    if-lez v14, :cond_7b

    #@ab
    const/16 v17, 0x1

    #@ad
    goto :goto_7b

    #@ae
    .line 8225
    :cond_ae
    if-eqz v9, :cond_7b

    #@b0
    int-to-float v0, v9

    #@b1
    move/from16 v18, v0

    #@b3
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    #@b6
    move-result v18

    #@b7
    move/from16 v0, v18

    #@b9
    float-to-int v0, v0

    #@ba
    move/from16 v18, v0

    #@bc
    int-to-float v0, v14

    #@bd
    move/from16 v19, v0

    #@bf
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->signum(F)F

    #@c2
    move-result v19

    #@c3
    move/from16 v0, v19

    #@c5
    float-to-int v0, v0

    #@c6
    move/from16 v19, v0

    #@c8
    move/from16 v0, v18

    #@ca
    move/from16 v1, v19

    #@cc
    if-eq v0, v1, :cond_7b

    #@ce
    .line 8226
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    #@d1
    move-result v18

    #@d2
    const/high16 v19, 0x4150

    #@d4
    mul-float v19, v19, v13

    #@d6
    move/from16 v0, v19

    #@d8
    float-to-int v0, v0

    #@d9
    move/from16 v19, v0

    #@db
    move/from16 v0, v18

    #@dd
    move/from16 v1, v19

    #@df
    if-lt v0, v1, :cond_7b

    #@e1
    .line 8227
    const/4 v12, 0x1

    #@e2
    goto :goto_7b

    #@e3
    .line 8242
    .end local v3           #arrayIndex:I
    :cond_e3
    if-nez v12, :cond_ed

    #@e5
    .line 8243
    if-le v5, v6, :cond_137

    #@e7
    .line 8244
    if-eqz p2, :cond_10d

    #@e9
    if-gez v15, :cond_10d

    #@eb
    .line 8245
    mul-int/lit8 p2, p2, -0x1

    #@ed
    .line 8263
    :cond_ed
    :goto_ed
    const/16 v18, 0x0

    #@ef
    sput v18, Landroid/webkit/WebViewClassic;->eventMonitor:I

    #@f1
    .line 8264
    const/16 v18, 0x0

    #@f3
    sput v18, Landroid/webkit/WebViewClassic;->moveCounter:I

    #@f5
    .line 8265
    const/16 v18, 0x0

    #@f7
    sput-boolean v18, Landroid/webkit/WebViewClassic;->dropFirstDeltaY:Z

    #@f9
    .line 8266
    const/4 v10, 0x0

    #@fa
    :goto_fa
    sget v18, Landroid/webkit/WebViewClassic;->COORDINATE_DIRECTION:I

    #@fc
    move/from16 v0, v18

    #@fe
    if-ge v10, v0, :cond_17a

    #@100
    .line 8267
    move-object/from16 v0, p0

    #@102
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->coorDirection:[I

    #@104
    move-object/from16 v18, v0

    #@106
    const/16 v19, 0x0

    #@108
    aput v19, v18, v10

    #@10a
    .line 8266
    add-int/lit8 v10, v10, 0x1

    #@10c
    goto :goto_fa

    #@10d
    .line 8247
    :cond_10d
    const-string/jumbo v18, "webview"

    #@110
    new-instance v19, Ljava/lang/StringBuilder;

    #@112
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v20, "CAPP_TOUCH_FLICK: DOWN vectorVelocity:"

    #@117
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v19

    #@11b
    move-object/from16 v0, v19

    #@11d
    move/from16 v1, p2

    #@11f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@122
    move-result-object v19

    #@123
    const-string v20, " signal:"

    #@125
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v19

    #@129
    move-object/from16 v0, v19

    #@12b
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v19

    #@12f
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@132
    move-result-object v19

    #@133
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@136
    goto :goto_ed

    #@137
    .line 8249
    :cond_137
    if-ge v5, v6, :cond_16a

    #@139
    .line 8250
    if-eqz p2, :cond_140

    #@13b
    if-lez v15, :cond_140

    #@13d
    .line 8251
    mul-int/lit8 p2, p2, -0x1

    #@13f
    goto :goto_ed

    #@140
    .line 8253
    :cond_140
    const-string/jumbo v18, "webview"

    #@143
    new-instance v19, Ljava/lang/StringBuilder;

    #@145
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@148
    const-string v20, "CAPP_TOUCH_FLICK: UP vectorVelocity:"

    #@14a
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v19

    #@14e
    move-object/from16 v0, v19

    #@150
    move/from16 v1, p2

    #@152
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@155
    move-result-object v19

    #@156
    const-string v20, " signal:"

    #@158
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v19

    #@15c
    move-object/from16 v0, v19

    #@15e
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@161
    move-result-object v19

    #@162
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@165
    move-result-object v19

    #@166
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@169
    goto :goto_ed

    #@16a
    .line 8256
    :cond_16a
    if-eqz v7, :cond_172

    #@16c
    if-gez v15, :cond_172

    #@16e
    .line 8257
    mul-int/lit8 p2, p2, -0x1

    #@170
    goto/16 :goto_ed

    #@172
    .line 8258
    :cond_172
    if-eqz v17, :cond_ed

    #@174
    if-lez v15, :cond_ed

    #@176
    .line 8259
    mul-int/lit8 p2, p2, -0x1

    #@178
    goto/16 :goto_ed

    #@17a
    .line 8269
    :cond_17a
    return p2
.end method

.method getHeight()I
    .registers 2

    #@0
    .prologue
    .line 1849
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebView;->getHeight()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getHistoryPictureWidth()I
    .registers 2

    #@0
    .prologue
    .line 5485
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHistoryPicture:Landroid/graphics/Picture;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHistoryPicture:Landroid/graphics/Picture;

    #@6
    invoke-virtual {v0}, Landroid/graphics/Picture;->getWidth()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getHitTestResult()Landroid/webkit/WebView$HitTestResult;
    .registers 2

    #@0
    .prologue
    .line 3376
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@2
    return-object v0
.end method

.method public getHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 4
    .parameter "host"
    .parameter "realm"

    #@0
    .prologue
    .line 2520
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mDatabase:Landroid/webkit/WebViewDatabaseClassic;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewDatabaseClassic;->getHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMaxZoomScale()F
    .registers 2

    #@0
    .prologue
    .line 8486
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->getMaxZoomScale()F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMinZoomScale()F
    .registers 2

    #@0
    .prologue
    .line 8495
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->getMinZoomScale()F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getOriginalUrl()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 3929
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v1}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@9
    move-result-object v0

    #@a
    .line 3930
    .local v0, h:Landroid/webkit/WebHistoryItem;
    if-eqz v0, :cond_11

    #@c
    invoke-virtual {v0}, Landroid/webkit/WebHistoryItemClassic;->getOriginalUrl()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    :goto_10
    return-object v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method public getPageBackgroundColor()I
    .registers 2

    #@0
    .prologue
    .line 3985
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, -0x1

    #@5
    .line 3986
    :goto_5
    return v0

    #@6
    :cond_6
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@8
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->nativeGetBackgroundColor(I)I

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public getParagraphRect()Landroid/graphics/Rect;
    .registers 7

    #@0
    .prologue
    .line 6416
    new-instance v0, Landroid/graphics/Rect;

    #@2
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@4
    iget v1, v1, Landroid/graphics/Point;->x:I

    #@6
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@9
    move-result v1

    #@a
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@d
    move-result v2

    #@e
    sub-int/2addr v1, v2

    #@f
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@11
    iget v2, v2, Landroid/graphics/Point;->y:I

    #@13
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@16
    move-result v2

    #@17
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@1a
    move-result v3

    #@1b
    sub-int/2addr v2, v3

    #@1c
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@1e
    iget v3, v3, Landroid/graphics/Point;->x:I

    #@20
    invoke-virtual {p0, v3}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@23
    move-result v3

    #@24
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@27
    move-result v4

    #@28
    sub-int/2addr v3, v4

    #@29
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@2b
    iget v4, v4, Landroid/graphics/Point;->y:I

    #@2d
    invoke-virtual {p0, v4}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@30
    move-result v4

    #@31
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@34
    move-result v5

    #@35
    sub-int/2addr v4, v5

    #@36
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    #@39
    return-object v0
.end method

.method public getParagraphScreenShot()Landroid/graphics/Bitmap;
    .registers 6

    #@0
    .prologue
    .line 6268
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@2
    if-nez v1, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 6280
    :goto_5
    return-object v0

    #@6
    .line 6270
    :cond_6
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@8
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@a
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@c
    invoke-direct {p0, v1, v2, v3}, Landroid/webkit/WebViewClassic;->nativeGetSelectionParagraphBounds(ILandroid/graphics/Point;Landroid/graphics/Point;)Z

    #@f
    .line 6274
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@11
    iget v1, v1, Landroid/graphics/Point;->x:I

    #@13
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@15
    iget v2, v2, Landroid/graphics/Point;->y:I

    #@17
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@19
    iget v3, v3, Landroid/graphics/Point;->x:I

    #@1b
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@1d
    iget v4, v4, Landroid/graphics/Point;->y:I

    #@1f
    invoke-direct {p0, v1, v2, v3, v4}, Landroid/webkit/WebViewClassic;->getScreenShot(IIII)Landroid/graphics/Bitmap;

    #@22
    move-result-object v0

    #@23
    .line 6280
    .local v0, image:Landroid/graphics/Bitmap;
    goto :goto_5
.end method

.method public getProgress()I
    .registers 2

    #@0
    .prologue
    .line 3965
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->getProgress()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getScale()F
    .registers 2

    #@0
    .prologue
    .line 3338
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->getScale()F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getScaledNavSlop()I
    .registers 2

    #@0
    .prologue
    .line 2253
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@2
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentDimension(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getScrollDelegate()Landroid/webkit/WebViewProvider$ScrollDelegate;
    .registers 1

    #@0
    .prologue
    .line 1828
    return-object p0
.end method

.method getScrollX()I
    .registers 2

    #@0
    .prologue
    .line 1837
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebView;->getScrollX()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getScrollY()I
    .registers 2

    #@0
    .prologue
    .line 1841
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebView;->getScrollY()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getSelection()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 6427
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v0, :cond_7

    #@4
    const-string v0, ""

    #@6
    .line 6428
    :goto_6
    return-object v0

    #@7
    :cond_7
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->nativeGetSelection()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    goto :goto_6
.end method

.method getSelectionRegion()Landroid/graphics/Rect;
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x3

    #@1
    const/4 v11, 0x2

    #@2
    const/4 v10, 0x1

    #@3
    const/4 v9, 0x0

    #@4
    .line 10519
    new-instance v1, Landroid/graphics/Rect;

    #@6
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@9
    .line 10521
    .local v1, rect:Landroid/graphics/Rect;
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->ensureSelectionHandles()V

    #@c
    .line 10522
    iget-boolean v4, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@e
    if-eqz v4, :cond_af

    #@10
    .line 10523
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@13
    move-result v2

    #@14
    .line 10524
    .local v2, scrollX:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@17
    move-result v3

    #@18
    .line 10525
    .local v3, scrollY:I
    const/4 v4, 0x4

    #@19
    new-array v0, v4, [I

    #@1b
    .line 10526
    .local v0, handles:[I
    iget v4, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@1d
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@1f
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@21
    invoke-direct {p0, v4, v5, v6}, Landroid/webkit/WebViewClassic;->nativeGetSelectionParagraphBounds(ILandroid/graphics/Point;Landroid/graphics/Point;)Z

    #@24
    .line 10527
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@26
    iget v4, v4, Landroid/graphics/Point;->x:I

    #@28
    aput v4, v0, v9

    #@2a
    .line 10528
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@2c
    iget v4, v4, Landroid/graphics/Point;->y:I

    #@2e
    aput v4, v0, v10

    #@30
    .line 10529
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@32
    iget v4, v4, Landroid/graphics/Point;->x:I

    #@34
    aput v4, v0, v11

    #@36
    .line 10530
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@38
    iget v4, v4, Landroid/graphics/Point;->y:I

    #@3a
    aput v4, v0, v12

    #@3c
    .line 10531
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@3e
    invoke-virtual {v4}, Landroid/webkit/WebViewCore;->inParagraphMode()Z

    #@41
    move-result v4

    #@42
    if-eqz v4, :cond_b0

    #@44
    .line 10532
    aget v4, v0, v9

    #@46
    invoke-virtual {p0, v4}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@49
    move-result v4

    #@4a
    sub-int/2addr v4, v2

    #@4b
    aget v5, v0, v10

    #@4d
    invoke-virtual {p0, v5}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@50
    move-result v5

    #@51
    sub-int/2addr v5, v3

    #@52
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@54
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@57
    move-result v6

    #@58
    mul-int/lit8 v6, v6, 0x3

    #@5a
    div-int/lit8 v6, v6, 0x2

    #@5c
    sub-int/2addr v5, v6

    #@5d
    aget v6, v0, v11

    #@5f
    invoke-virtual {p0, v6}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@62
    move-result v6

    #@63
    sub-int/2addr v6, v2

    #@64
    aget v7, v0, v12

    #@66
    invoke-virtual {p0, v7}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@69
    move-result v7

    #@6a
    sub-int/2addr v7, v3

    #@6b
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@6d
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@70
    move-result v8

    #@71
    add-int/2addr v7, v8

    #@72
    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    #@75
    .line 10547
    :goto_75
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    #@78
    move-result v4

    #@79
    if-eqz v4, :cond_af

    #@7b
    .line 10548
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->getSelectionHandles([I)V

    #@7e
    .line 10549
    aget v4, v0, v9

    #@80
    invoke-virtual {p0, v4}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@83
    move-result v4

    #@84
    sub-int/2addr v4, v2

    #@85
    aget v5, v0, v10

    #@87
    invoke-virtual {p0, v5}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@8a
    move-result v5

    #@8b
    sub-int/2addr v5, v3

    #@8c
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@8e
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@91
    move-result v6

    #@92
    sub-int/2addr v5, v6

    #@93
    aget v6, v0, v11

    #@95
    invoke-virtual {p0, v6}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@98
    move-result v6

    #@99
    sub-int/2addr v6, v2

    #@9a
    aget v7, v0, v12

    #@9c
    invoke-virtual {p0, v7}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@9f
    move-result v7

    #@a0
    sub-int/2addr v7, v3

    #@a1
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@a3
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@a6
    move-result v8

    #@a7
    mul-int/lit8 v8, v8, 0x3

    #@a9
    div-int/lit8 v8, v8, 0x2

    #@ab
    add-int/2addr v7, v8

    #@ac
    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    #@af
    .line 10557
    .end local v0           #handles:[I
    .end local v2           #scrollX:I
    .end local v3           #scrollY:I
    :cond_af
    return-object v1

    #@b0
    .line 10539
    .restart local v0       #handles:[I
    .restart local v2       #scrollX:I
    .restart local v3       #scrollY:I
    :cond_b0
    aget v4, v0, v9

    #@b2
    invoke-virtual {p0, v4}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@b5
    move-result v4

    #@b6
    sub-int/2addr v4, v2

    #@b7
    aget v5, v0, v10

    #@b9
    invoke-virtual {p0, v5}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@bc
    move-result v5

    #@bd
    sub-int/2addr v5, v3

    #@be
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@c0
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@c3
    move-result v6

    #@c4
    sub-int/2addr v5, v6

    #@c5
    aget v6, v0, v11

    #@c7
    invoke-virtual {p0, v6}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@ca
    move-result v6

    #@cb
    sub-int/2addr v6, v2

    #@cc
    aget v7, v0, v12

    #@ce
    invoke-virtual {p0, v7}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@d1
    move-result v7

    #@d2
    sub-int/2addr v7, v3

    #@d3
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@d5
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@d8
    move-result v8

    #@d9
    mul-int/lit8 v8, v8, 0x3

    #@db
    div-int/lit8 v8, v8, 0x2

    #@dd
    add-int/2addr v7, v8

    #@de
    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    #@e1
    goto :goto_75
.end method

.method public bridge synthetic getSettings()Landroid/webkit/WebSettings;
    .registers 2

    #@0
    .prologue
    .line 189
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getSettings()Landroid/webkit/WebSettingsClassic;
    .registers 2

    #@0
    .prologue
    .line 4762
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getTitle()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 3938
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v1}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@9
    move-result-object v0

    #@a
    .line 3939
    .local v0, h:Landroid/webkit/WebHistoryItem;
    if-eqz v0, :cond_11

    #@c
    invoke-virtual {v0}, Landroid/webkit/WebHistoryItemClassic;->getTitle()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    :goto_10
    return-object v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method protected getTitleHeight()I
    .registers 2

    #@0
    .prologue
    .line 2420
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    instance-of v0, v0, Landroid/webkit/WebViewClassic$TitleBarDelegate;

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 2421
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@8
    check-cast v0, Landroid/webkit/WebViewClassic$TitleBarDelegate;

    #@a
    invoke-interface {v0}, Landroid/webkit/WebViewClassic$TitleBarDelegate;->getTitleHeight()I

    #@d
    move-result v0

    #@e
    .line 2423
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public getTouchIconUrl()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 3956
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v1}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@9
    move-result-object v0

    #@a
    .line 3957
    .local v0, h:Landroid/webkit/WebHistoryItemClassic;
    if-eqz v0, :cond_11

    #@c
    invoke-virtual {v0}, Landroid/webkit/WebHistoryItemClassic;->getTouchIconUrl()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    :goto_10
    return-object v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method public getTranslateMode()Z
    .registers 2

    #@0
    .prologue
    .line 6403
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mTranslateMode:Z

    #@2
    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 3920
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v1}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@9
    move-result-object v0

    #@a
    .line 3921
    .local v0, h:Landroid/webkit/WebHistoryItem;
    if-eqz v0, :cond_11

    #@c
    invoke-virtual {v0}, Landroid/webkit/WebHistoryItemClassic;->getUrl()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    :goto_10
    return-object v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method public getViewDelegate()Landroid/webkit/WebViewProvider$ViewDelegate;
    .registers 1

    #@0
    .prologue
    .line 1823
    return-object p0
.end method

.method getViewHeight()I
    .registers 3

    #@0
    .prologue
    .line 2463
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@3
    move-result v0

    #@4
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getVisibleTitleHeightImpl()I

    #@7
    move-result v1

    #@8
    sub-int/2addr v0, v1

    #@9
    return v0
.end method

.method getViewHeightWithTitle()I
    .registers 3

    #@0
    .prologue
    .line 2467
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@3
    move-result v0

    #@4
    .line 2468
    .local v0, height:I
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@6
    invoke-virtual {v1}, Landroid/webkit/WebView;->isHorizontalScrollBarEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_17

    #@c
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mOverlayHorizontalScrollbar:Z

    #@e
    if-nez v1, :cond_17

    #@10
    .line 2469
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@12
    invoke-virtual {v1}, Landroid/webkit/WebView$PrivateAccess;->getHorizontalScrollbarHeight()I

    #@15
    move-result v1

    #@16
    sub-int/2addr v0, v1

    #@17
    .line 2471
    :cond_17
    return v0
.end method

.method getViewManager()Landroid/webkit/ViewManager;
    .registers 2

    #@0
    .prologue
    .line 10364
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mViewManager:Landroid/webkit/ViewManager;

    #@2
    return-object v0
.end method

.method getViewWidth()I
    .registers 4

    #@0
    .prologue
    .line 2402
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebView;->isVerticalScrollBarEnabled()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_c

    #@8
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mOverlayVerticalScrollbar:Z

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 2403
    :cond_c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWidth()I

    #@f
    move-result v0

    #@10
    .line 2405
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWidth()I

    #@15
    move-result v1

    #@16
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@18
    invoke-virtual {v2}, Landroid/webkit/WebView;->getVerticalScrollbarWidth()I

    #@1b
    move-result v2

    #@1c
    sub-int/2addr v1, v2

    #@1d
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@20
    move-result v0

    #@21
    goto :goto_10
.end method

.method public getVisibleTitleHeight()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2434
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getVisibleTitleHeightImpl()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getWebBackForwardListClient()Landroid/webkit/WebBackForwardListClient;
    .registers 2

    #@0
    .prologue
    .line 4697
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->getWebBackForwardListClient()Landroid/webkit/WebBackForwardListClient;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getWebChromeClient()Landroid/webkit/WebChromeClient;
    .registers 2

    #@0
    .prologue
    .line 4680
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->getWebChromeClient()Landroid/webkit/WebChromeClient;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getWebView()Landroid/webkit/WebView;
    .registers 2

    #@0
    .prologue
    .line 1818
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    return-object v0
.end method

.method public getWebViewClient()Landroid/webkit/WebViewClient;
    .registers 2

    #@0
    .prologue
    .line 4647
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->getWebViewClient()Landroid/webkit/WebViewClient;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public declared-synchronized getWebViewCore()Landroid/webkit/WebViewCore;
    .registers 2

    #@0
    .prologue
    .line 8768
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method getWidth()I
    .registers 2

    #@0
    .prologue
    .line 1845
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebView;->getWidth()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getZoomControls()Landroid/view/View;
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 8401
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->supportZoom()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_14

    #@a
    .line 8402
    const-string/jumbo v0, "webview"

    #@d
    const-string v1, "This WebView doesn\'t support zoom."

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 8403
    const/4 v0, 0x0

    #@13
    .line 8405
    :goto_13
    return-object v0

    #@14
    :cond_14
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@16
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->getExternalZoomPicker()Landroid/view/View;

    #@19
    move-result-object v0

    #@1a
    goto :goto_13
.end method

.method getZoomOverviewScale()F
    .registers 2

    #@0
    .prologue
    .line 8421
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->getZoomOverviewScale()F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public goBack()V
    .registers 2

    #@0
    .prologue
    .line 3174
    const/4 v0, -0x1

    #@1
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->goBackOrForwardImpl(I)V

    #@4
    .line 3175
    return-void
.end method

.method public goBackOrForward(I)V
    .registers 2
    .parameter "steps"

    #@0
    .prologue
    .line 3221
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->goBackOrForwardImpl(I)V

    #@3
    .line 3222
    return-void
.end method

.method public goForward()V
    .registers 2

    #@0
    .prologue
    .line 3197
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->goBackOrForwardImpl(I)V

    #@4
    .line 3198
    return-void
.end method

.method incrementTextGeneration()V
    .registers 2

    #@0
    .prologue
    .line 1073
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@6
    return-void
.end method

.method public init(Ljava/util/Map;Z)V
    .registers 9
    .parameter
    .parameter "privateBrowsing"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, javaScriptInterfaces:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v3, 0x0

    #@1
    .line 1618
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@3
    .line 1621
    .local v1, context:Landroid/content/Context;
    invoke-static {v1}, Landroid/webkit/JniUtil;->setContext(Landroid/content/Context;)V

    #@6
    .line 1623
    new-instance v0, Landroid/webkit/CallbackProxy;

    #@8
    invoke-direct {v0, v1, p0}, Landroid/webkit/CallbackProxy;-><init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;)V

    #@b
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@d
    .line 1624
    new-instance v0, Landroid/webkit/ViewManager;

    #@f
    invoke-direct {v0, p0}, Landroid/webkit/ViewManager;-><init>(Landroid/webkit/WebViewClassic;)V

    #@12
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mViewManager:Landroid/webkit/ViewManager;

    #@14
    .line 1625
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@17
    move-result-object v0

    #@18
    invoke-static {v0}, Landroid/webkit/L10nUtils;->setApplicationContext(Landroid/content/Context;)V

    #@1b
    .line 1626
    new-instance v0, Landroid/webkit/WebViewCore;

    #@1d
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@1f
    invoke-direct {v0, v1, p0, v2, p1}, Landroid/webkit/WebViewCore;-><init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;Landroid/webkit/CallbackProxy;Ljava/util/Map;)V

    #@22
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@24
    .line 1627
    invoke-static {v1}, Landroid/webkit/WebViewDatabaseClassic;->getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabaseClassic;

    #@27
    move-result-object v0

    #@28
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mDatabase:Landroid/webkit/WebViewDatabaseClassic;

    #@2a
    .line 1628
    new-instance v0, Landroid/widget/OverScroller;

    #@2c
    const/4 v2, 0x0

    #@2d
    const/4 v5, 0x0

    #@2e
    move v4, v3

    #@2f
    invoke-direct/range {v0 .. v5}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;FFZ)V

    #@32
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@34
    .line 1629
    new-instance v0, Landroid/webkit/ZoomManager;

    #@36
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@38
    invoke-direct {v0, p0, v2}, Landroid/webkit/ZoomManager;-><init>(Landroid/webkit/WebViewClassic;Landroid/webkit/CallbackProxy;)V

    #@3b
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@3d
    .line 1634
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->init()V

    #@40
    .line 1635
    invoke-direct {p0, v1}, Landroid/webkit/WebViewClassic;->setupPackageListener(Landroid/content/Context;)V

    #@43
    .line 1636
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->setupProxyListener(Landroid/content/Context;)V

    #@46
    .line 1637
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->setupTrustStorageListener(Landroid/content/Context;)V

    #@49
    .line 1638
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->updateMultiTouchSupport(Landroid/content/Context;)V

    #@4c
    .line 1640
    if-eqz p2, :cond_51

    #@4e
    .line 1641
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->startPrivateBrowsing()V

    #@51
    .line 1644
    :cond_51
    new-instance v0, Landroid/webkit/WebViewCore$AutoFillData;

    #@53
    invoke-direct {v0}, Landroid/webkit/WebViewCore$AutoFillData;-><init>()V

    #@56
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoFillData:Landroid/webkit/WebViewCore$AutoFillData;

    #@58
    .line 1645
    new-instance v0, Landroid/widget/Scroller;

    #@5a
    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    #@5d
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mEditTextScroller:Landroid/widget/Scroller;

    #@5f
    .line 1646
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@62
    move-result-object v0

    #@63
    const v2, 0x2060022

    #@66
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@69
    move-result v0

    #@6a
    sput-boolean v0, Landroid/webkit/WebViewClassic;->mCapptouchFlickNoti:Z

    #@6c
    .line 1648
    return-void
.end method

.method invalidate()V
    .registers 2

    #@0
    .prologue
    .line 1857
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebView;->invalidate()V

    #@5
    .line 1858
    return-void
.end method

.method public invokeZoomPicker()V
    .registers 3

    #@0
    .prologue
    .line 3363
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->supportZoom()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_13

    #@a
    .line 3364
    const-string/jumbo v0, "webview"

    #@d
    const-string v1, "This WebView doesn\'t support zoom."

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 3369
    :goto_12
    return-void

    #@13
    .line 3367
    :cond_13
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->clearHelpers()V

    #@16
    .line 3368
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@18
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->invokeZoomPicker()V

    #@1b
    goto :goto_12
.end method

.method public isPaused()Z
    .registers 2

    #@0
    .prologue
    .line 4077
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsPaused:Z

    #@2
    return v0
.end method

.method public isPrivateBrowsingEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 3247
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@3
    move-result-object v0

    #@4
    .line 3248
    .local v0, settings:Landroid/webkit/WebSettingsClassic;
    if-eqz v0, :cond_b

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->isPrivateBrowsingEnabled()Z

    #@9
    move-result v1

    #@a
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method isRectFitOnScreen(Landroid/graphics/Rect;)Z
    .registers 10
    .parameter "rect"

    #@0
    .prologue
    .line 8511
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@3
    move-result v1

    #@4
    .line 8512
    .local v1, rectWidth:I
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@7
    move-result v0

    #@8
    .line 8513
    .local v0, rectHeight:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@b
    move-result v4

    #@c
    .line 8514
    .local v4, viewWidth:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@f
    move-result v3

    #@10
    .line 8515
    .local v3, viewHeight:I
    int-to-float v5, v4

    #@11
    int-to-float v6, v1

    #@12
    div-float/2addr v5, v6

    #@13
    int-to-float v6, v3

    #@14
    int-to-float v7, v0

    #@15
    div-float/2addr v6, v7

    #@16
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    #@19
    move-result v2

    #@1a
    .line 8516
    .local v2, scale:F
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@1c
    invoke-virtual {v5, v2}, Landroid/webkit/ZoomManager;->computeScaleWithLimits(F)F

    #@1f
    move-result v2

    #@20
    .line 8517
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@22
    invoke-virtual {v5, v2}, Landroid/webkit/ZoomManager;->willScaleTriggerZoom(F)Z

    #@25
    move-result v5

    #@26
    if-nez v5, :cond_5c

    #@28
    iget v5, p1, Landroid/graphics/Rect;->left:I

    #@2a
    invoke-virtual {p0, v5}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@2d
    move-result v5

    #@2e
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@31
    move-result v6

    #@32
    if-lt v5, v6, :cond_5c

    #@34
    iget v5, p1, Landroid/graphics/Rect;->right:I

    #@36
    invoke-virtual {p0, v5}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@39
    move-result v5

    #@3a
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@3d
    move-result v6

    #@3e
    add-int/2addr v6, v4

    #@3f
    if-gt v5, v6, :cond_5c

    #@41
    iget v5, p1, Landroid/graphics/Rect;->top:I

    #@43
    invoke-virtual {p0, v5}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@46
    move-result v5

    #@47
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@4a
    move-result v6

    #@4b
    if-lt v5, v6, :cond_5c

    #@4d
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    #@4f
    invoke-virtual {p0, v5}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@52
    move-result v5

    #@53
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@56
    move-result v6

    #@57
    add-int/2addr v6, v3

    #@58
    if-gt v5, v6, :cond_5c

    #@5a
    const/4 v5, 0x1

    #@5b
    :goto_5b
    return v5

    #@5c
    :cond_5c
    const/4 v5, 0x0

    #@5d
    goto :goto_5b
.end method

.method isSelectingInEditText()Z
    .registers 2

    #@0
    .prologue
    .line 5962
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsSelectingInEditText:Z

    #@2
    return v0
.end method

.method public isSelectingText()Z
    .registers 2

    #@0
    .prologue
    .line 10753
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@2
    return v0
.end method

.method public isUserScalable()Z
    .registers 2

    #@0
    .prologue
    .line 8472
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 8473
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->getViewportUserScalable()Z

    #@9
    move-result v0

    #@a
    .line 8476
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "data"
    .parameter "mimeType"
    .parameter "encoding"

    #@0
    .prologue
    .line 3050
    const-string/jumbo v1, "utf-8"

    #@3
    invoke-virtual {v1, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_25

    #@9
    const-string/jumbo v1, "text/html"

    #@c
    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_25

    #@12
    .line 3051
    new-instance v0, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v0, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@17
    .line 3052
    .local v0, newMimeType:Ljava/lang/StringBuilder;
    const-string v1, ";charset=utf-8"

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 3053
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    const/4 v2, 0x0

    #@21
    invoke-direct {p0, p1, v1, v2}, Landroid/webkit/WebViewClassic;->loadDataImpl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 3058
    .end local v0           #newMimeType:Ljava/lang/StringBuilder;
    :goto_24
    return-void

    #@25
    .line 3055
    :cond_25
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewClassic;->loadDataImpl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@28
    goto :goto_24
.end method

.method public loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "baseUrl"
    .parameter "data"
    .parameter "mimeType"
    .parameter "encoding"
    .parameter "historyUrl"

    #@0
    .prologue
    .line 3078
    if-eqz p1, :cond_12

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    const-string v2, "data:"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_12

    #@e
    .line 3079
    invoke-direct {p0, p2, p3, p4}, Landroid/webkit/WebViewClassic;->loadDataImpl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 3091
    :goto_11
    return-void

    #@12
    .line 3082
    :cond_12
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@15
    .line 3083
    new-instance v0, Landroid/webkit/WebViewCore$BaseUrlData;

    #@17
    invoke-direct {v0}, Landroid/webkit/WebViewCore$BaseUrlData;-><init>()V

    #@1a
    .line 3084
    .local v0, arg:Landroid/webkit/WebViewCore$BaseUrlData;
    iput-object p1, v0, Landroid/webkit/WebViewCore$BaseUrlData;->mBaseUrl:Ljava/lang/String;

    #@1c
    .line 3085
    iput-object p2, v0, Landroid/webkit/WebViewCore$BaseUrlData;->mData:Ljava/lang/String;

    #@1e
    .line 3086
    iput-object p3, v0, Landroid/webkit/WebViewCore$BaseUrlData;->mMimeType:Ljava/lang/String;

    #@20
    .line 3087
    iput-object p4, v0, Landroid/webkit/WebViewCore$BaseUrlData;->mEncoding:Ljava/lang/String;

    #@22
    .line 3088
    iput-object p5, v0, Landroid/webkit/WebViewCore$BaseUrlData;->mHistoryUrl:Ljava/lang/String;

    #@24
    .line 3089
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@26
    const/16 v2, 0x8b

    #@28
    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@2b
    .line 3090
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->clearHelpers()V

    #@2e
    goto :goto_11
.end method

.method public loadUrl(Ljava/lang/String;)V
    .registers 2
    .parameter "url"

    #@0
    .prologue
    .line 3017
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->loadUrlImpl(Ljava/lang/String;)V

    #@3
    .line 3018
    return-void
.end method

.method public loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .registers 3
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2993
    .local p2, additionalHttpHeaders:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewClassic;->loadUrlImpl(Ljava/lang/String;Ljava/util/Map;)V

    #@3
    .line 2994
    return-void
.end method

.method public loadViewState(Ljava/io/InputStream;)V
    .registers 5
    .parameter "stream"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2889
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@3
    .line 2890
    new-instance v0, Landroid/webkit/WebViewClassic$8;

    #@5
    invoke-direct {v0, p0}, Landroid/webkit/WebViewClassic$8;-><init>(Landroid/webkit/WebViewClassic;)V

    #@8
    new-array v1, v1, [Ljava/io/InputStream;

    #@a
    const/4 v2, 0x0

    #@b
    aput-object p1, v1, v2

    #@d
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic$8;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@10
    .line 2917
    return-void
.end method

.method native nativeGetProperty(Ljava/lang/String;)Ljava/lang/String;
.end method

.method native nativeSetProperty(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method notifyFindDialogDismissed()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4332
    const/4 v0, 0x0

    #@2
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@4
    .line 4333
    const/4 v0, -0x1

    #@5
    iput v0, p0, Landroid/webkit/WebViewClassic;->mCachedOverlappingActionModeHeight:I

    #@7
    .line 4334
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@9
    if-nez v0, :cond_c

    #@b
    .line 4343
    :goto_b
    return-void

    #@c
    .line 4337
    :cond_c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->clearMatches()V

    #@f
    .line 4338
    invoke-direct {p0, v2}, Landroid/webkit/WebViewClassic;->setFindIsUp(Z)V

    #@12
    .line 4341
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@15
    move-result v0

    #@16
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@19
    move-result v1

    #@1a
    invoke-direct {p0, v0, v1, v2, v2}, Landroid/webkit/WebViewClassic;->pinScrollTo(IIZI)Z

    #@1d
    .line 4342
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@20
    goto :goto_b
.end method

.method public notifyForUiThread()V
    .registers 2

    #@0
    .prologue
    .line 10665
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->notifyForUiThread()V

    #@5
    .line 10666
    return-void
.end method

.method public onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 6433
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@3
    invoke-virtual {v0}, Landroid/webkit/WebView;->hasWindowFocus()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_c

    #@9
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->setActive(Z)V

    #@c
    .line 6435
    :cond_c
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isAccessibilityInjectionEnabled()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_19

    #@12
    .line 6436
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0, v1}, Landroid/webkit/AccessibilityInjector;->toggleAccessibilityFeedback(Z)V

    #@19
    .line 6439
    :cond_19
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->updateHwAccelerated()V

    #@1c
    .line 6440
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 5158
    const/4 v0, -0x1

    #@1
    iput v0, p0, Landroid/webkit/WebViewClassic;->mCachedOverlappingActionModeHeight:I

    #@3
    .line 5164
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    #@5
    iput v0, p0, Landroid/webkit/WebViewClassic;->mOrientation:I

    #@7
    .line 5165
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@9
    if-eqz v0, :cond_16

    #@b
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@d
    if-nez v0, :cond_16

    #@f
    .line 5166
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@11
    const/16 v1, 0x86

    #@13
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@16
    .line 5168
    :cond_16
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .registers 4
    .parameter "outAttrs"

    #@0
    .prologue
    .line 5536
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@2
    if-nez v0, :cond_14

    #@4
    .line 5537
    new-instance v0, Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@6
    invoke-direct {v0, p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;-><init>(Landroid/webkit/WebViewClassic;)V

    #@9
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@b
    .line 5538
    new-instance v0, Landroid/webkit/AutoCompletePopup;

    #@d
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@f
    invoke-direct {v0, p0, v1}, Landroid/webkit/AutoCompletePopup;-><init>(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewClassic$WebViewInputConnection;)V

    #@12
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@14
    .line 5540
    :cond_14
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@16
    invoke-virtual {v0, p1}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setupEditorInfo(Landroid/view/inputmethod/EditorInfo;)V

    #@19
    .line 5541
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@1b
    return-object v0
.end method

.method public onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 6444
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->clearHelpers()V

    #@4
    .line 6445
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@6
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->dismissZoomPicker()V

    #@9
    .line 6446
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@b
    invoke-virtual {v0}, Landroid/webkit/WebView;->hasWindowFocus()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_14

    #@11
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->setActive(Z)V

    #@14
    .line 6448
    :cond_14
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isAccessibilityInjectionEnabled()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_21

    #@1a
    .line 6449
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, v1}, Landroid/webkit/AccessibilityInjector;->toggleAccessibilityFeedback(Z)V

    #@21
    .line 6452
    :cond_21
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->updateHwAccelerated()V

    #@24
    .line 6454
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->ensureFunctorDetached()V

    #@27
    .line 6455
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 11
    .parameter "canvas"

    #@0
    .prologue
    .line 4915
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->inFullScreenMode()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_7

    #@6
    .line 4986
    :cond_6
    :goto_6
    return-void

    #@7
    .line 4920
    :cond_7
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@9
    if-nez v0, :cond_11

    #@b
    .line 4921
    iget v0, p0, Landroid/webkit/WebViewClassic;->mBackgroundColor:I

    #@d
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    #@10
    goto :goto_6

    #@11
    .line 4928
    :cond_11
    iget v0, p0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@13
    iget v1, p0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@15
    or-int/2addr v0, v1

    #@16
    if-nez v0, :cond_22

    #@18
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHistoryPicture:Landroid/graphics/Picture;

    #@1a
    if-nez v0, :cond_22

    #@1c
    .line 4929
    iget v0, p0, Landroid/webkit/WebViewClassic;->mBackgroundColor:I

    #@1e
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    #@21
    goto :goto_6

    #@22
    .line 4933
    :cond_22
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_d7

    #@28
    .line 4934
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2a
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->setHardwareAccelerated()V

    #@2d
    .line 4939
    :goto_2d
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@30
    move-result v8

    #@31
    .line 4940
    .local v8, saveCount:I
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mInOverScrollMode:Z

    #@33
    if-eqz v0, :cond_42

    #@35
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getUseWebViewBackgroundForOverscrollBackground()Z

    #@3c
    move-result v0

    #@3d
    if-nez v0, :cond_42

    #@3f
    .line 4942
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->drawOverScrollBackground(Landroid/graphics/Canvas;)V

    #@42
    .line 4945
    :cond_42
    const/4 v0, 0x0

    #@43
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@46
    move-result v1

    #@47
    int-to-float v1, v1

    #@48
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    #@4b
    .line 4946
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->drawContent(Landroid/graphics/Canvas;)V

    #@4e
    .line 4947
    invoke-virtual {p1, v8}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@51
    .line 4952
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@53
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->signalRepaintDone()V

    #@56
    .line 4954
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@58
    if-eqz v0, :cond_65

    #@5a
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@5c
    invoke-virtual {v0, p1}, Landroid/webkit/OverScrollGlow;->drawEdgeGlows(Landroid/graphics/Canvas;)Z

    #@5f
    move-result v0

    #@60
    if-eqz v0, :cond_65

    #@62
    .line 4955
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@65
    .line 4958
    :cond_65
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFocusTransition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

    #@67
    if-eqz v0, :cond_de

    #@69
    .line 4959
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mFocusTransition:Landroid/webkit/WebViewClassic$FocusTransitionDrawable;

    #@6b
    invoke-virtual {v0, p1}, Landroid/webkit/WebViewClassic$FocusTransitionDrawable;->draw(Landroid/graphics/Canvas;)V

    #@6e
    .line 4968
    :cond_6e
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getNavDump()Z

    #@75
    move-result v0

    #@76
    if-eqz v0, :cond_6

    #@78
    .line 4969
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightX:I

    #@7a
    iget v1, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightY:I

    #@7c
    or-int/2addr v0, v1

    #@7d
    if-eqz v0, :cond_6

    #@7f
    .line 4970
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mTouchCrossHairColor:Landroid/graphics/Paint;

    #@81
    if-nez v0, :cond_91

    #@83
    .line 4971
    new-instance v0, Landroid/graphics/Paint;

    #@85
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@88
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mTouchCrossHairColor:Landroid/graphics/Paint;

    #@8a
    .line 4972
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mTouchCrossHairColor:Landroid/graphics/Paint;

    #@8c
    const/high16 v1, -0x1

    #@8e
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    #@91
    .line 4974
    :cond_91
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightX:I

    #@93
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@95
    sub-int/2addr v0, v1

    #@96
    int-to-float v1, v0

    #@97
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightY:I

    #@99
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@9b
    sub-int/2addr v0, v2

    #@9c
    int-to-float v2, v0

    #@9d
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightX:I

    #@9f
    iget v3, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@a1
    add-int/2addr v0, v3

    #@a2
    add-int/lit8 v0, v0, 0x1

    #@a4
    int-to-float v3, v0

    #@a5
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightY:I

    #@a7
    iget v4, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@a9
    add-int/2addr v0, v4

    #@aa
    add-int/lit8 v0, v0, 0x1

    #@ac
    int-to-float v4, v0

    #@ad
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mTouchCrossHairColor:Landroid/graphics/Paint;

    #@af
    move-object v0, p1

    #@b0
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@b3
    .line 4978
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightX:I

    #@b5
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@b7
    add-int/2addr v0, v1

    #@b8
    add-int/lit8 v0, v0, 0x1

    #@ba
    int-to-float v1, v0

    #@bb
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightY:I

    #@bd
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@bf
    sub-int/2addr v0, v2

    #@c0
    int-to-float v2, v0

    #@c1
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightX:I

    #@c3
    iget v3, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@c5
    sub-int/2addr v0, v3

    #@c6
    int-to-float v3, v0

    #@c7
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightY:I

    #@c9
    iget v4, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@cb
    add-int/2addr v0, v4

    #@cc
    add-int/lit8 v0, v0, 0x1

    #@ce
    int-to-float v4, v0

    #@cf
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mTouchCrossHairColor:Landroid/graphics/Paint;

    #@d1
    move-object v0, p1

    #@d2
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@d5
    goto/16 :goto_6

    #@d7
    .line 4936
    .end local v8           #saveCount:I
    :cond_d7
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@d9
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->resumeWebKitDraw()V

    #@dc
    goto/16 :goto_2d

    #@de
    .line 4960
    .restart local v8       #saveCount:I
    :cond_de
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->shouldDrawHighlightRect()Z

    #@e1
    move-result v0

    #@e2
    if-eqz v0, :cond_6e

    #@e4
    .line 4961
    new-instance v6, Landroid/graphics/RegionIterator;

    #@e6
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@e8
    invoke-direct {v6, v0}, Landroid/graphics/RegionIterator;-><init>(Landroid/graphics/Region;)V

    #@eb
    .line 4962
    .local v6, iter:Landroid/graphics/RegionIterator;
    new-instance v7, Landroid/graphics/Rect;

    #@ed
    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    #@f0
    .line 4963
    .local v7, r:Landroid/graphics/Rect;
    :goto_f0
    invoke-virtual {v6, v7}, Landroid/graphics/RegionIterator;->next(Landroid/graphics/Rect;)Z

    #@f3
    move-result v0

    #@f4
    if-eqz v0, :cond_6e

    #@f6
    .line 4964
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mTouchHightlightPaint:Landroid/graphics/Paint;

    #@f8
    invoke-virtual {p1, v7, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@fb
    goto :goto_f0
.end method

.method public onDrawVerticalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V
    .registers 8
    .parameter "canvas"
    .parameter "scrollBar"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 3869
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@3
    move-result v0

    #@4
    if-gez v0, :cond_b

    #@6
    .line 3870
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@9
    move-result v0

    #@a
    sub-int/2addr p4, v0

    #@b
    .line 3872
    :cond_b
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getVisibleTitleHeightImpl()I

    #@e
    move-result v0

    #@f
    add-int/2addr v0, p4

    #@10
    invoke-virtual {p2, p3, v0, p5, p6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@13
    .line 3873
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@16
    .line 3874
    return-void
.end method

.method onFixedLengthZoomAnimationEnd()V
    .registers 2

    #@0
    .prologue
    .line 5227
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@2
    if-nez v0, :cond_d

    #@4
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@6
    if-nez v0, :cond_d

    #@8
    .line 5228
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@a
    invoke-static {v0}, Landroid/webkit/WebViewCore;->resumeUpdatePicture(Landroid/webkit/WebViewCore;)V

    #@d
    .line 5230
    :cond_d
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->onZoomAnimationEnd()V

    #@10
    .line 5231
    return-void
.end method

.method onFixedLengthZoomAnimationStart()V
    .registers 2

    #@0
    .prologue
    .line 5222
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWebViewCore()Landroid/webkit/WebViewCore;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/webkit/WebViewCore;->pauseUpdatePicture(Landroid/webkit/WebViewCore;)V

    #@7
    .line 5223
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->onZoomAnimationStart()V

    #@a
    .line 5224
    return-void
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 6
    .parameter "focused"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 6541
    if-eqz p1, :cond_28

    #@4
    .line 6542
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mDrawCursorRing:Z

    #@6
    .line 6543
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->setFocusControllerActive(Z)V

    #@9
    .line 6545
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@b
    if-eqz v0, :cond_14

    #@d
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@f
    if-nez v0, :cond_14

    #@11
    .line 6546
    invoke-direct {p0, v1}, Landroid/webkit/WebViewClassic;->showLGSelectActionPopupWindow(I)V

    #@14
    .line 6553
    :cond_14
    :goto_14
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@16
    invoke-virtual {v0}, Landroid/graphics/Region;->isEmpty()Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_27

    #@1c
    .line 6554
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@1e
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mTouchHighlightRegion:Landroid/graphics/Region;

    #@20
    invoke-virtual {v1}, Landroid/graphics/Region;->getBounds()Landroid/graphics/Rect;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->invalidate(Landroid/graphics/Rect;)V

    #@27
    .line 6556
    :cond_27
    return-void

    #@28
    .line 6549
    :cond_28
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mDrawCursorRing:Z

    #@2a
    .line 6550
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->setFocusControllerActive(Z)V

    #@2d
    .line 6551
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mKeysPressed:Ljava/util/Vector;

    #@2f
    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    #@32
    goto :goto_14
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "event"

    #@0
    .prologue
    const/16 v7, 0x9

    #@2
    const/4 v6, 0x0

    #@3
    .line 7856
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@6
    move-result v5

    #@7
    and-int/lit8 v5, v5, 0x2

    #@9
    if-eqz v5, :cond_12

    #@b
    .line 7857
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@e
    move-result v5

    #@f
    packed-switch v5, :pswitch_data_62

    #@12
    .line 7884
    :cond_12
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@14
    invoke-virtual {v5, p1}, Landroid/webkit/WebView$PrivateAccess;->super_onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@17
    move-result v5

    #@18
    :goto_18
    return v5

    #@19
    .line 7861
    :pswitch_19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@1c
    move-result v5

    #@1d
    and-int/lit8 v5, v5, 0x1

    #@1f
    if-eqz v5, :cond_55

    #@21
    .line 7862
    const/4 v4, 0x0

    #@22
    .line 7863
    .local v4, vscroll:F
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@25
    move-result v1

    #@26
    .line 7868
    .local v1, hscroll:F
    :goto_26
    cmpl-float v5, v1, v6

    #@28
    if-nez v5, :cond_2e

    #@2a
    cmpl-float v5, v4, v6

    #@2c
    if-eqz v5, :cond_12

    #@2e
    .line 7869
    :cond_2e
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@30
    invoke-virtual {v5}, Landroid/webkit/WebView$PrivateAccess;->getVerticalScrollFactor()F

    #@33
    move-result v5

    #@34
    mul-float/2addr v5, v4

    #@35
    float-to-int v3, v5

    #@36
    .line 7871
    .local v3, vdelta:I
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@38
    invoke-virtual {v5}, Landroid/webkit/WebView$PrivateAccess;->getHorizontalScrollFactor()F

    #@3b
    move-result v5

    #@3c
    mul-float/2addr v5, v1

    #@3d
    float-to-int v0, v5

    #@3e
    .line 7874
    .local v0, hdelta:I
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->abortAnimation()V

    #@41
    .line 7875
    iget v2, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@43
    .line 7876
    .local v2, oldTouchMode:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@46
    move-result v5

    #@47
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@4a
    move-result v6

    #@4b
    invoke-direct {p0, v5, v6}, Landroid/webkit/WebViewClassic;->startScrollingLayer(FF)V

    #@4e
    .line 7877
    invoke-direct {p0, v0, v3}, Landroid/webkit/WebViewClassic;->doDrag(II)Z

    #@51
    .line 7878
    iput v2, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@53
    .line 7879
    const/4 v5, 0x1

    #@54
    goto :goto_18

    #@55
    .line 7865
    .end local v0           #hdelta:I
    .end local v1           #hscroll:F
    .end local v2           #oldTouchMode:I
    .end local v3           #vdelta:I
    .end local v4           #vscroll:F
    :cond_55
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@58
    move-result v5

    #@59
    neg-float v4, v5

    #@5a
    .line 7866
    .restart local v4       #vscroll:F
    const/16 v5, 0xa

    #@5c
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@5f
    move-result v1

    #@60
    .restart local v1       #hscroll:F
    goto :goto_26

    #@61
    .line 7857
    nop

    #@62
    :pswitch_data_62
    .packed-switch 0x8
        :pswitch_19
    .end packed-switch
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 6899
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v2, :cond_6

    #@4
    .line 6900
    const/4 v2, 0x0

    #@5
    .line 6906
    :goto_5
    return v2

    #@6
    .line 6902
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@9
    move-result v2

    #@a
    float-to-int v2, v2

    #@b
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@e
    move-result v3

    #@f
    add-int/2addr v2, v3

    #@10
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@13
    move-result v0

    #@14
    .line 6903
    .local v0, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@17
    move-result v2

    #@18
    float-to-int v2, v2

    #@19
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@1c
    move-result v3

    #@1d
    add-int/2addr v2, v3

    #@1e
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@21
    move-result v1

    #@22
    .line 6904
    .local v1, y:I
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@24
    const/16 v3, 0x87

    #@26
    invoke-virtual {v2, v3, v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@29
    .line 6905
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@2b
    invoke-virtual {v2, p1}, Landroid/webkit/WebView$PrivateAccess;->super_onHoverEvent(Landroid/view/MotionEvent;)Z

    #@2e
    .line 6906
    const/4 v2, 0x1

    #@2f
    goto :goto_5
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 9
    .parameter "event"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2186
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isScrollableForAccessibility()Z

    #@4
    move-result v4

    #@5
    invoke-virtual {p1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    #@8
    .line 2187
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@b
    move-result v4

    #@c
    invoke-virtual {p1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setScrollX(I)V

    #@f
    .line 2188
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@12
    move-result v4

    #@13
    invoke-virtual {p1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    #@16
    .line 2189
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getContentWidth()I

    #@19
    move-result v4

    #@1a
    invoke-virtual {p0, v4}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@1d
    move-result v3

    #@1e
    .line 2190
    .local v3, convertedContentWidth:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWidth()I

    #@21
    move-result v4

    #@22
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@24
    invoke-virtual {v5}, Landroid/webkit/WebView;->getPaddingLeft()I

    #@27
    move-result v5

    #@28
    sub-int/2addr v4, v5

    #@29
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2b
    invoke-virtual {v5}, Landroid/webkit/WebView;->getPaddingLeft()I

    #@2e
    move-result v5

    #@2f
    sub-int v1, v4, v5

    #@31
    .line 2192
    .local v1, adjustedViewWidth:I
    sub-int v4, v3, v1

    #@33
    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    #@36
    move-result v4

    #@37
    invoke-virtual {p1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollX(I)V

    #@3a
    .line 2193
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getContentHeight()I

    #@3d
    move-result v4

    #@3e
    invoke-virtual {p0, v4}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@41
    move-result v2

    #@42
    .line 2194
    .local v2, convertedContentHeight:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@45
    move-result v4

    #@46
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@48
    invoke-virtual {v5}, Landroid/webkit/WebView;->getPaddingTop()I

    #@4b
    move-result v5

    #@4c
    sub-int/2addr v4, v5

    #@4d
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@4f
    invoke-virtual {v5}, Landroid/webkit/WebView;->getPaddingBottom()I

    #@52
    move-result v5

    #@53
    sub-int v0, v4, v5

    #@55
    .line 2196
    .local v0, adjustedViewHeight:I
    sub-int v4, v2, v0

    #@57
    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    #@5a
    move-result v4

    #@5b
    invoke-virtual {p1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    #@5e
    .line 2197
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 11
    .parameter "info"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 2159
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@4
    invoke-virtual {v7}, Landroid/webkit/WebView;->isEnabled()Z

    #@7
    move-result v7

    #@8
    if-nez v7, :cond_b

    #@a
    .line 2182
    :goto_a
    return-void

    #@b
    .line 2164
    :cond_b
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isScrollableForAccessibility()Z

    #@e
    move-result v7

    #@f
    invoke-virtual {p1, v7}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    #@12
    .line 2166
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getContentHeight()I

    #@15
    move-result v7

    #@16
    invoke-virtual {p0, v7}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@19
    move-result v3

    #@1a
    .line 2167
    .local v3, convertedContentHeight:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@1d
    move-result v7

    #@1e
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@20
    invoke-virtual {v8}, Landroid/webkit/WebView;->getPaddingTop()I

    #@23
    move-result v8

    #@24
    sub-int/2addr v7, v8

    #@25
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@27
    invoke-virtual {v8}, Landroid/webkit/WebView;->getPaddingBottom()I

    #@2a
    move-result v8

    #@2b
    sub-int v0, v7, v8

    #@2d
    .line 2169
    .local v0, adjustedViewHeight:I
    sub-int v7, v3, v0

    #@2f
    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    #@32
    move-result v4

    #@33
    .line 2170
    .local v4, maxScrollY:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@36
    move-result v7

    #@37
    if-lez v7, :cond_58

    #@39
    move v1, v5

    #@3a
    .line 2171
    .local v1, canScrollBackward:Z
    :goto_3a
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@3d
    move-result v7

    #@3e
    sub-int/2addr v7, v4

    #@3f
    if-lez v7, :cond_5a

    #@41
    move v2, v5

    #@42
    .line 2173
    .local v2, canScrollForward:Z
    :goto_42
    if-eqz v2, :cond_49

    #@44
    .line 2174
    const/16 v5, 0x1000

    #@46
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@49
    .line 2177
    :cond_49
    if-eqz v2, :cond_50

    #@4b
    .line 2178
    const/16 v5, 0x2000

    #@4d
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@50
    .line 2181
    :cond_50
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@53
    move-result-object v5

    #@54
    invoke-virtual {v5, p1}, Landroid/webkit/AccessibilityInjector;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@57
    goto :goto_a

    #@58
    .end local v1           #canScrollBackward:Z
    .end local v2           #canScrollForward:Z
    :cond_58
    move v1, v6

    #@59
    .line 2170
    goto :goto_3a

    #@5a
    .restart local v1       #canScrollBackward:Z
    :cond_5a
    move v2, v6

    #@5b
    .line 2171
    goto :goto_42
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 9
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x1

    #@3
    .line 5792
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@5
    if-eqz v2, :cond_a

    #@7
    .line 5793
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@a
    .line 5795
    :cond_a
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@c
    if-eqz v2, :cond_10

    #@e
    move v1, v0

    #@f
    .line 5897
    :cond_f
    :goto_f
    return v1

    #@10
    .line 5800
    :cond_10
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_18

    #@16
    move v1, v0

    #@17
    .line 5801
    goto :goto_f

    #@18
    .line 5804
    :cond_18
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@1a
    if-nez v2, :cond_1e

    #@1c
    move v1, v0

    #@1d
    .line 5805
    goto :goto_f

    #@1e
    .line 5820
    :cond_1e
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    #@21
    move-result v2

    #@22
    if-nez v2, :cond_2c

    #@24
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@26
    invoke-virtual {v2, p2}, Landroid/webkit/CallbackProxy;->uiOverrideKeyEvent(Landroid/view/KeyEvent;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_2e

    #@2c
    :cond_2c
    move v1, v0

    #@2d
    .line 5822
    goto :goto_f

    #@2e
    .line 5826
    :cond_2e
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isAccessibilityInjectionEnabled()Z

    #@31
    move-result v2

    #@32
    if-eqz v2, :cond_3e

    #@34
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2, p2}, Landroid/webkit/AccessibilityInjector;->handleKeyEventIfNecessary(Landroid/view/KeyEvent;)Z

    #@3b
    move-result v2

    #@3c
    if-nez v2, :cond_f

    #@3e
    .line 5831
    :cond_3e
    const/16 v2, 0x5c

    #@40
    if-ne p1, v2, :cond_56

    #@42
    .line 5832
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@45
    move-result v2

    #@46
    if-eqz v2, :cond_4c

    #@48
    .line 5833
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->pageUp(Z)Z

    #@4b
    goto :goto_f

    #@4c
    .line 5835
    :cond_4c
    invoke-virtual {p2, v3}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@4f
    move-result v2

    #@50
    if-eqz v2, :cond_56

    #@52
    .line 5836
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->pageUp(Z)Z

    #@55
    goto :goto_f

    #@56
    .line 5841
    :cond_56
    const/16 v2, 0x5d

    #@58
    if-ne p1, v2, :cond_6e

    #@5a
    .line 5842
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@5d
    move-result v2

    #@5e
    if-eqz v2, :cond_64

    #@60
    .line 5843
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->pageDown(Z)Z

    #@63
    goto :goto_f

    #@64
    .line 5845
    :cond_64
    invoke-virtual {p2, v3}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@67
    move-result v2

    #@68
    if-eqz v2, :cond_6e

    #@6a
    .line 5846
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->pageDown(Z)Z

    #@6d
    goto :goto_f

    #@6e
    .line 5851
    :cond_6e
    const/16 v2, 0x7a

    #@70
    if-ne p1, v2, :cond_7c

    #@72
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@75
    move-result v2

    #@76
    if-eqz v2, :cond_7c

    #@78
    .line 5852
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->pageUp(Z)Z

    #@7b
    goto :goto_f

    #@7c
    .line 5856
    :cond_7c
    const/16 v2, 0x7b

    #@7e
    if-ne p1, v2, :cond_8a

    #@80
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@83
    move-result v2

    #@84
    if-eqz v2, :cond_8a

    #@86
    .line 5857
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->pageDown(Z)Z

    #@89
    goto :goto_f

    #@8a
    .line 5861
    :cond_8a
    const/16 v2, 0x13

    #@8c
    if-lt p1, v2, :cond_95

    #@8e
    const/16 v2, 0x16

    #@90
    if-gt p1, v2, :cond_95

    #@92
    .line 5863
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@95
    .line 5866
    :cond_95
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->isEnterActionKey(I)Z

    #@98
    move-result v2

    #@99
    if-eqz v2, :cond_b9

    #@9b
    .line 5867
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@9e
    .line 5868
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@a1
    move-result v2

    #@a2
    if-nez v2, :cond_b9

    #@a4
    .line 5869
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@a6
    if-nez v2, :cond_f

    #@a8
    .line 5872
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mGotCenterDown:Z

    #@aa
    .line 5873
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@ac
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@ae
    const/16 v4, 0x72

    #@b0
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@b3
    move-result-object v3

    #@b4
    const-wide/16 v4, 0x3e8

    #@b6
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@b9
    .line 5878
    :cond_b9
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@bc
    move-result-object v2

    #@bd
    invoke-virtual {v2}, Landroid/webkit/WebSettingsClassic;->getNavDump()Z

    #@c0
    move-result v2

    #@c1
    if-eqz v2, :cond_c6

    #@c3
    .line 5879
    packed-switch p1, :pswitch_data_e2

    #@c6
    .line 5895
    :cond_c6
    :goto_c6
    invoke-direct {p0, p2}, Landroid/webkit/WebViewClassic;->sendKeyEvent(Landroid/view/KeyEvent;)V

    #@c9
    goto/16 :goto_f

    #@cb
    .line 5881
    :pswitch_cb
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->dumpDisplayTree()V

    #@ce
    goto :goto_c6

    #@cf
    .line 5885
    :pswitch_cf
    const/16 v2, 0xc

    #@d1
    if-ne p1, v2, :cond_d4

    #@d3
    move v0, v1

    #@d4
    :cond_d4
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->dumpDomTree(Z)V

    #@d7
    goto :goto_c6

    #@d8
    .line 5889
    :pswitch_d8
    const/16 v2, 0xe

    #@da
    if-ne p1, v2, :cond_dd

    #@dc
    move v0, v1

    #@dd
    :cond_dd
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->dumpRenderTree(Z)V

    #@e0
    goto :goto_c6

    #@e1
    .line 5879
    nop

    #@e2
    :pswitch_data_e2
    .packed-switch 0xb
        :pswitch_cb
        :pswitch_cf
        :pswitch_cf
        :pswitch_d8
        :pswitch_d8
    .end packed-switch
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "repeatCount"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 5758
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@3
    if-eqz v1, :cond_6

    #@5
    .line 5769
    :cond_5
    :goto_5
    return v0

    #@6
    .line 5762
    :cond_6
    if-nez p1, :cond_5

    #@8
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    if-eqz v1, :cond_5

    #@e
    .line 5764
    const/16 v1, 0x67

    #@10
    invoke-virtual {p0, v1, v0, v0, p3}, Landroid/webkit/WebViewClassic;->sendBatchableInputMessage(IIILjava/lang/Object;)V

    #@13
    .line 5765
    const/16 v1, 0x68

    #@15
    invoke-virtual {p0, v1, v0, v0, p3}, Landroid/webkit/WebViewClassic;->sendBatchableInputMessage(IIILjava/lang/Object;)V

    #@18
    .line 5767
    const/4 v0, 0x1

    #@19
    goto :goto_5
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 5779
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 5780
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@6
    invoke-virtual {v0, p1, p2}, Landroid/webkit/AutoCompletePopup;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    #@9
    move-result v0

    #@a
    .line 5782
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 9
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 5906
    iget-boolean v4, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@4
    if-eqz v4, :cond_7

    #@6
    .line 5953
    :cond_6
    :goto_6
    return v2

    #@7
    .line 5910
    :cond_7
    iget v4, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@9
    if-eqz v4, :cond_6

    #@b
    .line 5915
    const/4 v4, 0x5

    #@c
    if-ne p1, v4, :cond_33

    #@e
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@10
    if-eqz v4, :cond_33

    #@12
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@14
    invoke-virtual {v4}, Landroid/webkit/WebView$HitTestResult;->getType()I

    #@17
    move-result v4

    #@18
    const/4 v5, 0x2

    #@19
    if-ne v4, v5, :cond_33

    #@1b
    .line 5918
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInitialHitTestResult:Landroid/webkit/WebView$HitTestResult;

    #@1d
    invoke-virtual {v2}, Landroid/webkit/WebView$HitTestResult;->getExtra()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    .line 5919
    .local v1, text:Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    #@23
    const-string v2, "android.intent.action.DIAL"

    #@25
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@28
    move-result-object v4

    #@29
    invoke-direct {v0, v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@2c
    .line 5920
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@2e
    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@31
    move v2, v3

    #@32
    .line 5921
    goto :goto_6

    #@33
    .line 5927
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #text:Ljava/lang/String;
    :cond_33
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    #@36
    move-result v4

    #@37
    if-nez v4, :cond_6

    #@39
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@3b
    invoke-virtual {v4, p2}, Landroid/webkit/CallbackProxy;->uiOverrideKeyEvent(Landroid/view/KeyEvent;)Z

    #@3e
    move-result v4

    #@3f
    if-nez v4, :cond_6

    #@41
    .line 5933
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isAccessibilityInjectionEnabled()Z

    #@44
    move-result v4

    #@45
    if-eqz v4, :cond_53

    #@47
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4, p2}, Landroid/webkit/AccessibilityInjector;->handleKeyEventIfNecessary(Landroid/view/KeyEvent;)Z

    #@4e
    move-result v4

    #@4f
    if-eqz v4, :cond_53

    #@51
    move v2, v3

    #@52
    .line 5935
    goto :goto_6

    #@53
    .line 5938
    :cond_53
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->isEnterActionKey(I)Z

    #@56
    move-result v4

    #@57
    if-eqz v4, :cond_6e

    #@59
    .line 5940
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@5b
    const/16 v5, 0x72

    #@5d
    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    #@60
    .line 5941
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mGotCenterDown:Z

    #@62
    .line 5943
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@64
    if-eqz v2, :cond_6e

    #@66
    .line 5944
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->copySelection()Z

    #@69
    .line 5945
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@6c
    move v2, v3

    #@6d
    .line 5946
    goto :goto_6

    #@6e
    .line 5951
    :cond_6e
    invoke-direct {p0, p2}, Landroid/webkit/WebViewClassic;->sendKeyEvent(Landroid/view/KeyEvent;)V

    #@71
    move v2, v3

    #@72
    .line 5953
    goto :goto_6
.end method

.method public onMeasure(II)V
    .registers 15
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/high16 v11, 0x100

    #@2
    const/4 v10, 0x1

    #@3
    const/4 v9, 0x0

    #@4
    .line 8626
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@7
    move-result v2

    #@8
    .line 8627
    .local v2, heightMode:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@b
    move-result v3

    #@c
    .line 8628
    .local v3, heightSize:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@f
    move-result v6

    #@10
    .line 8629
    .local v6, widthMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@13
    move-result v7

    #@14
    .line 8631
    .local v7, widthSize:I
    move v4, v3

    #@15
    .line 8632
    .local v4, measuredHeight:I
    move v5, v7

    #@16
    .line 8635
    .local v5, measuredWidth:I
    iget v8, p0, Landroid/webkit/WebViewClassic;->mContentHeight:I

    #@18
    invoke-virtual {p0, v8}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@1b
    move-result v0

    #@1c
    .line 8636
    .local v0, contentHeight:I
    iget v8, p0, Landroid/webkit/WebViewClassic;->mContentWidth:I

    #@1e
    invoke-virtual {p0, v8}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@21
    move-result v1

    #@22
    .line 8640
    .local v1, contentWidth:I
    const/high16 v8, 0x4000

    #@24
    if-eq v2, v8, :cond_49

    #@26
    .line 8641
    iput-boolean v10, p0, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@28
    .line 8642
    move v4, v0

    #@29
    .line 8643
    const/high16 v8, -0x8000

    #@2b
    if-ne v2, v8, :cond_33

    #@2d
    .line 8646
    if-le v4, v3, :cond_33

    #@2f
    .line 8647
    move v4, v3

    #@30
    .line 8648
    iput-boolean v9, p0, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@32
    .line 8649
    or-int/2addr v4, v11

    #@33
    .line 8655
    :cond_33
    :goto_33
    iget v8, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@35
    if-eqz v8, :cond_3c

    #@37
    .line 8656
    iget-boolean v8, p0, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@39
    invoke-direct {p0, v8}, Landroid/webkit/WebViewClassic;->nativeSetHeightCanMeasure(Z)V

    #@3c
    .line 8659
    :cond_3c
    if-nez v6, :cond_4c

    #@3e
    .line 8660
    iput-boolean v10, p0, Landroid/webkit/WebViewClassic;->mWidthCanMeasure:Z

    #@40
    .line 8661
    move v5, v1

    #@41
    .line 8669
    :goto_41
    monitor-enter p0

    #@42
    .line 8670
    :try_start_42
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@44
    invoke-virtual {v8, v5, v4}, Landroid/webkit/WebView$PrivateAccess;->setMeasuredDimension(II)V

    #@47
    .line 8671
    monitor-exit p0
    :try_end_48
    .catchall {:try_start_42 .. :try_end_48} :catchall_52

    #@48
    .line 8672
    return-void

    #@49
    .line 8653
    :cond_49
    iput-boolean v9, p0, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@4b
    goto :goto_33

    #@4c
    .line 8663
    :cond_4c
    if-ge v5, v1, :cond_4f

    #@4e
    .line 8664
    or-int/2addr v5, v11

    #@4f
    .line 8666
    :cond_4f
    iput-boolean v9, p0, Landroid/webkit/WebViewClassic;->mWidthCanMeasure:Z

    #@51
    goto :goto_41

    #@52
    .line 8671
    :catchall_52
    move-exception v8

    #@53
    :try_start_53
    monitor-exit p0
    :try_end_54
    .catchall {:try_start_53 .. :try_end_54} :catchall_52

    #@54
    throw v8
.end method

.method public onOverScrolled(IIZZ)V
    .registers 12
    .parameter "scrollX"
    .parameter "scrollY"
    .parameter "clampedX"
    .parameter "clampedY"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 3881
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@3
    const/16 v1, 0xa

    #@5
    if-ne v0, v1, :cond_b

    #@7
    .line 3882
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewClassic;->scrollEditText(II)V

    #@a
    .line 3913
    :cond_a
    :goto_a
    return-void

    #@b
    .line 3885
    :cond_b
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@d
    const/16 v1, 0x9

    #@f
    if-ne v0, v1, :cond_18

    #@11
    .line 3886
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewClassic;->scrollLayerTo(II)V

    #@14
    .line 3887
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->animateHandles()V

    #@17
    goto :goto_a

    #@18
    .line 3890
    :cond_18
    const/4 v0, 0x0

    #@19
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mInOverScrollMode:Z

    #@1b
    .line 3891
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollX()I

    #@1e
    move-result v5

    #@1f
    .line 3892
    .local v5, maxX:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->computeMaxScrollY()I

    #@22
    move-result v6

    #@23
    .line 3893
    .local v6, maxY:I
    if-nez v5, :cond_51

    #@25
    .line 3895
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic;->pinLocX(I)I

    #@28
    move-result p1

    #@29
    .line 3899
    :cond_29
    :goto_29
    if-ltz p2, :cond_2d

    #@2b
    if-le p2, v6, :cond_2f

    #@2d
    .line 3900
    :cond_2d
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mInOverScrollMode:Z

    #@2f
    .line 3903
    :cond_2f
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@32
    move-result v3

    #@33
    .line 3904
    .local v3, oldX:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@36
    move-result v4

    #@37
    .line 3906
    .local v4, oldY:I
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@39
    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebView$PrivateAccess;->super_scrollTo(II)V

    #@3c
    .line 3908
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->animateHandles()V

    #@3f
    .line 3910
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@41
    if-eqz v0, :cond_a

    #@43
    .line 3911
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@45
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@48
    move-result v1

    #@49
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@4c
    move-result v2

    #@4d
    invoke-virtual/range {v0 .. v6}, Landroid/webkit/OverScrollGlow;->pullGlow(IIIIII)V

    #@50
    goto :goto_a

    #@51
    .line 3896
    .end local v3           #oldX:I
    .end local v4           #oldY:I
    :cond_51
    if-ltz p1, :cond_55

    #@53
    if-le p1, v5, :cond_29

    #@55
    .line 3897
    :cond_55
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mInOverScrollMode:Z

    #@57
    goto :goto_29
.end method

.method onPageFinished(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 4584
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/ZoomManager;->onPageFinished(Ljava/lang/String;)V

    #@5
    .line 4586
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isAccessibilityInjectionEnabled()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 4587
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0, p1}, Landroid/webkit/AccessibilityInjector;->onPageFinished(Ljava/lang/String;)V

    #@12
    .line 4589
    :cond_12
    return-void
.end method

.method onPageStarted(Ljava/lang/String;)V
    .registers 4
    .parameter "url"

    #@0
    .prologue
    .line 4569
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setCertificate(Landroid/net/http/SslCertificate;)V

    #@6
    .line 4571
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isAccessibilityInjectionEnabled()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_13

    #@c
    .line 4572
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0, p1}, Landroid/webkit/AccessibilityInjector;->onPageStarted(Ljava/lang/String;)V

    #@13
    .line 4576
    :cond_13
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@16
    .line 4577
    return-void
.end method

.method public onPause()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 4010
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsPaused:Z

    #@3
    if-nez v0, :cond_2f

    #@5
    .line 4012
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSentAutoScrollMessage:Z

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 4013
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/webkit/WebViewClassic;->mAutoScrollY:I

    #@c
    iput v0, p0, Landroid/webkit/WebViewClassic;->mAutoScrollX:I

    #@e
    .line 4016
    :cond_e
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsPaused:Z

    #@10
    .line 4017
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@12
    const/16 v1, 0x8f

    #@14
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@17
    .line 4020
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHTML5VideoViewProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@19
    if-eqz v0, :cond_20

    #@1b
    .line 4021
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHTML5VideoViewProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@1d
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoViewProxy;->pauseAndDispatch()V

    #@20
    .line 4023
    :cond_20
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@22
    if-eqz v0, :cond_29

    #@24
    .line 4024
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@26
    invoke-static {v0, v2}, Landroid/webkit/WebViewClassic;->nativeSetPauseDrawing(IZ)V

    #@29
    .line 4027
    :cond_29
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->cancelDialogs()V

    #@2c
    .line 4028
    invoke-static {}, Landroid/webkit/WebCoreThreadWatchdog;->pause()V

    #@2f
    .line 4030
    :cond_2f
    return-void
.end method

.method onPinchToZoomAnimationEnd(Landroid/view/ScaleGestureDetector;)V
    .registers 6
    .parameter "detector"

    #@0
    .prologue
    .line 6864
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->onZoomAnimationEnd()V

    #@3
    .line 6869
    const/16 v0, 0x8

    #@5
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@7
    .line 6870
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mConfirmMove:Z

    #@a
    .line 6871
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    #@d
    move-result v0

    #@e
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    #@11
    move-result v1

    #@12
    iget-wide v2, p0, Landroid/webkit/WebViewClassic;->mLastTouchTime:J

    #@14
    invoke-direct {p0, v0, v1, v2, v3}, Landroid/webkit/WebViewClassic;->startTouch(FFJ)V

    #@17
    .line 6872
    return-void
.end method

.method onPinchToZoomAnimationStart()V
    .registers 1

    #@0
    .prologue
    .line 6823
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->cancelTouch()V

    #@3
    .line 6824
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->onZoomAnimationStart()V

    #@6
    .line 6825
    return-void
.end method

.method public onResume()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4053
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsPaused:Z

    #@3
    if-eqz v0, :cond_17

    #@5
    .line 4054
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsPaused:Z

    #@7
    .line 4055
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@9
    const/16 v1, 0x90

    #@b
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@e
    .line 4056
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 4057
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@14
    invoke-static {v0, v2}, Landroid/webkit/WebViewClassic;->nativeSetPauseDrawing(IZ)V

    #@17
    .line 4063
    :cond_17
    invoke-static {}, Landroid/webkit/WebCoreThreadWatchdog;->resume()V

    #@1a
    .line 4066
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@1c
    if-eqz v0, :cond_25

    #@1e
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@20
    if-nez v0, :cond_25

    #@22
    .line 4067
    invoke-direct {p0, v2}, Landroid/webkit/WebViewClassic;->showLGSelectActionPopupWindow(I)V

    #@25
    .line 4070
    :cond_25
    return-void
.end method

.method onSavePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)Z
    .registers 14
    .parameter "schemePlusHost"
    .parameter "username"
    .parameter "password"
    .parameter "resumeMsg"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 2258
    const/4 v3, 0x0

    #@2
    .line 2259
    .local v3, rVal:Z
    if-nez p4, :cond_b

    #@4
    .line 2261
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mDatabase:Landroid/webkit/WebViewDatabaseClassic;

    #@6
    invoke-virtual {v6, p1, p2, p3}, Landroid/webkit/WebViewDatabaseClassic;->setUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@9
    :goto_9
    move v6, v3

    #@a
    .line 2351
    :goto_a
    return v6

    #@b
    .line 2263
    :cond_b
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mResumeMsg:Landroid/os/Message;

    #@d
    if-eqz v7, :cond_1c

    #@f
    .line 2264
    const-string/jumbo v7, "webview"

    #@12
    const-string/jumbo v8, "onSavePassword should not be called while dialog is up"

    #@15
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2265
    invoke-virtual {p4}, Landroid/os/Message;->sendToTarget()V

    #@1b
    goto :goto_a

    #@1c
    .line 2268
    :cond_1c
    iput-object p4, p0, Landroid/webkit/WebViewClassic;->mResumeMsg:Landroid/os/Message;

    #@1e
    .line 2269
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@20
    invoke-virtual {v7, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@23
    move-result-object v4

    #@24
    .line 2271
    .local v4, remember:Landroid/os/Message;
    invoke-virtual {v4}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@27
    move-result-object v7

    #@28
    const-string v8, "host"

    #@2a
    invoke-virtual {v7, v8, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 2272
    invoke-virtual {v4}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@30
    move-result-object v7

    #@31
    const-string/jumbo v8, "username"

    #@34
    invoke-virtual {v7, v8, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 2273
    invoke-virtual {v4}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@3a
    move-result-object v7

    #@3b
    const-string/jumbo v8, "password"

    #@3e
    invoke-virtual {v7, v8, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    .line 2274
    iput-object p4, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@43
    .line 2276
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@45
    const/4 v8, 0x2

    #@46
    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@49
    move-result-object v1

    #@4a
    .line 2278
    .local v1, neverRemember:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@4d
    move-result-object v7

    #@4e
    const-string v8, "host"

    #@50
    invoke-virtual {v7, v8, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    .line 2279
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@56
    move-result-object v7

    #@57
    const-string/jumbo v8, "username"

    #@5a
    invoke-virtual {v7, v8, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    .line 2280
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@60
    move-result-object v7

    #@61
    const-string/jumbo v8, "password"

    #@64
    invoke-virtual {v7, v8, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@67
    .line 2281
    iput-object p4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@69
    .line 2285
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@6c
    move-result-object v5

    #@6d
    .line 2286
    .local v5, settings:Landroid/webkit/WebSettings;
    if-eqz v5, :cond_ef

    #@6f
    invoke-virtual {v5}, Landroid/webkit/WebSettingsClassic;->getFloatingMode()Z

    #@72
    move-result v7

    #@73
    if-eqz v7, :cond_ef

    #@75
    .line 2287
    new-instance v2, Landroid/util/TypedValue;

    #@77
    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    #@7a
    .line 2288
    .local v2, outValue:Landroid/util/TypedValue;
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@7c
    invoke-virtual {v7}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@7f
    move-result-object v7

    #@80
    const v8, 0x1010309

    #@83
    invoke-virtual {v7, v8, v2, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@86
    .line 2290
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@88
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@8a
    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@8d
    move-result-object v6

    #@8e
    iget v7, v2, Landroid/util/TypedValue;->resourceId:I

    #@90
    invoke-direct {v0, v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@93
    .line 2295
    .end local v2           #outValue:Landroid/util/TypedValue;
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    :goto_93
    const v6, 0x104036c

    #@96
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@99
    move-result-object v6

    #@9a
    const v7, 0x10403bd

    #@9d
    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@a0
    move-result-object v6

    #@a1
    const v7, 0x10403be

    #@a4
    new-instance v8, Landroid/webkit/WebViewClassic$5;

    #@a6
    invoke-direct {v8, p0, p4}, Landroid/webkit/WebViewClassic$5;-><init>(Landroid/webkit/WebViewClassic;Landroid/os/Message;)V

    #@a9
    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@ac
    move-result-object v6

    #@ad
    const v7, 0x10403bf

    #@b0
    new-instance v8, Landroid/webkit/WebViewClassic$4;

    #@b2
    invoke-direct {v8, p0, v4}, Landroid/webkit/WebViewClassic$4;-><init>(Landroid/webkit/WebViewClassic;Landroid/os/Message;)V

    #@b5
    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@b8
    move-result-object v6

    #@b9
    const v7, 0x10403c0

    #@bc
    new-instance v8, Landroid/webkit/WebViewClassic$3;

    #@be
    invoke-direct {v8, p0, v1}, Landroid/webkit/WebViewClassic$3;-><init>(Landroid/webkit/WebViewClassic;Landroid/os/Message;)V

    #@c1
    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@c4
    move-result-object v6

    #@c5
    new-instance v7, Landroid/webkit/WebViewClassic$2;

    #@c7
    invoke-direct {v7, p0, p4}, Landroid/webkit/WebViewClassic$2;-><init>(Landroid/webkit/WebViewClassic;Landroid/os/Message;)V

    #@ca
    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    #@cd
    move-result-object v6

    #@ce
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@d1
    move-result-object v6

    #@d2
    iput-object v6, p0, Landroid/webkit/WebViewClassic;->mSavePasswordDialog:Landroid/app/AlertDialog;

    #@d4
    .line 2341
    if-eqz v5, :cond_e7

    #@d6
    invoke-virtual {v5}, Landroid/webkit/WebSettingsClassic;->getFloatingMode()Z

    #@d9
    move-result v6

    #@da
    if-eqz v6, :cond_e7

    #@dc
    .line 2342
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSavePasswordDialog:Landroid/app/AlertDialog;

    #@de
    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@e1
    move-result-object v6

    #@e2
    const/16 v7, 0x7d2

    #@e4
    invoke-virtual {v6, v7}, Landroid/view/Window;->setType(I)V

    #@e7
    .line 2344
    :cond_e7
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mSavePasswordDialog:Landroid/app/AlertDialog;

    #@e9
    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    #@ec
    .line 2349
    const/4 v3, 0x1

    #@ed
    goto/16 :goto_9

    #@ef
    .line 2293
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :cond_ef
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@f1
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@f3
    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@f6
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    goto :goto_93
.end method

.method public onScrollChanged(IIII)V
    .registers 9
    .parameter "l"
    .parameter "t"
    .parameter "oldl"
    .parameter "oldt"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 6742
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSendScroll:Landroid/webkit/WebViewClassic$SendScrollToWebCore;

    #@3
    invoke-virtual {v1, v3}, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->send(Z)V

    #@6
    .line 6744
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mInOverScrollMode:Z

    #@8
    if-nez v1, :cond_1f

    #@a
    .line 6747
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@d
    move-result v0

    #@e
    .line 6748
    .local v0, titleHeight:I
    sub-int v1, v0, p2

    #@10
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    #@13
    move-result v1

    #@14
    sub-int v2, v0, p4

    #@16
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@19
    move-result v2

    #@1a
    if-eq v1, v2, :cond_1f

    #@1c
    .line 6749
    invoke-virtual {p0, v3}, Landroid/webkit/WebViewClassic;->sendViewSizeZoom(Z)Z

    #@1f
    .line 6752
    .end local v0           #titleHeight:I
    :cond_1f
    return-void
.end method

.method public onSizeChanged(IIII)V
    .registers 8
    .parameter "w"
    .parameter "h"
    .parameter "ow"
    .parameter "oh"

    #@0
    .prologue
    .line 6633
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    #@3
    move-result v1

    #@4
    int-to-float v1, v1

    #@5
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@7
    invoke-virtual {v2}, Landroid/webkit/ZoomManager;->getDefaultMinZoomScale()F

    #@a
    move-result v2

    #@b
    div-float/2addr v1, v2

    #@c
    float-to-int v0, v1

    #@d
    .line 6634
    .local v0, newMaxViewportWidth:I
    sget v1, Landroid/webkit/WebViewClassic;->sMaxViewportWidth:I

    #@f
    if-le v0, v1, :cond_13

    #@11
    .line 6635
    sput v0, Landroid/webkit/WebViewClassic;->sMaxViewportWidth:I

    #@13
    .line 6638
    :cond_13
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@15
    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/webkit/ZoomManager;->onSizeChanged(IIII)V

    #@18
    .line 6640
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mLoadedPicture:Landroid/webkit/WebViewCore$DrawData;

    #@1a
    if-eqz v1, :cond_26

    #@1c
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mDelaySetPicture:Landroid/webkit/WebViewCore$DrawData;

    #@1e
    if-nez v1, :cond_26

    #@20
    .line 6644
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mLoadedPicture:Landroid/webkit/WebViewCore$DrawData;

    #@22
    const/4 v2, 0x0

    #@23
    invoke-virtual {p0, v1, v2}, Landroid/webkit/WebViewClassic;->setNewPicture(Landroid/webkit/WebViewCore$DrawData;Z)V

    #@26
    .line 6646
    :cond_26
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@28
    if-eqz v1, :cond_2d

    #@2a
    .line 6647
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->scrollEditIntoView()V

    #@2d
    .line 6649
    :cond_2d
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->relocateAutoCompletePopup()V

    #@30
    .line 6650
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "ev"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 6911
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@3
    if-eqz v1, :cond_15

    #@5
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@7
    invoke-virtual {v1}, Landroid/webkit/WebView;->isClickable()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_16

    #@d
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@f
    invoke-virtual {v1}, Landroid/webkit/WebView;->isLongClickable()Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_16

    #@15
    .line 6930
    :cond_15
    :goto_15
    return v0

    #@16
    .line 6915
    :cond_16
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@18
    if-eqz v1, :cond_15

    #@1a
    .line 6919
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@1c
    invoke-virtual {v1}, Landroid/webkit/WebView;->isFocusable()Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_37

    #@22
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@24
    invoke-virtual {v1}, Landroid/webkit/WebView;->isFocusableInTouchMode()Z

    #@27
    move-result v1

    #@28
    if-eqz v1, :cond_37

    #@2a
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2c
    invoke-virtual {v1}, Landroid/webkit/WebView;->isFocused()Z

    #@2f
    move-result v1

    #@30
    if-nez v1, :cond_37

    #@32
    .line 6921
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@34
    invoke-virtual {v1}, Landroid/webkit/WebView;->requestFocus()Z

    #@37
    .line 6924
    :cond_37
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@39
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@3c
    move-result v2

    #@3d
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@40
    move-result v3

    #@41
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@44
    move-result v4

    #@45
    sub-int/2addr v3, v4

    #@46
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@48
    invoke-virtual {v4}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@4b
    move-result v4

    #@4c
    invoke-virtual {v1, p1, v2, v3, v4}, Landroid/webkit/WebViewInputDispatcher;->postPointerEvent(Landroid/view/MotionEvent;IIF)Z

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_59

    #@52
    .line 6926
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mInputDispatcher:Landroid/webkit/WebViewInputDispatcher;

    #@54
    invoke-virtual {v0}, Landroid/webkit/WebViewInputDispatcher;->dispatchUiEvents()V

    #@57
    .line 6927
    const/4 v0, 0x1

    #@58
    goto :goto_15

    #@59
    .line 6929
    :cond_59
    const-string/jumbo v1, "webview"

    #@5c
    const-string/jumbo v2, "mInputDispatcher rejected the event!"

    #@5f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    goto :goto_15
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "ev"

    #@0
    .prologue
    const-wide/16 v6, 0xc8

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, 0x1

    #@5
    .line 7937
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@8
    move-result-wide v0

    #@9
    .line 7938
    .local v0, time:J
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@c
    move-result v4

    #@d
    and-int/lit8 v4, v4, 0x2

    #@f
    if-eqz v4, :cond_28

    #@11
    .line 7939
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@14
    move-result v3

    #@15
    cmpl-float v3, v3, v5

    #@17
    if-lez v3, :cond_1c

    #@19
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->pageDown(Z)Z

    #@1c
    .line 7940
    :cond_1c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@1f
    move-result v3

    #@20
    cmpg-float v3, v3, v5

    #@22
    if-gez v3, :cond_27

    #@24
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->pageUp(Z)Z

    #@27
    .line 8006
    :cond_27
    :goto_27
    return v2

    #@28
    .line 7943
    :cond_28
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@2b
    move-result v4

    #@2c
    if-nez v4, :cond_49

    #@2e
    .line 7944
    iget-boolean v4, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@30
    if-nez v4, :cond_27

    #@32
    .line 7947
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mTrackballDown:Z

    #@34
    .line 7948
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@36
    if-nez v2, :cond_3a

    #@38
    move v2, v3

    #@39
    .line 7949
    goto :goto_27

    #@3a
    .line 7956
    :cond_3a
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@3c
    invoke-virtual {v2}, Landroid/webkit/WebView;->isInTouchMode()Z

    #@3f
    move-result v2

    #@40
    if-eqz v2, :cond_47

    #@42
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@44
    invoke-virtual {v2}, Landroid/webkit/WebView;->requestFocusFromTouch()Z

    #@47
    :cond_47
    move v2, v3

    #@48
    .line 7957
    goto :goto_27

    #@49
    .line 7959
    :cond_49
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@4c
    move-result v4

    #@4d
    if-ne v4, v2, :cond_67

    #@4f
    .line 7961
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@51
    const/16 v5, 0x72

    #@53
    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    #@56
    .line 7962
    iput-boolean v3, p0, Landroid/webkit/WebViewClassic;->mTrackballDown:Z

    #@58
    .line 7963
    iput-wide v0, p0, Landroid/webkit/WebViewClassic;->mTrackballUpTime:J

    #@5a
    .line 7964
    iget-boolean v4, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@5c
    if-eqz v4, :cond_65

    #@5e
    .line 7965
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->copySelection()Z

    #@61
    .line 7966
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@64
    goto :goto_27

    #@65
    :cond_65
    move v2, v3

    #@66
    .line 7974
    goto :goto_27

    #@67
    .line 7976
    :cond_67
    iget-boolean v4, p0, Landroid/webkit/WebViewClassic;->mMapTrackballToArrowKeys:Z

    #@69
    if-eqz v4, :cond_73

    #@6b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@6e
    move-result v4

    #@6f
    and-int/lit8 v4, v4, 0x1

    #@71
    if-eqz v4, :cond_7f

    #@73
    :cond_73
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@75
    invoke-static {v4}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@7c
    move-result v4

    #@7d
    if-eqz v4, :cond_81

    #@7f
    :cond_7f
    move v2, v3

    #@80
    .line 7979
    goto :goto_27

    #@81
    .line 7981
    :cond_81
    iget-boolean v4, p0, Landroid/webkit/WebViewClassic;->mTrackballDown:Z

    #@83
    if-nez v4, :cond_27

    #@85
    .line 7985
    iget-wide v4, p0, Landroid/webkit/WebViewClassic;->mTrackballUpTime:J

    #@87
    sub-long v4, v0, v4

    #@89
    cmp-long v4, v4, v6

    #@8b
    if-ltz v4, :cond_27

    #@8d
    .line 7990
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@90
    .line 7991
    iget-wide v4, p0, Landroid/webkit/WebViewClassic;->mTrackballLastTime:J

    #@92
    sub-long v4, v0, v4

    #@94
    cmp-long v4, v4, v6

    #@96
    if-lez v4, :cond_9e

    #@98
    .line 7996
    iput-wide v0, p0, Landroid/webkit/WebViewClassic;->mTrackballFirstTime:J

    #@9a
    .line 7997
    iput v3, p0, Landroid/webkit/WebViewClassic;->mTrackballYMove:I

    #@9c
    iput v3, p0, Landroid/webkit/WebViewClassic;->mTrackballXMove:I

    #@9e
    .line 7999
    :cond_9e
    iput-wide v0, p0, Landroid/webkit/WebViewClassic;->mTrackballLastTime:J

    #@a0
    .line 8003
    iget v3, p0, Landroid/webkit/WebViewClassic;->mTrackballRemainsX:F

    #@a2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@a5
    move-result v4

    #@a6
    add-float/2addr v3, v4

    #@a7
    iput v3, p0, Landroid/webkit/WebViewClassic;->mTrackballRemainsX:F

    #@a9
    .line 8004
    iget v3, p0, Landroid/webkit/WebViewClassic;->mTrackballRemainsY:F

    #@ab
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@ae
    move-result v4

    #@af
    add-float/2addr v3, v4

    #@b0
    iput v3, p0, Landroid/webkit/WebViewClassic;->mTrackballRemainsY:F

    #@b2
    .line 8005
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@b5
    move-result v3

    #@b6
    invoke-direct {p0, v0, v1, v3}, Landroid/webkit/WebViewClassic;->doTrackball(JI)V

    #@b9
    goto/16 :goto_27
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .registers 4
    .parameter "changedView"
    .parameter "visibility"

    #@0
    .prologue
    .line 6461
    if-eqz p2, :cond_b

    #@2
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 6462
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@8
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->dismissZoomPicker()V

    #@b
    .line 6464
    :cond_b
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->updateDrawingState()V

    #@e
    .line 6465
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 4
    .parameter "hasWindowFocus"

    #@0
    .prologue
    .line 6501
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic;->setActive(Z)V

    #@3
    .line 6502
    if-eqz p1, :cond_15

    #@5
    .line 6503
    invoke-static {p0}, Landroid/webkit/JWebCoreJavaBridge;->setActiveWebView(Landroid/webkit/WebViewClassic;)V

    #@8
    .line 6504
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mPictureUpdatePausedForFocusChange:Z

    #@a
    if-eqz v1, :cond_14

    #@c
    .line 6505
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@e
    invoke-static {v1}, Landroid/webkit/WebViewCore;->resumeUpdatePicture(Landroid/webkit/WebViewCore;)V

    #@11
    .line 6506
    const/4 v1, 0x0

    #@12
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mPictureUpdatePausedForFocusChange:Z

    #@14
    .line 6517
    :cond_14
    :goto_14
    return-void

    #@15
    .line 6509
    :cond_15
    invoke-static {p0}, Landroid/webkit/JWebCoreJavaBridge;->removeActiveWebView(Landroid/webkit/WebViewClassic;)V

    #@18
    .line 6510
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@1b
    move-result-object v0

    #@1c
    .line 6511
    .local v0, settings:Landroid/webkit/WebSettings;
    if-eqz v0, :cond_14

    #@1e
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->enableSmoothTransition()Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_14

    #@24
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@26
    if-eqz v1, :cond_14

    #@28
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2a
    invoke-static {v1}, Landroid/webkit/WebViewCore;->isUpdatePicturePaused(Landroid/webkit/WebViewCore;)Z

    #@2d
    move-result v1

    #@2e
    if-nez v1, :cond_14

    #@30
    .line 6513
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@32
    invoke-static {v1}, Landroid/webkit/WebViewCore;->pauseUpdatePicture(Landroid/webkit/WebViewCore;)V

    #@35
    .line 6514
    const/4 v1, 0x1

    #@36
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mPictureUpdatePausedForFocusChange:Z

    #@38
    goto :goto_14
.end method

.method public onWindowVisibilityChanged(I)V
    .registers 2
    .parameter "visibility"

    #@0
    .prologue
    .line 4034
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->updateDrawingState()V

    #@3
    .line 4035
    return-void
.end method

.method public overlayHorizontalScrollbar()Z
    .registers 2

    #@0
    .prologue
    .line 2385
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mOverlayHorizontalScrollbar:Z

    #@2
    return v0
.end method

.method public overlayVerticalScrollbar()Z
    .registers 2

    #@0
    .prologue
    .line 2393
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mOverlayVerticalScrollbar:Z

    #@2
    return v0
.end method

.method public pageDown(Z)Z
    .registers 8
    .parameter "bottom"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 3293
    iget v3, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@4
    if-nez v3, :cond_7

    #@6
    .line 3307
    :goto_6
    return v2

    #@7
    .line 3296
    :cond_7
    if-eqz p1, :cond_16

    #@9
    .line 3297
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@c
    move-result v3

    #@d
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->computeRealVerticalScrollRange()I

    #@10
    move-result v4

    #@11
    invoke-direct {p0, v3, v4, v5, v2}, Landroid/webkit/WebViewClassic;->pinScrollTo(IIZI)Z

    #@14
    move-result v2

    #@15
    goto :goto_6

    #@16
    .line 3300
    :cond_16
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@19
    move-result v0

    #@1a
    .line 3302
    .local v0, h:I
    const/16 v3, 0x30

    #@1c
    if-le v0, v3, :cond_2d

    #@1e
    .line 3303
    add-int/lit8 v1, v0, -0x18

    #@20
    .line 3307
    .local v1, y:I
    :goto_20
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@22
    invoke-virtual {v3}, Landroid/widget/OverScroller;->isFinished()Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_30

    #@28
    invoke-direct {p0, v2, v1, v5, v2}, Landroid/webkit/WebViewClassic;->pinScrollBy(IIZI)Z

    #@2b
    move-result v2

    #@2c
    goto :goto_6

    #@2d
    .line 3305
    .end local v1           #y:I
    :cond_2d
    div-int/lit8 v1, v0, 0x2

    #@2f
    .restart local v1       #y:I
    goto :goto_20

    #@30
    .line 3307
    :cond_30
    invoke-direct {p0, v1}, Landroid/webkit/WebViewClassic;->extendScroll(I)Z

    #@33
    move-result v2

    #@34
    goto :goto_6
.end method

.method protected pageSwapCallback(Z)V
    .registers 5
    .parameter "notifyAnimationStarted"

    #@0
    .prologue
    .line 9580
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->resumeWebKitDraw()V

    #@5
    .line 9581
    if-eqz p1, :cond_e

    #@7
    .line 9582
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@9
    const/16 v1, 0xc4

    #@b
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@e
    .line 9584
    :cond_e
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@10
    instance-of v0, v0, Landroid/webkit/WebViewClassic$PageSwapDelegate;

    #@12
    if-eqz v0, :cond_1b

    #@14
    .line 9586
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@16
    check-cast v0, Landroid/webkit/WebViewClassic$PageSwapDelegate;

    #@18
    invoke-interface {v0, p1}, Landroid/webkit/WebViewClassic$PageSwapDelegate;->onPageSwapOccurred(Z)V

    #@1b
    .line 9589
    :cond_1b
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    #@1d
    if-eqz v0, :cond_2c

    #@1f
    .line 9592
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    #@21
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->capturePicture()Landroid/graphics/Picture;

    #@28
    move-result-object v2

    #@29
    invoke-interface {v0, v1, v2}, Landroid/webkit/WebView$PictureListener;->onNewPicture(Landroid/webkit/WebView;Landroid/graphics/Picture;)V

    #@2c
    .line 9594
    :cond_2c
    return-void
.end method

.method public pageUp(Z)Z
    .registers 7
    .parameter "top"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 3269
    iget v3, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@4
    if-nez v3, :cond_7

    #@6
    .line 3284
    :goto_6
    return v2

    #@7
    .line 3272
    :cond_7
    if-eqz p1, :cond_12

    #@9
    .line 3274
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@c
    move-result v3

    #@d
    invoke-direct {p0, v3, v2, v4, v2}, Landroid/webkit/WebViewClassic;->pinScrollTo(IIZI)Z

    #@10
    move-result v2

    #@11
    goto :goto_6

    #@12
    .line 3277
    :cond_12
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@15
    move-result v0

    #@16
    .line 3279
    .local v0, h:I
    const/16 v3, 0x30

    #@18
    if-le v0, v3, :cond_2a

    #@1a
    .line 3280
    neg-int v3, v0

    #@1b
    add-int/lit8 v1, v3, 0x18

    #@1d
    .line 3284
    .local v1, y:I
    :goto_1d
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@1f
    invoke-virtual {v3}, Landroid/widget/OverScroller;->isFinished()Z

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_2e

    #@25
    invoke-direct {p0, v2, v1, v4, v2}, Landroid/webkit/WebViewClassic;->pinScrollBy(IIZI)Z

    #@28
    move-result v2

    #@29
    goto :goto_6

    #@2a
    .line 3282
    .end local v1           #y:I
    :cond_2a
    neg-int v3, v0

    #@2b
    div-int/lit8 v1, v3, 0x2

    #@2d
    .restart local v1       #y:I
    goto :goto_1d

    #@2e
    .line 3284
    :cond_2e
    invoke-direct {p0, v1}, Landroid/webkit/WebViewClassic;->extendScroll(I)Z

    #@31
    move-result v2

    #@32
    goto :goto_6
.end method

.method passToJavaScript(Ljava/lang/String;Landroid/view/KeyEvent;)V
    .registers 9
    .parameter "currentText"
    .parameter "event"

    #@0
    .prologue
    const/16 v5, 0x80

    #@2
    .line 8750
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@4
    if-nez v1, :cond_7

    #@6
    .line 8765
    :goto_6
    return-void

    #@7
    .line 8753
    :cond_7
    new-instance v0, Landroid/webkit/WebViewCore$JSKeyData;

    #@9
    invoke-direct {v0}, Landroid/webkit/WebViewCore$JSKeyData;-><init>()V

    #@c
    .line 8754
    .local v0, arg:Landroid/webkit/WebViewCore$JSKeyData;
    iput-object p2, v0, Landroid/webkit/WebViewCore$JSKeyData;->mEvent:Landroid/view/KeyEvent;

    #@e
    .line 8755
    iput-object p1, v0, Landroid/webkit/WebViewCore$JSKeyData;->mCurrentText:Ljava/lang/String;

    #@10
    .line 8757
    iget v1, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@12
    add-int/lit8 v1, v1, 0x1

    #@14
    iput v1, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@16
    .line 8758
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@18
    const/16 v2, 0x73

    #@1a
    iget v3, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@1c
    const/4 v4, 0x0

    #@1d
    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/webkit/WebViewCore;->sendMessage(IIILjava/lang/Object;)V

    #@20
    .line 8763
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@22
    invoke-virtual {v1, v5}, Landroid/webkit/WebViewCore;->removeMessages(I)V

    #@25
    .line 8764
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@27
    const/4 v2, 0x0

    #@28
    const-wide/16 v3, 0x3e8

    #@2a
    invoke-virtual {v1, v5, v2, v3, v4}, Landroid/webkit/WebViewCore;->sendMessageDelayed(ILjava/lang/Object;J)V

    #@2d
    goto :goto_6
.end method

.method public pasteFromClipboard()V
    .registers 7

    #@0
    .prologue
    .line 6383
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@2
    const-string v5, "clipboard"

    #@4
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Landroid/content/ClipboardManager;

    #@a
    .line 6385
    .local v2, cm:Landroid/content/ClipboardManager;
    invoke-virtual {v2}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    #@d
    move-result-object v0

    #@e
    .line 6386
    .local v0, clipData:Landroid/content/ClipData;
    if-eqz v0, :cond_2c

    #@10
    .line 6387
    const/4 v4, 0x0

    #@11
    invoke-virtual {v0, v4}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@14
    move-result-object v1

    #@15
    .line 6388
    .local v1, clipItem:Landroid/content/ClipData$Item;
    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@18
    move-result-object v3

    #@19
    .line 6390
    .local v3, pasteText:Ljava/lang/CharSequence;
    if-nez v3, :cond_23

    #@1b
    .line 6391
    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@1e
    move-result-object v4

    #@1f
    if-nez v4, :cond_2d

    #@21
    const-string v3, ""

    #@23
    .line 6393
    :cond_23
    :goto_23
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@25
    if-eqz v4, :cond_2c

    #@27
    .line 6394
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@29
    invoke-virtual {v4, v3}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->replaceSelection(Ljava/lang/CharSequence;)V

    #@2c
    .line 6397
    .end local v1           #clipItem:Landroid/content/ClipData$Item;
    .end local v3           #pasteText:Ljava/lang/CharSequence;
    :cond_2c
    return-void

    #@2d
    .line 6391
    .restart local v1       #clipItem:Landroid/content/ClipData$Item;
    .restart local v3       #pasteText:Ljava/lang/CharSequence;
    :cond_2d
    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    goto :goto_23
.end method

.method public pauseTimers()V
    .registers 3

    #@0
    .prologue
    .line 3994
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0x6d

    #@4
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@7
    .line 3995
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 12
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 2124
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@4
    invoke-virtual {v7}, Landroid/webkit/WebView;->isEnabled()Z

    #@7
    move-result v7

    #@8
    if-nez v7, :cond_11

    #@a
    .line 2126
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@c
    invoke-virtual {v5, p1, p2}, Landroid/webkit/WebView$PrivateAccess;->super_performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@f
    move-result v5

    #@10
    .line 2154
    :goto_10
    return v5

    #@11
    .line 2129
    :cond_11
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@14
    move-result-object v7

    #@15
    invoke-virtual {v7, p1}, Landroid/webkit/AccessibilityInjector;->supportsAccessibilityAction(I)Z

    #@18
    move-result v7

    #@19
    if-eqz v7, :cond_24

    #@1b
    .line 2130
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5, p1, p2}, Landroid/webkit/AccessibilityInjector;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@22
    move-result v5

    #@23
    goto :goto_10

    #@24
    .line 2133
    :cond_24
    sparse-switch p1, :sswitch_data_7e

    #@27
    .line 2154
    iget-object v5, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@29
    invoke-virtual {v5, p1, p2}, Landroid/webkit/WebView$PrivateAccess;->super_performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@2c
    move-result v5

    #@2d
    goto :goto_10

    #@2e
    .line 2136
    :sswitch_2e
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getContentHeight()I

    #@31
    move-result v7

    #@32
    invoke-virtual {p0, v7}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@35
    move-result v3

    #@36
    .line 2137
    .local v3, convertedContentHeight:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getHeight()I

    #@39
    move-result v7

    #@3a
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@3c
    invoke-virtual {v8}, Landroid/webkit/WebView;->getPaddingTop()I

    #@3f
    move-result v8

    #@40
    sub-int/2addr v7, v8

    #@41
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@43
    invoke-virtual {v8}, Landroid/webkit/WebView;->getPaddingBottom()I

    #@46
    move-result v8

    #@47
    sub-int v0, v7, v8

    #@49
    .line 2139
    .local v0, adjustedViewHeight:I
    sub-int v7, v3, v0

    #@4b
    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    #@4e
    move-result v4

    #@4f
    .line 2140
    .local v4, maxScrollY:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@52
    move-result v7

    #@53
    if-lez v7, :cond_6a

    #@55
    move v1, v5

    #@56
    .line 2141
    .local v1, canScrollBackward:Z
    :goto_56
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@59
    move-result v7

    #@5a
    sub-int/2addr v7, v4

    #@5b
    if-lez v7, :cond_6c

    #@5d
    move v2, v5

    #@5e
    .line 2142
    .local v2, canScrollForward:Z
    :goto_5e
    const/16 v7, 0x2000

    #@60
    if-ne p1, v7, :cond_6e

    #@62
    if-eqz v1, :cond_6e

    #@64
    .line 2143
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@66
    invoke-virtual {v7, v6, v0}, Landroid/webkit/WebView;->scrollBy(II)V

    #@69
    goto :goto_10

    #@6a
    .end local v1           #canScrollBackward:Z
    .end local v2           #canScrollForward:Z
    :cond_6a
    move v1, v6

    #@6b
    .line 2140
    goto :goto_56

    #@6c
    .restart local v1       #canScrollBackward:Z
    :cond_6c
    move v2, v6

    #@6d
    .line 2141
    goto :goto_5e

    #@6e
    .line 2146
    .restart local v2       #canScrollForward:Z
    :cond_6e
    const/16 v7, 0x1000

    #@70
    if-ne p1, v7, :cond_7b

    #@72
    if-eqz v2, :cond_7b

    #@74
    .line 2147
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@76
    neg-int v8, v0

    #@77
    invoke-virtual {v7, v6, v8}, Landroid/webkit/WebView;->scrollBy(II)V

    #@7a
    goto :goto_10

    #@7b
    :cond_7b
    move v5, v6

    #@7c
    .line 2150
    goto :goto_10

    #@7d
    .line 2133
    nop

    #@7e
    :sswitch_data_7e
    .sparse-switch
        0x1000 -> :sswitch_2e
        0x2000 -> :sswitch_2e
    .end sparse-switch
.end method

.method public performLongClick()Z
    .registers 15

    #@0
    .prologue
    .line 5005
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@2
    invoke-virtual {v10}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    #@5
    move-result-object v10

    #@6
    if-nez v10, :cond_a

    #@8
    const/4 v5, 0x0

    #@9
    .line 5060
    :cond_9
    :goto_9
    return v5

    #@a
    .line 5009
    :cond_a
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@c
    invoke-virtual {v10}, Landroid/webkit/ZoomManager;->getScaleGestureDetector()Landroid/view/ScaleGestureDetector;

    #@f
    move-result-object v3

    #@10
    .line 5010
    .local v3, detector:Landroid/view/ScaleGestureDetector;
    if-eqz v3, :cond_1a

    #@12
    invoke-virtual {v3}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    #@15
    move-result v10

    #@16
    if-eqz v10, :cond_1a

    #@18
    .line 5011
    const/4 v5, 0x0

    #@19
    goto :goto_9

    #@1a
    .line 5015
    :cond_1a
    iget-boolean v10, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@1c
    if-eqz v10, :cond_81

    #@1e
    .line 5016
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@21
    move-result-object v7

    #@22
    .line 5017
    .local v7, settings:Landroid/webkit/WebSettings;
    if-eqz v7, :cond_7f

    #@24
    invoke-virtual {v7}, Landroid/webkit/WebSettingsClassic;->getTextDragDropEnabled()Z

    #@27
    move-result v10

    #@28
    if-eqz v10, :cond_7f

    #@2a
    .line 5018
    new-instance v6, Landroid/graphics/Rect;

    #@2c
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@2e
    iget v10, v10, Landroid/graphics/Point;->x:I

    #@30
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mParagraphTopLeft:Landroid/graphics/Point;

    #@32
    iget v11, v11, Landroid/graphics/Point;->y:I

    #@34
    iget-object v12, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@36
    iget v12, v12, Landroid/graphics/Point;->x:I

    #@38
    iget-object v13, p0, Landroid/webkit/WebViewClassic;->mParagraphBottomRight:Landroid/graphics/Point;

    #@3a
    iget v13, v13, Landroid/graphics/Point;->y:I

    #@3c
    invoke-direct {v6, v10, v11, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    #@3f
    .line 5020
    .local v6, selectionRect:Landroid/graphics/Rect;
    iget v10, p0, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@41
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@44
    move-result v11

    #@45
    add-int/2addr v10, v11

    #@46
    invoke-virtual {p0, v10}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@49
    move-result v0

    #@4a
    .line 5021
    .local v0, contentX:I
    iget v10, p0, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@4c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@4f
    move-result v11

    #@50
    add-int/2addr v10, v11

    #@51
    invoke-virtual {p0, v10}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@54
    move-result v1

    #@55
    .line 5022
    .local v1, contentY:I
    invoke-virtual {v6, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    #@58
    move-result v10

    #@59
    if-eqz v10, :cond_77

    #@5b
    .line 5023
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@5e
    move-result-object v9

    #@5f
    .line 5024
    .local v9, textSelected:Ljava/lang/String;
    const/4 v10, 0x0

    #@60
    invoke-static {v10, v9}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@63
    move-result-object v2

    #@64
    .line 5026
    .local v2, data:Landroid/content/ClipData;
    :try_start_64
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@66
    invoke-static {v10, v9}, Landroid/webkit/LGTextDragShadowBuilder;->fromCharSequence(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/view/View$DragShadowBuilder;

    #@69
    move-result-object v8

    #@6a
    .line 5028
    .local v8, shadowBuilder:Landroid/view/View$DragShadowBuilder;
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@6c
    const/4 v11, 0x0

    #@6d
    const/4 v12, 0x1

    #@6e
    invoke-virtual {v10, v2, v8, v11, v12}, Landroid/webkit/WebView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    #@71
    .line 5029
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@73
    const/4 v11, 0x0

    #@74
    invoke-virtual {v10, v11}, Landroid/webkit/WebView;->performHapticFeedback(I)Z
    :try_end_77
    .catch Ljava/lang/IllegalArgumentException; {:try_start_64 .. :try_end_77} :catch_79

    #@77
    .line 5035
    .end local v2           #data:Landroid/content/ClipData;
    .end local v8           #shadowBuilder:Landroid/view/View$DragShadowBuilder;
    .end local v9           #textSelected:Ljava/lang/String;
    :cond_77
    const/4 v5, 0x1

    #@78
    goto :goto_9

    #@79
    .line 5030
    .restart local v2       #data:Landroid/content/ClipData;
    .restart local v9       #textSelected:Ljava/lang/String;
    :catch_79
    move-exception v4

    #@7a
    .line 5031
    .local v4, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@7d
    .line 5032
    const/4 v5, 0x0

    #@7e
    goto :goto_9

    #@7f
    .line 5037
    .end local v0           #contentX:I
    .end local v1           #contentY:I
    .end local v2           #data:Landroid/content/ClipData;
    .end local v4           #e:Ljava/lang/IllegalArgumentException;
    .end local v6           #selectionRect:Landroid/graphics/Rect;
    .end local v9           #textSelected:Ljava/lang/String;
    :cond_7f
    const/4 v5, 0x0

    #@80
    goto :goto_9

    #@81
    .line 5044
    .end local v7           #settings:Landroid/webkit/WebSettings;
    :cond_81
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@83
    invoke-virtual {v10}, Landroid/webkit/WebView$PrivateAccess;->super_performLongClick()Z

    #@86
    move-result v10

    #@87
    if-eqz v10, :cond_8c

    #@89
    .line 5045
    const/4 v5, 0x1

    #@8a
    goto/16 :goto_9

    #@8c
    .line 5051
    :cond_8c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectText()Z

    #@8f
    move-result v5

    #@90
    .line 5052
    .local v5, isSelecting:Z
    if-eqz v5, :cond_9a

    #@92
    .line 5053
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@94
    const/4 v11, 0x0

    #@95
    invoke-virtual {v10, v11}, Landroid/webkit/WebView;->performHapticFeedback(I)Z

    #@98
    goto/16 :goto_9

    #@9a
    .line 5054
    :cond_9a
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->focusCandidateIsEditableText()Z

    #@9d
    move-result v10

    #@9e
    if-eqz v10, :cond_9

    #@a0
    .line 5055
    new-instance v10, Landroid/webkit/SelectActionModeCallback;

    #@a2
    invoke-direct {v10}, Landroid/webkit/SelectActionModeCallback;-><init>()V

    #@a5
    iput-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@a7
    .line 5056
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@a9
    invoke-virtual {v10, p0}, Landroid/webkit/SelectActionModeCallback;->setWebView(Landroid/webkit/WebViewClassic;)V

    #@ac
    .line 5057
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@ae
    const/4 v11, 0x0

    #@af
    invoke-virtual {v10, v11}, Landroid/webkit/SelectActionModeCallback;->setTextSelected(Z)V

    #@b2
    .line 5058
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@b4
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@b6
    invoke-virtual {v10, v11}, Landroid/webkit/WebView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@b9
    goto/16 :goto_9
.end method

.method pinLocX(I)I
    .registers 4
    .parameter "x"

    #@0
    .prologue
    .line 3459
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mInOverScrollMode:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 3460
    .end local p1
    :goto_4
    return p1

    #@5
    .restart local p1
    :cond_5
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@8
    move-result v0

    #@9
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->computeRealHorizontalScrollRange()I

    #@c
    move-result v1

    #@d
    invoke-static {p1, v0, v1}, Landroid/webkit/WebViewClassic;->pinLoc(III)I

    #@10
    move-result p1

    #@11
    goto :goto_4
.end method

.method pinLocY(I)I
    .registers 6
    .parameter "y"

    #@0
    .prologue
    .line 3465
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mInOverScrollMode:Z

    #@2
    if-eqz v1, :cond_5

    #@4
    .line 3468
    .end local p1
    :goto_4
    return p1

    #@5
    .line 3467
    .restart local p1
    :cond_5
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getOverlappingActionModeHeight()I

    #@8
    move-result v0

    #@9
    .line 3468
    .local v0, overlappingActionModeHeight:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@c
    move-result v1

    #@d
    sub-int/2addr v1, v0

    #@e
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->computeRealVerticalScrollRange()I

    #@11
    move-result v2

    #@12
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@15
    move-result v3

    #@16
    add-int/2addr v2, v3

    #@17
    invoke-static {p1, v1, v2}, Landroid/webkit/WebViewClassic;->pinLoc(III)I

    #@1a
    move-result p1

    #@1b
    goto :goto_4
.end method

.method public postUrl(Ljava/lang/String;[B)V
    .registers 6
    .parameter "url"
    .parameter "postData"

    #@0
    .prologue
    .line 3032
    invoke-static {p1}, Landroid/webkit/URLUtil;->isNetworkUrl(Ljava/lang/String;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_1d

    #@6
    .line 3033
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@9
    .line 3034
    new-instance v0, Landroid/webkit/WebViewCore$PostUrlData;

    #@b
    invoke-direct {v0}, Landroid/webkit/WebViewCore$PostUrlData;-><init>()V

    #@e
    .line 3035
    .local v0, arg:Landroid/webkit/WebViewCore$PostUrlData;
    iput-object p1, v0, Landroid/webkit/WebViewCore$PostUrlData;->mUrl:Ljava/lang/String;

    #@10
    .line 3036
    iput-object p2, v0, Landroid/webkit/WebViewCore$PostUrlData;->mPostData:[B

    #@12
    .line 3037
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@14
    const/16 v2, 0x84

    #@16
    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@19
    .line 3038
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->clearHelpers()V

    #@1c
    .line 3042
    .end local v0           #arg:Landroid/webkit/WebViewCore$PostUrlData;
    :goto_1c
    return-void

    #@1d
    .line 3040
    :cond_1d
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->loadUrlImpl(Ljava/lang/String;)V

    #@20
    goto :goto_1c
.end method

.method public recalculateVisibleRect()V
    .registers 3

    #@0
    .prologue
    .line 10638
    const-string/jumbo v0, "webview"

    #@3
    const-string/jumbo v1, "recalculateVisibleRect() called"

    #@6
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 10639
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->sendOurVisibleRect()Landroid/graphics/Rect;

    #@c
    .line 10640
    return-void
.end method

.method public refreshPlugins(Z)V
    .registers 2
    .parameter "reloadOpenPages"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 4778
    return-void
.end method

.method public reload()V
    .registers 3

    #@0
    .prologue
    .line 3144
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->clearHelpers()V

    #@3
    .line 3145
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@6
    .line 3146
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@8
    const/16 v1, 0x66

    #@a
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@d
    .line 3147
    return-void
.end method

.method public removeJavascriptInterface(Ljava/lang/String;)V
    .registers 5
    .parameter "interfaceName"

    #@0
    .prologue
    .line 4748
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-eqz v1, :cond_12

    #@4
    .line 4749
    new-instance v0, Landroid/webkit/WebViewCore$JSInterfaceData;

    #@6
    invoke-direct {v0}, Landroid/webkit/WebViewCore$JSInterfaceData;-><init>()V

    #@9
    .line 4750
    .local v0, arg:Landroid/webkit/WebViewCore$JSInterfaceData;
    iput-object p1, v0, Landroid/webkit/WebViewCore$JSInterfaceData;->mInterfaceName:Ljava/lang/String;

    #@b
    .line 4751
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@d
    const/16 v2, 0x95

    #@f
    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@12
    .line 4753
    .end local v0           #arg:Landroid/webkit/WebViewCore$JSInterfaceData;
    :cond_12
    return-void
.end method

.method replaceTextfieldText(IILjava/lang/String;II)V
    .registers 8
    .parameter "oldStart"
    .parameter "oldEnd"
    .parameter "replace"
    .parameter "newStart"
    .parameter "newEnd"

    #@0
    .prologue
    .line 8739
    new-instance v0, Landroid/webkit/WebViewCore$ReplaceTextData;

    #@2
    invoke-direct {v0}, Landroid/webkit/WebViewCore$ReplaceTextData;-><init>()V

    #@5
    .line 8740
    .local v0, arg:Landroid/webkit/WebViewCore$ReplaceTextData;
    iput-object p3, v0, Landroid/webkit/WebViewCore$ReplaceTextData;->mReplace:Ljava/lang/String;

    #@7
    .line 8741
    iput p4, v0, Landroid/webkit/WebViewCore$ReplaceTextData;->mNewStart:I

    #@9
    .line 8742
    iput p5, v0, Landroid/webkit/WebViewCore$ReplaceTextData;->mNewEnd:I

    #@b
    .line 8743
    iget v1, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@d
    add-int/lit8 v1, v1, 0x1

    #@f
    iput v1, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@11
    .line 8744
    iget v1, p0, Landroid/webkit/WebViewClassic;->mTextGeneration:I

    #@13
    iput v1, v0, Landroid/webkit/WebViewCore$ReplaceTextData;->mTextGeneration:I

    #@15
    .line 8745
    const/16 v1, 0x72

    #@17
    invoke-virtual {p0, v1, p1, p2, v0}, Landroid/webkit/WebViewClassic;->sendBatchableInputMessage(IIILjava/lang/Object;)V

    #@1a
    .line 8746
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 20
    .parameter "child"
    .parameter "rect"
    .parameter "immediate"

    #@0
    .prologue
    .line 8678
    move-object/from16 v0, p0

    #@2
    iget v11, v0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@4
    if-nez v11, :cond_8

    #@6
    .line 8679
    const/4 v11, 0x0

    #@7
    .line 8734
    :goto_7
    return v11

    #@8
    .line 8683
    :cond_8
    move-object/from16 v0, p0

    #@a
    iget-object v11, v0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@c
    invoke-virtual {v11}, Landroid/webkit/ZoomManager;->isFixedLengthAnimationInProgress()Z

    #@f
    move-result v11

    #@10
    if-eqz v11, :cond_14

    #@12
    .line 8684
    const/4 v11, 0x0

    #@13
    goto :goto_7

    #@14
    .line 8687
    :cond_14
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    #@17
    move-result v11

    #@18
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getScrollX()I

    #@1b
    move-result v12

    #@1c
    sub-int/2addr v11, v12

    #@1d
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    #@20
    move-result v12

    #@21
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getScrollY()I

    #@24
    move-result v13

    #@25
    sub-int/2addr v12, v13

    #@26
    move-object/from16 v0, p2

    #@28
    invoke-virtual {v0, v11, v12}, Landroid/graphics/Rect;->offset(II)V

    #@2b
    .line 8690
    new-instance v1, Landroid/graphics/Rect;

    #@2d
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@30
    move-result v11

    #@31
    move-object/from16 v0, p0

    #@33
    invoke-virtual {v0, v11}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@36
    move-result v11

    #@37
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@3a
    move-result v12

    #@3b
    move-object/from16 v0, p0

    #@3d
    invoke-virtual {v0, v12}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@40
    move-result v12

    #@41
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@44
    move-result v13

    #@45
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getWidth()I

    #@48
    move-result v14

    #@49
    add-int/2addr v13, v14

    #@4a
    move-object/from16 v0, p0

    #@4c
    iget-object v14, v0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@4e
    invoke-virtual {v14}, Landroid/webkit/WebView;->getVerticalScrollbarWidth()I

    #@51
    move-result v14

    #@52
    sub-int/2addr v13, v14

    #@53
    move-object/from16 v0, p0

    #@55
    invoke-virtual {v0, v13}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@58
    move-result v13

    #@59
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@5c
    move-result v14

    #@5d
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@60
    move-result v15

    #@61
    add-int/2addr v14, v15

    #@62
    move-object/from16 v0, p0

    #@64
    invoke-virtual {v0, v14}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@67
    move-result v14

    #@68
    invoke-direct {v1, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    #@6b
    .line 8695
    .local v1, content:Landroid/graphics/Rect;
    iget v11, v1, Landroid/graphics/Rect;->top:I

    #@6d
    move-object/from16 v0, p0

    #@6f
    invoke-virtual {v0, v11}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@72
    move-result v7

    #@73
    .line 8696
    .local v7, screenTop:I
    iget v11, v1, Landroid/graphics/Rect;->bottom:I

    #@75
    move-object/from16 v0, p0

    #@77
    invoke-virtual {v0, v11}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@7a
    move-result v4

    #@7b
    .line 8697
    .local v4, screenBottom:I
    sub-int v2, v4, v7

    #@7d
    .line 8698
    .local v2, height:I
    const/4 v9, 0x0

    #@7e
    .line 8700
    .local v9, scrollYDelta:I
    move-object/from16 v0, p2

    #@80
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    #@82
    if-le v11, v4, :cond_d8

    #@84
    .line 8701
    div-int/lit8 v3, v2, 0x3

    #@86
    .line 8702
    .local v3, oneThirdOfScreenHeight:I
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    #@89
    move-result v11

    #@8a
    mul-int/lit8 v12, v3, 0x2

    #@8c
    if-le v11, v12, :cond_cf

    #@8e
    .line 8705
    move-object/from16 v0, p2

    #@90
    iget v11, v0, Landroid/graphics/Rect;->top:I

    #@92
    sub-int v9, v11, v7

    #@94
    .line 8715
    .end local v3           #oneThirdOfScreenHeight:I
    :cond_94
    :goto_94
    iget v11, v1, Landroid/graphics/Rect;->left:I

    #@96
    move-object/from16 v0, p0

    #@98
    invoke-virtual {v0, v11}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@9b
    move-result v5

    #@9c
    .line 8716
    .local v5, screenLeft:I
    iget v11, v1, Landroid/graphics/Rect;->right:I

    #@9e
    move-object/from16 v0, p0

    #@a0
    invoke-virtual {v0, v11}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@a3
    move-result v6

    #@a4
    .line 8717
    .local v6, screenRight:I
    sub-int v10, v6, v5

    #@a6
    .line 8718
    .local v10, width:I
    const/4 v8, 0x0

    #@a7
    .line 8720
    .local v8, scrollXDelta:I
    move-object/from16 v0, p2

    #@a9
    iget v11, v0, Landroid/graphics/Rect;->right:I

    #@ab
    if-le v11, v6, :cond_ec

    #@ad
    move-object/from16 v0, p2

    #@af
    iget v11, v0, Landroid/graphics/Rect;->left:I

    #@b1
    if-le v11, v5, :cond_ec

    #@b3
    .line 8721
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    #@b6
    move-result v11

    #@b7
    if-le v11, v10, :cond_e5

    #@b9
    .line 8722
    move-object/from16 v0, p2

    #@bb
    iget v11, v0, Landroid/graphics/Rect;->left:I

    #@bd
    sub-int/2addr v11, v5

    #@be
    add-int/2addr v8, v11

    #@bf
    .line 8730
    :cond_bf
    :goto_bf
    or-int v11, v9, v8

    #@c1
    if-eqz v11, :cond_fc

    #@c3
    .line 8731
    if-nez p3, :cond_fa

    #@c5
    const/4 v11, 0x1

    #@c6
    :goto_c6
    const/4 v12, 0x0

    #@c7
    move-object/from16 v0, p0

    #@c9
    invoke-direct {v0, v8, v9, v11, v12}, Landroid/webkit/WebViewClassic;->pinScrollBy(IIZI)Z

    #@cc
    move-result v11

    #@cd
    goto/16 :goto_7

    #@cf
    .line 8709
    .end local v5           #screenLeft:I
    .end local v6           #screenRight:I
    .end local v8           #scrollXDelta:I
    .end local v10           #width:I
    .restart local v3       #oneThirdOfScreenHeight:I
    :cond_cf
    move-object/from16 v0, p2

    #@d1
    iget v11, v0, Landroid/graphics/Rect;->top:I

    #@d3
    add-int v12, v7, v3

    #@d5
    sub-int v9, v11, v12

    #@d7
    goto :goto_94

    #@d8
    .line 8711
    .end local v3           #oneThirdOfScreenHeight:I
    :cond_d8
    move-object/from16 v0, p2

    #@da
    iget v11, v0, Landroid/graphics/Rect;->top:I

    #@dc
    if-ge v11, v7, :cond_94

    #@de
    .line 8712
    move-object/from16 v0, p2

    #@e0
    iget v11, v0, Landroid/graphics/Rect;->top:I

    #@e2
    sub-int v9, v11, v7

    #@e4
    goto :goto_94

    #@e5
    .line 8724
    .restart local v5       #screenLeft:I
    .restart local v6       #screenRight:I
    .restart local v8       #scrollXDelta:I
    .restart local v10       #width:I
    :cond_e5
    move-object/from16 v0, p2

    #@e7
    iget v11, v0, Landroid/graphics/Rect;->right:I

    #@e9
    sub-int/2addr v11, v6

    #@ea
    add-int/2addr v8, v11

    #@eb
    goto :goto_bf

    #@ec
    .line 8726
    :cond_ec
    move-object/from16 v0, p2

    #@ee
    iget v11, v0, Landroid/graphics/Rect;->left:I

    #@f0
    if-ge v11, v5, :cond_bf

    #@f2
    .line 8727
    move-object/from16 v0, p2

    #@f4
    iget v11, v0, Landroid/graphics/Rect;->left:I

    #@f6
    sub-int v11, v5, v11

    #@f8
    sub-int/2addr v8, v11

    #@f9
    goto :goto_bf

    #@fa
    .line 8731
    :cond_fa
    const/4 v11, 0x0

    #@fb
    goto :goto_c6

    #@fc
    .line 8734
    :cond_fc
    const/4 v11, 0x0

    #@fd
    goto/16 :goto_7
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .registers 7
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 8586
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 8621
    :cond_5
    :goto_5
    return v1

    #@6
    .line 8589
    :cond_6
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mFindIsUp:Z

    #@8
    if-nez v2, :cond_5

    #@a
    .line 8591
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@c
    if-eqz v2, :cond_5

    #@e
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@10
    invoke-virtual {v2}, Landroid/webkit/WebViewCore;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@13
    move-result-object v2

    #@14
    if-eqz v2, :cond_5

    #@16
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@18
    if-eqz v2, :cond_5

    #@1a
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@1c
    if-eqz v2, :cond_5

    #@1e
    .line 8595
    const/4 v1, 0x0

    #@1f
    .line 8596
    .local v1, result:Z
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@21
    invoke-virtual {v2, p1, p2}, Landroid/webkit/WebView$PrivateAccess;->super_requestFocus(ILandroid/graphics/Rect;)Z

    #@24
    move-result v1

    #@25
    .line 8597
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@27
    if-eqz v2, :cond_5

    #@29
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2b
    invoke-virtual {v2}, Landroid/webkit/WebViewCore;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@2e
    move-result-object v2

    #@2f
    if-eqz v2, :cond_5

    #@31
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@33
    invoke-virtual {v2}, Landroid/webkit/WebViewCore;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Landroid/webkit/WebSettingsClassic;->getNeedInitialFocus()Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_5

    #@3d
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@3f
    invoke-virtual {v2}, Landroid/webkit/WebView;->isInTouchMode()Z

    #@42
    move-result v2

    #@43
    if-nez v2, :cond_5

    #@45
    .line 8602
    const/4 v0, 0x0

    #@46
    .line 8603
    .local v0, fakeKeyDirection:I
    sparse-switch p1, :sswitch_data_5e

    #@49
    goto :goto_5

    #@4a
    .line 8611
    :sswitch_4a
    const/16 v0, 0x15

    #@4c
    .line 8619
    :goto_4c
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@4e
    const/16 v3, 0xe0

    #@50
    invoke-virtual {v2, v3, v0}, Landroid/webkit/WebViewCore;->sendMessage(II)V

    #@53
    goto :goto_5

    #@54
    .line 8605
    :sswitch_54
    const/16 v0, 0x13

    #@56
    .line 8606
    goto :goto_4c

    #@57
    .line 8608
    :sswitch_57
    const/16 v0, 0x14

    #@59
    .line 8609
    goto :goto_4c

    #@5a
    .line 8614
    :sswitch_5a
    const/16 v0, 0x16

    #@5c
    .line 8615
    goto :goto_4c

    #@5d
    .line 8603
    nop

    #@5e
    :sswitch_data_5e
    .sparse-switch
        0x11 -> :sswitch_4a
        0x21 -> :sswitch_54
        0x42 -> :sswitch_5a
        0x82 -> :sswitch_57
    .end sparse-switch
.end method

.method public requestFocusNodeHref(Landroid/os/Message;)V
    .registers 7
    .parameter "hrefMsg"

    #@0
    .prologue
    .line 3411
    if-nez p1, :cond_3

    #@2
    .line 3426
    :goto_2
    return-void

    #@3
    .line 3414
    :cond_3
    iget v2, p0, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@5
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@8
    move-result v3

    #@9
    add-int/2addr v2, v3

    #@a
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@d
    move-result v0

    #@e
    .line 3415
    .local v0, contentX:I
    iget v2, p0, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@10
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@13
    move-result v3

    #@14
    add-int/2addr v2, v3

    #@15
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@18
    move-result v1

    #@19
    .line 3416
    .local v1, contentY:I
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@1b
    if-eqz v2, :cond_57

    #@1d
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@1f
    iget v2, v2, Landroid/webkit/WebViewCore$WebKitHitTest;->mHitTestX:I

    #@21
    if-ne v2, v0, :cond_57

    #@23
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@25
    iget v2, v2, Landroid/webkit/WebViewCore$WebKitHitTest;->mHitTestY:I

    #@27
    if-ne v2, v1, :cond_57

    #@29
    .line 3418
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@2c
    move-result-object v2

    #@2d
    const-string/jumbo v3, "url"

    #@30
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@32
    iget-object v4, v4, Landroid/webkit/WebViewCore$WebKitHitTest;->mLinkUrl:Ljava/lang/String;

    #@34
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 3419
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@3a
    move-result-object v2

    #@3b
    const-string/jumbo v3, "title"

    #@3e
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@40
    iget-object v4, v4, Landroid/webkit/WebViewCore$WebKitHitTest;->mAnchorText:Ljava/lang/String;

    #@42
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 3420
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@48
    move-result-object v2

    #@49
    const-string/jumbo v3, "src"

    #@4c
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@4e
    iget-object v4, v4, Landroid/webkit/WebViewCore$WebKitHitTest;->mImageUrl:Ljava/lang/String;

    #@50
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    .line 3421
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@56
    goto :goto_2

    #@57
    .line 3424
    :cond_57
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@59
    const/16 v3, 0x89

    #@5b
    invoke-virtual {v2, v3, v0, v1, p1}, Landroid/webkit/WebViewCore;->sendMessage(IIILjava/lang/Object;)V

    #@5e
    goto :goto_2
.end method

.method requestFormData(Ljava/lang/String;IZZ)V
    .registers 13
    .parameter "name"
    .parameter "nodePointer"
    .parameter "autoFillable"
    .parameter "autoComplete"

    #@0
    .prologue
    .line 5595
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    invoke-virtual {v1}, Landroid/webkit/WebViewCore;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getSaveFormData()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_2a

    #@c
    .line 5596
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@e
    const/4 v2, 0x6

    #@f
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@12
    move-result-object v4

    #@13
    .line 5597
    .local v4, update:Landroid/os/Message;
    iput p2, v4, Landroid/os/Message;->arg1:I

    #@15
    .line 5598
    new-instance v0, Landroid/webkit/WebViewClassic$RequestFormData;

    #@17
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getUrl()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    move-object v1, p0

    #@1c
    move-object v2, p1

    #@1d
    move v5, p3

    #@1e
    move v6, p4

    #@1f
    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebViewClassic$RequestFormData;-><init>(Landroid/webkit/WebViewClassic;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;ZZ)V

    #@22
    .line 5600
    .local v0, updater:Landroid/webkit/WebViewClassic$RequestFormData;
    new-instance v7, Ljava/lang/Thread;

    #@24
    invoke-direct {v7, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@27
    .line 5601
    .local v7, t:Ljava/lang/Thread;
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    #@2a
    .line 5603
    .end local v0           #updater:Landroid/webkit/WebViewClassic$RequestFormData;
    .end local v4           #update:Landroid/os/Message;
    .end local v7           #t:Ljava/lang/Thread;
    :cond_2a
    return-void
.end method

.method public requestImageRef(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 3433
    iget v2, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v2, :cond_5

    #@4
    .line 3439
    :goto_4
    return-void

    #@5
    .line 3434
    :cond_5
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@7
    if-eqz v2, :cond_1e

    #@9
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFocusedNode:Landroid/webkit/WebViewCore$WebKitHitTest;

    #@b
    iget-object v1, v2, Landroid/webkit/WebViewCore$WebKitHitTest;->mImageUrl:Ljava/lang/String;

    #@d
    .line 3435
    .local v1, url:Ljava/lang/String;
    :goto_d
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@10
    move-result-object v0

    #@11
    .line 3436
    .local v0, data:Landroid/os/Bundle;
    const-string/jumbo v2, "url"

    #@14
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 3437
    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@1a
    .line 3438
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@1d
    goto :goto_4

    #@1e
    .line 3434
    .end local v0           #data:Landroid/os/Bundle;
    .end local v1           #url:Ljava/lang/String;
    :cond_1e
    const/4 v1, 0x0

    #@1f
    goto :goto_d
.end method

.method requestListBox([Ljava/lang/String;[IIZ)V
    .registers 13
    .parameter "array"
    .parameter "enabledArray"
    .parameter "selection"
    .parameter "isDatalist"

    #@0
    .prologue
    .line 10190
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@2
    new-instance v0, Landroid/webkit/WebViewClassic$InvokeListBox;

    #@4
    const/4 v6, 0x0

    #@5
    move-object v1, p0

    #@6
    move-object v2, p1

    #@7
    move-object v3, p2

    #@8
    move v4, p3

    #@9
    move v5, p4

    #@a
    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebViewClassic$InvokeListBox;-><init>(Landroid/webkit/WebViewClassic;[Ljava/lang/String;[IIZLandroid/webkit/WebViewClassic$1;)V

    #@d
    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@10
    .line 10192
    return-void
.end method

.method requestListBox([Ljava/lang/String;[I[I)V
    .registers 11
    .parameter "array"
    .parameter "enabledArray"
    .parameter "selectedArray"

    #@0
    .prologue
    .line 10174
    iget-object v6, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@2
    new-instance v0, Landroid/webkit/WebViewClassic$InvokeListBox;

    #@4
    const/4 v5, 0x0

    #@5
    move-object v1, p0

    #@6
    move-object v2, p1

    #@7
    move-object v3, p2

    #@8
    move-object v4, p3

    #@9
    invoke-direct/range {v0 .. v5}, Landroid/webkit/WebViewClassic$InvokeListBox;-><init>(Landroid/webkit/WebViewClassic;[Ljava/lang/String;[I[ILandroid/webkit/WebViewClassic$1;)V

    #@c
    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@f
    .line 10176
    return-void
.end method

.method resetTrackballTime()V
    .registers 3

    #@0
    .prologue
    .line 7932
    const-wide/16 v0, 0x0

    #@2
    iput-wide v0, p0, Landroid/webkit/WebViewClassic;->mTrackballLastTime:J

    #@4
    .line 7933
    return-void
.end method

.method public restorePicture(Landroid/os/Bundle;Ljava/io/File;)Z
    .registers 8
    .parameter "b"
    .parameter "src"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2828
    if-eqz p2, :cond_5

    #@3
    if-nez p1, :cond_6

    #@5
    .line 2864
    :cond_5
    :goto_5
    return v3

    #@6
    .line 2831
    :cond_6
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_5

    #@c
    .line 2835
    :try_start_c
    new-instance v2, Ljava/io/FileInputStream;

    #@e
    invoke-direct {v2, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@11
    .line 2836
    .local v2, in:Ljava/io/FileInputStream;
    new-instance v0, Landroid/os/Bundle;

    #@13
    invoke-direct {v0, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@16
    .line 2837
    .local v0, copy:Landroid/os/Bundle;
    new-instance v3, Ljava/lang/Thread;

    #@18
    new-instance v4, Landroid/webkit/WebViewClassic$7;

    #@1a
    invoke-direct {v4, p0, v2, v0}, Landroid/webkit/WebViewClassic$7;-><init>(Landroid/webkit/WebViewClassic;Ljava/io/FileInputStream;Landroid/os/Bundle;)V

    #@1d
    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@20
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V
    :try_end_23
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_23} :catch_25

    #@23
    .line 2864
    .end local v0           #copy:Landroid/os/Bundle;
    .end local v2           #in:Ljava/io/FileInputStream;
    :goto_23
    const/4 v3, 0x1

    #@24
    goto :goto_5

    #@25
    .line 2861
    :catch_25
    move-exception v1

    #@26
    .line 2862
    .local v1, e:Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    #@29
    goto :goto_23
.end method

.method public restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;
    .registers 12
    .parameter "inState"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 2934
    const/4 v6, 0x0

    #@2
    .line 2935
    .local v6, returnList:Landroid/webkit/WebBackForwardListClassic;
    if-nez p1, :cond_6

    #@4
    move-object v8, v6

    #@5
    .line 2985
    :goto_5
    return-object v8

    #@6
    .line 2938
    :cond_6
    const-string v9, "index"

    #@8
    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@b
    move-result v9

    #@c
    if-eqz v9, :cond_94

    #@e
    const-string v9, "history"

    #@10
    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@13
    move-result v9

    #@14
    if-eqz v9, :cond_94

    #@16
    .line 2939
    const-string v9, "certificate"

    #@18
    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@1b
    move-result-object v9

    #@1c
    invoke-static {v9}, Landroid/net/http/SslCertificate;->restoreState(Landroid/os/Bundle;)Landroid/net/http/SslCertificate;

    #@1f
    move-result-object v9

    #@20
    iput-object v9, p0, Landroid/webkit/WebViewClassic;->mCertificate:Landroid/net/http/SslCertificate;

    #@22
    .line 2942
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@24
    invoke-virtual {v9}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@27
    move-result-object v5

    #@28
    .line 2943
    .local v5, list:Landroid/webkit/WebBackForwardListClassic;
    const-string v9, "index"

    #@2a
    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@2d
    move-result v3

    #@2e
    .line 2947
    .local v3, index:I
    monitor-enter v5

    #@2f
    .line 2948
    :try_start_2f
    const-string v9, "history"

    #@31
    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    #@34
    move-result-object v1

    #@35
    check-cast v1, Ljava/util/List;

    #@37
    .line 2950
    .local v1, history:Ljava/util/List;,"Ljava/util/List<[B>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@3a
    move-result v7

    #@3b
    .line 2953
    .local v7, size:I
    if-ltz v3, :cond_3f

    #@3d
    if-lt v3, v7, :cond_44

    #@3f
    .line 2954
    :cond_3f
    monitor-exit v5

    #@40
    goto :goto_5

    #@41
    .line 2970
    .end local v1           #history:Ljava/util/List;,"Ljava/util/List<[B>;"
    .end local v7           #size:I
    :catchall_41
    move-exception v8

    #@42
    monitor-exit v5
    :try_end_43
    .catchall {:try_start_2f .. :try_end_43} :catchall_41

    #@43
    throw v8

    #@44
    .line 2956
    .restart local v1       #history:Ljava/util/List;,"Ljava/util/List<[B>;"
    .restart local v7       #size:I
    :cond_44
    const/4 v2, 0x0

    #@45
    .local v2, i:I
    :goto_45
    if-ge v2, v7, :cond_5d

    #@47
    .line 2957
    const/4 v9, 0x0

    #@48
    :try_start_48
    invoke-interface {v1, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@4b
    move-result-object v0

    #@4c
    check-cast v0, [B

    #@4e
    .line 2958
    .local v0, data:[B
    if-nez v0, :cond_52

    #@50
    .line 2961
    monitor-exit v5

    #@51
    goto :goto_5

    #@52
    .line 2963
    :cond_52
    new-instance v4, Landroid/webkit/WebHistoryItemClassic;

    #@54
    invoke-direct {v4, v0}, Landroid/webkit/WebHistoryItemClassic;-><init>([B)V

    #@57
    .line 2964
    .local v4, item:Landroid/webkit/WebHistoryItem;
    invoke-virtual {v5, v4}, Landroid/webkit/WebBackForwardListClassic;->addHistoryItem(Landroid/webkit/WebHistoryItem;)V

    #@5a
    .line 2956
    add-int/lit8 v2, v2, 0x1

    #@5c
    goto :goto_45

    #@5d
    .line 2967
    .end local v0           #data:[B
    .end local v4           #item:Landroid/webkit/WebHistoryItem;
    :cond_5d
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->copyBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@60
    move-result-object v6

    #@61
    .line 2969
    invoke-virtual {v6, v3}, Landroid/webkit/WebBackForwardListClassic;->setCurrentIndex(I)V

    #@64
    .line 2970
    monitor-exit v5
    :try_end_65
    .catchall {:try_start_48 .. :try_end_65} :catchall_41

    #@65
    .line 2972
    const-string/jumbo v8, "privateBrowsingEnabled"

    #@68
    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@6b
    move-result v8

    #@6c
    if-eqz v8, :cond_76

    #@6e
    .line 2973
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@71
    move-result-object v8

    #@72
    const/4 v9, 0x1

    #@73
    invoke-virtual {v8, v9}, Landroid/webkit/WebSettingsClassic;->setPrivateBrowsingEnabled(Z)V

    #@76
    .line 2975
    :cond_76
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@78
    invoke-virtual {v8, p1}, Landroid/webkit/ZoomManager;->restoreZoomState(Landroid/os/Bundle;)V

    #@7b
    .line 2978
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@7d
    invoke-virtual {v8}, Landroid/webkit/WebViewCore;->removeMessages()V

    #@80
    .line 2979
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isAccessibilityInjectionEnabled()Z

    #@83
    move-result v8

    #@84
    if-eqz v8, :cond_8d

    #@86
    .line 2980
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@89
    move-result-object v8

    #@8a
    invoke-virtual {v8}, Landroid/webkit/AccessibilityInjector;->addAccessibilityApisIfNecessary()V

    #@8d
    .line 2983
    :cond_8d
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@8f
    const/16 v9, 0x6c

    #@91
    invoke-virtual {v8, v9, v3}, Landroid/webkit/WebViewCore;->sendMessage(II)V

    #@94
    .end local v1           #history:Ljava/util/List;,"Ljava/util/List<[B>;"
    .end local v2           #i:I
    .end local v3           #index:I
    .end local v5           #list:Landroid/webkit/WebBackForwardListClassic;
    .end local v7           #size:I
    :cond_94
    move-object v8, v6

    #@95
    .line 2985
    goto/16 :goto_5
.end method

.method public resumeTimers()V
    .registers 3

    #@0
    .prologue
    .line 4002
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0x6e

    #@4
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@7
    .line 4003
    return-void
.end method

.method public saveCachedImageToFile(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "imageUrl"
    .parameter "filePath"

    #@0
    .prologue
    .line 10648
    const-string/jumbo v1, "webview"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string/jumbo v3, "saveCachedImageToFile: "

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, " => "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 10649
    const/4 v1, 0x2

    #@25
    new-array v0, v1, [Ljava/lang/String;

    #@27
    const/4 v1, 0x0

    #@28
    aput-object p1, v0, v1

    #@2a
    const/4 v1, 0x1

    #@2b
    aput-object p2, v0, v1

    #@2d
    .line 10650
    .local v0, param:[Ljava/lang/String;
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2f
    const/16 v2, 0xe6

    #@31
    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@34
    .line 10651
    return-void
.end method

.method public savePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "host"
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    .line 2503
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mDatabase:Landroid/webkit/WebViewDatabaseClassic;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/WebViewDatabaseClassic;->setUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 2504
    return-void
.end method

.method public savePicture(Landroid/os/Bundle;Ljava/io/File;)Z
    .registers 7
    .parameter "b"
    .parameter "dest"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2767
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_6

    #@4
    .line 2768
    :cond_4
    const/4 v2, 0x0

    #@5
    .line 2802
    :goto_5
    return v2

    #@6
    .line 2770
    :cond_6
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->capturePicture()Landroid/graphics/Picture;

    #@9
    move-result-object v0

    #@a
    .line 2773
    .local v0, p:Landroid/graphics/Picture;
    new-instance v1, Ljava/io/File;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    const-string v3, ".writing"

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@26
    .line 2774
    .local v1, temp:Ljava/io/File;
    new-instance v2, Ljava/lang/Thread;

    #@28
    new-instance v3, Landroid/webkit/WebViewClassic$6;

    #@2a
    invoke-direct {v3, p0, v1, v0, p2}, Landroid/webkit/WebViewClassic$6;-><init>(Landroid/webkit/WebViewClassic;Ljava/io/File;Landroid/graphics/Picture;Ljava/io/File;)V

    #@2d
    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@30
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    #@33
    .line 2799
    const-string/jumbo v2, "scrollX"

    #@36
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@39
    move-result v3

    #@3a
    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@3d
    .line 2800
    const-string/jumbo v2, "scrollY"

    #@40
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@43
    move-result v3

    #@44
    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@47
    .line 2801
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@49
    invoke-virtual {v2, p1}, Landroid/webkit/ZoomManager;->saveZoomState(Landroid/os/Bundle;)V

    #@4c
    .line 2802
    const/4 v2, 0x1

    #@4d
    goto :goto_5
.end method

.method public saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;
    .registers 12
    .parameter "outState"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 2716
    if-nez p1, :cond_5

    #@3
    move-object v5, v7

    #@4
    .line 2758
    :goto_4
    return-object v5

    #@5
    .line 2721
    :cond_5
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->copyBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@8
    move-result-object v5

    #@9
    .line 2722
    .local v5, list:Landroid/webkit/WebBackForwardListClassic;
    invoke-virtual {v5}, Landroid/webkit/WebBackForwardListClassic;->getCurrentIndex()I

    #@c
    move-result v0

    #@d
    .line 2723
    .local v0, currentIndex:I
    invoke-virtual {v5}, Landroid/webkit/WebBackForwardListClassic;->getSize()I

    #@10
    move-result v6

    #@11
    .line 2726
    .local v6, size:I
    if-ltz v0, :cond_17

    #@13
    if-ge v0, v6, :cond_17

    #@15
    if-nez v6, :cond_19

    #@17
    :cond_17
    move-object v5, v7

    #@18
    .line 2727
    goto :goto_4

    #@19
    .line 2729
    :cond_19
    const-string v8, "index"

    #@1b
    invoke-virtual {p1, v8, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@1e
    .line 2733
    new-instance v2, Ljava/util/ArrayList;

    #@20
    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    #@23
    .line 2734
    .local v2, history:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    const/4 v3, 0x0

    #@24
    .local v3, i:I
    :goto_24
    if-ge v3, v6, :cond_45

    #@26
    .line 2735
    invoke-virtual {v5, v3}, Landroid/webkit/WebBackForwardListClassic;->getItemAtIndex(I)Landroid/webkit/WebHistoryItemClassic;

    #@29
    move-result-object v4

    #@2a
    .line 2736
    .local v4, item:Landroid/webkit/WebHistoryItemClassic;
    if-nez v4, :cond_37

    #@2c
    .line 2739
    const-string/jumbo v8, "webview"

    #@2f
    const-string/jumbo v9, "saveState: Unexpected null history item."

    #@32
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    move-object v5, v7

    #@36
    .line 2740
    goto :goto_4

    #@37
    .line 2742
    :cond_37
    invoke-virtual {v4}, Landroid/webkit/WebHistoryItemClassic;->getFlattenedData()[B

    #@3a
    move-result-object v1

    #@3b
    .line 2743
    .local v1, data:[B
    if-nez v1, :cond_3f

    #@3d
    move-object v5, v7

    #@3e
    .line 2747
    goto :goto_4

    #@3f
    .line 2749
    :cond_3f
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@42
    .line 2734
    add-int/lit8 v3, v3, 0x1

    #@44
    goto :goto_24

    #@45
    .line 2751
    .end local v1           #data:[B
    .end local v4           #item:Landroid/webkit/WebHistoryItemClassic;
    :cond_45
    const-string v7, "history"

    #@47
    invoke-virtual {p1, v7, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    #@4a
    .line 2752
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mCertificate:Landroid/net/http/SslCertificate;

    #@4c
    if-eqz v7, :cond_59

    #@4e
    .line 2753
    const-string v7, "certificate"

    #@50
    iget-object v8, p0, Landroid/webkit/WebViewClassic;->mCertificate:Landroid/net/http/SslCertificate;

    #@52
    invoke-static {v8}, Landroid/net/http/SslCertificate;->saveState(Landroid/net/http/SslCertificate;)Landroid/os/Bundle;

    #@55
    move-result-object v8

    #@56
    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@59
    .line 2756
    :cond_59
    const-string/jumbo v7, "privateBrowsingEnabled"

    #@5c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->isPrivateBrowsingEnabled()Z

    #@5f
    move-result v8

    #@60
    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@63
    .line 2757
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@65
    invoke-virtual {v7, p1}, Landroid/webkit/ZoomManager;->saveZoomState(Landroid/os/Bundle;)V

    #@68
    goto :goto_4
.end method

.method public saveViewState(Ljava/io/OutputStream;Landroid/webkit/ValueCallback;)V
    .registers 6
    .parameter "stream"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/OutputStream;",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2875
    .local p2, callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/lang/Boolean;>;"
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 2876
    const/4 v0, 0x0

    #@5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@8
    move-result-object v0

    #@9
    invoke-interface {p2, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    #@c
    .line 2881
    :goto_c
    return-void

    #@d
    .line 2879
    :cond_d
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@f
    const/16 v1, 0xe1

    #@11
    new-instance v2, Landroid/webkit/WebViewCore$SaveViewStateRequest;

    #@13
    invoke-direct {v2, p1, p2}, Landroid/webkit/WebViewCore$SaveViewStateRequest;-><init>(Ljava/io/OutputStream;Landroid/webkit/ValueCallback;)V

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebViewCore;->sendMessageAtFrontOfQueue(ILjava/lang/Object;)V

    #@19
    goto :goto_c
.end method

.method public saveWebArchive(Ljava/lang/String;)V
    .registers 4
    .parameter "filename"

    #@0
    .prologue
    .line 3098
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, v1}, Landroid/webkit/WebViewClassic;->saveWebArchiveImpl(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V

    #@5
    .line 3099
    return-void
.end method

.method public saveWebArchive(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V
    .registers 4
    .parameter "basename"
    .parameter "autoname"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3119
    .local p3, callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewClassic;->saveWebArchiveImpl(Ljava/lang/String;ZLandroid/webkit/ValueCallback;)V

    #@3
    .line 3120
    return-void
.end method

.method public selectAll()V
    .registers 2

    #@0
    .prologue
    .line 6125
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic;->selectAll(Z)V

    #@4
    .line 6126
    return-void
.end method

.method selectAll(Z)V
    .registers 6
    .parameter "editable"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 6136
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@3
    const/16 v3, 0xd7

    #@5
    if-eqz p1, :cond_e

    #@7
    move v0, v1

    #@8
    :goto_8
    invoke-virtual {v2, v3, v0}, Landroid/webkit/WebViewCore;->sendMessage(II)V

    #@b
    .line 6137
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsSelectingAll:Z

    #@d
    .line 6138
    return-void

    #@e
    .line 6136
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_8
.end method

.method public selectText()Z
    .registers 5

    #@0
    .prologue
    .line 5069
    iget v2, p0, Landroid/webkit/WebViewClassic;->mLastTouchX:I

    #@2
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@5
    move-result v3

    #@6
    add-int/2addr v2, v3

    #@7
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@a
    move-result v0

    #@b
    .line 5070
    .local v0, x:I
    iget v2, p0, Landroid/webkit/WebViewClassic;->mLastTouchY:I

    #@d
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@10
    move-result v3

    #@11
    add-int/2addr v2, v3

    #@12
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@15
    move-result v1

    #@16
    .line 5071
    .local v1, y:I
    invoke-virtual {p0, v0, v1}, Landroid/webkit/WebViewClassic;->selectText(II)Z

    #@19
    move-result v2

    #@1a
    return v2
.end method

.method selectText(II)Z
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 5078
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-nez v2, :cond_6

    #@4
    .line 5079
    const/4 v2, 0x0

    #@5
    .line 5109
    :goto_5
    return v2

    #@6
    .line 5083
    :cond_6
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mFindIsUp:Z

    #@8
    if-eqz v2, :cond_13

    #@a
    .line 5084
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@c
    if-eqz v2, :cond_13

    #@e
    .line 5085
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@10
    invoke-virtual {v2}, Landroid/webkit/FindActionModeCallback;->finish()V

    #@13
    .line 5090
    :cond_13
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@15
    const/16 v3, 0xd6

    #@17
    invoke-virtual {v2, v3, p1, p2}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@1a
    .line 5092
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mTouchInEditText:Z

    #@1c
    if-nez v2, :cond_36

    #@1e
    .line 5094
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@21
    move-result-object v1

    #@22
    .line 5095
    .local v1, settings:Landroid/webkit/WebSettings;
    if-eqz v1, :cond_33

    #@24
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@27
    move-result v2

    #@28
    if-eqz v2, :cond_33

    #@2a
    .line 5096
    invoke-static {}, Landroid/webkit/LGCliptrayManager;->getInstance()Landroid/webkit/LGCliptrayManager;

    #@2d
    move-result-object v0

    #@2e
    .line 5097
    .local v0, cliptray:Landroid/webkit/LGCliptrayManager;
    if-eqz v0, :cond_33

    #@30
    .line 5098
    invoke-virtual {v0}, Landroid/webkit/LGCliptrayManager;->hideCliptray()V

    #@33
    .line 5102
    .end local v0           #cliptray:Landroid/webkit/LGCliptrayManager;
    :cond_33
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->hideSoftKeyboard()V

    #@36
    .line 5104
    .end local v1           #settings:Landroid/webkit/WebSettings;
    :cond_36
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->removeTouchHighlight()V

    #@39
    .line 5107
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewClassic;->setupSelectionScroll(II)V

    #@3c
    .line 5109
    const/4 v2, 0x1

    #@3d
    goto :goto_5
.end method

.method public selectionDone()V
    .registers 5

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 6147
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@4
    if-eqz v1, :cond_3c

    #@6
    .line 6148
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsSelectingInEditText:Z

    #@8
    .line 6149
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->hidePasteButton()V

    #@b
    .line 6150
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->endSelectingText()V

    #@e
    .line 6153
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@10
    if-eqz v1, :cond_1a

    #@12
    .line 6154
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@14
    invoke-virtual {v1}, Landroid/webkit/SelectActionModeCallback;->finish()V

    #@17
    .line 6155
    const/4 v1, 0x0

    #@18
    iput-object v1, p0, Landroid/webkit/WebViewClassic;->mSelectCallback:Landroid/webkit/SelectActionModeCallback;

    #@1a
    .line 6158
    :cond_1a
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->hideLGSelectActionPopupWindow()V

    #@1d
    .line 6160
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@20
    .line 6161
    iput v2, p0, Landroid/webkit/WebViewClassic;->mAutoScrollX:I

    #@22
    .line 6162
    iput v2, p0, Landroid/webkit/WebViewClassic;->mAutoScrollY:I

    #@24
    .line 6163
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mSentAutoScrollMessage:Z

    #@26
    .line 6165
    iput v3, p0, Landroid/webkit/WebViewClassic;->SELECTION_SCROLL_PosX:I

    #@28
    .line 6166
    iput v3, p0, Landroid/webkit/WebViewClassic;->SELECTION_SCROLL_PosY:I

    #@2a
    .line 6170
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTranslateMode()Z

    #@2d
    move-result v1

    #@2e
    if-eqz v1, :cond_3c

    #@30
    .line 6171
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->setTranslateMode(Z)V

    #@33
    .line 6172
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@35
    invoke-static {v1, p0}, Landroid/webkit/LGTranslateReceiver;->getInstance(Landroid/content/Context;Landroid/webkit/WebViewClassic;)Landroid/webkit/LGTranslateReceiver;

    #@38
    move-result-object v0

    #@39
    .line 6173
    .local v0, translate:Landroid/webkit/LGTranslateReceiver;
    invoke-virtual {v0}, Landroid/webkit/LGTranslateReceiver;->disableTranslateListener()V

    #@3c
    .line 6177
    .end local v0           #translate:Landroid/webkit/LGTranslateReceiver;
    :cond_3c
    return-void
.end method

.method sendBatchableInputMessage(IIILjava/lang/Object;)V
    .registers 7
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 9818
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 9828
    :goto_4
    return-void

    #@5
    .line 9821
    :cond_5
    const/4 v1, 0x0

    #@6
    invoke-static {v1, p1, p2, p3, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    .line 9822
    .local v0, message:Landroid/os/Message;
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsBatchingTextChanges:Z

    #@c
    if-eqz v1, :cond_17

    #@e
    .line 9823
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mBatchedTextChanges:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13
    .line 9827
    :goto_13
    const/4 v1, 0x1

    #@14
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mForceScrollEditIntoView:Z

    #@16
    goto :goto_4

    #@17
    .line 9825
    :cond_17
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@19
    invoke-virtual {v1, v0}, Landroid/webkit/WebViewCore;->sendMessage(Landroid/os/Message;)V

    #@1c
    goto :goto_13
.end method

.method sendOurVisibleRect()Landroid/graphics/Rect;
    .registers 7

    #@0
    .prologue
    const/16 v5, 0x6b

    #@2
    .line 3638
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@4
    invoke-virtual {v2}, Landroid/webkit/ZoomManager;->isPreventingWebkitUpdates()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_d

    #@a
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mLastVisibleRectSent:Landroid/graphics/Rect;

    #@c
    .line 3676
    :goto_c
    return-object v2

    #@d
    .line 3639
    :cond_d
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mVisibleRect:Landroid/graphics/Rect;

    #@f
    invoke-direct {p0, v2}, Landroid/webkit/WebViewClassic;->calcOurContentVisibleRect(Landroid/graphics/Rect;)V

    #@12
    .line 3641
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mVisibleRect:Landroid/graphics/Rect;

    #@14
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mLastVisibleRectSent:Landroid/graphics/Rect;

    #@16
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    if-nez v2, :cond_75

    #@1c
    .line 3642
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@1e
    if-nez v2, :cond_42

    #@20
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@22
    if-eqz v2, :cond_42

    #@24
    .line 3643
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mScrollOffset:Landroid/graphics/Point;

    #@26
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mVisibleRect:Landroid/graphics/Rect;

    #@28
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@2a
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mVisibleRect:Landroid/graphics/Rect;

    #@2c
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@2e
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    #@31
    .line 3644
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@33
    invoke-virtual {v2, v5}, Landroid/webkit/WebViewCore;->removeMessages(I)V

    #@36
    .line 3645
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@38
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mSendScrollEvent:Z

    #@3a
    if-eqz v2, :cond_a5

    #@3c
    const/4 v2, 0x1

    #@3d
    :goto_3d
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mScrollOffset:Landroid/graphics/Point;

    #@3f
    invoke-virtual {v3, v5, v2, v4}, Landroid/webkit/WebViewCore;->sendMessage(IILjava/lang/Object;)V

    #@42
    .line 3650
    :cond_42
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@44
    if-eqz v2, :cond_68

    #@46
    .line 3651
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->computeRealVerticalScrollRange()I

    #@49
    move-result v2

    #@4a
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@4d
    move-result v3

    #@4e
    add-int/2addr v2, v3

    #@4f
    invoke-direct {p0, v2}, Landroid/webkit/WebViewClassic;->viewToContentDimension(I)I

    #@52
    move-result v0

    #@53
    .line 3652
    .local v0, maxDocHeight:I
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mVisibleRect:Landroid/graphics/Rect;

    #@55
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    #@57
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mVisibleRect:Landroid/graphics/Rect;

    #@59
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@5b
    sub-int v1, v2, v3

    #@5d
    .line 3653
    .local v1, visibleHeight:I
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mLastVisibleRectSent:Landroid/graphics/Rect;

    #@5f
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@61
    sub-int v2, v0, v2

    #@63
    if-le v1, v2, :cond_68

    #@65
    .line 3654
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@68
    .line 3658
    .end local v0           #maxDocHeight:I
    .end local v1           #visibleHeight:I
    :cond_68
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mLastVisibleRectSent:Landroid/graphics/Rect;

    #@6a
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mVisibleRect:Landroid/graphics/Rect;

    #@6c
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@6f
    .line 3659
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@71
    const/4 v3, 0x4

    #@72
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@75
    .line 3661
    :cond_75
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@77
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleRect:Landroid/graphics/Rect;

    #@79
    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    #@7c
    move-result v2

    #@7d
    if-eqz v2, :cond_a1

    #@7f
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleRect:Landroid/graphics/Rect;

    #@81
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mLastGlobalRect:Landroid/graphics/Rect;

    #@83
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v2

    #@87
    if-nez v2, :cond_a1

    #@89
    .line 3671
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@8b
    if-nez v2, :cond_9a

    #@8d
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@8f
    if-eqz v2, :cond_9a

    #@91
    .line 3672
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@93
    const/16 v3, 0x74

    #@95
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleRect:Landroid/graphics/Rect;

    #@97
    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@9a
    .line 3674
    :cond_9a
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mLastGlobalRect:Landroid/graphics/Rect;

    #@9c
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mGlobalVisibleRect:Landroid/graphics/Rect;

    #@9e
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@a1
    .line 3676
    :cond_a1
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mVisibleRect:Landroid/graphics/Rect;

    #@a3
    goto/16 :goto_c

    #@a5
    .line 3645
    :cond_a5
    const/4 v2, 0x0

    #@a6
    goto :goto_3d
.end method

.method public sendPickerValue(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 8502
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0x5f

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@7
    .line 8503
    return-void
.end method

.method public sendUpdatePosMessage()V
    .registers 3

    #@0
    .prologue
    .line 812
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0xec

    #@4
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@7
    .line 813
    return-void
.end method

.method sendViewSizeZoom(Z)Z
    .registers 13
    .parameter "force"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 3731
    iget-boolean v9, p0, Landroid/webkit/WebViewClassic;->mBlockWebkitViewMessages:Z

    #@4
    if-eqz v9, :cond_7

    #@6
    .line 3790
    :cond_6
    :goto_6
    return v7

    #@7
    .line 3732
    :cond_7
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@9
    invoke-virtual {v9}, Landroid/webkit/ZoomManager;->isPreventingWebkitUpdates()Z

    #@c
    move-result v9

    #@d
    if-nez v9, :cond_6

    #@f
    .line 3734
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@12
    move-result v6

    #@13
    .line 3735
    .local v6, viewWidth:I
    int-to-float v9, v6

    #@14
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@16
    invoke-virtual {v10}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@19
    move-result v10

    #@1a
    mul-float/2addr v9, v10

    #@1b
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@1e
    move-result v4

    #@1f
    .line 3737
    .local v4, newWidth:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@22
    move-result v9

    #@23
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@26
    move-result v10

    #@27
    sub-int v5, v9, v10

    #@29
    .line 3738
    .local v5, viewHeight:I
    int-to-float v9, v5

    #@2a
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2c
    invoke-virtual {v10}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@2f
    move-result v10

    #@30
    mul-float/2addr v9, v10

    #@31
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@34
    move-result v3

    #@35
    .line 3741
    .local v3, newHeight:I
    int-to-float v9, v5

    #@36
    int-to-float v10, v6

    #@37
    div-float v2, v9, v10

    #@39
    .line 3750
    .local v2, heightWidthRatio:F
    iget v9, p0, Landroid/webkit/WebViewClassic;->mLastWidthSent:I

    #@3b
    if-le v4, v9, :cond_43

    #@3d
    iget-boolean v9, p0, Landroid/webkit/WebViewClassic;->mWrapContent:Z

    #@3f
    if-eqz v9, :cond_43

    #@41
    .line 3751
    const/4 v3, 0x0

    #@42
    .line 3752
    const/4 v2, 0x0

    #@43
    .line 3755
    :cond_43
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getViewHeight()I

    #@46
    move-result v9

    #@47
    int-to-float v9, v9

    #@48
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@4a
    invoke-virtual {v10}, Landroid/webkit/ZoomManager;->getInvScale()F

    #@4d
    move-result v10

    #@4e
    mul-float/2addr v9, v10

    #@4f
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@52
    move-result v0

    #@53
    .line 3757
    .local v0, actualViewHeight:I
    iget v9, p0, Landroid/webkit/WebViewClassic;->mLastWidthSent:I

    #@55
    if-ne v4, v9, :cond_61

    #@57
    iget v9, p0, Landroid/webkit/WebViewClassic;->mLastHeightSent:I

    #@59
    if-ne v3, v9, :cond_61

    #@5b
    if-nez p1, :cond_61

    #@5d
    iget v9, p0, Landroid/webkit/WebViewClassic;->mLastActualHeightSent:I

    #@5f
    if-eq v0, v9, :cond_6

    #@61
    .line 3759
    :cond_61
    new-instance v1, Landroid/webkit/WebViewClassic$ViewSizeData;

    #@63
    invoke-direct {v1}, Landroid/webkit/WebViewClassic$ViewSizeData;-><init>()V

    #@66
    .line 3760
    .local v1, data:Landroid/webkit/WebViewClassic$ViewSizeData;
    iput v4, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mWidth:I

    #@68
    .line 3761
    iput v3, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mHeight:I

    #@6a
    .line 3762
    iput v2, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mHeightWidthRatio:F

    #@6c
    .line 3763
    iput v0, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mActualViewHeight:I

    #@6e
    .line 3764
    int-to-float v9, v6

    #@6f
    iget-object v10, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@71
    invoke-virtual {v10}, Landroid/webkit/ZoomManager;->getTextWrapScale()F

    #@74
    move-result v10

    #@75
    div-float/2addr v9, v10

    #@76
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@79
    move-result v9

    #@7a
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mTextWrapWidth:I

    #@7c
    .line 3765
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@7e
    invoke-virtual {v9}, Landroid/webkit/ZoomManager;->getScale()F

    #@81
    move-result v9

    #@82
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mScale:F

    #@84
    .line 3766
    iget-object v9, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@86
    invoke-virtual {v9}, Landroid/webkit/ZoomManager;->isFixedLengthAnimationInProgress()Z

    #@89
    move-result v9

    #@8a
    if-eqz v9, :cond_91

    #@8c
    iget-boolean v9, p0, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@8e
    if-nez v9, :cond_91

    #@90
    move v7, v8

    #@91
    :cond_91
    iput-boolean v7, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mIgnoreHeight:Z

    #@93
    .line 3768
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@95
    invoke-virtual {v7}, Landroid/webkit/ZoomManager;->getDocumentAnchorX()I

    #@98
    move-result v7

    #@99
    iput v7, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mAnchorX:I

    #@9b
    .line 3769
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@9d
    invoke-virtual {v7}, Landroid/webkit/ZoomManager;->getDocumentAnchorY()I

    #@a0
    move-result v7

    #@a1
    iput v7, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mAnchorY:I

    #@a3
    .line 3771
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@a5
    if-eqz v7, :cond_c8

    #@a7
    .line 3772
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@a9
    const/16 v9, 0x69

    #@ab
    invoke-virtual {v7, v9, v1}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@ae
    .line 3774
    iget-boolean v7, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@b0
    if-eqz v7, :cond_b8

    #@b2
    iget-boolean v7, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@b4
    if-nez v7, :cond_b8

    #@b6
    .line 3775
    iput-boolean v8, p0, Landroid/webkit/WebViewClassic;->mShowLGPopupNeeded:Z

    #@b8
    .line 3779
    :cond_b8
    iget-boolean v7, p0, Landroid/webkit/WebViewClassic;->mSentAutoScrollMessage:Z

    #@ba
    if-nez v7, :cond_c8

    #@bc
    iget v7, p0, Landroid/webkit/WebViewClassic;->SELECTION_SCROLL_PosY:I

    #@be
    const/4 v9, -0x1

    #@bf
    if-eq v7, v9, :cond_c8

    #@c1
    .line 3780
    iget v7, p0, Landroid/webkit/WebViewClassic;->SELECTION_SCROLL_PosX:I

    #@c3
    iget v9, p0, Landroid/webkit/WebViewClassic;->SELECTION_SCROLL_PosY:I

    #@c5
    invoke-direct {p0, v7, v9}, Landroid/webkit/WebViewClassic;->setupSelectionScroll(II)V

    #@c8
    .line 3784
    :cond_c8
    iput v4, p0, Landroid/webkit/WebViewClassic;->mLastWidthSent:I

    #@ca
    .line 3785
    iput v3, p0, Landroid/webkit/WebViewClassic;->mLastHeightSent:I

    #@cc
    .line 3786
    iput v0, p0, Landroid/webkit/WebViewClassic;->mLastActualHeightSent:I

    #@ce
    .line 3787
    iget-object v7, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@d0
    invoke-virtual {v7}, Landroid/webkit/ZoomManager;->clearDocumentAnchor()V

    #@d3
    move v7, v8

    #@d4
    .line 3788
    goto/16 :goto_6
.end method

.method setActive(Z)V
    .registers 5
    .parameter "active"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 6468
    if-eqz p1, :cond_1b

    #@4
    .line 6469
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebView;->hasFocus()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_15

    #@c
    .line 6472
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mDrawCursorRing:Z

    #@e
    .line 6473
    invoke-virtual {p0, v1}, Landroid/webkit/WebViewClassic;->setFocusControllerActive(Z)V

    #@11
    .line 6494
    :goto_11
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@14
    .line 6495
    return-void

    #@15
    .line 6475
    :cond_15
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mDrawCursorRing:Z

    #@17
    .line 6476
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->setFocusControllerActive(Z)V

    #@1a
    goto :goto_11

    #@1b
    .line 6479
    :cond_1b
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@1d
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->isZoomPickerVisible()Z

    #@20
    move-result v0

    #@21
    if-nez v0, :cond_25

    #@23
    .line 6487
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mDrawCursorRing:Z

    #@25
    .line 6489
    :cond_25
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mKeysPressed:Ljava/util/Vector;

    #@27
    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    #@2a
    .line 6490
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@2c
    const/4 v1, 0x4

    #@2d
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@30
    .line 6491
    const/4 v0, 0x7

    #@31
    iput v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@33
    .line 6492
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->setFocusControllerActive(Z)V

    #@36
    goto :goto_11
.end method

.method public setBackgroundColor(I)V
    .registers 4
    .parameter "color"

    #@0
    .prologue
    .line 10323
    iput p1, p0, Landroid/webkit/WebViewClassic;->mBackgroundColor:I

    #@2
    .line 10324
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@4
    const/16 v1, 0x7e

    #@6
    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewCore;->sendMessage(II)V

    #@9
    .line 10325
    return-void
.end method

.method setBaseLayer(IZZ)V
    .registers 11
    .parameter "layer"
    .parameter "showVisualIndicator"
    .parameter "isPictureAfterFirstLayout"

    #@0
    .prologue
    .line 5178
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v0, :cond_5

    #@4
    .line 5205
    :goto_4
    return-void

    #@5
    .line 5181
    :cond_5
    iget v0, p0, Landroid/webkit/WebViewClassic;->mTouchMode:I

    #@7
    const/16 v1, 0x9

    #@9
    if-ne v0, v1, :cond_28

    #@b
    iget v5, p0, Landroid/webkit/WebViewClassic;->mCurrentScrollingLayerId:I

    #@d
    .line 5185
    .local v5, scrollingLayer:I
    :goto_d
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHTML5VideoViewProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 5186
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mHTML5VideoViewProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@13
    invoke-virtual {v0, p1}, Landroid/webkit/HTML5VideoViewProxy;->setBaseLayer(I)V

    #@16
    .line 5190
    :cond_16
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@18
    move-object v0, p0

    #@19
    move v2, p1

    #@1a
    move v3, p2

    #@1b
    move v4, p3

    #@1c
    invoke-direct/range {v0 .. v5}, Landroid/webkit/WebViewClassic;->nativeSetBaseLayer(IIZZI)Z

    #@1f
    move-result v6

    #@20
    .line 5194
    .local v6, queueFull:Z
    if-eqz v6, :cond_2a

    #@22
    .line 5195
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@24
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->pauseWebKitDraw()V

    #@27
    goto :goto_4

    #@28
    .line 5181
    .end local v5           #scrollingLayer:I
    .end local v6           #queueFull:Z
    :cond_28
    const/4 v5, 0x0

    #@29
    goto :goto_d

    #@2a
    .line 5197
    .restart local v5       #scrollingLayer:I
    .restart local v6       #queueFull:Z
    :cond_2a
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2c
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->resumeWebKitDraw()V

    #@2f
    goto :goto_4
.end method

.method public setCertificate(Landroid/net/http/SslCertificate;)V
    .registers 2
    .parameter "certificate"

    #@0
    .prologue
    .line 2491
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mCertificate:Landroid/net/http/SslCertificate;

    #@2
    .line 2492
    return-void
.end method

.method public setDownloadListener(Landroid/webkit/DownloadListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 4655
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    #@5
    .line 4656
    return-void
.end method

.method public setDownloadListenerExt(Landroid/webkit/DownloadListenerExt;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 4661
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->setDownloadListenerExt(Landroid/webkit/DownloadListenerExt;)V

    #@5
    .line 4662
    return-void
.end method

.method public setFindListener(Landroid/webkit/WebView$FindListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 4141
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mFindListener:Landroid/webkit/WebView$FindListener;

    #@2
    .line 4142
    return-void
.end method

.method public setFloatingWindowState(Z)V
    .registers 2
    .parameter "isSwitchingToFloating"

    #@0
    .prologue
    .line 1594
    sput-boolean p1, Landroid/webkit/WebViewClassic;->mIsSwitchingToFloating:Z

    #@2
    .line 1595
    return-void
.end method

.method setFocusControllerActive(Z)V
    .registers 6
    .parameter "active"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 6526
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@3
    if-nez v0, :cond_6

    #@5
    .line 6533
    :cond_5
    :goto_5
    return-void

    #@6
    .line 6527
    :cond_6
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@8
    const/16 v3, 0x8e

    #@a
    if-eqz p1, :cond_21

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    invoke-virtual {v2, v3, v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@10
    .line 6529
    if-eqz p1, :cond_5

    #@12
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mListBoxMessage:Landroid/os/Message;

    #@14
    if-eqz v0, :cond_5

    #@16
    .line 6530
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@18
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mListBoxMessage:Landroid/os/Message;

    #@1a
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(Landroid/os/Message;)V

    #@1d
    .line 6531
    const/4 v0, 0x0

    #@1e
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mListBoxMessage:Landroid/os/Message;

    #@20
    goto :goto_5

    #@21
    :cond_21
    move v0, v1

    #@22
    .line 6527
    goto :goto_d
.end method

.method public setForeground(Z)V
    .registers 3
    .parameter "foreground"

    #@0
    .prologue
    .line 10764
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mIsForeground:Z

    #@2
    .line 10765
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsForeground:Z

    #@4
    if-eqz v0, :cond_17

    #@6
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@8
    if-eqz v0, :cond_17

    #@a
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@c
    if-nez v0, :cond_17

    #@e
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsPaused:Z

    #@10
    if-nez v0, :cond_17

    #@12
    .line 10766
    const/4 v0, 0x0

    #@13
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->showLGSelectActionPopupWindow(I)V

    #@16
    .line 10771
    :goto_16
    return-void

    #@17
    .line 10769
    :cond_17
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->hideLGSelectActionPopupWindow()V

    #@1a
    goto :goto_16
.end method

.method public setFrame(IIII)Z
    .registers 8
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 6610
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@3
    if-eqz v1, :cond_c

    #@5
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@7
    if-nez v1, :cond_c

    #@9
    .line 6611
    invoke-direct {p0, v2}, Landroid/webkit/WebViewClassic;->showLGSelectActionPopupWindow(I)V

    #@c
    .line 6614
    :cond_c
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@e
    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/webkit/WebView$PrivateAccess;->super_setFrame(IIII)Z

    #@11
    move-result v0

    #@12
    .line 6615
    .local v0, changed:Z
    if-nez v0, :cond_1b

    #@14
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@16
    if-eqz v1, :cond_1b

    #@18
    .line 6622
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->sendViewSizeZoom(Z)Z

    #@1b
    .line 6624
    :cond_1b
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->updateRectsForGL()V

    #@1e
    .line 6625
    return v0
.end method

.method public setGestureZoomScale(F)V
    .registers 5
    .parameter "Scale"

    #@0
    .prologue
    .line 8463
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@5
    move-result v1

    #@6
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@9
    move-result v2

    #@a
    invoke-virtual {v0, p1, v1, v2}, Landroid/webkit/ZoomManager;->setCenterZoomScale(FII)V

    #@d
    .line 8464
    return-void
.end method

.method public setHTML5VideoViewProxy(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 2
    .parameter "proxy"

    #@0
    .prologue
    .line 10333
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mHTML5VideoViewProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@2
    .line 10334
    return-void
.end method

.method public setHorizontalScrollbarOverlay(Z)V
    .registers 2
    .parameter "overlay"

    #@0
    .prologue
    .line 2369
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mOverlayHorizontalScrollbar:Z

    #@2
    .line 2370
    return-void
.end method

.method public setHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "host"
    .parameter "realm"
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    .line 2512
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mDatabase:Landroid/webkit/WebViewDatabaseClassic;

    #@2
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewDatabaseClassic;->setHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 2513
    return-void
.end method

.method public setInitialScale(I)V
    .registers 3
    .parameter "scaleInPercent"

    #@0
    .prologue
    .line 3355
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/ZoomManager;->setInitialScaleInPercent(I)V

    #@5
    .line 3356
    return-void
.end method

.method public setJsFlags(Ljava/lang/String;)V
    .registers 4
    .parameter "flags"

    #@0
    .prologue
    .line 2689
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0xae

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@7
    .line 2690
    return-void
.end method

.method public setLayerType(ILandroid/graphics/Paint;)V
    .registers 3
    .parameter "layerType"
    .parameter "paint"

    #@0
    .prologue
    .line 10381
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->updateHwAccelerated()V

    #@3
    .line 10382
    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "params"

    #@0
    .prologue
    .line 4994
    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2
    const/4 v1, -0x2

    #@3
    if-ne v0, v1, :cond_8

    #@5
    .line 4995
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mWrapContent:Z

    #@8
    .line 4997
    :cond_8
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@a
    invoke-virtual {v0, p1}, Landroid/webkit/WebView$PrivateAccess;->super_setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@d
    .line 4998
    return-void
.end method

.method public setMapTrackballToArrowKeys(Z)V
    .registers 2
    .parameter "setMap"

    #@0
    .prologue
    .line 7928
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mMapTrackballToArrowKeys:Z

    #@2
    .line 7929
    return-void
.end method

.method public setMockDeviceOrientation(ZDZDZD)V
    .registers 20
    .parameter "canProvideAlpha"
    .parameter "alpha"
    .parameter "canProvideBeta"
    .parameter "beta"
    .parameter "canProvideGamma"
    .parameter "gamma"

    #@0
    .prologue
    .line 5748
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    move v1, p1

    #@3
    move-wide v2, p2

    #@4
    move v4, p4

    #@5
    move-wide v5, p5

    #@6
    move/from16 v7, p7

    #@8
    move-wide/from16 v8, p8

    #@a
    invoke-virtual/range {v0 .. v9}, Landroid/webkit/WebViewCore;->setMockDeviceOrientation(ZDZDZD)V

    #@d
    .line 5750
    return-void
.end method

.method public setMockGeolocationError(ILjava/lang/String;)V
    .registers 4
    .parameter "code"
    .parameter "message"

    #@0
    .prologue
    .line 5729
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewCore;->setMockGeolocationError(ILjava/lang/String;)V

    #@5
    .line 5730
    return-void
.end method

.method public setMockGeolocationPermission(Z)V
    .registers 3
    .parameter "allow"

    #@0
    .prologue
    .line 5738
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/WebViewCore;->setMockGeolocationPermission(Z)V

    #@5
    .line 5739
    return-void
.end method

.method public setMockGeolocationPosition(DDD)V
    .registers 14
    .parameter "latitude"
    .parameter "longitude"
    .parameter "accuracy"

    #@0
    .prologue
    .line 5720
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    move-wide v1, p1

    #@3
    move-wide v3, p3

    #@4
    move-wide v5, p5

    #@5
    invoke-virtual/range {v0 .. v6}, Landroid/webkit/WebViewCore;->setMockGeolocationPosition(DDD)V

    #@8
    .line 5721
    return-void
.end method

.method public setNetworkAvailable(Z)V
    .registers 6
    .parameter "networkUp"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2697
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@3
    const/16 v3, 0x77

    #@5
    if-eqz p1, :cond_c

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    invoke-virtual {v2, v3, v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@b
    .line 2699
    return-void

    #@c
    :cond_c
    move v0, v1

    #@d
    .line 2697
    goto :goto_8
.end method

.method public setNetworkType(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "type"
    .parameter "subtype"

    #@0
    .prologue
    .line 2705
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 2706
    .local v0, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v1, "type"

    #@8
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    .line 2707
    const-string/jumbo v1, "subtype"

    #@e
    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    .line 2708
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@13
    const/16 v2, 0xb7

    #@15
    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebViewCore;->sendMessage(ILjava/lang/Object;)V

    #@18
    .line 2709
    return-void
.end method

.method setNewPicture(Landroid/webkit/WebViewCore$DrawData;Z)V
    .registers 17
    .parameter "draw"
    .parameter "updateBaseLayer"

    #@0
    .prologue
    .line 9597
    iget v11, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@2
    if-nez v11, :cond_13

    #@4
    .line 9598
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mDelaySetPicture:Landroid/webkit/WebViewCore$DrawData;

    #@6
    if-eqz v11, :cond_10

    #@8
    .line 9599
    new-instance v11, Ljava/lang/IllegalStateException;

    #@a
    const-string v12, "Tried to setNewPicture with a delay picture already set! (memory leak)"

    #@c
    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v11

    #@10
    .line 9603
    :cond_10
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mDelaySetPicture:Landroid/webkit/WebViewCore$DrawData;

    #@12
    .line 9689
    :cond_12
    :goto_12
    return-void

    #@13
    .line 9606
    :cond_13
    iget-object v10, p1, Landroid/webkit/WebViewCore$DrawData;->mViewState:Landroid/webkit/WebViewCore$ViewState;

    #@15
    .line 9607
    .local v10, viewState:Landroid/webkit/WebViewCore$ViewState;
    if-eqz v10, :cond_f0

    #@17
    const/4 v3, 0x1

    #@18
    .line 9609
    .local v3, isPictureAfterFirstLayout:Z
    :goto_18
    if-eqz p2, :cond_27

    #@1a
    .line 9610
    iget v11, p1, Landroid/webkit/WebViewCore$DrawData;->mBaseLayer:I

    #@1c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@1f
    move-result-object v12

    #@20
    invoke-virtual {v12}, Landroid/webkit/WebSettingsClassic;->getShowVisualIndicator()Z

    #@23
    move-result v12

    #@24
    invoke-virtual {p0, v11, v12, v3}, Landroid/webkit/WebViewClassic;->setBaseLayer(IZZ)V

    #@27
    .line 9614
    :cond_27
    iget-object v9, p1, Landroid/webkit/WebViewCore$DrawData;->mViewSize:Landroid/graphics/Point;

    #@29
    .line 9619
    .local v9, viewSize:Landroid/graphics/Point;
    iget v11, v9, Landroid/graphics/Point;->x:I

    #@2b
    iget v12, p0, Landroid/webkit/WebViewClassic;->mLastWidthSent:I

    #@2d
    if-ne v11, v12, :cond_f3

    #@2f
    iget v11, v9, Landroid/graphics/Point;->y:I

    #@31
    iget v12, p0, Landroid/webkit/WebViewClassic;->mLastHeightSent:I

    #@33
    if-ne v11, v12, :cond_f3

    #@35
    const/4 v7, 0x1

    #@36
    .line 9624
    .local v7, updateLayout:Z
    :goto_36
    const/4 v11, 0x0

    #@37
    iput-boolean v11, p0, Landroid/webkit/WebViewClassic;->mSendScrollEvent:Z

    #@39
    .line 9625
    iget-object v11, p1, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@3b
    iget v11, v11, Landroid/graphics/Point;->x:I

    #@3d
    iget-object v12, p1, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@3f
    iget v12, v12, Landroid/graphics/Point;->y:I

    #@41
    invoke-direct {p0, v11, v12, v7}, Landroid/webkit/WebViewClassic;->recordNewContentSize(IIZ)V

    #@44
    .line 9627
    if-eqz v3, :cond_78

    #@46
    .line 9629
    const/4 v11, 0x0

    #@47
    iput v11, p0, Landroid/webkit/WebViewClassic;->mLastWidthSent:I

    #@49
    .line 9630
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@4b
    invoke-virtual {v11, p1}, Landroid/webkit/ZoomManager;->onFirstLayout(Landroid/webkit/WebViewCore$DrawData;)V

    #@4e
    .line 9631
    iget-boolean v11, v10, Landroid/webkit/WebViewCore$ViewState;->mShouldStartScrolledRight:Z

    #@50
    if-eqz v11, :cond_f6

    #@52
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getContentWidth()I

    #@55
    move-result v4

    #@56
    .line 9633
    .local v4, scrollX:I
    :goto_56
    iget v5, v10, Landroid/webkit/WebViewCore$ViewState;->mScrollY:I

    #@58
    .line 9634
    .local v5, scrollY:I
    const/4 v11, 0x0

    #@59
    invoke-direct {p0, v4, v5, v11}, Landroid/webkit/WebViewClassic;->contentScrollTo(IIZ)V

    #@5c
    .line 9635
    iget-boolean v11, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@5e
    if-nez v11, :cond_78

    #@60
    .line 9637
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@63
    move-result-object v6

    #@64
    .line 9638
    .local v6, settings:Landroid/webkit/WebSettings;
    if-eqz v6, :cond_75

    #@66
    invoke-virtual {v6}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@69
    move-result v11

    #@6a
    if-eqz v11, :cond_75

    #@6c
    .line 9639
    invoke-static {}, Landroid/webkit/LGCliptrayManager;->getInstance()Landroid/webkit/LGCliptrayManager;

    #@6f
    move-result-object v0

    #@70
    .line 9640
    .local v0, cliptray:Landroid/webkit/LGCliptrayManager;
    if-eqz v0, :cond_75

    #@72
    .line 9641
    invoke-virtual {v0}, Landroid/webkit/LGCliptrayManager;->hideCliptray()V

    #@75
    .line 9646
    .end local v0           #cliptray:Landroid/webkit/LGCliptrayManager;
    :cond_75
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->hideSoftKeyboard()V

    #@78
    .line 9649
    .end local v4           #scrollX:I
    .end local v5           #scrollY:I
    .end local v6           #settings:Landroid/webkit/WebSettings;
    :cond_78
    const/4 v11, 0x1

    #@79
    iput-boolean v11, p0, Landroid/webkit/WebViewClassic;->mSendScrollEvent:Z

    #@7b
    .line 9651
    const/4 v2, 0x0

    #@7c
    .line 9652
    .local v2, functor:I
    move v1, v3

    #@7d
    .line 9653
    .local v1, forceInval:Z
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@7f
    invoke-virtual {v11}, Landroid/webkit/WebView;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@82
    move-result-object v8

    #@83
    .line 9654
    .local v8, viewRoot:Landroid/view/ViewRootImpl;
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@85
    invoke-virtual {v11}, Landroid/webkit/WebView;->isHardwareAccelerated()Z

    #@88
    move-result v11

    #@89
    if-eqz v11, :cond_a6

    #@8b
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@8d
    invoke-virtual {v11}, Landroid/webkit/WebView;->getLayerType()I

    #@90
    move-result v11

    #@91
    const/4 v12, 0x1

    #@92
    if-eq v11, v12, :cond_a6

    #@94
    if-eqz v8, :cond_a6

    #@96
    .line 9657
    iget v11, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@98
    invoke-direct {p0, v11}, Landroid/webkit/WebViewClassic;->nativeGetDrawGLFunction(I)I

    #@9b
    move-result v2

    #@9c
    .line 9658
    if-eqz v2, :cond_a6

    #@9e
    .line 9660
    invoke-virtual {v8, v2}, Landroid/view/ViewRootImpl;->attachFunctor(I)Z

    #@a1
    move-result v11

    #@a2
    if-nez v11, :cond_fa

    #@a4
    const/4 v11, 0x1

    #@a5
    :goto_a5
    or-int/2addr v1, v11

    #@a6
    .line 9664
    :cond_a6
    if-eqz v2, :cond_b2

    #@a8
    if-nez v1, :cond_b2

    #@aa
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@ac
    invoke-virtual {v11}, Landroid/webkit/WebView;->getLayerType()I

    #@af
    move-result v11

    #@b0
    if-eqz v11, :cond_b7

    #@b2
    .line 9669
    :cond_b2
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@b4
    invoke-virtual {v11}, Landroid/webkit/WebView;->invalidate()V

    #@b7
    .line 9673
    :cond_b7
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@b9
    invoke-virtual {v11, p1}, Landroid/webkit/ZoomManager;->onNewPicture(Landroid/webkit/WebViewCore$DrawData;)Z

    #@bc
    move-result v11

    #@bd
    if-eqz v11, :cond_c2

    #@bf
    .line 9674
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@c2
    .line 9676
    :cond_c2
    if-eqz v3, :cond_c9

    #@c4
    .line 9677
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mViewManager:Landroid/webkit/ViewManager;

    #@c6
    invoke-virtual {v11}, Landroid/webkit/ViewManager;->postReadyToDrawAll()V

    #@c9
    .line 9679
    :cond_c9
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->scrollEditWithCursor()V

    #@cc
    .line 9681
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    #@ce
    if-eqz v11, :cond_12

    #@d0
    .line 9682
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@d2
    invoke-virtual {v11}, Landroid/webkit/WebView;->isHardwareAccelerated()Z

    #@d5
    move-result v11

    #@d6
    if-eqz v11, :cond_e1

    #@d8
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@da
    invoke-virtual {v11}, Landroid/webkit/WebView;->getLayerType()I

    #@dd
    move-result v11

    #@de
    const/4 v12, 0x1

    #@df
    if-ne v11, v12, :cond_12

    #@e1
    .line 9686
    :cond_e1
    iget-object v11, p0, Landroid/webkit/WebViewClassic;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    #@e3
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@e6
    move-result-object v12

    #@e7
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->capturePicture()Landroid/graphics/Picture;

    #@ea
    move-result-object v13

    #@eb
    invoke-interface {v11, v12, v13}, Landroid/webkit/WebView$PictureListener;->onNewPicture(Landroid/webkit/WebView;Landroid/graphics/Picture;)V

    #@ee
    goto/16 :goto_12

    #@f0
    .line 9607
    .end local v1           #forceInval:Z
    .end local v2           #functor:I
    .end local v3           #isPictureAfterFirstLayout:Z
    .end local v7           #updateLayout:Z
    .end local v8           #viewRoot:Landroid/view/ViewRootImpl;
    .end local v9           #viewSize:Landroid/graphics/Point;
    :cond_f0
    const/4 v3, 0x0

    #@f1
    goto/16 :goto_18

    #@f3
    .line 9619
    .restart local v3       #isPictureAfterFirstLayout:Z
    .restart local v9       #viewSize:Landroid/graphics/Point;
    :cond_f3
    const/4 v7, 0x0

    #@f4
    goto/16 :goto_36

    #@f6
    .line 9631
    .restart local v7       #updateLayout:Z
    :cond_f6
    iget v4, v10, Landroid/webkit/WebViewCore$ViewState;->mScrollX:I

    #@f8
    goto/16 :goto_56

    #@fa
    .line 9660
    .restart local v1       #forceInval:Z
    .restart local v2       #functor:I
    .restart local v8       #viewRoot:Landroid/view/ViewRootImpl;
    :cond_fa
    const/4 v11, 0x0

    #@fb
    goto :goto_a5
.end method

.method public setOverScrollMode(I)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 2232
    const/4 v0, 0x2

    #@1
    if-eq p1, v0, :cond_f

    #@3
    .line 2233
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 2234
    new-instance v0, Landroid/webkit/OverScrollGlow;

    #@9
    invoke-direct {v0, p0}, Landroid/webkit/OverScrollGlow;-><init>(Landroid/webkit/WebViewClassic;)V

    #@c
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@e
    .line 2239
    :cond_e
    :goto_e
    return-void

    #@f
    .line 2237
    :cond_f
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mOverScrollGlow:Landroid/webkit/OverScrollGlow;

    #@12
    goto :goto_e
.end method

.method public setPictureListener(Landroid/webkit/WebView$PictureListener;)V
    .registers 2
    .parameter "listener"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 4706
    iput-object p1, p0, Landroid/webkit/WebViewClassic;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    #@2
    .line 4707
    return-void
.end method

.method public setScrollBarStyle(I)V
    .registers 3
    .parameter "style"

    #@0
    .prologue
    .line 2356
    const/high16 v0, 0x100

    #@2
    if-eq p1, v0, :cond_8

    #@4
    const/high16 v0, 0x300

    #@6
    if-ne p1, v0, :cond_e

    #@8
    .line 2358
    :cond_8
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mOverlayVerticalScrollbar:Z

    #@b
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mOverlayHorizontalScrollbar:Z

    #@d
    .line 2362
    :goto_d
    return-void

    #@e
    .line 2360
    :cond_e
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mOverlayVerticalScrollbar:Z

    #@11
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic;->mOverlayHorizontalScrollbar:Z

    #@13
    goto :goto_d
.end method

.method setScrollXRaw(I)V
    .registers 3
    .parameter "mScrollX"

    #@0
    .prologue
    .line 1862
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/WebView$PrivateAccess;->setScrollXRaw(I)V

    #@5
    .line 1863
    return-void
.end method

.method setScrollYRaw(I)V
    .registers 3
    .parameter "mScrollY"

    #@0
    .prologue
    .line 1866
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/WebView$PrivateAccess;->setScrollYRaw(I)V

    #@5
    .line 1867
    return-void
.end method

.method setSelection(II)V
    .registers 5
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 5529
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 5530
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@6
    const/16 v1, 0x71

    #@8
    invoke-virtual {v0, v1, p1, p2}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@b
    .line 5532
    :cond_b
    return-void
.end method

.method public setTouchInterval(I)V
    .registers 2
    .parameter "interval"

    #@0
    .prologue
    .line 10343
    iput p1, p0, Landroid/webkit/WebViewClassic;->mCurrentTouchInterval:I

    #@2
    .line 10344
    return-void
.end method

.method public setTranslateMode(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 6407
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mTranslateMode:Z

    #@2
    if-eq v0, p1, :cond_6

    #@4
    .line 6408
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mTranslateMode:Z

    #@6
    .line 6410
    :cond_6
    const/4 v0, 0x1

    #@7
    if-ne p1, v0, :cond_c

    #@9
    .line 6411
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->animateHandles()V

    #@c
    .line 6413
    :cond_c
    return-void
.end method

.method public setUseMockDeviceOrientation()V
    .registers 3

    #@0
    .prologue
    .line 5701
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0xbf

    #@4
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@7
    .line 5702
    return-void
.end method

.method public setUseMockGeolocation()V
    .registers 3

    #@0
    .prologue
    .line 5711
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0xe2

    #@4
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@7
    .line 5712
    return-void
.end method

.method public setVerticalScrollbarOverlay(Z)V
    .registers 2
    .parameter "overlay"

    #@0
    .prologue
    .line 2377
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic;->mOverlayVerticalScrollbar:Z

    #@2
    .line 2378
    return-void
.end method

.method public setWebBackForwardListClient(Landroid/webkit/WebBackForwardListClient;)V
    .registers 3
    .parameter "client"

    #@0
    .prologue
    .line 4690
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->setWebBackForwardListClient(Landroid/webkit/WebBackForwardListClient;)V

    #@5
    .line 4691
    return-void
.end method

.method public setWebChromeClient(Landroid/webkit/WebChromeClient;)V
    .registers 3
    .parameter "client"

    #@0
    .prologue
    .line 4670
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    #@5
    .line 4671
    return-void
.end method

.method public setWebViewClient(Landroid/webkit/WebViewClient;)V
    .registers 3
    .parameter "client"

    #@0
    .prologue
    .line 4637
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    #@5
    .line 4638
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 2119
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public showActionPopup()V
    .registers 3

    #@0
    .prologue
    .line 10624
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@3
    move-result-object v0

    #@4
    .line 10625
    .local v0, settings:Landroid/webkit/WebSettingsClassic;
    if-eqz v0, :cond_18

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getLGBubbleActionEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_18

    #@c
    .line 10626
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@e
    if-eqz v1, :cond_18

    #@10
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@12
    if-nez v1, :cond_18

    #@14
    .line 10627
    const/4 v1, 0x0

    #@15
    invoke-direct {p0, v1}, Landroid/webkit/WebViewClassic;->showLGSelectActionPopupWindow(I)V

    #@18
    .line 10630
    :cond_18
    return-void
.end method

.method public showFindDialog(Ljava/lang/String;Z)Z
    .registers 6
    .parameter "text"
    .parameter "showIme"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 4204
    new-instance v0, Landroid/webkit/FindActionModeCallback;

    #@3
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@5
    invoke-direct {v0, v2}, Landroid/webkit/FindActionModeCallback;-><init>(Landroid/content/Context;)V

    #@8
    .line 4205
    .local v0, callback:Landroid/webkit/FindActionModeCallback;
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@a
    invoke-virtual {v2}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    #@d
    move-result-object v2

    #@e
    if-eqz v2, :cond_18

    #@10
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@12
    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@15
    move-result-object v2

    #@16
    if-nez v2, :cond_1a

    #@18
    .line 4207
    :cond_18
    const/4 v1, 0x0

    #@19
    .line 4232
    :cond_19
    :goto_19
    return v1

    #@1a
    .line 4210
    :cond_1a
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@1c
    if-eqz v2, :cond_21

    #@1e
    .line 4211
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@21
    .line 4214
    :cond_21
    const/4 v2, -0x1

    #@22
    iput v2, p0, Landroid/webkit/WebViewClassic;->mCachedOverlappingActionModeHeight:I

    #@24
    .line 4215
    iput-object v0, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@26
    .line 4216
    invoke-direct {p0, v1}, Landroid/webkit/WebViewClassic;->setFindIsUp(Z)V

    #@29
    .line 4217
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@2b
    invoke-virtual {v2, p0}, Landroid/webkit/FindActionModeCallback;->setWebView(Landroid/webkit/WebViewClassic;)V

    #@2e
    .line 4218
    if-eqz p2, :cond_49

    #@30
    .line 4219
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@32
    invoke-virtual {v2}, Landroid/webkit/FindActionModeCallback;->showSoftInput()V

    #@35
    .line 4225
    :cond_35
    if-nez p1, :cond_3c

    #@37
    .line 4226
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@39
    if-nez v2, :cond_56

    #@3b
    const/4 p1, 0x0

    #@3c
    .line 4228
    :cond_3c
    :goto_3c
    if-eqz p1, :cond_19

    #@3e
    .line 4229
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@40
    invoke-virtual {v2, p1}, Landroid/webkit/FindActionModeCallback;->setText(Ljava/lang/String;)V

    #@43
    .line 4230
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@45
    invoke-virtual {v2}, Landroid/webkit/FindActionModeCallback;->findAll()V

    #@48
    goto :goto_19

    #@49
    .line 4220
    :cond_49
    if-eqz p1, :cond_35

    #@4b
    .line 4221
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@4d
    invoke-virtual {v2, p1}, Landroid/webkit/FindActionModeCallback;->setText(Ljava/lang/String;)V

    #@50
    .line 4222
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindCallback:Landroid/webkit/FindActionModeCallback;

    #@52
    invoke-virtual {v2}, Landroid/webkit/FindActionModeCallback;->findAll()V

    #@55
    goto :goto_19

    #@56
    .line 4226
    :cond_56
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mFindRequest:Landroid/webkit/WebViewCore$FindAllRequest;

    #@58
    iget-object p1, v2, Landroid/webkit/WebViewCore$FindAllRequest;->mSearchText:Ljava/lang/String;

    #@5a
    goto :goto_3c
.end method

.method public stopLoading()V
    .registers 3

    #@0
    .prologue
    .line 3135
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@3
    .line 3136
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@5
    const/16 v1, 0x65

    #@7
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@a
    .line 3137
    return-void
.end method

.method public stopScroll()V
    .registers 3

    #@0
    .prologue
    .line 4360
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mScroller:Landroid/widget/OverScroller;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->forceFinished(Z)V

    #@6
    .line 4361
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/webkit/WebViewClassic;->mLastVelocity:F

    #@9
    .line 4362
    return-void
.end method

.method switchOutDrawHistory()V
    .registers 6

    #@0
    .prologue
    .line 5490
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 5505
    :cond_4
    :goto_4
    return-void

    #@5
    .line 5491
    :cond_5
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@7
    if-eqz v2, :cond_4

    #@9
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getProgress()I

    #@c
    move-result v2

    #@d
    const/16 v3, 0x64

    #@f
    if-eq v2, v3, :cond_17

    #@11
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->nativeHasContent()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_4

    #@17
    .line 5492
    :cond_17
    const/4 v2, 0x0

    #@18
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic;->mDrawHistory:Z

    #@1a
    .line 5493
    const/4 v2, 0x0

    #@1b
    iput-object v2, p0, Landroid/webkit/WebViewClassic;->mHistoryPicture:Landroid/graphics/Picture;

    #@1d
    .line 5494
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@20
    .line 5495
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@23
    move-result v0

    #@24
    .line 5496
    .local v0, oldScrollX:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@27
    move-result v1

    #@28
    .line 5497
    .local v1, oldScrollY:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@2b
    move-result v2

    #@2c
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->pinLocX(I)I

    #@2f
    move-result v2

    #@30
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->setScrollXRaw(I)V

    #@33
    .line 5498
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@36
    move-result v2

    #@37
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->pinLocY(I)I

    #@3a
    move-result v2

    #@3b
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic;->setScrollYRaw(I)V

    #@3e
    .line 5499
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@41
    move-result v2

    #@42
    if-ne v0, v2, :cond_4a

    #@44
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@47
    move-result v2

    #@48
    if-eq v1, v2, :cond_58

    #@4a
    .line 5500
    :cond_4a
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@4c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@4f
    move-result v3

    #@50
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@53
    move-result v4

    #@54
    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/webkit/WebView$PrivateAccess;->onScrollChanged(IIII)V

    #@57
    goto :goto_4

    #@58
    .line 5502
    :cond_58
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mSendScroll:Landroid/webkit/WebViewClassic$SendScrollToWebCore;

    #@5a
    const/4 v3, 0x1

    #@5b
    invoke-virtual {v2, v3}, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->send(Z)V

    #@5e
    goto :goto_4
.end method

.method public textLinkSelection()V
    .registers 6

    #@0
    .prologue
    .line 6243
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    .line 6244
    .local v3, text:Ljava/lang/String;
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    .line 6246
    .local v2, packageName:Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    #@c
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@f
    .line 6247
    .local v1, i:Landroid/content/Intent;
    const-string v4, "com.lge.smarttext.action.SEND"

    #@11
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@14
    .line 6248
    const-string/jumbo v4, "text/plain"

    #@17
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    #@1a
    .line 6249
    const-string v4, "calling_package"

    #@1c
    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1f
    .line 6250
    const-string v4, "android.intent.extra.TEXT"

    #@21
    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@24
    .line 6253
    :try_start_24
    const-string v4, "com.lge.browser"

    #@26
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v4

    #@2a
    if-nez v4, :cond_3b

    #@2c
    .line 6254
    const/high16 v4, 0x1000

    #@2e
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@31
    .line 6255
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@33
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@3a
    .line 6263
    :goto_3a
    return-void

    #@3b
    .line 6257
    :cond_3b
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mContext:Landroid/content/Context;

    #@3d
    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_40
    .catch Landroid/content/ActivityNotFoundException; {:try_start_24 .. :try_end_40} :catch_41

    #@40
    goto :goto_3a

    #@41
    .line 6259
    :catch_41
    move-exception v0

    #@42
    .line 6260
    .local v0, e:Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    #@45
    goto :goto_3a
.end method

.method public tileProfilingClear()V
    .registers 1

    #@0
    .prologue
    .line 10420
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->nativeTileProfilingClear()V

    #@3
    .line 10421
    return-void
.end method

.method public tileProfilingGetFloat(IILjava/lang/String;)F
    .registers 5
    .parameter "frame"
    .parameter "tile"
    .parameter "key"

    #@0
    .prologue
    .line 10436
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewClassic;->nativeTileProfilingGetFloat(IILjava/lang/String;)F

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public tileProfilingGetInt(IILjava/lang/String;)I
    .registers 5
    .parameter "frame"
    .parameter "tile"
    .parameter "key"

    #@0
    .prologue
    .line 10432
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewClassic;->nativeTileProfilingGetInt(IILjava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public tileProfilingNumFrames()I
    .registers 2

    #@0
    .prologue
    .line 10424
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->nativeTileProfilingNumFrames()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public tileProfilingNumTilesInFrame(I)I
    .registers 3
    .parameter "frame"

    #@0
    .prologue
    .line 10428
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->nativeTileProfilingNumTilesInFrame(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public tileProfilingStart()V
    .registers 1

    #@0
    .prologue
    .line 10407
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->nativeTileProfilingStart()V

    #@3
    .line 10408
    return-void
.end method

.method public tileProfilingStop()F
    .registers 2

    #@0
    .prologue
    .line 10415
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->nativeTileProfilingStop()F

    #@3
    move-result v0

    #@4
    return v0
.end method

.method updateDefaultZoomDensity(F)V
    .registers 3
    .parameter "density"

    #@0
    .prologue
    .line 2248
    iget v0, p0, Landroid/webkit/WebViewClassic;->TOUCH_NAV_SLOP_ABSOLUTE:I

    #@2
    int-to-float v0, v0

    #@3
    mul-float/2addr v0, p1

    #@4
    float-to-int v0, v0

    #@5
    iput v0, p0, Landroid/webkit/WebViewClassic;->mNavSlop:I

    #@7
    .line 2249
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@9
    invoke-virtual {v0, p1}, Landroid/webkit/ZoomManager;->updateDefaultZoomDensity(F)V

    #@c
    .line 2250
    return-void
.end method

.method updateDoubleTapZoom(I)V
    .registers 3
    .parameter "doubleTapZoom"

    #@0
    .prologue
    .line 3797
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/ZoomManager;->updateDoubleTapZoom(I)V

    #@5
    .line 3798
    return-void
.end method

.method updateDrawingState()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 4038
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@3
    if-eqz v0, :cond_9

    #@5
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsPaused:Z

    #@7
    if-eqz v0, :cond_a

    #@9
    .line 4046
    :cond_9
    :goto_9
    return-void

    #@a
    .line 4039
    :cond_a
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@c
    invoke-virtual {v0}, Landroid/webkit/WebView;->getWindowVisibility()I

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_18

    #@12
    .line 4040
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@14
    invoke-static {v0, v1}, Landroid/webkit/WebViewClassic;->nativeSetPauseDrawing(IZ)V

    #@17
    goto :goto_9

    #@18
    .line 4041
    :cond_18
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@1a
    invoke-virtual {v0}, Landroid/webkit/WebView;->getVisibility()I

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_26

    #@20
    .line 4042
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@22
    invoke-static {v0, v1}, Landroid/webkit/WebViewClassic;->nativeSetPauseDrawing(IZ)V

    #@25
    goto :goto_9

    #@26
    .line 4044
    :cond_26
    iget v0, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@28
    const/4 v1, 0x0

    #@29
    invoke-static {v0, v1}, Landroid/webkit/WebViewClassic;->nativeSetPauseDrawing(IZ)V

    #@2c
    goto :goto_9
.end method

.method updateJavaScriptEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 2081
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->isAccessibilityInjectionEnabled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 2082
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getAccessibilityInjector()Landroid/webkit/AccessibilityInjector;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0, p1}, Landroid/webkit/AccessibilityInjector;->updateJavaScriptEnabled(Z)V

    #@d
    .line 2084
    :cond_d
    return-void
.end method

.method updateMultiTouchSupport(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 2077
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/ZoomManager;->updateMultiTouchSupport(Landroid/content/Context;)V

    #@5
    .line 2078
    return-void
.end method

.method updateRectsForGL()V
    .registers 11

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 6568
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@3
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mTempVisibleRect:Landroid/graphics/Rect;

    #@5
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mTempVisibleRectOffset:Landroid/graphics/Point;

    #@7
    invoke-virtual {v1, v2, v3}, Landroid/webkit/WebView;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    #@a
    move-result v9

    #@b
    .line 6569
    .local v9, visible:Z
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInvScreenRect:Landroid/graphics/Rect;

    #@d
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mTempVisibleRect:Landroid/graphics/Rect;

    #@f
    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@12
    .line 6570
    if-eqz v9, :cond_70

    #@14
    .line 6572
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mWebView:Landroid/webkit/WebView;

    #@16
    invoke-virtual {v1}, Landroid/webkit/WebView;->getRootView()Landroid/view/View;

    #@19
    move-result-object v6

    #@1a
    .line 6573
    .local v6, rootView:Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    #@1d
    move-result v7

    #@1e
    .line 6574
    .local v7, rootViewHeight:I
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mScreenRect:Landroid/graphics/Rect;

    #@20
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInvScreenRect:Landroid/graphics/Rect;

    #@22
    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@25
    .line 6575
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInvScreenRect:Landroid/graphics/Rect;

    #@27
    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    #@29
    .line 6576
    .local v8, savedWebViewBottom:I
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInvScreenRect:Landroid/graphics/Rect;

    #@2b
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInvScreenRect:Landroid/graphics/Rect;

    #@2d
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@2f
    sub-int v2, v7, v2

    #@31
    invoke-direct {p0}, Landroid/webkit/WebViewClassic;->getVisibleTitleHeightImpl()I

    #@34
    move-result v3

    #@35
    sub-int/2addr v2, v3

    #@36
    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    #@38
    .line 6577
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mInvScreenRect:Landroid/graphics/Rect;

    #@3a
    sub-int v2, v7, v8

    #@3c
    iput v2, v1, Landroid/graphics/Rect;->top:I

    #@3e
    .line 6578
    const/4 v1, 0x1

    #@3f
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsWebViewVisible:Z

    #@41
    .line 6583
    .end local v6           #rootView:Landroid/view/View;
    .end local v7           #rootViewHeight:I
    .end local v8           #savedWebViewBottom:I
    :goto_41
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mTempVisibleRect:Landroid/graphics/Rect;

    #@43
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mTempVisibleRectOffset:Landroid/graphics/Point;

    #@45
    iget v2, v2, Landroid/graphics/Point;->x:I

    #@47
    neg-int v2, v2

    #@48
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mTempVisibleRectOffset:Landroid/graphics/Point;

    #@4a
    iget v3, v3, Landroid/graphics/Point;->y:I

    #@4c
    neg-int v3, v3

    #@4d
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    #@50
    .line 6584
    iget-object v1, p0, Landroid/webkit/WebViewClassic;->mVisibleContentRect:Landroid/graphics/RectF;

    #@52
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mTempVisibleRect:Landroid/graphics/Rect;

    #@54
    invoke-direct {p0, v1, v2}, Landroid/webkit/WebViewClassic;->viewToContentVisibleRect(Landroid/graphics/RectF;Landroid/graphics/Rect;)V

    #@57
    .line 6586
    iget v1, p0, Landroid/webkit/WebViewClassic;->mNativeClass:I

    #@59
    iget-boolean v2, p0, Landroid/webkit/WebViewClassic;->mIsWebViewVisible:Z

    #@5b
    if-eqz v2, :cond_74

    #@5d
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mInvScreenRect:Landroid/graphics/Rect;

    #@5f
    :goto_5f
    iget-boolean v3, p0, Landroid/webkit/WebViewClassic;->mIsWebViewVisible:Z

    #@61
    if-eqz v3, :cond_76

    #@63
    iget-object v3, p0, Landroid/webkit/WebViewClassic;->mScreenRect:Landroid/graphics/Rect;

    #@65
    :goto_65
    iget-object v4, p0, Landroid/webkit/WebViewClassic;->mVisibleContentRect:Landroid/graphics/RectF;

    #@67
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScale()F

    #@6a
    move-result v5

    #@6b
    move-object v0, p0

    #@6c
    invoke-direct/range {v0 .. v5}, Landroid/webkit/WebViewClassic;->nativeUpdateDrawGLFunction(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/RectF;F)V

    #@6f
    .line 6589
    return-void

    #@70
    .line 6580
    :cond_70
    const/4 v1, 0x0

    #@71
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic;->mIsWebViewVisible:Z

    #@73
    goto :goto_41

    #@74
    :cond_74
    move-object v2, v0

    #@75
    .line 6586
    goto :goto_5f

    #@76
    :cond_76
    move-object v3, v0

    #@77
    goto :goto_65
.end method

.method updateScrollCoordinates(II)Z
    .registers 8
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 8141
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@3
    move-result v0

    #@4
    .line 8142
    .local v0, oldX:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@7
    move-result v1

    #@8
    .line 8143
    .local v1, oldY:I
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic;->setScrollXRaw(I)V

    #@b
    .line 8144
    invoke-virtual {p0, p2}, Landroid/webkit/WebViewClassic;->setScrollYRaw(I)V

    #@e
    .line 8145
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@11
    move-result v2

    #@12
    if-ne v0, v2, :cond_1a

    #@14
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@17
    move-result v2

    #@18
    if-eq v1, v2, :cond_29

    #@1a
    .line 8146
    :cond_1a
    iget-object v2, p0, Landroid/webkit/WebViewClassic;->mWebViewPrivate:Landroid/webkit/WebView$PrivateAccess;

    #@1c
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@1f
    move-result v3

    #@20
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@23
    move-result v4

    #@24
    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/webkit/WebView$PrivateAccess;->onScrollChanged(IIII)V

    #@27
    .line 8147
    const/4 v2, 0x1

    #@28
    .line 8149
    :goto_28
    return v2

    #@29
    :cond_29
    const/4 v2, 0x0

    #@2a
    goto :goto_28
.end method

.method updateTextSelectionNeeded()Z
    .registers 2

    #@0
    .prologue
    .line 10746
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mSelectingText:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsCaretSelection:Z

    #@6
    if-eqz v0, :cond_18

    #@8
    :cond_8
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@a
    if-eqz v0, :cond_1a

    #@c
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mAutoCompletePopup:Landroid/webkit/AutoCompletePopup;

    #@e
    invoke-virtual {v0}, Landroid/webkit/AutoCompletePopup;->isShowing()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1a

    #@14
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@16
    if-eqz v0, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method viewToContentX(I)I
    .registers 3
    .parameter "x"

    #@0
    .prologue
    .line 3490
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic;->viewToContentDimension(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method viewToContentY(I)I
    .registers 3
    .parameter "y"

    #@0
    .prologue
    .line 3499
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@3
    move-result v0

    #@4
    sub-int v0, p1, v0

    #@6
    invoke-direct {p0, v0}, Landroid/webkit/WebViewClassic;->viewToContentDimension(I)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public waitForUiThreadSync()Z
    .registers 2

    #@0
    .prologue
    .line 10658
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->waitForUiThreadSync()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public zoomIn()Z
    .registers 2

    #@0
    .prologue
    .line 8445
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->zoomIn()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public zoomOut()Z
    .registers 2

    #@0
    .prologue
    .line 8453
    iget-object v0, p0, Landroid/webkit/WebViewClassic;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->zoomOut()Z

    #@5
    move-result v0

    #@6
    return v0
.end method
