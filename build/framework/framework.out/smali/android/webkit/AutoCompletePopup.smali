.class Landroid/webkit/AutoCompletePopup;
.super Ljava/lang/Object;
.source "AutoCompletePopup.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/Filter$FilterListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/AutoCompletePopup$AnchorView;
    }
.end annotation


# static fields
.field private static final AUTOFILL_FORM:I = 0x64


# instance fields
.field private mAdapter:Landroid/widget/ListAdapter;

.field private mAnchor:Landroid/view/View;

.field private mDoNotApplyFilter:Z

.field private mFilter:Landroid/widget/Filter;

.field private mHandler:Landroid/os/Handler;

.field private mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

.field private mIsAutoFillProfileSet:Z

.field private mItemClicked:Z

.field private mPopup:Landroid/widget/ListPopupWindow;

.field private mQueryId:I

.field private mText:Ljava/lang/CharSequence;

.field private mWebView:Landroid/webkit/WebViewClassic;


# direct methods
.method public constructor <init>(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewClassic$WebViewInputConnection;)V
    .registers 4
    .parameter "webView"
    .parameter "inputConnection"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 59
    iput-object p2, p0, Landroid/webkit/AutoCompletePopup;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@6
    .line 60
    iput-object p1, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@8
    .line 62
    iput-boolean v0, p0, Landroid/webkit/AutoCompletePopup;->mDoNotApplyFilter:Z

    #@a
    .line 63
    iput-boolean v0, p0, Landroid/webkit/AutoCompletePopup;->mItemClicked:Z

    #@c
    .line 65
    new-instance v0, Landroid/webkit/AutoCompletePopup$1;

    #@e
    invoke-direct {v0, p0}, Landroid/webkit/AutoCompletePopup$1;-><init>(Landroid/webkit/AutoCompletePopup;)V

    #@11
    iput-object v0, p0, Landroid/webkit/AutoCompletePopup;->mHandler:Landroid/os/Handler;

    #@13
    .line 75
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/AutoCompletePopup;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget v0, p0, Landroid/webkit/AutoCompletePopup;->mQueryId:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/webkit/AutoCompletePopup;)Landroid/webkit/WebViewClassic;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    return-object v0
.end method

.method private ensurePopup()V
    .registers 3

    #@0
    .prologue
    .line 288
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    if-nez v0, :cond_3c

    #@4
    .line 289
    new-instance v0, Landroid/widget/ListPopupWindow;

    #@6
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@8
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@b
    move-result-object v1

    #@c
    invoke-direct {v0, v1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    #@f
    iput-object v0, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@11
    .line 290
    new-instance v0, Landroid/webkit/AutoCompletePopup$AnchorView;

    #@13
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@15
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@18
    move-result-object v1

    #@19
    invoke-direct {v0, v1}, Landroid/webkit/AutoCompletePopup$AnchorView;-><init>(Landroid/content/Context;)V

    #@1c
    iput-object v0, p0, Landroid/webkit/AutoCompletePopup;->mAnchor:Landroid/view/View;

    #@1e
    .line 291
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@20
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@23
    move-result-object v0

    #@24
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mAnchor:Landroid/view/View;

    #@26
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->addView(Landroid/view/View;)V

    #@29
    .line 292
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@2b
    invoke-virtual {v0, p0}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@2e
    .line 293
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@30
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mAnchor:Landroid/view/View;

    #@32
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    #@35
    .line 294
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@37
    const/4 v1, 0x1

    #@38
    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setPromptPosition(I)V

    #@3b
    .line 298
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 295
    :cond_3c
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@3e
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@41
    move-result-object v0

    #@42
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mAnchor:Landroid/view/View;

    #@44
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->indexOfChild(Landroid/view/View;)I

    #@47
    move-result v0

    #@48
    if-gez v0, :cond_3b

    #@4a
    .line 296
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@4c
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@4f
    move-result-object v0

    #@50
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mAnchor:Landroid/view/View;

    #@52
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->addView(Landroid/view/View;)V

    #@55
    goto :goto_3b
.end method

.method private pushTextToInputConnection()V
    .registers 5

    #@0
    .prologue
    .line 234
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@2
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@5
    move-result-object v0

    #@6
    .line 235
    .local v0, oldText:Landroid/text/Editable;
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@c
    move-result v3

    #@d
    invoke-virtual {v1, v2, v3}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setSelection(II)Z

    #@10
    .line 236
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@12
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mText:Ljava/lang/CharSequence;

    #@14
    invoke-virtual {v1, v2}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->replaceSelection(Ljava/lang/CharSequence;)V

    #@17
    .line 237
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@19
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mText:Ljava/lang/CharSequence;

    #@1b
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@1e
    move-result v2

    #@1f
    iget-object v3, p0, Landroid/webkit/AutoCompletePopup;->mText:Ljava/lang/CharSequence;

    #@21
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    #@24
    move-result v3

    #@25
    invoke-virtual {v1, v2, v3}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setSelection(II)Z

    #@28
    .line 238
    return-void
.end method


# virtual methods
.method public clearAdapter()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 126
    iput-object v3, p0, Landroid/webkit/AutoCompletePopup;->mAdapter:Landroid/widget/ListAdapter;

    #@3
    .line 127
    iput-object v3, p0, Landroid/webkit/AutoCompletePopup;->mFilter:Landroid/widget/Filter;

    #@5
    .line 128
    const/4 v1, 0x0

    #@6
    iput-boolean v1, p0, Landroid/webkit/AutoCompletePopup;->mDoNotApplyFilter:Z

    #@8
    .line 129
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@a
    if-eqz v1, :cond_16

    #@c
    .line 132
    :try_start_c
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@e
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->dismiss()V
    :try_end_11
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_11} :catch_17

    #@11
    .line 138
    :goto_11
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@13
    invoke-virtual {v1, v3}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    #@16
    .line 140
    :cond_16
    return-void

    #@17
    .line 134
    :catch_17
    move-exception v0

    #@18
    .line 135
    .local v0, e:Ljava/lang/IllegalArgumentException;
    const-string v1, "AutoCompletePopup"

    #@1a
    const-string v2, "Adapter is already detached from window"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_11
.end method

.method isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 301
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@6
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public onDismiss()V
    .registers 3

    #@0
    .prologue
    .line 284
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@5
    move-result-object v0

    #@6
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mAnchor:Landroid/view/View;

    #@8
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->removeView(Landroid/view/View;)V

    #@b
    .line 285
    return-void
.end method

.method public onFilterComplete(I)V
    .registers 10
    .parameter "count"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 242
    invoke-direct {p0}, Landroid/webkit/AutoCompletePopup;->ensurePopup()V

    #@5
    .line 243
    if-lez p1, :cond_5f

    #@7
    iget-object v6, p0, Landroid/webkit/AutoCompletePopup;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@9
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getIsAutoFillable()Z

    #@c
    move-result v6

    #@d
    if-nez v6, :cond_17

    #@f
    iget-object v6, p0, Landroid/webkit/AutoCompletePopup;->mText:Ljava/lang/CharSequence;

    #@11
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    #@14
    move-result v6

    #@15
    if-lez v6, :cond_5f

    #@17
    :cond_17
    move v2, v4

    #@18
    .line 245
    .local v2, showDropDown:Z
    :goto_18
    iput-boolean v5, p0, Landroid/webkit/AutoCompletePopup;->mDoNotApplyFilter:Z

    #@1a
    .line 246
    if-eqz v2, :cond_6c

    #@1c
    .line 248
    const/4 v3, 0x1

    #@1d
    .line 249
    .local v3, showPopup:Z
    iget-boolean v6, p0, Landroid/webkit/AutoCompletePopup;->mItemClicked:Z

    #@1f
    if-ne v6, v4, :cond_3f

    #@21
    .line 250
    const/4 v1, 0x0

    #@22
    .local v1, i:I
    :goto_22
    if-ge v1, p1, :cond_3f

    #@24
    .line 252
    iget-object v6, p0, Landroid/webkit/AutoCompletePopup;->mAdapter:Landroid/widget/ListAdapter;

    #@26
    invoke-interface {v6, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    #@29
    move-result-object v0

    #@2a
    .line 253
    .local v0, Item:Ljava/lang/Object;
    if-eqz v0, :cond_61

    #@2c
    iget-object v6, p0, Landroid/webkit/AutoCompletePopup;->mText:Ljava/lang/CharSequence;

    #@2e
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    iget-object v7, p0, Landroid/webkit/AutoCompletePopup;->mFilter:Landroid/widget/Filter;

    #@34
    invoke-virtual {v7, v0}, Landroid/widget/Filter;->convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    #@37
    move-result-object v7

    #@38
    invoke-virtual {v6, v7}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    #@3b
    move-result v6

    #@3c
    if-ne v6, v4, :cond_61

    #@3e
    .line 254
    const/4 v3, 0x0

    #@3f
    .line 260
    .end local v0           #Item:Ljava/lang/Object;
    .end local v1           #i:I
    :cond_3f
    iget-object v6, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@41
    invoke-virtual {v6}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@44
    move-result v6

    #@45
    if-nez v6, :cond_4c

    #@47
    .line 262
    iget-object v6, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@49
    invoke-virtual {v6, v4}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    #@4c
    .line 267
    :cond_4c
    if-ne v3, v4, :cond_64

    #@4e
    .line 268
    iget-object v4, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@50
    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->show()V

    #@53
    .line 269
    iget-object v4, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@55
    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOverScrollMode(I)V

    #@5c
    .line 279
    .end local v3           #showPopup:Z
    :goto_5c
    iput-boolean v5, p0, Landroid/webkit/AutoCompletePopup;->mItemClicked:Z

    #@5e
    .line 280
    return-void

    #@5f
    .end local v2           #showDropDown:Z
    :cond_5f
    move v2, v5

    #@60
    .line 243
    goto :goto_18

    #@61
    .line 250
    .restart local v0       #Item:Ljava/lang/Object;
    .restart local v1       #i:I
    .restart local v2       #showDropDown:Z
    .restart local v3       #showPopup:Z
    :cond_61
    add-int/lit8 v1, v1, 0x1

    #@63
    goto :goto_22

    #@64
    .line 272
    .end local v0           #Item:Ljava/lang/Object;
    .end local v1           #i:I
    :cond_64
    iput-boolean v4, p0, Landroid/webkit/AutoCompletePopup;->mDoNotApplyFilter:Z

    #@66
    .line 273
    iget-object v4, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@68
    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->dismiss()V

    #@6b
    goto :goto_5c

    #@6c
    .line 277
    .end local v3           #showPopup:Z
    :cond_6c
    iget-object v4, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@6e
    invoke-virtual {v4}, Landroid/widget/ListPopupWindow;->dismiss()V

    #@71
    goto :goto_5c
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 10
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 193
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 227
    :goto_4
    return-void

    #@5
    .line 196
    :cond_5
    const-wide/16 v2, 0x0

    #@7
    cmp-long v2, p4, v2

    #@9
    if-nez v2, :cond_41

    #@b
    if-nez p3, :cond_41

    #@d
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@f
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getIsAutoFillable()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_41

    #@15
    .line 197
    const-string v2, ""

    #@17
    iput-object v2, p0, Landroid/webkit/AutoCompletePopup;->mText:Ljava/lang/CharSequence;

    #@19
    .line 198
    invoke-direct {p0}, Landroid/webkit/AutoCompletePopup;->pushTextToInputConnection()V

    #@1c
    .line 200
    iget-boolean v2, p0, Landroid/webkit/AutoCompletePopup;->mIsAutoFillProfileSet:Z

    #@1e
    if-eqz v2, :cond_2d

    #@20
    .line 202
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@22
    iget v3, p0, Landroid/webkit/AutoCompletePopup;->mQueryId:I

    #@24
    invoke-virtual {v2, v3}, Landroid/webkit/WebViewClassic;->autoFillForm(I)V

    #@27
    .line 226
    :cond_27
    :goto_27
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@29
    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->dismiss()V

    #@2c
    goto :goto_4

    #@2d
    .line 207
    :cond_2d
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@2f
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getWebChromeClient()Landroid/webkit/WebChromeClient;

    #@32
    move-result-object v1

    #@33
    .line 208
    .local v1, webChromeClient:Landroid/webkit/WebChromeClient;
    if-eqz v1, :cond_27

    #@35
    .line 209
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mHandler:Landroid/os/Handler;

    #@37
    const/16 v3, 0x64

    #@39
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v1, v2}, Landroid/webkit/WebChromeClient;->setupAutoFill(Landroid/os/Message;)V

    #@40
    goto :goto_27

    #@41
    .line 215
    .end local v1           #webChromeClient:Landroid/webkit/WebChromeClient;
    :cond_41
    if-gez p3, :cond_5b

    #@43
    .line 216
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@45
    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->getSelectedItem()Ljava/lang/Object;

    #@48
    move-result-object v0

    #@49
    .line 220
    .local v0, selectedItem:Ljava/lang/Object;
    :goto_49
    if-eqz v0, :cond_27

    #@4b
    .line 221
    const/4 v2, 0x1

    #@4c
    iput-boolean v2, p0, Landroid/webkit/AutoCompletePopup;->mItemClicked:Z

    #@4e
    .line 222
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mFilter:Landroid/widget/Filter;

    #@50
    invoke-virtual {v2, v0}, Landroid/widget/Filter;->convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {p0, v2}, Landroid/webkit/AutoCompletePopup;->setText(Ljava/lang/CharSequence;)V

    #@57
    .line 223
    invoke-direct {p0}, Landroid/webkit/AutoCompletePopup;->pushTextToInputConnection()V

    #@5a
    goto :goto_27

    #@5b
    .line 218
    .end local v0           #selectedItem:Ljava/lang/Object;
    :cond_5b
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mAdapter:Landroid/widget/ListAdapter;

    #@5d
    invoke-interface {v2, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    #@60
    move-result-object v0

    #@61
    .restart local v0       #selectedItem:Ljava/lang/Object;
    goto :goto_49
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 78
    iget-object v3, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@4
    if-nez v3, :cond_7

    #@6
    .line 104
    :cond_6
    :goto_6
    return v1

    #@7
    .line 81
    :cond_7
    const/4 v3, 0x4

    #@8
    if-ne p1, v3, :cond_4f

    #@a
    iget-object v3, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@c
    invoke-virtual {v3}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_4f

    #@12
    .line 84
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@15
    move-result v3

    #@16
    if-nez v3, :cond_2b

    #@18
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@1b
    move-result v3

    #@1c
    if-nez v3, :cond_2b

    #@1e
    .line 85
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mAnchor:Landroid/view/View;

    #@20
    invoke-virtual {v1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@23
    move-result-object v0

    #@24
    .line 86
    .local v0, state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_29

    #@26
    .line 87
    invoke-virtual {v0, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@29
    :cond_29
    move v1, v2

    #@2a
    .line 89
    goto :goto_6

    #@2b
    .line 90
    .end local v0           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_2b
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@2e
    move-result v3

    #@2f
    if-ne v3, v2, :cond_4f

    #@31
    .line 91
    iget-object v3, p0, Landroid/webkit/AutoCompletePopup;->mAnchor:Landroid/view/View;

    #@33
    invoke-virtual {v3}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@36
    move-result-object v0

    #@37
    .line 92
    .restart local v0       #state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_3c

    #@39
    .line 93
    invoke-virtual {v0, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    #@3c
    .line 95
    :cond_3c
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    #@3f
    move-result v3

    #@40
    if-eqz v3, :cond_4f

    #@42
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@45
    move-result v3

    #@46
    if-nez v3, :cond_4f

    #@48
    .line 96
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@4a
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->dismiss()V

    #@4d
    move v1, v2

    #@4e
    .line 97
    goto :goto_6

    #@4f
    .line 101
    .end local v0           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_4f
    iget-object v2, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@51
    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@54
    move-result v2

    #@55
    if-eqz v2, :cond_6

    #@57
    .line 102
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@59
    invoke-virtual {v1, p1, p2}, Landroid/widget/ListPopupWindow;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    #@5c
    move-result v1

    #@5d
    goto :goto_6
.end method

.method public resetRect()V
    .registers 11

    #@0
    .prologue
    .line 157
    invoke-direct {p0}, Landroid/webkit/AutoCompletePopup;->ensurePopup()V

    #@3
    .line 158
    iget-object v8, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@5
    iget-object v9, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v9, v9, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@9
    iget v9, v9, Landroid/graphics/Rect;->left:I

    #@b
    invoke-virtual {v8, v9}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@e
    move-result v2

    #@f
    .line 159
    .local v2, left:I
    iget-object v8, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@11
    iget-object v9, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@13
    iget-object v9, v9, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@15
    iget v9, v9, Landroid/graphics/Rect;->right:I

    #@17
    invoke-virtual {v8, v9}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@1a
    move-result v5

    #@1b
    .line 160
    .local v5, right:I
    sub-int v7, v5, v2

    #@1d
    .line 161
    .local v7, width:I
    iget-object v8, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@1f
    invoke-virtual {v8, v7}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    #@22
    .line 163
    iget-object v8, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@24
    iget-object v9, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@26
    iget-object v9, v9, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@28
    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    #@2a
    invoke-virtual {v8, v9}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@2d
    move-result v0

    #@2e
    .line 164
    .local v0, bottom:I
    iget-object v8, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@30
    iget-object v9, p0, Landroid/webkit/AutoCompletePopup;->mWebView:Landroid/webkit/WebViewClassic;

    #@32
    iget-object v9, v9, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@34
    iget v9, v9, Landroid/graphics/Rect;->top:I

    #@36
    invoke-virtual {v8, v9}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@39
    move-result v6

    #@3a
    .line 165
    .local v6, top:I
    sub-int v1, v0, v6

    #@3c
    .line 167
    .local v1, height:I
    iget-object v8, p0, Landroid/webkit/AutoCompletePopup;->mAnchor:Landroid/view/View;

    #@3e
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@41
    move-result-object v3

    #@42
    check-cast v3, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@44
    .line 169
    .local v3, lp:Landroid/widget/AbsoluteLayout$LayoutParams;
    const/4 v4, 0x0

    #@45
    .line 170
    .local v4, needsUpdate:Z
    if-nez v3, :cond_61

    #@47
    .line 171
    new-instance v3, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@49
    .end local v3           #lp:Landroid/widget/AbsoluteLayout$LayoutParams;
    invoke-direct {v3, v7, v1, v2, v6}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    #@4c
    .line 182
    .restart local v3       #lp:Landroid/widget/AbsoluteLayout$LayoutParams;
    :cond_4c
    :goto_4c
    if-eqz v4, :cond_53

    #@4e
    .line 183
    iget-object v8, p0, Landroid/webkit/AutoCompletePopup;->mAnchor:Landroid/view/View;

    #@50
    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@53
    .line 185
    :cond_53
    iget-object v8, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@55
    invoke-virtual {v8}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@58
    move-result v8

    #@59
    if-eqz v8, :cond_60

    #@5b
    .line 186
    iget-object v8, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@5d
    invoke-virtual {v8}, Landroid/widget/ListPopupWindow;->show()V

    #@60
    .line 188
    :cond_60
    return-void

    #@61
    .line 173
    :cond_61
    iget v8, v3, Landroid/widget/AbsoluteLayout$LayoutParams;->x:I

    #@63
    if-ne v8, v2, :cond_71

    #@65
    iget v8, v3, Landroid/widget/AbsoluteLayout$LayoutParams;->y:I

    #@67
    if-ne v8, v6, :cond_71

    #@69
    iget v8, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@6b
    if-ne v8, v7, :cond_71

    #@6d
    iget v8, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@6f
    if-eq v8, v1, :cond_4c

    #@71
    .line 175
    :cond_71
    const/4 v4, 0x1

    #@72
    .line 176
    iput v2, v3, Landroid/widget/AbsoluteLayout$LayoutParams;->x:I

    #@74
    .line 177
    iput v6, v3, Landroid/widget/AbsoluteLayout$LayoutParams;->y:I

    #@76
    .line 178
    iput v7, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@78
    .line 179
    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@7a
    goto :goto_4c
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/widget/ListAdapter;",
            ":",
            "Landroid/widget/Filterable;",
            ">(TT;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 143
    .local p1, adapter:Landroid/widget/ListAdapter;,"TT;"
    invoke-direct {p0}, Landroid/webkit/AutoCompletePopup;->ensurePopup()V

    #@3
    .line 144
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mPopup:Landroid/widget/ListPopupWindow;

    #@5
    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    #@8
    .line 145
    iput-object p1, p0, Landroid/webkit/AutoCompletePopup;->mAdapter:Landroid/widget/ListAdapter;

    #@a
    .line 146
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/webkit/AutoCompletePopup;->mDoNotApplyFilter:Z

    #@d
    .line 147
    if-eqz p1, :cond_22

    #@f
    .line 148
    check-cast p1, Landroid/widget/Filterable;

    #@11
    .end local p1           #adapter:Landroid/widget/ListAdapter;,"TT;"
    invoke-interface {p1}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Landroid/webkit/AutoCompletePopup;->mFilter:Landroid/widget/Filter;

    #@17
    .line 149
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mFilter:Landroid/widget/Filter;

    #@19
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mText:Ljava/lang/CharSequence;

    #@1b
    invoke-virtual {v0, v1, p0}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    #@1e
    .line 153
    :goto_1e
    invoke-virtual {p0}, Landroid/webkit/AutoCompletePopup;->resetRect()V

    #@21
    .line 154
    return-void

    #@22
    .line 151
    .restart local p1       #adapter:Landroid/widget/ListAdapter;,"TT;"
    :cond_22
    const/4 v0, 0x0

    #@23
    iput-object v0, p0, Landroid/webkit/AutoCompletePopup;->mFilter:Landroid/widget/Filter;

    #@25
    goto :goto_1e
.end method

.method public setAutoFillQueryId(I)V
    .registers 2
    .parameter "queryId"

    #@0
    .prologue
    .line 122
    iput p1, p0, Landroid/webkit/AutoCompletePopup;->mQueryId:I

    #@2
    .line 123
    return-void
.end method

.method public setIsAutoFillProfileSet(Z)V
    .registers 2
    .parameter "isAutoFillProfileSet"

    #@0
    .prologue
    .line 230
    iput-boolean p1, p0, Landroid/webkit/AutoCompletePopup;->mIsAutoFillProfileSet:Z

    #@2
    .line 231
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "text"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 109
    if-eqz p1, :cond_18

    #@3
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mText:Ljava/lang/CharSequence;

    #@5
    if-eqz v0, :cond_18

    #@7
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/webkit/AutoCompletePopup;->mText:Ljava/lang/CharSequence;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    #@10
    move-result v0

    #@11
    if-ne v0, v2, :cond_18

    #@13
    iget-boolean v0, p0, Landroid/webkit/AutoCompletePopup;->mDoNotApplyFilter:Z

    #@15
    if-ne v0, v2, :cond_18

    #@17
    .line 119
    :cond_17
    :goto_17
    return-void

    #@18
    .line 112
    :cond_18
    const/4 v0, 0x0

    #@19
    iput-boolean v0, p0, Landroid/webkit/AutoCompletePopup;->mDoNotApplyFilter:Z

    #@1b
    .line 114
    iput-object p1, p0, Landroid/webkit/AutoCompletePopup;->mText:Ljava/lang/CharSequence;

    #@1d
    .line 115
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mFilter:Landroid/widget/Filter;

    #@1f
    if-eqz v0, :cond_17

    #@21
    .line 116
    iput-boolean v2, p0, Landroid/webkit/AutoCompletePopup;->mDoNotApplyFilter:Z

    #@23
    .line 117
    iget-object v0, p0, Landroid/webkit/AutoCompletePopup;->mFilter:Landroid/widget/Filter;

    #@25
    invoke-virtual {v0, p1, p0}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    #@28
    goto :goto_17
.end method
