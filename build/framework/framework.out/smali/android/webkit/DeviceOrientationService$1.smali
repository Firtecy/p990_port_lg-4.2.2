.class Landroid/webkit/DeviceOrientationService$1;
.super Ljava/lang/Object;
.source "DeviceOrientationService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/DeviceOrientationService;->sendErrorEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Landroid/webkit/DeviceOrientationService;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 83
    const-class v0, Landroid/webkit/DeviceOrientationService;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/DeviceOrientationService$1;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method constructor <init>(Landroid/webkit/DeviceOrientationService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 83
    iput-object p1, p0, Landroid/webkit/DeviceOrientationService$1;->this$0:Landroid/webkit/DeviceOrientationService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 86
    sget-boolean v0, Landroid/webkit/DeviceOrientationService$1;->$assertionsDisabled:Z

    #@3
    if-nez v0, :cond_1b

    #@5
    const-string v0, "WebViewCoreThread"

    #@7
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_1b

    #@15
    new-instance v0, Ljava/lang/AssertionError;

    #@17
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@1a
    throw v0

    #@1b
    .line 87
    :cond_1b
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService$1;->this$0:Landroid/webkit/DeviceOrientationService;

    #@1d
    invoke-static {v0}, Landroid/webkit/DeviceOrientationService;->access$000(Landroid/webkit/DeviceOrientationService;)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_2c

    #@23
    .line 89
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService$1;->this$0:Landroid/webkit/DeviceOrientationService;

    #@25
    invoke-static {v0}, Landroid/webkit/DeviceOrientationService;->access$100(Landroid/webkit/DeviceOrientationService;)Landroid/webkit/DeviceMotionAndOrientationManager;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0, v2, v2, v2}, Landroid/webkit/DeviceMotionAndOrientationManager;->onOrientationChange(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)V

    #@2c
    .line 91
    :cond_2c
    return-void
.end method
