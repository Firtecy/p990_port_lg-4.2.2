.class Landroid/webkit/LGWebNotifications$1Task;
.super Landroid/os/AsyncTask;
.source "LGWebNotifications.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/LGWebNotifications;->show(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Task"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field iconUrl:Ljava/lang/String;

.field id:I

.field text:Ljava/lang/String;

.field final synthetic this$0:Landroid/webkit/LGWebNotifications;

.field title:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/webkit/LGWebNotifications;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 147
    iput-object p1, p0, Landroid/webkit/LGWebNotifications$1Task;->this$0:Landroid/webkit/LGWebNotifications;

    #@2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "params"

    #@0
    .prologue
    .line 155
    iget-object v0, p0, Landroid/webkit/LGWebNotifications$1Task;->this$0:Landroid/webkit/LGWebNotifications;

    #@2
    iget-object v1, p0, Landroid/webkit/LGWebNotifications$1Task;->iconUrl:Ljava/lang/String;

    #@4
    invoke-static {v0, v1}, Landroid/webkit/LGWebNotifications;->access$000(Landroid/webkit/LGWebNotifications;Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 147
    check-cast p1, [Ljava/lang/Void;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/webkit/LGWebNotifications$1Task;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .registers 6
    .parameter "result"

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Landroid/webkit/LGWebNotifications$1Task;->this$0:Landroid/webkit/LGWebNotifications;

    #@2
    iget v1, p0, Landroid/webkit/LGWebNotifications$1Task;->id:I

    #@4
    iget-object v2, p0, Landroid/webkit/LGWebNotifications$1Task;->title:Ljava/lang/String;

    #@6
    iget-object v3, p0, Landroid/webkit/LGWebNotifications$1Task;->text:Ljava/lang/String;

    #@8
    invoke-static {v0, v1, v2, v3, p1}, Landroid/webkit/LGWebNotifications;->access$100(Landroid/webkit/LGWebNotifications;ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    #@b
    .line 159
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 147
    check-cast p1, Landroid/graphics/Bitmap;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/webkit/LGWebNotifications$1Task;->onPostExecute(Landroid/graphics/Bitmap;)V

    #@5
    return-void
.end method
