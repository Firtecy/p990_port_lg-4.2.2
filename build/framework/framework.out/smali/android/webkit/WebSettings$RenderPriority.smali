.class public final enum Landroid/webkit/WebSettings$RenderPriority;
.super Ljava/lang/Enum;
.source "WebSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RenderPriority"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/webkit/WebSettings$RenderPriority;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/webkit/WebSettings$RenderPriority;

.field public static final enum HIGH:Landroid/webkit/WebSettings$RenderPriority;

.field public static final enum LOW:Landroid/webkit/WebSettings$RenderPriority;

.field public static final enum NORMAL:Landroid/webkit/WebSettings$RenderPriority;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 133
    new-instance v0, Landroid/webkit/WebSettings$RenderPriority;

    #@5
    const-string v1, "NORMAL"

    #@7
    invoke-direct {v0, v1, v2}, Landroid/webkit/WebSettings$RenderPriority;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Landroid/webkit/WebSettings$RenderPriority;->NORMAL:Landroid/webkit/WebSettings$RenderPriority;

    #@c
    .line 134
    new-instance v0, Landroid/webkit/WebSettings$RenderPriority;

    #@e
    const-string v1, "HIGH"

    #@10
    invoke-direct {v0, v1, v3}, Landroid/webkit/WebSettings$RenderPriority;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    #@15
    .line 135
    new-instance v0, Landroid/webkit/WebSettings$RenderPriority;

    #@17
    const-string v1, "LOW"

    #@19
    invoke-direct {v0, v1, v4}, Landroid/webkit/WebSettings$RenderPriority;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Landroid/webkit/WebSettings$RenderPriority;->LOW:Landroid/webkit/WebSettings$RenderPriority;

    #@1e
    .line 132
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Landroid/webkit/WebSettings$RenderPriority;

    #@21
    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->NORMAL:Landroid/webkit/WebSettings$RenderPriority;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->LOW:Landroid/webkit/WebSettings$RenderPriority;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Landroid/webkit/WebSettings$RenderPriority;->$VALUES:[Landroid/webkit/WebSettings$RenderPriority;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/webkit/WebSettings$RenderPriority;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 132
    const-class v0, Landroid/webkit/WebSettings$RenderPriority;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/webkit/WebSettings$RenderPriority;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/webkit/WebSettings$RenderPriority;
    .registers 1

    #@0
    .prologue
    .line 132
    sget-object v0, Landroid/webkit/WebSettings$RenderPriority;->$VALUES:[Landroid/webkit/WebSettings$RenderPriority;

    #@2
    invoke-virtual {v0}, [Landroid/webkit/WebSettings$RenderPriority;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/webkit/WebSettings$RenderPriority;

    #@8
    return-object v0
.end method
