.class public Landroid/webkit/JsPromptResult;
.super Landroid/webkit/JsResult;
.source "JsPromptResult.java"


# instance fields
.field private mStringResult:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/webkit/JsResult$ResultReceiver;)V
    .registers 2
    .parameter "receiver"

    #@0
    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/webkit/JsResult;-><init>(Landroid/webkit/JsResult$ResultReceiver;)V

    #@3
    .line 44
    return-void
.end method


# virtual methods
.method public confirm(Ljava/lang/String;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 35
    iput-object p1, p0, Landroid/webkit/JsPromptResult;->mStringResult:Ljava/lang/String;

    #@2
    .line 36
    invoke-virtual {p0}, Landroid/webkit/JsPromptResult;->confirm()V

    #@5
    .line 37
    return-void
.end method

.method public getStringResult()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/webkit/JsPromptResult;->mStringResult:Ljava/lang/String;

    #@2
    return-object v0
.end method
