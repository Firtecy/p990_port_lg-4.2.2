.class Landroid/webkit/WebViewClassic$PastePopupWindow;
.super Landroid/widget/PopupWindow;
.source "WebViewClassic.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PastePopupWindow"
.end annotation


# instance fields
.field private mCliptrayView:Landroid/widget/TextView;

.field private mContentView:Landroid/view/ViewGroup;

.field private mPasteTextView:Landroid/widget/TextView;

.field private mTracks:Landroid/view/ViewGroup;

.field final synthetic this$0:Landroid/webkit/WebViewClassic;


# direct methods
.method public constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 10
    .parameter

    #@0
    .prologue
    const v7, 0x2030004

    #@3
    const/4 v6, -0x2

    #@4
    const/4 v5, 0x0

    #@5
    .line 824
    iput-object p1, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->this$0:Landroid/webkit/WebViewClassic;

    #@7
    .line 825
    invoke-static {p1}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@a
    move-result-object v3

    #@b
    const v4, 0x10102c8

    #@e
    invoke-direct {p0, v3, v5, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@11
    .line 827
    const/4 v3, 0x1

    #@12
    invoke-virtual {p0, v3}, Landroid/webkit/WebViewClassic$PastePopupWindow;->setClippingEnabled(Z)V

    #@15
    .line 828
    invoke-static {p1}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@18
    move-result-object v3

    #@19
    const-string/jumbo v4, "layout_inflater"

    #@1c
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/view/LayoutInflater;

    #@22
    .line 832
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v3, 0x2030003

    #@25
    invoke-virtual {v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@28
    move-result-object v3

    #@29
    check-cast v3, Landroid/view/ViewGroup;

    #@2b
    iput-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@2d
    .line 834
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@2f
    const v4, 0x20d0041

    #@32
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@35
    move-result-object v3

    #@36
    check-cast v3, Landroid/view/ViewGroup;

    #@38
    iput-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@3a
    .line 835
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@3c
    const v4, 0x10805d0

    #@3f
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    #@42
    .line 839
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    #@44
    invoke-direct {v2, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@47
    .line 842
    .local v2, wrapContent:Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v0, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@4a
    move-result-object v3

    #@4b
    check-cast v3, Landroid/widget/TextView;

    #@4d
    iput-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@4f
    .line 844
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@51
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@54
    .line 846
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@56
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@58
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@5b
    .line 847
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@5d
    const v4, 0x104000b

    #@60
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    #@63
    .line 850
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@65
    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@68
    .line 854
    invoke-virtual {p1}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@6b
    move-result-object v1

    #@6c
    .line 855
    .local v1, settings:Landroid/webkit/WebSettings;
    if-eqz v1, :cond_95

    #@6e
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@71
    move-result v3

    #@72
    if-eqz v3, :cond_95

    #@74
    .line 856
    invoke-virtual {v0, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@77
    move-result-object v3

    #@78
    check-cast v3, Landroid/widget/TextView;

    #@7a
    iput-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@7c
    .line 858
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@7e
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@81
    .line 859
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@83
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@85
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@88
    .line 860
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@8a
    const v4, 0x20902a0

    #@8d
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    #@90
    .line 863
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@92
    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@95
    .line 867
    :cond_95
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@97
    if-eqz v3, :cond_9e

    #@99
    .line 868
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@9b
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@9e
    .line 870
    :cond_9e
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@a0
    invoke-virtual {p0, v3}, Landroid/webkit/WebViewClassic$PastePopupWindow;->setContentView(Landroid/view/View;)V

    #@a3
    .line 871
    return-void
.end method


# virtual methods
.method public hide()V
    .registers 1

    #@0
    .prologue
    .line 898
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$PastePopupWindow;->dismiss()V

    #@3
    .line 899
    return-void
.end method

.method protected measureContent()V
    .registers 6

    #@0
    .prologue
    const/high16 v4, -0x8000

    #@2
    .line 954
    iget-object v1, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->this$0:Landroid/webkit/WebViewClassic;

    #@4
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@f
    move-result-object v0

    #@10
    .line 955
    .local v0, displayMetrics:Landroid/util/DisplayMetrics;
    iget-object v1, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@12
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@14
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@17
    move-result v2

    #@18
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@1a
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1d
    move-result v3

    #@1e
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->measure(II)V

    #@21
    .line 960
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "v"
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 904
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    #@4
    move-result v2

    #@5
    packed-switch v2, :pswitch_data_4a

    #@8
    .line 930
    :goto_8
    :pswitch_8
    const/4 v2, 0x1

    #@9
    return v2

    #@a
    .line 906
    :pswitch_a
    const v2, -0x3c2d29

    #@d
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    #@10
    goto :goto_8

    #@11
    .line 909
    :pswitch_11
    invoke-virtual {p1, v3}, Landroid/view/View;->playSoundEffect(I)V

    #@14
    .line 911
    iget-object v2, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mPasteTextView:Landroid/widget/TextView;

    #@16
    if-ne p1, v2, :cond_26

    #@18
    .line 912
    iget-object v2, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->this$0:Landroid/webkit/WebViewClassic;

    #@1a
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->pasteFromClipboard()V

    #@1d
    .line 923
    :cond_1d
    :goto_1d
    iget-object v2, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->this$0:Landroid/webkit/WebViewClassic;

    #@1f
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@22
    .line 924
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackgroundColor(I)V

    #@25
    goto :goto_8

    #@26
    .line 913
    :cond_26
    iget-object v2, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@28
    if-ne p1, v2, :cond_1d

    #@2a
    .line 914
    iget-object v2, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->this$0:Landroid/webkit/WebViewClassic;

    #@2c
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@2f
    move-result-object v1

    #@30
    .line 915
    .local v1, settings:Landroid/webkit/WebSettings;
    if-eqz v1, :cond_1d

    #@32
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@35
    move-result v2

    #@36
    if-eqz v2, :cond_1d

    #@38
    .line 916
    invoke-static {}, Landroid/webkit/LGCliptrayManager;->getInstance()Landroid/webkit/LGCliptrayManager;

    #@3b
    move-result-object v0

    #@3c
    .line 917
    .local v0, cliptray:Landroid/webkit/LGCliptrayManager;
    if-eqz v0, :cond_1d

    #@3e
    .line 918
    iget-object v2, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->this$0:Landroid/webkit/WebViewClassic;

    #@40
    iget-object v2, v2, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@42
    invoke-virtual {v0, v2}, Landroid/webkit/LGCliptrayManager;->showCliptray(Landroid/webkit/WebViewClassic$WebViewInputConnection;)V

    #@45
    goto :goto_1d

    #@46
    .line 927
    .end local v0           #cliptray:Landroid/webkit/LGCliptrayManager;
    .end local v1           #settings:Landroid/webkit/WebSettings;
    :pswitch_46
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackgroundColor(I)V

    #@49
    goto :goto_8

    #@4a
    .line 904
    :pswitch_data_4a
    .packed-switch 0x0
        :pswitch_a
        :pswitch_11
        :pswitch_8
        :pswitch_46
    .end packed-switch
.end method

.method public show(Landroid/graphics/Point;Landroid/graphics/Point;II)V
    .registers 11
    .parameter "cursorBottom"
    .parameter "cursorTop"
    .parameter "windowLeft"
    .parameter "windowTop"

    #@0
    .prologue
    .line 875
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$PastePopupWindow;->measureContent()V

    #@3
    .line 877
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@5
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    #@8
    move-result v1

    #@9
    .line 878
    .local v1, width:I
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@b
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    #@e
    move-result v0

    #@f
    .line 879
    .local v0, height:I
    iget v4, p2, Landroid/graphics/Point;->y:I

    #@11
    sub-int v3, v4, v0

    #@13
    .line 880
    .local v3, y:I
    iget v4, p2, Landroid/graphics/Point;->x:I

    #@15
    div-int/lit8 v5, v1, 0x2

    #@17
    sub-int v2, v4, v5

    #@19
    .line 881
    .local v2, x:I
    if-ge v3, p4, :cond_34

    #@1b
    .line 884
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->this$0:Landroid/webkit/WebViewClassic;

    #@1d
    invoke-static {v4}, Landroid/webkit/WebViewClassic;->access$900(Landroid/webkit/WebViewClassic;)V

    #@20
    .line 885
    iget v4, p1, Landroid/graphics/Point;->y:I

    #@22
    iget-object v5, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->this$0:Landroid/webkit/WebViewClassic;

    #@24
    invoke-static {v5}, Landroid/webkit/WebViewClassic;->access$1000(Landroid/webkit/WebViewClassic;)Landroid/graphics/drawable/Drawable;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@2b
    move-result v5

    #@2c
    add-int v3, v4, v5

    #@2e
    .line 886
    iget v4, p1, Landroid/graphics/Point;->x:I

    #@30
    div-int/lit8 v5, v1, 0x2

    #@32
    sub-int v2, v4, v5

    #@34
    .line 888
    :cond_34
    if-ge v2, p3, :cond_37

    #@36
    .line 889
    move v2, p3

    #@37
    .line 891
    :cond_37
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$PastePopupWindow;->isShowing()Z

    #@3a
    move-result v4

    #@3b
    if-nez v4, :cond_47

    #@3d
    .line 892
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PastePopupWindow;->this$0:Landroid/webkit/WebViewClassic;

    #@3f
    invoke-static {v4}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@42
    move-result-object v4

    #@43
    const/4 v5, 0x0

    #@44
    invoke-virtual {p0, v4, v5, v2, v3}, Landroid/webkit/WebViewClassic$PastePopupWindow;->showAtLocation(Landroid/view/View;III)V

    #@47
    .line 894
    :cond_47
    invoke-virtual {p0, v2, v3, v1, v0}, Landroid/webkit/WebViewClassic$PastePopupWindow;->update(IIII)V

    #@4a
    .line 895
    return-void
.end method
