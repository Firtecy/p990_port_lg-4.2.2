.class public final Landroid/webkit/ClientCertRequestHandler;
.super Landroid/os/Handler;
.source "ClientCertRequestHandler.java"


# instance fields
.field private final mBrowserFrame:Landroid/webkit/BrowserFrame;

.field private final mHandle:I

.field private final mHostAndPort:Ljava/lang/String;

.field private final mTable:Landroid/webkit/SslClientCertLookupTable;


# direct methods
.method constructor <init>(Landroid/webkit/BrowserFrame;ILjava/lang/String;Landroid/webkit/SslClientCertLookupTable;)V
    .registers 5
    .parameter "browserFrame"
    .parameter "handle"
    .parameter "host_and_port"
    .parameter "table"

    #@0
    .prologue
    .line 44
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 45
    iput-object p1, p0, Landroid/webkit/ClientCertRequestHandler;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@5
    .line 46
    iput p2, p0, Landroid/webkit/ClientCertRequestHandler;->mHandle:I

    #@7
    .line 47
    iput-object p3, p0, Landroid/webkit/ClientCertRequestHandler;->mHostAndPort:Ljava/lang/String;

    #@9
    .line 48
    iput-object p4, p0, Landroid/webkit/ClientCertRequestHandler;->mTable:Landroid/webkit/SslClientCertLookupTable;

    #@b
    .line 49
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/ClientCertRequestHandler;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget v0, p0, Landroid/webkit/ClientCertRequestHandler;->mHandle:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/webkit/ClientCertRequestHandler;)Landroid/webkit/BrowserFrame;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget-object v0, p0, Landroid/webkit/ClientCertRequestHandler;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@2
    return-object v0
.end method

.method private setSslClientCertFromCtx(I[[B)V
    .registers 4
    .parameter "ctx"
    .parameter "chainBytes"

    #@0
    .prologue
    .line 82
    new-instance v0, Landroid/webkit/ClientCertRequestHandler$2;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/webkit/ClientCertRequestHandler$2;-><init>(Landroid/webkit/ClientCertRequestHandler;I[[B)V

    #@5
    invoke-virtual {p0, v0}, Landroid/webkit/ClientCertRequestHandler;->post(Ljava/lang/Runnable;)Z

    #@8
    .line 87
    return-void
.end method

.method private setSslClientCertFromPKCS8([B[[B)V
    .registers 4
    .parameter "key"
    .parameter "chainBytes"

    #@0
    .prologue
    .line 93
    new-instance v0, Landroid/webkit/ClientCertRequestHandler$3;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/webkit/ClientCertRequestHandler$3;-><init>(Landroid/webkit/ClientCertRequestHandler;[B[[B)V

    #@5
    invoke-virtual {p0, v0}, Landroid/webkit/ClientCertRequestHandler;->post(Ljava/lang/Runnable;)Z

    #@8
    .line 98
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 3

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Landroid/webkit/ClientCertRequestHandler;->mTable:Landroid/webkit/SslClientCertLookupTable;

    #@2
    iget-object v1, p0, Landroid/webkit/ClientCertRequestHandler;->mHostAndPort:Ljava/lang/String;

    #@4
    invoke-virtual {v0, v1}, Landroid/webkit/SslClientCertLookupTable;->Deny(Ljava/lang/String;)V

    #@7
    .line 116
    new-instance v0, Landroid/webkit/ClientCertRequestHandler$5;

    #@9
    invoke-direct {v0, p0}, Landroid/webkit/ClientCertRequestHandler$5;-><init>(Landroid/webkit/ClientCertRequestHandler;)V

    #@c
    invoke-virtual {p0, v0}, Landroid/webkit/ClientCertRequestHandler;->post(Ljava/lang/Runnable;)Z

    #@f
    .line 121
    return-void
.end method

.method public ignore()V
    .registers 2

    #@0
    .prologue
    .line 104
    new-instance v0, Landroid/webkit/ClientCertRequestHandler$4;

    #@2
    invoke-direct {v0, p0}, Landroid/webkit/ClientCertRequestHandler$4;-><init>(Landroid/webkit/ClientCertRequestHandler;)V

    #@5
    invoke-virtual {p0, v0}, Landroid/webkit/ClientCertRequestHandler;->post(Ljava/lang/Runnable;)Z

    #@8
    .line 109
    return-void
.end method

.method public proceed(Ljava/security/PrivateKey;[Ljava/security/cert/X509Certificate;)V
    .registers 7
    .parameter "privateKey"
    .parameter "chain"

    #@0
    .prologue
    .line 56
    :try_start_0
    invoke-static {p2}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->encodeCertificates([Ljava/security/cert/Certificate;)[[B

    #@3
    move-result-object v0

    #@4
    .line 57
    .local v0, chainBytes:[[B
    iget-object v2, p0, Landroid/webkit/ClientCertRequestHandler;->mTable:Landroid/webkit/SslClientCertLookupTable;

    #@6
    iget-object v3, p0, Landroid/webkit/ClientCertRequestHandler;->mHostAndPort:Ljava/lang/String;

    #@8
    invoke-virtual {v2, v3, p1, v0}, Landroid/webkit/SslClientCertLookupTable;->Allow(Ljava/lang/String;Ljava/security/PrivateKey;[[B)V

    #@b
    .line 59
    instance-of v2, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    #@d
    if-eqz v2, :cond_19

    #@f
    .line 60
    check-cast p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    #@11
    .end local p1
    invoke-virtual {p1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;->getPkeyContext()I

    #@14
    move-result v2

    #@15
    invoke-direct {p0, v2, v0}, Landroid/webkit/ClientCertRequestHandler;->setSslClientCertFromCtx(I[[B)V

    #@18
    .line 76
    .end local v0           #chainBytes:[[B
    :goto_18
    return-void

    #@19
    .line 62
    .restart local v0       #chainBytes:[[B
    .restart local p1
    :cond_19
    instance-of v2, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;

    #@1b
    if-eqz v2, :cond_31

    #@1d
    .line 63
    check-cast p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;

    #@1f
    .end local p1
    invoke-virtual {p1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;->getPkeyContext()I

    #@22
    move-result v2

    #@23
    invoke-direct {p0, v2, v0}, Landroid/webkit/ClientCertRequestHandler;->setSslClientCertFromCtx(I[[B)V
    :try_end_26
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_26} :catch_27

    #@26
    goto :goto_18

    #@27
    .line 68
    .end local v0           #chainBytes:[[B
    :catch_27
    move-exception v1

    #@28
    .line 69
    .local v1, e:Ljava/security/cert/CertificateEncodingException;
    new-instance v2, Landroid/webkit/ClientCertRequestHandler$1;

    #@2a
    invoke-direct {v2, p0}, Landroid/webkit/ClientCertRequestHandler$1;-><init>(Landroid/webkit/ClientCertRequestHandler;)V

    #@2d
    invoke-virtual {p0, v2}, Landroid/webkit/ClientCertRequestHandler;->post(Ljava/lang/Runnable;)Z

    #@30
    goto :goto_18

    #@31
    .line 66
    .end local v1           #e:Ljava/security/cert/CertificateEncodingException;
    .restart local v0       #chainBytes:[[B
    .restart local p1
    :cond_31
    :try_start_31
    invoke-interface {p1}, Ljava/security/PrivateKey;->getEncoded()[B

    #@34
    move-result-object v2

    #@35
    invoke-direct {p0, v2, v0}, Landroid/webkit/ClientCertRequestHandler;->setSslClientCertFromPKCS8([B[[B)V
    :try_end_38
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_31 .. :try_end_38} :catch_27

    #@38
    goto :goto_18
.end method
