.class public Landroid/webkit/LGTextDragShadowBuilder;
.super Landroid/view/View$DragShadowBuilder;
.source "LGTextDragShadowBuilder.java"


# static fields
.field private static final DRAG_SHADOW_MAX_TEXT_LENGTH:I = 0x14


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 25
    invoke-direct {p0}, Landroid/view/View$DragShadowBuilder;-><init>()V

    #@3
    .line 26
    return-void
.end method

.method private constructor <init>(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    #@3
    .line 30
    return-void
.end method

.method public static fromCharSequence(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/view/View$DragShadowBuilder;
    .registers 10
    .parameter "context"
    .parameter "text"

    #@0
    .prologue
    const/16 v7, 0x14

    #@2
    const/4 v6, -0x2

    #@3
    const/4 v5, 0x0

    #@4
    .line 38
    if-nez p1, :cond_e

    #@6
    .line 39
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v4, "String cannot be null"

    #@a
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v3

    #@e
    .line 42
    :cond_e
    const v3, 0x10900d6

    #@11
    const/4 v4, 0x0

    #@12
    invoke-static {p0, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/widget/TextView;

    #@18
    .line 45
    .local v1, shadowView:Landroid/widget/TextView;
    if-nez v1, :cond_22

    #@1a
    .line 46
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@1c
    const-string v4, "Unable to inflate text drag thumbnail"

    #@1e
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v3

    #@22
    .line 49
    :cond_22
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@25
    move-result v3

    #@26
    if-le v3, v7, :cond_2c

    #@28
    .line 50
    invoke-interface {p1, v5, v7}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@2b
    move-result-object p1

    #@2c
    .line 52
    :cond_2c
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@2f
    .line 53
    const/high16 v3, -0x100

    #@31
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    #@34
    .line 54
    const/16 v3, 0x10

    #@36
    invoke-virtual {v1, p0, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@39
    .line 55
    const/16 v3, 0x11

    #@3b
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setGravity(I)V

    #@3e
    .line 56
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    #@40
    invoke-direct {v3, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@43
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@46
    .line 59
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@49
    move-result v2

    #@4a
    .line 60
    .local v2, size:I
    invoke-virtual {v1, v2, v2}, Landroid/widget/TextView;->measure(II)V

    #@4d
    .line 61
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    #@50
    move-result v3

    #@51
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    #@54
    move-result v4

    #@55
    invoke-virtual {v1, v5, v5, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    #@58
    .line 63
    new-instance v0, Landroid/webkit/LGTextDragShadowBuilder;

    #@5a
    invoke-direct {v0, v1}, Landroid/webkit/LGTextDragShadowBuilder;-><init>(Landroid/view/View;)V

    #@5d
    .line 64
    .local v0, builder:Landroid/webkit/LGTextDragShadowBuilder;
    return-object v0
.end method

.method public static fromTextView(Landroid/content/Context;Landroid/widget/TextView;)Landroid/view/View$DragShadowBuilder;
    .registers 3
    .parameter "context"
    .parameter "textView"

    #@0
    .prologue
    .line 33
    new-instance v0, Landroid/webkit/LGTextDragShadowBuilder;

    #@2
    invoke-direct {v0, p1}, Landroid/webkit/LGTextDragShadowBuilder;-><init>(Landroid/view/View;)V

    #@5
    .line 34
    .local v0, builder:Landroid/webkit/LGTextDragShadowBuilder;
    return-object v0
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 70
    invoke-super {p0, p1}, Landroid/view/View$DragShadowBuilder;->onDrawShadow(Landroid/graphics/Canvas;)V

    #@3
    .line 71
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .registers 3
    .parameter "shadowSize"
    .parameter "shadowTouchPoint"

    #@0
    .prologue
    .line 75
    invoke-super {p0, p1, p2}, Landroid/view/View$DragShadowBuilder;->onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V

    #@3
    .line 76
    return-void
.end method
