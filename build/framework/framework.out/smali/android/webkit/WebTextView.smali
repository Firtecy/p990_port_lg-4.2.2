.class abstract Landroid/webkit/WebTextView;
.super Ljava/lang/Object;
.source "WebTextView.java"


# static fields
.field static final EMAIL:I = 0x4

.field static final FORM_NOT_AUTOFILLABLE:I = -0x1

.field private static final LOGTAG:Ljava/lang/String; = "WebTextView"

.field static final NORMAL_TEXT_FIELD:I = 0x0

.field static final NUMBER:I = 0x5

.field static final PASSWORD:I = 0x2

.field static final SEARCH:I = 0x3

.field static final TELEPHONE:I = 0x6

.field static final TEXT_AREA:I = 0x1

.field static final URL:I = 0x7


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 25
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static urlForAutoCompleteData(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "urlString"

    #@0
    .prologue
    .line 43
    const/4 v1, 0x0

    #@1
    .line 45
    .local v1, url:Ljava/net/URL;
    :try_start_1
    new-instance v2, Ljava/net/URL;

    #@3
    invoke-direct {v2, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_6} :catch_31

    #@6
    .end local v1           #url:Ljava/net/URL;
    .local v2, url:Ljava/net/URL;
    move-object v1, v2

    #@7
    .line 50
    .end local v2           #url:Ljava/net/URL;
    .restart local v1       #url:Ljava/net/URL;
    :goto_7
    if-eqz v1, :cond_4b

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, "://"

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v1}, Ljava/net/URL;->getPath()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    :goto_30
    return-object v3

    #@31
    .line 46
    :catch_31
    move-exception v0

    #@32
    .line 47
    .local v0, e:Ljava/net/MalformedURLException;
    const-string v3, "WebTextView"

    #@34
    new-instance v4, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v5, "Unable to parse URL "

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_7

    #@4b
    .line 50
    .end local v0           #e:Ljava/net/MalformedURLException;
    :cond_4b
    const/4 v3, 0x0

    #@4c
    goto :goto_30
.end method
