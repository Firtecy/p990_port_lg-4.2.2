.class public Landroid/webkit/MimeTypeMap;
.super Ljava/lang/Object;
.source "MimeTypeMap.java"


# static fields
.field private static final sMimeTypeMap:Landroid/webkit/MimeTypeMap;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 31
    new-instance v0, Landroid/webkit/MimeTypeMap;

    #@2
    invoke-direct {v0}, Landroid/webkit/MimeTypeMap;-><init>()V

    #@5
    sput-object v0, Landroid/webkit/MimeTypeMap;->sMimeTypeMap:Landroid/webkit/MimeTypeMap;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    return-void
.end method

.method public static getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "url"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 44
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v5

    #@5
    if-nez v5, :cond_4c

    #@7
    .line 45
    const/16 v5, 0x23

    #@9
    invoke-virtual {p0, v5}, Ljava/lang/String;->lastIndexOf(I)I

    #@c
    move-result v3

    #@d
    .line 46
    .local v3, fragment:I
    if-lez v3, :cond_13

    #@f
    .line 47
    invoke-virtual {p0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@12
    move-result-object p0

    #@13
    .line 50
    :cond_13
    const/16 v5, 0x3f

    #@15
    invoke-virtual {p0, v5}, Ljava/lang/String;->lastIndexOf(I)I

    #@18
    move-result v4

    #@19
    .line 51
    .local v4, query:I
    if-lez v4, :cond_1f

    #@1b
    .line 52
    invoke-virtual {p0, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1e
    move-result-object p0

    #@1f
    .line 55
    :cond_1f
    const/16 v5, 0x2f

    #@21
    invoke-virtual {p0, v5}, Ljava/lang/String;->lastIndexOf(I)I

    #@24
    move-result v2

    #@25
    .line 56
    .local v2, filenamePos:I
    if-ltz v2, :cond_4a

    #@27
    add-int/lit8 v5, v2, 0x1

    #@29
    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    .line 61
    .local v1, filename:Ljava/lang/String;
    :goto_2d
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    #@30
    move-result v5

    #@31
    if-nez v5, :cond_4c

    #@33
    const-string v5, "[a-zA-Z_0-9\\.\\-\\(\\)\\%]+"

    #@35
    invoke-static {v5, v1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    #@38
    move-result v5

    #@39
    if-eqz v5, :cond_4c

    #@3b
    .line 63
    const/16 v5, 0x2e

    #@3d
    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    #@40
    move-result v0

    #@41
    .line 64
    .local v0, dotPos:I
    if-ltz v0, :cond_4c

    #@43
    .line 65
    add-int/lit8 v5, v0, 0x1

    #@45
    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    .line 70
    .end local v0           #dotPos:I
    .end local v1           #filename:Ljava/lang/String;
    .end local v2           #filenamePos:I
    .end local v3           #fragment:I
    .end local v4           #query:I
    :goto_49
    return-object v5

    #@4a
    .restart local v2       #filenamePos:I
    .restart local v3       #fragment:I
    .restart local v4       #query:I
    :cond_4a
    move-object v1, p0

    #@4b
    .line 56
    goto :goto_2d

    #@4c
    .line 70
    .end local v2           #filenamePos:I
    .end local v3           #fragment:I
    .end local v4           #query:I
    :cond_4c
    const-string v5, ""

    #@4e
    goto :goto_49
.end method

.method public static getSingleton()Landroid/webkit/MimeTypeMap;
    .registers 1

    #@0
    .prologue
    .line 165
    sget-object v0, Landroid/webkit/MimeTypeMap;->sMimeTypeMap:Landroid/webkit/MimeTypeMap;

    #@2
    return-object v0
.end method

.method private static mimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "extension"

    #@0
    .prologue
    .line 93
    invoke-static {p0}, Llibcore/net/MimeUtils;->guessMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method


# virtual methods
.method public getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "mimeType"

    #@0
    .prologue
    .line 113
    invoke-static {p1}, Llibcore/net/MimeUtils;->guessExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "extension"

    #@0
    .prologue
    .line 88
    invoke-static {p1}, Llibcore/net/MimeUtils;->guessMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public hasExtension(Ljava/lang/String;)Z
    .registers 3
    .parameter "extension"

    #@0
    .prologue
    .line 102
    invoke-static {p1}, Llibcore/net/MimeUtils;->hasExtension(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public hasMimeType(Ljava/lang/String;)Z
    .registers 3
    .parameter "mimeType"

    #@0
    .prologue
    .line 79
    invoke-static {p1}, Llibcore/net/MimeUtils;->hasMimeType(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method remapGenericMimeType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "mimeType"
    .parameter "url"
    .parameter "contentDisposition"

    #@0
    .prologue
    .line 130
    const-string/jumbo v3, "text/plain"

    #@3
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v3

    #@7
    if-nez v3, :cond_11

    #@9
    const-string v3, "application/octet-stream"

    #@b
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_27

    #@11
    .line 135
    :cond_11
    const/4 v1, 0x0

    #@12
    .line 136
    .local v1, filename:Ljava/lang/String;
    if-eqz p3, :cond_18

    #@14
    .line 137
    invoke-static {p3}, Landroid/webkit/URLUtil;->parseContentDisposition(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    .line 139
    :cond_18
    if-eqz v1, :cond_1b

    #@1a
    .line 140
    move-object p2, v1

    #@1b
    .line 142
    :cond_1b
    invoke-static {p2}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 143
    .local v0, extension:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 144
    .local v2, newMimeType:Ljava/lang/String;
    if-eqz v2, :cond_26

    #@25
    .line 145
    move-object p1, v2

    #@26
    .line 157
    .end local v0           #extension:Ljava/lang/String;
    .end local v1           #filename:Ljava/lang/String;
    .end local v2           #newMimeType:Ljava/lang/String;
    :cond_26
    :goto_26
    return-object p1

    #@27
    .line 147
    :cond_27
    const-string/jumbo v3, "text/vnd.wap.wml"

    #@2a
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_34

    #@30
    .line 149
    const-string/jumbo p1, "text/plain"

    #@33
    goto :goto_26

    #@34
    .line 153
    :cond_34
    const-string v3, "application/vnd.wap.xhtml+xml"

    #@36
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v3

    #@3a
    if-eqz v3, :cond_26

    #@3c
    .line 154
    const-string p1, "application/xhtml+xml"

    #@3e
    goto :goto_26
.end method
