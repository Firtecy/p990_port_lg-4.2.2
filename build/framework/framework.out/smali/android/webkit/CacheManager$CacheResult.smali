.class public Landroid/webkit/CacheManager$CacheResult;
.super Ljava/lang/Object;
.source "CacheManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/CacheManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CacheResult"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field contentLength:J

.field contentdisposition:Ljava/lang/String;

.field crossDomain:Ljava/lang/String;

.field encoding:Ljava/lang/String;

.field etag:Ljava/lang/String;

.field expires:J

.field expiresString:Ljava/lang/String;

.field httpStatusCode:I

.field inStream:Ljava/io/InputStream;

.field lastModified:Ljava/lang/String;

.field localPath:Ljava/lang/String;

.field location:Ljava/lang/String;

.field mimeType:Ljava/lang/String;

.field outFile:Ljava/io/File;

.field outStream:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getContentDisposition()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 188
    iget-object v0, p0, Landroid/webkit/CacheManager$CacheResult;->contentdisposition:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getContentLength()J
    .registers 3

    #@0
    .prologue
    .line 99
    iget-wide v0, p0, Landroid/webkit/CacheManager$CacheResult;->contentLength:J

    #@2
    return-wide v0
.end method

.method public getETag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 149
    iget-object v0, p0, Landroid/webkit/CacheManager$CacheResult;->etag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getEncoding()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 177
    iget-object v0, p0, Landroid/webkit/CacheManager$CacheResult;->encoding:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getExpires()J
    .registers 3

    #@0
    .prologue
    .line 120
    iget-wide v0, p0, Landroid/webkit/CacheManager$CacheResult;->expires:J

    #@2
    return-wide v0
.end method

.method public getExpiresString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 130
    iget-object v0, p0, Landroid/webkit/CacheManager$CacheResult;->expiresString:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getHttpStatusCode()I
    .registers 2

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/webkit/CacheManager$CacheResult;->httpStatusCode:I

    #@2
    return v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .registers 2

    #@0
    .prologue
    .line 199
    iget-object v0, p0, Landroid/webkit/CacheManager$CacheResult;->inStream:Ljava/io/InputStream;

    #@2
    return-object v0
.end method

.method public getLastModified()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Landroid/webkit/CacheManager$CacheResult;->lastModified:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLocalPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Landroid/webkit/CacheManager$CacheResult;->localPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Landroid/webkit/CacheManager$CacheResult;->location:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Landroid/webkit/CacheManager$CacheResult;->mimeType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .registers 2

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Landroid/webkit/CacheManager$CacheResult;->outStream:Ljava/io/OutputStream;

    #@2
    return-object v0
.end method

.method public setContentLength(J)V
    .registers 3
    .parameter "contentLength"

    #@0
    .prologue
    .line 237
    iput-wide p1, p0, Landroid/webkit/CacheManager$CacheResult;->contentLength:J

    #@2
    .line 238
    return-void
.end method

.method public setEncoding(Ljava/lang/String;)V
    .registers 2
    .parameter "encoding"

    #@0
    .prologue
    .line 230
    iput-object p1, p0, Landroid/webkit/CacheManager$CacheResult;->encoding:Ljava/lang/String;

    #@2
    .line 231
    return-void
.end method

.method public setInputStream(Ljava/io/InputStream;)V
    .registers 2
    .parameter "stream"

    #@0
    .prologue
    .line 221
    iput-object p1, p0, Landroid/webkit/CacheManager$CacheResult;->inStream:Ljava/io/InputStream;

    #@2
    .line 222
    return-void
.end method
