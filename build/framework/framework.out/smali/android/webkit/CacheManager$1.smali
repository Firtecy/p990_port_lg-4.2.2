.class final Landroid/webkit/CacheManager$1;
.super Ljava/lang/Object;
.source "CacheManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/CacheManager;->removeAllCacheFiles()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 435
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    .line 439
    :try_start_0
    invoke-static {}, Landroid/webkit/CacheManager;->access$000()Ljava/io/File;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 441
    .local v1, files:[Ljava/lang/String;
    if-eqz v1, :cond_3f

    #@a
    .line 442
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    array-length v3, v1

    #@c
    if-ge v2, v3, :cond_3f

    #@e
    .line 443
    new-instance v0, Ljava/io/File;

    #@10
    invoke-static {}, Landroid/webkit/CacheManager;->access$000()Ljava/io/File;

    #@13
    move-result-object v3

    #@14
    aget-object v4, v1, v2

    #@16
    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@19
    .line 444
    .local v0, f:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_3b

    #@1f
    .line 445
    const-string v3, "cache"

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    const-string v5, " delete failed."

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3b
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_3b} :catch_3e

    #@3b
    .line 442
    :cond_3b
    add-int/lit8 v2, v2, 0x1

    #@3d
    goto :goto_b

    #@3e
    .line 449
    .end local v0           #f:Ljava/io/File;
    .end local v1           #files:[Ljava/lang/String;
    .end local v2           #i:I
    :catch_3e
    move-exception v3

    #@3f
    .line 452
    :cond_3f
    return-void
.end method
