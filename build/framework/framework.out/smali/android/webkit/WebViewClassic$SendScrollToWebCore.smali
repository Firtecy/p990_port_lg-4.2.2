.class final Landroid/webkit/WebViewClassic$SendScrollToWebCore;
.super Ljava/lang/Object;
.source "WebViewClassic.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SendScrollToWebCore"
.end annotation


# instance fields
.field private mPostpone:Z

.field final synthetic this$0:Landroid/webkit/WebViewClassic;


# direct methods
.method private constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 6720
    iput-object p1, p0, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 6726
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->mPostpone:Z

    #@8
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewClassic$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 6720
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic$SendScrollToWebCore;-><init>(Landroid/webkit/WebViewClassic;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 2

    #@0
    .prologue
    .line 6722
    iget-object v0, p0, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->access$2700(Landroid/webkit/WebViewClassic;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_d

    #@8
    .line 6723
    iget-object v0, p0, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->this$0:Landroid/webkit/WebViewClassic;

    #@a
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->sendOurVisibleRect()Landroid/graphics/Rect;

    #@d
    .line 6725
    :cond_d
    return-void
.end method

.method public send(Z)V
    .registers 3
    .parameter "force"

    #@0
    .prologue
    .line 6729
    iget-object v0, p0, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@4
    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 6730
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->mPostpone:Z

    #@9
    if-eqz v0, :cond_d

    #@b
    if-eqz p1, :cond_11

    #@d
    .line 6731
    :cond_d
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->run()V

    #@10
    .line 6735
    :goto_10
    return-void

    #@11
    .line 6733
    :cond_11
    iget-object v0, p0, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->this$0:Landroid/webkit/WebViewClassic;

    #@13
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@15
    invoke-virtual {v0, p0}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    #@18
    goto :goto_10
.end method

.method public setPostpone(Z)V
    .registers 2
    .parameter "set"

    #@0
    .prologue
    .line 6727
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic$SendScrollToWebCore;->mPostpone:Z

    #@2
    return-void
.end method
