.class Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;
.super Ljava/lang/Object;
.source "WebCoreThreadWatchdog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebCoreThreadWatchdog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PageNotRespondingRunnable"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field mActiveWebView:Landroid/webkit/WebView;

.field mContext:Landroid/content/Context;

.field private mWatchdogHandler:Landroid/os/Handler;

.field final synthetic this$0:Landroid/webkit/WebCoreThreadWatchdog;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 234
    const-class v0, Landroid/webkit/WebCoreThreadWatchdog;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>(Landroid/webkit/WebCoreThreadWatchdog;Landroid/content/Context;Landroid/os/Handler;)V
    .registers 4
    .parameter
    .parameter "context"
    .parameter "watchdogHandler"

    #@0
    .prologue
    .line 241
    iput-object p1, p0, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->this$0:Landroid/webkit/WebCoreThreadWatchdog;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 242
    iput-object p2, p0, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->mContext:Landroid/content/Context;

    #@7
    .line 243
    iput-object p3, p0, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->mWatchdogHandler:Landroid/os/Handler;

    #@9
    .line 244
    return-void
.end method

.method public constructor <init>(Landroid/webkit/WebCoreThreadWatchdog;Landroid/webkit/WebView;Landroid/os/Handler;)V
    .registers 5
    .parameter
    .parameter "webView"
    .parameter "watchdogHandler"

    #@0
    .prologue
    .line 248
    invoke-virtual {p2}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, p1, v0, p3}, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;-><init>(Landroid/webkit/WebCoreThreadWatchdog;Landroid/content/Context;Landroid/os/Handler;)V

    #@7
    .line 249
    iput-object p2, p0, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->mActiveWebView:Landroid/webkit/WebView;

    #@9
    .line 250
    return-void
.end method

.method static synthetic access$400(Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 234
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->mWatchdogHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    .line 256
    sget-boolean v4, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->$assertionsDisabled:Z

    #@2
    if-nez v4, :cond_18

    #@4
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@7
    move-result-object v4

    #@8
    invoke-virtual {v4}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    #@b
    move-result-object v4

    #@c
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@f
    move-result-object v5

    #@10
    if-eq v4, v5, :cond_18

    #@12
    new-instance v4, Ljava/lang/AssertionError;

    #@14
    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    #@17
    throw v4

    #@18
    .line 260
    :cond_18
    iget-object v4, p0, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->mActiveWebView:Landroid/webkit/WebView;

    #@1a
    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    #@1d
    move-result-object v3

    #@1e
    .line 261
    .local v3, settings:Landroid/webkit/WebSettings;
    if-eqz v3, :cond_8d

    #@20
    invoke-virtual {v3}, Landroid/webkit/WebSettings;->getFloatingMode()Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_8d

    #@26
    .line 262
    new-instance v2, Landroid/util/TypedValue;

    #@28
    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    #@2b
    .line 263
    .local v2, outValue:Landroid/util/TypedValue;
    iget-object v4, p0, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->mContext:Landroid/content/Context;

    #@2d
    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@30
    move-result-object v4

    #@31
    const v5, 0x1010309

    #@34
    const/4 v6, 0x1

    #@35
    invoke-virtual {v4, v5, v2, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@38
    .line 265
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@3a
    iget-object v4, p0, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->mContext:Landroid/content/Context;

    #@3c
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@3f
    move-result-object v4

    #@40
    iget v5, v2, Landroid/util/TypedValue;->resourceId:I

    #@42
    invoke-direct {v0, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@45
    .line 270
    .end local v2           #outValue:Landroid/util/TypedValue;
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    :goto_45
    const v4, 0x1040406

    #@48
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@4b
    move-result-object v4

    #@4c
    const v5, 0x1040403

    #@4f
    new-instance v6, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable$3;

    #@51
    invoke-direct {v6, p0}, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable$3;-><init>(Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;)V

    #@54
    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@57
    move-result-object v4

    #@58
    const v5, 0x1040405

    #@5b
    new-instance v6, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable$2;

    #@5d
    invoke-direct {v6, p0}, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable$2;-><init>(Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;)V

    #@60
    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@63
    move-result-object v4

    #@64
    new-instance v5, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable$1;

    #@66
    invoke-direct {v5, p0}, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable$1;-><init>(Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;)V

    #@69
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@6c
    move-result-object v4

    #@6d
    const v5, 0x1080027

    #@70
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@77
    move-result-object v1

    #@78
    .line 305
    .local v1, dlg:Landroid/app/AlertDialog;
    if-eqz v3, :cond_89

    #@7a
    invoke-virtual {v3}, Landroid/webkit/WebSettings;->getFloatingMode()Z

    #@7d
    move-result v4

    #@7e
    if-eqz v4, :cond_89

    #@80
    .line 306
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@83
    move-result-object v4

    #@84
    const/16 v5, 0x7d2

    #@86
    invoke-virtual {v4, v5}, Landroid/view/Window;->setType(I)V

    #@89
    .line 307
    :cond_89
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@8c
    .line 309
    return-void

    #@8d
    .line 268
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    .end local v1           #dlg:Landroid/app/AlertDialog;
    :cond_8d
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@8f
    iget-object v4, p0, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;->mContext:Landroid/content/Context;

    #@91
    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@94
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    goto :goto_45
.end method
