.class public Landroid/webkit/WebIconDatabase;
.super Ljava/lang/Object;
.source "WebIconDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebIconDatabase$IconListener;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 112
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Landroid/webkit/WebIconDatabase;
    .registers 1

    #@0
    .prologue
    .line 106
    invoke-static {}, Landroid/webkit/WebViewFactory;->getProvider()Landroid/webkit/WebViewFactoryProvider;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Landroid/webkit/WebViewFactoryProvider;->getWebIconDatabase()Landroid/webkit/WebIconDatabase;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method


# virtual methods
.method public bulkRequestIconForPageUrl(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V
    .registers 5
    .parameter "cr"
    .parameter "where"
    .parameter "listener"

    #@0
    .prologue
    .line 79
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public close()V
    .registers 2

    #@0
    .prologue
    .line 55
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public open(Ljava/lang/String;)V
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 48
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public releaseIconForPageUrl(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 95
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public removeAllIcons()V
    .registers 2

    #@0
    .prologue
    .line 62
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public requestIconForPageUrl(Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V
    .registers 4
    .parameter "url"
    .parameter "listener"

    #@0
    .prologue
    .line 72
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public retainIconForPageUrl(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 87
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method
