.class Landroid/webkit/WebViewClassic$Factory;
.super Ljava/lang/Object;
.source "WebViewClassic.java"

# interfaces
.implements Landroid/webkit/WebViewFactoryProvider;
.implements Landroid/webkit/WebViewFactoryProvider$Statics;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Factory"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1652
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createWebView(Landroid/webkit/WebView;Landroid/webkit/WebView$PrivateAccess;)Landroid/webkit/WebViewProvider;
    .registers 4
    .parameter "webView"
    .parameter "privateAccess"

    #@0
    .prologue
    .line 1678
    new-instance v0, Landroid/webkit/WebViewClassic;

    #@2
    invoke-direct {v0, p1, p2}, Landroid/webkit/WebViewClassic;-><init>(Landroid/webkit/WebView;Landroid/webkit/WebView$PrivateAccess;)V

    #@5
    return-object v0
.end method

.method public findAddress(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "addr"

    #@0
    .prologue
    .line 1655
    invoke-static {p1}, Landroid/webkit/WebViewClassic;->findAddress(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getCookieManager()Landroid/webkit/CookieManager;
    .registers 2

    #@0
    .prologue
    .line 1688
    invoke-static {}, Landroid/webkit/CookieManagerClassic;->getInstance()Landroid/webkit/CookieManagerClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 1708
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v0

    #@4
    invoke-static {p1, v0}, Landroid/webkit/WebSettingsClassic;->getDefaultUserAgentForLocale(Landroid/content/Context;Ljava/util/Locale;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getGeolocationPermissions()Landroid/webkit/GeolocationPermissions;
    .registers 2

    #@0
    .prologue
    .line 1683
    invoke-static {}, Landroid/webkit/GeolocationPermissionsClassic;->getInstance()Landroid/webkit/GeolocationPermissionsClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getStatics()Landroid/webkit/WebViewFactoryProvider$Statics;
    .registers 1

    #@0
    .prologue
    .line 1674
    return-object p0
.end method

.method public getWebIconDatabase()Landroid/webkit/WebIconDatabase;
    .registers 2

    #@0
    .prologue
    .line 1693
    invoke-static {}, Landroid/webkit/WebIconDatabaseClassic;->getInstance()Landroid/webkit/WebIconDatabaseClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getWebStorage()Landroid/webkit/WebStorage;
    .registers 2

    #@0
    .prologue
    .line 1698
    invoke-static {}, Landroid/webkit/WebStorageClassic;->getInstance()Landroid/webkit/WebStorageClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getWebViewDatabase(Landroid/content/Context;)Landroid/webkit/WebViewDatabase;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 1703
    invoke-static {p1}, Landroid/webkit/WebViewDatabaseClassic;->getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabaseClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public setPlatformNotificationsEnabled(Z)V
    .registers 2
    .parameter "enable"

    #@0
    .prologue
    .line 1659
    if-eqz p1, :cond_6

    #@2
    .line 1660
    invoke-static {}, Landroid/webkit/WebViewClassic;->enablePlatformNotifications()V

    #@5
    .line 1664
    :goto_5
    return-void

    #@6
    .line 1662
    :cond_6
    invoke-static {}, Landroid/webkit/WebViewClassic;->disablePlatformNotifications()V

    #@9
    goto :goto_5
.end method

.method public setWebViewProxyEnable(Z)V
    .registers 2
    .parameter "enable"

    #@0
    .prologue
    .line 1669
    invoke-static {p1}, Landroid/webkit/WebViewClassic;->setWebViewProxyEnable(Z)V

    #@3
    .line 1670
    return-void
.end method
