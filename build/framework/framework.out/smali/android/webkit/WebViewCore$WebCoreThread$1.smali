.class Landroid/webkit/WebViewCore$WebCoreThread$1;
.super Landroid/os/Handler;
.source "WebViewCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/WebViewCore$WebCoreThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/WebViewCore$WebCoreThread;


# direct methods
.method constructor <init>(Landroid/webkit/WebViewCore$WebCoreThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 796
    iput-object p1, p0, Landroid/webkit/WebViewCore$WebCoreThread$1;->this$0:Landroid/webkit/WebViewCore$WebCoreThread;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 799
    iget v2, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v2, :sswitch_data_6a

    #@5
    .line 853
    :goto_5
    return-void

    #@6
    .line 801
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v0, Landroid/webkit/WebViewCore;

    #@a
    .line 802
    .local v0, core:Landroid/webkit/WebViewCore;
    invoke-static {v0}, Landroid/webkit/WebViewCore;->access$600(Landroid/webkit/WebViewCore;)V

    #@d
    goto :goto_5

    #@e
    .line 807
    .end local v0           #core:Landroid/webkit/WebViewCore;
    :sswitch_e
    const/16 v2, 0xa

    #@10
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@13
    goto :goto_5

    #@14
    .line 813
    :sswitch_14
    const/4 v2, 0x0

    #@15
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@18
    goto :goto_5

    #@19
    .line 818
    :sswitch_19
    sget-object v2, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@1b
    if-nez v2, :cond_25

    #@1d
    .line 819
    new-instance v2, Ljava/lang/IllegalStateException;

    #@1f
    const-string v3, "No WebView has been created in this process!"

    #@21
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@24
    throw v2

    #@25
    .line 822
    :cond_25
    sget-object v3, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@27
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@29
    check-cast v2, Ljava/lang/String;

    #@2b
    invoke-virtual {v3, v2}, Landroid/webkit/JWebCoreJavaBridge;->addPackageName(Ljava/lang/String;)V

    #@2e
    goto :goto_5

    #@2f
    .line 826
    :sswitch_2f
    sget-object v2, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@31
    if-nez v2, :cond_3b

    #@33
    .line 827
    new-instance v2, Ljava/lang/IllegalStateException;

    #@35
    const-string v3, "No WebView has been created in this process!"

    #@37
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v2

    #@3b
    .line 830
    :cond_3b
    sget-object v3, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@3d
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3f
    check-cast v2, Ljava/lang/String;

    #@41
    invoke-virtual {v3, v2}, Landroid/webkit/JWebCoreJavaBridge;->removePackageName(Ljava/lang/String;)V

    #@44
    goto :goto_5

    #@45
    .line 834
    :sswitch_45
    sget-object v2, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@47
    if-nez v2, :cond_51

    #@49
    .line 835
    new-instance v2, Ljava/lang/IllegalStateException;

    #@4b
    const-string v3, "No WebView has been created in this process!"

    #@4d
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@50
    throw v2

    #@51
    .line 838
    :cond_51
    sget-object v3, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@53
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@55
    check-cast v2, Landroid/net/ProxyProperties;

    #@57
    invoke-virtual {v3, v2}, Landroid/webkit/JWebCoreJavaBridge;->updateProxy(Landroid/net/ProxyProperties;)V

    #@5a
    goto :goto_5

    #@5b
    .line 844
    :sswitch_5b
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5d
    check-cast v1, Landroid/os/Message;

    #@5f
    .line 845
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@62
    goto :goto_5

    #@63
    .line 849
    .end local v1           #m:Landroid/os/Message;
    :sswitch_63
    invoke-static {}, Landroid/webkit/WebViewCore;->access$700()V

    #@66
    .line 850
    invoke-static {}, Landroid/net/http/CertificateChainValidator;->handleTrustStorageUpdate()V

    #@69
    goto :goto_5

    #@6a
    .line 799
    :sswitch_data_6a
    .sparse-switch
        0x0 -> :sswitch_6
        0x1 -> :sswitch_e
        0x2 -> :sswitch_14
        0xb9 -> :sswitch_19
        0xba -> :sswitch_2f
        0xc1 -> :sswitch_45
        0xc5 -> :sswitch_5b
        0xdc -> :sswitch_63
    .end sparse-switch
.end method
