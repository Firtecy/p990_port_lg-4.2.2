.class Landroid/webkit/WebViewFactory;
.super Ljava/lang/Object;
.source "WebViewFactory.java"


# static fields
.field private static final CHROMIUM_WEBVIEW_FACTORY:Ljava/lang/String; = "com.android.webviewchromium.WebViewChromiumFactoryProvider"

.field private static final CHROMIUM_WEBVIEW_JAR:Ljava/lang/String; = "/system/framework/webviewchromium.jar"

.field private static final DEBUG:Z = false

.field private static final DEFAULT_WEBVIEW_FACTORY:Ljava/lang/String; = "android.webkit.WebViewClassic$Factory"

.field private static final LOGTAG:Ljava/lang/String; = "WebViewFactory"

.field private static sProviderInstance:Landroid/webkit/WebViewFactoryProvider;

.field private static final sProviderLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 44
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/webkit/WebViewFactory;->sProviderLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static getFactoryByName(Ljava/lang/String;Ljava/lang/ClassLoader;)Landroid/webkit/WebViewFactoryProvider;
    .registers 7
    .parameter "providerName"
    .parameter "loader"

    #@0
    .prologue
    .line 91
    const/4 v2, 0x1

    #@1
    :try_start_1
    invoke-static {p0, v2, p1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@4
    move-result-object v0

    #@5
    .line 93
    .local v0, c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@8
    move-result-object v2

    #@9
    check-cast v2, Landroid/webkit/WebViewFactoryProvider;
    :try_end_b
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_b} :catch_c
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_b} :catch_27
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_b} :catch_41

    #@b
    .line 101
    .end local v0           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :goto_b
    return-object v2

    #@c
    .line 94
    :catch_c
    move-exception v1

    #@d
    .line 95
    .local v1, e:Ljava/lang/ClassNotFoundException;
    const-string v2, "WebViewFactory"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "error loading "

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25
    .line 101
    .end local v1           #e:Ljava/lang/ClassNotFoundException;
    :goto_25
    const/4 v2, 0x0

    #@26
    goto :goto_b

    #@27
    .line 96
    :catch_27
    move-exception v1

    #@28
    .line 97
    .local v1, e:Ljava/lang/IllegalAccessException;
    const-string v2, "WebViewFactory"

    #@2a
    new-instance v3, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v4, "error loading "

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@40
    goto :goto_25

    #@41
    .line 98
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_41
    move-exception v1

    #@42
    .line 99
    .local v1, e:Ljava/lang/InstantiationException;
    const-string v2, "WebViewFactory"

    #@44
    new-instance v3, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v4, "error loading "

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5a
    goto :goto_25
.end method

.method static getProvider()Landroid/webkit/WebViewFactoryProvider;
    .registers 4

    #@0
    .prologue
    .line 47
    sget-object v2, Landroid/webkit/WebViewFactory;->sProviderLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 50
    :try_start_3
    sget-object v1, Landroid/webkit/WebViewFactory;->sProviderInstance:Landroid/webkit/WebViewFactoryProvider;

    #@5
    if-eqz v1, :cond_b

    #@7
    sget-object v1, Landroid/webkit/WebViewFactory;->sProviderInstance:Landroid/webkit/WebViewFactoryProvider;

    #@9
    monitor-exit v2

    #@a
    .line 75
    .local v0, oldPolicy:Landroid/os/StrictMode$ThreadPolicy;
    :goto_a
    return-object v1

    #@b
    .line 55
    .end local v0           #oldPolicy:Landroid/os/StrictMode$ThreadPolicy;
    :cond_b
    sget-boolean v1, Landroid/os/Build;->IS_DEBUGGABLE:Z

    #@d
    if-eqz v1, :cond_26

    #@f
    const-string/jumbo v1, "webview.use_chromium"

    #@12
    const/4 v3, 0x0

    #@13
    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_26

    #@19
    .line 56
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_47

    #@1c
    move-result-object v0

    #@1d
    .line 58
    .restart local v0       #oldPolicy:Landroid/os/StrictMode$ThreadPolicy;
    :try_start_1d
    invoke-static {}, Landroid/webkit/WebViewFactory;->loadChromiumProvider()Landroid/webkit/WebViewFactoryProvider;

    #@20
    move-result-object v1

    #@21
    sput-object v1, Landroid/webkit/WebViewFactory;->sProviderInstance:Landroid/webkit/WebViewFactoryProvider;
    :try_end_23
    .catchall {:try_start_1d .. :try_end_23} :catchall_4a

    #@23
    .line 61
    :try_start_23
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@26
    .line 65
    :cond_26
    sget-object v1, Landroid/webkit/WebViewFactory;->sProviderInstance:Landroid/webkit/WebViewFactoryProvider;

    #@28
    if-nez v1, :cond_43

    #@2a
    .line 68
    const-string v1, "android.webkit.WebViewClassic$Factory"

    #@2c
    const-class v3, Landroid/webkit/WebViewFactory;

    #@2e
    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@31
    move-result-object v3

    #@32
    invoke-static {v1, v3}, Landroid/webkit/WebViewFactory;->getFactoryByName(Ljava/lang/String;Ljava/lang/ClassLoader;)Landroid/webkit/WebViewFactoryProvider;

    #@35
    move-result-object v1

    #@36
    sput-object v1, Landroid/webkit/WebViewFactory;->sProviderInstance:Landroid/webkit/WebViewFactoryProvider;

    #@38
    .line 70
    sget-object v1, Landroid/webkit/WebViewFactory;->sProviderInstance:Landroid/webkit/WebViewFactoryProvider;

    #@3a
    if-nez v1, :cond_43

    #@3c
    .line 72
    new-instance v1, Landroid/webkit/WebViewClassic$Factory;

    #@3e
    invoke-direct {v1}, Landroid/webkit/WebViewClassic$Factory;-><init>()V

    #@41
    sput-object v1, Landroid/webkit/WebViewFactory;->sProviderInstance:Landroid/webkit/WebViewFactoryProvider;

    #@43
    .line 75
    :cond_43
    sget-object v1, Landroid/webkit/WebViewFactory;->sProviderInstance:Landroid/webkit/WebViewFactoryProvider;

    #@45
    monitor-exit v2

    #@46
    goto :goto_a

    #@47
    .line 76
    :catchall_47
    move-exception v1

    #@48
    monitor-exit v2
    :try_end_49
    .catchall {:try_start_23 .. :try_end_49} :catchall_47

    #@49
    throw v1

    #@4a
    .line 61
    :catchall_4a
    move-exception v1

    #@4b
    :try_start_4b
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@4e
    throw v1
    :try_end_4f
    .catchall {:try_start_4b .. :try_end_4f} :catchall_47
.end method

.method private static loadChromiumProvider()Landroid/webkit/WebViewFactoryProvider;
    .registers 4

    #@0
    .prologue
    .line 82
    new-instance v0, Ldalvik/system/PathClassLoader;

    #@2
    const-string v1, "/system/framework/webviewchromium.jar"

    #@4
    const/4 v2, 0x0

    #@5
    const-class v3, Landroid/webkit/WebViewFactory;

    #@7
    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@a
    move-result-object v3

    #@b
    invoke-direct {v0, v1, v2, v3}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@e
    .line 84
    .local v0, clazzLoader:Ljava/lang/ClassLoader;
    const-string v1, "com.android.webviewchromium.WebViewChromiumFactoryProvider"

    #@10
    invoke-static {v1, v0}, Landroid/webkit/WebViewFactory;->getFactoryByName(Ljava/lang/String;Ljava/lang/ClassLoader;)Landroid/webkit/WebViewFactoryProvider;

    #@13
    move-result-object v1

    #@14
    return-object v1
.end method
