.class Landroid/webkit/WebSocket$1;
.super Landroid/os/Handler;
.source "WebSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/WebSocket;->createWebCoreHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/WebSocket;


# direct methods
.method constructor <init>(Landroid/webkit/WebSocket;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 696
    iput-object p1, p0, Landroid/webkit/WebSocket$1;->this$0:Landroid/webkit/WebSocket;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 699
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v1, :pswitch_data_44

    #@5
    .line 724
    :cond_5
    :goto_5
    return-void

    #@6
    .line 701
    :pswitch_6
    iget-object v1, p0, Landroid/webkit/WebSocket$1;->this$0:Landroid/webkit/WebSocket;

    #@8
    iget-object v2, p0, Landroid/webkit/WebSocket$1;->this$0:Landroid/webkit/WebSocket;

    #@a
    iget v2, v2, Landroid/webkit/WebSocket;->mNativePointer:I

    #@c
    invoke-static {v1, v2}, Landroid/webkit/WebSocket;->access$100(Landroid/webkit/WebSocket;I)V

    #@f
    goto :goto_5

    #@10
    .line 705
    :pswitch_10
    iget-object v1, p0, Landroid/webkit/WebSocket$1;->this$0:Landroid/webkit/WebSocket;

    #@12
    iget-object v2, p0, Landroid/webkit/WebSocket$1;->this$0:Landroid/webkit/WebSocket;

    #@14
    iget v2, v2, Landroid/webkit/WebSocket;->mNativePointer:I

    #@16
    invoke-static {v1, v2}, Landroid/webkit/WebSocket;->access$200(Landroid/webkit/WebSocket;I)V

    #@19
    goto :goto_5

    #@1a
    .line 709
    :pswitch_1a
    const/4 v0, 0x0

    #@1b
    .line 711
    .local v0, msgData:Ljava/nio/ByteBuffer;
    :cond_1b
    iget-object v1, p0, Landroid/webkit/WebSocket$1;->this$0:Landroid/webkit/WebSocket;

    #@1d
    iget-object v1, v1, Landroid/webkit/WebSocket;->mWebSocketImpl:Landroid/webkit/WebSocket$WebSocketImpl;

    #@1f
    invoke-virtual {v1}, Landroid/webkit/WebSocket$WebSocketImpl;->getData()Ljava/nio/ByteBuffer;

    #@22
    move-result-object v0

    #@23
    .line 712
    if-eqz v0, :cond_5

    #@25
    .line 715
    iget-object v1, p0, Landroid/webkit/WebSocket$1;->this$0:Landroid/webkit/WebSocket;

    #@27
    iget-object v2, p0, Landroid/webkit/WebSocket$1;->this$0:Landroid/webkit/WebSocket;

    #@29
    iget v2, v2, Landroid/webkit/WebSocket;->mNativePointer:I

    #@2b
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    #@32
    move-result v4

    #@33
    invoke-static {v1, v2, v3, v4}, Landroid/webkit/WebSocket;->access$300(Landroid/webkit/WebSocket;I[BI)V

    #@36
    .line 716
    if-nez p1, :cond_1b

    #@38
    goto :goto_5

    #@39
    .line 720
    .end local v0           #msgData:Ljava/nio/ByteBuffer;
    :pswitch_39
    iget-object v1, p0, Landroid/webkit/WebSocket$1;->this$0:Landroid/webkit/WebSocket;

    #@3b
    iget-object v2, p0, Landroid/webkit/WebSocket$1;->this$0:Landroid/webkit/WebSocket;

    #@3d
    iget v2, v2, Landroid/webkit/WebSocket;->mNativePointer:I

    #@3f
    invoke-static {v1, v2}, Landroid/webkit/WebSocket;->access$400(Landroid/webkit/WebSocket;I)V

    #@42
    goto :goto_5

    #@43
    .line 699
    nop

    #@44
    :pswitch_data_44
    .packed-switch 0xc8
        :pswitch_6
        :pswitch_10
        :pswitch_1a
        :pswitch_39
    .end packed-switch
.end method
