.class final Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;
.super Ljava/lang/Object;
.source "HTML5VideoViewProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/HTML5VideoViewProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "VideoPlayer"
.end annotation


# static fields
.field private static isVideoSelfEnded:Z

.field private static mBaseLayer:I

.field private static mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

.field private static mHTML5VideoView:Landroid/webkit/HTML5VideoView;

.field private static mIsShowingStreaming:Z

.field private static mOverlayReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 138
    sput-boolean v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->isVideoSelfEnded:Z

    #@3
    .line 142
    sput v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mBaseLayer:I

    #@5
    .line 146
    const/4 v0, 0x0

    #@6
    sput-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mOverlayReceiver:Landroid/content/BroadcastReceiver;

    #@8
    .line 147
    sput-boolean v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mIsShowingStreaming:Z

    #@a
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 131
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$600()Landroid/webkit/HTML5VideoViewProxy;
    .registers 1

    #@0
    .prologue
    .line 131
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Z)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-static {p0}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->setShowingStreaming(Z)V

    #@3
    return-void
.end method

.method static synthetic access$802(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    sput-boolean p0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->isVideoSelfEnded:Z

    #@2
    return p0
.end method

.method static synthetic access$900(Z)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-static {p0}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->setPlayerBuffering(Z)V

    #@3
    return-void
.end method

.method public static end()V
    .registers 1

    #@0
    .prologue
    .line 493
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@2
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoView;->showControllerInFullScreen()V

    #@5
    .line 494
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@7
    if-eqz v0, :cond_12

    #@9
    .line 495
    sget-boolean v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->isVideoSelfEnded:Z

    #@b
    if-eqz v0, :cond_16

    #@d
    .line 496
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@f
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoViewProxy;->dispatchOnEnded()V

    #@12
    .line 500
    :cond_12
    :goto_12
    const/4 v0, 0x0

    #@13
    sput-boolean v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->isVideoSelfEnded:Z

    #@15
    .line 501
    return-void

    #@16
    .line 498
    :cond_16
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@18
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoViewProxy;->dispatchOnPaused()V

    #@1b
    goto :goto_12
.end method

.method public static enterFullScreenVideo(ILjava/lang/String;Landroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebViewClassic;)V
    .registers 19
    .parameter "layerId"
    .parameter "url"
    .parameter "proxy"
    .parameter "webView"

    #@0
    .prologue
    .line 229
    const/4 v10, 0x0

    #@1
    .line 230
    .local v10, savePosition:I
    const/4 v2, 0x0

    #@2
    .line 231
    .local v2, canSkipPrepare:Z
    const/4 v4, 0x0

    #@3
    .line 234
    .local v4, forceStart:Z
    const/4 v7, 0x0

    #@4
    .line 236
    .local v7, isPlaying:Z
    invoke-virtual/range {p2 .. p2}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@7
    move-result-object v8

    #@8
    .line 239
    .local v8, mContext:Landroid/content/Context;
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@a
    if-eqz v12, :cond_151

    #@c
    .line 242
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@e
    invoke-virtual {v12}, Landroid/webkit/HTML5VideoView;->fullScreenExited()Z

    #@11
    move-result v12

    #@12
    if-nez v12, :cond_24

    #@14
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@16
    invoke-virtual {v12}, Landroid/webkit/HTML5VideoView;->isFullScreenMode()Z

    #@19
    move-result v12

    #@1a
    if-eqz v12, :cond_24

    #@1c
    .line 243
    const-string v12, "HTML5VideoViewProxy"

    #@1e
    const-string v13, "Try to reenter the full screen mode"

    #@20
    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 359
    :goto_23
    return-void

    #@24
    .line 246
    :cond_24
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@26
    invoke-virtual {v12}, Landroid/webkit/HTML5VideoView;->getCurrentState()I

    #@29
    move-result v9

    #@2a
    .line 249
    .local v9, playerState:I
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@2c
    invoke-virtual {v12}, Landroid/webkit/HTML5VideoView;->getVideoLayerId()I

    #@2f
    move-result v12

    #@30
    if-ne p0, v12, :cond_76

    #@32
    .line 250
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@34
    invoke-virtual {v12}, Landroid/webkit/HTML5VideoView;->getCurrentPosition()I

    #@37
    move-result v10

    #@38
    .line 251
    const/4 v12, 0x1

    #@39
    if-eq v9, v12, :cond_41

    #@3b
    const/4 v12, 0x2

    #@3c
    if-eq v9, v12, :cond_41

    #@3e
    const/4 v12, 0x3

    #@3f
    if-ne v9, v12, :cond_140

    #@41
    :cond_41
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@43
    invoke-virtual {v12}, Landroid/webkit/HTML5VideoView;->isFullScreenMode()Z

    #@46
    move-result v12

    #@47
    if-nez v12, :cond_140

    #@49
    const/4 v2, 0x1

    #@4a
    .line 256
    :goto_4a
    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@4d
    move-result-object v12

    #@4e
    const-string v13, "com.android.browser"

    #@50
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v12

    #@54
    if-eqz v12, :cond_76

    #@56
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy;->access$200()Z

    #@59
    move-result v12

    #@5a
    if-eqz v12, :cond_76

    #@5c
    .line 257
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy;->access$000()Z

    #@5f
    move-result v12

    #@60
    if-nez v12, :cond_63

    #@62
    .line 258
    const/4 v2, 0x0

    #@63
    .line 260
    :cond_63
    const/4 v12, 0x1

    #@64
    if-eq v9, v12, :cond_69

    #@66
    const/4 v12, 0x3

    #@67
    if-ne v9, v12, :cond_143

    #@69
    :cond_69
    const/4 v7, 0x1

    #@6a
    .line 262
    :goto_6a
    invoke-static/range {p2 .. p2}, Landroid/webkit/HTML5VideoViewProxy;->access$300(Landroid/webkit/HTML5VideoViewProxy;)Z

    #@6d
    move-result v12

    #@6e
    if-eqz v12, :cond_76

    #@70
    .line 263
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@72
    invoke-virtual {v12}, Landroid/webkit/HTML5VideoView;->getSaveSeekTime()I

    #@75
    move-result v10

    #@76
    .line 268
    :cond_76
    if-nez v2, :cond_146

    #@78
    .line 269
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@7a
    invoke-virtual {v12}, Landroid/webkit/HTML5VideoView;->reset()V

    #@7d
    .line 283
    .end local v9           #playerState:I
    :goto_7d
    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@80
    move-result-object v12

    #@81
    const-string v13, "com.android.browser"

    #@83
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v12

    #@87
    if-eqz v12, :cond_170

    #@89
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy;->access$200()Z

    #@8c
    move-result v12

    #@8d
    if-eqz v12, :cond_170

    #@8f
    .line 286
    invoke-virtual/range {p2 .. p2}, Landroid/webkit/HTML5VideoViewProxy;->dispatchOnEnterFromLGBrowser()V

    #@92
    .line 288
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@94
    invoke-virtual {v12}, Landroid/webkit/HTML5VideoView;->getHeaders()Ljava/util/Map;

    #@97
    move-result-object v5

    #@98
    check-cast v5, Ljava/util/HashMap;

    #@9a
    .line 289
    .local v5, headers:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v12, 0x1

    #@9b
    invoke-static {v12}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->setShowingStreaming(Z)V

    #@9e
    .line 291
    :try_start_9e
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy;->access$000()Z

    #@a1
    move-result v12

    #@a2
    if-eqz v12, :cond_b9

    #@a4
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy;->access$400()Z

    #@a7
    move-result v12

    #@a8
    if-eqz v12, :cond_b9

    #@aa
    .line 292
    const/4 v10, 0x0

    #@ab
    .line 293
    const/4 v12, 0x0

    #@ac
    invoke-static {v12}, Landroid/webkit/HTML5VideoViewProxy;->access$402(Z)Z

    #@af
    .line 294
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@b1
    invoke-virtual {v12, v10}, Landroid/webkit/HTML5VideoViewProxy;->onTimeupdateManually(I)V

    #@b4
    .line 295
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@b6
    invoke-virtual {v12, v10}, Landroid/webkit/HTML5VideoViewProxy;->seek(I)V

    #@b9
    .line 297
    :cond_b9
    new-instance v6, Landroid/content/Intent;

    #@bb
    const-string/jumbo v12, "lge.browser.intent.action.HTML5VIDEO_STREAMING_PLAY"

    #@be
    invoke-direct {v6, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c1
    .line 298
    .local v6, i:Landroid/content/Intent;
    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@c4
    move-result-object v12

    #@c5
    invoke-virtual {v6, v12}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@c8
    .line 299
    const-string/jumbo v12, "position"

    #@cb
    invoke-virtual {v6, v12, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@ce
    .line 300
    const-string v12, "Header"

    #@d0
    invoke-virtual {v6, v12, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@d3
    .line 301
    if-nez v10, :cond_e1

    #@d5
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@d7
    move-object/from16 v0, p2

    #@d9
    if-eq v12, v0, :cond_e1

    #@db
    .line 302
    const/4 v12, 0x1

    #@dc
    move-object/from16 v0, p2

    #@de
    invoke-static {v0, v12}, Landroid/webkit/HTML5VideoViewProxy;->access$500(Landroid/webkit/HTML5VideoViewProxy;Z)V

    #@e1
    .line 304
    :cond_e1
    sput-object p2, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@e3
    .line 305
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@e5
    invoke-static {v12}, Landroid/webkit/HTML5VideoViewProxy;->access$300(Landroid/webkit/HTML5VideoViewProxy;)Z

    #@e8
    move-result v12

    #@e9
    if-eqz v12, :cond_ec

    #@eb
    .line 306
    const/4 v7, 0x1

    #@ec
    .line 308
    :cond_ec
    const-string/jumbo v12, "isplay"

    #@ef
    invoke-virtual {v6, v12, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@f2
    .line 309
    new-instance v12, Landroid/content/ComponentName;

    #@f4
    const-string v13, "com.lge.streamingplayer"

    #@f6
    const-string v14, "com.lge.streamingplayer.StreamingPlayer"

    #@f8
    invoke-direct {v12, v13, v14}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@fb
    invoke-virtual {v6, v12}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@fe
    .line 310
    invoke-virtual/range {p3 .. p3}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@101
    move-result-object v11

    #@102
    .line 311
    .local v11, settings:Landroid/webkit/WebSettings;
    if-eqz v11, :cond_16c

    #@104
    invoke-virtual {v11}, Landroid/webkit/WebSettingsClassic;->getFloatingMode()Z

    #@107
    move-result v12

    #@108
    if-eqz v12, :cond_16c

    #@10a
    .line 312
    const/high16 v12, 0x1000

    #@10c
    invoke-virtual {v6, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@10f
    .line 313
    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@112
    move-result-object v12

    #@113
    invoke-virtual {v12, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@116
    .line 321
    :goto_116
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@118
    const/4 v13, 0x0

    #@119
    invoke-virtual {v12, v13}, Landroid/webkit/HTML5VideoViewProxy;->dispatchOnStopFullScreen(Z)V

    #@11c
    .line 322
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->unRegOverlayReceiver()V

    #@11f
    .line 323
    new-instance v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer$1;

    #@121
    invoke-direct {v12}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer$1;-><init>()V

    #@124
    sput-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mOverlayReceiver:Landroid/content/BroadcastReceiver;

    #@126
    .line 340
    new-instance v3, Landroid/content/IntentFilter;

    #@128
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@12b
    .line 341
    .local v3, filter:Landroid/content/IntentFilter;
    const-string v12, "android.intent.category.DEFAULT"

    #@12d
    invoke-virtual {v3, v12}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    #@130
    .line 342
    const-string/jumbo v12, "lge.browser.intent.action.HTML5VIDEO_BROWSER_PLAY"

    #@133
    invoke-virtual {v3, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@136
    .line 343
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mOverlayReceiver:Landroid/content/BroadcastReceiver;

    #@138
    invoke-virtual {v8, v12, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_13b
    .catch Landroid/content/ActivityNotFoundException; {:try_start_9e .. :try_end_13b} :catch_13d

    #@13b
    goto/16 :goto_23

    #@13d
    .line 346
    .end local v3           #filter:Landroid/content/IntentFilter;
    .end local v6           #i:Landroid/content/Intent;
    .end local v11           #settings:Landroid/webkit/WebSettings;
    :catch_13d
    move-exception v12

    #@13e
    goto/16 :goto_23

    #@140
    .line 251
    .end local v5           #headers:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v9       #playerState:I
    :cond_140
    const/4 v2, 0x0

    #@141
    goto/16 :goto_4a

    #@143
    .line 260
    :cond_143
    const/4 v7, 0x0

    #@144
    goto/16 :goto_6a

    #@146
    .line 271
    :cond_146
    const/4 v12, 0x1

    #@147
    if-eq v9, v12, :cond_14c

    #@149
    const/4 v12, 0x3

    #@14a
    if-ne v9, v12, :cond_14f

    #@14c
    :cond_14c
    const/4 v4, 0x1

    #@14d
    :goto_14d
    goto/16 :goto_7d

    #@14f
    :cond_14f
    const/4 v4, 0x0

    #@150
    goto :goto_14d

    #@151
    .line 276
    .end local v9           #playerState:I
    :cond_151
    new-instance v12, Landroid/webkit/HTML5VideoFullScreen;

    #@153
    invoke-virtual/range {p2 .. p2}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@156
    move-result-object v13

    #@157
    invoke-direct {v12, v13, p0, v10, v2}, Landroid/webkit/HTML5VideoFullScreen;-><init>(Landroid/content/Context;IIZ)V

    #@15a
    sput-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@15c
    .line 278
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@15e
    invoke-virtual {v12, v4}, Landroid/webkit/HTML5VideoView;->setStartWhenPrepared(Z)V

    #@161
    .line 279
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@163
    move-object/from16 v0, p1

    #@165
    move-object/from16 v1, p2

    #@167
    invoke-virtual {v12, v0, v1}, Landroid/webkit/HTML5VideoView;->setVideoURI(Ljava/lang/String;Landroid/webkit/HTML5VideoViewProxy;)V

    #@16a
    goto/16 :goto_7d

    #@16c
    .line 315
    .restart local v5       #headers:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v6       #i:Landroid/content/Intent;
    .restart local v11       #settings:Landroid/webkit/WebSettings;
    :cond_16c
    :try_start_16c
    invoke-virtual {v8, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_16f
    .catch Landroid/content/ActivityNotFoundException; {:try_start_16c .. :try_end_16f} :catch_13d

    #@16f
    goto :goto_116

    #@170
    .line 351
    .end local v5           #headers:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6           #i:Landroid/content/Intent;
    .end local v11           #settings:Landroid/webkit/WebSettings;
    :cond_170
    new-instance v12, Landroid/webkit/HTML5VideoFullScreen;

    #@172
    invoke-virtual/range {p2 .. p2}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@175
    move-result-object v13

    #@176
    invoke-direct {v12, v13, p0, v10, v2}, Landroid/webkit/HTML5VideoFullScreen;-><init>(Landroid/content/Context;IIZ)V

    #@179
    sput-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@17b
    .line 353
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@17d
    invoke-virtual {v12, v4}, Landroid/webkit/HTML5VideoView;->setStartWhenPrepared(Z)V

    #@180
    .line 354
    sput-object p2, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@182
    .line 355
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@184
    sget-object v13, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@186
    move-object/from16 v0, p1

    #@188
    invoke-virtual {v12, v0, v13}, Landroid/webkit/HTML5VideoView;->setVideoURI(Ljava/lang/String;Landroid/webkit/HTML5VideoViewProxy;)V

    #@18b
    .line 356
    sget-object v12, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@18d
    move-object/from16 v0, p2

    #@18f
    move-object/from16 v1, p3

    #@191
    invoke-virtual {v12, p0, v0, v1}, Landroid/webkit/HTML5VideoView;->enterFullScreenVideoState(ILandroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebViewClassic;)V

    #@194
    goto/16 :goto_23
.end method

.method public static exitFullScreenVideo(Landroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebViewClassic;)V
    .registers 4
    .parameter "proxy"
    .parameter "webView"

    #@0
    .prologue
    .line 363
    sget-object v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@2
    if-eqz v1, :cond_1d

    #@4
    sget-object v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@6
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoView;->fullScreenExited()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_1d

    #@c
    sget-object v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@e
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoView;->isFullScreenMode()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1d

    #@14
    .line 364
    invoke-virtual {p1}, Landroid/webkit/WebViewClassic;->getWebChromeClient()Landroid/webkit/WebChromeClient;

    #@17
    move-result-object v0

    #@18
    .line 365
    .local v0, client:Landroid/webkit/WebChromeClient;
    if-eqz v0, :cond_1d

    #@1a
    .line 366
    invoke-virtual {v0}, Landroid/webkit/WebChromeClient;->onHideCustomView()V

    #@1d
    .line 369
    .end local v0           #client:Landroid/webkit/WebChromeClient;
    :cond_1d
    return-void
.end method

.method public static getCurrentPosition()I
    .registers 2

    #@0
    .prologue
    .line 462
    const/4 v0, 0x0

    #@1
    .line 463
    .local v0, currentPosMs:I
    sget-object v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@3
    if-eqz v1, :cond_b

    #@5
    .line 464
    sget-object v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@7
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoView;->getCurrentPosition()I

    #@a
    move-result v0

    #@b
    .line 466
    :cond_b
    return v0
.end method

.method public static isPlaying(Landroid/webkit/HTML5VideoViewProxy;)Z
    .registers 2
    .parameter "proxy"

    #@0
    .prologue
    .line 457
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@2
    if-ne v0, p0, :cond_12

    #@4
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@6
    if-eqz v0, :cond_12

    #@8
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@a
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoView;->isPlaying()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method private static isShowingStreaming()Z
    .registers 1

    #@0
    .prologue
    .line 150
    sget-boolean v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mIsShowingStreaming:Z

    #@2
    return v0
.end method

.method public static onPrepared()V
    .registers 1

    #@0
    .prologue
    .line 482
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@2
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoView;->isFullScreenMode()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_d

    #@8
    .line 483
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@a
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoView;->start()V

    #@d
    .line 486
    :cond_d
    sget v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mBaseLayer:I

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 487
    sget v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mBaseLayer:I

    #@13
    invoke-static {v0}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->setBaseLayer(I)V

    #@16
    .line 490
    :cond_16
    return-void
.end method

.method public static pause(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 2
    .parameter "proxy"

    #@0
    .prologue
    .line 476
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@2
    if-ne v0, p0, :cond_d

    #@4
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 477
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@a
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoView;->pause()V

    #@d
    .line 479
    :cond_d
    return-void
.end method

.method public static pauseAndDispatch()V
    .registers 2

    #@0
    .prologue
    .line 220
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 221
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@6
    sget-object v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@8
    invoke-virtual {v0, v1}, Landroid/webkit/HTML5VideoView;->pauseAndDispatch(Landroid/webkit/HTML5VideoViewProxy;)V

    #@b
    .line 222
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@d
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoView;->reset()V

    #@10
    .line 224
    :cond_10
    return-void
.end method

.method public static play(Ljava/lang/String;ILandroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebChromeClient;I)V
    .registers 13
    .parameter "url"
    .parameter "time"
    .parameter "proxy"
    .parameter "client"
    .parameter "videoLayerId"

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    .line 376
    const/4 v2, -0x1

    #@2
    .line 377
    .local v2, currentVideoLayerId:I
    const/4 v0, 0x0

    #@3
    .line 379
    .local v0, backFromFullScreenMode:Z
    invoke-virtual {p2}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@6
    move-result-object v3

    #@7
    .line 380
    .local v3, mContext:Landroid/content/Context;
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@a
    move-result-object v5

    #@b
    const-string v6, "com.android.browser"

    #@d
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v5

    #@11
    if-eqz v5, :cond_34

    #@13
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@15
    if-eqz v5, :cond_34

    #@17
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@19
    if-eqz v5, :cond_34

    #@1b
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy;->access$200()Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_34

    #@21
    .line 381
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy;->access$000()Z

    #@24
    move-result v5

    #@25
    if-nez v5, :cond_2d

    #@27
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->isShowingStreaming()Z

    #@2a
    move-result v5

    #@2b
    if-nez v5, :cond_33

    #@2d
    :cond_2d
    invoke-static {p2}, Landroid/webkit/HTML5VideoViewProxy;->access$300(Landroid/webkit/HTML5VideoViewProxy;)Z

    #@30
    move-result v5

    #@31
    if-eqz v5, :cond_34

    #@33
    .line 454
    :cond_33
    :goto_33
    return-void

    #@34
    .line 386
    :cond_34
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@36
    if-eqz v5, :cond_67

    #@38
    .line 387
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@3a
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->getVideoLayerId()I

    #@3d
    move-result v2

    #@3e
    .line 388
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@40
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->fullScreenExited()Z

    #@43
    move-result v0

    #@44
    .line 394
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@46
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->isFullScreenMode()Z

    #@49
    move-result v5

    #@4a
    if-eqz v5, :cond_67

    #@4c
    if-nez v0, :cond_67

    #@4e
    if-eq v2, p4, :cond_67

    #@50
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@52
    if-eq v5, p2, :cond_67

    #@54
    .line 398
    sput-object p2, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@56
    .line 399
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@58
    const/4 v6, 0x1

    #@59
    invoke-virtual {v5, v6}, Landroid/webkit/HTML5VideoView;->setStartWhenPrepared(Z)V

    #@5c
    .line 400
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@5e
    invoke-virtual {v5, p0, p2}, Landroid/webkit/HTML5VideoView;->setVideoURI(Ljava/lang/String;Landroid/webkit/HTML5VideoViewProxy;)V

    #@61
    .line 401
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@63
    invoke-virtual {v5, p2}, Landroid/webkit/HTML5VideoView;->reprepareData(Landroid/webkit/HTML5VideoViewProxy;)V

    #@66
    goto :goto_33

    #@67
    .line 406
    :cond_67
    const/4 v4, 0x0

    #@68
    .line 407
    .local v4, skipPrepare:Z
    const/4 v1, 0x0

    #@69
    .line 408
    .local v1, createInlineView:Z
    if-eqz v0, :cond_8f

    #@6b
    if-ne v2, p4, :cond_8f

    #@6d
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@6f
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->isReleased()Z

    #@72
    move-result v5

    #@73
    if-nez v5, :cond_8f

    #@75
    .line 411
    const/4 v4, 0x1

    #@76
    .line 412
    const/4 v1, 0x1

    #@77
    .line 434
    :cond_77
    :goto_77
    if-eqz v1, :cond_c5

    #@79
    .line 435
    sput-object p2, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@7b
    .line 436
    new-instance v5, Landroid/webkit/HTML5VideoInline;

    #@7d
    invoke-direct {v5, p4, p1, v4}, Landroid/webkit/HTML5VideoInline;-><init>(IIZ)V

    #@80
    sput-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@82
    .line 438
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@84
    sget-object v6, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@86
    invoke-virtual {v5, p0, v6}, Landroid/webkit/HTML5VideoView;->setVideoURI(Ljava/lang/String;Landroid/webkit/HTML5VideoViewProxy;)V

    #@89
    .line 439
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@8b
    invoke-virtual {v5, p2}, Landroid/webkit/HTML5VideoView;->prepareDataAndDisplayMode(Landroid/webkit/HTML5VideoViewProxy;)V

    #@8e
    goto :goto_33

    #@8f
    .line 413
    :cond_8f
    if-nez v0, :cond_a7

    #@91
    if-ne v2, p4, :cond_a7

    #@93
    invoke-static {}, Landroid/webkit/HTML5VideoInline;->surfaceTextureDeleted()Z

    #@96
    move-result v5

    #@97
    if-nez v5, :cond_a7

    #@99
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@9b
    if-eqz v5, :cond_77

    #@9d
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@9f
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->getCurrentState()I

    #@a2
    move-result v5

    #@a3
    sget-object v6, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@a5
    if-ne v5, v7, :cond_77

    #@a7
    .line 425
    :cond_a7
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@a9
    if-eqz v5, :cond_c3

    #@ab
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@ad
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->getCurrentState()I

    #@b0
    move-result v5

    #@b1
    sget-object v6, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@b3
    if-eq v5, v7, :cond_c3

    #@b5
    .line 427
    if-nez v0, :cond_be

    #@b7
    .line 428
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@b9
    sget-object v6, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@bb
    invoke-virtual {v5, v6}, Landroid/webkit/HTML5VideoView;->pauseAndDispatch(Landroid/webkit/HTML5VideoViewProxy;)V

    #@be
    .line 430
    :cond_be
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@c0
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->reset()V

    #@c3
    .line 432
    :cond_c3
    const/4 v1, 0x1

    #@c4
    goto :goto_77

    #@c5
    .line 443
    :cond_c5
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@c7
    if-ne v5, p2, :cond_dd

    #@c9
    .line 445
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@cb
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->isPlaying()Z

    #@ce
    move-result v5

    #@cf
    if-nez v5, :cond_33

    #@d1
    .line 446
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@d3
    invoke-virtual {v5, p1}, Landroid/webkit/HTML5VideoView;->seekTo(I)V

    #@d6
    .line 447
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@d8
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->start()V

    #@db
    goto/16 :goto_33

    #@dd
    .line 449
    :cond_dd
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@df
    if-eqz v5, :cond_33

    #@e1
    .line 452
    invoke-virtual {p2}, Landroid/webkit/HTML5VideoViewProxy;->dispatchOnEnded()V

    #@e4
    goto/16 :goto_33
.end method

.method public static seek(ILandroid/webkit/HTML5VideoViewProxy;)V
    .registers 3
    .parameter "time"
    .parameter "proxy"

    #@0
    .prologue
    .line 470
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@2
    if-ne v0, p1, :cond_f

    #@4
    if-ltz p0, :cond_f

    #@6
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 471
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@c
    invoke-virtual {v0, p0}, Landroid/webkit/HTML5VideoView;->seekTo(I)V

    #@f
    .line 473
    :cond_f
    return-void
.end method

.method public static setBaseLayer(I)V
    .registers 8
    .parameter "layer"

    #@0
    .prologue
    .line 180
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@2
    if-nez v5, :cond_5

    #@4
    .line 216
    :cond_4
    :goto_4
    return-void

    #@5
    .line 183
    :cond_5
    sput p0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mBaseLayer:I

    #@7
    .line 185
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@9
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->isFullScreenMode()Z

    #@c
    move-result v5

    #@d
    if-nez v5, :cond_4

    #@f
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@11
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->isReleased()Z

    #@14
    move-result v5

    #@15
    if-nez v5, :cond_4

    #@17
    .line 193
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy;->access$000()Z

    #@1a
    move-result v5

    #@1b
    if-nez v5, :cond_23

    #@1d
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->isShowingStreaming()Z

    #@20
    move-result v5

    #@21
    if-nez v5, :cond_4

    #@23
    .line 198
    :cond_23
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@25
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->getVideoLayerId()I

    #@28
    move-result v0

    #@29
    .line 199
    .local v0, currentVideoLayerId:I
    invoke-static {v0}, Landroid/webkit/HTML5VideoInline;->getSurfaceTexture(I)Landroid/graphics/SurfaceTexture;

    #@2c
    move-result-object v3

    #@2d
    .line 201
    .local v3, surfTexture:Landroid/graphics/SurfaceTexture;
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@2f
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->getTextureName()I

    #@32
    move-result v4

    #@33
    .line 203
    .local v4, textureName:I
    if-eqz p0, :cond_4

    #@35
    if-eqz v3, :cond_4

    #@37
    const/4 v5, -0x1

    #@38
    if-eq v0, v5, :cond_4

    #@3a
    .line 204
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@3c
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->getCurrentState()I

    #@3f
    move-result v2

    #@40
    .line 205
    .local v2, playerState:I
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@42
    invoke-virtual {v5}, Landroid/webkit/HTML5VideoView;->getPlayerBuffering()Z

    #@45
    move-result v5

    #@46
    if-eqz v5, :cond_49

    #@48
    .line 206
    const/4 v2, 0x1

    #@49
    .line 207
    :cond_49
    invoke-static {v3, p0, v0, v4, v2}, Landroid/webkit/HTML5VideoViewProxy;->access$100(Landroid/graphics/SurfaceTexture;IIII)Z

    #@4c
    move-result v1

    #@4d
    .line 210
    .local v1, foundInTree:Z
    const/4 v5, 0x2

    #@4e
    if-lt v2, v5, :cond_4

    #@50
    if-nez v1, :cond_4

    #@52
    .line 212
    sget-object v5, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@54
    sget-object v6, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@56
    invoke-virtual {v5, v6}, Landroid/webkit/HTML5VideoView;->pauseAndDispatch(Landroid/webkit/HTML5VideoViewProxy;)V

    #@59
    goto :goto_4
.end method

.method private static setPlayerBuffering(Z)V
    .registers 2
    .parameter "playerBuffering"

    #@0
    .prologue
    .line 171
    sget-object v0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mHTML5VideoView:Landroid/webkit/HTML5VideoView;

    #@2
    invoke-virtual {v0, p0}, Landroid/webkit/HTML5VideoView;->setPlayerBuffering(Z)V

    #@5
    .line 172
    return-void
.end method

.method private static setShowingStreaming(Z)V
    .registers 1
    .parameter "flag"

    #@0
    .prologue
    .line 154
    sput-boolean p0, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mIsShowingStreaming:Z

    #@2
    .line 155
    return-void
.end method

.method public static unRegOverlayReceiver()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 158
    sget-object v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mOverlayReceiver:Landroid/content/BroadcastReceiver;

    #@3
    if-eqz v1, :cond_12

    #@5
    .line 159
    sget-object v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mCurrentProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@7
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@a
    move-result-object v0

    #@b
    .line 161
    .local v0, mContext:Landroid/content/Context;
    :try_start_b
    sget-object v1, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mOverlayReceiver:Landroid/content/BroadcastReceiver;

    #@d
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_10
    .catchall {:try_start_b .. :try_end_10} :catchall_13
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_10} :catch_17

    #@10
    .line 164
    :goto_10
    sput-object v2, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mOverlayReceiver:Landroid/content/BroadcastReceiver;

    #@12
    .line 167
    :cond_12
    return-void

    #@13
    .line 164
    :catchall_13
    move-exception v1

    #@14
    sput-object v2, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->mOverlayReceiver:Landroid/content/BroadcastReceiver;

    #@16
    throw v1

    #@17
    .line 162
    :catch_17
    move-exception v1

    #@18
    goto :goto_10
.end method
