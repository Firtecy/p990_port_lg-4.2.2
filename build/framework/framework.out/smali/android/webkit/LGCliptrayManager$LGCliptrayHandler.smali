.class Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;
.super Landroid/os/Handler;
.source "LGCliptrayManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/LGCliptrayManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LGCliptrayHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/LGCliptrayManager;


# direct methods
.method private constructor <init>(Landroid/webkit/LGCliptrayManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 122
    iput-object p1, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/LGCliptrayManager;Landroid/webkit/LGCliptrayManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 122
    invoke-direct {p0, p1}, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;-><init>(Landroid/webkit/LGCliptrayManager;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    .line 125
    iget v5, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v5, :pswitch_data_ee

    #@5
    .line 184
    :cond_5
    :goto_5
    return-void

    #@6
    .line 127
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v0, Landroid/content/ClipData;

    #@a
    .line 128
    .local v0, clipData:Landroid/content/ClipData;
    if-nez v0, :cond_14

    #@c
    .line 129
    const-string v5, "LGCliptrayManager"

    #@e
    const-string v6, "LGCliptrayHandler::LGCLIPTRAY_ONPASTE: clipData is null"

    #@10
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    goto :goto_5

    #@14
    .line 133
    :cond_14
    const/4 v3, 0x0

    #@15
    .local v3, i:I
    :goto_15
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    #@18
    move-result v5

    #@19
    if-ge v3, v5, :cond_5

    #@1b
    .line 134
    const/4 v4, 0x0

    #@1c
    .line 135
    .local v4, pasteText:Ljava/lang/CharSequence;
    invoke-virtual {v0, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@1f
    move-result-object v1

    #@20
    .line 136
    .local v1, clipItem:Landroid/content/ClipData$Item;
    if-nez v1, :cond_2c

    #@22
    .line 137
    const-string v5, "LGCliptrayManager"

    #@24
    const-string v6, "LGCliptrayHandler::LGCLIPTRAY_ONPASTE: clipItem is null"

    #@26
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 133
    :cond_29
    :goto_29
    add-int/lit8 v3, v3, 0x1

    #@2b
    goto :goto_15

    #@2c
    .line 140
    :cond_2c
    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@2f
    move-result-object v2

    #@30
    .line 141
    .local v2, clipUri:Landroid/net/Uri;
    if-eqz v2, :cond_3e

    #@32
    const-string v5, "file"

    #@34
    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v5

    #@3c
    if-nez v5, :cond_29

    #@3e
    .line 145
    :cond_3e
    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    #@41
    move-result-object v4

    #@42
    .line 146
    if-nez v4, :cond_4a

    #@44
    if-eqz v2, :cond_4a

    #@46
    .line 147
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    .line 150
    :cond_4a
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@4c
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$200(Landroid/webkit/LGCliptrayManager;)Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@4f
    move-result-object v5

    #@50
    if-eqz v5, :cond_29

    #@52
    if-eqz v4, :cond_29

    #@54
    .line 151
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@56
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$200(Landroid/webkit/LGCliptrayManager;)Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5, v4}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->replaceSelection(Ljava/lang/CharSequence;)V

    #@5d
    goto :goto_29

    #@5e
    .line 158
    .end local v0           #clipData:Landroid/content/ClipData;
    .end local v1           #clipItem:Landroid/content/ClipData$Item;
    .end local v2           #clipUri:Landroid/net/Uri;
    .end local v3           #i:I
    .end local v4           #pasteText:Ljava/lang/CharSequence;
    :pswitch_5e
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@60
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$300(Landroid/webkit/LGCliptrayManager;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@63
    move-result-object v5

    #@64
    if-eqz v5, :cond_84

    #@66
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@68
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$300(Landroid/webkit/LGCliptrayManager;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@6b
    move-result-object v5

    #@6c
    invoke-interface {v5}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isServiceConnected()Z

    #@6f
    move-result v5

    #@70
    if-eqz v5, :cond_84

    #@72
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@74
    if-eqz v5, :cond_84

    #@76
    .line 159
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@78
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$300(Landroid/webkit/LGCliptrayManager;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@7b
    move-result-object v6

    #@7c
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7e
    check-cast v5, Ljava/lang/CharSequence;

    #@80
    invoke-interface {v6, v5}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->copyToCliptray(Ljava/lang/CharSequence;)V

    #@83
    goto :goto_5

    #@84
    .line 161
    :cond_84
    const-string v5, "LGCliptrayManager"

    #@86
    const-string v6, "LGCliptrayHandler::LGCLIPTRAY_COPY_TEXT: mCliptrayManager is null or not connected"

    #@88
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    goto/16 :goto_5

    #@8d
    .line 166
    :pswitch_8d
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@8f
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$300(Landroid/webkit/LGCliptrayManager;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@92
    move-result-object v5

    #@93
    if-eqz v5, :cond_b4

    #@95
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@97
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$300(Landroid/webkit/LGCliptrayManager;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@9a
    move-result-object v5

    #@9b
    invoke-interface {v5}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isServiceConnected()Z

    #@9e
    move-result v5

    #@9f
    if-eqz v5, :cond_b4

    #@a1
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a3
    if-eqz v5, :cond_b4

    #@a5
    .line 167
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@a7
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$300(Landroid/webkit/LGCliptrayManager;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@aa
    move-result-object v6

    #@ab
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ad
    check-cast v5, Landroid/net/Uri;

    #@af
    invoke-interface {v6, v5}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->copyImageToCliptray(Landroid/net/Uri;)V

    #@b2
    goto/16 :goto_5

    #@b4
    .line 169
    :cond_b4
    const-string v5, "LGCliptrayManager"

    #@b6
    const-string v6, "LGCliptrayHandler::LGCLIPTRAY_COPY_IMAGE: mCliptrayManager is null or not connected"

    #@b8
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    goto/16 :goto_5

    #@bd
    .line 174
    :pswitch_bd
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@bf
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$300(Landroid/webkit/LGCliptrayManager;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@c2
    move-result-object v5

    #@c3
    if-eqz v5, :cond_e4

    #@c5
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@c7
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$300(Landroid/webkit/LGCliptrayManager;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@ca
    move-result-object v5

    #@cb
    invoke-interface {v5}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isServiceConnected()Z

    #@ce
    move-result v5

    #@cf
    if-eqz v5, :cond_e4

    #@d1
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d3
    if-eqz v5, :cond_e4

    #@d5
    .line 175
    iget-object v5, p0, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;->this$0:Landroid/webkit/LGCliptrayManager;

    #@d7
    invoke-static {v5}, Landroid/webkit/LGCliptrayManager;->access$300(Landroid/webkit/LGCliptrayManager;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@da
    move-result-object v6

    #@db
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@dd
    check-cast v5, Landroid/content/ClipData;

    #@df
    invoke-interface {v6, v5}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->copyToCliptray(Landroid/content/ClipData;)V

    #@e2
    goto/16 :goto_5

    #@e4
    .line 177
    :cond_e4
    const-string v5, "LGCliptrayManager"

    #@e6
    const-string v6, "LGCliptrayHandler::LGCLIPTRAY_COPY_DATA: mCliptrayManager is null or not connected"

    #@e8
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    goto/16 :goto_5

    #@ed
    .line 125
    nop

    #@ee
    :pswitch_data_ee
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5e
        :pswitch_8d
        :pswitch_bd
    .end packed-switch
.end method
