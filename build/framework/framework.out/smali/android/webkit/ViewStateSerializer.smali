.class Landroid/webkit/ViewStateSerializer;
.super Ljava/lang/Object;
.source "ViewStateSerializer.java"


# static fields
.field static final VERSION:I = 0x1

.field private static final WORKING_STREAM_STORAGE:I = 0x4000


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 86
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static deserializeViewState(Ljava/io/InputStream;)Landroid/webkit/WebViewCore$DrawData;
    .registers 10
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 52
    new-instance v3, Ljava/io/DataInputStream;

    #@2
    invoke-direct {v3, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@5
    .line 53
    .local v3, dis:Ljava/io/DataInputStream;
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@8
    move-result v5

    #@9
    .line 54
    .local v5, version:I
    const/4 v6, 0x1

    #@a
    if-le v5, v6, :cond_25

    #@c
    .line 55
    new-instance v6, Ljava/io/IOException;

    #@e
    new-instance v7, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v8, "Unexpected version: "

    #@15
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v7

    #@19
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v7

    #@21
    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@24
    throw v6

    #@25
    .line 57
    :cond_25
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@28
    move-result v2

    #@29
    .line 58
    .local v2, contentWidth:I
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@2c
    move-result v1

    #@2d
    .line 59
    .local v1, contentHeight:I
    const/16 v6, 0x4000

    #@2f
    new-array v6, v6, [B

    #@31
    invoke-static {v5, v3, v6}, Landroid/webkit/ViewStateSerializer;->nativeDeserializeViewState(ILjava/io/InputStream;[B)I

    #@34
    move-result v0

    #@35
    .line 62
    .local v0, baseLayer:I
    new-instance v4, Landroid/webkit/WebViewCore$DrawData;

    #@37
    invoke-direct {v4}, Landroid/webkit/WebViewCore$DrawData;-><init>()V

    #@3a
    .line 63
    .local v4, draw:Landroid/webkit/WebViewCore$DrawData;
    new-instance v6, Landroid/webkit/WebViewCore$ViewState;

    #@3c
    invoke-direct {v6}, Landroid/webkit/WebViewCore$ViewState;-><init>()V

    #@3f
    iput-object v6, v4, Landroid/webkit/WebViewCore$DrawData;->mViewState:Landroid/webkit/WebViewCore$ViewState;

    #@41
    .line 64
    new-instance v6, Landroid/graphics/Point;

    #@43
    invoke-direct {v6, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    #@46
    iput-object v6, v4, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@48
    .line 65
    iput v0, v4, Landroid/webkit/WebViewCore$DrawData;->mBaseLayer:I

    #@4a
    .line 66
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    #@4d
    .line 67
    return-object v4
.end method

.method public static dumpLayerHierarchy(ILjava/io/OutputStream;I)V
    .registers 4
    .parameter "baseLayer"
    .parameter "out"
    .parameter "level"

    #@0
    .prologue
    .line 71
    const/16 v0, 0x4000

    #@2
    new-array v0, v0, [B

    #@4
    invoke-static {p0, p2, p1, v0}, Landroid/webkit/ViewStateSerializer;->nativeDumpLayerHierarchy(IILjava/io/OutputStream;[B)V

    #@7
    .line 73
    return-void
.end method

.method private static native nativeDeserializeViewState(ILjava/io/InputStream;[B)I
.end method

.method private static native nativeDumpLayerHierarchy(IILjava/io/OutputStream;[B)V
.end method

.method private static native nativeSerializeViewState(ILjava/io/OutputStream;[B)Z
.end method

.method static serializeViewState(Ljava/io/OutputStream;Landroid/webkit/WebViewCore$DrawData;)Z
    .registers 5
    .parameter "stream"
    .parameter "draw"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 38
    iget v0, p1, Landroid/webkit/WebViewCore$DrawData;->mBaseLayer:I

    #@2
    .line 39
    .local v0, baseLayer:I
    if-nez v0, :cond_6

    #@4
    .line 40
    const/4 v2, 0x0

    #@5
    .line 46
    :goto_5
    return v2

    #@6
    .line 42
    :cond_6
    new-instance v1, Ljava/io/DataOutputStream;

    #@8
    invoke-direct {v1, p0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@b
    .line 43
    .local v1, dos:Ljava/io/DataOutputStream;
    const/4 v2, 0x1

    #@c
    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@f
    .line 44
    iget-object v2, p1, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@11
    iget v2, v2, Landroid/graphics/Point;->x:I

    #@13
    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@16
    .line 45
    iget-object v2, p1, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@18
    iget v2, v2, Landroid/graphics/Point;->y:I

    #@1a
    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@1d
    .line 46
    const/16 v2, 0x4000

    #@1f
    new-array v2, v2, [B

    #@21
    invoke-static {v0, v1, v2}, Landroid/webkit/ViewStateSerializer;->nativeSerializeViewState(ILjava/io/OutputStream;[B)Z

    #@24
    move-result v2

    #@25
    goto :goto_5
.end method
