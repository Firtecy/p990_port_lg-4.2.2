.class public Landroid/webkit/CookieManager;
.super Ljava/lang/Object;
.source "CookieManager.java"


# direct methods
.method protected constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    return-void
.end method

.method public static allowFileSchemeCookies()Z
    .registers 1

    #@0
    .prologue
    .line 180
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->allowFileSchemeCookiesImpl()Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static declared-synchronized getInstance()Landroid/webkit/CookieManager;
    .registers 2

    #@0
    .prologue
    .line 46
    const-class v1, Landroid/webkit/CookieManager;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    invoke-static {}, Landroid/webkit/WebViewFactory;->getProvider()Landroid/webkit/WebViewFactoryProvider;

    #@6
    move-result-object v0

    #@7
    invoke-interface {v0}, Landroid/webkit/WebViewFactoryProvider;->getCookieManager()Landroid/webkit/CookieManager;
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_d

    #@a
    move-result-object v0

    #@b
    monitor-exit v1

    #@c
    return-object v0

    #@d
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1

    #@f
    throw v0
.end method

.method public static setAcceptFileSchemeCookies(Z)V
    .registers 2
    .parameter "accept"

    #@0
    .prologue
    .line 204
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0}, Landroid/webkit/CookieManager;->setAcceptFileSchemeCookiesImpl(Z)V

    #@7
    .line 205
    return-void
.end method


# virtual methods
.method public declared-synchronized acceptCookie()Z
    .registers 2

    #@0
    .prologue
    .line 67
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method protected allowFileSchemeCookiesImpl()Z
    .registers 2

    #@0
    .prologue
    .line 189
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method protected clone()Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 34
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    #@2
    const-string v1, "doesn\'t implement Cloneable"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method protected flushCookieStore()V
    .registers 2

    #@0
    .prologue
    .line 168
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized getCookie(Landroid/net/WebAddress;)Ljava/lang/String;
    .registers 3
    .parameter "uri"

    #@0
    .prologue
    .line 118
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public getCookie(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 92
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getCookie(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 4
    .parameter "url"
    .parameter "privateBrowsing"

    #@0
    .prologue
    .line 105
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized hasCookies()Z
    .registers 2

    #@0
    .prologue
    .line 142
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized hasCookies(Z)Z
    .registers 3
    .parameter "privateBrowsing"

    #@0
    .prologue
    .line 152
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public removeAllCookie()V
    .registers 2

    #@0
    .prologue
    .line 133
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public removeExpiredCookie()V
    .registers 2

    #@0
    .prologue
    .line 159
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public removeSessionCookie()V
    .registers 2

    #@0
    .prologue
    .line 126
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized setAcceptCookie(Z)V
    .registers 3
    .parameter "accept"

    #@0
    .prologue
    .line 57
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method protected setAcceptFileSchemeCookiesImpl(Z)V
    .registers 3
    .parameter "accept"

    #@0
    .prologue
    .line 213
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public setCookie(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "url"
    .parameter "value"

    #@0
    .prologue
    .line 81
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method
