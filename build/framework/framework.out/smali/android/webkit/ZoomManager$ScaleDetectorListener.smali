.class Landroid/webkit/ZoomManager$ScaleDetectorListener;
.super Ljava/lang/Object;
.source "ZoomManager.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/ZoomManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleDetectorListener"
.end annotation


# instance fields
.field private mAccumulatedSpan:F

.field final synthetic this$0:Landroid/webkit/ZoomManager;


# direct methods
.method private constructor <init>(Landroid/webkit/ZoomManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 819
    iput-object p1, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/ZoomManager;Landroid/webkit/ZoomManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 819
    invoke-direct {p0, p1}, Landroid/webkit/ZoomManager$ScaleDetectorListener;-><init>(Landroid/webkit/ZoomManager;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleScale(Landroid/view/ScaleGestureDetector;)Z
    .registers 9
    .parameter "detector"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 857
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    #@5
    move-result v4

    #@6
    iget-object v5, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@8
    invoke-static {v5}, Landroid/webkit/ZoomManager;->access$900(Landroid/webkit/ZoomManager;)F

    #@b
    move-result v5

    #@c
    mul-float v1, v4, v5

    #@e
    .line 861
    .local v1, scale:F
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@10
    invoke-virtual {v4, v1}, Landroid/webkit/ZoomManager;->isScaleOverLimits(F)Z

    #@13
    move-result v4

    #@14
    if-nez v4, :cond_20

    #@16
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@18
    invoke-virtual {v4}, Landroid/webkit/ZoomManager;->getZoomOverviewScale()F

    #@1b
    move-result v4

    #@1c
    cmpg-float v4, v1, v4

    #@1e
    if-gez v4, :cond_78

    #@20
    :cond_20
    move v0, v3

    #@21
    .line 865
    .local v0, isScaleLimited:Z
    :goto_21
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@23
    invoke-virtual {v4, v1}, Landroid/webkit/ZoomManager;->computeScaleWithLimits(F)F

    #@26
    move-result v4

    #@27
    iget-object v5, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@29
    invoke-virtual {v5}, Landroid/webkit/ZoomManager;->getZoomOverviewScale()F

    #@2c
    move-result v5

    #@2d
    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    #@30
    move-result v1

    #@31
    .line 867
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@33
    invoke-static {v4}, Landroid/webkit/ZoomManager;->access$1000(Landroid/webkit/ZoomManager;)Z

    #@36
    move-result v4

    #@37
    if-nez v4, :cond_41

    #@39
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@3b
    invoke-virtual {v4, v1}, Landroid/webkit/ZoomManager;->willScaleTriggerZoom(F)Z

    #@3e
    move-result v4

    #@3f
    if-eqz v4, :cond_77

    #@41
    .line 868
    :cond_41
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@43
    invoke-static {v4, v3}, Landroid/webkit/ZoomManager;->access$1002(Landroid/webkit/ZoomManager;Z)Z

    #@46
    .line 870
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@48
    invoke-static {v4}, Landroid/webkit/ZoomManager;->access$900(Landroid/webkit/ZoomManager;)F

    #@4b
    move-result v4

    #@4c
    cmpl-float v4, v1, v4

    #@4e
    if-lez v4, :cond_7a

    #@50
    .line 871
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@52
    invoke-static {v4}, Landroid/webkit/ZoomManager;->access$900(Landroid/webkit/ZoomManager;)F

    #@55
    move-result v4

    #@56
    const/high16 v5, 0x3fa0

    #@58
    mul-float/2addr v4, v5

    #@59
    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    #@5c
    move-result v1

    #@5d
    .line 875
    :goto_5d
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@5f
    invoke-virtual {v4, v1}, Landroid/webkit/ZoomManager;->computeScaleWithLimits(F)F

    #@62
    move-result v1

    #@63
    .line 877
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@65
    invoke-static {v4}, Landroid/webkit/ZoomManager;->access$900(Landroid/webkit/ZoomManager;)F

    #@68
    move-result v4

    #@69
    sub-float v4, v1, v4

    #@6b
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    #@6e
    move-result v4

    #@6f
    invoke-static {}, Landroid/webkit/ZoomManager;->access$1100()F

    #@72
    move-result v5

    #@73
    cmpg-float v4, v4, v5

    #@75
    if-gez v4, :cond_89

    #@77
    .line 885
    .end local v0           #isScaleLimited:Z
    :cond_77
    :goto_77
    return v0

    #@78
    :cond_78
    move v0, v2

    #@79
    .line 861
    goto :goto_21

    #@7a
    .line 873
    .restart local v0       #isScaleLimited:Z
    :cond_7a
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@7c
    invoke-static {v4}, Landroid/webkit/ZoomManager;->access$900(Landroid/webkit/ZoomManager;)F

    #@7f
    move-result v4

    #@80
    const v5, 0x3f4ccccd

    #@83
    mul-float/2addr v4, v5

    #@84
    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    #@87
    move-result v1

    #@88
    goto :goto_5d

    #@89
    .line 880
    :cond_89
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@8b
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    #@8e
    move-result v5

    #@8f
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    #@92
    move-result v6

    #@93
    invoke-virtual {v4, v5, v6}, Landroid/webkit/ZoomManager;->setZoomCenter(FF)V

    #@96
    .line 881
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@98
    invoke-virtual {v4, v1, v2}, Landroid/webkit/ZoomManager;->setZoomScale(FZ)V

    #@9b
    .line 882
    iget-object v2, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@9d
    invoke-static {v2}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@a0
    move-result-object v2

    #@a1
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@a4
    move v0, v3

    #@a5
    .line 883
    goto :goto_77
.end method

.method public isPanningOnly(Landroid/view/ScaleGestureDetector;)Z
    .registers 11
    .parameter "detector"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 837
    iget-object v6, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@3
    invoke-static {v6}, Landroid/webkit/ZoomManager;->access$400(Landroid/webkit/ZoomManager;)F

    #@6
    move-result v2

    #@7
    .line 838
    .local v2, prevFocusX:F
    iget-object v6, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@9
    invoke-static {v6}, Landroid/webkit/ZoomManager;->access$500(Landroid/webkit/ZoomManager;)F

    #@c
    move-result v3

    #@d
    .line 839
    .local v3, prevFocusY:F
    iget-object v6, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@f
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    #@12
    move-result v7

    #@13
    invoke-static {v6, v7}, Landroid/webkit/ZoomManager;->access$402(Landroid/webkit/ZoomManager;F)F

    #@16
    .line 840
    iget-object v6, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@18
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    #@1b
    move-result v7

    #@1c
    invoke-static {v6, v7}, Landroid/webkit/ZoomManager;->access$502(Landroid/webkit/ZoomManager;F)F

    #@1f
    .line 841
    cmpl-float v6, v2, v5

    #@21
    if-nez v6, :cond_59

    #@23
    cmpl-float v6, v3, v5

    #@25
    if-nez v6, :cond_59

    #@27
    move v1, v5

    #@28
    .line 844
    .local v1, focusDelta:F
    :goto_28
    iget-object v6, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@2a
    invoke-static {v6}, Landroid/webkit/ZoomManager;->access$200(Landroid/webkit/ZoomManager;)Landroid/webkit/ZoomManager$FocusMovementQueue;

    #@2d
    move-result-object v6

    #@2e
    invoke-static {v6, v1}, Landroid/webkit/ZoomManager$FocusMovementQueue;->access$700(Landroid/webkit/ZoomManager$FocusMovementQueue;F)V

    #@31
    .line 845
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    #@34
    move-result v6

    #@35
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getPreviousSpan()F

    #@38
    move-result v7

    #@39
    sub-float/2addr v6, v7

    #@3a
    iget v7, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->mAccumulatedSpan:F

    #@3c
    add-float v0, v6, v7

    #@3e
    .line 847
    .local v0, deltaSpan:F
    iget-object v6, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@40
    invoke-static {v6}, Landroid/webkit/ZoomManager;->access$200(Landroid/webkit/ZoomManager;)Landroid/webkit/ZoomManager$FocusMovementQueue;

    #@43
    move-result-object v6

    #@44
    invoke-static {v6}, Landroid/webkit/ZoomManager$FocusMovementQueue;->access$800(Landroid/webkit/ZoomManager$FocusMovementQueue;)F

    #@47
    move-result v6

    #@48
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@4b
    move-result v7

    #@4c
    cmpl-float v6, v6, v7

    #@4e
    if-lez v6, :cond_7d

    #@50
    const/4 v4, 0x1

    #@51
    .line 848
    .local v4, result:Z
    :goto_51
    if-eqz v4, :cond_7f

    #@53
    .line 849
    iget v5, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->mAccumulatedSpan:F

    #@55
    add-float/2addr v5, v0

    #@56
    iput v5, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->mAccumulatedSpan:F

    #@58
    .line 853
    :goto_58
    return v4

    #@59
    .line 841
    .end local v0           #deltaSpan:F
    .end local v1           #focusDelta:F
    .end local v4           #result:Z
    :cond_59
    iget-object v6, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@5b
    invoke-static {v6}, Landroid/webkit/ZoomManager;->access$400(Landroid/webkit/ZoomManager;)F

    #@5e
    move-result v6

    #@5f
    sub-float/2addr v6, v2

    #@60
    iget-object v7, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@62
    invoke-static {v7}, Landroid/webkit/ZoomManager;->access$400(Landroid/webkit/ZoomManager;)F

    #@65
    move-result v7

    #@66
    sub-float/2addr v7, v2

    #@67
    mul-float/2addr v6, v7

    #@68
    iget-object v7, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@6a
    invoke-static {v7}, Landroid/webkit/ZoomManager;->access$500(Landroid/webkit/ZoomManager;)F

    #@6d
    move-result v7

    #@6e
    sub-float/2addr v7, v3

    #@6f
    iget-object v8, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@71
    invoke-static {v8}, Landroid/webkit/ZoomManager;->access$500(Landroid/webkit/ZoomManager;)F

    #@74
    move-result v8

    #@75
    sub-float/2addr v8, v3

    #@76
    mul-float/2addr v7, v8

    #@77
    add-float/2addr v6, v7

    #@78
    invoke-static {v6}, Landroid/util/FloatMath;->sqrt(F)F

    #@7b
    move-result v1

    #@7c
    goto :goto_28

    #@7d
    .line 847
    .restart local v0       #deltaSpan:F
    .restart local v1       #focusDelta:F
    :cond_7d
    const/4 v4, 0x0

    #@7e
    goto :goto_51

    #@7f
    .line 851
    .restart local v4       #result:Z
    :cond_7f
    iput v5, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->mAccumulatedSpan:F

    #@81
    goto :goto_58
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .registers 3
    .parameter "detector"

    #@0
    .prologue
    .line 889
    invoke-virtual {p0, p1}, Landroid/webkit/ZoomManager$ScaleDetectorListener;->isPanningOnly(Landroid/view/ScaleGestureDetector;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-virtual {p0, p1}, Landroid/webkit/ZoomManager$ScaleDetectorListener;->handleScale(Landroid/view/ScaleGestureDetector;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_17

    #@c
    .line 890
    :cond_c
    iget-object v0, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@e
    invoke-static {v0}, Landroid/webkit/ZoomManager;->access$200(Landroid/webkit/ZoomManager;)Landroid/webkit/ZoomManager$FocusMovementQueue;

    #@11
    move-result-object v0

    #@12
    invoke-static {v0}, Landroid/webkit/ZoomManager$FocusMovementQueue;->access$300(Landroid/webkit/ZoomManager$FocusMovementQueue;)V

    #@15
    .line 891
    const/4 v0, 0x1

    #@16
    .line 893
    :goto_16
    return v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .registers 4
    .parameter "detector"

    #@0
    .prologue
    .line 823
    iget-object v0, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/webkit/ZoomManager;->access$102(Landroid/webkit/ZoomManager;Z)Z

    #@6
    .line 824
    iget-object v0, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@8
    invoke-virtual {v0}, Landroid/webkit/ZoomManager;->dismissZoomPicker()V

    #@b
    .line 825
    iget-object v0, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@d
    invoke-static {v0}, Landroid/webkit/ZoomManager;->access$200(Landroid/webkit/ZoomManager;)Landroid/webkit/ZoomManager$FocusMovementQueue;

    #@10
    move-result-object v0

    #@11
    invoke-static {v0}, Landroid/webkit/ZoomManager$FocusMovementQueue;->access$300(Landroid/webkit/ZoomManager$FocusMovementQueue;)V

    #@14
    .line 826
    iget-object v0, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@16
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    #@19
    move-result v1

    #@1a
    invoke-static {v0, v1}, Landroid/webkit/ZoomManager;->access$402(Landroid/webkit/ZoomManager;F)F

    #@1d
    .line 827
    iget-object v0, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@1f
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    #@22
    move-result v1

    #@23
    invoke-static {v0, v1}, Landroid/webkit/ZoomManager;->access$502(Landroid/webkit/ZoomManager;F)F

    #@26
    .line 828
    iget-object v0, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@28
    invoke-static {v0}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@2b
    move-result-object v0

    #@2c
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mViewManager:Landroid/webkit/ViewManager;

    #@2e
    invoke-virtual {v0}, Landroid/webkit/ViewManager;->startZoom()V

    #@31
    .line 829
    iget-object v0, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@33
    invoke-static {v0}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->onPinchToZoomAnimationStart()V

    #@3a
    .line 830
    const/4 v0, 0x0

    #@3b
    iput v0, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->mAccumulatedSpan:F

    #@3d
    .line 831
    const/4 v0, 0x1

    #@3e
    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .registers 11
    .parameter "detector"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 897
    iget-object v3, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@4
    invoke-static {v3}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@7
    move-result-object v3

    #@8
    if-eqz v3, :cond_ae

    #@a
    .line 898
    iget-object v3, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@c
    invoke-static {v3}, Landroid/webkit/ZoomManager;->access$1000(Landroid/webkit/ZoomManager;)Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_9a

    #@12
    .line 899
    iget-object v3, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@14
    invoke-static {v3, v1}, Landroid/webkit/ZoomManager;->access$1002(Landroid/webkit/ZoomManager;Z)Z

    #@17
    .line 900
    iget-object v3, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@19
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@1b
    invoke-static {v4}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@1e
    move-result-object v4

    #@1f
    iget-object v5, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@21
    invoke-static {v5}, Landroid/webkit/ZoomManager;->access$1300(Landroid/webkit/ZoomManager;)F

    #@24
    move-result v5

    #@25
    float-to-int v5, v5

    #@26
    iget-object v6, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@28
    invoke-static {v6}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@2f
    move-result v6

    #@30
    add-int/2addr v5, v6

    #@31
    invoke-virtual {v4, v5}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@34
    move-result v4

    #@35
    invoke-static {v3, v4}, Landroid/webkit/ZoomManager;->access$1202(Landroid/webkit/ZoomManager;I)I

    #@38
    .line 901
    iget-object v3, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@3a
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@3c
    invoke-static {v4}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@3f
    move-result-object v4

    #@40
    iget-object v5, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@42
    invoke-static {v5}, Landroid/webkit/ZoomManager;->access$1500(Landroid/webkit/ZoomManager;)F

    #@45
    move-result v5

    #@46
    float-to-int v5, v5

    #@47
    iget-object v6, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@49
    invoke-static {v6}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@50
    move-result v6

    #@51
    add-int/2addr v5, v6

    #@52
    invoke-virtual {v4, v5}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@55
    move-result v4

    #@56
    invoke-static {v3, v4}, Landroid/webkit/ZoomManager;->access$1402(Landroid/webkit/ZoomManager;I)I

    #@59
    .line 904
    iget-object v3, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@5b
    invoke-virtual {v3}, Landroid/webkit/ZoomManager;->canZoomOut()Z

    #@5e
    move-result v3

    #@5f
    if-eqz v3, :cond_79

    #@61
    iget-object v3, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@63
    invoke-static {v3}, Landroid/webkit/ZoomManager;->access$900(Landroid/webkit/ZoomManager;)F

    #@66
    move-result v3

    #@67
    float-to-double v3, v3

    #@68
    const-wide v5, 0x3fe999999999999aL

    #@6d
    iget-object v7, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@6f
    invoke-static {v7}, Landroid/webkit/ZoomManager;->access$1600(Landroid/webkit/ZoomManager;)F

    #@72
    move-result v7

    #@73
    float-to-double v7, v7

    #@74
    mul-double/2addr v5, v7

    #@75
    cmpg-double v3, v3, v5

    #@77
    if-gtz v3, :cond_af

    #@79
    :cond_79
    move v0, v2

    #@7a
    .line 907
    .local v0, reflowNow:Z
    :goto_7a
    iget-object v3, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@7c
    if-eqz v0, :cond_b1

    #@7e
    iget-object v4, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@80
    invoke-static {v4}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@83
    move-result-object v4

    #@84
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@87
    move-result-object v4

    #@88
    invoke-virtual {v4}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@8b
    move-result v4

    #@8c
    if-nez v4, :cond_b1

    #@8e
    :goto_8e
    invoke-virtual {v3, v2}, Landroid/webkit/ZoomManager;->refreshZoomScale(Z)V

    #@91
    .line 910
    iget-object v1, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@93
    invoke-static {v1}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@96
    move-result-object v1

    #@97
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@9a
    .line 913
    .end local v0           #reflowNow:Z
    :cond_9a
    iget-object v1, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@9c
    invoke-static {v1}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@9f
    move-result-object v1

    #@a0
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mViewManager:Landroid/webkit/ViewManager;

    #@a2
    invoke-virtual {v1}, Landroid/webkit/ViewManager;->endZoom()V

    #@a5
    .line 914
    iget-object v1, p0, Landroid/webkit/ZoomManager$ScaleDetectorListener;->this$0:Landroid/webkit/ZoomManager;

    #@a7
    invoke-static {v1}, Landroid/webkit/ZoomManager;->access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;

    #@aa
    move-result-object v1

    #@ab
    invoke-virtual {v1, p1}, Landroid/webkit/WebViewClassic;->onPinchToZoomAnimationEnd(Landroid/view/ScaleGestureDetector;)V

    #@ae
    .line 916
    :cond_ae
    return-void

    #@af
    :cond_af
    move v0, v1

    #@b0
    .line 904
    goto :goto_7a

    #@b1
    .restart local v0       #reflowNow:Z
    :cond_b1
    move v2, v1

    #@b2
    .line 907
    goto :goto_8e
.end method
