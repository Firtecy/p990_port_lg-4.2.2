.class Landroid/webkit/WebCoreThreadWatchdog;
.super Ljava/lang/Object;
.source "WebCoreThreadWatchdog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;
    }
.end annotation


# static fields
.field private static final HEARTBEAT_PERIOD:I = 0x2710

.field private static final IS_ALIVE:I = 0x64

.field private static final SUBSEQUENT_TIMEOUT_PERIOD:I = 0x3a98

.field private static final TIMED_OUT:I = 0x65

.field private static final TIMEOUT_PERIOD:I = 0x7530

.field private static sInstance:Landroid/webkit/WebCoreThreadWatchdog;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mPaused:Z

.field private mWebCoreThreadHandler:Landroid/os/Handler;

.field private mWebViews:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/webkit/WebViewClassic;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/os/Handler;)V
    .registers 2
    .parameter "webCoreThreadHandler"

    #@0
    .prologue
    .line 114
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 115
    iput-object p1, p0, Landroid/webkit/WebCoreThreadWatchdog;->mWebCoreThreadHandler:Landroid/os/Handler;

    #@5
    .line 116
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/WebCoreThreadWatchdog;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-boolean v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mPaused:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/webkit/WebCoreThreadWatchdog;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/webkit/WebCoreThreadWatchdog;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mWebCoreThreadHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/webkit/WebCoreThreadWatchdog;)Ljava/util/Set;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mWebViews:Ljava/util/Set;

    #@2
    return-object v0
.end method

.method private addWebView(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "w"

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mWebViews:Ljava/util/Set;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 105
    new-instance v0, Ljava/util/HashSet;

    #@6
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@9
    iput-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mWebViews:Ljava/util/Set;

    #@b
    .line 107
    :cond_b
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mWebViews:Ljava/util/Set;

    #@d
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@10
    .line 108
    return-void
.end method

.method private createHandler()V
    .registers 3

    #@0
    .prologue
    .line 149
    const-class v1, Landroid/webkit/WebCoreThreadWatchdog;

    #@2
    monitor-enter v1

    #@3
    .line 150
    :try_start_3
    new-instance v0, Landroid/webkit/WebCoreThreadWatchdog$1;

    #@5
    invoke-direct {v0, p0}, Landroid/webkit/WebCoreThreadWatchdog$1;-><init>(Landroid/webkit/WebCoreThreadWatchdog;)V

    #@8
    iput-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@a
    .line 212
    monitor-exit v1

    #@b
    .line 213
    return-void

    #@c
    .line 212
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public static declared-synchronized pause()V
    .registers 2

    #@0
    .prologue
    .line 92
    const-class v1, Landroid/webkit/WebCoreThreadWatchdog;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 93
    sget-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@9
    invoke-direct {v0}, Landroid/webkit/WebCoreThreadWatchdog;->pauseWatchdog()V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_e

    #@c
    .line 95
    :cond_c
    monitor-exit v1

    #@d
    return-void

    #@e
    .line 92
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1

    #@10
    throw v0
.end method

.method private pauseWatchdog()V
    .registers 3

    #@0
    .prologue
    .line 119
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mPaused:Z

    #@3
    .line 121
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@5
    if-nez v0, :cond_8

    #@7
    .line 128
    :goto_7
    return-void

    #@8
    .line 125
    :cond_8
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@a
    const/16 v1, 0x65

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@f
    .line 126
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@11
    const/16 v1, 0x64

    #@13
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@16
    .line 127
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mWebCoreThreadHandler:Landroid/os/Handler;

    #@18
    const/16 v1, 0xc5

    #@1a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@1d
    goto :goto_7
.end method

.method public static declared-synchronized registerWebView(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "w"

    #@0
    .prologue
    .line 80
    const-class v1, Landroid/webkit/WebCoreThreadWatchdog;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 81
    sget-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@9
    invoke-direct {v0, p0}, Landroid/webkit/WebCoreThreadWatchdog;->addWebView(Landroid/webkit/WebViewClassic;)V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_e

    #@c
    .line 83
    :cond_c
    monitor-exit v1

    #@d
    return-void

    #@e
    .line 80
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1

    #@10
    throw v0
.end method

.method private removeWebView(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "w"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mWebViews:Ljava/util/Set;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    #@5
    .line 112
    return-void
.end method

.method public static declared-synchronized resume()V
    .registers 2

    #@0
    .prologue
    .line 98
    const-class v1, Landroid/webkit/WebCoreThreadWatchdog;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 99
    sget-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@9
    invoke-direct {v0}, Landroid/webkit/WebCoreThreadWatchdog;->resumeWatchdog()V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_e

    #@c
    .line 101
    :cond_c
    monitor-exit v1

    #@d
    return-void

    #@e
    .line 98
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1

    #@10
    throw v0
.end method

.method private resumeWatchdog()V
    .registers 5

    #@0
    .prologue
    .line 131
    iget-boolean v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mPaused:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 146
    :cond_4
    :goto_4
    return-void

    #@5
    .line 137
    :cond_5
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mPaused:Z

    #@8
    .line 139
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@a
    if-eqz v0, :cond_4

    #@c
    .line 143
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mWebCoreThreadHandler:Landroid/os/Handler;

    #@e
    const/16 v1, 0xc5

    #@10
    iget-object v2, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@12
    const/16 v3, 0x64

    #@14
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@1f
    .line 145
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@21
    iget-object v1, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@23
    const/16 v2, 0x65

    #@25
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@28
    move-result-object v1

    #@29
    const-wide/16 v2, 0x7530

    #@2b
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2e
    goto :goto_4
.end method

.method public static declared-synchronized start(Landroid/os/Handler;)Landroid/webkit/WebCoreThreadWatchdog;
    .registers 5
    .parameter "webCoreThreadHandler"

    #@0
    .prologue
    .line 72
    const-class v1, Landroid/webkit/WebCoreThreadWatchdog;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@5
    if-nez v0, :cond_1a

    #@7
    .line 73
    new-instance v0, Landroid/webkit/WebCoreThreadWatchdog;

    #@9
    invoke-direct {v0, p0}, Landroid/webkit/WebCoreThreadWatchdog;-><init>(Landroid/os/Handler;)V

    #@c
    sput-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@e
    .line 74
    new-instance v0, Ljava/lang/Thread;

    #@10
    sget-object v2, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@12
    const-string v3, "WebCoreThreadWatchdog"

    #@14
    invoke-direct {v0, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@17
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@1a
    .line 76
    :cond_1a
    sget-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1e

    #@1c
    monitor-exit v1

    #@1d
    return-object v0

    #@1e
    .line 72
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit v1

    #@20
    throw v0
.end method

.method public static declared-synchronized unregisterWebView(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "w"

    #@0
    .prologue
    .line 86
    const-class v1, Landroid/webkit/WebCoreThreadWatchdog;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 87
    sget-object v0, Landroid/webkit/WebCoreThreadWatchdog;->sInstance:Landroid/webkit/WebCoreThreadWatchdog;

    #@9
    invoke-direct {v0, p0}, Landroid/webkit/WebCoreThreadWatchdog;->removeWebView(Landroid/webkit/WebViewClassic;)V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_e

    #@c
    .line 89
    :cond_c
    monitor-exit v1

    #@d
    return-void

    #@e
    .line 86
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1

    #@10
    throw v0
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 217
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@3
    .line 219
    invoke-direct {p0}, Landroid/webkit/WebCoreThreadWatchdog;->createHandler()V

    #@6
    .line 223
    const-class v1, Landroid/webkit/WebCoreThreadWatchdog;

    #@8
    monitor-enter v1

    #@9
    .line 224
    :try_start_9
    iget-boolean v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mPaused:Z

    #@b
    if-nez v0, :cond_2f

    #@d
    .line 225
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mWebCoreThreadHandler:Landroid/os/Handler;

    #@f
    const/16 v2, 0xc5

    #@11
    iget-object v3, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@13
    const/16 v4, 0x64

    #@15
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v0, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 227
    iget-object v0, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@22
    iget-object v2, p0, Landroid/webkit/WebCoreThreadWatchdog;->mHandler:Landroid/os/Handler;

    #@24
    const/16 v3, 0x65

    #@26
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@29
    move-result-object v2

    #@2a
    const-wide/16 v3, 0x7530

    #@2c
    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2f
    .line 229
    :cond_2f
    monitor-exit v1
    :try_end_30
    .catchall {:try_start_9 .. :try_end_30} :catchall_34

    #@30
    .line 231
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@33
    .line 232
    return-void

    #@34
    .line 229
    :catchall_34
    move-exception v0

    #@35
    :try_start_35
    monitor-exit v1
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    #@36
    throw v0
.end method
