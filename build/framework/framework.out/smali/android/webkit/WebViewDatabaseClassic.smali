.class final Landroid/webkit/WebViewDatabaseClassic;
.super Landroid/webkit/WebViewDatabase;
.source "WebViewDatabaseClassic.java"


# static fields
.field private static final CACHE_DATABASE_FILE:Ljava/lang/String; = "webviewCache.db"

.field private static final DATABASE_FILE:Ljava/lang/String; = "webview.db"

.field private static final DATABASE_VERSION:I = 0xd

.field private static final FORMDATA_NAME_COL:Ljava/lang/String; = "name"

.field private static final FORMDATA_URLID_COL:Ljava/lang/String; = "urlid"

.field private static final FORMDATA_VALUE_COL:Ljava/lang/String; = "value"

.field private static final FORMURL_URL_COL:Ljava/lang/String; = "url"

.field private static final HTTPAUTH_HOST_COL:Ljava/lang/String; = "host"

.field private static final HTTPAUTH_PASSWORD_COL:Ljava/lang/String; = "password"

.field private static final HTTPAUTH_REALM_COL:Ljava/lang/String; = "realm"

.field private static final HTTPAUTH_USERNAME_COL:Ljava/lang/String; = "username"

.field private static final ID_COL:Ljava/lang/String; = "_id"

.field private static final ID_PROJECTION:[Ljava/lang/String; = null

.field private static final LOGTAG:Ljava/lang/String; = "WebViewDatabaseClassic"

.field private static final MEM_THRESHOLD:I = 0x2000

.field private static final PASSWORD_HOST_COL:Ljava/lang/String; = "host"

.field private static final PASSWORD_PASSWORD_COL:Ljava/lang/String; = "password"

.field private static final PASSWORD_PASSWORD_NAME_COL:Ljava/lang/String; = "password_name"

.field private static final PASSWORD_USERNAME_COL:Ljava/lang/String; = "username"

.field private static final PASSWORD_USERNAME_NAME_COL:Ljava/lang/String; = "username_name"

.field private static final TABLE_FORMDATA_ID:I = 0x2

.field private static final TABLE_FORMURL_ID:I = 0x1

.field private static final TABLE_HTTPAUTH_ID:I = 0x3

.field private static final TABLE_PASSWORD_ID:I

.field private static final mTableNames:[Ljava/lang/String;

.field private static sDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private static sInstance:Landroid/webkit/WebViewDatabaseClassic;

.field private static final sInstanceLock:Ljava/lang/Object;


# instance fields
.field private final mFormLock:Ljava/lang/Object;

.field private final mHttpAuthLock:Ljava/lang/Object;

.field private mInitialized:Z

.field private final mPasswordLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 62
    sput-object v1, Landroid/webkit/WebViewDatabaseClassic;->sInstance:Landroid/webkit/WebViewDatabaseClassic;

    #@5
    .line 63
    new-instance v0, Ljava/lang/Object;

    #@7
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@a
    sput-object v0, Landroid/webkit/WebViewDatabaseClassic;->sInstanceLock:Ljava/lang/Object;

    #@c
    .line 65
    sput-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@e
    .line 72
    const/4 v0, 0x5

    #@f
    new-array v0, v0, [Ljava/lang/String;

    #@11
    const-string/jumbo v1, "password"

    #@14
    aput-object v1, v0, v3

    #@16
    const-string v1, "formurl"

    #@18
    aput-object v1, v0, v4

    #@1a
    const/4 v1, 0x2

    #@1b
    const-string v2, "formdata"

    #@1d
    aput-object v2, v0, v1

    #@1f
    const/4 v1, 0x3

    #@20
    const-string v2, "httpauth"

    #@22
    aput-object v2, v0, v1

    #@24
    const/4 v1, 0x4

    #@25
    const-string/jumbo v2, "password_extend"

    #@28
    aput-object v2, v0, v1

    #@2a
    sput-object v0, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@2c
    .line 86
    new-array v0, v4, [Ljava/lang/String;

    #@2e
    const-string v1, "_id"

    #@30
    aput-object v1, v0, v3

    #@32
    sput-object v0, Landroid/webkit/WebViewDatabaseClassic;->ID_PROJECTION:[Ljava/lang/String;

    #@34
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 119
    invoke-direct {p0}, Landroid/webkit/WebViewDatabase;-><init>()V

    #@3
    .line 68
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/WebViewDatabaseClassic;->mPasswordLock:Ljava/lang/Object;

    #@a
    .line 69
    new-instance v0, Ljava/lang/Object;

    #@c
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@f
    iput-object v0, p0, Landroid/webkit/WebViewDatabaseClassic;->mFormLock:Ljava/lang/Object;

    #@11
    .line 70
    new-instance v0, Ljava/lang/Object;

    #@13
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@16
    iput-object v0, p0, Landroid/webkit/WebViewDatabaseClassic;->mHttpAuthLock:Ljava/lang/Object;

    #@18
    .line 117
    const/4 v0, 0x0

    #@19
    iput-boolean v0, p0, Landroid/webkit/WebViewDatabaseClassic;->mInitialized:Z

    #@1b
    .line 120
    invoke-static {p1}, Landroid/webkit/JniUtil;->setContext(Landroid/content/Context;)V

    #@1e
    .line 121
    new-instance v0, Landroid/webkit/WebViewDatabaseClassic$1;

    #@20
    invoke-direct {v0, p0, p1}, Landroid/webkit/WebViewDatabaseClassic$1;-><init>(Landroid/webkit/WebViewDatabaseClassic;Landroid/content/Context;)V

    #@23
    invoke-virtual {v0}, Landroid/webkit/WebViewDatabaseClassic$1;->start()V

    #@26
    .line 129
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/WebViewDatabaseClassic;Landroid/content/Context;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroid/webkit/WebViewDatabaseClassic;->init(Landroid/content/Context;)V

    #@3
    return-void
.end method

.method private checkInitialized()Z
    .registers 4

    #@0
    .prologue
    .line 332
    monitor-enter p0

    #@1
    .line 333
    :goto_1
    :try_start_1
    iget-boolean v1, p0, Landroid/webkit/WebViewDatabaseClassic;->mInitialized:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_1b

    #@3
    if-nez v1, :cond_1e

    #@5
    .line 335
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_1b
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_9

    #@8
    goto :goto_1

    #@9
    .line 336
    :catch_9
    move-exception v0

    #@a
    .line 337
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_a
    const-string v1, "WebViewDatabaseClassic"

    #@c
    const-string v2, "Caught exception while checking initialization"

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 339
    const-string v1, "WebViewDatabaseClassic"

    #@13
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    goto :goto_1

    #@1b
    .line 342
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_a .. :try_end_1d} :catchall_1b

    #@1d
    throw v1

    #@1e
    :cond_1e
    :try_start_1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1b

    #@1f
    .line 343
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@21
    if-eqz v1, :cond_25

    #@23
    const/4 v1, 0x1

    #@24
    :goto_24
    return v1

    #@25
    :cond_25
    const/4 v1, 0x0

    #@26
    goto :goto_24
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabaseClassic;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 132
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 133
    :try_start_3
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sInstance:Landroid/webkit/WebViewDatabaseClassic;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 134
    new-instance v0, Landroid/webkit/WebViewDatabaseClassic;

    #@9
    invoke-direct {v0, p0}, Landroid/webkit/WebViewDatabaseClassic;-><init>(Landroid/content/Context;)V

    #@c
    sput-object v0, Landroid/webkit/WebViewDatabaseClassic;->sInstance:Landroid/webkit/WebViewDatabaseClassic;

    #@e
    .line 136
    :cond_e
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sInstance:Landroid/webkit/WebViewDatabaseClassic;

    #@10
    monitor-exit v1

    #@11
    return-object v0

    #@12
    .line 137
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method private hasEntries(I)Z
    .registers 15
    .parameter "tableId"

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    const/4 v12, 0x0

    #@2
    .line 347
    invoke-direct {p0}, Landroid/webkit/WebViewDatabaseClassic;->checkInitialized()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_9

    #@8
    .line 362
    :goto_8
    return v12

    #@9
    .line 351
    :cond_9
    const/4 v8, 0x0

    #@a
    .line 352
    .local v8, cursor:Landroid/database/Cursor;
    const/4 v10, 0x0

    #@b
    .line 354
    .local v10, ret:Z
    :try_start_b
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@d
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@f
    aget-object v1, v1, p1

    #@11
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->ID_PROJECTION:[Ljava/lang/String;

    #@13
    const/4 v3, 0x0

    #@14
    const/4 v4, 0x0

    #@15
    const/4 v5, 0x0

    #@16
    const/4 v6, 0x0

    #@17
    const/4 v7, 0x0

    #@18
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1b
    move-result-object v8

    #@1c
    .line 356
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1f
    .catchall {:try_start_b .. :try_end_1f} :catchall_37
    .catch Ljava/lang/IllegalStateException; {:try_start_b .. :try_end_1f} :catch_2c

    #@1f
    move-result v0

    #@20
    if-ne v0, v11, :cond_2a

    #@22
    move v10, v11

    #@23
    .line 360
    :goto_23
    if-eqz v8, :cond_28

    #@25
    :goto_25
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@28
    :cond_28
    move v12, v10

    #@29
    .line 362
    goto :goto_8

    #@2a
    :cond_2a
    move v10, v12

    #@2b
    .line 356
    goto :goto_23

    #@2c
    .line 357
    :catch_2c
    move-exception v9

    #@2d
    .line 358
    .local v9, e:Ljava/lang/IllegalStateException;
    :try_start_2d
    const-string v0, "WebViewDatabaseClassic"

    #@2f
    const-string v1, "hasEntries"

    #@31
    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_34
    .catchall {:try_start_2d .. :try_end_34} :catchall_37

    #@34
    .line 360
    if-eqz v8, :cond_28

    #@36
    goto :goto_25

    #@37
    .end local v9           #e:Ljava/lang/IllegalStateException;
    :catchall_37
    move-exception v0

    #@38
    if-eqz v8, :cond_3d

    #@3a
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@3d
    :cond_3d
    throw v0
.end method

.method private declared-synchronized init(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 141
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebViewDatabaseClassic;->mInitialized:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_17

    #@3
    if-eqz v0, :cond_7

    #@5
    .line 153
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 145
    :cond_7
    :try_start_7
    invoke-direct {p0, p1}, Landroid/webkit/WebViewDatabaseClassic;->initDatabase(Landroid/content/Context;)V

    #@a
    .line 148
    const-string/jumbo v0, "webviewCache.db"

    #@d
    invoke-virtual {p1, v0}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    #@10
    .line 151
    const/4 v0, 0x1

    #@11
    iput-boolean v0, p0, Landroid/webkit/WebViewDatabaseClassic;->mInitialized:Z

    #@13
    .line 152
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_16
    .catchall {:try_start_7 .. :try_end_16} :catchall_17

    #@16
    goto :goto_5

    #@17
    .line 141
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method private initDatabase(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 157
    :try_start_3
    const-string/jumbo v1, "webview.db"

    #@6
    const/4 v2, 0x0

    #@7
    const/4 v3, 0x0

    #@8
    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    #@b
    move-result-object v1

    #@c
    sput-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_e
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_e} :catch_18

    #@e
    .line 178
    :cond_e
    :goto_e
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@10
    if-nez v1, :cond_48

    #@12
    .line 179
    iput-boolean v5, p0, Landroid/webkit/WebViewDatabaseClassic;->mInitialized:Z

    #@14
    .line 180
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    #@17
    .line 193
    :cond_17
    :goto_17
    return-void

    #@18
    .line 158
    :catch_18
    move-exception v0

    #@19
    .line 160
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    invoke-direct {p0, v0}, Landroid/webkit/WebViewDatabaseClassic;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_35

    #@1f
    .line 161
    const-string v1, "WebViewDatabaseClassic"

    #@21
    const-string v2, "Caught memory full exception in initDatabase()"

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 162
    const-string v1, "WebViewDatabaseClassic"

    #@28
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 163
    iput-boolean v5, p0, Landroid/webkit/WebViewDatabaseClassic;->mInitialized:Z

    #@31
    .line 164
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    #@34
    goto :goto_17

    #@35
    .line 170
    :cond_35
    const-string/jumbo v1, "webview.db"

    #@38
    invoke-virtual {p1, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_e

    #@3e
    .line 171
    const-string/jumbo v1, "webview.db"

    #@41
    invoke-virtual {p1, v1, v4, v6}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    #@44
    move-result-object v1

    #@45
    sput-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@47
    goto :goto_e

    #@48
    .line 184
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :cond_48
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@4a
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    #@4d
    move-result v1

    #@4e
    const/16 v2, 0xd

    #@50
    if-eq v1, v2, :cond_17

    #@52
    .line 185
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@54
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    #@57
    .line 187
    :try_start_57
    invoke-static {}, Landroid/webkit/WebViewDatabaseClassic;->upgradeDatabase()V

    #@5a
    .line 188
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@5c
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5f
    .catchall {:try_start_57 .. :try_end_5f} :catchall_65

    #@5f
    .line 190
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@61
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@64
    goto :goto_17

    #@65
    :catchall_65
    move-exception v1

    #@66
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@68
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@6b
    throw v1
.end method

.method private isLowMemory(Landroid/database/sqlite/SQLiteException;)Z
    .registers 12
    .parameter "e"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 795
    instance-of v6, p1, Landroid/database/sqlite/SQLiteFullException;

    #@3
    if-eqz v6, :cond_6

    #@5
    .line 804
    :cond_5
    :goto_5
    return v5

    #@6
    .line 797
    :cond_6
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    #@9
    move-result-object v6

    #@a
    const-string/jumbo v7, "no transaction is active"

    #@d
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_2c

    #@13
    .line 798
    new-instance v4, Landroid/os/StatFs;

    #@15
    const-string v6, "/data"

    #@17
    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    #@1a
    .line 799
    .local v4, stat:Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    #@1d
    move-result v6

    #@1e
    int-to-long v0, v6

    #@1f
    .line 800
    .local v0, availBlocks:J
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    #@22
    move-result v6

    #@23
    int-to-long v2, v6

    #@24
    .line 801
    .local v2, blockSize:J
    mul-long v6, v0, v2

    #@26
    const-wide/16 v8, 0x2000

    #@28
    cmp-long v6, v6, v8

    #@2a
    if-ltz v6, :cond_5

    #@2c
    .line 804
    .end local v0           #availBlocks:J
    .end local v2           #blockSize:J
    .end local v4           #stat:Landroid/os/StatFs;
    :cond_2c
    const/4 v5, 0x0

    #@2d
    goto :goto_5
.end method

.method private static upgradeDatabase()V
    .registers 2

    #@0
    .prologue
    .line 196
    invoke-static {}, Landroid/webkit/WebViewDatabaseClassic;->upgradeDatabaseToV10()V

    #@3
    .line 197
    invoke-static {}, Landroid/webkit/WebViewDatabaseClassic;->upgradeDatabaseFromV10ToV11()V

    #@6
    .line 198
    invoke-static {}, Landroid/webkit/WebViewDatabaseClassic;->upgradeDatabaseFromV11ToV12()V

    #@9
    .line 199
    invoke-static {}, Landroid/webkit/WebViewDatabaseClassic;->upgradeDatabaseFromV12ToV13()V

    #@c
    .line 202
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@e
    const/16 v1, 0xd

    #@10
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->setVersion(I)V

    #@13
    .line 203
    return-void
.end method

.method private static upgradeDatabaseFromV10ToV11()V
    .registers 14

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 233
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    #@7
    move-result v10

    #@8
    .line 235
    .local v10, oldVersion:I
    const/16 v0, 0xb

    #@a
    if-lt v10, v0, :cond_d

    #@c
    .line 259
    :goto_c
    return-void

    #@d
    .line 242
    :cond_d
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@f
    const-string v1, "DROP TABLE IF EXISTS cookies"

    #@11
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@14
    .line 245
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@16
    const-string v1, "DROP TABLE IF EXISTS cache"

    #@18
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@1b
    .line 248
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@1d
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@1f
    aget-object v1, v1, v13

    #@21
    move-object v3, v2

    #@22
    move-object v4, v2

    #@23
    move-object v5, v2

    #@24
    move-object v6, v2

    #@25
    move-object v7, v2

    #@26
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@29
    move-result-object v8

    #@2a
    .line 250
    .local v8, c:Landroid/database/Cursor;
    :goto_2a
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_69

    #@30
    .line 251
    const-string v0, "_id"

    #@32
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@35
    move-result v0

    #@36
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@39
    move-result-wide v0

    #@3a
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@3d
    move-result-object v12

    #@3e
    .line 252
    .local v12, urlId:Ljava/lang/String;
    const-string/jumbo v0, "url"

    #@41
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@44
    move-result v0

    #@45
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@48
    move-result-object v11

    #@49
    .line 253
    .local v11, url:Ljava/lang/String;
    new-instance v9, Landroid/content/ContentValues;

    #@4b
    invoke-direct {v9, v13}, Landroid/content/ContentValues;-><init>(I)V

    #@4e
    .line 254
    .local v9, cv:Landroid/content/ContentValues;
    const-string/jumbo v0, "url"

    #@51
    invoke-static {v11}, Landroid/webkit/WebTextView;->urlForAutoCompleteData(Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@58
    .line 255
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@5a
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@5c
    aget-object v1, v1, v13

    #@5e
    const-string v2, "_id=?"

    #@60
    new-array v3, v13, [Ljava/lang/String;

    #@62
    const/4 v4, 0x0

    #@63
    aput-object v12, v3, v4

    #@65
    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@68
    goto :goto_2a

    #@69
    .line 258
    .end local v9           #cv:Landroid/content/ContentValues;
    .end local v11           #url:Ljava/lang/String;
    .end local v12           #urlId:Ljava/lang/String;
    :cond_69
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@6c
    goto :goto_c
.end method

.method private static upgradeDatabaseFromV11ToV12()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 219
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    #@6
    move-result v0

    #@7
    const/16 v1, 0xc

    #@9
    if-lt v0, v1, :cond_c

    #@b
    .line 230
    :goto_b
    return-void

    #@c
    .line 221
    :cond_c
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "DROP TABLE IF EXISTS "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@1b
    aget-object v2, v2, v3

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@28
    .line 222
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@2a
    new-instance v1, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v2, "CREATE TABLE "

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@37
    aget-object v2, v2, v3

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, " ("

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    const-string v2, "_id"

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    const-string v2, " INTEGER PRIMARY KEY, "

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    const-string v2, "host"

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    const-string v2, " TEXT, "

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    const-string/jumbo v2, "username"

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v1

    #@62
    const-string v2, " TEXT, "

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v1

    #@68
    const-string/jumbo v2, "password"

    #@6b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    const-string v2, " TEXT, "

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    const-string/jumbo v2, "username_name"

    #@78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v1

    #@7c
    const-string v2, " TEXT, "

    #@7e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v1

    #@82
    const-string/jumbo v2, "password_name"

    #@85
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v1

    #@89
    const-string v2, " TEXT,"

    #@8b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v1

    #@8f
    const-string v2, " UNIQUE ("

    #@91
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    const-string v2, "host"

    #@97
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v1

    #@9b
    const-string v2, ", "

    #@9d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v1

    #@a1
    const-string/jumbo v2, "username"

    #@a4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v1

    #@a8
    const-string v2, ") ON CONFLICT REPLACE);"

    #@aa
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v1

    #@ae
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v1

    #@b2
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@b5
    goto/16 :goto_b
.end method

.method private static upgradeDatabaseFromV12ToV13()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 207
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    #@6
    move-result v0

    #@7
    .line 209
    .local v0, oldVersion:I
    const/16 v1, 0xd

    #@9
    if-lt v0, v1, :cond_c

    #@b
    .line 215
    :goto_b
    return-void

    #@c
    .line 214
    :cond_c
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@e
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@10
    const/4 v3, 0x0

    #@11
    aget-object v2, v2, v3

    #@13
    invoke-virtual {v1, v2, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@16
    goto :goto_b
.end method

.method private static upgradeDatabaseToV10()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v4, 0x3

    #@4
    .line 262
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@6
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    #@9
    move-result v0

    #@a
    .line 264
    .local v0, oldVersion:I
    const/16 v1, 0xa

    #@c
    if-lt v0, v1, :cond_f

    #@e
    .line 327
    :goto_e
    return-void

    #@f
    .line 269
    :cond_f
    if-eqz v0, :cond_3b

    #@11
    .line 270
    const-string v1, "WebViewDatabaseClassic"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "Upgrading database from version "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, " to "

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    const/16 v3, 0xd

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    const-string v3, ", which will destroy old data"

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 275
    :cond_3b
    const/16 v1, 0x9

    #@3d
    if-ne v1, v0, :cond_dd

    #@3f
    .line 276
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@41
    new-instance v2, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v3, "DROP TABLE IF EXISTS "

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@4e
    aget-object v3, v3, v4

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@5b
    .line 278
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@5d
    new-instance v2, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v3, "CREATE TABLE "

    #@64
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v2

    #@68
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@6a
    aget-object v3, v3, v4

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v2

    #@70
    const-string v3, " ("

    #@72
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v2

    #@76
    const-string v3, "_id"

    #@78
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v2

    #@7c
    const-string v3, " INTEGER PRIMARY KEY, "

    #@7e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v2

    #@82
    const-string v3, "host"

    #@84
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v2

    #@88
    const-string v3, " TEXT, "

    #@8a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v2

    #@8e
    const-string/jumbo v3, "realm"

    #@91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v2

    #@95
    const-string v3, " TEXT, "

    #@97
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v2

    #@9b
    const-string/jumbo v3, "username"

    #@9e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v2

    #@a2
    const-string v3, " TEXT, "

    #@a4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v2

    #@a8
    const-string/jumbo v3, "password"

    #@ab
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v2

    #@af
    const-string v3, " TEXT,"

    #@b1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v2

    #@b5
    const-string v3, " UNIQUE ("

    #@b7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v2

    #@bb
    const-string v3, "host"

    #@bd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v2

    #@c1
    const-string v3, ", "

    #@c3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v2

    #@c7
    const-string/jumbo v3, "realm"

    #@ca
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v2

    #@ce
    const-string v3, ") ON CONFLICT REPLACE);"

    #@d0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v2

    #@d4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v2

    #@d8
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@db
    goto/16 :goto_e

    #@dd
    .line 288
    :cond_dd
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@df
    const-string v2, "DROP TABLE IF EXISTS cookies"

    #@e1
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@e4
    .line 289
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@e6
    const-string v2, "DROP TABLE IF EXISTS cache"

    #@e8
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@eb
    .line 290
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@ed
    new-instance v2, Ljava/lang/StringBuilder;

    #@ef
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f2
    const-string v3, "DROP TABLE IF EXISTS "

    #@f4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v2

    #@f8
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@fa
    aget-object v3, v3, v6

    #@fc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v2

    #@100
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v2

    #@104
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@107
    .line 292
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@109
    new-instance v2, Ljava/lang/StringBuilder;

    #@10b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10e
    const-string v3, "DROP TABLE IF EXISTS "

    #@110
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v2

    #@114
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@116
    aget-object v3, v3, v7

    #@118
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v2

    #@11c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v2

    #@120
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@123
    .line 294
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@125
    new-instance v2, Ljava/lang/StringBuilder;

    #@127
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12a
    const-string v3, "DROP TABLE IF EXISTS "

    #@12c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v2

    #@130
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@132
    aget-object v3, v3, v4

    #@134
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v2

    #@138
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13b
    move-result-object v2

    #@13c
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@13f
    .line 296
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@141
    new-instance v2, Ljava/lang/StringBuilder;

    #@143
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@146
    const-string v3, "DROP TABLE IF EXISTS "

    #@148
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v2

    #@14c
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@14e
    aget-object v3, v3, v5

    #@150
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v2

    #@154
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@157
    move-result-object v2

    #@158
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@15b
    .line 300
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@15d
    new-instance v2, Ljava/lang/StringBuilder;

    #@15f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@162
    const-string v3, "CREATE TABLE "

    #@164
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v2

    #@168
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@16a
    aget-object v3, v3, v6

    #@16c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v2

    #@170
    const-string v3, " ("

    #@172
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@175
    move-result-object v2

    #@176
    const-string v3, "_id"

    #@178
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v2

    #@17c
    const-string v3, " INTEGER PRIMARY KEY, "

    #@17e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v2

    #@182
    const-string/jumbo v3, "url"

    #@185
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v2

    #@189
    const-string v3, " TEXT"

    #@18b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v2

    #@18f
    const-string v3, ");"

    #@191
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v2

    #@195
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v2

    #@199
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@19c
    .line 305
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@19e
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a3
    const-string v3, "CREATE TABLE "

    #@1a5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v2

    #@1a9
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@1ab
    aget-object v3, v3, v7

    #@1ad
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v2

    #@1b1
    const-string v3, " ("

    #@1b3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b6
    move-result-object v2

    #@1b7
    const-string v3, "_id"

    #@1b9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v2

    #@1bd
    const-string v3, " INTEGER PRIMARY KEY, "

    #@1bf
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v2

    #@1c3
    const-string/jumbo v3, "urlid"

    #@1c6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v2

    #@1ca
    const-string v3, " INTEGER, "

    #@1cc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cf
    move-result-object v2

    #@1d0
    const-string/jumbo v3, "name"

    #@1d3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v2

    #@1d7
    const-string v3, " TEXT, "

    #@1d9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v2

    #@1dd
    const-string/jumbo v3, "value"

    #@1e0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v2

    #@1e4
    const-string v3, " TEXT,"

    #@1e6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v2

    #@1ea
    const-string v3, " UNIQUE ("

    #@1ec
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v2

    #@1f0
    const-string/jumbo v3, "urlid"

    #@1f3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v2

    #@1f7
    const-string v3, ", "

    #@1f9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v2

    #@1fd
    const-string/jumbo v3, "name"

    #@200
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@203
    move-result-object v2

    #@204
    const-string v3, ", "

    #@206
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v2

    #@20a
    const-string/jumbo v3, "value"

    #@20d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v2

    #@211
    const-string v3, ") ON CONFLICT IGNORE);"

    #@213
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@216
    move-result-object v2

    #@217
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21a
    move-result-object v2

    #@21b
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@21e
    .line 313
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@220
    new-instance v2, Ljava/lang/StringBuilder;

    #@222
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@225
    const-string v3, "CREATE TABLE "

    #@227
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v2

    #@22b
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@22d
    aget-object v3, v3, v4

    #@22f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@232
    move-result-object v2

    #@233
    const-string v3, " ("

    #@235
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@238
    move-result-object v2

    #@239
    const-string v3, "_id"

    #@23b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23e
    move-result-object v2

    #@23f
    const-string v3, " INTEGER PRIMARY KEY, "

    #@241
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@244
    move-result-object v2

    #@245
    const-string v3, "host"

    #@247
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24a
    move-result-object v2

    #@24b
    const-string v3, " TEXT, "

    #@24d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v2

    #@251
    const-string/jumbo v3, "realm"

    #@254
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@257
    move-result-object v2

    #@258
    const-string v3, " TEXT, "

    #@25a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25d
    move-result-object v2

    #@25e
    const-string/jumbo v3, "username"

    #@261
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@264
    move-result-object v2

    #@265
    const-string v3, " TEXT, "

    #@267
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26a
    move-result-object v2

    #@26b
    const-string/jumbo v3, "password"

    #@26e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@271
    move-result-object v2

    #@272
    const-string v3, " TEXT,"

    #@274
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@277
    move-result-object v2

    #@278
    const-string v3, " UNIQUE ("

    #@27a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27d
    move-result-object v2

    #@27e
    const-string v3, "host"

    #@280
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@283
    move-result-object v2

    #@284
    const-string v3, ", "

    #@286
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@289
    move-result-object v2

    #@28a
    const-string/jumbo v3, "realm"

    #@28d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@290
    move-result-object v2

    #@291
    const-string v3, ") ON CONFLICT REPLACE);"

    #@293
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@296
    move-result-object v2

    #@297
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29a
    move-result-object v2

    #@29b
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@29e
    .line 321
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@2a0
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a5
    const-string v3, "CREATE TABLE "

    #@2a7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2aa
    move-result-object v2

    #@2ab
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@2ad
    aget-object v3, v3, v5

    #@2af
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b2
    move-result-object v2

    #@2b3
    const-string v3, " ("

    #@2b5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b8
    move-result-object v2

    #@2b9
    const-string v3, "_id"

    #@2bb
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2be
    move-result-object v2

    #@2bf
    const-string v3, " INTEGER PRIMARY KEY, "

    #@2c1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c4
    move-result-object v2

    #@2c5
    const-string v3, "host"

    #@2c7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ca
    move-result-object v2

    #@2cb
    const-string v3, " TEXT, "

    #@2cd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d0
    move-result-object v2

    #@2d1
    const-string/jumbo v3, "username"

    #@2d4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d7
    move-result-object v2

    #@2d8
    const-string v3, " TEXT, "

    #@2da
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2dd
    move-result-object v2

    #@2de
    const-string/jumbo v3, "password"

    #@2e1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e4
    move-result-object v2

    #@2e5
    const-string v3, " TEXT,"

    #@2e7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ea
    move-result-object v2

    #@2eb
    const-string v3, " UNIQUE ("

    #@2ed
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f0
    move-result-object v2

    #@2f1
    const-string v3, "host"

    #@2f3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f6
    move-result-object v2

    #@2f7
    const-string v3, ", "

    #@2f9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fc
    move-result-object v2

    #@2fd
    const-string/jumbo v3, "username"

    #@300
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@303
    move-result-object v2

    #@304
    const-string v3, ") ON CONFLICT REPLACE);"

    #@306
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@309
    move-result-object v2

    #@30a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30d
    move-result-object v2

    #@30e
    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@311
    goto/16 :goto_e
.end method


# virtual methods
.method public clearFormData()V
    .registers 7

    #@0
    .prologue
    .line 770
    invoke-direct {p0}, Landroid/webkit/WebViewDatabaseClassic;->checkInitialized()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 787
    :goto_6
    return-void

    #@7
    .line 774
    :cond_7
    iget-object v2, p0, Landroid/webkit/WebViewDatabaseClassic;->mFormLock:Ljava/lang/Object;

    #@9
    monitor-enter v2

    #@a
    .line 777
    :try_start_a
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@c
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@e
    const/4 v4, 0x1

    #@f
    aget-object v3, v3, v4

    #@11
    const/4 v4, 0x0

    #@12
    const/4 v5, 0x0

    #@13
    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@16
    .line 778
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@18
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@1a
    const/4 v4, 0x2

    #@1b
    aget-object v3, v3, v4

    #@1d
    const/4 v4, 0x0

    #@1e
    const/4 v5, 0x0

    #@1f
    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_22
    .catchall {:try_start_a .. :try_end_22} :catchall_24
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_22} :catch_27

    #@22
    .line 786
    :cond_22
    :goto_22
    :try_start_22
    monitor-exit v2

    #@23
    goto :goto_6

    #@24
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_22 .. :try_end_26} :catchall_24

    #@26
    throw v1

    #@27
    .line 779
    :catch_27
    move-exception v0

    #@28
    .line 780
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    :try_start_28
    invoke-direct {p0, v0}, Landroid/webkit/WebViewDatabaseClassic;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_22

    #@2e
    .line 781
    const-string v1, "WebViewDatabaseClassic"

    #@30
    const-string v3, "Caught memory full exception in clearFormData()"

    #@32
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 782
    const-string v1, "WebViewDatabaseClassic"

    #@37
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3e
    .catchall {:try_start_28 .. :try_end_3e} :catchall_24

    #@3e
    goto :goto_22
.end method

.method public clearHttpAuthUsernamePassword()V
    .registers 7

    #@0
    .prologue
    .line 612
    invoke-direct {p0}, Landroid/webkit/WebViewDatabaseClassic;->checkInitialized()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 628
    :goto_6
    return-void

    #@7
    .line 616
    :cond_7
    iget-object v2, p0, Landroid/webkit/WebViewDatabaseClassic;->mHttpAuthLock:Ljava/lang/Object;

    #@9
    monitor-enter v2

    #@a
    .line 619
    :try_start_a
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@c
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@e
    const/4 v4, 0x3

    #@f
    aget-object v3, v3, v4

    #@11
    const/4 v4, 0x0

    #@12
    const/4 v5, 0x0

    #@13
    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_16
    .catchall {:try_start_a .. :try_end_16} :catchall_18
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_16} :catch_1b

    #@16
    .line 627
    :cond_16
    :goto_16
    :try_start_16
    monitor-exit v2

    #@17
    goto :goto_6

    #@18
    :catchall_18
    move-exception v1

    #@19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_16 .. :try_end_1a} :catchall_18

    #@1a
    throw v1

    #@1b
    .line 620
    :catch_1b
    move-exception v0

    #@1c
    .line 621
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    :try_start_1c
    invoke-direct {p0, v0}, Landroid/webkit/WebViewDatabaseClassic;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_16

    #@22
    .line 622
    const-string v1, "WebViewDatabaseClassic"

    #@24
    const-string v3, "Caught memory full exception in clearHttpAuthUsernamePassword()"

    #@26
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 623
    const-string v1, "WebViewDatabaseClassic"

    #@2b
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_32
    .catchall {:try_start_1c .. :try_end_32} :catchall_18

    #@32
    goto :goto_16
.end method

.method public clearUsernamePassword()V
    .registers 7

    #@0
    .prologue
    .line 497
    invoke-direct {p0}, Landroid/webkit/WebViewDatabaseClassic;->checkInitialized()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 513
    :goto_6
    return-void

    #@7
    .line 501
    :cond_7
    iget-object v2, p0, Landroid/webkit/WebViewDatabaseClassic;->mPasswordLock:Ljava/lang/Object;

    #@9
    monitor-enter v2

    #@a
    .line 504
    :try_start_a
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@c
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@e
    const/4 v4, 0x0

    #@f
    aget-object v3, v3, v4

    #@11
    const/4 v4, 0x0

    #@12
    const/4 v5, 0x0

    #@13
    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_16
    .catchall {:try_start_a .. :try_end_16} :catchall_18
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_16} :catch_1b

    #@16
    .line 512
    :cond_16
    :goto_16
    :try_start_16
    monitor-exit v2

    #@17
    goto :goto_6

    #@18
    :catchall_18
    move-exception v1

    #@19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_16 .. :try_end_1a} :catchall_18

    #@1a
    throw v1

    #@1b
    .line 505
    :catch_1b
    move-exception v0

    #@1c
    .line 506
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    :try_start_1c
    invoke-direct {p0, v0}, Landroid/webkit/WebViewDatabaseClassic;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_16

    #@22
    .line 507
    const-string v1, "WebViewDatabaseClassic"

    #@24
    const-string v3, "Caught memory full exception in clearUsernamePassword()"

    #@26
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 508
    const-string v1, "WebViewDatabaseClassic"

    #@2b
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_32
    .catchall {:try_start_1c .. :try_end_32} :catchall_18

    #@32
    goto :goto_16
.end method

.method getFormData(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 22
    .parameter "url"
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 709
    new-instance v17, Ljava/util/ArrayList;

    #@2
    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 710
    .local v17, values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_f

    #@7
    if-eqz p2, :cond_f

    #@9
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewDatabaseClassic;->checkInitialized()Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_10

    #@f
    .line 751
    :cond_f
    :goto_f
    return-object v17

    #@10
    .line 714
    :cond_10
    const-string v13, "(url == ?)"

    #@12
    .line 715
    .local v13, urlSelection:Ljava/lang/String;
    const-string v11, "(urlid == ?) AND (name == ?)"

    #@14
    .line 717
    .local v11, dataSelection:Ljava/lang/String;
    move-object/from16 v0, p0

    #@16
    iget-object v0, v0, Landroid/webkit/WebViewDatabaseClassic;->mFormLock:Ljava/lang/Object;

    #@18
    move-object/from16 v18, v0

    #@1a
    monitor-enter v18

    #@1b
    .line 718
    const/4 v9, 0x0

    #@1c
    .line 720
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_1c
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@1e
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@20
    const/4 v3, 0x1

    #@21
    aget-object v2, v2, v3

    #@23
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->ID_PROJECTION:[Ljava/lang/String;

    #@25
    const-string v4, "(url == ?)"

    #@27
    const/4 v5, 0x1

    #@28
    new-array v5, v5, [Ljava/lang/String;

    #@2a
    const/4 v6, 0x0

    #@2b
    aput-object p1, v5, v6

    #@2d
    const/4 v6, 0x0

    #@2e
    const/4 v7, 0x0

    #@2f
    const/4 v8, 0x0

    #@30
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@33
    move-result-object v9

    #@34
    .line 723
    :cond_34
    :goto_34
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@37
    move-result v1

    #@38
    if-eqz v1, :cond_c3

    #@3a
    .line 724
    const-string v1, "_id"

    #@3c
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3f
    move-result v1

    #@40
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_43
    .catchall {:try_start_1c .. :try_end_43} :catchall_b5
    .catch Ljava/lang/IllegalStateException; {:try_start_1c .. :try_end_43} :catch_94

    #@43
    move-result-wide v14

    #@44
    .line 725
    .local v14, urlid:J
    const/4 v10, 0x0

    #@45
    .line 727
    .local v10, dataCursor:Landroid/database/Cursor;
    :try_start_45
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@47
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@49
    const/4 v3, 0x2

    #@4a
    aget-object v2, v2, v3

    #@4c
    const/4 v3, 0x2

    #@4d
    new-array v3, v3, [Ljava/lang/String;

    #@4f
    const/4 v4, 0x0

    #@50
    const-string v5, "_id"

    #@52
    aput-object v5, v3, v4

    #@54
    const/4 v4, 0x1

    #@55
    const-string/jumbo v5, "value"

    #@58
    aput-object v5, v3, v4

    #@5a
    const-string v4, "(urlid == ?) AND (name == ?)"

    #@5c
    const/4 v5, 0x2

    #@5d
    new-array v5, v5, [Ljava/lang/String;

    #@5f
    const/4 v6, 0x0

    #@60
    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@63
    move-result-object v7

    #@64
    aput-object v7, v5, v6

    #@66
    const/4 v6, 0x1

    #@67
    aput-object p2, v5, v6

    #@69
    const/4 v6, 0x0

    #@6a
    const/4 v7, 0x0

    #@6b
    const/4 v8, 0x0

    #@6c
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@6f
    move-result-object v10

    #@70
    .line 733
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    #@73
    move-result v1

    #@74
    if-eqz v1, :cond_8e

    #@76
    .line 734
    const-string/jumbo v1, "value"

    #@79
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@7c
    move-result v16

    #@7d
    .line 737
    .local v16, valueCol:I
    :cond_7d
    move/from16 v0, v16

    #@7f
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    move-object/from16 v0, v17

    #@85
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@88
    .line 738
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_8b
    .catchall {:try_start_45 .. :try_end_8b} :catchall_bc
    .catch Ljava/lang/IllegalStateException; {:try_start_45 .. :try_end_8b} :catch_a7

    #@8b
    move-result v1

    #@8c
    if-nez v1, :cond_7d

    #@8e
    .line 743
    .end local v16           #valueCol:I
    :cond_8e
    if-eqz v10, :cond_34

    #@90
    :try_start_90
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_93
    .catchall {:try_start_90 .. :try_end_93} :catchall_b5
    .catch Ljava/lang/IllegalStateException; {:try_start_90 .. :try_end_93} :catch_94

    #@93
    goto :goto_34

    #@94
    .line 746
    .end local v10           #dataCursor:Landroid/database/Cursor;
    .end local v14           #urlid:J
    :catch_94
    move-exception v12

    #@95
    .line 747
    .local v12, e:Ljava/lang/IllegalStateException;
    :try_start_95
    const-string v1, "WebViewDatabaseClassic"

    #@97
    const-string v2, "getFormData cursor"

    #@99
    invoke-static {v1, v2, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9c
    .catchall {:try_start_95 .. :try_end_9c} :catchall_b5

    #@9c
    .line 749
    if-eqz v9, :cond_a1

    #@9e
    :try_start_9e
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@a1
    .line 751
    .end local v12           #e:Ljava/lang/IllegalStateException;
    :cond_a1
    :goto_a1
    monitor-exit v18

    #@a2
    goto/16 :goto_f

    #@a4
    .line 752
    :catchall_a4
    move-exception v1

    #@a5
    monitor-exit v18
    :try_end_a6
    .catchall {:try_start_9e .. :try_end_a6} :catchall_a4

    #@a6
    throw v1

    #@a7
    .line 740
    .restart local v10       #dataCursor:Landroid/database/Cursor;
    .restart local v14       #urlid:J
    :catch_a7
    move-exception v12

    #@a8
    .line 741
    .restart local v12       #e:Ljava/lang/IllegalStateException;
    :try_start_a8
    const-string v1, "WebViewDatabaseClassic"

    #@aa
    const-string v2, "getFormData dataCursor"

    #@ac
    invoke-static {v1, v2, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_af
    .catchall {:try_start_a8 .. :try_end_af} :catchall_bc

    #@af
    .line 743
    if-eqz v10, :cond_34

    #@b1
    :try_start_b1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_b4
    .catchall {:try_start_b1 .. :try_end_b4} :catchall_b5
    .catch Ljava/lang/IllegalStateException; {:try_start_b1 .. :try_end_b4} :catch_94

    #@b4
    goto :goto_34

    #@b5
    .line 749
    .end local v10           #dataCursor:Landroid/database/Cursor;
    .end local v12           #e:Ljava/lang/IllegalStateException;
    .end local v14           #urlid:J
    :catchall_b5
    move-exception v1

    #@b6
    if-eqz v9, :cond_bb

    #@b8
    :try_start_b8
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@bb
    :cond_bb
    throw v1
    :try_end_bc
    .catchall {:try_start_b8 .. :try_end_bc} :catchall_a4

    #@bc
    .line 743
    .restart local v10       #dataCursor:Landroid/database/Cursor;
    .restart local v14       #urlid:J
    :catchall_bc
    move-exception v1

    #@bd
    if-eqz v10, :cond_c2

    #@bf
    :try_start_bf
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@c2
    :cond_c2
    throw v1
    :try_end_c3
    .catchall {:try_start_bf .. :try_end_c3} :catchall_b5
    .catch Ljava/lang/IllegalStateException; {:try_start_bf .. :try_end_c3} :catch_94

    #@c3
    .line 749
    .end local v10           #dataCursor:Landroid/database/Cursor;
    .end local v14           #urlid:J
    :cond_c3
    if-eqz v9, :cond_a1

    #@c5
    :try_start_c5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_c8
    .catchall {:try_start_c5 .. :try_end_c8} :catchall_a4

    #@c8
    goto :goto_a1
.end method

.method getHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 16
    .parameter "host"
    .parameter "realm"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v10, 0x0

    #@4
    .line 565
    if-eqz p1, :cond_e

    #@6
    if-eqz p2, :cond_e

    #@8
    invoke-direct {p0}, Landroid/webkit/WebViewDatabaseClassic;->checkInitialized()Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_f

    #@e
    .line 593
    :cond_e
    :goto_e
    return-object v10

    #@f
    .line 569
    :cond_f
    new-array v2, v4, [Ljava/lang/String;

    #@11
    const-string/jumbo v0, "username"

    #@14
    aput-object v0, v2, v1

    #@16
    const-string/jumbo v0, "password"

    #@19
    aput-object v0, v2, v3

    #@1b
    .line 572
    .local v2, columns:[Ljava/lang/String;
    const-string v11, "(host == ?) AND (realm == ?)"

    #@1d
    .line 574
    .local v11, selection:Ljava/lang/String;
    iget-object v12, p0, Landroid/webkit/WebViewDatabaseClassic;->mHttpAuthLock:Ljava/lang/Object;

    #@1f
    monitor-enter v12

    #@20
    .line 575
    const/4 v10, 0x0

    #@21
    .line 576
    .local v10, ret:[Ljava/lang/String;
    const/4 v8, 0x0

    #@22
    .line 578
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_22
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@24
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@26
    const/4 v3, 0x3

    #@27
    aget-object v1, v1, v3

    #@29
    const-string v3, "(host == ?) AND (realm == ?)"

    #@2b
    const/4 v4, 0x2

    #@2c
    new-array v4, v4, [Ljava/lang/String;

    #@2e
    const/4 v5, 0x0

    #@2f
    aput-object p1, v4, v5

    #@31
    const/4 v5, 0x1

    #@32
    aput-object p2, v4, v5

    #@34
    const/4 v5, 0x0

    #@35
    const/4 v6, 0x0

    #@36
    const/4 v7, 0x0

    #@37
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3a
    move-result-object v8

    #@3b
    .line 581
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@3e
    move-result v0

    #@3f
    if-eqz v0, :cond_60

    #@41
    .line 582
    const/4 v0, 0x2

    #@42
    new-array v10, v0, [Ljava/lang/String;

    #@44
    .line 583
    const/4 v0, 0x0

    #@45
    const-string/jumbo v1, "username"

    #@48
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@4b
    move-result v1

    #@4c
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4f
    move-result-object v1

    #@50
    aput-object v1, v10, v0

    #@52
    .line 585
    const/4 v0, 0x1

    #@53
    const-string/jumbo v1, "password"

    #@56
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@59
    move-result v1

    #@5a
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    aput-object v1, v10, v0
    :try_end_60
    .catchall {:try_start_22 .. :try_end_60} :catchall_78
    .catch Ljava/lang/IllegalStateException; {:try_start_22 .. :try_end_60} :catch_6a

    #@60
    .line 591
    :cond_60
    if-eqz v8, :cond_65

    #@62
    :try_start_62
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@65
    .line 593
    :cond_65
    :goto_65
    monitor-exit v12

    #@66
    goto :goto_e

    #@67
    .line 594
    :catchall_67
    move-exception v0

    #@68
    monitor-exit v12
    :try_end_69
    .catchall {:try_start_62 .. :try_end_69} :catchall_67

    #@69
    throw v0

    #@6a
    .line 588
    :catch_6a
    move-exception v9

    #@6b
    .line 589
    .local v9, e:Ljava/lang/IllegalStateException;
    :try_start_6b
    const-string v0, "WebViewDatabaseClassic"

    #@6d
    const-string v1, "getHttpAuthUsernamePassword"

    #@6f
    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_72
    .catchall {:try_start_6b .. :try_end_72} :catchall_78

    #@72
    .line 591
    if-eqz v8, :cond_65

    #@74
    :try_start_74
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@77
    goto :goto_65

    #@78
    .end local v9           #e:Ljava/lang/IllegalStateException;
    :catchall_78
    move-exception v0

    #@79
    if-eqz v8, :cond_7e

    #@7b
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@7e
    :cond_7e
    throw v0
    :try_end_7f
    .catchall {:try_start_74 .. :try_end_7f} :catchall_67
.end method

.method getUsernamePassword(Ljava/lang/String;)[Ljava/lang/String;
    .registers 17
    .parameter "schemePlusHost"

    #@0
    .prologue
    .line 437
    if-eqz p1, :cond_8

    #@2
    invoke-direct {p0}, Landroid/webkit/WebViewDatabaseClassic;->checkInitialized()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    .line 438
    :cond_8
    const/4 v11, 0x0

    #@9
    .line 478
    :goto_9
    return-object v11

    #@a
    .line 441
    :cond_a
    const/4 v0, 0x4

    #@b
    new-array v2, v0, [Ljava/lang/String;

    #@d
    const/4 v0, 0x0

    #@e
    const-string/jumbo v1, "username"

    #@11
    aput-object v1, v2, v0

    #@13
    const/4 v0, 0x1

    #@14
    const-string/jumbo v1, "password"

    #@17
    aput-object v1, v2, v0

    #@19
    const/4 v0, 0x2

    #@1a
    const-string/jumbo v1, "username_name"

    #@1d
    aput-object v1, v2, v0

    #@1f
    const/4 v0, 0x3

    #@20
    const-string/jumbo v1, "password_name"

    #@23
    aput-object v1, v2, v0

    #@25
    .line 445
    .local v2, columns:[Ljava/lang/String;
    const-string v12, "(host == ?)"

    #@27
    .line 446
    .local v12, selection:Ljava/lang/String;
    iget-object v14, p0, Landroid/webkit/WebViewDatabaseClassic;->mPasswordLock:Ljava/lang/Object;

    #@29
    monitor-enter v14

    #@2a
    .line 447
    const/4 v11, 0x0

    #@2b
    .line 448
    .local v11, ret:[Ljava/lang/String;
    const/4 v8, 0x0

    #@2c
    .line 450
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_2c
    sget-object v0, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@2e
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@30
    const/4 v3, 0x0

    #@31
    aget-object v1, v1, v3

    #@33
    const-string v3, "(host == ?)"

    #@35
    const/4 v4, 0x1

    #@36
    new-array v4, v4, [Ljava/lang/String;

    #@38
    const/4 v5, 0x0

    #@39
    aput-object p1, v4, v5

    #@3b
    const/4 v5, 0x0

    #@3c
    const/4 v6, 0x0

    #@3d
    const/4 v7, 0x0

    #@3e
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@41
    move-result-object v8

    #@42
    .line 453
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@45
    move-result v0

    #@46
    if-eqz v0, :cond_c9

    #@48
    .line 454
    const/4 v0, 0x2

    #@49
    new-array v11, v0, [Ljava/lang/String;

    #@4b
    .line 462
    const/4 v0, 0x0

    #@4c
    const-string/jumbo v1, "username"

    #@4f
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@52
    move-result v1

    #@53
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    aput-object v1, v11, v0

    #@59
    .line 463
    const/4 v0, 0x1

    #@5a
    const-string/jumbo v1, "password"

    #@5d
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@60
    move-result v1

    #@61
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    aput-object v1, v11, v0

    #@67
    .line 464
    const/4 v0, 0x0

    #@68
    aget-object v0, v11, v0

    #@6a
    if-nez v0, :cond_71

    #@6c
    const/4 v0, 0x1

    #@6d
    aget-object v0, v11, v0

    #@6f
    if-eqz v0, :cond_c9

    #@71
    .line 466
    :cond_71
    const-string/jumbo v0, "username_name"

    #@74
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@77
    move-result v0

    #@78
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@7b
    move-result-object v13

    #@7c
    .line 467
    .local v13, username_name:Ljava/lang/String;
    const-string/jumbo v0, "password_name"

    #@7f
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@82
    move-result v0

    #@83
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@86
    move-result-object v10

    #@87
    .line 468
    .local v10, password_name:Ljava/lang/String;
    const/4 v0, 0x0

    #@88
    new-instance v1, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    if-nez v13, :cond_91

    #@8f
    const-string v13, ""

    #@91
    .end local v13           #username_name:Ljava/lang/String;
    :cond_91
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    const-string v3, ","

    #@97
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v1

    #@9b
    const/4 v3, 0x0

    #@9c
    aget-object v3, v11, v3

    #@9e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v1

    #@a2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v1

    #@a6
    aput-object v1, v11, v0

    #@a8
    .line 469
    const/4 v0, 0x1

    #@a9
    new-instance v1, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    if-nez v10, :cond_b2

    #@b0
    const-string v10, ""

    #@b2
    .end local v10           #password_name:Ljava/lang/String;
    :cond_b2
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v1

    #@b6
    const-string v3, ","

    #@b8
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v1

    #@bc
    const/4 v3, 0x1

    #@bd
    aget-object v3, v11, v3

    #@bf
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v1

    #@c3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v1

    #@c7
    aput-object v1, v11, v0
    :try_end_c9
    .catchall {:try_start_2c .. :try_end_c9} :catchall_e2
    .catch Ljava/lang/IllegalStateException; {:try_start_2c .. :try_end_c9} :catch_d4

    #@c9
    .line 476
    :cond_c9
    if-eqz v8, :cond_ce

    #@cb
    :try_start_cb
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@ce
    .line 478
    :cond_ce
    :goto_ce
    monitor-exit v14

    #@cf
    goto/16 :goto_9

    #@d1
    .line 479
    :catchall_d1
    move-exception v0

    #@d2
    monitor-exit v14
    :try_end_d3
    .catchall {:try_start_cb .. :try_end_d3} :catchall_d1

    #@d3
    throw v0

    #@d4
    .line 473
    :catch_d4
    move-exception v9

    #@d5
    .line 474
    .local v9, e:Ljava/lang/IllegalStateException;
    :try_start_d5
    const-string v0, "WebViewDatabaseClassic"

    #@d7
    const-string v1, "getUsernamePassword"

    #@d9
    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_dc
    .catchall {:try_start_d5 .. :try_end_dc} :catchall_e2

    #@dc
    .line 476
    if-eqz v8, :cond_ce

    #@de
    :try_start_de
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@e1
    goto :goto_ce

    #@e2
    .end local v9           #e:Ljava/lang/IllegalStateException;
    :catchall_e2
    move-exception v0

    #@e3
    if-eqz v8, :cond_e8

    #@e5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@e8
    :cond_e8
    throw v0
    :try_end_e9
    .catchall {:try_start_de .. :try_end_e9} :catchall_d1
.end method

.method public hasFormData()Z
    .registers 3

    #@0
    .prologue
    .line 760
    iget-object v1, p0, Landroid/webkit/WebViewDatabaseClassic;->mFormLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 761
    const/4 v0, 0x1

    #@4
    :try_start_4
    invoke-direct {p0, v0}, Landroid/webkit/WebViewDatabaseClassic;->hasEntries(I)Z

    #@7
    move-result v0

    #@8
    monitor-exit v1

    #@9
    return v0

    #@a
    .line 762
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public hasHttpAuthUsernamePassword()Z
    .registers 3

    #@0
    .prologue
    .line 602
    iget-object v1, p0, Landroid/webkit/WebViewDatabaseClassic;->mHttpAuthLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 603
    const/4 v0, 0x3

    #@4
    :try_start_4
    invoke-direct {p0, v0}, Landroid/webkit/WebViewDatabaseClassic;->hasEntries(I)Z

    #@7
    move-result v0

    #@8
    monitor-exit v1

    #@9
    return v0

    #@a
    .line 604
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public hasUsernamePassword()Z
    .registers 3

    #@0
    .prologue
    .line 487
    iget-object v1, p0, Landroid/webkit/WebViewDatabaseClassic;->mPasswordLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 488
    const/4 v0, 0x0

    #@4
    :try_start_4
    invoke-direct {p0, v0}, Landroid/webkit/WebViewDatabaseClassic;->hasEntries(I)Z

    #@7
    move-result v0

    #@8
    monitor-exit v1

    #@9
    return v0

    #@a
    .line 489
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method setFormData(Ljava/lang/String;Ljava/util/HashMap;)V
    .registers 23
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 642
    .local p2, formdata:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p1, :cond_a

    #@2
    if-eqz p2, :cond_a

    #@4
    invoke-direct/range {p0 .. p0}, Landroid/webkit/WebViewDatabaseClassic;->checkInitialized()Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_b

    #@a
    .line 699
    :cond_a
    :goto_a
    return-void

    #@b
    .line 646
    :cond_b
    const-string v15, "(url == ?)"

    #@d
    .line 647
    .local v15, selection:Ljava/lang/String;
    move-object/from16 v0, p0

    #@f
    iget-object v0, v0, Landroid/webkit/WebViewDatabaseClassic;->mFormLock:Ljava/lang/Object;

    #@11
    move-object/from16 v19, v0

    #@13
    monitor-enter v19

    #@14
    .line 648
    const-wide/16 v17, -0x1

    #@16
    .line 649
    .local v17, urlid:J
    const/4 v10, 0x0

    #@17
    .line 651
    .local v10, cursor:Landroid/database/Cursor;
    :try_start_17
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@19
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@1b
    const/4 v3, 0x1

    #@1c
    aget-object v2, v2, v3

    #@1e
    sget-object v3, Landroid/webkit/WebViewDatabaseClassic;->ID_PROJECTION:[Ljava/lang/String;

    #@20
    const-string v4, "(url == ?)"

    #@22
    const/4 v5, 0x1

    #@23
    new-array v5, v5, [Ljava/lang/String;

    #@25
    const/4 v6, 0x0

    #@26
    aput-object p1, v5, v6

    #@28
    const/4 v6, 0x0

    #@29
    const/4 v7, 0x0

    #@2a
    const/4 v8, 0x0

    #@2b
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2e
    move-result-object v10

    #@2f
    .line 654
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    #@32
    move-result v1

    #@33
    if-eqz v1, :cond_b0

    #@35
    .line 655
    const-string v1, "_id"

    #@37
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3a
    move-result v1

    #@3b
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_3e
    .catchall {:try_start_17 .. :try_end_3e} :catchall_fb
    .catch Ljava/lang/IllegalStateException; {:try_start_17 .. :try_end_3e} :catch_cb
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_17 .. :try_end_3e} :catch_db

    #@3e
    move-result-wide v17

    #@3f
    .line 674
    :goto_3f
    if-eqz v10, :cond_44

    #@41
    :try_start_41
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@44
    .line 676
    :cond_44
    :goto_44
    const-wide/16 v1, 0x0

    #@46
    cmp-long v1, v17, v1

    #@48
    if-ltz v1, :cond_102

    #@4a
    .line 677
    invoke-virtual/range {p2 .. p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@4d
    move-result-object v16

    #@4e
    .line 678
    .local v16, set:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@51
    move-result-object v13

    #@52
    .line 679
    .local v13, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v14, Landroid/content/ContentValues;

    #@54
    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    #@57
    .line 680
    .local v14, map:Landroid/content/ContentValues;
    const-string/jumbo v1, "urlid"

    #@5a
    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v14, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@61
    .line 681
    :cond_61
    :goto_61
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@64
    move-result v1

    #@65
    if-eqz v1, :cond_102

    #@67
    .line 682
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6a
    move-result-object v12

    #@6b
    check-cast v12, Ljava/util/Map$Entry;

    #@6d
    .line 683
    .local v12, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v2, "name"

    #@70
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@73
    move-result-object v1

    #@74
    check-cast v1, Ljava/lang/String;

    #@76
    invoke-virtual {v14, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@79
    .line 684
    const-string/jumbo v2, "value"

    #@7c
    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@7f
    move-result-object v1

    #@80
    check-cast v1, Ljava/lang/String;

    #@82
    invoke-virtual {v14, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_85
    .catchall {:try_start_41 .. :try_end_85} :catchall_ad

    #@85
    .line 687
    :try_start_85
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@87
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@89
    const/4 v3, 0x2

    #@8a
    aget-object v2, v2, v3

    #@8c
    const/4 v3, 0x0

    #@8d
    invoke-virtual {v1, v2, v3, v14}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_90
    .catchall {:try_start_85 .. :try_end_90} :catchall_ad
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_85 .. :try_end_90} :catch_91

    #@90
    goto :goto_61

    #@91
    .line 688
    :catch_91
    move-exception v11

    #@92
    .line 689
    .local v11, e:Landroid/database/sqlite/SQLiteException;
    :try_start_92
    move-object/from16 v0, p0

    #@94
    invoke-direct {v0, v11}, Landroid/webkit/WebViewDatabaseClassic;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@97
    move-result v1

    #@98
    if-eqz v1, :cond_61

    #@9a
    .line 690
    const-string v1, "WebViewDatabaseClassic"

    #@9c
    const-string v2, "Caught memory full exception in setFormData() - formdata"

    #@9e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    .line 691
    const-string v1, "WebViewDatabaseClassic"

    #@a3
    invoke-static {v11}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@a6
    move-result-object v2

    #@a7
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 692
    monitor-exit v19

    #@ab
    goto/16 :goto_a

    #@ad
    .line 698
    .end local v11           #e:Landroid/database/sqlite/SQLiteException;
    .end local v12           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v13           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v14           #map:Landroid/content/ContentValues;
    .end local v16           #set:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :catchall_ad
    move-exception v1

    #@ae
    monitor-exit v19
    :try_end_af
    .catchall {:try_start_92 .. :try_end_af} :catchall_ad

    #@af
    throw v1

    #@b0
    .line 657
    :cond_b0
    :try_start_b0
    new-instance v9, Landroid/content/ContentValues;

    #@b2
    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    #@b5
    .line 658
    .local v9, c:Landroid/content/ContentValues;
    const-string/jumbo v1, "url"

    #@b8
    move-object/from16 v0, p1

    #@ba
    invoke-virtual {v9, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@bd
    .line 659
    sget-object v1, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@bf
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@c1
    const/4 v3, 0x1

    #@c2
    aget-object v2, v2, v3

    #@c4
    const/4 v3, 0x0

    #@c5
    invoke-virtual {v1, v2, v3, v9}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_c8
    .catchall {:try_start_b0 .. :try_end_c8} :catchall_fb
    .catch Ljava/lang/IllegalStateException; {:try_start_b0 .. :try_end_c8} :catch_cb
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b0 .. :try_end_c8} :catch_db

    #@c8
    move-result-wide v17

    #@c9
    goto/16 :goto_3f

    #@cb
    .line 662
    .end local v9           #c:Landroid/content/ContentValues;
    :catch_cb
    move-exception v11

    #@cc
    .line 663
    .local v11, e:Ljava/lang/IllegalStateException;
    :try_start_cc
    const-string v1, "WebViewDatabaseClassic"

    #@ce
    const-string/jumbo v2, "setFormData"

    #@d1
    invoke-static {v1, v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d4
    .catchall {:try_start_cc .. :try_end_d4} :catchall_fb

    #@d4
    .line 674
    if-eqz v10, :cond_44

    #@d6
    :try_start_d6
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_d9
    .catchall {:try_start_d6 .. :try_end_d9} :catchall_ad

    #@d9
    goto/16 :goto_44

    #@db
    .line 666
    .end local v11           #e:Ljava/lang/IllegalStateException;
    :catch_db
    move-exception v11

    #@dc
    .line 667
    .local v11, e:Landroid/database/sqlite/SQLiteException;
    :try_start_dc
    move-object/from16 v0, p0

    #@de
    invoke-direct {v0, v11}, Landroid/webkit/WebViewDatabaseClassic;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@e1
    move-result v1

    #@e2
    if-eqz v1, :cond_f4

    #@e4
    .line 668
    const-string v1, "WebViewDatabaseClassic"

    #@e6
    const-string v2, "Caught memory full exception in setFormData() - formurl"

    #@e8
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    .line 669
    const-string v1, "WebViewDatabaseClassic"

    #@ed
    invoke-static {v11}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@f0
    move-result-object v2

    #@f1
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f4
    .catchall {:try_start_dc .. :try_end_f4} :catchall_fb

    #@f4
    .line 674
    :cond_f4
    if-eqz v10, :cond_44

    #@f6
    :try_start_f6
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@f9
    goto/16 :goto_44

    #@fb
    .end local v11           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_fb
    move-exception v1

    #@fc
    if-eqz v10, :cond_101

    #@fe
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@101
    :cond_101
    throw v1

    #@102
    .line 698
    :cond_102
    monitor-exit v19
    :try_end_103
    .catchall {:try_start_f6 .. :try_end_103} :catchall_ad

    #@103
    goto/16 :goto_a
.end method

.method setHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "host"
    .parameter "realm"
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    .line 531
    if-eqz p1, :cond_a

    #@2
    if-eqz p2, :cond_a

    #@4
    invoke-direct {p0}, Landroid/webkit/WebViewDatabaseClassic;->checkInitialized()Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_b

    #@a
    .line 553
    :cond_a
    :goto_a
    return-void

    #@b
    .line 535
    :cond_b
    iget-object v3, p0, Landroid/webkit/WebViewDatabaseClassic;->mHttpAuthLock:Ljava/lang/Object;

    #@d
    monitor-enter v3

    #@e
    .line 536
    :try_start_e
    new-instance v0, Landroid/content/ContentValues;

    #@10
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@13
    .line 537
    .local v0, c:Landroid/content/ContentValues;
    const-string v2, "host"

    #@15
    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 538
    const-string/jumbo v2, "realm"

    #@1b
    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 539
    const-string/jumbo v2, "username"

    #@21
    invoke-virtual {v0, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 540
    const-string/jumbo v2, "password"

    #@27
    invoke-virtual {v0, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2a
    .catchall {:try_start_e .. :try_end_2a} :catchall_38

    #@2a
    .line 543
    :try_start_2a
    sget-object v2, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@2c
    sget-object v4, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@2e
    const/4 v5, 0x3

    #@2f
    aget-object v4, v4, v5

    #@31
    const-string v5, "host"

    #@33
    invoke-virtual {v2, v4, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_36
    .catchall {:try_start_2a .. :try_end_36} :catchall_38
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2a .. :try_end_36} :catch_3b

    #@36
    .line 552
    :cond_36
    :goto_36
    :try_start_36
    monitor-exit v3

    #@37
    goto :goto_a

    #@38
    .end local v0           #c:Landroid/content/ContentValues;
    :catchall_38
    move-exception v2

    #@39
    monitor-exit v3
    :try_end_3a
    .catchall {:try_start_36 .. :try_end_3a} :catchall_38

    #@3a
    throw v2

    #@3b
    .line 545
    .restart local v0       #c:Landroid/content/ContentValues;
    :catch_3b
    move-exception v1

    #@3c
    .line 546
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_3c
    invoke-direct {p0, v1}, Landroid/webkit/WebViewDatabaseClassic;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@3f
    move-result v2

    #@40
    if-eqz v2, :cond_36

    #@42
    .line 547
    const-string v2, "WebViewDatabaseClassic"

    #@44
    const-string v4, "Caught memory full exception in setHttpAuthUsernamePassword()"

    #@46
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 548
    const-string v2, "WebViewDatabaseClassic"

    #@4b
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_52
    .catchall {:try_start_3c .. :try_end_52} :catchall_38

    #@52
    goto :goto_36
.end method

.method setUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "schemePlusHost"
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    .line 379
    if-eqz p1, :cond_9

    #@3
    invoke-direct {p0}, Landroid/webkit/WebViewDatabaseClassic;->checkInitialized()Z

    #@6
    move-result v4

    #@7
    if-nez v4, :cond_a

    #@9
    .line 427
    :cond_9
    :goto_9
    return-void

    #@a
    .line 383
    :cond_a
    iget-object v5, p0, Landroid/webkit/WebViewDatabaseClassic;->mPasswordLock:Ljava/lang/Object;

    #@c
    monitor-enter v5

    #@d
    .line 384
    :try_start_d
    new-instance v0, Landroid/content/ContentValues;

    #@f
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@12
    .line 385
    .local v0, c:Landroid/content/ContentValues;
    const-string v4, "host"

    #@14
    invoke-virtual {v0, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 391
    if-nez p2, :cond_52

    #@19
    if-nez p3, :cond_52

    #@1b
    .line 393
    const/4 v4, 0x1

    #@1c
    new-array v3, v4, [Ljava/lang/String;

    #@1e
    .local v3, usernames:[Ljava/lang/String;
    const/4 v4, 0x0

    #@1f
    const/4 v6, 0x0

    #@20
    aput-object v6, v3, v4

    #@22
    .line 394
    const/4 v4, 0x1

    #@23
    new-array v2, v4, [Ljava/lang/String;

    #@25
    .local v2, passwords:[Ljava/lang/String;
    const/4 v4, 0x0

    #@26
    const/4 v6, 0x0

    #@27
    aput-object v6, v2, v4

    #@29
    .line 402
    :goto_29
    array-length v4, v3

    #@2a
    if-lt v4, v7, :cond_2f

    #@2c
    array-length v4, v2

    #@2d
    if-ge v4, v7, :cond_65

    #@2f
    .line 404
    :cond_2f
    const-string/jumbo v4, "username"

    #@32
    const/4 v6, 0x0

    #@33
    aget-object v6, v3, v6

    #@35
    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@38
    .line 405
    const-string/jumbo v4, "password"

    #@3b
    const/4 v6, 0x0

    #@3c
    aget-object v6, v2, v6

    #@3e
    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_41
    .catchall {:try_start_d .. :try_end_41} :catchall_4f

    #@41
    .line 417
    :goto_41
    :try_start_41
    sget-object v4, Landroid/webkit/WebViewDatabaseClassic;->sDatabase:Landroid/database/sqlite/SQLiteDatabase;

    #@43
    sget-object v6, Landroid/webkit/WebViewDatabaseClassic;->mTableNames:[Ljava/lang/String;

    #@45
    const/4 v7, 0x0

    #@46
    aget-object v6, v6, v7

    #@48
    const-string v7, "host"

    #@4a
    invoke-virtual {v4, v6, v7, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_4d
    .catchall {:try_start_41 .. :try_end_4d} :catchall_4f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_41 .. :try_end_4d} :catch_8a

    #@4d
    .line 426
    :cond_4d
    :goto_4d
    :try_start_4d
    monitor-exit v5

    #@4e
    goto :goto_9

    #@4f
    .end local v0           #c:Landroid/content/ContentValues;
    .end local v2           #passwords:[Ljava/lang/String;
    .end local v3           #usernames:[Ljava/lang/String;
    :catchall_4f
    move-exception v4

    #@50
    monitor-exit v5
    :try_end_51
    .catchall {:try_start_4d .. :try_end_51} :catchall_4f

    #@51
    throw v4

    #@52
    .line 396
    .restart local v0       #c:Landroid/content/ContentValues;
    :cond_52
    if-eqz p2, :cond_56

    #@54
    if-nez p3, :cond_58

    #@56
    :cond_56
    :try_start_56
    monitor-exit v5

    #@57
    goto :goto_9

    #@58
    .line 399
    :cond_58
    const-string v4, ","

    #@5a
    invoke-virtual {p2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    .line 400
    .restart local v3       #usernames:[Ljava/lang/String;
    const-string v4, ","

    #@60
    invoke-virtual {p3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@63
    move-result-object v2

    #@64
    .restart local v2       #passwords:[Ljava/lang/String;
    goto :goto_29

    #@65
    .line 409
    :cond_65
    const-string/jumbo v4, "username_name"

    #@68
    const/4 v6, 0x0

    #@69
    aget-object v6, v3, v6

    #@6b
    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6e
    .line 410
    const-string/jumbo v4, "password_name"

    #@71
    const/4 v6, 0x0

    #@72
    aget-object v6, v2, v6

    #@74
    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@77
    .line 411
    const-string/jumbo v4, "username"

    #@7a
    const/4 v6, 0x1

    #@7b
    aget-object v6, v3, v6

    #@7d
    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@80
    .line 412
    const-string/jumbo v4, "password"

    #@83
    const/4 v6, 0x1

    #@84
    aget-object v6, v2, v6

    #@86
    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@89
    goto :goto_41

    #@8a
    .line 419
    :catch_8a
    move-exception v1

    #@8b
    .line 420
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    invoke-direct {p0, v1}, Landroid/webkit/WebViewDatabaseClassic;->isLowMemory(Landroid/database/sqlite/SQLiteException;)Z

    #@8e
    move-result v4

    #@8f
    if-eqz v4, :cond_4d

    #@91
    .line 421
    const-string v4, "WebViewDatabaseClassic"

    #@93
    const-string v6, "Caught memory full exception in setUsernamePassword()"

    #@95
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 422
    const-string v4, "WebViewDatabaseClassic"

    #@9a
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@9d
    move-result-object v6

    #@9e
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a1
    .catchall {:try_start_56 .. :try_end_a1} :catchall_4f

    #@a1
    goto :goto_4d
.end method
