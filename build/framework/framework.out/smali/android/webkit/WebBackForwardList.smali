.class public Landroid/webkit/WebBackForwardList;
.super Ljava/lang/Object;
.source "WebBackForwardList.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/io/Serializable;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 32
    return-void
.end method


# virtual methods
.method protected declared-synchronized clone()Landroid/webkit/WebBackForwardList;
    .registers 2

    #@0
    .prologue
    .line 75
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 26
    invoke-virtual {p0}, Landroid/webkit/WebBackForwardList;->clone()Landroid/webkit/WebBackForwardList;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public declared-synchronized getCurrentIndex()I
    .registers 2

    #@0
    .prologue
    .line 49
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getCurrentItem()Landroid/webkit/WebHistoryItem;
    .registers 2

    #@0
    .prologue
    .line 40
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getItemAtIndex(I)Landroid/webkit/WebHistoryItem;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 58
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getSize()I
    .registers 2

    #@0
    .prologue
    .line 66
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method
