.class Landroid/webkit/WebViewCore$AutoFillData;
.super Ljava/lang/Object;
.source "WebViewCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewCore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AutoFillData"
.end annotation


# instance fields
.field private mPreview:Ljava/lang/String;

.field private mQueryId:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 970
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 971
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/webkit/WebViewCore$AutoFillData;->mQueryId:I

    #@6
    .line 972
    const-string v0, ""

    #@8
    iput-object v0, p0, Landroid/webkit/WebViewCore$AutoFillData;->mPreview:Ljava/lang/String;

    #@a
    .line 973
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .registers 3
    .parameter "queryId"
    .parameter "preview"

    #@0
    .prologue
    .line 975
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 976
    iput p1, p0, Landroid/webkit/WebViewCore$AutoFillData;->mQueryId:I

    #@5
    .line 977
    iput-object p2, p0, Landroid/webkit/WebViewCore$AutoFillData;->mPreview:Ljava/lang/String;

    #@7
    .line 978
    return-void
.end method


# virtual methods
.method public getPreviewString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 985
    iget-object v0, p0, Landroid/webkit/WebViewCore$AutoFillData;->mPreview:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getQueryId()I
    .registers 2

    #@0
    .prologue
    .line 981
    iget v0, p0, Landroid/webkit/WebViewCore$AutoFillData;->mQueryId:I

    #@2
    return v0
.end method
