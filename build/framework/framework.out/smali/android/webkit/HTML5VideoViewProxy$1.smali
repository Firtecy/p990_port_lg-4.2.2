.class Landroid/webkit/HTML5VideoViewProxy$1;
.super Landroid/os/Handler;
.source "HTML5VideoViewProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/HTML5VideoViewProxy;->createWebCoreHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/HTML5VideoViewProxy;


# direct methods
.method constructor <init>(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 849
    iput-object p1, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 12
    .parameter "msg"

    #@0
    .prologue
    .line 852
    iget v5, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v5, :sswitch_data_8e

    #@5
    .line 887
    :goto_5
    return-void

    #@6
    .line 854
    :sswitch_6
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v2, Ljava/util/Map;

    #@a
    .line 855
    .local v2, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v5, "dur"

    #@c
    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Ljava/lang/Integer;

    #@12
    .line 856
    .local v0, duration:Ljava/lang/Integer;
    const-string/jumbo v5, "width"

    #@15
    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v4

    #@19
    check-cast v4, Ljava/lang/Integer;

    #@1b
    .line 857
    .local v4, width:Ljava/lang/Integer;
    const-string v5, "height"

    #@1d
    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Ljava/lang/Integer;

    #@23
    .line 858
    .local v1, height:Ljava/lang/Integer;
    iget-object v5, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@25
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@28
    move-result v6

    #@29
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@2c
    move-result v7

    #@2d
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@30
    move-result v8

    #@31
    iget-object v9, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@33
    iget v9, v9, Landroid/webkit/HTML5VideoViewProxy;->mNativePointer:I

    #@35
    invoke-static {v5, v6, v7, v8, v9}, Landroid/webkit/HTML5VideoViewProxy;->access$1400(Landroid/webkit/HTML5VideoViewProxy;IIII)V

    #@38
    goto :goto_5

    #@39
    .line 863
    .end local v0           #duration:Ljava/lang/Integer;
    .end local v1           #height:Ljava/lang/Integer;
    .end local v2           #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4           #width:Ljava/lang/Integer;
    :sswitch_39
    iget-object v5, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@3b
    const/4 v6, 0x0

    #@3c
    invoke-static {v5, v6}, Landroid/webkit/HTML5VideoViewProxy;->access$1502(Landroid/webkit/HTML5VideoViewProxy;I)I

    #@3f
    .line 864
    iget-object v5, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@41
    iget-object v6, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@43
    iget v6, v6, Landroid/webkit/HTML5VideoViewProxy;->mNativePointer:I

    #@45
    invoke-static {v5, v6}, Landroid/webkit/HTML5VideoViewProxy;->access$1600(Landroid/webkit/HTML5VideoViewProxy;I)V

    #@48
    goto :goto_5

    #@49
    .line 867
    :sswitch_49
    iget-object v5, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@4b
    iget-object v6, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@4d
    iget v6, v6, Landroid/webkit/HTML5VideoViewProxy;->mNativePointer:I

    #@4f
    invoke-static {v5, v6}, Landroid/webkit/HTML5VideoViewProxy;->access$1700(Landroid/webkit/HTML5VideoViewProxy;I)V

    #@52
    goto :goto_5

    #@53
    .line 870
    :sswitch_53
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@55
    check-cast v3, Landroid/graphics/Bitmap;

    #@57
    .line 871
    .local v3, poster:Landroid/graphics/Bitmap;
    iget-object v5, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@59
    iget-object v6, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@5b
    iget v6, v6, Landroid/webkit/HTML5VideoViewProxy;->mNativePointer:I

    #@5d
    invoke-static {v5, v3, v6}, Landroid/webkit/HTML5VideoViewProxy;->access$1800(Landroid/webkit/HTML5VideoViewProxy;Landroid/graphics/Bitmap;I)V

    #@60
    goto :goto_5

    #@61
    .line 874
    .end local v3           #poster:Landroid/graphics/Bitmap;
    :sswitch_61
    iget-object v5, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@63
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@65
    iget-object v7, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@67
    iget v7, v7, Landroid/webkit/HTML5VideoViewProxy;->mNativePointer:I

    #@69
    invoke-static {v5, v6, v7}, Landroid/webkit/HTML5VideoViewProxy;->access$1900(Landroid/webkit/HTML5VideoViewProxy;II)V

    #@6c
    goto :goto_5

    #@6d
    .line 877
    :sswitch_6d
    iget-object v5, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@6f
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@71
    iget-object v7, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@73
    iget v7, v7, Landroid/webkit/HTML5VideoViewProxy;->mNativePointer:I

    #@75
    invoke-static {v5, v6, v7}, Landroid/webkit/HTML5VideoViewProxy;->access$2000(Landroid/webkit/HTML5VideoViewProxy;II)V

    #@78
    goto :goto_5

    #@79
    .line 880
    :sswitch_79
    iget-object v5, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@7b
    iget-object v6, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@7d
    iget v6, v6, Landroid/webkit/HTML5VideoViewProxy;->mNativePointer:I

    #@7f
    invoke-static {v5, v6}, Landroid/webkit/HTML5VideoViewProxy;->access$2100(Landroid/webkit/HTML5VideoViewProxy;I)V

    #@82
    goto :goto_5

    #@83
    .line 884
    :sswitch_83
    iget-object v5, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@85
    iget-object v6, p0, Landroid/webkit/HTML5VideoViewProxy$1;->this$0:Landroid/webkit/HTML5VideoViewProxy;

    #@87
    iget v6, v6, Landroid/webkit/HTML5VideoViewProxy;->mNativePointer:I

    #@89
    invoke-static {v5, v6}, Landroid/webkit/HTML5VideoViewProxy;->access$2200(Landroid/webkit/HTML5VideoViewProxy;I)V

    #@8c
    goto/16 :goto_5

    #@8e
    .line 852
    :sswitch_data_8e
    .sparse-switch
        0xc8 -> :sswitch_6
        0xc9 -> :sswitch_39
        0xca -> :sswitch_53
        0xcb -> :sswitch_49
        0xcc -> :sswitch_6d
        0xcd -> :sswitch_79
        0xce -> :sswitch_83
        0x12c -> :sswitch_61
    .end sparse-switch
.end method
