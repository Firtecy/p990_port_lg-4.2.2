.class Landroid/webkit/PluginFullScreenHolder;
.super Ljava/lang/Object;
.source "PluginFullScreenHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;
    }
.end annotation


# static fields
.field private static mLayout:Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;


# instance fields
.field private final mCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field private mContentView:Landroid/view/View;

.field private final mNpp:I

.field private final mOrientation:I

.field private final mWebView:Landroid/webkit/WebViewClassic;


# direct methods
.method constructor <init>(Landroid/webkit/WebViewClassic;II)V
    .registers 5
    .parameter "webView"
    .parameter "orientation"
    .parameter "npp"

    #@0
    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 133
    new-instance v0, Landroid/webkit/PluginFullScreenHolder$1;

    #@5
    invoke-direct {v0, p0}, Landroid/webkit/PluginFullScreenHolder$1;-><init>(Landroid/webkit/PluginFullScreenHolder;)V

    #@8
    iput-object v0, p0, Landroid/webkit/PluginFullScreenHolder;->mCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    #@a
    .line 48
    iput-object p1, p0, Landroid/webkit/PluginFullScreenHolder;->mWebView:Landroid/webkit/WebViewClassic;

    #@c
    .line 49
    iput p3, p0, Landroid/webkit/PluginFullScreenHolder;->mNpp:I

    #@e
    .line 50
    iput p2, p0, Landroid/webkit/PluginFullScreenHolder;->mOrientation:I

    #@10
    .line 51
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/PluginFullScreenHolder;)Landroid/webkit/WebViewClassic;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Landroid/webkit/PluginFullScreenHolder;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/webkit/PluginFullScreenHolder;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget v0, p0, Landroid/webkit/PluginFullScreenHolder;->mNpp:I

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/webkit/PluginFullScreenHolder;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Landroid/webkit/PluginFullScreenHolder;->mContentView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$300()Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;
    .registers 1

    #@0
    .prologue
    .line 36
    sget-object v0, Landroid/webkit/PluginFullScreenHolder;->mLayout:Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;)Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 36
    sput-object p0, Landroid/webkit/PluginFullScreenHolder;->mLayout:Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;

    #@2
    return-object p0
.end method


# virtual methods
.method public hide()V
    .registers 3

    #@0
    .prologue
    .line 89
    iget-object v1, p0, Landroid/webkit/PluginFullScreenHolder;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getWebChromeClient()Landroid/webkit/WebChromeClient;

    #@5
    move-result-object v0

    #@6
    .line 90
    .local v0, client:Landroid/webkit/WebChromeClient;
    invoke-virtual {v0}, Landroid/webkit/WebChromeClient;->onHideCustomView()V

    #@9
    .line 91
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 7
    .parameter "contentView"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 56
    new-instance v2, Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;

    #@3
    iget-object v3, p0, Landroid/webkit/PluginFullScreenHolder;->mWebView:Landroid/webkit/WebViewClassic;

    #@5
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@8
    move-result-object v3

    #@9
    invoke-direct {v2, p0, v3}, Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;-><init>(Landroid/webkit/PluginFullScreenHolder;Landroid/content/Context;)V

    #@c
    sput-object v2, Landroid/webkit/PluginFullScreenHolder;->mLayout:Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;

    #@e
    .line 57
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    #@10
    const/16 v2, 0x11

    #@12
    invoke-direct {v0, v4, v4, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    #@15
    .line 62
    .local v0, layoutParams:Landroid/widget/FrameLayout$LayoutParams;
    sget-object v2, Landroid/webkit/PluginFullScreenHolder;->mLayout:Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;

    #@17
    invoke-virtual {v2, p1, v0}, Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@1a
    .line 63
    sget-object v2, Landroid/webkit/PluginFullScreenHolder;->mLayout:Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;

    #@1c
    const/4 v3, 0x0

    #@1d
    invoke-virtual {v2, v3}, Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;->setVisibility(I)V

    #@20
    .line 69
    instance-of v2, p1, Landroid/view/SurfaceView;

    #@22
    if-eqz v2, :cond_34

    #@24
    move-object v1, p1

    #@25
    .line 70
    check-cast v1, Landroid/view/SurfaceView;

    #@27
    .line 71
    .local v1, sView:Landroid/view/SurfaceView;
    invoke-virtual {v1}, Landroid/view/SurfaceView;->isFixedSize()Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_34

    #@2d
    .line 72
    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@30
    move-result-object v2

    #@31
    invoke-interface {v2}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    #@34
    .line 76
    .end local v1           #sView:Landroid/view/SurfaceView;
    :cond_34
    iput-object p1, p0, Landroid/webkit/PluginFullScreenHolder;->mContentView:Landroid/view/View;

    #@36
    .line 77
    return-void
.end method

.method public show()V
    .registers 5

    #@0
    .prologue
    .line 81
    iget-object v1, p0, Landroid/webkit/PluginFullScreenHolder;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getViewManager()Landroid/webkit/ViewManager;

    #@5
    move-result-object v1

    #@6
    if-eqz v1, :cond_11

    #@8
    .line 82
    iget-object v1, p0, Landroid/webkit/PluginFullScreenHolder;->mWebView:Landroid/webkit/WebViewClassic;

    #@a
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getViewManager()Landroid/webkit/ViewManager;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Landroid/webkit/ViewManager;->hideAll()V

    #@11
    .line 84
    :cond_11
    iget-object v1, p0, Landroid/webkit/PluginFullScreenHolder;->mWebView:Landroid/webkit/WebViewClassic;

    #@13
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getWebChromeClient()Landroid/webkit/WebChromeClient;

    #@16
    move-result-object v0

    #@17
    .line 85
    .local v0, client:Landroid/webkit/WebChromeClient;
    sget-object v1, Landroid/webkit/PluginFullScreenHolder;->mLayout:Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;

    #@19
    iget v2, p0, Landroid/webkit/PluginFullScreenHolder;->mOrientation:I

    #@1b
    iget-object v3, p0, Landroid/webkit/PluginFullScreenHolder;->mCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    #@1d
    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebChromeClient;->onShowCustomView(Landroid/view/View;ILandroid/webkit/WebChromeClient$CustomViewCallback;)V

    #@20
    .line 86
    return-void
.end method
