.class Landroid/webkit/WebCoreThreadWatchdog$1;
.super Landroid/os/Handler;
.source "WebCoreThreadWatchdog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/WebCoreThreadWatchdog;->createHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/WebCoreThreadWatchdog;


# direct methods
.method constructor <init>(Landroid/webkit/WebCoreThreadWatchdog;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 150
    iput-object p1, p0, Landroid/webkit/WebCoreThreadWatchdog$1;->this$0:Landroid/webkit/WebCoreThreadWatchdog;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    .line 153
    iget v3, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v3, :pswitch_data_96

    #@5
    .line 210
    :goto_5
    return-void

    #@6
    .line 155
    :pswitch_6
    const-class v4, Landroid/webkit/WebCoreThreadWatchdog;

    #@8
    monitor-enter v4

    #@9
    .line 156
    :try_start_9
    iget-object v3, p0, Landroid/webkit/WebCoreThreadWatchdog$1;->this$0:Landroid/webkit/WebCoreThreadWatchdog;

    #@b
    invoke-static {v3}, Landroid/webkit/WebCoreThreadWatchdog;->access$000(Landroid/webkit/WebCoreThreadWatchdog;)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_16

    #@11
    .line 157
    monitor-exit v4

    #@12
    goto :goto_5

    #@13
    .line 167
    :catchall_13
    move-exception v3

    #@14
    monitor-exit v4
    :try_end_15
    .catchall {:try_start_9 .. :try_end_15} :catchall_13

    #@15
    throw v3

    #@16
    .line 161
    :cond_16
    const/16 v3, 0x65

    #@18
    :try_start_18
    invoke-virtual {p0, v3}, Landroid/webkit/WebCoreThreadWatchdog$1;->removeMessages(I)V

    #@1b
    .line 162
    const/16 v3, 0x65

    #@1d
    invoke-virtual {p0, v3}, Landroid/webkit/WebCoreThreadWatchdog$1;->obtainMessage(I)Landroid/os/Message;

    #@20
    move-result-object v3

    #@21
    const-wide/16 v5, 0x7530

    #@23
    invoke-virtual {p0, v3, v5, v6}, Landroid/webkit/WebCoreThreadWatchdog$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@26
    .line 163
    iget-object v3, p0, Landroid/webkit/WebCoreThreadWatchdog$1;->this$0:Landroid/webkit/WebCoreThreadWatchdog;

    #@28
    invoke-static {v3}, Landroid/webkit/WebCoreThreadWatchdog;->access$200(Landroid/webkit/WebCoreThreadWatchdog;)Landroid/os/Handler;

    #@2b
    move-result-object v3

    #@2c
    iget-object v5, p0, Landroid/webkit/WebCoreThreadWatchdog$1;->this$0:Landroid/webkit/WebCoreThreadWatchdog;

    #@2e
    invoke-static {v5}, Landroid/webkit/WebCoreThreadWatchdog;->access$200(Landroid/webkit/WebCoreThreadWatchdog;)Landroid/os/Handler;

    #@31
    move-result-object v5

    #@32
    const/16 v6, 0xc5

    #@34
    iget-object v7, p0, Landroid/webkit/WebCoreThreadWatchdog$1;->this$0:Landroid/webkit/WebCoreThreadWatchdog;

    #@36
    invoke-static {v7}, Landroid/webkit/WebCoreThreadWatchdog;->access$100(Landroid/webkit/WebCoreThreadWatchdog;)Landroid/os/Handler;

    #@39
    move-result-object v7

    #@3a
    const/16 v8, 0x64

    #@3c
    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3f
    move-result-object v7

    #@40
    invoke-virtual {v5, v6, v7}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@43
    move-result-object v5

    #@44
    const-wide/16 v6, 0x2710

    #@46
    invoke-virtual {v3, v5, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@49
    .line 167
    monitor-exit v4
    :try_end_4a
    .catchall {:try_start_18 .. :try_end_4a} :catchall_13

    #@4a
    goto :goto_5

    #@4b
    .line 171
    :pswitch_4b
    const/4 v2, 0x0

    #@4c
    .line 172
    .local v2, postedDialog:Z
    const-class v4, Landroid/webkit/WebCoreThreadWatchdog;

    #@4e
    monitor-enter v4

    #@4f
    .line 173
    :try_start_4f
    iget-object v3, p0, Landroid/webkit/WebCoreThreadWatchdog$1;->this$0:Landroid/webkit/WebCoreThreadWatchdog;

    #@51
    invoke-static {v3}, Landroid/webkit/WebCoreThreadWatchdog;->access$300(Landroid/webkit/WebCoreThreadWatchdog;)Ljava/util/Set;

    #@54
    move-result-object v3

    #@55
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@58
    move-result-object v1

    #@59
    .line 176
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/webkit/WebViewClassic;>;"
    :cond_59
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@5c
    move-result v3

    #@5d
    if-eqz v3, :cond_82

    #@5f
    .line 177
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@62
    move-result-object v3

    #@63
    check-cast v3, Landroid/webkit/WebViewClassic;

    #@65
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@68
    move-result-object v0

    #@69
    .line 179
    .local v0, activeView:Landroid/webkit/WebView;
    invoke-virtual {v0}, Landroid/webkit/WebView;->getWindowToken()Landroid/os/IBinder;

    #@6c
    move-result-object v3

    #@6d
    if-eqz v3, :cond_59

    #@6f
    invoke-virtual {v0}, Landroid/webkit/WebView;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@72
    move-result-object v3

    #@73
    if-eqz v3, :cond_59

    #@75
    .line 182
    new-instance v3, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;

    #@77
    iget-object v5, p0, Landroid/webkit/WebCoreThreadWatchdog$1;->this$0:Landroid/webkit/WebCoreThreadWatchdog;

    #@79
    invoke-direct {v3, v5, v0, p0}, Landroid/webkit/WebCoreThreadWatchdog$PageNotRespondingRunnable;-><init>(Landroid/webkit/WebCoreThreadWatchdog;Landroid/webkit/WebView;Landroid/os/Handler;)V

    #@7c
    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    #@7f
    move-result v2

    #@80
    .line 186
    if-eqz v2, :cond_59

    #@82
    .line 199
    .end local v0           #activeView:Landroid/webkit/WebView;
    :cond_82
    if-nez v2, :cond_8f

    #@84
    .line 204
    const/16 v3, 0x65

    #@86
    invoke-virtual {p0, v3}, Landroid/webkit/WebCoreThreadWatchdog$1;->obtainMessage(I)Landroid/os/Message;

    #@89
    move-result-object v3

    #@8a
    const-wide/16 v5, 0x3a98

    #@8c
    invoke-virtual {p0, v3, v5, v6}, Landroid/webkit/WebCoreThreadWatchdog$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@8f
    .line 207
    :cond_8f
    monitor-exit v4

    #@90
    goto/16 :goto_5

    #@92
    .end local v1           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/webkit/WebViewClassic;>;"
    :catchall_92
    move-exception v3

    #@93
    monitor-exit v4
    :try_end_94
    .catchall {:try_start_4f .. :try_end_94} :catchall_92

    #@94
    throw v3

    #@95
    .line 153
    nop

    #@96
    :pswitch_data_96
    .packed-switch 0x64
        :pswitch_6
        :pswitch_4b
    .end packed-switch
.end method
