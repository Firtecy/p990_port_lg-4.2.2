.class Landroid/webkit/ZoomControlExternal$2;
.super Ljava/lang/Object;
.source "ZoomControlExternal.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/ZoomControlExternal;->createZoomControls()Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/ZoomControlExternal;


# direct methods
.method constructor <init>(Landroid/webkit/ZoomControlExternal;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 97
    iput-object p1, p0, Landroid/webkit/ZoomControlExternal$2;->this$0:Landroid/webkit/ZoomControlExternal;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .parameter "v"

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal$2;->this$0:Landroid/webkit/ZoomControlExternal;

    #@2
    invoke-static {v0}, Landroid/webkit/ZoomControlExternal;->access$200(Landroid/webkit/ZoomControlExternal;)Landroid/os/Handler;

    #@5
    move-result-object v0

    #@6
    iget-object v1, p0, Landroid/webkit/ZoomControlExternal$2;->this$0:Landroid/webkit/ZoomControlExternal;

    #@8
    invoke-static {v1}, Landroid/webkit/ZoomControlExternal;->access$100(Landroid/webkit/ZoomControlExternal;)Ljava/lang/Runnable;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@f
    .line 101
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal$2;->this$0:Landroid/webkit/ZoomControlExternal;

    #@11
    invoke-static {v0}, Landroid/webkit/ZoomControlExternal;->access$200(Landroid/webkit/ZoomControlExternal;)Landroid/os/Handler;

    #@14
    move-result-object v0

    #@15
    iget-object v1, p0, Landroid/webkit/ZoomControlExternal$2;->this$0:Landroid/webkit/ZoomControlExternal;

    #@17
    invoke-static {v1}, Landroid/webkit/ZoomControlExternal;->access$100(Landroid/webkit/ZoomControlExternal;)Ljava/lang/Runnable;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {}, Landroid/webkit/ZoomControlExternal;->access$300()J

    #@1e
    move-result-wide v2

    #@1f
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@22
    .line 102
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal$2;->this$0:Landroid/webkit/ZoomControlExternal;

    #@24
    invoke-static {v0}, Landroid/webkit/ZoomControlExternal;->access$400(Landroid/webkit/ZoomControlExternal;)Landroid/webkit/WebViewClassic;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->zoomIn()Z

    #@2b
    .line 103
    return-void
.end method
