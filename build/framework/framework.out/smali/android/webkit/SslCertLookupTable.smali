.class final Landroid/webkit/SslCertLookupTable;
.super Ljava/lang/Object;
.source "SslCertLookupTable.java"


# static fields
.field private static sTable:Landroid/webkit/SslCertLookupTable;


# instance fields
.field private final table:Landroid/os/Bundle;


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 44
    new-instance v0, Landroid/os/Bundle;

    #@5
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/SslCertLookupTable;->table:Landroid/os/Bundle;

    #@a
    .line 45
    return-void
.end method

.method public static getInstance()Landroid/webkit/SslCertLookupTable;
    .registers 1

    #@0
    .prologue
    .line 37
    sget-object v0, Landroid/webkit/SslCertLookupTable;->sTable:Landroid/webkit/SslCertLookupTable;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 38
    new-instance v0, Landroid/webkit/SslCertLookupTable;

    #@6
    invoke-direct {v0}, Landroid/webkit/SslCertLookupTable;-><init>()V

    #@9
    sput-object v0, Landroid/webkit/SslCertLookupTable;->sTable:Landroid/webkit/SslCertLookupTable;

    #@b
    .line 40
    :cond_b
    sget-object v0, Landroid/webkit/SslCertLookupTable;->sTable:Landroid/webkit/SslCertLookupTable;

    #@d
    return-object v0
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Landroid/webkit/SslCertLookupTable;->table:Landroid/os/Bundle;

    #@2
    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    #@5
    .line 71
    return-void
.end method

.method public isAllowed(Landroid/net/http/SslError;)Z
    .registers 7
    .parameter "sslError"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 62
    :try_start_1
    new-instance v3, Ljava/net/URL;

    #@3
    invoke-virtual {p1}, Landroid/net/http/SslError;->getUrl()Ljava/lang/String;

    #@6
    move-result-object v4

    #@7
    invoke-direct {v3, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@a
    invoke-virtual {v3}, Ljava/net/URL;->getHost()Ljava/lang/String;
    :try_end_d
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_d} :catch_24

    #@d
    move-result-object v1

    #@e
    .line 66
    .local v1, host:Ljava/lang/String;
    iget-object v3, p0, Landroid/webkit/SslCertLookupTable;->table:Landroid/os/Bundle;

    #@10
    invoke-virtual {v3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_23

    #@16
    invoke-virtual {p1}, Landroid/net/http/SslError;->getPrimaryError()I

    #@19
    move-result v3

    #@1a
    iget-object v4, p0, Landroid/webkit/SslCertLookupTable;->table:Landroid/os/Bundle;

    #@1c
    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@1f
    move-result v4

    #@20
    if-gt v3, v4, :cond_23

    #@22
    const/4 v2, 0x1

    #@23
    .end local v1           #host:Ljava/lang/String;
    :cond_23
    :goto_23
    return v2

    #@24
    .line 63
    :catch_24
    move-exception v0

    #@25
    .line 64
    .local v0, e:Ljava/net/MalformedURLException;
    goto :goto_23
.end method

.method public setIsAllowed(Landroid/net/http/SslError;)V
    .registers 6
    .parameter "sslError"

    #@0
    .prologue
    .line 50
    :try_start_0
    new-instance v2, Ljava/net/URL;

    #@2
    invoke-virtual {p1}, Landroid/net/http/SslError;->getUrl()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@9
    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;
    :try_end_c
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_c} :catch_17

    #@c
    move-result-object v1

    #@d
    .line 54
    .local v1, host:Ljava/lang/String;
    iget-object v2, p0, Landroid/webkit/SslCertLookupTable;->table:Landroid/os/Bundle;

    #@f
    invoke-virtual {p1}, Landroid/net/http/SslError;->getPrimaryError()I

    #@12
    move-result v3

    #@13
    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@16
    .line 55
    .end local v1           #host:Ljava/lang/String;
    :goto_16
    return-void

    #@17
    .line 51
    :catch_17
    move-exception v0

    #@18
    .line 52
    .local v0, e:Ljava/net/MalformedURLException;
    goto :goto_16
.end method
