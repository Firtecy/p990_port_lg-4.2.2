.class Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$2;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "AccessibilityInjector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;


# direct methods
.method constructor <init>(Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 780
    iput-object p1, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$2;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@2
    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .registers 2
    .parameter "utteranceId"

    #@0
    .prologue
    .line 797
    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .registers 5
    .parameter "utteranceId"

    #@0
    .prologue
    .line 788
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_2c

    #@6
    .line 789
    invoke-static {}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->access$900()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "["

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget-object v2, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$2;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@17
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    #@1a
    move-result v2

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, "] Failed to speak utterance"

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 792
    :cond_2c
    return-void
.end method

.method public onStart(Ljava/lang/String;)V
    .registers 2
    .parameter "utteranceId"

    #@0
    .prologue
    .line 784
    return-void
.end method
