.class public Landroid/webkit/HTML5VideoView;
.super Ljava/lang/Object;
.source "HTML5VideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/HTML5VideoView$TimeupdateTask;
    }
.end annotation


# static fields
.field protected static final COOKIE:Ljava/lang/String; = "Cookie"

.field protected static final HIDE_URL_LOGS:Ljava/lang/String; = "x-hide-urls-from-log"

.field protected static final LOGTAG:Ljava/lang/String; = "HTML5VideoView"

.field static final STATE_INITIALIZED:I = 0x0

.field static final STATE_PLAYING:I = 0x3

.field static final STATE_PREPARED:I = 0x2

.field static final STATE_PREPARING:I = 0x1

.field static final STATE_RELEASED:I = 0x5

.field static final STATE_RESETTED:I = 0x4

.field private static final TIMEUPDATE_PERIOD:I = 0xfa

.field protected static mCurrentState:I

.field protected static mPlayer:Landroid/media/MediaPlayer;

.field protected static mTimer:Ljava/util/Timer;


# instance fields
.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field protected mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mPauseDuringPreparing:Z

.field public mPlayerBuffering:Z

.field protected mProxy:Landroid/webkit/HTML5VideoViewProxy;

.field protected mSaveSeekTime:I

.field private mSkipPrepare:Z

.field private mStartWhenPrepared:Z

.field protected mUri:Landroid/net/Uri;

.field protected mVideoLayerId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 70
    const/4 v0, 0x0

    #@1
    sput-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@3
    .line 71
    const/4 v0, -0x1

    #@4
    sput v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@6
    return-void
.end method

.method protected constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 244
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 85
    iput-boolean v1, p0, Landroid/webkit/HTML5VideoView;->mSkipPrepare:Z

    #@6
    .line 88
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioManager:Landroid/media/AudioManager;

    #@9
    .line 89
    new-instance v0, Landroid/webkit/HTML5VideoView$1;

    #@b
    invoke-direct {v0, p0}, Landroid/webkit/HTML5VideoView$1;-><init>(Landroid/webkit/HTML5VideoView;)V

    #@e
    iput-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    #@10
    .line 410
    iput-boolean v1, p0, Landroid/webkit/HTML5VideoView;->mPlayerBuffering:Z

    #@12
    .line 431
    iput-boolean v1, p0, Landroid/webkit/HTML5VideoView;->mStartWhenPrepared:Z

    #@14
    .line 245
    return-void
.end method

.method protected static generateHeaders(Ljava/lang/String;Landroid/webkit/HTML5VideoViewProxy;)Ljava/util/Map;
    .registers 7
    .parameter "url"
    .parameter "proxy"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/webkit/HTML5VideoViewProxy;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 249
    invoke-virtual {p1}, Landroid/webkit/HTML5VideoViewProxy;->getWebView()Landroid/webkit/WebViewClassic;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->isPrivateBrowsingEnabled()Z

    #@7
    move-result v2

    #@8
    .line 250
    .local v2, isPrivate:Z
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v3, p0, v2}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;Z)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 251
    .local v0, cookieValue:Ljava/lang/String;
    new-instance v1, Ljava/util/HashMap;

    #@12
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@15
    .line 252
    .local v1, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_1c

    #@17
    .line 253
    const-string v3, "Cookie"

    #@19
    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    .line 255
    :cond_1c
    if-eqz v2, :cond_27

    #@1e
    .line 256
    const-string/jumbo v3, "x-hide-urls-from-log"

    #@21
    const-string/jumbo v4, "true"

    #@24
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    .line 259
    :cond_27
    return-object v1
.end method

.method public static release()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x5

    #@1
    .line 211
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@3
    if-eqz v0, :cond_11

    #@5
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@7
    if-eq v0, v1, :cond_11

    #@9
    .line 212
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@b
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    #@e
    .line 213
    const/4 v0, 0x0

    #@f
    sput-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@11
    .line 215
    :cond_11
    sput v1, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@13
    .line 216
    return-void
.end method


# virtual methods
.method public decideDisplayMode()V
    .registers 1

    #@0
    .prologue
    .line 396
    return-void
.end method

.method public deleteSurfaceTexture()V
    .registers 1

    #@0
    .prologue
    .line 403
    return-void
.end method

.method public enterFullScreenVideoState(ILandroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebViewClassic;)V
    .registers 4
    .parameter "layerId"
    .parameter "proxy"
    .parameter "webView"

    #@0
    .prologue
    .line 389
    return-void
.end method

.method public fullScreenExited()Z
    .registers 2

    #@0
    .prologue
    .line 428
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getCurrentPosition()I
    .registers 3

    #@0
    .prologue
    .line 164
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_c

    #@5
    .line 165
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@7
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    #@a
    move-result v0

    #@b
    .line 167
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public getCurrentState()I
    .registers 2

    #@0
    .prologue
    .line 343
    invoke-virtual {p0}, Landroid/webkit/HTML5VideoView;->isPlaying()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 344
    const/4 v0, 0x3

    #@7
    .line 346
    :goto_7
    return v0

    #@8
    :cond_8
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@a
    goto :goto_7
.end method

.method public getDuration()I
    .registers 3

    #@0
    .prologue
    .line 156
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_c

    #@5
    .line 157
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@7
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    #@a
    move-result v0

    #@b
    .line 159
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, -0x1

    #@d
    goto :goto_b
.end method

.method public getHeaders()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 450
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mHeaders:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method public getPauseDuringPreparing()Z
    .registers 2

    #@0
    .prologue
    .line 223
    iget-boolean v0, p0, Landroid/webkit/HTML5VideoView;->mPauseDuringPreparing:Z

    #@2
    return v0
.end method

.method public getPlayerBuffering()Z
    .registers 2

    #@0
    .prologue
    .line 413
    iget-boolean v0, p0, Landroid/webkit/HTML5VideoView;->mPlayerBuffering:Z

    #@2
    return v0
.end method

.method public getReadyToUseSurfTex()Z
    .registers 2

    #@0
    .prologue
    .line 399
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getSaveSeekTime()I
    .registers 2

    #@0
    .prologue
    .line 446
    iget v0, p0, Landroid/webkit/HTML5VideoView;->mSaveSeekTime:I

    #@2
    return v0
.end method

.method public getStartWhenPrepared()Z
    .registers 2

    #@0
    .prologue
    .line 438
    iget-boolean v0, p0, Landroid/webkit/HTML5VideoView;->mStartWhenPrepared:Z

    #@2
    return v0
.end method

.method public getTextureName()I
    .registers 2

    #@0
    .prologue
    .line 406
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getVideoLayerId()I
    .registers 2

    #@0
    .prologue
    .line 338
    iget v0, p0, Landroid/webkit/HTML5VideoView;->mVideoLayerId:I

    #@2
    return v0
.end method

.method public init(IIZ)V
    .registers 7
    .parameter "videoLayerId"
    .parameter "position"
    .parameter "skipPrepare"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 228
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@4
    if-nez v0, :cond_f

    #@6
    .line 229
    new-instance v0, Landroid/media/MediaPlayer;

    #@8
    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    #@b
    sput-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@d
    .line 230
    sput v1, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@f
    .line 232
    :cond_f
    iput-boolean p3, p0, Landroid/webkit/HTML5VideoView;->mSkipPrepare:Z

    #@11
    .line 234
    iget-boolean v0, p0, Landroid/webkit/HTML5VideoView;->mSkipPrepare:Z

    #@13
    if-nez v0, :cond_17

    #@15
    .line 235
    sput v1, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@17
    .line 237
    :cond_17
    iput-object v2, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@19
    .line 238
    iput p1, p0, Landroid/webkit/HTML5VideoView;->mVideoLayerId:I

    #@1b
    .line 239
    iput p2, p0, Landroid/webkit/HTML5VideoView;->mSaveSeekTime:I

    #@1d
    .line 240
    sput-object v2, Landroid/webkit/HTML5VideoView;->mTimer:Ljava/util/Timer;

    #@1f
    .line 241
    iput-boolean v1, p0, Landroid/webkit/HTML5VideoView;->mPauseDuringPreparing:Z

    #@21
    .line 242
    return-void
.end method

.method public isFullScreenMode()Z
    .registers 2

    #@0
    .prologue
    .line 392
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isPlaying()Z
    .registers 3

    #@0
    .prologue
    .line 178
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_c

    #@5
    .line 179
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@7
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@a
    move-result v0

    #@b
    .line 181
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isReleased()Z
    .registers 3

    #@0
    .prologue
    .line 219
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@2
    const/4 v1, 0x5

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .registers 3
    .parameter "mp"

    #@0
    .prologue
    .line 365
    const/4 v0, 0x2

    #@1
    sput v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@3
    .line 366
    iget v0, p0, Landroid/webkit/HTML5VideoView;->mSaveSeekTime:I

    #@5
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoView;->seekTo(I)V

    #@8
    .line 367
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 368
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@e
    invoke-virtual {v0, p1}, Landroid/webkit/HTML5VideoViewProxy;->onPrepared(Landroid/media/MediaPlayer;)V

    #@11
    .line 370
    :cond_11
    iget-boolean v0, p0, Landroid/webkit/HTML5VideoView;->mPauseDuringPreparing:Z

    #@13
    if-eqz v0, :cond_1d

    #@15
    .line 371
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@17
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoView;->pauseAndDispatch(Landroid/webkit/HTML5VideoViewProxy;)V

    #@1a
    .line 372
    const/4 v0, 0x0

    #@1b
    iput-boolean v0, p0, Landroid/webkit/HTML5VideoView;->mPauseDuringPreparing:Z

    #@1d
    .line 374
    :cond_1d
    return-void
.end method

.method public pause()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 135
    invoke-virtual {p0}, Landroid/webkit/HTML5VideoView;->isPlaying()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_29

    #@7
    .line 136
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@9
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    #@c
    .line 139
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioManager:Landroid/media/AudioManager;

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 140
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioManager:Landroid/media/AudioManager;

    #@12
    iget-object v1, p0, Landroid/webkit/HTML5VideoView;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    #@14
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    #@17
    .line 148
    :cond_17
    :goto_17
    sget-object v0, Landroid/webkit/HTML5VideoView;->mTimer:Ljava/util/Timer;

    #@19
    if-eqz v0, :cond_28

    #@1b
    .line 149
    sget-object v0, Landroid/webkit/HTML5VideoView;->mTimer:Ljava/util/Timer;

    #@1d
    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    #@20
    .line 150
    sget-object v0, Landroid/webkit/HTML5VideoView;->mTimer:Ljava/util/Timer;

    #@22
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    #@25
    .line 151
    const/4 v0, 0x0

    #@26
    sput-object v0, Landroid/webkit/HTML5VideoView;->mTimer:Ljava/util/Timer;

    #@28
    .line 153
    :cond_28
    return-void

    #@29
    .line 144
    :cond_29
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@2b
    if-ne v0, v1, :cond_17

    #@2d
    .line 145
    iput-boolean v1, p0, Landroid/webkit/HTML5VideoView;->mPauseDuringPreparing:Z

    #@2f
    goto :goto_17
.end method

.method public pauseAndDispatch(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 2
    .parameter "proxy"

    #@0
    .prologue
    .line 378
    invoke-virtual {p0}, Landroid/webkit/HTML5VideoView;->pause()V

    #@3
    .line 379
    if-eqz p1, :cond_8

    #@5
    .line 380
    invoke-virtual {p1}, Landroid/webkit/HTML5VideoViewProxy;->dispatchOnPaused()V

    #@8
    .line 382
    :cond_8
    return-void
.end method

.method public prepareDataAndDisplayMode(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 2
    .parameter "proxy"

    #@0
    .prologue
    .line 325
    invoke-virtual {p0}, Landroid/webkit/HTML5VideoView;->decideDisplayMode()V

    #@3
    .line 327
    invoke-virtual {p0, p1}, Landroid/webkit/HTML5VideoView;->setOnCompletionListener(Landroid/webkit/HTML5VideoViewProxy;)V

    #@6
    .line 328
    invoke-virtual {p0, p1}, Landroid/webkit/HTML5VideoView;->setOnPreparedListener(Landroid/webkit/HTML5VideoViewProxy;)V

    #@9
    .line 329
    invoke-virtual {p0, p1}, Landroid/webkit/HTML5VideoView;->setOnErrorListener(Landroid/webkit/HTML5VideoViewProxy;)V

    #@c
    .line 330
    invoke-virtual {p0, p1}, Landroid/webkit/HTML5VideoView;->setOnInfoListener(Landroid/webkit/HTML5VideoViewProxy;)V

    #@f
    .line 332
    invoke-virtual {p0, p1}, Landroid/webkit/HTML5VideoView;->prepareDataCommon(Landroid/webkit/HTML5VideoViewProxy;)V

    #@12
    .line 333
    return-void
.end method

.method public prepareDataCommon(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 7
    .parameter "proxy"

    #@0
    .prologue
    .line 292
    iget-boolean v1, p0, Landroid/webkit/HTML5VideoView;->mSkipPrepare:Z

    #@2
    if-nez v1, :cond_2e

    #@4
    .line 294
    :try_start_4
    sget-object v1, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@6
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    #@9
    .line 295
    sget-object v1, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@b
    invoke-virtual {p1}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@e
    move-result-object v2

    #@f
    iget-object v3, p0, Landroid/webkit/HTML5VideoView;->mUri:Landroid/net/Uri;

    #@11
    iget-object v4, p0, Landroid/webkit/HTML5VideoView;->mHeaders:Ljava/util/Map;

    #@13
    invoke-virtual {v1, v2, v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    #@16
    .line 296
    sget-object v1, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@18
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_1b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_1b} :catch_1f
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_1b} :catch_24
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_1b} :catch_29

    #@1b
    .line 304
    :goto_1b
    const/4 v1, 0x1

    #@1c
    sput v1, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@1e
    .line 314
    :goto_1e
    return-void

    #@1f
    .line 297
    :catch_1f
    move-exception v0

    #@20
    .line 298
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@23
    goto :goto_1b

    #@24
    .line 299
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    :catch_24
    move-exception v0

    #@25
    .line 300
    .local v0, e:Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@28
    goto :goto_1b

    #@29
    .line 301
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :catch_29
    move-exception v0

    #@2a
    .line 302
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@2d
    goto :goto_1b

    #@2e
    .line 309
    .end local v0           #e:Ljava/io/IOException;
    :cond_2e
    sget v1, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@30
    const/4 v2, 0x2

    #@31
    if-lt v1, v2, :cond_38

    #@33
    .line 310
    sget-object v1, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@35
    invoke-virtual {p0, v1}, Landroid/webkit/HTML5VideoView;->onPrepared(Landroid/media/MediaPlayer;)V

    #@38
    .line 312
    :cond_38
    const/4 v1, 0x0

    #@39
    iput-boolean v1, p0, Landroid/webkit/HTML5VideoView;->mSkipPrepare:Z

    #@3b
    goto :goto_1e
.end method

.method public reprepareData(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 3
    .parameter "proxy"

    #@0
    .prologue
    .line 317
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@2
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    #@5
    .line 318
    invoke-virtual {p0, p1}, Landroid/webkit/HTML5VideoView;->prepareDataCommon(Landroid/webkit/HTML5VideoViewProxy;)V

    #@8
    .line 319
    return-void
.end method

.method public reset()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x4

    #@1
    .line 186
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@3
    if-ge v0, v2, :cond_15

    #@5
    .line 187
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@7
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    #@a
    .line 190
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioManager:Landroid/media/AudioManager;

    #@c
    if-eqz v0, :cond_15

    #@e
    .line 191
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioManager:Landroid/media/AudioManager;

    #@10
    iget-object v1, p0, Landroid/webkit/HTML5VideoView;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    #@12
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    #@15
    .line 195
    :cond_15
    sput v2, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@17
    .line 196
    return-void
.end method

.method public seekTo(I)V
    .registers 4
    .parameter "pos"

    #@0
    .prologue
    .line 171
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_b

    #@5
    .line 172
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@7
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    #@a
    .line 175
    :goto_a
    return-void

    #@b
    .line 174
    :cond_b
    iput p1, p0, Landroid/webkit/HTML5VideoView;->mSaveSeekTime:I

    #@d
    goto :goto_a
.end method

.method public setOnCompletionListener(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 3
    .parameter "proxy"

    #@0
    .prologue
    .line 270
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@2
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    #@5
    .line 271
    return-void
.end method

.method public setOnErrorListener(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 3
    .parameter "proxy"

    #@0
    .prologue
    .line 274
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@2
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    #@5
    .line 275
    return-void
.end method

.method public setOnInfoListener(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 3
    .parameter "proxy"

    #@0
    .prologue
    .line 288
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@2
    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    #@5
    .line 289
    return-void
.end method

.method public setOnPreparedListener(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 4
    .parameter "proxy"

    #@0
    .prologue
    .line 278
    iput-object p1, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@2
    .line 280
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@4
    if-eqz v0, :cond_16

    #@6
    .line 281
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@8
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@b
    move-result-object v0

    #@c
    const-string v1, "audio"

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/media/AudioManager;

    #@14
    iput-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioManager:Landroid/media/AudioManager;

    #@16
    .line 284
    :cond_16
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@18
    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    #@1b
    .line 285
    return-void
.end method

.method public setPlayerBuffering(Z)V
    .registers 2
    .parameter "playerBuffering"

    #@0
    .prologue
    .line 417
    iput-boolean p1, p0, Landroid/webkit/HTML5VideoView;->mPlayerBuffering:Z

    #@2
    .line 418
    invoke-virtual {p0, p1}, Landroid/webkit/HTML5VideoView;->switchProgressView(Z)V

    #@5
    .line 419
    return-void
.end method

.method public setStartWhenPrepared(Z)V
    .registers 2
    .parameter "willPlay"

    #@0
    .prologue
    .line 434
    iput-boolean p1, p0, Landroid/webkit/HTML5VideoView;->mStartWhenPrepared:Z

    #@2
    .line 435
    return-void
.end method

.method public setVideoURI(Ljava/lang/String;Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 4
    .parameter "uri"
    .parameter "proxy"

    #@0
    .prologue
    .line 264
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/webkit/HTML5VideoView;->mUri:Landroid/net/Uri;

    #@6
    .line 265
    invoke-static {p1, p2}, Landroid/webkit/HTML5VideoView;->generateHeaders(Ljava/lang/String;Landroid/webkit/HTML5VideoViewProxy;)Ljava/util/Map;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/webkit/HTML5VideoView;->mHeaders:Ljava/util/Map;

    #@c
    .line 266
    return-void
.end method

.method public showControllerInFullScreen()V
    .registers 1

    #@0
    .prologue
    .line 442
    return-void
.end method

.method public start()V
    .registers 7

    #@0
    .prologue
    const-wide/16 v2, 0xfa

    #@2
    .line 112
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@4
    const/4 v1, 0x2

    #@5
    if-ne v0, v1, :cond_35

    #@7
    .line 115
    sget-object v0, Landroid/webkit/HTML5VideoView;->mTimer:Ljava/util/Timer;

    #@9
    if-nez v0, :cond_1f

    #@b
    .line 117
    new-instance v0, Ljava/util/Timer;

    #@d
    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    #@10
    sput-object v0, Landroid/webkit/HTML5VideoView;->mTimer:Ljava/util/Timer;

    #@12
    .line 118
    sget-object v0, Landroid/webkit/HTML5VideoView;->mTimer:Ljava/util/Timer;

    #@14
    new-instance v1, Landroid/webkit/HTML5VideoView$TimeupdateTask;

    #@16
    iget-object v4, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@18
    invoke-direct {v1, v4}, Landroid/webkit/HTML5VideoView$TimeupdateTask;-><init>(Landroid/webkit/HTML5VideoViewProxy;)V

    #@1b
    move-wide v4, v2

    #@1c
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    #@1f
    .line 123
    :cond_1f
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioManager:Landroid/media/AudioManager;

    #@21
    if-eqz v0, :cond_2c

    #@23
    .line 124
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioManager:Landroid/media/AudioManager;

    #@25
    iget-object v1, p0, Landroid/webkit/HTML5VideoView;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    #@27
    const/4 v2, 0x3

    #@28
    const/4 v3, 0x1

    #@29
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    #@2c
    .line 129
    :cond_2c
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@2e
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    #@31
    .line 130
    const/4 v0, 0x0

    #@32
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoView;->setPlayerBuffering(Z)V

    #@35
    .line 132
    :cond_35
    return-void
.end method

.method public stopPlayback()V
    .registers 3

    #@0
    .prologue
    .line 199
    sget v0, Landroid/webkit/HTML5VideoView;->mCurrentState:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_15

    #@5
    .line 200
    sget-object v0, Landroid/webkit/HTML5VideoView;->mPlayer:Landroid/media/MediaPlayer;

    #@7
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    #@a
    .line 203
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioManager:Landroid/media/AudioManager;

    #@c
    if-eqz v0, :cond_15

    #@e
    .line 204
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mAudioManager:Landroid/media/AudioManager;

    #@10
    iget-object v1, p0, Landroid/webkit/HTML5VideoView;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    #@12
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    #@15
    .line 208
    :cond_15
    return-void
.end method

.method protected switchProgressView(Z)V
    .registers 2
    .parameter "playerBuffering"

    #@0
    .prologue
    .line 424
    return-void
.end method
