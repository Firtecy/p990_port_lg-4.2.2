.class Landroid/webkit/BrowserFrame$1;
.super Landroid/webkit/HttpAuthHandler;
.source "BrowserFrame.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/BrowserFrame;->didReceiveAuthenticationChallenge(ILjava/lang/String;Ljava/lang/String;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/BrowserFrame;

.field final synthetic val$handle:I

.field final synthetic val$suppressDialog:Z

.field final synthetic val$useCachedCredentials:Z


# direct methods
.method constructor <init>(Landroid/webkit/BrowserFrame;ZIZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 1068
    iput-object p1, p0, Landroid/webkit/BrowserFrame$1;->this$0:Landroid/webkit/BrowserFrame;

    #@2
    iput-boolean p2, p0, Landroid/webkit/BrowserFrame$1;->val$useCachedCredentials:Z

    #@4
    iput p3, p0, Landroid/webkit/BrowserFrame$1;->val$handle:I

    #@6
    iput-boolean p4, p0, Landroid/webkit/BrowserFrame$1;->val$suppressDialog:Z

    #@8
    invoke-direct {p0}, Landroid/webkit/HttpAuthHandler;-><init>()V

    #@b
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 3

    #@0
    .prologue
    .line 1082
    iget-object v0, p0, Landroid/webkit/BrowserFrame$1;->this$0:Landroid/webkit/BrowserFrame;

    #@2
    iget v1, p0, Landroid/webkit/BrowserFrame$1;->val$handle:I

    #@4
    invoke-static {v0, v1}, Landroid/webkit/BrowserFrame;->access$100(Landroid/webkit/BrowserFrame;I)V

    #@7
    .line 1083
    return-void
.end method

.method public proceed(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    .line 1077
    iget-object v0, p0, Landroid/webkit/BrowserFrame$1;->this$0:Landroid/webkit/BrowserFrame;

    #@2
    iget v1, p0, Landroid/webkit/BrowserFrame$1;->val$handle:I

    #@4
    invoke-static {v0, v1, p1, p2}, Landroid/webkit/BrowserFrame;->access$000(Landroid/webkit/BrowserFrame;ILjava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1078
    return-void
.end method

.method public suppressDialog()Z
    .registers 2

    #@0
    .prologue
    .line 1087
    iget-boolean v0, p0, Landroid/webkit/BrowserFrame$1;->val$suppressDialog:Z

    #@2
    return v0
.end method

.method public useHttpAuthUsernamePassword()Z
    .registers 2

    #@0
    .prologue
    .line 1072
    iget-boolean v0, p0, Landroid/webkit/BrowserFrame$1;->val$useCachedCredentials:Z

    #@2
    return v0
.end method
