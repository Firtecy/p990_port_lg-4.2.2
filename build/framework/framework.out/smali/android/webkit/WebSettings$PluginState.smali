.class public final enum Landroid/webkit/WebSettings$PluginState;
.super Ljava/lang/Enum;
.source "WebSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PluginState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/webkit/WebSettings$PluginState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/webkit/WebSettings$PluginState;

.field public static final enum OFF:Landroid/webkit/WebSettings$PluginState;

.field public static final enum ON:Landroid/webkit/WebSettings$PluginState;

.field public static final enum ON_DEMAND:Landroid/webkit/WebSettings$PluginState;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 148
    new-instance v0, Landroid/webkit/WebSettings$PluginState;

    #@5
    const-string v1, "ON"

    #@7
    invoke-direct {v0, v1, v2}, Landroid/webkit/WebSettings$PluginState;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    #@c
    .line 149
    new-instance v0, Landroid/webkit/WebSettings$PluginState;

    #@e
    const-string v1, "ON_DEMAND"

    #@10
    invoke-direct {v0, v1, v3}, Landroid/webkit/WebSettings$PluginState;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Landroid/webkit/WebSettings$PluginState;->ON_DEMAND:Landroid/webkit/WebSettings$PluginState;

    #@15
    .line 150
    new-instance v0, Landroid/webkit/WebSettings$PluginState;

    #@17
    const-string v1, "OFF"

    #@19
    invoke-direct {v0, v1, v4}, Landroid/webkit/WebSettings$PluginState;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    #@1e
    .line 147
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Landroid/webkit/WebSettings$PluginState;

    #@21
    sget-object v1, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Landroid/webkit/WebSettings$PluginState;->ON_DEMAND:Landroid/webkit/WebSettings$PluginState;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Landroid/webkit/WebSettings$PluginState;->$VALUES:[Landroid/webkit/WebSettings$PluginState;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/webkit/WebSettings$PluginState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 147
    const-class v0, Landroid/webkit/WebSettings$PluginState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/webkit/WebSettings$PluginState;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/webkit/WebSettings$PluginState;
    .registers 1

    #@0
    .prologue
    .line 147
    sget-object v0, Landroid/webkit/WebSettings$PluginState;->$VALUES:[Landroid/webkit/WebSettings$PluginState;

    #@2
    invoke-virtual {v0}, [Landroid/webkit/WebSettings$PluginState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/webkit/WebSettings$PluginState;

    #@8
    return-object v0
.end method
