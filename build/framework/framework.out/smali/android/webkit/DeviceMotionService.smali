.class final Landroid/webkit/DeviceMotionService;
.super Ljava/lang/Object;
.source "DeviceMotionService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final INTERVAL_MILLIS:I = 0x64


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHaveSentErrorEvent:Z

.field private mIsRunning:Z

.field private mLastAcceleration:[F

.field private mManager:Landroid/webkit/DeviceMotionAndOrientationManager;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mUpdateRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 31
    const-class v0, Landroid/webkit/DeviceMotionService;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/DeviceMotionService;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>(Landroid/webkit/DeviceMotionAndOrientationManager;Landroid/content/Context;)V
    .registers 4
    .parameter "manager"
    .parameter "context"

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 44
    iput-object p1, p0, Landroid/webkit/DeviceMotionService;->mManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@5
    .line 45
    sget-boolean v0, Landroid/webkit/DeviceMotionService;->$assertionsDisabled:Z

    #@7
    if-nez v0, :cond_13

    #@9
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@b
    if-nez v0, :cond_13

    #@d
    new-instance v0, Ljava/lang/AssertionError;

    #@f
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@12
    throw v0

    #@13
    .line 46
    :cond_13
    iput-object p2, p0, Landroid/webkit/DeviceMotionService;->mContext:Landroid/content/Context;

    #@15
    .line 47
    sget-boolean v0, Landroid/webkit/DeviceMotionService;->$assertionsDisabled:Z

    #@17
    if-nez v0, :cond_23

    #@19
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mContext:Landroid/content/Context;

    #@1b
    if-nez v0, :cond_23

    #@1d
    new-instance v0, Ljava/lang/AssertionError;

    #@1f
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@22
    throw v0

    #@23
    .line 48
    :cond_23
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/DeviceMotionService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-boolean v0, p0, Landroid/webkit/DeviceMotionService;->mIsRunning:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/webkit/DeviceMotionService;)Landroid/webkit/DeviceMotionAndOrientationManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/webkit/DeviceMotionService;)[F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mLastAcceleration:[F

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/webkit/DeviceMotionService;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mUpdateRunnable:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/webkit/DeviceMotionService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Landroid/webkit/DeviceMotionService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 31
    iput-boolean p1, p0, Landroid/webkit/DeviceMotionService;->mHaveSentErrorEvent:Z

    #@2
    return p1
.end method

.method private createHandler()V
    .registers 2

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mHandler:Landroid/os/Handler;

    #@2
    if-nez v0, :cond_8

    #@4
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@6
    if-nez v0, :cond_9

    #@8
    .line 111
    :cond_8
    :goto_8
    return-void

    #@9
    .line 98
    :cond_9
    new-instance v0, Landroid/os/Handler;

    #@b
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@e
    iput-object v0, p0, Landroid/webkit/DeviceMotionService;->mHandler:Landroid/os/Handler;

    #@10
    .line 99
    new-instance v0, Landroid/webkit/DeviceMotionService$2;

    #@12
    invoke-direct {v0, p0}, Landroid/webkit/DeviceMotionService$2;-><init>(Landroid/webkit/DeviceMotionService;)V

    #@15
    iput-object v0, p0, Landroid/webkit/DeviceMotionService;->mUpdateRunnable:Ljava/lang/Runnable;

    #@17
    goto :goto_8
.end method

.method private getSensorManager()Landroid/hardware/SensorManager;
    .registers 3

    #@0
    .prologue
    .line 130
    sget-boolean v0, Landroid/webkit/DeviceMotionService;->$assertionsDisabled:Z

    #@2
    if-nez v0, :cond_1a

    #@4
    const-string v0, "WebViewCoreThread"

    #@6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1a

    #@14
    new-instance v0, Ljava/lang/AssertionError;

    #@16
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@19
    throw v0

    #@1a
    .line 131
    :cond_1a
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mSensorManager:Landroid/hardware/SensorManager;

    #@1c
    if-nez v0, :cond_2b

    #@1e
    .line 132
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mContext:Landroid/content/Context;

    #@20
    const-string/jumbo v1, "sensor"

    #@23
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    check-cast v0, Landroid/hardware/SensorManager;

    #@29
    iput-object v0, p0, Landroid/webkit/DeviceMotionService;->mSensorManager:Landroid/hardware/SensorManager;

    #@2b
    .line 134
    :cond_2b
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mSensorManager:Landroid/hardware/SensorManager;

    #@2d
    return-object v0
.end method

.method private registerForAccelerometerSensor()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 138
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->getSensorManager()Landroid/hardware/SensorManager;

    #@4
    move-result-object v2

    #@5
    const/4 v3, 0x1

    #@6
    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    #@9
    move-result-object v0

    #@a
    .line 139
    .local v0, sensors:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_11

    #@10
    .line 144
    :goto_10
    return v1

    #@11
    .line 142
    :cond_11
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->createHandler()V

    #@14
    .line 144
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->getSensorManager()Landroid/hardware/SensorManager;

    #@17
    move-result-object v2

    #@18
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Landroid/hardware/Sensor;

    #@1e
    const/4 v3, 0x2

    #@1f
    iget-object v4, p0, Landroid/webkit/DeviceMotionService;->mHandler:Landroid/os/Handler;

    #@21
    invoke-virtual {v2, p0, v1, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    #@24
    move-result v1

    #@25
    goto :goto_10
.end method

.method private registerForSensor()V
    .registers 2

    #@0
    .prologue
    .line 124
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->registerForAccelerometerSensor()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 125
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->sendErrorEvent()V

    #@9
    .line 127
    :cond_9
    return-void
.end method

.method private sendErrorEvent()V
    .registers 3

    #@0
    .prologue
    .line 75
    sget-boolean v0, Landroid/webkit/DeviceMotionService;->$assertionsDisabled:Z

    #@2
    if-nez v0, :cond_1a

    #@4
    const-string v0, "WebViewCoreThread"

    #@6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1a

    #@14
    new-instance v0, Ljava/lang/AssertionError;

    #@16
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@19
    throw v0

    #@1a
    .line 77
    :cond_1a
    iget-boolean v0, p0, Landroid/webkit/DeviceMotionService;->mHaveSentErrorEvent:Z

    #@1c
    if-eqz v0, :cond_1f

    #@1e
    .line 91
    :goto_1e
    return-void

    #@1f
    .line 79
    :cond_1f
    const/4 v0, 0x1

    #@20
    iput-boolean v0, p0, Landroid/webkit/DeviceMotionService;->mHaveSentErrorEvent:Z

    #@22
    .line 80
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->createHandler()V

    #@25
    .line 81
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mHandler:Landroid/os/Handler;

    #@27
    new-instance v1, Landroid/webkit/DeviceMotionService$1;

    #@29
    invoke-direct {v1, p0}, Landroid/webkit/DeviceMotionService$1;-><init>(Landroid/webkit/DeviceMotionService;)V

    #@2c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@2f
    goto :goto_1e
.end method

.method private startSendingUpdates()V
    .registers 2

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->createHandler()V

    #@3
    .line 115
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mUpdateRunnable:Ljava/lang/Runnable;

    #@5
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@8
    .line 116
    return-void
.end method

.method private stopSendingUpdates()V
    .registers 3

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Landroid/webkit/DeviceMotionService;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Landroid/webkit/DeviceMotionService;->mUpdateRunnable:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 120
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Landroid/webkit/DeviceMotionService;->mLastAcceleration:[F

    #@a
    .line 121
    return-void
.end method

.method private unregisterFromSensor()V
    .registers 2

    #@0
    .prologue
    .line 149
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->getSensorManager()Landroid/hardware/SensorManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@7
    .line 150
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 5
    .parameter "sensor"
    .parameter "accuracy"

    #@0
    .prologue
    .line 176
    sget-boolean v0, Landroid/webkit/DeviceMotionService;->$assertionsDisabled:Z

    #@2
    if-nez v0, :cond_1a

    #@4
    const-string v0, "WebViewCoreThread"

    #@6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1a

    #@14
    new-instance v0, Ljava/lang/AssertionError;

    #@16
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@19
    throw v0

    #@1a
    .line 177
    :cond_1a
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 158
    sget-boolean v1, Landroid/webkit/DeviceMotionService;->$assertionsDisabled:Z

    #@3
    if-nez v1, :cond_11

    #@5
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    #@7
    array-length v1, v1

    #@8
    const/4 v2, 0x3

    #@9
    if-eq v1, v2, :cond_11

    #@b
    new-instance v1, Ljava/lang/AssertionError;

    #@d
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@10
    throw v1

    #@11
    .line 159
    :cond_11
    sget-boolean v1, Landroid/webkit/DeviceMotionService;->$assertionsDisabled:Z

    #@13
    if-nez v1, :cond_2b

    #@15
    const-string v1, "WebViewCoreThread"

    #@17
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v1

    #@23
    if-nez v1, :cond_2b

    #@25
    new-instance v1, Ljava/lang/AssertionError;

    #@27
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@2a
    throw v1

    #@2b
    .line 160
    :cond_2b
    sget-boolean v1, Landroid/webkit/DeviceMotionService;->$assertionsDisabled:Z

    #@2d
    if-nez v1, :cond_3d

    #@2f
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    #@31
    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    #@34
    move-result v1

    #@35
    if-eq v1, v0, :cond_3d

    #@37
    new-instance v1, Ljava/lang/AssertionError;

    #@39
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@3c
    throw v1

    #@3d
    .line 163
    :cond_3d
    iget-boolean v1, p0, Landroid/webkit/DeviceMotionService;->mIsRunning:Z

    #@3f
    if-nez v1, :cond_42

    #@41
    .line 172
    :cond_41
    :goto_41
    return-void

    #@42
    .line 167
    :cond_42
    iget-object v1, p0, Landroid/webkit/DeviceMotionService;->mLastAcceleration:[F

    #@44
    if-nez v1, :cond_50

    #@46
    .line 168
    .local v0, firstData:Z
    :goto_46
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    #@48
    iput-object v1, p0, Landroid/webkit/DeviceMotionService;->mLastAcceleration:[F

    #@4a
    .line 169
    if-eqz v0, :cond_41

    #@4c
    .line 170
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->startSendingUpdates()V

    #@4f
    goto :goto_41

    #@50
    .line 167
    .end local v0           #firstData:Z
    :cond_50
    const/4 v0, 0x0

    #@51
    goto :goto_46
.end method

.method public resume()V
    .registers 2

    #@0
    .prologue
    .line 69
    iget-boolean v0, p0, Landroid/webkit/DeviceMotionService;->mIsRunning:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 70
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->registerForSensor()V

    #@7
    .line 72
    :cond_7
    return-void
.end method

.method public start()V
    .registers 2

    #@0
    .prologue
    .line 51
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/webkit/DeviceMotionService;->mIsRunning:Z

    #@3
    .line 52
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->registerForSensor()V

    #@6
    .line 53
    return-void
.end method

.method public stop()V
    .registers 2

    #@0
    .prologue
    .line 56
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/webkit/DeviceMotionService;->mIsRunning:Z

    #@3
    .line 57
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->stopSendingUpdates()V

    #@6
    .line 58
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->unregisterFromSensor()V

    #@9
    .line 59
    return-void
.end method

.method public suspend()V
    .registers 2

    #@0
    .prologue
    .line 62
    iget-boolean v0, p0, Landroid/webkit/DeviceMotionService;->mIsRunning:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 63
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->stopSendingUpdates()V

    #@7
    .line 64
    invoke-direct {p0}, Landroid/webkit/DeviceMotionService;->unregisterFromSensor()V

    #@a
    .line 66
    :cond_a
    return-void
.end method
