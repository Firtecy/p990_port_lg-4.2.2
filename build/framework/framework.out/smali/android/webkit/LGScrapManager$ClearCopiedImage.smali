.class Landroid/webkit/LGScrapManager$ClearCopiedImage;
.super Landroid/os/AsyncTask;
.source "LGScrapManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/LGScrapManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ClearCopiedImage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/io/File;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 89
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/LGScrapManager$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    invoke-direct {p0}, Landroid/webkit/LGScrapManager$ClearCopiedImage;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 89
    check-cast p1, [Ljava/io/File;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/webkit/LGScrapManager$ClearCopiedImage;->doInBackground([Ljava/io/File;)Ljava/lang/Void;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public varargs doInBackground([Ljava/io/File;)Ljava/lang/Void;
    .registers 9
    .parameter "files"

    #@0
    .prologue
    .line 91
    if-eqz p1, :cond_2e

    #@2
    .line 92
    move-object v0, p1

    #@3
    .local v0, arr$:[Ljava/io/File;
    array-length v3, v0

    #@4
    .local v3, len$:I
    const/4 v2, 0x0

    #@5
    .local v2, i$:I
    :goto_5
    if-ge v2, v3, :cond_2e

    #@7
    aget-object v1, v0, v2

    #@9
    .line 93
    .local v1, f:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@c
    move-result v4

    #@d
    if-nez v4, :cond_2b

    #@f
    .line 94
    const-string v4, "LGScrapManager"

    #@11
    new-instance v5, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    const-string v6, " was not deleted"

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v5

    #@28
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 92
    :cond_2b
    add-int/lit8 v2, v2, 0x1

    #@2d
    goto :goto_5

    #@2e
    .line 98
    .end local v0           #arr$:[Ljava/io/File;
    .end local v1           #f:Ljava/io/File;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_2e
    const/4 v4, 0x0

    #@2f
    return-object v4
.end method
