.class Landroid/webkit/WebIconDatabaseClassic;
.super Landroid/webkit/WebIconDatabase;
.source "WebIconDatabaseClassic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebIconDatabaseClassic$1;,
        Landroid/webkit/WebIconDatabaseClassic$EventHandler;
    }
.end annotation


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "WebIconDatabase"

.field private static sIconDatabase:Landroid/webkit/WebIconDatabaseClassic;


# instance fields
.field private final mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;


# direct methods
.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 280
    invoke-direct {p0}, Landroid/webkit/WebIconDatabase;-><init>()V

    #@3
    .line 37
    new-instance v0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@5
    const/4 v1, 0x0

    #@6
    invoke-direct {v0, v1}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;-><init>(Landroid/webkit/WebIconDatabaseClassic$1;)V

    #@9
    iput-object v0, p0, Landroid/webkit/WebIconDatabaseClassic;->mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@b
    .line 280
    return-void
.end method

.method static synthetic access$100(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 31
    invoke-static {p0}, Landroid/webkit/WebIconDatabaseClassic;->nativeOpen(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$200()V
    .registers 0

    #@0
    .prologue
    .line 31
    invoke-static {}, Landroid/webkit/WebIconDatabaseClassic;->nativeClose()V

    #@3
    return-void
.end method

.method static synthetic access$300()V
    .registers 0

    #@0
    .prologue
    .line 31
    invoke-static {}, Landroid/webkit/WebIconDatabaseClassic;->nativeRemoveAllIcons()V

    #@3
    return-void
.end method

.method static synthetic access$600(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 31
    invoke-static {p0}, Landroid/webkit/WebIconDatabaseClassic;->nativeRetainIconForPageUrl(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 31
    invoke-static {p0}, Landroid/webkit/WebIconDatabaseClassic;->nativeReleaseIconForPageUrl(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    invoke-static {p0}, Landroid/webkit/WebIconDatabaseClassic;->nativeIconForPageUrl(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getInstance()Landroid/webkit/WebIconDatabaseClassic;
    .registers 1

    #@0
    .prologue
    .line 263
    sget-object v0, Landroid/webkit/WebIconDatabaseClassic;->sIconDatabase:Landroid/webkit/WebIconDatabaseClassic;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 264
    new-instance v0, Landroid/webkit/WebIconDatabaseClassic;

    #@6
    invoke-direct {v0}, Landroid/webkit/WebIconDatabaseClassic;-><init>()V

    #@9
    sput-object v0, Landroid/webkit/WebIconDatabaseClassic;->sIconDatabase:Landroid/webkit/WebIconDatabaseClassic;

    #@b
    .line 266
    :cond_b
    sget-object v0, Landroid/webkit/WebIconDatabaseClassic;->sIconDatabase:Landroid/webkit/WebIconDatabaseClassic;

    #@d
    return-object v0
.end method

.method private static native nativeClose()V
.end method

.method private static native nativeIconForPageUrl(Ljava/lang/String;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeOpen(Ljava/lang/String;)V
.end method

.method private static native nativeReleaseIconForPageUrl(Ljava/lang/String;)V
.end method

.method private static native nativeRemoveAllIcons()V
.end method

.method private static native nativeRetainIconForPageUrl(Ljava/lang/String;)V
.end method


# virtual methods
.method public bulkRequestIconForPageUrl(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V
    .registers 8
    .parameter "cr"
    .parameter "where"
    .parameter "listener"

    #@0
    .prologue
    .line 219
    if-nez p3, :cond_3

    #@2
    .line 237
    :cond_2
    :goto_2
    return-void

    #@3
    .line 227
    :cond_3
    iget-object v2, p0, Landroid/webkit/WebIconDatabaseClassic;->mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@5
    invoke-static {v2}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$1000(Landroid/webkit/WebIconDatabaseClassic$EventHandler;)Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_2

    #@b
    .line 229
    new-instance v0, Ljava/util/HashMap;

    #@d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@10
    .line 230
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "contentResolver"

    #@12
    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    .line 231
    const-string/jumbo v2, "where"

    #@18
    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 232
    const-string/jumbo v2, "listener"

    #@1e
    invoke-virtual {v0, v2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    .line 233
    const/4 v2, 0x0

    #@22
    const/4 v3, 0x6

    #@23
    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@26
    move-result-object v1

    #@27
    .line 235
    .local v1, msg:Landroid/os/Message;
    iget-object v2, p0, Landroid/webkit/WebIconDatabaseClassic;->mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@29
    invoke-static {v2, v1}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$900(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Landroid/os/Message;)V

    #@2c
    goto :goto_2
.end method

.method public close()V
    .registers 4

    #@0
    .prologue
    .line 190
    iget-object v0, p0, Landroid/webkit/WebIconDatabaseClassic;->mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    invoke-static {v0, v1}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$900(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Landroid/os/Message;)V

    #@b
    .line 192
    return-void
.end method

.method createHandler()V
    .registers 2

    #@0
    .prologue
    .line 274
    iget-object v0, p0, Landroid/webkit/WebIconDatabaseClassic;->mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@2
    invoke-static {v0}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$1100(Landroid/webkit/WebIconDatabaseClassic$EventHandler;)V

    #@5
    .line 275
    return-void
.end method

.method public open(Ljava/lang/String;)V
    .registers 7
    .parameter "path"

    #@0
    .prologue
    .line 177
    if-eqz p1, :cond_1f

    #@2
    .line 179
    new-instance v0, Ljava/io/File;

    #@4
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    .line 180
    .local v0, db:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_10

    #@d
    .line 181
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@10
    .line 183
    :cond_10
    iget-object v1, p0, Landroid/webkit/WebIconDatabaseClassic;->mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@12
    const/4 v2, 0x0

    #@13
    const/4 v3, 0x0

    #@14
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    invoke-static {v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$900(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Landroid/os/Message;)V

    #@1f
    .line 186
    .end local v0           #db:Ljava/io/File;
    :cond_1f
    return-void
.end method

.method public releaseIconForPageUrl(Ljava/lang/String;)V
    .registers 5
    .parameter "url"

    #@0
    .prologue
    .line 249
    if-eqz p1, :cond_d

    #@2
    .line 250
    iget-object v0, p0, Landroid/webkit/WebIconDatabaseClassic;->mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@4
    const/4 v1, 0x0

    #@5
    const/4 v2, 0x5

    #@6
    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-static {v0, v1}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$900(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Landroid/os/Message;)V

    #@d
    .line 253
    :cond_d
    return-void
.end method

.method public removeAllIcons()V
    .registers 4

    #@0
    .prologue
    .line 196
    iget-object v0, p0, Landroid/webkit/WebIconDatabaseClassic;->mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x2

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    invoke-static {v0, v1}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$900(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Landroid/os/Message;)V

    #@b
    .line 198
    return-void
.end method

.method public requestIconForPageUrl(Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V
    .registers 6
    .parameter "url"
    .parameter "listener"

    #@0
    .prologue
    .line 207
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_5

    #@4
    .line 213
    :cond_4
    :goto_4
    return-void

    #@5
    .line 210
    :cond_5
    const/4 v1, 0x0

    #@6
    const/4 v2, 0x3

    #@7
    invoke-static {v1, v2, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 211
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@e
    move-result-object v1

    #@f
    const-string/jumbo v2, "url"

    #@12
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 212
    iget-object v1, p0, Landroid/webkit/WebIconDatabaseClassic;->mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@17
    invoke-static {v1, v0}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$900(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Landroid/os/Message;)V

    #@1a
    goto :goto_4
.end method

.method public retainIconForPageUrl(Ljava/lang/String;)V
    .registers 5
    .parameter "url"

    #@0
    .prologue
    .line 241
    if-eqz p1, :cond_d

    #@2
    .line 242
    iget-object v0, p0, Landroid/webkit/WebIconDatabaseClassic;->mEventHandler:Landroid/webkit/WebIconDatabaseClassic$EventHandler;

    #@4
    const/4 v1, 0x0

    #@5
    const/4 v2, 0x4

    #@6
    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-static {v0, v1}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->access$900(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Landroid/os/Message;)V

    #@d
    .line 245
    :cond_d
    return-void
.end method
