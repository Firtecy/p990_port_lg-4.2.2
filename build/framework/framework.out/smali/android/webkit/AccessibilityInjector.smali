.class Landroid/webkit/AccessibilityInjector;
.super Ljava/lang/Object;
.source "AccessibilityInjector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/AccessibilityInjector$CallbackHandler;,
        Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;
    }
.end annotation


# static fields
.field private static final ACCESSIBILITY_ANDROIDVOX_TEMPLATE:Ljava/lang/String; = "(function() {  if ((typeof(cvox) != \'undefined\')      && (cvox != null)      && (typeof(cvox.ChromeVox) != \'undefined\')      && (cvox.ChromeVox != null)      && (typeof(cvox.AndroidVox) != \'undefined\')      && (cvox.AndroidVox != null)      && cvox.ChromeVox.isActive) {    return cvox.AndroidVox.performAction(\'%1s\');  } else {    return false;  }})()"

.field private static final ACCESSIBILITY_SCREEN_READER_JAVASCRIPT_TEMPLATE:Ljava/lang/String; = "javascript:(function() {    var chooser = document.createElement(\'script\');    chooser.type = \'text/javascript\';    chooser.src = \'%1s\';    document.getElementsByTagName(\'head\')[0].appendChild(chooser);  })();"

.field private static final ACCESSIBILITY_SCRIPT_INJECTION_OPTED_OUT:I = 0x0

.field private static final ACCESSIBILITY_SCRIPT_INJECTION_PROVIDED:I = 0x1

.field private static final ACCESSIBILITY_SCRIPT_INJECTION_UNDEFINED:I = -0x1

.field private static final ALIAS_TRAVERSAL_JS_INTERFACE:Ljava/lang/String; = "accessibilityTraversal"

.field private static final ALIAS_TTS_JS_INTERFACE:Ljava/lang/String; = "accessibility"

.field private static DEBUG:Z = false

.field private static final PATTERN_MATCH_AXS_URL_PARAMETER:Ljava/lang/String; = "(\\?axs=(0|1))|(&axs=(0|1))"

.field private static final TAG:Ljava/lang/String; = null

.field private static final TOGGLE_CVOX_TEMPLATE:Ljava/lang/String; = "javascript:(function() {  if ((typeof(cvox) != \'undefined\')      && (cvox != null)      && (typeof(cvox.ChromeVox) != \'undefined\')      && (cvox.ChromeVox != null)      && (typeof(cvox.ChromeVox.host) != \'undefined\')      && (cvox.ChromeVox.host != null)) {    cvox.ChromeVox.host.activateOrDeactivateChromeVox(%b);  }})();"


# instance fields
.field private mAccessibilityInjectorFallback:Landroid/webkit/AccessibilityInjectorFallback;

.field private mAccessibilityJSONObject:Lorg/json/JSONObject;

.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAccessibilityScriptInjected:Z

.field private mCallback:Landroid/webkit/AccessibilityInjector$CallbackHandler;

.field private final mContext:Landroid/content/Context;

.field private mInjectScriptRunnable:Ljava/lang/Runnable;

.field private mMatchAxsUrlParameterPattern:Ljava/util/regex/Pattern;

.field private mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

.field private final mWebView:Landroid/webkit/WebView;

.field private final mWebViewClassic:Landroid/webkit/WebViewClassic;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 56
    const-class v0, Landroid/webkit/AccessibilityInjector;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/webkit/AccessibilityInjector;->TAG:Ljava/lang/String;

    #@8
    .line 58
    const/4 v0, 0x0

    #@9
    sput-boolean v0, Landroid/webkit/AccessibilityInjector;->DEBUG:Z

    #@b
    return-void
.end method

.method public constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "webViewClassic"

    #@0
    .prologue
    .line 142
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 382
    new-instance v0, Landroid/webkit/AccessibilityInjector$1;

    #@5
    invoke-direct {v0, p0}, Landroid/webkit/AccessibilityInjector$1;-><init>(Landroid/webkit/AccessibilityInjector;)V

    #@8
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector;->mInjectScriptRunnable:Ljava/lang/Runnable;

    #@a
    .line 143
    iput-object p1, p0, Landroid/webkit/AccessibilityInjector;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@c
    .line 144
    invoke-virtual {p1}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@12
    .line 145
    invoke-virtual {p1}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector;->mContext:Landroid/content/Context;

    #@18
    .line 146
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mContext:Landroid/content/Context;

    #@1a
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@1d
    move-result-object v0

    #@1e
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@20
    .line 147
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 55
    sget-boolean v0, Landroid/webkit/AccessibilityInjector;->DEBUG:Z

    #@2
    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 55
    sget-object v0, Landroid/webkit/AccessibilityInjector;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/webkit/AccessibilityInjector;)Landroid/webkit/WebView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/webkit/AccessibilityInjector;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->injectJavaScript()V

    #@3
    return-void
.end method

.method private addCallbackApis()V
    .registers 4

    #@0
    .prologue
    .line 509
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mCallback:Landroid/webkit/AccessibilityInjector$CallbackHandler;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 510
    new-instance v0, Landroid/webkit/AccessibilityInjector$CallbackHandler;

    #@6
    const-string v1, "accessibilityTraversal"

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v0, v1, v2}, Landroid/webkit/AccessibilityInjector$CallbackHandler;-><init>(Ljava/lang/String;Landroid/webkit/AccessibilityInjector$1;)V

    #@c
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector;->mCallback:Landroid/webkit/AccessibilityInjector$CallbackHandler;

    #@e
    .line 513
    :cond_e
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@10
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mCallback:Landroid/webkit/AccessibilityInjector$CallbackHandler;

    #@12
    const-string v2, "accessibilityTraversal"

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    #@17
    .line 514
    return-void
.end method

.method private addTtsApis()V
    .registers 4

    #@0
    .prologue
    .line 487
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 488
    new-instance v0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@6
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mContext:Landroid/content/Context;

    #@8
    invoke-direct {v0, v1}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;-><init>(Landroid/content/Context;)V

    #@b
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@d
    .line 491
    :cond_d
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@f
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@11
    const-string v2, "accessibility"

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    #@16
    .line 492
    return-void
.end method

.method private getAxsUrlParameterValue(Ljava/lang/String;)I
    .registers 6
    .parameter "url"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 533
    if-nez p1, :cond_4

    #@3
    .line 546
    :cond_3
    :goto_3
    return v2

    #@4
    .line 537
    :cond_4
    iget-object v3, p0, Landroid/webkit/AccessibilityInjector;->mMatchAxsUrlParameterPattern:Ljava/util/regex/Pattern;

    #@6
    if-nez v3, :cond_10

    #@8
    .line 538
    const-string v3, "(\\?axs=(0|1))|(&axs=(0|1))"

    #@a
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@d
    move-result-object v3

    #@e
    iput-object v3, p0, Landroid/webkit/AccessibilityInjector;->mMatchAxsUrlParameterPattern:Ljava/util/regex/Pattern;

    #@10
    .line 540
    :cond_10
    iget-object v3, p0, Landroid/webkit/AccessibilityInjector;->mMatchAxsUrlParameterPattern:Ljava/util/regex/Pattern;

    #@12
    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@15
    move-result-object v1

    #@16
    .line 541
    .local v1, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_3

    #@1c
    .line 542
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    #@1f
    move-result v2

    #@20
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    #@23
    move-result v3

    #@24
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .line 543
    .local v0, keyValuePair:Ljava/lang/String;
    const-string v2, "="

    #@2a
    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    const/4 v3, 0x1

    #@2f
    aget-object v2, v2, v3

    #@31
    invoke-direct {p0, v2}, Landroid/webkit/AccessibilityInjector;->verifyInjectionValue(Ljava/lang/String;)I

    #@34
    move-result v2

    #@35
    goto :goto_3
.end method

.method private getScreenReaderInjectionUrl()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 570
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string v2, "accessibility_script_injection_url"

    #@8
    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 572
    .local v0, screenReaderUrl:Ljava/lang/String;
    const-string/jumbo v1, "javascript:(function() {    var chooser = document.createElement(\'script\');    chooser.type = \'text/javascript\';    chooser.src = \'%1s\';    document.getElementsByTagName(\'head\')[0].appendChild(chooser);  })();"

    #@f
    const/4 v2, 0x1

    #@10
    new-array v2, v2, [Ljava/lang/Object;

    #@12
    const/4 v3, 0x0

    #@13
    aput-object v0, v2, v3

    #@15
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    return-object v1
.end method

.method private injectJavaScript()V
    .registers 5

    #@0
    .prologue
    .line 399
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, v1}, Landroid/webkit/AccessibilityInjector;->toggleFallbackAccessibilityInjector(Z)V

    #@4
    .line 401
    iget-boolean v1, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityScriptInjected:Z

    #@6
    if-nez v1, :cond_3d

    #@8
    .line 402
    const/4 v1, 0x1

    #@9
    iput-boolean v1, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityScriptInjected:Z

    #@b
    .line 403
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->getScreenReaderInjectionUrl()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 404
    .local v0, injectionUrl:Ljava/lang/String;
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@11
    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    #@14
    .line 405
    sget-boolean v1, Landroid/webkit/AccessibilityInjector;->DEBUG:Z

    #@16
    if-eqz v1, :cond_3c

    #@18
    .line 406
    sget-object v1, Landroid/webkit/AccessibilityInjector;->TAG:Ljava/lang/String;

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "["

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    iget-object v3, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@27
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    #@2a
    move-result v3

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, "] Loading screen reader into WebView"

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 413
    .end local v0           #injectionUrl:Ljava/lang/String;
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 409
    :cond_3d
    sget-boolean v1, Landroid/webkit/AccessibilityInjector;->DEBUG:Z

    #@3f
    if-eqz v1, :cond_3c

    #@41
    .line 410
    sget-object v1, Landroid/webkit/AccessibilityInjector;->TAG:Ljava/lang/String;

    #@43
    new-instance v2, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v3, "["

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    iget-object v3, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@50
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    #@53
    move-result v3

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    const-string v3, "] Attempted to inject screen reader twice"

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    goto :goto_3c
.end method

.method private isAccessibilityEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 592
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@2
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private isJavaScriptEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 580
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@2
    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    #@5
    move-result-object v0

    #@6
    .line 581
    .local v0, settings:Landroid/webkit/WebSettings;
    if-nez v0, :cond_a

    #@8
    .line 582
    const/4 v1, 0x0

    #@9
    .line 585
    :goto_9
    return v1

    #@a
    :cond_a
    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getJavaScriptEnabled()Z

    #@d
    move-result v1

    #@e
    goto :goto_9
.end method

.method private isScriptInjectionEnabled()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 477
    iget-object v3, p0, Landroid/webkit/AccessibilityInjector;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v3

    #@8
    const-string v4, "accessibility_script_injection"

    #@a
    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v0

    #@e
    .line 479
    .local v0, injectionSetting:I
    if-ne v0, v1, :cond_11

    #@10
    :goto_10
    return v1

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_10
.end method

.method private removeAccessibilityApisIfNecessary()V
    .registers 1

    #@0
    .prologue
    .line 186
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->removeTtsApis()V

    #@3
    .line 187
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->removeCallbackApis()V

    #@6
    .line 188
    return-void
.end method

.method private removeCallbackApis()V
    .registers 3

    #@0
    .prologue
    .line 517
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mCallback:Landroid/webkit/AccessibilityInjector$CallbackHandler;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 518
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector;->mCallback:Landroid/webkit/AccessibilityInjector$CallbackHandler;

    #@7
    .line 521
    :cond_7
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@9
    const-string v1, "accessibilityTraversal"

    #@b
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->removeJavascriptInterface(Ljava/lang/String;)V

    #@e
    .line 522
    return-void
.end method

.method private removeTtsApis()V
    .registers 3

    #@0
    .prologue
    .line 499
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 500
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@6
    invoke-virtual {v0}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->stop()I

    #@9
    .line 501
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@b
    invoke-virtual {v0}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->shutdown()V

    #@e
    .line 502
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@11
    .line 505
    :cond_11
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@13
    const-string v1, "accessibility"

    #@15
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->removeJavascriptInterface(Ljava/lang/String;)V

    #@18
    .line 506
    return-void
.end method

.method private sendActionToAndroidVox(ILandroid/os/Bundle;)Z
    .registers 13
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 603
    iget-object v7, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityJSONObject:Lorg/json/JSONObject;

    #@3
    if-nez v7, :cond_34

    #@5
    .line 604
    new-instance v7, Lorg/json/JSONObject;

    #@7
    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    #@a
    iput-object v7, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityJSONObject:Lorg/json/JSONObject;

    #@c
    .line 615
    :cond_c
    :try_start_c
    iget-object v7, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityJSONObject:Lorg/json/JSONObject;

    #@e
    const-string v8, "action"

    #@10
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v9

    #@14
    invoke-virtual {v7, v8, v9}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_17
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_17} :catch_5b

    #@17
    .line 617
    sparse-switch p1, :sswitch_data_6e

    #@1a
    .line 639
    :cond_1a
    :goto_1a
    iget-object v7, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityJSONObject:Lorg/json/JSONObject;

    #@1c
    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    .line 640
    .local v4, jsonString:Ljava/lang/String;
    const-string v7, "(function() {  if ((typeof(cvox) != \'undefined\')      && (cvox != null)      && (typeof(cvox.ChromeVox) != \'undefined\')      && (cvox.ChromeVox != null)      && (typeof(cvox.AndroidVox) != \'undefined\')      && (cvox.AndroidVox != null)      && cvox.ChromeVox.isActive) {    return cvox.AndroidVox.performAction(\'%1s\');  } else {    return false;  }})()"

    #@22
    const/4 v8, 0x1

    #@23
    new-array v8, v8, [Ljava/lang/Object;

    #@25
    aput-object v4, v8, v6

    #@27
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    .line 641
    .local v3, jsCode:Ljava/lang/String;
    iget-object v6, p0, Landroid/webkit/AccessibilityInjector;->mCallback:Landroid/webkit/AccessibilityInjector$CallbackHandler;

    #@2d
    iget-object v7, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@2f
    invoke-static {v6, v7, v3}, Landroid/webkit/AccessibilityInjector$CallbackHandler;->access$500(Landroid/webkit/AccessibilityInjector$CallbackHandler;Landroid/webkit/WebView;Ljava/lang/String;)Z

    #@32
    move-result v6

    #@33
    .end local v3           #jsCode:Ljava/lang/String;
    .end local v4           #jsonString:Ljava/lang/String;
    :goto_33
    return v6

    #@34
    .line 607
    :cond_34
    iget-object v7, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityJSONObject:Lorg/json/JSONObject;

    #@36
    invoke-virtual {v7}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    #@39
    move-result-object v5

    #@3a
    .line 608
    .local v5, keys:Ljava/util/Iterator;,"Ljava/util/Iterator<*>;"
    :goto_3a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@3d
    move-result v7

    #@3e
    if-eqz v7, :cond_c

    #@40
    .line 609
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@43
    .line 610
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    #@46
    goto :goto_3a

    #@47
    .line 620
    .end local v5           #keys:Ljava/util/Iterator;,"Ljava/util/Iterator<*>;"
    :sswitch_47
    if-eqz p2, :cond_1a

    #@49
    .line 621
    :try_start_49
    const-string v7, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    #@4b
    invoke-virtual {p2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@4e
    move-result v2

    #@4f
    .line 623
    .local v2, granularity:I
    iget-object v7, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityJSONObject:Lorg/json/JSONObject;

    #@51
    const-string v8, "granularity"

    #@53
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@56
    move-result-object v9

    #@57
    invoke-virtual {v7, v8, v9}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    #@5a
    goto :goto_1a

    #@5b
    .line 635
    .end local v2           #granularity:I
    :catch_5b
    move-exception v0

    #@5c
    .line 636
    .local v0, e:Lorg/json/JSONException;
    goto :goto_33

    #@5d
    .line 628
    .end local v0           #e:Lorg/json/JSONException;
    :sswitch_5d
    if-eqz p2, :cond_1a

    #@5f
    .line 629
    const-string v7, "ACTION_ARGUMENT_HTML_ELEMENT_STRING"

    #@61
    invoke-virtual {p2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    .line 631
    .local v1, element:Ljava/lang/String;
    iget-object v7, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityJSONObject:Lorg/json/JSONObject;

    #@67
    const-string v8, "element"

    #@69
    invoke-virtual {v7, v8, v1}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_6c
    .catch Lorg/json/JSONException; {:try_start_49 .. :try_end_6c} :catch_5b

    #@6c
    goto :goto_1a

    #@6d
    .line 617
    nop

    #@6e
    :sswitch_data_6e
    .sparse-switch
        0x100 -> :sswitch_47
        0x200 -> :sswitch_47
        0x400 -> :sswitch_5d
        0x800 -> :sswitch_5d
    .end sparse-switch
.end method

.method private shouldInjectJavaScript(Ljava/lang/String;)Z
    .registers 4
    .parameter "url"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 455
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->isJavaScriptEnabled()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_8

    #@7
    .line 469
    :cond_7
    :goto_7
    return v0

    #@8
    .line 460
    :cond_8
    invoke-direct {p0, p1}, Landroid/webkit/AccessibilityInjector;->getAxsUrlParameterValue(Ljava/lang/String;)I

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_7

    #@e
    .line 465
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->isScriptInjectionEnabled()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_7

    #@14
    .line 469
    const/4 v0, 0x1

    #@15
    goto :goto_7
.end method

.method private toggleAndroidVox(Z)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    .line 205
    iget-boolean v1, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityScriptInjected:Z

    #@2
    if-nez v1, :cond_5

    #@4
    .line 211
    :goto_4
    return-void

    #@5
    .line 209
    :cond_5
    const-string/jumbo v1, "javascript:(function() {  if ((typeof(cvox) != \'undefined\')      && (cvox != null)      && (typeof(cvox.ChromeVox) != \'undefined\')      && (cvox.ChromeVox != null)      && (typeof(cvox.ChromeVox.host) != \'undefined\')      && (cvox.ChromeVox.host != null)) {    cvox.ChromeVox.host.activateOrDeactivateChromeVox(%b);  }})();"

    #@8
    const/4 v2, 0x1

    #@9
    new-array v2, v2, [Ljava/lang/Object;

    #@b
    const/4 v3, 0x0

    #@c
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@f
    move-result-object v4

    #@10
    aput-object v4, v2, v3

    #@12
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 210
    .local v0, code:Ljava/lang/String;
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@18
    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    #@1b
    goto :goto_4
.end method

.method private toggleFallbackAccessibilityInjector(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 439
    if-eqz p1, :cond_10

    #@2
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityInjectorFallback:Landroid/webkit/AccessibilityInjectorFallback;

    #@4
    if-nez v0, :cond_10

    #@6
    .line 440
    new-instance v0, Landroid/webkit/AccessibilityInjectorFallback;

    #@8
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@a
    invoke-direct {v0, v1}, Landroid/webkit/AccessibilityInjectorFallback;-><init>(Landroid/webkit/WebViewClassic;)V

    #@d
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityInjectorFallback:Landroid/webkit/AccessibilityInjectorFallback;

    #@f
    .line 444
    :goto_f
    return-void

    #@10
    .line 442
    :cond_10
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityInjectorFallback:Landroid/webkit/AccessibilityInjectorFallback;

    #@13
    goto :goto_f
.end method

.method private verifyInjectionValue(Ljava/lang/String;)I
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 551
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_3} :catch_d

    #@3
    move-result v0

    #@4
    .line 553
    .local v0, parsed:I
    packed-switch v0, :pswitch_data_10

    #@7
    .line 563
    .end local v0           #parsed:I
    :goto_7
    const/4 v1, -0x1

    #@8
    :goto_8
    return v1

    #@9
    .line 555
    .restart local v0       #parsed:I
    :pswitch_9
    const/4 v1, 0x0

    #@a
    goto :goto_8

    #@b
    .line 557
    :pswitch_b
    const/4 v1, 0x1

    #@c
    goto :goto_8

    #@d
    .line 559
    .end local v0           #parsed:I
    :catch_d
    move-exception v1

    #@e
    goto :goto_7

    #@f
    .line 553
    nop

    #@10
    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_9
        :pswitch_b
    .end packed-switch
.end method


# virtual methods
.method public addAccessibilityApisIfNecessary()V
    .registers 2

    #@0
    .prologue
    .line 172
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->isAccessibilityEnabled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->isJavaScriptEnabled()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_d

    #@c
    .line 178
    :cond_c
    :goto_c
    return-void

    #@d
    .line 176
    :cond_d
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->addTtsApis()V

    #@10
    .line 177
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->addCallbackApis()V

    #@13
    goto :goto_c
.end method

.method public destroy()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 194
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 195
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@7
    invoke-virtual {v0}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->shutdown()V

    #@a
    .line 196
    iput-object v1, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@c
    .line 199
    :cond_c
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mCallback:Landroid/webkit/AccessibilityInjector$CallbackHandler;

    #@e
    if-eqz v0, :cond_12

    #@10
    .line 200
    iput-object v1, p0, Landroid/webkit/AccessibilityInjector;->mCallback:Landroid/webkit/AccessibilityInjector$CallbackHandler;

    #@12
    .line 202
    :cond_12
    return-void
.end method

.method public handleKeyEventIfNecessary(Landroid/view/KeyEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 294
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->isAccessibilityEnabled()Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_e

    #@8
    .line 295
    iput-boolean v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityScriptInjected:Z

    #@a
    .line 296
    invoke-direct {p0, v0}, Landroid/webkit/AccessibilityInjector;->toggleFallbackAccessibilityInjector(Z)V

    #@d
    .line 323
    :cond_d
    :goto_d
    return v0

    #@e
    .line 300
    :cond_e
    iget-boolean v2, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityScriptInjected:Z

    #@10
    if-eqz v2, :cond_2f

    #@12
    .line 305
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@15
    move-result v2

    #@16
    if-ne v2, v1, :cond_21

    #@18
    .line 306
    iget-object v2, p0, Landroid/webkit/AccessibilityInjector;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@1a
    const/16 v3, 0x68

    #@1c
    invoke-virtual {v2, v3, v0, v0, p1}, Landroid/webkit/WebViewClassic;->sendBatchableInputMessage(IIILjava/lang/Object;)V

    #@1f
    :goto_1f
    move v0, v1

    #@20
    .line 313
    goto :goto_d

    #@21
    .line 307
    :cond_21
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@24
    move-result v2

    #@25
    if-nez v2, :cond_d

    #@27
    .line 308
    iget-object v2, p0, Landroid/webkit/AccessibilityInjector;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@29
    const/16 v3, 0x67

    #@2b
    invoke-virtual {v2, v3, v0, v0, p1}, Landroid/webkit/WebViewClassic;->sendBatchableInputMessage(IIILjava/lang/Object;)V

    #@2e
    goto :goto_1f

    #@2f
    .line 316
    :cond_2f
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityInjectorFallback:Landroid/webkit/AccessibilityInjectorFallback;

    #@31
    if-eqz v1, :cond_d

    #@33
    .line 320
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityInjectorFallback:Landroid/webkit/AccessibilityInjectorFallback;

    #@35
    invoke-virtual {v0, p1}, Landroid/webkit/AccessibilityInjectorFallback;->onKeyEvent(Landroid/view/KeyEvent;)Z

    #@38
    move-result v0

    #@39
    goto :goto_d
.end method

.method public handleSelectionChangedIfNecessary(Ljava/lang/String;)V
    .registers 3
    .parameter "selectionString"

    #@0
    .prologue
    .line 333
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityInjectorFallback:Landroid/webkit/AccessibilityInjectorFallback;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 334
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityInjectorFallback:Landroid/webkit/AccessibilityInjectorFallback;

    #@6
    invoke-virtual {v0, p1}, Landroid/webkit/AccessibilityInjectorFallback;->onSelectionStringChange(Ljava/lang/String;)V

    #@9
    .line 336
    :cond_9
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 227
    const/16 v0, 0x1f

    #@2
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setMovementGranularities(I)V

    #@5
    .line 232
    const/16 v0, 0x100

    #@7
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@a
    .line 233
    const/16 v0, 0x200

    #@c
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@f
    .line 234
    const/16 v0, 0x400

    #@11
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@14
    .line 235
    const/16 v0, 0x800

    #@16
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@19
    .line 236
    const/16 v0, 0x10

    #@1b
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@1e
    .line 237
    const/4 v0, 0x1

    #@1f
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClickable(Z)V

    #@22
    .line 238
    return-void
.end method

.method public onPageFinished(Ljava/lang/String;)V
    .registers 5
    .parameter "url"

    #@0
    .prologue
    .line 360
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->isAccessibilityEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_b

    #@6
    .line 361
    const/4 v0, 0x0

    #@7
    invoke-direct {p0, v0}, Landroid/webkit/AccessibilityInjector;->toggleFallbackAccessibilityInjector(Z)V

    #@a
    .line 376
    :cond_a
    :goto_a
    return-void

    #@b
    .line 365
    :cond_b
    const/4 v0, 0x1

    #@c
    invoke-direct {p0, v0}, Landroid/webkit/AccessibilityInjector;->toggleFallbackAccessibilityInjector(Z)V

    #@f
    .line 367
    invoke-direct {p0, p1}, Landroid/webkit/AccessibilityInjector;->shouldInjectJavaScript(Ljava/lang/String;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_a

    #@15
    .line 370
    sget-boolean v0, Landroid/webkit/AccessibilityInjector;->DEBUG:Z

    #@17
    if-eqz v0, :cond_3d

    #@19
    .line 371
    sget-object v0, Landroid/webkit/AccessibilityInjector;->TAG:Ljava/lang/String;

    #@1b
    new-instance v1, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v2, "["

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    iget-object v2, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@28
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    #@2b
    move-result v2

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    const-string v2, "] Request callback "

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 374
    :cond_3d
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mCallback:Landroid/webkit/AccessibilityInjector$CallbackHandler;

    #@3f
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@41
    iget-object v2, p0, Landroid/webkit/AccessibilityInjector;->mInjectScriptRunnable:Ljava/lang/Runnable;

    #@43
    invoke-virtual {v0, v1, v2}, Landroid/webkit/AccessibilityInjector$CallbackHandler;->requestCallback(Landroid/webkit/WebView;Ljava/lang/Runnable;)V

    #@46
    goto :goto_a
.end method

.method public onPageStarted(Ljava/lang/String;)V
    .registers 5
    .parameter "url"

    #@0
    .prologue
    .line 344
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityScriptInjected:Z

    #@3
    .line 345
    sget-boolean v0, Landroid/webkit/AccessibilityInjector;->DEBUG:Z

    #@5
    if-eqz v0, :cond_2b

    #@7
    .line 346
    sget-object v0, Landroid/webkit/AccessibilityInjector;->TAG:Ljava/lang/String;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "["

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget-object v2, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@16
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    #@19
    move-result v2

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v2, "] Started loading new page"

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 348
    :cond_2b
    invoke-virtual {p0}, Landroid/webkit/AccessibilityInjector;->addAccessibilityApisIfNecessary()V

    #@2e
    .line 349
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 5
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 270
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->isAccessibilityEnabled()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_d

    #@7
    .line 271
    iput-boolean v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityScriptInjected:Z

    #@9
    .line 272
    invoke-direct {p0, v0}, Landroid/webkit/AccessibilityInjector;->toggleFallbackAccessibilityInjector(Z)V

    #@c
    .line 284
    :cond_c
    :goto_c
    return v0

    #@d
    .line 276
    :cond_d
    iget-boolean v1, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityScriptInjected:Z

    #@f
    if-eqz v1, :cond_16

    #@11
    .line 277
    invoke-direct {p0, p1, p2}, Landroid/webkit/AccessibilityInjector;->sendActionToAndroidVox(ILandroid/os/Bundle;)Z

    #@14
    move-result v0

    #@15
    goto :goto_c

    #@16
    .line 280
    :cond_16
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityInjectorFallback:Landroid/webkit/AccessibilityInjectorFallback;

    #@18
    if-eqz v1, :cond_c

    #@1a
    .line 281
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mAccessibilityInjectorFallback:Landroid/webkit/AccessibilityInjectorFallback;

    #@1c
    invoke-virtual {v0, p1, p2}, Landroid/webkit/AccessibilityInjectorFallback;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@1f
    move-result v0

    #@20
    goto :goto_c
.end method

.method public supportsAccessibilityAction(I)Z
    .registers 3
    .parameter "action"

    #@0
    .prologue
    .line 249
    sparse-switch p1, :sswitch_data_8

    #@3
    .line 257
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 255
    :sswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 249
    nop

    #@8
    :sswitch_data_8
    .sparse-switch
        0x10 -> :sswitch_5
        0x100 -> :sswitch_5
        0x200 -> :sswitch_5
        0x400 -> :sswitch_5
        0x800 -> :sswitch_5
    .end sparse-switch
.end method

.method public toggleAccessibilityFeedback(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 155
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->isAccessibilityEnabled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->isJavaScriptEnabled()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_d

    #@c
    .line 164
    :cond_c
    :goto_c
    return-void

    #@d
    .line 159
    :cond_d
    invoke-direct {p0, p1}, Landroid/webkit/AccessibilityInjector;->toggleAndroidVox(Z)V

    #@10
    .line 161
    if-nez p1, :cond_c

    #@12
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@14
    if-eqz v0, :cond_c

    #@16
    .line 162
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mTextToSpeech:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@18
    invoke-virtual {v0}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->stop()I

    #@1b
    goto :goto_c
.end method

.method public updateJavaScriptEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 422
    if-eqz p1, :cond_b

    #@2
    .line 423
    invoke-virtual {p0}, Landroid/webkit/AccessibilityInjector;->addAccessibilityApisIfNecessary()V

    #@5
    .line 429
    :goto_5
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector;->mWebView:Landroid/webkit/WebView;

    #@7
    invoke-virtual {v0}, Landroid/webkit/WebView;->reload()V

    #@a
    .line 430
    return-void

    #@b
    .line 425
    :cond_b
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector;->removeAccessibilityApisIfNecessary()V

    #@e
    goto :goto_5
.end method
