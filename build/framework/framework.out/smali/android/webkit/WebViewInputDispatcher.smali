.class final Landroid/webkit/WebViewInputDispatcher;
.super Ljava/lang/Object;
.source "WebViewInputDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebViewInputDispatcher$1;,
        Landroid/webkit/WebViewInputDispatcher$TouchStream;,
        Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;,
        Landroid/webkit/WebViewInputDispatcher$DispatchEvent;,
        Landroid/webkit/WebViewInputDispatcher$WebKitHandler;,
        Landroid/webkit/WebViewInputDispatcher$UiHandler;,
        Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;,
        Landroid/webkit/WebViewInputDispatcher$UiCallbacks;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final DEBUG:Z = false

#the value of this static final field might be set in the static constructor
.field private static final DOUBLE_TAP_TIMEOUT:I = 0x0

.field private static final ENABLE_EVENT_BATCHING:Z = true

.field public static final EVENT_TYPE_CLICK:I = 0x4

.field public static final EVENT_TYPE_DOUBLE_TAP:I = 0x5

.field public static final EVENT_TYPE_HIT_TEST:I = 0x6

.field public static final EVENT_TYPE_HOVER:I = 0x1

.field public static final EVENT_TYPE_LONG_PRESS:I = 0x3

.field public static final EVENT_TYPE_SCROLL:I = 0x2

.field public static final EVENT_TYPE_TOUCH:I = 0x0

.field public static final FLAG_PRIVATE:I = 0x1

.field public static final FLAG_WEBKIT_IN_PROGRESS:I = 0x2

.field public static final FLAG_WEBKIT_SOUNDEFFECT_EVENT:I = 0x10

.field public static final FLAG_WEBKIT_TIMEOUT:I = 0x4

.field public static final FLAG_WEBKIT_TRANSFORMED_EVENT:I = 0x8

#the value of this static final field might be set in the static constructor
.field private static final LONG_PRESS_TIMEOUT:I = 0x0

.field private static final MAX_DISPATCH_EVENT_POOL_SIZE:I = 0xa

#the value of this static final field might be set in the static constructor
.field private static final PRESSED_STATE_DURATION:I = 0x0

.field private static final TAG:Ljava/lang/String; = "WebViewInputDispatcher"

#the value of this static final field might be set in the static constructor
.field private static final TAP_TIMEOUT:I = 0x0

.field private static final WEBKIT_TIMEOUT_MILLIS:J = 0xc8L


# instance fields
.field private mDispatchEventPool:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

.field private mDispatchEventPoolSize:I

.field private mDoubleTapSlopSquared:F

.field private mInitialDownX:F

.field private mInitialDownY:F

.field private mIsDoubleTapCandidate:Z

.field private mIsOverJSContentDragSlop:Z

.field private mIsPreventDefault:Z

.field private mIsSentMoveEventForX:Z

.field private mIsSentMoveEventForY:Z

.field private mIsTapCandidate:Z

.field private mJScontentDragSlop:I

.field private final mLock:Ljava/lang/Object;

.field private mPostClickScheduled:Z

.field private mPostDoNotSendClickEventsToWebKit:Z

.field private mPostDoNotSendTouchEventsToWebKitUntilNextGesture:Z

.field private mPostDoNotSendTouchMoveEventsToWebKit:Z

.field private mPostHideTapHighlightScheduled:Z

.field private mPostLastWebKitScale:F

.field private mPostLastWebKitXOffset:I

.field private mPostLastWebKitYOffset:I

.field private mPostLongPressScheduled:Z

.field private mPostSendTouchEventsToWebKit:Z

.field private mPostShowTapHighlightScheduled:Z

.field private final mPostTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

.field private mPreventDoubleTap:Z

.field private mTouchDragMode:Z

.field private mTouchSlopSquared:F

.field private final mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

.field private final mUiDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

.field private mUiDispatchScheduled:Z

.field private final mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

.field private final mUiTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

.field private final mWebKitCallbacks:Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;

.field private final mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

.field private mWebKitDispatchScheduled:Z

.field private final mWebKitHandler:Landroid/webkit/WebViewInputDispatcher$WebKitHandler;

.field private mWebKitTimeoutScheduled:Z

.field private mWebKitTimeoutTime:J

.field private final mWebKitTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 78
    const-class v0, Landroid/webkit/WebViewInputDispatcher;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_27

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/WebViewInputDispatcher;->$assertionsDisabled:Z

    #@b
    .line 150
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    #@e
    move-result v0

    #@f
    sput v0, Landroid/webkit/WebViewInputDispatcher;->TAP_TIMEOUT:I

    #@11
    .line 151
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@14
    move-result v0

    #@15
    sget v1, Landroid/webkit/WebViewInputDispatcher;->TAP_TIMEOUT:I

    #@17
    add-int/2addr v0, v1

    #@18
    sput v0, Landroid/webkit/WebViewInputDispatcher;->LONG_PRESS_TIMEOUT:I

    #@1a
    .line 153
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    #@1d
    move-result v0

    #@1e
    sput v0, Landroid/webkit/WebViewInputDispatcher;->DOUBLE_TAP_TIMEOUT:I

    #@20
    .line 154
    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    #@23
    move-result v0

    #@24
    sput v0, Landroid/webkit/WebViewInputDispatcher;->PRESSED_STATE_DURATION:I

    #@26
    return-void

    #@27
    .line 78
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_9
.end method

.method public constructor <init>(Landroid/webkit/WebViewInputDispatcher$UiCallbacks;Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;)V
    .registers 7
    .parameter "uiCallbacks"
    .parameter "webKitCallbacks"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 251
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 89
    new-instance v1, Ljava/lang/Object;

    #@7
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@a
    iput-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mLock:Ljava/lang/Object;

    #@c
    .line 97
    new-instance v1, Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@e
    invoke-direct {v1, v2}, Landroid/webkit/WebViewInputDispatcher$TouchStream;-><init>(Landroid/webkit/WebViewInputDispatcher$1;)V

    #@11
    iput-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mPostTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@13
    .line 109
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendClickEventsToWebKit:Z

    #@15
    .line 110
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchMoveEventsToWebKit:Z

    #@17
    .line 121
    const/16 v1, 0x40

    #@19
    iput v1, p0, Landroid/webkit/WebViewInputDispatcher;->mJScontentDragSlop:I

    #@1b
    .line 125
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mTouchDragMode:Z

    #@1d
    .line 130
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mPreventDoubleTap:Z

    #@1f
    .line 133
    new-instance v1, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@21
    invoke-direct {v1, v2}, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;-><init>(Landroid/webkit/WebViewInputDispatcher$1;)V

    #@24
    iput-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@26
    .line 134
    new-instance v1, Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@28
    invoke-direct {v1, v2}, Landroid/webkit/WebViewInputDispatcher$TouchStream;-><init>(Landroid/webkit/WebViewInputDispatcher$1;)V

    #@2b
    iput-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@2d
    .line 142
    new-instance v1, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@2f
    invoke-direct {v1, v2}, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;-><init>(Landroid/webkit/WebViewInputDispatcher$1;)V

    #@32
    iput-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mUiDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@34
    .line 143
    new-instance v1, Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@36
    invoke-direct {v1, v2}, Landroid/webkit/WebViewInputDispatcher$TouchStream;-><init>(Landroid/webkit/WebViewInputDispatcher$1;)V

    #@39
    iput-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mUiTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@3b
    .line 252
    iput-object p1, p0, Landroid/webkit/WebViewInputDispatcher;->mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

    #@3d
    .line 253
    new-instance v1, Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@3f
    invoke-interface {p1}, Landroid/webkit/WebViewInputDispatcher$UiCallbacks;->getUiLooper()Landroid/os/Looper;

    #@42
    move-result-object v2

    #@43
    invoke-direct {v1, p0, v2}, Landroid/webkit/WebViewInputDispatcher$UiHandler;-><init>(Landroid/webkit/WebViewInputDispatcher;Landroid/os/Looper;)V

    #@46
    iput-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@48
    .line 255
    iput-object p2, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitCallbacks:Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;

    #@4a
    .line 256
    new-instance v1, Landroid/webkit/WebViewInputDispatcher$WebKitHandler;

    #@4c
    invoke-interface {p2}, Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;->getWebKitLooper()Landroid/os/Looper;

    #@4f
    move-result-object v2

    #@50
    invoke-direct {v1, p0, v2}, Landroid/webkit/WebViewInputDispatcher$WebKitHandler;-><init>(Landroid/webkit/WebViewInputDispatcher;Landroid/os/Looper;)V

    #@53
    iput-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitHandler:Landroid/webkit/WebViewInputDispatcher$WebKitHandler;

    #@55
    .line 258
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

    #@57
    invoke-interface {v1}, Landroid/webkit/WebViewInputDispatcher$UiCallbacks;->getContext()Landroid/content/Context;

    #@5a
    move-result-object v1

    #@5b
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@5e
    move-result-object v0

    #@5f
    .line 259
    .local v0, config:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    #@62
    move-result v1

    #@63
    int-to-float v1, v1

    #@64
    iput v1, p0, Landroid/webkit/WebViewInputDispatcher;->mDoubleTapSlopSquared:F

    #@66
    .line 260
    iget v1, p0, Landroid/webkit/WebViewInputDispatcher;->mDoubleTapSlopSquared:F

    #@68
    iget v2, p0, Landroid/webkit/WebViewInputDispatcher;->mDoubleTapSlopSquared:F

    #@6a
    mul-float/2addr v1, v2

    #@6b
    iput v1, p0, Landroid/webkit/WebViewInputDispatcher;->mDoubleTapSlopSquared:F

    #@6d
    .line 261
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@70
    move-result v1

    #@71
    int-to-float v1, v1

    #@72
    iput v1, p0, Landroid/webkit/WebViewInputDispatcher;->mTouchSlopSquared:F

    #@74
    .line 262
    iget v1, p0, Landroid/webkit/WebViewInputDispatcher;->mTouchSlopSquared:F

    #@76
    iget v2, p0, Landroid/webkit/WebViewInputDispatcher;->mTouchSlopSquared:F

    #@78
    mul-float/2addr v1, v2

    #@79
    iput v1, p0, Landroid/webkit/WebViewInputDispatcher;->mTouchSlopSquared:F

    #@7b
    .line 263
    return-void
.end method

.method static synthetic access$300(Landroid/webkit/WebViewInputDispatcher;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 78
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->dispatchUiEvents(Z)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/webkit/WebViewInputDispatcher;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->handleWebKitTimeout()V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/webkit/WebViewInputDispatcher;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->postLongPress()V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/webkit/WebViewInputDispatcher;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->postClick()V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/webkit/WebViewInputDispatcher;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 78
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->postShowTapHighlight(Z)V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/webkit/WebViewInputDispatcher;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 78
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->dispatchWebKitEvents(Z)V

    #@3
    return-void
.end method

.method private batchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Z
    .registers 5
    .parameter "in"
    .parameter "tail"

    #@0
    .prologue
    .line 1106
    if-eqz p2, :cond_33

    #@2
    iget-object v0, p2, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@4
    if-eqz v0, :cond_33

    #@6
    iget-object v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@8
    if-eqz v0, :cond_33

    #@a
    iget v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@c
    iget v1, p2, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@e
    if-ne v0, v1, :cond_33

    #@10
    iget v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@12
    iget v1, p2, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@14
    if-ne v0, v1, :cond_33

    #@16
    iget v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitXOffset:I

    #@18
    iget v1, p2, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitXOffset:I

    #@1a
    if-ne v0, v1, :cond_33

    #@1c
    iget v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitYOffset:I

    #@1e
    iget v1, p2, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitYOffset:I

    #@20
    if-ne v0, v1, :cond_33

    #@22
    iget v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitScale:F

    #@24
    iget v1, p2, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitScale:F

    #@26
    cmpl-float v0, v0, v1

    #@28
    if-nez v0, :cond_33

    #@2a
    .line 1112
    iget-object v0, p2, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@2c
    iget-object v1, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@2e
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->addBatch(Landroid/view/MotionEvent;)Z

    #@31
    move-result v0

    #@32
    .line 1114
    :goto_32
    return v0

    #@33
    :cond_33
    const/4 v0, 0x0

    #@34
    goto :goto_32
.end method

.method private checkForDoubleTapOnDownLocked(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 535
    const/4 v2, 0x0

    #@1
    iput-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsDoubleTapCandidate:Z

    #@3
    .line 536
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mPostClickScheduled:Z

    #@5
    if-nez v2, :cond_8

    #@7
    .line 545
    :cond_7
    :goto_7
    return-void

    #@8
    .line 539
    :cond_8
    iget v2, p0, Landroid/webkit/WebViewInputDispatcher;->mInitialDownX:F

    #@a
    float-to-int v2, v2

    #@b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@e
    move-result v3

    #@f
    float-to-int v3, v3

    #@10
    sub-int v0, v2, v3

    #@12
    .line 540
    .local v0, deltaX:I
    iget v2, p0, Landroid/webkit/WebViewInputDispatcher;->mInitialDownY:F

    #@14
    float-to-int v2, v2

    #@15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@18
    move-result v3

    #@19
    float-to-int v3, v3

    #@1a
    sub-int v1, v2, v3

    #@1c
    .line 541
    .local v1, deltaY:I
    mul-int v2, v0, v0

    #@1e
    mul-int v3, v1, v1

    #@20
    add-int/2addr v2, v3

    #@21
    int-to-float v2, v2

    #@22
    iget v3, p0, Landroid/webkit/WebViewInputDispatcher;->mDoubleTapSlopSquared:F

    #@24
    cmpg-float v2, v2, v3

    #@26
    if-gez v2, :cond_7

    #@28
    .line 542
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleClickLocked()V

    #@2b
    .line 543
    const/4 v2, 0x1

    #@2c
    iput-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsDoubleTapCandidate:Z

    #@2e
    goto :goto_7
.end method

.method private checkForJSContentDragSlop(Landroid/view/MotionEvent;)V
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 587
    const/4 v2, 0x0

    #@2
    iput-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsOverJSContentDragSlop:Z

    #@4
    .line 588
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsSentMoveEventForX:Z

    #@6
    if-nez v2, :cond_c

    #@8
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsSentMoveEventForY:Z

    #@a
    if-eqz v2, :cond_d

    #@c
    .line 610
    :cond_c
    :goto_c
    return-void

    #@d
    .line 591
    :cond_d
    iget v2, p0, Landroid/webkit/WebViewInputDispatcher;->mInitialDownX:F

    #@f
    float-to-int v2, v2

    #@10
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@13
    move-result v3

    #@14
    float-to-int v3, v3

    #@15
    sub-int v0, v2, v3

    #@17
    .line 592
    .local v0, deltaX:I
    iget v2, p0, Landroid/webkit/WebViewInputDispatcher;->mInitialDownY:F

    #@19
    float-to-int v2, v2

    #@1a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@1d
    move-result v3

    #@1e
    float-to-int v3, v3

    #@1f
    sub-int v1, v2, v3

    #@21
    .line 598
    .local v1, deltaY:I
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsSentMoveEventForX:Z

    #@23
    if-nez v2, :cond_31

    #@25
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    #@28
    move-result v2

    #@29
    iget v3, p0, Landroid/webkit/WebViewInputDispatcher;->mJScontentDragSlop:I

    #@2b
    if-le v2, v3, :cond_31

    #@2d
    .line 599
    iput-boolean v4, p0, Landroid/webkit/WebViewInputDispatcher;->mIsOverJSContentDragSlop:Z

    #@2f
    .line 600
    iput-boolean v4, p0, Landroid/webkit/WebViewInputDispatcher;->mIsSentMoveEventForX:Z

    #@31
    .line 602
    :cond_31
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsSentMoveEventForY:Z

    #@33
    if-nez v2, :cond_c

    #@35
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    #@38
    move-result v2

    #@39
    iget v3, p0, Landroid/webkit/WebViewInputDispatcher;->mJScontentDragSlop:I

    #@3b
    if-le v2, v3, :cond_c

    #@3d
    .line 603
    iput-boolean v4, p0, Landroid/webkit/WebViewInputDispatcher;->mIsOverJSContentDragSlop:Z

    #@3f
    .line 604
    iput-boolean v4, p0, Landroid/webkit/WebViewInputDispatcher;->mIsSentMoveEventForY:Z

    #@41
    goto :goto_c
.end method

.method private checkForSlopLocked(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 573
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsTapCandidate:Z

    #@2
    if-nez v2, :cond_5

    #@4
    .line 583
    :cond_4
    :goto_4
    return-void

    #@5
    .line 576
    :cond_5
    iget v2, p0, Landroid/webkit/WebViewInputDispatcher;->mInitialDownX:F

    #@7
    float-to-int v2, v2

    #@8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@b
    move-result v3

    #@c
    float-to-int v3, v3

    #@d
    sub-int v0, v2, v3

    #@f
    .line 577
    .local v0, deltaX:I
    iget v2, p0, Landroid/webkit/WebViewInputDispatcher;->mInitialDownY:F

    #@11
    float-to-int v2, v2

    #@12
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@15
    move-result v3

    #@16
    float-to-int v3, v3

    #@17
    sub-int v1, v2, v3

    #@19
    .line 578
    .local v1, deltaY:I
    mul-int v2, v0, v0

    #@1b
    mul-int v3, v1, v1

    #@1d
    add-int/2addr v2, v3

    #@1e
    int-to-float v2, v2

    #@1f
    iget v3, p0, Landroid/webkit/WebViewInputDispatcher;->mTouchSlopSquared:F

    #@21
    cmpl-float v2, v2, v3

    #@23
    if-lez v2, :cond_4

    #@25
    .line 579
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleLongPressLocked()V

    #@28
    .line 580
    const/4 v2, 0x0

    #@29
    iput-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsTapCandidate:Z

    #@2b
    .line 581
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->hideTapCandidateLocked()V

    #@2e
    goto :goto_4
.end method

.method private copyDispatchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    .registers 5
    .parameter "d"

    #@0
    .prologue
    .line 1134
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->obtainUninitializedDispatchEventLocked()Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@3
    move-result-object v0

    #@4
    .line 1135
    .local v0, copy:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    iget-object v1, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@6
    if-eqz v1, :cond_10

    #@8
    .line 1136
    iget-object v1, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@a
    invoke-virtual {v1}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;

    #@d
    move-result-object v1

    #@e
    iput-object v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@10
    .line 1138
    :cond_10
    iget v1, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@12
    iput v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@14
    .line 1139
    iget v1, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@16
    iput v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@18
    .line 1140
    iget-wide v1, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mTimeoutTime:J

    #@1a
    iput-wide v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mTimeoutTime:J

    #@1c
    .line 1141
    iget v1, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitXOffset:I

    #@1e
    iput v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitXOffset:I

    #@20
    .line 1142
    iget v1, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitYOffset:I

    #@22
    iput v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitYOffset:I

    #@24
    .line 1143
    iget v1, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitScale:F

    #@26
    iput v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitScale:F

    #@28
    .line 1144
    iget-object v1, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@2a
    iput-object v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@2c
    .line 1145
    return-object v0
.end method

.method private dispatchUiEvent(Landroid/view/MotionEvent;II)V
    .registers 5
    .parameter "event"
    .parameter "eventType"
    .parameter "flags"

    #@0
    .prologue
    .line 964
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

    #@2
    invoke-interface {v0, p1, p2, p3}, Landroid/webkit/WebViewInputDispatcher$UiCallbacks;->dispatchUiEvent(Landroid/view/MotionEvent;II)V

    #@5
    .line 965
    return-void
.end method

.method private dispatchUiEvents(Z)V
    .registers 9
    .parameter "calledFromHandler"

    #@0
    .prologue
    .line 911
    :cond_0
    :goto_0
    iget-object v5, p0, Landroid/webkit/WebViewInputDispatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 912
    :try_start_3
    iget-object v4, p0, Landroid/webkit/WebViewInputDispatcher;->mUiDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@5
    invoke-virtual {v4}, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->dequeue()Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@8
    move-result-object v0

    #@9
    .line 913
    .local v0, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    if-nez v0, :cond_1c

    #@b
    .line 914
    iget-boolean v4, p0, Landroid/webkit/WebViewInputDispatcher;->mUiDispatchScheduled:Z

    #@d
    if-eqz v4, :cond_1a

    #@f
    .line 915
    const/4 v4, 0x0

    #@10
    iput-boolean v4, p0, Landroid/webkit/WebViewInputDispatcher;->mUiDispatchScheduled:Z

    #@12
    .line 916
    if-nez p1, :cond_1a

    #@14
    .line 917
    iget-object v4, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@16
    const/4 v6, 0x1

    #@17
    invoke-virtual {v4, v6}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->removeMessages(I)V

    #@1a
    .line 920
    :cond_1a
    monitor-exit v5

    #@1b
    return-void

    #@1c
    .line 923
    :cond_1c
    iget-object v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@1e
    .line 924
    .local v1, event:Landroid/view/MotionEvent;
    if-eqz v1, :cond_3f

    #@20
    iget v4, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@22
    and-int/lit8 v4, v4, 0x8

    #@24
    if-eqz v4, :cond_3f

    #@26
    .line 925
    const/high16 v4, 0x3f80

    #@28
    iget v6, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitScale:F

    #@2a
    div-float/2addr v4, v6

    #@2b
    invoke-virtual {v1, v4}, Landroid/view/MotionEvent;->scale(F)V

    #@2e
    .line 926
    iget v4, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitXOffset:I

    #@30
    neg-int v4, v4

    #@31
    int-to-float v4, v4

    #@32
    iget v6, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitYOffset:I

    #@34
    neg-int v6, v6

    #@35
    int-to-float v6, v6

    #@36
    invoke-virtual {v1, v4, v6}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@39
    .line 927
    iget v4, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@3b
    and-int/lit8 v4, v4, -0x9

    #@3d
    iput v4, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@3f
    .line 930
    :cond_3f
    iget v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@41
    .line 931
    .local v2, eventType:I
    if-nez v2, :cond_49

    #@43
    .line 932
    iget-object v4, p0, Landroid/webkit/WebViewInputDispatcher;->mUiTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@45
    invoke-virtual {v4, v1}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->update(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@48
    move-result-object v1

    #@49
    .line 938
    :cond_49
    iget v3, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@4b
    .line 940
    .local v3, flags:I
    iget-object v4, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@4d
    if-ne v1, v4, :cond_52

    #@4f
    .line 941
    const/4 v4, 0x0

    #@50
    iput-object v4, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@52
    .line 943
    :cond_52
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->recycleDispatchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@55
    .line 945
    const/4 v4, 0x4

    #@56
    if-ne v2, v4, :cond_5b

    #@58
    .line 946
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->scheduleHideTapHighlightLocked()V

    #@5b
    .line 948
    :cond_5b
    monitor-exit v5
    :try_end_5c
    .catchall {:try_start_3 .. :try_end_5c} :catchall_65

    #@5c
    .line 951
    if-eqz v1, :cond_0

    #@5e
    .line 952
    invoke-direct {p0, v1, v2, v3}, Landroid/webkit/WebViewInputDispatcher;->dispatchUiEvent(Landroid/view/MotionEvent;II)V

    #@61
    .line 953
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    #@64
    goto :goto_0

    #@65
    .line 948
    .end local v0           #d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    .end local v1           #event:Landroid/view/MotionEvent;
    .end local v2           #eventType:I
    .end local v3           #flags:I
    :catchall_65
    move-exception v4

    #@66
    :try_start_66
    monitor-exit v5
    :try_end_67
    .catchall {:try_start_66 .. :try_end_67} :catchall_65

    #@67
    throw v4
.end method

.method private dispatchWebKitEvent(Landroid/view/MotionEvent;II)Z
    .registers 6
    .parameter "event"
    .parameter "eventType"
    .parameter "flags"

    #@0
    .prologue
    .line 811
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitCallbacks:Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;

    #@2
    invoke-interface {v1, p0, p1, p2, p3}, Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;->dispatchWebKitEvent(Landroid/webkit/WebViewInputDispatcher;Landroid/view/MotionEvent;II)Z

    #@5
    move-result v0

    #@6
    .line 816
    .local v0, preventDefault:Z
    return v0
.end method

.method private dispatchWebKitEvents(Z)V
    .registers 13
    .parameter "calledFromHandler"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 701
    :goto_2
    iget-object v8, p0, Landroid/webkit/WebViewInputDispatcher;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v8

    #@5
    .line 705
    :try_start_5
    iget-object v9, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@7
    iget-object v0, v9, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->mHead:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@9
    .line 706
    .local v0, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    if-nez v0, :cond_1c

    #@b
    .line 707
    iget-boolean v6, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchScheduled:Z

    #@d
    if-eqz v6, :cond_1a

    #@f
    .line 708
    const/4 v6, 0x0

    #@10
    iput-boolean v6, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchScheduled:Z

    #@12
    .line 709
    if-nez p1, :cond_1a

    #@14
    .line 710
    iget-object v6, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitHandler:Landroid/webkit/WebViewInputDispatcher$WebKitHandler;

    #@16
    const/4 v7, 0x1

    #@17
    invoke-virtual {v6, v7}, Landroid/webkit/WebViewInputDispatcher$WebKitHandler;->removeMessages(I)V

    #@1a
    .line 714
    :cond_1a
    monitor-exit v8

    #@1b
    return-void

    #@1c
    .line 717
    :cond_1c
    iget-object v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@1e
    .line 718
    .local v1, event:Landroid/view/MotionEvent;
    if-eqz v1, :cond_34

    #@20
    .line 719
    iget v9, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitXOffset:I

    #@22
    int-to-float v9, v9

    #@23
    iget v10, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitYOffset:I

    #@25
    int-to-float v10, v10

    #@26
    invoke-virtual {v1, v9, v10}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@29
    .line 720
    iget v9, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitScale:F

    #@2b
    invoke-virtual {v1, v9}, Landroid/view/MotionEvent;->scale(F)V

    #@2e
    .line 721
    iget v9, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@30
    or-int/lit8 v9, v9, 0x8

    #@32
    iput v9, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@34
    .line 724
    :cond_34
    iget v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@36
    .line 725
    .local v2, eventType:I
    if-nez v2, :cond_3e

    #@38
    .line 726
    iget-object v9, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@3a
    invoke-virtual {v9, v1}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->update(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@3d
    move-result-object v1

    #@3e
    .line 732
    :cond_3e
    iget v9, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@40
    or-int/lit8 v9, v9, 0x2

    #@42
    iput v9, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@44
    .line 733
    iget v3, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@46
    .line 734
    .local v3, flags:I
    monitor-exit v8
    :try_end_47
    .catchall {:try_start_5 .. :try_end_47} :catchall_7c

    #@47
    .line 738
    if-nez v1, :cond_7f

    #@49
    .line 739
    const/4 v4, 0x0

    #@4a
    .line 748
    .local v4, preventDefault:Z
    :cond_4a
    :goto_4a
    iget-object v8, p0, Landroid/webkit/WebViewInputDispatcher;->mLock:Ljava/lang/Object;

    #@4c
    monitor-enter v8

    #@4d
    .line 749
    :try_start_4d
    iget v3, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@4f
    .line 750
    and-int/lit8 v9, v3, -0x3

    #@51
    iput v9, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@53
    .line 751
    iget-object v9, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@55
    if-eq v1, v9, :cond_8c

    #@57
    move v5, v6

    #@58
    .line 753
    .local v5, recycleEvent:Z
    :goto_58
    and-int/lit8 v9, v3, 0x4

    #@5a
    if-eqz v9, :cond_8e

    #@5c
    .line 755
    if-eqz v1, :cond_67

    #@5e
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    #@61
    move-result v9

    #@62
    if-ne v9, v6, :cond_67

    #@64
    .line 756
    const/4 v9, 0x1

    #@65
    iput-boolean v9, p0, Landroid/webkit/WebViewInputDispatcher;->mPreventDoubleTap:Z

    #@67
    .line 759
    :cond_67
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->recycleDispatchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@6a
    .line 794
    :cond_6a
    :goto_6a
    if-eqz v1, :cond_71

    #@6c
    if-eqz v5, :cond_71

    #@6e
    .line 795
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    #@71
    .line 798
    :cond_71
    const/4 v9, 0x4

    #@72
    if-ne v2, v9, :cond_77

    #@74
    .line 799
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->scheduleHideTapHighlightLocked()V

    #@77
    .line 801
    :cond_77
    monitor-exit v8

    #@78
    goto :goto_2

    #@79
    .end local v5           #recycleEvent:Z
    :catchall_79
    move-exception v6

    #@7a
    monitor-exit v8
    :try_end_7b
    .catchall {:try_start_4d .. :try_end_7b} :catchall_79

    #@7b
    throw v6

    #@7c
    .line 734
    .end local v0           #d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    .end local v1           #event:Landroid/view/MotionEvent;
    .end local v2           #eventType:I
    .end local v3           #flags:I
    .end local v4           #preventDefault:Z
    :catchall_7c
    move-exception v6

    #@7d
    :try_start_7d
    monitor-exit v8
    :try_end_7e
    .catchall {:try_start_7d .. :try_end_7e} :catchall_7c

    #@7e
    throw v6

    #@7f
    .line 741
    .restart local v0       #d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    .restart local v1       #event:Landroid/view/MotionEvent;
    .restart local v2       #eventType:I
    .restart local v3       #flags:I
    :cond_7f
    invoke-direct {p0, v1, v2, v3}, Landroid/webkit/WebViewInputDispatcher;->dispatchWebKitEvent(Landroid/view/MotionEvent;II)Z

    #@82
    move-result v4

    #@83
    .line 743
    .restart local v4       #preventDefault:Z
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    #@86
    move-result v8

    #@87
    if-nez v8, :cond_4a

    #@89
    .line 744
    iput-boolean v4, p0, Landroid/webkit/WebViewInputDispatcher;->mIsPreventDefault:Z

    #@8b
    goto :goto_4a

    #@8c
    :cond_8c
    move v5, v7

    #@8d
    .line 751
    goto :goto_58

    #@8e
    .line 762
    .restart local v5       #recycleEvent:Z
    :cond_8e
    :try_start_8e
    sget-boolean v9, Landroid/webkit/WebViewInputDispatcher;->$assertionsDisabled:Z

    #@90
    if-nez v9, :cond_9e

    #@92
    iget-object v9, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@94
    iget-object v9, v9, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->mHead:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@96
    if-eq v9, v0, :cond_9e

    #@98
    new-instance v6, Ljava/lang/AssertionError;

    #@9a
    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    #@9d
    throw v6

    #@9e
    .line 763
    :cond_9e
    iget-object v9, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@a0
    invoke-virtual {v9}, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->dequeue()Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@a3
    .line 765
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->updateWebKitTimeoutLocked()V

    #@a6
    .line 767
    and-int/lit8 v9, v3, 0x1

    #@a8
    if-eqz v9, :cond_ae

    #@aa
    .line 769
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->recycleDispatchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@ad
    goto :goto_6a

    #@ae
    .line 771
    :cond_ae
    if-eqz v4, :cond_c8

    #@b0
    .line 772
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    #@b3
    move-result v9

    #@b4
    if-eq v9, v6, :cond_c1

    #@b6
    .line 774
    iget v9, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@b8
    if-nez v9, :cond_6a

    #@ba
    .line 775
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->enqueueUiCancelTouchEventIfNeededLocked()V

    #@bd
    .line 776
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleLongPressLocked()V

    #@c0
    goto :goto_6a

    #@c1
    .line 779
    :cond_c1
    const/4 v9, 0x1

    #@c2
    iput-boolean v9, p0, Landroid/webkit/WebViewInputDispatcher;->mPreventDoubleTap:Z

    #@c4
    .line 781
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->enqueueUiEventUnbatchedLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@c7
    goto :goto_6a

    #@c8
    .line 786
    :cond_c8
    iget-boolean v9, p0, Landroid/webkit/WebViewInputDispatcher;->mPreventDoubleTap:Z

    #@ca
    if-eqz v9, :cond_d1

    #@cc
    iget v9, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@ce
    const/4 v10, 0x5

    #@cf
    if-eq v9, v10, :cond_6a

    #@d1
    .line 788
    :cond_d1
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->enqueueUiEventUnbatchedLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
    :try_end_d4
    .catchall {:try_start_8e .. :try_end_d4} :catchall_79

    #@d4
    goto :goto_6a
.end method

.method private drainStaleWebKitEventsLocked()V
    .registers 4

    #@0
    .prologue
    .line 825
    iget-object v2, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@2
    iget-object v0, v2, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->mHead:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@4
    .line 828
    .local v0, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    :goto_4
    if-eqz v0, :cond_1f

    #@6
    iget-object v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@8
    if-eqz v2, :cond_1f

    #@a
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->isMoveEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_1f

    #@10
    iget-object v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@12
    invoke-direct {p0, v2}, Landroid/webkit/WebViewInputDispatcher;->isMoveEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_1f

    #@18
    .line 829
    iget-object v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@1a
    .line 830
    .local v1, next:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->skipWebKitEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@1d
    .line 831
    move-object v0, v1

    #@1e
    .line 832
    goto :goto_4

    #@1f
    .line 833
    .end local v1           #next:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    :cond_1f
    iget-object v2, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@21
    iput-object v0, v2, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->mHead:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@23
    .line 834
    return-void
.end method

.method private enqueueDoubleTapLocked(Landroid/view/MotionEvent;)V
    .registers 10
    .parameter "event"

    #@0
    .prologue
    .line 558
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@3
    move-result-object v1

    #@4
    .line 559
    .local v1, eventToEnqueue:Landroid/view/MotionEvent;
    const/4 v2, 0x5

    #@5
    const/4 v3, 0x0

    #@6
    iget v4, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitXOffset:I

    #@8
    iget v5, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitYOffset:I

    #@a
    iget v6, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitScale:F

    #@c
    move-object v0, p0

    #@d
    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebViewInputDispatcher;->obtainDispatchEventLocked(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@10
    move-result-object v7

    #@11
    .line 561
    .local v7, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    invoke-direct {p0, v7}, Landroid/webkit/WebViewInputDispatcher;->enqueueEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@14
    .line 562
    return-void
.end method

.method private enqueueEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 968
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->shouldSkipWebKit(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    .line 969
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->enqueueWebKitEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@9
    .line 973
    :goto_9
    return-void

    #@a
    .line 971
    :cond_a
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->enqueueUiEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@d
    goto :goto_9
.end method

.method private enqueueHitTestLocked(Landroid/view/MotionEvent;)V
    .registers 10
    .parameter "event"

    #@0
    .prologue
    .line 565
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

    #@2
    invoke-interface {v0}, Landroid/webkit/WebViewInputDispatcher$UiCallbacks;->clearPreviousHitTest()V

    #@5
    .line 566
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@8
    move-result-object v1

    #@9
    .line 567
    .local v1, eventToEnqueue:Landroid/view/MotionEvent;
    const/4 v2, 0x6

    #@a
    const/4 v3, 0x0

    #@b
    iget v4, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitXOffset:I

    #@d
    iget v5, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitYOffset:I

    #@f
    iget v6, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitScale:F

    #@11
    move-object v0, p0

    #@12
    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebViewInputDispatcher;->obtainDispatchEventLocked(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@15
    move-result-object v7

    #@16
    .line 569
    .local v7, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    invoke-direct {p0, v7}, Landroid/webkit/WebViewInputDispatcher;->enqueueEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@19
    .line 570
    return-void
.end method

.method private enqueueUiCancelTouchEventIfNeededLocked()V
    .registers 9

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1069
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@3
    invoke-virtual {v0}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->isCancelNeeded()Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_11

    #@9
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@b
    invoke-virtual {v0}, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->isEmpty()Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_1f

    #@11
    .line 1070
    :cond_11
    const/4 v1, 0x0

    #@12
    const/4 v3, 0x1

    #@13
    const/high16 v6, 0x3f80

    #@15
    move-object v0, p0

    #@16
    move v4, v2

    #@17
    move v5, v2

    #@18
    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebViewInputDispatcher;->obtainDispatchEventLocked(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@1b
    move-result-object v7

    #@1c
    .line 1072
    .local v7, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    invoke-direct {p0, v7}, Landroid/webkit/WebViewInputDispatcher;->enqueueUiEventUnbatchedLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@1f
    .line 1074
    .end local v7           #d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    :cond_1f
    return-void
.end method

.method private enqueueUiEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 1077
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@2
    iget-object v0, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->mTail:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@4
    invoke-direct {p0, p1, v0}, Landroid/webkit/WebViewInputDispatcher;->batchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_e

    #@a
    .line 1081
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->recycleDispatchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@d
    .line 1085
    :goto_d
    return-void

    #@e
    .line 1083
    :cond_e
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->enqueueUiEventUnbatchedLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@11
    goto :goto_d
.end method

.method private enqueueUiEventUnbatchedLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 1091
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->enqueue(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@5
    .line 1092
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->scheduleUiDispatchLocked()V

    #@8
    .line 1093
    return-void
.end method

.method private enqueueWebKitCancelTouchEventIfNeededLocked()V
    .registers 9

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1015
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@4
    invoke-virtual {v0}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->isCancelNeeded()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@c
    invoke-virtual {v0}, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->isEmpty()Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_21

    #@12
    .line 1016
    :cond_12
    const/4 v1, 0x0

    #@13
    const/high16 v6, 0x3f80

    #@15
    move-object v0, p0

    #@16
    move v4, v2

    #@17
    move v5, v2

    #@18
    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebViewInputDispatcher;->obtainDispatchEventLocked(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@1b
    move-result-object v7

    #@1c
    .line 1018
    .local v7, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    invoke-direct {p0, v7}, Landroid/webkit/WebViewInputDispatcher;->enqueueWebKitEventUnbatchedLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@1f
    .line 1019
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchEventsToWebKitUntilNextGesture:Z

    #@21
    .line 1021
    .end local v7           #d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    :cond_21
    return-void
.end method

.method private enqueueWebKitEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 1024
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@2
    iget-object v0, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->mTail:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@4
    invoke-direct {p0, p1, v0}, Landroid/webkit/WebViewInputDispatcher;->batchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_e

    #@a
    .line 1028
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->recycleDispatchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@d
    .line 1032
    :goto_d
    return-void

    #@e
    .line 1030
    :cond_e
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->enqueueWebKitEventUnbatchedLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@11
    goto :goto_d
.end method

.method private enqueueWebKitEventUnbatchedLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 1038
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->enqueue(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@5
    .line 1039
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->scheduleWebKitDispatchLocked()V

    #@8
    .line 1040
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->updateWebKitTimeoutLocked()V

    #@b
    .line 1041
    return-void
.end method

.method private handleWebKitTimeout()V
    .registers 6

    #@0
    .prologue
    .line 844
    iget-object v3, p0, Landroid/webkit/WebViewInputDispatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 845
    :try_start_3
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTimeoutScheduled:Z

    #@5
    if-nez v2, :cond_9

    #@7
    .line 846
    monitor-exit v3

    #@8
    .line 882
    :goto_8
    return-void

    #@9
    .line 848
    :cond_9
    const/4 v2, 0x0

    #@a
    iput-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTimeoutScheduled:Z

    #@c
    .line 855
    iget-object v2, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@e
    invoke-virtual {v2}, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->dequeueList()Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@11
    move-result-object v0

    #@12
    .line 859
    .local v0, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    iget v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@14
    and-int/lit8 v2, v2, 0x2

    #@16
    if-eqz v2, :cond_26

    #@18
    .line 860
    iget v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@1a
    or-int/lit8 v2, v2, 0x4

    #@1c
    iput v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@1e
    .line 861
    iget v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@20
    and-int/lit8 v2, v2, 0x1

    #@22
    if-eqz v2, :cond_34

    #@24
    .line 862
    iget-object v0, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@26
    .line 870
    :cond_26
    :goto_26
    if-eqz v0, :cond_42

    #@28
    .line 871
    iget-object v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@2a
    .line 872
    .local v1, next:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    iget v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@2c
    const/4 v4, 0x5

    #@2d
    if-eq v2, v4, :cond_32

    #@2f
    .line 873
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->skipWebKitEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@32
    .line 874
    :cond_32
    move-object v0, v1

    #@33
    .line 875
    goto :goto_26

    #@34
    .line 864
    .end local v1           #next:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    :cond_34
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->copyDispatchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@37
    move-result-object v0

    #@38
    .line 865
    iget v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@3a
    and-int/lit8 v2, v2, -0x3

    #@3c
    iput v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@3e
    goto :goto_26

    #@3f
    .line 881
    .end local v0           #d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    :catchall_3f
    move-exception v2

    #@40
    monitor-exit v3
    :try_end_41
    .catchall {:try_start_3 .. :try_end_41} :catchall_3f

    #@41
    throw v2

    #@42
    .line 880
    .restart local v0       #d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    :cond_42
    :try_start_42
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->enqueueWebKitCancelTouchEventIfNeededLocked()V

    #@45
    .line 881
    monitor-exit v3
    :try_end_46
    .catchall {:try_start_42 .. :try_end_46} :catchall_3f

    #@46
    goto :goto_8
.end method

.method private hideTapCandidateLocked()V
    .registers 3

    #@0
    .prologue
    .line 431
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleHideTapHighlightLocked()V

    #@3
    .line 432
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleShowTapHighlightLocked()V

    #@6
    .line 433
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-interface {v0, v1}, Landroid/webkit/WebViewInputDispatcher$UiCallbacks;->showTapHighlight(Z)V

    #@c
    .line 434
    return-void
.end method

.method private isClickCandidateLocked(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 548
    if-eqz p1, :cond_e

    #@4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@7
    move-result v4

    #@8
    if-ne v4, v2, :cond_e

    #@a
    iget-boolean v4, p0, Landroid/webkit/WebViewInputDispatcher;->mIsTapCandidate:Z

    #@c
    if-nez v4, :cond_10

    #@e
    :cond_e
    move v2, v3

    #@f
    .line 554
    :cond_f
    :goto_f
    return v2

    #@10
    .line 553
    :cond_10
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@13
    move-result-wide v4

    #@14
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    #@17
    move-result-wide v6

    #@18
    sub-long v0, v4, v6

    #@1a
    .line 554
    .local v0, downDuration:J
    sget v4, Landroid/webkit/WebViewInputDispatcher;->LONG_PRESS_TIMEOUT:I

    #@1c
    int-to-long v4, v4

    #@1d
    cmp-long v4, v0, v4

    #@1f
    if-ltz v4, :cond_f

    #@21
    move v2, v3

    #@22
    goto :goto_f
.end method

.method private isMoveEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Z
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 820
    iget-object v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@6
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    #@9
    move-result v0

    #@a
    const/4 v1, 0x2

    #@b
    if-ne v0, v1, :cond_f

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method private obtainDispatchEventLocked(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    .registers 12
    .parameter "event"
    .parameter "eventType"
    .parameter "flags"
    .parameter "webKitXOffset"
    .parameter "webKitYOffset"
    .parameter "webKitScale"

    #@0
    .prologue
    .line 1119
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->obtainUninitializedDispatchEventLocked()Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@3
    move-result-object v0

    #@4
    .line 1120
    .local v0, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    iput-object p1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@6
    .line 1121
    iput p2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@8
    .line 1122
    iput p3, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@a
    .line 1123
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@d
    move-result-wide v1

    #@e
    const-wide/16 v3, 0xc8

    #@10
    add-long/2addr v1, v3

    #@11
    iput-wide v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mTimeoutTime:J

    #@13
    .line 1124
    iput p4, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitXOffset:I

    #@15
    .line 1125
    iput p5, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitYOffset:I

    #@17
    .line 1126
    iput p6, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitScale:F

    #@19
    .line 1130
    return-object v0
.end method

.method private obtainUninitializedDispatchEventLocked()Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1149
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mDispatchEventPool:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@3
    .line 1150
    .local v0, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    if-eqz v0, :cond_12

    #@5
    .line 1151
    iget v1, p0, Landroid/webkit/WebViewInputDispatcher;->mDispatchEventPoolSize:I

    #@7
    add-int/lit8 v1, v1, -0x1

    #@9
    iput v1, p0, Landroid/webkit/WebViewInputDispatcher;->mDispatchEventPoolSize:I

    #@b
    .line 1152
    iget-object v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@d
    iput-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mDispatchEventPool:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@f
    .line 1153
    iput-object v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@11
    .line 1157
    :goto_11
    return-object v0

    #@12
    .line 1155
    :cond_12
    new-instance v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@14
    .end local v0           #d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    invoke-direct {v0, v2}, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;-><init>(Landroid/webkit/WebViewInputDispatcher$1;)V

    #@17
    .restart local v0       #d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    goto :goto_11
.end method

.method private postClick()V
    .registers 12

    #@0
    .prologue
    .line 501
    iget-object v10, p0, Landroid/webkit/WebViewInputDispatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v10

    #@3
    .line 502
    :try_start_3
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostClickScheduled:Z

    #@5
    if-nez v0, :cond_9

    #@7
    .line 503
    monitor-exit v10

    #@8
    .line 532
    :goto_8
    return-void

    #@9
    .line 505
    :cond_9
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostClickScheduled:Z

    #@c
    .line 507
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@e
    invoke-virtual {v0}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->getLastEvent()Landroid/view/MotionEvent;

    #@11
    move-result-object v9

    #@12
    .line 512
    .local v9, event:Landroid/view/MotionEvent;
    if-eqz v9, :cond_23

    #@14
    invoke-virtual {v9}, Landroid/view/MotionEvent;->getAction()I

    #@17
    move-result v0

    #@18
    const/4 v2, 0x1

    #@19
    if-ne v0, v2, :cond_23

    #@1b
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendClickEventsToWebKit:Z

    #@1d
    if-nez v0, :cond_23

    #@1f
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mIsPreventDefault:Z

    #@21
    if-eqz v0, :cond_2b

    #@23
    .line 515
    :cond_23
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->scheduleHideTapHighlightLocked()V

    #@26
    .line 516
    monitor-exit v10

    #@27
    goto :goto_8

    #@28
    .line 531
    .end local v9           #event:Landroid/view/MotionEvent;
    :catchall_28
    move-exception v0

    #@29
    monitor-exit v10
    :try_end_2a
    .catchall {:try_start_3 .. :try_end_2a} :catchall_28

    #@2a
    throw v0

    #@2b
    .line 520
    .restart local v9       #event:Landroid/view/MotionEvent;
    :cond_2b
    :try_start_2b
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->showTapCandidateLocked()V

    #@2e
    .line 521
    invoke-static {v9}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@31
    move-result-object v1

    #@32
    .line 522
    .local v1, eventToEnqueue:Landroid/view/MotionEvent;
    const/4 v2, 0x4

    #@33
    const/4 v3, 0x0

    #@34
    iget v4, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitXOffset:I

    #@36
    iget v5, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitYOffset:I

    #@38
    iget v6, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitScale:F

    #@3a
    move-object v0, p0

    #@3b
    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebViewInputDispatcher;->obtainDispatchEventLocked(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@3e
    move-result-object v7

    #@3f
    .line 525
    .local v7, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    invoke-direct {p0, v7}, Landroid/webkit/WebViewInputDispatcher;->copyDispatchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@42
    move-result-object v8

    #@43
    .line 526
    .local v8, dCopyed:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    iget v0, v7, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@45
    or-int/lit8 v0, v0, 0x10

    #@47
    iput v0, v7, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@49
    .line 527
    invoke-direct {p0, v7}, Landroid/webkit/WebViewInputDispatcher;->enqueueUiEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@4c
    .line 528
    invoke-direct {p0, v8}, Landroid/webkit/WebViewInputDispatcher;->enqueueEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@4f
    .line 531
    monitor-exit v10
    :try_end_50
    .catchall {:try_start_2b .. :try_end_50} :catchall_28

    #@50
    goto :goto_8
.end method

.method private postLongPress()V
    .registers 11

    #@0
    .prologue
    .line 401
    iget-object v9, p0, Landroid/webkit/WebViewInputDispatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v9

    #@3
    .line 402
    :try_start_3
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLongPressScheduled:Z

    #@5
    if-nez v0, :cond_9

    #@7
    .line 403
    monitor-exit v9

    #@8
    .line 428
    :goto_8
    return-void

    #@9
    .line 405
    :cond_9
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLongPressScheduled:Z

    #@c
    .line 407
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@e
    invoke-virtual {v0}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->getLastEvent()Landroid/view/MotionEvent;

    #@11
    move-result-object v8

    #@12
    .line 408
    .local v8, event:Landroid/view/MotionEvent;
    if-nez v8, :cond_19

    #@14
    .line 409
    monitor-exit v9

    #@15
    goto :goto_8

    #@16
    .line 427
    .end local v8           #event:Landroid/view/MotionEvent;
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v9
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v0

    #@19
    .line 412
    .restart local v8       #event:Landroid/view/MotionEvent;
    :cond_19
    :try_start_19
    invoke-virtual {v8}, Landroid/view/MotionEvent;->getActionMasked()I

    #@1c
    move-result v0

    #@1d
    packed-switch v0, :pswitch_data_3c

    #@20
    .line 419
    :pswitch_20
    monitor-exit v9

    #@21
    goto :goto_8

    #@22
    .line 422
    :pswitch_22
    invoke-static {v8}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@25
    move-result-object v1

    #@26
    .line 423
    .local v1, eventToEnqueue:Landroid/view/MotionEvent;
    const/4 v0, 0x2

    #@27
    invoke-virtual {v1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    #@2a
    .line 424
    const/4 v2, 0x3

    #@2b
    const/4 v3, 0x0

    #@2c
    iget v4, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitXOffset:I

    #@2e
    iget v5, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitYOffset:I

    #@30
    iget v6, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitScale:F

    #@32
    move-object v0, p0

    #@33
    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebViewInputDispatcher;->obtainDispatchEventLocked(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@36
    move-result-object v7

    #@37
    .line 426
    .local v7, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    invoke-direct {p0, v7}, Landroid/webkit/WebViewInputDispatcher;->enqueueEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@3a
    .line 427
    monitor-exit v9
    :try_end_3b
    .catchall {:try_start_19 .. :try_end_3b} :catchall_16

    #@3b
    goto :goto_8

    #@3c
    .line 412
    :pswitch_data_3c
    .packed-switch 0x0
        :pswitch_22
        :pswitch_20
        :pswitch_22
        :pswitch_20
        :pswitch_20
        :pswitch_22
        :pswitch_22
    .end packed-switch
.end method

.method private postShowTapHighlight(Z)V
    .registers 4
    .parameter "show"

    #@0
    .prologue
    .line 471
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 472
    if-eqz p1, :cond_18

    #@5
    .line 473
    :try_start_5
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostShowTapHighlightScheduled:Z

    #@7
    if-nez v0, :cond_b

    #@9
    .line 474
    monitor-exit v1

    #@a
    .line 485
    :goto_a
    return-void

    #@b
    .line 476
    :cond_b
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostShowTapHighlightScheduled:Z

    #@e
    .line 483
    :goto_e
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

    #@10
    invoke-interface {v0, p1}, Landroid/webkit/WebViewInputDispatcher$UiCallbacks;->showTapHighlight(Z)V

    #@13
    .line 484
    monitor-exit v1

    #@14
    goto :goto_a

    #@15
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_15

    #@17
    throw v0

    #@18
    .line 478
    :cond_18
    :try_start_18
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostHideTapHighlightScheduled:Z

    #@1a
    if-nez v0, :cond_1e

    #@1c
    .line 479
    monitor-exit v1

    #@1d
    goto :goto_a

    #@1e
    .line 481
    :cond_1e
    const/4 v0, 0x0

    #@1f
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostHideTapHighlightScheduled:Z
    :try_end_21
    .catchall {:try_start_18 .. :try_end_21} :catchall_15

    #@21
    goto :goto_e
.end method

.method private recycleDispatchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
    .registers 4
    .parameter "d"

    #@0
    .prologue
    .line 1161
    iget-object v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 1162
    iget-object v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@6
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    #@9
    .line 1163
    const/4 v0, 0x0

    #@a
    iput-object v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@c
    .line 1166
    :cond_c
    iget v0, p0, Landroid/webkit/WebViewInputDispatcher;->mDispatchEventPoolSize:I

    #@e
    const/16 v1, 0xa

    #@10
    if-ge v0, v1, :cond_1e

    #@12
    .line 1167
    iget v0, p0, Landroid/webkit/WebViewInputDispatcher;->mDispatchEventPoolSize:I

    #@14
    add-int/lit8 v0, v0, 0x1

    #@16
    iput v0, p0, Landroid/webkit/WebViewInputDispatcher;->mDispatchEventPoolSize:I

    #@18
    .line 1168
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mDispatchEventPool:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@1a
    iput-object v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@1c
    .line 1169
    iput-object p1, p0, Landroid/webkit/WebViewInputDispatcher;->mDispatchEventPool:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@1e
    .line 1171
    :cond_1e
    return-void
.end method

.method private scheduleClickLocked()V
    .registers 5

    #@0
    .prologue
    .line 488
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleClickLocked()V

    #@3
    .line 489
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostClickScheduled:Z

    #@6
    .line 490
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@8
    const/4 v1, 0x4

    #@9
    sget v2, Landroid/webkit/WebViewInputDispatcher;->DOUBLE_TAP_TIMEOUT:I

    #@b
    int-to-long v2, v2

    #@c
    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->sendEmptyMessageDelayed(IJ)Z

    #@f
    .line 491
    return-void
.end method

.method private scheduleHideTapHighlightLocked()V
    .registers 5

    #@0
    .prologue
    .line 457
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleHideTapHighlightLocked()V

    #@3
    .line 458
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostHideTapHighlightScheduled:Z

    #@6
    .line 459
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@8
    const/4 v1, 0x6

    #@9
    sget v2, Landroid/webkit/WebViewInputDispatcher;->PRESSED_STATE_DURATION:I

    #@b
    int-to-long v2, v2

    #@c
    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->sendEmptyMessageDelayed(IJ)Z

    #@f
    .line 461
    return-void
.end method

.method private scheduleLongPressLocked()V
    .registers 5

    #@0
    .prologue
    .line 387
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleLongPressLocked()V

    #@3
    .line 388
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLongPressScheduled:Z

    #@6
    .line 389
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@8
    const/4 v1, 0x3

    #@9
    sget v2, Landroid/webkit/WebViewInputDispatcher;->LONG_PRESS_TIMEOUT:I

    #@b
    int-to-long v2, v2

    #@c
    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->sendEmptyMessageDelayed(IJ)Z

    #@f
    .line 391
    return-void
.end method

.method private scheduleShowTapHighlightLocked()V
    .registers 5

    #@0
    .prologue
    .line 443
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleShowTapHighlightLocked()V

    #@3
    .line 444
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostShowTapHighlightScheduled:Z

    #@6
    .line 445
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@8
    const/4 v1, 0x5

    #@9
    sget v2, Landroid/webkit/WebViewInputDispatcher;->TAP_TIMEOUT:I

    #@b
    int-to-long v2, v2

    #@c
    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->sendEmptyMessageDelayed(IJ)Z

    #@f
    .line 447
    return-void
.end method

.method private scheduleUiDispatchLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1096
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiDispatchScheduled:Z

    #@3
    if-nez v0, :cond_c

    #@5
    .line 1097
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@7
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->sendEmptyMessage(I)Z

    #@a
    .line 1098
    iput-boolean v1, p0, Landroid/webkit/WebViewInputDispatcher;->mUiDispatchScheduled:Z

    #@c
    .line 1100
    :cond_c
    return-void
.end method

.method private scheduleWebKitDispatchLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1044
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchScheduled:Z

    #@3
    if-nez v0, :cond_c

    #@5
    .line 1045
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitHandler:Landroid/webkit/WebViewInputDispatcher$WebKitHandler;

    #@7
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewInputDispatcher$WebKitHandler;->sendEmptyMessage(I)Z

    #@a
    .line 1046
    iput-boolean v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchScheduled:Z

    #@c
    .line 1048
    :cond_c
    return-void
.end method

.method private shouldSkipWebKit(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)Z
    .registers 6
    .parameter "d"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x1

    #@3
    .line 976
    iget v2, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@5
    packed-switch v2, :pswitch_data_42

    #@8
    :pswitch_8
    move v0, v1

    #@9
    .line 1009
    :cond_9
    :goto_9
    :pswitch_9
    return v0

    #@a
    .line 992
    :pswitch_a
    iget-object v2, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@c
    if-eqz v2, :cond_22

    #@e
    iget-object v2, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@10
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getActionMasked()I

    #@13
    move-result v2

    #@14
    if-ne v2, v3, :cond_22

    #@16
    invoke-virtual {p0}, Landroid/webkit/WebViewInputDispatcher;->isTouchDragMode()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_22

    #@1c
    .line 994
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsOverJSContentDragSlop:Z

    #@1e
    if-nez v2, :cond_9

    #@20
    move v0, v1

    #@21
    .line 997
    goto :goto_9

    #@22
    .line 1000
    :cond_22
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mIsTapCandidate:Z

    #@24
    if-eqz v2, :cond_34

    #@26
    iget-object v2, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@28
    if-eqz v2, :cond_34

    #@2a
    iget-object v2, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEvent:Landroid/view/MotionEvent;

    #@2c
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getActionMasked()I

    #@2f
    move-result v2

    #@30
    if-ne v2, v3, :cond_34

    #@32
    move v0, v1

    #@33
    .line 1002
    goto :goto_9

    #@34
    .line 1005
    :cond_34
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mPostSendTouchEventsToWebKit:Z

    #@36
    if-eqz v2, :cond_40

    #@38
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchEventsToWebKitUntilNextGesture:Z

    #@3a
    if-nez v2, :cond_40

    #@3c
    iget-boolean v2, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchMoveEventsToWebKit:Z

    #@3e
    if-eqz v2, :cond_9

    #@40
    :cond_40
    move v0, v1

    #@41
    goto :goto_9

    #@42
    .line 976
    :pswitch_data_42
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method private showTapCandidateLocked()V
    .registers 3

    #@0
    .prologue
    .line 437
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleHideTapHighlightLocked()V

    #@3
    .line 438
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleShowTapHighlightLocked()V

    #@6
    .line 439
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

    #@8
    const/4 v1, 0x1

    #@9
    invoke-interface {v0, v1}, Landroid/webkit/WebViewInputDispatcher$UiCallbacks;->showTapHighlight(Z)V

    #@c
    .line 440
    return-void
.end method

.method private skipWebKitEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 885
    const/4 v0, 0x0

    #@1
    iput-object v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mNext:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@3
    .line 886
    iget v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@5
    and-int/lit8 v0, v0, 0x1

    #@7
    if-eqz v0, :cond_d

    #@9
    .line 887
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->recycleDispatchEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@c
    .line 892
    :goto_c
    return-void

    #@d
    .line 889
    :cond_d
    iget v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@f
    or-int/lit8 v0, v0, 0x4

    #@11
    iput v0, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mFlags:I

    #@13
    .line 890
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher;->enqueueUiEventUnbatchedLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@16
    goto :goto_c
.end method

.method private unscheduleClickLocked()V
    .registers 3

    #@0
    .prologue
    .line 494
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostClickScheduled:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 495
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostClickScheduled:Z

    #@7
    .line 496
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@9
    const/4 v1, 0x4

    #@a
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->removeMessages(I)V

    #@d
    .line 498
    :cond_d
    return-void
.end method

.method private unscheduleHideTapHighlightLocked()V
    .registers 3

    #@0
    .prologue
    .line 464
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostHideTapHighlightScheduled:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 465
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostHideTapHighlightScheduled:Z

    #@7
    .line 466
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@9
    const/4 v1, 0x6

    #@a
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->removeMessages(I)V

    #@d
    .line 468
    :cond_d
    return-void
.end method

.method private unscheduleLongPressLocked()V
    .registers 3

    #@0
    .prologue
    .line 394
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLongPressScheduled:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 395
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLongPressScheduled:Z

    #@7
    .line 396
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@9
    const/4 v1, 0x3

    #@a
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->removeMessages(I)V

    #@d
    .line 398
    :cond_d
    return-void
.end method

.method private unscheduleShowTapHighlightLocked()V
    .registers 3

    #@0
    .prologue
    .line 450
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostShowTapHighlightScheduled:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 451
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostShowTapHighlightScheduled:Z

    #@7
    .line 452
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@9
    const/4 v1, 0x5

    #@a
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->removeMessages(I)V

    #@d
    .line 454
    :cond_d
    return-void
.end method

.method private updateStateTrackersLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;Landroid/view/MotionEvent;)V
    .registers 8
    .parameter "d"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 614
    iget v2, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitXOffset:I

    #@5
    iput v2, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitXOffset:I

    #@7
    .line 615
    iget v2, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitYOffset:I

    #@9
    iput v2, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitYOffset:I

    #@b
    .line 616
    iget v2, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mWebKitScale:F

    #@d
    iput v2, p0, Landroid/webkit/WebViewInputDispatcher;->mPostLastWebKitScale:F

    #@f
    .line 617
    if-eqz p2, :cond_1a

    #@11
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    #@14
    move-result v0

    #@15
    .line 618
    .local v0, action:I
    :goto_15
    iget v2, p1, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mEventType:I

    #@17
    if-eqz v2, :cond_1c

    #@19
    .line 667
    :cond_19
    :goto_19
    return-void

    #@1a
    .end local v0           #action:I
    :cond_1a
    move v0, v1

    #@1b
    .line 617
    goto :goto_15

    #@1c
    .line 622
    .restart local v0       #action:I
    :cond_1c
    if-eq v0, v1, :cond_24

    #@1e
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    #@21
    move-result v1

    #@22
    if-le v1, v4, :cond_39

    #@24
    .line 624
    :cond_24
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleLongPressLocked()V

    #@27
    .line 625
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleClickLocked()V

    #@2a
    .line 626
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->hideTapCandidateLocked()V

    #@2d
    .line 627
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mIsDoubleTapCandidate:Z

    #@2f
    .line 628
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mIsTapCandidate:Z

    #@31
    .line 629
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->hideTapCandidateLocked()V

    #@34
    .line 630
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mIsPreventDefault:Z

    #@36
    .line 631
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mPreventDoubleTap:Z

    #@38
    goto :goto_19

    #@39
    .line 632
    :cond_39
    if-nez v0, :cond_66

    #@3b
    .line 633
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mIsPreventDefault:Z

    #@3d
    .line 634
    invoke-direct {p0, p2}, Landroid/webkit/WebViewInputDispatcher;->checkForDoubleTapOnDownLocked(Landroid/view/MotionEvent;)V

    #@40
    .line 635
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->scheduleLongPressLocked()V

    #@43
    .line 636
    iput-boolean v4, p0, Landroid/webkit/WebViewInputDispatcher;->mIsTapCandidate:Z

    #@45
    .line 638
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mIsSentMoveEventForX:Z

    #@47
    .line 639
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mIsSentMoveEventForY:Z

    #@49
    .line 641
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    #@4c
    move-result v1

    #@4d
    iput v1, p0, Landroid/webkit/WebViewInputDispatcher;->mInitialDownX:F

    #@4f
    .line 642
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    #@52
    move-result v1

    #@53
    iput v1, p0, Landroid/webkit/WebViewInputDispatcher;->mInitialDownY:F

    #@55
    .line 643
    invoke-direct {p0, p2}, Landroid/webkit/WebViewInputDispatcher;->enqueueHitTestLocked(Landroid/view/MotionEvent;)V

    #@58
    .line 644
    iget-boolean v1, p0, Landroid/webkit/WebViewInputDispatcher;->mIsDoubleTapCandidate:Z

    #@5a
    if-eqz v1, :cond_60

    #@5c
    .line 645
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->hideTapCandidateLocked()V

    #@5f
    goto :goto_19

    #@60
    .line 647
    :cond_60
    iput-boolean v3, p0, Landroid/webkit/WebViewInputDispatcher;->mPreventDoubleTap:Z

    #@62
    .line 648
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->scheduleShowTapHighlightLocked()V

    #@65
    goto :goto_19

    #@66
    .line 650
    :cond_66
    if-ne v0, v4, :cond_84

    #@68
    .line 651
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleLongPressLocked()V

    #@6b
    .line 652
    invoke-direct {p0, p2}, Landroid/webkit/WebViewInputDispatcher;->isClickCandidateLocked(Landroid/view/MotionEvent;)Z

    #@6e
    move-result v1

    #@6f
    if-eqz v1, :cond_80

    #@71
    .line 653
    iget-boolean v1, p0, Landroid/webkit/WebViewInputDispatcher;->mIsDoubleTapCandidate:Z

    #@73
    if-eqz v1, :cond_7c

    #@75
    .line 654
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->hideTapCandidateLocked()V

    #@78
    .line 655
    invoke-direct {p0, p2}, Landroid/webkit/WebViewInputDispatcher;->enqueueDoubleTapLocked(Landroid/view/MotionEvent;)V

    #@7b
    goto :goto_19

    #@7c
    .line 657
    :cond_7c
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->scheduleClickLocked()V

    #@7f
    goto :goto_19

    #@80
    .line 660
    :cond_80
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->hideTapCandidateLocked()V

    #@83
    goto :goto_19

    #@84
    .line 662
    :cond_84
    const/4 v1, 0x2

    #@85
    if-ne v0, v1, :cond_19

    #@87
    .line 664
    invoke-direct {p0, p2}, Landroid/webkit/WebViewInputDispatcher;->checkForJSContentDragSlop(Landroid/view/MotionEvent;)V

    #@8a
    .line 665
    invoke-direct {p0, p2}, Landroid/webkit/WebViewInputDispatcher;->checkForSlopLocked(Landroid/view/MotionEvent;)V

    #@8d
    goto :goto_19
.end method

.method private updateWebKitTimeoutLocked()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    .line 1051
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitDispatchEventQueue:Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;

    #@3
    iget-object v0, v1, Landroid/webkit/WebViewInputDispatcher$DispatchEventQueue;->mHead:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@5
    .line 1052
    .local v0, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    if-eqz v0, :cond_14

    #@7
    iget-boolean v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTimeoutScheduled:Z

    #@9
    if-eqz v1, :cond_14

    #@b
    iget-wide v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTimeoutTime:J

    #@d
    iget-wide v3, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mTimeoutTime:J

    #@f
    cmp-long v1, v1, v3

    #@11
    if-nez v1, :cond_14

    #@13
    .line 1064
    :cond_13
    :goto_13
    return-void

    #@14
    .line 1055
    :cond_14
    iget-boolean v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTimeoutScheduled:Z

    #@16
    if-eqz v1, :cond_20

    #@18
    .line 1056
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@1a
    invoke-virtual {v1, v5}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->removeMessages(I)V

    #@1d
    .line 1057
    const/4 v1, 0x0

    #@1e
    iput-boolean v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTimeoutScheduled:Z

    #@20
    .line 1059
    :cond_20
    if-eqz v0, :cond_13

    #@22
    .line 1060
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mUiHandler:Landroid/webkit/WebViewInputDispatcher$UiHandler;

    #@24
    iget-wide v2, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mTimeoutTime:J

    #@26
    invoke-virtual {v1, v5, v2, v3}, Landroid/webkit/WebViewInputDispatcher$UiHandler;->sendEmptyMessageAtTime(IJ)Z

    #@29
    .line 1061
    const/4 v1, 0x1

    #@2a
    iput-boolean v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTimeoutScheduled:Z

    #@2c
    .line 1062
    iget-wide v1, v0, Landroid/webkit/WebViewInputDispatcher$DispatchEvent;->mTimeoutTime:J

    #@2e
    iput-wide v1, p0, Landroid/webkit/WebViewInputDispatcher;->mWebKitTimeoutTime:J

    #@30
    goto :goto_13
.end method


# virtual methods
.method public dispatchUiEvents()V
    .registers 2

    #@0
    .prologue
    .line 903
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->dispatchUiEvents(Z)V

    #@4
    .line 904
    return-void
.end method

.method public dispatchWebKitEvents()V
    .registers 2

    #@0
    .prologue
    .line 690
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher;->dispatchWebKitEvents(Z)V

    #@4
    .line 691
    return-void
.end method

.method public isTouchDragMode()Z
    .registers 2

    #@0
    .prologue
    .line 672
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mTouchDragMode:Z

    #@2
    return v0
.end method

.method public postPointerEvent(Landroid/view/MotionEvent;IIF)Z
    .registers 16
    .parameter "event"
    .parameter "webKitXOffset"
    .parameter "webKitYOffset"
    .parameter "webKitScale"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 297
    if-nez p1, :cond_c

    #@4
    .line 298
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v3, "event cannot be null"

    #@8
    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 305
    :cond_c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@f
    move-result v7

    #@10
    .line 307
    .local v7, action:I
    packed-switch v7, :pswitch_data_96

    #@13
    .line 383
    :goto_13
    :pswitch_13
    return v0

    #@14
    .line 314
    :pswitch_14
    const/4 v2, 0x0

    #@15
    .line 328
    .local v2, eventType:I
    :goto_15
    iget-object v10, p0, Landroid/webkit/WebViewInputDispatcher;->mLock:Ljava/lang/Object;

    #@17
    monitor-enter v10

    #@18
    .line 330
    move-object v1, p1

    #@19
    .line 331
    .local v1, eventToEnqueue:Landroid/view/MotionEvent;
    if-nez v2, :cond_6d

    #@1b
    .line 332
    :try_start_1b
    iget-object v3, p0, Landroid/webkit/WebViewInputDispatcher;->mPostTouchStream:Landroid/webkit/WebViewInputDispatcher$TouchStream;

    #@1d
    invoke-virtual {v3, p1}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->update(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@20
    move-result-object v1

    #@21
    .line 333
    if-nez v1, :cond_35

    #@23
    .line 337
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleLongPressLocked()V

    #@26
    .line 338
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->unscheduleClickLocked()V

    #@29
    .line 339
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->hideTapCandidateLocked()V

    #@2c
    .line 340
    monitor-exit v10

    #@2d
    goto :goto_13

    #@2e
    .line 382
    :catchall_2e
    move-exception v0

    #@2f
    monitor-exit v10
    :try_end_30
    .catchall {:try_start_1b .. :try_end_30} :catchall_2e

    #@30
    throw v0

    #@31
    .line 317
    .end local v1           #eventToEnqueue:Landroid/view/MotionEvent;
    .end local v2           #eventType:I
    :pswitch_31
    const/4 v2, 0x2

    #@32
    .line 318
    .restart local v2       #eventType:I
    goto :goto_15

    #@33
    .line 322
    .end local v2           #eventType:I
    :pswitch_33
    const/4 v2, 0x1

    #@34
    .line 323
    .restart local v2       #eventType:I
    goto :goto_15

    #@35
    .line 343
    .restart local v1       #eventToEnqueue:Landroid/view/MotionEvent;
    :cond_35
    if-nez v7, :cond_46

    #@37
    :try_start_37
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostSendTouchEventsToWebKit:Z

    #@39
    if-eqz v0, :cond_46

    #@3b
    .line 344
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

    #@3d
    invoke-interface {v0, v1}, Landroid/webkit/WebViewInputDispatcher$UiCallbacks;->shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    #@40
    move-result v0

    #@41
    if-eqz v0, :cond_85

    #@43
    .line 345
    const/4 v0, 0x1

    #@44
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchEventsToWebKitUntilNextGesture:Z

    #@46
    .line 352
    :cond_46
    :goto_46
    if-nez v7, :cond_56

    #@48
    .line 353
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

    #@4a
    invoke-interface {v0, v1}, Landroid/webkit/WebViewInputDispatcher$UiCallbacks;->shouldIgnoreClickEvent(Landroid/view/MotionEvent;)Z

    #@4d
    move-result v0

    #@4e
    if-eqz v0, :cond_8d

    #@50
    .line 354
    const/4 v0, 0x1

    #@51
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendClickEventsToWebKit:Z

    #@53
    .line 358
    :goto_53
    const/4 v0, 0x0

    #@54
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchMoveEventsToWebKit:Z

    #@56
    .line 360
    :cond_56
    const/4 v0, 0x2

    #@57
    if-ne v7, v0, :cond_64

    #@59
    .line 361
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher;->mUiCallbacks:Landroid/webkit/WebViewInputDispatcher$UiCallbacks;

    #@5b
    invoke-interface {v0, v1}, Landroid/webkit/WebViewInputDispatcher$UiCallbacks;->shouldIgnoreTouchMoveEvent(Landroid/view/MotionEvent;)Z

    #@5e
    move-result v0

    #@5f
    if-eqz v0, :cond_91

    #@61
    .line 362
    const/4 v0, 0x1

    #@62
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchMoveEventsToWebKit:Z

    #@64
    .line 367
    :cond_64
    :goto_64
    if-ne v7, v9, :cond_6d

    #@66
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mIsPreventDefault:Z

    #@68
    if-eqz v0, :cond_6d

    #@6a
    .line 368
    const/4 v0, 0x0

    #@6b
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchMoveEventsToWebKit:Z

    #@6d
    .line 374
    :cond_6d
    if-ne v1, p1, :cond_73

    #@6f
    .line 375
    invoke-virtual {p1}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;

    #@72
    move-result-object v1

    #@73
    .line 378
    :cond_73
    const/4 v3, 0x0

    #@74
    move-object v0, p0

    #@75
    move v4, p2

    #@76
    move v5, p3

    #@77
    move v6, p4

    #@78
    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebViewInputDispatcher;->obtainDispatchEventLocked(Landroid/view/MotionEvent;IIIIF)Landroid/webkit/WebViewInputDispatcher$DispatchEvent;

    #@7b
    move-result-object v8

    #@7c
    .line 380
    .local v8, d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    invoke-direct {p0, v8, p1}, Landroid/webkit/WebViewInputDispatcher;->updateStateTrackersLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;Landroid/view/MotionEvent;)V

    #@7f
    .line 381
    invoke-direct {p0, v8}, Landroid/webkit/WebViewInputDispatcher;->enqueueEventLocked(Landroid/webkit/WebViewInputDispatcher$DispatchEvent;)V

    #@82
    .line 382
    monitor-exit v10

    #@83
    move v0, v9

    #@84
    .line 383
    goto :goto_13

    #@85
    .line 346
    .end local v8           #d:Landroid/webkit/WebViewInputDispatcher$DispatchEvent;
    :cond_85
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchEventsToWebKitUntilNextGesture:Z

    #@87
    if-eqz v0, :cond_46

    #@89
    .line 348
    const/4 v0, 0x0

    #@8a
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchEventsToWebKitUntilNextGesture:Z

    #@8c
    goto :goto_46

    #@8d
    .line 356
    :cond_8d
    const/4 v0, 0x0

    #@8e
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendClickEventsToWebKit:Z

    #@90
    goto :goto_53

    #@91
    .line 364
    :cond_91
    const/4 v0, 0x0

    #@92
    iput-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostDoNotSendTouchMoveEventsToWebKit:Z
    :try_end_94
    .catchall {:try_start_37 .. :try_end_94} :catchall_2e

    #@94
    goto :goto_64

    #@95
    .line 307
    nop

    #@96
    :pswitch_data_96
    .packed-switch 0x0
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_33
        :pswitch_31
        :pswitch_33
        :pswitch_33
    .end packed-switch
.end method

.method public setTouchDragMode(Z)V
    .registers 2
    .parameter "isTouchDragMode"

    #@0
    .prologue
    .line 677
    iput-boolean p1, p0, Landroid/webkit/WebViewInputDispatcher;->mTouchDragMode:Z

    #@2
    .line 678
    return-void
.end method

.method public setWebKitWantsTouchEvents(Z)V
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 275
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 276
    :try_start_3
    iget-boolean v0, p0, Landroid/webkit/WebViewInputDispatcher;->mPostSendTouchEventsToWebKit:Z

    #@5
    if-eq v0, p1, :cond_e

    #@7
    .line 277
    if-nez p1, :cond_c

    #@9
    .line 278
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->enqueueWebKitCancelTouchEventIfNeededLocked()V

    #@c
    .line 280
    :cond_c
    iput-boolean p1, p0, Landroid/webkit/WebViewInputDispatcher;->mPostSendTouchEventsToWebKit:Z

    #@e
    .line 282
    :cond_e
    monitor-exit v1

    #@f
    .line 283
    return-void

    #@10
    .line 282
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public skipWebkitForRemainingTouchStream()V
    .registers 1

    #@0
    .prologue
    .line 839
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher;->handleWebKitTimeout()V

    #@3
    .line 840
    return-void
.end method
