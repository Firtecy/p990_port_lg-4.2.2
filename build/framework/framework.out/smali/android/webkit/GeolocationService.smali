.class final Landroid/webkit/GeolocationService;
.super Ljava/lang/Object;
.source "GeolocationService.java"

# interfaces
.implements Landroid/location/LocationListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "geolocationService"


# instance fields
.field private mIsGpsEnabled:Z

.field private mIsGpsProviderAvailable:Z

.field private mIsNetworkProviderAvailable:Z

.field private mIsRunning:Z

.field private mLocationManager:Landroid/location/LocationManager;

.field private mNativeObject:J


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .registers 6
    .parameter "context"
    .parameter "nativeObject"

    #@0
    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    iput-wide p2, p0, Landroid/webkit/GeolocationService;->mNativeObject:J

    #@5
    .line 54
    const-string/jumbo v0, "location"

    #@8
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/location/LocationManager;

    #@e
    iput-object v0, p0, Landroid/webkit/GeolocationService;->mLocationManager:Landroid/location/LocationManager;

    #@10
    .line 55
    iget-object v0, p0, Landroid/webkit/GeolocationService;->mLocationManager:Landroid/location/LocationManager;

    #@12
    if-nez v0, :cond_1b

    #@14
    .line 56
    const-string v0, "geolocationService"

    #@16
    const-string v1, "Could not get location manager."

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 58
    :cond_1b
    return-void
.end method

.method private maybeReportError(Ljava/lang/String;)V
    .registers 4
    .parameter "message"

    #@0
    .prologue
    .line 193
    iget-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsRunning:Z

    #@2
    if-eqz v0, :cond_11

    #@4
    iget-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsNetworkProviderAvailable:Z

    #@6
    if-nez v0, :cond_11

    #@8
    iget-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsGpsProviderAvailable:Z

    #@a
    if-nez v0, :cond_11

    #@c
    .line 194
    iget-wide v0, p0, Landroid/webkit/GeolocationService;->mNativeObject:J

    #@e
    invoke-static {v0, v1, p1}, Landroid/webkit/GeolocationService;->nativeNewErrorAvailable(JLjava/lang/String;)V

    #@11
    .line 196
    :cond_11
    return-void
.end method

.method private static native nativeNewErrorAvailable(JLjava/lang/String;)V
.end method

.method private static native nativeNewLocationAvailable(JLandroid/location/Location;)V
.end method

.method private registerForLocationUpdates()V
    .registers 8

    #@0
    .prologue
    .line 162
    :try_start_0
    iget-object v0, p0, Landroid/webkit/GeolocationService;->mLocationManager:Landroid/location/LocationManager;

    #@2
    const-string/jumbo v1, "network"

    #@5
    const-wide/16 v2, 0x0

    #@7
    const/4 v4, 0x0

    #@8
    move-object v5, p0

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    #@c
    .line 163
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsNetworkProviderAvailable:Z
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_f} :catch_2d
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_f} :catch_22

    #@f
    .line 165
    :goto_f
    :try_start_f
    iget-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsGpsEnabled:Z
    :try_end_11
    .catch Ljava/lang/SecurityException; {:try_start_f .. :try_end_11} :catch_22

    #@11
    if-eqz v0, :cond_21

    #@13
    .line 167
    :try_start_13
    iget-object v0, p0, Landroid/webkit/GeolocationService;->mLocationManager:Landroid/location/LocationManager;

    #@15
    const-string v1, "gps"

    #@17
    const-wide/16 v2, 0x0

    #@19
    const/4 v4, 0x0

    #@1a
    move-object v5, p0

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    #@1e
    .line 168
    const/4 v0, 0x1

    #@1f
    iput-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsGpsProviderAvailable:Z
    :try_end_21
    .catch Ljava/lang/IllegalArgumentException; {:try_start_13 .. :try_end_21} :catch_2b
    .catch Ljava/lang/SecurityException; {:try_start_13 .. :try_end_21} :catch_22

    #@21
    .line 175
    :cond_21
    :goto_21
    return-void

    #@22
    .line 171
    :catch_22
    move-exception v6

    #@23
    .line 172
    .local v6, e:Ljava/lang/SecurityException;
    const-string v0, "geolocationService"

    #@25
    const-string v1, "Caught security exception registering for location updates from system. This should only happen in DumpRenderTree."

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_21

    #@2b
    .line 169
    .end local v6           #e:Ljava/lang/SecurityException;
    :catch_2b
    move-exception v0

    #@2c
    goto :goto_21

    #@2d
    .line 164
    :catch_2d
    move-exception v0

    #@2e
    goto :goto_f
.end method

.method private unregisterFromLocationUpdates()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 181
    iget-object v0, p0, Landroid/webkit/GeolocationService;->mLocationManager:Landroid/location/LocationManager;

    #@3
    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    #@6
    .line 182
    iput-boolean v1, p0, Landroid/webkit/GeolocationService;->mIsNetworkProviderAvailable:Z

    #@8
    .line 183
    iput-boolean v1, p0, Landroid/webkit/GeolocationService;->mIsGpsProviderAvailable:Z

    #@a
    .line 184
    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .registers 4
    .parameter "location"

    #@0
    .prologue
    .line 104
    iget-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsRunning:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 105
    iget-wide v0, p0, Landroid/webkit/GeolocationService;->mNativeObject:J

    #@6
    invoke-static {v0, v1, p1}, Landroid/webkit/GeolocationService;->nativeNewLocationAvailable(JLandroid/location/Location;)V

    #@9
    .line 107
    :cond_9
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 4
    .parameter "providerName"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 147
    const-string/jumbo v0, "network"

    #@4
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_12

    #@a
    .line 148
    iput-boolean v1, p0, Landroid/webkit/GeolocationService;->mIsNetworkProviderAvailable:Z

    #@c
    .line 152
    :cond_c
    :goto_c
    const-string v0, "The last location provider was disabled"

    #@e
    invoke-direct {p0, v0}, Landroid/webkit/GeolocationService;->maybeReportError(Ljava/lang/String;)V

    #@11
    .line 153
    return-void

    #@12
    .line 149
    :cond_12
    const-string v0, "gps"

    #@14
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_c

    #@1a
    .line 150
    iput-boolean v1, p0, Landroid/webkit/GeolocationService;->mIsGpsProviderAvailable:Z

    #@1c
    goto :goto_c
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 4
    .parameter "providerName"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 134
    const-string/jumbo v0, "network"

    #@4
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_d

    #@a
    .line 135
    iput-boolean v1, p0, Landroid/webkit/GeolocationService;->mIsNetworkProviderAvailable:Z

    #@c
    .line 139
    :cond_c
    :goto_c
    return-void

    #@d
    .line 136
    :cond_d
    const-string v0, "gps"

    #@f
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_c

    #@15
    .line 137
    iput-boolean v1, p0, Landroid/webkit/GeolocationService;->mIsGpsProviderAvailable:Z

    #@17
    goto :goto_c
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 6
    .parameter "providerName"
    .parameter "status"
    .parameter "extras"

    #@0
    .prologue
    .line 117
    const/4 v1, 0x2

    #@1
    if-ne p2, v1, :cond_15

    #@3
    const/4 v0, 0x1

    #@4
    .line 118
    .local v0, isAvailable:Z
    :goto_4
    const-string/jumbo v1, "network"

    #@7
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_17

    #@d
    .line 119
    iput-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsNetworkProviderAvailable:Z

    #@f
    .line 123
    :cond_f
    :goto_f
    const-string v1, "The last location provider is no longer available"

    #@11
    invoke-direct {p0, v1}, Landroid/webkit/GeolocationService;->maybeReportError(Ljava/lang/String;)V

    #@14
    .line 124
    return-void

    #@15
    .line 117
    .end local v0           #isAvailable:Z
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_4

    #@17
    .line 120
    .restart local v0       #isAvailable:Z
    :cond_17
    const-string v1, "gps"

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_f

    #@1f
    .line 121
    iput-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsGpsProviderAvailable:Z

    #@21
    goto :goto_f
.end method

.method public setEnableGps(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 82
    iget-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsGpsEnabled:Z

    #@2
    if-eq v0, p1, :cond_15

    #@4
    .line 83
    iput-boolean p1, p0, Landroid/webkit/GeolocationService;->mIsGpsEnabled:Z

    #@6
    .line 84
    iget-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsRunning:Z

    #@8
    if-eqz v0, :cond_15

    #@a
    .line 87
    invoke-direct {p0}, Landroid/webkit/GeolocationService;->unregisterFromLocationUpdates()V

    #@d
    .line 88
    invoke-direct {p0}, Landroid/webkit/GeolocationService;->registerForLocationUpdates()V

    #@10
    .line 90
    const-string v0, "The last location provider is no longer available"

    #@12
    invoke-direct {p0, v0}, Landroid/webkit/GeolocationService;->maybeReportError(Ljava/lang/String;)V

    #@15
    .line 93
    :cond_15
    return-void
.end method

.method public start()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 64
    invoke-direct {p0}, Landroid/webkit/GeolocationService;->registerForLocationUpdates()V

    #@4
    .line 65
    iput-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsRunning:Z

    #@6
    .line 66
    iget-boolean v1, p0, Landroid/webkit/GeolocationService;->mIsNetworkProviderAvailable:Z

    #@8
    if-nez v1, :cond_e

    #@a
    iget-boolean v1, p0, Landroid/webkit/GeolocationService;->mIsGpsProviderAvailable:Z

    #@c
    if-eqz v1, :cond_f

    #@e
    :cond_e
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public stop()V
    .registers 2

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Landroid/webkit/GeolocationService;->unregisterFromLocationUpdates()V

    #@3
    .line 74
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/webkit/GeolocationService;->mIsRunning:Z

    #@6
    .line 75
    return-void
.end method
