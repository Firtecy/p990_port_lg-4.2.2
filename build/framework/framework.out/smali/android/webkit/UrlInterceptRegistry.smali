.class public final Landroid/webkit/UrlInterceptRegistry;
.super Ljava/lang/Object;
.source "UrlInterceptRegistry.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "intercept"

.field private static mDisabled:Z

.field private static mHandlerList:Ljava/util/LinkedList;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 37
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Landroid/webkit/UrlInterceptRegistry;->mDisabled:Z

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static declared-synchronized getHandlers()Ljava/util/LinkedList;
    .registers 2

    #@0
    .prologue
    .line 42
    const-class v1, Landroid/webkit/UrlInterceptRegistry;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/UrlInterceptRegistry;->mHandlerList:Ljava/util/LinkedList;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 43
    new-instance v0, Ljava/util/LinkedList;

    #@9
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@c
    sput-object v0, Landroid/webkit/UrlInterceptRegistry;->mHandlerList:Ljava/util/LinkedList;

    #@e
    .line 44
    :cond_e
    sget-object v0, Landroid/webkit/UrlInterceptRegistry;->mHandlerList:Ljava/util/LinkedList;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    monitor-exit v1

    #@11
    return-object v0

    #@12
    .line 42
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1

    #@14
    throw v0
.end method

.method public static declared-synchronized getPluginData(Ljava/lang/String;Ljava/util/Map;)Landroid/webkit/PluginData;
    .registers 8
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/webkit/PluginData;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .local p1, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@1
    .line 154
    const-class v4, Landroid/webkit/UrlInterceptRegistry;

    #@3
    monitor-enter v4

    #@4
    :try_start_4
    invoke-static {}, Landroid/webkit/UrlInterceptRegistry;->urlInterceptDisabled()Z
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_2a

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_d

    #@a
    move-object v0, v3

    #@b
    .line 165
    :goto_b
    monitor-exit v4

    #@c
    return-object v0

    #@d
    .line 157
    :cond_d
    :try_start_d
    invoke-static {}, Landroid/webkit/UrlInterceptRegistry;->getHandlers()Ljava/util/LinkedList;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    #@14
    move-result-object v2

    #@15
    .line 158
    .local v2, iter:Ljava/util/Iterator;
    :cond_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v5

    #@19
    if-eqz v5, :cond_28

    #@1b
    .line 159
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v1

    #@1f
    check-cast v1, Landroid/webkit/UrlInterceptHandler;

    #@21
    .line 160
    .local v1, handler:Landroid/webkit/UrlInterceptHandler;
    invoke-interface {v1, p0, p1}, Landroid/webkit/UrlInterceptHandler;->getPluginData(Ljava/lang/String;Ljava/util/Map;)Landroid/webkit/PluginData;
    :try_end_24
    .catchall {:try_start_d .. :try_end_24} :catchall_2a

    #@24
    move-result-object v0

    #@25
    .line 161
    .local v0, data:Landroid/webkit/PluginData;
    if-eqz v0, :cond_15

    #@27
    goto :goto_b

    #@28
    .end local v0           #data:Landroid/webkit/PluginData;
    .end local v1           #handler:Landroid/webkit/UrlInterceptHandler;
    :cond_28
    move-object v0, v3

    #@29
    .line 165
    goto :goto_b

    #@2a
    .line 154
    .end local v2           #iter:Ljava/util/Iterator;
    :catchall_2a
    move-exception v3

    #@2b
    monitor-exit v4

    #@2c
    throw v3
.end method

.method public static declared-synchronized getSurrogate(Ljava/lang/String;Ljava/util/Map;)Landroid/webkit/CacheManager$CacheResult;
    .registers 8
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/webkit/CacheManager$CacheResult;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .local p1, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@1
    .line 126
    const-class v4, Landroid/webkit/UrlInterceptRegistry;

    #@3
    monitor-enter v4

    #@4
    :try_start_4
    invoke-static {}, Landroid/webkit/UrlInterceptRegistry;->urlInterceptDisabled()Z
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_2a

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_d

    #@a
    move-object v2, v3

    #@b
    .line 137
    :goto_b
    monitor-exit v4

    #@c
    return-object v2

    #@d
    .line 129
    :cond_d
    :try_start_d
    invoke-static {}, Landroid/webkit/UrlInterceptRegistry;->getHandlers()Ljava/util/LinkedList;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    #@14
    move-result-object v1

    #@15
    .line 130
    .local v1, iter:Ljava/util/Iterator;
    :cond_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v5

    #@19
    if-eqz v5, :cond_28

    #@1b
    .line 131
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/webkit/UrlInterceptHandler;

    #@21
    .line 132
    .local v0, handler:Landroid/webkit/UrlInterceptHandler;
    invoke-interface {v0, p0, p1}, Landroid/webkit/UrlInterceptHandler;->service(Ljava/lang/String;Ljava/util/Map;)Landroid/webkit/CacheManager$CacheResult;
    :try_end_24
    .catchall {:try_start_d .. :try_end_24} :catchall_2a

    #@24
    move-result-object v2

    #@25
    .line 133
    .local v2, result:Landroid/webkit/CacheManager$CacheResult;
    if-eqz v2, :cond_15

    #@27
    goto :goto_b

    #@28
    .end local v0           #handler:Landroid/webkit/UrlInterceptHandler;
    .end local v2           #result:Landroid/webkit/CacheManager$CacheResult;
    :cond_28
    move-object v2, v3

    #@29
    .line 137
    goto :goto_b

    #@2a
    .line 126
    .end local v1           #iter:Ljava/util/Iterator;
    :catchall_2a
    move-exception v3

    #@2b
    monitor-exit v4

    #@2c
    throw v3
.end method

.method public static declared-synchronized registerHandler(Landroid/webkit/UrlInterceptHandler;)Z
    .registers 3
    .parameter "handler"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 89
    const-class v1, Landroid/webkit/UrlInterceptRegistry;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    invoke-static {}, Landroid/webkit/UrlInterceptRegistry;->getHandlers()Ljava/util/LinkedList;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_17

    #@d
    .line 90
    invoke-static {}, Landroid/webkit/UrlInterceptRegistry;->getHandlers()Ljava/util/LinkedList;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_19

    #@14
    .line 91
    const/4 v0, 0x1

    #@15
    .line 93
    :goto_15
    monitor-exit v1

    #@16
    return v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_15

    #@19
    .line 89
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1

    #@1b
    throw v0
.end method

.method public static declared-synchronized setUrlInterceptDisabled(Z)V
    .registers 3
    .parameter "disabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 58
    const-class v0, Landroid/webkit/UrlInterceptRegistry;

    #@2
    monitor-enter v0

    #@3
    :try_start_3
    sput-boolean p0, Landroid/webkit/UrlInterceptRegistry;->mDisabled:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    #@5
    .line 59
    monitor-exit v0

    #@6
    return-void

    #@7
    .line 58
    :catchall_7
    move-exception v1

    #@8
    monitor-exit v0

    #@9
    throw v1
.end method

.method public static declared-synchronized unregisterHandler(Landroid/webkit/UrlInterceptHandler;)Z
    .registers 3
    .parameter "handler"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 110
    const-class v1, Landroid/webkit/UrlInterceptRegistry;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    invoke-static {}, Landroid/webkit/UrlInterceptRegistry;->getHandlers()Ljava/util/LinkedList;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_d

    #@a
    move-result v0

    #@b
    monitor-exit v1

    #@c
    return v0

    #@d
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1

    #@f
    throw v0
.end method

.method public static declared-synchronized urlInterceptDisabled()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 72
    const-class v0, Landroid/webkit/UrlInterceptRegistry;

    #@2
    monitor-enter v0

    #@3
    :try_start_3
    sget-boolean v1, Landroid/webkit/UrlInterceptRegistry;->mDisabled:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    #@5
    monitor-exit v0

    #@6
    return v1

    #@7
    :catchall_7
    move-exception v1

    #@8
    monitor-exit v0

    #@9
    throw v1
.end method
