.class public final Landroid/webkit/WebViewCore;
.super Ljava/lang/Object;
.source "WebViewCore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebViewCore$ShowRectData;,
        Landroid/webkit/WebViewCore$DrawData;,
        Landroid/webkit/WebViewCore$ViewState;,
        Landroid/webkit/WebViewCore$EventHub;,
        Landroid/webkit/WebViewCore$SaveViewStateRequest;,
        Landroid/webkit/WebViewCore$FindAllRequest;,
        Landroid/webkit/WebViewCore$GeolocationPermissionsData;,
        Landroid/webkit/WebViewCore$TouchEventData;,
        Landroid/webkit/WebViewCore$TextFieldInitData;,
        Landroid/webkit/WebViewCore$AutoFillData;,
        Landroid/webkit/WebViewCore$WebKitHitTest;,
        Landroid/webkit/WebViewCore$TouchHighlightData;,
        Landroid/webkit/WebViewCore$TouchUpData;,
        Landroid/webkit/WebViewCore$TextSelectionData;,
        Landroid/webkit/WebViewCore$ReplaceTextData;,
        Landroid/webkit/WebViewCore$PostUrlData;,
        Landroid/webkit/WebViewCore$GetUrlData;,
        Landroid/webkit/WebViewCore$MotionUpData;,
        Landroid/webkit/WebViewCore$JSKeyData;,
        Landroid/webkit/WebViewCore$JSInterfaceData;,
        Landroid/webkit/WebViewCore$BaseUrlData;,
        Landroid/webkit/WebViewCore$WebCoreThread;
    }
.end annotation


# static fields
.field static final ACTION_DOUBLETAP:I = 0x200

.field static final ACTION_LONGPRESS:I = 0x100

.field static final HandlerDebugString:[Ljava/lang/String; = null

.field private static final LOGTAG:Ljava/lang/String; = "webcore"

.field static final THREAD_NAME:Ljava/lang/String; = "WebViewCoreThread"

.field private static final TOUCH_FLAG_HIT_HANDLER:I = 0x1

.field private static final TOUCH_FLAG_PREVENT_DEFAULT:I = 0x2

.field private static mRepaintScheduled:Z

.field private static sShouldMonitorWebCoreThread:Z

.field private static sWebCoreHandler:Landroid/os/Handler;


# instance fields
.field private mBrowserFrame:Landroid/webkit/BrowserFrame;

.field private final mCallbackProxy:Landroid/webkit/CallbackProxy;

.field private mChromeCanFocusDirection:I

.field private final mContext:Landroid/content/Context;

.field private mCurrentViewHeight:I

.field private mCurrentViewScale:F

.field private mCurrentViewWidth:I

.field private mDeviceMotionAndOrientationManager:Landroid/webkit/DeviceMotionAndOrientationManager;

.field private mDeviceMotionService:Landroid/webkit/DeviceMotionService;

.field private mDeviceOrientationService:Landroid/webkit/DeviceOrientationService;

.field private mDrawIsPaused:Z

.field private mDrawIsScheduled:Z

.field private final mEventHub:Landroid/webkit/WebViewCore$EventHub;

.field private mFirstLayoutForNonStandardLoad:Z

.field private mHighMemoryUsageThresholdMb:I

.field private mHighUsageDeltaMb:I

.field private mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

.field private mIsRestored:Z

.field private mJavascriptInterfaces:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mLastDrawData:Landroid/webkit/WebViewCore$DrawData;

.field private mLowMemoryUsageThresholdMb:I

.field private mMockGeolocation:Landroid/webkit/MockGeolocation;

.field private mNativeClass:I

.field private mNotifications:Landroid/webkit/LGWebNotifications;

.field private mRestoredScale:F

.field private mRestoredTextWrapScale:F

.field private mRestoredX:I

.field private mRestoredY:I

.field private final mSettings:Landroid/webkit/WebSettingsClassic;

.field private mTextSelectionChangeReason:I

.field private mUpdateTextSelectionNeeded:Z

.field private mViewportDensityDpi:I

.field private mViewportHeight:I

.field private mViewportInitialScale:I

.field private mViewportMaximumScale:I

.field private mViewportMinimumScale:I

.field private mViewportUserScalable:Z

.field private mViewportWidth:I

.field private mWebViewClassic:Landroid/webkit/WebViewClassic;

.field private m_drawWasSkipped:Z

.field private m_skipDrawFlag:Z

.field private m_skipDrawFlagLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 72
    :try_start_1
    const-string/jumbo v1, "webcore"

    #@4
    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@7
    .line 73
    const-string v1, "chromium_net"

    #@9
    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_c} :catch_156

    #@c
    .line 1037
    .local v0, e:Ljava/lang/UnsatisfiedLinkError;
    :goto_c
    const/16 v1, 0x37

    #@e
    new-array v1, v1, [Ljava/lang/String;

    #@10
    const-string v2, "SEND_PICKER_VALUE"

    #@12
    aput-object v2, v1, v4

    #@14
    const/4 v2, 0x1

    #@15
    const-string v3, "REVEAL_SELECTION"

    #@17
    aput-object v3, v1, v2

    #@19
    const/4 v2, 0x2

    #@1a
    const-string v3, ""

    #@1c
    aput-object v3, v1, v2

    #@1e
    const/4 v2, 0x3

    #@1f
    const-string v3, ""

    #@21
    aput-object v3, v1, v2

    #@23
    const/4 v2, 0x4

    #@24
    const-string v3, "SCROLL_TEXT_INPUT"

    #@26
    aput-object v3, v1, v2

    #@28
    const/4 v2, 0x5

    #@29
    const-string v3, "LOAD_URL"

    #@2b
    aput-object v3, v1, v2

    #@2d
    const/4 v2, 0x6

    #@2e
    const-string v3, "STOP_LOADING"

    #@30
    aput-object v3, v1, v2

    #@32
    const/4 v2, 0x7

    #@33
    const-string v3, "RELOAD"

    #@35
    aput-object v3, v1, v2

    #@37
    const/16 v2, 0x8

    #@39
    const-string v3, "KEY_DOWN"

    #@3b
    aput-object v3, v1, v2

    #@3d
    const/16 v2, 0x9

    #@3f
    const-string v3, "KEY_UP"

    #@41
    aput-object v3, v1, v2

    #@43
    const/16 v2, 0xa

    #@45
    const-string v3, "VIEW_SIZE_CHANGED"

    #@47
    aput-object v3, v1, v2

    #@49
    const/16 v2, 0xb

    #@4b
    const-string v3, "GO_BACK_FORWARD"

    #@4d
    aput-object v3, v1, v2

    #@4f
    const/16 v2, 0xc

    #@51
    const-string v3, "SET_SCROLL_OFFSET"

    #@53
    aput-object v3, v1, v2

    #@55
    const/16 v2, 0xd

    #@57
    const-string v3, "RESTORE_STATE"

    #@59
    aput-object v3, v1, v2

    #@5b
    const/16 v2, 0xe

    #@5d
    const-string v3, "PAUSE_TIMERS"

    #@5f
    aput-object v3, v1, v2

    #@61
    const/16 v2, 0xf

    #@63
    const-string v3, "RESUME_TIMERS"

    #@65
    aput-object v3, v1, v2

    #@67
    const/16 v2, 0x10

    #@69
    const-string v3, "CLEAR_CACHE"

    #@6b
    aput-object v3, v1, v2

    #@6d
    const/16 v2, 0x11

    #@6f
    const-string v3, "CLEAR_HISTORY"

    #@71
    aput-object v3, v1, v2

    #@73
    const/16 v2, 0x12

    #@75
    const-string v3, "SET_SELECTION"

    #@77
    aput-object v3, v1, v2

    #@79
    const/16 v2, 0x13

    #@7b
    const-string v3, "REPLACE_TEXT"

    #@7d
    aput-object v3, v1, v2

    #@7f
    const/16 v2, 0x14

    #@81
    const-string v3, "PASS_TO_JS"

    #@83
    aput-object v3, v1, v2

    #@85
    const/16 v2, 0x15

    #@87
    const-string v3, "SET_GLOBAL_BOUNDS"

    #@89
    aput-object v3, v1, v2

    #@8b
    const/16 v2, 0x16

    #@8d
    const-string v3, ""

    #@8f
    aput-object v3, v1, v2

    #@91
    const/16 v2, 0x17

    #@93
    const-string v3, "CLICK"

    #@95
    aput-object v3, v1, v2

    #@97
    const/16 v2, 0x18

    #@99
    const-string v3, "SET_NETWORK_STATE"

    #@9b
    aput-object v3, v1, v2

    #@9d
    const/16 v2, 0x19

    #@9f
    const-string v3, "DOC_HAS_IMAGES"

    #@a1
    aput-object v3, v1, v2

    #@a3
    const/16 v2, 0x1a

    #@a5
    const-string v3, "FAKE_CLICK"

    #@a7
    aput-object v3, v1, v2

    #@a9
    const/16 v2, 0x1b

    #@ab
    const-string v3, "DELETE_SELECTION"

    #@ad
    aput-object v3, v1, v2

    #@af
    const/16 v2, 0x1c

    #@b1
    const-string v3, "LISTBOX_CHOICES"

    #@b3
    aput-object v3, v1, v2

    #@b5
    const/16 v2, 0x1d

    #@b7
    const-string v3, "SINGLE_LISTBOX_CHOICE"

    #@b9
    aput-object v3, v1, v2

    #@bb
    const/16 v2, 0x1e

    #@bd
    const-string v3, "MESSAGE_RELAY"

    #@bf
    aput-object v3, v1, v2

    #@c1
    const/16 v2, 0x1f

    #@c3
    const-string v3, "SET_BACKGROUND_COLOR"

    #@c5
    aput-object v3, v1, v2

    #@c7
    const/16 v2, 0x20

    #@c9
    const-string v3, "SET_MOVE_FOCUS"

    #@cb
    aput-object v3, v1, v2

    #@cd
    const/16 v2, 0x21

    #@cf
    const-string v3, "SAVE_DOCUMENT_STATE"

    #@d1
    aput-object v3, v1, v2

    #@d3
    const/16 v2, 0x22

    #@d5
    const-string v3, "129"

    #@d7
    aput-object v3, v1, v2

    #@d9
    const/16 v2, 0x23

    #@db
    const-string v3, "WEBKIT_DRAW"

    #@dd
    aput-object v3, v1, v2

    #@df
    const/16 v2, 0x24

    #@e1
    const-string v3, "131"

    #@e3
    aput-object v3, v1, v2

    #@e5
    const/16 v2, 0x25

    #@e7
    const-string v3, "POST_URL"

    #@e9
    aput-object v3, v1, v2

    #@eb
    const/16 v2, 0x26

    #@ed
    const-string v3, ""

    #@ef
    aput-object v3, v1, v2

    #@f1
    const/16 v2, 0x27

    #@f3
    const-string v3, "CLEAR_CONTENT"

    #@f5
    aput-object v3, v1, v2

    #@f7
    const/16 v2, 0x28

    #@f9
    const-string v3, ""

    #@fb
    aput-object v3, v1, v2

    #@fd
    const/16 v2, 0x29

    #@ff
    const-string v3, ""

    #@101
    aput-object v3, v1, v2

    #@103
    const/16 v2, 0x2a

    #@105
    const-string v3, "REQUEST_CURSOR_HREF"

    #@107
    aput-object v3, v1, v2

    #@109
    const/16 v2, 0x2b

    #@10b
    const-string v3, "ADD_JS_INTERFACE"

    #@10d
    aput-object v3, v1, v2

    #@10f
    const/16 v2, 0x2c

    #@111
    const-string v3, "LOAD_DATA"

    #@113
    aput-object v3, v1, v2

    #@115
    const/16 v2, 0x2d

    #@117
    const-string v3, ""

    #@119
    aput-object v3, v1, v2

    #@11b
    const/16 v2, 0x2e

    #@11d
    const-string v3, ""

    #@11f
    aput-object v3, v1, v2

    #@121
    const/16 v2, 0x2f

    #@123
    const-string v3, "SET_ACTIVE"

    #@125
    aput-object v3, v1, v2

    #@127
    const/16 v2, 0x30

    #@129
    const-string v3, "ON_PAUSE"

    #@12b
    aput-object v3, v1, v2

    #@12d
    const/16 v2, 0x31

    #@12f
    const-string v3, "ON_RESUME"

    #@131
    aput-object v3, v1, v2

    #@133
    const/16 v2, 0x32

    #@135
    const-string v3, "FREE_MEMORY"

    #@137
    aput-object v3, v1, v2

    #@139
    const/16 v2, 0x33

    #@13b
    const-string v3, "VALID_NODE_BOUNDS"

    #@13d
    aput-object v3, v1, v2

    #@13f
    const/16 v2, 0x34

    #@141
    const-string v3, "SAVE_WEBARCHIVE"

    #@143
    aput-object v3, v1, v2

    #@145
    const/16 v2, 0x35

    #@147
    const-string v3, "WEBKIT_DRAW_LAYERS"

    #@149
    aput-object v3, v1, v2

    #@14b
    const/16 v2, 0x36

    #@14d
    const-string v3, "REMOVE_JS_INTERFACE"

    #@14f
    aput-object v3, v1, v2

    #@151
    sput-object v1, Landroid/webkit/WebViewCore;->HandlerDebugString:[Ljava/lang/String;

    #@153
    .line 2552
    sput-boolean v4, Landroid/webkit/WebViewCore;->mRepaintScheduled:Z

    #@155
    return-void

    #@156
    .line 74
    .end local v0           #e:Ljava/lang/UnsatisfiedLinkError;
    :catch_156
    move-exception v0

    #@157
    .line 75
    .restart local v0       #e:Ljava/lang/UnsatisfiedLinkError;
    const-string/jumbo v1, "webcore"

    #@15a
    const-string v2, "Unable to load native support libraries."

    #@15c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15f
    goto/16 :goto_c
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;Landroid/webkit/CallbackProxy;Ljava/util/Map;)V
    .registers 15
    .parameter "context"
    .parameter "w"
    .parameter "proxy"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/webkit/WebViewClassic;",
            "Landroid/webkit/CallbackProxy;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p4, javascriptInterfaces:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v7, 0x0

    #@1
    const/4 v6, -0x1

    #@2
    const/4 v8, 0x0

    #@3
    const/4 v9, 0x0

    #@4
    .line 165
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 101
    iput v6, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@9
    .line 107
    iput v6, p0, Landroid/webkit/WebViewCore;->mViewportHeight:I

    #@b
    .line 112
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@d
    .line 117
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@f
    .line 122
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@11
    .line 124
    const/4 v5, 0x1

    #@12
    iput-boolean v5, p0, Landroid/webkit/WebViewCore;->mViewportUserScalable:Z

    #@14
    .line 133
    iput v6, p0, Landroid/webkit/WebViewCore;->mViewportDensityDpi:I

    #@16
    .line 135
    iput-boolean v9, p0, Landroid/webkit/WebViewCore;->mIsRestored:Z

    #@18
    .line 136
    iput v7, p0, Landroid/webkit/WebViewCore;->mRestoredScale:F

    #@1a
    .line 137
    iput v7, p0, Landroid/webkit/WebViewCore;->mRestoredTextWrapScale:F

    #@1c
    .line 138
    iput v9, p0, Landroid/webkit/WebViewCore;->mRestoredX:I

    #@1e
    .line 139
    iput v9, p0, Landroid/webkit/WebViewCore;->mRestoredY:I

    #@20
    .line 141
    new-instance v5, Landroid/webkit/MockGeolocation;

    #@22
    invoke-direct {v5, p0}, Landroid/webkit/MockGeolocation;-><init>(Landroid/webkit/WebViewCore;)V

    #@25
    iput-object v5, p0, Landroid/webkit/WebViewCore;->mMockGeolocation:Landroid/webkit/MockGeolocation;

    #@27
    .line 143
    new-instance v5, Landroid/webkit/DeviceMotionAndOrientationManager;

    #@29
    invoke-direct {v5, p0}, Landroid/webkit/DeviceMotionAndOrientationManager;-><init>(Landroid/webkit/WebViewCore;)V

    #@2c
    iput-object v5, p0, Landroid/webkit/WebViewCore;->mDeviceMotionAndOrientationManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@2e
    .line 153
    iput v9, p0, Landroid/webkit/WebViewCore;->mTextSelectionChangeReason:I

    #@30
    .line 2183
    iput v9, p0, Landroid/webkit/WebViewCore;->mCurrentViewWidth:I

    #@32
    .line 2184
    iput v9, p0, Landroid/webkit/WebViewCore;->mCurrentViewHeight:I

    #@34
    .line 2185
    const/high16 v5, 0x3f80

    #@36
    iput v5, p0, Landroid/webkit/WebViewCore;->mCurrentViewScale:F

    #@38
    .line 2187
    iput-boolean v9, p0, Landroid/webkit/WebViewCore;->mUpdateTextSelectionNeeded:Z

    #@3a
    .line 2274
    iput-object v8, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@3c
    .line 2305
    iput-object v8, p0, Landroid/webkit/WebViewCore;->mLastDrawData:Landroid/webkit/WebViewCore$DrawData;

    #@3e
    .line 2307
    new-instance v5, Ljava/lang/Object;

    #@40
    invoke-direct/range {v5 .. v5}, Ljava/lang/Object;-><init>()V

    #@43
    iput-object v5, p0, Landroid/webkit/WebViewCore;->m_skipDrawFlagLock:Ljava/lang/Object;

    #@45
    .line 2308
    iput-boolean v9, p0, Landroid/webkit/WebViewCore;->m_skipDrawFlag:Z

    #@47
    .line 2309
    iput-boolean v9, p0, Landroid/webkit/WebViewCore;->m_drawWasSkipped:Z

    #@49
    .line 3353
    iput-object v8, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@4b
    .line 167
    iput-object p3, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@4d
    .line 168
    iput-object p2, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@4f
    .line 169
    iput-object p4, p0, Landroid/webkit/WebViewCore;->mJavascriptInterfaces:Ljava/util/Map;

    #@51
    .line 172
    iput-object p1, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@53
    .line 178
    const-class v6, Landroid/webkit/WebViewCore;

    #@55
    monitor-enter v6

    #@56
    .line 179
    :try_start_56
    sget-object v5, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@58
    if-nez v5, :cond_7b

    #@5a
    .line 181
    new-instance v4, Ljava/lang/Thread;

    #@5c
    new-instance v5, Landroid/webkit/WebViewCore$WebCoreThread;

    #@5e
    const/4 v7, 0x0

    #@5f
    invoke-direct {v5, v7}, Landroid/webkit/WebViewCore$WebCoreThread;-><init>(Landroid/webkit/WebViewCore$1;)V

    #@62
    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@65
    .line 182
    .local v4, t:Ljava/lang/Thread;
    const-string v5, "WebViewCoreThread"

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    #@6a
    .line 183
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V
    :try_end_6d
    .catchall {:try_start_56 .. :try_end_6d} :catchall_e9

    #@6d
    .line 185
    :try_start_6d
    const-class v5, Landroid/webkit/WebViewCore;

    #@6f
    invoke-virtual {v5}, Ljava/lang/Object;->wait()V
    :try_end_72
    .catchall {:try_start_6d .. :try_end_72} :catchall_e9
    .catch Ljava/lang/InterruptedException; {:try_start_6d .. :try_end_72} :catch_d5

    #@72
    .line 192
    :goto_72
    :try_start_72
    sget-boolean v5, Landroid/webkit/WebViewCore;->sShouldMonitorWebCoreThread:Z

    #@74
    if-eqz v5, :cond_7b

    #@76
    .line 197
    sget-object v5, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@78
    invoke-static {v5}, Landroid/webkit/WebCoreThreadWatchdog;->start(Landroid/os/Handler;)Landroid/webkit/WebCoreThreadWatchdog;

    #@7b
    .line 201
    .end local v4           #t:Ljava/lang/Thread;
    :cond_7b
    invoke-static {p2}, Landroid/webkit/WebCoreThreadWatchdog;->registerWebView(Landroid/webkit/WebViewClassic;)V

    #@7e
    .line 202
    monitor-exit v6
    :try_end_7f
    .catchall {:try_start_72 .. :try_end_7f} :catchall_e9

    #@7f
    .line 205
    new-instance v5, Landroid/webkit/WebViewCore$EventHub;

    #@81
    invoke-direct {v5, p0, v8}, Landroid/webkit/WebViewCore$EventHub;-><init>(Landroid/webkit/WebViewCore;Landroid/webkit/WebViewCore$1;)V

    #@84
    iput-object v5, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@86
    .line 207
    new-instance v5, Landroid/webkit/WebSettingsClassic;

    #@88
    iget-object v6, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@8a
    iget-object v7, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@8c
    invoke-direct {v5, v6, v7}, Landroid/webkit/WebSettingsClassic;-><init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;)V

    #@8f
    iput-object v5, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@91
    .line 210
    invoke-static {}, Landroid/webkit/WebIconDatabase;->getInstance()Landroid/webkit/WebIconDatabase;

    #@94
    .line 212
    invoke-static {}, Landroid/webkit/WebStorageClassic;->getInstance()Landroid/webkit/WebStorageClassic;

    #@97
    move-result-object v5

    #@98
    invoke-virtual {v5}, Landroid/webkit/WebStorageClassic;->createUIHandler()V

    #@9b
    .line 214
    invoke-static {}, Landroid/webkit/GeolocationPermissionsClassic;->getInstance()Landroid/webkit/GeolocationPermissionsClassic;

    #@9e
    move-result-object v5

    #@9f
    invoke-virtual {v5}, Landroid/webkit/GeolocationPermissionsClassic;->createUIHandler()V

    #@a2
    .line 218
    iget-object v5, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@a4
    const-string v6, "activity"

    #@a6
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@a9
    move-result-object v2

    #@aa
    check-cast v2, Landroid/app/ActivityManager;

    #@ac
    .line 220
    .local v2, manager:Landroid/app/ActivityManager;
    new-instance v3, Landroid/app/ActivityManager$MemoryInfo;

    #@ae
    invoke-direct {v3}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    #@b1
    .line 221
    .local v3, memInfo:Landroid/app/ActivityManager$MemoryInfo;
    invoke-virtual {v2, v3}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    #@b4
    .line 225
    invoke-virtual {v2}, Landroid/app/ActivityManager;->getLargeMemoryClass()I

    #@b7
    move-result v5

    #@b8
    iput v5, p0, Landroid/webkit/WebViewCore;->mLowMemoryUsageThresholdMb:I

    #@ba
    .line 226
    iget v5, p0, Landroid/webkit/WebViewCore;->mLowMemoryUsageThresholdMb:I

    #@bc
    int-to-double v5, v5

    #@bd
    const-wide/high16 v7, 0x3ff8

    #@bf
    mul-double/2addr v5, v7

    #@c0
    double-to-int v5, v5

    #@c1
    iput v5, p0, Landroid/webkit/WebViewCore;->mHighMemoryUsageThresholdMb:I

    #@c3
    .line 228
    iget v5, p0, Landroid/webkit/WebViewCore;->mLowMemoryUsageThresholdMb:I

    #@c5
    div-int/lit8 v5, v5, 0x20

    #@c7
    iput v5, p0, Landroid/webkit/WebViewCore;->mHighUsageDeltaMb:I

    #@c9
    .line 231
    sget-object v5, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@cb
    invoke-virtual {v5, v9, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@ce
    move-result-object v1

    #@cf
    .line 233
    .local v1, init:Landroid/os/Message;
    sget-object v5, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@d1
    invoke-virtual {v5, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d4
    .line 234
    return-void

    #@d5
    .line 186
    .end local v1           #init:Landroid/os/Message;
    .end local v2           #manager:Landroid/app/ActivityManager;
    .end local v3           #memInfo:Landroid/app/ActivityManager$MemoryInfo;
    .restart local v4       #t:Ljava/lang/Thread;
    :catch_d5
    move-exception v0

    #@d6
    .line 187
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_d6
    const-string/jumbo v5, "webcore"

    #@d9
    const-string v7, "Caught exception while waiting for thread creation."

    #@db
    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@de
    .line 189
    const-string/jumbo v5, "webcore"

    #@e1
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@e4
    move-result-object v7

    #@e5
    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    goto :goto_72

    #@e9
    .line 202
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v4           #t:Ljava/lang/Thread;
    :catchall_e9
    move-exception v5

    #@ea
    monitor-exit v6
    :try_end_eb
    .catchall {:try_start_d6 .. :try_end_eb} :catchall_e9

    #@eb
    throw v5
.end method

.method static synthetic access$1000(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativeCloseIdleConnections(I)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Landroid/webkit/WebViewCore;)Landroid/webkit/WebViewClassic;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    return-object v0
.end method

.method static synthetic access$1102(Landroid/webkit/WebViewCore;Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewClassic;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput-object p1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    return-object p1
.end method

.method static synthetic access$1300(Landroid/webkit/WebViewCore;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->webkitDraw()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Landroid/webkit/WebViewCore;)Landroid/webkit/CallbackProxy;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Landroid/webkit/WebViewCore;)Landroid/webkit/BrowserFrame;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@2
    return-object v0
.end method

.method static synthetic access$1502(Landroid/webkit/WebViewCore;Landroid/webkit/BrowserFrame;)Landroid/webkit/BrowserFrame;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput-object p1, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@2
    return-object p1
.end method

.method static synthetic access$1600(Landroid/webkit/WebViewCore;)Landroid/webkit/WebSettingsClassic;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativeRevealSelection(I)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Landroid/webkit/WebViewCore;IFI)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->nativeScrollFocusedTextInput(IFI)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Landroid/webkit/WebViewCore;Ljava/lang/String;Ljava/util/Map;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    #@3
    return-void
.end method

.method static synthetic access$2000(Landroid/webkit/WebViewCore;ILjava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeRegisterURLSchemeAsLocal(ILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativeContentInvalidateAll(I)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Landroid/webkit/WebViewCore;Landroid/view/KeyEvent;IZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->key(Landroid/view/KeyEvent;IZ)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->keyPress(I)V

    #@3
    return-void
.end method

.method static synthetic access$2400(Landroid/webkit/WebViewCore;Landroid/webkit/WebViewClassic$ViewSizeData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->viewSizeChanged(Landroid/webkit/WebViewClassic$ViewSizeData;)V

    #@3
    return-void
.end method

.method static synthetic access$2500(Landroid/webkit/WebViewCore;IZII)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewCore;->nativeSetScrollOffset(IZII)V

    #@3
    return-void
.end method

.method static synthetic access$2600(Landroid/webkit/WebViewCore;IIIII)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p5}, Landroid/webkit/WebViewCore;->nativeSetGlobalBounds(IIIII)V

    #@3
    return-void
.end method

.method static synthetic access$2700(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->restoreState(I)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativePause(I)V

    #@3
    return-void
.end method

.method static synthetic access$2900(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativeResume(I)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/webkit/WebViewCore;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@2
    return v0
.end method

.method static synthetic access$3000(Landroid/webkit/WebViewCore;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->clearCache(Z)V

    #@3
    return-void
.end method

.method static synthetic access$302(Landroid/webkit/WebViewCore;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput p1, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@2
    return p1
.end method

.method static synthetic access$3100(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativeFreeMemory(I)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Landroid/webkit/WebViewCore;IIILjava/lang/String;III)V
    .registers 8
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p7}, Landroid/webkit/WebViewCore;->nativeReplaceTextfieldText(IIILjava/lang/String;III)V

    #@3
    return-void
.end method

.method static synthetic access$3300(Landroid/webkit/WebViewCore;IILjava/lang/String;IIZZZZ)V
    .registers 10
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"
    .parameter "x8"
    .parameter "x9"

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p9}, Landroid/webkit/WebViewCore;->passToJs(IILjava/lang/String;IIZZZZ)V

    #@3
    return-void
.end method

.method static synthetic access$3400(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativeSaveDocumentState(I)V

    #@3
    return-void
.end method

.method static synthetic access$3500(Landroid/webkit/WebViewCore;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeSetFocusControllerActive(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$3600(Landroid/webkit/WebViewCore;III)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->nativeMoveMouse(III)V

    #@3
    return-void
.end method

.method static synthetic access$3700(Landroid/webkit/WebViewCore;IIIZ)Landroid/webkit/WebViewCore$WebKitHitTest;
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewCore;->performHitTest(IIIZ)Landroid/webkit/WebViewCore$WebKitHitTest;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$3800(Landroid/webkit/WebViewCore;IIII)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewCore;->nativeDeleteSelection(IIII)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Landroid/webkit/WebViewCore;III)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->nativeSetSelection(III)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/webkit/WebViewCore;IJ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->nativeSetNewStorageLimit(IJ)V

    #@3
    return-void
.end method

.method static synthetic access$4002(Landroid/webkit/WebViewCore;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput p1, p0, Landroid/webkit/WebViewCore;->mTextSelectionChangeReason:I

    #@2
    return p1
.end method

.method static synthetic access$4100(Landroid/webkit/WebViewCore;III)Ljava/lang/String;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->nativeModifySelection(III)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$4200(Landroid/webkit/WebViewCore;I[ZI)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->nativeSendListBoxChoices(I[ZI)V

    #@3
    return-void
.end method

.method static synthetic access$4300(Landroid/webkit/WebViewCore;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeSendListBoxChoice(II)V

    #@3
    return-void
.end method

.method static synthetic access$4400(Landroid/webkit/WebViewCore;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeSetBackgroundColor(II)V

    #@3
    return-void
.end method

.method static synthetic access$4500(Landroid/webkit/WebViewCore;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeDumpDomTree(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$4600(Landroid/webkit/WebViewCore;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeDumpRenderTree(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$4700(Landroid/webkit/WebViewCore;ILjava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeSetJsFlags(ILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$4800(Landroid/webkit/WebViewCore;Ljava/lang/String;Z)Ljava/lang/String;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->saveWebArchive(Ljava/lang/String;Z)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$4900(Landroid/webkit/WebViewCore;ILjava/lang/String;ZZ)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewCore;->nativeGeolocationPermissionsProvide(ILjava/lang/String;ZZ)V

    #@3
    return-void
.end method

.method static synthetic access$500()Landroid/os/Handler;
    .registers 1

    #@0
    .prologue
    .line 63
    sget-object v0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$5000(Landroid/webkit/WebViewCore;I[Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeProvideVisitedHistory(I[Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$502(Landroid/os/Handler;)Landroid/os/Handler;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    sput-object p0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@2
    return-object p0
.end method

.method static synthetic access$5100(Landroid/webkit/WebViewCore;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeFullScreenPluginHidden(II)V

    #@3
    return-void
.end method

.method static synthetic access$5200(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativePluginSurfaceReady(I)V

    #@3
    return-void
.end method

.method static synthetic access$5300(Landroid/webkit/WebViewCore;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativeNotifyAnimationStarted(I)V

    #@3
    return-void
.end method

.method static synthetic access$5400(Landroid/webkit/WebViewCore;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->setUseMockGeolocation()V

    #@3
    return-void
.end method

.method static synthetic access$5500(Landroid/webkit/WebViewCore;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->setUseMockDeviceOrientation()V

    #@3
    return-void
.end method

.method static synthetic access$5600(Landroid/webkit/WebViewCore;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeAutoFillForm(II)V

    #@3
    return-void
.end method

.method static synthetic access$5700(Landroid/webkit/WebViewCore;IILandroid/graphics/Rect;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->nativeScrollLayer(IILandroid/graphics/Rect;)V

    #@3
    return-void
.end method

.method static synthetic access$5800(Landroid/webkit/WebViewCore;IIIII)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p5}, Landroid/webkit/WebViewCore;->nativeDeleteText(IIIII)V

    #@3
    return-void
.end method

.method static synthetic access$5900(Landroid/webkit/WebViewCore;IIIII)Ljava/lang/String;
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p5}, Landroid/webkit/WebViewCore;->nativeGetText(IIIII)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600(Landroid/webkit/WebViewCore;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->initialize()V

    #@3
    return-void
.end method

.method static synthetic access$6000(Landroid/webkit/WebViewCore;ILjava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeInsertText(ILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$6100(Landroid/webkit/WebViewCore;IIII)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewCore;->nativeSelectText(IIII)V

    #@3
    return-void
.end method

.method static synthetic access$6200(Landroid/webkit/WebViewCore;III)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->nativeSelectWordAt(III)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$6300(Landroid/webkit/WebViewCore;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeSelectAll(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$6400(Landroid/webkit/WebViewCore;ILjava/lang/String;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeFindAll(ILjava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$6500(Landroid/webkit/WebViewCore;IZ)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeFindNext(IZ)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$6600(Landroid/webkit/WebViewCore;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeSetInitialFocus(II)V

    #@3
    return-void
.end method

.method static synthetic access$6700(Landroid/webkit/WebViewCore;Ljava/io/OutputStream;Landroid/webkit/ValueCallback;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->saveViewState(Ljava/io/OutputStream;Landroid/webkit/ValueCallback;)V

    #@3
    return-void
.end method

.method static synthetic access$6800(Landroid/webkit/WebViewCore;ILjava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeSendPickerValue(ILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$6900(Landroid/webkit/WebViewCore;ILjava/lang/String;Ljava/lang/String;)I
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->nativeSaveCachedImageToFile(ILjava/lang/String;Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700()V
    .registers 0

    #@0
    .prologue
    .line 63
    invoke-static {}, Landroid/webkit/WebViewCore;->nativeCertTrustChanged()V

    #@3
    return-void
.end method

.method static synthetic access$7000(Landroid/webkit/WebViewCore;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeNotificationSendClick(II)V

    #@3
    return-void
.end method

.method static synthetic access$7100(Landroid/webkit/WebViewCore;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebViewCore;->nativeNotificationSendClose(II)V

    #@3
    return-void
.end method

.method static synthetic access$7200(Landroid/webkit/WebViewCore;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativeGetTextInputPos(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$7300(Landroid/webkit/WebViewCore;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->nativeMouseClick(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$7400(Landroid/webkit/WebViewCore;II[I[I[IIII)I
    .registers 10
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"
    .parameter "x8"

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p8}, Landroid/webkit/WebViewCore;->nativeHandleTouchEvent(II[I[I[IIII)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$7502(Landroid/webkit/WebViewCore;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput-boolean p1, p0, Landroid/webkit/WebViewCore;->mDrawIsScheduled:Z

    #@2
    return p1
.end method

.method private addSurface(Landroid/view/View;IIII)Landroid/webkit/ViewManager$ChildView;
    .registers 7
    .parameter "pluginView"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 3135
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->createSurface(Landroid/view/View;)Landroid/webkit/ViewManager$ChildView;

    #@3
    move-result-object v0

    #@4
    .line 3136
    .local v0, view:Landroid/webkit/ViewManager$ChildView;
    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/webkit/ViewManager$ChildView;->attachView(IIII)V

    #@7
    .line 3137
    return-object v0
.end method

.method private calculateWindowWidth(I)I
    .registers 5
    .parameter "viewWidth"

    #@0
    .prologue
    .line 2233
    move v0, p1

    #@1
    .line 2234
    .local v0, width:I
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@3
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_10

    #@9
    .line 2235
    iget v1, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@b
    const/4 v2, -0x1

    #@c
    if-ne v1, v2, :cond_11

    #@e
    .line 2237
    const/16 v0, 0x3d4

    #@10
    .line 2247
    :cond_10
    :goto_10
    return v0

    #@11
    .line 2238
    :cond_11
    iget v1, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@13
    if-lez v1, :cond_18

    #@15
    .line 2240
    iget v0, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@17
    goto :goto_10

    #@18
    .line 2243
    :cond_18
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@1a
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@1d
    move-result v1

    #@1e
    int-to-float v1, v1

    #@1f
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@21
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getDefaultZoomScale()F

    #@24
    move-result v2

    #@25
    div-float/2addr v1, v2

    #@26
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@29
    move-result v0

    #@2a
    goto :goto_10
.end method

.method private centerFitRect(IIII)V
    .registers 10
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 3185
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 3190
    :goto_4
    return-void

    #@5
    .line 3188
    :cond_5
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0x7f

    #@b
    new-instance v2, Landroid/graphics/Rect;

    #@d
    add-int v3, p1, p3

    #@f
    add-int v4, p2, p4

    #@11
    invoke-direct {v2, p1, p2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@1b
    goto :goto_4
.end method

.method private chromeCanTakeFocus(I)Z
    .registers 4
    .parameter "webkitDirection"

    #@0
    .prologue
    .line 412
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->mapDirection(I)I

    #@3
    move-result v0

    #@4
    .line 413
    .local v0, direction:I
    iget v1, p0, Landroid/webkit/WebViewCore;->mChromeCanFocusDirection:I

    #@6
    if-ne v0, v1, :cond_c

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v1, 0x1

    #@b
    :goto_b
    return v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method private chromeTakeFocus(I)V
    .registers 5
    .parameter "webkitDirection"

    #@0
    .prologue
    .line 401
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 406
    :goto_4
    return-void

    #@5
    .line 402
    :cond_5
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v2, 0x6e

    #@b
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    .line 404
    .local v0, m:Landroid/os/Message;
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore;->mapDirection(I)I

    #@12
    move-result v1

    #@13
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@15
    .line 405
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@18
    goto :goto_4
.end method

.method private clearCache(Z)V
    .registers 3
    .parameter "includeDiskFiles"

    #@0
    .prologue
    .line 2122
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@2
    invoke-virtual {v0}, Landroid/webkit/BrowserFrame;->clearCache()V

    #@5
    .line 2123
    if-eqz p1, :cond_a

    #@7
    .line 2124
    invoke-static {}, Landroid/webkit/CacheManager;->removeAllCacheFiles()Z

    #@a
    .line 2126
    :cond_a
    return-void
.end method

.method private clearTextEntry()V
    .registers 3

    #@0
    .prologue
    .line 2973
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 2976
    :goto_4
    return-void

    #@5
    .line 2974
    :cond_5
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0x6f

    #@b
    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@12
    goto :goto_4
.end method

.method private contentScrollTo(IIZZ)V
    .registers 11
    .parameter "x"
    .parameter "y"
    .parameter "animate"
    .parameter "onlyIfImeIsShowing"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2512
    iget-object v3, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@4
    invoke-virtual {v3}, Landroid/webkit/BrowserFrame;->firstLayoutDone()Z

    #@7
    move-result v3

    #@8
    if-nez v3, :cond_f

    #@a
    .line 2518
    iput p1, p0, Landroid/webkit/WebViewCore;->mRestoredX:I

    #@c
    .line 2519
    iput p2, p0, Landroid/webkit/WebViewCore;->mRestoredY:I

    #@e
    .line 2533
    :cond_e
    :goto_e
    return-void

    #@f
    .line 2522
    :cond_f
    iget-object v3, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@11
    if-eqz v3, :cond_e

    #@13
    .line 2523
    iget-object v3, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@15
    iget-object v4, v3, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@17
    const/16 v5, 0x65

    #@19
    if-eqz p3, :cond_38

    #@1b
    move v3, v1

    #@1c
    :goto_1c
    if-eqz p4, :cond_3a

    #@1e
    :goto_1e
    new-instance v2, Landroid/graphics/Point;

    #@20
    invoke-direct {v2, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    #@23
    invoke-static {v4, v5, v3, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@26
    move-result-object v0

    #@27
    .line 2526
    .local v0, msg:Landroid/os/Message;
    iget-boolean v1, p0, Landroid/webkit/WebViewCore;->mDrawIsScheduled:Z

    #@29
    if-eqz v1, :cond_3c

    #@2b
    .line 2527
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2d
    const/4 v2, 0x0

    #@2e
    const/16 v3, 0x7d

    #@30
    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@33
    move-result-object v2

    #@34
    invoke-static {v1, v2}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@37
    goto :goto_e

    #@38
    .end local v0           #msg:Landroid/os/Message;
    :cond_38
    move v3, v2

    #@39
    .line 2523
    goto :goto_1c

    #@3a
    :cond_3a
    move v1, v2

    #@3b
    goto :goto_1e

    #@3c
    .line 2530
    .restart local v0       #msg:Landroid/os/Message;
    :cond_3c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@3f
    goto :goto_e
.end method

.method private createSurface(Landroid/view/View;)Landroid/webkit/ViewManager$ChildView;
    .registers 5
    .parameter "pluginView"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3111
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 3128
    :goto_5
    return-object v0

    #@6
    .line 3115
    :cond_6
    if-nez p1, :cond_11

    #@8
    .line 3116
    const-string/jumbo v1, "webcore"

    #@b
    const-string v2, "Attempted to add an empty plugin view to the view hierarchy"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    goto :goto_5

    #@11
    .line 3121
    :cond_11
    const/4 v1, 0x0

    #@12
    invoke-virtual {p1, v1}, Landroid/view/View;->setWillNotDraw(Z)V

    #@15
    .line 3123
    instance-of v1, p1, Landroid/view/SurfaceView;

    #@17
    if-eqz v1, :cond_20

    #@19
    move-object v1, p1

    #@1a
    .line 3124
    check-cast v1, Landroid/view/SurfaceView;

    #@1c
    const/4 v2, 0x1

    #@1d
    invoke-virtual {v1, v2}, Landroid/view/SurfaceView;->setZOrderOnTop(Z)V

    #@20
    .line 3126
    :cond_20
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@22
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mViewManager:Landroid/webkit/ViewManager;

    #@24
    invoke-virtual {v1}, Landroid/webkit/ViewManager;->createView()Landroid/webkit/ViewManager$ChildView;

    #@27
    move-result-object v0

    #@28
    .line 3127
    .local v0, view:Landroid/webkit/ViewManager$ChildView;
    iput-object p1, v0, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@2a
    goto :goto_5
.end method

.method private createTextSelection(III)Landroid/webkit/WebViewCore$TextSelectionData;
    .registers 6
    .parameter "start"
    .parameter "end"
    .parameter "selPtr"

    #@0
    .prologue
    .line 2924
    new-instance v0, Landroid/webkit/WebViewCore$TextSelectionData;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/webkit/WebViewCore$TextSelectionData;-><init>(III)V

    #@5
    .line 2925
    .local v0, data:Landroid/webkit/WebViewCore$TextSelectionData;
    iget v1, p0, Landroid/webkit/WebViewCore;->mTextSelectionChangeReason:I

    #@7
    iput v1, v0, Landroid/webkit/WebViewCore$TextSelectionData;->mSelectionReason:I

    #@9
    .line 2926
    return-object v0
.end method

.method private createTextSelection(IIILjava/lang/String;)Landroid/webkit/WebViewCore$TextSelectionData;
    .registers 7
    .parameter "start"
    .parameter "end"
    .parameter "selPtr"
    .parameter "text"

    #@0
    .prologue
    .line 2931
    new-instance v0, Landroid/webkit/WebViewCore$TextSelectionData;

    #@2
    invoke-direct {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewCore$TextSelectionData;-><init>(IIILjava/lang/String;)V

    #@5
    .line 2932
    .local v0, data:Landroid/webkit/WebViewCore$TextSelectionData;
    iget v1, p0, Landroid/webkit/WebViewCore;->mTextSelectionChangeReason:I

    #@7
    iput v1, v0, Landroid/webkit/WebViewCore$TextSelectionData;->mSelectionReason:I

    #@9
    .line 2933
    return-object v0
.end method

.method private destroySurface(Landroid/webkit/ViewManager$ChildView;)V
    .registers 2
    .parameter "childView"

    #@0
    .prologue
    .line 3146
    invoke-virtual {p1}, Landroid/webkit/ViewManager$ChildView;->removeView()V

    #@3
    .line 3147
    return-void
.end method

.method private didFirstLayout(Z)V
    .registers 6
    .parameter "standardLoad"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2585
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@3
    invoke-virtual {v2}, Landroid/webkit/BrowserFrame;->didFirstLayout()V

    #@6
    .line 2587
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@8
    if-nez v2, :cond_b

    #@a
    .line 2606
    :goto_a
    return-void

    #@b
    .line 2589
    :cond_b
    if-nez p1, :cond_11

    #@d
    iget-boolean v2, p0, Landroid/webkit/WebViewCore;->mIsRestored:Z

    #@f
    if-eqz v2, :cond_33

    #@11
    :cond_11
    const/4 v0, 0x1

    #@12
    .line 2590
    .local v0, updateViewState:Z
    :goto_12
    invoke-direct {p0, v0}, Landroid/webkit/WebViewCore;->setupViewport(Z)V

    #@15
    .line 2594
    if-nez v0, :cond_1e

    #@17
    .line 2595
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@19
    iget-object v2, v2, Landroid/webkit/WebViewClassic;->mViewManager:Landroid/webkit/ViewManager;

    #@1b
    invoke-virtual {v2}, Landroid/webkit/ViewManager;->postReadyToDrawAll()V

    #@1e
    .line 2599
    :cond_1e
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@20
    iget-object v2, v2, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@22
    const/16 v3, 0x83

    #@24
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@27
    .line 2603
    iput v1, p0, Landroid/webkit/WebViewCore;->mRestoredY:I

    #@29
    iput v1, p0, Landroid/webkit/WebViewCore;->mRestoredX:I

    #@2b
    .line 2604
    iput-boolean v1, p0, Landroid/webkit/WebViewCore;->mIsRestored:Z

    #@2d
    .line 2605
    const/4 v1, 0x0

    #@2e
    iput v1, p0, Landroid/webkit/WebViewCore;->mRestoredTextWrapScale:F

    #@30
    iput v1, p0, Landroid/webkit/WebViewCore;->mRestoredScale:F

    #@32
    goto :goto_a

    #@33
    .end local v0           #updateViewState:Z
    :cond_33
    move v0, v1

    #@34
    .line 2589
    goto :goto_12
.end method

.method private focusNodeChanged(ILandroid/webkit/WebViewCore$WebKitHitTest;)V
    .registers 6
    .parameter "nodePointer"
    .parameter "hitTest"

    #@0
    .prologue
    .line 392
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 395
    :goto_4
    return-void

    #@5
    .line 393
    :cond_5
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0x93

    #@b
    const/4 v2, 0x0

    #@c
    invoke-virtual {v0, v1, p1, v2, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@13
    goto :goto_4
.end method

.method static getFixedDisplayDensity(Landroid/content/Context;)F
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/high16 v2, 0x42c8

    #@2
    .line 2617
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@9
    move-result-object v1

    #@a
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    #@c
    .line 2618
    .local v0, density:F
    mul-float v1, v0, v2

    #@e
    float-to-int v1, v1

    #@f
    int-to-float v1, v1

    #@10
    div-float/2addr v1, v2

    #@11
    return v1
.end method

.method private getPluginClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Class;
    .registers 10
    .parameter "libName"
    .parameter "clsName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3062
    iget-object v4, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@3
    if-nez v4, :cond_6

    #@5
    .line 3083
    :goto_5
    return-object v3

    #@6
    .line 3066
    :cond_6
    invoke-static {v3}, Landroid/webkit/PluginManager;->getInstance(Landroid/content/Context;)Landroid/webkit/PluginManager;

    #@9
    move-result-object v2

    #@a
    .line 3068
    .local v2, pluginManager:Landroid/webkit/PluginManager;
    invoke-virtual {v2, p1}, Landroid/webkit/PluginManager;->getPluginsAPKName(Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .line 3069
    .local v1, pkgName:Ljava/lang/String;
    if-nez v1, :cond_30

    #@10
    .line 3070
    const-string/jumbo v4, "webcore"

    #@13
    new-instance v5, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v6, "Unable to resolve "

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    const-string v6, " to a plugin APK"

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_5

    #@30
    .line 3075
    :cond_30
    :try_start_30
    invoke-virtual {v2, v1, p2}, Landroid/webkit/PluginManager;->getPluginClass(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Class;
    :try_end_33
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_30 .. :try_end_33} :catch_35
    .catch Ljava/lang/ClassNotFoundException; {:try_start_30 .. :try_end_33} :catch_56

    #@33
    move-result-object v3

    #@34
    goto :goto_5

    #@35
    .line 3076
    :catch_35
    move-exception v0

    #@36
    .line 3077
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string/jumbo v4, "webcore"

    #@39
    new-instance v5, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v6, "Unable to find plugin classloader for the apk ("

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    const-string v6, ")"

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_5

    #@56
    .line 3078
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_56
    move-exception v0

    #@57
    .line 3079
    .local v0, e:Ljava/lang/ClassNotFoundException;
    const-string/jumbo v4, "webcore"

    #@5a
    new-instance v5, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v6, "Unable to find plugin class ("

    #@61
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    const-string v6, ") in the apk ("

    #@6b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v5

    #@73
    const-string v6, ")"

    #@75
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v5

    #@79
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    goto :goto_5
.end method

.method private getUsedQuota()J
    .registers 9

    #@0
    .prologue
    .line 2253
    invoke-static {}, Landroid/webkit/WebStorageClassic;->getInstance()Landroid/webkit/WebStorageClassic;

    #@3
    move-result-object v4

    #@4
    .line 2254
    .local v4, webStorage:Landroid/webkit/WebStorageClassic;
    invoke-virtual {v4}, Landroid/webkit/WebStorageClassic;->getOriginsSync()Ljava/util/Collection;

    #@7
    move-result-object v1

    #@8
    .line 2256
    .local v1, origins:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/webkit/WebStorage$Origin;>;"
    if-nez v1, :cond_d

    #@a
    .line 2257
    const-wide/16 v2, 0x0

    #@c
    .line 2263
    :cond_c
    return-wide v2

    #@d
    .line 2259
    :cond_d
    const-wide/16 v2, 0x0

    #@f
    .line 2260
    .local v2, usedQuota:J
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v0

    #@13
    .local v0, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v6

    #@17
    if-eqz v6, :cond_c

    #@19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v5

    #@1d
    check-cast v5, Landroid/webkit/WebStorage$Origin;

    #@1f
    .line 2261
    .local v5, website:Landroid/webkit/WebStorage$Origin;
    invoke-virtual {v5}, Landroid/webkit/WebStorage$Origin;->getQuota()J

    #@22
    move-result-wide v6

    #@23
    add-long/2addr v2, v6

    #@24
    goto :goto_13
.end method

.method private getWebView()Landroid/webkit/WebView;
    .registers 2

    #@0
    .prologue
    .line 2569
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private hideFullScreenPlugin()V
    .registers 3

    #@0
    .prologue
    .line 3103
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 3108
    :goto_4
    return-void

    #@5
    .line 3106
    :cond_5
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0x79

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@12
    goto :goto_4
.end method

.method private inBrowserApp()Z
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 238
    iget-object v5, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@6
    move-result-object v3

    #@7
    .line 239
    .local v3, pm:Landroid/content/pm/PackageManager;
    const-string v0, "com.android.browser.Browser"

    #@9
    .line 241
    .local v0, LGBrowserClassName:Ljava/lang/String;
    :try_start_9
    iget-object v5, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@e
    move-result-object v5

    #@f
    const/4 v6, 0x0

    #@10
    invoke-virtual {v3, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@13
    move-result-object v1

    #@14
    .line 242
    .local v1, ai:Landroid/content/pm/ApplicationInfo;
    if-eqz v1, :cond_25

    #@16
    iget-object v5, v1, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@18
    if-eqz v5, :cond_25

    #@1a
    iget-object v5, v1, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@1c
    const-string v6, "com.android.browser.Browser"

    #@1e
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_21
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9 .. :try_end_21} :catch_26

    #@21
    move-result v5

    #@22
    if-eqz v5, :cond_25

    #@24
    .line 243
    const/4 v4, 0x1

    #@25
    .line 248
    .end local v1           #ai:Landroid/content/pm/ApplicationInfo;
    :cond_25
    :goto_25
    return v4

    #@26
    .line 247
    :catch_26
    move-exception v2

    #@27
    .line 248
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_25
.end method

.method private initEditField(IIILandroid/webkit/WebViewCore$TextFieldInitData;)V
    .registers 10
    .parameter "start"
    .parameter "end"
    .parameter "selectionPtr"
    .parameter "initData"

    #@0
    .prologue
    .line 2981
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 2991
    :goto_4
    return-void

    #@5
    .line 2984
    :cond_5
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0x8e

    #@b
    invoke-static {v0, v1, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@12
    .line 2986
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@14
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@16
    const/16 v1, 0x70

    #@18
    iget v2, p4, Landroid/webkit/WebViewCore$TextFieldInitData;->mFieldPointer:I

    #@1a
    const/4 v3, 0x0

    #@1b
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore;->createTextSelection(III)Landroid/webkit/WebViewCore$TextSelectionData;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v0, v1, v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@26
    goto :goto_4
.end method

.method private initialize()V
    .registers 7

    #@0
    .prologue
    .line 260
    new-instance v0, Landroid/webkit/BrowserFrame;

    #@2
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@4
    iget-object v3, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@6
    iget-object v4, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@8
    iget-object v5, p0, Landroid/webkit/WebViewCore;->mJavascriptInterfaces:Ljava/util/Map;

    #@a
    move-object v2, p0

    #@b
    invoke-direct/range {v0 .. v5}, Landroid/webkit/BrowserFrame;-><init>(Landroid/content/Context;Landroid/webkit/WebViewCore;Landroid/webkit/CallbackProxy;Landroid/webkit/WebSettingsClassic;Ljava/util/Map;)V

    #@e
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@10
    .line 262
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mJavascriptInterfaces:Ljava/util/Map;

    #@13
    .line 264
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@15
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@17
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettingsClassic;->syncSettingsAndCreateHandler(Landroid/webkit/BrowserFrame;)V

    #@1a
    .line 266
    invoke-static {}, Landroid/webkit/WebIconDatabaseClassic;->getInstance()Landroid/webkit/WebIconDatabaseClassic;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Landroid/webkit/WebIconDatabaseClassic;->createHandler()V

    #@21
    .line 268
    invoke-static {}, Landroid/webkit/WebStorageClassic;->getInstance()Landroid/webkit/WebStorageClassic;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Landroid/webkit/WebStorageClassic;->createHandler()V

    #@28
    .line 270
    invoke-static {}, Landroid/webkit/GeolocationPermissionsClassic;->getInstance()Landroid/webkit/GeolocationPermissionsClassic;

    #@2b
    move-result-object v0

    #@2c
    invoke-virtual {v0}, Landroid/webkit/GeolocationPermissionsClassic;->createHandler()V

    #@2f
    .line 273
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@31
    invoke-static {v0}, Landroid/webkit/WebViewCore$EventHub;->access$200(Landroid/webkit/WebViewCore$EventHub;)V

    #@34
    .line 277
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@36
    if-eqz v0, :cond_48

    #@38
    .line 278
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@3a
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@3c
    const/16 v1, 0x6b

    #@3e
    iget v2, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@40
    const/4 v3, 0x0

    #@41
    invoke-static {v0, v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@48
    .line 283
    :cond_48
    return-void
.end method

.method static isSupportedMediaMimeType(Ljava/lang/String;)Z
    .registers 3
    .parameter "mimeType"

    #@0
    .prologue
    .line 359
    invoke-static {p0}, Landroid/media/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    .line 360
    .local v0, fileType:I
    invoke-static {v0}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_21

    #@a
    invoke-static {v0}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_21

    #@10
    invoke-static {v0}, Landroid/media/MediaFile;->isPlayListFileType(I)Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_21

    #@16
    if-eqz p0, :cond_23

    #@18
    const-string/jumbo v1, "video/m4v"

    #@1b
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_23

    #@21
    :cond_21
    const/4 v1, 0x1

    #@22
    :goto_22
    return v1

    #@23
    :cond_23
    const/4 v1, 0x0

    #@24
    goto :goto_22
.end method

.method static isUpdatePicturePaused(Landroid/webkit/WebViewCore;)Z
    .registers 2
    .parameter "core"

    #@0
    .prologue
    .line 2470
    if-eqz p0, :cond_5

    #@2
    iget-boolean v0, p0, Landroid/webkit/WebViewCore;->mDrawIsPaused:Z

    #@4
    :goto_4
    return v0

    #@5
    :cond_5
    const/4 v0, 0x0

    #@6
    goto :goto_4
.end method

.method private keepScreenOn(Z)V
    .registers 5
    .parameter "screenOn"

    #@0
    .prologue
    .line 3051
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-eqz v1, :cond_16

    #@4
    .line 3052
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@6
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@8
    const/16 v2, 0x88

    #@a
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 3054
    .local v0, message:Landroid/os/Message;
    if-eqz p1, :cond_17

    #@10
    const/4 v1, 0x1

    #@11
    :goto_11
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@13
    .line 3055
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@16
    .line 3057
    .end local v0           #message:Landroid/os/Message;
    :cond_16
    return-void

    #@17
    .line 3054
    .restart local v0       #message:Landroid/os/Message;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_11
.end method

.method private key(Landroid/view/KeyEvent;IZ)V
    .registers 16
    .parameter "evt"
    .parameter "canTakeFocusDirection"
    .parameter "isDown"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 2145
    iput p2, p0, Landroid/webkit/WebViewCore;->mChromeCanFocusDirection:I

    #@3
    .line 2146
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@6
    move-result v2

    #@7
    .line 2147
    .local v2, keyCode:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    #@a
    move-result v3

    #@b
    .line 2149
    .local v3, unicodeChar:I
    if-nez v2, :cond_25

    #@d
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    if-eqz v0, :cond_25

    #@13
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@1a
    move-result v0

    #@1b
    if-lez v0, :cond_25

    #@1d
    .line 2152
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0, v11}, Ljava/lang/String;->codePointAt(I)I

    #@24
    move-result v3

    #@25
    .line 2155
    :cond_25
    iget v1, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@27
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@2a
    move-result v4

    #@2b
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z

    #@2e
    move-result v5

    #@2f
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    #@32
    move-result v6

    #@33
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isSymPressed()Z

    #@36
    move-result v7

    #@37
    move-object v0, p0

    #@38
    move v8, p3

    #@39
    invoke-direct/range {v0 .. v8}, Landroid/webkit/WebViewCore;->nativeKey(IIIIZZZZ)Z

    #@3c
    move-result v9

    #@3d
    .line 2158
    .local v9, handled:Z
    iput v11, p0, Landroid/webkit/WebViewCore;->mChromeCanFocusDirection:I

    #@3f
    .line 2159
    if-nez v9, :cond_60

    #@41
    const/16 v0, 0x42

    #@43
    if-eq v2, v0, :cond_60

    #@45
    .line 2160
    const/16 v0, 0x13

    #@47
    if-lt v2, v0, :cond_61

    #@49
    const/16 v0, 0x16

    #@4b
    if-gt v2, v0, :cond_61

    #@4d
    .line 2162
    if-eqz p2, :cond_60

    #@4f
    if-eqz p3, :cond_60

    #@51
    .line 2163
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@53
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@55
    const/16 v1, 0x6e

    #@57
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@5a
    move-result-object v10

    #@5b
    .line 2165
    .local v10, m:Landroid/os/Message;
    iput p2, v10, Landroid/os/Message;->arg1:I

    #@5d
    .line 2166
    invoke-virtual {v10}, Landroid/os/Message;->sendToTarget()V

    #@60
    .line 2175
    .end local v10           #m:Landroid/os/Message;
    :cond_60
    :goto_60
    return-void

    #@61
    .line 2173
    :cond_61
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@63
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->onUnhandledKeyEvent(Landroid/view/KeyEvent;)V

    #@66
    goto :goto_60
.end method

.method private keyPress(I)V
    .registers 11
    .parameter "unicodeChar"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2178
    iget v1, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@3
    const/4 v8, 0x1

    #@4
    move-object v0, p0

    #@5
    move v3, p1

    #@6
    move v4, v2

    #@7
    move v5, v2

    #@8
    move v6, v2

    #@9
    move v7, v2

    #@a
    invoke-direct/range {v0 .. v8}, Landroid/webkit/WebViewCore;->nativeKey(IIIIZZZZ)Z

    #@d
    .line 2179
    iget v1, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@f
    move-object v0, p0

    #@10
    move v3, p1

    #@11
    move v4, v2

    #@12
    move v5, v2

    #@13
    move v6, v2

    #@14
    move v7, v2

    #@15
    move v8, v2

    #@16
    invoke-direct/range {v0 .. v8}, Landroid/webkit/WebViewCore;->nativeKey(IIIIZZZZ)Z

    #@19
    .line 2180
    return-void
.end method

.method private loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .registers 4
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2130
    .local p2, extraHeaders:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/BrowserFrame;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    #@5
    .line 2131
    return-void
.end method

.method private mapDirection(I)I
    .registers 3
    .parameter "webkitDirection"

    #@0
    .prologue
    .line 432
    packed-switch p1, :pswitch_data_16

    #@3
    .line 446
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 434
    :pswitch_5
    const/4 v0, 0x2

    #@6
    goto :goto_4

    #@7
    .line 436
    :pswitch_7
    const/4 v0, 0x1

    #@8
    goto :goto_4

    #@9
    .line 438
    :pswitch_9
    const/16 v0, 0x21

    #@b
    goto :goto_4

    #@c
    .line 440
    :pswitch_c
    const/16 v0, 0x82

    #@e
    goto :goto_4

    #@f
    .line 442
    :pswitch_f
    const/16 v0, 0x11

    #@11
    goto :goto_4

    #@12
    .line 444
    :pswitch_12
    const/16 v0, 0x42

    #@14
    goto :goto_4

    #@15
    .line 432
    nop

    #@16
    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_5
        :pswitch_7
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_12
    .end packed-switch
.end method

.method private native nativeAutoFillForm(II)V
.end method

.method private static native nativeCertTrustChanged()V
.end method

.method private native nativeClearContent(I)V
.end method

.method private native nativeClearTextSelection(I)V
.end method

.method private native nativeCloseIdleConnections(I)V
.end method

.method private native nativeContentInvalidateAll(I)V
.end method

.method private native nativeDeleteSelection(IIII)V
.end method

.method private native nativeDeleteText(IIIII)V
.end method

.method private native nativeDumpDomTree(IZ)V
.end method

.method private native nativeDumpRenderTree(IZ)V
.end method

.method static native nativeFindAddress(Ljava/lang/String;Z)Ljava/lang/String;
.end method

.method private native nativeFindAll(ILjava/lang/String;)I
.end method

.method private native nativeFindNext(IZ)I
.end method

.method private native nativeFreeMemory(I)V
.end method

.method private native nativeFullScreenPluginHidden(II)V
.end method

.method private native nativeGeolocationPermissionsProvide(ILjava/lang/String;ZZ)V
.end method

.method private native nativeGetContentMinPrefWidth(I)I
.end method

.method private native nativeGetText(IIIII)Ljava/lang/String;
.end method

.method private native nativeGetTextInputPos(I)I
.end method

.method private native nativeHandleTouchEvent(II[I[I[IIII)I
.end method

.method private native nativeHitTest(IIIIZ)Landroid/webkit/WebViewCore$WebKitHitTest;
.end method

.method private native nativeInParagraphMode(I)Z
.end method

.method private native nativeInsertText(ILjava/lang/String;)V
.end method

.method private native nativeKey(IIIIZZZZ)Z
.end method

.method private native nativeModifySelection(III)Ljava/lang/String;
.end method

.method private native nativeMouseClick(I)Z
.end method

.method private native nativeMoveMouse(III)V
.end method

.method private native nativeNotificationSendClick(II)V
.end method

.method private native nativeNotificationSendClose(II)V
.end method

.method private native nativeNotificationSendRequestCallback(I)V
.end method

.method private native nativeNotifyAnimationStarted(I)V
.end method

.method private native nativePause(I)V
.end method

.method private native nativePluginSurfaceReady(I)V
.end method

.method private native nativeProvideVisitedHistory(I[Ljava/lang/String;)V
.end method

.method private native nativeRecordContent(ILandroid/graphics/Point;)I
.end method

.method private native nativeRegisterURLSchemeAsLocal(ILjava/lang/String;)V
.end method

.method private native nativeReplaceTextfieldText(IIILjava/lang/String;III)V
.end method

.method private native nativeRequestLabel(III)Ljava/lang/String;
.end method

.method private native nativeResume(I)V
.end method

.method private native nativeRetrieveAnchorText(III)Ljava/lang/String;
.end method

.method private native nativeRetrieveHref(III)Ljava/lang/String;
.end method

.method private native nativeRetrieveImageSource(III)Ljava/lang/String;
.end method

.method private native nativeRevealSelection(I)V
.end method

.method private native nativeSaveCachedImageToFile(ILjava/lang/String;Ljava/lang/String;)I
.end method

.method private native nativeSaveDocumentState(I)V
.end method

.method private native nativeScrollFocusedTextInput(IFI)V
.end method

.method private native nativeScrollLayer(IILandroid/graphics/Rect;)V
.end method

.method private native nativeSelectAll(I)V
.end method

.method private final nativeSelectAll(IZ)V
	.registers 3
	
	invoke-virtual {p0, p1}, Landroid/webkit/WebViewCore;->nativeSelectAll(I)V
.end method

.method private native nativeSelectText(IIII)V
.end method

.method private native nativeSelectWordAt(III)Z
.end method

.method private native nativeSendListBoxChoice(II)V
.end method

.method private native nativeSendListBoxChoices(I[ZI)V
.end method

.method private native nativeSendPickerValue(ILjava/lang/String;)V
.end method

.method private native nativeSetBackgroundColor(II)V
.end method

.method private native nativeSetFocusControllerActive(IZ)V
.end method

.method private native nativeSetGlobalBounds(IIIII)V
.end method

.method private native nativeSetInitialFocus(II)V
.end method

.method private native nativeSetJsFlags(ILjava/lang/String;)V
.end method

.method private native nativeSetNewStorageLimit(IJ)V
.end method

.method private native nativeSetScrollOffset(IZII)V
.end method

.method private native nativeSetSelection(III)V
.end method

.method private native nativeSetSize(IIIIFIIIIZ)V
.end method

.method private native nativeUpdateTextSelection(I)V
.end method

.method private needTouchEvents(Z)V
    .registers 6
    .parameter "need"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2907
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@3
    if-eqz v0, :cond_15

    #@5
    .line 2908
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v3, 0x74

    #@b
    if-eqz p1, :cond_16

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    invoke-static {v2, v3, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@15
    .line 2912
    :cond_15
    return-void

    #@16
    :cond_16
    move v0, v1

    #@17
    .line 2908
    goto :goto_e
.end method

.method private openColorPicker(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 3262
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->openColorPicker(Ljava/lang/String;)V

    #@5
    .line 3263
    return-void
.end method

.method private openDateTimePicker(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    .line 3255
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/CallbackProxy;->openDateTimePicker(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 3256
    return-void
.end method

.method private openFileChooser(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "acceptType"
    .parameter "capture"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 458
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/webkit/CallbackProxy;->openFileChooser(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v1

    #@8
    .line 459
    .local v1, uri:Landroid/net/Uri;
    const-string v6, "file://"

    #@a
    .line 460
    .local v6, FILE_PATH_PREFIX:Ljava/lang/String;
    if-eqz v1, :cond_87

    #@c
    .line 461
    const-string v8, ""

    #@e
    .line 464
    .local v8, filePath:Ljava/lang/String;
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v0

    #@14
    const/4 v2, 0x1

    #@15
    new-array v2, v2, [Ljava/lang/String;

    #@17
    const-string v4, "_data"

    #@19
    aput-object v4, v2, v5

    #@1b
    move-object v4, v3

    #@1c
    move-object v5, v3

    #@1d
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@20
    move-result-object v7

    #@21
    .line 468
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_82

    #@23
    .line 470
    :try_start_23
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_2e

    #@29
    .line 471
    const/4 v0, 0x0

    #@2a
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2d
    .catchall {:try_start_23 .. :try_end_2d} :catchall_5c

    #@2d
    move-result-object v8

    #@2e
    .line 474
    :cond_2e
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@31
    .line 476
    if-eqz v8, :cond_52

    #@33
    const-string v0, "file://"

    #@35
    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@38
    move-result v0

    #@39
    if-nez v0, :cond_52

    #@3b
    .line 477
    new-instance v0, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v2, "file://"

    #@42
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v0

    #@4a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v0

    #@4e
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@51
    move-result-object v1

    #@52
    .line 483
    :cond_52
    :goto_52
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@55
    move-result-object v9

    #@56
    .line 484
    .local v9, uriString:Ljava/lang/String;
    sget-object v0, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@58
    invoke-virtual {v0, v8, v9}, Landroid/webkit/JWebCoreJavaBridge;->storeFilePathForContentUri(Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    .line 487
    .end local v7           #cursor:Landroid/database/Cursor;
    .end local v8           #filePath:Ljava/lang/String;
    .end local v9           #uriString:Ljava/lang/String;
    :goto_5b
    return-object v9

    #@5c
    .line 474
    .restart local v7       #cursor:Landroid/database/Cursor;
    .restart local v8       #filePath:Ljava/lang/String;
    :catchall_5c
    move-exception v0

    #@5d
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@60
    .line 476
    if-eqz v8, :cond_81

    #@62
    const-string v2, "file://"

    #@64
    invoke-virtual {v8, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@67
    move-result v2

    #@68
    if-nez v2, :cond_81

    #@6a
    .line 477
    new-instance v2, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v3, "file://"

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v2

    #@7d
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@80
    move-result-object v1

    #@81
    .line 474
    :cond_81
    throw v0

    #@82
    .line 481
    :cond_82
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@85
    move-result-object v8

    #@86
    goto :goto_52

    #@87
    .line 487
    .end local v7           #cursor:Landroid/database/Cursor;
    .end local v8           #filePath:Ljava/lang/String;
    :cond_87
    const-string v9, ""

    #@89
    goto :goto_5b
.end method

.method private paragraphSelectionEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 3334
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getParagraphCopyEnabled()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private native passToJs(IILjava/lang/String;IIZZZZ)V
.end method

.method public static pauseTimers()V
    .registers 2

    #@0
    .prologue
    .line 315
    sget-object v0, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 316
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "No WebView has been created in this process!"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 319
    :cond_c
    sget-object v0, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@e
    invoke-virtual {v0}, Landroid/webkit/JWebCoreJavaBridge;->pause()V

    #@11
    .line 320
    return-void
.end method

.method static pauseUpdatePicture(Landroid/webkit/WebViewCore;)V
    .registers 3
    .parameter "core"

    #@0
    .prologue
    .line 2437
    if-eqz p0, :cond_c

    #@2
    .line 2438
    invoke-virtual {p0}, Landroid/webkit/WebViewCore;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->enableSmoothTransition()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_d

    #@c
    .line 2449
    :cond_c
    :goto_c
    return-void

    #@d
    .line 2440
    :cond_d
    monitor-enter p0

    #@e
    .line 2441
    :try_start_e
    iget v0, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@10
    if-nez v0, :cond_1f

    #@12
    .line 2442
    const-string/jumbo v0, "webcore"

    #@15
    const-string v1, "Cannot pauseUpdatePicture, core destroyed or not initialized!"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 2443
    monitor-exit p0

    #@1b
    goto :goto_c

    #@1c
    .line 2446
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_e .. :try_end_1e} :catchall_1c

    #@1e
    throw v0

    #@1f
    .line 2445
    :cond_1f
    const/4 v0, 0x1

    #@20
    :try_start_20
    iput-boolean v0, p0, Landroid/webkit/WebViewCore;->mDrawIsPaused:Z

    #@22
    .line 2446
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_1c

    #@23
    goto :goto_c
.end method

.method private performHitTest(IIIZ)Landroid/webkit/WebViewCore$WebKitHitTest;
    .registers 12
    .parameter "x"
    .parameter "y"
    .parameter "slop"
    .parameter "moveMouse"

    #@0
    .prologue
    .line 2113
    iget v1, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@2
    move-object v0, p0

    #@3
    move v2, p1

    #@4
    move v3, p2

    #@5
    move v4, p3

    #@6
    move v5, p4

    #@7
    invoke-direct/range {v0 .. v5}, Landroid/webkit/WebViewCore;->nativeHitTest(IIIIZ)Landroid/webkit/WebViewCore$WebKitHitTest;

    #@a
    move-result-object v6

    #@b
    .line 2114
    .local v6, hit:Landroid/webkit/WebViewCore$WebKitHitTest;
    iput p1, v6, Landroid/webkit/WebViewCore$WebKitHitTest;->mHitTestX:I

    #@d
    .line 2115
    iput p2, v6, Landroid/webkit/WebViewCore$WebKitHitTest;->mHitTestY:I

    #@f
    .line 2116
    iput p3, v6, Landroid/webkit/WebViewCore$WebKitHitTest;->mHitTestSlop:I

    #@11
    .line 2117
    iput-boolean p4, v6, Landroid/webkit/WebViewCore$WebKitHitTest;->mHitTestMovedMouse:Z

    #@13
    .line 2118
    return-object v6
.end method

.method static reducePriority()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2412
    sget-object v0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@3
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@6
    .line 2413
    sget-object v0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@8
    const/4 v1, 0x2

    #@9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@c
    .line 2414
    sget-object v0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@e
    sget-object v1, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    #@17
    .line 2416
    return-void
.end method

.method private requestKeyboard(Z)V
    .registers 6
    .parameter "showKeyboard"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3030
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@3
    if-eqz v0, :cond_15

    #@5
    .line 3031
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v2, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v3, 0x76

    #@b
    if-eqz p1, :cond_16

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    invoke-static {v2, v3, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@15
    .line 3035
    :cond_15
    return-void

    #@16
    :cond_16
    move v0, v1

    #@17
    .line 3031
    goto :goto_e
.end method

.method private requestListBox([Ljava/lang/String;[IIZ)V
    .registers 6
    .parameter "array"
    .parameter "enabledArray"
    .parameter "selection"
    .parameter "isDatalist"

    #@0
    .prologue
    .line 3021
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 3022
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@6
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewClassic;->requestListBox([Ljava/lang/String;[IIZ)V

    #@9
    .line 3025
    :cond_9
    return-void
.end method

.method private requestListBox([Ljava/lang/String;[I[I)V
    .registers 5
    .parameter "array"
    .parameter "enabledArray"
    .parameter "selectedArray"

    #@0
    .prologue
    .line 3012
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 3013
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@6
    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/WebViewClassic;->requestListBox([Ljava/lang/String;[I[I)V

    #@9
    .line 3015
    :cond_9
    return-void
.end method

.method private restoreScale(FF)V
    .registers 4
    .parameter "scale"
    .parameter "textWrapScale"

    #@0
    .prologue
    .line 2896
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@2
    invoke-virtual {v0}, Landroid/webkit/BrowserFrame;->firstLayoutDone()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_17

    #@8
    .line 2897
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Landroid/webkit/WebViewCore;->mIsRestored:Z

    #@b
    .line 2898
    iput p1, p0, Landroid/webkit/WebViewCore;->mRestoredScale:F

    #@d
    .line 2899
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@f
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_17

    #@15
    .line 2900
    iput p2, p0, Landroid/webkit/WebViewCore;->mRestoredTextWrapScale:F

    #@17
    .line 2903
    :cond_17
    return-void
.end method

.method private restoreState(I)V
    .registers 7
    .parameter "index"

    #@0
    .prologue
    .line 2476
    iget-object v3, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v3}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@5
    move-result-object v1

    #@6
    .line 2477
    .local v1, list:Landroid/webkit/WebBackForwardListClassic;
    invoke-virtual {v1}, Landroid/webkit/WebBackForwardListClassic;->getSize()I

    #@9
    move-result v2

    #@a
    .line 2478
    .local v2, size:I
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v2, :cond_1b

    #@d
    .line 2479
    invoke-virtual {v1, v0}, Landroid/webkit/WebBackForwardListClassic;->getItemAtIndex(I)Landroid/webkit/WebHistoryItemClassic;

    #@10
    move-result-object v3

    #@11
    iget-object v4, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@13
    iget v4, v4, Landroid/webkit/BrowserFrame;->mNativeFrame:I

    #@15
    invoke-virtual {v3, v4}, Landroid/webkit/WebHistoryItemClassic;->inflate(I)V

    #@18
    .line 2478
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_b

    #@1b
    .line 2481
    :cond_1b
    iget-object v3, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@1d
    const/4 v4, 0x1

    #@1e
    iput-boolean v4, v3, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@20
    .line 2482
    iget-object v3, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@22
    iget v3, v3, Landroid/webkit/BrowserFrame;->mNativeFrame:I

    #@24
    invoke-static {v3, p1}, Landroid/webkit/WebBackForwardListClassic;->restoreIndex(II)V

    #@27
    .line 2483
    iget-object v3, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@29
    const/4 v4, 0x0

    #@2a
    iput-boolean v4, v3, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@2c
    .line 2484
    return-void
.end method

.method static resumePriority()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 2420
    sget-object v0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@3
    const/4 v1, 0x1

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@7
    .line 2421
    sget-object v0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@c
    .line 2422
    sget-object v0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@e
    sget-object v1, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    #@17
    .line 2424
    return-void
.end method

.method public static resumeTimers()V
    .registers 2

    #@0
    .prologue
    .line 326
    sget-object v0, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 327
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "No WebView has been created in this process!"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 330
    :cond_c
    sget-object v0, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@e
    invoke-virtual {v0}, Landroid/webkit/JWebCoreJavaBridge;->resume()V

    #@11
    .line 331
    return-void
.end method

.method static resumeUpdatePicture(Landroid/webkit/WebViewCore;)V
    .registers 3
    .parameter "core"

    #@0
    .prologue
    .line 2452
    if-eqz p0, :cond_6

    #@2
    .line 2454
    iget-boolean v0, p0, Landroid/webkit/WebViewCore;->mDrawIsPaused:Z

    #@4
    if-nez v0, :cond_7

    #@6
    .line 2467
    :cond_6
    :goto_6
    return-void

    #@7
    .line 2457
    :cond_7
    monitor-enter p0

    #@8
    .line 2458
    :try_start_8
    iget v0, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@a
    if-nez v0, :cond_19

    #@c
    .line 2459
    const-string/jumbo v0, "webcore"

    #@f
    const-string v1, "Cannot resumeUpdatePicture, core destroyed!"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 2460
    monitor-exit p0

    #@15
    goto :goto_6

    #@16
    .line 2465
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_16

    #@18
    throw v0

    #@19
    .line 2462
    :cond_19
    const/4 v0, 0x0

    #@1a
    :try_start_1a
    iput-boolean v0, p0, Landroid/webkit/WebViewCore;->mDrawIsPaused:Z

    #@1c
    .line 2464
    const/4 v0, 0x0

    #@1d
    iput-boolean v0, p0, Landroid/webkit/WebViewCore;->mDrawIsScheduled:Z

    #@1f
    .line 2465
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_1a .. :try_end_20} :catchall_16

    #@20
    goto :goto_6
.end method

.method private saveViewState(Ljava/io/OutputStream;Landroid/webkit/ValueCallback;)V
    .registers 8
    .parameter "stream"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/OutputStream;",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2390
    .local p2, callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/lang/Boolean;>;"
    new-instance v0, Landroid/webkit/WebViewCore$DrawData;

    #@2
    invoke-direct {v0}, Landroid/webkit/WebViewCore$DrawData;-><init>()V

    #@5
    .line 2392
    .local v0, draw:Landroid/webkit/WebViewCore$DrawData;
    iget v3, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@7
    iget-object v4, v0, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@9
    invoke-direct {p0, v3, v4}, Landroid/webkit/WebViewCore;->nativeRecordContent(ILandroid/graphics/Point;)I

    #@c
    move-result v3

    #@d
    iput v3, v0, Landroid/webkit/WebViewCore$DrawData;->mBaseLayer:I

    #@f
    .line 2393
    const/4 v1, 0x0

    #@10
    .line 2395
    .local v1, result:Z
    :try_start_10
    invoke-static {p1, v0}, Landroid/webkit/ViewStateSerializer;->serializeViewState(Ljava/io/OutputStream;Landroid/webkit/WebViewCore$DrawData;)Z
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_13} :catch_33

    #@13
    move-result v1

    #@14
    .line 2399
    :goto_14
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@17
    move-result-object v3

    #@18
    invoke-interface {p2, v3}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    #@1b
    .line 2400
    iget v3, v0, Landroid/webkit/WebViewCore$DrawData;->mBaseLayer:I

    #@1d
    if-eqz v3, :cond_32

    #@1f
    .line 2401
    iget-boolean v3, p0, Landroid/webkit/WebViewCore;->mDrawIsScheduled:Z

    #@21
    if-eqz v3, :cond_2d

    #@23
    .line 2402
    const/4 v3, 0x0

    #@24
    iput-boolean v3, p0, Landroid/webkit/WebViewCore;->mDrawIsScheduled:Z

    #@26
    .line 2403
    iget-object v3, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@28
    const/16 v4, 0x82

    #@2a
    invoke-static {v3, v4}, Landroid/webkit/WebViewCore$EventHub;->access$7900(Landroid/webkit/WebViewCore$EventHub;I)V

    #@2d
    .line 2405
    :cond_2d
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mLastDrawData:Landroid/webkit/WebViewCore$DrawData;

    #@2f
    .line 2406
    invoke-direct {p0, v0}, Landroid/webkit/WebViewCore;->webkitDraw(Landroid/webkit/WebViewCore$DrawData;)V

    #@32
    .line 2408
    :cond_32
    return-void

    #@33
    .line 2396
    :catch_33
    move-exception v2

    #@34
    .line 2397
    .local v2, t:Ljava/lang/Throwable;
    const-string/jumbo v3, "webcore"

    #@37
    const-string v4, "Failed to save view state"

    #@39
    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3c
    goto :goto_14
.end method

.method private saveWebArchive(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 4
    .parameter "filename"
    .parameter "autoname"

    #@0
    .prologue
    .line 2137
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/BrowserFrame;->saveWebArchive(Ljava/lang/String;Z)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private selectAt(II)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 3204
    return-void
.end method

.method private sendNotifyProgressFinished()V
    .registers 1

    #@0
    .prologue
    .line 2537
    invoke-virtual {p0}, Landroid/webkit/WebViewCore;->contentDraw()V

    #@3
    .line 2538
    return-void
.end method

.method private sendPluginDrawMsg()V
    .registers 2

    #@0
    .prologue
    .line 2574
    const/16 v0, 0xc3

    #@2
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewCore;->sendMessage(I)V

    #@5
    .line 2575
    return-void
.end method

.method static sendStaticMessage(ILjava/lang/Object;)V
    .registers 4
    .parameter "messageType"
    .parameter "argument"

    #@0
    .prologue
    .line 2427
    sget-object v0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 2431
    :goto_4
    return-void

    #@5
    .line 2430
    :cond_5
    sget-object v0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@7
    sget-object v1, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v1, p0, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@10
    goto :goto_4
.end method

.method private sendViewInvalidate(IIII)V
    .registers 8
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 2545
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-eqz v0, :cond_16

    #@4
    .line 2546
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@6
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@8
    const/16 v1, 0x75

    #@a
    new-instance v2, Landroid/graphics/Rect;

    #@c
    invoke-direct {v2, p1, p2, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    #@f
    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@16
    .line 2550
    :cond_16
    return-void
.end method

.method private setScrollbarModes(II)V
    .registers 5
    .parameter "hMode"
    .parameter "vMode"

    #@0
    .prologue
    .line 3194
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 3199
    :goto_4
    return-void

    #@5
    .line 3197
    :cond_5
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0x81

    #@b
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@12
    goto :goto_4
.end method

.method static setShouldMonitorWebCoreThread()V
    .registers 1

    #@0
    .prologue
    .line 3249
    const/4 v0, 0x1

    #@1
    sput-boolean v0, Landroid/webkit/WebViewCore;->sShouldMonitorWebCoreThread:Z

    #@3
    .line 3250
    return-void
.end method

.method private setUseMockDeviceOrientation()V
    .registers 2

    #@0
    .prologue
    .line 3207
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mDeviceMotionAndOrientationManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@2
    invoke-virtual {v0}, Landroid/webkit/DeviceMotionAndOrientationManager;->setUseMock()V

    #@5
    .line 3208
    return-void
.end method

.method private setUseMockGeolocation()V
    .registers 2

    #@0
    .prologue
    .line 3211
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mMockGeolocation:Landroid/webkit/MockGeolocation;

    #@2
    invoke-virtual {v0}, Landroid/webkit/MockGeolocation;->setUseMock()V

    #@5
    .line 3212
    return-void
.end method

.method private native setViewportSettingsFromNative(I)V
.end method

.method private setWebTextViewAutoFillable(ILjava/lang/String;)V
    .registers 6
    .parameter "queryId"
    .parameter "preview"

    #@0
    .prologue
    .line 3038
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-eqz v0, :cond_16

    #@4
    .line 3039
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@6
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@8
    const/16 v1, 0x85

    #@a
    new-instance v2, Landroid/webkit/WebViewCore$AutoFillData;

    #@c
    invoke-direct {v2, p1, p2}, Landroid/webkit/WebViewCore$AutoFillData;-><init>(ILjava/lang/String;)V

    #@f
    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@16
    .line 3043
    :cond_16
    return-void
.end method

.method private setupViewport(Z)V
    .registers 15
    .parameter "updateViewState"

    #@0
    .prologue
    .line 2622
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-eqz v9, :cond_8

    #@4
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@6
    if-nez v9, :cond_9

    #@8
    .line 2892
    :cond_8
    :goto_8
    return-void

    #@9
    .line 2627
    :cond_9
    iget v9, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@b
    invoke-direct {p0, v9}, Landroid/webkit/WebViewCore;->setViewportSettingsFromNative(I)V

    #@e
    .line 2630
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@10
    if-lez v9, :cond_2e

    #@12
    .line 2631
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@14
    if-lez v9, :cond_20

    #@16
    .line 2632
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@18
    iget v10, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@1a
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    #@1d
    move-result v9

    #@1e
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@20
    .line 2635
    :cond_20
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@22
    if-lez v9, :cond_2e

    #@24
    .line 2636
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@26
    iget v10, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@28
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    #@2b
    move-result v9

    #@2c
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@2e
    .line 2641
    :cond_2e
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@30
    invoke-virtual {v9}, Landroid/webkit/WebSettingsClassic;->forceUserScalable()Z

    #@33
    move-result v9

    #@34
    if-eqz v9, :cond_5d

    #@36
    .line 2642
    const/4 v9, 0x1

    #@37
    iput-boolean v9, p0, Landroid/webkit/WebViewCore;->mViewportUserScalable:Z

    #@39
    .line 2643
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@3b
    if-lez v9, :cond_127

    #@3d
    .line 2644
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@3f
    if-lez v9, :cond_4d

    #@41
    .line 2645
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@43
    iget v10, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@45
    div-int/lit8 v10, v10, 0x2

    #@47
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    #@4a
    move-result v9

    #@4b
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@4d
    .line 2648
    :cond_4d
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@4f
    if-lez v9, :cond_5d

    #@51
    .line 2649
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@53
    iget v10, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@55
    mul-int/lit8 v10, v10, 0x2

    #@57
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    #@5a
    move-result v9

    #@5b
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@5d
    .line 2665
    :cond_5d
    :goto_5d
    const/high16 v0, 0x3f80

    #@5f
    .line 2666
    .local v0, adjust:F
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->inBrowserApp()Z

    #@62
    move-result v9

    #@63
    if-eqz v9, :cond_145

    #@65
    .line 2667
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@67
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->getDefaultZoomScale()F

    #@6a
    move-result v0

    #@6b
    .line 2681
    :cond_6b
    :goto_6b
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@6d
    iget-object v9, v9, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@6f
    const/16 v10, 0x8b

    #@71
    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeMessages(I)V

    #@74
    .line 2683
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@76
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->getDefaultZoomScale()F

    #@79
    move-result v9

    #@7a
    cmpl-float v9, v0, v9

    #@7c
    if-eqz v9, :cond_8f

    #@7e
    .line 2684
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@80
    iget-object v9, v9, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@82
    const/16 v10, 0x8b

    #@84
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@87
    move-result-object v11

    #@88
    invoke-static {v9, v10, v11}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@8b
    move-result-object v9

    #@8c
    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    #@8f
    .line 2687
    :cond_8f
    const/high16 v9, 0x42c8

    #@91
    mul-float/2addr v9, v0

    #@92
    float-to-int v2, v9

    #@93
    .line 2689
    .local v2, defaultScale:I
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@95
    if-lez v9, :cond_9e

    #@97
    .line 2690
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@99
    int-to-float v9, v9

    #@9a
    mul-float/2addr v9, v0

    #@9b
    float-to-int v9, v9

    #@9c
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@9e
    .line 2692
    :cond_9e
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@a0
    if-lez v9, :cond_a9

    #@a2
    .line 2693
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@a4
    int-to-float v9, v9

    #@a5
    mul-float/2addr v9, v0

    #@a6
    float-to-int v9, v9

    #@a7
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@a9
    .line 2695
    :cond_a9
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@ab
    if-lez v9, :cond_b4

    #@ad
    .line 2696
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@af
    int-to-float v9, v9

    #@b0
    mul-float/2addr v9, v0

    #@b1
    float-to-int v9, v9

    #@b2
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@b4
    .line 2700
    :cond_b4
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@b6
    if-nez v9, :cond_be

    #@b8
    .line 2701
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@ba
    if-nez v9, :cond_be

    #@bc
    .line 2702
    iput v2, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@be
    .line 2705
    :cond_be
    iget-boolean v9, p0, Landroid/webkit/WebViewCore;->mViewportUserScalable:Z

    #@c0
    if-nez v9, :cond_c8

    #@c2
    .line 2706
    iput v2, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@c4
    .line 2707
    iput v2, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@c6
    .line 2708
    iput v2, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@c8
    .line 2710
    :cond_c8
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@ca
    iget v10, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@cc
    if-le v9, v10, :cond_d6

    #@ce
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@d0
    if-eqz v9, :cond_d6

    #@d2
    .line 2712
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@d4
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@d6
    .line 2714
    :cond_d6
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@d8
    if-lez v9, :cond_e4

    #@da
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@dc
    iget v10, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@de
    if-ge v9, v10, :cond_e4

    #@e0
    .line 2716
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@e2
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@e4
    .line 2718
    :cond_e4
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@e6
    if-gez v9, :cond_ef

    #@e8
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@ea
    if-ne v9, v2, :cond_ef

    #@ec
    .line 2719
    const/4 v9, 0x0

    #@ed
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@ef
    .line 2723
    :cond_ef
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@f1
    if-eqz v9, :cond_173

    #@f3
    if-nez p1, :cond_173

    #@f5
    .line 2725
    const/4 v9, 0x1

    #@f6
    iput-boolean v9, p0, Landroid/webkit/WebViewCore;->mFirstLayoutForNonStandardLoad:Z

    #@f8
    .line 2726
    new-instance v5, Landroid/webkit/WebViewCore$ViewState;

    #@fa
    invoke-direct {v5}, Landroid/webkit/WebViewCore$ViewState;-><init>()V

    #@fd
    .line 2727
    .local v5, viewState:Landroid/webkit/WebViewCore$ViewState;
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@ff
    int-to-float v9, v9

    #@100
    const/high16 v10, 0x42c8

    #@102
    div-float/2addr v9, v10

    #@103
    iput v9, v5, Landroid/webkit/WebViewCore$ViewState;->mMinScale:F

    #@105
    .line 2728
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@107
    int-to-float v9, v9

    #@108
    const/high16 v10, 0x42c8

    #@10a
    div-float/2addr v9, v10

    #@10b
    iput v9, v5, Landroid/webkit/WebViewCore$ViewState;->mMaxScale:F

    #@10d
    .line 2729
    iput v0, v5, Landroid/webkit/WebViewCore$ViewState;->mDefaultScale:F

    #@10f
    .line 2731
    const/4 v9, 0x0

    #@110
    iput-boolean v9, v5, Landroid/webkit/WebViewCore$ViewState;->mMobileSite:Z

    #@112
    .line 2733
    const/4 v9, 0x0

    #@113
    iput v9, v5, Landroid/webkit/WebViewCore$ViewState;->mScrollX:I

    #@115
    .line 2734
    const/4 v9, 0x0

    #@116
    iput-boolean v9, v5, Landroid/webkit/WebViewCore$ViewState;->mShouldStartScrolledRight:Z

    #@118
    .line 2735
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@11a
    iget-object v9, v9, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@11c
    const/16 v10, 0x6d

    #@11e
    invoke-static {v9, v10, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@121
    move-result-object v9

    #@122
    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    #@125
    goto/16 :goto_8

    #@127
    .line 2653
    .end local v0           #adjust:F
    .end local v2           #defaultScale:I
    .end local v5           #viewState:Landroid/webkit/WebViewCore$ViewState;
    :cond_127
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@129
    if-lez v9, :cond_135

    #@12b
    .line 2654
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@12d
    const/16 v10, 0x32

    #@12f
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    #@132
    move-result v9

    #@133
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@135
    .line 2656
    :cond_135
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@137
    if-lez v9, :cond_5d

    #@139
    .line 2657
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@13b
    const/16 v10, 0xc8

    #@13d
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    #@140
    move-result v9

    #@141
    iput v9, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@143
    goto/16 :goto_5d

    #@145
    .line 2669
    .restart local v0       #adjust:F
    :cond_145
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportDensityDpi:I

    #@147
    const/4 v10, -0x1

    #@148
    if-ne v9, v10, :cond_152

    #@14a
    .line 2670
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@14c
    invoke-static {v9}, Landroid/webkit/WebViewCore;->getFixedDisplayDensity(Landroid/content/Context;)F

    #@14f
    move-result v0

    #@150
    goto/16 :goto_6b

    #@152
    .line 2671
    :cond_152
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportDensityDpi:I

    #@154
    if-lez v9, :cond_6b

    #@156
    .line 2672
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@158
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@15b
    move-result-object v9

    #@15c
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@15f
    move-result-object v9

    #@160
    iget v9, v9, Landroid/util/DisplayMetrics;->densityDpi:I

    #@162
    int-to-float v9, v9

    #@163
    iget v10, p0, Landroid/webkit/WebViewCore;->mViewportDensityDpi:I

    #@165
    int-to-float v10, v10

    #@166
    div-float v0, v9, v10

    #@168
    .line 2674
    const/high16 v9, 0x42c8

    #@16a
    mul-float/2addr v9, v0

    #@16b
    float-to-int v9, v9

    #@16c
    int-to-float v9, v9

    #@16d
    const/high16 v10, 0x42c8

    #@16f
    div-float v0, v9, v10

    #@171
    goto/16 :goto_6b

    #@173
    .line 2744
    .restart local v2       #defaultScale:I
    :cond_173
    iget v6, p0, Landroid/webkit/WebViewCore;->mCurrentViewWidth:I

    #@175
    .line 2745
    .local v6, viewportWidth:I
    if-nez v6, :cond_229

    #@177
    .line 2749
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@179
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@17c
    move-result v7

    #@17d
    .line 2750
    .local v7, webViewWidth:I
    int-to-float v9, v7

    #@17e
    div-float/2addr v9, v0

    #@17f
    float-to-int v6, v9

    #@180
    .line 2751
    if-nez v6, :cond_182

    #@182
    .line 2759
    :cond_182
    :goto_182
    new-instance v9, Landroid/webkit/WebViewCore$ViewState;

    #@184
    invoke-direct {v9}, Landroid/webkit/WebViewCore$ViewState;-><init>()V

    #@187
    iput-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@189
    .line 2760
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@18b
    iget v10, p0, Landroid/webkit/WebViewCore;->mViewportMinimumScale:I

    #@18d
    int-to-float v10, v10

    #@18e
    const/high16 v11, 0x42c8

    #@190
    div-float/2addr v10, v11

    #@191
    iput v10, v9, Landroid/webkit/WebViewCore$ViewState;->mMinScale:F

    #@193
    .line 2761
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@195
    iget v10, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@197
    int-to-float v10, v10

    #@198
    const/high16 v11, 0x42c8

    #@19a
    div-float/2addr v10, v11

    #@19b
    iput v10, v9, Landroid/webkit/WebViewCore$ViewState;->mMaxScale:F

    #@19d
    .line 2762
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@19f
    iput v0, v9, Landroid/webkit/WebViewCore$ViewState;->mDefaultScale:F

    #@1a1
    .line 2763
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@1a3
    iget v10, p0, Landroid/webkit/WebViewCore;->mRestoredX:I

    #@1a5
    iput v10, v9, Landroid/webkit/WebViewCore$ViewState;->mScrollX:I

    #@1a7
    .line 2764
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@1a9
    iget v10, p0, Landroid/webkit/WebViewCore;->mRestoredY:I

    #@1ab
    iput v10, v9, Landroid/webkit/WebViewCore$ViewState;->mScrollY:I

    #@1ad
    .line 2765
    iget-object v10, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@1af
    iget v9, p0, Landroid/webkit/WebViewCore;->mRestoredX:I

    #@1b1
    if-nez v9, :cond_233

    #@1b3
    iget v9, p0, Landroid/webkit/WebViewCore;->mRestoredY:I

    #@1b5
    if-nez v9, :cond_233

    #@1b7
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@1b9
    if-eqz v9, :cond_233

    #@1bb
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@1bd
    invoke-virtual {v9}, Landroid/webkit/BrowserFrame;->getShouldStartScrolledRight()Z

    #@1c0
    move-result v9

    #@1c1
    if-eqz v9, :cond_233

    #@1c3
    const/4 v9, 0x1

    #@1c4
    :goto_1c4
    iput-boolean v9, v10, Landroid/webkit/WebViewCore$ViewState;->mShouldStartScrolledRight:Z

    #@1c6
    .line 2770
    iget-object v10, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@1c8
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@1ca
    if-nez v9, :cond_235

    #@1cc
    const/4 v9, 0x1

    #@1cd
    :goto_1cd
    iput-boolean v9, v10, Landroid/webkit/WebViewCore$ViewState;->mMobileSite:Z

    #@1cf
    .line 2771
    iget-boolean v9, p0, Landroid/webkit/WebViewCore;->mIsRestored:Z

    #@1d1
    if-eqz v9, :cond_240

    #@1d3
    .line 2772
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@1d5
    const/4 v10, 0x1

    #@1d6
    iput-boolean v10, v9, Landroid/webkit/WebViewCore$ViewState;->mIsRestored:Z

    #@1d8
    .line 2773
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@1da
    iget v10, p0, Landroid/webkit/WebViewCore;->mRestoredScale:F

    #@1dc
    iput v10, v9, Landroid/webkit/WebViewCore$ViewState;->mViewScale:F

    #@1de
    .line 2774
    iget v9, p0, Landroid/webkit/WebViewCore;->mRestoredTextWrapScale:F

    #@1e0
    const/4 v10, 0x0

    #@1e1
    cmpl-float v9, v9, v10

    #@1e3
    if-lez v9, :cond_237

    #@1e5
    .line 2775
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@1e7
    iget v10, p0, Landroid/webkit/WebViewCore;->mRestoredTextWrapScale:F

    #@1e9
    iput v10, v9, Landroid/webkit/WebViewCore$ViewState;->mTextWrapScale:F

    #@1eb
    .line 2798
    :goto_1eb
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@1ed
    iget-boolean v9, v9, Landroid/webkit/WebViewClassic;->mHeightCanMeasure:Z

    #@1ef
    if-eqz v9, :cond_28d

    #@1f1
    .line 2804
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@1f3
    const/4 v10, 0x0

    #@1f4
    iput v10, v9, Landroid/webkit/WebViewClassic;->mLastHeightSent:I

    #@1f6
    .line 2807
    new-instance v1, Landroid/webkit/WebViewClassic$ViewSizeData;

    #@1f8
    invoke-direct {v1}, Landroid/webkit/WebViewClassic$ViewSizeData;-><init>()V

    #@1fb
    .line 2808
    .local v1, data:Landroid/webkit/WebViewClassic$ViewSizeData;
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@1fd
    iget v9, v9, Landroid/webkit/WebViewClassic;->mLastWidthSent:I

    #@1ff
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mWidth:I

    #@201
    .line 2809
    const/4 v9, 0x0

    #@202
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mHeight:I

    #@204
    .line 2812
    iget v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mWidth:I

    #@206
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mTextWrapWidth:I

    #@208
    .line 2813
    const/high16 v9, -0x4080

    #@20a
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mScale:F

    #@20c
    .line 2814
    const/4 v9, 0x0

    #@20d
    iput-boolean v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mIgnoreHeight:Z

    #@20f
    .line 2815
    const/4 v9, 0x0

    #@210
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mAnchorY:I

    #@212
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mAnchorX:I

    #@214
    .line 2822
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@216
    const/16 v10, 0x69

    #@218
    invoke-static {v9, v10}, Landroid/webkit/WebViewCore$EventHub;->access$7900(Landroid/webkit/WebViewCore$EventHub;I)V

    #@21b
    .line 2823
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@21d
    const/4 v10, 0x0

    #@21e
    const/16 v11, 0x69

    #@220
    invoke-static {v10, v11, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@223
    move-result-object v10

    #@224
    invoke-static {v9, v10}, Landroid/webkit/WebViewCore$EventHub;->access$7700(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@227
    goto/16 :goto_8

    #@229
    .line 2757
    .end local v1           #data:Landroid/webkit/WebViewClassic$ViewSizeData;
    .end local v7           #webViewWidth:I
    :cond_229
    int-to-float v9, v6

    #@22a
    iget v10, p0, Landroid/webkit/WebViewCore;->mCurrentViewScale:F

    #@22c
    mul-float/2addr v9, v10

    #@22d
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@230
    move-result v7

    #@231
    .restart local v7       #webViewWidth:I
    goto/16 :goto_182

    #@233
    .line 2765
    :cond_233
    const/4 v9, 0x0

    #@234
    goto :goto_1c4

    #@235
    .line 2770
    :cond_235
    const/4 v9, 0x0

    #@236
    goto :goto_1cd

    #@237
    .line 2777
    :cond_237
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@239
    iget-object v10, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@23b
    iget v10, v10, Landroid/webkit/WebViewCore$ViewState;->mViewScale:F

    #@23d
    iput v10, v9, Landroid/webkit/WebViewCore$ViewState;->mTextWrapScale:F

    #@23f
    goto :goto_1eb

    #@240
    .line 2780
    :cond_240
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@242
    if-lez v9, :cond_253

    #@244
    .line 2781
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@246
    iget-object v10, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@248
    iget v11, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@24a
    int-to-float v11, v11

    #@24b
    const/high16 v12, 0x42c8

    #@24d
    div-float/2addr v11, v12

    #@24e
    iput v11, v10, Landroid/webkit/WebViewCore$ViewState;->mTextWrapScale:F

    #@250
    iput v11, v9, Landroid/webkit/WebViewCore$ViewState;->mViewScale:F

    #@252
    goto :goto_1eb

    #@253
    .line 2783
    :cond_253
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@255
    if-lez v9, :cond_274

    #@257
    iget v9, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@259
    if-ge v9, v7, :cond_274

    #@25b
    invoke-virtual {p0}, Landroid/webkit/WebViewCore;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@25e
    move-result-object v9

    #@25f
    invoke-virtual {v9}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@262
    move-result v9

    #@263
    if-nez v9, :cond_274

    #@265
    .line 2785
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@267
    iget-object v10, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@269
    int-to-float v11, v7

    #@26a
    iget v12, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@26c
    int-to-float v12, v12

    #@26d
    div-float/2addr v11, v12

    #@26e
    iput v11, v10, Landroid/webkit/WebViewCore$ViewState;->mTextWrapScale:F

    #@270
    iput v11, v9, Landroid/webkit/WebViewCore$ViewState;->mViewScale:F

    #@272
    goto/16 :goto_1eb

    #@274
    .line 2788
    :cond_274
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@276
    iput v0, v9, Landroid/webkit/WebViewCore$ViewState;->mTextWrapScale:F

    #@278
    .line 2789
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@27a
    invoke-virtual {v9}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@27d
    move-result v9

    #@27e
    if-eqz v9, :cond_287

    #@280
    .line 2791
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@282
    const/4 v10, 0x0

    #@283
    iput v10, v9, Landroid/webkit/WebViewCore$ViewState;->mViewScale:F

    #@285
    goto/16 :goto_1eb

    #@287
    .line 2793
    :cond_287
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@289
    iput v0, v9, Landroid/webkit/WebViewCore$ViewState;->mViewScale:F

    #@28b
    goto/16 :goto_1eb

    #@28d
    .line 2826
    :cond_28d
    if-nez v6, :cond_296

    #@28f
    .line 2829
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@291
    const/4 v10, 0x0

    #@292
    iput v10, v9, Landroid/webkit/WebViewClassic;->mLastWidthSent:I

    #@294
    goto/16 :goto_8

    #@296
    .line 2831
    :cond_296
    new-instance v1, Landroid/webkit/WebViewClassic$ViewSizeData;

    #@298
    invoke-direct {v1}, Landroid/webkit/WebViewClassic$ViewSizeData;-><init>()V

    #@29b
    .line 2835
    .restart local v1       #data:Landroid/webkit/WebViewClassic$ViewSizeData;
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@29d
    iget v3, v9, Landroid/webkit/WebViewCore$ViewState;->mViewScale:F

    #@29f
    .line 2836
    .local v3, tentativeScale:F
    const/4 v9, 0x0

    #@2a0
    cmpl-float v9, v3, v9

    #@2a2
    if-nez v9, :cond_319

    #@2a4
    .line 2843
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@2a6
    iget v3, v9, Landroid/webkit/WebViewCore$ViewState;->mTextWrapScale:F

    #@2a8
    .line 2844
    int-to-float v9, v7

    #@2a9
    div-float/2addr v9, v3

    #@2aa
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@2ad
    move-result v4

    #@2ae
    .line 2845
    .local v4, tentativeViewWidth:I
    invoke-direct {p0, v4}, Landroid/webkit/WebViewCore;->calculateWindowWidth(I)I

    #@2b1
    move-result v8

    #@2b2
    .line 2849
    .local v8, windowWidth:I
    int-to-float v9, v7

    #@2b3
    int-to-float v10, v8

    #@2b4
    div-float/2addr v9, v10

    #@2b5
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mScale:F

    #@2b7
    .line 2850
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@2b9
    invoke-virtual {v9}, Landroid/webkit/WebSettingsClassic;->getLoadWithOverviewMode()Z

    #@2bc
    move-result v9

    #@2bd
    if-nez v9, :cond_2c7

    #@2bf
    .line 2852
    iget v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mScale:F

    #@2c1
    invoke-static {v9, v3}, Ljava/lang/Math;->max(FF)F

    #@2c4
    move-result v9

    #@2c5
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mScale:F

    #@2c7
    .line 2854
    :cond_2c7
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@2c9
    invoke-virtual {v9}, Landroid/webkit/WebSettingsClassic;->isNarrowColumnLayout()Z

    #@2cc
    move-result v9

    #@2cd
    if-eqz v9, :cond_2db

    #@2cf
    .line 2856
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@2d1
    iget-object v10, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2d3
    iget v11, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mScale:F

    #@2d5
    invoke-virtual {v10, v11}, Landroid/webkit/WebViewClassic;->computeReadingLevelScale(F)F

    #@2d8
    move-result v10

    #@2d9
    iput v10, v9, Landroid/webkit/WebViewCore$ViewState;->mTextWrapScale:F

    #@2db
    .line 2871
    .end local v4           #tentativeViewWidth:I
    .end local v8           #windowWidth:I
    :cond_2db
    :goto_2db
    int-to-float v9, v7

    #@2dc
    iget v10, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mScale:F

    #@2de
    div-float/2addr v9, v10

    #@2df
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@2e2
    move-result v9

    #@2e3
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mWidth:I

    #@2e5
    .line 2877
    iget v9, p0, Landroid/webkit/WebViewCore;->mCurrentViewHeight:I

    #@2e7
    if-nez v9, :cond_31c

    #@2e9
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2eb
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->getViewHeight()I

    #@2ee
    move-result v9

    #@2ef
    int-to-float v9, v9

    #@2f0
    iget v10, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mScale:F

    #@2f2
    div-float/2addr v9, v10

    #@2f3
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@2f6
    move-result v9

    #@2f7
    :goto_2f7
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mHeight:I

    #@2f9
    .line 2880
    int-to-float v9, v7

    #@2fa
    iget-object v10, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@2fc
    iget v10, v10, Landroid/webkit/WebViewCore$ViewState;->mTextWrapScale:F

    #@2fe
    div-float/2addr v9, v10

    #@2ff
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@302
    move-result v9

    #@303
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mTextWrapWidth:I

    #@305
    .line 2882
    const/4 v9, 0x0

    #@306
    iput-boolean v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mIgnoreHeight:Z

    #@308
    .line 2883
    const/4 v9, 0x0

    #@309
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mAnchorY:I

    #@30b
    iput v9, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mAnchorX:I

    #@30d
    .line 2886
    iget-object v9, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@30f
    const/16 v10, 0x69

    #@311
    invoke-static {v9, v10}, Landroid/webkit/WebViewCore$EventHub;->access$7900(Landroid/webkit/WebViewCore$EventHub;I)V

    #@314
    .line 2889
    invoke-direct {p0, v1}, Landroid/webkit/WebViewCore;->viewSizeChanged(Landroid/webkit/WebViewClassic$ViewSizeData;)V

    #@317
    goto/16 :goto_8

    #@319
    .line 2861
    :cond_319
    iput v3, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mScale:F

    #@31b
    goto :goto_2db

    #@31c
    .line 2877
    :cond_31c
    iget v9, p0, Landroid/webkit/WebViewCore;->mCurrentViewHeight:I

    #@31e
    int-to-float v9, v9

    #@31f
    iget v10, v1, Landroid/webkit/WebViewClassic$ViewSizeData;->mWidth:I

    #@321
    int-to-float v10, v10

    #@322
    mul-float/2addr v9, v10

    #@323
    int-to-float v10, v6

    #@324
    div-float/2addr v9, v10

    #@325
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@328
    move-result v9

    #@329
    goto :goto_2f7
.end method

.method private showFullScreenPlugin(Landroid/webkit/ViewManager$ChildView;II)V
    .registers 7
    .parameter "childView"
    .parameter "orientation"
    .parameter "npp"

    #@0
    .prologue
    .line 3089
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 3099
    :goto_4
    return-void

    #@5
    .line 3093
    :cond_5
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v2, 0x78

    #@b
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    .line 3095
    .local v0, message:Landroid/os/Message;
    iget-object v1, p1, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@11
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    .line 3096
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@15
    .line 3097
    iput p3, v0, Landroid/os/Message;->arg2:I

    #@17
    .line 3098
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@1a
    goto :goto_4
.end method

.method private showRect(IIIIIIFFFF)V
    .registers 14
    .parameter "left"
    .parameter "top"
    .parameter "width"
    .parameter "height"
    .parameter "contentWidth"
    .parameter "contentHeight"
    .parameter "xPercentInDoc"
    .parameter "xPercentInView"
    .parameter "yPercentInDoc"
    .parameter "yPercentInView"

    #@0
    .prologue
    .line 3166
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-eqz v1, :cond_2a

    #@4
    .line 3167
    new-instance v0, Landroid/webkit/WebViewCore$ShowRectData;

    #@6
    invoke-direct {v0}, Landroid/webkit/WebViewCore$ShowRectData;-><init>()V

    #@9
    .line 3168
    .local v0, data:Landroid/webkit/WebViewCore$ShowRectData;
    iput p1, v0, Landroid/webkit/WebViewCore$ShowRectData;->mLeft:I

    #@b
    .line 3169
    iput p2, v0, Landroid/webkit/WebViewCore$ShowRectData;->mTop:I

    #@d
    .line 3170
    iput p3, v0, Landroid/webkit/WebViewCore$ShowRectData;->mWidth:I

    #@f
    .line 3171
    iput p4, v0, Landroid/webkit/WebViewCore$ShowRectData;->mHeight:I

    #@11
    .line 3172
    iput p5, v0, Landroid/webkit/WebViewCore$ShowRectData;->mContentWidth:I

    #@13
    .line 3173
    iput p6, v0, Landroid/webkit/WebViewCore$ShowRectData;->mContentHeight:I

    #@15
    .line 3174
    iput p7, v0, Landroid/webkit/WebViewCore$ShowRectData;->mXPercentInDoc:F

    #@17
    .line 3175
    iput p8, v0, Landroid/webkit/WebViewCore$ShowRectData;->mXPercentInView:F

    #@19
    .line 3176
    iput p9, v0, Landroid/webkit/WebViewCore$ShowRectData;->mYPercentInDoc:F

    #@1b
    .line 3177
    iput p10, v0, Landroid/webkit/WebViewCore$ShowRectData;->mYPercentInView:F

    #@1d
    .line 3178
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@1f
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@21
    const/16 v2, 0x71

    #@23
    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@2a
    .line 3181
    .end local v0           #data:Landroid/webkit/WebViewCore$ShowRectData;
    :cond_2a
    return-void
.end method

.method private updateContentBounds(IIII)V
    .registers 8
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 3341
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 3346
    :goto_4
    return-void

    #@5
    .line 3344
    :cond_5
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v1, 0x98

    #@b
    new-instance v2, Landroid/graphics/Rect;

    #@d
    invoke-direct {v2, p1, p2, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@17
    goto :goto_4
.end method

.method private updateSurface(Landroid/webkit/ViewManager$ChildView;IIII)V
    .registers 6
    .parameter "childView"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 3142
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/webkit/ViewManager$ChildView;->attachView(IIII)V

    #@3
    .line 3143
    return-void
.end method

.method private updateTextSelection(IIIIILjava/lang/String;)V
    .registers 10
    .parameter "pointer"
    .parameter "start"
    .parameter "end"
    .parameter "textGeneration"
    .parameter "selectionPtr"
    .parameter "text"

    #@0
    .prologue
    .line 2951
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-eqz v0, :cond_15

    #@4
    .line 2952
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@6
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@8
    const/16 v1, 0x70

    #@a
    invoke-direct {p0, p2, p3, p5, p6}, Landroid/webkit/WebViewCore;->createTextSelection(IIILjava/lang/String;)Landroid/webkit/WebViewCore$TextSelectionData;

    #@d
    move-result-object v2

    #@e
    invoke-static {v0, v1, p1, p4, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@15
    .line 2956
    :cond_15
    return-void
.end method

.method private updateTextSizeAndScroll(IIIII)V
    .registers 11
    .parameter "pointer"
    .parameter "width"
    .parameter "height"
    .parameter "scrollX"
    .parameter "scrollY"

    #@0
    .prologue
    .line 2962
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-eqz v1, :cond_1d

    #@4
    .line 2963
    new-instance v0, Landroid/graphics/Rect;

    #@6
    neg-int v1, p4

    #@7
    neg-int v2, p5

    #@8
    sub-int v3, p2, p4

    #@a
    sub-int v4, p3, p5

    #@c
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    #@f
    .line 2965
    .local v0, rect:Landroid/graphics/Rect;
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@11
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@13
    const/16 v2, 0x96

    #@15
    const/4 v3, 0x0

    #@16
    invoke-static {v1, v2, p1, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@1d
    .line 2969
    .end local v0           #rect:Landroid/graphics/Rect;
    :cond_1d
    return-void
.end method

.method private updateTextfield(ILjava/lang/String;I)V
    .registers 6
    .parameter "ptr"
    .parameter "text"
    .parameter "textGeneration"

    #@0
    .prologue
    .line 2916
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 2917
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@6
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@8
    const/16 v1, 0x6c

    #@a
    invoke-static {v0, v1, p1, p3, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@11
    .line 2921
    :cond_11
    return-void
.end method

.method private updateViewport()V
    .registers 2

    #@0
    .prologue
    .line 2611
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/webkit/WebViewCore;->setupViewport(Z)V

    #@4
    .line 2612
    return-void
.end method

.method private viewSizeChanged(Landroid/webkit/WebViewClassic$ViewSizeData;)V
    .registers 18
    .parameter "data"

    #@0
    .prologue
    .line 2191
    move-object/from16 v0, p1

    #@2
    iget v7, v0, Landroid/webkit/WebViewClassic$ViewSizeData;->mWidth:I

    #@4
    .line 2192
    .local v7, w:I
    move-object/from16 v0, p1

    #@6
    iget v12, v0, Landroid/webkit/WebViewClassic$ViewSizeData;->mHeight:I

    #@8
    .line 2193
    .local v12, h:I
    move-object/from16 v0, p1

    #@a
    iget v5, v0, Landroid/webkit/WebViewClassic$ViewSizeData;->mTextWrapWidth:I

    #@c
    .line 2194
    .local v5, textwrapWidth:I
    move-object/from16 v0, p1

    #@e
    iget v6, v0, Landroid/webkit/WebViewClassic$ViewSizeData;->mScale:F

    #@10
    .line 2199
    .local v6, scale:F
    if-nez v7, :cond_1c

    #@12
    .line 2200
    const-string/jumbo v1, "webcore"

    #@15
    const-string/jumbo v2, "skip viewSizeChanged as w is 0"

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 2229
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 2203
    :cond_1c
    move-object/from16 v0, p0

    #@1e
    invoke-direct {v0, v7}, Landroid/webkit/WebViewCore;->calculateWindowWidth(I)I

    #@21
    move-result v3

    #@22
    .line 2204
    .local v3, width:I
    move v4, v12

    #@23
    .line 2205
    .local v4, height:I
    if-eq v3, v7, :cond_35

    #@25
    .line 2206
    move-object/from16 v0, p1

    #@27
    iget v13, v0, Landroid/webkit/WebViewClassic$ViewSizeData;->mHeightWidthRatio:F

    #@29
    .line 2207
    .local v13, heightWidthRatio:F
    const/4 v1, 0x0

    #@2a
    cmpl-float v1, v13, v1

    #@2c
    if-lez v1, :cond_7c

    #@2e
    move v15, v13

    #@2f
    .line 2208
    .local v15, ratio:F
    :goto_2f
    int-to-float v1, v3

    #@30
    mul-float/2addr v1, v15

    #@31
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@34
    move-result v4

    #@35
    .line 2210
    .end local v13           #heightWidthRatio:F
    .end local v15           #ratio:F
    :cond_35
    move-object/from16 v0, p1

    #@37
    iget v1, v0, Landroid/webkit/WebViewClassic$ViewSizeData;->mActualViewHeight:I

    #@39
    if-lez v1, :cond_81

    #@3b
    move-object/from16 v0, p1

    #@3d
    iget v8, v0, Landroid/webkit/WebViewClassic$ViewSizeData;->mActualViewHeight:I

    #@3f
    .line 2211
    .local v8, screenHeight:I
    :goto_3f
    move-object/from16 v0, p0

    #@41
    iget v2, v0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@43
    move-object/from16 v0, p1

    #@45
    iget v9, v0, Landroid/webkit/WebViewClassic$ViewSizeData;->mAnchorX:I

    #@47
    move-object/from16 v0, p1

    #@49
    iget v10, v0, Landroid/webkit/WebViewClassic$ViewSizeData;->mAnchorY:I

    #@4b
    move-object/from16 v0, p1

    #@4d
    iget-boolean v11, v0, Landroid/webkit/WebViewClassic$ViewSizeData;->mIgnoreHeight:Z

    #@4f
    move-object/from16 v1, p0

    #@51
    invoke-direct/range {v1 .. v11}, Landroid/webkit/WebViewCore;->nativeSetSize(IIIIFIIIIZ)V

    #@54
    .line 2214
    move-object/from16 v0, p0

    #@56
    iget-object v1, v0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@58
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->updateTextSelectionNeeded()Z

    #@5b
    move-result v1

    #@5c
    if-eqz v1, :cond_63

    #@5e
    .line 2215
    const/4 v1, 0x1

    #@5f
    move-object/from16 v0, p0

    #@61
    iput-boolean v1, v0, Landroid/webkit/WebViewCore;->mUpdateTextSelectionNeeded:Z

    #@63
    .line 2219
    :cond_63
    move-object/from16 v0, p0

    #@65
    iget v1, v0, Landroid/webkit/WebViewCore;->mCurrentViewWidth:I

    #@67
    if-nez v1, :cond_83

    #@69
    const/4 v14, 0x1

    #@6a
    .line 2220
    .local v14, needInvalidate:Z
    :goto_6a
    move-object/from16 v0, p0

    #@6c
    iput v7, v0, Landroid/webkit/WebViewCore;->mCurrentViewWidth:I

    #@6e
    .line 2221
    move-object/from16 v0, p0

    #@70
    iput v12, v0, Landroid/webkit/WebViewCore;->mCurrentViewHeight:I

    #@72
    .line 2222
    move-object/from16 v0, p0

    #@74
    iput v6, v0, Landroid/webkit/WebViewCore;->mCurrentViewScale:F

    #@76
    .line 2223
    if-eqz v14, :cond_1b

    #@78
    .line 2227
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/WebViewCore;->contentDraw()V

    #@7b
    goto :goto_1b

    #@7c
    .line 2207
    .end local v8           #screenHeight:I
    .end local v14           #needInvalidate:Z
    .restart local v13       #heightWidthRatio:F
    :cond_7c
    int-to-float v1, v12

    #@7d
    int-to-float v2, v7

    #@7e
    div-float v15, v1, v2

    #@80
    goto :goto_2f

    #@81
    .end local v13           #heightWidthRatio:F
    :cond_81
    move v8, v12

    #@82
    .line 2210
    goto :goto_3f

    #@83
    .line 2219
    .restart local v8       #screenHeight:I
    :cond_83
    const/4 v14, 0x0

    #@84
    goto :goto_6a
.end method

.method private webkitDraw()V
    .registers 6

    #@0
    .prologue
    .line 2331
    iget-object v2, p0, Landroid/webkit/WebViewCore;->m_skipDrawFlagLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 2332
    :try_start_3
    iget-boolean v1, p0, Landroid/webkit/WebViewCore;->m_skipDrawFlag:Z

    #@5
    if-eqz v1, :cond_c

    #@7
    .line 2333
    const/4 v1, 0x1

    #@8
    iput-boolean v1, p0, Landroid/webkit/WebViewCore;->m_drawWasSkipped:Z

    #@a
    .line 2334
    monitor-exit v2

    #@b
    .line 2353
    :cond_b
    :goto_b
    return-void

    #@c
    .line 2336
    :cond_c
    monitor-exit v2
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_3e

    #@d
    .line 2338
    const/4 v1, 0x0

    #@e
    iput-boolean v1, p0, Landroid/webkit/WebViewCore;->mDrawIsScheduled:Z

    #@10
    .line 2339
    new-instance v0, Landroid/webkit/WebViewCore$DrawData;

    #@12
    invoke-direct {v0}, Landroid/webkit/WebViewCore$DrawData;-><init>()V

    #@15
    .line 2341
    .local v0, draw:Landroid/webkit/WebViewCore$DrawData;
    iget v1, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@17
    iget-object v2, v0, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@19
    invoke-direct {p0, v1, v2}, Landroid/webkit/WebViewCore;->nativeRecordContent(ILandroid/graphics/Point;)I

    #@1c
    move-result v1

    #@1d
    iput v1, v0, Landroid/webkit/WebViewCore$DrawData;->mBaseLayer:I

    #@1f
    .line 2342
    iget v1, v0, Landroid/webkit/WebViewCore$DrawData;->mBaseLayer:I

    #@21
    if-nez v1, :cond_41

    #@23
    .line 2343
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@25
    if-eqz v1, :cond_b

    #@27
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@29
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->isPaused()Z

    #@2c
    move-result v1

    #@2d
    if-nez v1, :cond_b

    #@2f
    .line 2345
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@31
    const/4 v2, 0x0

    #@32
    const/16 v3, 0x82

    #@34
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@37
    move-result-object v2

    #@38
    const-wide/16 v3, 0xa

    #@3a
    invoke-static {v1, v2, v3, v4}, Landroid/webkit/WebViewCore$EventHub;->access$7800(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;J)V

    #@3d
    goto :goto_b

    #@3e
    .line 2336
    .end local v0           #draw:Landroid/webkit/WebViewCore$DrawData;
    :catchall_3e
    move-exception v1

    #@3f
    :try_start_3f
    monitor-exit v2
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_3e

    #@40
    throw v1

    #@41
    .line 2351
    .restart local v0       #draw:Landroid/webkit/WebViewCore$DrawData;
    :cond_41
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mLastDrawData:Landroid/webkit/WebViewCore$DrawData;

    #@43
    .line 2352
    invoke-direct {p0, v0}, Landroid/webkit/WebViewCore;->webkitDraw(Landroid/webkit/WebViewCore$DrawData;)V

    #@46
    goto :goto_b
.end method

.method private webkitDraw(Landroid/webkit/WebViewCore$DrawData;)V
    .registers 6
    .parameter "draw"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2356
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@3
    if-eqz v0, :cond_5a

    #@5
    .line 2357
    new-instance v0, Landroid/graphics/Point;

    #@7
    iget v1, p0, Landroid/webkit/WebViewCore;->mCurrentViewWidth:I

    #@9
    iget v2, p0, Landroid/webkit/WebViewCore;->mCurrentViewHeight:I

    #@b
    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    #@e
    iput-object v0, p1, Landroid/webkit/WebViewCore$DrawData;->mViewSize:Landroid/graphics/Point;

    #@10
    .line 2358
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@12
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_2b

    #@18
    .line 2359
    iget v0, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@1a
    const/4 v1, -0x1

    #@1b
    if-ne v0, v1, :cond_5b

    #@1d
    const/16 v0, 0x3d4

    #@1f
    :goto_1f
    iget v1, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@21
    invoke-direct {p0, v1}, Landroid/webkit/WebViewCore;->nativeGetContentMinPrefWidth(I)I

    #@24
    move-result v1

    #@25
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@28
    move-result v0

    #@29
    iput v0, p1, Landroid/webkit/WebViewCore$DrawData;->mMinPrefWidth:I

    #@2b
    .line 2365
    :cond_2b
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@2d
    if-eqz v0, :cond_36

    #@2f
    .line 2366
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@31
    iput-object v0, p1, Landroid/webkit/WebViewCore$DrawData;->mViewState:Landroid/webkit/WebViewCore$ViewState;

    #@33
    .line 2367
    const/4 v0, 0x0

    #@34
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mInitialViewState:Landroid/webkit/WebViewCore$ViewState;

    #@36
    .line 2369
    :cond_36
    iget-boolean v0, p0, Landroid/webkit/WebViewCore;->mFirstLayoutForNonStandardLoad:Z

    #@38
    if-eqz v0, :cond_3f

    #@3a
    .line 2370
    const/4 v0, 0x1

    #@3b
    iput-boolean v0, p1, Landroid/webkit/WebViewCore$DrawData;->mFirstLayoutForNonStandardLoad:Z

    #@3d
    .line 2371
    iput-boolean v3, p0, Landroid/webkit/WebViewCore;->mFirstLayoutForNonStandardLoad:Z

    #@3f
    .line 2374
    :cond_3f
    invoke-virtual {p0}, Landroid/webkit/WebViewCore;->pauseWebKitDraw()V

    #@42
    .line 2375
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@44
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@46
    const/16 v1, 0x69

    #@48
    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@4b
    move-result-object v0

    #@4c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@4f
    .line 2378
    iget-boolean v0, p0, Landroid/webkit/WebViewCore;->mUpdateTextSelectionNeeded:Z

    #@51
    if-eqz v0, :cond_5a

    #@53
    .line 2379
    iget v0, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@55
    invoke-direct {p0, v0}, Landroid/webkit/WebViewCore;->nativeUpdateTextSelection(I)V

    #@58
    .line 2380
    iput-boolean v3, p0, Landroid/webkit/WebViewCore;->mUpdateTextSelectionNeeded:Z

    #@5a
    .line 2384
    :cond_5a
    return-void

    #@5b
    .line 2359
    :cond_5b
    iget v0, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@5d
    if-nez v0, :cond_62

    #@5f
    iget v0, p0, Landroid/webkit/WebViewCore;->mCurrentViewWidth:I

    #@61
    goto :goto_1f

    #@62
    :cond_62
    iget v0, p0, Landroid/webkit/WebViewCore;->mViewportWidth:I

    #@64
    goto :goto_1f
.end method


# virtual methods
.method protected addMessageToConsole(Ljava/lang/String;ILjava/lang/String;I)V
    .registers 6
    .parameter "message"
    .parameter "lineNumber"
    .parameter "sourceID"
    .parameter "msgLevel"

    #@0
    .prologue
    .line 377
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/webkit/CallbackProxy;->addMessageToConsole(Ljava/lang/String;ILjava/lang/String;I)V

    #@5
    .line 378
    return-void
.end method

.method clearContent()V
    .registers 2

    #@0
    .prologue
    .line 627
    iget v0, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@2
    invoke-direct {p0, v0}, Landroid/webkit/WebViewCore;->nativeClearContent(I)V

    #@5
    .line 628
    return-void
.end method

.method contentDraw()V
    .registers 4

    #@0
    .prologue
    .line 2492
    monitor-enter p0

    #@1
    .line 2493
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@3
    if-eqz v0, :cond_9

    #@5
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@7
    if-nez v0, :cond_b

    #@9
    .line 2495
    :cond_9
    monitor-exit p0

    #@a
    .line 2507
    :goto_a
    return-void

    #@b
    .line 2499
    :cond_b
    iget v0, p0, Landroid/webkit/WebViewCore;->mCurrentViewWidth:I

    #@d
    if-eqz v0, :cond_17

    #@f
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@11
    invoke-virtual {v0}, Landroid/webkit/BrowserFrame;->firstLayoutDone()Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_1c

    #@17
    .line 2500
    :cond_17
    monitor-exit p0

    #@18
    goto :goto_a

    #@19
    .line 2506
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_19

    #@1b
    throw v0

    #@1c
    .line 2503
    :cond_1c
    :try_start_1c
    iget-boolean v0, p0, Landroid/webkit/WebViewCore;->mDrawIsScheduled:Z

    #@1e
    if-eqz v0, :cond_22

    #@20
    monitor-exit p0

    #@21
    goto :goto_a

    #@22
    .line 2504
    :cond_22
    const/4 v0, 0x1

    #@23
    iput-boolean v0, p0, Landroid/webkit/WebViewCore;->mDrawIsScheduled:Z

    #@25
    .line 2505
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@27
    const/4 v1, 0x0

    #@28
    const/16 v2, 0x82

    #@2a
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@2d
    move-result-object v1

    #@2e
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@31
    .line 2506
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_1c .. :try_end_32} :catchall_19

    #@32
    goto :goto_a
.end method

.method destroy()V
    .registers 5

    #@0
    .prologue
    .line 2093
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 2094
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@6
    invoke-virtual {v0}, Landroid/webkit/LGWebNotifications;->destroy()V

    #@9
    .line 2097
    :cond_9
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@b
    monitor-enter v1

    #@c
    .line 2100
    :try_start_c
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@e
    const/4 v2, 0x1

    #@f
    invoke-static {v0, v2}, Landroid/webkit/WebViewCore$EventHub;->access$1202(Landroid/webkit/WebViewCore$EventHub;Z)Z

    #@12
    .line 2101
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@14
    const/4 v2, 0x0

    #@15
    const/16 v3, 0xc8

    #@17
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v0, v2}, Landroid/webkit/WebViewCore$EventHub;->access$7700(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@1e
    .line 2103
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@20
    invoke-static {v0}, Landroid/webkit/WebViewCore$EventHub;->access$8100(Landroid/webkit/WebViewCore$EventHub;)V

    #@23
    .line 2104
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@25
    invoke-static {v0}, Landroid/webkit/WebCoreThreadWatchdog;->unregisterWebView(Landroid/webkit/WebViewClassic;)V

    #@28
    .line 2105
    monitor-exit v1

    #@29
    .line 2106
    return-void

    #@2a
    .line 2105
    :catchall_2a
    move-exception v0

    #@2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_c .. :try_end_2c} :catchall_2a

    #@2c
    throw v0
.end method

.method protected exceededDatabaseQuota(Ljava/lang/String;Ljava/lang/String;JJ)V
    .registers 17
    .parameter "url"
    .parameter "databaseIdentifier"
    .parameter "quota"
    .parameter "estimatedDatabaseSize"

    #@0
    .prologue
    .line 505
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->getUsedQuota()J

    #@5
    move-result-wide v7

    #@6
    new-instance v9, Landroid/webkit/WebViewCore$1;

    #@8
    invoke-direct {v9, p0}, Landroid/webkit/WebViewCore$1;-><init>(Landroid/webkit/WebViewCore;)V

    #@b
    move-object v1, p1

    #@c
    move-object v2, p2

    #@d
    move-wide v3, p3

    #@e
    move-wide v5, p5

    #@f
    invoke-virtual/range {v0 .. v9}, Landroid/webkit/CallbackProxy;->onExceededDatabaseQuota(Ljava/lang/String;Ljava/lang/String;JJJLandroid/webkit/WebStorage$QuotaUpdater;)V

    #@12
    .line 513
    return-void
.end method

.method protected exitFullscreenVideo()V
    .registers 4

    #@0
    .prologue
    .line 617
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 621
    :goto_4
    return-void

    #@5
    .line 618
    :cond_5
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@7
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@9
    const/16 v2, 0x8c

    #@b
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    .line 620
    .local v0, message:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@12
    goto :goto_4
.end method

.method protected geolocationPermissionsHidePrompt()V
    .registers 2

    #@0
    .prologue
    .line 568
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->onGeolocationPermissionsHidePrompt()V

    #@5
    .line 569
    return-void
.end method

.method protected geolocationPermissionsShowPrompt(Ljava/lang/String;)V
    .registers 4
    .parameter "origin"

    #@0
    .prologue
    .line 550
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    new-instance v1, Landroid/webkit/WebViewCore$4;

    #@4
    invoke-direct {v1, p0}, Landroid/webkit/WebViewCore$4;-><init>(Landroid/webkit/WebViewCore;)V

    #@7
    invoke-virtual {v0, p1, v1}, Landroid/webkit/CallbackProxy;->onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V

    #@a
    .line 562
    return-void
.end method

.method declared-synchronized getBrowserFrame()Landroid/webkit/BrowserFrame;
    .registers 2

    #@0
    .prologue
    .line 299
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 3046
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method protected getDeviceMotionService()Landroid/webkit/DeviceMotionService;
    .registers 4

    #@0
    .prologue
    .line 3233
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mDeviceMotionService:Landroid/webkit/DeviceMotionService;

    #@2
    if-nez v0, :cond_f

    #@4
    .line 3234
    new-instance v0, Landroid/webkit/DeviceMotionService;

    #@6
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mDeviceMotionAndOrientationManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@8
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@a
    invoke-direct {v0, v1, v2}, Landroid/webkit/DeviceMotionService;-><init>(Landroid/webkit/DeviceMotionAndOrientationManager;Landroid/content/Context;)V

    #@d
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mDeviceMotionService:Landroid/webkit/DeviceMotionService;

    #@f
    .line 3237
    :cond_f
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mDeviceMotionService:Landroid/webkit/DeviceMotionService;

    #@11
    return-object v0
.end method

.method protected getDeviceOrientationService()Landroid/webkit/DeviceOrientationService;
    .registers 4

    #@0
    .prologue
    .line 3241
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mDeviceOrientationService:Landroid/webkit/DeviceOrientationService;

    #@2
    if-nez v0, :cond_f

    #@4
    .line 3242
    new-instance v0, Landroid/webkit/DeviceOrientationService;

    #@6
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mDeviceMotionAndOrientationManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@8
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@a
    invoke-direct {v0, v1, v2}, Landroid/webkit/DeviceOrientationService;-><init>(Landroid/webkit/DeviceMotionAndOrientationManager;Landroid/content/Context;)V

    #@d
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mDeviceOrientationService:Landroid/webkit/DeviceOrientationService;

    #@f
    .line 3245
    :cond_f
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mDeviceOrientationService:Landroid/webkit/DeviceOrientationService;

    #@11
    return-object v0
.end method

.method public getInputDispatcherCallbacks()Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;
    .registers 2

    #@0
    .prologue
    .line 303
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    return-object v0
.end method

.method public getSettings()Landroid/webkit/WebSettingsClassic;
    .registers 2

    #@0
    .prologue
    .line 334
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@2
    return-object v0
.end method

.method protected getViewportUserScalable()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 346
    iget v1, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@3
    if-gtz v1, :cond_6

    #@5
    .line 348
    :cond_5
    :goto_5
    return v0

    #@6
    :cond_6
    iget v1, p0, Landroid/webkit/WebViewCore;->mViewportInitialScale:I

    #@8
    iget v2, p0, Landroid/webkit/WebViewCore;->mViewportMaximumScale:I

    #@a
    if-lt v1, v2, :cond_5

    #@c
    const/4 v0, 0x0

    #@d
    goto :goto_5
.end method

.method getWebViewClassic()Landroid/webkit/WebViewClassic;
    .registers 2

    #@0
    .prologue
    .line 2564
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    return-object v0
.end method

.method inParagraphMode()Z
    .registers 2

    #@0
    .prologue
    .line 3330
    iget v0, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@2
    invoke-direct {p0, v0}, Landroid/webkit/WebViewCore;->nativeInParagraphMode(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method initializeSubwindow()V
    .registers 3

    #@0
    .prologue
    .line 291
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->initialize()V

    #@3
    .line 293
    sget-object v0, Landroid/webkit/WebViewCore;->sWebCoreHandler:Landroid/os/Handler;

    #@5
    const/4 v1, 0x0

    #@6
    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    #@9
    .line 294
    return-void
.end method

.method protected jsAlert(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "url"
    .parameter "message"

    #@0
    .prologue
    .line 385
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/CallbackProxy;->onJsAlert(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 386
    return-void
.end method

.method protected jsConfirm(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "url"
    .parameter "message"

    #@0
    .prologue
    .line 577
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/CallbackProxy;->onJsConfirm(Ljava/lang/String;Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected jsInterrupt()Z
    .registers 2

    #@0
    .prologue
    .line 609
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->onJsTimeout()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected jsPrompt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "url"
    .parameter "message"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 588
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/CallbackProxy;->onJsPrompt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected jsUnload(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "url"
    .parameter "message"

    #@0
    .prologue
    .line 599
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/CallbackProxy;->onJsBeforeUnload(Ljava/lang/String;Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method notificationCancel(I)V
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 3369
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@2
    if-nez v0, :cond_15

    #@4
    .line 3370
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->inBrowserApp()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1a

    #@a
    .line 3371
    new-instance v0, Landroid/webkit/LGWebNotifications;

    #@c
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@e
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@10
    invoke-direct {v0, v1, p0, v2}, Landroid/webkit/LGWebNotifications;-><init>(Landroid/content/Context;Landroid/webkit/WebViewCore;Landroid/webkit/WebViewClassic;)V

    #@13
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@15
    .line 3376
    :cond_15
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@17
    invoke-virtual {v0, p1}, Landroid/webkit/LGWebNotifications;->cancel(I)V

    #@1a
    .line 3377
    :cond_1a
    return-void
.end method

.method notificationCancelRequestsForPermission()V
    .registers 4

    #@0
    .prologue
    .line 3402
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@2
    if-nez v0, :cond_15

    #@4
    .line 3403
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->inBrowserApp()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1a

    #@a
    .line 3404
    new-instance v0, Landroid/webkit/LGWebNotifications;

    #@c
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@e
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@10
    invoke-direct {v0, v1, p0, v2}, Landroid/webkit/LGWebNotifications;-><init>(Landroid/content/Context;Landroid/webkit/WebViewCore;Landroid/webkit/WebViewClassic;)V

    #@13
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@15
    .line 3409
    :cond_15
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@17
    invoke-virtual {v0}, Landroid/webkit/LGWebNotifications;->cancelRequestsForPermission()V

    #@1a
    .line 3410
    :cond_1a
    return-void
.end method

.method notificationCheckPermission()I
    .registers 4

    #@0
    .prologue
    .line 3391
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@2
    if-nez v0, :cond_15

    #@4
    .line 3392
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->inBrowserApp()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1c

    #@a
    .line 3393
    new-instance v0, Landroid/webkit/LGWebNotifications;

    #@c
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@e
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@10
    invoke-direct {v0, v1, p0, v2}, Landroid/webkit/LGWebNotifications;-><init>(Landroid/content/Context;Landroid/webkit/WebViewCore;Landroid/webkit/WebViewClassic;)V

    #@13
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@15
    .line 3398
    :cond_15
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@17
    invoke-virtual {v0}, Landroid/webkit/LGWebNotifications;->checkPermission()I

    #@1a
    move-result v0

    #@1b
    :goto_1b
    return v0

    #@1c
    .line 3395
    :cond_1c
    const/4 v0, 0x2

    #@1d
    goto :goto_1b
.end method

.method notificationCreate(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "id"
    .parameter "title"
    .parameter "text"
    .parameter "iconUrl"

    #@0
    .prologue
    .line 3357
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@2
    if-nez v0, :cond_15

    #@4
    .line 3358
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->inBrowserApp()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1a

    #@a
    .line 3359
    new-instance v0, Landroid/webkit/LGWebNotifications;

    #@c
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@e
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@10
    invoke-direct {v0, v1, p0, v2}, Landroid/webkit/LGWebNotifications;-><init>(Landroid/content/Context;Landroid/webkit/WebViewCore;Landroid/webkit/WebViewClassic;)V

    #@13
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@15
    .line 3364
    :cond_15
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@17
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/webkit/LGWebNotifications;->show(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 3365
    :cond_1a
    return-void
.end method

.method notificationRequestPermission()V
    .registers 4

    #@0
    .prologue
    .line 3380
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@2
    if-nez v0, :cond_15

    #@4
    .line 3381
    invoke-direct {p0}, Landroid/webkit/WebViewCore;->inBrowserApp()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1a

    #@a
    .line 3382
    new-instance v0, Landroid/webkit/LGWebNotifications;

    #@c
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mContext:Landroid/content/Context;

    #@e
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@10
    invoke-direct {v0, v1, p0, v2}, Landroid/webkit/LGWebNotifications;-><init>(Landroid/content/Context;Landroid/webkit/WebViewCore;Landroid/webkit/WebViewClassic;)V

    #@13
    iput-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@15
    .line 3387
    :cond_15
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mNotifications:Landroid/webkit/LGWebNotifications;

    #@17
    invoke-virtual {v0}, Landroid/webkit/LGWebNotifications;->requestPermission()V

    #@1a
    .line 3388
    :cond_1a
    return-void
.end method

.method pauseWebKitDraw()V
    .registers 3

    #@0
    .prologue
    .line 2312
    iget-object v1, p0, Landroid/webkit/WebViewCore;->m_skipDrawFlagLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2313
    :try_start_3
    iget-boolean v0, p0, Landroid/webkit/WebViewCore;->m_skipDrawFlag:Z

    #@5
    if-nez v0, :cond_a

    #@7
    .line 2314
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/webkit/WebViewCore;->m_skipDrawFlag:Z

    #@a
    .line 2316
    :cond_a
    monitor-exit v1

    #@b
    .line 2317
    return-void

    #@c
    .line 2316
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method protected populateVisitedLinks()V
    .registers 3

    #@0
    .prologue
    .line 534
    new-instance v0, Landroid/webkit/WebViewCore$3;

    #@2
    invoke-direct {v0, p0}, Landroid/webkit/WebViewCore$3;-><init>(Landroid/webkit/WebViewCore;)V

    #@5
    .line 540
    .local v0, callback:Landroid/webkit/ValueCallback;
    iget-object v1, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@7
    invoke-virtual {v1, v0}, Landroid/webkit/CallbackProxy;->getVisitedHistory(Landroid/webkit/ValueCallback;)V

    #@a
    .line 541
    return-void
.end method

.method protected reachedMaxAppCacheSize(JJ)V
    .registers 11
    .parameter "requiredStorage"
    .parameter "maxSize"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    new-instance v5, Landroid/webkit/WebViewCore$2;

    #@4
    invoke-direct {v5, p0}, Landroid/webkit/WebViewCore$2;-><init>(Landroid/webkit/WebViewCore;)V

    #@7
    move-wide v1, p1

    #@8
    move-wide v3, p3

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/webkit/CallbackProxy;->onReachedMaxAppCacheSize(JJLandroid/webkit/WebStorage$QuotaUpdater;)V

    #@c
    .line 531
    return-void
.end method

.method removeMessages()V
    .registers 2

    #@0
    .prologue
    .line 2084
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    invoke-static {v0}, Landroid/webkit/WebViewCore$EventHub;->access$8000(Landroid/webkit/WebViewCore$EventHub;)V

    #@5
    .line 2085
    return-void
.end method

.method removeMessages(I)V
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 2080
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    invoke-static {v0, p1}, Landroid/webkit/WebViewCore$EventHub;->access$7900(Landroid/webkit/WebViewCore$EventHub;I)V

    #@5
    .line 2081
    return-void
.end method

.method resumeWebKitDraw()V
    .registers 5

    #@0
    .prologue
    .line 2320
    iget-object v1, p0, Landroid/webkit/WebViewCore;->m_skipDrawFlagLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2321
    :try_start_3
    iget-boolean v0, p0, Landroid/webkit/WebViewCore;->m_skipDrawFlag:Z

    #@5
    if-eqz v0, :cond_1a

    #@7
    iget-boolean v0, p0, Landroid/webkit/WebViewCore;->m_drawWasSkipped:Z

    #@9
    if-eqz v0, :cond_1a

    #@b
    .line 2323
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/webkit/WebViewCore;->m_drawWasSkipped:Z

    #@e
    .line 2324
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@10
    const/4 v2, 0x0

    #@11
    const/16 v3, 0x82

    #@13
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@16
    move-result-object v2

    #@17
    invoke-static {v0, v2}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@1a
    .line 2326
    :cond_1a
    const/4 v0, 0x0

    #@1b
    iput-boolean v0, p0, Landroid/webkit/WebViewCore;->m_skipDrawFlag:Z

    #@1d
    .line 2327
    monitor-exit v1

    #@1e
    .line 2328
    return-void

    #@1f
    .line 2327
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v0
.end method

.method sendMessage(I)V
    .registers 4
    .parameter "what"

    #@0
    .prologue
    .line 2040
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@a
    .line 2041
    return-void
.end method

.method sendMessage(II)V
    .registers 6
    .parameter "what"
    .parameter "arg1"

    #@0
    .prologue
    .line 2054
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v1, p1, p2, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@b
    .line 2055
    return-void
.end method

.method sendMessage(III)V
    .registers 6
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 2058
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v1, p1, p2, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@a
    .line 2059
    return-void
.end method

.method sendMessage(IIILjava/lang/Object;)V
    .registers 7
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 2067
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v1, p1, p2, p3, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@a
    .line 2068
    return-void
.end method

.method sendMessage(IILjava/lang/Object;)V
    .registers 7
    .parameter "what"
    .parameter "arg1"
    .parameter "obj"

    #@0
    .prologue
    .line 2063
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v1, p1, p2, v2, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@b
    .line 2064
    return-void
.end method

.method sendMessage(ILjava/lang/Object;)V
    .registers 5
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 2049
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v1, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@a
    .line 2050
    return-void
.end method

.method public sendMessage(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 2028
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    invoke-static {v0, p1}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@5
    .line 2029
    return-void
.end method

.method sendMessageAtFrontOfQueue(IIILjava/lang/Object;)V
    .registers 7
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 2044
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v1, p1, p2, p3, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore$EventHub;->access$7700(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@a
    .line 2046
    return-void
.end method

.method sendMessageAtFrontOfQueue(ILjava/lang/Object;)V
    .registers 5
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 2071
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v1, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore$EventHub;->access$7700(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@a
    .line 2073
    return-void
.end method

.method sendMessageDelayed(ILjava/lang/Object;J)V
    .registers 7
    .parameter "what"
    .parameter "obj"
    .parameter "delay"

    #@0
    .prologue
    .line 2076
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v1, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1, p3, p4}, Landroid/webkit/WebViewCore$EventHub;->access$7800(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;J)V

    #@a
    .line 2077
    return-void
.end method

.method sendMessages(Ljava/util/ArrayList;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Message;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2032
    .local p1, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Message;>;"
    iget-object v2, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@2
    monitor-enter v2

    #@3
    .line 2033
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    :try_start_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v1

    #@8
    if-ge v0, v1, :cond_18

    #@a
    .line 2034
    iget-object v3, p0, Landroid/webkit/WebViewCore;->mEventHub:Landroid/webkit/WebViewCore$EventHub;

    #@c
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/os/Message;

    #@12
    invoke-static {v3, v1}, Landroid/webkit/WebViewCore$EventHub;->access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V

    #@15
    .line 2033
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_4

    #@18
    .line 2036
    :cond_18
    monitor-exit v2

    #@19
    .line 2037
    return-void

    #@1a
    .line 2036
    :catchall_1a
    move-exception v1

    #@1b
    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_4 .. :try_end_1c} :catchall_1a

    #@1c
    throw v1
.end method

.method sendNotificationMessage(II)V
    .registers 4
    .parameter "event"
    .parameter "id"

    #@0
    .prologue
    .line 3413
    iget v0, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@2
    invoke-virtual {p0, p1, v0, p2}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@5
    .line 3414
    return-void
.end method

.method public setMockDeviceOrientation(ZDZDZD)V
    .registers 20
    .parameter "canProvideAlpha"
    .parameter "alpha"
    .parameter "canProvideBeta"
    .parameter "beta"
    .parameter "canProvideGamma"
    .parameter "gamma"

    #@0
    .prologue
    .line 3228
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mDeviceMotionAndOrientationManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@2
    move v1, p1

    #@3
    move-wide v2, p2

    #@4
    move v4, p4

    #@5
    move-wide v5, p5

    #@6
    move/from16 v7, p7

    #@8
    move-wide/from16 v8, p8

    #@a
    invoke-virtual/range {v0 .. v9}, Landroid/webkit/DeviceMotionAndOrientationManager;->setMockOrientation(ZDZDZD)V

    #@d
    .line 3230
    return-void
.end method

.method public setMockGeolocationError(ILjava/lang/String;)V
    .registers 4
    .parameter "code"
    .parameter "message"

    #@0
    .prologue
    .line 3219
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mMockGeolocation:Landroid/webkit/MockGeolocation;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/MockGeolocation;->setError(ILjava/lang/String;)V

    #@5
    .line 3220
    return-void
.end method

.method public setMockGeolocationPermission(Z)V
    .registers 3
    .parameter "allow"

    #@0
    .prologue
    .line 3223
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mMockGeolocation:Landroid/webkit/MockGeolocation;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/MockGeolocation;->setPermission(Z)V

    #@5
    .line 3224
    return-void
.end method

.method public setMockGeolocationPosition(DDD)V
    .registers 14
    .parameter "latitude"
    .parameter "longitude"
    .parameter "accuracy"

    #@0
    .prologue
    .line 3215
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mMockGeolocation:Landroid/webkit/MockGeolocation;

    #@2
    move-wide v1, p1

    #@3
    move-wide v3, p3

    #@4
    move-wide v5, p5

    #@5
    invoke-virtual/range {v0 .. v6}, Landroid/webkit/MockGeolocation;->setPosition(DDD)V

    #@8
    .line 3216
    return-void
.end method

.method setNotificationSendRequestCallback()V
    .registers 2

    #@0
    .prologue
    .line 3417
    iget v0, p0, Landroid/webkit/WebViewCore;->mNativeClass:I

    #@2
    invoke-direct {p0, v0}, Landroid/webkit/WebViewCore;->nativeNotificationSendRequestCallback(I)V

    #@5
    .line 3418
    return-void
.end method

.method signalRepaintDone()V
    .registers 2

    #@0
    .prologue
    .line 2558
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Landroid/webkit/WebViewCore;->mRepaintScheduled:Z

    #@3
    .line 2559
    return-void
.end method

.method stopLoading()V
    .registers 2

    #@0
    .prologue
    .line 2013
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 2014
    iget-object v0, p0, Landroid/webkit/WebViewCore;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@6
    invoke-virtual {v0}, Landroid/webkit/BrowserFrame;->stopLoading()V

    #@9
    .line 2016
    :cond_9
    return-void
.end method
