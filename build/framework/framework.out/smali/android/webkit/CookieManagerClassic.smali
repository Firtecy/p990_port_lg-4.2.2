.class Landroid/webkit/CookieManagerClassic;
.super Landroid/webkit/CookieManager;
.source "CookieManagerClassic.java"


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final LOGTAG:Ljava/lang/String; = "webkit"

.field private static sRef:Landroid/webkit/CookieManagerClassic;


# instance fields
.field private mPendingCookieOperations:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 24
    const-class v0, Landroid/webkit/CookieManagerClassic;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/CookieManagerClassic;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/webkit/CookieManager;-><init>()V

    #@3
    .line 30
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/webkit/CookieManagerClassic;->mPendingCookieOperations:I

    #@6
    .line 33
    return-void
.end method

.method static synthetic access$000()V
    .registers 0

    #@0
    .prologue
    .line 24
    invoke-static {}, Landroid/webkit/CookieManagerClassic;->nativeRemoveSessionCookie()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/webkit/CookieManagerClassic;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 24
    invoke-direct {p0}, Landroid/webkit/CookieManagerClassic;->signalCookieOperationsComplete()V

    #@3
    return-void
.end method

.method public static declared-synchronized getInstance()Landroid/webkit/CookieManagerClassic;
    .registers 2

    #@0
    .prologue
    .line 36
    const-class v1, Landroid/webkit/CookieManagerClassic;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/CookieManagerClassic;->sRef:Landroid/webkit/CookieManagerClassic;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 37
    new-instance v0, Landroid/webkit/CookieManagerClassic;

    #@9
    invoke-direct {v0}, Landroid/webkit/CookieManagerClassic;-><init>()V

    #@c
    sput-object v0, Landroid/webkit/CookieManagerClassic;->sRef:Landroid/webkit/CookieManagerClassic;

    #@e
    .line 39
    :cond_e
    sget-object v0, Landroid/webkit/CookieManagerClassic;->sRef:Landroid/webkit/CookieManagerClassic;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    monitor-exit v1

    #@11
    return-object v0

    #@12
    .line 36
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1

    #@14
    throw v0
.end method

.method private static native nativeAcceptCookie()Z
.end method

.method private static native nativeAcceptFileSchemeCookies()Z
.end method

.method private static native nativeFlushCookieStore()V
.end method

.method private static native nativeGetCookie(Ljava/lang/String;Z)Ljava/lang/String;
.end method

.method private static native nativeHasCookies(Z)Z
.end method

.method private static native nativeRemoveAllCookie()V
.end method

.method private static native nativeRemoveExpiredCookie()V
.end method

.method private static native nativeRemoveSessionCookie()V
.end method

.method private static native nativeSetAcceptCookie(Z)V
.end method

.method private static native nativeSetAcceptFileSchemeCookies(Z)V
.end method

.method private static native nativeSetCookie(Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method private declared-synchronized signalCookieOperationsComplete()V
    .registers 3

    #@0
    .prologue
    .line 115
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/CookieManagerClassic;->mPendingCookieOperations:I

    #@3
    add-int/lit8 v0, v0, -0x1

    #@5
    iput v0, p0, Landroid/webkit/CookieManagerClassic;->mPendingCookieOperations:I

    #@7
    .line 116
    sget-boolean v0, Landroid/webkit/CookieManagerClassic;->$assertionsDisabled:Z

    #@9
    if-nez v0, :cond_19

    #@b
    iget v0, p0, Landroid/webkit/CookieManagerClassic;->mPendingCookieOperations:I

    #@d
    const/4 v1, -0x1

    #@e
    if-gt v0, v1, :cond_19

    #@10
    new-instance v0, Ljava/lang/AssertionError;

    #@12
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@15
    throw v0
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_16

    #@16
    .line 115
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0

    #@18
    throw v0

    #@19
    .line 117
    :cond_19
    :try_start_19
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_1c
    .catchall {:try_start_19 .. :try_end_1c} :catchall_16

    #@1c
    .line 118
    monitor-exit p0

    #@1d
    return-void
.end method

.method private declared-synchronized signalCookieOperationsStart()V
    .registers 2

    #@0
    .prologue
    .line 121
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/CookieManagerClassic;->mPendingCookieOperations:I

    #@3
    add-int/lit8 v0, v0, 0x1

    #@5
    iput v0, p0, Landroid/webkit/CookieManagerClassic;->mPendingCookieOperations:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 122
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 121
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method


# virtual methods
.method public declared-synchronized acceptCookie()Z
    .registers 2

    #@0
    .prologue
    .line 49
    monitor-enter p0

    #@1
    :try_start_1
    invoke-static {}, Landroid/webkit/CookieManagerClassic;->nativeAcceptCookie()Z
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_7

    #@4
    move-result v0

    #@5
    monitor-exit p0

    #@6
    return v0

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method protected allowFileSchemeCookiesImpl()Z
    .registers 2

    #@0
    .prologue
    .line 164
    invoke-static {}, Landroid/webkit/CookieManagerClassic;->nativeAcceptFileSchemeCookies()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected flushCookieStore()V
    .registers 1

    #@0
    .prologue
    .line 159
    invoke-static {}, Landroid/webkit/CookieManagerClassic;->nativeFlushCookieStore()V

    #@3
    .line 160
    return-void
.end method

.method public declared-synchronized getCookie(Landroid/net/WebAddress;)Ljava/lang/String;
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 96
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p1}, Landroid/net/WebAddress;->toString()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    const/4 v1, 0x0

    #@6
    invoke-static {v0, v1}, Landroid/webkit/CookieManagerClassic;->nativeGetCookie(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_c

    #@9
    move-result-object v0

    #@a
    monitor-exit p0

    #@b
    return-object v0

    #@c
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public getCookie(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 78
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/webkit/CookieManagerClassic;->getCookie(Ljava/lang/String;Z)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getCookie(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 8
    .parameter "url"
    .parameter "privateBrowsing"

    #@0
    .prologue
    .line 85
    :try_start_0
    new-instance v1, Landroid/net/WebAddress;

    #@2
    invoke-direct {v1, p1}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/net/ParseException; {:try_start_0 .. :try_end_5} :catch_e

    #@5
    .line 91
    .local v1, uri:Landroid/net/WebAddress;
    invoke-virtual {v1}, Landroid/net/WebAddress;->toString()Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    invoke-static {v2, p2}, Landroid/webkit/CookieManagerClassic;->nativeGetCookie(Ljava/lang/String;Z)Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    .end local v1           #uri:Landroid/net/WebAddress;
    :goto_d
    return-object v2

    #@e
    .line 86
    :catch_e
    move-exception v0

    #@f
    .line 87
    .local v0, ex:Landroid/net/ParseException;
    const-string/jumbo v2, "webkit"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "Bad address: "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 88
    const/4 v2, 0x0

    #@29
    goto :goto_d
.end method

.method public declared-synchronized hasCookies()Z
    .registers 2

    #@0
    .prologue
    .line 144
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    invoke-virtual {p0, v0}, Landroid/webkit/CookieManagerClassic;->hasCookies(Z)Z
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_8

    #@5
    move-result v0

    #@6
    monitor-exit p0

    #@7
    return v0

    #@8
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method public declared-synchronized hasCookies(Z)Z
    .registers 3
    .parameter "privateBrowsing"

    #@0
    .prologue
    .line 149
    monitor-enter p0

    #@1
    :try_start_1
    invoke-static {p1}, Landroid/webkit/CookieManagerClassic;->nativeHasCookies(Z)Z
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_7

    #@4
    move-result v0

    #@5
    monitor-exit p0

    #@6
    return v0

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public removeAllCookie()V
    .registers 1

    #@0
    .prologue
    .line 139
    invoke-static {}, Landroid/webkit/CookieManagerClassic;->nativeRemoveAllCookie()V

    #@3
    .line 140
    return-void
.end method

.method public removeExpiredCookie()V
    .registers 1

    #@0
    .prologue
    .line 154
    invoke-static {}, Landroid/webkit/CookieManagerClassic;->nativeRemoveExpiredCookie()V

    #@3
    .line 155
    return-void
.end method

.method public removeSessionCookie()V
    .registers 3

    #@0
    .prologue
    .line 126
    invoke-direct {p0}, Landroid/webkit/CookieManagerClassic;->signalCookieOperationsStart()V

    #@3
    .line 127
    new-instance v0, Landroid/webkit/CookieManagerClassic$1;

    #@5
    invoke-direct {v0, p0}, Landroid/webkit/CookieManagerClassic$1;-><init>(Landroid/webkit/CookieManagerClassic;)V

    #@8
    const/4 v1, 0x0

    #@9
    new-array v1, v1, [Ljava/lang/Void;

    #@b
    invoke-virtual {v0, v1}, Landroid/webkit/CookieManagerClassic$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@e
    .line 135
    return-void
.end method

.method public declared-synchronized setAcceptCookie(Z)V
    .registers 3
    .parameter "accept"

    #@0
    .prologue
    .line 44
    monitor-enter p0

    #@1
    :try_start_1
    invoke-static {p1}, Landroid/webkit/CookieManagerClassic;->nativeSetAcceptCookie(Z)V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_6

    #@4
    .line 45
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 44
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method

.method protected setAcceptFileSchemeCookiesImpl(Z)V
    .registers 2
    .parameter "accept"

    #@0
    .prologue
    .line 169
    invoke-static {p1}, Landroid/webkit/CookieManagerClassic;->nativeSetAcceptFileSchemeCookies(Z)V

    #@3
    .line 170
    return-void
.end method

.method public setCookie(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "url"
    .parameter "value"

    #@0
    .prologue
    .line 54
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/webkit/CookieManagerClassic;->setCookie(Ljava/lang/String;Ljava/lang/String;Z)V

    #@4
    .line 55
    return-void
.end method

.method setCookie(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 9
    .parameter "url"
    .parameter "value"
    .parameter "privateBrowsing"

    #@0
    .prologue
    .line 67
    :try_start_0
    new-instance v1, Landroid/net/WebAddress;

    #@2
    invoke-direct {v1, p1}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/net/ParseException; {:try_start_0 .. :try_end_5} :catch_d

    #@5
    .line 73
    .local v1, uri:Landroid/net/WebAddress;
    invoke-virtual {v1}, Landroid/net/WebAddress;->toString()Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    invoke-static {v2, p2, p3}, Landroid/webkit/CookieManagerClassic;->nativeSetCookie(Ljava/lang/String;Ljava/lang/String;Z)V

    #@c
    .line 74
    .end local v1           #uri:Landroid/net/WebAddress;
    :goto_c
    return-void

    #@d
    .line 68
    :catch_d
    move-exception v0

    #@e
    .line 69
    .local v0, ex:Landroid/net/ParseException;
    const-string/jumbo v2, "webkit"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "Bad address: "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    goto :goto_c
.end method

.method waitForCookieOperationsToComplete()V
    .registers 2

    #@0
    .prologue
    .line 105
    monitor-enter p0

    #@1
    .line 106
    :goto_1
    :try_start_1
    iget v0, p0, Landroid/webkit/CookieManagerClassic;->mPendingCookieOperations:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_d

    #@3
    if-lez v0, :cond_b

    #@5
    .line 108
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_d
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_9

    #@8
    goto :goto_1

    #@9
    .line 109
    :catch_9
    move-exception v0

    #@a
    goto :goto_1

    #@b
    .line 111
    :cond_b
    :try_start_b
    monitor-exit p0

    #@c
    .line 112
    return-void

    #@d
    .line 111
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_b .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method
