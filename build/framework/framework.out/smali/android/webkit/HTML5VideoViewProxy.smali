.class Landroid/webkit/HTML5VideoViewProxy;
.super Landroid/os/Handler;
.source "HTML5VideoViewProxy.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;,
        Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;
    }
.end annotation


# static fields
.field private static final ACTION_HTML5VIDEO_BROWSER_PLAY:Ljava/lang/String; = "lge.browser.intent.action.HTML5VIDEO_BROWSER_PLAY"

.field private static final ACTION_HTML5VIDEO_STREAM_PLAY:Ljava/lang/String; = "lge.browser.intent.action.HTML5VIDEO_STREAMING_PLAY"

.field private static final BUFFERING_END:I = 0x6a

.field private static final BUFFERING_START:I = 0x69

.field private static final ENDED:I = 0xc9

.field private static final ENTER_FROM_LGBROWSER:I = 0xce

.field private static final ENTER_FULLSCREEN:I = 0x6b

.field private static final ERROR:I = 0x67

.field private static final HEADER:Ljava/lang/String; = "Header"

.field private static IS_THERE_STREAMING_PLAYER:Z = false

.field private static final KEY_IS_PLAYING:Ljava/lang/String; = "isplay"

.field private static final KEY_POS:Ljava/lang/String; = "position"

.field private static final LOAD_DEFAULT_POSTER:I = 0x68

.field private static final LOGTAG:Ljava/lang/String; = "HTML5VideoViewProxy"

.field private static final ON_ENDED:I = 0x1

.field private static final PAUSE:I = 0x66

.field private static final PAUSED:I = 0xcb

.field private static final PLAY:I = 0x64

.field private static final POSTER_FETCHED:I = 0xca

.field private static final PREPARED:I = 0xc8

.field private static final RESTORESTATE:I = 0xcd

.field private static final SEEK:I = 0x65

.field private static final STOPFULLSCREEN:I = 0xcc

.field private static final TIMEUPDATE:I = 0x12c

.field private static mIsFloatingwindowSupport:Z

.field private static mIsInlineVideoEnded:Z


# instance fields
.field private mIsDirectFullScreen:Z

.field mNativePointer:I

.field private mPoster:Landroid/graphics/Bitmap;

.field private mPosterDownloader:Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;

.field private mSeekPosition:I

.field private mWebCoreHandler:Landroid/os/Handler;

.field private mWebView:Landroid/webkit/WebViewClassic;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 119
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Landroid/webkit/HTML5VideoViewProxy;->mIsInlineVideoEnded:Z

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/webkit/WebViewClassic;I)V
    .registers 10
    .parameter "webView"
    .parameter "nativePtr"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 829
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@5
    move-result-object v3

    #@6
    invoke-direct {p0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@9
    .line 114
    iput-boolean v2, p0, Landroid/webkit/HTML5VideoViewProxy;->mIsDirectFullScreen:Z

    #@b
    .line 831
    iput-object p1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@d
    .line 835
    iget-object v3, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@f
    invoke-virtual {v3, p0}, Landroid/webkit/WebViewClassic;->setHTML5VideoViewProxy(Landroid/webkit/HTML5VideoViewProxy;)V

    #@12
    .line 837
    iput p2, p0, Landroid/webkit/HTML5VideoViewProxy;->mNativePointer:I

    #@14
    .line 839
    invoke-direct {p0}, Landroid/webkit/HTML5VideoViewProxy;->createWebCoreHandler()V

    #@17
    .line 841
    new-instance v3, Landroid/content/Intent;

    #@19
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    #@1c
    new-instance v4, Landroid/content/ComponentName;

    #@1e
    const-string v5, "com.lge.streamingplayer"

    #@20
    const-string v6, "com.lge.streamingplayer.StreamingPlayer"

    #@22
    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@28
    move-result-object v0

    #@29
    .line 842
    .local v0, intent:Landroid/content/Intent;
    iget-object v3, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@2b
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    #@36
    move-result-object v3

    #@37
    if-eqz v3, :cond_4c

    #@39
    :goto_39
    sput-boolean v1, Landroid/webkit/HTML5VideoViewProxy;->IS_THERE_STREAMING_PLAYER:Z

    #@3b
    .line 844
    const-string/jumbo v1, "ro.lge.capp_qwindow"

    #@3e
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    const-string/jumbo v2, "true"

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@48
    move-result v1

    #@49
    sput-boolean v1, Landroid/webkit/HTML5VideoViewProxy;->mIsFloatingwindowSupport:Z

    #@4b
    .line 846
    return-void

    #@4c
    :cond_4c
    move v1, v2

    #@4d
    .line 842
    goto :goto_39
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 61
    sget-boolean v0, Landroid/webkit/HTML5VideoViewProxy;->mIsFloatingwindowSupport:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/graphics/SurfaceTexture;IIII)Z
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 61
    invoke-static {p0, p1, p2, p3, p4}, Landroid/webkit/HTML5VideoViewProxy;->nativeSendSurfaceTexture(Landroid/graphics/SurfaceTexture;IIII)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1000(Landroid/webkit/HTML5VideoViewProxy;Landroid/graphics/Bitmap;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1}, Landroid/webkit/HTML5VideoViewProxy;->doSetPoster(Landroid/graphics/Bitmap;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Landroid/webkit/HTML5VideoViewProxy;IIII)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/HTML5VideoViewProxy;->nativeOnPrepared(IIII)V

    #@3
    return-void
.end method

.method static synthetic access$1502(Landroid/webkit/HTML5VideoViewProxy;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 61
    iput p1, p0, Landroid/webkit/HTML5VideoViewProxy;->mSeekPosition:I

    #@2
    return p1
.end method

.method static synthetic access$1600(Landroid/webkit/HTML5VideoViewProxy;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1}, Landroid/webkit/HTML5VideoViewProxy;->nativeOnEnded(I)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Landroid/webkit/HTML5VideoViewProxy;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1}, Landroid/webkit/HTML5VideoViewProxy;->nativeOnPaused(I)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Landroid/webkit/HTML5VideoViewProxy;Landroid/graphics/Bitmap;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Landroid/webkit/HTML5VideoViewProxy;->nativeOnPosterFetched(Landroid/graphics/Bitmap;I)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Landroid/webkit/HTML5VideoViewProxy;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Landroid/webkit/HTML5VideoViewProxy;->nativeOnTimeupdate(II)V

    #@3
    return-void
.end method

.method static synthetic access$200()Z
    .registers 1

    #@0
    .prologue
    .line 61
    sget-boolean v0, Landroid/webkit/HTML5VideoViewProxy;->IS_THERE_STREAMING_PLAYER:Z

    #@2
    return v0
.end method

.method static synthetic access$2000(Landroid/webkit/HTML5VideoViewProxy;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Landroid/webkit/HTML5VideoViewProxy;->nativeOnStopFullscreen(II)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Landroid/webkit/HTML5VideoViewProxy;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1}, Landroid/webkit/HTML5VideoViewProxy;->nativeOnRestoreState(I)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Landroid/webkit/HTML5VideoViewProxy;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1}, Landroid/webkit/HTML5VideoViewProxy;->nativeOnEnterFromLGBrowser(I)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/webkit/HTML5VideoViewProxy;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    invoke-direct {p0}, Landroid/webkit/HTML5VideoViewProxy;->isDirectFullScreen()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400()Z
    .registers 1

    #@0
    .prologue
    .line 61
    sget-boolean v0, Landroid/webkit/HTML5VideoViewProxy;->mIsInlineVideoEnded:Z

    #@2
    return v0
.end method

.method static synthetic access$402(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 61
    sput-boolean p0, Landroid/webkit/HTML5VideoViewProxy;->mIsInlineVideoEnded:Z

    #@2
    return p0
.end method

.method static synthetic access$500(Landroid/webkit/HTML5VideoViewProxy;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1}, Landroid/webkit/HTML5VideoViewProxy;->setDirectFullScreen(Z)V

    #@3
    return-void
.end method

.method private createWebCoreHandler()V
    .registers 2

    #@0
    .prologue
    .line 849
    new-instance v0, Landroid/webkit/HTML5VideoViewProxy$1;

    #@2
    invoke-direct {v0, p0}, Landroid/webkit/HTML5VideoViewProxy$1;-><init>(Landroid/webkit/HTML5VideoViewProxy;)V

    #@5
    iput-object v0, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@7
    .line 889
    return-void
.end method

.method private doSetPoster(Landroid/graphics/Bitmap;)V
    .registers 5
    .parameter "poster"

    #@0
    .prologue
    .line 892
    if-nez p1, :cond_3

    #@2
    .line 900
    :goto_2
    return-void

    #@3
    .line 896
    :cond_3
    iput-object p1, p0, Landroid/webkit/HTML5VideoViewProxy;->mPoster:Landroid/graphics/Bitmap;

    #@5
    .line 897
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@7
    const/16 v2, 0xca

    #@9
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    .line 898
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f
    .line 899
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@11
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@14
    goto :goto_2
.end method

.method public static getInstance(Landroid/webkit/WebViewCore;I)Landroid/webkit/HTML5VideoViewProxy;
    .registers 4
    .parameter "webViewCore"
    .parameter "nativePtr"

    #@0
    .prologue
    .line 1019
    new-instance v0, Landroid/webkit/HTML5VideoViewProxy;

    #@2
    invoke-virtual {p0}, Landroid/webkit/WebViewCore;->getWebViewClassic()Landroid/webkit/WebViewClassic;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/webkit/HTML5VideoViewProxy;-><init>(Landroid/webkit/WebViewClassic;I)V

    #@9
    return-object v0
.end method

.method private isDirectFullScreen()Z
    .registers 2

    #@0
    .prologue
    .line 122
    iget-boolean v0, p0, Landroid/webkit/HTML5VideoViewProxy;->mIsDirectFullScreen:Z

    #@2
    return v0
.end method

.method private native nativeOnEnded(I)V
.end method

.method private native nativeOnEnterFromLGBrowser(I)V
.end method

.method private native nativeOnPaused(I)V
.end method

.method private native nativeOnPosterFetched(Landroid/graphics/Bitmap;I)V
.end method

.method private native nativeOnPrepared(IIII)V
.end method

.method private native nativeOnRestoreState(I)V
.end method

.method private native nativeOnStopFullscreen(II)V
.end method

.method private native nativeOnTimeupdate(II)V
.end method

.method private static native nativeSendSurfaceTexture(Landroid/graphics/SurfaceTexture;IIII)Z
.end method

.method private sendTimeupdate()V
    .registers 4

    #@0
    .prologue
    .line 903
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0x12c

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 904
    .local v0, msg:Landroid/os/Message;
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->getCurrentPosition()I

    #@b
    move-result v1

    #@c
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@e
    .line 905
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 906
    return-void
.end method

.method private setDirectFullScreen(Z)V
    .registers 2
    .parameter "flag"

    #@0
    .prologue
    .line 126
    iput-boolean p1, p0, Landroid/webkit/HTML5VideoViewProxy;->mIsDirectFullScreen:Z

    #@2
    .line 127
    return-void
.end method


# virtual methods
.method public dispatchOnEnded()V
    .registers 4

    #@0
    .prologue
    .line 536
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xc9

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 537
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 538
    return-void
.end method

.method public dispatchOnEnterFromLGBrowser()V
    .registers 4

    #@0
    .prologue
    .line 558
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xce

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 559
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 560
    return-void
.end method

.method public dispatchOnPaused()V
    .registers 4

    #@0
    .prologue
    .line 541
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xcb

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 542
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 543
    return-void
.end method

.method public dispatchOnRestoreState()V
    .registers 4

    #@0
    .prologue
    .line 552
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xcd

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 553
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 554
    return-void
.end method

.method public dispatchOnStopFullScreen(Z)V
    .registers 5
    .parameter "stillPlaying"

    #@0
    .prologue
    .line 546
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xcc

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 547
    .local v0, msg:Landroid/os/Message;
    if-eqz p1, :cond_13

    #@a
    const/4 v1, 0x1

    #@b
    :goto_b
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 548
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@f
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@12
    .line 549
    return-void

    #@13
    .line 547
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_b
.end method

.method public enterFullScreenVideo(ILjava/lang/String;)V
    .registers 4
    .parameter "layerId"
    .parameter "url"

    #@0
    .prologue
    .line 1005
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-static {p1, p2, p0, v0}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->enterFullScreenVideo(ILjava/lang/String;Landroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebViewClassic;)V

    #@5
    .line 1006
    return-void
.end method

.method public enterFullscreenForVideoLayer(Ljava/lang/String;I)V
    .registers 5
    .parameter "url"
    .parameter "videoLayerID"

    #@0
    .prologue
    .line 936
    if-nez p1, :cond_3

    #@2
    .line 944
    :goto_2
    return-void

    #@3
    .line 940
    :cond_3
    const/16 v1, 0x6b

    #@5
    invoke-virtual {p0, v1}, Landroid/webkit/HTML5VideoViewProxy;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 941
    .local v0, message:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@b
    .line 942
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    .line 943
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->sendMessage(Landroid/os/Message;)Z

    #@10
    goto :goto_2
.end method

.method public exitFullScreenVideo()V
    .registers 2

    #@0
    .prologue
    .line 1009
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-static {p0, v0}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->exitFullScreenVideo(Landroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebViewClassic;)V

    #@5
    .line 1010
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 909
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method getWebView()Landroid/webkit/WebViewClassic;
    .registers 2

    #@0
    .prologue
    .line 1023
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 587
    iget v4, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v4, :sswitch_data_86

    #@7
    .line 663
    :cond_7
    :goto_7
    return-void

    #@8
    .line 589
    :sswitch_8
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    check-cast v2, Ljava/lang/String;

    #@c
    .line 590
    .local v2, url:Ljava/lang/String;
    iget-object v4, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@e
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebChromeClient()Landroid/webkit/WebChromeClient;

    #@11
    move-result-object v0

    #@12
    .line 591
    .local v0, client:Landroid/webkit/WebChromeClient;
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@14
    .line 592
    .local v3, videoLayerID:I
    if-eqz v0, :cond_7

    #@16
    .line 594
    sput-boolean v5, Landroid/webkit/HTML5VideoViewProxy;->mIsInlineVideoEnded:Z

    #@18
    .line 596
    iget v4, p0, Landroid/webkit/HTML5VideoViewProxy;->mSeekPosition:I

    #@1a
    invoke-static {v2, v4, p0, v0, v3}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->play(Ljava/lang/String;ILandroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebChromeClient;I)V

    #@1d
    goto :goto_7

    #@1e
    .line 601
    .end local v0           #client:Landroid/webkit/WebChromeClient;
    .end local v2           #url:Ljava/lang/String;
    .end local v3           #videoLayerID:I
    :sswitch_1e
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20
    check-cast v2, Ljava/lang/String;

    #@22
    .line 602
    .restart local v2       #url:Ljava/lang/String;
    iget-object v4, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@24
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebChromeClient()Landroid/webkit/WebChromeClient;

    #@27
    move-result-object v0

    #@28
    .line 603
    .restart local v0       #client:Landroid/webkit/WebChromeClient;
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@2a
    .line 604
    .restart local v3       #videoLayerID:I
    if-eqz v0, :cond_7

    #@2c
    .line 605
    iget-object v4, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@2e
    invoke-static {v3, v2, p0, v4}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->enterFullScreenVideo(ILjava/lang/String;Landroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebViewClassic;)V

    #@31
    goto :goto_7

    #@32
    .line 611
    .end local v0           #client:Landroid/webkit/WebChromeClient;
    .end local v2           #url:Ljava/lang/String;
    .end local v3           #videoLayerID:I
    :sswitch_32
    sput-boolean v5, Landroid/webkit/HTML5VideoViewProxy;->mIsInlineVideoEnded:Z

    #@34
    .line 613
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@36
    check-cast v1, Ljava/lang/Integer;

    #@38
    .line 614
    .local v1, time:Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@3b
    move-result v4

    #@3c
    iput v4, p0, Landroid/webkit/HTML5VideoViewProxy;->mSeekPosition:I

    #@3e
    .line 615
    iget v4, p0, Landroid/webkit/HTML5VideoViewProxy;->mSeekPosition:I

    #@40
    invoke-static {v4, p0}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->seek(ILandroid/webkit/HTML5VideoViewProxy;)V

    #@43
    goto :goto_7

    #@44
    .line 619
    .end local v1           #time:Ljava/lang/Integer;
    :sswitch_44
    invoke-static {p0}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->pause(Landroid/webkit/HTML5VideoViewProxy;)V

    #@47
    goto :goto_7

    #@48
    .line 623
    :sswitch_48
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@4a
    if-ne v4, v6, :cond_51

    #@4c
    .line 625
    sput-boolean v6, Landroid/webkit/HTML5VideoViewProxy;->mIsInlineVideoEnded:Z

    #@4e
    .line 627
    invoke-static {v6}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->access$802(Z)Z

    #@51
    .line 629
    :cond_51
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->end()V

    #@54
    goto :goto_7

    #@55
    .line 632
    :sswitch_55
    iget-object v4, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@57
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebChromeClient()Landroid/webkit/WebChromeClient;

    #@5a
    move-result-object v0

    #@5b
    .line 633
    .restart local v0       #client:Landroid/webkit/WebChromeClient;
    if-eqz v0, :cond_60

    #@5d
    .line 634
    invoke-virtual {v0}, Landroid/webkit/WebChromeClient;->onHideCustomView()V

    #@60
    .line 637
    :cond_60
    iput v5, p0, Landroid/webkit/HTML5VideoViewProxy;->mSeekPosition:I

    #@62
    goto :goto_7

    #@63
    .line 642
    .end local v0           #client:Landroid/webkit/WebChromeClient;
    :sswitch_63
    iget-object v4, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@65
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebChromeClient()Landroid/webkit/WebChromeClient;

    #@68
    move-result-object v0

    #@69
    .line 643
    .restart local v0       #client:Landroid/webkit/WebChromeClient;
    if-eqz v0, :cond_7

    #@6b
    .line 644
    invoke-virtual {v0}, Landroid/webkit/WebChromeClient;->getDefaultVideoPoster()Landroid/graphics/Bitmap;

    #@6e
    move-result-object v4

    #@6f
    invoke-direct {p0, v4}, Landroid/webkit/HTML5VideoViewProxy;->doSetPoster(Landroid/graphics/Bitmap;)V

    #@72
    goto :goto_7

    #@73
    .line 649
    .end local v0           #client:Landroid/webkit/WebChromeClient;
    :sswitch_73
    invoke-static {p0}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->isPlaying(Landroid/webkit/HTML5VideoViewProxy;)Z

    #@76
    move-result v4

    #@77
    if-eqz v4, :cond_7

    #@79
    .line 650
    invoke-direct {p0}, Landroid/webkit/HTML5VideoViewProxy;->sendTimeupdate()V

    #@7c
    goto :goto_7

    #@7d
    .line 655
    :sswitch_7d
    invoke-static {v6}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->access$900(Z)V

    #@80
    goto :goto_7

    #@81
    .line 659
    :sswitch_81
    invoke-static {v5}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->access$900(Z)V

    #@84
    goto :goto_7

    #@85
    .line 587
    nop

    #@86
    :sswitch_data_86
    .sparse-switch
        0x64 -> :sswitch_8
        0x65 -> :sswitch_32
        0x66 -> :sswitch_44
        0x67 -> :sswitch_55
        0x68 -> :sswitch_63
        0x69 -> :sswitch_7d
        0x6a -> :sswitch_81
        0x6b -> :sswitch_1e
        0xc9 -> :sswitch_48
        0x12c -> :sswitch_73
    .end sparse-switch
.end method

.method public loadPoster(Ljava/lang/String;)V
    .registers 4
    .parameter "url"

    #@0
    .prologue
    .line 981
    if-nez p1, :cond_c

    #@2
    .line 982
    const/16 v1, 0x68

    #@4
    invoke-virtual {p0, v1}, Landroid/webkit/HTML5VideoViewProxy;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 983
    .local v0, message:Landroid/os/Message;
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->sendMessage(Landroid/os/Message;)Z

    #@b
    .line 993
    .end local v0           #message:Landroid/os/Message;
    :goto_b
    return-void

    #@c
    .line 987
    :cond_c
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mPosterDownloader:Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;

    #@e
    if-eqz v1, :cond_15

    #@10
    .line 988
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mPosterDownloader:Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;

    #@12
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->cancelAndReleaseQueue()V

    #@15
    .line 991
    :cond_15
    new-instance v1, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;

    #@17
    invoke-direct {v1, p1, p0}, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;-><init>(Ljava/lang/String;Landroid/webkit/HTML5VideoViewProxy;)V

    #@1a
    iput-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mPosterDownloader:Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;

    #@1c
    .line 992
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mPosterDownloader:Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;

    #@1e
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->start()V

    #@21
    goto :goto_b
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .registers 5
    .parameter "mp"

    #@0
    .prologue
    .line 525
    const/16 v0, 0xc9

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    invoke-virtual {p0, v0, v1, v2}, Landroid/webkit/HTML5VideoViewProxy;->obtainMessage(III)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->sendMessage(Landroid/os/Message;)Z

    #@b
    .line 526
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .registers 5
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    #@0
    .prologue
    .line 531
    const/16 v0, 0x67

    #@2
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->sendMessage(Landroid/os/Message;)Z

    #@9
    .line 532
    const/4 v0, 0x0

    #@a
    return v0
.end method

.method public onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .registers 3
    .parameter "surfaceTexture"

    #@0
    .prologue
    .line 580
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@5
    .line 581
    return-void
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .registers 5
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    #@0
    .prologue
    .line 1043
    const/16 v0, 0x2bd

    #@2
    if-ne p2, v0, :cond_f

    #@4
    .line 1044
    const/16 v0, 0x69

    #@6
    invoke-virtual {p0, v0, p2, p3}, Landroid/webkit/HTML5VideoViewProxy;->obtainMessage(III)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 1048
    :cond_d
    :goto_d
    const/4 v0, 0x0

    #@e
    return v0

    #@f
    .line 1045
    :cond_f
    const/16 v0, 0x2be

    #@11
    if-ne p2, v0, :cond_d

    #@13
    .line 1046
    const/16 v0, 0x6a

    #@15
    invoke-virtual {p0, v0, p2, p3}, Landroid/webkit/HTML5VideoViewProxy;->obtainMessage(III)Landroid/os/Message;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->sendMessage(Landroid/os/Message;)Z

    #@1c
    goto :goto_d
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1053
    const/4 v1, 0x4

    #@2
    if-ne p2, v1, :cond_1d

    #@4
    .line 1054
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_b

    #@a
    .line 1061
    :goto_a
    return v0

    #@b
    .line 1056
    :cond_b
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@e
    move-result v1

    #@f
    if-ne v1, v0, :cond_1d

    #@11
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_1d

    #@17
    .line 1057
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@19
    invoke-static {p0, v1}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->exitFullScreenVideo(Landroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebViewClassic;)V

    #@1c
    goto :goto_a

    #@1d
    .line 1061
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_a
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .registers 7
    .parameter "mp"

    #@0
    .prologue
    .line 508
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->onPrepared()V

    #@3
    .line 509
    iget-object v2, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@5
    const/16 v3, 0xc8

    #@7
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@a
    move-result-object v1

    #@b
    .line 510
    .local v1, msg:Landroid/os/Message;
    new-instance v0, Ljava/util/HashMap;

    #@d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@10
    .line 511
    .local v0, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "dur"

    #@12
    new-instance v3, Ljava/lang/Integer;

    #@14
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    #@17
    move-result v4

    #@18
    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    #@1b
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    .line 512
    const-string/jumbo v2, "width"

    #@21
    new-instance v3, Ljava/lang/Integer;

    #@23
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    #@26
    move-result v4

    #@27
    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    #@2a
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 513
    const-string v2, "height"

    #@2f
    new-instance v3, Ljava/lang/Integer;

    #@31
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    #@34
    move-result v4

    #@35
    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    #@38
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    .line 514
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3d
    .line 515
    iget-object v2, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@3f
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@42
    .line 516
    return-void
.end method

.method public onTimeupdate()V
    .registers 2

    #@0
    .prologue
    .line 564
    const/16 v0, 0x12c

    #@2
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->sendMessage(Landroid/os/Message;)Z

    #@9
    .line 565
    return-void
.end method

.method public onTimeupdateManually(I)V
    .registers 5
    .parameter "position"

    #@0
    .prologue
    .line 569
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0x12c

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 570
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@a
    .line 571
    iget-object v1, p0, Landroid/webkit/HTML5VideoViewProxy;->mWebCoreHandler:Landroid/os/Handler;

    #@c
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@f
    .line 572
    return-void
.end method

.method public pause()V
    .registers 3

    #@0
    .prologue
    .line 960
    const/16 v1, 0x66

    #@2
    invoke-virtual {p0, v1}, Landroid/webkit/HTML5VideoViewProxy;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 961
    .local v0, message:Landroid/os/Message;
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->sendMessage(Landroid/os/Message;)Z

    #@9
    .line 962
    return-void
.end method

.method public pauseAndDispatch()V
    .registers 1

    #@0
    .prologue
    .line 1001
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->pauseAndDispatch()V

    #@3
    .line 1002
    return-void
.end method

.method public play(Ljava/lang/String;II)V
    .registers 6
    .parameter "url"
    .parameter "position"
    .parameter "videoLayerID"

    #@0
    .prologue
    .line 918
    if-nez p1, :cond_3

    #@2
    .line 929
    :goto_2
    return-void

    #@3
    .line 922
    :cond_3
    if-lez p2, :cond_8

    #@5
    .line 923
    invoke-virtual {p0, p2}, Landroid/webkit/HTML5VideoViewProxy;->seek(I)V

    #@8
    .line 925
    :cond_8
    const/16 v1, 0x64

    #@a
    invoke-virtual {p0, v1}, Landroid/webkit/HTML5VideoViewProxy;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 926
    .local v0, message:Landroid/os/Message;
    iput p3, v0, Landroid/os/Message;->arg1:I

    #@10
    .line 927
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12
    .line 928
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->sendMessage(Landroid/os/Message;)Z

    #@15
    goto :goto_2
.end method

.method public seek(I)V
    .registers 4
    .parameter "time"

    #@0
    .prologue
    .line 951
    const/16 v1, 0x65

    #@2
    invoke-virtual {p0, v1}, Landroid/webkit/HTML5VideoViewProxy;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 952
    .local v0, message:Landroid/os/Message;
    new-instance v1, Ljava/lang/Integer;

    #@8
    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    #@b
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    .line 953
    invoke-virtual {p0, v0}, Landroid/webkit/HTML5VideoViewProxy;->sendMessage(Landroid/os/Message;)Z

    #@10
    .line 954
    return-void
.end method

.method public setBaseLayer(I)V
    .registers 2
    .parameter "layer"

    #@0
    .prologue
    .line 997
    invoke-static {p1}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->setBaseLayer(I)V

    #@3
    .line 998
    return-void
.end method

.method public teardown()V
    .registers 2

    #@0
    .prologue
    .line 970
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy;->mPosterDownloader:Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 971
    iget-object v0, p0, Landroid/webkit/HTML5VideoViewProxy;->mPosterDownloader:Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;

    #@6
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoViewProxy$PosterDownloader;->cancelAndReleaseQueue()V

    #@9
    .line 973
    :cond_9
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/webkit/HTML5VideoViewProxy;->mNativePointer:I

    #@c
    .line 974
    return-void
.end method
