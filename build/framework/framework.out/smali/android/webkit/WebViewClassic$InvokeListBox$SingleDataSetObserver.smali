.class Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "WebViewClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic$InvokeListBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SingleDataSetObserver"
.end annotation


# instance fields
.field private mAdapter:Landroid/widget/Adapter;

.field private mCheckedId:J

.field private mListView:Landroid/widget/ListView;

.field final synthetic this$1:Landroid/webkit/WebViewClassic$InvokeListBox;


# direct methods
.method public constructor <init>(Landroid/webkit/WebViewClassic$InvokeListBox;JLandroid/widget/ListView;Landroid/widget/Adapter;)V
    .registers 6
    .parameter
    .parameter "id"
    .parameter "l"
    .parameter "a"

    #@0
    .prologue
    .line 10021
    iput-object p1, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->this$1:Landroid/webkit/WebViewClassic$InvokeListBox;

    #@2
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    #@5
    .line 10022
    iput-wide p2, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mCheckedId:J

    #@7
    .line 10023
    iput-object p4, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mListView:Landroid/widget/ListView;

    #@9
    .line 10024
    iput-object p5, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mAdapter:Landroid/widget/Adapter;

    #@b
    .line 10025
    return-void
.end method


# virtual methods
.method public onChanged()V
    .registers 10

    #@0
    .prologue
    .line 10031
    iget-object v5, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mListView:Landroid/widget/ListView;

    #@2
    invoke-virtual {v5}, Landroid/widget/ListView;->getCheckedItemPosition()I

    #@5
    move-result v4

    #@6
    .line 10032
    .local v4, position:I
    iget-object v5, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mAdapter:Landroid/widget/Adapter;

    #@8
    invoke-interface {v5, v4}, Landroid/widget/Adapter;->getItemId(I)J

    #@b
    move-result-wide v2

    #@c
    .line 10033
    .local v2, id:J
    iget-wide v5, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mCheckedId:J

    #@e
    cmp-long v5, v5, v2

    #@10
    if-eqz v5, :cond_32

    #@12
    .line 10036
    iget-object v5, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mListView:Landroid/widget/ListView;

    #@14
    invoke-virtual {v5}, Landroid/widget/ListView;->clearChoices()V

    #@17
    .line 10039
    iget-object v5, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mAdapter:Landroid/widget/Adapter;

    #@19
    invoke-interface {v5}, Landroid/widget/Adapter;->getCount()I

    #@1c
    move-result v0

    #@1d
    .line 10040
    .local v0, count:I
    const/4 v1, 0x0

    #@1e
    .local v1, i:I
    :goto_1e
    if-ge v1, v0, :cond_32

    #@20
    .line 10041
    iget-object v5, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mAdapter:Landroid/widget/Adapter;

    #@22
    invoke-interface {v5, v1}, Landroid/widget/Adapter;->getItemId(I)J

    #@25
    move-result-wide v5

    #@26
    iget-wide v7, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mCheckedId:J

    #@28
    cmp-long v5, v5, v7

    #@2a
    if-nez v5, :cond_33

    #@2c
    .line 10042
    iget-object v5, p0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;->mListView:Landroid/widget/ListView;

    #@2e
    const/4 v6, 0x1

    #@2f
    invoke-virtual {v5, v1, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    #@32
    .line 10047
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_32
    return-void

    #@33
    .line 10040
    .restart local v0       #count:I
    .restart local v1       #i:I
    :cond_33
    add-int/lit8 v1, v1, 0x1

    #@35
    goto :goto_1e
.end method
