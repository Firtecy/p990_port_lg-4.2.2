.class public Landroid/webkit/ByteArrayBuilder$Chunk;
.super Ljava/lang/Object;
.source "ByteArrayBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/ByteArrayBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Chunk"
.end annotation


# instance fields
.field public mArray:[B

.field public mLength:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "length"

    #@0
    .prologue
    .line 135
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 136
    new-array v0, p1, [B

    #@5
    iput-object v0, p0, Landroid/webkit/ByteArrayBuilder$Chunk;->mArray:[B

    #@7
    .line 137
    const/4 v0, 0x0

    #@8
    iput v0, p0, Landroid/webkit/ByteArrayBuilder$Chunk;->mLength:I

    #@a
    .line 138
    return-void
.end method


# virtual methods
.method public release()V
    .registers 1

    #@0
    .prologue
    .line 155
    return-void
.end method
