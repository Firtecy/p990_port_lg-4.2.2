.class Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "WebViewClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic$InvokeListBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyArrayListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/webkit/WebViewClassic$InvokeListBox$Container;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Landroid/webkit/WebViewClassic$InvokeListBox;


# direct methods
.method public constructor <init>(Landroid/webkit/WebViewClassic$InvokeListBox;)V
    .registers 5
    .parameter

    #@0
    .prologue
    .line 9874
    iput-object p1, p0, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->this$1:Landroid/webkit/WebViewClassic$InvokeListBox;

    #@2
    .line 9875
    iget-object v0, p1, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@4
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@7
    move-result-object v1

    #@8
    invoke-static {p1}, Landroid/webkit/WebViewClassic$InvokeListBox;->access$9000(Landroid/webkit/WebViewClassic$InvokeListBox;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_19

    #@e
    const v0, 0x1090013

    #@11
    :goto_11
    invoke-static {p1}, Landroid/webkit/WebViewClassic$InvokeListBox;->access$9100(Landroid/webkit/WebViewClassic$InvokeListBox;)[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@14
    move-result-object v2

    #@15
    invoke-direct {p0, v1, v0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    #@18
    .line 9879
    return-void

    #@19
    .line 9875
    :cond_19
    const v0, 0x10900eb

    #@1c
    goto :goto_11
.end method

.method private item(I)Landroid/webkit/WebViewClassic$InvokeListBox$Container;
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 9943
    if-ltz p1, :cond_8

    #@2
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->getCount()I

    #@5
    move-result v0

    #@6
    if-lt p1, v0, :cond_a

    #@8
    .line 9944
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 9946
    :goto_9
    return-object v0

    #@a
    :cond_a
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->getItem(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@10
    goto :goto_9
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 9960
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getItemId(I)J
    .registers 5
    .parameter "position"

    #@0
    .prologue
    .line 9951
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->item(I)Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@3
    move-result-object v0

    #@4
    .line 9952
    .local v0, item:Landroid/webkit/WebViewClassic$InvokeListBox$Container;
    if-nez v0, :cond_9

    #@6
    .line 9953
    const-wide/16 v1, -0x1

    #@8
    .line 9955
    :goto_8
    return-wide v1

    #@9
    :cond_9
    iget v1, v0, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mId:I

    #@b
    int-to-long v1, v1

    #@c
    goto :goto_8
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const v7, 0x1080012

    #@4
    const/4 v6, 0x1

    #@5
    .line 9889
    invoke-super {p0, p1, v8, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@8
    move-result-object p2

    #@9
    .line 9890
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->item(I)Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@c
    move-result-object v0

    #@d
    .line 9893
    .local v0, c:Landroid/webkit/WebViewClassic$InvokeListBox$Container;
    if-eqz v0, :cond_2a

    #@f
    if-nez p1, :cond_2a

    #@11
    iget-object v4, p0, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->this$1:Landroid/webkit/WebViewClassic$InvokeListBox;

    #@13
    invoke-static {v4}, Landroid/webkit/WebViewClassic$InvokeListBox;->access$9200(Landroid/webkit/WebViewClassic$InvokeListBox;)Z

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_2a

    #@19
    .line 9894
    iget-object v4, p0, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->this$1:Landroid/webkit/WebViewClassic$InvokeListBox;

    #@1b
    iget-object v4, v4, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@1d
    invoke-static {v4}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@20
    move-result-object v4

    #@21
    const v5, 0x209037e

    #@24
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    iput-object v4, v0, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mString:Ljava/lang/String;

    #@2a
    .line 9897
    :cond_2a
    if-eqz v0, :cond_91

    #@2c
    iget v4, v0, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mEnabled:I

    #@2e
    if-eq v6, v4, :cond_91

    #@30
    .line 9900
    new-instance v3, Landroid/widget/LinearLayout;

    #@32
    iget-object v4, p0, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->this$1:Landroid/webkit/WebViewClassic$InvokeListBox;

    #@34
    iget-object v4, v4, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@36
    invoke-static {v4}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@39
    move-result-object v4

    #@3a
    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@3d
    .line 9901
    .local v3, layout:Landroid/widget/LinearLayout;
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    #@40
    .line 9902
    if-lez p1, :cond_55

    #@42
    .line 9903
    new-instance v2, Landroid/view/View;

    #@44
    iget-object v4, p0, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->this$1:Landroid/webkit/WebViewClassic$InvokeListBox;

    #@46
    iget-object v4, v4, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@48
    invoke-static {v4}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@4b
    move-result-object v4

    #@4c
    invoke-direct {v2, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@4f
    .line 9904
    .local v2, dividerTop:Landroid/view/View;
    invoke-virtual {v2, v7}, Landroid/view/View;->setBackgroundResource(I)V

    #@52
    .line 9906
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@55
    .line 9909
    .end local v2           #dividerTop:Landroid/view/View;
    :cond_55
    const/4 v4, -0x1

    #@56
    iget v5, v0, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mEnabled:I

    #@58
    if-ne v4, v5, :cond_8c

    #@5a
    .line 9912
    iget-object v4, p0, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->this$1:Landroid/webkit/WebViewClassic$InvokeListBox;

    #@5c
    invoke-static {v4}, Landroid/webkit/WebViewClassic$InvokeListBox;->access$9000(Landroid/webkit/WebViewClassic$InvokeListBox;)Z

    #@5f
    move-result v4

    #@60
    if-eqz v4, :cond_6d

    #@62
    .line 9913
    instance-of v4, p2, Landroid/widget/CheckedTextView;

    #@64
    invoke-static {v4}, Ljunit/framework/Assert;->assertTrue(Z)V

    #@67
    move-object v4, p2

    #@68
    .line 9914
    check-cast v4, Landroid/widget/CheckedTextView;

    #@6a
    invoke-virtual {v4, v8}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    #@6d
    .line 9922
    :cond_6d
    :goto_6d
    invoke-virtual {v3, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@70
    .line 9923
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->getCount()I

    #@73
    move-result v4

    #@74
    add-int/lit8 v4, v4, -0x1

    #@76
    if-ge p1, v4, :cond_8b

    #@78
    .line 9924
    new-instance v1, Landroid/view/View;

    #@7a
    iget-object v4, p0, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->this$1:Landroid/webkit/WebViewClassic$InvokeListBox;

    #@7c
    iget-object v4, v4, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@7e
    invoke-static {v4}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@81
    move-result-object v4

    #@82
    invoke-direct {v1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@85
    .line 9925
    .local v1, dividerBottom:Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/view/View;->setBackgroundResource(I)V

    #@88
    .line 9927
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@8b
    .line 9931
    .end local v1           #dividerBottom:Landroid/view/View;
    .end local v3           #layout:Landroid/widget/LinearLayout;
    :cond_8b
    :goto_8b
    return-object v3

    #@8c
    .line 9919
    .restart local v3       #layout:Landroid/widget/LinearLayout;
    :cond_8c
    const/4 v4, 0x0

    #@8d
    invoke-virtual {p2, v4}, Landroid/view/View;->setEnabled(Z)V

    #@90
    goto :goto_6d

    #@91
    .end local v3           #layout:Landroid/widget/LinearLayout;
    :cond_91
    move-object v3, p2

    #@92
    .line 9931
    goto :goto_8b
.end method

.method public hasStableIds()Z
    .registers 2

    #@0
    .prologue
    .line 9939
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isEnabled(I)Z
    .registers 6
    .parameter "position"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 9965
    invoke-direct {p0, p1}, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->item(I)Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@5
    move-result-object v0

    #@6
    .line 9966
    .local v0, item:Landroid/webkit/WebViewClassic$InvokeListBox$Container;
    if-nez v0, :cond_9

    #@8
    .line 9969
    :goto_8
    return v2

    #@9
    :cond_9
    iget v3, v0, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mEnabled:I

    #@b
    if-ne v1, v3, :cond_f

    #@d
    :goto_d
    move v2, v1

    #@e
    goto :goto_8

    #@f
    :cond_f
    move v1, v2

    #@10
    goto :goto_d
.end method
