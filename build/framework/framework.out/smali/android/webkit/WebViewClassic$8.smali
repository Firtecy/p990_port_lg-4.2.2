.class Landroid/webkit/WebViewClassic$8;
.super Landroid/os/AsyncTask;
.source "WebViewClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/WebViewClassic;->loadViewState(Ljava/io/InputStream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/io/InputStream;",
        "Ljava/lang/Void;",
        "Landroid/webkit/WebViewCore$DrawData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/WebViewClassic;


# direct methods
.method constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2890
    iput-object p1, p0, Landroid/webkit/WebViewClassic$8;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/io/InputStream;)Landroid/webkit/WebViewCore$DrawData;
    .registers 4
    .parameter "params"

    #@0
    .prologue
    .line 2895
    const/4 v1, 0x0

    #@1
    :try_start_1
    aget-object v1, p1, v1

    #@3
    invoke-static {v1}, Landroid/webkit/ViewStateSerializer;->deserializeViewState(Ljava/io/InputStream;)Landroid/webkit/WebViewCore$DrawData;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_8

    #@6
    move-result-object v1

    #@7
    .line 2897
    :goto_7
    return-object v1

    #@8
    .line 2896
    :catch_8
    move-exception v0

    #@9
    .line 2897
    .local v0, e:Ljava/io/IOException;
    const/4 v1, 0x0

    #@a
    goto :goto_7
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 2890
    check-cast p1, [Ljava/io/InputStream;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic$8;->doInBackground([Ljava/io/InputStream;)Landroid/webkit/WebViewCore$DrawData;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected onPostExecute(Landroid/webkit/WebViewCore$DrawData;)V
    .registers 7
    .parameter "draw"

    #@0
    .prologue
    .line 2903
    if-nez p1, :cond_b

    #@2
    .line 2904
    const-string/jumbo v2, "webview"

    #@5
    const-string v3, "Failed to load view state!"

    #@7
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 2914
    :goto_a
    return-void

    #@b
    .line 2907
    :cond_b
    iget-object v2, p0, Landroid/webkit/WebViewClassic$8;->this$0:Landroid/webkit/WebViewClassic;

    #@d
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@10
    move-result v1

    #@11
    .line 2908
    .local v1, viewWidth:I
    iget-object v2, p0, Landroid/webkit/WebViewClassic$8;->this$0:Landroid/webkit/WebViewClassic;

    #@13
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@16
    move-result v2

    #@17
    iget-object v3, p0, Landroid/webkit/WebViewClassic$8;->this$0:Landroid/webkit/WebViewClassic;

    #@19
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@1c
    move-result v3

    #@1d
    sub-int v0, v2, v3

    #@1f
    .line 2909
    .local v0, viewHeight:I
    new-instance v2, Landroid/graphics/Point;

    #@21
    invoke-direct {v2, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    #@24
    iput-object v2, p1, Landroid/webkit/WebViewCore$DrawData;->mViewSize:Landroid/graphics/Point;

    #@26
    .line 2910
    iget-object v2, p1, Landroid/webkit/WebViewCore$DrawData;->mViewState:Landroid/webkit/WebViewCore$ViewState;

    #@28
    iget-object v3, p0, Landroid/webkit/WebViewClassic$8;->this$0:Landroid/webkit/WebViewClassic;

    #@2a
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getDefaultZoomScale()F

    #@2d
    move-result v3

    #@2e
    iput v3, v2, Landroid/webkit/WebViewCore$ViewState;->mDefaultScale:F

    #@30
    .line 2911
    iget-object v2, p0, Landroid/webkit/WebViewClassic$8;->this$0:Landroid/webkit/WebViewClassic;

    #@32
    invoke-static {v2, p1}, Landroid/webkit/WebViewClassic;->access$2402(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$DrawData;)Landroid/webkit/WebViewCore$DrawData;

    #@35
    .line 2912
    iget-object v2, p0, Landroid/webkit/WebViewClassic$8;->this$0:Landroid/webkit/WebViewClassic;

    #@37
    iget-object v3, p0, Landroid/webkit/WebViewClassic$8;->this$0:Landroid/webkit/WebViewClassic;

    #@39
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$2400(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$DrawData;

    #@3c
    move-result-object v3

    #@3d
    const/4 v4, 0x1

    #@3e
    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebViewClassic;->setNewPicture(Landroid/webkit/WebViewCore$DrawData;Z)V

    #@41
    .line 2913
    iget-object v2, p0, Landroid/webkit/WebViewClassic$8;->this$0:Landroid/webkit/WebViewClassic;

    #@43
    invoke-static {v2}, Landroid/webkit/WebViewClassic;->access$2400(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$DrawData;

    #@46
    move-result-object v2

    #@47
    const/4 v3, 0x0

    #@48
    iput-object v3, v2, Landroid/webkit/WebViewCore$DrawData;->mViewState:Landroid/webkit/WebViewCore$ViewState;

    #@4a
    goto :goto_a
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2890
    check-cast p1, Landroid/webkit/WebViewCore$DrawData;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/webkit/WebViewClassic$8;->onPostExecute(Landroid/webkit/WebViewCore$DrawData;)V

    #@5
    return-void
.end method
