.class Landroid/webkit/ZoomManager;
.super Ljava/lang/Object;
.source "ZoomManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/ZoomManager$1;,
        Landroid/webkit/ZoomManager$PostScale;,
        Landroid/webkit/ZoomManager$ScaleDetectorListener;,
        Landroid/webkit/ZoomManager$FocusMovementQueue;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field protected static final DEFAULT_MAX_ZOOM_SCALE_FACTOR:F = 4.0f

.field protected static final DEFAULT_MIN_ZOOM_SCALE_FACTOR:F = 0.25f

.field static final LOGTAG:Ljava/lang/String; = "webviewZoom"

.field private static MINIMUM_SCALE_INCREMENT:F = 0.0f

.field private static MINIMUM_SCALE_WITHOUT_JITTER:F = 0.0f

.field private static MIN_DOUBLE_TAP_SCALE_INCREMENT:F = 0.0f

.field private static final ZOOM_ANIMATION_LENGTH:I = 0xaf


# instance fields
.field private mActualScale:F

.field private mAllowPanAndScale:Z

.field private mAnchorX:I

.field private mAnchorY:I

.field private final mCallbackProxy:Landroid/webkit/CallbackProxy;

.field private mDefaultMaxZoomScale:F

.field private mDefaultMinZoomScale:F

.field private mDefaultScale:F

.field private mDisplayDensity:F

.field private mDoubleTapZoomFactor:F

.field private mEmbeddedZoomControl:Landroid/webkit/ZoomControlEmbedded;

.field private mExternalZoomControl:Landroid/webkit/ZoomControlExternal;

.field private mFocusMovementQueue:Landroid/webkit/ZoomManager$FocusMovementQueue;

.field private mFocusX:F

.field private mFocusY:F

.field private mHardwareAccelerated:Z

.field private mInHWAcceleratedZoom:Z

.field private mInZoomOverview:Z

.field private mInitialScale:F

.field private mInitialScrollX:I

.field private mInitialScrollY:I

.field private mInitialZoomOverview:Z

.field private mInvActualScale:F

.field private mInvDefaultScale:F

.field private mInvFinalZoomScale:F

.field private mInvInitialZoomScale:F

.field private mInvZoomOverviewWidth:F

.field private mMaxZoomScale:F

.field private mMinZoomScale:F

.field private mMinZoomScaleFixed:Z

.field private mPinchToZoomAnimating:Z

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private mSupportMultiTouch:Z

.field private mTextWrapScale:F

.field private final mWebView:Landroid/webkit/WebViewClassic;

.field private mZoomCenterX:F

.field private mZoomCenterY:F

.field private mZoomOverviewWidth:I

.field private mZoomScale:F

.field private mZoomStart:J


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const v1, 0x3be56042

    #@3
    .line 49
    const-class v0, Landroid/webkit/ZoomManager;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_17

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    sput-boolean v0, Landroid/webkit/ZoomManager;->$assertionsDisabled:Z

    #@e
    .line 164
    const/high16 v0, 0x3f00

    #@10
    sput v0, Landroid/webkit/ZoomManager;->MIN_DOUBLE_TAP_SCALE_INCREMENT:F

    #@12
    .line 176
    sput v1, Landroid/webkit/ZoomManager;->MINIMUM_SCALE_INCREMENT:F

    #@14
    .line 182
    sput v1, Landroid/webkit/ZoomManager;->MINIMUM_SCALE_WITHOUT_JITTER:F

    #@16
    return-void

    #@17
    .line 49
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_c
.end method

.method public constructor <init>(Landroid/webkit/WebViewClassic;Landroid/webkit/CallbackProxy;)V
    .registers 5
    .parameter "webView"
    .parameter "callbackProxy"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 214
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 77
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/webkit/ZoomManager;->mMinZoomScaleFixed:Z

    #@7
    .line 86
    iput-boolean v1, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@9
    .line 96
    iput-boolean v1, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@b
    .line 158
    const/high16 v0, 0x3f80

    #@d
    iput v0, p0, Landroid/webkit/ZoomManager;->mDoubleTapZoomFactor:F

    #@f
    .line 209
    iput-boolean v1, p0, Landroid/webkit/ZoomManager;->mPinchToZoomAnimating:Z

    #@11
    .line 211
    iput-boolean v1, p0, Landroid/webkit/ZoomManager;->mHardwareAccelerated:Z

    #@13
    .line 212
    iput-boolean v1, p0, Landroid/webkit/ZoomManager;->mInHWAcceleratedZoom:Z

    #@15
    .line 215
    iput-object p1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@17
    .line 216
    iput-object p2, p0, Landroid/webkit/ZoomManager;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@19
    .line 223
    const/16 v0, 0x3d4

    #@1b
    invoke-direct {p0, v0}, Landroid/webkit/ZoomManager;->setZoomOverviewWidth(I)V

    #@1e
    .line 225
    new-instance v0, Landroid/webkit/ZoomManager$FocusMovementQueue;

    #@20
    invoke-direct {v0, p0}, Landroid/webkit/ZoomManager$FocusMovementQueue;-><init>(Landroid/webkit/ZoomManager;)V

    #@23
    iput-object v0, p0, Landroid/webkit/ZoomManager;->mFocusMovementQueue:Landroid/webkit/ZoomManager$FocusMovementQueue;

    #@25
    .line 226
    return-void
.end method

.method static synthetic access$1000(Landroid/webkit/ZoomManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-boolean v0, p0, Landroid/webkit/ZoomManager;->mPinchToZoomAnimating:Z

    #@2
    return v0
.end method

.method static synthetic access$1002(Landroid/webkit/ZoomManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-boolean p1, p0, Landroid/webkit/ZoomManager;->mPinchToZoomAnimating:Z

    #@2
    return p1
.end method

.method static synthetic access$102(Landroid/webkit/ZoomManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-boolean p1, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@2
    return p1
.end method

.method static synthetic access$1100()F
    .registers 1

    #@0
    .prologue
    .line 49
    sget v0, Landroid/webkit/ZoomManager;->MINIMUM_SCALE_WITHOUT_JITTER:F

    #@2
    return v0
.end method

.method static synthetic access$1202(Landroid/webkit/ZoomManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput p1, p0, Landroid/webkit/ZoomManager;->mAnchorX:I

    #@2
    return p1
.end method

.method static synthetic access$1300(Landroid/webkit/ZoomManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget v0, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@2
    return v0
.end method

.method static synthetic access$1402(Landroid/webkit/ZoomManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput p1, p0, Landroid/webkit/ZoomManager;->mAnchorY:I

    #@2
    return p1
.end method

.method static synthetic access$1500(Landroid/webkit/ZoomManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget v0, p0, Landroid/webkit/ZoomManager;->mZoomCenterY:F

    #@2
    return v0
.end method

.method static synthetic access$1600(Landroid/webkit/ZoomManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget v0, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@2
    return v0
.end method

.method static synthetic access$1700(Landroid/webkit/ZoomManager;FZZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/ZoomManager;->setZoomScale(FZZ)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/webkit/ZoomManager;)Landroid/webkit/ZoomManager$FocusMovementQueue;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mFocusMovementQueue:Landroid/webkit/ZoomManager$FocusMovementQueue;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/webkit/ZoomManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget v0, p0, Landroid/webkit/ZoomManager;->mFocusX:F

    #@2
    return v0
.end method

.method static synthetic access$402(Landroid/webkit/ZoomManager;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput p1, p0, Landroid/webkit/ZoomManager;->mFocusX:F

    #@2
    return p1
.end method

.method static synthetic access$500(Landroid/webkit/ZoomManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget v0, p0, Landroid/webkit/ZoomManager;->mFocusY:F

    #@2
    return v0
.end method

.method static synthetic access$502(Landroid/webkit/ZoomManager;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput p1, p0, Landroid/webkit/ZoomManager;->mFocusY:F

    #@2
    return p1
.end method

.method static synthetic access$600(Landroid/webkit/ZoomManager;)Landroid/webkit/WebViewClassic;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/webkit/ZoomManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget v0, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@2
    return v0
.end method

.method public static final exceedsMinScaleIncrement(FF)Z
    .registers 4
    .parameter "scaleA"
    .parameter "scaleB"

    #@0
    .prologue
    .line 380
    sub-float v0, p0, p1

    #@2
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@5
    move-result v0

    #@6
    sget v1, Landroid/webkit/ZoomManager;->MINIMUM_SCALE_INCREMENT:F

    #@8
    cmpl-float v0, v0, v1

    #@a
    if-ltz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private getCurrentZoomControl()Landroid/webkit/ZoomControlBase;
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1219
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 1234
    :cond_5
    :goto_5
    return-object v0

    #@6
    .line 1220
    :cond_6
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@8
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@b
    move-result-object v1

    #@c
    if-eqz v1, :cond_5

    #@e
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@10
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->supportZoom()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_5

    #@1a
    .line 1221
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@1c
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getBuiltInZoomControls()Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_42

    #@26
    .line 1222
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mEmbeddedZoomControl:Landroid/webkit/ZoomControlEmbedded;

    #@28
    if-nez v0, :cond_3f

    #@2a
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@2c
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@2f
    move-result-object v0

    #@30
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getDisplayZoomControls()Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_3f

    #@36
    .line 1224
    new-instance v0, Landroid/webkit/ZoomControlEmbedded;

    #@38
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@3a
    invoke-direct {v0, p0, v1}, Landroid/webkit/ZoomControlEmbedded;-><init>(Landroid/webkit/ZoomManager;Landroid/webkit/WebViewClassic;)V

    #@3d
    iput-object v0, p0, Landroid/webkit/ZoomManager;->mEmbeddedZoomControl:Landroid/webkit/ZoomControlEmbedded;

    #@3f
    .line 1226
    :cond_3f
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mEmbeddedZoomControl:Landroid/webkit/ZoomControlEmbedded;

    #@41
    goto :goto_5

    #@42
    .line 1228
    :cond_42
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mExternalZoomControl:Landroid/webkit/ZoomControlExternal;

    #@44
    if-nez v0, :cond_4f

    #@46
    .line 1229
    new-instance v0, Landroid/webkit/ZoomControlExternal;

    #@48
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@4a
    invoke-direct {v0, v1}, Landroid/webkit/ZoomControlExternal;-><init>(Landroid/webkit/WebViewClassic;)V

    #@4d
    iput-object v0, p0, Landroid/webkit/ZoomManager;->mExternalZoomControl:Landroid/webkit/ZoomControlExternal;

    #@4f
    .line 1231
    :cond_4f
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mExternalZoomControl:Landroid/webkit/ZoomControlExternal;

    #@51
    goto :goto_5
.end method

.method private sanitizeMinMaxScales()V
    .registers 4

    #@0
    .prologue
    .line 920
    iget v0, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@2
    iget v1, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@4
    cmpl-float v0, v0, v1

    #@6
    if-lez v0, :cond_39

    #@8
    .line 921
    const-string/jumbo v0, "webviewZoom"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v2, "mMinZoom > mMaxZoom!!! "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget v2, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, " > "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    iget v2, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    new-instance v2, Ljava/lang/Exception;

    #@2f
    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    #@32
    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@35
    .line 923
    iget v0, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@37
    iput v0, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@39
    .line 925
    :cond_39
    return-void
.end method

.method private setDefaultZoomScale(F)V
    .registers 7
    .parameter "defaultScale"

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 270
    iget v0, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@4
    .line 271
    .local v0, originalDefault:F
    iput p1, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@6
    .line 272
    const/high16 v1, 0x3f80

    #@8
    div-float/2addr v1, p1

    #@9
    iput v1, p0, Landroid/webkit/ZoomManager;->mInvDefaultScale:F

    #@b
    .line 273
    const/high16 v1, 0x4080

    #@d
    mul-float/2addr v1, p1

    #@e
    iput v1, p0, Landroid/webkit/ZoomManager;->mDefaultMaxZoomScale:F

    #@10
    .line 274
    const/high16 v1, 0x3e80

    #@12
    mul-float/2addr v1, p1

    #@13
    iput v1, p0, Landroid/webkit/ZoomManager;->mDefaultMinZoomScale:F

    #@15
    .line 275
    float-to-double v1, v0

    #@16
    cmpl-double v1, v1, v3

    #@18
    if-lez v1, :cond_4d

    #@1a
    iget v1, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@1c
    float-to-double v1, v1

    #@1d
    cmpl-double v1, v1, v3

    #@1f
    if-lez v1, :cond_4d

    #@21
    .line 277
    div-float v1, p1, v0

    #@23
    iget v2, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@25
    mul-float/2addr v1, v2

    #@26
    iput v1, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@28
    .line 281
    :goto_28
    float-to-double v1, v0

    #@29
    cmpl-double v1, v1, v3

    #@2b
    if-lez v1, :cond_52

    #@2d
    iget v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@2f
    float-to-double v1, v1

    #@30
    cmpl-double v1, v1, v3

    #@32
    if-lez v1, :cond_52

    #@34
    .line 283
    div-float v1, p1, v0

    #@36
    iget v2, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@38
    mul-float/2addr v1, v2

    #@39
    iput v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@3b
    .line 287
    :goto_3b
    iget v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@3d
    iget v2, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@3f
    invoke-static {v1, v2}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@42
    move-result v1

    #@43
    if-nez v1, :cond_49

    #@45
    .line 288
    iget v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@47
    iput v1, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@49
    .line 290
    :cond_49
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->sanitizeMinMaxScales()V

    #@4c
    .line 291
    return-void

    #@4d
    .line 279
    :cond_4d
    iget v1, p0, Landroid/webkit/ZoomManager;->mDefaultMaxZoomScale:F

    #@4f
    iput v1, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@51
    goto :goto_28

    #@52
    .line 285
    :cond_52
    iget v1, p0, Landroid/webkit/ZoomManager;->mDefaultMinZoomScale:F

    #@54
    iput v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@56
    goto :goto_3b
.end method

.method private setZoomOverviewWidth(I)V
    .registers 4
    .parameter "width"

    #@0
    .prologue
    .line 684
    if-nez p1, :cond_d

    #@2
    .line 685
    const/16 v0, 0x3d4

    #@4
    iput v0, p0, Landroid/webkit/ZoomManager;->mZoomOverviewWidth:I

    #@6
    .line 689
    :goto_6
    const/high16 v0, 0x3f80

    #@8
    int-to-float v1, p1

    #@9
    div-float/2addr v0, v1

    #@a
    iput v0, p0, Landroid/webkit/ZoomManager;->mInvZoomOverviewWidth:F

    #@c
    .line 690
    return-void

    #@d
    .line 687
    :cond_d
    iput p1, p0, Landroid/webkit/ZoomManager;->mZoomOverviewWidth:I

    #@f
    goto :goto_6
.end method

.method private setZoomScale(FZZ)V
    .registers 19
    .parameter "scale"
    .parameter "reflowText"
    .parameter "force"

    #@0
    .prologue
    .line 551
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->getWebViewCore()Landroid/webkit/WebViewCore;

    #@5
    move-result-object v11

    #@6
    if-nez v11, :cond_9

    #@8
    .line 613
    :cond_8
    :goto_8
    return-void

    #@9
    .line 554
    :cond_9
    iget v11, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@b
    cmpg-float v11, p1, v11

    #@d
    if-gez v11, :cond_c5

    #@f
    const/4 v1, 0x1

    #@10
    .line 555
    .local v1, isScaleLessThanMinZoom:Z
    :goto_10
    invoke-virtual/range {p0 .. p1}, Landroid/webkit/ZoomManager;->computeScaleWithLimits(F)F

    #@13
    move-result p1

    #@14
    .line 558
    if-eqz v1, :cond_c8

    #@16
    iget v11, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@18
    iget v12, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@1a
    cmpg-float v11, v11, v12

    #@1c
    if-gez v11, :cond_c8

    #@1e
    .line 559
    const/4 v11, 0x1

    #@1f
    iput-boolean v11, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@21
    .line 564
    :goto_21
    if-eqz p2, :cond_33

    #@23
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@25
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@28
    move-result-object v11

    #@29
    invoke-virtual {v11}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@2c
    move-result v11

    #@2d
    if-nez v11, :cond_33

    #@2f
    .line 565
    move/from16 v0, p1

    #@31
    iput v0, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@33
    .line 568
    :cond_33
    iget v11, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@35
    cmpl-float v11, p1, v11

    #@37
    if-nez v11, :cond_3b

    #@39
    if-eqz p3, :cond_8

    #@3b
    .line 569
    :cond_3b
    iget v3, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@3d
    .line 570
    .local v3, oldScale:F
    iget v2, p0, Landroid/webkit/ZoomManager;->mInvActualScale:F

    #@3f
    .line 572
    .local v2, oldInvScale:F
    iget v11, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@41
    cmpl-float v11, p1, v11

    #@43
    if-eqz v11, :cond_52

    #@45
    iget-boolean v11, p0, Landroid/webkit/ZoomManager;->mPinchToZoomAnimating:Z

    #@47
    if-nez v11, :cond_52

    #@49
    .line 573
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@4b
    iget v12, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@4d
    move/from16 v0, p1

    #@4f
    invoke-virtual {v11, v12, v0}, Landroid/webkit/CallbackProxy;->onScaleChanged(FF)V

    #@52
    .line 576
    :cond_52
    move/from16 v0, p1

    #@54
    iput v0, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@56
    .line 577
    const/high16 v11, 0x3f80

    #@58
    div-float v11, v11, p1

    #@5a
    iput v11, p0, Landroid/webkit/ZoomManager;->mInvActualScale:F

    #@5c
    .line 579
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@5e
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->drawHistory()Z

    #@61
    move-result v11

    #@62
    if-nez v11, :cond_bc

    #@64
    iget-boolean v11, p0, Landroid/webkit/ZoomManager;->mInHWAcceleratedZoom:Z

    #@66
    if-nez v11, :cond_bc

    #@68
    .line 587
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@6a
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@6d
    move-result v4

    #@6e
    .line 588
    .local v4, oldX:I
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@70
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@73
    move-result v5

    #@74
    .line 589
    .local v5, oldY:I
    mul-float v6, p1, v2

    #@76
    .line 590
    .local v6, ratio:F
    int-to-float v11, v4

    #@77
    mul-float/2addr v11, v6

    #@78
    const/high16 v12, 0x3f80

    #@7a
    sub-float v12, v6, v12

    #@7c
    iget v13, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@7e
    mul-float/2addr v12, v13

    #@7f
    add-float v9, v11, v12

    #@81
    .line 591
    .local v9, sx:F
    int-to-float v11, v5

    #@82
    mul-float/2addr v11, v6

    #@83
    const/high16 v12, 0x3f80

    #@85
    sub-float v12, v6, v12

    #@87
    iget v13, p0, Landroid/webkit/ZoomManager;->mZoomCenterY:F

    #@89
    iget-object v14, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@8b
    invoke-virtual {v14}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@8e
    move-result v14

    #@8f
    int-to-float v14, v14

    #@90
    sub-float/2addr v13, v14

    #@91
    mul-float/2addr v12, v13

    #@92
    add-float v10, v11, v12

    #@94
    .line 595
    .local v10, sy:F
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@96
    iget-object v11, v11, Landroid/webkit/WebViewClassic;->mViewManager:Landroid/webkit/ViewManager;

    #@98
    invoke-virtual {v11}, Landroid/webkit/ViewManager;->scaleAll()V

    #@9b
    .line 599
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@9d
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@a0
    move-result v12

    #@a1
    invoke-virtual {v11, v12}, Landroid/webkit/WebViewClassic;->pinLocX(I)I

    #@a4
    move-result v7

    #@a5
    .line 600
    .local v7, scrollX:I
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@a7
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    #@aa
    move-result v12

    #@ab
    invoke-virtual {v11, v12}, Landroid/webkit/WebViewClassic;->pinLocY(I)I

    #@ae
    move-result v8

    #@af
    .line 601
    .local v8, scrollY:I
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@b1
    invoke-virtual {v11, v7, v8}, Landroid/webkit/WebViewClassic;->updateScrollCoordinates(II)Z

    #@b4
    move-result v11

    #@b5
    if-nez v11, :cond_bc

    #@b7
    .line 605
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@b9
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->sendOurVisibleRect()Landroid/graphics/Rect;

    #@bc
    .line 611
    .end local v4           #oldX:I
    .end local v5           #oldY:I
    .end local v6           #ratio:F
    .end local v7           #scrollX:I
    .end local v8           #scrollY:I
    .end local v9           #sx:F
    .end local v10           #sy:F
    :cond_bc
    iget-object v11, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@be
    move/from16 v0, p2

    #@c0
    invoke-virtual {v11, v0}, Landroid/webkit/WebViewClassic;->sendViewSizeZoom(Z)Z

    #@c3
    goto/16 :goto_8

    #@c5
    .line 554
    .end local v1           #isScaleLessThanMinZoom:Z
    .end local v2           #oldInvScale:F
    .end local v3           #oldScale:F
    :cond_c5
    const/4 v1, 0x0

    #@c6
    goto/16 :goto_10

    #@c8
    .line 561
    .restart local v1       #isScaleLessThanMinZoom:Z
    :cond_c8
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getZoomOverviewScale()F

    #@cb
    move-result v11

    #@cc
    move/from16 v0, p1

    #@ce
    invoke-static {v0, v11}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@d1
    move-result v11

    #@d2
    if-nez v11, :cond_d9

    #@d4
    const/4 v11, 0x1

    #@d5
    :goto_d5
    iput-boolean v11, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@d7
    goto/16 :goto_21

    #@d9
    :cond_d9
    const/4 v11, 0x0

    #@da
    goto :goto_d5
.end method

.method private setupZoomOverviewWidth(Landroid/webkit/WebViewCore$DrawData;I)Z
    .registers 7
    .parameter "drawData"
    .parameter "viewWidth"

    #@0
    .prologue
    .line 1106
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@5
    move-result-object v1

    #@6
    .line 1107
    .local v1, settings:Landroid/webkit/WebSettings;
    iget v0, p0, Landroid/webkit/ZoomManager;->mZoomOverviewWidth:I

    #@8
    .line 1108
    .local v0, newZoomOverviewWidth:I
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_27

    #@e
    .line 1109
    iget-object v2, p1, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@10
    iget v2, v2, Landroid/graphics/Point;->x:I

    #@12
    if-lez v2, :cond_1e

    #@14
    .line 1112
    sget v2, Landroid/webkit/WebViewClassic;->sMaxViewportWidth:I

    #@16
    iget-object v3, p1, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@18
    iget v3, v3, Landroid/graphics/Point;->x:I

    #@1a
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    #@1d
    move-result v0

    #@1e
    .line 1129
    :cond_1e
    :goto_1e
    iget v2, p0, Landroid/webkit/ZoomManager;->mZoomOverviewWidth:I

    #@20
    if-eq v0, v2, :cond_49

    #@22
    .line 1130
    invoke-direct {p0, v0}, Landroid/webkit/ZoomManager;->setZoomOverviewWidth(I)V

    #@25
    .line 1131
    const/4 v2, 0x1

    #@26
    .line 1133
    :goto_26
    return v2

    #@27
    .line 1117
    :cond_27
    if-eqz v1, :cond_40

    #@29
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getForceZoomoutEnabled()Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_40

    #@2f
    .line 1118
    iget-object v2, p1, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@31
    iget v2, v2, Landroid/graphics/Point;->x:I

    #@33
    if-lez v2, :cond_1e

    #@35
    .line 1121
    sget v2, Landroid/webkit/WebViewClassic;->sMaxViewportWidth:I

    #@37
    iget-object v3, p1, Landroid/webkit/WebViewCore$DrawData;->mContentSize:Landroid/graphics/Point;

    #@39
    iget v3, v3, Landroid/graphics/Point;->x:I

    #@3b
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@3e
    move-result v0

    #@3f
    goto :goto_1e

    #@40
    .line 1127
    :cond_40
    int-to-float v2, p2

    #@41
    iget v3, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@43
    div-float/2addr v2, v3

    #@44
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    #@47
    move-result v0

    #@48
    goto :goto_1e

    #@49
    .line 1133
    :cond_49
    const/4 v2, 0x0

    #@4a
    goto :goto_26
.end method

.method private zoom(F)Z
    .registers 6
    .parameter "zoomMultiplier"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/high16 v2, 0x3f00

    #@3
    .line 405
    iput-boolean v0, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@5
    .line 407
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@7
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@a
    .line 409
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@c
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@f
    move-result v1

    #@10
    int-to-float v1, v1

    #@11
    mul-float/2addr v1, v2

    #@12
    iput v1, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@14
    .line 410
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@16
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getViewHeight()I

    #@19
    move-result v1

    #@1a
    int-to-float v1, v1

    #@1b
    mul-float/2addr v1, v2

    #@1c
    iput v1, p0, Landroid/webkit/ZoomManager;->mZoomCenterY:F

    #@1e
    .line 411
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@20
    iget v2, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@22
    float-to-int v2, v2

    #@23
    iget-object v3, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@25
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@28
    move-result v3

    #@29
    add-int/2addr v2, v3

    #@2a
    invoke-virtual {v1, v2}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@2d
    move-result v1

    #@2e
    iput v1, p0, Landroid/webkit/ZoomManager;->mAnchorX:I

    #@30
    .line 412
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@32
    iget v2, p0, Landroid/webkit/ZoomManager;->mZoomCenterY:F

    #@34
    float-to-int v2, v2

    #@35
    iget-object v3, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@37
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@3a
    move-result v3

    #@3b
    add-int/2addr v2, v3

    #@3c
    invoke-virtual {v1, v2}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@3f
    move-result v1

    #@40
    iput v1, p0, Landroid/webkit/ZoomManager;->mAnchorY:I

    #@42
    .line 413
    iget v1, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@44
    mul-float/2addr v1, p1

    #@45
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@47
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@4e
    move-result v2

    #@4f
    if-nez v2, :cond_52

    #@51
    const/4 v0, 0x1

    #@52
    :cond_52
    invoke-virtual {p0, v1, v0}, Landroid/webkit/ZoomManager;->startZoomAnimation(FZ)Z

    #@55
    move-result v0

    #@56
    return v0
.end method

.method private zoomToOverview()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 702
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@3
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@6
    move-result v0

    #@7
    .line 703
    .local v0, scrollY:I
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@9
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@c
    move-result v2

    #@d
    if-ge v0, v2, :cond_1a

    #@f
    .line 704
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@11
    iget-object v3, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@13
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@16
    move-result v3

    #@17
    invoke-virtual {v2, v3, v1}, Landroid/webkit/WebViewClassic;->updateScrollCoordinates(II)Z

    #@1a
    .line 706
    :cond_1a
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getZoomOverviewScale()F

    #@1d
    move-result v2

    #@1e
    iget-object v3, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@20
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@27
    move-result v3

    #@28
    if-nez v3, :cond_2b

    #@2a
    const/4 v1, 0x1

    #@2b
    :cond_2b
    invoke-virtual {p0, v2, v1}, Landroid/webkit/ZoomManager;->startZoomAnimation(FZ)Z

    #@2e
    .line 708
    return-void
.end method

.method private zoomToReadingLevel()V
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 711
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@4
    move-result v1

    #@5
    .line 713
    .local v1, readingScale:F
    iget-object v3, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@7
    iget v5, p0, Landroid/webkit/ZoomManager;->mAnchorX:I

    #@9
    iget v6, p0, Landroid/webkit/ZoomManager;->mAnchorY:I

    #@b
    invoke-virtual {v3, v5, v6, v1}, Landroid/webkit/WebViewClassic;->getBlockLeftEdge(IIF)I

    #@e
    move-result v0

    #@f
    .line 714
    .local v0, left:I
    const/4 v3, -0x1

    #@10
    if-eq v0, v3, :cond_2f

    #@12
    .line 716
    iget-object v5, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@14
    const/4 v3, 0x5

    #@15
    if-ge v0, v3, :cond_40

    #@17
    move v3, v4

    #@18
    :goto_18
    invoke-virtual {v5, v3}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@1b
    move-result v3

    #@1c
    iget-object v5, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@1e
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@21
    move-result v5

    #@22
    sub-int v2, v3, v5

    #@24
    .line 720
    .local v2, viewLeft:I
    if-lez v2, :cond_43

    #@26
    .line 721
    int-to-float v3, v2

    #@27
    mul-float/2addr v3, v1

    #@28
    iget v5, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@2a
    sub-float v5, v1, v5

    #@2c
    div-float/2addr v3, v5

    #@2d
    iput v3, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@2f
    .line 727
    .end local v2           #viewLeft:I
    :cond_2f
    :goto_2f
    iget-object v3, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@31
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@38
    move-result v3

    #@39
    if-nez v3, :cond_3c

    #@3b
    const/4 v4, 0x1

    #@3c
    :cond_3c
    invoke-virtual {p0, v1, v4}, Landroid/webkit/ZoomManager;->startZoomAnimation(FZ)Z

    #@3f
    .line 729
    return-void

    #@40
    .line 716
    :cond_40
    add-int/lit8 v3, v0, -0x5

    #@42
    goto :goto_18

    #@43
    .line 723
    .restart local v2       #viewLeft:I
    :cond_43
    iget-object v3, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@45
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3, v2, v4}, Landroid/webkit/WebView;->scrollBy(II)V

    #@4c
    .line 724
    const/4 v3, 0x0

    #@4d
    iput v3, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@4f
    goto :goto_2f
.end method


# virtual methods
.method public animateZoom(Landroid/graphics/Canvas;)V
    .registers 15
    .parameter "canvas"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v11, 0x0

    #@2
    .line 466
    iput-boolean v11, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@4
    .line 467
    iget v7, p0, Landroid/webkit/ZoomManager;->mZoomScale:F

    #@6
    cmpl-float v7, v7, v12

    #@8
    if-nez v7, :cond_15

    #@a
    .line 468
    const-string/jumbo v7, "webviewZoom"

    #@d
    const-string v8, "A WebView is attempting to perform a fixed length zoom animation when no zoom is in progress"

    #@f
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 471
    iput-boolean v11, p0, Landroid/webkit/ZoomManager;->mInHWAcceleratedZoom:Z

    #@14
    .line 522
    :cond_14
    :goto_14
    return-void

    #@15
    .line 476
    :cond_15
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@18
    move-result-wide v7

    #@19
    iget-wide v9, p0, Landroid/webkit/ZoomManager;->mZoomStart:J

    #@1b
    sub-long/2addr v7, v9

    #@1c
    long-to-int v0, v7

    #@1d
    .line 477
    .local v0, interval:I
    const/16 v7, 0xaf

    #@1f
    if-ge v0, v7, :cond_be

    #@21
    .line 478
    int-to-float v7, v0

    #@22
    const/high16 v8, 0x432f

    #@24
    div-float v1, v7, v8

    #@26
    .line 479
    .local v1, ratio:F
    const/high16 v7, 0x3f80

    #@28
    iget v8, p0, Landroid/webkit/ZoomManager;->mInvInitialZoomScale:F

    #@2a
    iget v9, p0, Landroid/webkit/ZoomManager;->mInvFinalZoomScale:F

    #@2c
    iget v10, p0, Landroid/webkit/ZoomManager;->mInvInitialZoomScale:F

    #@2e
    sub-float/2addr v9, v10

    #@2f
    mul-float/2addr v9, v1

    #@30
    add-float/2addr v8, v9

    #@31
    div-float v6, v7, v8

    #@33
    .line 481
    .local v6, zoomScale:F
    iget-object v7, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@35
    invoke-virtual {v7}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@38
    .line 490
    .end local v1           #ratio:F
    :goto_38
    iget v7, p0, Landroid/webkit/ZoomManager;->mInvInitialZoomScale:F

    #@3a
    mul-float v2, v6, v7

    #@3c
    .line 491
    .local v2, scale:F
    iget v7, p0, Landroid/webkit/ZoomManager;->mInitialScrollX:I

    #@3e
    int-to-float v7, v7

    #@3f
    iget v8, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@41
    add-float/2addr v7, v8

    #@42
    mul-float/2addr v7, v2

    #@43
    iget v8, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@45
    sub-float/2addr v7, v8

    #@46
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    #@49
    move-result v4

    #@4a
    .line 492
    .local v4, tx:I
    iget-object v7, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@4c
    invoke-virtual {v7}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@4f
    move-result v7

    #@50
    iget-object v8, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@52
    invoke-virtual {v8}, Landroid/webkit/WebViewClassic;->getContentWidth()I

    #@55
    move-result v8

    #@56
    int-to-float v8, v8

    #@57
    mul-float/2addr v8, v6

    #@58
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    #@5b
    move-result v8

    #@5c
    invoke-static {v4, v7, v8}, Landroid/webkit/WebViewClassic;->pinLoc(III)I

    #@5f
    move-result v7

    #@60
    neg-int v7, v7

    #@61
    iget-object v8, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@63
    invoke-virtual {v8}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@66
    move-result v8

    #@67
    add-int v4, v7, v8

    #@69
    .line 494
    iget-object v7, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@6b
    invoke-virtual {v7}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@6e
    move-result v3

    #@6f
    .line 495
    .local v3, titleHeight:I
    iget v7, p0, Landroid/webkit/ZoomManager;->mInitialScrollY:I

    #@71
    int-to-float v7, v7

    #@72
    iget v8, p0, Landroid/webkit/ZoomManager;->mZoomCenterY:F

    #@74
    add-float/2addr v7, v8

    #@75
    int-to-float v8, v3

    #@76
    sub-float/2addr v7, v8

    #@77
    mul-float/2addr v7, v2

    #@78
    iget v8, p0, Landroid/webkit/ZoomManager;->mZoomCenterY:F

    #@7a
    int-to-float v9, v3

    #@7b
    sub-float/2addr v8, v9

    #@7c
    sub-float/2addr v7, v8

    #@7d
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    #@80
    move-result v5

    #@81
    .line 498
    .local v5, ty:I
    if-gt v5, v3, :cond_c9

    #@83
    invoke-static {v5, v11}, Ljava/lang/Math;->max(II)I

    #@86
    move-result v7

    #@87
    :goto_87
    neg-int v7, v7

    #@88
    iget-object v8, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@8a
    invoke-virtual {v8}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@8d
    move-result v8

    #@8e
    add-int v5, v7, v8

    #@90
    .line 502
    iget-boolean v7, p0, Landroid/webkit/ZoomManager;->mHardwareAccelerated:Z

    #@92
    if-eqz v7, :cond_e3

    #@94
    .line 503
    iget-object v7, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@96
    iget-object v8, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@98
    invoke-virtual {v8}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@9b
    move-result v8

    #@9c
    sub-int/2addr v8, v4

    #@9d
    iget-object v9, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@9f
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@a2
    move-result v9

    #@a3
    sub-int/2addr v9, v5

    #@a4
    invoke-virtual {v7, v8, v9}, Landroid/webkit/WebViewClassic;->updateScrollCoordinates(II)Z

    #@a7
    .line 506
    int-to-float v7, v4

    #@a8
    int-to-float v8, v5

    #@a9
    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    #@ac
    .line 507
    invoke-virtual {p0, v6, v11}, Landroid/webkit/ZoomManager;->setZoomScale(FZ)V

    #@af
    .line 509
    iget v7, p0, Landroid/webkit/ZoomManager;->mZoomScale:F

    #@b1
    cmpl-float v7, v7, v12

    #@b3
    if-nez v7, :cond_14

    #@b5
    .line 511
    iput-boolean v11, p0, Landroid/webkit/ZoomManager;->mInHWAcceleratedZoom:Z

    #@b7
    .line 516
    iget-object v7, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@b9
    invoke-virtual {v7, v11}, Landroid/webkit/WebViewClassic;->sendViewSizeZoom(Z)Z

    #@bc
    goto/16 :goto_14

    #@be
    .line 483
    .end local v2           #scale:F
    .end local v3           #titleHeight:I
    .end local v4           #tx:I
    .end local v5           #ty:I
    .end local v6           #zoomScale:F
    :cond_be
    iget v6, p0, Landroid/webkit/ZoomManager;->mZoomScale:F

    #@c0
    .line 485
    .restart local v6       #zoomScale:F
    iput v12, p0, Landroid/webkit/ZoomManager;->mZoomScale:F

    #@c2
    .line 486
    iget-object v7, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@c4
    invoke-virtual {v7}, Landroid/webkit/WebViewClassic;->onFixedLengthZoomAnimationEnd()V

    #@c7
    goto/16 :goto_38

    #@c9
    .line 498
    .restart local v2       #scale:F
    .restart local v3       #titleHeight:I
    .restart local v4       #tx:I
    .restart local v5       #ty:I
    :cond_c9
    sub-int v7, v5, v3

    #@cb
    iget-object v8, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@cd
    invoke-virtual {v8}, Landroid/webkit/WebViewClassic;->getViewHeight()I

    #@d0
    move-result v8

    #@d1
    iget-object v9, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@d3
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->getContentHeight()I

    #@d6
    move-result v9

    #@d7
    int-to-float v9, v9

    #@d8
    mul-float/2addr v9, v6

    #@d9
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@dc
    move-result v9

    #@dd
    invoke-static {v7, v8, v9}, Landroid/webkit/WebViewClassic;->pinLoc(III)I

    #@e0
    move-result v7

    #@e1
    add-int/2addr v7, v3

    #@e2
    goto :goto_87

    #@e3
    .line 519
    :cond_e3
    int-to-float v7, v4

    #@e4
    int-to-float v8, v5

    #@e5
    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    #@e8
    .line 520
    invoke-virtual {p1, v6, v6}, Landroid/graphics/Canvas;->scale(FF)V

    #@eb
    goto/16 :goto_14
.end method

.method public final canZoomIn()Z
    .registers 3

    #@0
    .prologue
    .line 388
    iget v0, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@2
    iget v1, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@4
    sub-float/2addr v0, v1

    #@5
    sget v1, Landroid/webkit/ZoomManager;->MINIMUM_SCALE_INCREMENT:F

    #@7
    cmpl-float v0, v0, v1

    #@9
    if-lez v0, :cond_d

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public final canZoomOut()Z
    .registers 3

    #@0
    .prologue
    .line 392
    iget v0, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@2
    iget v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@4
    sub-float/2addr v0, v1

    #@5
    sget v1, Landroid/webkit/ZoomManager;->MINIMUM_SCALE_INCREMENT:F

    #@7
    cmpl-float v0, v0, v1

    #@9
    if-lez v0, :cond_d

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public final clearDocumentAnchor()V
    .registers 2

    #@0
    .prologue
    .line 350
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/webkit/ZoomManager;->mAnchorY:I

    #@3
    iput v0, p0, Landroid/webkit/ZoomManager;->mAnchorX:I

    #@5
    .line 351
    return-void
.end method

.method final computeReadingLevelScale(F)F
    .registers 4
    .parameter "scale"

    #@0
    .prologue
    .line 325
    iget v0, p0, Landroid/webkit/ZoomManager;->mDisplayDensity:F

    #@2
    iget v1, p0, Landroid/webkit/ZoomManager;->mDoubleTapZoomFactor:F

    #@4
    mul-float/2addr v0, v1

    #@5
    sget v1, Landroid/webkit/ZoomManager;->MIN_DOUBLE_TAP_SCALE_INCREMENT:F

    #@7
    add-float/2addr v1, p1

    #@8
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public final computeScaleWithLimits(F)F
    .registers 3
    .parameter "scale"

    #@0
    .prologue
    .line 363
    iget v0, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@2
    cmpg-float v0, p1, v0

    #@4
    if-gez v0, :cond_9

    #@6
    .line 364
    iget p1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@8
    .line 368
    :cond_8
    :goto_8
    return p1

    #@9
    .line 365
    :cond_9
    iget v0, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@b
    cmpl-float v0, p1, v0

    #@d
    if-lez v0, :cond_8

    #@f
    .line 366
    iget p1, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@11
    goto :goto_8
.end method

.method public dismissZoomPicker()V
    .registers 2

    #@0
    .prologue
    .line 1245
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->getCurrentZoomControl()Landroid/webkit/ZoomControlBase;

    #@3
    move-result-object v0

    #@4
    .line 1246
    .local v0, control:Landroid/webkit/ZoomControlBase;
    if-eqz v0, :cond_9

    #@6
    .line 1247
    invoke-interface {v0}, Landroid/webkit/ZoomControlBase;->hide()V

    #@9
    .line 1249
    :cond_9
    return-void
.end method

.method public final getDefaultMaxZoomScale()F
    .registers 2

    #@0
    .prologue
    .line 334
    iget v0, p0, Landroid/webkit/ZoomManager;->mDefaultMaxZoomScale:F

    #@2
    return v0
.end method

.method public final getDefaultMinZoomScale()F
    .registers 2

    #@0
    .prologue
    .line 338
    iget v0, p0, Landroid/webkit/ZoomManager;->mDefaultMinZoomScale:F

    #@2
    return v0
.end method

.method public final getDefaultScale()F
    .registers 2

    #@0
    .prologue
    .line 314
    iget v0, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@2
    return v0
.end method

.method public final getDocumentAnchorX()I
    .registers 2

    #@0
    .prologue
    .line 342
    iget v0, p0, Landroid/webkit/ZoomManager;->mAnchorX:I

    #@2
    return v0
.end method

.method public final getDocumentAnchorY()I
    .registers 2

    #@0
    .prologue
    .line 346
    iget v0, p0, Landroid/webkit/ZoomManager;->mAnchorY:I

    #@2
    return v0
.end method

.method public getExternalZoomPicker()Landroid/view/View;
    .registers 3

    #@0
    .prologue
    .line 1276
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->getCurrentZoomControl()Landroid/webkit/ZoomControlBase;

    #@3
    move-result-object v0

    #@4
    .line 1277
    .local v0, control:Landroid/webkit/ZoomControlBase;
    if-eqz v0, :cond_11

    #@6
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mExternalZoomControl:Landroid/webkit/ZoomControlExternal;

    #@8
    if-ne v0, v1, :cond_11

    #@a
    .line 1278
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mExternalZoomControl:Landroid/webkit/ZoomControlExternal;

    #@c
    invoke-virtual {v1}, Landroid/webkit/ZoomControlExternal;->getControls()Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@f
    move-result-object v1

    #@10
    .line 1280
    :goto_10
    return-object v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method public final getInvDefaultScale()F
    .registers 2

    #@0
    .prologue
    .line 330
    iget v0, p0, Landroid/webkit/ZoomManager;->mInvDefaultScale:F

    #@2
    return v0
.end method

.method public final getInvScale()F
    .registers 2

    #@0
    .prologue
    .line 298
    iget v0, p0, Landroid/webkit/ZoomManager;->mInvActualScale:F

    #@2
    return v0
.end method

.method public final getMaxZoomScale()F
    .registers 2

    #@0
    .prologue
    .line 306
    iget v0, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@2
    return v0
.end method

.method public final getMinZoomScale()F
    .registers 2

    #@0
    .prologue
    .line 310
    iget v0, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@2
    return v0
.end method

.method public final getReadingLevelScale()F
    .registers 2

    #@0
    .prologue
    .line 321
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getZoomOverviewScale()F

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/webkit/ZoomManager;->computeReadingLevelScale(F)F

    #@7
    move-result v0

    #@8
    invoke-virtual {p0, v0}, Landroid/webkit/ZoomManager;->computeScaleWithLimits(F)F

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public final getScale()F
    .registers 2

    #@0
    .prologue
    .line 294
    iget v0, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@2
    return v0
.end method

.method public getScaleGestureDetector()Landroid/view/ScaleGestureDetector;
    .registers 2

    #@0
    .prologue
    .line 777
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    #@2
    return-object v0
.end method

.method public final getTextWrapScale()F
    .registers 2

    #@0
    .prologue
    .line 302
    iget v0, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@2
    return v0
.end method

.method getZoomOverviewScale()F
    .registers 3

    #@0
    .prologue
    .line 693
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@5
    move-result v0

    #@6
    int-to-float v0, v0

    #@7
    iget v1, p0, Landroid/webkit/ZoomManager;->mInvZoomOverviewWidth:F

    #@9
    mul-float/2addr v0, v1

    #@a
    return v0
.end method

.method public handleDoubleTap(FF)V
    .registers 10
    .parameter "lastTouchX"
    .parameter "lastTouchY"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 638
    iput-boolean v0, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@4
    .line 639
    iget-object v4, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@6
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@9
    move-result-object v2

    #@a
    .line 640
    .local v2, settings:Landroid/webkit/WebSettingsClassic;
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->isDoubleTapEnabled()Z

    #@d
    move-result v4

    #@e
    if-nez v4, :cond_11

    #@10
    .line 681
    :goto_10
    return-void

    #@11
    .line 644
    :cond_11
    invoke-virtual {p0, p1, p2}, Landroid/webkit/ZoomManager;->setZoomCenter(FF)V

    #@14
    .line 645
    iget-object v4, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@16
    float-to-int v5, p1

    #@17
    iget-object v6, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@19
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@1c
    move-result v6

    #@1d
    add-int/2addr v5, v6

    #@1e
    invoke-virtual {v4, v5}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@21
    move-result v4

    #@22
    iput v4, p0, Landroid/webkit/ZoomManager;->mAnchorX:I

    #@24
    .line 646
    iget-object v4, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@26
    float-to-int v5, p2

    #@27
    iget-object v6, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@29
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@2c
    move-result v6

    #@2d
    add-int/2addr v5, v6

    #@2e
    invoke-virtual {v4, v5}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@31
    move-result v4

    #@32
    iput v4, p0, Landroid/webkit/ZoomManager;->mAnchorY:I

    #@34
    .line 647
    invoke-virtual {v2, v0}, Landroid/webkit/WebSettingsClassic;->setDoubleTapToastCount(I)V

    #@37
    .line 650
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->dismissZoomPicker()V

    #@3a
    .line 653
    invoke-virtual {v2}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@3d
    move-result v4

    #@3e
    if-eqz v4, :cond_77

    #@40
    .line 654
    iget v4, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@42
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@45
    move-result v5

    #@46
    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    #@49
    move-result v1

    #@4a
    .line 658
    .local v1, newTextWrapScale:F
    :goto_4a
    iget v4, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@4c
    iget v5, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@4e
    invoke-static {v4, v5}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@51
    move-result v4

    #@52
    if-nez v4, :cond_55

    #@54
    move v0, v3

    #@55
    .line 659
    .local v0, firstTimeReflow:Z
    :cond_55
    if-nez v0, :cond_5b

    #@57
    iget-boolean v4, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@59
    if-eqz v4, :cond_5d

    #@5b
    .line 662
    :cond_5b
    iput v1, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@5d
    .line 664
    :cond_5d
    invoke-virtual {v2}, Landroid/webkit/WebSettingsClassic;->isNarrowColumnLayout()Z

    #@60
    move-result v4

    #@61
    if-eqz v4, :cond_7a

    #@63
    iget v4, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@65
    invoke-static {v4, v1}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@68
    move-result v4

    #@69
    if-eqz v4, :cond_7a

    #@6b
    if-nez v0, :cond_7a

    #@6d
    iget-boolean v4, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@6f
    if-nez v4, :cond_7a

    #@71
    .line 669
    iput v1, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@73
    .line 670
    invoke-virtual {p0, v3}, Landroid/webkit/ZoomManager;->refreshZoomScale(Z)V

    #@76
    goto :goto_10

    #@77
    .line 656
    .end local v0           #firstTimeReflow:Z
    .end local v1           #newTextWrapScale:F
    :cond_77
    iget v1, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@79
    .restart local v1       #newTextWrapScale:F
    goto :goto_4a

    #@7a
    .line 671
    .restart local v0       #firstTimeReflow:Z
    :cond_7a
    iget-boolean v4, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@7c
    if-nez v4, :cond_a0

    #@7e
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getZoomOverviewScale()F

    #@81
    move-result v4

    #@82
    invoke-virtual {p0, v4}, Landroid/webkit/ZoomManager;->willScaleTriggerZoom(F)Z

    #@85
    move-result v4

    #@86
    if-eqz v4, :cond_a0

    #@88
    .line 673
    iget v4, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@8a
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@8d
    move-result v5

    #@8e
    cmpl-float v4, v4, v5

    #@90
    if-lez v4, :cond_9b

    #@92
    .line 674
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@95
    move-result v4

    #@96
    iput v4, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@98
    .line 675
    invoke-virtual {p0, v3}, Landroid/webkit/ZoomManager;->refreshZoomScale(Z)V

    #@9b
    .line 677
    :cond_9b
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->zoomToOverview()V

    #@9e
    goto/16 :goto_10

    #@a0
    .line 679
    :cond_a0
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->zoomToReadingLevel()V

    #@a3
    goto/16 :goto_10
.end method

.method public init(F)V
    .registers 3
    .parameter "density"

    #@0
    .prologue
    .line 236
    sget-boolean v0, Landroid/webkit/ZoomManager;->$assertionsDisabled:Z

    #@2
    if-nez v0, :cond_f

    #@4
    const/4 v0, 0x0

    #@5
    cmpl-float v0, p1, v0

    #@7
    if-gtz v0, :cond_f

    #@9
    new-instance v0, Ljava/lang/AssertionError;

    #@b
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@e
    throw v0

    #@f
    .line 238
    :cond_f
    iput p1, p0, Landroid/webkit/ZoomManager;->mDisplayDensity:F

    #@11
    .line 239
    invoke-direct {p0, p1}, Landroid/webkit/ZoomManager;->setDefaultZoomScale(F)V

    #@14
    .line 240
    iput p1, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@16
    .line 241
    const/high16 v0, 0x3f80

    #@18
    div-float/2addr v0, p1

    #@19
    iput v0, p0, Landroid/webkit/ZoomManager;->mInvActualScale:F

    #@1b
    .line 242
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@21
    .line 243
    return-void
.end method

.method public invokeZoomPicker()V
    .registers 2

    #@0
    .prologue
    .line 1238
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->getCurrentZoomControl()Landroid/webkit/ZoomControlBase;

    #@3
    move-result-object v0

    #@4
    .line 1239
    .local v0, control:Landroid/webkit/ZoomControlBase;
    if-eqz v0, :cond_9

    #@6
    .line 1240
    invoke-interface {v0}, Landroid/webkit/ZoomControlBase;->show()V

    #@9
    .line 1242
    :cond_9
    return-void
.end method

.method public isDoubleTapEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 616
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@5
    move-result-object v0

    #@6
    .line 617
    .local v0, settings:Landroid/webkit/WebSettings;
    if-eqz v0, :cond_10

    #@8
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_10

    #@e
    const/4 v1, 0x1

    #@f
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method public isFixedLengthAnimationInProgress()Z
    .registers 3

    #@0
    .prologue
    .line 529
    iget v0, p0, Landroid/webkit/ZoomManager;->mZoomScale:F

    #@2
    const/4 v1, 0x0

    #@3
    cmpl-float v0, v0, v1

    #@5
    if-nez v0, :cond_b

    #@7
    iget-boolean v0, p0, Landroid/webkit/ZoomManager;->mInHWAcceleratedZoom:Z

    #@9
    if-eqz v0, :cond_d

    #@b
    :cond_b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public isInZoomOverview()Z
    .registers 2

    #@0
    .prologue
    .line 697
    iget-boolean v0, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@2
    return v0
.end method

.method public isPreventingWebkitUpdates()Z
    .registers 2

    #@0
    .prologue
    .line 773
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->isZoomAnimating()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public final isScaleOverLimits(F)Z
    .registers 3
    .parameter "scale"

    #@0
    .prologue
    .line 372
    iget v0, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@2
    cmpg-float v0, p1, v0

    #@4
    if-lez v0, :cond_c

    #@6
    iget v0, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@8
    cmpl-float v0, p1, v0

    #@a
    if-ltz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isZoomAnimating()Z
    .registers 2

    #@0
    .prologue
    .line 525
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->isFixedLengthAnimationInProgress()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    iget-boolean v0, p0, Landroid/webkit/ZoomManager;->mPinchToZoomAnimating:Z

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isZoomPickerVisible()Z
    .registers 3

    #@0
    .prologue
    .line 1252
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->getCurrentZoomControl()Landroid/webkit/ZoomControlBase;

    #@3
    move-result-object v0

    #@4
    .line 1253
    .local v0, control:Landroid/webkit/ZoomControlBase;
    if-eqz v0, :cond_b

    #@6
    invoke-interface {v0}, Landroid/webkit/ZoomControlBase;->isVisible()Z

    #@9
    move-result v1

    #@a
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public final isZoomScaleFixed()Z
    .registers 3

    #@0
    .prologue
    .line 376
    iget v0, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@2
    iget v1, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@4
    cmpl-float v0, v0, v1

    #@6
    if-ltz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public keepZoomPickerVisible()V
    .registers 3

    #@0
    .prologue
    .line 1269
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->getCurrentZoomControl()Landroid/webkit/ZoomControlBase;

    #@3
    move-result-object v0

    #@4
    .line 1270
    .local v0, control:Landroid/webkit/ZoomControlBase;
    if-eqz v0, :cond_d

    #@6
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mExternalZoomControl:Landroid/webkit/ZoomControlExternal;

    #@8
    if-ne v0, v1, :cond_d

    #@a
    .line 1271
    invoke-interface {v0}, Landroid/webkit/ZoomControlBase;->show()V

    #@d
    .line 1273
    :cond_d
    return-void
.end method

.method public onFirstLayout(Landroid/webkit/WebViewCore$DrawData;)V
    .registers 11
    .parameter "drawData"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1145
    sget-boolean v6, Landroid/webkit/ZoomManager;->$assertionsDisabled:Z

    #@3
    if-nez v6, :cond_d

    #@5
    if-nez p1, :cond_d

    #@7
    new-instance v6, Ljava/lang/AssertionError;

    #@9
    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    #@c
    throw v6

    #@d
    .line 1146
    :cond_d
    sget-boolean v6, Landroid/webkit/ZoomManager;->$assertionsDisabled:Z

    #@f
    if-nez v6, :cond_1b

    #@11
    iget-object v6, p1, Landroid/webkit/WebViewCore$DrawData;->mViewState:Landroid/webkit/WebViewCore$ViewState;

    #@13
    if-nez v6, :cond_1b

    #@15
    new-instance v6, Ljava/lang/AssertionError;

    #@17
    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    #@1a
    throw v6

    #@1b
    .line 1147
    :cond_1b
    sget-boolean v6, Landroid/webkit/ZoomManager;->$assertionsDisabled:Z

    #@1d
    if-nez v6, :cond_2d

    #@1f
    iget-object v6, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@21
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@24
    move-result-object v6

    #@25
    if-nez v6, :cond_2d

    #@27
    new-instance v6, Ljava/lang/AssertionError;

    #@29
    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    #@2c
    throw v6

    #@2d
    .line 1149
    :cond_2d
    iget-object v5, p1, Landroid/webkit/WebViewCore$DrawData;->mViewState:Landroid/webkit/WebViewCore$ViewState;

    #@2f
    .line 1150
    .local v5, viewState:Landroid/webkit/WebViewCore$ViewState;
    iget-object v4, p1, Landroid/webkit/WebViewCore$DrawData;->mViewSize:Landroid/graphics/Point;

    #@31
    .line 1151
    .local v4, viewSize:Landroid/graphics/Point;
    iget v6, v4, Landroid/graphics/Point;->x:I

    #@33
    iget v7, p1, Landroid/webkit/WebViewCore$DrawData;->mMinPrefWidth:I

    #@35
    invoke-virtual {p0, v5, v6, v7}, Landroid/webkit/ZoomManager;->updateZoomRange(Landroid/webkit/WebViewCore$ViewState;II)V

    #@38
    .line 1152
    iget-object v6, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@3a
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@3d
    move-result v6

    #@3e
    invoke-direct {p0, p1, v6}, Landroid/webkit/ZoomManager;->setupZoomOverviewWidth(Landroid/webkit/WebViewCore$DrawData;I)Z

    #@41
    .line 1153
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getZoomOverviewScale()F

    #@44
    move-result v0

    #@45
    .line 1154
    .local v0, overviewScale:F
    iget-object v6, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@47
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@4a
    move-result-object v3

    #@4b
    .line 1155
    .local v3, settings:Landroid/webkit/WebSettingsClassic;
    iget-boolean v6, p0, Landroid/webkit/ZoomManager;->mMinZoomScaleFixed:Z

    #@4d
    if-eqz v6, :cond_55

    #@4f
    invoke-virtual {v3}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@52
    move-result v6

    #@53
    if-eqz v6, :cond_70

    #@55
    .line 1156
    :cond_55
    iget v6, p0, Landroid/webkit/ZoomManager;->mInitialScale:F

    #@57
    cmpl-float v6, v6, v8

    #@59
    if-lez v6, :cond_b3

    #@5b
    iget v6, p0, Landroid/webkit/ZoomManager;->mInitialScale:F

    #@5d
    invoke-static {v6, v0}, Ljava/lang/Math;->min(FF)F

    #@60
    move-result v6

    #@61
    :goto_61
    iput v6, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@63
    .line 1158
    iget v6, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@65
    iget v7, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@67
    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    #@6a
    move-result v6

    #@6b
    iput v6, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@6d
    .line 1159
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->sanitizeMinMaxScales()V

    #@70
    .line 1162
    :cond_70
    iget-object v6, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@72
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->drawHistory()Z

    #@75
    move-result v6

    #@76
    if-nez v6, :cond_b2

    #@78
    .line 1164
    iget v6, p0, Landroid/webkit/ZoomManager;->mInitialScale:F

    #@7a
    cmpl-float v6, v6, v8

    #@7c
    if-lez v6, :cond_b5

    #@7e
    .line 1165
    iget v2, p0, Landroid/webkit/ZoomManager;->mInitialScale:F

    #@80
    .line 1184
    .local v2, scale:F
    :cond_80
    :goto_80
    const/4 v1, 0x0

    #@81
    .line 1185
    .local v1, reflowText:Z
    iget-boolean v6, v5, Landroid/webkit/WebViewCore$ViewState;->mIsRestored:Z

    #@83
    if-nez v6, :cond_9d

    #@85
    .line 1186
    invoke-virtual {v3}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@88
    move-result v6

    #@89
    if-eqz v6, :cond_97

    #@8b
    .line 1188
    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    #@8e
    move-result v2

    #@8f
    .line 1189
    iget v6, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@91
    invoke-static {v6, v0}, Ljava/lang/Math;->max(FF)F

    #@94
    move-result v6

    #@95
    iput v6, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@97
    .line 1191
    :cond_97
    iget v6, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@99
    invoke-static {v6, v2}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@9c
    move-result v1

    #@9d
    .line 1193
    :cond_9d
    invoke-virtual {v3}, Landroid/webkit/WebSettingsClassic;->getLoadWithOverviewMode()Z

    #@a0
    move-result v6

    #@a1
    if-eqz v6, :cond_ff

    #@a3
    invoke-static {v2, v0}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@a6
    move-result v6

    #@a7
    if-nez v6, :cond_ff

    #@a9
    const/4 v6, 0x1

    #@aa
    :goto_aa
    iput-boolean v6, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@ac
    .line 1195
    invoke-virtual {p0, v2, v1}, Landroid/webkit/ZoomManager;->setZoomScale(FZ)V

    #@af
    .line 1198
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->updateZoomPicker()V

    #@b2
    .line 1200
    .end local v1           #reflowText:Z
    .end local v2           #scale:F
    :cond_b2
    return-void

    #@b3
    :cond_b3
    move v6, v0

    #@b4
    .line 1156
    goto :goto_61

    #@b5
    .line 1166
    :cond_b5
    iget-boolean v6, v5, Landroid/webkit/WebViewCore$ViewState;->mIsRestored:Z

    #@b7
    if-nez v6, :cond_bf

    #@b9
    iget v6, v5, Landroid/webkit/WebViewCore$ViewState;->mViewScale:F

    #@bb
    cmpl-float v6, v6, v8

    #@bd
    if-lez v6, :cond_d9

    #@bf
    .line 1167
    :cond_bf
    iget v6, v5, Landroid/webkit/WebViewCore$ViewState;->mViewScale:F

    #@c1
    cmpl-float v6, v6, v8

    #@c3
    if-lez v6, :cond_d2

    #@c5
    iget v2, v5, Landroid/webkit/WebViewCore$ViewState;->mViewScale:F

    #@c7
    .line 1169
    .restart local v2       #scale:F
    :goto_c7
    iget v6, v5, Landroid/webkit/WebViewCore$ViewState;->mTextWrapScale:F

    #@c9
    cmpl-float v6, v6, v8

    #@cb
    if-lez v6, :cond_d4

    #@cd
    iget v6, v5, Landroid/webkit/WebViewCore$ViewState;->mTextWrapScale:F

    #@cf
    :goto_cf
    iput v6, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@d1
    goto :goto_80

    #@d2
    .end local v2           #scale:F
    :cond_d2
    move v2, v0

    #@d3
    .line 1167
    goto :goto_c7

    #@d4
    .line 1169
    .restart local v2       #scale:F
    :cond_d4
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@d7
    move-result v6

    #@d8
    goto :goto_cf

    #@d9
    .line 1172
    .end local v2           #scale:F
    :cond_d9
    move v2, v0

    #@da
    .line 1173
    .restart local v2       #scale:F
    invoke-virtual {v3}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@dd
    move-result v6

    #@de
    if-eqz v6, :cond_e6

    #@e0
    invoke-virtual {v3}, Landroid/webkit/WebSettingsClassic;->getLoadWithOverviewMode()Z

    #@e3
    move-result v6

    #@e4
    if-nez v6, :cond_ec

    #@e6
    .line 1175
    :cond_e6
    iget v6, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@e8
    invoke-static {v6, v2}, Ljava/lang/Math;->max(FF)F

    #@eb
    move-result v2

    #@ec
    .line 1177
    :cond_ec
    invoke-virtual {v3}, Landroid/webkit/WebSettingsClassic;->isNarrowColumnLayout()Z

    #@ef
    move-result v6

    #@f0
    if-eqz v6, :cond_80

    #@f2
    invoke-virtual {v3}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@f5
    move-result v6

    #@f6
    if-eqz v6, :cond_80

    #@f8
    .line 1181
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@fb
    move-result v6

    #@fc
    iput v6, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@fe
    goto :goto_80

    #@ff
    .line 1193
    .restart local v1       #reflowText:Z
    :cond_ff
    const/4 v6, 0x0

    #@100
    goto :goto_aa
.end method

.method public onNewPicture(Landroid/webkit/WebViewCore$DrawData;)Z
    .registers 13
    .parameter "drawData"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 1048
    iget-object v9, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@4
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@7
    move-result v5

    #@8
    .line 1049
    .local v5, viewWidth:I
    invoke-direct {p0, p1, v5}, Landroid/webkit/ZoomManager;->setupZoomOverviewWidth(Landroid/webkit/WebViewCore$DrawData;I)Z

    #@b
    move-result v6

    #@c
    .line 1050
    .local v6, zoomOverviewWidthChanged:Z
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getZoomOverviewScale()F

    #@f
    move-result v1

    #@10
    .line 1051
    .local v1, newZoomOverviewScale:F
    iget-object v9, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@12
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@15
    move-result-object v4

    #@16
    .line 1052
    .local v4, settings:Landroid/webkit/WebSettingsClassic;
    if-eqz v6, :cond_44

    #@18
    invoke-virtual {v4}, Landroid/webkit/WebSettingsClassic;->isNarrowColumnLayout()Z

    #@1b
    move-result v9

    #@1c
    if-eqz v9, :cond_44

    #@1e
    invoke-virtual {v4}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@21
    move-result v9

    #@22
    if-eqz v9, :cond_44

    #@24
    iget-boolean v9, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@26
    if-nez v9, :cond_2c

    #@28
    iget-boolean v9, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@2a
    if-eqz v9, :cond_44

    #@2c
    .line 1057
    :cond_2c
    iget v9, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@2e
    iget v10, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@30
    invoke-static {v9, v10}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@33
    move-result v9

    #@34
    if-nez v9, :cond_3e

    #@36
    iget v9, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@38
    invoke-static {v1, v9}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@3b
    move-result v9

    #@3c
    if-eqz v9, :cond_bd

    #@3e
    .line 1059
    :cond_3e
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@41
    move-result v9

    #@42
    iput v9, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@44
    .line 1065
    :cond_44
    :goto_44
    iget-boolean v9, p0, Landroid/webkit/ZoomManager;->mMinZoomScaleFixed:Z

    #@46
    if-eqz v9, :cond_4e

    #@48
    invoke-virtual {v4}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@4b
    move-result v9

    #@4c
    if-eqz v9, :cond_5d

    #@4e
    .line 1066
    :cond_4e
    iput v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@50
    .line 1067
    iget v9, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@52
    iget v10, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@54
    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    #@57
    move-result v9

    #@58
    iput v9, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@5a
    .line 1068
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->sanitizeMinMaxScales()V

    #@5d
    .line 1072
    :cond_5d
    iget v9, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@5f
    invoke-static {v1, v9}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@62
    move-result v2

    #@63
    .line 1074
    .local v2, scaleHasDiff:Z
    iget v9, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@65
    sub-float v9, v1, v9

    #@67
    sget v10, Landroid/webkit/ZoomManager;->MINIMUM_SCALE_INCREMENT:F

    #@69
    cmpl-float v9, v9, v10

    #@6b
    if-ltz v9, :cond_c0

    #@6d
    move v3, v7

    #@6e
    .line 1078
    .local v3, scaleLessThanOverview:Z
    :goto_6e
    iget-boolean v9, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@70
    if-eqz v9, :cond_c2

    #@72
    iget v9, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@74
    invoke-static {v1, v9}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@77
    move-result v9

    #@78
    if-nez v9, :cond_c2

    #@7a
    move v0, v7

    #@7b
    .line 1080
    .local v0, mobileSiteInOverview:Z
    :goto_7b
    iget-object v9, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@7d
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->drawHistory()Z

    #@80
    move-result v9

    #@81
    if-nez v9, :cond_c6

    #@83
    if-eqz v3, :cond_8b

    #@85
    invoke-virtual {v4}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@88
    move-result v9

    #@89
    if-nez v9, :cond_95

    #@8b
    :cond_8b
    iget-boolean v9, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@8d
    if-nez v9, :cond_91

    #@8f
    if-eqz v0, :cond_c6

    #@91
    :cond_91
    if-eqz v2, :cond_c6

    #@93
    if-eqz v6, :cond_c6

    #@95
    .line 1084
    :cond_95
    iput-boolean v8, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@97
    .line 1085
    iget v9, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@99
    invoke-virtual {p0, v9}, Landroid/webkit/ZoomManager;->willScaleTriggerZoom(F)Z

    #@9c
    move-result v9

    #@9d
    if-nez v9, :cond_c4

    #@9f
    iget-object v9, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@a1
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@a4
    move-result-object v9

    #@a5
    invoke-virtual {v9}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@a8
    move-result v9

    #@a9
    if-nez v9, :cond_c4

    #@ab
    :goto_ab
    invoke-virtual {p0, v1, v7}, Landroid/webkit/ZoomManager;->setZoomScale(FZ)V

    #@ae
    .line 1090
    :goto_ae
    iget-boolean v7, p1, Landroid/webkit/WebViewCore$DrawData;->mFirstLayoutForNonStandardLoad:Z

    #@b0
    if-eqz v7, :cond_bc

    #@b2
    invoke-virtual {v4}, Landroid/webkit/WebSettingsClassic;->getLoadWithOverviewMode()Z

    #@b5
    move-result v7

    #@b6
    if-eqz v7, :cond_bc

    #@b8
    .line 1093
    iget-boolean v7, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@ba
    iput-boolean v7, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@bc
    .line 1096
    :cond_bc
    return v2

    #@bd
    .line 1061
    .end local v0           #mobileSiteInOverview:Z
    .end local v2           #scaleHasDiff:Z
    .end local v3           #scaleLessThanOverview:Z
    :cond_bd
    iput v1, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@bf
    goto :goto_44

    #@c0
    .restart local v2       #scaleHasDiff:Z
    :cond_c0
    move v3, v8

    #@c1
    .line 1074
    goto :goto_6e

    #@c2
    .restart local v3       #scaleLessThanOverview:Z
    :cond_c2
    move v0, v8

    #@c3
    .line 1078
    goto :goto_7b

    #@c4
    .restart local v0       #mobileSiteInOverview:Z
    :cond_c4
    move v7, v8

    #@c5
    .line 1085
    goto :goto_ab

    #@c6
    .line 1088
    :cond_c6
    if-nez v2, :cond_cb

    #@c8
    :goto_c8
    iput-boolean v7, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@ca
    goto :goto_ae

    #@cb
    :cond_cb
    move v7, v8

    #@cc
    goto :goto_c8
.end method

.method onPageFinished(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 1293
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@3
    .line 1294
    return-void
.end method

.method public onSizeChanged(IIII)V
    .registers 12
    .parameter "w"
    .parameter "h"
    .parameter "ow"
    .parameter "oh"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    .line 929
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@5
    if-eqz v1, :cond_f

    #@7
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@9
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@c
    move-result-object v1

    #@d
    if-nez v1, :cond_10

    #@f
    .line 971
    :cond_f
    :goto_f
    return-void

    #@10
    .line 934
    :cond_10
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->isFixedLengthAnimationInProgress()Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_3e

    #@16
    .line 935
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@18
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getVisibleTitleHeight()I

    #@1b
    move-result v0

    #@1c
    .line 936
    .local v0, visibleTitleHeight:I
    iput v6, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@1e
    .line 937
    int-to-float v1, v0

    #@1f
    iput v1, p0, Landroid/webkit/ZoomManager;->mZoomCenterY:F

    #@21
    .line 938
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@23
    iget-object v4, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@25
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@28
    move-result v4

    #@29
    invoke-virtual {v1, v4}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@2c
    move-result v1

    #@2d
    iput v1, p0, Landroid/webkit/ZoomManager;->mAnchorX:I

    #@2f
    .line 939
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@31
    iget-object v4, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@33
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@36
    move-result v4

    #@37
    add-int/2addr v4, v0

    #@38
    invoke-virtual {v1, v4}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@3b
    move-result v1

    #@3c
    iput v1, p0, Landroid/webkit/ZoomManager;->mAnchorY:I

    #@3e
    .line 943
    .end local v0           #visibleTitleHeight:I
    :cond_3e
    iget-boolean v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScaleFixed:Z

    #@40
    if-nez v1, :cond_77

    #@42
    .line 948
    const/high16 v4, 0x3f80

    #@44
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@46
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@49
    move-result v1

    #@4a
    int-to-float v5, v1

    #@4b
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@4d
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->drawHistory()Z

    #@50
    move-result v1

    #@51
    if-eqz v1, :cond_a9

    #@53
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@55
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getHistoryPictureWidth()I

    #@58
    move-result v1

    #@59
    :goto_59
    int-to-float v1, v1

    #@5a
    div-float v1, v5, v1

    #@5c
    invoke-static {v4, v1}, Ljava/lang/Math;->min(FF)F

    #@5f
    move-result v1

    #@60
    iput v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@62
    .line 952
    iget v1, p0, Landroid/webkit/ZoomManager;->mInitialScale:F

    #@64
    cmpl-float v1, v1, v6

    #@66
    if-lez v1, :cond_74

    #@68
    iget v1, p0, Landroid/webkit/ZoomManager;->mInitialScale:F

    #@6a
    iget v4, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@6c
    cmpg-float v1, v1, v4

    #@6e
    if-gez v1, :cond_74

    #@70
    .line 953
    iget v1, p0, Landroid/webkit/ZoomManager;->mInitialScale:F

    #@72
    iput v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@74
    .line 955
    :cond_74
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->sanitizeMinMaxScales()V

    #@77
    .line 958
    :cond_77
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->dismissZoomPicker()V

    #@7a
    .line 966
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@7c
    if-eqz v1, :cond_f

    #@7e
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@80
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@83
    move-result-object v1

    #@84
    if-eqz v1, :cond_f

    #@86
    .line 967
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@88
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@8b
    move-result-object v4

    #@8c
    new-instance v5, Landroid/webkit/ZoomManager$PostScale;

    #@8e
    if-eq p1, p3, :cond_ac

    #@90
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@92
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@95
    move-result-object v1

    #@96
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getUseFixedViewport()Z

    #@99
    move-result v1

    #@9a
    if-nez v1, :cond_ac

    #@9c
    move v1, v2

    #@9d
    :goto_9d
    iget-boolean v6, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@9f
    if-ge p1, p3, :cond_ae

    #@a1
    :goto_a1
    invoke-direct {v5, p0, v1, v6, v2}, Landroid/webkit/ZoomManager$PostScale;-><init>(Landroid/webkit/ZoomManager;ZZZ)V

    #@a4
    invoke-virtual {v4, v5}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    #@a7
    goto/16 :goto_f

    #@a9
    .line 948
    :cond_a9
    iget v1, p0, Landroid/webkit/ZoomManager;->mZoomOverviewWidth:I

    #@ab
    goto :goto_59

    #@ac
    :cond_ac
    move v1, v3

    #@ad
    .line 967
    goto :goto_9d

    #@ae
    :cond_ae
    move v2, v3

    #@af
    goto :goto_a1
.end method

.method public refreshZoomScale(Z)V
    .registers 4
    .parameter "reflowText"

    #@0
    .prologue
    .line 542
    iget v0, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@2
    const/4 v1, 0x1

    #@3
    invoke-direct {p0, v0, p1, v1}, Landroid/webkit/ZoomManager;->setZoomScale(FZZ)V

    #@6
    .line 543
    return-void
.end method

.method public restoreZoomState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "b"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    .line 1212
    const-string/jumbo v0, "scale"

    #@5
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    #@8
    move-result v0

    #@9
    iput v0, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@b
    .line 1213
    iget v0, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@d
    div-float v0, v1, v0

    #@f
    iput v0, p0, Landroid/webkit/ZoomManager;->mInvActualScale:F

    #@11
    .line 1214
    const-string/jumbo v0, "textwrapScale"

    #@14
    iget v1, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@16
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    #@19
    move-result v0

    #@1a
    iput v0, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@1c
    .line 1215
    const-string/jumbo v0, "overview"

    #@1f
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@22
    move-result v0

    #@23
    iput-boolean v0, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@25
    .line 1216
    return-void
.end method

.method public saveZoomState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "b"

    #@0
    .prologue
    .line 1203
    const-string/jumbo v0, "scale"

    #@3
    iget v1, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@5
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@8
    .line 1204
    const-string/jumbo v0, "textwrapScale"

    #@b
    iget v1, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@d
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@10
    .line 1205
    const-string/jumbo v0, "overview"

    #@13
    iget-boolean v1, p0, Landroid/webkit/ZoomManager;->mInZoomOverview:Z

    #@15
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@18
    .line 1206
    return-void
.end method

.method public setCenterZoomScale(FII)V
    .registers 7
    .parameter "Scale"
    .parameter "ScrollX"
    .parameter "ScrollY"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/high16 v1, 0x3f00

    #@3
    .line 1303
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@5
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@8
    .line 1306
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@a
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@d
    move-result v0

    #@e
    int-to-float v0, v0

    #@f
    mul-float/2addr v0, v1

    #@10
    iput v0, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@12
    .line 1307
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@14
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getViewHeight()I

    #@17
    move-result v0

    #@18
    int-to-float v0, v0

    #@19
    mul-float/2addr v0, v1

    #@1a
    iput v0, p0, Landroid/webkit/ZoomManager;->mZoomCenterY:F

    #@1c
    .line 1308
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@1e
    iget v1, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@20
    float-to-int v1, v1

    #@21
    add-int/2addr v1, p2

    #@22
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->viewToContentX(I)I

    #@25
    move-result v0

    #@26
    iput v0, p0, Landroid/webkit/ZoomManager;->mAnchorX:I

    #@28
    .line 1309
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@2a
    iget v1, p0, Landroid/webkit/ZoomManager;->mZoomCenterY:F

    #@2c
    float-to-int v1, v1

    #@2d
    add-int/2addr v1, p3

    #@2e
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->viewToContentY(I)I

    #@31
    move-result v0

    #@32
    iput v0, p0, Landroid/webkit/ZoomManager;->mAnchorY:I

    #@34
    .line 1312
    iget v0, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@36
    cmpl-float v0, p1, v0

    #@38
    if-lez v0, :cond_3d

    #@3a
    .line 1313
    invoke-direct {p0, p1, v2, v2}, Landroid/webkit/ZoomManager;->setZoomScale(FZZ)V

    #@3d
    .line 1314
    :cond_3d
    iget-object v0, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@3f
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@42
    .line 1315
    return-void
.end method

.method public setHardwareAccelerated()V
    .registers 2

    #@0
    .prologue
    .line 1285
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/webkit/ZoomManager;->mHardwareAccelerated:Z

    #@3
    .line 1286
    return-void
.end method

.method public final setInitialScaleInPercent(I)V
    .registers 4
    .parameter "scaleInPercent"

    #@0
    .prologue
    .line 359
    int-to-float v0, p1

    #@1
    const v1, 0x3c23d70a

    #@4
    mul-float/2addr v0, v1

    #@5
    iput v0, p0, Landroid/webkit/ZoomManager;->mInitialScale:F

    #@7
    .line 360
    return-void
.end method

.method public final setZoomCenter(FF)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 354
    iput p1, p0, Landroid/webkit/ZoomManager;->mZoomCenterX:F

    #@2
    .line 355
    iput p2, p0, Landroid/webkit/ZoomManager;->mZoomCenterY:F

    #@4
    .line 356
    return-void
.end method

.method public setZoomScale(FZ)V
    .registers 4
    .parameter "scale"
    .parameter "reflowText"

    #@0
    .prologue
    .line 546
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/webkit/ZoomManager;->setZoomScale(FZZ)V

    #@4
    .line 547
    return-void
.end method

.method public startZoomAnimation(FZ)Z
    .registers 8
    .parameter "scale"
    .parameter "reflowText"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    const/high16 v4, 0x3f80

    #@4
    .line 423
    iput-boolean v2, p0, Landroid/webkit/ZoomManager;->mInitialZoomOverview:Z

    #@6
    .line 424
    iget v0, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@8
    .line 425
    .local v0, oldScale:F
    iget-object v3, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@a
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@d
    move-result v3

    #@e
    iput v3, p0, Landroid/webkit/ZoomManager;->mInitialScrollX:I

    #@10
    .line 426
    iget-object v3, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@12
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@15
    move-result v3

    #@16
    iput v3, p0, Landroid/webkit/ZoomManager;->mInitialScrollY:I

    #@18
    .line 429
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@1b
    move-result v3

    #@1c
    invoke-static {p1, v3}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@1f
    move-result v3

    #@20
    if-nez v3, :cond_26

    #@22
    .line 430
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@25
    move-result p1

    #@26
    .line 433
    :cond_26
    invoke-virtual {p0, p1, p2}, Landroid/webkit/ZoomManager;->setZoomScale(FZ)V

    #@29
    .line 435
    iget v3, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@2b
    cmpl-float v3, v0, v3

    #@2d
    if-eqz v3, :cond_54

    #@2f
    .line 436
    iget-boolean v2, p0, Landroid/webkit/ZoomManager;->mHardwareAccelerated:Z

    #@31
    if-eqz v2, :cond_35

    #@33
    .line 437
    iput-boolean v1, p0, Landroid/webkit/ZoomManager;->mInHWAcceleratedZoom:Z

    #@35
    .line 440
    :cond_35
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@38
    move-result-wide v2

    #@39
    iput-wide v2, p0, Landroid/webkit/ZoomManager;->mZoomStart:J

    #@3b
    .line 441
    div-float v2, v4, v0

    #@3d
    iput v2, p0, Landroid/webkit/ZoomManager;->mInvInitialZoomScale:F

    #@3f
    .line 442
    iget v2, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@41
    div-float v2, v4, v2

    #@43
    iput v2, p0, Landroid/webkit/ZoomManager;->mInvFinalZoomScale:F

    #@45
    .line 443
    iget v2, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@47
    iput v2, p0, Landroid/webkit/ZoomManager;->mZoomScale:F

    #@49
    .line 444
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@4b
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->onFixedLengthZoomAnimationStart()V

    #@4e
    .line 445
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@50
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@53
    .line 448
    :goto_53
    return v1

    #@54
    :cond_54
    move v1, v2

    #@55
    goto :goto_53
.end method

.method public supportsMultiTouchZoom()Z
    .registers 2

    #@0
    .prologue
    .line 753
    iget-boolean v0, p0, Landroid/webkit/ZoomManager;->mSupportMultiTouch:Z

    #@2
    return v0
.end method

.method public supportsPanDuringZoom()Z
    .registers 2

    #@0
    .prologue
    .line 757
    iget-boolean v0, p0, Landroid/webkit/ZoomManager;->mAllowPanAndScale:Z

    #@2
    return v0
.end method

.method public updateDefaultZoomDensity(F)V
    .registers 8
    .parameter "density"

    #@0
    .prologue
    .line 255
    sget-boolean v2, Landroid/webkit/ZoomManager;->$assertionsDisabled:Z

    #@2
    if-nez v2, :cond_f

    #@4
    const/4 v2, 0x0

    #@5
    cmpl-float v2, p1, v2

    #@7
    if-gtz v2, :cond_f

    #@9
    new-instance v2, Ljava/lang/AssertionError;

    #@b
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@e
    throw v2

    #@f
    .line 257
    :cond_f
    iget v2, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@11
    sub-float v2, p1, v2

    #@13
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    #@16
    move-result v2

    #@17
    sget v3, Landroid/webkit/ZoomManager;->MINIMUM_SCALE_INCREMENT:F

    #@19
    cmpl-float v2, v2, v3

    #@1b
    if-lez v2, :cond_34

    #@1d
    .line 259
    iget v0, p0, Landroid/webkit/ZoomManager;->mDefaultScale:F

    #@1f
    .line 261
    .local v0, originalDefault:F
    iput p1, p0, Landroid/webkit/ZoomManager;->mDisplayDensity:F

    #@21
    .line 262
    invoke-direct {p0, p1}, Landroid/webkit/ZoomManager;->setDefaultZoomScale(F)V

    #@24
    .line 263
    float-to-double v2, v0

    #@25
    const-wide/16 v4, 0x0

    #@27
    cmpl-double v2, v2, v4

    #@29
    if-lez v2, :cond_35

    #@2b
    div-float v1, p1, v0

    #@2d
    .line 265
    .local v1, scaleChange:F
    :goto_2d
    iget v2, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@2f
    mul-float/2addr v2, v1

    #@30
    const/4 v3, 0x1

    #@31
    invoke-virtual {p0, v2, v3}, Landroid/webkit/ZoomManager;->setZoomScale(FZ)V

    #@34
    .line 267
    .end local v0           #originalDefault:F
    .end local v1           #scaleChange:F
    :cond_34
    return-void

    #@35
    .line 263
    .restart local v0       #originalDefault:F
    :cond_35
    const/high16 v1, 0x3f80

    #@37
    goto :goto_2d
.end method

.method public updateDoubleTapZoom(I)V
    .registers 7
    .parameter "doubleTapZoom"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 533
    iget v3, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@3
    iget v4, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@5
    sub-float/2addr v3, v4

    #@6
    const v4, 0x3dcccccd

    #@9
    cmpg-float v3, v3, v4

    #@b
    if-gez v3, :cond_22

    #@d
    move v1, v2

    #@e
    .line 534
    .local v1, zoomIn:Z
    :goto_e
    int-to-float v3, p1

    #@f
    const/high16 v4, 0x42c8

    #@11
    div-float/2addr v3, v4

    #@12
    iput v3, p0, Landroid/webkit/ZoomManager;->mDoubleTapZoomFactor:F

    #@14
    .line 535
    invoke-virtual {p0}, Landroid/webkit/ZoomManager;->getReadingLevelScale()F

    #@17
    move-result v3

    #@18
    iput v3, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@1a
    .line 536
    if-eqz v1, :cond_24

    #@1c
    iget v0, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@1e
    .line 538
    .local v0, newScale:F
    :goto_1e
    invoke-direct {p0, v0, v2, v2}, Landroid/webkit/ZoomManager;->setZoomScale(FZZ)V

    #@21
    .line 539
    return-void

    #@22
    .line 533
    .end local v0           #newScale:F
    .end local v1           #zoomIn:Z
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_e

    #@24
    .line 536
    .restart local v1       #zoomIn:Z
    :cond_24
    iget v3, p0, Landroid/webkit/ZoomManager;->mTextWrapScale:F

    #@26
    iget v4, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@28
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    #@2b
    move-result v0

    #@2c
    goto :goto_1e
.end method

.method public updateMultiTouchSupport(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 733
    sget-boolean v2, Landroid/webkit/ZoomManager;->$assertionsDisabled:Z

    #@5
    if-nez v2, :cond_15

    #@7
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@9
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@c
    move-result-object v2

    #@d
    if-nez v2, :cond_15

    #@f
    new-instance v2, Ljava/lang/AssertionError;

    #@11
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@14
    throw v2

    #@15
    .line 735
    :cond_15
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@17
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@1a
    move-result-object v1

    #@1b
    .line 736
    .local v1, settings:Landroid/webkit/WebSettings;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1e
    move-result-object v0

    #@1f
    .line 737
    .local v0, pm:Landroid/content/pm/PackageManager;
    const-string v2, "android.hardware.touchscreen.multitouch"

    #@21
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@24
    move-result v2

    #@25
    if-nez v2, :cond_2f

    #@27
    const-string v2, "android.hardware.faketouch.multitouch.distinct"

    #@29
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_66

    #@2f
    :cond_2f
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->supportZoom()Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_66

    #@35
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getBuiltInZoomControls()Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_66

    #@3b
    move v2, v3

    #@3c
    :goto_3c
    iput-boolean v2, p0, Landroid/webkit/ZoomManager;->mSupportMultiTouch:Z

    #@3e
    .line 741
    const-string v2, "android.hardware.touchscreen.multitouch.distinct"

    #@40
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@43
    move-result v2

    #@44
    if-nez v2, :cond_4e

    #@46
    const-string v2, "android.hardware.faketouch.multitouch.distinct"

    #@48
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@4b
    move-result v2

    #@4c
    if-eqz v2, :cond_4f

    #@4e
    :cond_4e
    move v4, v3

    #@4f
    :cond_4f
    iput-boolean v4, p0, Landroid/webkit/ZoomManager;->mAllowPanAndScale:Z

    #@51
    .line 745
    iget-boolean v2, p0, Landroid/webkit/ZoomManager;->mSupportMultiTouch:Z

    #@53
    if-eqz v2, :cond_68

    #@55
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    #@57
    if-nez v2, :cond_68

    #@59
    .line 746
    new-instance v2, Landroid/view/ScaleGestureDetector;

    #@5b
    new-instance v3, Landroid/webkit/ZoomManager$ScaleDetectorListener;

    #@5d
    invoke-direct {v3, p0, v5}, Landroid/webkit/ZoomManager$ScaleDetectorListener;-><init>(Landroid/webkit/ZoomManager;Landroid/webkit/ZoomManager$1;)V

    #@60
    invoke-direct {v2, p1, v3}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    #@63
    iput-object v2, p0, Landroid/webkit/ZoomManager;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    #@65
    .line 750
    :cond_65
    :goto_65
    return-void

    #@66
    :cond_66
    move v2, v4

    #@67
    .line 737
    goto :goto_3c

    #@68
    .line 747
    :cond_68
    iget-boolean v2, p0, Landroid/webkit/ZoomManager;->mSupportMultiTouch:Z

    #@6a
    if-nez v2, :cond_65

    #@6c
    iget-object v2, p0, Landroid/webkit/ZoomManager;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    #@6e
    if-eqz v2, :cond_65

    #@70
    .line 748
    iput-object v5, p0, Landroid/webkit/ZoomManager;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    #@72
    goto :goto_65
.end method

.method public updateZoomPicker()V
    .registers 2

    #@0
    .prologue
    .line 1257
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->getCurrentZoomControl()Landroid/webkit/ZoomControlBase;

    #@3
    move-result-object v0

    #@4
    .line 1258
    .local v0, control:Landroid/webkit/ZoomControlBase;
    if-eqz v0, :cond_9

    #@6
    .line 1259
    invoke-interface {v0}, Landroid/webkit/ZoomControlBase;->update()V

    #@9
    .line 1261
    :cond_9
    return-void
.end method

.method public updateZoomRange(Landroid/webkit/WebViewCore$ViewState;II)V
    .registers 9
    .parameter "viewState"
    .parameter "viewWidth"
    .parameter "minPrefWidth"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 1009
    iget v1, p1, Landroid/webkit/WebViewCore$ViewState;->mMinScale:F

    #@5
    cmpl-float v1, v1, v4

    #@7
    if-nez v1, :cond_4b

    #@9
    .line 1010
    iget-boolean v1, p1, Landroid/webkit/WebViewCore$ViewState;->mMobileSite:Z

    #@b
    if-eqz v1, :cond_44

    #@d
    .line 1011
    invoke-static {v3, p2}, Ljava/lang/Math;->max(II)I

    #@10
    move-result v1

    #@11
    if-le p3, v1, :cond_28

    #@13
    .line 1012
    int-to-float v1, p2

    #@14
    int-to-float v2, p3

    #@15
    div-float/2addr v1, v2

    #@16
    iput v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@18
    .line 1013
    iput-boolean v3, p0, Landroid/webkit/ZoomManager;->mMinZoomScaleFixed:Z

    #@1a
    .line 1033
    :cond_1a
    :goto_1a
    iget v1, p1, Landroid/webkit/WebViewCore$ViewState;->mMaxScale:F

    #@1c
    cmpl-float v1, v1, v4

    #@1e
    if-nez v1, :cond_52

    #@20
    .line 1034
    iget v1, p0, Landroid/webkit/ZoomManager;->mDefaultMaxZoomScale:F

    #@22
    iput v1, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@24
    .line 1038
    :goto_24
    invoke-direct {p0}, Landroid/webkit/ZoomManager;->sanitizeMinMaxScales()V

    #@27
    .line 1039
    return-void

    #@28
    .line 1015
    :cond_28
    iget v1, p1, Landroid/webkit/WebViewCore$ViewState;->mDefaultScale:F

    #@2a
    iput v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@2c
    .line 1016
    iput-boolean v2, p0, Landroid/webkit/ZoomManager;->mMinZoomScaleFixed:Z

    #@2e
    .line 1018
    iget-object v1, p0, Landroid/webkit/ZoomManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@30
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@33
    move-result-object v0

    #@34
    .line 1019
    .local v0, settings:Landroid/webkit/WebSettings;
    if-eqz v0, :cond_1a

    #@36
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getForceZoomoutEnabled()Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_1a

    #@3c
    .line 1020
    const v1, 0x3ecccccd

    #@3f
    iput v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@41
    .line 1021
    iput-boolean v2, p0, Landroid/webkit/ZoomManager;->mMinZoomScaleFixed:Z

    #@43
    goto :goto_1a

    #@44
    .line 1026
    .end local v0           #settings:Landroid/webkit/WebSettings;
    :cond_44
    iget v1, p0, Landroid/webkit/ZoomManager;->mDefaultMinZoomScale:F

    #@46
    iput v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@48
    .line 1027
    iput-boolean v3, p0, Landroid/webkit/ZoomManager;->mMinZoomScaleFixed:Z

    #@4a
    goto :goto_1a

    #@4b
    .line 1030
    :cond_4b
    iget v1, p1, Landroid/webkit/WebViewCore$ViewState;->mMinScale:F

    #@4d
    iput v1, p0, Landroid/webkit/ZoomManager;->mMinZoomScale:F

    #@4f
    .line 1031
    iput-boolean v2, p0, Landroid/webkit/ZoomManager;->mMinZoomScaleFixed:Z

    #@51
    goto :goto_1a

    #@52
    .line 1036
    :cond_52
    iget v1, p1, Landroid/webkit/WebViewCore$ViewState;->mMaxScale:F

    #@54
    iput v1, p0, Landroid/webkit/ZoomManager;->mMaxZoomScale:F

    #@56
    goto :goto_24
.end method

.method public willScaleTriggerZoom(F)Z
    .registers 3
    .parameter "scale"

    #@0
    .prologue
    .line 384
    iget v0, p0, Landroid/webkit/ZoomManager;->mActualScale:F

    #@2
    invoke-static {p1, v0}, Landroid/webkit/ZoomManager;->exceedsMinScaleIncrement(FF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public zoomIn()Z
    .registers 2

    #@0
    .prologue
    .line 396
    const/high16 v0, 0x3fa0

    #@2
    invoke-direct {p0, v0}, Landroid/webkit/ZoomManager;->zoom(F)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public zoomOut()Z
    .registers 2

    #@0
    .prologue
    .line 400
    const v0, 0x3f4ccccd

    #@3
    invoke-direct {p0, v0}, Landroid/webkit/ZoomManager;->zoom(F)Z

    #@6
    move-result v0

    #@7
    return v0
.end method
