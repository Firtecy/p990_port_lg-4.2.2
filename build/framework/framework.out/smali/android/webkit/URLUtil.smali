.class public final Landroid/webkit/URLUtil;
.super Ljava/lang/Object;
.source "URLUtil.java"


# static fields
.field static final ASSET_BASE:Ljava/lang/String; = "file:///android_asset/"

.field static final CONTENT_BASE:Ljava/lang/String; = "content:"

.field private static final CONTENT_DISPOSITION_INLINE_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final CONTENT_DISPOSITION_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final COUNTRY_CODE:Ljava/lang/String; = null

.field private static final DEFAULT_ENCODING_TYPE:Ljava/lang/String; = "ISO-8859-1"

.field private static final DEFAULT_ENCODING_TYPE_JAVA:Ljava/lang/String; = "UTF-8"

.field private static final ENCODING_TYPE_UNKNOWN:Ljava/lang/String; = "unknown-encoding type"

.field static final FILE_BASE:Ljava/lang/String; = "file://"

.field private static final KR_ENCODINGTYPES:[Ljava/lang/String; = null

.field private static final LOGTAG:Ljava/lang/String; = "webkit"

.field private static final NOKR_ENCODINGTYPES:[Ljava/lang/String; = null

.field static final PROXY_BASE:Ljava/lang/String; = "file:///cookieless_proxy/"

.field static final RESOURCE_BASE:Ljava/lang/String; = "file:///android_res/"


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v3, 0x2

    #@5
    .line 53
    const/16 v0, 0x8

    #@7
    new-array v0, v0, [Ljava/lang/String;

    #@9
    const-string v1, "EUC-KR"

    #@b
    aput-object v1, v0, v4

    #@d
    const-string v1, "UTF-8"

    #@f
    aput-object v1, v0, v5

    #@11
    const-string v1, "UTF-16"

    #@13
    aput-object v1, v0, v3

    #@15
    const-string v1, "US-ASCII"

    #@17
    aput-object v1, v0, v6

    #@19
    const-string v1, "UTF-16BE"

    #@1b
    aput-object v1, v0, v7

    #@1d
    const/4 v1, 0x5

    #@1e
    const-string v2, "UTF-16LE"

    #@20
    aput-object v2, v0, v1

    #@22
    const/4 v1, 0x6

    #@23
    const-string v2, "ISO-2022-KR"

    #@25
    aput-object v2, v0, v1

    #@27
    const/4 v1, 0x7

    #@28
    const-string v2, "ISO-8859-1"

    #@2a
    aput-object v2, v0, v1

    #@2c
    sput-object v0, Landroid/webkit/URLUtil;->KR_ENCODINGTYPES:[Ljava/lang/String;

    #@2e
    .line 54
    const/16 v0, 0x8

    #@30
    new-array v0, v0, [Ljava/lang/String;

    #@32
    const-string v1, "UTF-8"

    #@34
    aput-object v1, v0, v4

    #@36
    const-string v1, "EUC-KR"

    #@38
    aput-object v1, v0, v5

    #@3a
    const-string v1, "UTF-16"

    #@3c
    aput-object v1, v0, v3

    #@3e
    const-string v1, "US-ASCII"

    #@40
    aput-object v1, v0, v6

    #@42
    const-string v1, "UTF-16BE"

    #@44
    aput-object v1, v0, v7

    #@46
    const/4 v1, 0x5

    #@47
    const-string v2, "UTF-16LE"

    #@49
    aput-object v2, v0, v1

    #@4b
    const/4 v1, 0x6

    #@4c
    const-string v2, "ISO-2022-KR"

    #@4e
    aput-object v2, v0, v1

    #@50
    const/4 v1, 0x7

    #@51
    const-string v2, "ISO-8859-1"

    #@53
    aput-object v2, v0, v1

    #@55
    sput-object v0, Landroid/webkit/URLUtil;->NOKR_ENCODINGTYPES:[Ljava/lang/String;

    #@57
    .line 58
    const-string/jumbo v0, "ro.build.target_country"

    #@5a
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5d
    move-result-object v0

    #@5e
    sput-object v0, Landroid/webkit/URLUtil;->COUNTRY_CODE:Ljava/lang/String;

    #@60
    .line 437
    const-string v0, "attachment;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1\\s*$"

    #@62
    invoke-static {v0, v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    #@65
    move-result-object v0

    #@66
    sput-object v0, Landroid/webkit/URLUtil;->CONTENT_DISPOSITION_PATTERN:Ljava/util/regex/Pattern;

    #@68
    .line 442
    const-string v0, "inline;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1\\s*$"

    #@6a
    invoke-static {v0, v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    #@6d
    move-result-object v0

    #@6e
    sput-object v0, Landroid/webkit/URLUtil;->CONTENT_DISPOSITION_INLINE_PATTERN:Ljava/util/regex/Pattern;

    #@70
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static composeSearchUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "inQuery"
    .parameter "template"
    .parameter "queryPlaceHolder"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 112
    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@4
    move-result v2

    #@5
    .line 113
    .local v2, placeHolderIndex:I
    if-gez v2, :cond_8

    #@7
    .line 131
    :goto_7
    return-object v4

    #@8
    .line 118
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    .line 119
    .local v0, buffer:Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    #@e
    invoke-virtual {p1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@11
    move-result-object v5

    #@12
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 122
    :try_start_15
    const-string/jumbo v5, "utf-8"

    #@18
    invoke-static {p0, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    .line 123
    .local v3, query:Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1f
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_15 .. :try_end_1f} :catch_30

    #@1f
    .line 128
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@22
    move-result v4

    #@23
    add-int/2addr v4, v2

    #@24
    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    .line 131
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    goto :goto_7

    #@30
    .line 124
    .end local v3           #query:Ljava/lang/String;
    :catch_30
    move-exception v1

    #@31
    .line 125
    .local v1, ex:Ljava/io/UnsupportedEncodingException;
    goto :goto_7
.end method

.method public static decode([B)[B
    .registers 10
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 135
    array-length v6, p0

    #@2
    if-nez v6, :cond_7

    #@4
    .line 136
    new-array v2, v8, [B

    #@6
    .line 158
    :goto_6
    return-object v2

    #@7
    .line 140
    :cond_7
    array-length v6, p0

    #@8
    new-array v5, v6, [B

    #@a
    .line 142
    .local v5, tempData:[B
    const/4 v3, 0x0

    #@b
    .line 143
    .local v3, tempCount:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    array-length v6, p0

    #@d
    if-ge v1, v6, :cond_40

    #@f
    .line 144
    aget-byte v0, p0, v1

    #@11
    .line 145
    .local v0, b:B
    const/16 v6, 0x25

    #@13
    if-ne v0, v6, :cond_30

    #@15
    .line 146
    array-length v6, p0

    #@16
    sub-int/2addr v6, v1

    #@17
    const/4 v7, 0x2

    #@18
    if-le v6, v7, :cond_38

    #@1a
    .line 147
    add-int/lit8 v6, v1, 0x1

    #@1c
    aget-byte v6, p0, v6

    #@1e
    invoke-static {v6}, Landroid/webkit/URLUtil;->parseHex(B)I

    #@21
    move-result v6

    #@22
    mul-int/lit8 v6, v6, 0x10

    #@24
    add-int/lit8 v7, v1, 0x2

    #@26
    aget-byte v7, p0, v7

    #@28
    invoke-static {v7}, Landroid/webkit/URLUtil;->parseHex(B)I

    #@2b
    move-result v7

    #@2c
    add-int/2addr v6, v7

    #@2d
    int-to-byte v0, v6

    #@2e
    .line 149
    add-int/lit8 v1, v1, 0x2

    #@30
    .line 154
    :cond_30
    add-int/lit8 v4, v3, 0x1

    #@32
    .end local v3           #tempCount:I
    .local v4, tempCount:I
    aput-byte v0, v5, v3

    #@34
    .line 143
    add-int/lit8 v1, v1, 0x1

    #@36
    move v3, v4

    #@37
    .end local v4           #tempCount:I
    .restart local v3       #tempCount:I
    goto :goto_c

    #@38
    .line 151
    :cond_38
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@3a
    const-string v7, "Invalid format"

    #@3c
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v6

    #@40
    .line 156
    .end local v0           #b:B
    :cond_40
    new-array v2, v3, [B

    #@42
    .line 157
    .local v2, retData:[B
    invoke-static {v5, v8, v2, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@45
    goto :goto_6
.end method

.method private static getEncodingType(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "fileName"

    #@0
    .prologue
    .line 484
    const/4 v4, 0x0

    #@1
    .line 486
    .local v4, midStr:Ljava/lang/String;
    sget-object v6, Landroid/webkit/URLUtil;->COUNTRY_CODE:Ljava/lang/String;

    #@3
    const-string v7, "KR"

    #@5
    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@8
    move-result v6

    #@9
    if-eqz v6, :cond_35

    #@b
    .line 487
    sget-object v0, Landroid/webkit/URLUtil;->KR_ENCODINGTYPES:[Ljava/lang/String;

    #@d
    .line 492
    .local v0, ENCODINGTYPES:[Ljava/lang/String;
    :goto_d
    const/4 v3, 0x0

    #@e
    .local v3, i:I
    move-object v5, v4

    #@f
    .end local v4           #midStr:Ljava/lang/String;
    .local v5, midStr:Ljava/lang/String;
    :goto_f
    :try_start_f
    array-length v6, v0

    #@10
    if-ge v3, v6, :cond_3c

    #@12
    .line 493
    new-instance v4, Ljava/lang/String;

    #@14
    const-string v6, "ISO-8859-1"

    #@16
    invoke-virtual {p0, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@19
    move-result-object v6

    #@1a
    aget-object v7, v0, v3

    #@1c
    invoke-direct {v4, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1f
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_f .. :try_end_1f} :catch_41

    #@1f
    .line 494
    .end local v5           #midStr:Ljava/lang/String;
    .restart local v4       #midStr:Ljava/lang/String;
    :try_start_1f
    new-instance v1, Ljava/lang/String;

    #@21
    aget-object v6, v0, v3

    #@23
    invoke-virtual {v4, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@26
    move-result-object v6

    #@27
    const-string v7, "ISO-8859-1"

    #@29
    invoke-direct {v1, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    #@2c
    .line 496
    .local v1, cmpStr:Ljava/lang/String;
    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@2f
    move-result v6

    #@30
    if-nez v6, :cond_38

    #@32
    .line 497
    aget-object v6, v0, v3
    :try_end_34
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1f .. :try_end_34} :catch_47

    #@34
    .line 503
    .end local v1           #cmpStr:Ljava/lang/String;
    :goto_34
    return-object v6

    #@35
    .line 489
    .end local v0           #ENCODINGTYPES:[Ljava/lang/String;
    .end local v3           #i:I
    :cond_35
    sget-object v0, Landroid/webkit/URLUtil;->NOKR_ENCODINGTYPES:[Ljava/lang/String;

    #@37
    .restart local v0       #ENCODINGTYPES:[Ljava/lang/String;
    goto :goto_d

    #@38
    .line 492
    .restart local v1       #cmpStr:Ljava/lang/String;
    .restart local v3       #i:I
    :cond_38
    add-int/lit8 v3, v3, 0x1

    #@3a
    move-object v5, v4

    #@3b
    .end local v4           #midStr:Ljava/lang/String;
    .restart local v5       #midStr:Ljava/lang/String;
    goto :goto_f

    #@3c
    .end local v1           #cmpStr:Ljava/lang/String;
    :cond_3c
    move-object v4, v5

    #@3d
    .line 503
    .end local v5           #midStr:Ljava/lang/String;
    .restart local v4       #midStr:Ljava/lang/String;
    :goto_3d
    const-string/jumbo v6, "unknown-encoding type"

    #@40
    goto :goto_34

    #@41
    .line 500
    .end local v4           #midStr:Ljava/lang/String;
    .restart local v5       #midStr:Ljava/lang/String;
    :catch_41
    move-exception v2

    #@42
    move-object v4, v5

    #@43
    .line 501
    .end local v5           #midStr:Ljava/lang/String;
    .local v2, e:Ljava/io/UnsupportedEncodingException;
    .restart local v4       #midStr:Ljava/lang/String;
    :goto_43
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    #@46
    goto :goto_3d

    #@47
    .line 500
    .end local v2           #e:Ljava/io/UnsupportedEncodingException;
    :catch_47
    move-exception v2

    #@48
    goto :goto_43
.end method

.method private static getURLEncodingType(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "url"

    #@0
    .prologue
    .line 508
    sget-object v5, Landroid/webkit/URLUtil;->COUNTRY_CODE:Ljava/lang/String;

    #@2
    const-string v6, "KR"

    #@4
    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_35

    #@a
    .line 509
    sget-object v0, Landroid/webkit/URLUtil;->KR_ENCODINGTYPES:[Ljava/lang/String;

    #@c
    .line 514
    .local v0, ENCODINGTYPES:[Ljava/lang/String;
    :goto_c
    :try_start_c
    const-string v5, " "

    #@e
    const-string v6, ""

    #@10
    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@13
    move-result-object v4

    #@14
    .line 515
    .local v4, nonSpaceUrl:Ljava/lang/String;
    const-string v5, "%20"

    #@16
    const-string v6, ""

    #@18
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    .line 516
    const/4 v3, 0x0

    #@1d
    .local v3, i:I
    :goto_1d
    array-length v5, v0

    #@1e
    if-ge v3, v5, :cond_3f

    #@20
    .line 517
    aget-object v5, v0, v3

    #@22
    invoke-static {v4, v5}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    aget-object v6, v0, v3

    #@28
    invoke-static {v5, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    .line 518
    .local v1, cmp:Ljava/lang/String;
    invoke-virtual {v4, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@2f
    move-result v5

    #@30
    if-nez v5, :cond_38

    #@32
    .line 519
    aget-object v5, v0, v3
    :try_end_34
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_c .. :try_end_34} :catch_3b

    #@34
    .line 526
    .end local v1           #cmp:Ljava/lang/String;
    .end local v3           #i:I
    .end local v4           #nonSpaceUrl:Ljava/lang/String;
    :goto_34
    return-object v5

    #@35
    .line 511
    .end local v0           #ENCODINGTYPES:[Ljava/lang/String;
    :cond_35
    sget-object v0, Landroid/webkit/URLUtil;->NOKR_ENCODINGTYPES:[Ljava/lang/String;

    #@37
    .restart local v0       #ENCODINGTYPES:[Ljava/lang/String;
    goto :goto_c

    #@38
    .line 516
    .restart local v1       #cmp:Ljava/lang/String;
    .restart local v3       #i:I
    .restart local v4       #nonSpaceUrl:Ljava/lang/String;
    :cond_38
    add-int/lit8 v3, v3, 0x1

    #@3a
    goto :goto_1d

    #@3b
    .line 523
    .end local v1           #cmp:Ljava/lang/String;
    .end local v3           #i:I
    .end local v4           #nonSpaceUrl:Ljava/lang/String;
    :catch_3b
    move-exception v2

    #@3c
    .line 524
    .local v2, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    #@3f
    .line 526
    .end local v2           #e:Ljava/io/UnsupportedEncodingException;
    :cond_3f
    const-string v5, "UTF-8"

    #@41
    goto :goto_34
.end method

.method public static final guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 16
    .parameter "url"
    .parameter "contentDisposition"
    .parameter "mimeType"

    #@0
    .prologue
    .line 328
    const/4 v6, 0x0

    #@1
    .line 329
    .local v6, filename:Ljava/lang/String;
    const/4 v5, 0x0

    #@2
    .line 332
    .local v5, extension:Ljava/lang/String;
    if-nez v6, :cond_1a

    #@4
    if-eqz p1, :cond_1a

    #@6
    .line 333
    invoke-static {p1}, Landroid/webkit/URLUtil;->parseContentDisposition(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v6

    #@a
    .line 334
    if-eqz v6, :cond_1a

    #@c
    .line 335
    const/16 v11, 0x2f

    #@e
    invoke-virtual {v6, v11}, Ljava/lang/String;->lastIndexOf(I)I

    #@11
    move-result v11

    #@12
    add-int/lit8 v7, v11, 0x1

    #@14
    .line 336
    .local v7, index:I
    if-lez v7, :cond_1a

    #@16
    .line 337
    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    .line 342
    .end local v7           #index:I
    :cond_1a
    sget-object v11, Landroid/webkit/URLUtil;->COUNTRY_CODE:Ljava/lang/String;

    #@1c
    const-string v12, "KR"

    #@1e
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@21
    move-result v11

    #@22
    if-eqz v11, :cond_b0

    #@24
    if-eqz v6, :cond_b0

    #@26
    .line 344
    :try_start_26
    invoke-static {v6}, Landroid/webkit/URLUtil;->getEncodingType(Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    .line 345
    .local v4, enType:Ljava/lang/String;
    const-string/jumbo v11, "unknown-encoding type"

    #@2d
    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v11

    #@31
    if-nez v11, :cond_42

    #@33
    .line 346
    new-instance v11, Ljava/lang/String;

    #@35
    const-string v12, "ISO-8859-1"

    #@37
    invoke-virtual {v6, v12}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@3a
    move-result-object v12

    #@3b
    invoke-direct {v11, v12, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    #@3e
    invoke-static {v11}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;
    :try_end_41
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_26 .. :try_end_41} :catch_ab

    #@41
    move-result-object v6

    #@42
    .line 379
    .end local v4           #enType:Ljava/lang/String;
    :cond_42
    :goto_42
    sget-object v11, Landroid/webkit/URLUtil;->COUNTRY_CODE:Ljava/lang/String;

    #@44
    const-string v12, "KR"

    #@46
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@49
    move-result v11

    #@4a
    if-eqz v11, :cond_ef

    #@4c
    if-eqz v6, :cond_ef

    #@4e
    .line 381
    :try_start_4e
    invoke-static {v6}, Landroid/webkit/URLUtil;->getURLEncodingType(Ljava/lang/String;)Ljava/lang/String;

    #@51
    move-result-object v11

    #@52
    invoke-static {v6, v11}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_55
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4e .. :try_end_55} :catch_e9

    #@55
    move-result-object v6

    #@56
    .line 394
    :cond_56
    :goto_56
    const/16 v11, 0x2e

    #@58
    invoke-virtual {v6, v11}, Ljava/lang/String;->indexOf(I)I

    #@5b
    move-result v1

    #@5c
    .line 395
    .local v1, dotIndex:I
    if-gez v1, :cond_fb

    #@5e
    .line 396
    if-eqz p2, :cond_7d

    #@60
    .line 397
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    #@63
    move-result-object v11

    #@64
    invoke-virtual {v11, p2}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    .line 398
    if-eqz v5, :cond_7d

    #@6a
    .line 399
    new-instance v11, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v12, "."

    #@71
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v11

    #@75
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v11

    #@79
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    .line 402
    :cond_7d
    if-nez v5, :cond_99

    #@7f
    .line 403
    if-eqz p2, :cond_f8

    #@81
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@84
    move-result-object v11

    #@85
    const-string/jumbo v12, "text/"

    #@88
    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@8b
    move-result v11

    #@8c
    if-eqz v11, :cond_f8

    #@8e
    .line 404
    const-string/jumbo v11, "text/html"

    #@91
    invoke-virtual {p2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@94
    move-result v11

    #@95
    if-eqz v11, :cond_f5

    #@97
    .line 405
    const-string v5, ".html"

    #@99
    .line 433
    :cond_99
    :goto_99
    new-instance v11, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v11

    #@a2
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v11

    #@a6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v11

    #@aa
    return-object v11

    #@ab
    .line 347
    .end local v1           #dotIndex:I
    :catch_ab
    move-exception v3

    #@ac
    .line 348
    .local v3, e2:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    #@af
    goto :goto_42

    #@b0
    .line 354
    .end local v3           #e2:Ljava/io/UnsupportedEncodingException;
    :cond_b0
    if-nez v6, :cond_42

    #@b2
    .line 357
    sget-object v11, Landroid/webkit/URLUtil;->COUNTRY_CODE:Ljava/lang/String;

    #@b4
    const-string v12, "KR"

    #@b6
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@b9
    move-result v11

    #@ba
    if-eqz v11, :cond_e4

    #@bc
    .line 358
    move-object v0, p0

    #@bd
    .line 362
    .local v0, decodedUrl:Ljava/lang/String;
    :goto_bd
    if-eqz v0, :cond_42

    #@bf
    .line 363
    const/16 v11, 0x3f

    #@c1
    invoke-virtual {v0, v11}, Ljava/lang/String;->indexOf(I)I

    #@c4
    move-result v9

    #@c5
    .line 365
    .local v9, queryIndex:I
    if-lez v9, :cond_cc

    #@c7
    .line 366
    const/4 v11, 0x0

    #@c8
    invoke-virtual {v0, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@cb
    move-result-object v0

    #@cc
    .line 368
    :cond_cc
    const-string v11, "/"

    #@ce
    invoke-virtual {v0, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@d1
    move-result v11

    #@d2
    if-nez v11, :cond_42

    #@d4
    .line 369
    const/16 v11, 0x2f

    #@d6
    invoke-virtual {v0, v11}, Ljava/lang/String;->lastIndexOf(I)I

    #@d9
    move-result v11

    #@da
    add-int/lit8 v7, v11, 0x1

    #@dc
    .line 370
    .restart local v7       #index:I
    if-lez v7, :cond_42

    #@de
    .line 371
    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@e1
    move-result-object v6

    #@e2
    goto/16 :goto_42

    #@e4
    .line 360
    .end local v0           #decodedUrl:Ljava/lang/String;
    .end local v7           #index:I
    .end local v9           #queryIndex:I
    :cond_e4
    invoke-static {p0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@e7
    move-result-object v0

    #@e8
    .restart local v0       #decodedUrl:Ljava/lang/String;
    goto :goto_bd

    #@e9
    .line 382
    .end local v0           #decodedUrl:Ljava/lang/String;
    :catch_e9
    move-exception v2

    #@ea
    .line 383
    .local v2, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    #@ed
    goto/16 :goto_56

    #@ef
    .line 388
    .end local v2           #e:Ljava/io/UnsupportedEncodingException;
    :cond_ef
    if-nez v6, :cond_56

    #@f1
    .line 389
    const-string v6, "downloadfile"

    #@f3
    goto/16 :goto_56

    #@f5
    .line 407
    .restart local v1       #dotIndex:I
    :cond_f5
    const-string v5, ".txt"

    #@f7
    goto :goto_99

    #@f8
    .line 410
    :cond_f8
    const-string v5, ".bin"

    #@fa
    goto :goto_99

    #@fb
    .line 414
    :cond_fb
    if-eqz p2, :cond_136

    #@fd
    .line 417
    const/16 v11, 0x2e

    #@ff
    invoke-virtual {v6, v11}, Ljava/lang/String;->lastIndexOf(I)I

    #@102
    move-result v8

    #@103
    .line 418
    .local v8, lastDotIndex:I
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    #@106
    move-result-object v11

    #@107
    add-int/lit8 v12, v8, 0x1

    #@109
    invoke-virtual {v6, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@10c
    move-result-object v12

    #@10d
    invoke-virtual {v11, v12}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    #@110
    move-result-object v10

    #@111
    .line 420
    .local v10, typeFromExt:Ljava/lang/String;
    if-eqz v10, :cond_136

    #@113
    invoke-virtual {v10, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@116
    move-result v11

    #@117
    if-nez v11, :cond_136

    #@119
    .line 421
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    #@11c
    move-result-object v11

    #@11d
    invoke-virtual {v11, p2}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    #@120
    move-result-object v5

    #@121
    .line 422
    if-eqz v5, :cond_136

    #@123
    .line 423
    new-instance v11, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v12, "."

    #@12a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v11

    #@12e
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v11

    #@132
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@135
    move-result-object v5

    #@136
    .line 427
    .end local v8           #lastDotIndex:I
    .end local v10           #typeFromExt:Ljava/lang/String;
    :cond_136
    if-nez v5, :cond_13c

    #@138
    .line 428
    invoke-virtual {v6, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@13b
    move-result-object v5

    #@13c
    .line 430
    :cond_13c
    const/4 v11, 0x0

    #@13d
    invoke-virtual {v6, v11, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@140
    move-result-object v6

    #@141
    goto/16 :goto_99
.end method

.method public static guessUrl(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "inUrl"

    #@0
    .prologue
    .line 67
    move-object v1, p0

    #@1
    .line 72
    .local v1, retVal:Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_8

    #@7
    .line 107
    .end local p0
    :cond_7
    :goto_7
    return-object p0

    #@8
    .line 73
    .restart local p0
    :cond_8
    const-string v3, "about:"

    #@a
    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d
    move-result v3

    #@e
    if-nez v3, :cond_7

    #@10
    .line 75
    const-string v3, "data:"

    #@12
    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@15
    move-result v3

    #@16
    if-nez v3, :cond_7

    #@18
    .line 77
    const-string v3, "file:"

    #@1a
    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1d
    move-result v3

    #@1e
    if-nez v3, :cond_7

    #@20
    .line 79
    const-string/jumbo v3, "javascript:"

    #@23
    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@26
    move-result v3

    #@27
    if-nez v3, :cond_7

    #@29
    .line 84
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    const-string/jumbo v4, "rtsp:"

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@33
    move-result v3

    #@34
    if-nez v3, :cond_7

    #@36
    .line 88
    const-string v3, "."

    #@38
    invoke-virtual {p0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@3b
    move-result v3

    #@3c
    const/4 v4, 0x1

    #@3d
    if-ne v3, v4, :cond_4a

    #@3f
    .line 89
    const/4 v3, 0x0

    #@40
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@43
    move-result v4

    #@44
    add-int/lit8 v4, v4, -0x1

    #@46
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@49
    move-result-object p0

    #@4a
    .line 93
    :cond_4a
    :try_start_4a
    new-instance v2, Landroid/net/WebAddress;

    #@4c
    invoke-direct {v2, p0}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V
    :try_end_4f
    .catch Landroid/net/ParseException; {:try_start_4a .. :try_end_4f} :catch_82

    #@4f
    .line 103
    .local v2, webAddress:Landroid/net/WebAddress;
    invoke-virtual {v2}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    const/16 v4, 0x2e

    #@55
    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    #@58
    move-result v3

    #@59
    const/4 v4, -0x1

    #@5a
    if-ne v3, v4, :cond_7d

    #@5c
    .line 105
    new-instance v3, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string/jumbo v4, "www."

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v2}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    const-string v4, ".com"

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v3

    #@7a
    invoke-virtual {v2, v3}, Landroid/net/WebAddress;->setHost(Ljava/lang/String;)V

    #@7d
    .line 107
    :cond_7d
    invoke-virtual {v2}, Landroid/net/WebAddress;->toString()Ljava/lang/String;

    #@80
    move-result-object p0

    #@81
    goto :goto_7

    #@82
    .line 94
    .end local v2           #webAddress:Landroid/net/WebAddress;
    :catch_82
    move-exception v0

    #@83
    .local v0, ex:Landroid/net/ParseException;
    move-object p0, v1

    #@84
    .line 99
    goto :goto_7
.end method

.method public static isAboutUrl(Ljava/lang/String;)Z
    .registers 2
    .parameter "url"

    #@0
    .prologue
    .line 233
    if-eqz p0, :cond_c

    #@2
    const-string v0, "about:"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public static isAssetUrl(Ljava/lang/String;)Z
    .registers 2
    .parameter "url"

    #@0
    .prologue
    .line 199
    if-eqz p0, :cond_c

    #@2
    const-string v0, "file:///android_asset/"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public static isContentUrl(Ljava/lang/String;)Z
    .registers 2
    .parameter "url"

    #@0
    .prologue
    .line 282
    if-eqz p0, :cond_c

    #@2
    const-string v0, "content:"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public static isCookielessProxyUrl(Ljava/lang/String;)Z
    .registers 2
    .parameter "url"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 217
    if-eqz p0, :cond_c

    #@2
    const-string v0, "file:///cookieless_proxy/"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public static isDataUrl(Ljava/lang/String;)Z
    .registers 2
    .parameter "url"

    #@0
    .prologue
    .line 240
    if-eqz p0, :cond_c

    #@2
    const-string v0, "data:"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public static isFileUrl(Ljava/lang/String;)Z
    .registers 2
    .parameter "url"

    #@0
    .prologue
    .line 224
    if-eqz p0, :cond_1c

    #@2
    const-string v0, "file://"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1c

    #@a
    const-string v0, "file:///android_asset/"

    #@c
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_1c

    #@12
    const-string v0, "file:///cookieless_proxy/"

    #@14
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_1c

    #@1a
    const/4 v0, 0x1

    #@1b
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method public static isHttpUrl(Ljava/lang/String;)Z
    .registers 4
    .parameter "url"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 254
    if-eqz p0, :cond_18

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v1

    #@7
    const/4 v2, 0x6

    #@8
    if-le v1, v2, :cond_18

    #@a
    const/4 v1, 0x7

    #@b
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    const-string v2, "http://"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_18

    #@17
    const/4 v0, 0x1

    #@18
    :cond_18
    return v0
.end method

.method public static isHttpsUrl(Ljava/lang/String;)Z
    .registers 4
    .parameter "url"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 263
    if-eqz p0, :cond_19

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v1

    #@7
    const/4 v2, 0x7

    #@8
    if-le v1, v2, :cond_19

    #@a
    const/16 v1, 0x8

    #@c
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    const-string v2, "https://"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_19

    #@18
    const/4 v0, 0x1

    #@19
    :cond_19
    return v0
.end method

.method public static isJavaScriptUrl(Ljava/lang/String;)Z
    .registers 2
    .parameter "url"

    #@0
    .prologue
    .line 247
    if-eqz p0, :cond_d

    #@2
    const-string/jumbo v0, "javascript:"

    #@5
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_d

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public static isNetworkUrl(Ljava/lang/String;)Z
    .registers 3
    .parameter "url"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 272
    if-eqz p0, :cond_9

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_a

    #@9
    .line 275
    :cond_9
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-static {p0}, Landroid/webkit/URLUtil;->isHttpUrl(Ljava/lang/String;)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_16

    #@10
    invoke-static {p0}, Landroid/webkit/URLUtil;->isHttpsUrl(Ljava/lang/String;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_9

    #@16
    :cond_16
    const/4 v0, 0x1

    #@17
    goto :goto_9
.end method

.method public static isResourceUrl(Ljava/lang/String;)Z
    .registers 2
    .parameter "url"

    #@0
    .prologue
    .line 207
    if-eqz p0, :cond_c

    #@2
    const-string v0, "file:///android_res/"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public static isValidUrl(Ljava/lang/String;)Z
    .registers 3
    .parameter "url"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 289
    if-eqz p0, :cond_9

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_a

    #@9
    .line 293
    :cond_9
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-static {p0}, Landroid/webkit/URLUtil;->isAssetUrl(Ljava/lang/String;)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_3a

    #@10
    invoke-static {p0}, Landroid/webkit/URLUtil;->isResourceUrl(Ljava/lang/String;)Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_3a

    #@16
    invoke-static {p0}, Landroid/webkit/URLUtil;->isFileUrl(Ljava/lang/String;)Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_3a

    #@1c
    invoke-static {p0}, Landroid/webkit/URLUtil;->isAboutUrl(Ljava/lang/String;)Z

    #@1f
    move-result v1

    #@20
    if-nez v1, :cond_3a

    #@22
    invoke-static {p0}, Landroid/webkit/URLUtil;->isHttpUrl(Ljava/lang/String;)Z

    #@25
    move-result v1

    #@26
    if-nez v1, :cond_3a

    #@28
    invoke-static {p0}, Landroid/webkit/URLUtil;->isHttpsUrl(Ljava/lang/String;)Z

    #@2b
    move-result v1

    #@2c
    if-nez v1, :cond_3a

    #@2e
    invoke-static {p0}, Landroid/webkit/URLUtil;->isJavaScriptUrl(Ljava/lang/String;)Z

    #@31
    move-result v1

    #@32
    if-nez v1, :cond_3a

    #@34
    invoke-static {p0}, Landroid/webkit/URLUtil;->isContentUrl(Ljava/lang/String;)Z

    #@37
    move-result v1

    #@38
    if-eqz v1, :cond_9

    #@3a
    :cond_3a
    const/4 v0, 0x1

    #@3b
    goto :goto_9
.end method

.method static parseContentDisposition(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "contentDisposition"

    #@0
    .prologue
    .line 459
    const/4 v0, 0x0

    #@1
    .line 460
    .local v0, contentDispositionEx:Ljava/lang/String;
    :try_start_1
    const-string v2, ";"

    #@3
    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_26

    #@9
    .line 461
    const/4 v2, 0x0

    #@a
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@d
    move-result v3

    #@e
    add-int/lit8 v3, v3, -0x1

    #@10
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 466
    :goto_14
    sget-object v2, Landroid/webkit/URLUtil;->CONTENT_DISPOSITION_PATTERN:Ljava/util/regex/Pattern;

    #@16
    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@19
    move-result-object v1

    #@1a
    .line 468
    .local v1, m:Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_28

    #@20
    .line 469
    const/4 v2, 0x2

    #@21
    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    .line 479
    .end local v1           #m:Ljava/util/regex/Matcher;
    :goto_25
    return-object v2

    #@26
    .line 464
    :cond_26
    move-object v0, p0

    #@27
    goto :goto_14

    #@28
    .line 472
    .restart local v1       #m:Ljava/util/regex/Matcher;
    :cond_28
    sget-object v2, Landroid/webkit/URLUtil;->CONTENT_DISPOSITION_INLINE_PATTERN:Ljava/util/regex/Pattern;

    #@2a
    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@2d
    move-result-object v1

    #@2e
    .line 473
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    #@31
    move-result v2

    #@32
    if-eqz v2, :cond_3b

    #@34
    .line 474
    const/4 v2, 0x2

    #@35
    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
    :try_end_38
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_38} :catch_3a

    #@38
    move-result-object v2

    #@39
    goto :goto_25

    #@3a
    .line 476
    .end local v1           #m:Ljava/util/regex/Matcher;
    :catch_3a
    move-exception v2

    #@3b
    .line 479
    :cond_3b
    const/4 v2, 0x0

    #@3c
    goto :goto_25
.end method

.method private static parseHex(B)I
    .registers 4
    .parameter "b"

    #@0
    .prologue
    .line 188
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_b

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_b

    #@8
    add-int/lit8 v0, p0, -0x30

    #@a
    .line 190
    :goto_a
    return v0

    #@b
    .line 189
    :cond_b
    const/16 v0, 0x41

    #@d
    if-lt p0, v0, :cond_18

    #@f
    const/16 v0, 0x46

    #@11
    if-gt p0, v0, :cond_18

    #@13
    add-int/lit8 v0, p0, -0x41

    #@15
    add-int/lit8 v0, v0, 0xa

    #@17
    goto :goto_a

    #@18
    .line 190
    :cond_18
    const/16 v0, 0x61

    #@1a
    if-lt p0, v0, :cond_25

    #@1c
    const/16 v0, 0x66

    #@1e
    if-gt p0, v0, :cond_25

    #@20
    add-int/lit8 v0, p0, -0x61

    #@22
    add-int/lit8 v0, v0, 0xa

    #@24
    goto :goto_a

    #@25
    .line 192
    :cond_25
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@27
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "Invalid hex char \'"

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    const-string v2, "\'"

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@43
    throw v0
.end method

.method public static stripAnchor(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 307
    const/16 v1, 0x23

    #@2
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    #@5
    move-result v0

    #@6
    .line 308
    .local v0, anchorIndex:I
    const/4 v1, -0x1

    #@7
    if-eq v0, v1, :cond_e

    #@9
    .line 309
    const/4 v1, 0x0

    #@a
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@d
    move-result-object p0

    #@e
    .line 311
    .end local p0
    :cond_e
    return-object p0
.end method

.method static verifyURLEncoding(Ljava/lang/String;)Z
    .registers 7
    .parameter "url"

    #@0
    .prologue
    const/16 v5, 0x25

    #@2
    const/4 v3, 0x0

    #@3
    .line 165
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v0

    #@7
    .line 166
    .local v0, count:I
    if-nez v0, :cond_a

    #@9
    .line 184
    :cond_9
    :goto_9
    return v3

    #@a
    .line 170
    :cond_a
    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(I)I

    #@d
    move-result v2

    #@e
    .line 171
    .local v2, index:I
    :goto_e
    if-ltz v2, :cond_33

    #@10
    if-ge v2, v0, :cond_33

    #@12
    .line 172
    add-int/lit8 v4, v0, -0x2

    #@14
    if-ge v2, v4, :cond_9

    #@16
    .line 174
    add-int/lit8 v2, v2, 0x1

    #@18
    :try_start_18
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@1b
    move-result v4

    #@1c
    int-to-byte v4, v4

    #@1d
    invoke-static {v4}, Landroid/webkit/URLUtil;->parseHex(B)I

    #@20
    .line 175
    add-int/lit8 v2, v2, 0x1

    #@22
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@25
    move-result v4

    #@26
    int-to-byte v4, v4

    #@27
    invoke-static {v4}, Landroid/webkit/URLUtil;->parseHex(B)I
    :try_end_2a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_18 .. :try_end_2a} :catch_31

    #@2a
    .line 182
    add-int/lit8 v4, v2, 0x1

    #@2c
    invoke-virtual {p0, v5, v4}, Ljava/lang/String;->indexOf(II)I

    #@2f
    move-result v2

    #@30
    goto :goto_e

    #@31
    .line 176
    :catch_31
    move-exception v1

    #@32
    .line 177
    .local v1, e:Ljava/lang/IllegalArgumentException;
    goto :goto_9

    #@33
    .line 184
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :cond_33
    const/4 v3, 0x1

    #@34
    goto :goto_9
.end method
