.class public Landroid/webkit/DateSorter;
.super Ljava/lang/Object;
.source "DateSorter.java"


# static fields
.field public static final DAY_COUNT:I = 0x5

.field private static final LOGTAG:Ljava/lang/String; = "webkit"

.field private static final NUM_DAYS_AGO:I = 0x7


# instance fields
.field private mBins:[J

.field private mLabels:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 16
    .parameter "context"

    #@0
    .prologue
    const/4 v13, 0x7

    #@1
    const/4 v9, 0x6

    #@2
    const/4 v12, 0x2

    #@3
    const/4 v11, 0x1

    #@4
    const/4 v10, 0x0

    #@5
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 44
    const/4 v6, 0x4

    #@9
    new-array v6, v6, [J

    #@b
    iput-object v6, p0, Landroid/webkit/DateSorter;->mBins:[J

    #@d
    .line 45
    const/4 v6, 0x5

    #@e
    new-array v6, v6, [Ljava/lang/String;

    #@10
    iput-object v6, p0, Landroid/webkit/DateSorter;->mLabels:[Ljava/lang/String;

    #@12
    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@15
    move-result-object v5

    #@16
    .line 55
    .local v5, resources:Landroid/content/res/Resources;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@19
    move-result-object v0

    #@1a
    .line 56
    .local v0, c:Ljava/util/Calendar;
    invoke-direct {p0, v0}, Landroid/webkit/DateSorter;->beginningOfDay(Ljava/util/Calendar;)V

    #@1d
    .line 59
    iget-object v6, p0, Landroid/webkit/DateSorter;->mBins:[J

    #@1f
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@22
    move-result-wide v7

    #@23
    aput-wide v7, v6, v10

    #@25
    .line 60
    const/4 v6, -0x1

    #@26
    invoke-virtual {v0, v9, v6}, Ljava/util/Calendar;->add(II)V

    #@29
    .line 61
    iget-object v6, p0, Landroid/webkit/DateSorter;->mBins:[J

    #@2b
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@2e
    move-result-wide v7

    #@2f
    aput-wide v7, v6, v11

    #@31
    .line 62
    const/4 v6, -0x6

    #@32
    invoke-virtual {v0, v9, v6}, Ljava/util/Calendar;->add(II)V

    #@35
    .line 63
    iget-object v6, p0, Landroid/webkit/DateSorter;->mBins:[J

    #@37
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@3a
    move-result-wide v7

    #@3b
    aput-wide v7, v6, v12

    #@3d
    .line 64
    invoke-virtual {v0, v9, v13}, Ljava/util/Calendar;->add(II)V

    #@40
    .line 65
    const/4 v6, -0x1

    #@41
    invoke-virtual {v0, v12, v6}, Ljava/util/Calendar;->add(II)V

    #@44
    .line 66
    iget-object v6, p0, Landroid/webkit/DateSorter;->mBins:[J

    #@46
    const/4 v7, 0x3

    #@47
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@4a
    move-result-wide v8

    #@4b
    aput-wide v8, v6, v7

    #@4d
    .line 69
    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@50
    move-result-object v6

    #@51
    iget-object v2, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@53
    .line 70
    .local v2, locale:Ljava/util/Locale;
    if-nez v2, :cond_59

    #@55
    .line 71
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@58
    move-result-object v2

    #@59
    .line 73
    :cond_59
    invoke-static {v2}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@5c
    move-result-object v3

    #@5d
    .line 74
    .local v3, localeData:Llibcore/icu/LocaleData;
    iget-object v6, p0, Landroid/webkit/DateSorter;->mLabels:[Ljava/lang/String;

    #@5f
    iget-object v7, v3, Llibcore/icu/LocaleData;->today:Ljava/lang/String;

    #@61
    aput-object v7, v6, v10

    #@63
    .line 75
    iget-object v6, p0, Landroid/webkit/DateSorter;->mLabels:[Ljava/lang/String;

    #@65
    iget-object v7, v3, Llibcore/icu/LocaleData;->yesterday:Ljava/lang/String;

    #@67
    aput-object v7, v6, v11

    #@69
    .line 77
    const v4, 0x1130003

    #@6c
    .line 78
    .local v4, resId:I
    invoke-virtual {v5, v4, v13}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    .line 79
    .local v1, format:Ljava/lang/String;
    iget-object v6, p0, Landroid/webkit/DateSorter;->mLabels:[Ljava/lang/String;

    #@72
    new-array v7, v11, [Ljava/lang/Object;

    #@74
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@77
    move-result-object v8

    #@78
    aput-object v8, v7, v10

    #@7a
    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@7d
    move-result-object v7

    #@7e
    aput-object v7, v6, v12

    #@80
    .line 81
    iget-object v6, p0, Landroid/webkit/DateSorter;->mLabels:[Ljava/lang/String;

    #@82
    const/4 v7, 0x3

    #@83
    const v8, 0x10403d1

    #@86
    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@89
    move-result-object v8

    #@8a
    aput-object v8, v6, v7

    #@8c
    .line 82
    iget-object v6, p0, Landroid/webkit/DateSorter;->mLabels:[Ljava/lang/String;

    #@8e
    const/4 v7, 0x4

    #@8f
    const v8, 0x10403d2

    #@92
    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@95
    move-result-object v8

    #@96
    aput-object v8, v6, v7

    #@98
    .line 83
    return-void
.end method

.method private beginningOfDay(Ljava/util/Calendar;)V
    .registers 4
    .parameter "c"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 127
    const/16 v0, 0xb

    #@3
    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@6
    .line 128
    const/16 v0, 0xc

    #@8
    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@b
    .line 129
    const/16 v0, 0xd

    #@d
    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@10
    .line 130
    const/16 v0, 0xe

    #@12
    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@15
    .line 131
    return-void
.end method


# virtual methods
.method public getBoundary(I)J
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 114
    const/4 v0, 0x4

    #@1
    .line 116
    .local v0, lastDay:I
    if-ltz p1, :cond_5

    #@3
    if-le p1, v0, :cond_6

    #@5
    :cond_5
    const/4 p1, 0x0

    #@6
    .line 119
    :cond_6
    if-ne p1, v0, :cond_b

    #@8
    const-wide/high16 v1, -0x8000

    #@a
    .line 120
    :goto_a
    return-wide v1

    #@b
    :cond_b
    iget-object v1, p0, Landroid/webkit/DateSorter;->mBins:[J

    #@d
    aget-wide v1, v1, p1

    #@f
    goto :goto_a
.end method

.method public getIndex(J)I
    .registers 7
    .parameter "time"

    #@0
    .prologue
    .line 92
    const/4 v1, 0x4

    #@1
    .line 93
    .local v1, lastDay:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, v1, :cond_10

    #@4
    .line 94
    iget-object v2, p0, Landroid/webkit/DateSorter;->mBins:[J

    #@6
    aget-wide v2, v2, v0

    #@8
    cmp-long v2, p1, v2

    #@a
    if-lez v2, :cond_d

    #@c
    .line 96
    .end local v0           #i:I
    :goto_c
    return v0

    #@d
    .line 93
    .restart local v0       #i:I
    :cond_d
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_2

    #@10
    :cond_10
    move v0, v1

    #@11
    .line 96
    goto :goto_c
.end method

.method public getLabel(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 104
    if-ltz p1, :cond_5

    #@2
    const/4 v0, 0x5

    #@3
    if-lt p1, v0, :cond_8

    #@5
    :cond_5
    const-string v0, ""

    #@7
    .line 105
    :goto_7
    return-object v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/webkit/DateSorter;->mLabels:[Ljava/lang/String;

    #@a
    aget-object v0, v0, p1

    #@c
    goto :goto_7
.end method
