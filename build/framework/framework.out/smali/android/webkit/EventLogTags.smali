.class public Landroid/webkit/EventLogTags;
.super Ljava/lang/Object;
.source "EventLogTags.java"


# static fields
.field public static final BROWSER_DOUBLE_TAP_DURATION:I = 0x111d6

.field public static final BROWSER_SNAP_CENTER:I = 0x11206

.field public static final BROWSER_TEXT_SIZE_CHANGE:I = 0x11207

.field public static final BROWSER_ZOOM_LEVEL_CHANGE:I = 0x111d5


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static writeBrowserDoubleTapDuration(IJ)V
    .registers 7
    .parameter "duration"
    .parameter "time"

    #@0
    .prologue
    .line 30
    const v0, 0x111d6

    #@3
    const/4 v1, 0x2

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@17
    .line 31
    return-void
.end method

.method public static writeBrowserSnapCenter()V
    .registers 2

    #@0
    .prologue
    .line 34
    const v0, 0x11206

    #@3
    const/4 v1, 0x0

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@9
    .line 35
    return-void
.end method

.method public static writeBrowserTextSizeChange(II)V
    .registers 6
    .parameter "oldsize"
    .parameter "newsize"

    #@0
    .prologue
    .line 38
    const v0, 0x11207

    #@3
    const/4 v1, 0x2

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@17
    .line 39
    return-void
.end method

.method public static writeBrowserZoomLevelChange(IIJ)V
    .registers 8
    .parameter "startLevel"
    .parameter "endLevel"
    .parameter "time"

    #@0
    .prologue
    .line 26
    const v0, 0x111d5

    #@3
    const/4 v1, 0x3

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1e
    .line 27
    return-void
.end method
