.class Landroid/webkit/WebStorageClassic$1;
.super Landroid/os/Handler;
.source "WebStorageClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/WebStorageClassic;->createUIHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/WebStorageClassic;


# direct methods
.method constructor <init>(Landroid/webkit/WebStorageClassic;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 64
    iput-object p1, p0, Landroid/webkit/WebStorageClassic$1;->this$0:Landroid/webkit/WebStorageClassic;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 67
    iget v4, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v4, :pswitch_data_52

    #@5
    .line 87
    :goto_5
    return-void

    #@6
    .line 69
    :pswitch_6
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v3, Ljava/util/Map;

    #@a
    .line 70
    .local v3, values:Ljava/util/Map;
    const-string/jumbo v4, "origins"

    #@d
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v2

    #@11
    check-cast v2, Ljava/util/Map;

    #@13
    .line 71
    .local v2, origins:Ljava/util/Map;
    const-string v4, "callback"

    #@15
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/webkit/ValueCallback;

    #@1b
    .line 72
    .local v1, callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/util/Map;>;"
    invoke-interface {v1, v2}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    #@1e
    goto :goto_5

    #@1f
    .line 76
    .end local v1           #callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/util/Map;>;"
    .end local v2           #origins:Ljava/util/Map;
    .end local v3           #values:Ljava/util/Map;
    :pswitch_1f
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    check-cast v3, Ljava/util/Map;

    #@23
    .line 77
    .restart local v3       #values:Ljava/util/Map;
    const-string v4, "callback"

    #@25
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    move-result-object v0

    #@29
    check-cast v0, Landroid/webkit/ValueCallback;

    #@2b
    .line 78
    .local v0, callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/lang/Long;>;"
    const-string/jumbo v4, "usage"

    #@2e
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v4

    #@32
    check-cast v4, Ljava/lang/Long;

    #@34
    invoke-interface {v0, v4}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    #@37
    goto :goto_5

    #@38
    .line 82
    .end local v0           #callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/lang/Long;>;"
    .end local v3           #values:Ljava/util/Map;
    :pswitch_38
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3a
    check-cast v3, Ljava/util/Map;

    #@3c
    .line 83
    .restart local v3       #values:Ljava/util/Map;
    const-string v4, "callback"

    #@3e
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    move-result-object v0

    #@42
    check-cast v0, Landroid/webkit/ValueCallback;

    #@44
    .line 84
    .restart local v0       #callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/lang/Long;>;"
    const-string/jumbo v4, "quota"

    #@47
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4a
    move-result-object v4

    #@4b
    check-cast v4, Ljava/lang/Long;

    #@4d
    invoke-interface {v0, v4}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    #@50
    goto :goto_5

    #@51
    .line 67
    nop

    #@52
    :pswitch_data_52
    .packed-switch 0x0
        :pswitch_6
        :pswitch_1f
        :pswitch_38
    .end packed-switch
.end method
