.class public final Landroid/webkit/CookieSyncManager;
.super Landroid/webkit/WebSyncManager;
.source "CookieSyncManager.java"


# static fields
.field private static sRef:Landroid/webkit/CookieSyncManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 63
    const-string v0, "CookieSyncManager"

    #@2
    invoke-direct {p0, p1, v0}, Landroid/webkit/WebSyncManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@5
    .line 64
    return-void
.end method

.method private static checkInstanceIsCreated()V
    .registers 2

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/webkit/CookieSyncManager;->sRef:Landroid/webkit/CookieSyncManager;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 115
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "CookieSyncManager::createInstance() needs to be called before CookieSyncManager::getInstance()"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 119
    :cond_c
    return-void
.end method

.method public static declared-synchronized createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 85
    const-class v1, Landroid/webkit/CookieSyncManager;

    #@2
    monitor-enter v1

    #@3
    if-nez p0, :cond_10

    #@5
    .line 86
    :try_start_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v2, "Invalid context argument"

    #@9
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_d

    #@d
    .line 85
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1

    #@f
    throw v0

    #@10
    .line 89
    :cond_10
    :try_start_10
    sget-object v0, Landroid/webkit/CookieSyncManager;->sRef:Landroid/webkit/CookieSyncManager;

    #@12
    if-nez v0, :cond_1b

    #@14
    .line 90
    new-instance v0, Landroid/webkit/CookieSyncManager;

    #@16
    invoke-direct {v0, p0}, Landroid/webkit/CookieSyncManager;-><init>(Landroid/content/Context;)V

    #@19
    sput-object v0, Landroid/webkit/CookieSyncManager;->sRef:Landroid/webkit/CookieSyncManager;

    #@1b
    .line 92
    :cond_1b
    sget-object v0, Landroid/webkit/CookieSyncManager;->sRef:Landroid/webkit/CookieSyncManager;
    :try_end_1d
    .catchall {:try_start_10 .. :try_end_1d} :catchall_d

    #@1d
    monitor-exit v1

    #@1e
    return-object v0
.end method

.method public static declared-synchronized getInstance()Landroid/webkit/CookieSyncManager;
    .registers 2

    #@0
    .prologue
    .line 74
    const-class v1, Landroid/webkit/CookieSyncManager;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    invoke-static {}, Landroid/webkit/CookieSyncManager;->checkInstanceIsCreated()V

    #@6
    .line 75
    sget-object v0, Landroid/webkit/CookieSyncManager;->sRef:Landroid/webkit/CookieSyncManager;
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    #@8
    monitor-exit v1

    #@9
    return-object v0

    #@a
    .line 74
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1

    #@c
    throw v0
.end method


# virtual methods
.method public bridge synthetic resetSync()V
    .registers 1

    #@0
    .prologue
    .line 58
    invoke-super {p0}, Landroid/webkit/WebSyncManager;->resetSync()V

    #@3
    return-void
.end method

.method public bridge synthetic run()V
    .registers 1

    #@0
    .prologue
    .line 58
    invoke-super {p0}, Landroid/webkit/WebSyncManager;->run()V

    #@3
    return-void
.end method

.method public bridge synthetic startSync()V
    .registers 1

    #@0
    .prologue
    .line 58
    invoke-super {p0}, Landroid/webkit/WebSyncManager;->startSync()V

    #@3
    return-void
.end method

.method public bridge synthetic stopSync()V
    .registers 1

    #@0
    .prologue
    .line 58
    invoke-super {p0}, Landroid/webkit/WebSyncManager;->stopSync()V

    #@3
    return-void
.end method

.method public bridge synthetic sync()V
    .registers 1

    #@0
    .prologue
    .line 58
    invoke-super {p0}, Landroid/webkit/WebSyncManager;->sync()V

    #@3
    return-void
.end method

.method protected syncFromRamToFlash()V
    .registers 3

    #@0
    .prologue
    .line 100
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    #@3
    move-result-object v0

    #@4
    .line 102
    .local v0, manager:Landroid/webkit/CookieManager;
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->acceptCookie()Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_b

    #@a
    .line 111
    :goto_a
    return-void

    #@b
    .line 106
    :cond_b
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->flushCookieStore()V

    #@e
    goto :goto_a
.end method
