.class Landroid/webkit/ByteArrayBuilder;
.super Ljava/lang/Object;
.source "ByteArrayBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/ByteArrayBuilder$Chunk;
    }
.end annotation


# static fields
.field private static final DEFAULT_CAPACITY:I = 0x2000

.field private static final sPool:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/webkit/ByteArrayBuilder$Chunk;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sQueue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Landroid/webkit/ByteArrayBuilder$Chunk;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mChunks:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/webkit/ByteArrayBuilder$Chunk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 34
    new-instance v0, Ljava/util/LinkedList;

    #@2
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@5
    sput-object v0, Landroid/webkit/ByteArrayBuilder;->sPool:Ljava/util/LinkedList;

    #@7
    .line 37
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    #@9
    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    #@c
    sput-object v0, Landroid/webkit/ByteArrayBuilder;->sQueue:Ljava/lang/ref/ReferenceQueue;

    #@e
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    new-instance v0, Ljava/util/LinkedList;

    #@5
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/ByteArrayBuilder;->mChunks:Ljava/util/LinkedList;

    #@a
    .line 44
    return-void
.end method

.method private obtainChunk(I)Landroid/webkit/ByteArrayBuilder$Chunk;
    .registers 5
    .parameter "length"

    #@0
    .prologue
    .line 113
    const/16 v1, 0x2000

    #@2
    if-ge p1, v1, :cond_6

    #@4
    .line 114
    const/16 p1, 0x2000

    #@6
    .line 116
    :cond_6
    sget-object v2, Landroid/webkit/ByteArrayBuilder;->sPool:Ljava/util/LinkedList;

    #@8
    monitor-enter v2

    #@9
    .line 118
    :try_start_9
    invoke-direct {p0}, Landroid/webkit/ByteArrayBuilder;->processPoolLocked()V

    #@c
    .line 119
    sget-object v1, Landroid/webkit/ByteArrayBuilder;->sPool:Ljava/util/LinkedList;

    #@e
    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_26

    #@14
    .line 120
    sget-object v1, Landroid/webkit/ByteArrayBuilder;->sPool:Ljava/util/LinkedList;

    #@16
    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Ljava/lang/ref/SoftReference;

    #@1c
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/webkit/ByteArrayBuilder$Chunk;

    #@22
    .line 123
    .local v0, c:Landroid/webkit/ByteArrayBuilder$Chunk;
    if-eqz v0, :cond_26

    #@24
    .line 124
    monitor-exit v2

    #@25
    .line 127
    .end local v0           #c:Landroid/webkit/ByteArrayBuilder$Chunk;
    :goto_25
    return-object v0

    #@26
    :cond_26
    new-instance v0, Landroid/webkit/ByteArrayBuilder$Chunk;

    #@28
    invoke-direct {v0, p1}, Landroid/webkit/ByteArrayBuilder$Chunk;-><init>(I)V

    #@2b
    monitor-exit v2

    #@2c
    goto :goto_25

    #@2d
    .line 128
    :catchall_2d
    move-exception v1

    #@2e
    monitor-exit v2
    :try_end_2f
    .catchall {:try_start_9 .. :try_end_2f} :catchall_2d

    #@2f
    throw v1
.end method

.method private processPoolLocked()V
    .registers 3

    #@0
    .prologue
    .line 103
    :goto_0
    sget-object v1, Landroid/webkit/ByteArrayBuilder;->sQueue:Ljava/lang/ref/ReferenceQueue;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/ref/SoftReference;

    #@8
    .line 104
    .local v0, entry:Ljava/lang/ref/SoftReference;,"Ljava/lang/ref/SoftReference<Landroid/webkit/ByteArrayBuilder$Chunk;>;"
    if-nez v0, :cond_b

    #@a
    .line 109
    return-void

    #@b
    .line 107
    :cond_b
    sget-object v1, Landroid/webkit/ByteArrayBuilder;->sPool:Ljava/util/LinkedList;

    #@d
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    #@10
    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized append([BII)V
    .registers 9
    .parameter "array"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 47
    monitor-enter p0

    #@1
    :goto_1
    if-lez p3, :cond_4c

    #@3
    .line 48
    const/4 v2, 0x0

    #@4
    .line 49
    .local v2, c:Landroid/webkit/ByteArrayBuilder$Chunk;
    :try_start_4
    iget-object v3, p0, Landroid/webkit/ByteArrayBuilder;->mChunks:Ljava/util/LinkedList;

    #@6
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_2e

    #@c
    .line 50
    invoke-direct {p0, p3}, Landroid/webkit/ByteArrayBuilder;->obtainChunk(I)Landroid/webkit/ByteArrayBuilder$Chunk;

    #@f
    move-result-object v2

    #@10
    .line 51
    iget-object v3, p0, Landroid/webkit/ByteArrayBuilder;->mChunks:Ljava/util/LinkedList;

    #@12
    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    #@15
    .line 59
    :cond_15
    :goto_15
    iget-object v3, v2, Landroid/webkit/ByteArrayBuilder$Chunk;->mArray:[B

    #@17
    array-length v3, v3

    #@18
    iget v4, v2, Landroid/webkit/ByteArrayBuilder$Chunk;->mLength:I

    #@1a
    sub-int/2addr v3, v4

    #@1b
    invoke-static {p3, v3}, Ljava/lang/Math;->min(II)I

    #@1e
    move-result v1

    #@1f
    .line 60
    .local v1, amount:I
    iget-object v3, v2, Landroid/webkit/ByteArrayBuilder$Chunk;->mArray:[B

    #@21
    iget v4, v2, Landroid/webkit/ByteArrayBuilder$Chunk;->mLength:I

    #@23
    invoke-static {p1, p2, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@26
    .line 61
    iget v3, v2, Landroid/webkit/ByteArrayBuilder$Chunk;->mLength:I

    #@28
    add-int/2addr v3, v1

    #@29
    iput v3, v2, Landroid/webkit/ByteArrayBuilder$Chunk;->mLength:I

    #@2b
    .line 62
    sub-int/2addr p3, v1

    #@2c
    .line 63
    add-int/2addr p2, v1

    #@2d
    .line 64
    goto :goto_1

    #@2e
    .line 53
    .end local v1           #amount:I
    :cond_2e
    iget-object v3, p0, Landroid/webkit/ByteArrayBuilder;->mChunks:Ljava/util/LinkedList;

    #@30
    invoke-virtual {v3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    #@33
    move-result-object v3

    #@34
    move-object v0, v3

    #@35
    check-cast v0, Landroid/webkit/ByteArrayBuilder$Chunk;

    #@37
    move-object v2, v0

    #@38
    .line 54
    iget v3, v2, Landroid/webkit/ByteArrayBuilder$Chunk;->mLength:I

    #@3a
    iget-object v4, v2, Landroid/webkit/ByteArrayBuilder$Chunk;->mArray:[B

    #@3c
    array-length v4, v4

    #@3d
    if-ne v3, v4, :cond_15

    #@3f
    .line 55
    invoke-direct {p0, p3}, Landroid/webkit/ByteArrayBuilder;->obtainChunk(I)Landroid/webkit/ByteArrayBuilder$Chunk;

    #@42
    move-result-object v2

    #@43
    .line 56
    iget-object v3, p0, Landroid/webkit/ByteArrayBuilder;->mChunks:Ljava/util/LinkedList;

    #@45
    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
    :try_end_48
    .catchall {:try_start_4 .. :try_end_48} :catchall_49

    #@48
    goto :goto_15

    #@49
    .line 47
    :catchall_49
    move-exception v3

    #@4a
    monitor-exit p0

    #@4b
    throw v3

    #@4c
    .line 65
    .end local v2           #c:Landroid/webkit/ByteArrayBuilder$Chunk;
    :cond_4c
    monitor-exit p0

    #@4d
    return-void
.end method

.method public declared-synchronized clear()V
    .registers 3

    #@0
    .prologue
    .line 93
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0}, Landroid/webkit/ByteArrayBuilder;->getFirstChunk()Landroid/webkit/ByteArrayBuilder$Chunk;

    #@4
    move-result-object v0

    #@5
    .line 94
    .local v0, c:Landroid/webkit/ByteArrayBuilder$Chunk;
    :goto_5
    if-eqz v0, :cond_f

    #@7
    .line 95
    invoke-virtual {v0}, Landroid/webkit/ByteArrayBuilder$Chunk;->release()V

    #@a
    .line 96
    invoke-virtual {p0}, Landroid/webkit/ByteArrayBuilder;->getFirstChunk()Landroid/webkit/ByteArrayBuilder$Chunk;
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_11

    #@d
    move-result-object v0

    #@e
    goto :goto_5

    #@f
    .line 98
    :cond_f
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 93
    .end local v0           #c:Landroid/webkit/ByteArrayBuilder$Chunk;
    :catchall_11
    move-exception v1

    #@12
    monitor-exit p0

    #@13
    throw v1
.end method

.method public declared-synchronized getByteSize()I
    .registers 6

    #@0
    .prologue
    .line 83
    monitor-enter p0

    #@1
    const/4 v2, 0x0

    #@2
    .line 84
    .local v2, total:I
    :try_start_2
    iget-object v3, p0, Landroid/webkit/ByteArrayBuilder;->mChunks:Ljava/util/LinkedList;

    #@4
    const/4 v4, 0x0

    #@5
    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    #@8
    move-result-object v1

    #@9
    .line 85
    .local v1, it:Ljava/util/ListIterator;,"Ljava/util/ListIterator<Landroid/webkit/ByteArrayBuilder$Chunk;>;"
    :goto_9
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_19

    #@f
    .line 86
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/webkit/ByteArrayBuilder$Chunk;

    #@15
    .line 87
    .local v0, c:Landroid/webkit/ByteArrayBuilder$Chunk;
    iget v3, v0, Landroid/webkit/ByteArrayBuilder$Chunk;->mLength:I
    :try_end_17
    .catchall {:try_start_2 .. :try_end_17} :catchall_1b

    #@17
    add-int/2addr v2, v3

    #@18
    .line 88
    goto :goto_9

    #@19
    .line 89
    .end local v0           #c:Landroid/webkit/ByteArrayBuilder$Chunk;
    :cond_19
    monitor-exit p0

    #@1a
    return v2

    #@1b
    .line 83
    .end local v1           #it:Ljava/util/ListIterator;,"Ljava/util/ListIterator<Landroid/webkit/ByteArrayBuilder$Chunk;>;"
    :catchall_1b
    move-exception v3

    #@1c
    monitor-exit p0

    #@1d
    throw v3
.end method

.method public declared-synchronized getFirstChunk()Landroid/webkit/ByteArrayBuilder$Chunk;
    .registers 2

    #@0
    .prologue
    .line 74
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/ByteArrayBuilder;->mChunks:Ljava/util/LinkedList;

    #@3
    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_15

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_c

    #@9
    const/4 v0, 0x0

    #@a
    .line 75
    :goto_a
    monitor-exit p0

    #@b
    return-object v0

    #@c
    :cond_c
    :try_start_c
    iget-object v0, p0, Landroid/webkit/ByteArrayBuilder;->mChunks:Ljava/util/LinkedList;

    #@e
    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/webkit/ByteArrayBuilder$Chunk;
    :try_end_14
    .catchall {:try_start_c .. :try_end_14} :catchall_15

    #@14
    goto :goto_a

    #@15
    .line 74
    :catchall_15
    move-exception v0

    #@16
    monitor-exit p0

    #@17
    throw v0
.end method

.method public declared-synchronized isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 79
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/ByteArrayBuilder;->mChunks:Ljava/util/LinkedList;

    #@3
    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method
