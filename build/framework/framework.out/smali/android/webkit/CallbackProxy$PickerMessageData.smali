.class Landroid/webkit/CallbackProxy$PickerMessageData;
.super Ljava/lang/Object;
.source "CallbackProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/CallbackProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PickerMessageData"
.end annotation


# instance fields
.field private mType:Ljava/lang/String;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    .line 1779
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1780
    iput-object p1, p0, Landroid/webkit/CallbackProxy$PickerMessageData;->mType:Ljava/lang/String;

    #@5
    .line 1781
    iput-object p2, p0, Landroid/webkit/CallbackProxy$PickerMessageData;->mValue:Ljava/lang/String;

    #@7
    .line 1782
    return-void
.end method


# virtual methods
.method public getType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1785
    iget-object v0, p0, Landroid/webkit/CallbackProxy$PickerMessageData;->mType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1789
    iget-object v0, p0, Landroid/webkit/CallbackProxy$PickerMessageData;->mValue:Ljava/lang/String;

    #@2
    return-object v0
.end method
