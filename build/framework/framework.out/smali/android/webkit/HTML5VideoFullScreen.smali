.class public Landroid/webkit/HTML5VideoFullScreen;
.super Landroid/webkit/HTML5VideoView;
.source "HTML5VideoFullScreen.java"

# interfaces
.implements Landroid/widget/MediaController$MediaPlayerControl;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;,
        Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;
    }
.end annotation


# static fields
.field static final FULLSCREEN_OFF:I = 0x0

.field static final FULLSCREEN_SURFACECREATED:I = 0x2

.field static final FULLSCREEN_SURFACECREATING:I = 0x1

.field private static mLayout:Landroid/widget/FrameLayout;

.field private static mProgressView:Landroid/view/View;


# instance fields
.field private mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private final mCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field private mCanPause:Z

.field private mCanSeekBack:Z

.field private mCanSeekForward:Z

.field private mCurrentBufferPercentage:I

.field private mFullScreenMode:I

.field private mMediaController:Landroid/widget/MediaController;

.field private mPlayingWhenDestroyed:Z

.field mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mVideoHeight:I

.field private mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

.field private mVideoWidth:I


# direct methods
.method constructor <init>(Landroid/content/Context;IIZ)V
    .registers 7
    .parameter "context"
    .parameter "videoLayerId"
    .parameter "position"
    .parameter "skipPrepare"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 150
    invoke-direct {p0}, Landroid/webkit/HTML5VideoView;-><init>()V

    #@4
    .line 76
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@7
    .line 93
    iput-boolean v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mPlayingWhenDestroyed:Z

    #@9
    .line 94
    new-instance v0, Landroid/webkit/HTML5VideoFullScreen$1;

    #@b
    invoke-direct {v0, p0}, Landroid/webkit/HTML5VideoFullScreen$1;-><init>(Landroid/webkit/HTML5VideoFullScreen;)V

    #@e
    iput-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    #@10
    .line 134
    new-instance v0, Landroid/webkit/HTML5VideoFullScreen$2;

    #@12
    invoke-direct {v0, p0}, Landroid/webkit/HTML5VideoFullScreen$2;-><init>(Landroid/webkit/HTML5VideoFullScreen;)V

    #@15
    iput-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@17
    .line 244
    new-instance v0, Landroid/webkit/HTML5VideoFullScreen$3;

    #@19
    invoke-direct {v0, p0}, Landroid/webkit/HTML5VideoFullScreen$3;-><init>(Landroid/webkit/HTML5VideoFullScreen;)V

    #@1c
    iput-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    #@1e
    .line 351
    new-instance v0, Landroid/webkit/HTML5VideoFullScreen$4;

    #@20
    invoke-direct {v0, p0}, Landroid/webkit/HTML5VideoFullScreen$4;-><init>(Landroid/webkit/HTML5VideoFullScreen;)V

    #@23
    iput-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    #@25
    .line 151
    new-instance v0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@27
    invoke-direct {v0, p0, p1}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;-><init>(Landroid/webkit/HTML5VideoFullScreen;Landroid/content/Context;)V

    #@2a
    iput-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@2c
    .line 152
    iput v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mFullScreenMode:I

    #@2e
    .line 153
    iput v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoWidth:I

    #@30
    .line 154
    iput v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoHeight:I

    #@32
    .line 155
    invoke-virtual {p0, p2, p3, p4}, Landroid/webkit/HTML5VideoFullScreen;->init(IIZ)V

    #@35
    .line 156
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/HTML5VideoFullScreen;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoWidth:I

    #@2
    return v0
.end method

.method static synthetic access$002(Landroid/webkit/HTML5VideoFullScreen;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    iput p1, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoWidth:I

    #@2
    return p1
.end method

.method static synthetic access$100(Landroid/webkit/HTML5VideoFullScreen;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoHeight:I

    #@2
    return v0
.end method

.method static synthetic access$1000()Landroid/view/View;
    .registers 1

    #@0
    .prologue
    .line 36
    sget-object v0, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Landroid/view/View;)Landroid/view/View;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 36
    sput-object p0, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@2
    return-object p0
.end method

.method static synthetic access$102(Landroid/webkit/HTML5VideoFullScreen;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    iput p1, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoHeight:I

    #@2
    return p1
.end method

.method static synthetic access$1102(Landroid/webkit/HTML5VideoFullScreen;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    iput p1, p0, Landroid/webkit/HTML5VideoFullScreen;->mCurrentBufferPercentage:I

    #@2
    return p1
.end method

.method static synthetic access$200(Landroid/webkit/HTML5VideoFullScreen;)Landroid/widget/MediaController;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Landroid/webkit/HTML5VideoFullScreen;Landroid/widget/MediaController;)Landroid/widget/MediaController;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    iput-object p1, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@2
    return-object p1
.end method

.method static synthetic access$302(Landroid/webkit/HTML5VideoFullScreen;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    iput-object p1, p0, Landroid/webkit/HTML5VideoFullScreen;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@2
    return-object p1
.end method

.method static synthetic access$402(Landroid/webkit/HTML5VideoFullScreen;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    iput p1, p0, Landroid/webkit/HTML5VideoFullScreen;->mFullScreenMode:I

    #@2
    return p1
.end method

.method static synthetic access$500(Landroid/webkit/HTML5VideoFullScreen;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Landroid/webkit/HTML5VideoFullScreen;->prepareForFullScreen()V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/webkit/HTML5VideoFullScreen;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-boolean v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mPlayingWhenDestroyed:Z

    #@2
    return v0
.end method

.method static synthetic access$602(Landroid/webkit/HTML5VideoFullScreen;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    iput-boolean p1, p0, Landroid/webkit/HTML5VideoFullScreen;->mPlayingWhenDestroyed:Z

    #@2
    return p1
.end method

.method static synthetic access$700(Landroid/webkit/HTML5VideoFullScreen;)Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/webkit/HTML5VideoFullScreen;)Landroid/view/SurfaceView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Landroid/webkit/HTML5VideoFullScreen;->getSurfaceView()Landroid/view/SurfaceView;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$900()Landroid/widget/FrameLayout;
    .registers 1

    #@0
    .prologue
    .line 36
    sget-object v0, Landroid/webkit/HTML5VideoFullScreen;->mLayout:Landroid/widget/FrameLayout;

    #@2
    return-object v0
.end method

.method static synthetic access$902(Landroid/widget/FrameLayout;)Landroid/widget/FrameLayout;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 36
    sput-object p0, Landroid/webkit/HTML5VideoFullScreen;->mLayout:Landroid/widget/FrameLayout;

    #@2
    return-object p0
.end method

.method private attachMediaController()V
    .registers 3

    #@0
    .prologue
    .line 164
    sget-object v0, Landroid/webkit/HTML5VideoFullScreen;->mPlayer:Landroid/media/MediaPlayer;

    #@2
    if-eqz v0, :cond_1a

    #@4
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@6
    if-eqz v0, :cond_1a

    #@8
    .line 165
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@a
    invoke-virtual {v0, p0}, Landroid/widget/MediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    #@d
    .line 166
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@f
    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@11
    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    #@14
    .line 168
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@16
    const/4 v1, 0x0

    #@17
    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->setEnabled(Z)V

    #@1a
    .line 170
    :cond_1a
    return-void
.end method

.method private getSurfaceView()Landroid/view/SurfaceView;
    .registers 2

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@2
    return-object v0
.end method

.method private prepareForFullScreen()V
    .registers 4

    #@0
    .prologue
    .line 178
    new-instance v0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;

    #@2
    iget-object v1, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@4
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@7
    move-result-object v1

    #@8
    sget-object v2, Landroid/webkit/HTML5VideoFullScreen;->mLayout:Landroid/widget/FrameLayout;

    #@a
    invoke-direct {v0, v1, v2}, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;-><init>(Landroid/content/Context;Landroid/view/View;)V

    #@d
    .line 179
    .local v0, mc:Landroid/widget/MediaController;
    sget-object v1, Landroid/webkit/HTML5VideoFullScreen;->mLayout:Landroid/widget/FrameLayout;

    #@f
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getSystemUiVisibility()I

    #@12
    move-result v1

    #@13
    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->setSystemUiVisibility(I)V

    #@16
    .line 180
    invoke-direct {p0, v0}, Landroid/webkit/HTML5VideoFullScreen;->setMediaController(Landroid/widget/MediaController;)V

    #@19
    .line 181
    sget-object v1, Landroid/webkit/HTML5VideoFullScreen;->mPlayer:Landroid/media/MediaPlayer;

    #@1b
    const/4 v2, 0x1

    #@1c
    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    #@1f
    .line 182
    sget-object v1, Landroid/webkit/HTML5VideoFullScreen;->mPlayer:Landroid/media/MediaPlayer;

    #@21
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@23
    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    #@26
    .line 183
    iget-object v1, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@28
    invoke-virtual {p0, v1}, Landroid/webkit/HTML5VideoFullScreen;->prepareDataAndDisplayMode(Landroid/webkit/HTML5VideoViewProxy;)V

    #@2b
    .line 184
    return-void
.end method

.method private setMediaController(Landroid/widget/MediaController;)V
    .registers 2
    .parameter "m"

    #@0
    .prologue
    .line 159
    iput-object p1, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@2
    .line 160
    invoke-direct {p0}, Landroid/webkit/HTML5VideoFullScreen;->attachMediaController()V

    #@5
    .line 161
    return-void
.end method

.method private toggleMediaControlsVisiblity()V
    .registers 2

    #@0
    .prologue
    .line 188
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@2
    invoke-virtual {v0}, Landroid/widget/MediaController;->isShowing()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_e

    #@8
    .line 189
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@a
    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    #@d
    .line 193
    :goto_d
    return-void

    #@e
    .line 191
    :cond_e
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@10
    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    #@13
    goto :goto_d
.end method


# virtual methods
.method public canPause()Z
    .registers 2

    #@0
    .prologue
    .line 322
    iget-boolean v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mCanPause:Z

    #@2
    return v0
.end method

.method public canSeekBackward()Z
    .registers 2

    #@0
    .prologue
    .line 327
    iget-boolean v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mCanSeekBack:Z

    #@2
    return v0
.end method

.method public canSeekForward()Z
    .registers 2

    #@0
    .prologue
    .line 332
    iget-boolean v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mCanSeekForward:Z

    #@2
    return v0
.end method

.method public decideDisplayMode()V
    .registers 3

    #@0
    .prologue
    .line 174
    sget-object v0, Landroid/webkit/HTML5VideoFullScreen;->mPlayer:Landroid/media/MediaPlayer;

    #@2
    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@4
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    #@7
    .line 175
    return-void
.end method

.method public enterFullScreenVideoState(ILandroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebViewClassic;)V
    .registers 11
    .parameter "layerId"
    .parameter "proxy"
    .parameter "webView"

    #@0
    .prologue
    const/4 v6, -0x2

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 272
    iput v5, p0, Landroid/webkit/HTML5VideoFullScreen;->mFullScreenMode:I

    #@5
    .line 273
    iput v4, p0, Landroid/webkit/HTML5VideoFullScreen;->mCurrentBufferPercentage:I

    #@7
    .line 274
    sget-object v2, Landroid/webkit/HTML5VideoFullScreen;->mPlayer:Landroid/media/MediaPlayer;

    #@9
    iget-object v3, p0, Landroid/webkit/HTML5VideoFullScreen;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    #@b
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    #@e
    .line 275
    iput-object p2, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@10
    .line 277
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@12
    invoke-virtual {v2}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@15
    move-result-object v2

    #@16
    iget-object v3, p0, Landroid/webkit/HTML5VideoFullScreen;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    #@18
    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    #@1b
    .line 278
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@1d
    invoke-virtual {v2}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@20
    move-result-object v2

    #@21
    const/4 v3, 0x3

    #@22
    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->setType(I)V

    #@25
    .line 279
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@27
    invoke-virtual {v2, v5}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->setFocusable(Z)V

    #@2a
    .line 280
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@2c
    invoke-virtual {v2, v5}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->setFocusableInTouchMode(Z)V

    #@2f
    .line 281
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@31
    invoke-virtual {v2}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->requestFocus()Z

    #@34
    .line 282
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@36
    iget-object v3, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@38
    invoke-virtual {v2, v3}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    #@3b
    .line 285
    new-instance v2, Landroid/widget/FrameLayout;

    #@3d
    iget-object v3, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@3f
    invoke-virtual {v3}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@42
    move-result-object v3

    #@43
    invoke-direct {v2, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@46
    sput-object v2, Landroid/webkit/HTML5VideoFullScreen;->mLayout:Landroid/widget/FrameLayout;

    #@48
    .line 286
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    #@4a
    const/16 v2, 0x11

    #@4c
    invoke-direct {v1, v6, v6, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    #@4f
    .line 291
    .local v1, layoutParams:Landroid/widget/FrameLayout$LayoutParams;
    sget-object v2, Landroid/webkit/HTML5VideoFullScreen;->mLayout:Landroid/widget/FrameLayout;

    #@51
    invoke-direct {p0}, Landroid/webkit/HTML5VideoFullScreen;->getSurfaceView()Landroid/view/SurfaceView;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v2, v3, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@58
    .line 293
    sget-object v2, Landroid/webkit/HTML5VideoFullScreen;->mLayout:Landroid/widget/FrameLayout;

    #@5a
    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@5d
    .line 294
    invoke-virtual {p3}, Landroid/webkit/WebViewClassic;->getWebChromeClient()Landroid/webkit/WebChromeClient;

    #@60
    move-result-object v0

    #@61
    .line 295
    .local v0, client:Landroid/webkit/WebChromeClient;
    if-eqz v0, :cond_8d

    #@63
    .line 296
    sget-object v2, Landroid/webkit/HTML5VideoFullScreen;->mLayout:Landroid/widget/FrameLayout;

    #@65
    iget-object v3, p0, Landroid/webkit/HTML5VideoFullScreen;->mCallback:Landroid/webkit/WebChromeClient$CustomViewCallback;

    #@67
    invoke-virtual {v0, v2, v3}, Landroid/webkit/WebChromeClient;->onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    #@6a
    .line 299
    invoke-virtual {p3}, Landroid/webkit/WebViewClassic;->getViewManager()Landroid/webkit/ViewManager;

    #@6d
    move-result-object v2

    #@6e
    if-eqz v2, :cond_77

    #@70
    .line 300
    invoke-virtual {p3}, Landroid/webkit/WebViewClassic;->getViewManager()Landroid/webkit/ViewManager;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v2}, Landroid/webkit/ViewManager;->hideAll()V

    #@77
    .line 302
    :cond_77
    invoke-virtual {v0}, Landroid/webkit/WebChromeClient;->getVideoLoadingProgressView()Landroid/view/View;

    #@7a
    move-result-object v2

    #@7b
    sput-object v2, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@7d
    .line 303
    sget-object v2, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@7f
    if-eqz v2, :cond_8d

    #@81
    .line 304
    sget-object v2, Landroid/webkit/HTML5VideoFullScreen;->mLayout:Landroid/widget/FrameLayout;

    #@83
    sget-object v3, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@85
    invoke-virtual {v2, v3, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@88
    .line 305
    sget-object v2, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@8a
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    #@8d
    .line 308
    :cond_8d
    return-void
.end method

.method public fullScreenExited()Z
    .registers 2

    #@0
    .prologue
    .line 241
    sget-object v0, Landroid/webkit/HTML5VideoFullScreen;->mLayout:Landroid/widget/FrameLayout;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public getBufferPercentage()I
    .registers 2

    #@0
    .prologue
    .line 337
    sget-object v0, Landroid/webkit/HTML5VideoFullScreen;->mPlayer:Landroid/media/MediaPlayer;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 338
    iget v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mCurrentBufferPercentage:I

    #@6
    .line 340
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public isFullScreenMode()Z
    .registers 2

    #@0
    .prologue
    .line 316
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .registers 8
    .parameter "mp"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v3, 0x1

    #@4
    .line 197
    invoke-super {p0, p1}, Landroid/webkit/HTML5VideoView;->onPrepared(Landroid/media/MediaPlayer;)V

    #@7
    .line 199
    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@9
    invoke-virtual {v1, p0}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@c
    .line 201
    invoke-virtual {p1, v2, v2}, Landroid/media/MediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    #@f
    move-result-object v0

    #@10
    .line 203
    .local v0, data:Landroid/media/Metadata;
    if-eqz v0, :cond_86

    #@12
    .line 204
    invoke-virtual {v0, v3}, Landroid/media/Metadata;->has(I)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_1e

    #@18
    invoke-virtual {v0, v3}, Landroid/media/Metadata;->getBoolean(I)Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_80

    #@1e
    :cond_1e
    move v1, v3

    #@1f
    :goto_1f
    iput-boolean v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mCanPause:Z

    #@21
    .line 206
    invoke-virtual {v0, v4}, Landroid/media/Metadata;->has(I)Z

    #@24
    move-result v1

    #@25
    if-eqz v1, :cond_2d

    #@27
    invoke-virtual {v0, v4}, Landroid/media/Metadata;->getBoolean(I)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_82

    #@2d
    :cond_2d
    move v1, v3

    #@2e
    :goto_2e
    iput-boolean v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mCanSeekBack:Z

    #@30
    .line 208
    invoke-virtual {v0, v5}, Landroid/media/Metadata;->has(I)Z

    #@33
    move-result v1

    #@34
    if-eqz v1, :cond_3c

    #@36
    invoke-virtual {v0, v5}, Landroid/media/Metadata;->getBoolean(I)Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_84

    #@3c
    :cond_3c
    move v1, v3

    #@3d
    :goto_3d
    iput-boolean v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mCanSeekForward:Z

    #@3f
    .line 214
    :goto_3f
    invoke-virtual {p0}, Landroid/webkit/HTML5VideoFullScreen;->getStartWhenPrepared()Z

    #@42
    move-result v1

    #@43
    if-eqz v1, :cond_4d

    #@45
    .line 215
    sget-object v1, Landroid/webkit/HTML5VideoFullScreen;->mPlayer:Landroid/media/MediaPlayer;

    #@47
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    #@4a
    .line 217
    invoke-virtual {p0, v2}, Landroid/webkit/HTML5VideoFullScreen;->setStartWhenPrepared(Z)V

    #@4d
    .line 223
    :cond_4d
    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@4f
    if-eqz v1, :cond_5b

    #@51
    .line 224
    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@53
    invoke-virtual {v1, v3}, Landroid/widget/MediaController;->setEnabled(Z)V

    #@56
    .line 225
    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@58
    invoke-virtual {v1}, Landroid/widget/MediaController;->show()V

    #@5b
    .line 228
    :cond_5b
    sget-object v1, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@5d
    if-eqz v1, :cond_66

    #@5f
    .line 229
    sget-object v1, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@61
    const/16 v2, 0x8

    #@63
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    #@66
    .line 232
    :cond_66
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    #@69
    move-result v1

    #@6a
    iput v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoWidth:I

    #@6c
    .line 233
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    #@6f
    move-result v1

    #@70
    iput v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoHeight:I

    #@72
    .line 235
    iget-object v1, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoSurfaceView:Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;

    #@74
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@77
    move-result-object v1

    #@78
    iget v2, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoWidth:I

    #@7a
    iget v3, p0, Landroid/webkit/HTML5VideoFullScreen;->mVideoHeight:I

    #@7c
    invoke-interface {v1, v2, v3}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    #@7f
    .line 237
    return-void

    #@80
    :cond_80
    move v1, v2

    #@81
    .line 204
    goto :goto_1f

    #@82
    :cond_82
    move v1, v2

    #@83
    .line 206
    goto :goto_2e

    #@84
    :cond_84
    move v1, v2

    #@85
    .line 208
    goto :goto_3d

    #@86
    .line 211
    :cond_86
    iput-boolean v3, p0, Landroid/webkit/HTML5VideoFullScreen;->mCanSeekForward:Z

    #@88
    iput-boolean v3, p0, Landroid/webkit/HTML5VideoFullScreen;->mCanSeekBack:Z

    #@8a
    iput-boolean v3, p0, Landroid/webkit/HTML5VideoFullScreen;->mCanPause:Z

    #@8c
    goto :goto_3f
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "v"
    .parameter "event"

    #@0
    .prologue
    .line 361
    iget v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mFullScreenMode:I

    #@2
    const/4 v1, 0x2

    #@3
    if-lt v0, v1, :cond_c

    #@5
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 363
    invoke-direct {p0}, Landroid/webkit/HTML5VideoFullScreen;->toggleMediaControlsVisiblity()V

    #@c
    .line 365
    :cond_c
    const/4 v0, 0x0

    #@d
    return v0
.end method

.method public showControllerInFullScreen()V
    .registers 3

    #@0
    .prologue
    .line 345
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 346
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen;->mMediaController:Landroid/widget/MediaController;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->show(I)V

    #@a
    .line 348
    :cond_a
    return-void
.end method

.method protected switchProgressView(Z)V
    .registers 4
    .parameter "playerBuffering"

    #@0
    .prologue
    .line 370
    sget-object v0, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 371
    if-eqz p1, :cond_d

    #@6
    .line 372
    sget-object v0, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@c
    .line 377
    :cond_c
    :goto_c
    return-void

    #@d
    .line 374
    :cond_d
    sget-object v0, Landroid/webkit/HTML5VideoFullScreen;->mProgressView:Landroid/view/View;

    #@f
    const/16 v1, 0x8

    #@11
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@14
    goto :goto_c
.end method
