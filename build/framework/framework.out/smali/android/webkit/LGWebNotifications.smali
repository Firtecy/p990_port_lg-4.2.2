.class public Landroid/webkit/LGWebNotifications;
.super Landroid/content/BroadcastReceiver;
.source "LGWebNotifications.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "LGWebNotifications"

.field static final PERMISSION_ALLOWED:I = 0x0

.field static final PERMISSION_DENIED:I = 0x2

.field static final PERMISSION_NOT_ALLOWED:I = 0x1

.field static final PERMISSION_PREF_HOSTNAME:Ljava/lang/String; = "url"

.field static final PERMISSION_PREF_KEY:Ljava/lang/String; = "privacy_enable_web_notifications"

.field static final PERMISSION_PREF_VALUE:Ljava/lang/String; = "permission"

.field private static volatile sNotifications:Landroid/webkit/LGWebNotifications;


# instance fields
.field private ACTION_CLICK:Ljava/lang/String;

.field private ACTION_CLOSE:Ljava/lang/String;

.field private NOTI_ID:Ljava/lang/String;

.field public final PERMISSION_PREF_URI:Landroid/net/Uri;

.field private mContext:Landroid/content/Context;

.field private mCore:Landroid/webkit/WebViewCore;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mPermissionRequestDialog:Landroid/app/AlertDialog;

.field private mVisibleNotis:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWebViewClassic:Landroid/webkit/WebViewClassic;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/webkit/WebViewCore;Landroid/webkit/WebViewClassic;)V
    .registers 7
    .parameter "c"
    .parameter "core"
    .parameter "v"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 58
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@4
    .line 45
    iput-object v1, p0, Landroid/webkit/LGWebNotifications;->mNotificationManager:Landroid/app/NotificationManager;

    #@6
    .line 46
    iput-object v1, p0, Landroid/webkit/LGWebNotifications;->mPermissionRequestDialog:Landroid/app/AlertDialog;

    #@8
    .line 51
    const-string v1, "android.webkit.noti.action_click"

    #@a
    iput-object v1, p0, Landroid/webkit/LGWebNotifications;->ACTION_CLICK:Ljava/lang/String;

    #@c
    .line 52
    const-string v1, "android.webkit.noti.action_close"

    #@e
    iput-object v1, p0, Landroid/webkit/LGWebNotifications;->ACTION_CLOSE:Ljava/lang/String;

    #@10
    .line 171
    sget-object v1, Landroid/provider/BrowserContract;->AUTHORITY_URI:Landroid/net/Uri;

    #@12
    const-string/jumbo v2, "noti_permissions"

    #@15
    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@18
    move-result-object v1

    #@19
    iput-object v1, p0, Landroid/webkit/LGWebNotifications;->PERMISSION_PREF_URI:Landroid/net/Uri;

    #@1b
    .line 59
    iput-object p1, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@1d
    .line 60
    iput-object p2, p0, Landroid/webkit/LGWebNotifications;->mCore:Landroid/webkit/WebViewCore;

    #@1f
    .line 61
    iput-object p3, p0, Landroid/webkit/LGWebNotifications;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@21
    .line 62
    const-string/jumbo v1, "notiId"

    #@24
    iput-object v1, p0, Landroid/webkit/LGWebNotifications;->NOTI_ID:Ljava/lang/String;

    #@26
    .line 64
    new-instance v0, Landroid/content/IntentFilter;

    #@28
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@2b
    .line 65
    .local v0, intFilt:Landroid/content/IntentFilter;
    new-instance v1, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    iget-object v2, p0, Landroid/webkit/LGWebNotifications;->ACTION_CLICK:Ljava/lang/String;

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@41
    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    iget-object v2, p0, Landroid/webkit/LGWebNotifications;->ACTION_CLOSE:Ljava/lang/String;

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@57
    .line 68
    iget-object v1, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@59
    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@5c
    .line 70
    iget-object v1, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@5e
    const-string/jumbo v2, "notification"

    #@61
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@64
    move-result-object v1

    #@65
    check-cast v1, Landroid/app/NotificationManager;

    #@67
    iput-object v1, p0, Landroid/webkit/LGWebNotifications;->mNotificationManager:Landroid/app/NotificationManager;

    #@69
    .line 72
    new-instance v1, Ljava/util/ArrayList;

    #@6b
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@6e
    iput-object v1, p0, Landroid/webkit/LGWebNotifications;->mVisibleNotis:Ljava/util/List;

    #@70
    .line 73
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/LGWebNotifications;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/webkit/LGWebNotifications;->downloadBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Landroid/webkit/LGWebNotifications;ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/LGWebNotifications;->show(ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    #@3
    return-void
.end method

.method static synthetic access$202(Landroid/webkit/LGWebNotifications;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    iput-object p1, p0, Landroid/webkit/LGWebNotifications;->mPermissionRequestDialog:Landroid/app/AlertDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Landroid/webkit/LGWebNotifications;)Landroid/webkit/WebViewCore;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mCore:Landroid/webkit/WebViewCore;

    #@2
    return-object v0
.end method

.method private click(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 317
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mCore:Landroid/webkit/WebViewCore;

    #@2
    const/16 v1, 0xe7

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewCore;->sendNotificationMessage(II)V

    #@7
    .line 318
    return-void
.end method

.method private close(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 312
    invoke-direct {p0, p1}, Landroid/webkit/LGWebNotifications;->setInvisible(I)V

    #@3
    .line 313
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mCore:Landroid/webkit/WebViewCore;

    #@5
    const/16 v1, 0xe8

    #@7
    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewCore;->sendNotificationMessage(II)V

    #@a
    .line 314
    return-void
.end method

.method private downloadBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 7
    .parameter "urlString"

    #@0
    .prologue
    .line 323
    :try_start_0
    new-instance v3, Ljava/net/URL;

    #@2
    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@5
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    #@8
    move-result-object v1

    #@9
    .line 324
    .local v1, conn:Ljava/net/URLConnection;
    new-instance v3, Ljava/io/BufferedInputStream;

    #@b
    invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    #@e
    move-result-object v4

    #@f
    invoke-direct {v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@12
    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_15} :catch_17

    #@15
    move-result-object v0

    #@16
    .line 328
    .end local v1           #conn:Ljava/net/URLConnection;
    :goto_16
    return-object v0

    #@17
    .line 326
    :catch_17
    move-exception v2

    #@18
    .line 327
    .local v2, e:Ljava/lang/Throwable;
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    #@1b
    .line 328
    const/4 v0, 0x0

    #@1c
    goto :goto_16
.end method

.method private isVisible(I)Z
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mVisibleNotis:Ljava/util/List;

    #@2
    new-instance v1, Ljava/lang/Integer;

    #@4
    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    #@7
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method private setInvisible(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mVisibleNotis:Ljava/util/List;

    #@2
    new-instance v1, Ljava/lang/Integer;

    #@4
    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    #@7
    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@a
    .line 117
    return-void
.end method

.method private setVisible(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mVisibleNotis:Ljava/util/List;

    #@2
    new-instance v1, Ljava/lang/Integer;

    #@4
    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    #@7
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@a
    .line 113
    return-void
.end method

.method private show(ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 12
    .parameter "id"
    .parameter "title"
    .parameter "text"
    .parameter "icon"

    #@0
    .prologue
    const/high16 v6, 0x800

    #@2
    .line 120
    invoke-direct {p0, p1}, Landroid/webkit/LGWebNotifications;->isVisible(I)Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_7b

    #@8
    .line 122
    new-instance v3, Landroid/content/Intent;

    #@a
    new-instance v4, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    iget-object v5, p0, Landroid/webkit/LGWebNotifications;->ACTION_CLICK:Ljava/lang/String;

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@20
    iget-object v4, p0, Landroid/webkit/LGWebNotifications;->NOTI_ID:Ljava/lang/String;

    #@22
    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@25
    move-result-object v0

    #@26
    .line 124
    .local v0, contentIntent:Landroid/content/Intent;
    new-instance v3, Landroid/content/Intent;

    #@28
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    iget-object v5, p0, Landroid/webkit/LGWebNotifications;->ACTION_CLOSE:Ljava/lang/String;

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3e
    iget-object v4, p0, Landroid/webkit/LGWebNotifications;->NOTI_ID:Ljava/lang/String;

    #@40
    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@43
    move-result-object v1

    #@44
    .line 127
    .local v1, deleteIntent:Landroid/content/Intent;
    new-instance v3, Landroid/app/Notification$Builder;

    #@46
    iget-object v4, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@48
    invoke-direct {v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    #@4b
    invoke-virtual {v3, p2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3, p3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@52
    move-result-object v3

    #@53
    const v4, 0x202036b

    #@56
    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v3, p4}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    #@5d
    move-result-object v3

    #@5e
    iget-object v4, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@60
    invoke-static {v4, p1, v0, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@67
    move-result-object v3

    #@68
    iget-object v4, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@6a
    invoke-static {v4, p1, v1, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    #@75
    move-result-object v2

    #@76
    .line 138
    .local v2, noti:Landroid/app/Notification;
    iget-object v3, p0, Landroid/webkit/LGWebNotifications;->mNotificationManager:Landroid/app/NotificationManager;

    #@78
    invoke-virtual {v3, p1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@7b
    .line 140
    .end local v0           #contentIntent:Landroid/content/Intent;
    .end local v1           #deleteIntent:Landroid/content/Intent;
    .end local v2           #noti:Landroid/app/Notification;
    :cond_7b
    return-void
.end method


# virtual methods
.method cancel(I)V
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 302
    invoke-direct {p0, p1}, Landroid/webkit/LGWebNotifications;->setInvisible(I)V

    #@3
    .line 303
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mNotificationManager:Landroid/app/NotificationManager;

    #@5
    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    #@8
    .line 304
    return-void
.end method

.method cancelAll()V
    .registers 2

    #@0
    .prologue
    .line 307
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mVisibleNotis:Ljava/util/List;

    #@2
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@5
    .line 308
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mNotificationManager:Landroid/app/NotificationManager;

    #@7
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    #@a
    .line 309
    return-void
.end method

.method cancelRequestsForPermission()V
    .registers 2

    #@0
    .prologue
    .line 294
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mPermissionRequestDialog:Landroid/app/AlertDialog;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 295
    iget-object v0, p0, Landroid/webkit/LGWebNotifications;->mPermissionRequestDialog:Landroid/app/AlertDialog;

    #@6
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    #@9
    .line 296
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/webkit/LGWebNotifications;->mPermissionRequestDialog:Landroid/app/AlertDialog;

    #@c
    .line 299
    :cond_c
    return-void
.end method

.method checkPermission()I
    .registers 18

    #@0
    .prologue
    .line 181
    :try_start_0
    move-object/from16 v0, p0

    #@2
    iget-object v1, v0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@7
    move-result-object v13

    #@8
    .line 182
    .local v13, prefs:Landroid/content/SharedPreferences;
    const-string/jumbo v1, "privacy_enable_web_notifications"

    #@b
    const/4 v2, 0x1

    #@c
    invoke-interface {v13, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_14

    #@12
    .line 183
    const/4 v14, 0x2

    #@13
    .line 223
    .end local v13           #prefs:Landroid/content/SharedPreferences;
    :cond_13
    :goto_13
    return v14

    #@14
    .line 185
    .restart local v13       #prefs:Landroid/content/SharedPreferences;
    :cond_14
    const/4 v14, 0x1

    #@15
    .line 187
    .local v14, result:I
    move-object/from16 v0, p0

    #@17
    iget-object v1, v0, Landroid/webkit/LGWebNotifications;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@19
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getUrl()Ljava/lang/String;

    #@1c
    move-result-object v12

    #@1d
    .line 188
    .local v12, pageUrl:Ljava/lang/String;
    new-instance v16, Ljava/net/URL;

    #@1f
    move-object/from16 v0, v16

    #@21
    invoke-direct {v0, v12}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@24
    .line 189
    .local v16, url:Ljava/net/URL;
    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@27
    move-result-object v10

    #@28
    .line 191
    .local v10, hostName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2a
    iget-object v1, v0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@2c
    move-object/from16 v0, p0

    #@2e
    iget-object v2, v0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@30
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@33
    move-result-object v2

    #@34
    move-object/from16 v0, p0

    #@36
    iget-object v3, v0, Landroid/webkit/LGWebNotifications;->PERMISSION_PREF_URI:Landroid/net/Uri;

    #@38
    const/4 v4, 0x0

    #@39
    const-string/jumbo v5, "url = ?"

    #@3c
    const/4 v6, 0x1

    #@3d
    new-array v6, v6, [Ljava/lang/String;

    #@3f
    const/4 v7, 0x0

    #@40
    aput-object v10, v6, v7

    #@42
    const/4 v7, 0x0

    #@43
    invoke-static/range {v1 .. v7}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_46
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_46} :catch_5e
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_46} :catch_6d
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_46} :catch_77

    #@46
    move-result-object v8

    #@47
    .line 201
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_13

    #@49
    .line 203
    :try_start_49
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@4c
    move-result v1

    #@4d
    if-eqz v1, :cond_5a

    #@4f
    .line 204
    const-string/jumbo v1, "permission"

    #@52
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@55
    move-result v11

    #@56
    .line 205
    .local v11, index:I
    invoke-interface {v8, v11}, Landroid/database/Cursor;->getInt(I)I
    :try_end_59
    .catchall {:try_start_49 .. :try_end_59} :catchall_68

    #@59
    move-result v14

    #@5a
    .line 208
    .end local v11           #index:I
    :cond_5a
    :try_start_5a
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_5d
    .catch Ljava/net/MalformedURLException; {:try_start_5a .. :try_end_5d} :catch_5e
    .catch Ljava/lang/ClassCastException; {:try_start_5a .. :try_end_5d} :catch_6d
    .catch Ljava/lang/Throwable; {:try_start_5a .. :try_end_5d} :catch_77

    #@5d
    goto :goto_13

    #@5e
    .line 214
    .end local v8           #cursor:Landroid/database/Cursor;
    .end local v10           #hostName:Ljava/lang/String;
    .end local v12           #pageUrl:Ljava/lang/String;
    .end local v13           #prefs:Landroid/content/SharedPreferences;
    .end local v14           #result:I
    .end local v16           #url:Ljava/net/URL;
    :catch_5e
    move-exception v9

    #@5f
    .line 215
    .local v9, e:Ljava/net/MalformedURLException;
    const-string v1, "NOTI"

    #@61
    const-string v2, "checkPermission. bad page url"

    #@63
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 216
    const/4 v14, 0x2

    #@67
    goto :goto_13

    #@68
    .line 208
    .end local v9           #e:Ljava/net/MalformedURLException;
    .restart local v8       #cursor:Landroid/database/Cursor;
    .restart local v10       #hostName:Ljava/lang/String;
    .restart local v12       #pageUrl:Ljava/lang/String;
    .restart local v13       #prefs:Landroid/content/SharedPreferences;
    .restart local v14       #result:I
    .restart local v16       #url:Ljava/net/URL;
    :catchall_68
    move-exception v1

    #@69
    :try_start_69
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@6c
    throw v1
    :try_end_6d
    .catch Ljava/net/MalformedURLException; {:try_start_69 .. :try_end_6d} :catch_5e
    .catch Ljava/lang/ClassCastException; {:try_start_69 .. :try_end_6d} :catch_6d
    .catch Ljava/lang/Throwable; {:try_start_69 .. :try_end_6d} :catch_77

    #@6d
    .line 217
    .end local v8           #cursor:Landroid/database/Cursor;
    .end local v10           #hostName:Ljava/lang/String;
    .end local v12           #pageUrl:Ljava/lang/String;
    .end local v13           #prefs:Landroid/content/SharedPreferences;
    .end local v14           #result:I
    .end local v16           #url:Ljava/net/URL;
    :catch_6d
    move-exception v9

    #@6e
    .line 218
    .local v9, e:Ljava/lang/ClassCastException;
    const-string v1, "NOTI"

    #@70
    const-string v2, "checkPermission. wrong data type in shared settings"

    #@72
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 219
    const/4 v14, 0x1

    #@76
    goto :goto_13

    #@77
    .line 221
    .end local v9           #e:Ljava/lang/ClassCastException;
    :catch_77
    move-exception v15

    #@78
    .line 222
    .local v15, t:Ljava/lang/Throwable;
    const-string v1, "NOTI"

    #@7a
    new-instance v2, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v3, "checkPermission. something else is wrong "

    #@81
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v2

    #@85
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v2

    #@89
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v2

    #@8d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    .line 223
    const/4 v14, 0x1

    #@91
    goto :goto_13
.end method

.method destroy()V
    .registers 5

    #@0
    .prologue
    .line 98
    :try_start_0
    iget-object v1, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_5} :catch_e

    #@5
    .line 103
    :goto_5
    iget-object v1, p0, Landroid/webkit/LGWebNotifications;->mNotificationManager:Landroid/app/NotificationManager;

    #@7
    invoke-virtual {v1}, Landroid/app/NotificationManager;->cancelAll()V

    #@a
    .line 104
    const/4 v1, 0x0

    #@b
    iput-object v1, p0, Landroid/webkit/LGWebNotifications;->mCore:Landroid/webkit/WebViewCore;

    #@d
    .line 105
    return-void

    #@e
    .line 99
    :catch_e
    move-exception v0

    #@f
    .line 101
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "NOTI"

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Error while unregistering notifications broadcast receiver "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    goto :goto_5
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 78
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@3
    move-result-object v1

    #@4
    .line 79
    .local v1, extras:Landroid/os/Bundle;
    if-nez v1, :cond_7

    #@6
    .line 94
    :goto_6
    return-void

    #@7
    .line 83
    :cond_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 85
    .local v0, action:Ljava/lang/String;
    iget-object v3, p0, Landroid/webkit/LGWebNotifications;->NOTI_ID:Ljava/lang/String;

    #@d
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@10
    move-result v2

    #@11
    .line 87
    .local v2, id:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    iget-object v4, p0, Landroid/webkit/LGWebNotifications;->ACTION_CLICK:Ljava/lang/String;

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2e

    #@2a
    .line 88
    invoke-direct {p0, v2}, Landroid/webkit/LGWebNotifications;->click(I)V

    #@2d
    goto :goto_6

    #@2e
    .line 89
    :cond_2e
    new-instance v3, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    iget-object v4, p0, Landroid/webkit/LGWebNotifications;->ACTION_CLOSE:Ljava/lang/String;

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v3

    #@45
    if-eqz v3, :cond_4b

    #@47
    .line 90
    invoke-direct {p0, v2}, Landroid/webkit/LGWebNotifications;->close(I)V

    #@4a
    goto :goto_6

    #@4b
    .line 92
    :cond_4b
    const-string v3, "NOTI"

    #@4d
    new-instance v4, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v5, "BroadcastReceiver action: "

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    goto :goto_6
.end method

.method requestPermission()V
    .registers 7

    #@0
    .prologue
    .line 247
    iget-object v2, p0, Landroid/webkit/LGWebNotifications;->mPermissionRequestDialog:Landroid/app/AlertDialog;

    #@2
    if-nez v2, :cond_5b

    #@4
    .line 248
    iget-object v2, p0, Landroid/webkit/LGWebNotifications;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@6
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getTitle()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 249
    .local v1, title:Ljava/lang/String;
    const/4 v0, 0x0

    #@b
    .line 250
    .local v0, message:Ljava/lang/String;
    if-nez v1, :cond_5c

    #@d
    .line 251
    iget-object v2, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@f
    const v3, 0x20902df

    #@12
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 256
    :goto_16
    new-instance v2, Landroid/app/AlertDialog$Builder;

    #@18
    iget-object v3, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@1a
    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@1d
    const v3, 0x202026e

    #@20
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@23
    move-result-object v2

    #@24
    const v3, 0x20902dd

    #@27
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@2e
    move-result-object v2

    #@2f
    const v3, 0x1040013

    #@32
    new-instance v4, Landroid/webkit/LGWebNotifications$3;

    #@34
    invoke-direct {v4, p0}, Landroid/webkit/LGWebNotifications$3;-><init>(Landroid/webkit/LGWebNotifications;)V

    #@37
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@3a
    move-result-object v2

    #@3b
    const v3, 0x1040009

    #@3e
    new-instance v4, Landroid/webkit/LGWebNotifications$2;

    #@40
    invoke-direct {v4, p0}, Landroid/webkit/LGWebNotifications$2;-><init>(Landroid/webkit/LGWebNotifications;)V

    #@43
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@46
    move-result-object v2

    #@47
    new-instance v3, Landroid/webkit/LGWebNotifications$1;

    #@49
    invoke-direct {v3, p0}, Landroid/webkit/LGWebNotifications$1;-><init>(Landroid/webkit/LGWebNotifications;)V

    #@4c
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@53
    move-result-object v2

    #@54
    iput-object v2, p0, Landroid/webkit/LGWebNotifications;->mPermissionRequestDialog:Landroid/app/AlertDialog;

    #@56
    .line 289
    iget-object v2, p0, Landroid/webkit/LGWebNotifications;->mPermissionRequestDialog:Landroid/app/AlertDialog;

    #@58
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    #@5b
    .line 291
    .end local v0           #message:Ljava/lang/String;
    .end local v1           #title:Ljava/lang/String;
    :cond_5b
    return-void

    #@5c
    .line 253
    .restart local v0       #message:Ljava/lang/String;
    .restart local v1       #title:Ljava/lang/String;
    :cond_5c
    iget-object v2, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@5e
    const v3, 0x20902de

    #@61
    const/4 v4, 0x1

    #@62
    new-array v4, v4, [Ljava/lang/Object;

    #@64
    const/4 v5, 0x0

    #@65
    aput-object v1, v4, v5

    #@67
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@6a
    move-result-object v0

    #@6b
    goto :goto_16
.end method

.method setPermissionValue(I)V
    .registers 11
    .parameter "value"

    #@0
    .prologue
    .line 228
    const-string v6, "NOTI"

    #@2
    const-string/jumbo v7, "setPermissionValue"

    #@5
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 230
    :try_start_8
    iget-object v6, p0, Landroid/webkit/LGWebNotifications;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@a
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getUrl()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 231
    .local v2, pageUrl:Ljava/lang/String;
    new-instance v4, Ljava/net/URL;

    #@10
    invoke-direct {v4, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@13
    .line 233
    .local v4, url:Ljava/net/URL;
    invoke-virtual {v4}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    .line 235
    .local v1, hostName:Ljava/lang/String;
    new-instance v5, Landroid/content/ContentValues;

    #@19
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    #@1c
    .line 236
    .local v5, values:Landroid/content/ContentValues;
    const-string/jumbo v6, "url"

    #@1f
    invoke-virtual {v5, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 237
    const-string/jumbo v6, "permission"

    #@25
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2c
    .line 238
    iget-object v6, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@2e
    iget-object v7, p0, Landroid/webkit/LGWebNotifications;->mContext:Landroid/content/Context;

    #@30
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@33
    move-result-object v7

    #@34
    iget-object v8, p0, Landroid/webkit/LGWebNotifications;->PERMISSION_PREF_URI:Landroid/net/Uri;

    #@36
    invoke-static {v6, v7, v8, v5}, Landroid/database/sqlite/SqliteWrapper;->insert(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_39
    .catch Ljava/net/MalformedURLException; {:try_start_8 .. :try_end_39} :catch_3a
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_39} :catch_55

    #@39
    .line 244
    .end local v1           #hostName:Ljava/lang/String;
    .end local v2           #pageUrl:Ljava/lang/String;
    .end local v4           #url:Ljava/net/URL;
    .end local v5           #values:Landroid/content/ContentValues;
    :goto_39
    return-void

    #@3a
    .line 239
    :catch_3a
    move-exception v0

    #@3b
    .line 240
    .local v0, e:Ljava/net/MalformedURLException;
    const-string v6, "NOTI"

    #@3d
    new-instance v7, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string/jumbo v8, "setPermissionValue bad url exception "

    #@45
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v7

    #@49
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v7

    #@4d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v7

    #@51
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    goto :goto_39

    #@55
    .line 241
    .end local v0           #e:Ljava/net/MalformedURLException;
    :catch_55
    move-exception v3

    #@56
    .line 242
    .local v3, t:Ljava/lang/Throwable;
    const-string v6, "NOTI"

    #@58
    new-instance v7, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string/jumbo v8, "setPermissionValue. something else is wrong "

    #@60
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v7

    #@64
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v7

    #@68
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v7

    #@6c
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    goto :goto_39
.end method

.method show(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "id"
    .parameter "title"
    .parameter "text"
    .parameter "iconUrl"

    #@0
    .prologue
    .line 143
    invoke-direct {p0, p1}, Landroid/webkit/LGWebNotifications;->setVisible(I)V

    #@3
    .line 145
    const/4 v1, 0x0

    #@4
    check-cast v1, Landroid/graphics/Bitmap;

    #@6
    invoke-direct {p0, p1, p2, p3, v1}, Landroid/webkit/LGWebNotifications;->show(ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    #@9
    .line 162
    new-instance v0, Landroid/webkit/LGWebNotifications$1Task;

    #@b
    invoke-direct {v0, p0}, Landroid/webkit/LGWebNotifications$1Task;-><init>(Landroid/webkit/LGWebNotifications;)V

    #@e
    .line 163
    .local v0, task:Landroid/webkit/LGWebNotifications$1Task;
    iput p1, v0, Landroid/webkit/LGWebNotifications$1Task;->id:I

    #@10
    .line 164
    iput-object p2, v0, Landroid/webkit/LGWebNotifications$1Task;->title:Ljava/lang/String;

    #@12
    .line 165
    iput-object p3, v0, Landroid/webkit/LGWebNotifications$1Task;->text:Ljava/lang/String;

    #@14
    .line 166
    iput-object p4, v0, Landroid/webkit/LGWebNotifications$1Task;->iconUrl:Ljava/lang/String;

    #@16
    .line 167
    const/4 v1, 0x0

    #@17
    new-array v1, v1, [Ljava/lang/Void;

    #@19
    invoke-virtual {v0, v1}, Landroid/webkit/LGWebNotifications$1Task;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    #@1c
    .line 168
    return-void
.end method
