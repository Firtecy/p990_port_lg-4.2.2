.class public abstract Landroid/webkit/BrowserDownloadListenerExt;
.super Ljava/lang/Object;
.source "BrowserDownloadListenerExt.java"

# interfaces
.implements Landroid/webkit/DownloadListenerExt;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 10
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .registers 17
    .parameter "url"
    .parameter "userAgent"
    .parameter "contentDisposition"
    .parameter "mimetype"
    .parameter "contentLength"
    .parameter "suggestedName"

    #@0
    .prologue
    .line 40
    const/4 v5, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move-object v4, p4

    #@7
    move-wide v6, p5

    #@8
    invoke-virtual/range {v0 .. v8}, Landroid/webkit/BrowserDownloadListenerExt;->onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    #@b
    .line 42
    return-void
.end method

.method public abstract onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
.end method
