.class final Landroid/webkit/DeviceMotionAndOrientationManager;
.super Ljava/lang/Object;
.source "DeviceMotionAndOrientationManager.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mWebViewCore:Landroid/webkit/WebViewCore;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 26
    const-class v0, Landroid/webkit/DeviceMotionAndOrientationManager;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/DeviceMotionAndOrientationManager;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>(Landroid/webkit/WebViewCore;)V
    .registers 2
    .parameter "webViewCore"

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    iput-object p1, p0, Landroid/webkit/DeviceMotionAndOrientationManager;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@5
    .line 31
    return-void
.end method

.method private static native nativeOnMotionChange(Landroid/webkit/WebViewCore;ZDZDZDD)V
.end method

.method private static native nativeOnOrientationChange(Landroid/webkit/WebViewCore;ZDZDZD)V
.end method

.method private static native nativeSetMockOrientation(Landroid/webkit/WebViewCore;ZDZDZD)V
.end method

.method private static native nativeSetUseMock(Landroid/webkit/WebViewCore;)V
.end method


# virtual methods
.method public onMotionChange(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;D)V
    .registers 18
    .parameter "x"
    .parameter "y"
    .parameter "z"
    .parameter "interval"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/webkit/DeviceMotionAndOrientationManager;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@2
    if-eqz p1, :cond_23

    #@4
    const/4 v1, 0x1

    #@5
    :goto_5
    if-eqz p1, :cond_25

    #@7
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    #@a
    move-result-wide v2

    #@b
    :goto_b
    if-eqz p2, :cond_28

    #@d
    const/4 v4, 0x1

    #@e
    :goto_e
    if-eqz p2, :cond_2a

    #@10
    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    #@13
    move-result-wide v5

    #@14
    :goto_14
    if-eqz p3, :cond_2d

    #@16
    const/4 v7, 0x1

    #@17
    :goto_17
    if-eqz p3, :cond_2f

    #@19
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    #@1c
    move-result-wide v8

    #@1d
    :goto_1d
    move-wide/from16 v10, p4

    #@1f
    invoke-static/range {v0 .. v11}, Landroid/webkit/DeviceMotionAndOrientationManager;->nativeOnMotionChange(Landroid/webkit/WebViewCore;ZDZDZDD)V

    #@22
    .line 59
    return-void

    #@23
    .line 54
    :cond_23
    const/4 v1, 0x0

    #@24
    goto :goto_5

    #@25
    :cond_25
    const-wide/16 v2, 0x0

    #@27
    goto :goto_b

    #@28
    :cond_28
    const/4 v4, 0x0

    #@29
    goto :goto_e

    #@2a
    :cond_2a
    const-wide/16 v5, 0x0

    #@2c
    goto :goto_14

    #@2d
    :cond_2d
    const/4 v7, 0x0

    #@2e
    goto :goto_17

    #@2f
    :cond_2f
    const-wide/16 v8, 0x0

    #@31
    goto :goto_1d
.end method

.method public onOrientationChange(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)V
    .registers 15
    .parameter "alpha"
    .parameter "beta"
    .parameter "gamma"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    const-wide/16 v8, 0x0

    #@4
    .line 61
    iget-object v0, p0, Landroid/webkit/DeviceMotionAndOrientationManager;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@6
    if-eqz p1, :cond_24

    #@8
    move v1, v7

    #@9
    :goto_9
    if-eqz p1, :cond_26

    #@b
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    #@e
    move-result-wide v2

    #@f
    :goto_f
    if-eqz p2, :cond_28

    #@11
    move v4, v7

    #@12
    :goto_12
    if-eqz p2, :cond_2a

    #@14
    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    #@17
    move-result-wide v5

    #@18
    :goto_18
    if-eqz p3, :cond_2c

    #@1a
    :goto_1a
    if-eqz p3, :cond_20

    #@1c
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    #@1f
    move-result-wide v8

    #@20
    :cond_20
    invoke-static/range {v0 .. v9}, Landroid/webkit/DeviceMotionAndOrientationManager;->nativeOnOrientationChange(Landroid/webkit/WebViewCore;ZDZDZD)V

    #@23
    .line 65
    return-void

    #@24
    :cond_24
    move v1, v10

    #@25
    .line 61
    goto :goto_9

    #@26
    :cond_26
    move-wide v2, v8

    #@27
    goto :goto_f

    #@28
    :cond_28
    move v4, v10

    #@29
    goto :goto_12

    #@2a
    :cond_2a
    move-wide v5, v8

    #@2b
    goto :goto_18

    #@2c
    :cond_2c
    move v7, v10

    #@2d
    goto :goto_1a
.end method

.method public setMockOrientation(ZDZDZD)V
    .registers 20
    .parameter "canProvideAlpha"
    .parameter "alpha"
    .parameter "canProvideBeta"
    .parameter "beta"
    .parameter "canProvideGamma"
    .parameter "gamma"

    #@0
    .prologue
    .line 47
    sget-boolean v0, Landroid/webkit/DeviceMotionAndOrientationManager;->$assertionsDisabled:Z

    #@2
    if-nez v0, :cond_1a

    #@4
    const-string v0, "WebViewCoreThread"

    #@6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1a

    #@14
    new-instance v0, Ljava/lang/AssertionError;

    #@16
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@19
    throw v0

    #@1a
    .line 48
    :cond_1a
    iget-object v0, p0, Landroid/webkit/DeviceMotionAndOrientationManager;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@1c
    move v1, p1

    #@1d
    move-wide v2, p2

    #@1e
    move v4, p4

    #@1f
    move-wide v5, p5

    #@20
    move/from16 v7, p7

    #@22
    move-wide/from16 v8, p8

    #@24
    invoke-static/range {v0 .. v9}, Landroid/webkit/DeviceMotionAndOrientationManager;->nativeSetMockOrientation(Landroid/webkit/WebViewCore;ZDZDZD)V

    #@27
    .line 50
    return-void
.end method

.method public setUseMock()V
    .registers 3

    #@0
    .prologue
    .line 38
    sget-boolean v0, Landroid/webkit/DeviceMotionAndOrientationManager;->$assertionsDisabled:Z

    #@2
    if-nez v0, :cond_1a

    #@4
    const-string v0, "WebViewCoreThread"

    #@6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1a

    #@14
    new-instance v0, Ljava/lang/AssertionError;

    #@16
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@19
    throw v0

    #@1a
    .line 39
    :cond_1a
    iget-object v0, p0, Landroid/webkit/DeviceMotionAndOrientationManager;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@1c
    invoke-static {v0}, Landroid/webkit/DeviceMotionAndOrientationManager;->nativeSetUseMock(Landroid/webkit/WebViewCore;)V

    #@1f
    .line 40
    return-void
.end method
