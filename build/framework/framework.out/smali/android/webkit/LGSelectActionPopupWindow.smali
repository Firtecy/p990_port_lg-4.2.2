.class public Landroid/webkit/LGSelectActionPopupWindow;
.super Ljava/lang/Object;
.source "LGSelectActionPopupWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final DEFAULT_FULL_THRESHOLD_BYTES:I = 0x1e00000

.field private static final GOOGLE_SEARCH_TEXT_MAX:I = 0xc8

.field private static final POPUP_TEXT_LAYOUT:I = 0x2030004

.field private static final QTRANSLATOR:Ljava/lang/String; = "com.cardcam.QTranslator"

.field private static final TEXTLINK:Ljava/lang/String; = "com.lge.smarttext"

.field private static final TRANSLATOR:Ljava/lang/String; = "com.lge.texttranslate"


# instance fields
.field private mClientContext:Landroid/content/Context;

.field private mCliptrayView:Landroid/widget/TextView;

.field private mContentView:Landroid/view/ViewGroup;

.field private mCopyView:Landroid/widget/TextView;

.field private mCutView:Landroid/widget/TextView;

.field private mEnableSelectAll:Z

.field private mFindView:Landroid/widget/TextView;

.field mIsEditable:Z

.field private mIsFloatingMode:Z

.field private mIsScrapEnable:Z

.field private mPasteView:Landroid/widget/TextView;

.field private mPopupWindow:Landroid/widget/PopupWindow;

.field mPositionX:I

.field mPositionY:I

.field private mScrapView:Landroid/widget/TextView;

.field private mSearchView:Landroid/widget/TextView;

.field private mSelectAllView:Landroid/widget/TextView;

.field private mShareView:Landroid/widget/TextView;

.field private mTextLink:Landroid/widget/TextView;

.field private mTracks:Landroid/view/ViewGroup;

.field private mTranslateView:Landroid/widget/TextView;

.field private mWebViewClassic:Landroid/webkit/WebViewClassic;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;)V
    .registers 11
    .parameter "c"
    .parameter "v"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v7, -0x2

    #@2
    const/4 v6, 0x1

    #@3
    .line 91
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 74
    iput-boolean v6, p0, Landroid/webkit/LGSelectActionPopupWindow;->mEnableSelectAll:Z

    #@8
    .line 75
    iput-boolean v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsEditable:Z

    #@a
    .line 76
    iput-boolean v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsFloatingMode:Z

    #@c
    .line 77
    iput-boolean v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsScrapEnable:Z

    #@e
    .line 92
    iput-object p2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@10
    .line 95
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@12
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@15
    move-result-object v0

    #@16
    .line 96
    .local v0, settings:Landroid/webkit/WebSettings;
    if-eqz v0, :cond_20

    #@18
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getFloatingMode()Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_20

    #@1e
    .line 97
    iput-boolean v6, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsFloatingMode:Z

    #@20
    .line 102
    :cond_20
    if-eqz v0, :cond_2a

    #@22
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getWebScrapEnabled()Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_2a

    #@28
    .line 103
    iput-boolean v6, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsScrapEnable:Z

    #@2a
    .line 107
    :cond_2a
    iput-object p1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@2c
    .line 108
    new-instance v2, Landroid/widget/PopupWindow;

    #@2e
    iget-object v3, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@30
    const/4 v4, 0x0

    #@31
    const v5, 0x10102c8

    #@34
    invoke-direct {v2, v3, v4, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@37
    iput-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@39
    .line 111
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@3b
    if-eqz v2, :cond_90

    #@3d
    .line 113
    iget-boolean v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsFloatingMode:Z

    #@3f
    if-eqz v2, :cond_48

    #@41
    .line 114
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@43
    const/16 v3, 0x7d2

    #@45
    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    #@48
    .line 118
    :cond_48
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@4a
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    #@4c
    invoke-direct {v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    #@4f
    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@52
    .line 119
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@54
    invoke-virtual {v2, v7}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@57
    .line 120
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@59
    invoke-virtual {v2, v7}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@5c
    .line 121
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@5e
    const v3, 0x1030002

    #@61
    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    #@64
    .line 122
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@66
    invoke-virtual {v2, v6}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    #@69
    .line 123
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@6b
    invoke-virtual {v2, v6}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    #@6e
    .line 125
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@70
    new-instance v3, Landroid/webkit/LGSelectActionPopupWindow$1;

    #@72
    invoke-direct {v3, p0}, Landroid/webkit/LGSelectActionPopupWindow$1;-><init>(Landroid/webkit/LGSelectActionPopupWindow;)V

    #@75
    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    #@78
    .line 135
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->initContentView()V

    #@7b
    .line 137
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    #@7d
    invoke-direct {v1, v7, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@80
    .line 140
    .local v1, wrapContent:Landroid/view/ViewGroup$LayoutParams;
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@82
    if-eqz v2, :cond_89

    #@84
    .line 141
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@86
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@89
    .line 142
    :cond_89
    iget-object v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@8b
    iget-object v3, p0, Landroid/webkit/LGSelectActionPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@8d
    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    #@90
    .line 144
    .end local v1           #wrapContent:Landroid/view/ViewGroup$LayoutParams;
    :cond_90
    return-void
.end method

.method private static isLowOnDisc()Z
    .registers 8

    #@0
    .prologue
    .line 514
    const-string v3, "/data"

    #@2
    .line 516
    .local v3, strDATA_PATH:Ljava/lang/String;
    new-instance v0, Landroid/os/StatFs;

    #@4
    const-string v4, "/data"

    #@6
    invoke-direct {v0, v4}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    #@9
    .line 517
    .local v0, mDataFileStats:Landroid/os/StatFs;
    const-string v4, "/data"

    #@b
    invoke-virtual {v0, v4}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    #@e
    .line 518
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    #@11
    move-result v4

    #@12
    int-to-long v4, v4

    #@13
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    #@16
    move-result v6

    #@17
    int-to-long v6, v6

    #@18
    mul-long v1, v4, v6

    #@1a
    .line 520
    .local v1, mFreeMem:J
    const-wide/32 v4, 0x1e00000

    #@1d
    cmp-long v4, v1, v4

    #@1f
    if-gez v4, :cond_23

    #@21
    .line 521
    const/4 v4, 0x1

    #@22
    .line 523
    :goto_22
    return v4

    #@23
    :cond_23
    const/4 v4, 0x0

    #@24
    goto :goto_22
.end method

.method private isPackageAvailable(Ljava/lang/String;)Z
    .registers 8
    .parameter "name"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 497
    :try_start_2
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@4
    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7
    move-result-object v2

    #@8
    .line 498
    .local v2, pm:Landroid/content/pm/PackageManager;
    invoke-virtual {v2, p1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_b} :catch_12

    #@b
    move-result v1

    #@c
    .line 499
    .local v1, mComponentState:I
    if-eq v1, v4, :cond_10

    #@e
    if-nez v1, :cond_11

    #@10
    :cond_10
    move v3, v4

    #@11
    .line 507
    .end local v1           #mComponentState:I
    .end local v2           #pm:Landroid/content/pm/PackageManager;
    :cond_11
    :goto_11
    return v3

    #@12
    .line 505
    :catch_12
    move-exception v0

    #@13
    .line 507
    .local v0, e:Ljava/lang/Exception;
    goto :goto_11
.end method

.method private measureContent()V
    .registers 6

    #@0
    .prologue
    const/high16 v4, -0x8000

    #@2
    .line 288
    iget-object v1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@b
    move-result-object v0

    #@c
    .line 289
    .local v0, displayMetrics:Landroid/util/DisplayMetrics;
    iget-object v1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@e
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@10
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@13
    move-result v2

    #@14
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@16
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@19
    move-result v3

    #@1a
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->measure(II)V

    #@1d
    .line 294
    return-void
.end method

.method private updateSelectAll()V
    .registers 4

    #@0
    .prologue
    .line 283
    iget-object v0, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSelectAllView:Landroid/widget/TextView;

    #@2
    iget-boolean v1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mEnableSelectAll:Z

    #@4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    #@7
    .line 284
    iget-object v1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSelectAllView:Landroid/widget/TextView;

    #@9
    iget-boolean v0, p0, Landroid/webkit/LGSelectActionPopupWindow;->mEnableSelectAll:Z

    #@b
    const/4 v2, 0x1

    #@c
    if-ne v0, v2, :cond_14

    #@e
    const/high16 v0, -0x100

    #@10
    :goto_10
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    #@13
    .line 285
    return-void

    #@14
    .line 284
    :cond_14
    const v0, -0x777778

    #@17
    goto :goto_10
.end method


# virtual methods
.method public enableSelectAll(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 278
    iput-boolean p1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mEnableSelectAll:Z

    #@2
    .line 279
    invoke-direct {p0}, Landroid/webkit/LGSelectActionPopupWindow;->updateSelectAll()V

    #@5
    .line 280
    return-void
.end method

.method public getEnableSelectAll()Z
    .registers 2

    #@0
    .prologue
    .line 274
    iget-boolean v0, p0, Landroid/webkit/LGSelectActionPopupWindow;->mEnableSelectAll:Z

    #@2
    return v0
.end method

.method protected getLocalPosition(Landroid/view/View;)Landroid/graphics/Point;
    .registers 15
    .parameter "v"

    #@0
    .prologue
    .line 297
    invoke-direct {p0}, Landroid/webkit/LGSelectActionPopupWindow;->measureContent()V

    #@3
    .line 299
    new-instance v2, Landroid/graphics/Point;

    #@5
    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    #@8
    .line 300
    .local v2, point:Landroid/graphics/Point;
    new-instance v4, Landroid/graphics/Rect;

    #@a
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@d
    .line 302
    .local v4, selection:Landroid/graphics/Rect;
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@f
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    #@12
    move-result v10

    #@13
    .line 303
    .local v10, width:I
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@15
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    #@18
    move-result v0

    #@19
    .line 305
    .local v0, height:I
    const/4 v11, 0x2

    #@1a
    new-array v1, v11, [I

    #@1c
    .line 306
    .local v1, location:[I
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    #@1f
    .line 307
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@21
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->getSelectionRegion()Landroid/graphics/Rect;

    #@24
    move-result-object v4

    #@25
    .line 308
    const/4 v11, 0x0

    #@26
    aget v11, v1, v11

    #@28
    const/4 v12, 0x1

    #@29
    aget v12, v1, v12

    #@2b
    invoke-virtual {v4, v11, v12}, Landroid/graphics/Rect;->offset(II)V

    #@2e
    .line 311
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@30
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@33
    move-result-object v5

    #@34
    .line 318
    .local v5, webView:Landroid/webkit/WebView;
    iget-boolean v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsFloatingMode:Z

    #@36
    if-eqz v11, :cond_66

    #@38
    .line 319
    new-instance v3, Landroid/graphics/Rect;

    #@3a
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@3d
    .line 320
    .local v3, r:Landroid/graphics/Rect;
    invoke-virtual {v5, v3}, Landroid/webkit/WebView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    #@40
    .line 322
    iget v11, v3, Landroid/graphics/Rect;->left:I

    #@42
    const/4 v12, 0x0

    #@43
    aget v12, v1, v12

    #@45
    add-int v7, v11, v12

    #@47
    .line 323
    .local v7, webviewLeft:I
    iget v11, v3, Landroid/graphics/Rect;->right:I

    #@49
    const/4 v12, 0x0

    #@4a
    aget v12, v1, v12

    #@4c
    add-int v8, v11, v12

    #@4e
    .line 324
    .local v8, webviewRight:I
    iget v11, v3, Landroid/graphics/Rect;->top:I

    #@50
    const/4 v12, 0x1

    #@51
    aget v12, v1, v12

    #@53
    add-int v9, v11, v12

    #@55
    .line 325
    .local v9, webviewTop:I
    iget v11, v3, Landroid/graphics/Rect;->bottom:I

    #@57
    const/4 v12, 0x1

    #@58
    aget v12, v1, v12

    #@5a
    add-int v6, v11, v12

    #@5c
    .line 338
    .local v6, webviewBottom:I
    :goto_5c
    iget v11, v4, Landroid/graphics/Rect;->right:I

    #@5e
    if-lt v11, v7, :cond_64

    #@60
    iget v11, v4, Landroid/graphics/Rect;->left:I

    #@62
    if-le v11, v8, :cond_77

    #@64
    .line 339
    :cond_64
    const/4 v2, 0x0

    #@65
    .line 357
    .end local v0           #height:I
    .end local v2           #point:Landroid/graphics/Point;
    :goto_65
    return-object v2

    #@66
    .line 327
    .end local v3           #r:Landroid/graphics/Rect;
    .end local v6           #webviewBottom:I
    .end local v7           #webviewLeft:I
    .end local v8           #webviewRight:I
    .end local v9           #webviewTop:I
    .restart local v0       #height:I
    .restart local v2       #point:Landroid/graphics/Point;
    :cond_66
    new-instance v3, Landroid/graphics/Rect;

    #@68
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@6b
    .line 328
    .restart local v3       #r:Landroid/graphics/Rect;
    invoke-virtual {v5, v3}, Landroid/webkit/WebView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    #@6e
    .line 330
    iget v7, v3, Landroid/graphics/Rect;->left:I

    #@70
    .line 331
    .restart local v7       #webviewLeft:I
    iget v8, v3, Landroid/graphics/Rect;->right:I

    #@72
    .line 332
    .restart local v8       #webviewRight:I
    iget v9, v3, Landroid/graphics/Rect;->top:I

    #@74
    .line 333
    .restart local v9       #webviewTop:I
    iget v6, v3, Landroid/graphics/Rect;->bottom:I

    #@76
    .restart local v6       #webviewBottom:I
    goto :goto_5c

    #@77
    .line 341
    :cond_77
    const/4 v11, 0x0

    #@78
    aget v11, v1, v11

    #@7a
    sub-int v12, v8, v7

    #@7c
    div-int/lit8 v12, v12, 0x2

    #@7e
    add-int/2addr v11, v12

    #@7f
    div-int/lit8 v12, v10, 0x2

    #@81
    sub-int/2addr v11, v12

    #@82
    iput v11, v2, Landroid/graphics/Point;->x:I

    #@84
    .line 345
    iget v11, v4, Landroid/graphics/Rect;->bottom:I

    #@86
    if-lt v11, v9, :cond_8c

    #@88
    iget v11, v4, Landroid/graphics/Rect;->top:I

    #@8a
    if-le v11, v6, :cond_8e

    #@8c
    .line 346
    :cond_8c
    const/4 v2, 0x0

    #@8d
    goto :goto_65

    #@8e
    .line 348
    :cond_8e
    iget v11, v4, Landroid/graphics/Rect;->top:I

    #@90
    sub-int/2addr v11, v9

    #@91
    if-ge v0, v11, :cond_9f

    #@93
    .line 349
    iget v11, v4, Landroid/graphics/Rect;->top:I

    #@95
    iget-boolean v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsFloatingMode:Z

    #@97
    if-eqz v12, :cond_9d

    #@99
    .end local v0           #height:I
    :goto_99
    sub-int/2addr v11, v0

    #@9a
    iput v11, v2, Landroid/graphics/Point;->y:I

    #@9c
    goto :goto_65

    #@9d
    .restart local v0       #height:I
    :cond_9d
    const/4 v0, 0x0

    #@9e
    goto :goto_99

    #@9f
    .line 350
    :cond_9f
    iget v11, v4, Landroid/graphics/Rect;->bottom:I

    #@a1
    sub-int v11, v6, v11

    #@a3
    if-ge v0, v11, :cond_b1

    #@a5
    .line 351
    iget v11, v4, Landroid/graphics/Rect;->bottom:I

    #@a7
    iget-boolean v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsFloatingMode:Z

    #@a9
    if-eqz v12, :cond_af

    #@ab
    .end local v0           #height:I
    :goto_ab
    sub-int/2addr v11, v0

    #@ac
    iput v11, v2, Landroid/graphics/Point;->y:I

    #@ae
    goto :goto_65

    #@af
    .restart local v0       #height:I
    :cond_af
    const/4 v0, 0x0

    #@b0
    goto :goto_ab

    #@b1
    .line 353
    :cond_b1
    sub-int v11, v6, v9

    #@b3
    div-int/lit8 v11, v11, 0x2

    #@b5
    add-int/2addr v11, v9

    #@b6
    div-int/lit8 v12, v0, 0x2

    #@b8
    sub-int/2addr v11, v12

    #@b9
    iget-boolean v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsFloatingMode:Z

    #@bb
    if-eqz v12, :cond_c1

    #@bd
    .end local v0           #height:I
    :goto_bd
    sub-int/2addr v11, v0

    #@be
    iput v11, v2, Landroid/graphics/Point;->y:I

    #@c0
    goto :goto_65

    #@c1
    .restart local v0       #height:I
    :cond_c1
    const/4 v0, 0x0

    #@c2
    goto :goto_bd
.end method

.method public hide()V
    .registers 2

    #@0
    .prologue
    .line 487
    iget-object v0, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@5
    .line 488
    return-void
.end method

.method protected initContentView()V
    .registers 14

    #@0
    .prologue
    .line 147
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@2
    const-string/jumbo v12, "layout_inflater"

    #@5
    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v8

    #@9
    check-cast v8, Landroid/view/LayoutInflater;

    #@b
    .line 150
    .local v8, inflater:Landroid/view/LayoutInflater;
    const v11, 0x2030003

    #@e
    const/4 v12, 0x0

    #@f
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@12
    move-result-object v11

    #@13
    check-cast v11, Landroid/view/ViewGroup;

    #@15
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@17
    .line 152
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@19
    const v12, 0x20d0041

    #@1c
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@1f
    move-result-object v11

    #@20
    check-cast v11, Landroid/view/ViewGroup;

    #@22
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@24
    .line 154
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mContentView:Landroid/view/ViewGroup;

    #@26
    const v12, 0x10805d0

    #@29
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    #@2c
    .line 157
    new-instance v10, Landroid/view/ViewGroup$LayoutParams;

    #@2e
    const/4 v11, -0x2

    #@2f
    const/4 v12, -0x2

    #@30
    invoke-direct {v10, v11, v12}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@33
    .line 160
    .local v10, wrapContent:Landroid/view/ViewGroup$LayoutParams;
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@35
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@37
    const-string v12, "clipboard"

    #@39
    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3c
    move-result-object v11

    #@3d
    check-cast v11, Landroid/content/ClipboardManager;

    #@3f
    move-object v7, v11

    #@40
    check-cast v7, Landroid/content/ClipboardManager;

    #@42
    .line 164
    .local v7, cm:Landroid/content/ClipboardManager;
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@44
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->isSelectingInEditText()Z

    #@47
    move-result v11

    #@48
    iput-boolean v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsEditable:Z

    #@4a
    .line 165
    iget-boolean v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsEditable:Z

    #@4c
    if-eqz v11, :cond_253

    #@4e
    invoke-virtual {v7}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    #@51
    move-result v11

    #@52
    if-eqz v11, :cond_253

    #@54
    const/4 v3, 0x1

    #@55
    .line 166
    .local v3, canPaste:Z
    :goto_55
    iget-boolean v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsEditable:Z

    #@57
    if-nez v11, :cond_256

    #@59
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@5b
    invoke-virtual {v11}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@5e
    move-result-object v11

    #@5f
    const-string v12, "com.android.browser"

    #@61
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v11

    #@65
    if-eqz v11, :cond_256

    #@67
    const/4 v2, 0x1

    #@68
    .line 167
    .local v2, canFind:Z
    :goto_68
    iget-boolean v1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsEditable:Z

    #@6a
    .line 169
    .local v1, canCut:Z
    iget-boolean v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsScrapEnable:Z

    #@6c
    if-eqz v11, :cond_259

    #@6e
    iget-boolean v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsEditable:Z

    #@70
    if-nez v11, :cond_259

    #@72
    const/4 v4, 0x1

    #@73
    .line 170
    .local v4, canScrap:Z
    :goto_73
    const-string v11, "com.lge.smarttext"

    #@75
    invoke-direct {p0, v11}, Landroid/webkit/LGSelectActionPopupWindow;->isPackageAvailable(Ljava/lang/String;)Z

    #@78
    move-result v11

    #@79
    if-eqz v11, :cond_25c

    #@7b
    iget-boolean v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsFloatingMode:Z

    #@7d
    if-nez v11, :cond_25c

    #@7f
    const/4 v5, 0x1

    #@80
    .line 171
    .local v5, canTextLink:Z
    :goto_80
    const-string v11, "com.lge.texttranslate"

    #@82
    invoke-direct {p0, v11}, Landroid/webkit/LGSelectActionPopupWindow;->isPackageAvailable(Ljava/lang/String;)Z

    #@85
    move-result v11

    #@86
    if-eqz v11, :cond_25f

    #@88
    const-string v11, "com.cardcam.QTranslator"

    #@8a
    invoke-direct {p0, v11}, Landroid/webkit/LGSelectActionPopupWindow;->isPackageAvailable(Ljava/lang/String;)Z

    #@8d
    move-result v11

    #@8e
    if-eqz v11, :cond_25f

    #@90
    iget-boolean v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsEditable:Z

    #@92
    if-nez v11, :cond_25f

    #@94
    const/4 v6, 0x1

    #@95
    .line 174
    .local v6, canTranslate:Z
    :goto_95
    iget-boolean v0, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsEditable:Z

    #@97
    .line 177
    .local v0, canClip:Z
    const v11, 0x2030004

    #@9a
    const/4 v12, 0x0

    #@9b
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@9e
    move-result-object v11

    #@9f
    check-cast v11, Landroid/widget/TextView;

    #@a1
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSelectAllView:Landroid/widget/TextView;

    #@a3
    .line 178
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSelectAllView:Landroid/widget/TextView;

    #@a5
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@a8
    .line 179
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@aa
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSelectAllView:Landroid/widget/TextView;

    #@ac
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@af
    .line 180
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSelectAllView:Landroid/widget/TextView;

    #@b1
    const v12, 0x104000d

    #@b4
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@b7
    .line 181
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSelectAllView:Landroid/widget/TextView;

    #@b9
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@bc
    .line 183
    const v11, 0x2030004

    #@bf
    const/4 v12, 0x0

    #@c0
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@c3
    move-result-object v11

    #@c4
    check-cast v11, Landroid/widget/TextView;

    #@c6
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCopyView:Landroid/widget/TextView;

    #@c8
    .line 184
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCopyView:Landroid/widget/TextView;

    #@ca
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@cd
    .line 185
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@cf
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCopyView:Landroid/widget/TextView;

    #@d1
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@d4
    .line 186
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCopyView:Landroid/widget/TextView;

    #@d6
    const v12, 0x1040001

    #@d9
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@dc
    .line 187
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCopyView:Landroid/widget/TextView;

    #@de
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@e1
    .line 190
    if-eqz v4, :cond_10c

    #@e3
    iget-boolean v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsFloatingMode:Z

    #@e5
    if-nez v11, :cond_10c

    #@e7
    .line 191
    const v11, 0x2030004

    #@ea
    const/4 v12, 0x0

    #@eb
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@ee
    move-result-object v11

    #@ef
    check-cast v11, Landroid/widget/TextView;

    #@f1
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mScrapView:Landroid/widget/TextView;

    #@f3
    .line 192
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mScrapView:Landroid/widget/TextView;

    #@f5
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@f8
    .line 193
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@fa
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mScrapView:Landroid/widget/TextView;

    #@fc
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@ff
    .line 194
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mScrapView:Landroid/widget/TextView;

    #@101
    const v12, 0x20902e2

    #@104
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@107
    .line 195
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mScrapView:Landroid/widget/TextView;

    #@109
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@10c
    .line 200
    :cond_10c
    if-eqz v5, :cond_133

    #@10e
    .line 201
    const v11, 0x2030004

    #@111
    const/4 v12, 0x0

    #@112
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@115
    move-result-object v11

    #@116
    check-cast v11, Landroid/widget/TextView;

    #@118
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTextLink:Landroid/widget/TextView;

    #@11a
    .line 202
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTextLink:Landroid/widget/TextView;

    #@11c
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@11f
    .line 203
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@121
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTextLink:Landroid/widget/TextView;

    #@123
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@126
    .line 204
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTextLink:Landroid/widget/TextView;

    #@128
    const v12, 0x20902e1

    #@12b
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@12e
    .line 205
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTextLink:Landroid/widget/TextView;

    #@130
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@133
    .line 209
    :cond_133
    if-eqz v1, :cond_15a

    #@135
    .line 210
    const v11, 0x2030004

    #@138
    const/4 v12, 0x0

    #@139
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@13c
    move-result-object v11

    #@13d
    check-cast v11, Landroid/widget/TextView;

    #@13f
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCutView:Landroid/widget/TextView;

    #@141
    .line 211
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCutView:Landroid/widget/TextView;

    #@143
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@146
    .line 212
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@148
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCutView:Landroid/widget/TextView;

    #@14a
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@14d
    .line 213
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCutView:Landroid/widget/TextView;

    #@14f
    const v12, 0x1040003

    #@152
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@155
    .line 214
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCutView:Landroid/widget/TextView;

    #@157
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@15a
    .line 217
    :cond_15a
    if-eqz v3, :cond_181

    #@15c
    .line 218
    const v11, 0x2030004

    #@15f
    const/4 v12, 0x0

    #@160
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@163
    move-result-object v11

    #@164
    check-cast v11, Landroid/widget/TextView;

    #@166
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPasteView:Landroid/widget/TextView;

    #@168
    .line 219
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPasteView:Landroid/widget/TextView;

    #@16a
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@16d
    .line 220
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@16f
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPasteView:Landroid/widget/TextView;

    #@171
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@174
    .line 221
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPasteView:Landroid/widget/TextView;

    #@176
    const v12, 0x104000b

    #@179
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@17c
    .line 222
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPasteView:Landroid/widget/TextView;

    #@17e
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@181
    .line 226
    :cond_181
    iget-boolean v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsFloatingMode:Z

    #@183
    if-nez v11, :cond_21d

    #@185
    .line 227
    const v11, 0x2030004

    #@188
    const/4 v12, 0x0

    #@189
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@18c
    move-result-object v11

    #@18d
    check-cast v11, Landroid/widget/TextView;

    #@18f
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mShareView:Landroid/widget/TextView;

    #@191
    .line 228
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mShareView:Landroid/widget/TextView;

    #@193
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@196
    .line 229
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@198
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mShareView:Landroid/widget/TextView;

    #@19a
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@19d
    .line 230
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mShareView:Landroid/widget/TextView;

    #@19f
    const v12, 0x10404d1

    #@1a2
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@1a5
    .line 231
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mShareView:Landroid/widget/TextView;

    #@1a7
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@1aa
    .line 233
    if-eqz v2, :cond_1d1

    #@1ac
    .line 234
    const v11, 0x2030004

    #@1af
    const/4 v12, 0x0

    #@1b0
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@1b3
    move-result-object v11

    #@1b4
    check-cast v11, Landroid/widget/TextView;

    #@1b6
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mFindView:Landroid/widget/TextView;

    #@1b8
    .line 235
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mFindView:Landroid/widget/TextView;

    #@1ba
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@1bd
    .line 236
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@1bf
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mFindView:Landroid/widget/TextView;

    #@1c1
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@1c4
    .line 237
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mFindView:Landroid/widget/TextView;

    #@1c6
    const v12, 0x10404d2

    #@1c9
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@1cc
    .line 238
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mFindView:Landroid/widget/TextView;

    #@1ce
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@1d1
    .line 241
    :cond_1d1
    const v11, 0x2030004

    #@1d4
    const/4 v12, 0x0

    #@1d5
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@1d8
    move-result-object v11

    #@1d9
    check-cast v11, Landroid/widget/TextView;

    #@1db
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSearchView:Landroid/widget/TextView;

    #@1dd
    .line 242
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSearchView:Landroid/widget/TextView;

    #@1df
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@1e2
    .line 243
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@1e4
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSearchView:Landroid/widget/TextView;

    #@1e6
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@1e9
    .line 244
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSearchView:Landroid/widget/TextView;

    #@1eb
    const v12, 0x10404d3

    #@1ee
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@1f1
    .line 245
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSearchView:Landroid/widget/TextView;

    #@1f3
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@1f6
    .line 248
    if-eqz v6, :cond_21d

    #@1f8
    .line 249
    const v11, 0x2030004

    #@1fb
    const/4 v12, 0x0

    #@1fc
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@1ff
    move-result-object v11

    #@200
    check-cast v11, Landroid/widget/TextView;

    #@202
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTranslateView:Landroid/widget/TextView;

    #@204
    .line 250
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTranslateView:Landroid/widget/TextView;

    #@206
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@209
    .line 251
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@20b
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTranslateView:Landroid/widget/TextView;

    #@20d
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@210
    .line 252
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTranslateView:Landroid/widget/TextView;

    #@212
    const v12, 0x20902e0

    #@215
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@218
    .line 253
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTranslateView:Landroid/widget/TextView;

    #@21a
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@21d
    .line 260
    :cond_21d
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@21f
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@222
    move-result-object v9

    #@223
    .line 261
    .local v9, settings:Landroid/webkit/WebSettings;
    if-eqz v9, :cond_252

    #@225
    invoke-virtual {v9}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@228
    move-result v11

    #@229
    if-eqz v11, :cond_252

    #@22b
    .line 262
    if-eqz v0, :cond_252

    #@22d
    .line 263
    const v11, 0x2030004

    #@230
    const/4 v12, 0x0

    #@231
    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@234
    move-result-object v11

    #@235
    check-cast v11, Landroid/widget/TextView;

    #@237
    iput-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@239
    .line 264
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@23b
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@23e
    .line 265
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTracks:Landroid/view/ViewGroup;

    #@240
    iget-object v12, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@242
    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@245
    .line 266
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@247
    const v12, 0x20902a0

    #@24a
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    #@24d
    .line 267
    iget-object v11, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@24f
    invoke-virtual {v11, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@252
    .line 271
    :cond_252
    return-void

    #@253
    .line 165
    .end local v0           #canClip:Z
    .end local v1           #canCut:Z
    .end local v2           #canFind:Z
    .end local v3           #canPaste:Z
    .end local v4           #canScrap:Z
    .end local v5           #canTextLink:Z
    .end local v6           #canTranslate:Z
    .end local v9           #settings:Landroid/webkit/WebSettings;
    :cond_253
    const/4 v3, 0x0

    #@254
    goto/16 :goto_55

    #@256
    .line 166
    .restart local v3       #canPaste:Z
    :cond_256
    const/4 v2, 0x0

    #@257
    goto/16 :goto_68

    #@259
    .line 169
    .restart local v1       #canCut:Z
    .restart local v2       #canFind:Z
    :cond_259
    const/4 v4, 0x0

    #@25a
    goto/16 :goto_73

    #@25c
    .line 170
    .restart local v4       #canScrap:Z
    :cond_25c
    const/4 v5, 0x0

    #@25d
    goto/16 :goto_80

    #@25f
    .line 171
    .restart local v5       #canTextLink:Z
    :cond_25f
    const/4 v6, 0x0

    #@260
    goto/16 :goto_95
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 491
    iget-object v0, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 12
    .parameter "v"
    .parameter "event"

    #@0
    .prologue
    const/16 v6, 0xc8

    #@2
    const/4 v8, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    .line 362
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    #@7
    move-result v5

    #@8
    packed-switch v5, :pswitch_data_154

    #@b
    .line 464
    :goto_b
    :pswitch_b
    invoke-direct {p0}, Landroid/webkit/LGSelectActionPopupWindow;->updateSelectAll()V

    #@e
    .line 466
    return v8

    #@f
    .line 364
    :pswitch_f
    const v5, -0x3c2d29

    #@12
    invoke-virtual {p1, v5}, Landroid/view/View;->setBackgroundColor(I)V

    #@15
    goto :goto_b

    #@16
    .line 368
    :pswitch_16
    invoke-virtual {p1, v7}, Landroid/view/View;->playSoundEffect(I)V

    #@19
    .line 370
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSelectAllView:Landroid/widget/TextView;

    #@1b
    if-ne p1, v5, :cond_2b

    #@1d
    .line 371
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@1f
    iget-boolean v6, p0, Landroid/webkit/LGSelectActionPopupWindow;->mIsEditable:Z

    #@21
    invoke-virtual {v5, v6}, Landroid/webkit/WebViewClassic;->selectAll(Z)V

    #@24
    .line 372
    invoke-virtual {p0, v7}, Landroid/webkit/LGSelectActionPopupWindow;->enableSelectAll(Z)V

    #@27
    .line 455
    :cond_27
    :goto_27
    invoke-virtual {p1, v7}, Landroid/view/View;->setBackgroundColor(I)V

    #@2a
    goto :goto_b

    #@2b
    .line 373
    :cond_2b
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCopyView:Landroid/widget/TextView;

    #@2d
    if-ne p1, v5, :cond_3d

    #@2f
    .line 374
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@31
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->copySelection()Z

    #@34
    .line 375
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@36
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@39
    .line 376
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@3c
    goto :goto_27

    #@3d
    .line 378
    :cond_3d
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mScrapView:Landroid/widget/TextView;

    #@3f
    if-ne p1, v5, :cond_6b

    #@41
    .line 379
    new-instance v2, Landroid/webkit/LGScrapManager;

    #@43
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@45
    iget-object v6, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@47
    invoke-direct {v2, v5, v6}, Landroid/webkit/LGScrapManager;-><init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;)V

    #@4a
    .line 380
    .local v2, scrap:Landroid/webkit/LGScrapManager;
    invoke-static {}, Landroid/webkit/LGSelectActionPopupWindow;->isLowOnDisc()Z

    #@4d
    move-result v5

    #@4e
    if-nez v5, :cond_5e

    #@50
    .line 381
    if-eqz v2, :cond_55

    #@52
    .line 382
    invoke-virtual {v2}, Landroid/webkit/LGScrapManager;->scrapSelection()V

    #@55
    .line 388
    :cond_55
    :goto_55
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@57
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@5a
    .line 389
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@5d
    goto :goto_27

    #@5e
    .line 385
    :cond_5e
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@60
    const v6, 0x2090262

    #@63
    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    #@6a
    goto :goto_55

    #@6b
    .line 392
    .end local v2           #scrap:Landroid/webkit/LGScrapManager;
    :cond_6b
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTextLink:Landroid/widget/TextView;

    #@6d
    if-ne p1, v5, :cond_7d

    #@6f
    .line 393
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@71
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->textLinkSelection()V

    #@74
    .line 394
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@76
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@79
    .line 395
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@7c
    goto :goto_27

    #@7d
    .line 397
    :cond_7d
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mShareView:Landroid/widget/TextView;

    #@7f
    if-ne p1, v5, :cond_a1

    #@81
    .line 398
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@83
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@86
    move-result-object v3

    #@87
    .line 399
    .local v3, text:Ljava/lang/String;
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@89
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@8c
    .line 401
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@8e
    instance-of v5, v5, Landroid/app/Activity;

    #@90
    if-eqz v5, :cond_9b

    #@92
    .line 402
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@94
    invoke-static {v5, v3}, Landroid/provider/Browser;->sendStringEx(Landroid/content/Context;Ljava/lang/String;)V

    #@97
    .line 406
    :goto_97
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@9a
    goto :goto_27

    #@9b
    .line 405
    :cond_9b
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@9d
    invoke-static {v5, v3}, Landroid/provider/Browser;->sendString(Landroid/content/Context;Ljava/lang/String;)V

    #@a0
    goto :goto_97

    #@a1
    .line 407
    .end local v3           #text:Ljava/lang/String;
    :cond_a1
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mFindView:Landroid/widget/TextView;

    #@a3
    if-ne p1, v5, :cond_ba

    #@a5
    .line 408
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@a7
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@aa
    move-result-object v3

    #@ab
    .line 409
    .restart local v3       #text:Ljava/lang/String;
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@ad
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@b0
    .line 410
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@b2
    invoke-virtual {v5, v3, v7}, Landroid/webkit/WebViewClassic;->showFindDialog(Ljava/lang/String;Z)Z

    #@b5
    .line 411
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@b8
    goto/16 :goto_27

    #@ba
    .line 412
    .end local v3           #text:Ljava/lang/String;
    :cond_ba
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mSearchView:Landroid/widget/TextView;

    #@bc
    if-ne p1, v5, :cond_fd

    #@be
    .line 413
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@c0
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@c3
    move-result-object v3

    #@c4
    .line 414
    .restart local v3       #text:Ljava/lang/String;
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@c6
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@c9
    .line 415
    if-eqz v3, :cond_f8

    #@cb
    .line 416
    new-instance v1, Landroid/content/Intent;

    #@cd
    const-string v5, "android.intent.action.WEB_SEARCH"

    #@cf
    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d2
    .line 417
    .local v1, i:Landroid/content/Intent;
    const-string/jumbo v5, "new_search"

    #@d5
    invoke-virtual {v1, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@d8
    .line 418
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@db
    move-result v5

    #@dc
    if-le v5, v6, :cond_e2

    #@de
    .line 419
    invoke-virtual {v3, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@e1
    move-result-object v3

    #@e2
    .line 420
    :cond_e2
    const-string/jumbo v5, "query"

    #@e5
    invoke-virtual {v1, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@e8
    .line 422
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@ea
    instance-of v5, v5, Landroid/app/Activity;

    #@ec
    if-nez v5, :cond_f3

    #@ee
    .line 423
    const/high16 v5, 0x1000

    #@f0
    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@f3
    .line 426
    :cond_f3
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@f5
    invoke-virtual {v5, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@f8
    .line 428
    .end local v1           #i:Landroid/content/Intent;
    :cond_f8
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@fb
    goto/16 :goto_27

    #@fd
    .line 429
    .end local v3           #text:Ljava/lang/String;
    :cond_fd
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCutView:Landroid/widget/TextView;

    #@ff
    if-ne p1, v5, :cond_110

    #@101
    .line 430
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@103
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->cutSelection()V

    #@106
    .line 431
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@108
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@10b
    .line 432
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@10e
    goto/16 :goto_27

    #@110
    .line 433
    :cond_110
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPasteView:Landroid/widget/TextView;

    #@112
    if-ne p1, v5, :cond_123

    #@114
    .line 434
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@116
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->pasteFromClipboard()V

    #@119
    .line 435
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@11c
    .line 436
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@11e
    invoke-virtual {v5}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@121
    goto/16 :goto_27

    #@123
    .line 438
    :cond_123
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mTranslateView:Landroid/widget/TextView;

    #@125
    if-ne p1, v5, :cond_139

    #@127
    .line 439
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mClientContext:Landroid/content/Context;

    #@129
    iget-object v6, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@12b
    invoke-static {v5, v6}, Landroid/webkit/LGTranslateReceiver;->getInstance(Landroid/content/Context;Landroid/webkit/WebViewClassic;)Landroid/webkit/LGTranslateReceiver;

    #@12e
    move-result-object v4

    #@12f
    .line 440
    .local v4, translate:Landroid/webkit/LGTranslateReceiver;
    if-eqz v4, :cond_134

    #@131
    .line 441
    invoke-virtual {v4}, Landroid/webkit/LGTranslateReceiver;->translateSelection()V

    #@134
    .line 443
    :cond_134
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@137
    goto/16 :goto_27

    #@139
    .line 447
    .end local v4           #translate:Landroid/webkit/LGTranslateReceiver;
    :cond_139
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mCliptrayView:Landroid/widget/TextView;

    #@13b
    if-ne p1, v5, :cond_27

    #@13d
    .line 448
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@140
    .line 449
    invoke-static {}, Landroid/webkit/LGCliptrayManager;->getInstance()Landroid/webkit/LGCliptrayManager;

    #@143
    move-result-object v0

    #@144
    .line 450
    .local v0, cliptray:Landroid/webkit/LGCliptrayManager;
    if-eqz v0, :cond_27

    #@146
    .line 451
    iget-object v5, p0, Landroid/webkit/LGSelectActionPopupWindow;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@148
    iget-object v5, v5, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@14a
    invoke-virtual {v0, v5}, Landroid/webkit/LGCliptrayManager;->showCliptray(Landroid/webkit/WebViewClassic$WebViewInputConnection;)V

    #@14d
    goto/16 :goto_27

    #@14f
    .line 458
    .end local v0           #cliptray:Landroid/webkit/LGCliptrayManager;
    :pswitch_14f
    invoke-virtual {p1, v7}, Landroid/view/View;->setBackgroundColor(I)V

    #@152
    goto/16 :goto_b

    #@154
    .line 362
    :pswitch_data_154
    .packed-switch 0x0
        :pswitch_f
        :pswitch_16
        :pswitch_b
        :pswitch_14f
    .end packed-switch
.end method

.method public show(Landroid/view/View;)V
    .registers 7
    .parameter "v"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 470
    invoke-virtual {p0, p1}, Landroid/webkit/LGSelectActionPopupWindow;->getLocalPosition(Landroid/view/View;)Landroid/graphics/Point;

    #@4
    move-result-object v0

    #@5
    .line 471
    .local v0, p:Landroid/graphics/Point;
    if-nez v0, :cond_b

    #@7
    .line 472
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->hide()V

    #@a
    .line 484
    :goto_a
    return-void

    #@b
    .line 475
    :cond_b
    iget v1, v0, Landroid/graphics/Point;->x:I

    #@d
    iput v1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPositionX:I

    #@f
    .line 476
    iget v1, v0, Landroid/graphics/Point;->y:I

    #@11
    iput v1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPositionY:I

    #@13
    .line 478
    invoke-virtual {p0}, Landroid/webkit/LGSelectActionPopupWindow;->isShowing()Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_23

    #@19
    .line 479
    iget-object v1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@1b
    iget v2, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPositionX:I

    #@1d
    iget v3, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPositionY:I

    #@1f
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/widget/PopupWindow;->update(IIII)V

    #@22
    goto :goto_a

    #@23
    .line 481
    :cond_23
    iget-object v1, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPopupWindow:Landroid/widget/PopupWindow;

    #@25
    const/4 v2, 0x0

    #@26
    iget v3, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPositionX:I

    #@28
    iget v4, p0, Landroid/webkit/LGSelectActionPopupWindow;->mPositionY:I

    #@2a
    invoke-virtual {v1, p1, v2, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    #@2d
    goto :goto_a
.end method
