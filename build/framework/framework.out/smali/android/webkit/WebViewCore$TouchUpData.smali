.class Landroid/webkit/WebViewCore$TouchUpData;
.super Ljava/lang/Object;
.source "WebViewCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewCore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TouchUpData"
.end annotation


# instance fields
.field mFrame:I

.field mMoveGeneration:I

.field mNativeLayer:I

.field mNativeLayerRect:Landroid/graphics/Rect;

.field mNode:I

.field mX:I

.field mY:I


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 930
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 937
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/WebViewCore$TouchUpData;->mNativeLayerRect:Landroid/graphics/Rect;

    #@a
    return-void
.end method
