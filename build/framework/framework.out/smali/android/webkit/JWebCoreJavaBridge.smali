.class final Landroid/webkit/JWebCoreJavaBridge;
.super Landroid/os/Handler;
.source "JWebCoreJavaBridge.java"


# static fields
.field private static final FUNCPTR_MESSAGE:I = 0x2

.field private static final LOGTAG:Ljava/lang/String; = "webkit-timers"

.field static final REFRESH_PLUGINS:I = 0x64

.field private static final TIMER_MESSAGE:I = 0x1

.field private static sCurrentMainWebView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/webkit/WebViewClassic;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContentUriToFilePathMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHasDeferredTimers:Z

.field private mHasInstantTimer:Z

.field private mNativeBridge:I

.field private mTimerPaused:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 48
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@6
    sput-object v0, Landroid/webkit/JWebCoreJavaBridge;->sCurrentMainWebView:Ljava/lang/ref/WeakReference;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 61
    invoke-direct {p0}, Landroid/webkit/JWebCoreJavaBridge;->nativeConstructor()V

    #@6
    .line 63
    return-void
.end method

.method private cookies(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 195
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private cookiesEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 202
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->acceptCookie()Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method private fireSharedTimer()V
    .registers 2

    #@0
    .prologue
    .line 91
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mHasInstantTimer:Z

    #@3
    .line 92
    invoke-direct {p0}, Landroid/webkit/JWebCoreJavaBridge;->sharedTimerFired()V

    #@6
    .line 93
    return-void
.end method

.method private getKeyStrengthList()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 257
    invoke-static {}, Landroid/webkit/CertTool;->getKeyStrengthList()[Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private getPluginDirectories()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 209
    const/4 v0, 0x0

    #@1
    invoke-static {v0}, Landroid/webkit/PluginManager;->getInstance(Landroid/content/Context;)Landroid/webkit/PluginManager;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {v0}, Landroid/webkit/PluginManager;->getPluginDirectories()[Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method private getPluginSharedDataDirectory()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 216
    const/4 v0, 0x0

    #@1
    invoke-static {v0}, Landroid/webkit/PluginManager;->getInstance(Landroid/content/Context;)Landroid/webkit/PluginManager;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {v0}, Landroid/webkit/PluginManager;->getPluginSharedDataDirectory()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method private declared-synchronized getSignedPublicKey(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "index"
    .parameter "challenge"
    .parameter "url"

    #@0
    .prologue
    .line 262
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v1, Landroid/webkit/JWebCoreJavaBridge;->sCurrentMainWebView:Ljava/lang/ref/WeakReference;

    #@3
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/webkit/WebViewClassic;

    #@9
    .line 263
    .local v0, current:Landroid/webkit/WebViewClassic;
    if-eqz v0, :cond_15

    #@b
    .line 266
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@e
    move-result-object v1

    #@f
    invoke-static {v1, p1, p2}, Landroid/webkit/CertTool;->getSignedPublicKey(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_20

    #@12
    move-result-object v1

    #@13
    .line 270
    :goto_13
    monitor-exit p0

    #@14
    return-object v1

    #@15
    .line 269
    :cond_15
    :try_start_15
    const-string/jumbo v1, "webkit-timers"

    #@18
    const-string v2, "There is no active WebView for getSignedPublicKey"

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 270
    const-string v1, ""
    :try_end_1f
    .catchall {:try_start_15 .. :try_end_1f} :catchall_20

    #@1f
    goto :goto_13

    #@20
    .line 262
    .end local v0           #current:Landroid/webkit/WebViewClassic;
    :catchall_20
    move-exception v1

    #@21
    monitor-exit p0

    #@22
    throw v1
.end method

.method private native nativeConstructor()V
.end method

.method private native nativeFinalize()V
.end method

.method private native nativeServiceFuncPtrQueue()V
.end method

.method private native nativeUpdatePluginDirectories([Ljava/lang/String;Z)V
.end method

.method static declared-synchronized removeActiveWebView(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "webview"

    #@0
    .prologue
    .line 79
    const-class v1, Landroid/webkit/JWebCoreJavaBridge;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/JWebCoreJavaBridge;->sCurrentMainWebView:Ljava/lang/ref/WeakReference;

    #@5
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_13

    #@8
    move-result-object v0

    #@9
    if-eq v0, p0, :cond_d

    #@b
    .line 84
    :goto_b
    monitor-exit v1

    #@c
    return-void

    #@d
    .line 83
    :cond_d
    :try_start_d
    sget-object v0, Landroid/webkit/JWebCoreJavaBridge;->sCurrentMainWebView:Ljava/lang/ref/WeakReference;

    #@f
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V
    :try_end_12
    .catchall {:try_start_d .. :try_end_12} :catchall_13

    #@12
    goto :goto_b

    #@13
    .line 79
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1

    #@15
    throw v0
.end method

.method private resolveFilePathForContentUri(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    .line 276
    iget-object v2, p0, Landroid/webkit/JWebCoreJavaBridge;->mContentUriToFilePathMap:Ljava/util/HashMap;

    #@2
    if-eqz v2, :cond_f

    #@4
    .line 277
    iget-object v2, p0, Landroid/webkit/JWebCoreJavaBridge;->mContentUriToFilePathMap:Ljava/util/HashMap;

    #@6
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/String;

    #@c
    .line 278
    .local v0, fileName:Ljava/lang/String;
    if-eqz v0, :cond_f

    #@e
    .line 286
    .end local v0           #fileName:Ljava/lang/String;
    :goto_e
    return-object v0

    #@f
    .line 285
    :cond_f
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@12
    move-result-object v1

    #@13
    .line 286
    .local v1, jUri:Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    goto :goto_e
.end method

.method static declared-synchronized setActiveWebView(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "webview"

    #@0
    .prologue
    .line 71
    const-class v1, Landroid/webkit/JWebCoreJavaBridge;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/JWebCoreJavaBridge;->sCurrentMainWebView:Ljava/lang/ref/WeakReference;

    #@5
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_15

    #@8
    move-result-object v0

    #@9
    if-eqz v0, :cond_d

    #@b
    .line 76
    :goto_b
    monitor-exit v1

    #@c
    return-void

    #@d
    .line 75
    :cond_d
    :try_start_d
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@f
    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@12
    sput-object v0, Landroid/webkit/JWebCoreJavaBridge;->sCurrentMainWebView:Ljava/lang/ref/WeakReference;
    :try_end_14
    .catchall {:try_start_d .. :try_end_14} :catchall_15

    #@14
    goto :goto_b

    #@15
    .line 71
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1

    #@17
    throw v0
.end method

.method private setCookies(Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "url"
    .parameter "value"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 166
    const-string v6, "\r"

    #@3
    invoke-virtual {p2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@6
    move-result v6

    #@7
    if-nez v6, :cond_11

    #@9
    const-string v6, "\n"

    #@b
    invoke-virtual {p2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@e
    move-result v6

    #@f
    if-eqz v6, :cond_51

    #@11
    .line 168
    :cond_11
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@14
    move-result v5

    #@15
    .line 169
    .local v5, size:I
    new-instance v0, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    #@1a
    .line 170
    .local v0, buffer:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@1b
    .line 171
    .local v1, i:I
    :goto_1b
    if-eq v1, v7, :cond_4d

    #@1d
    if-ge v1, v5, :cond_4d

    #@1f
    .line 172
    const/16 v6, 0xd

    #@21
    invoke-virtual {p2, v6, v1}, Ljava/lang/String;->indexOf(II)I

    #@24
    move-result v3

    #@25
    .line 173
    .local v3, ir:I
    const/16 v6, 0xa

    #@27
    invoke-virtual {p2, v6, v1}, Ljava/lang/String;->indexOf(II)I

    #@2a
    move-result v2

    #@2b
    .line 174
    .local v2, in:I
    if-ne v3, v7, :cond_3a

    #@2d
    move v4, v2

    #@2e
    .line 176
    .local v4, newi:I
    :goto_2e
    if-le v4, v1, :cond_44

    #@30
    .line 177
    invoke-virtual {p2, v1, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@37
    .line 182
    :cond_37
    add-int/lit8 v1, v4, 0x1

    #@39
    .line 183
    goto :goto_1b

    #@3a
    .line 174
    .end local v4           #newi:I
    :cond_3a
    if-ne v2, v7, :cond_3e

    #@3c
    move v4, v3

    #@3d
    goto :goto_2e

    #@3e
    :cond_3e
    if-ge v3, v2, :cond_42

    #@40
    move v4, v3

    #@41
    goto :goto_2e

    #@42
    :cond_42
    move v4, v2

    #@43
    goto :goto_2e

    #@44
    .line 178
    .restart local v4       #newi:I
    :cond_44
    if-ne v4, v7, :cond_37

    #@46
    .line 179
    invoke-virtual {p2, v1, v5}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@4d
    .line 184
    .end local v2           #in:I
    .end local v3           #ir:I
    .end local v4           #newi:I
    :cond_4d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object p2

    #@51
    .line 186
    .end local v0           #buffer:Ljava/lang/StringBuilder;
    .end local v1           #i:I
    .end local v5           #size:I
    :cond_51
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    #@54
    move-result-object v6

    #@55
    invoke-virtual {v6, p1, p2}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    #@58
    .line 187
    return-void
.end method

.method private setSharedTimer(J)V
    .registers 7
    .parameter "timemillis"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 226
    const-wide/16 v1, 0x0

    #@3
    cmp-long v1, p1, v1

    #@5
    if-gtz v1, :cond_16

    #@7
    .line 231
    iget-boolean v1, p0, Landroid/webkit/JWebCoreJavaBridge;->mHasInstantTimer:Z

    #@9
    if-eqz v1, :cond_c

    #@b
    .line 242
    :goto_b
    return-void

    #@c
    .line 234
    :cond_c
    iput-boolean v3, p0, Landroid/webkit/JWebCoreJavaBridge;->mHasInstantTimer:Z

    #@e
    .line 235
    invoke-virtual {p0, v3}, Landroid/webkit/JWebCoreJavaBridge;->obtainMessage(I)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 236
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {p0, v0, p1, p2}, Landroid/webkit/JWebCoreJavaBridge;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@15
    goto :goto_b

    #@16
    .line 239
    .end local v0           #msg:Landroid/os/Message;
    :cond_16
    invoke-virtual {p0, v3}, Landroid/webkit/JWebCoreJavaBridge;->obtainMessage(I)Landroid/os/Message;

    #@19
    move-result-object v0

    #@1a
    .line 240
    .restart local v0       #msg:Landroid/os/Message;
    invoke-virtual {p0, v0, p1, p2}, Landroid/webkit/JWebCoreJavaBridge;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1d
    goto :goto_b
.end method

.method private native sharedTimerFired()V
.end method

.method private signalServiceFuncPtrQueue()V
    .registers 3

    #@0
    .prologue
    .line 125
    const/4 v1, 0x2

    #@1
    invoke-virtual {p0, v1}, Landroid/webkit/JWebCoreJavaBridge;->obtainMessage(I)Landroid/os/Message;

    #@4
    move-result-object v0

    #@5
    .line 126
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {p0, v0}, Landroid/webkit/JWebCoreJavaBridge;->sendMessage(Landroid/os/Message;)Z

    #@8
    .line 127
    return-void
.end method

.method private stopSharedTimer()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 251
    const/4 v0, 0x1

    #@2
    invoke-virtual {p0, v0}, Landroid/webkit/JWebCoreJavaBridge;->removeMessages(I)V

    #@5
    .line 252
    iput-boolean v1, p0, Landroid/webkit/JWebCoreJavaBridge;->mHasInstantTimer:Z

    #@7
    .line 253
    iput-boolean v1, p0, Landroid/webkit/JWebCoreJavaBridge;->mHasDeferredTimers:Z

    #@9
    .line 254
    return-void
.end method


# virtual methods
.method public native addPackageName(Ljava/lang/String;)V
.end method

.method public native addPackageNames(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 67
    invoke-direct {p0}, Landroid/webkit/JWebCoreJavaBridge;->nativeFinalize()V

    #@3
    .line 68
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 103
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v0, :sswitch_data_2c

    #@5
    .line 121
    :goto_5
    return-void

    #@6
    .line 105
    :sswitch_6
    iget-boolean v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mTimerPaused:Z

    #@8
    if-eqz v0, :cond_e

    #@a
    .line 106
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mHasDeferredTimers:Z

    #@d
    goto :goto_5

    #@e
    .line 108
    :cond_e
    invoke-direct {p0}, Landroid/webkit/JWebCoreJavaBridge;->fireSharedTimer()V

    #@11
    goto :goto_5

    #@12
    .line 113
    :sswitch_12
    invoke-direct {p0}, Landroid/webkit/JWebCoreJavaBridge;->nativeServiceFuncPtrQueue()V

    #@15
    goto :goto_5

    #@16
    .line 116
    :sswitch_16
    const/4 v0, 0x0

    #@17
    invoke-static {v0}, Landroid/webkit/PluginManager;->getInstance(Landroid/content/Context;)Landroid/webkit/PluginManager;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Landroid/webkit/PluginManager;->getPluginDirectories()[Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    check-cast v0, Ljava/lang/Boolean;

    #@23
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@26
    move-result v0

    #@27
    invoke-direct {p0, v1, v0}, Landroid/webkit/JWebCoreJavaBridge;->nativeUpdatePluginDirectories([Ljava/lang/String;Z)V

    #@2a
    goto :goto_5

    #@2b
    .line 103
    nop

    #@2c
    :sswitch_data_2c
    .sparse-switch
        0x1 -> :sswitch_6
        0x2 -> :sswitch_12
        0x64 -> :sswitch_16
    .end sparse-switch
.end method

.method public native nativeUpdateProxy(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public pause()V
    .registers 2

    #@0
    .prologue
    .line 135
    iget-boolean v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mTimerPaused:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 136
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mTimerPaused:Z

    #@7
    .line 137
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mHasDeferredTimers:Z

    #@a
    .line 139
    :cond_a
    return-void
.end method

.method public native removePackageName(Ljava/lang/String;)V
.end method

.method public resume()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 145
    iget-boolean v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mTimerPaused:Z

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 146
    iput-boolean v1, p0, Landroid/webkit/JWebCoreJavaBridge;->mTimerPaused:Z

    #@7
    .line 147
    iget-boolean v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mHasDeferredTimers:Z

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 148
    iput-boolean v1, p0, Landroid/webkit/JWebCoreJavaBridge;->mHasDeferredTimers:Z

    #@d
    .line 149
    invoke-direct {p0}, Landroid/webkit/JWebCoreJavaBridge;->fireSharedTimer()V

    #@10
    .line 152
    :cond_10
    return-void
.end method

.method public native setCacheSize(I)V
.end method

.method public native setNetworkOnLine(Z)V
.end method

.method public native setNetworkType(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public storeFilePathForContentUri(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "path"
    .parameter "contentUri"

    #@0
    .prologue
    .line 290
    iget-object v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mContentUriToFilePathMap:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 291
    new-instance v0, Ljava/util/HashMap;

    #@6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mContentUriToFilePathMap:Ljava/util/HashMap;

    #@b
    .line 293
    :cond_b
    iget-object v0, p0, Landroid/webkit/JWebCoreJavaBridge;->mContentUriToFilePathMap:Ljava/util/HashMap;

    #@d
    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    .line 294
    return-void
.end method

.method public updateProxy(Landroid/net/ProxyProperties;)V
    .registers 6
    .parameter "proxyProperties"

    #@0
    .prologue
    .line 297
    if-nez p1, :cond_a

    #@2
    .line 298
    const-string v2, ""

    #@4
    const-string v3, ""

    #@6
    invoke-virtual {p0, v2, v3}, Landroid/webkit/JWebCoreJavaBridge;->nativeUpdateProxy(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 308
    :goto_9
    return-void

    #@a
    .line 302
    :cond_a
    invoke-virtual {p1}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 303
    .local v0, host:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/ProxyProperties;->getPort()I

    #@11
    move-result v1

    #@12
    .line 304
    .local v1, port:I
    if-eqz v1, :cond_2b

    #@14
    .line 305
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, ":"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    .line 307
    :cond_2b
    invoke-virtual {p1}, Landroid/net/ProxyProperties;->getExclusionList()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {p0, v0, v2}, Landroid/webkit/JWebCoreJavaBridge;->nativeUpdateProxy(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    goto :goto_9
.end method
