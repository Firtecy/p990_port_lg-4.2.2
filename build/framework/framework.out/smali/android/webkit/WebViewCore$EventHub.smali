.class public Landroid/webkit/WebViewCore$EventHub;
.super Ljava/lang/Object;
.source "WebViewCore.java"

# interfaces
.implements Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewCore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EventHub"
.end annotation


# static fields
.field static final ADD_JS_INTERFACE:I = 0x8a

.field static final ADD_PACKAGE_NAME:I = 0xb9

.field static final ADD_PACKAGE_NAMES:I = 0xb8

.field static final AUTOFILL_FORM:I = 0xc0

.field static final CLEAR_CACHE:I = 0x6f

.field static final CLEAR_CONTENT:I = 0x86

.field static final CLEAR_HISTORY:I = 0x70

.field static final CLEAR_SSL_PREF_TABLE:I = 0x96

.field static final CONTENT_INVALIDATE_ALL:I = 0xaf

.field static final COPY_TEXT:I = 0xd2

.field static final DELETE_SELECTION:I = 0x7a

.field static final DELETE_SURROUNDING_TEXT:I = 0x81

.field static final DELETE_TEXT:I = 0xd3

.field private static final DESTROY:I = 0xc8

.field static final DOC_HAS_IMAGES:I = 0x78

.field static final DUMP_DOMTREE:I = 0xaa

.field static final DUMP_RENDERTREE:I = 0xab

.field static final EXECUTE_JS:I = 0xc2

.field static final FIND_ALL:I = 0xdd

.field static final FIND_NEXT:I = 0xde

.field private static final FIRST_PACKAGE_MSG_ID:I = 0x5f

.field static final FREE_MEMORY:I = 0x91

.field static final GEOLOCATION_PERMISSIONS_PROVIDE:I = 0xb4

.field static final GET_TEXTINPUT_POS:I = 0xec

.field static final GO_BACK_FORWARD:I = 0x6a

.field static final HEARTBEAT:I = 0xc5

.field static final HIDE_FULLSCREEN:I = 0xb6

.field static final INSERT_TEXT:I = 0xd4

.field static final KEY_DOWN:I = 0x67

.field static final KEY_PRESS:I = 0xdf

.field static final KEY_UP:I = 0x68

.field private static final LAST_PACKAGE_MSG_ID:I = 0x95

.field static final LISTBOX_CHOICES:I = 0x7b

.field static final LOAD_DATA:I = 0x8b

.field static final LOAD_URL:I = 0x64

.field public static final MESSAGE_RELAY:I = 0x7d

.field static final MODIFY_SELECTION:I = 0xbe

.field static final NOTIFY_ANIMATION_STARTED:I = 0xc4

.field static final ON_PAUSE:I = 0x8f

.field static final ON_RESUME:I = 0x90

.field static final PASS_TO_JS:I = 0x73

.field static final PAUSE_TIMERS:I = 0x6d

.field static final PLUGIN_SURFACE_READY:I = 0xc3

.field static final POPULATE_VISITED_LINKS:I = 0xb5

.field static final POST_URL:I = 0x84

.field static final PROXY_CHANGED:I = 0xc1

.field static final RELOAD:I = 0x66

.field static final REMOVE_JS_INTERFACE:I = 0x95

.field static final REMOVE_PACKAGE_NAME:I = 0xba

.field static final REPLACE_TEXT:I = 0x72

.field static final REQUEST_CURSOR_HREF:I = 0x89

.field static final REQUEST_DOC_AS_TEXT:I = 0xa1

.field static final REQUEST_EXT_REPRESENTATION:I = 0xa0

.field static final RESTORE_STATE:I = 0x6c

.field static final RESUME_TIMERS:I = 0x6e

.field static final REVEAL_SELECTION:I = 0x60

.field static final SAVE_CACHED_IMAGE:I = 0xe6

.field static final SAVE_DOCUMENT_STATE:I = 0x80

.field static final SAVE_VIEW_STATE:I = 0xe1

.field static final SAVE_WEBARCHIVE:I = 0x93

.field static final SCROLL_LAYER:I = 0xc6

.field static final SCROLL_TEXT_INPUT:I = 0x63

.field static final SELECT_ALL:I = 0xd7

.field static final SELECT_TEXT:I = 0xd5

.field static final SELECT_WORD_AT:I = 0xd6

.field static final SEND_NOTI_CLICKED:I = 0xe7

.field static final SEND_NOTI_CLOSED:I = 0xe8

.field static final SEND_PICKER_VALUE:I = 0x5f

.field static final SET_ACTIVE:I = 0x8e

.field static final SET_BACKGROUND_COLOR:I = 0x7e

.field static final SET_GLOBAL_BOUNDS:I = 0x74

.field static final SET_INITIAL_FOCUS:I = 0xe0

.field static final SET_JS_FLAGS:I = 0xae

.field static final SET_MOVE_MOUSE:I = 0x87

.field static final SET_NETWORK_STATE:I = 0x77

.field static final SET_NETWORK_TYPE:I = 0xb7

.field static final SET_SCROLL_OFFSET:I = 0x6b

.field static final SET_SELECTION:I = 0x71

.field static final SET_USE_MOCK_DEVICE_ORIENTATION:I = 0xbf

.field static final SET_USE_MOCK_GEOLOCATION:I = 0xe2

.field static final SINGLE_LISTBOX_CHOICE:I = 0x7c

.field static final STOP_LOADING:I = 0x65

.field static final TRUST_STORAGE_UPDATED:I = 0xdc

.field static final VIEW_SIZE_CHANGED:I = 0x69

.field static final WEBKIT_DRAW:I = 0x82


# instance fields
.field private mBlockMessages:Z

.field private mDestroying:Z

.field private mHandler:Landroid/os/Handler;

.field private mMessages:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private mSavedPriority:I

.field private mTid:I

.field final synthetic this$0:Landroid/webkit/WebViewCore;


# direct methods
.method private constructor <init>(Landroid/webkit/WebViewCore;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1279
    iput-object p1, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1267
    new-instance v0, Ljava/util/LinkedList;

    #@7
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@c
    .line 1279
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/WebViewCore;Landroid/webkit/WebViewCore$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1120
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore$EventHub;-><init>(Landroid/webkit/WebViewCore;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Landroid/webkit/WebViewCore$EventHub;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1120
    iget-boolean v0, p0, Landroid/webkit/WebViewCore$EventHub;->mDestroying:Z

    #@2
    return v0
.end method

.method static synthetic access$1202(Landroid/webkit/WebViewCore$EventHub;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1120
    iput-boolean p1, p0, Landroid/webkit/WebViewCore$EventHub;->mDestroying:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Landroid/webkit/WebViewCore$EventHub;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 1120
    invoke-direct {p0}, Landroid/webkit/WebViewCore$EventHub;->transferMessages()V

    #@3
    return-void
.end method

.method static synthetic access$7600(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1120
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore$EventHub;->sendMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$7700(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1120
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore$EventHub;->sendMessageAtFrontOfQueue(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$7800(Landroid/webkit/WebViewCore$EventHub;Landroid/os/Message;J)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 1120
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebViewCore$EventHub;->sendMessageDelayed(Landroid/os/Message;J)V

    #@3
    return-void
.end method

.method static synthetic access$7900(Landroid/webkit/WebViewCore$EventHub;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1120
    invoke-direct {p0, p1}, Landroid/webkit/WebViewCore$EventHub;->removeMessages(I)V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/webkit/WebViewCore$EventHub;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1120
    iget v0, p0, Landroid/webkit/WebViewCore$EventHub;->mSavedPriority:I

    #@2
    return v0
.end method

.method static synthetic access$8000(Landroid/webkit/WebViewCore$EventHub;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 1120
    invoke-direct {p0}, Landroid/webkit/WebViewCore$EventHub;->removeMessages()V

    #@3
    return-void
.end method

.method static synthetic access$802(Landroid/webkit/WebViewCore$EventHub;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1120
    iput p1, p0, Landroid/webkit/WebViewCore$EventHub;->mSavedPriority:I

    #@2
    return p1
.end method

.method static synthetic access$8100(Landroid/webkit/WebViewCore$EventHub;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 1120
    invoke-direct {p0}, Landroid/webkit/WebViewCore$EventHub;->blockMessages()V

    #@3
    return-void
.end method

.method static synthetic access$900(Landroid/webkit/WebViewCore$EventHub;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1120
    iget v0, p0, Landroid/webkit/WebViewCore$EventHub;->mTid:I

    #@2
    return v0
.end method

.method private declared-synchronized blockMessages()V
    .registers 2

    #@0
    .prologue
    .line 2003
    monitor-enter p0

    #@1
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Landroid/webkit/WebViewCore$EventHub;->mBlockMessages:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    #@4
    .line 2004
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 2003
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method

.method private declared-synchronized removeMessages()V
    .registers 3

    #@0
    .prologue
    .line 1991
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/webkit/WebViewCore;->access$7502(Landroid/webkit/WebViewCore;Z)Z

    #@7
    .line 1992
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 1993
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@d
    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_19

    #@10
    .line 1997
    :goto_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 1995
    :cond_12
    :try_start_12
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mHandler:Landroid/os/Handler;

    #@14
    const/4 v1, 0x0

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
    :try_end_18
    .catchall {:try_start_12 .. :try_end_18} :catchall_19

    #@18
    goto :goto_10

    #@19
    .line 1991
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit p0

    #@1b
    throw v0
.end method

.method private declared-synchronized removeMessages(I)V
    .registers 6
    .parameter "what"

    #@0
    .prologue
    .line 1946
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v2, p0, Landroid/webkit/WebViewCore$EventHub;->mBlockMessages:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_2f

    #@3
    if-eqz v2, :cond_7

    #@5
    .line 1963
    :cond_5
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 1949
    :cond_7
    const/16 v2, 0x82

    #@9
    if-ne p1, v2, :cond_11

    #@b
    .line 1950
    :try_start_b
    iget-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@d
    const/4 v3, 0x0

    #@e
    invoke-static {v2, v3}, Landroid/webkit/WebViewCore;->access$7502(Landroid/webkit/WebViewCore;Z)Z

    #@11
    .line 1952
    :cond_11
    iget-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@13
    if-eqz v2, :cond_32

    #@15
    .line 1953
    iget-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@17
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v0

    #@1b
    .line 1954
    .local v0, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/os/Message;>;"
    :cond_1b
    :goto_1b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_5

    #@21
    .line 1955
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Landroid/os/Message;

    #@27
    .line 1956
    .local v1, m:Landroid/os/Message;
    iget v2, v1, Landroid/os/Message;->what:I

    #@29
    if-ne v2, p1, :cond_1b

    #@2b
    .line 1957
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V
    :try_end_2e
    .catchall {:try_start_b .. :try_end_2e} :catchall_2f

    #@2e
    goto :goto_1b

    #@2f
    .line 1946
    .end local v0           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/os/Message;>;"
    .end local v1           #m:Landroid/os/Message;
    :catchall_2f
    move-exception v2

    #@30
    monitor-exit p0

    #@31
    throw v2

    #@32
    .line 1961
    :cond_32
    :try_start_32
    iget-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->mHandler:Landroid/os/Handler;

    #@34
    invoke-virtual {v2, p1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_37
    .catchall {:try_start_32 .. :try_end_37} :catchall_2f

    #@37
    goto :goto_5
.end method

.method private declared-synchronized sendMessage(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 1935
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebViewCore$EventHub;->mBlockMessages:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-eqz v0, :cond_7

    #@5
    .line 1943
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 1938
    :cond_7
    :try_start_7
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@9
    if-eqz v0, :cond_14

    #@b
    .line 1939
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_11

    #@10
    goto :goto_5

    #@11
    .line 1935
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0

    #@14
    .line 1941
    :cond_14
    :try_start_14
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mHandler:Landroid/os/Handler;

    #@16
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_19
    .catchall {:try_start_14 .. :try_end_19} :catchall_11

    #@19
    goto :goto_5
.end method

.method private declared-synchronized sendMessageAtFrontOfQueue(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 1976
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebViewCore$EventHub;->mBlockMessages:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_12

    #@3
    if-eqz v0, :cond_7

    #@5
    .line 1984
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 1979
    :cond_7
    :try_start_7
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@9
    if-eqz v0, :cond_15

    #@b
    .line 1980
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V
    :try_end_11
    .catchall {:try_start_7 .. :try_end_11} :catchall_12

    #@11
    goto :goto_5

    #@12
    .line 1976
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0

    #@15
    .line 1982
    :cond_15
    :try_start_15
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mHandler:Landroid/os/Handler;

    #@17
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z
    :try_end_1a
    .catchall {:try_start_15 .. :try_end_1a} :catchall_12

    #@1a
    goto :goto_5
.end method

.method private declared-synchronized sendMessageDelayed(Landroid/os/Message;J)V
    .registers 5
    .parameter "msg"
    .parameter "delay"

    #@0
    .prologue
    .line 1966
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebViewCore$EventHub;->mBlockMessages:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_d

    #@3
    if-eqz v0, :cond_7

    #@5
    .line 1970
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 1969
    :cond_7
    :try_start_7
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_d

    #@c
    goto :goto_5

    #@d
    .line 1966
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0

    #@f
    throw v0
.end method

.method private transferMessages()V
    .registers 5

    #@0
    .prologue
    .line 1291
    invoke-static {}, Landroid/os/Process;->myTid()I

    #@3
    move-result v2

    #@4
    iput v2, p0, Landroid/webkit/WebViewCore$EventHub;->mTid:I

    #@6
    .line 1292
    iget v2, p0, Landroid/webkit/WebViewCore$EventHub;->mTid:I

    #@8
    invoke-static {v2}, Landroid/os/Process;->getThreadPriority(I)I

    #@b
    move-result v2

    #@c
    iput v2, p0, Landroid/webkit/WebViewCore$EventHub;->mSavedPriority:I

    #@e
    .line 1294
    new-instance v2, Landroid/webkit/WebViewCore$EventHub$1;

    #@10
    invoke-direct {v2, p0}, Landroid/webkit/WebViewCore$EventHub$1;-><init>(Landroid/webkit/WebViewCore$EventHub;)V

    #@13
    iput-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->mHandler:Landroid/os/Handler;

    #@15
    .line 1871
    monitor-enter p0

    #@16
    .line 1872
    :try_start_16
    iget-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@18
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    #@1b
    move-result v1

    #@1c
    .line 1873
    .local v1, size:I
    const/4 v0, 0x0

    #@1d
    .local v0, i:I
    :goto_1d
    if-ge v0, v1, :cond_2f

    #@1f
    .line 1874
    iget-object v3, p0, Landroid/webkit/WebViewCore$EventHub;->mHandler:Landroid/os/Handler;

    #@21
    iget-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@23
    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v2

    #@27
    check-cast v2, Landroid/os/Message;

    #@29
    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@2c
    .line 1873
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_1d

    #@2f
    .line 1876
    :cond_2f
    const/4 v2, 0x0

    #@30
    iput-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->mMessages:Ljava/util/LinkedList;

    #@32
    .line 1877
    monitor-exit p0

    #@33
    .line 1878
    return-void

    #@34
    .line 1877
    .end local v0           #i:I
    .end local v1           #size:I
    :catchall_34
    move-exception v2

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_16 .. :try_end_36} :catchall_34

    #@36
    throw v2
.end method


# virtual methods
.method public dispatchWebKitEvent(Landroid/webkit/WebViewInputDispatcher;Landroid/view/MotionEvent;II)Z
    .registers 20
    .parameter "dispatcher"
    .parameter "event"
    .parameter "eventType"
    .parameter "flags"

    #@0
    .prologue
    .line 1888
    iget-object v1, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@2
    invoke-static {v1}, Landroid/webkit/WebViewCore;->access$300(Landroid/webkit/WebViewCore;)I

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_a

    #@8
    .line 1889
    const/4 v1, 0x0

    #@9
    .line 1927
    :goto_9
    return v1

    #@a
    .line 1891
    :cond_a
    sparse-switch p3, :sswitch_data_ac

    #@d
    .line 1927
    const/4 v1, 0x0

    #@e
    goto :goto_9

    #@f
    .line 1893
    :sswitch_f
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    #@12
    move-result v1

    #@13
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@16
    move-result v13

    #@17
    .line 1894
    .local v13, x:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    #@1a
    move-result v1

    #@1b
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@1e
    move-result v14

    #@1f
    .line 1895
    .local v14, y:I
    iget-object v1, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@21
    iget-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@23
    invoke-static {v2}, Landroid/webkit/WebViewCore;->access$1100(Landroid/webkit/WebViewCore;)Landroid/webkit/WebViewClassic;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getScaledNavSlop()I

    #@2a
    move-result v2

    #@2b
    const/4 v3, 0x1

    #@2c
    invoke-static {v1, v13, v14, v2, v3}, Landroid/webkit/WebViewCore;->access$3700(Landroid/webkit/WebViewCore;IIIZ)Landroid/webkit/WebViewCore$WebKitHitTest;

    #@2f
    move-result-object v10

    #@30
    .line 1897
    .local v10, hit:Landroid/webkit/WebViewCore$WebKitHitTest;
    iget-object v1, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@32
    invoke-static {v1}, Landroid/webkit/WebViewCore;->access$1100(Landroid/webkit/WebViewCore;)Landroid/webkit/WebViewClassic;

    #@35
    move-result-object v1

    #@36
    iget-object v1, v1, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@38
    const/16 v2, 0x83

    #@3a
    invoke-virtual {v1, v2, v10}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@41
    .line 1899
    const/4 v1, 0x0

    #@42
    goto :goto_9

    #@43
    .line 1902
    .end local v10           #hit:Landroid/webkit/WebViewCore$WebKitHitTest;
    .end local v13           #x:I
    .end local v14           #y:I
    :sswitch_43
    iget-object v1, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@45
    iget-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@47
    invoke-static {v2}, Landroid/webkit/WebViewCore;->access$300(Landroid/webkit/WebViewCore;)I

    #@4a
    move-result v2

    #@4b
    invoke-static {v1, v2}, Landroid/webkit/WebViewCore;->access$7300(Landroid/webkit/WebViewCore;I)Z

    #@4e
    move-result v1

    #@4f
    goto :goto_9

    #@50
    .line 1905
    :sswitch_50
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    #@53
    move-result v7

    #@54
    .line 1906
    .local v7, count:I
    new-array v4, v7, [I

    #@56
    .line 1907
    .local v4, idArray:[I
    new-array v5, v7, [I

    #@58
    .line 1908
    .local v5, xArray:[I
    new-array v6, v7, [I

    #@5a
    .line 1909
    .local v6, yArray:[I
    const/4 v11, 0x0

    #@5b
    .local v11, i:I
    :goto_5b
    if-ge v11, v7, :cond_7a

    #@5d
    .line 1910
    move-object/from16 v0, p2

    #@5f
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@62
    move-result v1

    #@63
    aput v1, v4, v11

    #@65
    .line 1911
    move-object/from16 v0, p2

    #@67
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    #@6a
    move-result v1

    #@6b
    float-to-int v1, v1

    #@6c
    aput v1, v5, v11

    #@6e
    .line 1912
    move-object/from16 v0, p2

    #@70
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getY(I)F

    #@73
    move-result v1

    #@74
    float-to-int v1, v1

    #@75
    aput v1, v6, v11

    #@77
    .line 1909
    add-int/lit8 v11, v11, 0x1

    #@79
    goto :goto_5b

    #@7a
    .line 1914
    :cond_7a
    iget-object v1, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@7c
    iget-object v2, p0, Landroid/webkit/WebViewCore$EventHub;->this$0:Landroid/webkit/WebViewCore;

    #@7e
    invoke-static {v2}, Landroid/webkit/WebViewCore;->access$300(Landroid/webkit/WebViewCore;)I

    #@81
    move-result v2

    #@82
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionMasked()I

    #@85
    move-result v3

    #@86
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionIndex()I

    #@89
    move-result v8

    #@8a
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getMetaState()I

    #@8d
    move-result v9

    #@8e
    invoke-static/range {v1 .. v9}, Landroid/webkit/WebViewCore;->access$7400(Landroid/webkit/WebViewCore;II[I[I[IIII)I

    #@91
    move-result v12

    #@92
    .line 1918
    .local v12, touchFlags:I
    if-nez v12, :cond_a2

    #@94
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionMasked()I

    #@97
    move-result v1

    #@98
    const/4 v2, 0x3

    #@99
    if-eq v1, v2, :cond_a2

    #@9b
    and-int/lit8 v1, p4, 0x1

    #@9d
    if-nez v1, :cond_a2

    #@9f
    .line 1921
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebViewInputDispatcher;->skipWebkitForRemainingTouchStream()V

    #@a2
    .line 1923
    :cond_a2
    and-int/lit8 v1, v12, 0x2

    #@a4
    if-lez v1, :cond_a9

    #@a6
    const/4 v1, 0x1

    #@a7
    goto/16 :goto_9

    #@a9
    :cond_a9
    const/4 v1, 0x0

    #@aa
    goto/16 :goto_9

    #@ac
    .line 1891
    :sswitch_data_ac
    .sparse-switch
        0x0 -> :sswitch_50
        0x4 -> :sswitch_43
        0x6 -> :sswitch_f
    .end sparse-switch
.end method

.method public getWebKitLooper()Landroid/os/Looper;
    .registers 2

    #@0
    .prologue
    .line 1882
    iget-object v0, p0, Landroid/webkit/WebViewCore$EventHub;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
