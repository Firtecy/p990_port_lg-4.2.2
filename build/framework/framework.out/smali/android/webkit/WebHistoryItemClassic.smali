.class Landroid/webkit/WebHistoryItemClassic;
.super Landroid/webkit/WebHistoryItem;
.source "WebHistoryItemClassic.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static sNextId:I


# instance fields
.field private mCustomData:Ljava/lang/Object;

.field private mFavicon:Landroid/graphics/Bitmap;

.field private mFlattenedData:[B

.field private final mId:I

.field private mNativeBridge:I

.field private mTouchIconUrlFromLink:Ljava/lang/String;

.field private mTouchIconUrlServerDefault:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 26
    const/4 v0, 0x0

    #@1
    sput v0, Landroid/webkit/WebHistoryItemClassic;->sNextId:I

    #@3
    return-void
.end method

.method private constructor <init>(I)V
    .registers 5
    .parameter "nativeBridge"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Landroid/webkit/WebHistoryItem;-><init>()V

    #@3
    .line 49
    const-class v1, Landroid/webkit/WebHistoryItemClassic;

    #@5
    monitor-enter v1

    #@6
    .line 50
    :try_start_6
    sget v0, Landroid/webkit/WebHistoryItemClassic;->sNextId:I

    #@8
    add-int/lit8 v2, v0, 0x1

    #@a
    sput v2, Landroid/webkit/WebHistoryItemClassic;->sNextId:I

    #@c
    iput v0, p0, Landroid/webkit/WebHistoryItemClassic;->mId:I

    #@e
    .line 51
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_17

    #@f
    .line 52
    iput p1, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@11
    .line 53
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@13
    invoke-direct {p0, v0}, Landroid/webkit/WebHistoryItemClassic;->nativeRef(I)V

    #@16
    .line 54
    return-void

    #@17
    .line 51
    :catchall_17
    move-exception v0

    #@18
    :try_start_18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_18 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method private constructor <init>(Landroid/webkit/WebHistoryItemClassic;)V
    .registers 3
    .parameter "item"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Landroid/webkit/WebHistoryItem;-><init>()V

    #@3
    .line 79
    iget-object v0, p1, Landroid/webkit/WebHistoryItemClassic;->mFlattenedData:[B

    #@5
    iput-object v0, p0, Landroid/webkit/WebHistoryItemClassic;->mFlattenedData:[B

    #@7
    .line 80
    iget v0, p1, Landroid/webkit/WebHistoryItemClassic;->mId:I

    #@9
    iput v0, p0, Landroid/webkit/WebHistoryItemClassic;->mId:I

    #@b
    .line 81
    iget-object v0, p1, Landroid/webkit/WebHistoryItemClassic;->mFavicon:Landroid/graphics/Bitmap;

    #@d
    iput-object v0, p0, Landroid/webkit/WebHistoryItemClassic;->mFavicon:Landroid/graphics/Bitmap;

    #@f
    .line 82
    iget v0, p1, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@11
    iput v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@13
    .line 83
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@15
    if-eqz v0, :cond_1c

    #@17
    .line 84
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@19
    invoke-direct {p0, v0}, Landroid/webkit/WebHistoryItemClassic;->nativeRef(I)V

    #@1c
    .line 86
    :cond_1c
    return-void
.end method

.method constructor <init>([B)V
    .registers 5
    .parameter "data"

    #@0
    .prologue
    .line 67
    invoke-direct {p0}, Landroid/webkit/WebHistoryItem;-><init>()V

    #@3
    .line 68
    iput-object p1, p0, Landroid/webkit/WebHistoryItemClassic;->mFlattenedData:[B

    #@5
    .line 69
    const-class v1, Landroid/webkit/WebHistoryItemClassic;

    #@7
    monitor-enter v1

    #@8
    .line 70
    :try_start_8
    sget v0, Landroid/webkit/WebHistoryItemClassic;->sNextId:I

    #@a
    add-int/lit8 v2, v0, 0x1

    #@c
    sput v2, Landroid/webkit/WebHistoryItemClassic;->sNextId:I

    #@e
    iput v0, p0, Landroid/webkit/WebHistoryItemClassic;->mId:I

    #@10
    .line 71
    monitor-exit v1

    #@11
    .line 72
    return-void

    #@12
    .line 71
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_8 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method private native inflate(I[B)I
.end method

.method private native nativeGetFavicon(I)Landroid/graphics/Bitmap;
.end method

.method private native nativeGetFlattenedData(I)[B
.end method

.method private native nativeGetOriginalUrl(I)Ljava/lang/String;
.end method

.method private native nativeGetTitle(I)Ljava/lang/String;
.end method

.method private native nativeGetUrl(I)Ljava/lang/String;
.end method

.method private native nativeRef(I)V
.end method

.method private native nativeUnref(I)V
.end method


# virtual methods
.method public bridge synthetic clone()Landroid/webkit/WebHistoryItem;
    .registers 2

    #@0
    .prologue
    .line 24
    invoke-virtual {p0}, Landroid/webkit/WebHistoryItemClassic;->clone()Landroid/webkit/WebHistoryItemClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public declared-synchronized clone()Landroid/webkit/WebHistoryItemClassic;
    .registers 2

    #@0
    .prologue
    .line 207
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/WebHistoryItemClassic;

    #@3
    invoke-direct {v0, p0}, Landroid/webkit/WebHistoryItemClassic;-><init>(Landroid/webkit/WebHistoryItemClassic;)V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    #@6
    monitor-exit p0

    #@7
    return-object v0

    #@8
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 24
    invoke-virtual {p0}, Landroid/webkit/WebHistoryItemClassic;->clone()Landroid/webkit/WebHistoryItemClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 57
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 58
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@6
    invoke-direct {p0, v0}, Landroid/webkit/WebHistoryItemClassic;->nativeUnref(I)V

    #@9
    .line 59
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@c
    .line 61
    :cond_c
    return-void
.end method

.method public getCustomData()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/webkit/WebHistoryItemClassic;->mCustomData:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method public getFavicon()Landroid/graphics/Bitmap;
    .registers 2

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Landroid/webkit/WebHistoryItemClassic;->mFavicon:Landroid/graphics/Bitmap;

    #@2
    if-nez v0, :cond_10

    #@4
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 110
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@a
    invoke-direct {p0, v0}, Landroid/webkit/WebHistoryItemClassic;->nativeGetFavicon(I)Landroid/graphics/Bitmap;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Landroid/webkit/WebHistoryItemClassic;->mFavicon:Landroid/graphics/Bitmap;

    #@10
    .line 112
    :cond_10
    iget-object v0, p0, Landroid/webkit/WebHistoryItemClassic;->mFavicon:Landroid/graphics/Bitmap;

    #@12
    return-object v0
.end method

.method getFlattenedData()[B
    .registers 2

    #@0
    .prologue
    .line 190
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 191
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@6
    invoke-direct {p0, v0}, Landroid/webkit/WebHistoryItemClassic;->nativeGetFlattenedData(I)[B

    #@9
    move-result-object v0

    #@a
    .line 193
    :goto_a
    return-object v0

    #@b
    :cond_b
    iget-object v0, p0, Landroid/webkit/WebHistoryItemClassic;->mFlattenedData:[B

    #@d
    goto :goto_a
.end method

.method public getId()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mId:I

    #@2
    return v0
.end method

.method public getOriginalUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 99
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 100
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@8
    invoke-direct {p0, v0}, Landroid/webkit/WebHistoryItemClassic;->nativeGetOriginalUrl(I)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getTitle()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 104
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 105
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@8
    invoke-direct {p0, v0}, Landroid/webkit/WebHistoryItemClassic;->nativeGetTitle(I)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getTouchIconUrl()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 125
    iget-object v2, p0, Landroid/webkit/WebHistoryItemClassic;->mTouchIconUrlFromLink:Ljava/lang/String;

    #@2
    if-eqz v2, :cond_7

    #@4
    .line 126
    iget-object v2, p0, Landroid/webkit/WebHistoryItemClassic;->mTouchIconUrlFromLink:Ljava/lang/String;

    #@6
    .line 138
    :goto_6
    return-object v2

    #@7
    .line 127
    :cond_7
    iget-object v2, p0, Landroid/webkit/WebHistoryItemClassic;->mTouchIconUrlServerDefault:Ljava/lang/String;

    #@9
    if-eqz v2, :cond_e

    #@b
    .line 128
    iget-object v2, p0, Landroid/webkit/WebHistoryItemClassic;->mTouchIconUrlServerDefault:Ljava/lang/String;

    #@d
    goto :goto_6

    #@e
    .line 132
    :cond_e
    :try_start_e
    new-instance v1, Ljava/net/URL;

    #@10
    invoke-virtual {p0}, Landroid/webkit/WebHistoryItemClassic;->getOriginalUrl()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@17
    .line 133
    .local v1, url:Ljava/net/URL;
    new-instance v2, Ljava/net/URL;

    #@19
    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v1}, Ljava/net/URL;->getPort()I

    #@24
    move-result v5

    #@25
    const-string v6, "/apple-touch-icon.png"

    #@27
    invoke-direct {v2, v3, v4, v5, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@2a
    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    iput-object v2, p0, Landroid/webkit/WebHistoryItemClassic;->mTouchIconUrlServerDefault:Ljava/lang/String;
    :try_end_30
    .catch Ljava/net/MalformedURLException; {:try_start_e .. :try_end_30} :catch_33

    #@30
    .line 138
    iget-object v2, p0, Landroid/webkit/WebHistoryItemClassic;->mTouchIconUrlServerDefault:Ljava/lang/String;

    #@32
    goto :goto_6

    #@33
    .line 135
    .end local v1           #url:Ljava/net/URL;
    :catch_33
    move-exception v0

    #@34
    .line 136
    .local v0, e:Ljava/net/MalformedURLException;
    const/4 v2, 0x0

    #@35
    goto :goto_6
.end method

.method public getUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 94
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 95
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@8
    invoke-direct {p0, v0}, Landroid/webkit/WebHistoryItemClassic;->nativeGetUrl(I)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method inflate(I)V
    .registers 3
    .parameter "nativeFrame"

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Landroid/webkit/WebHistoryItemClassic;->mFlattenedData:[B

    #@2
    invoke-direct {p0, p1, v0}, Landroid/webkit/WebHistoryItemClassic;->inflate(I[B)I

    #@5
    move-result v0

    #@6
    iput v0, p0, Landroid/webkit/WebHistoryItemClassic;->mNativeBridge:I

    #@8
    .line 203
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Landroid/webkit/WebHistoryItemClassic;->mFlattenedData:[B

    #@b
    .line 204
    return-void
.end method

.method public setCustomData(Ljava/lang/Object;)V
    .registers 2
    .parameter "data"

    #@0
    .prologue
    .line 160
    iput-object p1, p0, Landroid/webkit/WebHistoryItemClassic;->mCustomData:Ljava/lang/Object;

    #@2
    .line 161
    return-void
.end method

.method setFavicon(Landroid/graphics/Bitmap;)V
    .registers 2
    .parameter "icon"

    #@0
    .prologue
    .line 170
    iput-object p1, p0, Landroid/webkit/WebHistoryItemClassic;->mFavicon:Landroid/graphics/Bitmap;

    #@2
    .line 171
    return-void
.end method

.method setTouchIconUrl(Ljava/lang/String;Z)V
    .registers 4
    .parameter "url"
    .parameter "precomposed"

    #@0
    .prologue
    .line 179
    if-nez p2, :cond_6

    #@2
    iget-object v0, p0, Landroid/webkit/WebHistoryItemClassic;->mTouchIconUrlFromLink:Ljava/lang/String;

    #@4
    if-nez v0, :cond_8

    #@6
    .line 180
    :cond_6
    iput-object p1, p0, Landroid/webkit/WebHistoryItemClassic;->mTouchIconUrlFromLink:Ljava/lang/String;

    #@8
    .line 182
    :cond_8
    return-void
.end method
