.class Landroid/webkit/WebViewClassic$ProxyReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WebViewClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProxyReceiver"
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1904
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/WebViewClassic$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1904
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$ProxyReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 5
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 1908
    invoke-static {}, Landroid/webkit/WebViewClassic;->access$1400()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_16

    #@6
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    const-string v1, "android.intent.action.PROXY_CHANGE"

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_16

    #@12
    .line 1909
    invoke-static {p2}, Landroid/webkit/WebViewClassic;->access$1500(Landroid/content/Intent;)V

    #@15
    .line 1916
    :cond_15
    :goto_15
    return-void

    #@16
    .line 1911
    :cond_16
    invoke-static {}, Landroid/webkit/WebViewClassic;->access$1400()Z

    #@19
    move-result v0

    #@1a
    const/4 v1, 0x1

    #@1b
    if-ne v0, v1, :cond_15

    #@1d
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    const-string v1, "com.lge.browser.BROWSER_PROXY_CHANGE"

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_15

    #@29
    .line 1912
    const-string/jumbo v0, "webview"

    #@2c
    const-string v1, "ProxyReceiver: com.lge.browser.BROWSER_PROXY_CHANGE"

    #@2e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 1913
    invoke-static {p2}, Landroid/webkit/WebViewClassic;->access$1500(Landroid/content/Intent;)V

    #@34
    goto :goto_15
.end method
