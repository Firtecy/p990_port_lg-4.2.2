.class Landroid/webkit/BrowserFrame$ConfigCallback;
.super Ljava/lang/Object;
.source "BrowserFrame.java"

# interfaces
.implements Landroid/content/ComponentCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/BrowserFrame;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConfigCallback"
.end annotation


# instance fields
.field private final mHandlers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method constructor <init>(Landroid/view/WindowManager;)V
    .registers 3
    .parameter "wm"

    #@0
    .prologue
    .line 145
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 141
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/BrowserFrame$ConfigCallback;->mHandlers:Ljava/util/ArrayList;

    #@a
    .line 146
    iput-object p1, p0, Landroid/webkit/BrowserFrame$ConfigCallback;->mWindowManager:Landroid/view/WindowManager;

    #@c
    .line 147
    return-void
.end method


# virtual methods
.method public declared-synchronized addHandler(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 154
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/BrowserFrame$ConfigCallback;->mHandlers:Ljava/util/ArrayList;

    #@3
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    #@b
    .line 155
    monitor-exit p0

    #@c
    return-void

    #@d
    .line 154
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0

    #@f
    throw v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 10
    .parameter "newConfig"

    #@0
    .prologue
    .line 158
    iget-object v6, p0, Landroid/webkit/BrowserFrame$ConfigCallback;->mHandlers:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v6

    #@6
    if-nez v6, :cond_9

    #@8
    .line 198
    :goto_8
    return-void

    #@9
    .line 161
    :cond_9
    iget-object v6, p0, Landroid/webkit/BrowserFrame$ConfigCallback;->mWindowManager:Landroid/view/WindowManager;

    #@b
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@e
    move-result-object v6

    #@f
    invoke-virtual {v6}, Landroid/view/Display;->getOrientation()I

    #@12
    move-result v3

    #@13
    .line 163
    .local v3, orientation:I
    packed-switch v3, :pswitch_data_72

    #@16
    .line 179
    :goto_16
    monitor-enter p0

    #@17
    .line 182
    :try_start_17
    new-instance v1, Ljava/util/ArrayList;

    #@19
    iget-object v6, p0, Landroid/webkit/BrowserFrame$ConfigCallback;->mHandlers:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@1e
    move-result v6

    #@1f
    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    #@22
    .line 184
    .local v1, handlersToRemove:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/ref/WeakReference;>;"
    iget-object v6, p0, Landroid/webkit/BrowserFrame$ConfigCallback;->mHandlers:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@27
    move-result-object v2

    #@28
    .local v2, i$:Ljava/util/Iterator;
    :goto_28
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2b
    move-result v6

    #@2c
    if-eqz v6, :cond_59

    #@2e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@31
    move-result-object v5

    #@32
    check-cast v5, Ljava/lang/ref/WeakReference;

    #@34
    .line 185
    .local v5, wh:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@37
    move-result-object v0

    #@38
    check-cast v0, Landroid/os/Handler;

    #@3a
    .line 186
    .local v0, h:Landroid/os/Handler;
    if-eqz v0, :cond_55

    #@3c
    .line 187
    const/16 v6, 0x3ea

    #@3e
    const/4 v7, 0x0

    #@3f
    invoke-virtual {v0, v6, v3, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@46
    goto :goto_28

    #@47
    .line 197
    .end local v0           #h:Landroid/os/Handler;
    .end local v1           #handlersToRemove:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/ref/WeakReference;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v5           #wh:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    :catchall_47
    move-exception v6

    #@48
    monitor-exit p0
    :try_end_49
    .catchall {:try_start_17 .. :try_end_49} :catchall_47

    #@49
    throw v6

    #@4a
    .line 165
    :pswitch_4a
    const/16 v3, 0x5a

    #@4c
    .line 166
    goto :goto_16

    #@4d
    .line 168
    :pswitch_4d
    const/16 v3, 0xb4

    #@4f
    .line 169
    goto :goto_16

    #@50
    .line 171
    :pswitch_50
    const/16 v3, -0x5a

    #@52
    .line 172
    goto :goto_16

    #@53
    .line 174
    :pswitch_53
    const/4 v3, 0x0

    #@54
    .line 175
    goto :goto_16

    #@55
    .line 190
    .restart local v0       #h:Landroid/os/Handler;
    .restart local v1       #handlersToRemove:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/ref/WeakReference;>;"
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v5       #wh:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    :cond_55
    :try_start_55
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@58
    goto :goto_28

    #@59
    .line 194
    .end local v0           #h:Landroid/os/Handler;
    .end local v5           #wh:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/os/Handler;>;"
    :cond_59
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5c
    move-result-object v2

    #@5d
    :goto_5d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@60
    move-result v6

    #@61
    if-eqz v6, :cond_6f

    #@63
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@66
    move-result-object v4

    #@67
    check-cast v4, Ljava/lang/ref/WeakReference;

    #@69
    .line 195
    .local v4, weak:Ljava/lang/ref/WeakReference;
    iget-object v6, p0, Landroid/webkit/BrowserFrame$ConfigCallback;->mHandlers:Ljava/util/ArrayList;

    #@6b
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@6e
    goto :goto_5d

    #@6f
    .line 197
    .end local v4           #weak:Ljava/lang/ref/WeakReference;
    :cond_6f
    monitor-exit p0
    :try_end_70
    .catchall {:try_start_55 .. :try_end_70} :catchall_47

    #@70
    goto :goto_8

    #@71
    .line 163
    nop

    #@72
    :pswitch_data_72
    .packed-switch 0x0
        :pswitch_53
        :pswitch_4a
        :pswitch_4d
        :pswitch_50
    .end packed-switch
.end method

.method public onLowMemory()V
    .registers 1

    #@0
    .prologue
    .line 200
    return-void
.end method
