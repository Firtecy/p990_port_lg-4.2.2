.class Landroid/webkit/ZoomControlExternal;
.super Ljava/lang/Object;
.source "ZoomControlExternal.java"

# interfaces
.implements Landroid/webkit/ZoomControlBase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final ZOOM_CONTROLS_TIMEOUT:J


# instance fields
.field private final mPrivateHandler:Landroid/os/Handler;

.field private final mWebView:Landroid/webkit/WebViewClassic;

.field private mZoomControlRunnable:Ljava/lang/Runnable;

.field private mZoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 31
    invoke-static {}, Landroid/view/ViewConfiguration;->getZoomControlsTimeout()J

    #@3
    move-result-wide v0

    #@4
    sput-wide v0, Landroid/webkit/ZoomControlExternal;->ZOOM_CONTROLS_TIMEOUT:J

    #@6
    return-void
.end method

.method public constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "webView"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 36
    new-instance v0, Landroid/os/Handler;

    #@5
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/ZoomControlExternal;->mPrivateHandler:Landroid/os/Handler;

    #@a
    .line 41
    iput-object p1, p0, Landroid/webkit/ZoomControlExternal;->mWebView:Landroid/webkit/WebViewClassic;

    #@c
    .line 42
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/ZoomControlExternal;)Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/webkit/ZoomControlExternal;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControlRunnable:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/webkit/ZoomControlExternal;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mPrivateHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$300()J
    .registers 2

    #@0
    .prologue
    .line 28
    sget-wide v0, Landroid/webkit/ZoomControlExternal;->ZOOM_CONTROLS_TIMEOUT:J

    #@2
    return-wide v0
.end method

.method static synthetic access$400(Landroid/webkit/ZoomControlExternal;)Landroid/webkit/WebViewClassic;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    return-object v0
.end method

.method private createZoomControls()Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;
    .registers 3

    #@0
    .prologue
    .line 96
    new-instance v0, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@2
    iget-object v1, p0, Landroid/webkit/ZoomControlExternal;->mWebView:Landroid/webkit/WebViewClassic;

    #@4
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@7
    move-result-object v1

    #@8
    invoke-direct {v0, v1}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;-><init>(Landroid/content/Context;)V

    #@b
    .line 97
    .local v0, zoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;
    new-instance v1, Landroid/webkit/ZoomControlExternal$2;

    #@d
    invoke-direct {v1, p0}, Landroid/webkit/ZoomControlExternal$2;-><init>(Landroid/webkit/ZoomControlExternal;)V

    #@10
    invoke-virtual {v0, v1}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->setOnZoomInClickListener(Landroid/view/View$OnClickListener;)V

    #@13
    .line 105
    new-instance v1, Landroid/webkit/ZoomControlExternal$3;

    #@15
    invoke-direct {v1, p0}, Landroid/webkit/ZoomControlExternal$3;-><init>(Landroid/webkit/ZoomControlExternal;)V

    #@18
    invoke-virtual {v0, v1}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->setOnZoomOutClickListener(Landroid/view/View$OnClickListener;)V

    #@1b
    .line 113
    return-object v0
.end method


# virtual methods
.method public getControls()Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;
    .registers 3

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@2
    if-nez v0, :cond_17

    #@4
    .line 69
    invoke-direct {p0}, Landroid/webkit/ZoomControlExternal;->createZoomControls()Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@a
    .line 76
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@c
    const/4 v1, 0x0

    #@d
    invoke-virtual {v0, v1}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->setVisibility(I)V

    #@10
    .line 77
    new-instance v0, Landroid/webkit/ZoomControlExternal$1;

    #@12
    invoke-direct {v0, p0}, Landroid/webkit/ZoomControlExternal$1;-><init>(Landroid/webkit/ZoomControlExternal;)V

    #@15
    iput-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControlRunnable:Ljava/lang/Runnable;

    #@17
    .line 92
    :cond_17
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@19
    return-object v0
.end method

.method public hide()V
    .registers 3

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControlRunnable:Ljava/lang/Runnable;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 54
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mPrivateHandler:Landroid/os/Handler;

    #@6
    iget-object v1, p0, Landroid/webkit/ZoomControlExternal;->mZoomControlRunnable:Ljava/lang/Runnable;

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@b
    .line 56
    :cond_b
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 57
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@11
    invoke-virtual {v0}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->hide()V

    #@14
    .line 59
    :cond_14
    return-void
.end method

.method public isVisible()Z
    .registers 2

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControls:Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@6
    invoke-virtual {v0}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->isShown()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public show()V
    .registers 5

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mZoomControlRunnable:Ljava/lang/Runnable;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 46
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mPrivateHandler:Landroid/os/Handler;

    #@6
    iget-object v1, p0, Landroid/webkit/ZoomControlExternal;->mZoomControlRunnable:Ljava/lang/Runnable;

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@b
    .line 48
    :cond_b
    invoke-virtual {p0}, Landroid/webkit/ZoomControlExternal;->getControls()Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@e
    move-result-object v0

    #@f
    const/4 v1, 0x1

    #@10
    invoke-virtual {v0, v1}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->show(Z)V

    #@13
    .line 49
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal;->mPrivateHandler:Landroid/os/Handler;

    #@15
    iget-object v1, p0, Landroid/webkit/ZoomControlExternal;->mZoomControlRunnable:Ljava/lang/Runnable;

    #@17
    sget-wide v2, Landroid/webkit/ZoomControlExternal;->ZOOM_CONTROLS_TIMEOUT:J

    #@19
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@1c
    .line 50
    return-void
.end method

.method public update()V
    .registers 1

    #@0
    .prologue
    .line 65
    return-void
.end method
