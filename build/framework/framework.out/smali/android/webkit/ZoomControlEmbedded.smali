.class Landroid/webkit/ZoomControlEmbedded;
.super Ljava/lang/Object;
.source "ZoomControlEmbedded.java"

# interfaces
.implements Landroid/webkit/ZoomControlBase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/ZoomControlEmbedded$1;,
        Landroid/webkit/ZoomControlEmbedded$ZoomListener;
    }
.end annotation


# instance fields
.field private final mWebView:Landroid/webkit/WebViewClassic;

.field private mZoomButtonsController:Landroid/widget/ZoomButtonsController;

.field private final mZoomManager:Landroid/webkit/ZoomManager;


# direct methods
.method public constructor <init>(Landroid/webkit/ZoomManager;Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "zoomManager"
    .parameter "webView"

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    iput-object p1, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomManager:Landroid/webkit/ZoomManager;

    #@5
    .line 35
    iput-object p2, p0, Landroid/webkit/ZoomControlEmbedded;->mWebView:Landroid/webkit/WebViewClassic;

    #@7
    .line 36
    return-void
.end method

.method static synthetic access$100(Landroid/webkit/ZoomControlEmbedded;)Landroid/webkit/WebViewClassic;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 25
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/webkit/ZoomControlEmbedded;)Landroid/widget/ZoomButtonsController;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 25
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@2
    return-object v0
.end method

.method private getControls()Landroid/widget/ZoomButtonsController;
    .registers 6

    #@0
    .prologue
    .line 84
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@2
    if-nez v2, :cond_31

    #@4
    .line 85
    new-instance v2, Landroid/widget/ZoomButtonsController;

    #@6
    iget-object v3, p0, Landroid/webkit/ZoomControlEmbedded;->mWebView:Landroid/webkit/WebViewClassic;

    #@8
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@b
    move-result-object v3

    #@c
    invoke-direct {v2, v3}, Landroid/widget/ZoomButtonsController;-><init>(Landroid/view/View;)V

    #@f
    iput-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@11
    .line 86
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@13
    new-instance v3, Landroid/webkit/ZoomControlEmbedded$ZoomListener;

    #@15
    const/4 v4, 0x0

    #@16
    invoke-direct {v3, p0, v4}, Landroid/webkit/ZoomControlEmbedded$ZoomListener;-><init>(Landroid/webkit/ZoomControlEmbedded;Landroid/webkit/ZoomControlEmbedded$1;)V

    #@19
    invoke-virtual {v2, v3}, Landroid/widget/ZoomButtonsController;->setOnZoomListener(Landroid/widget/ZoomButtonsController$OnZoomListener;)V

    #@1c
    .line 90
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@1e
    invoke-virtual {v2}, Landroid/widget/ZoomButtonsController;->getZoomControls()Landroid/view/View;

    #@21
    move-result-object v0

    #@22
    .line 91
    .local v0, controls:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@25
    move-result-object v1

    #@26
    .line 92
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    instance-of v2, v1, Landroid/widget/FrameLayout$LayoutParams;

    #@28
    if-eqz v2, :cond_31

    #@2a
    .line 93
    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    #@2c
    .end local v1           #params:Landroid/view/ViewGroup$LayoutParams;
    const v2, 0x800005

    #@2f
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@31
    .line 96
    .end local v0           #controls:Landroid/view/View;
    :cond_31
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@33
    return-object v2
.end method


# virtual methods
.method public hide()V
    .registers 3

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 58
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    #@a
    .line 60
    :cond_a
    return-void
.end method

.method public isVisible()Z
    .registers 2

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@6
    invoke-virtual {v0}, Landroid/widget/ZoomButtonsController;->isVisible()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public show()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 39
    invoke-direct {p0}, Landroid/webkit/ZoomControlEmbedded;->getControls()Landroid/widget/ZoomButtonsController;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {v2}, Landroid/widget/ZoomButtonsController;->isVisible()Z

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_49

    #@b
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomManager:Landroid/webkit/ZoomManager;

    #@d
    invoke-virtual {v2}, Landroid/webkit/ZoomManager;->isZoomScaleFixed()Z

    #@10
    move-result v2

    #@11
    if-nez v2, :cond_49

    #@13
    .line 41
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@15
    invoke-virtual {v2, v4}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    #@18
    .line 43
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomManager:Landroid/webkit/ZoomManager;

    #@1a
    invoke-virtual {v2}, Landroid/webkit/ZoomManager;->isDoubleTapEnabled()Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_49

    #@20
    .line 44
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mWebView:Landroid/webkit/WebViewClassic;

    #@22
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@25
    move-result-object v1

    #@26
    .line 45
    .local v1, settings:Landroid/webkit/WebSettingsClassic;
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getDoubleTapToastCount()I

    #@29
    move-result v0

    #@2a
    .line 46
    .local v0, count:I
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomManager:Landroid/webkit/ZoomManager;

    #@2c
    invoke-virtual {v2}, Landroid/webkit/ZoomManager;->isInZoomOverview()Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_49

    #@32
    if-lez v0, :cond_49

    #@34
    .line 47
    add-int/lit8 v0, v0, -0x1

    #@36
    invoke-virtual {v1, v0}, Landroid/webkit/WebSettingsClassic;->setDoubleTapToastCount(I)V

    #@39
    .line 48
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mWebView:Landroid/webkit/WebViewClassic;

    #@3b
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@3e
    move-result-object v2

    #@3f
    const v3, 0x104036d

    #@42
    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    #@49
    .line 54
    .end local v0           #count:I
    .end local v1           #settings:Landroid/webkit/WebSettingsClassic;
    :cond_49
    return-void
.end method

.method public update()V
    .registers 5

    #@0
    .prologue
    .line 67
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 81
    :goto_4
    return-void

    #@5
    .line 71
    :cond_5
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomManager:Landroid/webkit/ZoomManager;

    #@7
    invoke-virtual {v2}, Landroid/webkit/ZoomManager;->canZoomIn()Z

    #@a
    move-result v0

    #@b
    .line 72
    .local v0, canZoomIn:Z
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomManager:Landroid/webkit/ZoomManager;

    #@d
    invoke-virtual {v2}, Landroid/webkit/ZoomManager;->canZoomOut()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_2c

    #@13
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomManager:Landroid/webkit/ZoomManager;

    #@15
    invoke-virtual {v2}, Landroid/webkit/ZoomManager;->isInZoomOverview()Z

    #@18
    move-result v2

    #@19
    if-nez v2, :cond_2c

    #@1b
    const/4 v1, 0x1

    #@1c
    .line 73
    .local v1, canZoomOut:Z
    :goto_1c
    if-nez v0, :cond_2e

    #@1e
    if-nez v1, :cond_2e

    #@20
    .line 75
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@22
    invoke-virtual {v2}, Landroid/widget/ZoomButtonsController;->getZoomControls()Landroid/view/View;

    #@25
    move-result-object v2

    #@26
    const/16 v3, 0x8

    #@28
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    #@2b
    goto :goto_4

    #@2c
    .line 72
    .end local v1           #canZoomOut:Z
    :cond_2c
    const/4 v1, 0x0

    #@2d
    goto :goto_1c

    #@2e
    .line 78
    .restart local v1       #canZoomOut:Z
    :cond_2e
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@30
    invoke-virtual {v2, v0}, Landroid/widget/ZoomButtonsController;->setZoomInEnabled(Z)V

    #@33
    .line 79
    iget-object v2, p0, Landroid/webkit/ZoomControlEmbedded;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    #@35
    invoke-virtual {v2, v1}, Landroid/widget/ZoomButtonsController;->setZoomOutEnabled(Z)V

    #@38
    goto :goto_4
.end method
