.class Landroid/webkit/ViewManager$ChildView;
.super Ljava/lang/Object;
.source "ViewManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/ViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ChildView"
.end annotation


# instance fields
.field height:I

.field mView:Landroid/view/View;

.field final synthetic this$0:Landroid/webkit/ViewManager;

.field width:I

.field x:I

.field y:I


# direct methods
.method constructor <init>(Landroid/webkit/ViewManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 46
    iput-object p1, p0, Landroid/webkit/ViewManager$ChildView;->this$0:Landroid/webkit/ViewManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 47
    return-void
.end method

.method static synthetic access$100(Landroid/webkit/ViewManager$ChildView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 39
    invoke-direct {p0}, Landroid/webkit/ViewManager$ChildView;->attachViewOnUIThread()V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/webkit/ViewManager$ChildView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 39
    invoke-direct {p0}, Landroid/webkit/ViewManager$ChildView;->removeViewOnUIThread()V

    #@3
    return-void
.end method

.method private attachViewOnUIThread()V
    .registers 3

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/webkit/ViewManager$ChildView;->this$0:Landroid/webkit/ViewManager;

    #@2
    invoke-static {v0}, Landroid/webkit/ViewManager;->access$200(Landroid/webkit/ViewManager;)Landroid/webkit/WebViewClassic;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@9
    move-result-object v0

    #@a
    iget-object v1, p0, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@c
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->addView(Landroid/view/View;)V

    #@f
    .line 79
    iget-object v0, p0, Landroid/webkit/ViewManager$ChildView;->this$0:Landroid/webkit/ViewManager;

    #@11
    invoke-static {v0}, Landroid/webkit/ViewManager;->access$300(Landroid/webkit/ViewManager;)Ljava/util/ArrayList;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@18
    .line 80
    iget-object v0, p0, Landroid/webkit/ViewManager$ChildView;->this$0:Landroid/webkit/ViewManager;

    #@1a
    invoke-static {v0}, Landroid/webkit/ViewManager;->access$400(Landroid/webkit/ViewManager;)Z

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_27

    #@20
    .line 81
    iget-object v0, p0, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@22
    const/16 v1, 0x8

    #@24
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@27
    .line 83
    :cond_27
    return-void
.end method

.method private removeViewOnUIThread()V
    .registers 3

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Landroid/webkit/ViewManager$ChildView;->this$0:Landroid/webkit/ViewManager;

    #@2
    invoke-static {v0}, Landroid/webkit/ViewManager;->access$200(Landroid/webkit/ViewManager;)Landroid/webkit/WebViewClassic;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@9
    move-result-object v0

    #@a
    iget-object v1, p0, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@c
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->removeView(Landroid/view/View;)V

    #@f
    .line 98
    iget-object v0, p0, Landroid/webkit/ViewManager$ChildView;->this$0:Landroid/webkit/ViewManager;

    #@11
    invoke-static {v0}, Landroid/webkit/ViewManager;->access$300(Landroid/webkit/ViewManager;)Ljava/util/ArrayList;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@18
    .line 99
    return-void
.end method


# virtual methods
.method attachView(IIII)V
    .registers 7
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 75
    :goto_4
    return-void

    #@5
    .line 60
    :cond_5
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/webkit/ViewManager$ChildView;->setBounds(IIII)V

    #@8
    .line 62
    iget-object v0, p0, Landroid/webkit/ViewManager$ChildView;->this$0:Landroid/webkit/ViewManager;

    #@a
    invoke-static {v0}, Landroid/webkit/ViewManager;->access$200(Landroid/webkit/ViewManager;)Landroid/webkit/WebViewClassic;

    #@d
    move-result-object v0

    #@e
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@10
    new-instance v1, Landroid/webkit/ViewManager$ChildView$1;

    #@12
    invoke-direct {v1, p0}, Landroid/webkit/ViewManager$ChildView$1;-><init>(Landroid/webkit/ViewManager$ChildView;)V

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@18
    goto :goto_4
.end method

.method removeView()V
    .registers 3

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 94
    :goto_4
    return-void

    #@5
    .line 89
    :cond_5
    iget-object v0, p0, Landroid/webkit/ViewManager$ChildView;->this$0:Landroid/webkit/ViewManager;

    #@7
    invoke-static {v0}, Landroid/webkit/ViewManager;->access$200(Landroid/webkit/ViewManager;)Landroid/webkit/WebViewClassic;

    #@a
    move-result-object v0

    #@b
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@d
    new-instance v1, Landroid/webkit/ViewManager$ChildView$2;

    #@f
    invoke-direct {v1, p0}, Landroid/webkit/ViewManager$ChildView$2;-><init>(Landroid/webkit/ViewManager$ChildView;)V

    #@12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@15
    goto :goto_4
.end method

.method setBounds(IIII)V
    .registers 5
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 50
    iput p1, p0, Landroid/webkit/ViewManager$ChildView;->x:I

    #@2
    .line 51
    iput p2, p0, Landroid/webkit/ViewManager$ChildView;->y:I

    #@4
    .line 52
    iput p3, p0, Landroid/webkit/ViewManager$ChildView;->width:I

    #@6
    .line 53
    iput p4, p0, Landroid/webkit/ViewManager$ChildView;->height:I

    #@8
    .line 54
    return-void
.end method
