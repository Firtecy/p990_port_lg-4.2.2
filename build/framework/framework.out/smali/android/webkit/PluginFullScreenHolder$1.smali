.class Landroid/webkit/PluginFullScreenHolder$1;
.super Ljava/lang/Object;
.source "PluginFullScreenHolder.java"

# interfaces
.implements Landroid/webkit/WebChromeClient$CustomViewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/PluginFullScreenHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/PluginFullScreenHolder;


# direct methods
.method constructor <init>(Landroid/webkit/PluginFullScreenHolder;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 134
    iput-object p1, p0, Landroid/webkit/PluginFullScreenHolder$1;->this$0:Landroid/webkit/PluginFullScreenHolder;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onCustomViewHidden()V
    .registers 5

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Landroid/webkit/PluginFullScreenHolder$1;->this$0:Landroid/webkit/PluginFullScreenHolder;

    #@2
    invoke-static {v0}, Landroid/webkit/PluginFullScreenHolder;->access$000(Landroid/webkit/PluginFullScreenHolder;)Landroid/webkit/WebViewClassic;

    #@5
    move-result-object v0

    #@6
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@8
    const/16 v1, 0x79

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@11
    .line 140
    iget-object v0, p0, Landroid/webkit/PluginFullScreenHolder$1;->this$0:Landroid/webkit/PluginFullScreenHolder;

    #@13
    invoke-static {v0}, Landroid/webkit/PluginFullScreenHolder;->access$000(Landroid/webkit/PluginFullScreenHolder;)Landroid/webkit/WebViewClassic;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getWebViewCore()Landroid/webkit/WebViewCore;

    #@1a
    move-result-object v0

    #@1b
    const/16 v1, 0xb6

    #@1d
    iget-object v2, p0, Landroid/webkit/PluginFullScreenHolder$1;->this$0:Landroid/webkit/PluginFullScreenHolder;

    #@1f
    invoke-static {v2}, Landroid/webkit/PluginFullScreenHolder;->access$100(Landroid/webkit/PluginFullScreenHolder;)I

    #@22
    move-result v2

    #@23
    const/4 v3, 0x0

    #@24
    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@27
    .line 143
    invoke-static {}, Landroid/webkit/PluginFullScreenHolder;->access$300()Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;

    #@2a
    move-result-object v0

    #@2b
    iget-object v1, p0, Landroid/webkit/PluginFullScreenHolder$1;->this$0:Landroid/webkit/PluginFullScreenHolder;

    #@2d
    invoke-static {v1}, Landroid/webkit/PluginFullScreenHolder;->access$200(Landroid/webkit/PluginFullScreenHolder;)Landroid/view/View;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v0, v1}, Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;->removeView(Landroid/view/View;)V

    #@34
    .line 144
    const/4 v0, 0x0

    #@35
    invoke-static {v0}, Landroid/webkit/PluginFullScreenHolder;->access$302(Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;)Landroid/webkit/PluginFullScreenHolder$CustomFrameLayout;

    #@38
    .line 147
    iget-object v0, p0, Landroid/webkit/PluginFullScreenHolder$1;->this$0:Landroid/webkit/PluginFullScreenHolder;

    #@3a
    invoke-static {v0}, Landroid/webkit/PluginFullScreenHolder;->access$000(Landroid/webkit/PluginFullScreenHolder;)Landroid/webkit/WebViewClassic;

    #@3d
    move-result-object v0

    #@3e
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getViewManager()Landroid/webkit/ViewManager;

    #@41
    move-result-object v0

    #@42
    invoke-virtual {v0}, Landroid/webkit/ViewManager;->showAll()V

    #@45
    .line 148
    return-void
.end method
