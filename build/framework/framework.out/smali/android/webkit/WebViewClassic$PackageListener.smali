.class Landroid/webkit/WebViewClassic$PackageListener;
.super Landroid/content/BroadcastReceiver;
.source "WebViewClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PackageListener"
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1997
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/WebViewClassic$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1997
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$PackageListener;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 2000
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 2001
    .local v0, action:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@7
    move-result-object v4

    #@8
    invoke-virtual {v4}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 2002
    .local v1, packageName:Ljava/lang/String;
    const-string v4, "android.intent.extra.REPLACING"

    #@e
    const/4 v5, 0x0

    #@f
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@12
    move-result v3

    #@13
    .line 2003
    .local v3, replacing:Z
    const-string v4, "android.intent.action.PACKAGE_REMOVED"

    #@15
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_1e

    #@1b
    if-eqz v3, :cond_1e

    #@1d
    .line 2020
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 2008
    :cond_1e
    invoke-static {}, Landroid/webkit/WebViewClassic;->access$1700()Ljava/util/Set;

    #@21
    move-result-object v4

    #@22
    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_35

    #@28
    .line 2009
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    #@2a
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_49

    #@30
    .line 2010
    const/16 v4, 0xb9

    #@32
    invoke-static {v4, v1}, Landroid/webkit/WebViewCore;->sendStaticMessage(ILjava/lang/Object;)V

    #@35
    .line 2016
    :cond_35
    :goto_35
    invoke-static {p1}, Landroid/webkit/PluginManager;->getInstance(Landroid/content/Context;)Landroid/webkit/PluginManager;

    #@38
    move-result-object v2

    #@39
    .line 2017
    .local v2, pm:Landroid/webkit/PluginManager;
    invoke-virtual {v2, v1}, Landroid/webkit/PluginManager;->containsPluginPermissionAndSignatures(Ljava/lang/String;)Z

    #@3c
    move-result v4

    #@3d
    if-eqz v4, :cond_1d

    #@3f
    .line 2018
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    #@41
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v4

    #@45
    invoke-virtual {v2, v4}, Landroid/webkit/PluginManager;->refreshPlugins(Z)V

    #@48
    goto :goto_1d

    #@49
    .line 2012
    .end local v2           #pm:Landroid/webkit/PluginManager;
    :cond_49
    const/16 v4, 0xba

    #@4b
    invoke-static {v4, v1}, Landroid/webkit/WebViewCore;->sendStaticMessage(ILjava/lang/Object;)V

    #@4e
    goto :goto_35
.end method
