.class public Landroid/webkit/L10nUtils;
.super Ljava/lang/Object;
.source "L10nUtils.java"


# static fields
.field private static mApplicationContext:Landroid/content/Context;

.field private static mIdsArray:[I

.field private static mStrings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 32
    const/16 v0, 0x39

    #@2
    new-array v0, v0, [I

    #@4
    fill-array-data v0, :array_a

    #@7
    sput-object v0, Landroid/webkit/L10nUtils;->mIdsArray:[I

    #@9
    return-void

    #@a
    :array_a
    .array-data 0x4
        0x70t 0x3t 0x4t 0x1t
        0x71t 0x3t 0x4t 0x1t
        0x72t 0x3t 0x4t 0x1t
        0x73t 0x3t 0x4t 0x1t
        0x74t 0x3t 0x4t 0x1t
        0x75t 0x3t 0x4t 0x1t
        0x76t 0x3t 0x4t 0x1t
        0x77t 0x3t 0x4t 0x1t
        0x78t 0x3t 0x4t 0x1t
        0x79t 0x3t 0x4t 0x1t
        0x7at 0x3t 0x4t 0x1t
        0x7bt 0x3t 0x4t 0x1t
        0x7ct 0x3t 0x4t 0x1t
        0x7dt 0x3t 0x4t 0x1t
        0x7et 0x3t 0x4t 0x1t
        0x7ft 0x3t 0x4t 0x1t
        0x80t 0x3t 0x4t 0x1t
        0x81t 0x3t 0x4t 0x1t
        0x82t 0x3t 0x4t 0x1t
        0x83t 0x3t 0x4t 0x1t
        0x84t 0x3t 0x4t 0x1t
        0x85t 0x3t 0x4t 0x1t
        0x86t 0x3t 0x4t 0x1t
        0x87t 0x3t 0x4t 0x1t
        0x88t 0x3t 0x4t 0x1t
        0x89t 0x3t 0x4t 0x1t
        0x8at 0x3t 0x4t 0x1t
        0x8bt 0x3t 0x4t 0x1t
        0x8ct 0x3t 0x4t 0x1t
        0x8dt 0x3t 0x4t 0x1t
        0x8et 0x3t 0x4t 0x1t
        0x8ft 0x3t 0x4t 0x1t
        0x90t 0x3t 0x4t 0x1t
        0x91t 0x3t 0x4t 0x1t
        0x92t 0x3t 0x4t 0x1t
        0x93t 0x3t 0x4t 0x1t
        0x94t 0x3t 0x4t 0x1t
        0x95t 0x3t 0x4t 0x1t
        0x96t 0x3t 0x4t 0x1t
        0x97t 0x3t 0x4t 0x1t
        0x98t 0x3t 0x4t 0x1t
        0x99t 0x3t 0x4t 0x1t
        0x9at 0x3t 0x4t 0x1t
        0x9bt 0x3t 0x4t 0x1t
        0x9ct 0x3t 0x4t 0x1t
        0x9dt 0x3t 0x4t 0x1t
        0x9et 0x3t 0x4t 0x1t
        0x9ft 0x3t 0x4t 0x1t
        0xa0t 0x3t 0x4t 0x1t
        0xa1t 0x3t 0x4t 0x1t
        0xa2t 0x3t 0x4t 0x1t
        0xa3t 0x3t 0x4t 0x1t
        0xa4t 0x3t 0x4t 0x1t
        0xa5t 0x3t 0x4t 0x1t
        0xa6t 0x3t 0x4t 0x1t
        0xa7t 0x3t 0x4t 0x1t
        0xa8t 0x3t 0x4t 0x1t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getLocalisedString(I)Ljava/lang/String;
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 110
    sget-object v2, Landroid/webkit/L10nUtils;->mStrings:Ljava/util/Map;

    #@2
    if-nez v2, :cond_9

    #@4
    .line 113
    invoke-static {p0}, Landroid/webkit/L10nUtils;->loadString(I)Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    .line 118
    :goto_8
    return-object v2

    #@9
    .line 116
    :cond_9
    sget-object v2, Landroid/webkit/L10nUtils;->mStrings:Ljava/util/Map;

    #@b
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v3

    #@f
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Ljava/lang/ref/SoftReference;

    #@15
    .line 117
    .local v1, ref:Ljava/lang/ref/SoftReference;,"Ljava/lang/ref/SoftReference<Ljava/lang/String;>;"
    if-eqz v1, :cond_1d

    #@17
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    if-nez v2, :cond_25

    #@1d
    :cond_1d
    const/4 v0, 0x1

    #@1e
    .line 118
    .local v0, needToLoad:Z
    :goto_1e
    if-eqz v0, :cond_27

    #@20
    invoke-static {p0}, Landroid/webkit/L10nUtils;->loadString(I)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    goto :goto_8

    #@25
    .line 117
    .end local v0           #needToLoad:Z
    :cond_25
    const/4 v0, 0x0

    #@26
    goto :goto_1e

    #@27
    .line 118
    .restart local v0       #needToLoad:Z
    :cond_27
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    #@2a
    move-result-object v2

    #@2b
    check-cast v2, Ljava/lang/String;

    #@2d
    goto :goto_8
.end method

.method private static loadString(I)Ljava/lang/String;
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 100
    sget-object v1, Landroid/webkit/L10nUtils;->mStrings:Ljava/util/Map;

    #@2
    if-nez v1, :cond_e

    #@4
    .line 101
    new-instance v1, Ljava/util/HashMap;

    #@6
    sget-object v2, Landroid/webkit/L10nUtils;->mIdsArray:[I

    #@8
    array-length v2, v2

    #@9
    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    #@c
    sput-object v1, Landroid/webkit/L10nUtils;->mStrings:Ljava/util/Map;

    #@e
    .line 104
    :cond_e
    sget-object v1, Landroid/webkit/L10nUtils;->mApplicationContext:Landroid/content/Context;

    #@10
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@13
    move-result-object v1

    #@14
    sget-object v2, Landroid/webkit/L10nUtils;->mIdsArray:[I

    #@16
    aget v2, v2, p0

    #@18
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 105
    .local v0, localisedString:Ljava/lang/String;
    sget-object v1, Landroid/webkit/L10nUtils;->mStrings:Ljava/util/Map;

    #@1e
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v2

    #@22
    new-instance v3, Ljava/lang/ref/SoftReference;

    #@24
    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    #@27
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    .line 106
    return-object v0
.end method

.method public static setApplicationContext(Landroid/content/Context;)V
    .registers 2
    .parameter "applicationContext"

    #@0
    .prologue
    .line 96
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    sput-object v0, Landroid/webkit/L10nUtils;->mApplicationContext:Landroid/content/Context;

    #@6
    .line 97
    return-void
.end method
