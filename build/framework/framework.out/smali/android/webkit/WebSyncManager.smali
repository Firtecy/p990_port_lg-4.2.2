.class abstract Landroid/webkit/WebSyncManager;
.super Ljava/lang/Object;
.source "WebSyncManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebSyncManager$1;,
        Landroid/webkit/WebSyncManager$SyncHandler;
    }
.end annotation


# static fields
.field protected static final LOGTAG:Ljava/lang/String; = "websync"

.field private static SYNC_LATER_INTERVAL:I = 0x0

.field private static final SYNC_MESSAGE:I = 0x65

.field private static SYNC_NOW_INTERVAL:I


# instance fields
.field protected mDataBase:Landroid/webkit/WebViewDatabase;

.field protected mHandler:Landroid/os/Handler;

.field private mStartSyncRefCount:I

.field private mSyncThread:Ljava/lang/Thread;

.field private mThreadName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 30
    const/16 v0, 0x64

    #@2
    sput v0, Landroid/webkit/WebSyncManager;->SYNC_NOW_INTERVAL:I

    #@4
    .line 32
    const v0, 0x493e0

    #@7
    sput v0, Landroid/webkit/WebSyncManager;->SYNC_LATER_INTERVAL:I

    #@9
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "name"

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    iput-object p2, p0, Landroid/webkit/WebSyncManager;->mThreadName:Ljava/lang/String;

    #@5
    .line 64
    if-eqz p1, :cond_21

    #@7
    .line 65
    invoke-static {p1}, Landroid/webkit/WebViewDatabase;->getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabase;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/webkit/WebSyncManager;->mDataBase:Landroid/webkit/WebViewDatabase;

    #@d
    .line 66
    new-instance v0, Ljava/lang/Thread;

    #@f
    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@12
    iput-object v0, p0, Landroid/webkit/WebSyncManager;->mSyncThread:Ljava/lang/Thread;

    #@14
    .line 67
    iget-object v0, p0, Landroid/webkit/WebSyncManager;->mSyncThread:Ljava/lang/Thread;

    #@16
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mThreadName:Ljava/lang/String;

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    #@1b
    .line 68
    iget-object v0, p0, Landroid/webkit/WebSyncManager;->mSyncThread:Ljava/lang/Thread;

    #@1d
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@20
    .line 73
    return-void

    #@21
    .line 70
    :cond_21
    new-instance v0, Ljava/lang/IllegalStateException;

    #@23
    const-string v1, "WebSyncManager can\'t be created without context"

    #@25
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0
.end method

.method static synthetic access$000()I
    .registers 1

    #@0
    .prologue
    .line 26
    sget v0, Landroid/webkit/WebSyncManager;->SYNC_LATER_INTERVAL:I

    #@2
    return v0
.end method


# virtual methods
.method protected clone()Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 76
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    #@2
    const-string v1, "doesn\'t implement Cloneable"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method protected onSyncInit()V
    .registers 1

    #@0
    .prologue
    .line 158
    return-void
.end method

.method public resetSync()V
    .registers 5

    #@0
    .prologue
    const/16 v2, 0x65

    #@2
    .line 115
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@4
    if-nez v1, :cond_7

    #@6
    .line 121
    :goto_6
    return-void

    #@7
    .line 118
    :cond_7
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@c
    .line 119
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 120
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@14
    sget v2, Landroid/webkit/WebSyncManager;->SYNC_LATER_INTERVAL:I

    #@16
    int-to-long v2, v2

    #@17
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1a
    goto :goto_6
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 81
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@3
    .line 82
    new-instance v1, Landroid/webkit/WebSyncManager$SyncHandler;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-direct {v1, p0, v2}, Landroid/webkit/WebSyncManager$SyncHandler;-><init>(Landroid/webkit/WebSyncManager;Landroid/webkit/WebSyncManager$1;)V

    #@9
    iput-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@b
    .line 83
    invoke-virtual {p0}, Landroid/webkit/WebSyncManager;->onSyncInit()V

    #@e
    .line 85
    const/16 v1, 0xa

    #@10
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    #@13
    .line 87
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@15
    const/16 v2, 0x65

    #@17
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1a
    move-result-object v0

    #@1b
    .line 88
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@1d
    sget v2, Landroid/webkit/WebSyncManager;->SYNC_LATER_INTERVAL:I

    #@1f
    int-to-long v2, v2

    #@20
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@23
    .line 90
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@26
    .line 91
    return-void
.end method

.method public startSync()V
    .registers 5

    #@0
    .prologue
    .line 131
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 138
    :cond_4
    :goto_4
    return-void

    #@5
    .line 134
    :cond_5
    iget v1, p0, Landroid/webkit/WebSyncManager;->mStartSyncRefCount:I

    #@7
    add-int/lit8 v1, v1, 0x1

    #@9
    iput v1, p0, Landroid/webkit/WebSyncManager;->mStartSyncRefCount:I

    #@b
    const/4 v2, 0x1

    #@c
    if-ne v1, v2, :cond_4

    #@e
    .line 135
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@10
    const/16 v2, 0x65

    #@12
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@15
    move-result-object v0

    #@16
    .line 136
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@18
    sget v2, Landroid/webkit/WebSyncManager;->SYNC_LATER_INTERVAL:I

    #@1a
    int-to-long v2, v2

    #@1b
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1e
    goto :goto_4
.end method

.method public stopSync()V
    .registers 3

    #@0
    .prologue
    .line 149
    iget-object v0, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 155
    :cond_4
    :goto_4
    return-void

    #@5
    .line 152
    :cond_5
    iget v0, p0, Landroid/webkit/WebSyncManager;->mStartSyncRefCount:I

    #@7
    add-int/lit8 v0, v0, -0x1

    #@9
    iput v0, p0, Landroid/webkit/WebSyncManager;->mStartSyncRefCount:I

    #@b
    if-nez v0, :cond_4

    #@d
    .line 153
    iget-object v0, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@f
    const/16 v1, 0x65

    #@11
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@14
    goto :goto_4
.end method

.method public sync()V
    .registers 5

    #@0
    .prologue
    const/16 v2, 0x65

    #@2
    .line 100
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@4
    if-nez v1, :cond_7

    #@6
    .line 106
    :goto_6
    return-void

    #@7
    .line 103
    :cond_7
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@c
    .line 104
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 105
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/WebSyncManager;->mHandler:Landroid/os/Handler;

    #@14
    sget v2, Landroid/webkit/WebSyncManager;->SYNC_NOW_INTERVAL:I

    #@16
    int-to-long v2, v2

    #@17
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1a
    goto :goto_6
.end method

.method abstract syncFromRamToFlash()V
.end method
