.class public Landroid/webkit/WebHistoryItem;
.super Ljava/lang/Object;
.source "WebHistoryItem.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    return-void
.end method


# virtual methods
.method protected declared-synchronized clone()Landroid/webkit/WebHistoryItem;
    .registers 2

    #@0
    .prologue
    .line 94
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 27
    invoke-virtual {p0}, Landroid/webkit/WebHistoryItem;->clone()Landroid/webkit/WebHistoryItem;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getFavicon()Landroid/graphics/Bitmap;
    .registers 2

    #@0
    .prologue
    .line 87
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getId()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 45
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getOriginalUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 67
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getTitle()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 77
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 57
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method
