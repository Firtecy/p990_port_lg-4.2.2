.class Landroid/webkit/AccessibilityInjectorFallback;
.super Ljava/lang/Object;
.source "AccessibilityInjectorFallback.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;
    }
.end annotation


# static fields
.field private static final ACTION_PERFORM_AXIS_TRANSITION:I = 0x3

.field private static final ACTION_SET_CURRENT_AXIS:I = 0x0

.field private static final ACTION_TRAVERSE_CURRENT_AXIS:I = 0x1

.field private static final ACTION_TRAVERSE_DEFAULT_WEB_VIEW_BEHAVIOR_AXIS:I = 0x4

.field private static final ACTION_TRAVERSE_GIVEN_AXIS:I = 0x2

.field private static final DEBUG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "AccessibilityInjector"

.field private static final NAVIGATION_AXIS_CHARACTER:I = 0x0

.field private static final NAVIGATION_AXIS_DEFAULT_WEB_VIEW_BEHAVIOR:I = 0x7

.field private static final NAVIGATION_AXIS_DOCUMENT:I = 0x6

.field private static final NAVIGATION_AXIS_HEADING:I = 0x3

.field private static final NAVIGATION_AXIS_PARENT_FIRST_CHILD:I = 0x5

.field private static final NAVIGATION_AXIS_SENTENCE:I = 0x2

.field private static final NAVIGATION_AXIS_SIBLING:I = 0x5

.field private static final NAVIGATION_AXIS_WORD:I = 0x1

.field private static final NAVIGATION_DIRECTION_BACKWARD:I = 0x0

.field private static final NAVIGATION_DIRECTION_FORWARD:I = 0x1

.field private static sBindings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCurrentAxis:I

.field private mIsLastSelectionStringNull:Z

.field private mLastDirection:I

.field private mLastDownEventHandled:Z

.field private final mScheduledEventStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Landroid/view/accessibility/AccessibilityEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mWebView:Landroid/webkit/WebViewClassic;

.field private final mWebViewInternal:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 95
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    sput-object v0, Landroid/webkit/AccessibilityInjectorFallback;->sBindings:Ljava/util/ArrayList;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "webView"

    #@0
    .prologue
    .line 122
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 103
    new-instance v0, Ljava/util/Stack;

    #@5
    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/AccessibilityInjectorFallback;->mScheduledEventStack:Ljava/util/Stack;

    #@a
    .line 106
    const/4 v0, 0x2

    #@b
    iput v0, p0, Landroid/webkit/AccessibilityInjectorFallback;->mCurrentAxis:I

    #@d
    .line 123
    iput-object p1, p0, Landroid/webkit/AccessibilityInjectorFallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@f
    .line 124
    iget-object v0, p0, Landroid/webkit/AccessibilityInjectorFallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@11
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Landroid/webkit/AccessibilityInjectorFallback;->mWebViewInternal:Landroid/webkit/WebView;

    #@17
    .line 125
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjectorFallback;->ensureWebContentKeyBindings()V

    #@1a
    .line 126
    return-void
.end method

.method private ensureWebContentKeyBindings()V
    .registers 15

    #@0
    .prologue
    .line 424
    sget-object v11, Landroid/webkit/AccessibilityInjectorFallback;->sBindings:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v11

    #@6
    if-lez v11, :cond_9

    #@8
    .line 459
    :cond_8
    return-void

    #@9
    .line 428
    :cond_9
    iget-object v11, p0, Landroid/webkit/AccessibilityInjectorFallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@b
    invoke-virtual {v11}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@e
    move-result-object v11

    #@f
    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v11

    #@13
    const-string v12, "accessibility_web_content_key_bindings"

    #@15
    invoke-static {v11, v12}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v10

    #@19
    .line 432
    .local v10, webContentKeyBindingsString:Ljava/lang/String;
    new-instance v9, Landroid/text/TextUtils$SimpleStringSplitter;

    #@1b
    const/16 v11, 0x3b

    #@1d
    invoke-direct {v9, v11}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@20
    .line 433
    .local v9, semiColonSplitter:Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v9, v10}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    #@23
    .line 435
    :goto_23
    invoke-virtual {v9}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    #@26
    move-result v11

    #@27
    if-eqz v11, :cond_8

    #@29
    .line 436
    invoke-virtual {v9}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    .line 437
    .local v2, bindingString:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@30
    move-result v11

    #@31
    if-eqz v11, :cond_4c

    #@33
    .line 438
    const-string v11, "AccessibilityInjector"

    #@35
    new-instance v12, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v13, "Disregarding malformed Web content key binding: "

    #@3c
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v12

    #@40
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v12

    #@44
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v12

    #@48
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    goto :goto_23

    #@4c
    .line 442
    :cond_4c
    const-string v11, "="

    #@4e
    invoke-virtual {v2, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@51
    move-result-object v7

    #@52
    .line 443
    .local v7, keyValueArray:[Ljava/lang/String;
    array-length v11, v7

    #@53
    const/4 v12, 0x2

    #@54
    if-eq v11, v12, :cond_6f

    #@56
    .line 444
    const-string v11, "AccessibilityInjector"

    #@58
    new-instance v12, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v13, "Disregarding malformed Web content key binding: "

    #@5f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v12

    #@63
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v12

    #@67
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v12

    #@6b
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    goto :goto_23

    #@6f
    .line 448
    :cond_6f
    const/4 v11, 0x0

    #@70
    :try_start_70
    aget-object v11, v7, v11

    #@72
    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@75
    move-result-object v11

    #@76
    invoke-static {v11}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    #@79
    move-result-object v11

    #@7a
    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    #@7d
    move-result-wide v5

    #@7e
    .line 449
    .local v5, keyCodeAndModifiers:J
    const/4 v11, 0x1

    #@7f
    aget-object v11, v7, v11

    #@81
    const-string v12, ":"

    #@83
    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@86
    move-result-object v0

    #@87
    .line 450
    .local v0, actionStrings:[Ljava/lang/String;
    array-length v11, v0

    #@88
    new-array v1, v11, [I

    #@8a
    .line 451
    .local v1, actions:[I
    const/4 v4, 0x0

    #@8b
    .local v4, i:I
    array-length v3, v1

    #@8c
    .local v3, count:I
    :goto_8c
    if-ge v4, v3, :cond_a1

    #@8e
    .line 452
    aget-object v11, v0, v4

    #@90
    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@93
    move-result-object v11

    #@94
    invoke-static {v11}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@97
    move-result-object v11

    #@98
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@9b
    move-result v11

    #@9c
    aput v11, v1, v4

    #@9e
    .line 451
    add-int/lit8 v4, v4, 0x1

    #@a0
    goto :goto_8c

    #@a1
    .line 454
    :cond_a1
    sget-object v11, Landroid/webkit/AccessibilityInjectorFallback;->sBindings:Ljava/util/ArrayList;

    #@a3
    new-instance v12, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;

    #@a5
    invoke-direct {v12, v5, v6, v1}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;-><init>(J[I)V

    #@a8
    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_ab
    .catch Ljava/lang/NumberFormatException; {:try_start_70 .. :try_end_ab} :catch_ad

    #@ab
    goto/16 :goto_23

    #@ad
    .line 455
    .end local v0           #actionStrings:[Ljava/lang/String;
    .end local v1           #actions:[I
    .end local v3           #count:I
    .end local v4           #i:I
    .end local v5           #keyCodeAndModifiers:J
    :catch_ad
    move-exception v8

    #@ae
    .line 456
    .local v8, nfe:Ljava/lang/NumberFormatException;
    const-string v11, "AccessibilityInjector"

    #@b0
    new-instance v12, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v13, "Disregarding malformed key binding: "

    #@b7
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v12

    #@bb
    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v12

    #@bf
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v12

    #@c3
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    goto/16 :goto_23
.end method

.method private static getAxisForGranularity(I)I
    .registers 2
    .parameter "granularity"

    #@0
    .prologue
    const/4 v0, 0x2

    #@1
    .line 321
    sparse-switch p0, :sswitch_data_c

    #@4
    .line 334
    const/4 v0, -0x1

    #@5
    :goto_5
    :sswitch_5
    return v0

    #@6
    .line 323
    :sswitch_6
    const/4 v0, 0x0

    #@7
    goto :goto_5

    #@8
    .line 325
    :sswitch_8
    const/4 v0, 0x1

    #@9
    goto :goto_5

    #@a
    .line 332
    :sswitch_a
    const/4 v0, 0x6

    #@b
    goto :goto_5

    #@c
    .line 321
    :sswitch_data_c
    .sparse-switch
        0x1 -> :sswitch_6
        0x2 -> :sswitch_8
        0x4 -> :sswitch_5
        0x8 -> :sswitch_5
        0x10 -> :sswitch_a
    .end sparse-switch
.end method

.method private static getDirectionForAction(I)I
    .registers 2
    .parameter "action"

    #@0
    .prologue
    .line 301
    sparse-switch p0, :sswitch_data_a

    #@3
    .line 309
    const/4 v0, -0x1

    #@4
    :goto_4
    return v0

    #@5
    .line 304
    :sswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 307
    :sswitch_7
    const/4 v0, 0x0

    #@8
    goto :goto_4

    #@9
    .line 301
    nop

    #@a
    :sswitch_data_a
    .sparse-switch
        0x100 -> :sswitch_5
        0x200 -> :sswitch_7
        0x400 -> :sswitch_5
        0x800 -> :sswitch_7
    .end sparse-switch
.end method

.method private getPartialyPopulatedAccessibilityEvent(I)Landroid/view/accessibility/AccessibilityEvent;
    .registers 4
    .parameter "eventType"

    #@0
    .prologue
    .line 415
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@3
    move-result-object v0

    #@4
    .line 416
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    iget-object v1, p0, Landroid/webkit/AccessibilityInjectorFallback;->mWebViewInternal:Landroid/webkit/WebView;

    #@6
    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@9
    .line 417
    return-object v0
.end method

.method private isEnterActionKey(I)Z
    .registers 3
    .parameter "keyCode"

    #@0
    .prologue
    .line 462
    const/16 v0, 0x17

    #@2
    if-eq p1, v0, :cond_c

    #@4
    const/16 v0, 0x42

    #@6
    if-eq p1, v0, :cond_c

    #@8
    const/16 v0, 0xa0

    #@a
    if-ne p1, v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private performAxisTransition(IIZLjava/lang/String;)V
    .registers 6
    .parameter "fromAxis"
    .parameter "toAxis"
    .parameter "sendEvent"
    .parameter "contentDescription"

    #@0
    .prologue
    .line 253
    iget v0, p0, Landroid/webkit/AccessibilityInjectorFallback;->mCurrentAxis:I

    #@2
    if-ne v0, p1, :cond_7

    #@4
    .line 254
    invoke-direct {p0, p2, p3, p4}, Landroid/webkit/AccessibilityInjectorFallback;->setCurrentAxis(IZLjava/lang/String;)V

    #@7
    .line 256
    :cond_7
    return-void
.end method

.method private sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 400
    const-string v1, "AccessibilityInjector"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Dispatching: "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 403
    iget-object v1, p0, Landroid/webkit/AccessibilityInjectorFallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@1a
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@21
    move-result-object v0

    #@22
    .line 405
    .local v0, accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_2b

    #@28
    .line 406
    invoke-virtual {v0, p1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@2b
    .line 408
    :cond_2b
    return-void
.end method

.method private setCurrentAxis(IZLjava/lang/String;)V
    .registers 7
    .parameter "axis"
    .parameter "sendEvent"
    .parameter "contentDescription"

    #@0
    .prologue
    .line 233
    iput p1, p0, Landroid/webkit/AccessibilityInjectorFallback;->mCurrentAxis:I

    #@2
    .line 234
    if-eqz p2, :cond_1b

    #@4
    .line 235
    const/16 v1, 0x4000

    #@6
    invoke-direct {p0, v1}, Landroid/webkit/AccessibilityInjectorFallback;->getPartialyPopulatedAccessibilityEvent(I)Landroid/view/accessibility/AccessibilityEvent;

    #@9
    move-result-object v0

    #@a
    .line 237
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@d
    move-result-object v1

    #@e
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@15
    .line 238
    invoke-virtual {v0, p3}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    #@18
    .line 239
    invoke-direct {p0, v0}, Landroid/webkit/AccessibilityInjectorFallback;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@1b
    .line 241
    .end local v0           #event:Landroid/view/accessibility/AccessibilityEvent;
    :cond_1b
    return-void
.end method

.method private traverseCurrentAxis(IZLjava/lang/String;)Z
    .registers 5
    .parameter "direction"
    .parameter "sendEvent"
    .parameter "contentDescription"

    #@0
    .prologue
    .line 269
    iget v0, p0, Landroid/webkit/AccessibilityInjectorFallback;->mCurrentAxis:I

    #@2
    invoke-direct {p0, p1, v0, p2, p3}, Landroid/webkit/AccessibilityInjectorFallback;->traverseGivenAxis(IIZLjava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private traverseGivenAxis(IIZLjava/lang/String;)Z
    .registers 9
    .parameter "direction"
    .parameter "axis"
    .parameter "sendEvent"
    .parameter "contentDescription"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 349
    iget-object v3, p0, Landroid/webkit/AccessibilityInjectorFallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@3
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getWebViewCore()Landroid/webkit/WebViewCore;

    #@6
    move-result-object v1

    #@7
    .line 350
    .local v1, webViewCore:Landroid/webkit/WebViewCore;
    if-nez v1, :cond_a

    #@9
    .line 370
    :cond_9
    :goto_9
    return v2

    #@a
    .line 354
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 355
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    if-eqz p3, :cond_16

    #@d
    .line 356
    const/high16 v3, 0x2

    #@f
    invoke-direct {p0, v3}, Landroid/webkit/AccessibilityInjectorFallback;->getPartialyPopulatedAccessibilityEvent(I)Landroid/view/accessibility/AccessibilityEvent;

    #@12
    move-result-object v0

    #@13
    .line 359
    invoke-virtual {v0, p4}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    #@16
    .line 361
    :cond_16
    iget-object v3, p0, Landroid/webkit/AccessibilityInjectorFallback;->mScheduledEventStack:Ljava/util/Stack;

    #@18
    invoke-virtual {v3, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 365
    const/4 v3, 0x7

    #@1c
    if-eq p2, v3, :cond_9

    #@1e
    .line 369
    const/16 v2, 0xbe

    #@20
    invoke-virtual {v1, v2, p1, p2}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@23
    .line 370
    const/4 v2, 0x1

    #@24
    goto :goto_9
.end method


# virtual methods
.method public onKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 18
    .parameter "event"

    #@0
    .prologue
    .line 135
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@3
    move-result v13

    #@4
    move-object/from16 v0, p0

    #@6
    invoke-direct {v0, v13}, Landroid/webkit/AccessibilityInjectorFallback;->isEnterActionKey(I)Z

    #@9
    move-result v13

    #@a
    if-eqz v13, :cond_e

    #@c
    .line 136
    const/4 v13, 0x0

    #@d
    .line 221
    :goto_d
    return v13

    #@e
    .line 139
    :cond_e
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getAction()I

    #@11
    move-result v13

    #@12
    const/4 v14, 0x1

    #@13
    if-ne v13, v14, :cond_1a

    #@15
    .line 140
    move-object/from16 v0, p0

    #@17
    iget-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDownEventHandled:Z

    #@19
    goto :goto_d

    #@1a
    .line 143
    :cond_1a
    const/4 v13, 0x0

    #@1b
    move-object/from16 v0, p0

    #@1d
    iput-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDownEventHandled:Z

    #@1f
    .line 145
    const/4 v3, 0x0

    #@20
    .line 146
    .local v3, binding:Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;
    sget-object v13, Landroid/webkit/AccessibilityInjectorFallback;->sBindings:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@25
    move-result-object v10

    #@26
    .local v10, i$:Ljava/util/Iterator;
    :cond_26
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@29
    move-result v13

    #@2a
    if-eqz v13, :cond_49

    #@2c
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f
    move-result-object v4

    #@30
    check-cast v4, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;

    #@32
    .line 147
    .local v4, candidate:Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@35
    move-result v13

    #@36
    invoke-virtual {v4}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getKeyCode()I

    #@39
    move-result v14

    #@3a
    if-ne v13, v14, :cond_26

    #@3c
    invoke-virtual {v4}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getModifiers()I

    #@3f
    move-result v13

    #@40
    move-object/from16 v0, p1

    #@42
    invoke-virtual {v0, v13}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@45
    move-result v13

    #@46
    if-eqz v13, :cond_26

    #@48
    .line 149
    move-object v3, v4

    #@49
    .line 154
    .end local v4           #candidate:Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;
    :cond_49
    if-nez v3, :cond_4d

    #@4b
    .line 155
    const/4 v13, 0x0

    #@4c
    goto :goto_d

    #@4d
    .line 158
    :cond_4d
    const/4 v9, 0x0

    #@4e
    .local v9, i:I
    invoke-virtual {v3}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getActionCount()I

    #@51
    move-result v6

    #@52
    .local v6, count:I
    :goto_52
    if-ge v9, v6, :cond_14c

    #@54
    .line 159
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getActionCode(I)I

    #@57
    move-result v1

    #@58
    .line 160
    .local v1, actionCode:I
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getAction(I)I

    #@5b
    move-result v13

    #@5c
    invoke-static {v13}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    .line 161
    .local v5, contentDescription:Ljava/lang/String;
    packed-switch v1, :pswitch_data_152

    #@63
    .line 217
    const-string v13, "AccessibilityInjector"

    #@65
    new-instance v14, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v15, "Unknown action code: "

    #@6c
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v14

    #@70
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v14

    #@74
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v14

    #@78
    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 158
    :goto_7b
    add-int/lit8 v9, v9, 0x1

    #@7d
    goto :goto_52

    #@7e
    .line 163
    :pswitch_7e
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getFirstArgument(I)I

    #@81
    move-result v2

    #@82
    .line 164
    .local v2, axis:I
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getSecondArgument(I)I

    #@85
    move-result v13

    #@86
    const/4 v14, 0x1

    #@87
    if-ne v13, v14, :cond_95

    #@89
    const/4 v11, 0x1

    #@8a
    .line 165
    .local v11, sendEvent:Z
    :goto_8a
    move-object/from16 v0, p0

    #@8c
    invoke-direct {v0, v2, v11, v5}, Landroid/webkit/AccessibilityInjectorFallback;->setCurrentAxis(IZLjava/lang/String;)V

    #@8f
    .line 166
    const/4 v13, 0x1

    #@90
    move-object/from16 v0, p0

    #@92
    iput-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDownEventHandled:Z

    #@94
    goto :goto_7b

    #@95
    .line 164
    .end local v11           #sendEvent:Z
    :cond_95
    const/4 v11, 0x0

    #@96
    goto :goto_8a

    #@97
    .line 169
    .end local v2           #axis:I
    :pswitch_97
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getFirstArgument(I)I

    #@9a
    move-result v7

    #@9b
    .line 171
    .local v7, direction:I
    move-object/from16 v0, p0

    #@9d
    iget v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDirection:I

    #@9f
    if-ne v7, v13, :cond_af

    #@a1
    move-object/from16 v0, p0

    #@a3
    iget-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mIsLastSelectionStringNull:Z

    #@a5
    if-eqz v13, :cond_af

    #@a7
    .line 172
    const/4 v13, 0x0

    #@a8
    move-object/from16 v0, p0

    #@aa
    iput-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mIsLastSelectionStringNull:Z

    #@ac
    .line 173
    const/4 v13, 0x0

    #@ad
    goto/16 :goto_d

    #@af
    .line 175
    :cond_af
    move-object/from16 v0, p0

    #@b1
    iput v7, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDirection:I

    #@b3
    .line 176
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getSecondArgument(I)I

    #@b6
    move-result v13

    #@b7
    const/4 v14, 0x1

    #@b8
    if-ne v13, v14, :cond_c6

    #@ba
    const/4 v11, 0x1

    #@bb
    .line 177
    .restart local v11       #sendEvent:Z
    :goto_bb
    move-object/from16 v0, p0

    #@bd
    invoke-direct {v0, v7, v11, v5}, Landroid/webkit/AccessibilityInjectorFallback;->traverseCurrentAxis(IZLjava/lang/String;)Z

    #@c0
    move-result v13

    #@c1
    move-object/from16 v0, p0

    #@c3
    iput-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDownEventHandled:Z

    #@c5
    goto :goto_7b

    #@c6
    .line 176
    .end local v11           #sendEvent:Z
    :cond_c6
    const/4 v11, 0x0

    #@c7
    goto :goto_bb

    #@c8
    .line 181
    .end local v7           #direction:I
    :pswitch_c8
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getFirstArgument(I)I

    #@cb
    move-result v7

    #@cc
    .line 183
    .restart local v7       #direction:I
    move-object/from16 v0, p0

    #@ce
    iget v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDirection:I

    #@d0
    if-ne v7, v13, :cond_e0

    #@d2
    move-object/from16 v0, p0

    #@d4
    iget-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mIsLastSelectionStringNull:Z

    #@d6
    if-eqz v13, :cond_e0

    #@d8
    .line 184
    const/4 v13, 0x0

    #@d9
    move-object/from16 v0, p0

    #@db
    iput-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mIsLastSelectionStringNull:Z

    #@dd
    .line 185
    const/4 v13, 0x0

    #@de
    goto/16 :goto_d

    #@e0
    .line 187
    :cond_e0
    move-object/from16 v0, p0

    #@e2
    iput v7, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDirection:I

    #@e4
    .line 188
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getSecondArgument(I)I

    #@e7
    move-result v2

    #@e8
    .line 189
    .restart local v2       #axis:I
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getThirdArgument(I)I

    #@eb
    move-result v13

    #@ec
    const/4 v14, 0x1

    #@ed
    if-ne v13, v14, :cond_fb

    #@ef
    const/4 v11, 0x1

    #@f0
    .line 190
    .restart local v11       #sendEvent:Z
    :goto_f0
    move-object/from16 v0, p0

    #@f2
    invoke-direct {v0, v7, v2, v11, v5}, Landroid/webkit/AccessibilityInjectorFallback;->traverseGivenAxis(IIZLjava/lang/String;)Z

    #@f5
    .line 191
    const/4 v13, 0x1

    #@f6
    move-object/from16 v0, p0

    #@f8
    iput-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDownEventHandled:Z

    #@fa
    goto :goto_7b

    #@fb
    .line 189
    .end local v11           #sendEvent:Z
    :cond_fb
    const/4 v11, 0x0

    #@fc
    goto :goto_f0

    #@fd
    .line 194
    .end local v2           #axis:I
    .end local v7           #direction:I
    :pswitch_fd
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getFirstArgument(I)I

    #@100
    move-result v8

    #@101
    .line 195
    .local v8, fromAxis:I
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getSecondArgument(I)I

    #@104
    move-result v12

    #@105
    .line 196
    .local v12, toAxis:I
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getThirdArgument(I)I

    #@108
    move-result v13

    #@109
    const/4 v14, 0x1

    #@10a
    if-ne v13, v14, :cond_119

    #@10c
    const/4 v11, 0x1

    #@10d
    .line 197
    .restart local v11       #sendEvent:Z
    :goto_10d
    move-object/from16 v0, p0

    #@10f
    invoke-direct {v0, v8, v12, v11, v5}, Landroid/webkit/AccessibilityInjectorFallback;->performAxisTransition(IIZLjava/lang/String;)V

    #@112
    .line 198
    const/4 v13, 0x1

    #@113
    move-object/from16 v0, p0

    #@115
    iput-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDownEventHandled:Z

    #@117
    goto/16 :goto_7b

    #@119
    .line 196
    .end local v11           #sendEvent:Z
    :cond_119
    const/4 v11, 0x0

    #@11a
    goto :goto_10d

    #@11b
    .line 204
    .end local v8           #fromAxis:I
    .end local v12           #toAxis:I
    :pswitch_11b
    move-object/from16 v0, p0

    #@11d
    iget v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mCurrentAxis:I

    #@11f
    const/4 v14, 0x7

    #@120
    if-ne v13, v14, :cond_145

    #@122
    .line 207
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getFirstArgument(I)I

    #@125
    move-result v13

    #@126
    move-object/from16 v0, p0

    #@128
    iput v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDirection:I

    #@12a
    .line 208
    invoke-virtual {v3, v9}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getSecondArgument(I)I

    #@12d
    move-result v13

    #@12e
    const/4 v14, 0x1

    #@12f
    if-ne v13, v14, :cond_143

    #@131
    const/4 v11, 0x1

    #@132
    .line 209
    .restart local v11       #sendEvent:Z
    :goto_132
    move-object/from16 v0, p0

    #@134
    iget v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDirection:I

    #@136
    const/4 v14, 0x7

    #@137
    move-object/from16 v0, p0

    #@139
    invoke-direct {v0, v13, v14, v11, v5}, Landroid/webkit/AccessibilityInjectorFallback;->traverseGivenAxis(IIZLjava/lang/String;)Z

    #@13c
    .line 211
    const/4 v13, 0x0

    #@13d
    move-object/from16 v0, p0

    #@13f
    iput-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDownEventHandled:Z

    #@141
    goto/16 :goto_7b

    #@143
    .line 208
    .end local v11           #sendEvent:Z
    :cond_143
    const/4 v11, 0x0

    #@144
    goto :goto_132

    #@145
    .line 213
    :cond_145
    const/4 v13, 0x1

    #@146
    move-object/from16 v0, p0

    #@148
    iput-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDownEventHandled:Z

    #@14a
    goto/16 :goto_7b

    #@14c
    .line 221
    .end local v1           #actionCode:I
    .end local v5           #contentDescription:Ljava/lang/String;
    :cond_14c
    move-object/from16 v0, p0

    #@14e
    iget-boolean v13, v0, Landroid/webkit/AccessibilityInjectorFallback;->mLastDownEventHandled:Z

    #@150
    goto/16 :goto_d

    #@152
    .line 161
    :pswitch_data_152
    .packed-switch 0x0
        :pswitch_7e
        :pswitch_97
        :pswitch_c8
        :pswitch_fd
        :pswitch_11b
    .end packed-switch
.end method

.method public onSelectionStringChange(Ljava/lang/String;)V
    .registers 7
    .parameter "selectionString"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 378
    const-string v1, "AccessibilityInjector"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "Selection string: "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 380
    if-nez p1, :cond_27

    #@1b
    const/4 v1, 0x1

    #@1c
    :goto_1c
    iput-boolean v1, p0, Landroid/webkit/AccessibilityInjectorFallback;->mIsLastSelectionStringNull:Z

    #@1e
    .line 381
    iget-object v1, p0, Landroid/webkit/AccessibilityInjectorFallback;->mScheduledEventStack:Ljava/util/Stack;

    #@20
    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_29

    #@26
    .line 391
    :cond_26
    :goto_26
    return-void

    #@27
    :cond_27
    move v1, v2

    #@28
    .line 380
    goto :goto_1c

    #@29
    .line 384
    :cond_29
    iget-object v1, p0, Landroid/webkit/AccessibilityInjectorFallback;->mScheduledEventStack:Ljava/util/Stack;

    #@2b
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Landroid/view/accessibility/AccessibilityEvent;

    #@31
    .line 385
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    if-eqz v0, :cond_26

    #@33
    if-eqz p1, :cond_26

    #@35
    .line 386
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@38
    move-result-object v1

    #@39
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@3c
    .line 387
    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    #@3f
    .line 388
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@42
    move-result v1

    #@43
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    #@46
    .line 389
    invoke-direct {p0, v0}, Landroid/webkit/AccessibilityInjectorFallback;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@49
    goto :goto_26
.end method

.method performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 8
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 273
    sparse-switch p1, :sswitch_data_26

    #@5
    .line 289
    const/4 v2, 0x0

    #@6
    :goto_6
    return v2

    #@7
    .line 276
    :sswitch_7
    invoke-static {p1}, Landroid/webkit/AccessibilityInjectorFallback;->getDirectionForAction(I)I

    #@a
    move-result v1

    #@b
    .line 277
    .local v1, direction:I
    const-string v2, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    #@d
    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@10
    move-result v2

    #@11
    invoke-static {v2}, Landroid/webkit/AccessibilityInjectorFallback;->getAxisForGranularity(I)I

    #@14
    move-result v0

    #@15
    .line 279
    .local v0, axis:I
    invoke-direct {p0, v1, v0, v3, v4}, Landroid/webkit/AccessibilityInjectorFallback;->traverseGivenAxis(IIZLjava/lang/String;)Z

    #@18
    move-result v2

    #@19
    goto :goto_6

    #@1a
    .line 283
    .end local v0           #axis:I
    .end local v1           #direction:I
    :sswitch_1a
    invoke-static {p1}, Landroid/webkit/AccessibilityInjectorFallback;->getDirectionForAction(I)I

    #@1d
    move-result v1

    #@1e
    .line 285
    .restart local v1       #direction:I
    const/4 v0, 0x2

    #@1f
    .line 286
    .restart local v0       #axis:I
    const/4 v2, 0x2

    #@20
    invoke-direct {p0, v1, v2, v3, v4}, Landroid/webkit/AccessibilityInjectorFallback;->traverseGivenAxis(IIZLjava/lang/String;)Z

    #@23
    move-result v2

    #@24
    goto :goto_6

    #@25
    .line 273
    nop

    #@26
    :sswitch_data_26
    .sparse-switch
        0x100 -> :sswitch_7
        0x200 -> :sswitch_7
        0x400 -> :sswitch_1a
        0x800 -> :sswitch_1a
    .end sparse-switch
.end method
