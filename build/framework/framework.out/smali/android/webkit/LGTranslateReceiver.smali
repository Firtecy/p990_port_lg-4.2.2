.class public Landroid/webkit/LGTranslateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LGTranslateReceiver.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final EMAIL:Ljava/lang/String; = "com.lge.email"

.field private static final LOG_TAG:Ljava/lang/String; = "LGTranslateReceiver"

.field private static final TRANSLATE_EXIT:Ljava/lang/String; = "com.lge.texttranslate.EXIT"

.field private static final TRANSLATE_ROTATE_DELAY:I = 0x5dc

.field private static final TRANSLATOR:Ljava/lang/String; = "com.lge.texttranslate"

.field private static volatile sTranslateReceiver:Landroid/webkit/LGTranslateReceiver;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTranslateMode:Z

.field private mWebViewClassic:Landroid/webkit/WebViewClassic;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 23
    const/4 v0, 0x0

    #@1
    sput-object v0, Landroid/webkit/LGTranslateReceiver;->sTranslateReceiver:Landroid/webkit/LGTranslateReceiver;

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;)V
    .registers 4
    .parameter "c"
    .parameter "v"

    #@0
    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    .line 26
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/webkit/LGTranslateReceiver;->mTranslateMode:Z

    #@6
    .line 35
    iput-object p1, p0, Landroid/webkit/LGTranslateReceiver;->mContext:Landroid/content/Context;

    #@8
    .line 36
    iput-object p2, p0, Landroid/webkit/LGTranslateReceiver;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@a
    .line 37
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/LGTranslateReceiver;)Landroid/webkit/WebViewClassic;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 21
    iget-object v0, p0, Landroid/webkit/LGTranslateReceiver;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@2
    return-object v0
.end method

.method private calcTranslateRect()Landroid/graphics/Rect;
    .registers 5

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 72
    iget-object v1, p0, Landroid/webkit/LGTranslateReceiver;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@3
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getParagraphRect()Landroid/graphics/Rect;

    #@6
    move-result-object v0

    #@7
    .line 74
    .local v0, rect:Landroid/graphics/Rect;
    const-string v1, "com.lge.email"

    #@9
    invoke-direct {p0}, Landroid/webkit/LGTranslateReceiver;->getRegisteredPackage()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_1f

    #@13
    .line 75
    iget v1, v0, Landroid/graphics/Rect;->top:I

    #@15
    add-int/lit8 v1, v1, 0x64

    #@17
    iput v1, v0, Landroid/graphics/Rect;->top:I

    #@19
    .line 76
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    #@1b
    add-int/lit8 v1, v1, 0x64

    #@1d
    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    #@1f
    .line 78
    :cond_1f
    iget v1, v0, Landroid/graphics/Rect;->top:I

    #@21
    if-gez v1, :cond_25

    #@23
    .line 79
    iput v3, v0, Landroid/graphics/Rect;->top:I

    #@25
    .line 81
    :cond_25
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    #@27
    iget-object v2, p0, Landroid/webkit/LGTranslateReceiver;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@29
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getViewHeightWithTitle()I

    #@2c
    move-result v2

    #@2d
    if-le v1, v2, :cond_31

    #@2f
    .line 82
    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    #@31
    .line 84
    :cond_31
    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;Landroid/webkit/WebViewClassic;)Landroid/webkit/LGTranslateReceiver;
    .registers 4
    .parameter "c"
    .parameter "v"

    #@0
    .prologue
    .line 40
    const-class v1, Landroid/webkit/LGTranslateReceiver;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/LGTranslateReceiver;->sTranslateReceiver:Landroid/webkit/LGTranslateReceiver;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 41
    new-instance v0, Landroid/webkit/LGTranslateReceiver;

    #@9
    invoke-direct {v0, p0, p1}, Landroid/webkit/LGTranslateReceiver;-><init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;)V

    #@c
    sput-object v0, Landroid/webkit/LGTranslateReceiver;->sTranslateReceiver:Landroid/webkit/LGTranslateReceiver;

    #@e
    .line 43
    :cond_e
    sget-object v0, Landroid/webkit/LGTranslateReceiver;->sTranslateReceiver:Landroid/webkit/LGTranslateReceiver;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    monitor-exit v1

    #@11
    return-object v0

    #@12
    .line 40
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1

    #@14
    throw v0
.end method

.method private getRegisteredPackage()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Landroid/webkit/LGTranslateReceiver;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@5
    move-result-object v0

    #@6
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@8
    return-object v0
.end method

.method private declared-synchronized setupTranslateListener()V
    .registers 4

    #@0
    .prologue
    .line 51
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/content/IntentFilter;

    #@3
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@6
    .line 52
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "com.lge.texttranslate.EXIT"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@b
    .line 53
    iget-object v1, p0, Landroid/webkit/LGTranslateReceiver;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@10
    move-result-object v1

    #@11
    sget-object v2, Landroid/webkit/LGTranslateReceiver;->sTranslateReceiver:Landroid/webkit/LGTranslateReceiver;

    #@13
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_18

    #@16
    .line 54
    monitor-exit p0

    #@17
    return-void

    #@18
    .line 51
    .end local v0           #filter:Landroid/content/IntentFilter;
    :catchall_18
    move-exception v1

    #@19
    monitor-exit p0

    #@1a
    throw v1
.end method


# virtual methods
.method public declared-synchronized disableTranslateListener()V
    .registers 4

    #@0
    .prologue
    .line 57
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v1, Landroid/webkit/LGTranslateReceiver;->sTranslateReceiver:Landroid/webkit/LGTranslateReceiver;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_1e

    #@3
    if-nez v1, :cond_7

    #@5
    .line 69
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 61
    :cond_7
    :try_start_7
    iget-object v1, p0, Landroid/webkit/LGTranslateReceiver;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@c
    move-result-object v1

    #@d
    sget-object v2, Landroid/webkit/LGTranslateReceiver;->sTranslateReceiver:Landroid/webkit/LGTranslateReceiver;

    #@f
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_12
    .catchall {:try_start_7 .. :try_end_12} :catchall_1e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_12} :catch_21

    #@12
    .line 65
    :goto_12
    const/4 v1, 0x0

    #@13
    :try_start_13
    sput-object v1, Landroid/webkit/LGTranslateReceiver;->sTranslateReceiver:Landroid/webkit/LGTranslateReceiver;

    #@15
    .line 67
    const-string v1, "LGTranslateReceiver"

    #@17
    const-string/jumbo v2, "sTranslateReceiver is destroy"

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d
    .catchall {:try_start_13 .. :try_end_1d} :catchall_1e

    #@1d
    goto :goto_5

    #@1e
    .line 57
    :catchall_1e
    move-exception v1

    #@1f
    monitor-exit p0

    #@20
    throw v1

    #@21
    .line 62
    :catch_21
    move-exception v0

    #@22
    .line 63
    .local v0, e:Ljava/lang/IllegalArgumentException;
    :try_start_22
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_1e

    #@25
    goto :goto_12
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 119
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v2

    #@5
    const-string v3, "com.lge.texttranslate.EXIT"

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_3e

    #@d
    .line 120
    const-string/jumbo v2, "orientation"

    #@10
    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@13
    move-result v1

    #@14
    .line 122
    .local v1, mTranslateOrientaion:Z
    const-string v2, "LGTranslateReceiver"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string/jumbo v4, "orientation is : "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 124
    if-eqz v1, :cond_3f

    #@2f
    .line 125
    new-instance v0, Landroid/os/Handler;

    #@31
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@34
    .line 126
    .local v0, mHandler:Landroid/os/Handler;
    new-instance v2, Landroid/webkit/LGTranslateReceiver$1;

    #@36
    invoke-direct {v2, p0}, Landroid/webkit/LGTranslateReceiver$1;-><init>(Landroid/webkit/LGTranslateReceiver;)V

    #@39
    const-wide/16 v3, 0x5dc

    #@3b
    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@3e
    .line 143
    .end local v0           #mHandler:Landroid/os/Handler;
    .end local v1           #mTranslateOrientaion:Z
    :cond_3e
    :goto_3e
    return-void

    #@3f
    .line 135
    .restart local v1       #mTranslateOrientaion:Z
    :cond_3f
    iget-object v2, p0, Landroid/webkit/LGTranslateReceiver;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@41
    invoke-virtual {v2, v5}, Landroid/webkit/WebViewClassic;->setTranslateMode(Z)V

    #@44
    .line 136
    iget-object v2, p0, Landroid/webkit/LGTranslateReceiver;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@46
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@49
    .line 137
    invoke-virtual {p0}, Landroid/webkit/LGTranslateReceiver;->disableTranslateListener()V

    #@4c
    .line 139
    const-string v2, "LGTranslateReceiver"

    #@4e
    new-instance v3, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string/jumbo v4, "not received orientation, so Be closed., getTranslateMode() : "

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    iget-object v4, p0, Landroid/webkit/LGTranslateReceiver;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@5c
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getTranslateMode()Z

    #@5f
    move-result v4

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v3

    #@68
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    goto :goto_3e
.end method

.method public translateSelection()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 88
    invoke-direct {p0}, Landroid/webkit/LGTranslateReceiver;->setupTranslateListener()V

    #@4
    .line 89
    iget-object v4, p0, Landroid/webkit/LGTranslateReceiver;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@6
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    .line 90
    .local v3, text:Ljava/lang/String;
    if-eqz v3, :cond_a5

    #@c
    .line 91
    invoke-direct {p0}, Landroid/webkit/LGTranslateReceiver;->calcTranslateRect()Landroid/graphics/Rect;

    #@f
    move-result-object v2

    #@10
    .line 93
    .local v2, rect:Landroid/graphics/Rect;
    const-string v4, "LGTranslateReceiver"

    #@12
    new-instance v5, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string/jumbo v6, "text is : "

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 94
    const-string v4, "LGTranslateReceiver"

    #@2b
    new-instance v5, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v6, "getSelectionRegion:topleft.y : "

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    iget v6, v2, Landroid/graphics/Rect;->top:I

    #@38
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 95
    const-string v4, "LGTranslateReceiver"

    #@45
    new-instance v5, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v6, "getSelectionRegion:bottomright.y : "

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    iget v6, v2, Landroid/graphics/Rect;->bottom:I

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 97
    new-instance v1, Landroid/content/Intent;

    #@5f
    const-string v4, "com.lge.texttranslate"

    #@61
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@64
    .line 98
    .local v1, i:Landroid/content/Intent;
    const-string/jumbo v4, "leftMost"

    #@67
    iget v5, v2, Landroid/graphics/Rect;->left:I

    #@69
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@6c
    .line 99
    const-string/jumbo v4, "topOfSelected"

    #@6f
    iget v5, v2, Landroid/graphics/Rect;->top:I

    #@71
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@74
    .line 100
    const-string/jumbo v4, "rightMost"

    #@77
    iget v5, v2, Landroid/graphics/Rect;->right:I

    #@79
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@7c
    .line 101
    const-string v4, "bottomOfSelected"

    #@7e
    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    #@80
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@83
    .line 102
    const-string/jumbo v4, "textString"

    #@86
    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@89
    .line 103
    const-string v4, "enable"

    #@8b
    const-string v5, "com.lge.texttranslate"

    #@8d
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@90
    .line 104
    const-string/jumbo v4, "isWebView"

    #@93
    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@96
    .line 105
    const/high16 v4, 0x1400

    #@98
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@9b
    .line 107
    iget-object v4, p0, Landroid/webkit/LGTranslateReceiver;->mWebViewClassic:Landroid/webkit/WebViewClassic;

    #@9d
    invoke-virtual {v4, v7}, Landroid/webkit/WebViewClassic;->setTranslateMode(Z)V

    #@a0
    .line 110
    :try_start_a0
    iget-object v4, p0, Landroid/webkit/LGTranslateReceiver;->mContext:Landroid/content/Context;

    #@a2
    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_a5
    .catch Landroid/content/ActivityNotFoundException; {:try_start_a0 .. :try_end_a5} :catch_a6

    #@a5
    .line 115
    .end local v1           #i:Landroid/content/Intent;
    .end local v2           #rect:Landroid/graphics/Rect;
    :cond_a5
    :goto_a5
    return-void

    #@a6
    .line 111
    .restart local v1       #i:Landroid/content/Intent;
    .restart local v2       #rect:Landroid/graphics/Rect;
    :catch_a6
    move-exception v0

    #@a7
    .line 112
    .local v0, e:Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    #@aa
    goto :goto_a5
.end method
