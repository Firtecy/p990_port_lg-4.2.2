.class Landroid/webkit/DeviceMotionService$1;
.super Ljava/lang/Object;
.source "DeviceMotionService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/DeviceMotionService;->sendErrorEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Landroid/webkit/DeviceMotionService;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 81
    const-class v0, Landroid/webkit/DeviceMotionService;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/DeviceMotionService$1;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method constructor <init>(Landroid/webkit/DeviceMotionService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 81
    iput-object p1, p0, Landroid/webkit/DeviceMotionService$1;->this$0:Landroid/webkit/DeviceMotionService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 84
    sget-boolean v0, Landroid/webkit/DeviceMotionService$1;->$assertionsDisabled:Z

    #@3
    if-nez v0, :cond_1b

    #@5
    const-string v0, "WebViewCoreThread"

    #@7
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_1b

    #@15
    new-instance v0, Ljava/lang/AssertionError;

    #@17
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@1a
    throw v0

    #@1b
    .line 85
    :cond_1b
    iget-object v0, p0, Landroid/webkit/DeviceMotionService$1;->this$0:Landroid/webkit/DeviceMotionService;

    #@1d
    invoke-static {v0}, Landroid/webkit/DeviceMotionService;->access$000(Landroid/webkit/DeviceMotionService;)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_30

    #@23
    .line 87
    iget-object v0, p0, Landroid/webkit/DeviceMotionService$1;->this$0:Landroid/webkit/DeviceMotionService;

    #@25
    invoke-static {v0}, Landroid/webkit/DeviceMotionService;->access$100(Landroid/webkit/DeviceMotionService;)Landroid/webkit/DeviceMotionAndOrientationManager;

    #@28
    move-result-object v0

    #@29
    const-wide/16 v4, 0x0

    #@2b
    move-object v2, v1

    #@2c
    move-object v3, v1

    #@2d
    invoke-virtual/range {v0 .. v5}, Landroid/webkit/DeviceMotionAndOrientationManager;->onMotionChange(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;D)V

    #@30
    .line 89
    :cond_30
    return-void
.end method
