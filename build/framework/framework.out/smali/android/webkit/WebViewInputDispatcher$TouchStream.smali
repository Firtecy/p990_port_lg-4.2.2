.class final Landroid/webkit/WebViewInputDispatcher$TouchStream;
.super Ljava/lang/Object;
.source "WebViewInputDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewInputDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TouchStream"
.end annotation


# instance fields
.field private mLastEvent:Landroid/view/MotionEvent;


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1376
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/WebViewInputDispatcher$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1376
    invoke-direct {p0}, Landroid/webkit/WebViewInputDispatcher$TouchStream;-><init>()V

    #@3
    return-void
.end method

.method private updateLastEvent(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1443
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1444
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@6
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    #@9
    .line 1446
    :cond_9
    if-eqz p1, :cond_12

    #@b
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@e
    move-result-object v0

    #@f
    :goto_f
    iput-object v0, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@11
    .line 1447
    return-void

    #@12
    .line 1446
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_f
.end method


# virtual methods
.method public getLastEvent()Landroid/view/MotionEvent;
    .registers 2

    #@0
    .prologue
    .line 1384
    iget-object v0, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@2
    return-object v0
.end method

.method public isCancelNeeded()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1439
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@3
    if-eqz v1, :cond_e

    #@5
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@7
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    #@a
    move-result v1

    #@b
    if-eq v1, v0, :cond_e

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public update(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1395
    if-nez p1, :cond_15

    #@3
    .line 1396
    invoke-virtual {p0}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->isCancelNeeded()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_13

    #@9
    .line 1397
    iget-object p1, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@b
    .line 1398
    if-eqz p1, :cond_13

    #@d
    .line 1399
    const/4 v1, 0x3

    #@e
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@11
    .line 1400
    iput-object v0, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 1430
    :cond_14
    :goto_14
    return-object v0

    #@15
    .line 1406
    :cond_15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@18
    move-result v1

    #@19
    packed-switch v1, :pswitch_data_3e

    #@1c
    :pswitch_1c
    goto :goto_14

    #@1d
    .line 1419
    :pswitch_1d
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->updateLastEvent(Landroid/view/MotionEvent;)V

    #@20
    move-object v0, p1

    #@21
    .line 1420
    goto :goto_14

    #@22
    .line 1411
    :pswitch_22
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@24
    if-eqz v1, :cond_14

    #@26
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@28
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    #@2b
    move-result v1

    #@2c
    const/4 v2, 0x1

    #@2d
    if-eq v1, v2, :cond_14

    #@2f
    .line 1415
    invoke-direct {p0, p1}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->updateLastEvent(Landroid/view/MotionEvent;)V

    #@32
    move-object v0, p1

    #@33
    .line 1416
    goto :goto_14

    #@34
    .line 1423
    :pswitch_34
    iget-object v1, p0, Landroid/webkit/WebViewInputDispatcher$TouchStream;->mLastEvent:Landroid/view/MotionEvent;

    #@36
    if-eqz v1, :cond_14

    #@38
    .line 1426
    invoke-direct {p0, v0}, Landroid/webkit/WebViewInputDispatcher$TouchStream;->updateLastEvent(Landroid/view/MotionEvent;)V

    #@3b
    move-object v0, p1

    #@3c
    .line 1427
    goto :goto_14

    #@3d
    .line 1406
    nop

    #@3e
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_22
        :pswitch_22
        :pswitch_34
        :pswitch_1c
        :pswitch_22
        :pswitch_22
    .end packed-switch
.end method
