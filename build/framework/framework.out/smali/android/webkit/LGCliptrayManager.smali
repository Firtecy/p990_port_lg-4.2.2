.class public Landroid/webkit/LGCliptrayManager;
.super Ljava/lang/Object;
.source "LGCliptrayManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;
    }
.end annotation


# static fields
.field public static final CLIPTRAY_FEATURE_NAME:Ljava/lang/String; = "com.lge.software.cliptray"

.field private static final LGCLIPTRAY_COPY_DATA:I = 0x4

.field private static final LGCLIPTRAY_COPY_IMAGE:I = 0x3

.field private static final LGCLIPTRAY_COPY_TEXT:I = 0x2

.field private static final LGCLIPTRAY_ONPASTE:I = 0x1

.field private static final LOGTAG:Ljava/lang/String; = "LGCliptrayManager"

.field private static sInstance:Landroid/webkit/LGCliptrayManager;


# instance fields
.field private mClipTrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

.field private mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

.field private mHandler:Landroid/os/Handler;

.field private mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 29
    iput-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@6
    .line 30
    iput-object v0, p0, Landroid/webkit/LGCliptrayManager;->mClipTrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@8
    .line 31
    iput-object v0, p0, Landroid/webkit/LGCliptrayManager;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@a
    .line 32
    iput-object v0, p0, Landroid/webkit/LGCliptrayManager;->mHandler:Landroid/os/Handler;

    #@c
    .line 44
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 29
    iput-object v3, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@6
    .line 30
    iput-object v3, p0, Landroid/webkit/LGCliptrayManager;->mClipTrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@8
    .line 31
    iput-object v3, p0, Landroid/webkit/LGCliptrayManager;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@a
    .line 32
    iput-object v3, p0, Landroid/webkit/LGCliptrayManager;->mHandler:Landroid/os/Handler;

    #@c
    .line 47
    sget-object v1, Lcom/lge/loader/RuntimeLibraryLoader;->CLIPTRAY_MANAGER:Ljava/lang/String;

    #@e
    invoke-static {v1}, Lcom/lge/loader/RuntimeLibraryLoader;->getCreator(Ljava/lang/String;)Lcom/lge/loader/InstanceCreator;

    #@11
    move-result-object v0

    #@12
    .line 48
    .local v0, creator:Lcom/lge/loader/InstanceCreator;
    if-nez v0, :cond_1c

    #@14
    .line 49
    const-string v1, "LGCliptrayManager"

    #@16
    const-string v2, "LGCliptrayManager(): Fail to get CliptrayManager"

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 68
    :goto_1b
    return-void

    #@1c
    .line 53
    :cond_1c
    invoke-virtual {v0, p1}, Lcom/lge/loader/InstanceCreator;->getDefaultInstance(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@22
    iput-object v1, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@24
    .line 54
    iget-object v1, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@26
    if-nez v1, :cond_2f

    #@28
    .line 55
    const-string v1, "LGCliptrayManager"

    #@2a
    const-string v2, "LGCliptrayManager(): mCliptrayManager is null"

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 58
    :cond_2f
    new-instance v1, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;

    #@31
    invoke-direct {v1, p0, v3}, Landroid/webkit/LGCliptrayManager$LGCliptrayHandler;-><init>(Landroid/webkit/LGCliptrayManager;Landroid/webkit/LGCliptrayManager$1;)V

    #@34
    iput-object v1, p0, Landroid/webkit/LGCliptrayManager;->mHandler:Landroid/os/Handler;

    #@36
    .line 60
    new-instance v1, Landroid/webkit/LGCliptrayManager$1;

    #@38
    invoke-direct {v1, p0}, Landroid/webkit/LGCliptrayManager$1;-><init>(Landroid/webkit/LGCliptrayManager;)V

    #@3b
    iput-object v1, p0, Landroid/webkit/LGCliptrayManager;->mClipTrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@3d
    goto :goto_1b
.end method

.method static synthetic access$100(Landroid/webkit/LGCliptrayManager;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 25
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/webkit/LGCliptrayManager;)Landroid/webkit/WebViewClassic$WebViewInputConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 25
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/webkit/LGCliptrayManager;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 25
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@2
    return-object v0
.end method

.method public static getInstance()Landroid/webkit/LGCliptrayManager;
    .registers 1

    #@0
    .prologue
    .line 71
    sget-object v0, Landroid/webkit/LGCliptrayManager;->sInstance:Landroid/webkit/LGCliptrayManager;

    #@2
    return-object v0
.end method

.method public static hasLGSystemCliptrayServiceFeature(Landroid/content/Context;)Z
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 75
    if-eqz p0, :cond_d

    #@2
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v0

    #@6
    const-string v1, "com.lge.software.cliptray"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@b
    move-result v0

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public static initCliptray(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 79
    const-class v1, Landroid/webkit/LGCliptrayManager;

    #@2
    monitor-enter v1

    #@3
    .line 80
    :try_start_3
    sget-object v0, Landroid/webkit/LGCliptrayManager;->sInstance:Landroid/webkit/LGCliptrayManager;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 81
    new-instance v0, Landroid/webkit/LGCliptrayManager;

    #@9
    invoke-direct {v0, p0}, Landroid/webkit/LGCliptrayManager;-><init>(Landroid/content/Context;)V

    #@c
    sput-object v0, Landroid/webkit/LGCliptrayManager;->sInstance:Landroid/webkit/LGCliptrayManager;

    #@e
    .line 83
    :cond_e
    monitor-exit v1

    #@f
    .line 84
    return-void

    #@10
    .line 83
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method


# virtual methods
.method public copyImageToCliptray(Landroid/net/Uri;)V
    .registers 4
    .parameter "imgUri"

    #@0
    .prologue
    .line 117
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mHandler:Landroid/os/Handler;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 118
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mHandler:Landroid/os/Handler;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@e
    .line 120
    :cond_e
    return-void
.end method

.method public copyToCliptray(Landroid/content/ClipData;)V
    .registers 4
    .parameter "clip"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mHandler:Landroid/os/Handler;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 112
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mHandler:Landroid/os/Handler;

    #@6
    const/4 v1, 0x4

    #@7
    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@e
    .line 114
    :cond_e
    return-void
.end method

.method public copyToCliptray(Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "selectedText"

    #@0
    .prologue
    .line 104
    iget-object v1, p0, Landroid/webkit/LGCliptrayManager;->mHandler:Landroid/os/Handler;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 105
    const/4 v1, 0x0

    #@5
    invoke-static {v1, p1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@8
    move-result-object v0

    #@9
    .line 106
    .local v0, clip:Landroid/content/ClipData;
    iget-object v1, p0, Landroid/webkit/LGCliptrayManager;->mHandler:Landroid/os/Handler;

    #@b
    const/4 v2, 0x4

    #@c
    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@13
    .line 108
    .end local v0           #clip:Landroid/content/ClipData;
    :cond_13
    return-void
.end method

.method public hideCliptray()V
    .registers 2

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@2
    if-eqz v0, :cond_19

    #@4
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@6
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isServiceConnected()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_19

    #@c
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@e
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->getVisibility()I

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_19

    #@14
    .line 99
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@16
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->hideCliptray()V

    #@19
    .line 101
    :cond_19
    return-void
.end method

.method public showCliptray(Landroid/webkit/WebViewClassic$WebViewInputConnection;)V
    .registers 4
    .parameter "inputConnection"

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@2
    if-eqz v0, :cond_20

    #@4
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@6
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isServiceConnected()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_20

    #@c
    .line 88
    iput-object p1, p0, Landroid/webkit/LGCliptrayManager;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@e
    .line 89
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@10
    iget-object v1, p0, Landroid/webkit/LGCliptrayManager;->mClipTrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@12
    invoke-interface {v0, v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@15
    .line 90
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@17
    const/4 v1, 0x0

    #@18
    invoke-interface {v0, v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@1b
    .line 91
    iget-object v0, p0, Landroid/webkit/LGCliptrayManager;->mCliptrayManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@1d
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->showCliptray()V

    #@20
    .line 93
    :cond_20
    return-void
.end method
