.class Landroid/webkit/WebViewClassic$InvokeListBox;
.super Ljava/lang/Object;
.source "WebViewClassic.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InvokeListBox"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;,
        Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;,
        Landroid/webkit/WebViewClassic$InvokeListBox$Container;
    }
.end annotation


# instance fields
.field private mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

.field private mIsDatalist:Z

.field private mMultiple:Z

.field private mSelectedArray:[I

.field private mSelection:I

.field final synthetic this$0:Landroid/webkit/WebViewClassic;


# direct methods
.method private constructor <init>(Landroid/webkit/WebViewClassic;[Ljava/lang/String;[IIZ)V
    .registers 11
    .parameter
    .parameter "array"
    .parameter "enabled"
    .parameter "selection"
    .parameter "isDatalist"

    #@0
    .prologue
    .line 9990
    iput-object p1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 9991
    iput p4, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mSelection:I

    #@7
    .line 9992
    const/4 v2, 0x0

    #@8
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mMultiple:Z

    #@a
    .line 9994
    iput-boolean p5, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mIsDatalist:Z

    #@c
    .line 9996
    array-length v1, p2

    #@d
    .line 9997
    .local v1, length:I
    new-array v2, v1, [Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@f
    iput-object v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@11
    .line 9998
    const/4 v0, 0x0

    #@12
    .local v0, i:I
    :goto_12
    if-ge v0, v1, :cond_37

    #@14
    .line 9999
    iget-object v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@16
    new-instance v3, Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@18
    const/4 v4, 0x0

    #@19
    invoke-direct {v3, p0, v4}, Landroid/webkit/WebViewClassic$InvokeListBox$Container;-><init>(Landroid/webkit/WebViewClassic$InvokeListBox;Landroid/webkit/WebViewClassic$1;)V

    #@1c
    aput-object v3, v2, v0

    #@1e
    .line 10000
    iget-object v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@20
    aget-object v2, v2, v0

    #@22
    aget-object v3, p2, v0

    #@24
    iput-object v3, v2, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mString:Ljava/lang/String;

    #@26
    .line 10001
    iget-object v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@28
    aget-object v2, v2, v0

    #@2a
    aget v3, p3, v0

    #@2c
    iput v3, v2, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mEnabled:I

    #@2e
    .line 10002
    iget-object v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@30
    aget-object v2, v2, v0

    #@32
    iput v0, v2, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mId:I

    #@34
    .line 9998
    add-int/lit8 v0, v0, 0x1

    #@36
    goto :goto_12

    #@37
    .line 10004
    :cond_37
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/WebViewClassic;[Ljava/lang/String;[IIZLandroid/webkit/WebViewClassic$1;)V
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 9831
    invoke-direct/range {p0 .. p5}, Landroid/webkit/WebViewClassic$InvokeListBox;-><init>(Landroid/webkit/WebViewClassic;[Ljava/lang/String;[IIZ)V

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/webkit/WebViewClassic;[Ljava/lang/String;[I[I)V
    .registers 10
    .parameter
    .parameter "array"
    .parameter "enabled"
    .parameter "selected"

    #@0
    .prologue
    .line 9973
    iput-object p1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 9974
    const/4 v2, 0x1

    #@6
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mMultiple:Z

    #@8
    .line 9975
    iput-object p4, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mSelectedArray:[I

    #@a
    .line 9977
    const/4 v2, 0x0

    #@b
    iput-boolean v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mIsDatalist:Z

    #@d
    .line 9979
    array-length v1, p2

    #@e
    .line 9980
    .local v1, length:I
    new-array v2, v1, [Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@10
    iput-object v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@12
    .line 9981
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    if-ge v0, v1, :cond_38

    #@15
    .line 9982
    iget-object v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@17
    new-instance v3, Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-direct {v3, p0, v4}, Landroid/webkit/WebViewClassic$InvokeListBox$Container;-><init>(Landroid/webkit/WebViewClassic$InvokeListBox;Landroid/webkit/WebViewClassic$1;)V

    #@1d
    aput-object v3, v2, v0

    #@1f
    .line 9983
    iget-object v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@21
    aget-object v2, v2, v0

    #@23
    aget-object v3, p2, v0

    #@25
    iput-object v3, v2, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mString:Ljava/lang/String;

    #@27
    .line 9984
    iget-object v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@29
    aget-object v2, v2, v0

    #@2b
    aget v3, p3, v0

    #@2d
    iput v3, v2, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mEnabled:I

    #@2f
    .line 9985
    iget-object v2, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@31
    aget-object v2, v2, v0

    #@33
    iput v0, v2, Landroid/webkit/WebViewClassic$InvokeListBox$Container;->mId:I

    #@35
    .line 9981
    add-int/lit8 v0, v0, 0x1

    #@37
    goto :goto_13

    #@38
    .line 9987
    :cond_38
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/WebViewClassic;[Ljava/lang/String;[I[ILandroid/webkit/WebViewClassic$1;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 9831
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClassic$InvokeListBox;-><init>(Landroid/webkit/WebViewClassic;[Ljava/lang/String;[I[I)V

    #@3
    return-void
.end method

.method static synthetic access$9000(Landroid/webkit/WebViewClassic$InvokeListBox;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 9831
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mMultiple:Z

    #@2
    return v0
.end method

.method static synthetic access$9100(Landroid/webkit/WebViewClassic$InvokeListBox;)[Landroid/webkit/WebViewClassic$InvokeListBox$Container;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 9831
    iget-object v0, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mContainers:[Landroid/webkit/WebViewClassic$InvokeListBox$Container;

    #@2
    return-object v0
.end method

.method static synthetic access$9200(Landroid/webkit/WebViewClassic$InvokeListBox;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 9831
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mIsDatalist:Z

    #@2
    return v0
.end method


# virtual methods
.method public run()V
    .registers 13

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 10052
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@3
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->access$1900(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore;

    #@6
    move-result-object v1

    #@7
    if-eqz v1, :cond_21

    #@9
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@b
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Landroid/webkit/WebView;->getWindowToken()Landroid/os/IBinder;

    #@12
    move-result-object v1

    #@13
    if-eqz v1, :cond_21

    #@15
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@17
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Landroid/webkit/WebView;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@1e
    move-result-object v1

    #@1f
    if-nez v1, :cond_22

    #@21
    .line 10159
    :cond_21
    :goto_21
    return-void

    #@22
    .line 10058
    :cond_22
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@24
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@27
    move-result-object v1

    #@28
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@2b
    move-result-object v1

    #@2c
    const v3, 0x10900c6

    #@2f
    const/4 v11, 0x0

    #@30
    invoke-virtual {v1, v3, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@33
    move-result-object v4

    #@34
    check-cast v4, Landroid/widget/ListView;

    #@36
    .line 10060
    .local v4, listView:Landroid/widget/ListView;
    new-instance v5, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;

    #@38
    invoke-direct {v5, p0}, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;-><init>(Landroid/webkit/WebViewClassic$InvokeListBox;)V

    #@3b
    .line 10064
    .local v5, adapter:Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@3d
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@40
    move-result-object v10

    #@41
    .line 10065
    .local v10, settings:Landroid/webkit/WebSettings;
    if-eqz v10, :cond_da

    #@43
    invoke-virtual {v10}, Landroid/webkit/WebSettingsClassic;->getFloatingMode()Z

    #@46
    move-result v1

    #@47
    if-eqz v1, :cond_da

    #@49
    .line 10066
    new-instance v9, Landroid/util/TypedValue;

    #@4b
    invoke-direct {v9}, Landroid/util/TypedValue;-><init>()V

    #@4e
    .line 10067
    .local v9, outValue:Landroid/util/TypedValue;
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@50
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@57
    move-result-object v1

    #@58
    const v3, 0x1010309

    #@5b
    invoke-virtual {v1, v3, v9, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@5e
    .line 10069
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@60
    iget-object v3, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@62
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@69
    move-result-object v3

    #@6a
    iget v11, v9, Landroid/util/TypedValue;->resourceId:I

    #@6c
    invoke-direct {v1, v3, v11}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@6f
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    #@76
    move-result-object v1

    #@77
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    #@7a
    move-result-object v6

    #@7b
    .line 10080
    .end local v9           #outValue:Landroid/util/TypedValue;
    .local v6, b:Landroid/app/AlertDialog$Builder;
    :goto_7b
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mMultiple:Z

    #@7d
    if-eqz v1, :cond_94

    #@7f
    .line 10081
    const v1, 0x104000a

    #@82
    new-instance v3, Landroid/webkit/WebViewClassic$InvokeListBox$1;

    #@84
    invoke-direct {v3, p0, v5, v4}, Landroid/webkit/WebViewClassic$InvokeListBox$1;-><init>(Landroid/webkit/WebViewClassic$InvokeListBox;Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;Landroid/widget/ListView;)V

    #@87
    invoke-virtual {v6, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@8a
    .line 10089
    const/high16 v1, 0x104

    #@8c
    new-instance v3, Landroid/webkit/WebViewClassic$InvokeListBox$2;

    #@8e
    invoke-direct {v3, p0}, Landroid/webkit/WebViewClassic$InvokeListBox$2;-><init>(Landroid/webkit/WebViewClassic$InvokeListBox;)V

    #@91
    invoke-virtual {v6, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@94
    .line 10097
    :cond_94
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@96
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@99
    move-result-object v3

    #@9a
    invoke-static {v1, v3}, Landroid/webkit/WebViewClassic;->access$9402(Landroid/webkit/WebViewClassic;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    #@9d
    .line 10100
    if-eqz v10, :cond_b4

    #@9f
    invoke-virtual {v10}, Landroid/webkit/WebSettingsClassic;->getFloatingMode()Z

    #@a2
    move-result v1

    #@a3
    if-eqz v1, :cond_b4

    #@a5
    .line 10101
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@a7
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->access$9400(Landroid/webkit/WebViewClassic;)Landroid/app/AlertDialog;

    #@aa
    move-result-object v1

    #@ab
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@ae
    move-result-object v1

    #@af
    const/16 v3, 0x7d2

    #@b1
    invoke-virtual {v1, v3}, Landroid/view/Window;->setType(I)V

    #@b4
    .line 10105
    :cond_b4
    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@b7
    .line 10106
    invoke-virtual {v4, v2}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    #@ba
    .line 10113
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mMultiple:Z

    #@bc
    if-nez v1, :cond_f2

    #@be
    move v1, v2

    #@bf
    :goto_bf
    invoke-virtual {v4, v1}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    #@c2
    .line 10114
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mMultiple:Z

    #@c4
    if-eqz v1, :cond_f4

    #@c6
    .line 10115
    const/4 v1, 0x2

    #@c7
    invoke-virtual {v4, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    #@ca
    .line 10116
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mSelectedArray:[I

    #@cc
    array-length v8, v1

    #@cd
    .line 10117
    .local v8, length:I
    const/4 v7, 0x0

    #@ce
    .local v7, i:I
    :goto_ce
    if-ge v7, v8, :cond_11d

    #@d0
    .line 10118
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mSelectedArray:[I

    #@d2
    aget v1, v1, v7

    #@d4
    invoke-virtual {v4, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    #@d7
    .line 10117
    add-int/lit8 v7, v7, 0x1

    #@d9
    goto :goto_ce

    #@da
    .line 10074
    .end local v6           #b:Landroid/app/AlertDialog$Builder;
    .end local v7           #i:I
    .end local v8           #length:I
    :cond_da
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@dc
    iget-object v3, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@de
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@e1
    move-result-object v3

    #@e2
    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@e5
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    #@e8
    move-result-object v1

    #@e9
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    #@ec
    move-result-object v1

    #@ed
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    #@f0
    move-result-object v6

    #@f1
    .restart local v6       #b:Landroid/app/AlertDialog$Builder;
    goto :goto_7b

    #@f2
    .line 10113
    :cond_f2
    const/4 v1, 0x0

    #@f3
    goto :goto_bf

    #@f4
    .line 10121
    :cond_f4
    new-instance v1, Landroid/webkit/WebViewClassic$InvokeListBox$3;

    #@f6
    invoke-direct {v1, p0}, Landroid/webkit/WebViewClassic$InvokeListBox$3;-><init>(Landroid/webkit/WebViewClassic$InvokeListBox;)V

    #@f9
    invoke-virtual {v4, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@fc
    .line 10135
    iget v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mSelection:I

    #@fe
    const/4 v3, -0x1

    #@ff
    if-eq v1, v3, :cond_11d

    #@101
    .line 10136
    iget v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mSelection:I

    #@103
    invoke-virtual {v4, v1}, Landroid/widget/ListView;->setSelection(I)V

    #@106
    .line 10137
    invoke-virtual {v4, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    #@109
    .line 10138
    iget v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mSelection:I

    #@10b
    invoke-virtual {v4, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    #@10e
    .line 10139
    new-instance v0, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;

    #@110
    iget v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->mSelection:I

    #@112
    invoke-virtual {v5, v1}, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->getItemId(I)J

    #@115
    move-result-wide v2

    #@116
    move-object v1, p0

    #@117
    invoke-direct/range {v0 .. v5}, Landroid/webkit/WebViewClassic$InvokeListBox$SingleDataSetObserver;-><init>(Landroid/webkit/WebViewClassic$InvokeListBox;JLandroid/widget/ListView;Landroid/widget/Adapter;)V

    #@11a
    .line 10141
    .local v0, observer:Landroid/database/DataSetObserver;
    invoke-virtual {v5, v0}, Landroid/webkit/WebViewClassic$InvokeListBox$MyArrayListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@11d
    .line 10144
    .end local v0           #observer:Landroid/database/DataSetObserver;
    :cond_11d
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@11f
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->access$9400(Landroid/webkit/WebViewClassic;)Landroid/app/AlertDialog;

    #@122
    move-result-object v1

    #@123
    new-instance v2, Landroid/webkit/WebViewClassic$InvokeListBox$4;

    #@125
    invoke-direct {v2, p0}, Landroid/webkit/WebViewClassic$InvokeListBox$4;-><init>(Landroid/webkit/WebViewClassic$InvokeListBox;)V

    #@128
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    #@12b
    .line 10158
    iget-object v1, p0, Landroid/webkit/WebViewClassic$InvokeListBox;->this$0:Landroid/webkit/WebViewClassic;

    #@12d
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->access$9400(Landroid/webkit/WebViewClassic;)Landroid/app/AlertDialog;

    #@130
    move-result-object v1

    #@131
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@134
    goto/16 :goto_21
.end method
