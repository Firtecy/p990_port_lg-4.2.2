.class public Landroid/webkit/WebResourceResponse;
.super Ljava/lang/Object;
.source "WebResourceResponse.java"


# instance fields
.field private mEncoding:Ljava/lang/String;

.field private mInputStream:Ljava/io/InputStream;

.field private mMimeType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V
    .registers 4
    .parameter "mimeType"
    .parameter "encoding"
    .parameter "data"

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    iput-object p1, p0, Landroid/webkit/WebResourceResponse;->mMimeType:Ljava/lang/String;

    #@5
    .line 47
    iput-object p2, p0, Landroid/webkit/WebResourceResponse;->mEncoding:Ljava/lang/String;

    #@7
    .line 48
    iput-object p3, p0, Landroid/webkit/WebResourceResponse;->mInputStream:Ljava/io/InputStream;

    #@9
    .line 49
    return-void
.end method


# virtual methods
.method public getData()Ljava/io/InputStream;
    .registers 2

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Landroid/webkit/WebResourceResponse;->mInputStream:Ljava/io/InputStream;

    #@2
    return-object v0
.end method

.method public getEncoding()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Landroid/webkit/WebResourceResponse;->mEncoding:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Landroid/webkit/WebResourceResponse;->mMimeType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public setData(Ljava/io/InputStream;)V
    .registers 2
    .parameter "data"

    #@0
    .prologue
    .line 95
    iput-object p1, p0, Landroid/webkit/WebResourceResponse;->mInputStream:Ljava/io/InputStream;

    #@2
    .line 96
    return-void
.end method

.method public setEncoding(Ljava/lang/String;)V
    .registers 2
    .parameter "encoding"

    #@0
    .prologue
    .line 76
    iput-object p1, p0, Landroid/webkit/WebResourceResponse;->mEncoding:Ljava/lang/String;

    #@2
    .line 77
    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .registers 2
    .parameter "mimeType"

    #@0
    .prologue
    .line 57
    iput-object p1, p0, Landroid/webkit/WebResourceResponse;->mMimeType:Ljava/lang/String;

    #@2
    .line 58
    return-void
.end method
