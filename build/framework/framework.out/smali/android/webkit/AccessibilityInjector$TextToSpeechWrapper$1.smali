.class Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$1;
.super Ljava/lang/Object;
.source "AccessibilityInjector.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;


# direct methods
.method constructor <init>(Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 753
    iput-object p1, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$1;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onInit(I)V
    .registers 6
    .parameter "status"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 757
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$1;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@3
    invoke-static {v0}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->access$600(Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;)Landroid/speech/tts/TextToSpeech;

    #@6
    move-result-object v0

    #@7
    if-nez v0, :cond_f

    #@9
    .line 758
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$1;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@b
    invoke-static {v0, v1}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->access$702(Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;Z)Z

    #@e
    .line 777
    :goto_e
    return-void

    #@f
    .line 762
    :cond_f
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$1;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@11
    invoke-static {v0}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->access$600(Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;)Landroid/speech/tts/TextToSpeech;

    #@14
    move-result-object v1

    #@15
    monitor-enter v1

    #@16
    .line 763
    :try_start_16
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$1;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@18
    invoke-static {v0}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->access$800(Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;)Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_57

    #@1e
    if-nez p1, :cond_57

    #@20
    .line 764
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_4c

    #@26
    .line 765
    invoke-static {}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->access$900()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "["

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    iget-object v3, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$1;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@37
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    #@3a
    move-result v3

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    const-string v3, "] Initialized successfully"

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 768
    :cond_4c
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$1;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@4e
    const/4 v2, 0x1

    #@4f
    invoke-static {v0, v2}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->access$702(Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;Z)Z

    #@52
    .line 776
    :goto_52
    monitor-exit v1

    #@53
    goto :goto_e

    #@54
    :catchall_54
    move-exception v0

    #@55
    monitor-exit v1
    :try_end_56
    .catchall {:try_start_16 .. :try_end_56} :catchall_54

    #@56
    throw v0

    #@57
    .line 770
    :cond_57
    :try_start_57
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@5a
    move-result v0

    #@5b
    if-eqz v0, :cond_83

    #@5d
    .line 771
    invoke-static {}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->access$900()Ljava/lang/String;

    #@60
    move-result-object v0

    #@61
    new-instance v2, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v3, "["

    #@68
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v2

    #@6c
    iget-object v3, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$1;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@6e
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    #@71
    move-result v3

    #@72
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v2

    #@76
    const-string v3, "] Failed to initialize"

    #@78
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v2

    #@7c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v2

    #@80
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 774
    :cond_83
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper$1;->this$0:Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;

    #@85
    const/4 v2, 0x0

    #@86
    invoke-static {v0, v2}, Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;->access$702(Landroid/webkit/AccessibilityInjector$TextToSpeechWrapper;Z)Z
    :try_end_89
    .catchall {:try_start_57 .. :try_end_89} :catchall_54

    #@89
    goto :goto_52
.end method
