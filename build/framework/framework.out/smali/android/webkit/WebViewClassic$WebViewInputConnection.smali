.class Landroid/webkit/WebViewClassic$WebViewInputConnection;
.super Landroid/view/inputmethod/BaseInputConnection;
.source "WebViewClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WebViewInputConnection"
.end annotation


# instance fields
.field private isKeyBoardInput:Z

.field private mBatchLevel:I

.field private mHint:Ljava/lang/String;

.field private mImeOptions:I

.field private mInputType:I

.field private mIsAutoCompleteEnabled:Z

.field private mIsAutoFillable:Z

.field private mIsKeySentByMe:Z

.field private mKeyCharacterMap:Landroid/view/KeyCharacterMap;

.field private mMaxLength:I

.field private mName:Ljava/lang/String;

.field final synthetic this$0:Landroid/webkit/WebViewClassic;


# direct methods
.method public constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 217
    iput-object p1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    .line 218
    invoke-static {p1}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x1

    #@7
    invoke-direct {p0, v0, v1}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    #@a
    .line 210
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->isKeyBoardInput:Z

    #@d
    .line 219
    return-void
.end method

.method private checkRightCursorMove()Z
    .registers 4

    #@0
    .prologue
    .line 312
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v1

    #@4
    .line 313
    .local v1, content:Landroid/text/Editable;
    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@7
    move-result v0

    #@8
    .line 314
    .local v0, b:I
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    #@b
    move-result v2

    #@c
    if-ge v0, v2, :cond_10

    #@e
    .line 315
    const/4 v2, 0x1

    #@f
    .line 317
    :goto_f
    return v2

    #@10
    :cond_10
    const/4 v2, 0x0

    #@11
    goto :goto_f
.end method

.method private getSelectionCountAndPrepareDelete(Z)I
    .registers 10
    .parameter "isBackward"

    #@0
    .prologue
    .line 262
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v2

    #@4
    .line 263
    .local v2, content:Landroid/text/Editable;
    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@7
    move-result v0

    #@8
    .line 264
    .local v0, a:I
    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@b
    move-result v1

    #@c
    .line 265
    .local v1, b:I
    if-ne v0, v1, :cond_51

    #@e
    .line 267
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@10
    if-eqz v6, :cond_4f

    #@12
    .line 268
    if-eqz p1, :cond_2e

    #@14
    add-int/lit8 v3, v0, -0x2

    #@16
    .line 269
    .local v3, index:I
    :goto_16
    if-ltz v3, :cond_30

    #@18
    add-int/lit8 v6, v3, 0x2

    #@1a
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    #@1d
    move-result v7

    #@1e
    if-gt v6, v7, :cond_30

    #@20
    .line 270
    add-int/lit8 v6, v3, 0x2

    #@22
    invoke-interface {v2, v3, v6}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@25
    move-result-object v5

    #@26
    .line 271
    .local v5, text:Ljava/lang/CharSequence;
    invoke-static {v5}, Landroid/text/Layout;->hasEmoji(Ljava/lang/CharSequence;)Z

    #@29
    move-result v6

    #@2a
    if-eqz v6, :cond_30

    #@2c
    .line 272
    const/4 v6, 0x2

    #@2d
    .line 295
    .end local v3           #index:I
    .end local v5           #text:Ljava/lang/CharSequence;
    :goto_2d
    return v6

    #@2e
    :cond_2e
    move v3, v0

    #@2f
    .line 268
    goto :goto_16

    #@30
    .line 275
    .restart local v3       #index:I
    :cond_30
    if-eqz p1, :cond_4d

    #@32
    add-int/lit8 v3, v0, -0x4

    #@34
    .line 276
    :goto_34
    if-ltz v3, :cond_4f

    #@36
    add-int/lit8 v6, v3, 0x4

    #@38
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    #@3b
    move-result v7

    #@3c
    if-gt v6, v7, :cond_4f

    #@3e
    .line 277
    add-int/lit8 v6, v3, 0x4

    #@40
    invoke-interface {v2, v3, v6}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@43
    move-result-object v5

    #@44
    .line 278
    .restart local v5       #text:Ljava/lang/CharSequence;
    const/4 v6, 0x0

    #@45
    invoke-static {v5, v6}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@48
    move-result v6

    #@49
    if-eqz v6, :cond_4f

    #@4b
    .line 279
    const/4 v6, 0x4

    #@4c
    goto :goto_2d

    #@4d
    .end local v5           #text:Ljava/lang/CharSequence;
    :cond_4d
    move v3, v0

    #@4e
    .line 275
    goto :goto_34

    #@4f
    .line 284
    .end local v3           #index:I
    :cond_4f
    const/4 v6, 0x1

    #@50
    goto :goto_2d

    #@51
    .line 286
    :cond_51
    if-le v0, v1, :cond_56

    #@53
    .line 287
    move v4, v0

    #@54
    .line 288
    .local v4, temp:I
    move v0, v1

    #@55
    .line 289
    move v1, v4

    #@56
    .line 291
    .end local v4           #temp:I
    :cond_56
    if-eqz p1, :cond_5e

    #@58
    .line 292
    invoke-static {v2, v1, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@5b
    .line 295
    :goto_5b
    sub-int v6, v1, v0

    #@5d
    goto :goto_2d

    #@5e
    .line 294
    :cond_5e
    invoke-static {v2, v0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@61
    goto :goto_5b
.end method

.method private limitReplaceTextByMaxLength(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;
    .registers 8
    .parameter "text"
    .parameter "numReplaced"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 788
    iget v2, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mMaxLength:I

    #@3
    if-lez v2, :cond_20

    #@5
    .line 789
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@8
    move-result-object v0

    #@9
    .line 790
    .local v0, editable:Landroid/text/Editable;
    iget v2, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mMaxLength:I

    #@b
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@e
    move-result v3

    #@f
    sub-int/2addr v2, v3

    #@10
    add-int v1, v2, p2

    #@12
    .line 791
    .local v1, maxReplace:I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@15
    move-result v2

    #@16
    if-ge v1, v2, :cond_20

    #@18
    .line 792
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    #@1b
    move-result v1

    #@1c
    .line 794
    invoke-interface {p1, v4, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@1f
    move-result-object p1

    #@20
    .line 797
    .end local v0           #editable:Landroid/text/Editable;
    .end local v1           #maxReplace:I
    :cond_20
    return-object p1
.end method

.method private moveCurosor(Z)Z
    .registers 7
    .parameter "isBackward"

    #@0
    .prologue
    .line 300
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v2

    #@4
    .line 301
    .local v2, content:Landroid/text/Editable;
    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@7
    move-result v0

    #@8
    .line 302
    .local v0, a:I
    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@b
    move-result v1

    #@c
    .line 303
    .local v1, b:I
    if-ne v0, v1, :cond_19

    #@e
    .line 304
    if-eqz p1, :cond_1b

    #@10
    if-lez v0, :cond_1b

    #@12
    .line 305
    add-int/lit8 v3, v0, -0x1

    #@14
    add-int/lit8 v4, v0, -0x1

    #@16
    invoke-static {v2, v3, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@19
    .line 309
    :cond_19
    :goto_19
    const/4 v3, 0x1

    #@1a
    return v3

    #@1b
    .line 306
    :cond_1b
    if-nez p1, :cond_19

    #@1d
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->checkRightCursorMove()Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_19

    #@23
    .line 307
    add-int/lit8 v3, v0, 0x1

    #@25
    add-int/lit8 v4, v0, 0x1

    #@27
    invoke-static {v2, v3, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@2a
    goto :goto_19
.end method

.method private restartInput()V
    .registers 3

    #@0
    .prologue
    .line 801
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@3
    move-result-object v0

    #@4
    .line 802
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_f

    #@6
    .line 805
    iget-object v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@8
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    #@f
    .line 807
    :cond_f
    return-void
.end method

.method private sendCharacter(C)V
    .registers 12
    .parameter "c"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 753
    iget-object v7, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mKeyCharacterMap:Landroid/view/KeyCharacterMap;

    #@3
    if-nez v7, :cond_c

    #@5
    .line 754
    const/4 v7, -0x1

    #@6
    invoke-static {v7}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    #@9
    move-result-object v7

    #@a
    iput-object v7, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mKeyCharacterMap:Landroid/view/KeyCharacterMap;

    #@c
    .line 756
    :cond_c
    const/4 v7, 0x1

    #@d
    new-array v1, v7, [C

    #@f
    .line 757
    .local v1, chars:[C
    aput-char p1, v1, v9

    #@11
    .line 758
    iget-object v7, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mKeyCharacterMap:Landroid/view/KeyCharacterMap;

    #@13
    invoke-virtual {v7, v1}, Landroid/view/KeyCharacterMap;->getEvents([C)[Landroid/view/KeyEvent;

    #@16
    move-result-object v3

    #@17
    .line 759
    .local v3, events:[Landroid/view/KeyEvent;
    if-eqz v3, :cond_26

    #@19
    .line 760
    move-object v0, v3

    #@1a
    .local v0, arr$:[Landroid/view/KeyEvent;
    array-length v5, v0

    #@1b
    .local v5, len$:I
    const/4 v4, 0x0

    #@1c
    .local v4, i$:I
    :goto_1c
    if-ge v4, v5, :cond_37

    #@1e
    aget-object v2, v0, v4

    #@20
    .line 761
    .local v2, event:Landroid/view/KeyEvent;
    invoke-virtual {p0, v2}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@23
    .line 760
    add-int/lit8 v4, v4, 0x1

    #@25
    goto :goto_1c

    #@26
    .line 764
    .end local v0           #arr$:[Landroid/view/KeyEvent;
    .end local v2           #event:Landroid/view/KeyEvent;
    .end local v4           #i$:I
    .end local v5           #len$:I
    :cond_26
    iget-object v7, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@28
    iget-object v7, v7, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@2a
    const/16 v8, 0x91

    #@2c
    invoke-virtual {v7, v8, p1, v9}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@2f
    move-result-object v6

    #@30
    .line 765
    .local v6, msg:Landroid/os/Message;
    iget-object v7, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@32
    iget-object v7, v7, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@34
    invoke-virtual {v7, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@37
    .line 767
    .end local v6           #msg:Landroid/os/Message;
    :cond_37
    return-void
.end method

.method private sendKey(I)V
    .registers 17
    .parameter "keyCode"

    #@0
    .prologue
    .line 775
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v1

    #@4
    .line 776
    .local v1, eventTime:J
    new-instance v0, Landroid/view/KeyEvent;

    #@6
    const/4 v5, 0x0

    #@7
    const/4 v7, 0x0

    #@8
    const/4 v8, 0x0

    #@9
    const/4 v9, -0x1

    #@a
    const/4 v10, 0x0

    #@b
    const/4 v11, 0x2

    #@c
    move-wide v3, v1

    #@d
    move/from16 v6, p1

    #@f
    invoke-direct/range {v0 .. v11}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@12
    invoke-virtual {p0, v0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@15
    .line 780
    new-instance v3, Landroid/view/KeyEvent;

    #@17
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1a
    move-result-wide v4

    #@1b
    const/4 v8, 0x1

    #@1c
    const/4 v10, 0x0

    #@1d
    const/4 v11, 0x0

    #@1e
    const/4 v12, -0x1

    #@1f
    const/4 v13, 0x0

    #@20
    const/4 v14, 0x2

    #@21
    move-wide v6, v1

    #@22
    move/from16 v9, p1

    #@24
    invoke-direct/range {v3 .. v14}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@27
    invoke-virtual {p0, v3}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@2a
    .line 784
    return-void
.end method

.method private setNewText(IILjava/lang/CharSequence;)V
    .registers 18
    .parameter "start"
    .parameter "end"
    .parameter "text"

    #@0
    .prologue
    .line 705
    const/4 v11, 0x1

    #@1
    iput-boolean v11, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mIsKeySentByMe:Z

    #@3
    .line 706
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@6
    move-result-object v1

    #@7
    .line 707
    .local v1, editable:Landroid/text/Editable;
    move/from16 v0, p2

    #@9
    invoke-interface {v1, p1, v0}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@c
    move-result-object v5

    #@d
    .line 708
    .local v5, original:Ljava/lang/CharSequence;
    const/4 v2, 0x0

    #@e
    .line 709
    .local v2, isCharacterAdd:Z
    const/4 v3, 0x0

    #@f
    .line 710
    .local v3, isCharacterDelete:Z
    invoke-interface/range {p3 .. p3}, Ljava/lang/CharSequence;->length()I

    #@12
    move-result v10

    #@13
    .line 711
    .local v10, textLength:I
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    #@16
    move-result v6

    #@17
    .line 712
    .local v6, originalLength:I
    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@1a
    move-result v9

    #@1b
    .line 713
    .local v9, selectionStart:I
    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@1e
    move-result v8

    #@1f
    .line 714
    .local v8, selectionEnd:I
    if-ne v9, v8, :cond_32

    #@21
    .line 715
    if-le v10, v6, :cond_79

    #@23
    .line 716
    add-int/lit8 v11, v6, 0x1

    #@25
    if-ne v10, v11, :cond_77

    #@27
    const/4 v11, 0x0

    #@28
    const/4 v12, 0x0

    #@29
    move-object/from16 v0, p3

    #@2b
    invoke-static {v0, v11, v5, v12, v6}, Landroid/text/TextUtils;->regionMatches(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    #@2e
    move-result v11

    #@2f
    if-eqz v11, :cond_77

    #@31
    const/4 v2, 0x1

    #@32
    .line 725
    :cond_32
    :goto_32
    if-eqz v2, :cond_8d

    #@34
    .line 726
    add-int/lit8 v11, v10, -0x1

    #@36
    move-object/from16 v0, p3

    #@38
    invoke-interface {v0, v11}, Ljava/lang/CharSequence;->charAt(I)C

    #@3b
    move-result v11

    #@3c
    invoke-direct {p0, v11}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->sendCharacter(C)V

    #@3f
    .line 738
    :cond_3f
    :goto_3f
    iget-object v11, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@41
    invoke-static {v11}, Landroid/webkit/WebViewClassic;->access$600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AutoCompletePopup;

    #@44
    move-result-object v11

    #@45
    if-eqz v11, :cond_73

    #@47
    .line 739
    new-instance v4, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    .line 740
    .local v4, newText:Ljava/lang/StringBuilder;
    const/4 v11, 0x0

    #@4d
    invoke-interface {v1, v11, p1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@50
    move-result-object v11

    #@51
    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@54
    .line 741
    move-object/from16 v0, p3

    #@56
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@59
    .line 742
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    #@5c
    move-result v11

    #@5d
    move/from16 v0, p2

    #@5f
    invoke-interface {v1, v0, v11}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@62
    move-result-object v11

    #@63
    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@66
    .line 743
    iget-object v11, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@68
    invoke-static {v11}, Landroid/webkit/WebViewClassic;->access$600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AutoCompletePopup;

    #@6b
    move-result-object v11

    #@6c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v12

    #@70
    invoke-virtual {v11, v12}, Landroid/webkit/AutoCompletePopup;->setText(Ljava/lang/CharSequence;)V

    #@73
    .line 745
    .end local v4           #newText:Ljava/lang/StringBuilder;
    :cond_73
    const/4 v11, 0x0

    #@74
    iput-boolean v11, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mIsKeySentByMe:Z

    #@76
    .line 746
    return-void

    #@77
    .line 716
    :cond_77
    const/4 v2, 0x0

    #@78
    goto :goto_32

    #@79
    .line 719
    :cond_79
    if-le v6, v10, :cond_32

    #@7b
    .line 720
    add-int/lit8 v11, v6, -0x1

    #@7d
    if-ne v10, v11, :cond_8b

    #@7f
    const/4 v11, 0x0

    #@80
    const/4 v12, 0x0

    #@81
    move-object/from16 v0, p3

    #@83
    invoke-static {v0, v11, v5, v12, v10}, Landroid/text/TextUtils;->regionMatches(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    #@86
    move-result v11

    #@87
    if-eqz v11, :cond_8b

    #@89
    const/4 v3, 0x1

    #@8a
    :goto_8a
    goto :goto_32

    #@8b
    :cond_8b
    const/4 v3, 0x0

    #@8c
    goto :goto_8a

    #@8d
    .line 727
    :cond_8d
    if-eqz v3, :cond_95

    #@8f
    .line 728
    const/16 v11, 0x43

    #@91
    invoke-direct {p0, v11}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->sendKey(I)V

    #@94
    goto :goto_3f

    #@95
    .line 729
    :cond_95
    if-ne v10, v6, :cond_a1

    #@97
    const/4 v11, 0x0

    #@98
    const/4 v12, 0x0

    #@99
    move-object/from16 v0, p3

    #@9b
    invoke-static {v0, v11, v5, v12, v10}, Landroid/text/TextUtils;->regionMatches(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    #@9e
    move-result v11

    #@9f
    if-nez v11, :cond_3f

    #@a1
    .line 734
    :cond_a1
    iget-object v11, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@a3
    iget-object v11, v11, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@a5
    const/16 v12, 0x8f

    #@a7
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@aa
    move-result-object v13

    #@ab
    move/from16 v0, p2

    #@ad
    invoke-virtual {v11, v12, p1, v0, v13}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@b0
    move-result-object v7

    #@b1
    .line 736
    .local v7, replaceMessage:Landroid/os/Message;
    iget-object v11, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@b3
    iget-object v11, v11, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@b5
    invoke-virtual {v11, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@b8
    goto :goto_3f
.end method

.method private updateSelection()V
    .registers 8

    #@0
    .prologue
    .line 684
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v6

    #@4
    .line 685
    .local v6, editable:Landroid/text/Editable;
    invoke-static {v6}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@7
    move-result v2

    #@8
    .line 686
    .local v2, selectionStart:I
    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@b
    move-result v3

    #@c
    .line 687
    .local v3, selectionEnd:I
    invoke-static {v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    #@f
    move-result v4

    #@10
    .line 688
    .local v4, composingStart:I
    invoke-static {v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    #@13
    move-result v5

    #@14
    .line 689
    .local v5, composingEnd:I
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@17
    move-result-object v0

    #@18
    .line 690
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_23

    #@1a
    .line 691
    iget-object v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@1c
    invoke-static {v1}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual/range {v0 .. v5}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    #@23
    .line 694
    :cond_23
    return-void
.end method


# virtual methods
.method public beginBatchEdit()Z
    .registers 2

    #@0
    .prologue
    .line 236
    iget v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mBatchLevel:I

    #@2
    if-nez v0, :cond_9

    #@4
    .line 237
    iget-object v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@6
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->access$200(Landroid/webkit/WebViewClassic;)V

    #@9
    .line 239
    :cond_9
    iget v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mBatchLevel:I

    #@b
    add-int/lit8 v0, v0, 0x1

    #@d
    iput v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mBatchLevel:I

    #@f
    .line 240
    const/4 v0, 0x0

    #@10
    return v0
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .registers 4
    .parameter "text"
    .parameter "newCursorPosition"

    #@0
    .prologue
    .line 503
    invoke-virtual {p0, p1, p2}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    #@3
    .line 504
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->finishComposingText()Z

    #@6
    .line 505
    const/4 v0, 0x1

    #@7
    return v0
.end method

.method public deleteSurroundingText(II)Z
    .registers 13
    .parameter "leftLength"
    .parameter "rightLength"

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    .line 512
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@4
    move-result-object v4

    #@5
    .line 513
    .local v4, content:Landroid/text/Editable;
    invoke-static {v4}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@8
    move-result v0

    #@9
    .line 514
    .local v0, a:I
    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@c
    move-result v1

    #@d
    .line 516
    .local v1, b:I
    if-le v0, v1, :cond_12

    #@f
    .line 517
    move v7, v0

    #@10
    .line 518
    .local v7, tmp:I
    move v0, v1

    #@11
    .line 519
    move v1, v7

    #@12
    .line 522
    .end local v7           #tmp:I
    :cond_12
    invoke-static {v4}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    #@15
    move-result v2

    #@16
    .line 523
    .local v2, ca:I
    invoke-static {v4}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    #@19
    move-result v3

    #@1a
    .line 524
    .local v3, cb:I
    if-ge v3, v2, :cond_1f

    #@1c
    .line 525
    move v7, v2

    #@1d
    .line 526
    .restart local v7       #tmp:I
    move v2, v3

    #@1e
    .line 527
    move v3, v7

    #@1f
    .line 529
    .end local v7           #tmp:I
    :cond_1f
    if-eq v2, v8, :cond_29

    #@21
    if-eq v3, v8, :cond_29

    #@23
    .line 530
    if-ge v2, v0, :cond_26

    #@25
    move v0, v2

    #@26
    .line 531
    :cond_26
    if-le v3, v1, :cond_29

    #@28
    move v1, v3

    #@29
    .line 534
    :cond_29
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    #@2c
    move-result v8

    #@2d
    add-int v9, v1, p2

    #@2f
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    #@32
    move-result v5

    #@33
    .line 535
    .local v5, endDelete:I
    if-le v5, v1, :cond_3a

    #@35
    .line 536
    const-string v8, ""

    #@37
    invoke-direct {p0, v1, v5, v8}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setNewText(IILjava/lang/CharSequence;)V

    #@3a
    .line 538
    :cond_3a
    const/4 v8, 0x0

    #@3b
    sub-int v9, v0, p1

    #@3d
    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    #@40
    move-result v6

    #@41
    .line 539
    .local v6, startDelete:I
    if-ge v6, v0, :cond_48

    #@43
    .line 540
    const-string v8, ""

    #@45
    invoke-direct {p0, v6, v0, v8}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setNewText(IILjava/lang/CharSequence;)V

    #@48
    .line 542
    :cond_48
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->deleteSurroundingText(II)Z

    #@4b
    move-result v8

    #@4c
    return v8
.end method

.method public endBatchEdit()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 245
    iget v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mBatchLevel:I

    #@3
    add-int/lit8 v0, v0, -0x1

    #@5
    iput v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mBatchLevel:I

    #@7
    .line 246
    iget v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mBatchLevel:I

    #@9
    if-nez v0, :cond_10

    #@b
    .line 247
    iget-object v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@d
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->access$300(Landroid/webkit/WebViewClassic;)V

    #@10
    .line 250
    :cond_10
    iget v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mBatchLevel:I

    #@12
    if-gez v0, :cond_16

    #@14
    .line 251
    iput v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mBatchLevel:I

    #@16
    .line 253
    :cond_16
    return v1
.end method

.method public getIsAutoFillable()Z
    .registers 2

    #@0
    .prologue
    .line 257
    iget-boolean v0, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mIsAutoFillable:Z

    #@2
    return v0
.end method

.method public initEditorInfo(Landroid/webkit/WebViewCore$TextFieldInitData;)V
    .registers 7
    .parameter "initData"

    #@0
    .prologue
    .line 578
    iget v3, p1, Landroid/webkit/WebViewCore$TextFieldInitData;->mType:I

    #@2
    .line 579
    .local v3, type:I
    const/16 v2, 0xa1

    #@4
    .line 581
    .local v2, inputType:I
    const/high16 v1, 0x1200

    #@6
    .line 583
    .local v1, imeOptions:I
    iget-boolean v4, p1, Landroid/webkit/WebViewCore$TextFieldInitData;->mIsSpellCheckEnabled:Z

    #@8
    if-nez v4, :cond_d

    #@a
    .line 584
    const/high16 v4, 0x8

    #@c
    or-int/2addr v2, v4

    #@d
    .line 586
    :cond_d
    const/4 v4, 0x1

    #@e
    if-eq v4, v3, :cond_1e

    #@10
    .line 587
    iget-boolean v4, p1, Landroid/webkit/WebViewCore$TextFieldInitData;->mIsTextFieldNext:Z

    #@12
    if-eqz v4, :cond_17

    #@14
    .line 588
    const/high16 v4, 0x800

    #@16
    or-int/2addr v1, v4

    #@17
    .line 590
    :cond_17
    iget-boolean v4, p1, Landroid/webkit/WebViewCore$TextFieldInitData;->mIsTextFieldPrev:Z

    #@19
    if-eqz v4, :cond_1e

    #@1b
    .line 591
    const/high16 v4, 0x400

    #@1d
    or-int/2addr v1, v4

    #@1e
    .line 594
    :cond_1e
    const/4 v0, 0x2

    #@1f
    .line 595
    .local v0, action:I
    packed-switch v3, :pswitch_data_62

    #@22
    .line 635
    :goto_22
    :pswitch_22
    or-int/2addr v1, v0

    #@23
    .line 636
    iget-object v4, p1, Landroid/webkit/WebViewCore$TextFieldInitData;->mLabel:Ljava/lang/String;

    #@25
    iput-object v4, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mHint:Ljava/lang/String;

    #@27
    .line 637
    iput v2, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mInputType:I

    #@29
    .line 638
    iput v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mImeOptions:I

    #@2b
    .line 639
    iget v4, p1, Landroid/webkit/WebViewCore$TextFieldInitData;->mMaxLength:I

    #@2d
    iput v4, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mMaxLength:I

    #@2f
    .line 640
    iget v4, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mMaxLength:I

    #@31
    if-gez v4, :cond_38

    #@33
    const v4, 0xb3b0

    #@36
    iput v4, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mMaxLength:I

    #@38
    .line 641
    :cond_38
    iget-boolean v4, p1, Landroid/webkit/WebViewCore$TextFieldInitData;->mIsAutoCompleteEnabled:Z

    #@3a
    iput-boolean v4, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mIsAutoCompleteEnabled:Z

    #@3c
    .line 642
    iget-object v4, p1, Landroid/webkit/WebViewCore$TextFieldInitData;->mName:Ljava/lang/String;

    #@3e
    iput-object v4, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mName:Ljava/lang/String;

    #@40
    .line 643
    iget-object v4, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@42
    invoke-static {v4}, Landroid/webkit/WebViewClassic;->access$600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AutoCompletePopup;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Landroid/webkit/AutoCompletePopup;->clearAdapter()V

    #@49
    .line 644
    return-void

    #@4a
    .line 599
    :pswitch_4a
    const v4, 0x2c000

    #@4d
    or-int/2addr v2, v4

    #@4e
    .line 602
    const/4 v0, 0x1

    #@4f
    .line 603
    goto :goto_22

    #@50
    .line 605
    :pswitch_50
    or-int/lit16 v2, v2, 0xe0

    #@52
    .line 606
    goto :goto_22

    #@53
    .line 608
    :pswitch_53
    const/4 v0, 0x3

    #@54
    .line 609
    goto :goto_22

    #@55
    .line 612
    :pswitch_55
    const/16 v2, 0xd1

    #@57
    .line 614
    goto :goto_22

    #@58
    .line 617
    :pswitch_58
    const/16 v2, 0x3002

    #@5a
    .line 621
    goto :goto_22

    #@5b
    .line 624
    :pswitch_5b
    const/4 v2, 0x3

    #@5c
    .line 625
    goto :goto_22

    #@5d
    .line 628
    :pswitch_5d
    const/16 v2, 0x11

    #@5f
    .line 629
    or-int/lit8 v1, v1, 0x2

    #@61
    .line 631
    goto :goto_22

    #@62
    .line 595
    :pswitch_data_62
    .packed-switch 0x0
        :pswitch_22
        :pswitch_4a
        :pswitch_50
        :pswitch_53
        :pswitch_55
        :pswitch_58
        :pswitch_5b
        :pswitch_5d
    .end packed-switch
.end method

.method public performEditorAction(I)Z
    .registers 9
    .parameter "editorAction"

    #@0
    .prologue
    const/16 v6, 0x42

    #@2
    const/4 v5, 0x1

    #@3
    .line 548
    const/4 v0, 0x1

    #@4
    .line 549
    .local v0, handled:Z
    packed-switch p1, :pswitch_data_4a

    #@7
    .line 570
    :pswitch_7
    invoke-super {p0, p1}, Landroid/view/inputmethod/BaseInputConnection;->performEditorAction(I)Z

    #@a
    move-result v0

    #@b
    .line 574
    :goto_b
    return v0

    #@c
    .line 551
    :pswitch_c
    iget-object v2, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@e
    invoke-static {v2}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@11
    move-result-object v2

    #@12
    const/4 v3, 0x2

    #@13
    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->requestFocus(I)Z

    #@16
    goto :goto_b

    #@17
    .line 554
    :pswitch_17
    iget-object v2, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@19
    invoke-static {v2}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, v5}, Landroid/webkit/WebView;->requestFocus(I)Z

    #@20
    goto :goto_b

    #@21
    .line 557
    :pswitch_21
    iget-object v2, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@23
    invoke-static {v2}, Landroid/webkit/WebViewClassic;->access$500(Landroid/webkit/WebViewClassic;)V

    #@26
    goto :goto_b

    #@27
    .line 561
    :pswitch_27
    iget-object v2, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@29
    invoke-static {v2}, Landroid/webkit/WebViewClassic;->access$500(Landroid/webkit/WebViewClassic;)V

    #@2c
    .line 562
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    .line 563
    .local v1, text:Ljava/lang/String;
    iget-object v2, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@36
    new-instance v3, Landroid/view/KeyEvent;

    #@38
    const/4 v4, 0x0

    #@39
    invoke-direct {v3, v4, v6}, Landroid/view/KeyEvent;-><init>(II)V

    #@3c
    invoke-virtual {v2, v1, v3}, Landroid/webkit/WebViewClassic;->passToJavaScript(Ljava/lang/String;Landroid/view/KeyEvent;)V

    #@3f
    .line 565
    iget-object v2, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@41
    new-instance v3, Landroid/view/KeyEvent;

    #@43
    invoke-direct {v3, v5, v6}, Landroid/view/KeyEvent;-><init>(II)V

    #@46
    invoke-virtual {v2, v1, v3}, Landroid/webkit/WebViewClassic;->passToJavaScript(Ljava/lang/String;Landroid/view/KeyEvent;)V

    #@49
    goto :goto_b

    #@4a
    .line 549
    :pswitch_data_4a
    .packed-switch 0x2
        :pswitch_27
        :pswitch_27
        :pswitch_7
        :pswitch_c
        :pswitch_21
        :pswitch_17
    .end packed-switch
.end method

.method public replaceSelection(Ljava/lang/CharSequence;)V
    .registers 7
    .parameter "text"

    #@0
    .prologue
    .line 453
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v0

    #@4
    .line 454
    .local v0, editable:Landroid/text/Editable;
    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@7
    move-result v3

    #@8
    .line 455
    .local v3, selectionStart:I
    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@b
    move-result v2

    #@c
    .line 456
    .local v2, selectionEnd:I
    sub-int v4, v2, v3

    #@e
    invoke-direct {p0, p1, v4}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->limitReplaceTextByMaxLength(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    #@11
    move-result-object p1

    #@12
    .line 457
    invoke-direct {p0, v3, v2, p1}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setNewText(IILjava/lang/CharSequence;)V

    #@15
    .line 458
    invoke-interface {v0, v3, v2, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@18
    .line 459
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->endBatchEdit()Z

    #@1b
    .line 460
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->restartInput()V

    #@1e
    .line 462
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@21
    move-result v4

    #@22
    add-int v1, v3, v4

    #@24
    .line 463
    .local v1, newCaret:I
    invoke-virtual {p0, v1, v1}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setSelection(II)Z

    #@27
    .line 464
    return-void
.end method

.method public sendKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 13
    .parameter "event"

    #@0
    .prologue
    const/16 v10, 0x70

    #@2
    const/16 v7, 0x43

    #@4
    const/16 v9, 0x16

    #@6
    const/4 v8, 0x0

    #@7
    const/4 v5, 0x1

    #@8
    .line 325
    iget-boolean v6, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mIsKeySentByMe:Z

    #@a
    if-nez v6, :cond_166

    #@c
    .line 327
    iget-boolean v6, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->isKeyBoardInput:Z

    #@e
    if-eqz v6, :cond_a8

    #@10
    .line 328
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@13
    move-result v6

    #@14
    if-nez v6, :cond_7e

    #@16
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@19
    move-result v6

    #@1a
    if-eq v6, v10, :cond_7e

    #@1c
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@1f
    move-result v6

    #@20
    if-eq v6, v7, :cond_7e

    #@22
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    #@25
    move-result v6

    #@26
    if-eqz v6, :cond_7e

    #@28
    .line 332
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    #@2b
    move-result v6

    #@2c
    int-to-char v6, v6

    #@2d
    invoke-static {v6}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    .line 338
    .local v1, newComposingText:Ljava/lang/String;
    const/4 v2, -0x1

    #@32
    .local v2, start:I
    const/4 v0, -0x1

    #@33
    .line 339
    .local v0, end:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@36
    move-result-object v6

    #@37
    invoke-static {v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    #@3a
    move-result v2

    #@3b
    if-ltz v2, :cond_79

    #@3d
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@40
    move-result-object v6

    #@41
    invoke-static {v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    #@44
    move-result v0

    #@45
    if-ltz v0, :cond_79

    #@47
    .line 340
    if-eq v0, v2, :cond_6f

    #@49
    .line 341
    if-le v2, v0, :cond_4e

    #@4b
    .line 342
    move v3, v2

    #@4c
    .line 343
    .local v3, temp:I
    move v2, v0

    #@4d
    .line 344
    move v0, v3

    #@4e
    .line 346
    .end local v3           #temp:I
    :cond_4e
    sub-int v6, v0, v2

    #@50
    new-array v4, v6, [C

    #@52
    .line 347
    .local v4, tempChar:[C
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@55
    move-result-object v6

    #@56
    invoke-interface {v6, v2, v0, v4, v8}, Landroid/text/Editable;->getChars(II[CI)V

    #@59
    .line 348
    new-instance v6, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    new-instance v7, Ljava/lang/String;

    #@60
    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    #@63
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v6

    #@67
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v6

    #@6b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v1

    #@6f
    .line 350
    .end local v4           #tempChar:[C
    :cond_6f
    invoke-virtual {p0, v1, v5}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    #@72
    .line 351
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->endBatchEdit()Z

    #@75
    .line 352
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->restartInput()V

    #@78
    .line 425
    .end local v0           #end:I
    .end local v1           #newComposingText:Ljava/lang/String;
    .end local v2           #start:I
    :cond_78
    :goto_78
    return v5

    #@79
    .line 356
    .restart local v0       #end:I
    .restart local v1       #newComposingText:Ljava/lang/String;
    .restart local v2       #start:I
    :cond_79
    invoke-virtual {p0, v1, v5}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    #@7c
    move-result v5

    #@7d
    goto :goto_78

    #@7e
    .line 359
    .end local v0           #end:I
    .end local v1           #newComposingText:Ljava/lang/String;
    .end local v2           #start:I
    :cond_7e
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@81
    move-result v6

    #@82
    if-ne v6, v5, :cond_78

    #@84
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@87
    move-result v6

    #@88
    const/16 v7, 0x15

    #@8a
    if-eq v6, v7, :cond_a2

    #@8c
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@8f
    move-result v6

    #@90
    if-eq v6, v9, :cond_a2

    #@92
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@95
    move-result v6

    #@96
    const/16 v7, 0x13

    #@98
    if-eq v6, v7, :cond_a2

    #@9a
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@9d
    move-result v6

    #@9e
    const/16 v7, 0x14

    #@a0
    if-ne v6, v7, :cond_78

    #@a2
    .line 364
    :cond_a2
    iget-object v6, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@a4
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->sendUpdatePosMessage()V

    #@a7
    goto :goto_78

    #@a8
    .line 371
    :cond_a8
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@ab
    move-result v6

    #@ac
    if-ne v6, v5, :cond_142

    #@ae
    .line 372
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@b1
    move-result v6

    #@b2
    if-ne v6, v7, :cond_bd

    #@b4
    .line 373
    invoke-direct {p0, v5}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getSelectionCountAndPrepareDelete(Z)I

    #@b7
    move-result v5

    #@b8
    invoke-virtual {p0, v5, v8}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->deleteSurroundingText(II)Z

    #@bb
    move-result v5

    #@bc
    goto :goto_78

    #@bd
    .line 374
    :cond_bd
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@c0
    move-result v6

    #@c1
    if-ne v6, v10, :cond_cc

    #@c3
    .line 375
    invoke-direct {p0, v8}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getSelectionCountAndPrepareDelete(Z)I

    #@c6
    move-result v5

    #@c7
    invoke-virtual {p0, v8, v5}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->deleteSurroundingText(II)Z

    #@ca
    move-result v5

    #@cb
    goto :goto_78

    #@cc
    .line 377
    :cond_cc
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@cf
    move-result v6

    #@d0
    const/16 v7, 0x15

    #@d2
    if-ne v6, v7, :cond_d9

    #@d4
    .line 378
    invoke-direct {p0, v5}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->moveCurosor(Z)Z

    #@d7
    move-result v5

    #@d8
    goto :goto_78

    #@d9
    .line 379
    :cond_d9
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@dc
    move-result v6

    #@dd
    if-ne v6, v9, :cond_e4

    #@df
    .line 380
    invoke-direct {p0, v8}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->moveCurosor(Z)Z

    #@e2
    move-result v5

    #@e3
    goto :goto_78

    #@e4
    .line 382
    :cond_e4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    #@e7
    move-result v6

    #@e8
    if-eqz v6, :cond_166

    #@ea
    .line 383
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    #@ed
    move-result v6

    #@ee
    int-to-char v6, v6

    #@ef
    invoke-static {v6}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    #@f2
    move-result-object v1

    #@f3
    .line 389
    .restart local v1       #newComposingText:Ljava/lang/String;
    const/4 v2, -0x1

    #@f4
    .restart local v2       #start:I
    const/4 v0, -0x1

    #@f5
    .line 390
    .restart local v0       #end:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@f8
    move-result-object v6

    #@f9
    invoke-static {v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    #@fc
    move-result v2

    #@fd
    if-ltz v2, :cond_13c

    #@ff
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@102
    move-result-object v6

    #@103
    invoke-static {v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    #@106
    move-result v0

    #@107
    if-ltz v0, :cond_13c

    #@109
    .line 391
    if-eq v0, v2, :cond_131

    #@10b
    .line 392
    if-le v2, v0, :cond_110

    #@10d
    .line 393
    move v3, v2

    #@10e
    .line 394
    .restart local v3       #temp:I
    move v2, v0

    #@10f
    .line 395
    move v0, v3

    #@110
    .line 397
    .end local v3           #temp:I
    :cond_110
    sub-int v6, v0, v2

    #@112
    new-array v4, v6, [C

    #@114
    .line 398
    .restart local v4       #tempChar:[C
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@117
    move-result-object v6

    #@118
    invoke-interface {v6, v2, v0, v4, v8}, Landroid/text/Editable;->getChars(II[CI)V

    #@11b
    .line 399
    new-instance v6, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    new-instance v7, Ljava/lang/String;

    #@122
    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    #@125
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v6

    #@129
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v6

    #@12d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v1

    #@131
    .line 401
    .end local v4           #tempChar:[C
    :cond_131
    invoke-virtual {p0, v1, v5}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    #@134
    .line 402
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->endBatchEdit()Z

    #@137
    .line 403
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->restartInput()V

    #@13a
    goto/16 :goto_78

    #@13c
    .line 407
    :cond_13c
    invoke-virtual {p0, v1, v5}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    #@13f
    move-result v5

    #@140
    goto/16 :goto_78

    #@142
    .line 409
    .end local v0           #end:I
    .end local v1           #newComposingText:Ljava/lang/String;
    .end local v2           #start:I
    :cond_142
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@145
    move-result v6

    #@146
    if-nez v6, :cond_166

    #@148
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@14b
    move-result v6

    #@14c
    if-eq v6, v7, :cond_78

    #@14e
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@151
    move-result v6

    #@152
    if-eq v6, v10, :cond_78

    #@154
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@157
    move-result v6

    #@158
    if-ne v6, v9, :cond_160

    #@15a
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->checkRightCursorMove()Z

    #@15d
    move-result v6

    #@15e
    if-eqz v6, :cond_78

    #@160
    :cond_160
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    #@163
    move-result v6

    #@164
    if-nez v6, :cond_78

    #@166
    .line 418
    :cond_166
    iget-boolean v6, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->isKeyBoardInput:Z

    #@168
    if-nez v6, :cond_78

    #@16a
    .line 424
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    #@16d
    move-result v5

    #@16e
    or-int/lit8 v5, v5, 0x4

    #@170
    invoke-static {p1, v5}, Landroid/view/KeyEvent;->changeFlags(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@173
    move-result-object p1

    #@174
    .line 425
    invoke-super {p0, p1}, Landroid/view/inputmethod/BaseInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@177
    move-result v5

    #@178
    goto/16 :goto_78
.end method

.method public setAutoFillable(I)V
    .registers 8
    .parameter "queryId"

    #@0
    .prologue
    .line 222
    iget-object v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/webkit/WebSettingsClassic;->getAutoFillEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_40

    #@c
    const/4 v1, -0x1

    #@d
    if-eq p1, v1, :cond_40

    #@f
    const/4 v1, 0x1

    #@10
    :goto_10
    iput-boolean v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mIsAutoFillable:Z

    #@12
    .line 224
    iget v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mInputType:I

    #@14
    and-int/lit16 v0, v1, 0xff0

    #@16
    .line 225
    .local v0, variation:I
    const/16 v1, 0xe0

    #@18
    if-eq v0, v1, :cond_3f

    #@1a
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mIsAutoFillable:Z

    #@1c
    if-nez v1, :cond_22

    #@1e
    iget-boolean v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mIsAutoCompleteEnabled:Z

    #@20
    if-eqz v1, :cond_3f

    #@22
    .line 227
    :cond_22
    iget-object v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mName:Ljava/lang/String;

    #@24
    if-eqz v1, :cond_3f

    #@26
    iget-object v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mName:Ljava/lang/String;

    #@28
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@2b
    move-result v1

    #@2c
    if-lez v1, :cond_3f

    #@2e
    .line 228
    iget-object v1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@30
    iget-object v2, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mName:Ljava/lang/String;

    #@32
    iget-object v3, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@34
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$100(Landroid/webkit/WebViewClassic;)I

    #@37
    move-result v3

    #@38
    iget-boolean v4, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mIsAutoFillable:Z

    #@3a
    iget-boolean v5, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mIsAutoCompleteEnabled:Z

    #@3c
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/webkit/WebViewClassic;->requestFormData(Ljava/lang/String;IZZ)V

    #@3f
    .line 232
    :cond_3f
    return-void

    #@40
    .line 222
    .end local v0           #variation:I
    :cond_40
    const/4 v1, 0x0

    #@41
    goto :goto_10
.end method

.method public setComposingRegion(II)Z
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 675
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingRegion(II)Z

    #@3
    move-result v0

    #@4
    .line 676
    .local v0, result:Z
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->updateSelection()V

    #@7
    .line 677
    return v0
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .registers 11
    .parameter "text"
    .parameter "newCursorPosition"

    #@0
    .prologue
    .line 469
    iget-object v6, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-static {v6}, Landroid/webkit/WebViewClassic;->access$400(Landroid/webkit/WebViewClassic;)Z

    #@5
    move-result v6

    #@6
    if-eqz v6, :cond_d

    #@8
    .line 470
    iget-object v6, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->this$0:Landroid/webkit/WebViewClassic;

    #@a
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@d
    .line 473
    :cond_d
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@10
    move-result-object v0

    #@11
    .line 474
    .local v0, editable:Landroid/text/Editable;
    invoke-static {v0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    #@14
    move-result v4

    #@15
    .line 475
    .local v4, start:I
    invoke-static {v0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    #@18
    move-result v1

    #@19
    .line 476
    .local v1, end:I
    if-ltz v4, :cond_1d

    #@1b
    if-gez v1, :cond_25

    #@1d
    .line 477
    :cond_1d
    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@20
    move-result v4

    #@21
    .line 478
    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@24
    move-result v1

    #@25
    .line 480
    :cond_25
    if-ge v1, v4, :cond_2a

    #@27
    .line 481
    move v5, v1

    #@28
    .line 482
    .local v5, temp:I
    move v1, v4

    #@29
    .line 483
    move v4, v5

    #@2a
    .line 485
    .end local v5           #temp:I
    :cond_2a
    sub-int v6, v1, v4

    #@2c
    invoke-direct {p0, p1, v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->limitReplaceTextByMaxLength(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    #@2f
    move-result-object v3

    #@30
    .line 486
    .local v3, limitedText:Ljava/lang/CharSequence;
    invoke-direct {p0, v4, v1, v3}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setNewText(IILjava/lang/CharSequence;)V

    #@33
    .line 487
    if-eq v3, p1, :cond_3f

    #@35
    .line 488
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@38
    move-result v6

    #@39
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    #@3c
    move-result v7

    #@3d
    sub-int/2addr v6, v7

    #@3e
    sub-int/2addr p2, v6

    #@3f
    .line 490
    :cond_3f
    invoke-super {p0, v3, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    #@42
    .line 491
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->updateSelection()V

    #@45
    .line 492
    if-eq v3, p1, :cond_56

    #@47
    .line 493
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->endBatchEdit()Z

    #@4a
    .line 494
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    #@4d
    move-result v6

    #@4e
    add-int v2, v4, v6

    #@50
    .line 495
    .local v2, lastCaret:I
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->finishComposingText()Z

    #@53
    .line 496
    invoke-virtual {p0, v2, v2}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setSelection(II)Z

    #@56
    .line 498
    .end local v2           #lastCaret:I
    :cond_56
    const/4 v6, 0x1

    #@57
    return v6
.end method

.method public setKeyBoardInput(Z)V
    .registers 2
    .parameter "fromKeyBoard"

    #@0
    .prologue
    .line 213
    iput-boolean p1, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->isKeyBoardInput:Z

    #@2
    .line 214
    return-void
.end method

.method public setSelection(II)Z
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 668
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setSelection(II)Z

    #@3
    move-result v0

    #@4
    .line 669
    .local v0, result:Z
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->updateSelection()V

    #@7
    .line 670
    return v0
.end method

.method public setTextAndKeepSelection(Ljava/lang/CharSequence;)V
    .registers 7
    .parameter "text"

    #@0
    .prologue
    .line 436
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v0

    #@4
    .line 437
    .local v0, editable:Landroid/text/Editable;
    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@7
    move-result v2

    #@8
    .line 438
    .local v2, selectionStart:I
    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@b
    move-result v1

    #@c
    .line 439
    .local v1, selectionEnd:I
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@f
    move-result v3

    #@10
    invoke-direct {p0, p1, v3}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->limitReplaceTextByMaxLength(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    #@13
    move-result-object p1

    #@14
    .line 440
    const/4 v3, 0x0

    #@15
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@18
    move-result v4

    #@19
    invoke-interface {v0, v3, v4, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@1c
    .line 441
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->endBatchEdit()Z

    #@1f
    .line 442
    invoke-direct {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->restartInput()V

    #@22
    .line 445
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@25
    move-result v3

    #@26
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@29
    move-result v2

    #@2a
    .line 446
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@2d
    move-result v3

    #@2e
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    #@31
    move-result v1

    #@32
    .line 448
    invoke-virtual {p0, v2, v1}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setSelection(II)Z

    #@35
    .line 449
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->finishComposingText()Z

    #@38
    .line 450
    return-void
.end method

.method public setupEditorInfo(Landroid/view/inputmethod/EditorInfo;)V
    .registers 6
    .parameter "outAttrs"

    #@0
    .prologue
    .line 647
    iget v3, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mInputType:I

    #@2
    iput v3, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    #@4
    .line 648
    iget v3, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mImeOptions:I

    #@6
    iput v3, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@8
    .line 649
    iget-object v3, p0, Landroid/webkit/WebViewClassic$WebViewInputConnection;->mHint:Ljava/lang/String;

    #@a
    iput-object v3, p1, Landroid/view/inputmethod/EditorInfo;->hintText:Ljava/lang/CharSequence;

    #@c
    .line 650
    const/4 v3, 0x1

    #@d
    invoke-virtual {p0, v3}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getCursorCapsMode(I)I

    #@10
    move-result v3

    #@11
    iput v3, p1, Landroid/view/inputmethod/EditorInfo;->initialCapsMode:I

    #@13
    .line 653
    invoke-static {}, Landroid/webkit/WebViewClassic;->access$700()Z

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_37

    #@19
    const-string v3, "com.lge.ime.opacity=85"

    #@1b
    :goto_1b
    iput-object v3, p1, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    #@1d
    .line 655
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@20
    move-result-object v0

    #@21
    .line 656
    .local v0, editable:Landroid/text/Editable;
    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@24
    move-result v2

    #@25
    .line 657
    .local v2, selectionStart:I
    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@28
    move-result v1

    #@29
    .line 658
    .local v1, selectionEnd:I
    if-ltz v2, :cond_2d

    #@2b
    if-gez v1, :cond_32

    #@2d
    .line 659
    :cond_2d
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@30
    move-result v2

    #@31
    .line 660
    move v1, v2

    #@32
    .line 662
    :cond_32
    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    #@34
    .line 663
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    #@36
    .line 664
    return-void

    #@37
    .line 653
    .end local v0           #editable:Landroid/text/Editable;
    .end local v1           #selectionEnd:I
    .end local v2           #selectionStart:I
    :cond_37
    const-string v3, "com.lge.ime.opacity=100"

    #@39
    goto :goto_1b
.end method

.method public updateSelectionPos(I)V
    .registers 3
    .parameter "selectionStart"

    #@0
    .prologue
    .line 430
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v0

    #@4
    .line 431
    .local v0, editable:Landroid/text/Editable;
    invoke-virtual {p0, p1, p1}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setSelection(II)Z

    #@7
    .line 432
    return-void
.end method
