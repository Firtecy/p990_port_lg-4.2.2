.class final Landroid/webkit/DeviceOrientationService;
.super Ljava/lang/Object;
.source "DeviceOrientationService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final DELTA_DEGRESS:D = 1.0


# instance fields
.field private mAlpha:Ljava/lang/Double;

.field private mBeta:Ljava/lang/Double;

.field private mContext:Landroid/content/Context;

.field private mGamma:Ljava/lang/Double;

.field private mGravityVector:[F

.field private mHandler:Landroid/os/Handler;

.field private mHaveSentErrorEvent:Z

.field private mIsRunning:Z

.field private mMagneticFieldVector:[F

.field private mManager:Landroid/webkit/DeviceMotionAndOrientationManager;

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 30
    const-class v0, Landroid/webkit/DeviceOrientationService;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/DeviceOrientationService;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>(Landroid/webkit/DeviceMotionAndOrientationManager;Landroid/content/Context;)V
    .registers 4
    .parameter "manager"
    .parameter "context"

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 49
    iput-object p1, p0, Landroid/webkit/DeviceOrientationService;->mManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@5
    .line 50
    sget-boolean v0, Landroid/webkit/DeviceOrientationService;->$assertionsDisabled:Z

    #@7
    if-nez v0, :cond_13

    #@9
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@b
    if-nez v0, :cond_13

    #@d
    new-instance v0, Ljava/lang/AssertionError;

    #@f
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@12
    throw v0

    #@13
    .line 51
    :cond_13
    iput-object p2, p0, Landroid/webkit/DeviceOrientationService;->mContext:Landroid/content/Context;

    #@15
    .line 52
    sget-boolean v0, Landroid/webkit/DeviceOrientationService;->$assertionsDisabled:Z

    #@17
    if-nez v0, :cond_23

    #@19
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mContext:Landroid/content/Context;

    #@1b
    if-nez v0, :cond_23

    #@1d
    new-instance v0, Ljava/lang/AssertionError;

    #@1f
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@22
    throw v0

    #@23
    .line 53
    :cond_23
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/DeviceOrientationService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-boolean v0, p0, Landroid/webkit/DeviceOrientationService;->mIsRunning:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/webkit/DeviceOrientationService;)Landroid/webkit/DeviceMotionAndOrientationManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@2
    return-object v0
.end method

.method private getOrientationUsingGetRotationMatrix()V
    .registers 14

    #@0
    .prologue
    const-wide v11, 0x4076800000000000L

    #@5
    .line 106
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mGravityVector:[F

    #@7
    if-eqz v0, :cond_d

    #@9
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mMagneticFieldVector:[F

    #@b
    if-nez v0, :cond_e

    #@d
    .line 134
    :cond_d
    :goto_d
    return-void

    #@e
    .line 112
    :cond_e
    const/16 v0, 0x9

    #@10
    new-array v7, v0, [F

    #@12
    .line 113
    .local v7, deviceRotationMatrix:[F
    const/4 v0, 0x0

    #@13
    iget-object v9, p0, Landroid/webkit/DeviceOrientationService;->mGravityVector:[F

    #@15
    iget-object v10, p0, Landroid/webkit/DeviceOrientationService;->mMagneticFieldVector:[F

    #@17
    invoke-static {v7, v0, v9, v10}, Landroid/hardware/SensorManager;->getRotationMatrix([F[F[F[F)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_d

    #@1d
    .line 124
    const/4 v0, 0x3

    #@1e
    new-array v8, v0, [F

    #@20
    .line 125
    .local v8, rotationAngles:[F
    invoke-static {v7, v8}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    #@23
    .line 126
    const/4 v0, 0x0

    #@24
    aget v0, v8, v0

    #@26
    neg-float v0, v0

    #@27
    float-to-double v9, v0

    #@28
    invoke-static {v9, v10}, Ljava/lang/Math;->toDegrees(D)D

    #@2b
    move-result-wide v1

    #@2c
    .line 127
    .local v1, alpha:D
    :goto_2c
    const-wide/16 v9, 0x0

    #@2e
    cmpg-double v0, v1, v9

    #@30
    if-gez v0, :cond_34

    #@32
    add-double/2addr v1, v11

    #@33
    goto :goto_2c

    #@34
    .line 128
    :cond_34
    const/4 v0, 0x1

    #@35
    aget v0, v8, v0

    #@37
    neg-float v0, v0

    #@38
    float-to-double v9, v0

    #@39
    invoke-static {v9, v10}, Ljava/lang/Math;->toDegrees(D)D

    #@3c
    move-result-wide v3

    #@3d
    .line 129
    .local v3, beta:D
    :goto_3d
    const-wide v9, -0x3f99800000000000L

    #@42
    cmpg-double v0, v3, v9

    #@44
    if-gez v0, :cond_48

    #@46
    add-double/2addr v3, v11

    #@47
    goto :goto_3d

    #@48
    .line 130
    :cond_48
    const/4 v0, 0x2

    #@49
    aget v0, v8, v0

    #@4b
    float-to-double v9, v0

    #@4c
    invoke-static {v9, v10}, Ljava/lang/Math;->toDegrees(D)D

    #@4f
    move-result-wide v5

    #@50
    .line 131
    .local v5, gamma:D
    :goto_50
    const-wide v9, -0x3fa9800000000000L

    #@55
    cmpg-double v0, v5, v9

    #@57
    if-gez v0, :cond_5b

    #@59
    add-double/2addr v5, v11

    #@5a
    goto :goto_50

    #@5b
    :cond_5b
    move-object v0, p0

    #@5c
    .line 133
    invoke-direct/range {v0 .. v6}, Landroid/webkit/DeviceOrientationService;->maybeSendChange(DDD)V

    #@5f
    goto :goto_d
.end method

.method private getSensorManager()Landroid/hardware/SensorManager;
    .registers 3

    #@0
    .prologue
    .line 137
    sget-boolean v0, Landroid/webkit/DeviceOrientationService;->$assertionsDisabled:Z

    #@2
    if-nez v0, :cond_1a

    #@4
    const-string v0, "WebViewCoreThread"

    #@6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1a

    #@14
    new-instance v0, Ljava/lang/AssertionError;

    #@16
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@19
    throw v0

    #@1a
    .line 138
    :cond_1a
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mSensorManager:Landroid/hardware/SensorManager;

    #@1c
    if-nez v0, :cond_2b

    #@1e
    .line 139
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mContext:Landroid/content/Context;

    #@20
    const-string/jumbo v1, "sensor"

    #@23
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    check-cast v0, Landroid/hardware/SensorManager;

    #@29
    iput-object v0, p0, Landroid/webkit/DeviceOrientationService;->mSensorManager:Landroid/hardware/SensorManager;

    #@2b
    .line 141
    :cond_2b
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mSensorManager:Landroid/hardware/SensorManager;

    #@2d
    return-object v0
.end method

.method private maybeSendChange(DDD)V
    .registers 11
    .parameter "alpha"
    .parameter "beta"
    .parameter "gamma"

    #@0
    .prologue
    const-wide/high16 v2, 0x3ff0

    #@2
    .line 169
    sget-boolean v0, Landroid/webkit/DeviceOrientationService;->$assertionsDisabled:Z

    #@4
    if-nez v0, :cond_1c

    #@6
    const-string v0, "WebViewCoreThread"

    #@8
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_1c

    #@16
    new-instance v0, Ljava/lang/AssertionError;

    #@18
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@1b
    throw v0

    #@1c
    .line 170
    :cond_1c
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mAlpha:Ljava/lang/Double;

    #@1e
    if-eqz v0, :cond_58

    #@20
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mBeta:Ljava/lang/Double;

    #@22
    if-eqz v0, :cond_58

    #@24
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mGamma:Ljava/lang/Double;

    #@26
    if-eqz v0, :cond_58

    #@28
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mAlpha:Ljava/lang/Double;

    #@2a
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    #@2d
    move-result-wide v0

    #@2e
    sub-double v0, p1, v0

    #@30
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    #@33
    move-result-wide v0

    #@34
    cmpl-double v0, v0, v2

    #@36
    if-gtz v0, :cond_58

    #@38
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mBeta:Ljava/lang/Double;

    #@3a
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    #@3d
    move-result-wide v0

    #@3e
    sub-double v0, p3, v0

    #@40
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    #@43
    move-result-wide v0

    #@44
    cmpl-double v0, v0, v2

    #@46
    if-gtz v0, :cond_58

    #@48
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mGamma:Ljava/lang/Double;

    #@4a
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    #@4d
    move-result-wide v0

    #@4e
    sub-double v0, p5, v0

    #@50
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    #@53
    move-result-wide v0

    #@54
    cmpl-double v0, v0, v2

    #@56
    if-lez v0, :cond_78

    #@58
    .line 174
    :cond_58
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@5b
    move-result-object v0

    #@5c
    iput-object v0, p0, Landroid/webkit/DeviceOrientationService;->mAlpha:Ljava/lang/Double;

    #@5e
    .line 175
    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@61
    move-result-object v0

    #@62
    iput-object v0, p0, Landroid/webkit/DeviceOrientationService;->mBeta:Ljava/lang/Double;

    #@64
    .line 176
    invoke-static {p5, p6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@67
    move-result-object v0

    #@68
    iput-object v0, p0, Landroid/webkit/DeviceOrientationService;->mGamma:Ljava/lang/Double;

    #@6a
    .line 177
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mManager:Landroid/webkit/DeviceMotionAndOrientationManager;

    #@6c
    iget-object v1, p0, Landroid/webkit/DeviceOrientationService;->mAlpha:Ljava/lang/Double;

    #@6e
    iget-object v2, p0, Landroid/webkit/DeviceOrientationService;->mBeta:Ljava/lang/Double;

    #@70
    iget-object v3, p0, Landroid/webkit/DeviceOrientationService;->mGamma:Ljava/lang/Double;

    #@72
    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/DeviceMotionAndOrientationManager;->onOrientationChange(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)V

    #@75
    .line 179
    const/4 v0, 0x0

    #@76
    iput-boolean v0, p0, Landroid/webkit/DeviceOrientationService;->mHaveSentErrorEvent:Z

    #@78
    .line 181
    :cond_78
    return-void
.end method

.method private registerForAccelerometerSensor()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 145
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->getSensorManager()Landroid/hardware/SensorManager;

    #@4
    move-result-object v1

    #@5
    const/4 v3, 0x1

    #@6
    invoke-virtual {v1, v3}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    #@9
    move-result-object v0

    #@a
    .line 146
    .local v0, sensors:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    move v1, v2

    #@11
    .line 150
    :goto_11
    return v1

    #@12
    :cond_12
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->getSensorManager()Landroid/hardware/SensorManager;

    #@15
    move-result-object v3

    #@16
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Landroid/hardware/Sensor;

    #@1c
    iget-object v4, p0, Landroid/webkit/DeviceOrientationService;->mHandler:Landroid/os/Handler;

    #@1e
    invoke-virtual {v3, p0, v1, v2, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    #@21
    move-result v1

    #@22
    goto :goto_11
.end method

.method private registerForMagneticFieldSensor()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 155
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->getSensorManager()Landroid/hardware/SensorManager;

    #@4
    move-result-object v1

    #@5
    const/4 v3, 0x2

    #@6
    invoke-virtual {v1, v3}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    #@9
    move-result-object v0

    #@a
    .line 156
    .local v0, sensors:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    move v1, v2

    #@11
    .line 160
    :goto_11
    return v1

    #@12
    :cond_12
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->getSensorManager()Landroid/hardware/SensorManager;

    #@15
    move-result-object v3

    #@16
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Landroid/hardware/Sensor;

    #@1c
    iget-object v4, p0, Landroid/webkit/DeviceOrientationService;->mHandler:Landroid/os/Handler;

    #@1e
    invoke-virtual {v3, p0, v1, v2, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    #@21
    move-result v1

    #@22
    goto :goto_11
.end method

.method private registerForSensors()V
    .registers 2

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mHandler:Landroid/os/Handler;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 97
    new-instance v0, Landroid/os/Handler;

    #@6
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@9
    iput-object v0, p0, Landroid/webkit/DeviceOrientationService;->mHandler:Landroid/os/Handler;

    #@b
    .line 99
    :cond_b
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->registerForAccelerometerSensor()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_17

    #@11
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->registerForMagneticFieldSensor()Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_1d

    #@17
    .line 100
    :cond_17
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->unregisterFromSensors()V

    #@1a
    .line 101
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->sendErrorEvent()V

    #@1d
    .line 103
    :cond_1d
    return-void
.end method

.method private sendErrorEvent()V
    .registers 3

    #@0
    .prologue
    .line 78
    sget-boolean v0, Landroid/webkit/DeviceOrientationService;->$assertionsDisabled:Z

    #@2
    if-nez v0, :cond_1a

    #@4
    const-string v0, "WebViewCoreThread"

    #@6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1a

    #@14
    new-instance v0, Ljava/lang/AssertionError;

    #@16
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@19
    throw v0

    #@1a
    .line 80
    :cond_1a
    iget-boolean v0, p0, Landroid/webkit/DeviceOrientationService;->mHaveSentErrorEvent:Z

    #@1c
    if-eqz v0, :cond_1f

    #@1e
    .line 93
    :goto_1e
    return-void

    #@1f
    .line 82
    :cond_1f
    const/4 v0, 0x1

    #@20
    iput-boolean v0, p0, Landroid/webkit/DeviceOrientationService;->mHaveSentErrorEvent:Z

    #@22
    .line 83
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mHandler:Landroid/os/Handler;

    #@24
    new-instance v1, Landroid/webkit/DeviceOrientationService$1;

    #@26
    invoke-direct {v1, p0}, Landroid/webkit/DeviceOrientationService$1;-><init>(Landroid/webkit/DeviceOrientationService;)V

    #@29
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@2c
    goto :goto_1e
.end method

.method private unregisterFromSensors()V
    .registers 2

    #@0
    .prologue
    .line 165
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->getSensorManager()Landroid/hardware/SensorManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@7
    .line 166
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 5
    .parameter "sensor"
    .parameter "accuracy"

    #@0
    .prologue
    .line 223
    sget-boolean v0, Landroid/webkit/DeviceOrientationService;->$assertionsDisabled:Z

    #@2
    if-nez v0, :cond_1a

    #@4
    const-string v0, "WebViewCoreThread"

    #@6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1a

    #@14
    new-instance v0, Ljava/lang/AssertionError;

    #@16
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@19
    throw v0

    #@1a
    .line 224
    :cond_1a
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 8
    .parameter "event"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 189
    sget-boolean v0, Landroid/webkit/DeviceOrientationService;->$assertionsDisabled:Z

    #@6
    if-nez v0, :cond_13

    #@8
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    #@a
    array-length v0, v0

    #@b
    if-eq v0, v5, :cond_13

    #@d
    new-instance v0, Ljava/lang/AssertionError;

    #@f
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@12
    throw v0

    #@13
    .line 190
    :cond_13
    sget-boolean v0, Landroid/webkit/DeviceOrientationService;->$assertionsDisabled:Z

    #@15
    if-nez v0, :cond_2d

    #@17
    const-string v0, "WebViewCoreThread"

    #@19
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v0

    #@25
    if-nez v0, :cond_2d

    #@27
    new-instance v0, Ljava/lang/AssertionError;

    #@29
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@2c
    throw v0

    #@2d
    .line 193
    :cond_2d
    iget-boolean v0, p0, Landroid/webkit/DeviceOrientationService;->mIsRunning:Z

    #@2f
    if-nez v0, :cond_32

    #@31
    .line 219
    :cond_31
    :goto_31
    return-void

    #@32
    .line 197
    :cond_32
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    #@34
    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    #@37
    move-result v0

    #@38
    packed-switch v0, :pswitch_data_8e

    #@3b
    .line 217
    sget-boolean v0, Landroid/webkit/DeviceOrientationService;->$assertionsDisabled:Z

    #@3d
    if-nez v0, :cond_31

    #@3f
    new-instance v0, Ljava/lang/AssertionError;

    #@41
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@44
    throw v0

    #@45
    .line 199
    :pswitch_45
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mGravityVector:[F

    #@47
    if-nez v0, :cond_4d

    #@49
    .line 200
    new-array v0, v5, [F

    #@4b
    iput-object v0, p0, Landroid/webkit/DeviceOrientationService;->mGravityVector:[F

    #@4d
    .line 202
    :cond_4d
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mGravityVector:[F

    #@4f
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    #@51
    aget v1, v1, v2

    #@53
    aput v1, v0, v2

    #@55
    .line 203
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mGravityVector:[F

    #@57
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    #@59
    aget v1, v1, v3

    #@5b
    aput v1, v0, v3

    #@5d
    .line 204
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mGravityVector:[F

    #@5f
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    #@61
    aget v1, v1, v4

    #@63
    aput v1, v0, v4

    #@65
    .line 205
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->getOrientationUsingGetRotationMatrix()V

    #@68
    goto :goto_31

    #@69
    .line 208
    :pswitch_69
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mMagneticFieldVector:[F

    #@6b
    if-nez v0, :cond_71

    #@6d
    .line 209
    new-array v0, v5, [F

    #@6f
    iput-object v0, p0, Landroid/webkit/DeviceOrientationService;->mMagneticFieldVector:[F

    #@71
    .line 211
    :cond_71
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mMagneticFieldVector:[F

    #@73
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    #@75
    aget v1, v1, v2

    #@77
    aput v1, v0, v2

    #@79
    .line 212
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mMagneticFieldVector:[F

    #@7b
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    #@7d
    aget v1, v1, v3

    #@7f
    aput v1, v0, v3

    #@81
    .line 213
    iget-object v0, p0, Landroid/webkit/DeviceOrientationService;->mMagneticFieldVector:[F

    #@83
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    #@85
    aget v1, v1, v4

    #@87
    aput v1, v0, v4

    #@89
    .line 214
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->getOrientationUsingGetRotationMatrix()V

    #@8c
    goto :goto_31

    #@8d
    .line 197
    nop

    #@8e
    :pswitch_data_8e
    .packed-switch 0x1
        :pswitch_45
        :pswitch_69
    .end packed-switch
.end method

.method public resume()V
    .registers 2

    #@0
    .prologue
    .line 72
    iget-boolean v0, p0, Landroid/webkit/DeviceOrientationService;->mIsRunning:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 73
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->registerForSensors()V

    #@7
    .line 75
    :cond_7
    return-void
.end method

.method public start()V
    .registers 2

    #@0
    .prologue
    .line 56
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/webkit/DeviceOrientationService;->mIsRunning:Z

    #@3
    .line 57
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->registerForSensors()V

    #@6
    .line 58
    return-void
.end method

.method public stop()V
    .registers 2

    #@0
    .prologue
    .line 61
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/webkit/DeviceOrientationService;->mIsRunning:Z

    #@3
    .line 62
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->unregisterFromSensors()V

    #@6
    .line 63
    return-void
.end method

.method public suspend()V
    .registers 2

    #@0
    .prologue
    .line 66
    iget-boolean v0, p0, Landroid/webkit/DeviceOrientationService;->mIsRunning:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 67
    invoke-direct {p0}, Landroid/webkit/DeviceOrientationService;->unregisterFromSensors()V

    #@7
    .line 69
    :cond_7
    return-void
.end method
