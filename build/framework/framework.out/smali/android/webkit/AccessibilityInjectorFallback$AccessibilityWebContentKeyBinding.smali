.class final Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;
.super Ljava/lang/Object;
.source "AccessibilityInjectorFallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/AccessibilityInjectorFallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AccessibilityWebContentKeyBinding"
.end annotation


# static fields
.field private static final ACTION_MASK:I = -0x1000000

.field private static final ACTION_OFFSET:I = 0x18

.field private static final FIRST_ARGUMENT_MASK:I = 0xff0000

.field private static final FIRST_ARGUMENT_OFFSET:I = 0x10

.field private static final KEY_CODE_MASK:J = 0xffffffffL

.field private static final KEY_CODE_OFFSET:I = 0x0

.field private static final MODIFIERS_MASK:J = 0xfffffff00000000L

.field private static final MODIFIERS_OFFSET:I = 0x20

.field private static final SECOND_ARGUMENT_MASK:I = 0xff00

.field private static final SECOND_ARGUMENT_OFFSET:I = 0x8

.field private static final THIRD_ARGUMENT_MASK:I = 0xff

.field private static final THIRD_ARGUMENT_OFFSET:I


# instance fields
.field private final mActionSequence:[I

.field private final mKeyCodeAndModifiers:J


# direct methods
.method public constructor <init>(J[I)V
    .registers 4
    .parameter "keyCodeAndModifiers"
    .parameter "actionSequence"

    #@0
    .prologue
    .line 555
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 556
    iput-wide p1, p0, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->mKeyCodeAndModifiers:J

    #@5
    .line 557
    iput-object p3, p0, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->mActionSequence:[I

    #@7
    .line 558
    return-void
.end method


# virtual methods
.method public getAction(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 519
    iget-object v0, p0, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->mActionSequence:[I

    #@2
    aget v0, v0, p1

    #@4
    return v0
.end method

.method public getActionCode(I)I
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 526
    iget-object v0, p0, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->mActionSequence:[I

    #@2
    aget v0, v0, p1

    #@4
    const/high16 v1, -0x100

    #@6
    and-int/2addr v0, v1

    #@7
    shr-int/lit8 v0, v0, 0x18

    #@9
    return v0
.end method

.method public getActionCount()I
    .registers 2

    #@0
    .prologue
    .line 512
    iget-object v0, p0, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->mActionSequence:[I

    #@2
    array-length v0, v0

    #@3
    return v0
.end method

.method public getFirstArgument(I)I
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 533
    iget-object v0, p0, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->mActionSequence:[I

    #@2
    aget v0, v0, p1

    #@4
    const/high16 v1, 0xff

    #@6
    and-int/2addr v0, v1

    #@7
    shr-int/lit8 v0, v0, 0x10

    #@9
    return v0
.end method

.method public getKeyCode()I
    .registers 5

    #@0
    .prologue
    .line 498
    iget-wide v0, p0, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->mKeyCodeAndModifiers:J

    #@2
    const-wide v2, 0xffffffffL

    #@7
    and-long/2addr v0, v2

    #@8
    const/4 v2, 0x0

    #@9
    shr-long/2addr v0, v2

    #@a
    long-to-int v0, v0

    #@b
    return v0
.end method

.method public getModifiers()I
    .registers 5

    #@0
    .prologue
    .line 505
    iget-wide v0, p0, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->mKeyCodeAndModifiers:J

    #@2
    const-wide v2, 0xfffffff00000000L

    #@7
    and-long/2addr v0, v2

    #@8
    const/16 v2, 0x20

    #@a
    shr-long/2addr v0, v2

    #@b
    long-to-int v0, v0

    #@c
    return v0
.end method

.method public getSecondArgument(I)I
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 540
    iget-object v0, p0, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->mActionSequence:[I

    #@2
    aget v0, v0, p1

    #@4
    const v1, 0xff00

    #@7
    and-int/2addr v0, v1

    #@8
    shr-int/lit8 v0, v0, 0x8

    #@a
    return v0
.end method

.method public getThirdArgument(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 547
    iget-object v0, p0, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->mActionSequence:[I

    #@2
    aget v0, v0, p1

    #@4
    and-int/lit16 v0, v0, 0xff

    #@6
    shr-int/lit8 v0, v0, 0x0

    #@8
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 562
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 563
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string/jumbo v3, "modifiers: "

    #@8
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    .line 564
    invoke-virtual {p0}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getModifiers()I

    #@e
    move-result v3

    #@f
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    .line 565
    const-string v3, ", keyCode: "

    #@14
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 566
    invoke-virtual {p0}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getKeyCode()I

    #@1a
    move-result v3

    #@1b
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    .line 567
    const-string v3, ", actions["

    #@20
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 568
    const/4 v2, 0x0

    #@24
    .local v2, i:I
    invoke-virtual {p0}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getActionCount()I

    #@27
    move-result v1

    #@28
    .local v1, count:I
    :goto_28
    if-ge v2, v1, :cond_6c

    #@2a
    .line 569
    const-string/jumbo v3, "{actionCode"

    #@2d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    .line 570
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    .line 571
    const-string v3, ": "

    #@35
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 572
    invoke-virtual {p0, v2}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getActionCode(I)I

    #@3b
    move-result v3

    #@3c
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    .line 573
    const-string v3, ", firstArgument: "

    #@41
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    .line 574
    invoke-virtual {p0, v2}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getFirstArgument(I)I

    #@47
    move-result v3

    #@48
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    .line 575
    const-string v3, ", secondArgument: "

    #@4d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    .line 576
    invoke-virtual {p0, v2}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getSecondArgument(I)I

    #@53
    move-result v3

    #@54
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    .line 577
    const-string v3, ", thirdArgument: "

    #@59
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    .line 578
    invoke-virtual {p0, v2}, Landroid/webkit/AccessibilityInjectorFallback$AccessibilityWebContentKeyBinding;->getThirdArgument(I)I

    #@5f
    move-result v3

    #@60
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    .line 579
    const-string/jumbo v3, "}"

    #@66
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    .line 568
    add-int/lit8 v2, v2, 0x1

    #@6b
    goto :goto_28

    #@6c
    .line 581
    :cond_6c
    const-string v3, "]"

    #@6e
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    .line 582
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    return-object v3
.end method
