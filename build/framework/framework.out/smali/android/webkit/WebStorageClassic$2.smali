.class Landroid/webkit/WebStorageClassic$2;
.super Landroid/os/Handler;
.source "WebStorageClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/WebStorageClassic;->createHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/WebStorageClassic;


# direct methods
.method constructor <init>(Landroid/webkit/WebStorageClassic;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 98
    iput-object p1, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 15
    .parameter "msg"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 101
    iget v10, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v10, :pswitch_data_f8

    #@6
    .line 161
    :goto_6
    return-void

    #@7
    .line 103
    :pswitch_7
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    check-cast v9, Landroid/webkit/WebStorage$Origin;

    #@b
    .line 104
    .local v9, website:Landroid/webkit/WebStorage$Origin;
    invoke-virtual {v9}, Landroid/webkit/WebStorage$Origin;->getOrigin()Ljava/lang/String;

    #@e
    move-result-object v10

    #@f
    invoke-virtual {v9}, Landroid/webkit/WebStorage$Origin;->getQuota()J

    #@12
    move-result-wide v11

    #@13
    invoke-static {v10, v11, v12}, Landroid/webkit/WebStorageClassic;->access$000(Ljava/lang/String;J)V

    #@16
    goto :goto_6

    #@17
    .line 109
    .end local v9           #website:Landroid/webkit/WebStorage$Origin;
    :pswitch_17
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19
    check-cast v9, Landroid/webkit/WebStorage$Origin;

    #@1b
    .line 110
    .restart local v9       #website:Landroid/webkit/WebStorage$Origin;
    invoke-virtual {v9}, Landroid/webkit/WebStorage$Origin;->getOrigin()Ljava/lang/String;

    #@1e
    move-result-object v10

    #@1f
    invoke-static {v10}, Landroid/webkit/WebStorageClassic;->access$100(Ljava/lang/String;)V

    #@22
    goto :goto_6

    #@23
    .line 114
    .end local v9           #website:Landroid/webkit/WebStorage$Origin;
    :pswitch_23
    invoke-static {}, Landroid/webkit/WebStorageClassic;->access$200()V

    #@26
    goto :goto_6

    #@27
    .line 118
    :pswitch_27
    iget-object v10, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@29
    invoke-static {v10}, Landroid/webkit/WebStorageClassic;->access$300(Landroid/webkit/WebStorageClassic;)V

    #@2c
    .line 119
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2e
    check-cast v0, Landroid/webkit/ValueCallback;

    #@30
    .line 120
    .local v0, callback:Landroid/webkit/ValueCallback;
    new-instance v2, Ljava/util/HashMap;

    #@32
    iget-object v10, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@34
    invoke-static {v10}, Landroid/webkit/WebStorageClassic;->access$400(Landroid/webkit/WebStorageClassic;)Ljava/util/Map;

    #@37
    move-result-object v10

    #@38
    invoke-direct {v2, v10}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@3b
    .line 121
    .local v2, origins:Ljava/util/Map;
    new-instance v8, Ljava/util/HashMap;

    #@3d
    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    #@40
    .line 122
    .local v8, values:Ljava/util/Map;
    const-string v10, "callback"

    #@42
    invoke-interface {v8, v10, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@45
    .line 123
    const-string/jumbo v10, "origins"

    #@48
    invoke-interface {v8, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4b
    .line 124
    iget-object v10, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@4d
    const/4 v11, 0x0

    #@4e
    invoke-static {v12, v11, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@51
    move-result-object v11

    #@52
    invoke-static {v10, v11}, Landroid/webkit/WebStorageClassic;->access$500(Landroid/webkit/WebStorageClassic;Landroid/os/Message;)V

    #@55
    goto :goto_6

    #@56
    .line 128
    .end local v0           #callback:Landroid/webkit/ValueCallback;
    .end local v2           #origins:Ljava/util/Map;
    .end local v8           #values:Ljava/util/Map;
    :pswitch_56
    iget-object v10, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@58
    invoke-static {v10}, Landroid/webkit/WebStorageClassic;->access$300(Landroid/webkit/WebStorageClassic;)V

    #@5b
    .line 129
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5d
    check-cast v8, Ljava/util/Map;

    #@5f
    .line 130
    .restart local v8       #values:Ljava/util/Map;
    const-string/jumbo v10, "origin"

    #@62
    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@65
    move-result-object v1

    #@66
    check-cast v1, Ljava/lang/String;

    #@68
    .line 131
    .local v1, origin:Ljava/lang/String;
    const-string v10, "callback"

    #@6a
    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6d
    move-result-object v0

    #@6e
    check-cast v0, Landroid/webkit/ValueCallback;

    #@70
    .line 132
    .restart local v0       #callback:Landroid/webkit/ValueCallback;
    iget-object v10, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@72
    invoke-static {v10}, Landroid/webkit/WebStorageClassic;->access$400(Landroid/webkit/WebStorageClassic;)Ljava/util/Map;

    #@75
    move-result-object v10

    #@76
    invoke-interface {v10, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@79
    move-result-object v9

    #@7a
    check-cast v9, Landroid/webkit/WebStorage$Origin;

    #@7c
    .line 133
    .restart local v9       #website:Landroid/webkit/WebStorage$Origin;
    new-instance v5, Ljava/util/HashMap;

    #@7e
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    #@81
    .line 134
    .local v5, retValues:Ljava/util/Map;
    const-string v10, "callback"

    #@83
    invoke-interface {v5, v10, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@86
    .line 135
    if-eqz v9, :cond_97

    #@88
    .line 136
    invoke-virtual {v9}, Landroid/webkit/WebStorage$Origin;->getUsage()J

    #@8b
    move-result-wide v6

    #@8c
    .line 137
    .local v6, usage:J
    const-string/jumbo v10, "usage"

    #@8f
    new-instance v11, Ljava/lang/Long;

    #@91
    invoke-direct {v11, v6, v7}, Ljava/lang/Long;-><init>(J)V

    #@94
    invoke-interface {v5, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@97
    .line 139
    .end local v6           #usage:J
    :cond_97
    iget-object v10, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@99
    const/4 v11, 0x1

    #@9a
    invoke-static {v12, v11, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@9d
    move-result-object v11

    #@9e
    invoke-static {v10, v11}, Landroid/webkit/WebStorageClassic;->access$500(Landroid/webkit/WebStorageClassic;Landroid/os/Message;)V

    #@a1
    goto/16 :goto_6

    #@a3
    .line 143
    .end local v0           #callback:Landroid/webkit/ValueCallback;
    .end local v1           #origin:Ljava/lang/String;
    .end local v5           #retValues:Ljava/util/Map;
    .end local v8           #values:Ljava/util/Map;
    .end local v9           #website:Landroid/webkit/WebStorage$Origin;
    :pswitch_a3
    iget-object v10, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@a5
    invoke-static {v10}, Landroid/webkit/WebStorageClassic;->access$300(Landroid/webkit/WebStorageClassic;)V

    #@a8
    .line 144
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@aa
    check-cast v8, Ljava/util/Map;

    #@ac
    .line 145
    .restart local v8       #values:Ljava/util/Map;
    const-string/jumbo v10, "origin"

    #@af
    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b2
    move-result-object v1

    #@b3
    check-cast v1, Ljava/lang/String;

    #@b5
    .line 146
    .restart local v1       #origin:Ljava/lang/String;
    const-string v10, "callback"

    #@b7
    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@ba
    move-result-object v0

    #@bb
    check-cast v0, Landroid/webkit/ValueCallback;

    #@bd
    .line 147
    .restart local v0       #callback:Landroid/webkit/ValueCallback;
    iget-object v10, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@bf
    invoke-static {v10}, Landroid/webkit/WebStorageClassic;->access$400(Landroid/webkit/WebStorageClassic;)Ljava/util/Map;

    #@c2
    move-result-object v10

    #@c3
    invoke-interface {v10, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c6
    move-result-object v9

    #@c7
    check-cast v9, Landroid/webkit/WebStorage$Origin;

    #@c9
    .line 148
    .restart local v9       #website:Landroid/webkit/WebStorage$Origin;
    new-instance v5, Ljava/util/HashMap;

    #@cb
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    #@ce
    .line 149
    .restart local v5       #retValues:Ljava/util/Map;
    const-string v10, "callback"

    #@d0
    invoke-interface {v5, v10, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d3
    .line 150
    if-eqz v9, :cond_e4

    #@d5
    .line 151
    invoke-virtual {v9}, Landroid/webkit/WebStorage$Origin;->getQuota()J

    #@d8
    move-result-wide v3

    #@d9
    .line 152
    .local v3, quota:J
    const-string/jumbo v10, "quota"

    #@dc
    new-instance v11, Ljava/lang/Long;

    #@de
    invoke-direct {v11, v3, v4}, Ljava/lang/Long;-><init>(J)V

    #@e1
    invoke-interface {v5, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e4
    .line 154
    .end local v3           #quota:J
    :cond_e4
    iget-object v10, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@e6
    const/4 v11, 0x2

    #@e7
    invoke-static {v12, v11, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@ea
    move-result-object v11

    #@eb
    invoke-static {v10, v11}, Landroid/webkit/WebStorageClassic;->access$500(Landroid/webkit/WebStorageClassic;Landroid/os/Message;)V

    #@ee
    goto/16 :goto_6

    #@f0
    .line 158
    .end local v0           #callback:Landroid/webkit/ValueCallback;
    .end local v1           #origin:Ljava/lang/String;
    .end local v5           #retValues:Ljava/util/Map;
    .end local v8           #values:Ljava/util/Map;
    .end local v9           #website:Landroid/webkit/WebStorage$Origin;
    :pswitch_f0
    iget-object v10, p0, Landroid/webkit/WebStorageClassic$2;->this$0:Landroid/webkit/WebStorageClassic;

    #@f2
    invoke-static {v10}, Landroid/webkit/WebStorageClassic;->access$300(Landroid/webkit/WebStorageClassic;)V

    #@f5
    goto/16 :goto_6

    #@f7
    .line 101
    nop

    #@f8
    :pswitch_data_f8
    .packed-switch 0x0
        :pswitch_f0
        :pswitch_7
        :pswitch_17
        :pswitch_23
        :pswitch_27
        :pswitch_56
        :pswitch_a3
    .end packed-switch
.end method
